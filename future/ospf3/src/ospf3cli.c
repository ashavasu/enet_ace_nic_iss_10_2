/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: ospf3cli.c,v 1.98 2018/02/07 09:32:29 siva Exp $
 *
 * Description: This file contains procedures related to configure
 *         Ospfv3/FsOspfv3 MIB objects through command line
 *         interface.
 *
 *******************************************************************/
#ifndef __OSPF3CLI_C__
#define __OSPF3CLI_C__

#include "o3inc.h"
#include "ospf3cli.h"
#include "ospf3lw.h"
#include "ospf3wr.h"
#include "fsos3lw.h"
#include "fsos3wr.h"
#include "fsmisolw.h"
#include "fsmio3lw.h"
#include "ospf3cli.h"
#include "fso3telw.h"
#include  "fsmitolw.h"
/* Cli global variable */

/* Cli global variable */
tNbrsInIf           gaNbrsInIf[MAX_OSPFV3_NBRS];
tOspfv3AreaInfo     gaOspfv3AreaInfo[MAX_OSPFV3_AREAS];

tV3OsAreaId         V3_LS_ADV_ROUTER_ID;

typedef struct
{
    UINT4               au4LsTypeCnt[11];
    INT4                au4LsTypeMaxAgedCnt[11];
} tLsdbCounter;

/* Prototype For Configuration Functions*/
PRIVATE INT4        V3OspfCliGetContext
PROTO ((tCliHandle CliHandle, UINT4 u4Command, UINT4 *pu4OspfCxtId,
        UINT4 *pu4IfIndex, UINT2 *pu2ShowCmdFlag));

PRIVATE INT1        V3OspfCliConfigIfAreaId
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId, UINT4 u4IfIndex, UINT1 u1Set,
        INT4 i4IfInstId));

PRIVATE INT1        V3OspfCliConfigIfDemand
PROTO ((tCliHandle cliHandle, INT4 i4DemandCkt, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfRetransInterval
PROTO ((tCliHandle cliHandle, INT4 i4RetransInterval, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfTransitDelay
PROTO ((tCliHandle cliHandle, INT4 i4TransitDelay, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfRtrPriority
PROTO ((tCliHandle cliHandle, INT4 i4RtrPriority, INT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfHelloInterval
PROTO ((tCliHandle cliHandle, INT4 i4HelloInterval, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfRtrDeadInterval
PROTO ((tCliHandle cliHandle, INT4 i4RtrDeadInterval, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfPollInterval
PROTO ((tCliHandle cliHandle, INT4 i4PollInterval, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfMetricValue
PROTO ((tCliHandle cliHandle, INT4 i4MetricValue, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfType
PROTO ((tCliHandle cliHandle, INT4 i4Type, UINT4 u4IfIndex, UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigIfNbmaNbr
PROTO ((tCliHandle cliHandle, UINT4 u4IfIndex, tIp6Addr ifIpAddr,
        INT4 i4NbrPriority, UINT4 u4BitMask, UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigIfPassive
PROTO ((tCliHandle cliHandle, INT4 i4Passive, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigDefaultPassiveIface
PROTO ((tCliHandle cliHandle, INT4 i4DefPassive));

PRIVATE INT1        V3OspfCliConfigIfDemandNbrProbe
PROTO ((tCliHandle cliHandle, INT4 i4DemandNbrProbe, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfNbrProbeRetxLimit
PROTO ((tCliHandle cliHandle, INT4 i4DemandNbrProbeRetxLimit, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigIfNbrProbeInterval
PROTO ((tCliHandle cliHandle, INT4 i4DemandNbrProbeRetxLimit, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliConfigAdminStatInCxt
PROTO ((tCliHandle CliHandle, INT4 i4AdminStat, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliConfigAreaType
PROTO ((tCliHandle cliHandle, INT4 i4ImportAsExtern, INT4 i4AreaSummary,
        UINT4 u4AreaId, UINT4 u4CmdStatus, INT4 i4StubMetric,
        INT4 i4MetricType));

PRIVATE INT1        V3OspfCliConfigNSSAStabilityIntval
PROTO ((tCliHandle cliHandle, UINT4 i4Interval, UINT4 u4AreaId));

PRIVATE INT1        V3OspfCliConfigNSSATranslatorRole
PROTO ((tCliHandle cliHandle, INT4 i4TransRole, UINT4 u4AreaId));

PRIVATE INT1        V3OspfCliConfigSpfTime
PROTO ((tCliHandle cliHandle, INT4 i4SpfDelay, INT4 i4SpfHoldTime));

PRIVATE INT4        V3OspfCliSetDefaultOriginate
PROTO ((tCliHandle CliHandle, INT4 i4Metric, INT4 i4MetricType,
        tIp6Addr FutOspfv3ExtRouteDest, UINT4 u4FutOspfv3ExtRoutePfxLength,
        tIp6Addr FutOspfv3ExtRouteNextHop));

PRIVATE INT4        V3OspfCliDelDefaultOriginate
PROTO ((tCliHandle CliHandle, INT4 i4Metric, INT4 i4MetricType,
        tIp6Addr FutOspfv3ExtRouteDest, UINT4 u4FutOspfv3ExtRoutePfxLength,
        tIp6Addr FutOspfv3ExtRouteNextHop));

PRIVATE INT1        V3OspfCliConfigRouterId
PROTO ((tCliHandle cliHandle, UINT4 u4RouterId));

PRIVATE INT1        V3OspfCliDelRouterId
PROTO ((tCliHandle cliHandle, UINT4 u4OspfCxtId));

PRIVATE INT1        V3OspfCliConfigABRType
PROTO ((tCliHandle cliHandle, INT4 i4AbrType));

PRIVATE INT1        V3OspfCliConfigStubMetric
PROTO ((tCliHandle cliHandle, INT4 i4StubMetric, UINT4 u4AreaId));

PRIVATE INT1        V3OspfCliConfigStubMetricType
PROTO ((tCliHandle cliHandle, INT4 i4StubMetricType, UINT4 u4AreaId));

PRIVATE INT1        V3OspfCliConfigVirtualLink
PROTO ((tCliHandle cliHandle, UINT4 u4RouterId, UINT4 u4AreaId,
        tV3OsInterface * pInterface, UINT4 u4BitMask, UINT1 u1Set,
        UINT4 u4OspfCxtId, INT4 i4AuthkeyId, UINT1 *au1AuthKey));

PRIVATE INT1        V3OspfCliConfigASBdrRtrStatus
PROTO ((tCliHandle cliHandle, INT4 i4ASBRStatus));

PRIVATE INT1        V3OspfCliConfigAreaRange
PROTO ((tCliHandle cliHandle, tIp6Addr summPrefix, UINT1 u1PrefixLen,
        UINT4 u4AreaId, INT4 i4LsaType, INT4 i4SummEffect, INT4 i4Tag,
        UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigAsExternalRangeInCxt
PROTO ((tCliHandle cliHandle, tV3OspfCxt * pV3OspfCxt, tIp6Addr summPrefix,
        UINT1 u1PrefixLen, UINT4 u4AreaId, INT4 i4SummEffect, INT4 i4Trans,
        UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigRedistribute
PROTO ((tCliHandle cliHandle, INT4 i4RedistProtoMask, UINT1 u1Set,
        UINT1 *pu1RouteMapName, INT4 i4MetricValue, INT4 i4MetricType));
#ifdef ROUTEMAP_WANTED
PRIVATE INT1        V3OspfCliSetDistribute
PROTO ((tCliHandle cliHandle, UINT1 *pu1RMapName, UINT1 u1Status));
#endif

PRIVATE INT1        V3OspfCliConfigHost
PROTO ((tCliHandle cliHandle, tIp6Addr hostAddr, INT4 i4Metric,
        UINT4 u4AreaId, UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigDeleteArea
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId));

PRIVATE INT1        V3OspfCliConfigNssaAsbrDfRtTrans
PROTO ((tCliHandle cliHandle, INT4 i4Status));

PRIVATE INT1        V3OspfCliConfigRRDRoute
PROTO ((tCliHandle cliHandle, tIp6Addr routeDest, INT4 i4RoutePfxLen,
        INT4 i4RouteMetric, INT4 i4RouteMetricType, INT4 i4RouteTag,
        UINT1 u1TagFlag, UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigTraceLevel
PROTO ((tCliHandle cliHandle, INT4 i4TraceLevel,
        INT4 i4ExtTraceLevel, UINT1 u1Set));

PRIVATE INT1        V3OspfCliConfigExtAreaLsdbLimit
PROTO ((tCliHandle cliHandle, INT4 i4ExtLsdbLimit));

PRIVATE INT1        V3OspfCliConfigExitOverflowInterval
PROTO ((tCliHandle cliHandle, INT4 i4Interval));

PRIVATE INT1        V3OspfCliConfigDemandExtensions
PROTO ((tCliHandle cliHandle, INT4 i4DemandExt));

PRIVATE INT1        V3OspfCliConfigReferenceBandwidth
PROTO ((tCliHandle cliHandle, UINT4 i4RefBW));

PRIVATE INT4        V3OspfCliShowDB
PROTO ((tV3OsLsaInfo * pLsaInfo, tCliHandle cliHandle, UINT2 u2BitMask));

PRIVATE INT4        V3OspfCliDisplayLsa
PROTO ((tCliHandle cliHandle, tV3OsLsaInfo * pLsaInfo, UINT2 u2BitMask));

PRIVATE INT4        V3OspfCliDisplayLsaSummary
PROTO ((tCliHandle cliHandle, tV3OsLsaInfo * pLsaInfo, UINT2 u2BitMask));

PRIVATE INT1        V3OspfCliShowInterfaceInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT1 u1ShowIfOption,
        UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliShowNeighborInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT1 u1ShowNbrOption,
        UINT4 u4NeighborId, UINT4 u4IfIndex));

PRIVATE INT1        V3OspfCliShowRequestListInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT1 u1ShowLsaOption,
        UINT4 u4IfIndex, UINT4 u4NbrId));

PRIVATE INT1        V3OspfCliShowRxmtListInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT1 u1ShowLsaOption,
        UINT4 u4IfIndex, UINT4 u4NbrId));

PRIVATE INT1        V3OspfCliShowVirtualIfInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowBorderRouterInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowSummaryAddressInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowExtSummaryAddressInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1 V3OspfCliShowOspfInfoInCxt PROTO ((tCliHandle cliHandle,
                                                UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowDatabaseInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT4 u4ShowLsaType,
        UINT4 u4ShowType, UINT1 u1AreaFlag, UINT4 u4AreaId,
        UINT4 u4LsaDispType));

PRIVATE INT1        V3OspfCliShowRoutingTableInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowAreaInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowHostInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1        V3OspfCliShowRedistRouteCfgTblInCxt
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT4        V3OspfCliSetStaggeringStatus
PROTO ((tCliHandle CliHandle, UINT4 u4StaggingStatus));

PRIVATE INT4        V3OspfCliSetStaggeringInterval
PROTO ((tCliHandle CliHandle, UINT4 u4StaggingInterval));

PRIVATE INT4        V3OspfCliGetCxtIdFromIfIndex
PROTO ((tCliHandle CliContext, UINT4 u4IfIndex, UINT4 *pu4OspfCxtId));

PRIVATE INT4        V3OspfCliGetCxtIdFromCxtName
PROTO ((tCliHandle CliContext, UINT1 *pu1OspfCxtName, UINT4 *pu4OspfCxtId));

PRIVATE INT4        V3OspfCliConfigRestartSupport
PROTO ((tCliHandle CliHandle, INT4 i4RestartSupport));

PRIVATE INT4        V3OspfCliConfigGracePeriod
PROTO ((tCliHandle CliHandle, INT4 i4GracePeriod));

PRIVATE INT4        V3OspfCliConfigGrAckState
PROTO ((tCliHandle CliHandle, INT4 i4GrAckState));

PRIVATE INT4        V3OspfCliConfigGraceRetransCount
PROTO ((tCliHandle CliHandle, INT4 i4GrLsaRetransCount));

PRIVATE INT4        V3OspfCliConfigRestartReason
PROTO ((tCliHandle CliHandle, INT4 i4GrRestartReason));

PRIVATE INT4        V3OspfCliConfigHelperSupport
PROTO ((tCliHandle CliHandle, INT4 i4HelperSupport, UINT1 u1HelperFlag));

PRIVATE INT4        V3OspfCliConfigHelperGraceLimitPeriod
PROTO ((tCliHandle CliHandle, INT4 i4GraceLimitPeriod));

PRIVATE INT4        V3OspfCliConfigStrictLsaCheck
PROTO ((tCliHandle CliHandle, INT4 i4StrictLsaCheck));

PRIVATE INT4        V3OspfCliSetBfdAdmState
PROTO ((tCliHandle CliHandle, INT4 i4BfdAdminStatus));

PRIVATE INT4        V3OspfCliSetBfdStatusAllInt
PROTO ((tCliHandle CliHandle, INT4 i4BfdAllIntStatus));

PRIVATE INT4        V3OspfCliSetBfdStatusSpecificInt
PROTO ((tCliHandle CliHandle, UINT4 u4InterfaceIndex,
        INT4 i4BfdSpecificIntStatus));

PRIVATE INT4 V3OspfCliShowRedStatus PROTO ((tCliHandle cliHandle));

PRIVATE INT4        V3OspfCliSetIfCryptoAuthType
PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4CryptoAuthType));

PRIVATE INT4        V3OspfCliSetIfAuthKeyStartAccept
PROTO ((tCliHandle CliHandle, INT4 KeyId, UINT1 *pu1StartAcceptTime,
        UINT4 u4OspfCxtId, UINT4 u4IfIndex));

PRIVATE INT4        V3OspfCliSetIfAuthKeyStartGenerate
PROTO ((tCliHandle CliHandle, INT4 KeyId, UINT1 *pu1StartGenerateTime,
        UINT4 u4OspfCxtId, UINT4 u4IfIndex));

PRIVATE INT4        V3OspfCliSetIfAuthKeyStopAccept
PROTO ((tCliHandle CliHandle, INT4 KeyId, UINT1 *pu1StopAcceptTime,
        UINT4 u4OspfCxtId, UINT4 u4IfIndex));

PRIVATE INT4        V3OspfCliSetIfAuthKeyStopGenerate
PROTO ((tCliHandle CliHandle, INT4 KeyId, UINT1 *pu1StopGenerateTime,
        UINT4 u4OspfCxtId, UINT4 u4IfIndex));

PRIVATE INT4        V3OspfCliSetIfAuthKey
PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IfIndex,
        INT4 i4AuthKeyId, UINT1 *pu1AuthKey, INT4 i4KeyStatus));

PRIVATE INT1        V3OspfCliSetIfAuthMode
PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AuthMode));

PRIVATE INT4        V3UtilOspfconvertToOctet
PROTO ((tCliHandle CliHandle, UINT1 *pu1StartAcceptTime,
        tSNMP_OCTET_STRING_TYPE * KeyStartTime));

PRIVATE INT1        V3OspfCliSetIfShowAuthPktStats
PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex));

PRIVATE VOID        V3PrintKeyData
PROTO ((tCliHandle CliHandle, tAuthkeyInfo * pAuthkeyInfo));

PRIVATE INT4        V3OspfIfAuthInfoInCxt
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));

PRIVATE INT1        V3OspfCliSetIfVirtAuthKeyStopAccept
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId,
        UINT4 u4NbrId, INT4 KeyId, UINT1 *pu1StopAcceptTime,
        UINT4 u4OspfCxtId));

PRIVATE INT1        V3OspfCliSetIfVirtAuthKeyStartAccept
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId,
        UINT4 u4NbrId, INT4 KeyId, UINT1 *pu1StartAcceptTime,
        UINT4 u4OspfCxtId));

PRIVATE INT1        V3OspfCliSetIfVirtAuthKeyStopGen
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId,
        UINT4 u4NbrId, INT4 KeyId, UINT1 *pu1StopGenTime, UINT4 u4OspfCxtId));

PRIVATE INT1        V3OspfCliSetIfVirtAuthKeyStartGen
PROTO ((tCliHandle cliHandle, UINT4 u4AreaId,
        UINT4 u4NbrId, INT4 KeyId, UINT1 *pu1StartGenTime, UINT4 u4OspfCxtId));

PRIVATE VOID
     
     
     
     V3OspfCliAuthInterface
PROTO ((tCliHandle cliHandle, tV3OsInterface * pInterface));

PRIVATE VOID V3OspfShowCounters PROTO ((tCliHandle cliHandle));

PRIVATE VOID
 
 
   Ospfv3CliPrintMetricAndMetricType (tCliHandle CliHandle, INT4 i4ProtoIndex);

PRIVATE INT4
    V3CliUserPromptResponse PROTO ((tCliHandle cliHandle, INT1 *pi1Prompt));

PRIVATE INT1
    V3OspfCliClearProcess PROTO ((tCliHandle cliHandle, UINT4 u4ContextId));

PRIVATE INT1
    V3OspfCliShowAreaDataBaseSummaryInCxt PROTO ((tCliHandle cliHandle,
                                                  UINT4 u4ContextId,
                                                  UINT4 u4AreaId,
                                                  tLsdbCounter * LsdbCounter));
PRIVATE INT1        V3OspfCliShowFilterSummaryInfo
PROTO ((tCliHandle cliHandle, UINT4 u4ContextId, UINT4 u4AreaId,
        UINT4 u4LsaDisType));

/******************************************************************************
Function : cli_process_ospfv3_cmd

Descrption : This routine process CLI commands and call corresponding action
        routine.

Input    : cliHandle - Index into Global array maintain by CLI module, provided
       CLI module and used in CliPrintf only.

Output   : NONE 

Returns  : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
cli_process_ospfv3_cmd (tCliHandle cliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tV3OsInterface     *ptmpInterface = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            ifIpAddr;
    tIp6Addr            routeDest;
    tIp6Addr            RouteNextHop;
    tIp6Addr            summPrefix;
    tUtlInAddr          InAddr;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4InterfaceIndex = OSPFV3_INIT_VAL;
    UINT4               u4NbrBitMask = OSPFV3_INIT_VAL;
    UINT4               u4AreaId = OSPFV3_INIT_VAL;
    UINT4               u4NbrId = OSPFV3_INIT_VAL;
    UINT4               u4BitMask = OSPFV3_INIT_VAL;
    UINT4               u4RetxLimit = OSPFV3_INIT_VAL;
    UINT4               u4ErrorCode;
    UINT4               u4SummPrefixLen = OSPFV3_INIT_VAL;
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    UINT4               u4IfIndex = OSPFV3_INIT_VAL;
    UINT2               u2ShowCmdFlag = OSPFV3_FALSE;
    UINT1              *pu1OspfCxtName = NULL;
    UINT4              *au4args[OSPFV3_CLI_MAX_ARGUMENTS];
    UINT1               u1Set = OSPFV3_VAL_NO;
    UINT1               u1TagFlag = OSPFV3_INIT_VAL;
    UINT1               au1TaskName[] = "OSV3";
    UINT1              *pu1TaskName = au1TaskName;
    INT4                i4RespStatus = CLI_SUCCESS;
    INT4                i4Interval = OSPFV3_INIT_VAL;
    INT4                i4Metric = OSPFV3_INIT_VAL;
    INT4                i4MetricType = OSPFV3_INIT_VAL;
    INT4                i4MetricTypeVal = OSPFV3_INIT_VAL;
    INT4                i4MetricValue = OSPFV3_INIT_VAL;
    INT4                i4SpfDelayTime = OSPFV3_INIT_VAL;
    INT4                i4SpfHoldTime = OSPFV3_INIT_VAL;
    INT4                i4SummLsaType = OSPFV3_INIT_VAL;
    INT4                i4SummEffect = OSPFV3_INIT_VAL;
    INT4                i4SummTag = OSPFV3_INIT_VAL;
    INT4                i4IfInstId = OSPFV3_INIT_VAL;
    INT4                i4SummTrans = OSPFV3_INIT_VAL;
    INT4                i4RoutePfxLen = OSPFV3_INIT_VAL;
    INT4                i4TagValue = OSPFV3_INIT_VAL;
    INT1                i1argno = OSPFV3_INIT_VAL;
    INT4                i4RetVal = CLI_SUCCESS;
    INT4                i4Key = OSPFV3_INIT_VAL;
    UINT1               au1AuthKey[OSPFV3_MAX_AUTHKEY_LEN];
    INT4                i4AuthkeyId = -1;
    UINT4               u4AdvrId;
    INT4                i4RetValue = CLI_FAILURE;
    INT4                i4AdminStat = 0;
    INT4                i4ConfigAdminStat = 0;
    INT1                i1RetVal = 0;
    if (IssGetModuleSystemControl (OSPF3_MODULE_ID) == MODULE_SHUTDOWN)
    {
        CliPrintf (cliHandle, "%%OSPFv3 Protocol is shutdown\r\n");
        return CLI_FAILURE;
    }

    if (V3OspfCliGetContext (cliHandle, u4Command, &u4ContextId, &u4IfIndex,
                             &u2ShowCmdFlag) == OSIX_FAILURE)
    {
        CLI_SET_CMD_STATUS (CLI_FAILURE);

        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* second argument is always Interface Name/Index */
    u4InterfaceIndex = va_arg (ap, UINT4);

#ifdef RM_WANTED
    if ((RmRetrieveNodeState () == RM_STANDBY)
        && (u4Command == OSPFV3_CLI_CLEAR_IPV6_OSPF))
    {
        CliPrintf (cliHandle,
                   "\r%% This command can not be executed in Standby Card\n\r");
        va_end (ap);
        return CLI_FAILURE;
    }
#endif

    if (u4Command == OSPFV3_CLI_CLEAR_IPV6_OSPF)
    {
        mmi_printf
            ("\r\nThis command may have an impact on active BFD sessions.\r\n");
        i4RetValue =
            V3CliUserPromptResponse (cliHandle,
                                     (INT1 *)
                                     "Reset OSPF process? [yes]/[no]:");
    }

    if (u2ShowCmdFlag != OSPFV3_TRUE)
    {
        CliRegisterLock (cliHandle, V3OspfLock, V3OspfUnLock);
        if (V3OspfLock () == SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "%%OSPFv3-Semaphore failed , Command not Executed\r\n");
            CliUnRegisterLock (cliHandle);
            va_end (ap);
            return CLI_FAILURE;
        }
    }
    else
    {
        CliRegisterLock (cliHandle, V3OspfTimedLock, V3OspfUnLock);
        if (V3OspfTimedLock () != OSIX_SUCCESS)
        {
            CliPrintf (cliHandle, "\r%s",
                       Ospf3CliErrString[OSPFV3_CLI_WAIT_TIMEOUT_ERR]);
            CliUnRegisterLock (cliHandle);
            va_end (ap);
            return CLI_FAILURE;
        }
    }

    if (((u2ShowCmdFlag == OSPFV3_TRUE || u4Command == OSPFV3_CLI_ADMINSTAT)))
    {
        pu1OspfCxtName = va_arg (ap, UINT1 *);

        if (pu1OspfCxtName != NULL)
        {
            if ((i4RespStatus = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                              pu1OspfCxtName,
                                                              &u4ContextId)) ==
                CLI_FAILURE)
            {
                V3OspfUnLock ();
                CliUnRegisterLock (cliHandle);
                va_end (ap);
                return CLI_FAILURE;
            }

        }
        else
        {
            u4ContextId = OSPFV3_DEFAULT_CXT_ID;
        }
        i1RetVal = nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                                 &i4AdminStat);

    }
    if (u2ShowCmdFlag == OSPFV3_TRUE)
    {
        if (i1RetVal == SNMP_SUCCESS)
        {

            if ((i4AdminStat == OSPFV3_DISABLED)
                && (u4Command != OSPFV3_CLI_ADMINSTAT))
            {

                CliPrintf (cliHandle,
                           "\r %% OSPFV3 process currently not running\r\n");
                V3OspfUnLock ();
                CliUnRegisterLock (cliHandle);
                va_end (ap);
                return CLI_FAILURE;

            }
        }
        else
        {
            CliPrintf (cliHandle,
                       "\r %% OSPFV3 process currently not running\r\n");
            V3OspfUnLock ();
            CliUnRegisterLock (cliHandle);
            va_end (ap);
            return CLI_FAILURE;
        }

    }
    /* If u2ShowCmdFlag is set and for router enable/disable the third
     * argument is always context name */
    /* Walk through the rest of the arguments and store in args array.
     * Store OSPFV3_CLI_MAX_ARGUMENTS arguements at the max.
     */

    while (1)
    {
        au4args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == OSPFV3_CLI_MAX_ARGUMENTS)
        {
            break;
        }
    }
    va_end (ap);

    if (u2ShowCmdFlag != OSPFV3_TRUE)
    {
        if (u4Command == OSPFV3_CLI_ADMINSTAT)
        {
            /* Get the Context Id from the Context name */
            if (pu1OspfCxtName != NULL)
            {
                i4RespStatus = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
            }
            else
            {
                u4ContextId = OSPFV3_DEFAULT_CXT_ID;
            }

            if (i4RespStatus == CLI_FAILURE)
            {
                CliPrintf (cliHandle, "\r%%  OSPFV3 module is not enabled\r\n");
                CliUnRegisterLock (cliHandle);
                V3OspfUnLock ();
                return CLI_FAILURE;
            }
        }

        if ((u4Command != OSPFV3_CLI_ADMINSTAT) &&
            (V3UtilSetContext (u4ContextId) == SNMP_FAILURE))
        {
            CliPrintf (cliHandle, "\r%% Invalid ospf Context Id\r\n");
            CliUnRegisterLock (cliHandle);
            V3OspfUnLock ();
            return CLI_FAILURE;
        }

        switch (u4Command)
        {
            case OSPFV3_CLI_ADMINSTAT:
            {
                i4ConfigAdminStat = CLI_PTR_TO_I4 (au4args[0]);
                if ((i4AdminStat != OSPFV3_ENABLED)
                    && i4ConfigAdminStat == OSPFV3_DISABLED)
                {
                    CliPrintf (cliHandle,
                               "\r%%  OSPFV3 module is not enabled\r\n");
                    CliUnRegisterLock (cliHandle);
                    V3OspfUnLock ();
                    return CLI_FAILURE;
                }

                i4RespStatus = V3OspfCliConfigAdminStatInCxt (cliHandle,
                                                              CLI_PTR_TO_I4
                                                              (au4args[0]),
                                                              u4ContextId);
                break;
            }
            case OSPFV3_CLI_AREAID:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    if (UtlInetAton ((const char *) au4args[0], &InAddr))
                    {
                        u1Set = OSPFV3_VAL_SET;
                    }
                    else
                    {
                        u1Set = OSPFV3_INIT_VAL;
                    }
                    if ((au4args[2]) != NULL)
                    {
                        i4IfInstId = *(INT4 *) (au4args[2]);
                    }
                    i4RespStatus = V3OspfCliConfigIfAreaId (cliHandle,
                                                            *au4args[0],
                                                            u4IfIndex, u1Set,
                                                            i4IfInstId);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfAreaId
                        (cliHandle, (UINT4) 0, u4IfIndex, OSPFV3_VAL_NO,
                         OSPFV3_INIT_VAL);
                }
                break;
            }
            case OSPFV3_CLI_DEMANDCIRCUIT:
            {
                if (CLI_PTR_TO_I4 (au4args[0]) == OSPFV3_VAL_SET)
                {
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }
                i4RespStatus = V3OspfCliConfigIfDemand (cliHandle, u1Set,
                                                        u4IfIndex);
                break;
            }
            case OSPFV3_CLI_RETRANSMITINT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfRetransInterval (cliHandle,
                                                                     *(INT4 *)
                                                                     au4args[0],
                                                                     u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfRetransInterval (cliHandle,
                                                                     OSPFV3_DEF_RXMT_INTERVAL,
                                                                     u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_TRANSITDELAY:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfTransitDelay (cliHandle,
                                                                  *(INT4 *)
                                                                  au4args[0],
                                                                  u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfTransitDelay (cliHandle,
                                                                  OSPFV3_DEF_IF_TRANS_DELAY,
                                                                  u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_PRIORITY:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfRtrPriority (cliHandle,
                                                                 *(INT4 *)
                                                                 au4args[0],
                                                                 (INT4)
                                                                 u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfRtrPriority (cliHandle,
                                                                 OSPFV3_DEF_RTR_PRIORITY,
                                                                 (INT4)
                                                                 u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_HELLOINT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfHelloInterval (cliHandle,
                                                                   *(INT4 *)
                                                                   au4args[0],
                                                                   u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfHelloInterval (cliHandle,
                                                                   OSPFV3_DEF_HELLO_INTERVAL,
                                                                   u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_DEADINT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfRtrDeadInterval (cliHandle,
                                                                     *(INT4 *)
                                                                     au4args[0],
                                                                     u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfRtrDeadInterval
                        (cliHandle, OSPFV3_DEF_RTR_DEAD_INTERVAL, u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_POLLINT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigIfPollInterval
                        (cliHandle, *(INT4 *) au4args[0], u4IfIndex);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfPollInterval
                        (cliHandle, OSPFV3_DEF_POLL_INTERVAL, u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_IF_NETWORK_TYPE:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    u1Set = OSPFV3_VAL_SET;
                    i4RespStatus = V3OspfCliConfigIfType (cliHandle,
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [0]),
                                                          u4IfIndex, u1Set);
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                    i4RespStatus = V3OspfCliConfigIfType (cliHandle, 0,
                                                          u4IfIndex, u1Set);
                }
                break;
            }
            case OSPFV3_CLI_METRIC:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4Metric = *(INT4 *) (au4args[0]);
                }
                else
                {
                    /* Get IfSpeed */
                    NetIpv6GetIfInfo (u4IfIndex, &netIp6IfInfo);

                    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

                    if (pV3OspfCxt != NULL)
                    {
                        i4Metric =
                            (INT4) V3IfGetMetricFromSpeedInCxt (pV3OspfCxt,
                                                                netIp6IfInfo.
                                                                u4IfSpeed,
                                                                netIp6IfInfo.
                                                                u4IfHighSpeed);
                    }
                }
                i4RespStatus = V3OspfCliConfigIfMetricValue (cliHandle,
                                                             i4Metric,
                                                             u4IfIndex);
                break;
            }
            case OSPFV3_CLI_CONF_NBR:
            {
                INET_ATON6 (au4args[0], &ip6Addr);

                OSPFV3_IP6_ADDR_COPY (ifIpAddr, ip6Addr);
                u4NbrBitMask = CLI_PTR_TO_U4 (au4args[2]);

                if (CLI_PTR_TO_I4 (au4args[3]) == OSPFV3_VAL_SET)
                {
                    if (au4args[1] != NULL)
                    {
                        i4RespStatus = V3OspfCliConfigIfNbmaNbr
                            (cliHandle, u4IfIndex, ifIpAddr,
                             CLI_ATOI (au4args[1]), u4NbrBitMask,
                             OSPFV3_VAL_SET);
                    }
                    else
                    {
                        i4RespStatus = V3OspfCliConfigIfNbmaNbr
                            (cliHandle, u4IfIndex, ifIpAddr,
                             OSPFV3_DEF_RTR_PRIORITY, u4NbrBitMask,
                             OSPFV3_VAL_SET);
                    }
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigIfNbmaNbr (cliHandle,
                                                             u4IfIndex,
                                                             ifIpAddr,
                                                             OSPFV3_DEF_RTR_PRIORITY,
                                                             u4NbrBitMask,
                                                             OSPFV3_VAL_NO);

                }
                break;
            }
            case OSPFV3_CLI_IF_PASSIVE:
            {
                u4IfIndex = CLI_GET_IFINDEX ();

                i4RespStatus = V3OspfCliConfigIfPassive (cliHandle,
                                                         CLI_PTR_TO_I4 (au4args
                                                                        [0]),
                                                         u4IfIndex);
                break;
            }
            case OSPFV3_CLI_LINK_SUPPRESS:
            {
                u4IfIndex = CLI_GET_IFINDEX ();
                i4RespStatus = V3OspfCliConfigIfLinkLSASuppression (cliHandle,
                                                                    CLI_PTR_TO_I4
                                                                    (au4args
                                                                     [0]),
                                                                    u4IfIndex);
                break;
            }
            case OSPFV3_CLI_NBR_PROBE:
            {
                i4RespStatus = V3OspfCliConfigIfDemandNbrProbe (cliHandle,
                                                                CLI_PTR_TO_I4
                                                                (au4args[0]),
                                                                u4IfIndex);
                break;
            }
            case OSPFV3_CLI_NBRPROBE_RETRANS_LIMIT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    u4RetxLimit = *(au4args[0]);
                }
                else
                {
                    u4RetxLimit = OSPFV3_DEF_NBR_PROBE_RXMT_LIMIT;
                }
                i4RespStatus = V3OspfCliConfigIfNbrProbeRetxLimit (cliHandle,
                                                                   (INT4)
                                                                   u4RetxLimit,
                                                                   u4IfIndex);
                break;
            }
            case OSPFV3_CLI_NBR_PROBEINT:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4Interval = *(INT4 *) (au4args[0]);
                }
                else
                {
                    i4Interval = OSPFV3_DEF_NBR_PROBE_INTERVAL;
                }
                i4RespStatus = V3OspfCliConfigIfNbrProbeInterval (cliHandle,
                                                                  i4Interval,
                                                                  u4IfIndex);
                break;
            }
            case OSPFV3_CLI_ROUTERID:
            {
                i4RespStatus = V3OspfCliConfigRouterId (cliHandle, *au4args[0]);
                break;
            }
            case OSPFV3_CLI_NO_ROUTERID:
            {
                i4RespStatus = V3OspfCliDelRouterId (cliHandle, u4ContextId);
                break;
            }

            case OSPFV3_CLI_AREATYPE:
            {
                if (CLI_PTR_TO_I4 (au4args[3]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigAreaType
                        (cliHandle,
                         CLI_PTR_TO_I4 (au4args[1]),
                         CLI_PTR_TO_I4 (au4args[2]),
                         CLI_PTR_TO_U4 (*au4args[0]),
                         CLI_PTR_TO_U4 (au4args[1]), 0, 0);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigAreaType
                        (cliHandle,
                         OSPFV3_NORMAL_AREA,
                         OSPFV3_SEND_AREA_SUMMARY, CLI_PTR_TO_U4 (*au4args[0]),
                         CLI_PTR_TO_U4 (au4args[1]), 0, 0);
                }
                break;
            }

            case OSPFV3_CLI_NSSA_CONFIG:
            {
                if ((CLI_PTR_TO_I4 (au4args[4])) == OSPFV3_VAL_SET)
                {
                    if (CLI_PTR_TO_I4 (au4args[1]) ==
                        OSPFV3_CLI_NO_AREA_SUMMARY)
                    {
                        i4RespStatus = V3OspfCliConfigAreaType
                            (cliHandle,
                             OSPFV3_NSSA_AREA,
                             OSPFV3_CLI_NO_AREA_SUMMARY, *au4args[0],
                             CLI_PTR_TO_U4 (au4args[1]), 0, 0);
                    }
                    else if (CLI_PTR_TO_I4 (au4args[1]) ==
                             OSPFV3_CLI_DEF_INFO_ORIG)
                    {
                        if (au4args[2] != NULL)
                        {
                            i4Metric = *(INT4 *) au4args[2];
                        }
                        else
                        {
                            i4Metric = OSPFV3_CLI_AS_EXT_DEF_METRIC;
                        }
                        if (au4args[3] != NULL)
                        {
                            i4MetricType = *(INT4 *) au4args[3];
                        }
                        else
                        {
                            i4MetricType = OSPFV3_TYPE2EXT_METRIC;
                        }
                        i4RespStatus = V3OspfCliConfigAreaType
                            (cliHandle,
                             OSPFV3_NSSA_AREA,
                             OSPFV3_CLI_SEND_AREA_SUMMARY, *au4args[0],
                             CLI_PTR_TO_I4 (au4args[1]), i4Metric,
                             i4MetricType);
                    }
                    else if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_CLI_NSSA_AREA)
                    {
                        i4RespStatus = V3OspfCliConfigAreaType
                            (cliHandle,
                             OSPFV3_NSSA_AREA,
                             OSPFV3_CLI_SEND_AREA_SUMMARY, *au4args[0],
                             CLI_PTR_TO_I4 (au4args[1]), 0, 0);
                    }

                }
                else
                {
                    if (au4args[2] != NULL)
                    {
                        i4Metric = *(INT4 *) au4args[2];
                    }
                    else
                    {
                        i4Metric = OSPFV3_CLI_AS_EXT_DEF_METRIC;
                    }
                    if (au4args[3] != NULL)
                    {
                        i4MetricType = *(INT4 *) au4args[3];
                    }
                    else
                    {
                        i4MetricType = OSPFV3_TYPE2EXT_METRIC;
                    }
                    if (CLI_PTR_TO_I4 (au4args[1]) ==
                        OSPFV3_CLI_NO_DEF_INFO_ORIG)
                    {
                        i4RespStatus = V3OspfCliConfigAreaType
                            (cliHandle,
                             OSPFV3_NSSA_AREA,
                             OSPFV3_CLI_SEND_AREA_SUMMARY, *au4args[0],
                             CLI_PTR_TO_I4 (au4args[1]), i4Metric,
                             i4MetricType);
                    }
                }
                break;
            }
            case OSPFV3_CLI_STABILITY_INT:
            {
                if (CLI_PTR_TO_I4 (au4args[2]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigNSSAStabilityIntval
                        (cliHandle, *au4args[1], *au4args[0]);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigNSSAStabilityIntval
                        (cliHandle,
                         OSPFV3_NSSA_INVALID_STBLTY_INTRVL, *au4args[0]);
                }
                break;
            }
            case OSPFV3_CLI_DEFAULT_INFO:
            {
                if (au4args[0] != NULL)
                {
                    i4Metric = *(INT4 *) au4args[0];
                }
                else
                {
                    i4Metric = OSPFV3_AS_EXT_DEF_METRIC;
                }
                if (au4args[1] != NULL)
                {
                    i4MetricTypeVal = *(INT4 *) au4args[1];
                    if (i4MetricTypeVal == 1)
                    {
                        i4MetricType = OSPFV3_TYPE_1_METRIC;
                    }
                    if (i4MetricTypeVal == 2)
                    {
                        i4MetricType = OSPFV3_TYPE_2_METRIC;
                    }
                }
                else
                {
                    i4MetricType = OSPFV3_TYPE_2_METRIC;
                }
                MEMSET (&routeDest, 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_COPY (routeDest, gV3OsNullIp6Addr);
                MEMSET (&RouteNextHop, 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_COPY (RouteNextHop, gV3OsNullIp6Addr);
                i4RespStatus =
                    V3OspfCliSetDefaultOriginate (cliHandle, i4Metric,
                                                  i4MetricType, routeDest,
                                                  0, RouteNextHop);
                break;

            }

            case OSPFV3_CLI_NO_DEFAULT_INFO:
            {
                MEMSET (&routeDest, 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_COPY (routeDest, gV3OsNullIp6Addr);
                MEMSET (&RouteNextHop, 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_COPY (RouteNextHop, gV3OsNullIp6Addr);
                if ((au4args[0] == NULL) && (au4args[1] == NULL))
                {
                    i4Metric = OSPFV3_INVALID;
                    i4MetricType = OSPFV3_INVALID;
                }
                else
                {
                    if (au4args[0] != NULL)
                    {
                        i4Metric = *(INT4 *) au4args[0];
                    }
                    else
                    {
                        i4Metric = OSPFV3_INVALID;
                    }
                    if (au4args[1] != NULL)
                    {
                        i4MetricTypeVal = *(INT4 *) au4args[1];
                        if (i4MetricTypeVal == 1)
                        {
                            i4MetricType = OSPFV3_TYPE_1_METRIC;
                        }
                        if (i4MetricTypeVal == 2)
                        {
                            i4MetricType = OSPFV3_TYPE_2_METRIC;
                        }
                    }
                    else
                    {
                        i4MetricType = OSPFV3_INVALID;
                    }
                }
                i4RespStatus =
                    V3OspfCliDelDefaultOriginate (cliHandle, i4Metric,
                                                  i4MetricType, routeDest,
                                                  0, RouteNextHop);
                break;
            }

            case OSPFV3_CLI_TRANS_ROLE:
            {
                if (CLI_PTR_TO_I4 (au4args[2]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigNSSATranslatorRole
                        (cliHandle,
                         CLI_PTR_TO_I4 (au4args[1]),
                         CLI_INET_ADDR (au4args[0]));
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigNSSATranslatorRole
                        (cliHandle,
                         OSPFV3_TRNSLTR_ROLE_CANDIDATE,
                         CLI_INET_ADDR (au4args[0]));
                }
                break;
            }
            case OSPFV3_CLI_SPF_TIME:
            {
                if (CLI_PTR_TO_I4 (au4args[2]) == OSPFV3_VAL_SET)
                {
                    i4SpfDelayTime = *(INT4 *) au4args[0];
                    i4SpfHoldTime = *(INT4 *) au4args[1];
                }
                else
                {
                    i4SpfDelayTime = OSPFV3_DEF_SPF_INT;
                    i4SpfHoldTime = OSPFV3_DEF_HOLD_TIME_INT;
                }
                i4RespStatus =
                    V3OspfCliConfigSpfTime (cliHandle, i4SpfDelayTime,
                                            i4SpfHoldTime);
                break;
            }
            case OSPFV3_CLI_SET_ABR_TYPE:
            {
                if (CLI_PTR_TO_I4 (au4args[1]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigABRType (cliHandle,
                                                           CLI_PTR_TO_I4
                                                           (au4args[0]));
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigABRType (cliHandle,
                                                           OSPFV3_DEF_ABR_TYPE);
                }
                break;
            }
            case OSPFV3_CLI_AREA_DEFAULT_METRIC:
            {
                if (CLI_PTR_TO_I4 (au4args[2]) == OSPFV3_VAL_SET)
                {
                    i4RespStatus = V3OspfCliConfigStubMetric
                        (cliHandle,
                         CLI_ATOI (au4args[1]), CLI_INET_ADDR (au4args[0]));
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigStubMetric
                        (cliHandle, OSPFV3_DEFAULT_SUMM_COST, *(au4args[0]));
                }
                break;
            }
            case OSPFV3_CLI_AREA_DEFAULT_METRICTYPE:
            {
                i4RespStatus = V3OspfCliConfigStubMetricType
                    (cliHandle, CLI_ATOI (au4args[1]),
                     CLI_INET_ADDR (au4args[0]));
                break;
            }
            case OSPFV3_CLI_VIRTUAL_LINK:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }

                OSPFV3_IF_ALLOC (&(ptmpInterface));
                if (ptmpInterface == NULL)
                {
                    return CLI_FAILURE;
                }
                MEMSET (ptmpInterface, 0, sizeof (tV3OsInterface));

                if (CLI_PTR_TO_I4 (au4args[8]) == OSPFV3_VAL_SET)
                {
                    if (au4args[2] != NULL)
                    {
                        ptmpInterface->u4InterfaceId =
                            CLI_PTR_TO_U4 (*au4args[2]);
                    }
                    if (au4args[3] != NULL)
                    {
                        ptmpInterface->u2HelloInterval =
                            (UINT2) CLI_ATOI (au4args[3]);
                    }
                    if (au4args[4] != NULL)
                    {
                        ptmpInterface->u2RxmtInterval =
                            (UINT2) CLI_ATOI (au4args[4]);
                    }
                    if (au4args[5] != NULL)
                    {
                        ptmpInterface->u2IfTransDelay =
                            (UINT2) CLI_ATOI (au4args[5]);
                    }
                    if (au4args[6] != NULL)
                    {
                        ptmpInterface->u2RtrDeadInterval =
                            (UINT2) CLI_ATOI (au4args[6]);
                    }
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }

                if (au4args[7] != NULL)
                {
                    u4BitMask = CLI_PTR_TO_U4 (au4args[7]);
                }

                if (au4args[9] != NULL)
                {
                    ptmpInterface->u2CryptoAuthType =
                        (UINT2) CLI_PTR_TO_U4 (au4args[9]);
                }

                if (au4args[10] != NULL)
                {
                    i4AuthkeyId = *(INT4 *) au4args[10];
                }

                if (au4args[11] != NULL)
                {
                    if (CLI_STRLEN (au4args[11]) > OSPFV3_MAX_AUTHKEY_LEN)
                    {
                        CliPrintf (cliHandle,
                                   "\r%% Key should be less than 16 characters\r\n");
                        i4RespStatus = CLI_FAILURE;
                    }
                    else
                    {
                        CLI_STRCPY (au1AuthKey, au4args[11]);
                    }
                }

                i4RespStatus =
                    V3OspfCliConfigVirtualLink (cliHandle, *au4args[1],
                                                *au4args[0], ptmpInterface,
                                                u4BitMask, u1Set, u4ContextId,
                                                i4AuthkeyId, au1AuthKey);
                OSPFV3_IF_FREE (ptmpInterface);
                break;
            }
            case OSPFV3_CLI_ASBR:
            {
                i4RespStatus = V3OspfCliConfigASBdrRtrStatus (cliHandle,
                                                              CLI_PTR_TO_I4
                                                              (au4args[0]));
                break;
            }
            case OSPFV3_CLI_AREA_RANGE:
            {
                u4SummPrefixLen = *au4args[2];

                MEMSET (&summPrefix, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (au4args[1], &ip6Addr) == OSPFV3_INIT_VAL)
                {
                    CLI_SET_ERR (OSPFV3_CLI_IP6_INVALID_ADDRESS);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                if (((UINT1) OSPFV3_GET_PREFIX_LEN_IN_BYTE (u4SummPrefixLen)) >
                    sizeof (tIp6Addr))
                {
                    CLI_SET_ERR (OSPFV3_CLI_INVALID_PREFIXLEN);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                OSPFV3_IP6_ADDR_PREFIX_COPY (summPrefix, ip6Addr,
                                             u4SummPrefixLen);

                i4SummLsaType = CLI_PTR_TO_I4 (au4args[4]);

                if (CLI_PTR_TO_I4 (au4args[6]) == OSPFV3_VAL_SET)
                {
                    i4SummEffect = CLI_PTR_TO_I4 (au4args[3]);
                    if (au4args[5] != NULL)
                    {
                        i4SummTag = ATOI (au4args[5]);
                    }
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }
                i4RespStatus = V3OspfCliConfigAreaRange (cliHandle, summPrefix,
                                                         (UINT1)
                                                         u4SummPrefixLen,
                                                         *au4args[0],
                                                         i4SummLsaType,
                                                         i4SummEffect,
                                                         i4SummTag, u1Set);
                break;
            }
            case OSPFV3_CLI_ADD_EXT_SUMM_ADDR:
            {
                u4SummPrefixLen = *au4args[2];

                MEMSET (&summPrefix, 0, sizeof (tIp6Addr));
                if (INET_ATON6 (au4args[1], &ip6Addr) == OSPFV3_INIT_VAL)
                {
                    CLI_SET_ERR (OSPFV3_CLI_IP6_INVALID_ADDRESS);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }
                if (((UINT1) OSPFV3_GET_PREFIX_LEN_IN_BYTE (u4SummPrefixLen)) >
                    sizeof (tIp6Addr))
                {
                    CLI_SET_ERR (OSPFV3_CLI_INVALID_PREFIXLEN);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                OSPFV3_IP6_ADDR_PREFIX_COPY (summPrefix, ip6Addr,
                                             u4SummPrefixLen);

                if (CLI_PTR_TO_I4 (au4args[5]) == OSPFV3_VAL_SET)
                {
                    i4SummEffect = CLI_PTR_TO_I4 (au4args[3]);
                    i4SummTrans = CLI_PTR_TO_I4 (au4args[4]);
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }

                pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

                if (pV3OspfCxt != NULL)
                {
                    i4RespStatus =
                        V3OspfCliConfigAsExternalRangeInCxt (cliHandle,
                                                             pV3OspfCxt,
                                                             summPrefix,
                                                             (UINT1)
                                                             u4SummPrefixLen,
                                                             *au4args[0],
                                                             i4SummEffect,
                                                             i4SummTrans,
                                                             u1Set);
                }
                break;
            }
            case OSPFV3_CLI_REDISTRIBUTE:
            {

                i4RespStatus = V3OspfCliConfigRedistribute (cliHandle,
                                                            CLI_PTR_TO_I4
                                                            (au4args[0]),
                                                            (UINT1)
                                                            CLI_PTR_TO_I4
                                                            (au4args[1]),
                                                            (UINT1
                                                             *) (au4args[2]),
                                                            (INT4) (*au4args
                                                                    [3]),
                                                            (INT4) (*au4args
                                                                    [4]));
                break;
            }
            case OSPFV3_CLI_DISTRIBUTE_LIST:
            {
#ifdef ROUTEMAP_WANTED
                i4RespStatus = V3OspfCliSetDistribute (cliHandle, (UINT1 *)
                                                       au4args[0],
                                                       (UINT1)
                                                       CLI_PTR_TO_U4 (au4args
                                                                      [1]));
#endif
                break;
            }
            case OSPFV3_CLI_DEFAULT_PASSIVE:
            {
                i4RespStatus = V3OspfCliConfigDefaultPassiveIface
                    (cliHandle, CLI_PTR_TO_I4 (au4args[0]));
                break;
            }
            case OSPFV3_CLI_HOST:
            {
                INET_ATON6 (au4args[0], &ip6Addr);
                OSPFV3_IP6_ADDR_COPY (ifIpAddr, ip6Addr);

                if (CLI_PTR_TO_I4 (au4args[3]) == OSPFV3_VAL_SET)
                {
                    if (au4args[2] != NULL)
                    {
                        u4AreaId = CLI_INET_ADDR (au4args[2]);
                    }
                    i4MetricValue = (INT4) CLI_ATOI (au4args[1]);
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }
                i4RespStatus = V3OspfCliConfigHost (cliHandle, ifIpAddr,
                                                    i4MetricValue, u4AreaId,
                                                    u1Set);
                break;
            }
            case OSPFV3_CLI_DEL_AREA:
            {
                i4RespStatus = V3OspfCliConfigDeleteArea (cliHandle,
                                                          *(au4args[0]));
                break;
            }
            case OSPFV3_CLI_NSSA_ASBR_DFRT:
            {
                i4RespStatus = V3OspfCliConfigNssaAsbrDfRtTrans (cliHandle,
                                                                 CLI_PTR_TO_I4
                                                                 (au4args[0]));
                break;
            }
            case OSPFV3_CLI_REDIST_CONFIG:
            {
                i4RoutePfxLen = *(INT4 *) (au4args[1]);

                MEMSET (&routeDest, 0, sizeof (tIp6Addr));

                if (INET_ATON6 (au4args[0], &ip6Addr) == OSPFV3_INIT_VAL)
                {
                    CLI_SET_ERR (OSPFV3_CLI_IP6_INVALID_ADDRESS);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                if (((UINT1) OSPFV3_GET_PREFIX_LEN_IN_BYTE (i4RoutePfxLen)) >
                    sizeof (tIp6Addr))
                {
                    CLI_SET_ERR (OSPFV3_CLI_INVALID_PREFIXLEN);
                    i4RespStatus = CLI_FAILURE;
                    break;
                }

                OSPFV3_IP6_ADDR_PREFIX_COPY (routeDest, ip6Addr, i4RoutePfxLen);

                if (CLI_PTR_TO_I4 (au4args[6]) == OSPFV3_VAL_SET)
                {
                    if ((CLI_PTR_TO_U4 (au4args[5]) & OSPFV3_CLI_SET_METRIC_BIT)
                        != OSPFV3_INIT_VAL)
                    {
                        i4MetricValue = *(INT4 *) (au4args[2]);
                    }
                    else
                    {
                        i4MetricValue = OSPFV3_CLI_AS_EXT_DEF_METRIC;
                    }

                    i4MetricType = CLI_PTR_TO_I4 (au4args[3]);

                    if ((CLI_PTR_TO_U4 (au4args[5]) & OSPFV3_CLI_SET_TAG_BIT) !=
                        0)
                    {
                        i4TagValue = *(INT4 *) (au4args[4]);
                        u1TagFlag = OSIX_TRUE;
                    }
                    else
                    {
                        u1TagFlag = OSIX_FALSE;
                    }
                    u1Set = OSPFV3_VAL_SET;
                }
                else
                {
                    u1Set = OSPFV3_VAL_NO;
                }
                i4RespStatus = V3OspfCliConfigRRDRoute (cliHandle, routeDest,
                                                        i4RoutePfxLen,
                                                        i4MetricValue,
                                                        i4MetricType,
                                                        i4TagValue, u1TagFlag,
                                                        u1Set);
                break;
            }
            case OSPFV3_CLI_SET_TRACE_LEVEL:
            {
                i4RespStatus = V3OspfCliConfigTraceLevel (cliHandle,
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [0]),
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [1]),
                                                          (UINT1) CLI_PTR_TO_I4
                                                          (au4args[2]));
                break;
            }
            case OSPFV3_CLI_LSDB_LIMIT:
            {
                i4RespStatus = V3OspfCliConfigExtAreaLsdbLimit
                    (cliHandle, CLI_PTR_TO_I4 (au4args[0]));
                break;
            }
            case OSPFV3_CLI_EXIT_OVERFLOW_STATE:
            {
                i4RespStatus = V3OspfCliConfigExitOverflowInterval
                    (cliHandle, *(INT4 *) au4args[0]);
                break;
            }

            case OSPFV3_CLI_DONT_EXIT_OVERFLOW_STATE:
            {
                i4RespStatus = V3OspfCliConfigExitOverflowInterval
                    (cliHandle, CLI_PTR_TO_I4 (au4args[0]));
                break;
            }

            case OSPFV3_CLI_DEMAND_EXT:
            {
                i4RespStatus = V3OspfCliConfigDemandExtensions (cliHandle,
                                                                CLI_PTR_TO_I4
                                                                (au4args[0]));
                break;
            }
            case OSPFV3_CLI_REF_BW:
            {
                i4RespStatus = V3OspfCliConfigReferenceBandwidth
                    (cliHandle, *au4args[0]);
                break;
            }
            case OSPFV3_CLI_STAGGERING_STATUS:
            {
                i4RespStatus = V3OspfCliSetStaggeringStatus (cliHandle,
                                                             CLI_PTR_TO_I4
                                                             (au4args[0]));
                break;
            }
            case OSPFV3_CLI_STAGGERING_INTERVAL:
            {
                i4RespStatus = V3OspfCliSetStaggeringInterval (cliHandle,
                                                               *(INT4 *)
                                                               au4args[0]);
                break;
            }

            case OSPFV3_CLI_NO_STAGGERING_INTERVAL:
            {
                i4RespStatus = V3OspfCliSetStaggeringInterval (cliHandle,
                                                               CLI_PTR_TO_I4
                                                               (au4args[0]));

                break;
            }

            case OSPFV3_CLI_DISTANCE:
            {
                /* args[0] - distance value (1-255)
                 * args[1] - route map name (optional)
                 */
#ifdef ROUTEMAP_WANTED
                i4RespStatus = V3OspfSetRouteDistance (cliHandle,
                                                       *(INT4 *) au4args[0],
                                                       (UINT1 *) au4args[1]);
#endif
                break;
            }
            case OSPFV3_CLI_NO_DISTANCE:
            {
                /* args[0] - route map name (optional)
                 */
#ifdef ROUTEMAP_WANTED
                i4RespStatus = V3OspfSetNoRouteDistance (cliHandle, (UINT1 *)
                                                         au4args[0]);
#endif
                break;
            }
            case OSPFV3_CLI_RESTART_SUPPORT:
            {
                if ((au4args[0] == NULL) && (au4args[1] == NULL))
                {
                    i4RespStatus = V3OspfCliConfigRestartSupport (cliHandle,
                                                                  OSPFV3_RESTART_BOTH);
                }
                else if (au4args[1] != NULL)
                {
                    i4RespStatus = V3OspfCliConfigRestartSupport (cliHandle,
                                                                  OSPFV3_RESTART_PLANNED);
                }
                if (au4args[0] != NULL)
                {
                    i4RespStatus = V3OspfCliConfigGracePeriod (cliHandle,
                                                               *(INT4
                                                                 *) (au4args
                                                                     [0]));
                }
                break;
            }

            case OSPFV3_CLI_NO_RESTART_SUPPORT:
            {
                if ((au4args[0] == NULL) && (au4args[1] == NULL))
                {
                    i4RespStatus = V3OspfCliConfigRestartSupport (cliHandle,
                                                                  OSPFV3_RESTART_NONE);
                }
                if (au4args[1] != NULL)
                {
                    i4RespStatus = V3OspfCliConfigGracePeriod (cliHandle,
                                                               OSPFV3_GR_DEF_INTERVAL);
                }
                break;
            }
            case OSPFV3_CLI_GR_ACK_STATE:
            {
                i4RespStatus = V3OspfCliConfigGrAckState (cliHandle,
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [0]));
                break;
            }

            case OSPFV3_CLI_GRLSA_RETRANS_COUNT:
            {
                if (au4args[0] == NULL)
                {
                    i4RespStatus = V3OspfCliConfigGraceRetransCount (cliHandle,
                                                                     OSPFV3_DEF_GRACE_LSA_SENT);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigGraceRetransCount (cliHandle,
                                                                     *(INT4 *)
                                                                     au4args
                                                                     [0]);
                }
                break;
            }
            case OSPFV3_CLI_RESTART_REASON:
            {
                i4RespStatus = V3OspfCliConfigRestartReason (cliHandle,
                                                             CLI_PTR_TO_I4
                                                             (au4args[0]));
                break;
            }

            case OSPFV3_CLI_NO_RESTART_REASON:
            {
                i4RespStatus = V3OspfCliConfigRestartReason (cliHandle,
                                                             CLI_PTR_TO_I4
                                                             (au4args[0]));
                break;

            }
            case OSPFV3_CLI_HELPER_SUPPORT:
            {
                i4RespStatus = V3OspfCliConfigHelperSupport (cliHandle,
                                                             CLI_PTR_TO_I4
                                                             (au4args[0]),
                                                             CLI_ENABLE);
                break;
            }

            case OSPFV3_CLI_NO_HELPER_SUPPORT:
            {
                i4RespStatus = V3OspfCliConfigHelperSupport (cliHandle,
                                                             CLI_PTR_TO_I4
                                                             (au4args[0]),
                                                             CLI_DISABLE);
                break;
            }

            case OSPFV3_CLI_HELPER_GRACE_LIMIT:
            {
                if (au4args[0] == NULL)
                {
                    i4RespStatus = V3OspfCliConfigHelperGraceLimitPeriod
                        (cliHandle, 0);
                }
                else
                {
                    i4RespStatus = V3OspfCliConfigHelperGraceLimitPeriod
                        (cliHandle, *(INT4 *) au4args[0]);
                }
                break;
            }

            case OSPFV3_CLI_STRICT_LSA_CHECK:
            {
                i4RespStatus = V3OspfCliConfigStrictLsaCheck (cliHandle,
                                                              CLI_PTR_TO_I4
                                                              (au4args[0]));
                break;
            }

            case OSPFV3_CLI_BFD_ADMIN_STATUS:
            {
                i4RespStatus = V3OspfCliSetBfdAdmState (cliHandle,
                                                        CLI_PTR_TO_I4
                                                        (au4args[0]));
                break;
            }
            case OSPFV3_CLI_BFD_STATUS_ALL_INTF:
            {
                i4RespStatus = V3OspfCliSetBfdStatusAllInt (cliHandle,
                                                            (INT4)
                                                            CLI_PTR_TO_U4
                                                            (au4args[0]));
                break;
            }
            case OSPFV3_CLI_BFD_STATUS_SPECIFIC_INTF:
            {
                i4RespStatus =
                    V3OspfCliSetBfdStatusSpecificInt (cliHandle,
                                                      u4InterfaceIndex,
                                                      (INT4)
                                                      CLI_PTR_TO_U4 (au4args
                                                                     [0]));
                break;
            }
            case OSPFV3_CLI_IPV6_OSPF_BFD:
            {

                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliSetBfdStatusSpecificInt (cliHandle, u4IfIndex,
                                                          (INT4)
                                                          CLI_PTR_TO_U4 (au4args
                                                                         [0]));
                }
                break;
            }

            case OSPFV3_CLI_AUTHTYPE:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliSetIfCryptoAuthType (cliHandle, u4IfIndex,
                                                      CLI_PTR_TO_I4 (au4args
                                                                     [0]));
                }
                break;
            }
            case OSPFV3_CLI_SHA_KEY_ACCEPT_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) au4args[0];
                    i4RespStatus =
                        V3OspfCliSetIfAuthKeyStartAccept (cliHandle, i4Key,
                                                          (UINT1 *) au4args[1],
                                                          u4ContextId,
                                                          u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_SHA_KEY_GEN_START:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) au4args[0];
                    i4RespStatus =
                        V3OspfCliSetIfAuthKeyStartGenerate (cliHandle, i4Key,
                                                            (UINT1 *)
                                                            au4args[1],
                                                            u4ContextId,
                                                            u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_SHA_KEY_GEN_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) au4args[0];
                    i4RespStatus =
                        V3OspfCliSetIfAuthKeyStopGenerate (cliHandle, i4Key,
                                                           (UINT1 *) au4args[1],
                                                           u4ContextId,
                                                           u4IfIndex);
                }

                break;
            }
            case OSPFV3_CLI_SHA_KEY_ACCEPT_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4Key = *(INT4 *) au4args[0];
                    i4RespStatus =
                        V3OspfCliSetIfAuthKeyStopAccept (cliHandle, i4Key,
                                                         (UINT1 *) au4args[1],
                                                         u4ContextId,
                                                         u4IfIndex);
                }
                break;
            }
            case OSPFV3_CLI_AUTHKEY:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }

                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliSetIfAuthKey (cliHandle, u4ContextId,
                                               u4IfIndex, *(INT4 *) au4args[0],
                                               (UINT1 *) au4args[1],
                                               CLI_PTR_TO_I4 (au4args[2]));
                }
                break;
            }
            case OSPFV3_CLI_AUTH_MODE:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliSetIfAuthMode (cliHandle, u4IfIndex,
                                                CLI_PTR_TO_I4 (au4args[0]));
                }
                break;
            }
            case OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_START:
            case OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_STOP:
            case OSPFV3_CLI_SHA_VIRT_KEY_GEN_START:
            case OSPFV3_CLI_SHA_VIRT_KEY_GEN_STOP:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                u4AreaId = *au4args[0];
                u4NbrId = *au4args[1];
                i4Key = *(INT4 *) au4args[2];

                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else if (u4Command == OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_START)
                {
                    i4RespStatus =
                        V3OspfCliSetIfVirtAuthKeyStartAccept (cliHandle,
                                                              u4AreaId, u4NbrId,
                                                              i4Key,
                                                              (UINT1 *)
                                                              au4args[3],
                                                              u4ContextId);
                }
                else if (u4Command == OSPFV3_CLI_SHA_VIRT_KEY_ACCEPT_STOP)
                {
                    i4RespStatus =
                        V3OspfCliSetIfVirtAuthKeyStopAccept (cliHandle,
                                                             u4AreaId, u4NbrId,
                                                             i4Key,
                                                             (UINT1 *)
                                                             au4args[3],
                                                             u4ContextId);
                }
                else if (u4Command == OSPFV3_CLI_SHA_VIRT_KEY_GEN_START)
                {
                    i4RespStatus =
                        V3OspfCliSetIfVirtAuthKeyStartGen (cliHandle, u4AreaId,
                                                           u4NbrId, i4Key,
                                                           (UINT1 *) au4args[3],
                                                           u4ContextId);
                }
                else if (u4Command == OSPFV3_CLI_SHA_VIRT_KEY_GEN_STOP)
                {
                    i4RespStatus =
                        V3OspfCliSetIfVirtAuthKeyStopGen (cliHandle, u4AreaId,
                                                          u4NbrId, i4Key,
                                                          (UINT1 *) au4args[3],
                                                          u4ContextId);
                }
                break;
            }

        }
    }
    else
    {
        u4ContextId = OSPFV3_INVALID_CXT_ID;

        switch (u4Command)
        {
            case OSPFV3_CLI_SHOW_ALL_INTERFACE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowInterfaceInCxt (cliHandle,
                                                                u4ContextId,
                                                                OSPFV3_CLI_SHOW_ALL_INTERFACE,
                                                                u4InterfaceIndex);
                    break;
                }
            }
            case OSPFV3_CLI_SHOW_INTERFACE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowInterfaceInCxt (cliHandle,
                                                                u4ContextId,
                                                                OSPFV3_CLI_SHOW_INTERFACE,
                                                                CLI_PTR_TO_U4
                                                                (au4args[0]));
                }
                break;
            }
            case OSPFV3_CLI_SHOW_ALL_NBR:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowNeighborInCxt (cliHandle,
                                                               u4ContextId,
                                                               OSPFV3_CLI_SHOW_ALL_NBR,
                                                               (UINT4) 0,
                                                               (UINT4) 0);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_NBR:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    if (au4args[0] != NULL)
                    {
                        i4RespStatus = V3OspfCliShowNeighborInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   OSPFV3_CLI_SHOW_NBR,
                                                                   CLI_INET_ADDR
                                                                   (au4args[0]),
                                                                   (UINT4) 0);
                    }
                    else
                    {
                        i4RespStatus = V3OspfCliShowNeighborInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   OSPFV3_CLI_SHOW_NBR_INTF,
                                                                   (UINT4) 0,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args
                                                                    [1]));

                    }

                }
                break;
            }
            case OSPFV3_CLI_SHOW_REQ_LSA_NBR:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    u4NbrId = 0;
                    if (au4args[1] != NULL)
                    {
                        u4NbrId = *au4args[1];
                    }

                    if (au4args[0] != NULL)
                    {
                        i4RespStatus = V3OspfCliShowRequestListInCxt (cliHandle,
                                                                      u4ContextId,
                                                                      OSPFV3_CLI_SHOW_REQ_LSA_NBR_INT,
                                                                      CLI_PTR_TO_U4
                                                                      (au4args
                                                                       [0]),
                                                                      u4NbrId);
                    }
                    else
                    {
                        i4RespStatus = V3OspfCliShowRequestListInCxt (cliHandle,
                                                                      u4ContextId,
                                                                      OSPFV3_CLI_SHOW_REQ_LSA_NBR,
                                                                      (UINT4) 0,
                                                                      u4NbrId);
                    }
                }
                break;
            }
            case OSPFV3_CLI_SHOW_REQ_LSA_ALL:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowRequestListInCxt (cliHandle,
                                                                  u4ContextId,
                                                                  OSPFV3_CLI_SHOW_REQ_LSA_ALL,
                                                                  (UINT4) 0,
                                                                  (UINT4) 0);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_TRANS_LSA_NBR:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    u4NbrId = 0;
                    if (au4args[1] != NULL)
                    {
                        u4NbrId = *au4args[1];
                    }

                    if (au4args[0] != NULL)
                    {

                        i4RespStatus = V3OspfCliShowRxmtListInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   OSPFV3_CLI_SHOW_TRANS_LSA_NBR_INTF,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[0]),
                                                                   u4NbrId);
                    }
                    else
                    {

                        i4RespStatus = V3OspfCliShowRxmtListInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   OSPFV3_CLI_SHOW_TRANS_LSA_NBR,
                                                                   (UINT4) 0,
                                                                   u4NbrId);

                    }

                }
                break;
            }
            case OSPFV3_CLI_SHOW_TRANS_LSA_ALL:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowRxmtListInCxt (cliHandle,
                                                               u4ContextId,
                                                               OSPFV3_CLI_SHOW_TRANS_LSA_ALL,
                                                               (UINT4) 0,
                                                               (UINT4) 0);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_VIRTUAL_LINKS:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowVirtualIfInCxt (cliHandle,
                                                                u4ContextId);
                }

                break;
            }
            case OSPFV3_CLI_SHOW_BORDER_ROUTERS:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowBorderRouterInCxt (cliHandle,
                                                                   u4ContextId);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_AREA_RANGE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowSummaryAddressInCxt (cliHandle,
                                                                     u4ContextId);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_SUMMARY_PREFIX:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliShowExtSummaryAddressInCxt (cliHandle,
                                                             u4ContextId);
                }

                break;
            }
            case OSPFV3_CLI_SHOW_OSPF:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowOspfInfoInCxt (cliHandle,
                                                               u4ContextId);
                }

                break;
            }
            case OSPFV3_CLI_SHOW_DB:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    if (CLI_PTR_TO_I4 (au4args[3]) == OSPFV3_VAL_SET)
                    {
                        i4RespStatus = V3OspfCliShowDatabaseInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[0]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[1]),
                                                                   1,
                                                                   CLI_INET_ADDR
                                                                   (au4args
                                                                    [2]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args
                                                                    [4]));
                    }
                    else
                    {
                        i4RespStatus = V3OspfCliShowDatabaseInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[0]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[1]),
                                                                   OSIX_FALSE,
                                                                   (UINT4) 0,
                                                                   (UINT4) 0);
                    }
                }
                break;
            }
            case OSPFV3_CLI_SHOWDATABASE:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    if (au4args[5] != NULL)
                    {
                        u4AdvrId = *au4args[5];
                        OSPFV3_BUFFER_DWTOPDU (V3_LS_ADV_ROUTER_ID, u4AdvrId);

                    }
                    if (CLI_PTR_TO_I4 (au4args[3]) == OSPFV3_VAL_SET)
                    {
                        i4RespStatus = V3OspfCliShowDatabaseInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[0]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[1]),
                                                                   1,
                                                                   CLI_INET_ADDR
                                                                   (au4args[2]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args
                                                                    [4]));
                    }
                    else
                    {
                        i4RespStatus = V3OspfCliShowDatabaseInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[0]),
                                                                   CLI_PTR_TO_U4
                                                                   (au4args[1]),
                                                                   OSIX_FALSE,
                                                                   (UINT4) 0,
                                                                   CLI_PTR_TO_U4
                                                                   (au4args
                                                                    [4]));
                    }
                }
                break;
            }
            case OSPFV3_CLI_SHOW_RT:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus = V3OspfCliShowRoutingTableInCxt (cliHandle,
                                                                   u4ContextId);
                }
                break;

            }
            case OSPFV3_CLI_SHOW_AREA:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliShowAreaInCxt (cliHandle, u4ContextId);
                }

                break;
            }
            case OSPFV3_CLI_SHOW_HOST:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliShowHostInCxt (cliHandle, u4ContextId);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_RRD_INFO:
            {
                /* Get the Context Id from Context Name */
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliShowRedistRouteCfgTblInCxt (cliHandle,
                                                             u4ContextId);
                }
                break;
            }
            case OSPFV3_CLI_SHOW_RED_INFO:
            {
                i4RespStatus = V3OspfCliShowRedStatus (cliHandle);
                break;
            }
            case OSPFV3_CLI_SET_TRACE_LEVEL:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RespStatus = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                                 pu1OspfCxtName,
                                                                 &u4ContextId);
                }
                else
                {
                    u4ContextId = OSPFV3_DEFAULT_CXT_ID;
                }
                if (V3UtilSetContext (u4ContextId) == SNMP_FAILURE)
                {
                    CliPrintf (cliHandle, "\r%% Invalid ospf Context Id\r\n");
                    CliUnRegisterLock (cliHandle);
                    V3OspfUnLock ();
                    return CLI_FAILURE;
                }

                i4RespStatus = V3OspfCliConfigTraceLevel (cliHandle,
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [0]),
                                                          CLI_PTR_TO_I4 (au4args
                                                                         [1]),
                                                          (UINT1) CLI_PTR_TO_I4
                                                          (au4args[2]));
                V3UtilReSetContext ();
                break;
            }
            case OSPFV3_CLI_SHOW_COUNTERS:
            {
                V3OspfShowCounters (cliHandle);
                break;
            }
            case OSPFV3_CLI_SHOW_PKT_STATS:
            {
                if (pu1OspfCxtName != NULL)
                {
                    i4RetVal = V3OspfCliGetCxtIdFromCxtName (cliHandle,
                                                             pu1OspfCxtName,
                                                             &u4ContextId);
                }
                if (u4InterfaceIndex != OSPFV3_INIT_VAL)
                {
                    if (pu1OspfCxtName == NULL)
                    {
                        i4RetVal = V3OspfCliGetCxtIdFromIfIndex
                            (cliHandle, u4InterfaceIndex, &u4ContextId);
                    }
                }
                if (i4RetVal == CLI_FAILURE)
                {
                    i4RespStatus = CLI_FAILURE;
                }
                else
                {
                    i4RespStatus =
                        V3OspfCliSetIfShowAuthPktStats (cliHandle,
                                                        u4InterfaceIndex);
                }
                break;
            }
            case OSPFV3_CLI_CLEAR_IPV6_OSPF:
            {

                if ((i4RetValue == CLI_FAILURE) || (i4RetVal == CLI_FAILURE))
                {
                    OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, u4ContextId,
                                "Given Input is not valid\n");
                    break;
                }
                if ((i4RetVal == CLI_SUCCESS) && (i4RetValue == CLI_SUCCESS))
                {
                    i4RetVal =
                        V3OspfCliClearProcess (cliHandle,
                                               OSPFV3_DEFAULT_CXT_ID);
                    if (i4RetVal == OSPFV3_FAILURE)
                    {
                        i4RespStatus = CLI_FAILURE;
                    }

                }
                break;
            }

            default:
                CliPrintf (cliHandle, "\r%% Invalid Command\r\n");
                i4RespStatus = CLI_FAILURE;
                break;
        }
    }

    if ((i4RespStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < OSPFV3_CLI_MAX_ERR))
        {
            CliPrintf (cliHandle, "\r%s", Ospf3CliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RespStatus);

    V3OspfUnLock ();
    CliUnRegisterLock (cliHandle);

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    /*Saving all cli commands for Graceful restart */
    if ((pV3OspfCxt != NULL) && (i4RespStatus == CLI_SUCCESS) &&
        (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS) &&
        (pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_BOTH))
    {
        IssCsrSaveCli (pu1TaskName);
    }

    return i4RespStatus;
}

/******************************************************************************
Function : V3OspfGetInterfaceNameFromIndex

Descrption : Get Interface Name from interfce index

Input    : u4IfIndex - Interface Index

Output   : pu1IfName - Interface Name

Returns  : OSIX_SUCCESS or SNMP_FAILURE
******************************************************************************/
PUBLIC INT1
V3OspfGetInterfaceNameFromIndex (UINT4 u4IfIndex, UINT1 *pu1IfName)
{
    tNetIpv6IfInfo      netIp6IfInfo;

    if (NetIpv6GetIfInfo (u4IfIndex, &netIp6IfInfo) == NETIPV6_FAILURE)
    {
        return OSIX_FAILURE;
    }
    STRNCPY (pu1IfName, netIp6IfInfo.au1IfName,
             (OSPFV3_CLI_MAX_STRING_LEN - 1));
    pu1IfName[OSPFV3_CLI_MAX_STRING_LEN - 1] = '\0';
    return OSIX_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigSpfTime
  
  Description  : This Routine set spfDelay and spfHoldTime for SPF calculation
  
  Input(s)      : i4SpfDelay - Time period between SPF calculation
         i4SpfHoldTime - when to start SPF calculation after 
                 change in network topology

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigSpfTime (tCliHandle cliHandle, INT4 i4SpfDelay,
                        INT4 i4SpfHoldTime)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3SpfDelay (&u4ErrorCode, i4SpfDelay) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetFutOspfv3SpfDelay (i4SpfDelay) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3SpfHoldTime (&u4ErrorCode, i4SpfHoldTime)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetFutOspfv3SpfHoldTime (i4SpfHoldTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigRouterId
  
  Description  : This Routine set Router Id
  
  Input(s)      : u4RouterId - Router Id 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigRouterId (tCliHandle cliHandle, UINT4 u4RouterId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2Ospfv3RouterId (&u4ErrorCode, u4RouterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3RouterId (u4RouterId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliDelRouterId

  Description  : This Routine Reset the Router Id configured

  Input(s)      : u4OspfCxtId - context Id

  Output(s)    : None

  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliDelRouterId (tCliHandle cliHandle, UINT4 u4OspfCxtId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    UINT4               u4RouterId = 0;
    tV3OsRouterId       rtrId;

    MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));

    UNUSED_PARAM (cliHandle);
    /* select valid lowest interface IP Address */
    if (V3UtilSelectOspfRouterId (u4OspfCxtId, &rtrId) != OSPFV3_SUCCESS)
    {
        CLI_SET_ERR (OSPFV3_CLI_NO_ACT_INT);
        return CLI_FAILURE;
    }
    u4RouterId = OSPFV3_BUFFER_DWFROMPDU (rtrId);
    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2Ospfv3RouterId (&u4ErrorCode, u4RouterId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RouterIdPermanence (OSPFV3_ROUTERID_DYNAMIC) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigABRType
  
  Description  : This Routine set ABR type
  
  Input(s)      : i4AbrType - ABR type 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigABRType (tCliHandle cliHandle, INT4 i4AbrType)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2FutOspfv3ABRType (&u4ErrorCode, i4AbrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3ABRType (i4AbrType) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfAreaId
  
  Description  : This Routine Set Area Id for the given interface or delete 
           interface entry
  
  Input(s)     : u4AreaId - Area Id 
                 u4IfIndex - Interface Index
               u1Set = OSPFV3_VAL_SET, set given value
         u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
         the object 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfAreaId (tCliHandle cliHandle, UINT4 u4AreaId,
                         UINT4 u4IfIndex, UINT1 u1Set, INT4 i4IfInstId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    tNetIpv6IfInfo      ipv6IfInfo;
    MEMSET (&ipv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1Set == OSPFV3_VAL_NO)
        {
            CLI_SET_ERR (OSPFV3_CLI_ERR_GET_STATUS_DESTROY);
            return CLI_FAILURE;
        }
        else
        {
            if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                         OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetOspfv3IfStatus
                ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (u1Set == OSPFV3_VAL_NO)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Checking whether the value falls with in range or not */
        if (nmhGetOspfv3AreaStatus (u4AreaId, &i4StatusVal) == SNMP_FAILURE)
        {
            if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, u4AreaId,
                                           OSPFV3_CLI_STATUS_CG) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Creating the row instance for the
             * OSPF Area table using the CREATE
             * AND GO */

            if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_CG)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }

        /* Checks whether the passed parameter value falls within the mib 
         * range */
        if (nmhTestv2Ospfv3IfAreaId (&u4ErrorCode, (INT4) u4IfIndex,
                                     u4AreaId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value    */
        if (nmhSetOspfv3IfAreaId ((INT4) u4IfIndex, u4AreaId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2Ospfv3IfInstId (&u4ErrorCode, (INT4) u4IfIndex,
                                     i4IfInstId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfInstId ((INT4) u4IfIndex, i4IfInstId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        /* Unnumbered interface will be of type -point to point */
        if ((V3GetUnnumberedInterface ((UINT4) u4IfIndex, &ipv6IfInfo))
            == NETIPV6_FAILURE)
        {
            CLI_SET_ERR (OSPFV3_CLI_ERR_IP6);
            return CLI_FAILURE;
        }
        /*To check if interface is unnumbered */
        if ((ipv6IfInfo.u4AddressLessIf) != 0)
        {

            if (nmhTestv2Ospfv3IfType (&u4ErrorCode, (INT4) u4IfIndex,
                                       OSPFV3_IF_PTOP) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* to set interface type-PTOP  */
            if (nmhSetOspfv3IfType ((INT4) u4IfIndex, OSPFV3_IF_PTOP) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
        /*Get the row status */
        if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
        {
            if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                         OSPFV3_CLI_STATUS_ACTIVE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfv3IfStatus
                ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfDemand
  
  Description  : This Routine Set Deamnd Circuit status for the given interface 
  
  Input(s)     : i4DemandCkt - Enable/Disbale deamand circuit
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfDemand (tCliHandle cliHandle, INT4 i4DemandCkt,
                         UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfDemand (&u4ErrorCode, (INT4) u4IfIndex,
                                 i4DemandCkt) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfDemand ((INT4) u4IfIndex, i4DemandCkt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfRetransInterval
  
  Description  : This Routine Set retransmit interval for the given interface 
  
  Input(s)     : i4RetransInterval - Retransmission Interval
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfRetransInterval (tCliHandle cliHandle,
                                  INT4 i4RetransInterval, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfRetransInterval (&u4ErrorCode, (INT4) u4IfIndex,
                                          i4RetransInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfRetransInterval ((INT4) u4IfIndex,
                                       i4RetransInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfTransitDelay
  
  Description  : This Routine set transit delay for the given interface 
  
  Input(s)     : i4TransitDelay - Transit Delay
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfTransitDelay (tCliHandle cliHandle,
                               INT4 i4TransitDelay, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfTransitDelay (&u4ErrorCode, (INT4) u4IfIndex,
                                       i4TransitDelay) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfTransitDelay ((INT4) u4IfIndex,
                                    i4TransitDelay) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfRtrPriority
  
  Description  : This Routine set router priority for the given interface 
  
  Input(s)     : i4RtrPriority - Router priority 
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfRtrPriority (tCliHandle cliHandle, INT4 i4RtrPriority,
                              INT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfRtrPriority (&u4ErrorCode, (INT4) u4IfIndex,
                                      i4RtrPriority) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfRtrPriority ((INT4) u4IfIndex, i4RtrPriority) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfHelloInterval
  
  Description  : This Routine set hello interval for the given interface 
  
  Input(s)     : i4HelloInterval - Hello interval
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfHelloInterval (tCliHandle cliHandle,
                                INT4 i4HelloInterval, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfHelloInterval (&u4ErrorCode, (INT4) u4IfIndex,
                                        i4HelloInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfHelloInterval ((INT4) u4IfIndex,
                                     i4HelloInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfRtrDeadInterval
  
  Description  : This Routine set router dead interval for the given interface 
  
  Input(s)     : i4RtrDeadInterval - Router dead interval
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfRtrDeadInterval (tCliHandle cliHandle,
                                  INT4 i4RtrDeadInterval, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfRtrDeadInterval (&u4ErrorCode, (INT4) u4IfIndex,
                                          i4RtrDeadInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfRtrDeadInterval ((INT4) u4IfIndex,
                                       i4RtrDeadInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfPollInterval
  
  Description  : This Routine set poll interval for the given interface 
  
  Input(s)     : i4PollInterval - Poll interval
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfPollInterval (tCliHandle cliHandle,
                               INT4 i4PollInterval, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfPollInterval (&u4ErrorCode, (INT4) u4IfIndex,
                                       (UINT4) i4PollInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfPollInterval ((INT4) u4IfIndex,
                                    (UINT4) i4PollInterval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfMetricValue
  
  Description  : This Routine set metric value for the given interface 
  
  Input(s)     : i4MetricValue - Metric value
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfMetricValue (tCliHandle cliHandle, INT4 i4MetricValue,
                              UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfMetricValue (&u4ErrorCode, (INT4) u4IfIndex,
                                      i4MetricValue) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfMetricValue ((INT4) u4IfIndex, i4MetricValue) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfType
  
  Description  : This Routine set network type for the given interface 
  
  Input(s)     : i4Type - Interface's network type
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfType (tCliHandle cliHandle, INT4 i4Type,
                       UINT4 u4IfIndex, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    UINT1               u1IfType = OSPFV3_INIT_VAL;
    tNetIpv6IfInfo      ipv6IfInfo;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (u1Set == OSPFV3_VAL_SET)
    {
        if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) ==
            SNMP_FAILURE)

        {
            if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                         OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetOspfv3IfStatus
                ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else                        /* Set Default If Type */
    {
        if ((NetIpv6GetIfInfo ((UINT4) u4IfIndex, &ipv6IfInfo))
            == NETIPV6_FAILURE)
        {
            CLI_SET_ERR (OSPFV3_CLI_ERR_IP6);
            return CLI_FAILURE;
        }

        if (V3MapIfType (ipv6IfInfo.u4InterfaceType, &u1IfType) == OSIX_FAILURE)
        {
            CLI_SET_ERR (OSPFV3_CLI_INV_IFTYPE);
            return CLI_FAILURE;
        }
        i4Type = (INT4) u1IfType;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfType (&u4ErrorCode, (INT4) u4IfIndex,
                               i4Type) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfType ((INT4) u4IfIndex, i4Type) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfNbmaNbr
  
  Description  : This Routine set Nbma Nbr for the given interface 
  
  Input(s)     : u4IfIndex - Interface Index
           ifIpAddr - Ipv6 address of the Nbr
           i4NbrPriority - Nbr priority
         u4BitMask - Indicates which optional value to set
               u1Set = OSPFV3_VAL_SET, set given value
         u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
         the object 
                 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfNbmaNbr (tCliHandle cliHandle, UINT4 u4IfIndex,
                          tIp6Addr ifIpAddr, INT4 i4NbrPriority,
                          UINT4 u4BitMask, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    INT4                i4AddressType = OSPFV3_INET_ADDR_TYPE;
    tSNMP_OCTET_STRING_TYPE octetString;

    octetString.pu1_OctetList = (UINT1 *) &ifIpAddr;
    octetString.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    /*Get the NBMA Nbr Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3NbmaNbrStatus ((INT4) u4IfIndex, i4AddressType,
                                   &octetString, &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1Set == OSPFV3_VAL_NO)    /* Delete Nbr entry */
        {
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return CLI_FAILURE;
        }

        if (nmhTestv2Ospfv3NbmaNbrStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                          i4AddressType,
                                          &octetString,
                                          OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3NbmaNbrStatus ((INT4) u4IfIndex, i4AddressType,
                                       &octetString,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u1Set == OSPFV3_VAL_NO)    /* Delete Nbr entry */
    {
        if ((u4BitMask & OSPFV3_CLI_SET_PRIORITY_BIT) == OSPFV3_INIT_VAL)
        {
            if (nmhTestv2Ospfv3NbmaNbrStatus (&u4ErrorCode,
                                              (INT4) u4IfIndex, i4AddressType,
                                              &octetString,
                                              OSPFV3_CLI_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetOspfv3NbmaNbrStatus ((INT4) u4IfIndex, i4AddressType,
                                           &octetString,
                                           OSPFV3_CLI_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }

            return CLI_SUCCESS;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */

    if (u4BitMask & OSPFV3_CLI_SET_PRIORITY_BIT)
    {
        if (nmhTestv2Ospfv3NbmaNbrPriority (&u4ErrorCode, (INT4) u4IfIndex,
                                            i4AddressType,
                                            &octetString,
                                            i4NbrPriority) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3NbmaNbrPriority ((INT4) u4IfIndex, i4AddressType,
                                         &octetString,
                                         i4NbrPriority) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /*Get the row stattus */
    if (nmhGetOspfv3NbmaNbrStatus ((INT4) u4IfIndex, i4AddressType,
                                   &octetString, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3NbmaNbrStatus ((INT4) u4IfIndex, i4AddressType,
                                       &octetString,
                                       OSPFV3_CLI_STATUS_ACTIVE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfPassive
  
  Description  : This Routine set enable or disable passive interface 
  
  Input(s)     : i4Passive - Enable or Disable passive interface
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfPassive (tCliHandle cliHandle, INT4 i4Passive, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3IfPassive (&u4ErrorCode, (INT4) u4IfIndex,
                                     i4Passive) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3IfPassive ((INT4) u4IfIndex, i4Passive) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
  Function Name: V3OspfCliConfigIfLinkLSASuppression
  
  Description  : This Routine set enable or disable passive interface 
  
  Input(s)     : i4Passive - Enable or Disable passive interface
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PUBLIC INT1
V3OspfCliConfigIfLinkLSASuppression (tCliHandle cliHandle, INT4 i4Passive,
                                     UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    UINT1               u1Flag = OSIX_FALSE;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

        u1Flag = OSIX_TRUE;
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3IfLinkLSASuppression (&u4ErrorCode, (INT4) u4IfIndex,
                                                i4Passive) == SNMP_FAILURE)
    {
        if (u1Flag == OSIX_TRUE)
        {
            if (nmhSetOspfv3IfStatus
                ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
            }
        }

        return CLI_FAILURE;

    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3IfLinkLSASuppression ((INT4) u4IfIndex, i4Passive)
        == SNMP_FAILURE)
    {
        if (u1Flag == OSIX_TRUE)
        {
            nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_DESTROY);
        }

        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) != SNMP_FAILURE)
    {

        if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
        {
            if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                         OSPFV3_CLI_STATUS_ACTIVE) ==
                SNMP_FAILURE)
            {
                if (u1Flag == OSIX_TRUE)
                {
                    nmhSetOspfv3IfStatus ((INT4) u4IfIndex,
                                          OSPFV3_CLI_STATUS_DESTROY);
                }

                return CLI_FAILURE;
            }

            if (nmhSetOspfv3IfStatus
                ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
            {
                if (u1Flag == OSIX_TRUE)
                {
                    nmhSetOspfv3IfStatus ((INT4) u4IfIndex,
                                          OSPFV3_CLI_STATUS_DESTROY);
                }

                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfDemandNbrProbe
  
  Description  : This Routine set Nbr probing on demand circuit enabled 
           interface
  
  Input(s)     : i4DemandNbrProbe - Enable or Disable Nbr probing
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfDemandNbrProbe (tCliHandle cliHandle,
                                 INT4 i4DemandNbrProbe, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfDemandNbrProbe (&u4ErrorCode, (INT4) u4IfIndex,
                                         i4DemandNbrProbe) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfDemandNbrProbe ((INT4) u4IfIndex,
                                      i4DemandNbrProbe) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfNbrProbeRetxLimit
  
  Description  : This Routine set Nbr probing retransmission limit  
  
  Input(s)     : i4DemandNbrProbeRetxLimit - retransmission limit
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfNbrProbeRetxLimit (tCliHandle cliHandle,
                                    INT4 i4DemandNbrProbeRetxLimit,
                                    UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit
        (&u4ErrorCode, (INT4) u4IfIndex,
         (UINT4) i4DemandNbrProbeRetxLimit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfDemandNbrProbeRetxLimit ((INT4) u4IfIndex,
                                               (UINT4)
                                               i4DemandNbrProbeRetxLimit) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigIfNbrProbeInterval
  
  Description  : This Routine set Nbr probing interval  
  
  Input(s)     : i4DemandNbrProbeInterval - Probgin interval
                 u4IfIndex - Interface Index

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigIfNbrProbeInterval (tCliHandle cliHandle,
                                   INT4 i4DemandNbrProbeInterval,
                                   UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3IfDemandNbrProbeInterval
        (&u4ErrorCode, (INT4) u4IfIndex,
         (UINT4) i4DemandNbrProbeInterval) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3IfDemandNbrProbeInterval ((INT4) u4IfIndex,
                                              (UINT4) i4DemandNbrProbeInterval)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Get the row stattus */
    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name : V3OspfCliConfigAdminStatInCxt
  
  Description   : This Routine Enable or Disable Ospfv3 protocol  
  
  Input(s)      : cliHandle    - Used by CliPrintf 
                  u4ContextId  - ospf context id
                  i4AdminStat  - Enable or Disable

  Output(s)     : None             
                 
  Return Values : CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigAdminStatInCxt (tCliHandle CliHandle, INT4 i4AdminStat,
                               UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    INT4                i4PrevAdminStat = OSPFV3_INIT_VAL;
    INT4                i4RowStat;
    tV3OspfCxt         *pV3OspfCxt = NULL;

    MEMSET (au1Cmd, 0, sizeof (au1Cmd));

    if (i4AdminStat == OSPFV3_ENABLED)
    {
        i4RowStat = CREATE_AND_GO;
    }
    else
    {
        /* Delete the OSPF Context */
        i4RowStat = DESTROY;
    }

    /* Check for context */
    if (i4AdminStat == OSPFV3_DISABLED)
    {
        pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
        if (pV3OspfCxt == NULL)
        {
            return CLI_FAILURE;
        }
        pV3OspfCxt->u1RouteLeakStatus = OSIX_TRUE;

        if (nmhTestv2FsMIStdOspfv3Status
            (&u4ErrorCode, (INT4) u4ContextId, i4RowStat) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Calls the SNMP set routine to create or delete context */
        if (nmhSetFsMIStdOspfv3Status ((INT4) u4ContextId, i4RowStat) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {

        if (nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId, &i4PrevAdminStat)
            == SNMP_FAILURE)
        {
            if (nmhTestv2FsMIStdOspfv3Status
                (&u4ErrorCode, (INT4) u4ContextId, i4RowStat) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            /* Calls the SNMP set routine to create or delete context */
            if (nmhSetFsMIStdOspfv3Status ((INT4) u4ContextId, i4RowStat) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

        if (i4PrevAdminStat != i4AdminStat)
        {

            /* Checks whether the passed parameter value falls 
             * within the mib range */
            if (nmhTestv2FsMIStdOspfv3AdminStat
                (&u4ErrorCode, (INT4) u4ContextId, i4AdminStat) == SNMP_FAILURE)
            {
                i4RowStat = DESTROY;
                nmhSetFsMIStdOspfv3Status ((INT4) u4ContextId, i4RowStat);
                return CLI_FAILURE;
            }

            /* Calls the SNMP set routine to set the value    */
            if (nmhSetFsMIStdOspfv3AdminStat ((INT4) u4ContextId, i4AdminStat)
                == SNMP_FAILURE)
            {
                i4RowStat = DESTROY;
                nmhSetFsMIStdOspfv3Status (u4ContextId, i4RowStat);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_MODE_ROUTER_OSPF3, u4ContextId);

    if (i4AdminStat == OSPFV3_ENABLED)
    {
        CliChangePath ((CONST CHR1 *) au1Cmd);
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigAreaType
  
  Description  : This Routine set area type   
  
  Input(s)     : i4ImportAsExtern - Area Type
           i4AreaSummary - Enable or Disable sending Summary Lsas 
         to NSS/Stub Area
         u4AreaId - Area Id, Index to area table

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigAreaType (tCliHandle cliHandle, INT4 i4ImportAsExtern,
                         INT4 i4AreaSummary, UINT4 u4AreaId, UINT4 u4CmdStatus,
                         INT4 i4StubMetric, INT4 i4MetricType)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    UINT4               u4NssaStabilityInterval = OSPFV3_INIT_VAL;
    UINT4               u4Ospfv3StubMetric = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3AreaStatus ((INT4) u4AreaId, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaStatus ((INT4) u4AreaId, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /*Checks the value of Stability Interval for NSSA area before it changes the area type.
     *If Stability Interval has been configured, then it changes it back to default value as
     * stability Interval configuration is only allowed for NSSA areas */

    if (nmhGetOspfv3AreaNssaTranslatorStabilityInterval
        (u4AreaId, &u4NssaStabilityInterval) == SNMP_SUCCESS)
    {
        if (u4NssaStabilityInterval != OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL)
        {
            if (nmhSetOspfv3AreaNssaTranslatorStabilityInterval
                (u4AreaId, OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }

    }

    /*Checks the value of Default Metric for NSSA area before it changes the area type.
     * If Default-Metric has been configured, then it changes it back to default value as
     * Default-metric configuration is only allowed for NSSA Area */

    if (nmhGetOspfv3StubMetric (u4AreaId, (INT4 *) &u4Ospfv3StubMetric) ==
        SNMP_SUCCESS)
    {
        if (u4Ospfv3StubMetric != OSPFV3_DEFAULT_SUMM_COST)
        {
            if (nmhSetOspfv3StubMetric (u4AreaId, OSPFV3_DEFAULT_SUMM_COST) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3ImportAsExtern
        (&u4ErrorCode, u4AreaId, i4ImportAsExtern) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3ImportAsExtern (u4AreaId, i4ImportAsExtern) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }
    /* Setting the no-summary or SendArea-summary depending on whether the 
     * optional argument is specified or not */

    /* Checks whether the passed parameter value falls within the mib range */

    if (nmhTestv2Ospfv3AreaSummary (&u4ErrorCode, u4AreaId, i4AreaSummary)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3AreaSummary (u4AreaId, i4AreaSummary) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (u4CmdStatus == OSPFV3_CLI_DEF_INFO_ORIG)
    {

        if (nmhTestv2Ospfv3AreaStubMetricType
            (&u4ErrorCode, u4AreaId, i4MetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaStubMetricType (u4AreaId, i4MetricType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2Ospfv3StubMetric (&u4ErrorCode, u4AreaId, i4StubMetric) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3StubMetric (u4AreaId, i4StubMetric) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2Ospfv3AreaDfInfOriginate
            (&u4ErrorCode, u4AreaId,
             OSPFV3_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaDfInfOriginate
            (u4AreaId, OSPFV3_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2Ospfv3AreaDfInfOriginate
            (&u4ErrorCode, u4AreaId,
             OSPFV3_NO_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaDfInfOriginate
            (u4AreaId, OSPFV3_NO_DEFAULT_INFO_ORIGINATE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

    }

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigNSSAStabilityIntval
  
  Description  : This Routine set stability interval for NSSA area
  
  Input(s)     : i4Interval - Stability interval
         u4AreaId - Area Id, Index to area table

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigNSSAStabilityIntval (tCliHandle cliHandle,
                                    UINT4 u4Interval, UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (u4Interval == (UINT4) OSPFV3_NSSA_INVALID_STBLTY_INTRVL)
    {
        if (nmhGetOspfv3AreaStatus ((INT4) u4AreaId, &i4StatusVal) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (OSPFV3_CLI_NO_AREA);
            return CLI_FAILURE;
        }
        u4Interval = OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL;
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3AreaNssaTranslatorStabilityInterval (&u4ErrorCode,
                                                            u4AreaId,
                                                            u4Interval) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3AreaNssaTranslatorStabilityInterval
        (u4AreaId, u4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigNSSATranslatorRole
  
  Description  : This Routine set translator role for NSSA area
  
  Input(s)     : i4TransRole - Translator role
         u4AreaId - Area Id, Index to area table

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigNSSATranslatorRole (tCliHandle cliHandle,
                                   INT4 i4TransRole, UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3AreaStatus ((INT4) u4AreaId, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaStatus ((INT4) u4AreaId, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3AreaNssaTranslatorRole
        (&u4ErrorCode, u4AreaId, i4TransRole) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3AreaNssaTranslatorRole (u4AreaId, i4TransRole) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigStubMetric
  
  Description  : This Routine set Metric value 
  
  Input(s)     : i4StubMetric - Metric value
         u4AreaId - Area Id, Index to area table

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigStubMetric (tCliHandle cliHandle, INT4 i4StubMetric,
                           UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3AreaStatus ((INT4) u4AreaId, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaStatus ((INT4) u4AreaId, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3StubMetric (&u4ErrorCode, u4AreaId, i4StubMetric)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3StubMetric (u4AreaId, i4StubMetric) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigStubMetricType
  
  Description  : This Routine set Metric Type 
  
  Input(s)     : i4StubMetricType - Metric type
         u4AreaId - Area Id, Index to area table

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigStubMetricType (tCliHandle cliHandle,
                               INT4 i4StubMetricType, UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;

    /*Get the Interface Status and check whether the entry is already created   
     * or not, create an entry in the interface table */

    if (nmhGetOspfv3AreaStatus ((INT4) u4AreaId, &i4StatusVal) == SNMP_FAILURE)

    {
        if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3AreaStatus ((INT4) u4AreaId, OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3AreaStubMetricType
        (&u4ErrorCode, u4AreaId, i4StubMetricType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3AreaStubMetricType (u4AreaId, i4StubMetricType) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, (INT4) u4AreaId,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigVirtualLink
  
  Description  : This Routine set or delete virrual link
  
  Input(s)     :
                u4RouterId - Router id
                u4AreaId - Area Id
                i4HelloInt - Hello Interval 
                i4RetransInt - Retransmit Interval
                i4TransitDelay - Transit Delay
                i4RtrDeadInt - Router Dead Interval
                u2CryptoAuthType - Crypto graphic authentication Type
                u1BitMask - Indicates, optional parameters to be set
                u1Set = OSPFV3_VAL_SET, set given value
                u1Set = OSPFV3_VAL_NO, delete/disable the object 
                i4AuthkeyId = Authentication Key ID
                au1AuthKey = Authentuication Key
  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigVirtualLink (tCliHandle cliHandle, UINT4 u4RouterId,
                            UINT4 u4AreaId, tV3OsInterface * pInterface,
                            UINT4 u4BitMask, UINT1 u1Set, UINT4 u4OspfCxtId,
                            INT4 i4AuthkeyId, UINT1 *au1AuthKey)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4RetVal = OSPFV3_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    INT4                i4CryptoAuthMode = OSPFV3_AUTH_MODE_TRANS;
    INT4                i4KeyStatus = AUTHKEY_STATUS_VALID;

    if (u1Set == OSPFV3_VAL_NO)
    {
        i4CryptoAuthMode = OSPFV3_AUTH_MODE_DEF;
        i4KeyStatus = AUTHKEY_STATUS_DELETE;
    }

    if (nmhGetFsMIStdOspfv3VirtIfState
        ((INT4) u4OspfCxtId, u4AreaId, u4RouterId, &i4RetVal) == SNMP_FAILURE)
    {
        if (u1Set == OSPFV3_VAL_NO)    /* Delete Virual Link */
        {
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return CLI_FAILURE;
        }

        /* Creating the row instance for the OSPF Virtual iftable 
         * using the */
        /* CREATE AND WAIT */
        if (nmhTestv2FsMIStdOspfv3VirtIfStatus
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfStatus
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

    }

    /* Delete Virual Link */
    if ((u1Set == OSPFV3_VAL_NO) && (u4BitMask == OSPFV3_CLI_NO_OPTIONS))
    {
        /* Destroying the row instance for the OSPFV3 Virtual iftable */
        if (nmhTestv2FsMIStdOspfv3VirtIfStatus
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfStatus
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (u1Set != OSPFV3_VAL_NO)
    {
        if (nmhTestv2FsMIStdOspfv3VirtIfIndex
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             (INT4) pInterface->u4InterfaceId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfIndex
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             (INT4) pInterface->u4InterfaceId) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_HELLO_INT_BIT)
    {
        /* Set the Hello interval  */
        /* Checks whether the passed parameter value falls within 
         * the mib range */

        if (nmhTestv2FsMIStdOspfv3VirtIfHelloInterval
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2HelloInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfHelloInterval
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             (INT4) pInterface->u2HelloInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_RETRANS_INT_BIT)
    {
        /* Set the Retransmission interval  */
        /* Checks whether the passed parameter value falls within 
         * the mib range */

        if (nmhTestv2FsMIStdOspfv3VirtIfRetransInterval
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2RxmtInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfRetransInterval
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2RxmtInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_TRANSIT_DELAY_BIT)
    {
        /* Set the Transit Delay  */
        /* Checks whether the passed parameter value falls within 
         * the mib range */

        if (nmhTestv2FsMIStdOspfv3VirtIfTransitDelay
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2IfTransDelay) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfTransitDelay
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2IfTransDelay) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_DEAD_INT_BIT)
    {
        /* Set the Router Dead Interval  */
        /* Checks whether the passed parameter value falls within t
         * he mib range */

        if (nmhTestv2FsMIStdOspfv3VirtIfRtrDeadInterval
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2RtrDeadInterval) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsMIStdOspfv3VirtIfRtrDeadInterval
            ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2RtrDeadInterval) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_VIRT_AUTH_BIT)
    {
        /* Set the Authentication type for the virtual interface  */
        if (nmhTestv2FsMIOspfv3VirtIfCryptoAuthType
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             pInterface->u2CryptoAuthType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value */
        if (nmhSetFsMIOspfv3VirtIfCryptoAuthType ((INT4) u4OspfCxtId, u4AreaId,
                                                  u4RouterId,
                                                  pInterface->
                                                  u2CryptoAuthType) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       " nmhSetFsMIOspfv3VirtIfCryptoAuthType failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_VIRT_AUTH_MODE_BIT)
    {
        /* Set the Authentication type for the virtual interface  */
        if (nmhTestv2FsMIOspfv3VirtIfCryptoAuthMode
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
             i4CryptoAuthMode) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Calls the SNMP set routine to set the value */
        if (nmhSetFsMIOspfv3VirtIfCryptoAuthMode ((INT4) u4OspfCxtId, u4AreaId,
                                                  u4RouterId,
                                                  i4CryptoAuthMode) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "nmhSetFsMIOspfv3VirtIfCryptoAuthMode failed\r\n");
            return CLI_FAILURE;
        }
    }

    if (u4BitMask & OSPFV3_CLI_SET_VIRT_AUTH_KEY_BIT)
    {
        /* Set the Authentication Key for the virtual interface  */
        if (i4KeyStatus == AUTHKEY_STATUS_DELETE)
        {
            if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus
                (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
                 i4AuthkeyId, DESTROY) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIOspfv3VirtIfAuthKeyStatus
                ((INT4) u4OspfCxtId, u4AreaId, u4RouterId, i4AuthkeyId,
                 DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "nmhSetFsMIOspfv3VirtIfAuthKeyStatus failed\r\n");
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
        }

        if (i4KeyStatus == AUTHKEY_STATUS_VALID)
        {
            pOctetString = SNMP_AGT_FormOctetString (au1AuthKey,
                                                     (INT4)
                                                     STRLEN (au1AuthKey));

            if (pOctetString == NULL)
            {
                return CLI_FAILURE;
            }
            if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus
                (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
                 i4AuthkeyId, CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIOspfv3VirtIfAuthKeyStatus
                ((INT4) u4OspfCxtId, u4AreaId, u4RouterId, i4AuthkeyId,
                 CREATE_AND_WAIT) == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "nmhSetFsMIOspfv3VirtIfAuthKeyStatus failed\r\n");
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            if (nmhTestv2FsMIOspfv3VirtIfAuthKey
                (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
                 i4AuthkeyId, pOctetString) == SNMP_FAILURE)
            {
                nmhSetFsMIOspfv3VirtIfAuthKeyStatus ((INT4) u4OspfCxtId,
                                                     u4AreaId, u4RouterId,
                                                     i4AuthkeyId, DESTROY);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            /* Calls the SNMP set routine to set the value */
            if (nmhSetFsMIOspfv3VirtIfAuthKey
                ((INT4) u4OspfCxtId, u4AreaId, u4RouterId, i4AuthkeyId,
                 pOctetString) == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "nmhSetFsMIOspfv3VirtIfAuthKey failed\r\n");
                nmhSetFsMIOspfv3VirtIfAuthKeyStatus ((INT4) u4OspfCxtId,
                                                     u4AreaId, u4RouterId,
                                                     i4AuthkeyId, DESTROY);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }

            if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus
                (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
                 i4AuthkeyId, ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsMIOspfv3VirtIfAuthKeyStatus ((INT4) u4OspfCxtId,
                                                     u4AreaId, u4RouterId,
                                                     i4AuthkeyId, DESTROY);
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            if (nmhSetFsMIOspfv3VirtIfAuthKeyStatus
                ((INT4) u4OspfCxtId, u4AreaId, u4RouterId, i4AuthkeyId,
                 ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsMIOspfv3VirtIfAuthKeyStatus ((INT4) u4OspfCxtId,
                                                     u4AreaId, u4RouterId,
                                                     i4AuthkeyId, DESTROY);
                CliPrintf (cliHandle,
                           "nmhSetFsMIOspfv3VirtIfAuthKeyStatus failed\r\n");
                SNMP_AGT_FreeOctetString (pOctetString);
                return CLI_FAILURE;
            }
            SNMP_AGT_FreeOctetString (pOctetString);
        }
    }

    /* After setting all the values in the VirtOspfv3 iftable,make the Row
     * status as 'Active'. */

    if (nmhTestv2FsMIStdOspfv3VirtIfStatus
        (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId, u4RouterId,
         OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIStdOspfv3VirtIfStatus
        ((INT4) u4OspfCxtId, u4AreaId, u4RouterId,
         OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigASBdrRtrStatus
  
  Description  : This Routine set ASBR status
  
  Input(s)      : i4ASBRStatus -  Enable or Disable ASBR status

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigASBdrRtrStatus (tCliHandle cliHandle, INT4 i4ASBRStatus)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2Ospfv3ASBdrRtrStatus (&u4ErrorCode, i4ASBRStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3ASBdrRtrStatus (i4ASBRStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigAreaRange
  
  Description  : This Routine set or delete area address range
  
  Input(s)      : summPrefix - Ipv6 Prefix
          u1PrefixLen - Prefix Length
          u4AreaId - Area Id
          i4LsaType - Lsa Type for aggregation
          i4SummEffect - Aggregation effect advertise/not-advertise
          i4Tag - Used for NSSA
                u1Set = OSPFV3_VAL_SET, set given value
          u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
          the object 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigAreaRange (tCliHandle cliHandle, tIp6Addr summPrefix,
                          UINT1 u1PrefixLen, UINT4 u4AreaId,
                          INT4 i4LsaType, INT4 i4SummEffect,
                          INT4 i4Tag, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    INT4                i4PrefixType = OSPFV3_INET_ADDR_TYPE;
    tSNMP_OCTET_STRING_TYPE octetString;

    octetString.pu1_OctetList = (UINT1 *) &summPrefix;
    octetString.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    /*Get the Area Aggregate Status and check whether the entry is already 
     * created or not, create an entry in the interface table */

    if (nmhGetOspfv3AreaAggregateStatus (u4AreaId, i4LsaType, i4PrefixType,
                                         &octetString, u1PrefixLen,
                                         &i4StatusVal) == SNMP_FAILURE)
    {

        if (u1Set == OSPFV3_VAL_NO)    /* Delete Area Aggregare Entry */
        {
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return CLI_FAILURE;
        }

        if (nmhTestv2Ospfv3AreaAggregateStatus (&u4ErrorCode, u4AreaId,
                                                i4LsaType, i4PrefixType,
                                                &octetString, u1PrefixLen,
                                                OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3AreaAggregateStatus
            (u4AreaId, i4LsaType, i4PrefixType, &octetString, u1PrefixLen,
             OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u1Set == OSPFV3_VAL_NO)    /* Delete Area Aggragte Entry */
    {
        if (nmhTestv2Ospfv3AreaAggregateStatus
            (&u4ErrorCode, u4AreaId, i4LsaType, i4PrefixType,
             &octetString,
             u1PrefixLen, OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3AreaAggregateStatus
            (u4AreaId, i4LsaType, i4PrefixType, &octetString, u1PrefixLen,
             OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3AreaAggregateEffect (&u4ErrorCode, u4AreaId,
                                            i4LsaType, i4PrefixType,
                                            &octetString,
                                            u1PrefixLen,
                                            i4SummEffect) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3AreaAggregateEffect (u4AreaId, i4LsaType, i4PrefixType,
                                         &octetString, u1PrefixLen,
                                         i4SummEffect) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2Ospfv3AreaAggregateRouteTag
        (&u4ErrorCode, u4AreaId, i4LsaType, i4PrefixType, &octetString,
         u1PrefixLen, i4Tag) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetOspfv3AreaAggregateRouteTag
        (u4AreaId, i4LsaType, i4PrefixType, &octetString, u1PrefixLen,
         i4Tag) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3AreaAggregateStatus
        (&u4ErrorCode, u4AreaId, i4LsaType, i4PrefixType, &octetString,
         u1PrefixLen, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetOspfv3AreaAggregateStatus (u4AreaId, i4LsaType, i4PrefixType,
                                         &octetString, u1PrefixLen,
                                         OSPFV3_CLI_STATUS_ACTIVE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name : V3OspfCliConfigAsExternalRangeInCxt
  
  Description   : This Routine set or delete external area address range
  
  Input(s)      : cliHandle    - Used by CliPrintf 
                  u4ContextId  - ospf context id
                  summPrefix   - Ipv6 Prefix
                  u1PrefixLen  - Prefix Length 
                  u4AreaId     - Area Id 
                  i4SummEffect - Aggregation effect allowAll/denyAll/advertise/
                                 not-advertise 
                  i4Trans      - Translation status 
                  u1Set        - OSPFV3_VAL_SET, set given value.
                                 OSPFV3_VAL_NO,set default value or 
                                 delete/disable the object 
  Output(s)     : None             
                 
  Return Values : CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigAsExternalRangeInCxt (tCliHandle cliHandle,
                                     tV3OspfCxt * pV3OspfCxt,
                                     tIp6Addr summPrefix, UINT1 u1PrefixLen,
                                     UINT4 u4AreaId, INT4 i4SummEffect,
                                     INT4 i4Trans, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    INT4                i4PrefixType = OSPFV3_INET_ADDR_TYPE;
    tSNMP_OCTET_STRING_TYPE octetString;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tIp6Addr            asExtNet;
    tV3OsAreaId         areaId;
    octetString.pu1_OctetList = (UINT1 *) &summPrefix;
    octetString.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    /*Get the Area Aggregate Status and check whether the entry is already 
     * created or not, create an entry in the interface table */

    if (nmhGetFutOspfv3AsExternalAggregationStatus
        (i4PrefixType, &octetString, u1PrefixLen, u4AreaId,
         &i4StatusVal) == SNMP_FAILURE)
    {

        if (u1Set == OSPFV3_VAL_NO)    /* Delete Area Aggregare Entry */
        {
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return CLI_FAILURE;
        }

        if (nmhTestv2FutOspfv3AsExternalAggregationStatus (&u4ErrorCode,
                                                           i4PrefixType,
                                                           &octetString,
                                                           u1PrefixLen,
                                                           u4AreaId,
                                                           OSPFV3_CLI_STATUS_CW)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3AsExternalAggregationStatus
            (i4PrefixType, &octetString, u1PrefixLen, u4AreaId,
             OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u1Set == OSPFV3_VAL_NO)    /* Delete Area Aggragte Entry */
    {
        if (nmhTestv2FutOspfv3AsExternalAggregationStatus (&u4ErrorCode,
                                                           i4PrefixType,
                                                           &octetString,
                                                           u1PrefixLen,
                                                           u4AreaId,
                                                           OSPFV3_CLI_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3AsExternalAggregationStatus
            (i4PrefixType, &octetString, u1PrefixLen, u4AreaId,
             OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    /*Checks whether the passed parameter value falls within the mib range */

    OSPFV3_IP6_ADDR_COPY (asExtNet.u1_addr, *(octetString.pu1_OctetList));

    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);

    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt, &asExtNet, u1PrefixLen,
                                            areaId);

    if ((pAsExtAddrRng != NULL) && (pAsExtAddrRng->rangeStatus == ACTIVE))
    {
        pAsExtAddrRng->rangeStatus = NOT_IN_SERVICE;

    }

    if (nmhTestv2FutOspfv3AsExternalAggregationEffect (&u4ErrorCode,
                                                       i4PrefixType,
                                                       &octetString,
                                                       u1PrefixLen,
                                                       u4AreaId,
                                                       i4SummEffect) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetFutOspfv3AsExternalAggregationEffect
        (i4PrefixType, &octetString, u1PrefixLen, u4AreaId,
         i4SummEffect) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    /*Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3AsExternalAggregationTranslation (&u4ErrorCode,
                                                            i4PrefixType,
                                                            &octetString,
                                                            u1PrefixLen,
                                                            u4AreaId,
                                                            i4Trans) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value */
    if (nmhSetFutOspfv3AsExternalAggregationTranslation
        (i4PrefixType, &octetString, u1PrefixLen, u4AreaId, i4Trans)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfv3AsExternalAggregationStatus (&u4ErrorCode,
                                                       i4PrefixType,
                                                       &octetString,
                                                       u1PrefixLen,
                                                       u4AreaId,
                                                       OSPFV3_CLI_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the Row Status to Active after the Set Operation */
    if (nmhSetFutOspfv3AsExternalAggregationStatus
        (i4PrefixType, &octetString, u1PrefixLen, u4AreaId,
         OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigRedistribute
  
  Description  : This Routine Enable or Disable redistribution protocol
  
  Input(s)      : i4RedistProtoMask - Redistribution protocol
                u1Set = OSPFV3_VAL_SET, set given value
          u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
          the object 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigRedistribute (tCliHandle cliHandle,
                             INT4 i4RedistProtoMask, UINT1 u1Set,
                             UINT1 *pu1RouteMapName, INT4 i4MetricValue,
                             INT4 i4MetricType)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4ProtoMaskValue = OSPFV3_INIT_VAL;
    INT4                i4ProtoId = 0;
    INT4                i4Count = 0;

    /* Based on the passed protocol, set the protcol ID */
    switch (i4RedistProtoMask)
    {
        case OSPFV3_CLI_LOCAL_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_CONNECTED;
            break;
        case OSPFV3_CLI_STATIC_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_STATIC;
            break;
        case OSPFV3_CLI_RIPNG_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_RIP;
            break;
        case OSPFV3_CLI_BGPPLUS_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_BGP;
            break;
        case OSPFV3_CLI_ISISL1_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_ISIS;
            break;
        case OSPFV3_CLI_ISISL2_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_ISIS;
            break;
        case OSPFV3_CLI_ISISL1L2_RT_MASK:
            i4ProtoId = OSPFV3_REDISTRUTE_ISIS;
            break;
        default:
            break;
    }

    /* Enabling the Redistribution */
    if (u1Set == OSPFV3_VAL_SET)
    {
        if (nmhTestv2FutOspfv3RRDStatus (&u4ErrorCode, OSPFV3_ENABLED)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RRDStatus (OSPFV3_ENABLED) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

        nmhGetFutOspfv3RRDSrcProtoMask (&i4ProtoMaskValue);

        i4ProtoMaskValue |= i4RedistProtoMask;
        if (pu1RouteMapName != NULL)
        {
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = (INT4) STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] =
                OSPFV3_INIT_VAL;

            if (nmhTestv2FutOspfv3RRDRouteMapName (&u4ErrorCode,
                                                   &RouteMapName)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFutOspfv3RRDRouteMapName (&RouteMapName) == SNMP_FAILURE)
            {
                CLI_SET_ERR (OSPFV3_CLI_INV_ASSOC_RMAP);
                return CLI_FAILURE;
            }
        }

        if (nmhTestv2FutOspfv3RRDSrcProtoMask (&u4ErrorCode,
                                               i4ProtoMaskValue) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RRDSrcProtoMask (i4ProtoMaskValue) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        if (i4ProtoId != 0)
        {
            if (nmhTestv2FutOspfv3RRDMetricValue (&u4ErrorCode,
                                                  i4ProtoId, i4MetricValue) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFutOspfv3RRDMetricValue (i4ProtoId,
                                               i4MetricValue) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            for (i4Count = 1; i4Count <= OSPFV3_MAX_PROTO_REDISTRUTE_SIZE;
                 i4Count++)
            {
                if (nmhTestv2FutOspfv3RRDMetricValue
                    (&u4ErrorCode, i4Count, i4MetricValue) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFutOspfv3RRDMetricValue (i4Count,
                                                   i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

        if (i4MetricType != 0)
        {
            if (i4ProtoId != 0)
            {
                if (nmhTestv2FutOspfv3RRDMetricType (&u4ErrorCode, i4ProtoId,
                                                     i4MetricType) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFutOspfv3RRDMetricType (i4ProtoId,
                                                  i4MetricType) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                for (i4Count = 1; i4Count <= OSPFV3_MAX_PROTO_REDISTRUTE_SIZE;
                     i4Count++)
                {
                    if (nmhTestv2FutOspfv3RRDMetricType
                        (&u4ErrorCode, i4Count, i4MetricType) == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhSetFutOspfv3RRDMetricType (i4Count,
                                                      i4MetricType) ==
                        SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                }
            }
        }

    }
    else                        /* Disabling the redistribution */
    {
        if (pu1RouteMapName != NULL)
        {
            MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
            RouteMapName.pu1_OctetList = au1RMapName;
            if (nmhGetFutOspfv3RRDRouteMapName (&RouteMapName) == SNMP_SUCCESS)
            {
                if ((STRLEN (pu1RouteMapName) == 0)
                    ||
                    (MEMCMP
                     (pu1RouteMapName, RouteMapName.pu1_OctetList,
                      STRLEN (pu1RouteMapName)) != 0))
                {
                    CLI_SET_ERR (OSPFV3_CLI_RMAP_NOT_PREV_ASSOC);
                    return CLI_FAILURE;
                }
            }
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = (INT4) STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] =
                OSPFV3_INIT_VAL;

            if (nmhTestv2FutOspfv3RRDRouteMapName (&u4ErrorCode,
                                                   &RouteMapName)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (OSPFV3_CLI_INV_DISASSOC_RMAP);
                return CLI_FAILURE;
            }

            RouteMapName.i4_Length = 0;
            if (nmhSetFutOspfv3RRDRouteMapName (&RouteMapName) == SNMP_FAILURE)
            {
                CLI_SET_ERR (OSPFV3_CLI_INV_DISASSOC_RMAP);
                return CLI_FAILURE;
            }
        }

        nmhGetFutOspfv3RRDSrcProtoMask (&i4ProtoMaskValue);

        i4ProtoMaskValue &= (~(i4RedistProtoMask));

        if (nmhTestv2FutOspfv3RRDSrcProtoMask (&u4ErrorCode,
                                               i4ProtoMaskValue) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RRDSrcProtoMask (i4ProtoMaskValue) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

        if (i4ProtoId != 0)
        {
            if (nmhTestv2FutOspfv3RRDMetricValue (&u4ErrorCode,
                                                  i4ProtoId, i4MetricValue) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFutOspfv3RRDMetricValue (i4ProtoId,
                                               i4MetricValue) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhTestv2FutOspfv3RRDMetricType (&u4ErrorCode, i4ProtoId,
                                                 i4MetricType) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFutOspfv3RRDMetricType (i4ProtoId,
                                              i4MetricType) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            for (i4Count = 1; i4Count <= OSPFV3_MAX_PROTO_REDISTRUTE_SIZE;
                 i4Count++)
            {
                if (nmhTestv2FutOspfv3RRDMetricValue
                    (&u4ErrorCode, i4Count, i4MetricValue) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFutOspfv3RRDMetricValue (i4Count,
                                                   i4MetricValue) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhTestv2FutOspfv3RRDMetricType
                    (&u4ErrorCode, i4Count, i4MetricType) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFutOspfv3RRDMetricType (i4Count,
                                                  i4MetricType) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
    }

    return CLI_SUCCESS;
}

#ifdef ROUTEMAP_WANTED
/*******************************************************************************
  Function Name: V3OspfCliSetDistribute

  Description  : This Routine Enable or Disable inbound filtering

  Input(s)      : 1. cliHandle        -  CLI context ID
                  2. pu1RouteMapName  -  Route map name
                  3. u1Status         -  Enable/Disable.

  Output(s)    : None

  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliSetDistribute (tCliHandle cliHandle,
                        UINT1 *pu1RMapName, UINT1 u1Status)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = OSPFV3_CLI_RMAP_ASSOC_FAILED;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4RowStatus;

    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    RouteMapName.pu1_OctetList = au1RMapName;

    UNUSED_PARAM (cliHandle);

    if (pu1RMapName != NULL)
    {
        RouteMapName.i4_Length = (INT4) STRLEN (pu1RMapName);
        if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
        {
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                    RouteMapName.i4_Length);
        }

        if (CLI_ENABLE == u1Status)
        {
            if (nmhGetFutOspfv3DistInOutRouteMapRowStatus (&RouteMapName,
                                                           FILTERING_TYPE_DISTRIB_IN,
                                                           &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
                    {
                        return SNMP_FAILURE;
                    }
                    if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                        (&RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         CREATE_AND_WAIT) == SNMP_SUCCESS)
                    {
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }
                }
            }
            else
            {
                u4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            if (SNMP_ERR_NO_ERROR == u4ErrorCode)
            {
                if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                        (&RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
        else if (CLI_DISABLE == u1Status)
        {
            u4CliError = OSPFV3_CLI_RMAP_NOT_PREV_ASSOC;
            if (nmhGetFutOspfv3DistInOutRouteMapRowStatus (&RouteMapName,
                                                           FILTERING_TYPE_DISTRIB_IN,
                                                           &i4RowStatus) ==
                SNMP_SUCCESS)
            {
                if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                     DESTROY) == SNMP_SUCCESS)
                {
                    if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                        (&RouteMapName, FILTERING_TYPE_DISTRIB_IN,
                         DESTROY) == SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}
#endif

/*******************************************************************************
  Function Name: V3OspfCliConfigDefaultPassiveIface
  
  Description  : This Routine Enable or Disable global passive interface
  
  Input(s)      : i4DefPassive - Enable or Disable

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigDefaultPassiveIface (tCliHandle cliHandle, INT4 i4DefPassive)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2FutOspfv3DefaultPassiveInterface
        (&u4ErrorCode, i4DefPassive) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3DefaultPassiveInterface (i4DefPassive) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigHost
  
  Description  : This Routine set or delete Host  entry
  
  Input(s)     : hostAddr - Host Ipv6 address
           i4Metric - Metric value
           u4AreaId - Area Id
               u1Set = OSPFV3_VAL_SET, set given value
         u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
         the object 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigHost (tCliHandle cliHandle, tIp6Addr hostAddr,
                     INT4 i4Metric, UINT4 u4AreaId, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    INT4                i4HostAddrType = OSPFV3_INET_ADDR_TYPE;
    UINT1               u1Flag4AreaId = OSIX_FALSE;
    tSNMP_OCTET_STRING_TYPE octetString;

    octetString.pu1_OctetList = (UINT1 *) &hostAddr;
    octetString.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    if (nmhGetOspfv3HostStatus (i4HostAddrType,
                                &octetString, &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1Set == OSPFV3_VAL_NO)    /* Delete Host Entry */
        {
            CLI_SET_ERR (OSPFV3_CLI_ERR_GET_STATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhTestv2Ospfv3HostStatus (&u4ErrorCode,
                                       i4HostAddrType,
                                       &octetString,
                                       OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3HostStatus (i4HostAddrType,
                                    &octetString,
                                    OSPFV3_CLI_STATUS_CW) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u1Set == OSPFV3_VAL_SET)    /* Set the Area Id */
    {
        if (nmhTestv2Ospfv3HostAreaID (&u4ErrorCode,
                                       i4HostAddrType,
                                       &octetString, u4AreaId) == SNMP_FAILURE)
        {
            u1Flag4AreaId = OSIX_TRUE;
            u1Set = OSPFV3_VAL_NO;
        }

        if (u1Flag4AreaId == OSIX_FALSE)
        {
            if (nmhSetOspfv3HostAreaID (i4HostAddrType,
                                        &octetString, u4AreaId) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (cliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (u1Set == OSPFV3_VAL_NO)    /* Delete Host Entry */
    {
        if (nmhTestv2Ospfv3HostStatus (&u4ErrorCode, i4HostAddrType,
                                       &octetString,
                                       OSPFV3_CLI_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3HostStatus (i4HostAddrType,
                                    &octetString,
                                    OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }

        if (u1Flag4AreaId == OSIX_TRUE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2Ospfv3HostMetric (&u4ErrorCode,
                                   i4HostAddrType,
                                   &octetString, i4Metric) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfv3HostMetric (i4HostAddrType,
                                &octetString, i4Metric) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2Ospfv3HostStatus (&u4ErrorCode,
                                   i4HostAddrType,
                                   &octetString,
                                   OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfv3HostStatus (i4HostAddrType,
                                &octetString,
                                OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigDeleteArea
  
  Description  : This Routine delete Area entry
  
  Input(s)     : u4AreaId - Area Id

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigDeleteArea (tCliHandle cliHandle, UINT4 u4AreaId)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */

    if (nmhTestv2Ospfv3AreaStatus (&u4ErrorCode, u4AreaId,
                                   OSPFV3_CLI_STATUS_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3AreaStatus (u4AreaId, OSPFV3_CLI_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigNssaAsbrDfRtTrans
  
  Description  : This Routine Enable or Disable Nssa Asbr Default Route 
           Translation status
  
  Input(s)     : i4Status - Enable or Disable

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigNssaAsbrDfRtTrans (tCliHandle cliHandle, INT4 i4Status)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2FutOspfv3NssaAsbrDefRtTrans (&u4ErrorCode, i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3NssaAsbrDefRtTrans (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigRRDRoute
  
  Description   :This Routine configures or delete the information to be 
           applied to routes learnt from RTM6
  
  Input(s)     : routeDest - Destiantion Ipv6 address
         i4RoutePfxLen - Prefix length
         i4RouteMetric - Metric value
         i4RouteMetricType - Metric Type
         i4RouteTag - Route Tag
         u1TagFlag - optional tag value is set or not
               u1Set = OSPFV3_VAL_SET, set given value
         u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
         the object 
         
  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
******************************************************************************/
PRIVATE INT1
V3OspfCliConfigRRDRoute (tCliHandle cliHandle, tIp6Addr routeDest,
                         INT4 i4RoutePfxLen, INT4 i4RouteMetric,
                         INT4 i4RouteMetricType, INT4 i4RouteTag,
                         UINT1 u1TagFlag, UINT1 u1Set)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;
    INT4                i4StatusVal = OSPFV3_INIT_VAL;
    INT4                i4PrefixType = OSPFV3_INET_ADDR_TYPE;
    tSNMP_OCTET_STRING_TYPE octetString;

    octetString.pu1_OctetList = (UINT1 *) &routeDest;
    octetString.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    if (nmhGetFutOspfv3RedistRouteStatus (i4PrefixType, &octetString,
                                          i4RoutePfxLen,
                                          &i4StatusVal) == SNMP_FAILURE)
    {
        if (u1Set == OSPFV3_VAL_NO)    /* Delete Route Entry */
        {
            CLI_SET_ERR (OSPFV3_CLI_ERR_GET_STATUS_DESTROY);
            return CLI_FAILURE;
        }

        if (nmhTestv2FutOspfv3RedistRouteStatus (&u4ErrorCode, i4PrefixType,
                                                 &octetString,
                                                 (UINT4) i4RoutePfxLen,
                                                 OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RedistRouteStatus (i4PrefixType, &octetString,
                                              (UINT4) i4RoutePfxLen,
                                              OSPFV3_CLI_STATUS_CW) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (u1Set == OSPFV3_VAL_NO)    /* Delete Route Entry */
    {
        if (nmhTestv2FutOspfv3RedistRouteStatus (&u4ErrorCode, i4PrefixType,
                                                 &octetString,
                                                 i4RoutePfxLen,
                                                 OSPFV3_CLI_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RedistRouteStatus (i4PrefixType, &octetString,
                                              (UINT4) i4RoutePfxLen,
                                              OSPFV3_CLI_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    if (nmhTestv2FutOspfv3RedistRouteMetric (&u4ErrorCode, i4PrefixType,
                                             &octetString,
                                             (UINT4) i4RoutePfxLen,
                                             i4RouteMetric) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RedistRouteMetric (i4PrefixType, &octetString,
                                          i4RoutePfxLen,
                                          i4RouteMetric) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfv3RedistRouteMetricType (&u4ErrorCode, i4PrefixType,
                                                 &octetString,
                                                 i4RoutePfxLen,
                                                 i4RouteMetricType) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RedistRouteMetricType (i4PrefixType, &octetString,
                                              (UINT4) i4RoutePfxLen,
                                              i4RouteMetricType) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    if (u1TagFlag == OSIX_TRUE)
    {
        if (nmhTestv2FutOspfv3RedistRouteTag (&u4ErrorCode, i4PrefixType,
                                              &octetString, i4RoutePfxLen,
                                              i4RouteTag) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3RedistRouteTag (i4PrefixType, &octetString,
                                           i4RoutePfxLen,
                                           i4RouteTag) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FutOspfv3RedistRouteStatus (&u4ErrorCode, i4PrefixType,
                                             &octetString, i4RoutePfxLen,
                                             OSPFV3_CLI_STATUS_ACTIVE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RedistRouteStatus
        (i4PrefixType, &octetString, i4RoutePfxLen,
         OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Ospf3SetTraceValue                                          */
/* Description   : Sets the Trace Value                                      */
/* Input(s)      : i4TrapLevel - TrapLevel                                   */
/*                 u1LoggingCmd - Set or Reset                               */
/* Return Values : CLI_SUCCESS/CLI_FAILURE                                   */
/*****************************************************************************/

INT4
Ospf3SetTraceValue (INT4 i4TrapLevel, UINT1 u1LoggingCmd)
{
    UINT4               u4Trace = 0;
    if (i4TrapLevel == SYSLOG_CRITICAL_LEVEL)
    {
        u4Trace = (OSPFV3_CRITICAL_TRC);
        gV3OsRtr.u4GblTrcValue = (OSPFV3_CRITICAL_TRC);
    }
    else if (i4TrapLevel == SYSLOG_INFO_LEVEL)
    {
        u4Trace = OSPFV3_NSM_TRC | OSPFV3_CRITICAL_TRC;
    }
    if (V3OspfCliConfigTraceLevel (1, (INT4) u4Trace, 0, u1LoggingCmd) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigTraceLevel
  
  Description  : This Routine Enable or Disable Trace level 
  
  Input(s)     : i4TraceLevel - Trace Level
               u1Set = OSPFV3_VAL_SET, set given value
         u1Set = OSPFV3_VAL_NO,set default value or delete/disable 
         the object 

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
*******************************************************************************/
PRIVATE INT1
V3OspfCliConfigTraceLevel (tCliHandle cliHandle, INT4 i4TraceLevel,
                           INT4 i4ExtTraceLevel, UINT1 u1Set)
{

    INT4                i4TraceVal = OSPFV3_INIT_VAL;
    INT4                i4ExtTraceVal = OSPFV3_INIT_VAL;

    /* Get OSPFV3 Trace Option */
    nmhGetFutOspfv3TraceLevel (&i4TraceVal);

    /* Check whether trace or no trace command called */
    if (u1Set == OSPFV3_VAL_SET)
    {
        if (i4TraceVal & OSPFV3_CLI_DUMP_HGH_TRC)
        {
            if ((i4TraceLevel & OSPFV3_CLI_DUMP_LOW_TRC) ||
                (i4TraceLevel & OSPFV3_CLI_DUMP_HEX_TRC))
            {
                i4TraceVal &= (~(OSPFV3_CLI_DUMP_HGH_TRC));
            }
        }
        if (i4TraceVal & OSPFV3_CLI_DUMP_LOW_TRC)
        {
            if ((i4TraceLevel & OSPFV3_CLI_DUMP_HEX_TRC) ||
                (i4TraceLevel & OSPFV3_CLI_DUMP_HGH_TRC))
            {
                i4TraceVal &= (~(OSPFV3_CLI_DUMP_LOW_TRC));
            }
        }
        if (i4TraceVal & OSPFV3_CLI_DUMP_HEX_TRC)
        {
            if ((i4TraceLevel & OSPFV3_CLI_DUMP_HGH_TRC) ||
                (i4TraceLevel & OSPFV3_CLI_DUMP_LOW_TRC))
            {
                i4TraceVal &= (~(OSPFV3_CLI_DUMP_HEX_TRC));
            }
        }

        i4TraceVal |= i4TraceLevel;

    }
    else
    {
        if (i4TraceLevel != OSPFV3_INIT_VAL)
        {
            i4TraceVal &= (~(i4TraceLevel));
        }
        else
        {
            i4TraceVal = OSPFV3_CRITICAL_TRC;
        }
    }

    if (nmhSetFutOspfv3TraceLevel (i4TraceVal) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }
    if ((nmhGetFutOspfv3ExtTraceLevel (&i4ExtTraceVal)) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((i4ExtTraceLevel & OSPFV3_RESTART_TRC) != 0)
    {
        if (u1Set == CLI_ENABLE)
        {
            i4ExtTraceVal = i4ExtTraceVal | OSPFV3_RESTART_TRC;
        }
        else
        {
            i4ExtTraceVal = i4ExtTraceVal & (~(OSPFV3_RESTART_TRC));
        }
    }
    if ((i4ExtTraceLevel & OSPFV3_HELPER_TRC) != 0)
    {
        if (u1Set == CLI_ENABLE)
        {
            i4ExtTraceVal = i4ExtTraceVal | OSPFV3_HELPER_TRC;
        }
        else
        {
            i4ExtTraceVal = i4ExtTraceVal & (~(OSPFV3_HELPER_TRC));
        }
    }
    if ((i4ExtTraceLevel & OSPFV3_RM_TRC) != 0)
    {

        if (u1Set == CLI_ENABLE)
        {
            i4ExtTraceVal = i4ExtTraceVal | OSPFV3_RM_TRC;
        }
        else
        {
            i4ExtTraceVal = i4ExtTraceVal & (~(OSPFV3_RM_TRC));

        }
    }

    if (nmhSetFutOspfv3ExtTraceLevel (i4ExtTraceVal) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigExtAreaLsdbLimit
  
  Description  : This Routine set non default As-External Lsas limit
  
  Input(s)     : i4ExtLsdbLimit - Ext Lsdb limit

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
*******************************************************************************/
PRIVATE INT1
V3OspfCliConfigExtAreaLsdbLimit (tCliHandle cliHandle, INT4 i4ExtLsdbLimit)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2Ospfv3ExtAreaLsdbLimit (&u4ErrorCode, i4ExtLsdbLimit)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3ExtAreaLsdbLimit (i4ExtLsdbLimit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigExitOverflowInterval
  
  Description  : This Routine set Exit overflow interval
  
  Input(s)     : i4Interval - Exit overflow interval

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
*******************************************************************************/
PRIVATE INT1
V3OspfCliConfigExitOverflowInterval (tCliHandle cliHandle, INT4 i4Interval)
{
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3ExitOverflowInterval (i4Interval) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigDemandExtensions
  
  Description  : This Routine Enable or Disable router's support of 
           Demand Extension
  
  Input(s)     : i4DemandExt - Enable or Disable

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
*******************************************************************************/
PRIVATE INT1
V3OspfCliConfigDemandExtensions (tCliHandle cliHandle, INT4 i4DemandExt)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    /* Checks whether the passed parameter value falls within
     * the mib range */
    if (nmhTestv2Ospfv3DemandExtensions (&u4ErrorCode, i4DemandExt)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3DemandExtensions (i4DemandExt) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliConfigReferenceBandwidth
  
  Description  : This Routine set reference bandwidth 
  
  Input(s)     : i4RefBW - Reference Bandwidth

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or Error Message
*******************************************************************************/
PRIVATE INT1
V3OspfCliConfigReferenceBandwidth (tCliHandle cliHandle, UINT4 u4RefBW)
{
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetOspfv3ReferenceBandwidth (u4RefBW) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (cliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliAuthInterface
  
  Description  : This Routine Displays the interface authentication info 
  
  Input(s)     : pInterface - Interface Pointer

  Output(s)    : None             
*******************************************************************************/
PRIVATE VOID
V3OspfCliAuthInterface (tCliHandle cliHandle, tV3OsInterface * pInterface)
{
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN];
    tAuthkeyInfo       *pAuthkeyInfo;
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN);

    if (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH)
    {
        CliPrintf (cliHandle, "\r\nAuthentication Trailer enabled\r\n");

        pAuthkeyInfo = (tAuthkeyInfo *) V3GetAuthkeyTouse (pInterface);
        if (pAuthkeyInfo != NULL)
        {
            pInterface->pLastAuthkey = pAuthkeyInfo;
        }

        switch (pInterface->u2CryptoAuthType)
        {
            case OSPFV3_AUTH_SHA1:

                CliPrintf (cliHandle,
                           "sha-1 Authentication Algorithm is enabled\r\n");
                if (pInterface->pLastAuthkey != NULL)
                {
                    CliPrintf (cliHandle,
                               "sha-1 authentication key is configured\r\n");
                    CliPrintf (cliHandle, "Youngest key id is %d\r\n",
                               pInterface->pLastAuthkey->u2AuthkeyId);
                }
                break;

            case OSPFV3_AUTH_SHA2_256:

                CliPrintf (cliHandle,
                           "sha-256 Authentication Algorithm enabled\r\n");
                if (pInterface->pLastAuthkey != NULL)
                {
                    CliPrintf (cliHandle,
                               "sha-256 authentication key is configured\r\n");
                    CliPrintf (cliHandle, "Youngest key id is %d\r\n",
                               pInterface->pLastAuthkey->u2AuthkeyId);
                }
                break;
            case OSPFV3_AUTH_SHA2_384:
                CliPrintf (cliHandle,
                           "sha-384 Authentication Algorithm enabled\r\n");
                if (pInterface->pLastAuthkey != NULL)
                {
                    CliPrintf (cliHandle,
                               "sha-384 authentication key is configured\r\n");
                    CliPrintf (cliHandle, "Youngest key id is %d\r\n",
                               pInterface->pLastAuthkey->u2AuthkeyId);
                }
                break;
            case OSPFV3_AUTH_SHA2_512:

                CliPrintf (cliHandle,
                           "sha-512 Authentication Algorithm enabled\r\n");
                if (pInterface->pLastAuthkey != NULL)
                {
                    CliPrintf (cliHandle,
                               "sha-512 authentication key is configured\r\n");
                    CliPrintf (cliHandle, "Youngest key id is %d\r\n",
                               pInterface->pLastAuthkey->u2AuthkeyId);
                }
                break;
            default:
                CliPrintf (cliHandle, "UnKnown Authentication Algorithm\r\n");
                break;
        }

        if (pInterface->pLastAuthkey != NULL)
        {
            V3OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStartAccept,
                                au1KeyConstantTime);
            CliPrintf (cliHandle, "Key Start Accept Time is %s",
                       au1KeyConstantTime);

            V3OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStartGenerate,
                                au1KeyConstantTime);
            CliPrintf (cliHandle, "Key Start Generate Time is %s",
                       au1KeyConstantTime);

            V3OspfPrintKeyTime (pInterface->pLastAuthkey->
                                u4KeyStopGenerate, au1KeyConstantTime);
            CliPrintf (cliHandle, "Key Stop Generate Time is %s",
                       au1KeyConstantTime);

            V3OspfPrintKeyTime (pInterface->pLastAuthkey->u4KeyStopAccept,
                                au1KeyConstantTime);
            CliPrintf (cliHandle, "Key Stop Accept Time is %s",
                       au1KeyConstantTime);
        }
        else
        {
            CliPrintf (cliHandle,
                       "No key configured, using default key id 0\r\n");
        }
        if (pInterface->u1AuthMode != OSPFV3_AUTH_MODE_DEF)
        {
            CliPrintf (cliHandle, "Authentication Mode is Transition\r\n");
        }
    }
    else
    {
        CliPrintf (cliHandle, "\r\nAuthentication Trailer Disabled\r\n");
    }
}

/*******************************************************************************
  Function Name: V3OspfCliPrintOspfInterfaceInfo
  
  Description  : This routine prints interface table information  
  
  Input(s)     : pInterface - Pointer to structure tV3OsInterface
                 cliHandle  - Pointer to structure tCliHandle,
                 
  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliPrintOspfInterfaceInfo (tV3OsInterface * pInterface,
                                 tCliHandle cliHandle)
{
    UINT1              *pu1String = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4HelloTime = OSPFV3_INIT_VAL;
    tIp6Addr           *pIfDRIp6Addr = NULL;
    tIp6Addr           *pIfBDRIp6Addr = NULL;
    UINT1               au1String[OSPFV3_CLI_MAX_STRING_LEN];
    tShowOspfv3IfInfo   showOspfv3IfInfo;
    INT4                i4NbrCount = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];

    /* Print all the required values from the interface structure using the 
     * pointer */

    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (&showOspfv3IfInfo, 0, sizeof (tShowOspfv3IfInfo));
    CfaCliGetIfName (pInterface->u4InterfaceId,
                     (INT1 *) showOspfv3IfInfo.u1IfName);
    showOspfv3IfInfo.u4InterfaceId = pInterface->u4InterfaceId;

    if (pInterface->pArea != NULL)
    {
        V3UtilGetCxtName (pInterface->pArea->pV3OspfCxt->u4ContextId,
                          au1OspfCxtName);
        OSPFV3_AREA_ID_COPY (&(showOspfv3IfInfo.u4AreaId),
                             pInterface->pArea->areaId);
        OSPFV3_IP6_ADDR_COPY (showOspfv3IfInfo.ifIp6Addr,
                              pInterface->ifIp6Addr);
        OSPFV3_RTR_ID_COPY (&(showOspfv3IfInfo.u4RouterId),
                            pInterface->pArea->pV3OspfCxt->rtrId);
        if (pInterface->u1NetworkType < OSPFV3_MAX_IF_TYPE)
        {
            MEMCPY (showOspfv3IfInfo.u1IfType,
                    gau1Os3DbgIfType[pInterface->u1NetworkType],
                    STRLEN (gau1Os3DbgIfType[pInterface->u1NetworkType]));
        }
        showOspfv3IfInfo.u4IfMetric = pInterface->u4IfMetric;

        if (pInterface->u1IsmState < OSPFV3_MAX_IF_STATE)
        {
            MEMCPY (showOspfv3IfInfo.u1IsmState,
                    gau1Os3DbgIfState[pInterface->u1IsmState],
                    STRLEN (gau1Os3DbgIfState[pInterface->u1IsmState]));
        }

        OSPFV3_RTR_ID_COPY (&(showOspfv3IfInfo.desgRtr), pInterface->desgRtr);

        pIfDRIp6Addr =
            V3IfFindIfAddrFromRouterId (pInterface->desgRtr, pInterface);
        if (pIfDRIp6Addr != NULL)
        {
            OSPFV3_IP6_ADDR_COPY (showOspfv3IfInfo.drIp6Addr, *pIfDRIp6Addr);
        }

        OSPFV3_RTR_ID_COPY (&(showOspfv3IfInfo.backupDesgRtr),
                            pInterface->backupDesgRtr);

        pIfBDRIp6Addr =
            V3IfFindIfAddrFromRouterId (pInterface->backupDesgRtr, pInterface);
        if (pIfBDRIp6Addr != NULL)
        {
            OSPFV3_IP6_ADDR_COPY (showOspfv3IfInfo.bdrIp6Addr, *pIfBDRIp6Addr);
        }

        showOspfv3IfInfo.u2IfTransDelay = pInterface->u2IfTransDelay;
        showOspfv3IfInfo.u1RtrPriority = pInterface->u1RtrPriority;
        MEMCPY (showOspfv3IfInfo.u1Options, pInterface->ifOptions, 3);

        showOspfv3IfInfo.u2HelloInterval = pInterface->u2HelloInterval;
        showOspfv3IfInfo.u2RtrDeadInterval = pInterface->u2RtrDeadInterval;
        showOspfv3IfInfo.u2RxmtInterval = pInterface->u2RxmtInterval;
        showOspfv3IfInfo.u4PollInterval = pInterface->u4PollInterval;

        showOspfv3IfInfo.bDcEndpt = pInterface->bDcEndpt;

        showOspfv3IfInfo.bDemandNbrProbe = pInterface->bDemandNbrProbe;
        showOspfv3IfInfo.bLinkLsaSuppress = pInterface->bLinkLsaSuppress;

        showOspfv3IfInfo.u4NbrProbeRxmtLimit = pInterface->u4NbrProbeRxmtLimit;
        showOspfv3IfInfo.u4NbrProbeInterval = pInterface->u4NbrProbeInterval;

        if (pInterface->bPassive == OSIX_FALSE)
        {
            if (pInterface->pArea->pV3OspfCxt->admnStat == OSPFV3_ENABLED)
            {
                if (TmrGetRemainingTime (gV3OsRtr.hellotimerLstId,
                                         &(pInterface->
                                           helloTimer.timerNode),
                                         &(u4HelloTime)) == TMR_SUCCESS)
                {

                    showOspfv3IfInfo.u4HelloTime =
                        u4HelloTime / OSPFV3_NO_OF_TICKS_PER_SEC;
                }
                else
                {
                    showOspfv3IfInfo.u4HelloTime = OSPFV3_INIT_VAL;
                }
            }
        }

        showOspfv3IfInfo.i4NbrCount = TMO_SLL_Count (&(pInterface->nbrsInIf));

        if (showOspfv3IfInfo.i4NbrCount != OSPFV3_INIT_VAL)
        {
            showOspfv3IfInfo.pNbrsInIf = gaNbrsInIf;

            MEMSET (showOspfv3IfInfo.pNbrsInIf, 0,
                    /*showOspfv3IfInfo.i4NbrCount * sizeof (tNbrsInIf)); */
                    sizeof (tNbrsInIf));
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
            {
                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                            pLstNode);

                if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
                {
                    OSPFV3_RTR_ID_COPY
                        (&
                         (showOspfv3IfInfo.pNbrsInIf[i4NbrCount].
                          nbrRtrId), pNbr->nbrRtrId);
                    if (pNbr->u1NbrIfStatus < OSPFV3_MAX_NBR_TYPE)
                    {
                        MEMCPY
                            (showOspfv3IfInfo.pNbrsInIf[i4NbrCount].au1NbrType,
                             gau1Os3DbgNbrType[pNbr->u1NbrIfStatus],
                             STRLEN (gau1Os3DbgNbrType[pNbr->u1NbrIfStatus]));
                    }

                    showOspfv3IfInfo.pNbrsInIf[i4NbrCount].u4NbrIfId =
                        pNbr->u4NbrInterfaceId;
                }
                if ((pNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR) &&
                    (pNbr->u1NbrIfStatus != OSPFV3_DOWN_ON_LINK))
                {
                    OSPFV3_RTR_ID_COPY
                        (&
                         (showOspfv3IfInfo.pNbrsInIf[i4NbrCount].
                          nbrRtrId), pNbr->nbrRtrId);
                    if (pNbr->u1NbrIfStatus < OSPFV3_MAX_NBR_TYPE)
                    {
                        MEMCPY
                            (showOspfv3IfInfo.pNbrsInIf[i4NbrCount].au1NbrType,
                             gau1Os3DbgNbrType[pNbr->u1NbrIfStatus],
                             STRLEN (gau1Os3DbgNbrType[pNbr->u1NbrIfStatus]));
                    }

                    showOspfv3IfInfo.pNbrsInIf[i4NbrCount].u4NbrIfId =
                        pNbr->u4NbrInterfaceId;

                }
                i4NbrCount++;
            }
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    i4PageStatus = CliPrintf (cliHandle, "\r\nInterface Name: %-12s",
                              showOspfv3IfInfo.u1IfName);
    CliPrintf (cliHandle, "Interface Id: %-6d", showOspfv3IfInfo.u4InterfaceId);
    OSPFV3_CLI_AREAID_TO_STR
        (pu1String, OSPFV3_BUFFER_DWFROMPDU (showOspfv3IfInfo.u4AreaId));
    CliPrintf (cliHandle, "Area Id: %s", pu1String);
    i4PageStatus = CliPrintf (cliHandle, " \r\nLocal Address: %-32s ",
                              Ip6PrintAddr (&(showOspfv3IfInfo.ifIp6Addr)));
    OSPFV3_CLI_RTRID_TO_STR (pu1String,
                             OSPFV3_BUFFER_DWFROMPDU (showOspfv3IfInfo.
                                                      u4RouterId));
    i4PageStatus = CliPrintf (cliHandle,
                              "Router Id: %-17s\r\nNetwork Type: %-14s"
                              "Cost: %-14d", pu1String,
                              showOspfv3IfInfo.u1IfType,
                              showOspfv3IfInfo.u4IfMetric);
    CliPrintf (cliHandle, "State: %s", showOspfv3IfInfo.u1IsmState);
    OSPFV3_CLI_RTRID_TO_STR (pu1String,
                             OSPFV3_BUFFER_DWFROMPDU (showOspfv3IfInfo.
                                                      desgRtr));
    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nDesignated Router Id: %-14s", pu1String);

    if (IS_ADDR_UNSPECIFIED (showOspfv3IfInfo.drIp6Addr))
    {
        CliPrintf (cliHandle, "local address: (null)");
    }
    else
    {
        CliPrintf (cliHandle, "local address: %s",
                   Ip6PrintAddr (&(showOspfv3IfInfo.drIp6Addr)));
    }
    OSPFV3_CLI_RTRID_TO_STR (pu1String,
                             OSPFV3_BUFFER_DWFROMPDU (showOspfv3IfInfo.
                                                      backupDesgRtr));
    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nBackup Designated Router Id: %-14s",
                              pu1String);

    if (IS_ADDR_UNSPECIFIED (showOspfv3IfInfo.bdrIp6Addr))
    {
        CliPrintf (cliHandle, "local address: (null)");
    }
    else
    {
        CliPrintf (cliHandle, "local address: %s",
                   Ip6PrintAddr (&(showOspfv3IfInfo.bdrIp6Addr)));
    }

    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nTransmit Delay: %d sec        "
                              "Priority: %-10dIfOptions: 0x%x",
                              showOspfv3IfInfo.u2IfTransDelay,
                              showOspfv3IfInfo.u1RtrPriority,
                              showOspfv3IfInfo.u1Options[OSPFV3_OPT_BYTE_TWO]);
    i4PageStatus =
        CliPrintf (cliHandle,
                   "\r\nTimer intervals configured:"
                   "\r\nHello: %hd, Dead: %hd, Retransmit: %hd, Poll: %u",
                   showOspfv3IfInfo.u2HelloInterval,
                   showOspfv3IfInfo.u2RtrDeadInterval,
                   showOspfv3IfInfo.u2RxmtInterval,
                   showOspfv3IfInfo.u4PollInterval);
    (showOspfv3IfInfo.bDcEndpt == OSIX_TRUE) ? STRCPY (au1String,
                                                       "Enable") :
        STRCPY (au1String, "Disable");
    i4PageStatus =
        CliPrintf (cliHandle, "\r\nDemand Circuit: %-19s", au1String);
    (showOspfv3IfInfo.bDemandNbrProbe == OSIX_TRUE) ? STRCPY (au1String,
                                                              "Enable") :
        STRCPY (au1String, "Disable");
    CliPrintf (cliHandle, "Neighbor Probing: %-10s", au1String);
    (showOspfv3IfInfo.bLinkLsaSuppress == OSIX_TRUE) ? STRCPY (au1String,
                                                               "Enable") :
        STRCPY (au1String, "Disable");
    CliPrintf (cliHandle, "Link Lsa Suppression: %s", au1String);
    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nNbr Probe Retrans Limit: %-10u"
                              "Nbr Probe Interval: %d",
                              showOspfv3IfInfo.u4NbrProbeRxmtLimit,
                              showOspfv3IfInfo.u4NbrProbeInterval);
    if (pInterface->bPassive == OSIX_FALSE)
    {
        i4PageStatus = CliPrintf (cliHandle, "\r\nHello due in %d sec",
                                  showOspfv3IfInfo.u4HelloTime);
    }
    else
    {
        i4PageStatus = CliPrintf (cliHandle,
                                  "\r\nNo Hellos (Passive interface)");
    }

    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nNeighbor Count is: %d",
                              showOspfv3IfInfo.i4NbrCount);
    for (i4NbrCount = OSPFV3_INIT_VAL; i4NbrCount < showOspfv3IfInfo.i4NbrCount;
         i4NbrCount++)
    {
        OSPFV3_CLI_RTRID_TO_STR (pu1String,
                                 OSPFV3_BUFFER_DWFROMPDU
                                 (showOspfv3IfInfo.pNbrsInIf[i4NbrCount].
                                  nbrRtrId));
        i4PageStatus =
            CliPrintf (cliHandle, "\rAdjacent with neighbor: %-10s", pu1String);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

        i4PageStatus = CliPrintf (cliHandle, "\tNbr Type:%-10s",
                                  showOspfv3IfInfo.pNbrsInIf[i4NbrCount].
                                  au1NbrType);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

        i4PageStatus = CliPrintf (cliHandle, "\tNbr's If Id:%d\n",
                                  showOspfv3IfInfo.pNbrsInIf[i4NbrCount].
                                  u4NbrIfId);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

    }
    V3OspfCliAuthInterface (cliHandle, pInterface);

    if (pInterface->u1BfdIfStatus != OSPFV3_BFD_ENABLED)
    {
        CliPrintf (cliHandle, "Bfd: Disabled \r\n");
    }
    else
    {
        CliPrintf (cliHandle, "Bfd: Enabled \r\n");
    }
    CliPrintf (cliHandle, "\r\n");
    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfShowAllInterfaceInCxt

  Description   : Function to display all Interface Info
 
  Input(s)      : cliHandle     -    CLI Handle
                  u4ContextId   -    Context Id
                  pInterface    -    Interface pointer
                                           
  Output(s)     : None
          
  Return Values : CLI_SUCCESS / CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfShowAllInterfaceInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                             tV3OsInterface * pInterface)
{
    INT4                i4RetVal = CLI_SUCCESS;

    if (pInterface != NULL)
    {
        if (u4ContextId == OSPFV3_INVALID_CXT_ID)
        {
            i4RetVal = V3OspfCliPrintOspfInterfaceInfo (pInterface, cliHandle);
        }
        else
        {
            if (pInterface->pArea->pV3OspfCxt->u4ContextId == u4ContextId)
            {
                i4RetVal = V3OspfCliPrintOspfInterfaceInfo
                    (pInterface, cliHandle);
            }
        }
        if (i4RetVal == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*************************************************************************
  Function Name : V3OspfCliShowInterfaceInCxt

  Cli Command   : show ipv6 ospf interface [ <interface-name> ] 
         
  Description   : Displays the ospfv3-related interface information
 
  Input(s)      : cliHandle    - Used by CliPrintf 
                  u4ContextId  - ospf context id
                  u1ShowIfOption - Interface option.
                  u4NeighborId - Interface id.
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS
**************************************************************************/
PRIVATE INT1
V3OspfCliShowInterfaceInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                             UINT1 u1ShowIfOption, UINT4 u4IfIndex)
{
    tV3OsInterface      interface;
    tV3OsInterface     *pInterface = NULL;

    /*Checking the optional arguments in the command */
    switch (u1ShowIfOption)
    {
        case OSPFV3_CLI_SHOW_ALL_INTERFACE:
            pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);

            if (pInterface != NULL)
            {
                CliPrintf (cliHandle, "\r\n%55s",
                           "Ospfv3  Interface  Information");
                CliPrintf (cliHandle, "\r\n%59s",
                           "--------------------------------\r\n");
            }

            do
            {
                if (V3OspfShowAllInterfaceInCxt (cliHandle, u4ContextId,
                                                 pInterface) == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }

            }
            while ((pInterface = (tV3OsInterface *)
                    RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                   (tRBElem *) pInterface, NULL)) != NULL);

            break;
        case OSPFV3_CLI_SHOW_INTERFACE:

            MEMSET (&interface, OSPFV3_ZERO, sizeof (tV3OsInterface));
            interface.u4InterfaceId = u4IfIndex;

            pInterface = RBTreeGet (gV3OsRtr.pIfRBRoot,
                                    (tRBElem *) & interface);

            if ((pInterface != NULL) &&
                ((u4ContextId == OSPFV3_INVALID_CXT_ID) ||
                 (pInterface->pArea->pV3OspfCxt->u4ContextId == u4ContextId)))
            {
                CliPrintf (cliHandle, "\r\n%55s",
                           "Ospfv3  Interface  Information");
                CliPrintf (cliHandle, "\r\n%59s",
                           "--------------------------------\r\n");
                V3OspfCliPrintOspfInterfaceInfo (pInterface, cliHandle);
            }
            break;
        default:
            CliPrintf (cliHandle, "\r\n Invalid options entered");
            break;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayNbr
  
  Description  : This routine prints neighbor table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 pNeighbor - Pointer to structure tV3OsNeighbor

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayNbr (tCliHandle cliHandle, tV3OsNeighbor * pNeighbor)
{
    UINT1              *pu1String = NULL;
    UINT1               au1NbrState[OSPFV3_CLI_MAX_STRING_LEN + 1];
    UINT1               u1NbrPriority = OSPFV3_INIT_VAL;
    UINT1               au1NbrId[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1HelperER[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1NbrBfdState[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];
    UINT4               u4DeadTime = OSPFV3_INIT_VAL;
    tIp6Addr            nbrIpv6Addr;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT4               u4HelperAge = OSPFV3_INIT_VAL;
    UINT4               u4NeighborId = 0;
    UINT4               u4IfDRRouterId = 0;
    UINT4               u4IfBDRRouterId = 0;
    UINT2               u2BufferLen = 0;

    MEMSET (au1HelperER, OSPFV3_INIT_VAL, OSPFV3_CLI_MAX_STRING_LEN);
    MEMSET (au1NbrBfdState, OSPFV3_INIT_VAL, OSPFV3_CLI_MAX_STRING_LEN);
    MEMSET (au1NbrId, OSPFV3_INIT_VAL, OSPFV3_CLI_MAX_STRING_LEN);
    OSPFV3_CLI_RTRID_TO_STR (pu1String, OSPFV3_BUFFER_DWFROMPDU
                             (pNeighbor->nbrRtrId));
    STRNCPY (au1NbrId, pu1String, (OSPFV3_CLI_MAX_STRING_LEN - 1));
    au1NbrId[OSPFV3_CLI_MAX_STRING_LEN - 1] = '\0';

    u1NbrPriority = pNeighbor->u1NbrRtrPriority;
    CfaCliGetIfName (pNeighbor->pInterface->u4InterfaceId, (INT1 *) au1IfName);
    if (pNeighbor->u1NbrBfdState == OSPFV3_BFD_ENABLED)
    {
        STRCPY (au1NbrBfdState, "enabled");
    }
    else
    {
        STRCPY (au1NbrBfdState, "disabled");
    }

    if ((pNeighbor->u1NsmState < OSPFV3_MAX_NBR_STATE) &&
        (pNeighbor->pInterface->u1IsmState < OSPFV3_MAX_IF_STATE))
    {

        if (pNeighbor->u1NbrIfStatus == OSPFV3_NORMAL_NBR)
        {
            STRCPY (au1NbrState, gau1Os3DbgNbrState[pNeighbor->u1NsmState]);
            STRCAT (au1NbrState, "/");
        }
        else if (pNeighbor->u1NbrIfStatus == OSPFV3_ACTIVE_ON_LINK)
        {
            STRCPY (au1NbrState, "ACT_LINK");
            STRCAT (au1NbrState, "/");
        }
        else
        {
            STRCPY (au1NbrState, "SBY_LINK");
            STRCAT (au1NbrState, "/");

        }
        if ((pNeighbor->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
            (pNeighbor->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST))
        {
            if ((pNeighbor->pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                (pNeighbor->pInterface->u1IsmState == OSPFV3_IFS_BACKUP) ||
                (pNeighbor->pInterface->u1IsmState == OSPFV3_IFS_DR_OTHER))
            {
                u4NeighborId = OSPFV3_BUFFER_DWFROMPDU (pNeighbor->nbrRtrId);
                u4IfDRRouterId =
                    OSPFV3_BUFFER_DWFROMPDU (pNeighbor->pInterface->desgRtr);
                u4IfBDRRouterId =
                    OSPFV3_BUFFER_DWFROMPDU (pNeighbor->pInterface->
                                             backupDesgRtr);
                if ((u4NeighborId == IP_ANY_ADDR)
                    && ((pNeighbor->u1NsmState == OSPFV3_NBRS_DOWN)
                        || (pNeighbor->u1NsmState == OSPFV3_NBRS_ATTEMPT)))
                {
                    u2BufferLen =
                        sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                    STRNCAT (au1NbrState, gau1Os3DbgIfState[OSPFV3_IFS_DOWN],
                             u2BufferLen <
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DOWN]) ?
                             u2BufferLen :
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DOWN]));
                }
                else if (u4NeighborId == u4IfDRRouterId)
                {
                    u2BufferLen =
                        sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                    STRNCAT (au1NbrState, gau1Os3DbgIfState[OSPFV3_IFS_DR],
                             u2BufferLen <
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DR]) ?
                             u2BufferLen :
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DR]));
                }
                else if (u4NeighborId == u4IfBDRRouterId)
                {
                    u2BufferLen =
                        sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                    STRNCAT (au1NbrState, gau1Os3DbgIfState[OSPFV3_IFS_BACKUP],
                             u2BufferLen <
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_BACKUP]) ?
                             u2BufferLen :
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_BACKUP]));
                }
                else
                {
                    u2BufferLen =
                        sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                    STRNCAT (au1NbrState,
                             gau1Os3DbgIfState[OSPFV3_IFS_DR_OTHER],
                             u2BufferLen <
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DR_OTHER]) ?
                             u2BufferLen :
                             STRLEN (gau1Os3DbgIfState[OSPFV3_IFS_DR_OTHER]));
                }
            }
            else
            {
                if (!(pNeighbor->pInterface->u1IsmState < OSPFV3_MAX_IF_STATE))
                {
                    return CLI_FAILURE;
                }
                u2BufferLen = sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
                STRNCAT (au1NbrState,
                         gau1Os3DbgIfState[pNeighbor->pInterface->u1IsmState],
                         u2BufferLen <
                         STRLEN (gau1Os3DbgIfState
                                 [pNeighbor->pInterface->
                                  u1IsmState]) ? u2BufferLen :
                         STRLEN (gau1Os3DbgIfState
                                 [pNeighbor->pInterface->u1IsmState]));
            }
        }
        else
        {
            if (!(pNeighbor->pInterface->u1IsmState < OSPFV3_MAX_IF_STATE))
            {
                return CLI_FAILURE;
            }
            u2BufferLen = sizeof (au1NbrState) - STRLEN (au1NbrState) - 1;
            STRNCAT (au1NbrState,
                     gau1Os3DbgIfState[pNeighbor->pInterface->u1IsmState],
                     u2BufferLen <
                     STRLEN (gau1Os3DbgIfState
                             [pNeighbor->pInterface->
                              u1IsmState]) ? u2BufferLen :
                     STRLEN (gau1Os3DbgIfState
                             [pNeighbor->pInterface->u1IsmState]));
        }
    }

    if (TmrGetRemainingTime (gV3OsRtr.timerLstId,
                             &(pNeighbor->inactivityTimer.timerNode),
                             &(u4DeadTime)) == TMR_SUCCESS)
    {
        u4DeadTime = u4DeadTime / OSPFV3_NO_OF_TICKS_PER_SEC;
    }
    else
    {
        u4DeadTime = OSPFV3_INIT_VAL;
    }

    OSPFV3_IP6_ADDR_COPY (nbrIpv6Addr, pNeighbor->nbrIpv6Addr);

    switch (pNeighbor->u1NbrHelperExitReason)
    {
        case OSPFV3_HELPER_NONE:
            STRCPY (au1HelperER, " None");
            break;
        case OSPFV3_HELPER_INPROGRESS:
            STRCPY (au1HelperER, " In Progress");
            break;
        case OSPFV3_HELPER_COMPLETED:
            STRCPY (au1HelperER, " Completed");
            break;
        case OSPFV3_HELPER_GRACE_TIMEDOUT:
            STRCPY (au1HelperER, " Timed Out");
            break;
        case OSPFV3_HELPER_TOP_CHG:
            STRCPY (au1HelperER, " Topology Change");
            break;
    }

    if (pNeighbor->u1NbrHelperStatus == OSPFV3_GR_HELPING)
    {
        if (TmrGetRemainingTime (gV3OsRtr.timerLstId,
                                 (tTmrAppTimer
                                  *) (&(pNeighbor->helperGraceTimer.timerNode)),
                                 &u4HelperAge) == TMR_SUCCESS)
        {
            u4HelperAge = u4HelperAge / OSPFV3_NO_OF_TICKS_PER_SEC;
        }
        else
        {
            u4HelperAge = OSPFV3_INIT_VAL;
        }
        i4PageStatus = CliPrintf (cliHandle,
                                  "%-16s%-8d%-18s%  -13d%-25s%-15s%-12u%-21s%-9s\r\n",
                                  au1NbrId, u1NbrPriority, au1NbrState,
                                  u4DeadTime, Ip6PrintAddr (&nbrIpv6Addr),
                                  "Helping", u4HelperAge, au1HelperER,
                                  au1NbrBfdState);
    }
    else
    {
        i4PageStatus = CliPrintf (cliHandle,
                                  "%-16s%-8d%-18s%  -13d%-25s%-15s%-12u%-21s%-9s\r\n",
                                  au1NbrId, u1NbrPriority, au1NbrState,
                                  u4DeadTime, Ip6PrintAddr (&nbrIpv6Addr),
                                  "Not Helping", OSPFV3_INIT_VAL, au1HelperER,
                                  au1NbrBfdState);
    }

    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowNeighborInCxt

  Cli Command   : show ipv6 ospf neighbor [ <Neighbor-RouterID> ] 
         
  Description   :  Displays ospfv3 neighbors information
 
  Input(s)      : cliHandle    - Used by CliPrintf 
                  u4ContextId  - ospf context id
                  u1ShowNbrOption - Neighbour option.
                  u4NeighborId - Neighbour id.

  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowNeighborInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                            UINT1 u1ShowNbrOption, UINT4 u4NeighborId,
                            UINT4 u4IfIndex)
{
    tTMO_SLL_NODE      *pListNode = NULL;
    tV3OsNeighbor      *pNeighbor = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1NbrId[OSPFV3_RTR_ID_LEN];
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4AdminStat;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;

        if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }
        /*Checking the optional arguments in the command */
        switch (u1ShowNbrOption)
        {
            case OSPFV3_CLI_SHOW_ALL_NBR:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

                CliPrintf (cliHandle,
                           "\r\n%-14s%-9s%-16s%-15s%-26s%-15s%-13s%-20s%-10s\r\n",
                           "ID", "Pri", "State", "DeadTime", "Address",
                           "Helper", "HelperAge", "HelperExitReason", "Bfd");

                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pListNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                     nextSortNbr, pListNode);

                    if (V3OspfCliDisplayNbr (cliHandle, pNeighbor) ==
                        CLI_FAILURE)
                    {
                        break;
                    }
                }
                break;

            case OSPFV3_CLI_SHOW_NBR:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

                OSPFV3_BUFFER_DWTOPDU (au1NbrId, u4NeighborId);

                CliPrintf (cliHandle,
                           "\r\n%-15s%-9s%-16s%-15s%-25s%-15s%-13s%-20s%-10s\r\n",
                           "ID", "Pri", "State", "DeadTime", "Address",
                           "Helper", "HelperAge", "HelperExitReason", "Bfd");

                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pListNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor =
                        OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                             pListNode);

                    if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, au1NbrId) ==
                        OSPFV3_EQUAL)
                    {
                        if (V3OspfCliDisplayNbr (cliHandle, pNeighbor)
                            == CLI_FAILURE)
                        {
                            break;
                        }
                    }
                }
                CliPrintf (cliHandle, "\r\n");
                break;
            case OSPFV3_CLI_SHOW_NBR_INTF:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\nContext Name  : %s", au1OspfCxtName);
                CliPrintf (cliHandle, "\r\n------------\r\n");

                CliPrintf (cliHandle,
                           /*"\r\n%-18s%-9s%-19s%-13s%-26s%-14s%-10s\r\n", */
                           "\r\n%-18s%-6s%-19s%-10s%-24s%-11s%-10s\r\n",
                           "Nbr ID", "Pri", "State", "DeadTime", "Address",
                           "Interface", "BFD");

                CliPrintf (cliHandle,
                           "%-18s%-6s%-19s%-10s%-24s%-11s%-10s\r\n",
                           "----------------", "-----", "-------------",
                           "--------", "----------------------", "---------",
                           "--------");

                /*TMO_SLL_Scan (&(pInterface->nbrsInIf), pListNode, tTMO_SLL_NODE *) */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pListNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor =
                        OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                             pListNode);
                    if (pNeighbor->pInterface->u4InterfaceId == u4IfIndex)
                    {
                        if (u4NeighborId == 0)
                        {
                            if (V3OspfCliDisplayNbr (cliHandle, pNeighbor)
                                == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                        else
                        {
                            OSPFV3_BUFFER_DWTOPDU (au1NbrId, u4NeighborId);
                            if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, au1NbrId)
                                == OSPFV3_EQUAL)
                            {
                                if (V3OspfCliDisplayNbr (cliHandle, pNeighbor)
                                    == CLI_FAILURE)
                                {
                                    break;
                                }
                            }

                        }

                    }
                }
                CliPrintf (cliHandle, "\r\n\n");
                break;
            default:
                CliPrintf (cliHandle, "\r\n Invalid options entered");
                break;
        }
        /* If u4ShowAllCxt is set to OSPF_TRUE then all context
         * must be displayed */
        if (u4ShowAllCxt == OSPFV3_FALSE)
        {
            break;
        }
    }
    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayArea
  
  Description  : This Routine Prints Area table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3AreaInfo - Holds display information

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayArea (tCliHandle cliHandle, tShowOspfv3AreaInfo ospfv3AreaInfo)
{
    UINT1              *pu1String = NULL;
    UINT1               au1AreaType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1AreaSummary[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1TransState[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1TransRole[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3AreaInfo.u4Ospfv3AreaId);
    i4PageStatus = CliPrintf (cliHandle, "\r\nAreaId: %-24s", pu1String);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    switch (ospfv3AreaInfo.i4Ospfv3ImportAsExtern)
    {
        case OSPFV3_NORMAL_AREA:
            STRCPY (au1AreaType, "NORMAL AREA");
            break;
        case OSPFV3_STUB_AREA:
            STRCPY (au1AreaType, "STUB AREA");
            break;
        case OSPFV3_NSSA_AREA:
            STRCPY (au1AreaType, "NSS AREA");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, "Area Type: %s", au1AreaType);

    i4PageStatus =
        CliPrintf (cliHandle, "\r\nSpf Calculation: %d (times) \t",
                   ospfv3AreaInfo.u4Ospfv3SpfRuns);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (cliHandle, "Area Bdr Rtr Count: %d",
               ospfv3AreaInfo.u4Ospfv3AreaBdrRtrCount);

    i4PageStatus = CliPrintf (cliHandle, "\r\nAs Bdr Rtr Count: %-14d",
                              ospfv3AreaInfo.u4Ospfv3AsBdrRtrCount);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    switch (ospfv3AreaInfo.i4Ospfv3AreaSummary)
    {
        case OSPFV3_NO_AREA_SUMMARY:
            STRCPY (au1AreaSummary, "No Summary");
            break;
        case OSPFV3_SEND_AREA_SUMMARY:
            STRCPY (au1AreaSummary, "Send Summary");
            break;
        default:
            break;
    }
    CliPrintf (cliHandle, "Area Summary: %s", au1AreaSummary);

    if (ospfv3AreaInfo.i4Ospfv3ImportAsExtern != OSPFV3_NORMAL_AREA)
    {
        i4PageStatus = CliPrintf (cliHandle, "\r\nStub Metric: 0x%-17x",
                                  ospfv3AreaInfo.i4Ospfv3StubMetric);
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        CliPrintf (cliHandle, "Stub Metric Type: %d",
                   ospfv3AreaInfo.i4Ospfv3StubMetricType);

    }
    if (ospfv3AreaInfo.i4Ospfv3ImportAsExtern == OSPFV3_NSSA_AREA)
    {
        if (ospfv3AreaInfo.i4Ospfv3AreaNssaTranslatorRole
            == OSPFV3_TRNSLTR_ROLE_ALWAYS)
        {
            STRCPY (au1TransRole, "Always");
        }
        else
        {
            STRCPY (au1TransRole, "Candidate");
        }

        i4PageStatus = CliPrintf (cliHandle,
                                  "\r\nTranslator Role: %-15s", au1TransRole);
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (ospfv3AreaInfo.i4Ospfv3AreaNssaTranslatorState ==
            OSPFV3_TRNSLTR_STATE_DISABLED)
        {
            STRCPY (au1TransState, "Disabled");
        }
        else
        {
            STRCPY (au1TransState, "Elected/Enabled");
        }
        i4PageStatus =
            CliPrintf (cliHandle, "Translator State: %s", au1TransState);
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4PageStatus =
            CliPrintf (cliHandle, "\r\nNssa Stability Interval: %d",
                       ospfv3AreaInfo.u4Ospsfv3AreaNssaStablityInterval);
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    i4PageStatus = CliPrintf (cliHandle, "\r\n");

    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowAreaInCxt

  Cli Command   : show ipv6 ospf areas 
         
  Description   :  Display the Area Table
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowAreaInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3AreaInfo showOspfv3AreaInfo;
    UINT4               u4Ospfv3AreaId = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfv3AreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT4               u4RetValOspfv3SpfRuns = OSPFV3_INIT_VAL;
    UINT4               u4RetValOspfv3AreaBdrRtrCount = OSPFV3_INIT_VAL;
    UINT4               u4RetValOspfv3AsBdrRtrCount = OSPFV3_INIT_VAL;
    UINT4               u4StablityInterval = OSPFV3_INIT_VAL;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4RetValOspfv3ImportAsExtern = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfv3AreaSummary = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfv3StubMetric = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfv3StubMetricType = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfv3AreaNssaTranslatorRole = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfv3AreaNssaTranslatorState = OSPFV3_INIT_VAL;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;

    CliPrintf (cliHandle, "\r\nOSPFV3  AREA  CONFIGURATION  INFORMATION \r\n");

    MEMSET (&showOspfv3AreaInfo, 0, sizeof (tShowOspfv3AreaInfo));

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        if (V3UtilSetContext (u4ContextId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFirstIndexOspfv3AreaTable (&u4Ospfv3AreaId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        do
        {
            if (nmhGetOspfv3ImportAsExtern (u4Ospfv3AreaId,
                                            &i4RetValOspfv3ImportAsExtern) ==
                SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           " \r\n Get Failure For ImportAsExtern \r\n ");
                return CLI_FAILURE;
            }

            if (nmhGetOspfv3AreaSpfRuns (u4Ospfv3AreaId, &u4RetValOspfv3SpfRuns)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhGetOspfv3AreaBdrRtrCount (u4Ospfv3AreaId,
                                             &u4RetValOspfv3AreaBdrRtrCount)
                == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           " \r\n Get Failure For AreaBdrRtrCount \r\n ");
                return CLI_FAILURE;
            }

            if (nmhGetOspfv3AreaAsBdrRtrCount (u4Ospfv3AreaId,
                                               &u4RetValOspfv3AsBdrRtrCount)
                == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           " \r\n Get Failure For AsBdrRtrCount \r\n ");
                return CLI_FAILURE;
            }

            if (nmhGetOspfv3AreaSummary
                (u4Ospfv3AreaId, &i4RetValOspfv3AreaSummary) == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           " \r\n Get Failure For AreaSummary \r\n ");
                return CLI_FAILURE;
            }

            if (i4RetValOspfv3ImportAsExtern != OSPFV3_NORMAL_AREA)
            {
                if (nmhGetOspfv3StubMetric (u4Ospfv3AreaId,
                                            &i4RetValOspfv3StubMetric) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (cliHandle,
                               " \r\n Get Failure For StubMetric \r\n ");
                    return CLI_FAILURE;
                }

                if (nmhGetOspfv3AreaStubMetricType (u4Ospfv3AreaId,
                                                    &i4RetValOspfv3StubMetricType)
                    == SNMP_FAILURE)
                {
                    CliPrintf (cliHandle,
                               " \r\n Get Failure For " "StubMetricType \r\n ");
                    return CLI_FAILURE;
                }

                if (nmhGetOspfv3AreaNssaTranslatorRole (u4Ospfv3AreaId,
                                                        &i4RetValOspfv3AreaNssaTranslatorRole)
                    == SNMP_FAILURE)
                {
                    CliPrintf (cliHandle,
                               " \r\n Get Failure For "
                               "NssaTranslatorRole\r\n ");
                    return CLI_FAILURE;
                }

                if (nmhGetOspfv3AreaNssaTranslatorState
                    (u4Ospfv3AreaId,
                     &i4RetValOspfv3AreaNssaTranslatorState) == SNMP_FAILURE)
                {
                    CliPrintf (cliHandle,
                               "\r\n Get Failure For "
                               "NssaTranslatorState\r\n");
                    return CLI_FAILURE;
                }
                if (nmhGetOspfv3AreaNssaTranslatorStabilityInterval
                    (u4Ospfv3AreaId, &u4StablityInterval) == SNMP_FAILURE)
                {
                    CliPrintf (cliHandle,
                               "\r\n Get Failure For "
                               "NssaStabilityInterval\r\n");
                    return CLI_FAILURE;
                }

                showOspfv3AreaInfo.i4Ospfv3StubMetric =
                    i4RetValOspfv3StubMetric;
                showOspfv3AreaInfo.i4Ospfv3StubMetricType =
                    i4RetValOspfv3StubMetricType;
                showOspfv3AreaInfo.i4Ospfv3AreaNssaTranslatorRole =
                    i4RetValOspfv3AreaNssaTranslatorRole;
                showOspfv3AreaInfo.i4Ospfv3AreaNssaTranslatorState =
                    i4RetValOspfv3AreaNssaTranslatorState;
                showOspfv3AreaInfo.u4Ospsfv3AreaNssaStablityInterval =
                    u4StablityInterval;

            }

            showOspfv3AreaInfo.u4Ospfv3AreaId = u4Ospfv3AreaId;
            showOspfv3AreaInfo.i4Ospfv3ImportAsExtern =
                i4RetValOspfv3ImportAsExtern;
            showOspfv3AreaInfo.u4Ospfv3SpfRuns = u4RetValOspfv3SpfRuns;
            showOspfv3AreaInfo.u4Ospfv3AreaBdrRtrCount =
                u4RetValOspfv3AreaBdrRtrCount;
            showOspfv3AreaInfo.u4Ospfv3AsBdrRtrCount =
                u4RetValOspfv3AsBdrRtrCount;
            showOspfv3AreaInfo.i4Ospfv3AreaSummary = i4RetValOspfv3AreaSummary;

            CliPrintf (cliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

            i1PageStatus =
                (INT1) V3OspfCliDisplayArea (cliHandle, showOspfv3AreaInfo);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }

            /* prepare to get the next indices */

            u4PrevOspfv3AreaId = u4Ospfv3AreaId;
        }
        while (nmhGetNextIndexOspfv3AreaTable
               (u4PrevOspfv3AreaId, &u4Ospfv3AreaId) != SNMP_FAILURE);

        V3UtilReSetContext ();
        /* If u4ShowAllCxt is set to OSPF_TRUE then all context
         * must be displayed */
        if (u4ShowAllCxt == OSPFV3_FALSE)
        {
            break;
        }

    }
    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    return i1PageStatus;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayExtSA
  
  Description  : This Routine Prints External Summary Address table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3ExtSAInfo - Holds display information

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayExtSA (tCliHandle cliHandle,
                       tShowOspfv3ExtSAInfo ospfv3ExtSAInfo)
{
    UINT1              *pu1String = NULL;
    UINT1               au1ExtSAEffect[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1ExtSATranslation[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;

    CliPrintf (cliHandle, "%-24s",
               Ip6PrintAddr ((&ospfv3ExtSAInfo.ospfExtSAPrefix)));

    CliPrintf (cliHandle, "%-11d", ospfv3ExtSAInfo.u4OspfExtSAPfxLen);

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3ExtSAInfo.u4OspfExtSAAreaId);
    CliPrintf (cliHandle, "%-12s", pu1String);

    switch (ospfv3ExtSAInfo.i4OspfExtSAEffect)
    {
        case OSPFV3_RAG_ADVERTISE:
            STRCPY (au1ExtSAEffect, "advertise");
            break;
        case OSPFV3_RAG_DO_NOT_ADVERTISE:
            STRCPY (au1ExtSAEffect, "doNotAdvertise");
            break;
        case OSPFV3_RAG_ALLOW_ALL:
            STRCPY (au1ExtSAEffect, "allowAll");
            break;
        case OSPFV3_RAG_DENY_ALL:
            STRCPY (au1ExtSAEffect, "denyAll");
            break;
        default:
            break;
    }
    CliPrintf (cliHandle, "%-16s", au1ExtSAEffect);

    switch (ospfv3ExtSAInfo.i4OspfExtSATranslation)
    {
        case OSPFV3_ENABLED:
            STRCPY (au1ExtSATranslation, "enabled");
            break;
        case OSPFV3_DISABLED:
            STRCPY (au1ExtSATranslation, "disabled");
            break;
        default:
            break;
    }

    i4PageStatus = CliPrintf (cliHandle, "%s\r\n", au1ExtSATranslation);
    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowExtSummaryAddressInCxt

  Cli Command   : show ipv6 ospf { area-range | summary-prefix } 
         
  Description   : Displays summary address information
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowExtSummaryAddressInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3ExtSAInfo showOspfv3ExtSAInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            prevIp6Addr;
    tSNMP_OCTET_STRING_TYPE ospfExtSANet;
    tSNMP_OCTET_STRING_TYPE prevOspfExtSANet;
    UINT4               u4OspfExtSAAreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfExtSAPfxLen = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfExtSAAreaId = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfExtSAPfxLen = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4RetValOspfExtSAEffect = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfExtSATranslation = OSPFV3_INIT_VAL;
    INT4                i4PrefixType = OSPFV3_INET_ADDR_TYPE;
    INT4                i4PfxType = OSPFV3_INIT_VAL;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;

    ospfExtSANet.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    prevOspfExtSANet.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    ospfExtSANet.pu1_OctetList = (UINT1 *) &ip6Addr;
    prevOspfExtSANet.pu1_OctetList = (UINT1 *) &prevIp6Addr;

    MEMSET (&ip6Addr, 0, OSPFV3_IPV6_ADDR_LEN);
    MEMSET (&prevIp6Addr, 0, OSPFV3_IPV6_ADDR_LEN);

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIOspfv3AsExternalAggregationTable ((INT4 *)
                                                                  &u4ContextId,
                                                                  &i4PfxType,
                                                                  &ospfExtSANet,
                                                                  &u4OspfExtSAPfxLen,
                                                                  &u4OspfExtSAAreaId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4OspfPrevCxtId = u4ContextId;
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable
            (u4OspfPrevCxtId, (INT4 *) &u4ContextId, i4PrefixType, &i4PfxType,
             &prevOspfExtSANet, &ospfExtSANet, u4PrevOspfExtSAPfxLen,
             &u4OspfExtSAPfxLen, u4PrevOspfExtSAAreaId,
             &u4OspfExtSAAreaId) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if ((u4ShowAllCxt == OSPFV3_TRUE) || (u4ContextId == u4OspfPrevCxtId))
    {

        CliPrintf (cliHandle,
                   "\r\nOspfv3  External Summary Address"
                   "Configuration Information \r\n");

        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n%-22s%-13s%-12s%-16s%s\r\n",
                   "Prefix", "PfxLength", "AreaId", "Effect",
                   "TranslationState");
    }

    do
    {
        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }
        else if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "%-22s%-13s%-12s%-16s%s\r\n",
                       "Prefix", "PfxLength", "AreaId", "Effect",
                       "TranslationState");
        }

        u4OspfPrevCxtId = u4ContextId;
        MEMCPY (prevOspfExtSANet.pu1_OctetList, ospfExtSANet.pu1_OctetList,
                OSPFV3_IPV6_ADDR_LEN);
        u4PrevOspfExtSAPfxLen = u4OspfExtSAPfxLen;
        u4PrevOspfExtSAAreaId = u4OspfExtSAAreaId;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }

        if (nmhGetFsMIOspfv3AsExternalAggregationEffect
            (u4ContextId, i4PrefixType, &ospfExtSANet, u4OspfExtSAPfxLen,
             u4OspfExtSAAreaId, &i4RetValOspfExtSAEffect) == SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "\r\n Get Failure For Aggregation effect \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIOspfv3AsExternalAggregationTranslation
            (u4ContextId, i4PrefixType, &ospfExtSANet, u4OspfExtSAPfxLen,
             u4OspfExtSAAreaId, &i4RetValOspfExtSATranslation) == SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       " \r\n Get Failure For Aggregation Translation "
                       "state \r\n ");
            return CLI_FAILURE;
        }

        OSPFV3_IP6_ADDR_COPY (showOspfv3ExtSAInfo.ospfExtSAPrefix,
                              *(ospfExtSANet.pu1_OctetList));
        showOspfv3ExtSAInfo.u4OspfExtSAPfxLen = u4OspfExtSAPfxLen;
        showOspfv3ExtSAInfo.u4OspfExtSAAreaId = u4OspfExtSAAreaId;
        showOspfv3ExtSAInfo.i4OspfExtSAEffect = i4RetValOspfExtSAEffect;
        showOspfv3ExtSAInfo.i4OspfExtSATranslation =
            i4RetValOspfExtSATranslation;

        i1PageStatus =
            (INT1) V3OspfCliDisplayExtSA (cliHandle, showOspfv3ExtSAInfo);

        if (i1PageStatus == CLI_FAILURE)
        {
            break;
        }

        /* prepare to get the next indices */

    }
    while (nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable
           (u4OspfPrevCxtId, (INT4 *) &u4ContextId, i4PrefixType, &i4PfxType,
            &prevOspfExtSANet, &ospfExtSANet, u4PrevOspfExtSAPfxLen,
            &u4OspfExtSAPfxLen, u4PrevOspfExtSAAreaId,
            &u4OspfExtSAAreaId) != SNMP_FAILURE);

    return i1PageStatus;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayVI 
  
  Description  : This Routine Prints virtual interface table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3VIInfo - Holds display information

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayVI (tCliHandle cliHandle, tShowOspfv3VIInfo ospfv3VIInfo)
{
    UINT1              *pu1String = NULL;
    UINT1               au1VIState[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4OspfVirtIfState = OSPFV3_INIT_VAL;
    INT4                i4OspfVirtNbrState = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    /* As per OSPFv3 MIB Possible values for interface state of a virtual 
     * link are down (1) and point-to-point (4).
     *  In OSPFv3 implementation, ISM states are:OSPFV3_IFS_DOWN (0) and 
     *  OSPFV3_IFS_PTOP (3). So nmhGetOspfVirtIfState function returns 
     *  pInterface->u1IsmState +1. To check against the #defined constants, 
     *  we need to decrement the returned value by 1 */

    i4OspfVirtIfState = ospfv3VIInfo.i4OspfVIStatus - 1;

    switch (i4OspfVirtIfState)
    {
        case OSPFV3_IFS_DOWN:
            STRCPY (au1VIState, "DOWN");
            break;
        case OSPFV3_IFS_PTOP:
            STRCPY (au1VIState, "PointToPoint");
            break;
        default:
            STRCPY (au1VIState, "Invalid State");
            break;
    }

    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nInterface State: %s,", au1VIState);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4OspfVirtNbrState = ospfv3VIInfo.i4OspfVNbrState - 1;

    switch (i4OspfVirtNbrState)
    {
        case OSPFV3_NBRS_DOWN:
            STRCPY (au1VIState, "DOWN");
            break;
        case OSPFV3_NBRS_ATTEMPT:
            STRCPY (au1VIState, "ATTEMPT");
            break;
        case OSPFV3_NBRS_INIT:
            STRCPY (au1VIState, "INIT");
            break;
        case OSPFV3_NBRS_2WAY:
            STRCPY (au1VIState, "2WAY");
            break;
        case OSPFV3_NBRS_EXSTART:
            STRCPY (au1VIState, "EXCHANGE START");
            break;
        case OSPFV3_NBRS_EXCHANGE:
            STRCPY (au1VIState, "EXCHANGE");
            break;
        case OSPFV3_NBRS_LOADING:
            STRCPY (au1VIState, "LOADING");
            break;
        case OSPFV3_NBRS_FULL:
            STRCPY (au1VIState, "FULL");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, " Neighbor State: %s", au1VIState);

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3VIInfo.u4OspfVIAreaId);
    i4PageStatus = CliPrintf (cliHandle, "\r\nTransit Area: %s,", pu1String);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    OSPFV3_CLI_RTRID_TO_STR (pu1String, ospfv3VIInfo.u4OspfVINbr);
    CliPrintf (cliHandle, "  Virtual Neighbor: %s", pu1String);

    i4PageStatus = CliPrintf (cliHandle,
                              "\r\nIntervals Configured for the "
                              "Virtual Interface: ");
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4PageStatus = CliPrintf (cliHandle, "\r\nHello: %d,",
                              ospfv3VIInfo.i4OspfVIHelloInt);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (cliHandle, " Dead: %d,", ospfv3VIInfo.i4OspfVIDeadInt);

    CliPrintf (cliHandle, " Transit: %d,", ospfv3VIInfo.i4OspfVITransInt);

    CliPrintf (cliHandle, " Retransmit : %d", ospfv3VIInfo.i4OspfVIRetransInt);

    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowVirtualIfInCxt 

  Cli Command   : (a) show ipv6 ospf virtual-links 
         
  Description   : Display Virtual Link Table Information
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowVirtualIfInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3VIInfo   showOspfv3VIInfo;
    UINT4               u4OspfVIAreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfVINbr = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfVIAreaId = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfVINbr = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT4               u4OspfPrevAreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevNbr = OSPFV3_INIT_VAL;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4RetValOspfVITransInt = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfVIRetranInt = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfVIHelloInt = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfVIDeadInt = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfVIState = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfVNbrState = OSPFV3_INIT_VAL;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;
    tV3OsRouterId       nbrId;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pOspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIStdOspfv3VirtIfTable ((INT4 *) &u4ContextId,
                                                      &u4OspfVIAreaId,
                                                      &u4OspfVINbr) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4OspfPrevCxtId = u4ContextId;
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIStdOspfv3VirtIfTable ((INT4) u4OspfPrevCxtId,
                                                     (INT4 *) &u4ContextId,
                                                     u4OspfPrevAreaId,
                                                     &u4OspfVIAreaId,
                                                     u4OspfPrevNbr,
                                                     &u4OspfVINbr) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

    CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

    do
    {
        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }
        else if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);
        }

        u4OspfPrevCxtId = u4ContextId;
        u4PrevOspfVIAreaId = u4OspfVIAreaId;
        u4PrevOspfVINbr = u4OspfVINbr;

        if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        if (nmhGetFsMIStdOspfv3VirtIfTransitDelay ((INT4) u4ContextId,
                                                   u4OspfVIAreaId, u4OspfVINbr,
                                                   &i4RetValOspfVITransInt) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, "\r\n Get Failure For Transit Delay \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3VirtIfRetransInterval ((INT4) u4ContextId,
                                                      u4OspfVIAreaId,
                                                      u4OspfVINbr,
                                                      &i4RetValOspfVIRetranInt)
            == SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "\r\n Get Failure For Retransit Interval \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3VirtIfHelloInterval ((INT4) u4ContextId,
                                                    u4OspfVIAreaId, u4OspfVINbr,
                                                    &i4RetValOspfVIHelloInt) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, "\r\n Get Failure For Hello Interval \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3VirtIfRtrDeadInterval ((INT4) u4ContextId,
                                                      u4OspfVIAreaId,
                                                      u4OspfVINbr,
                                                      &i4RetValOspfVIDeadInt) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, "\r\n Get Failure For Dead Interval \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3VirtIfState ((INT4) u4ContextId,
                                            u4OspfVIAreaId, u4OspfVINbr,
                                            &i4RetValOspfVIState) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "\r\n Get Failure For State of Interface \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3VirtNbrState ((INT4) u4ContextId,
                                             u4OspfVIAreaId, u4OspfVINbr,
                                             &i4RetValOspfVNbrState) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle,
                       "\r\n Get Failure For State of Neighbor \r\n ");
            return CLI_FAILURE;
        }

        showOspfv3VIInfo.u4OspfVIAreaId = u4OspfVIAreaId;
        showOspfv3VIInfo.u4OspfVINbr = u4OspfVINbr;
        showOspfv3VIInfo.i4OspfVIStatus = i4RetValOspfVIState;
        showOspfv3VIInfo.i4OspfVITransInt = i4RetValOspfVITransInt;
        showOspfv3VIInfo.i4OspfVIRetransInt = i4RetValOspfVIRetranInt;
        showOspfv3VIInfo.i4OspfVIHelloInt = i4RetValOspfVIHelloInt;
        showOspfv3VIInfo.i4OspfVIDeadInt = i4RetValOspfVIDeadInt;
        showOspfv3VIInfo.i4OspfVNbrState = i4RetValOspfVNbrState;

        OSPFV3_BUFFER_DWTOPDU (areaId, u4OspfVIAreaId);
        OSPFV3_BUFFER_DWTOPDU (nbrId, u4OspfVINbr);

        pOspfCxt = V3UtilOspfGetCxt (u4ContextId);
        if (pOspfCxt == NULL)
        {
            return CLI_FAILURE;
        }

        pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &(nbrId));

        if (pInterface == NULL)
        {
            OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                        pOspfCxt->u4ContextId, "Interface Not Found \n");
            return SNMP_FAILURE;
        }

        i1PageStatus = (INT1) V3OspfCliDisplayVI (cliHandle, showOspfv3VIInfo);
        V3OspfCliAuthInterface (cliHandle, pInterface);

        if (i1PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while (nmhGetNextIndexFsMIStdOspfv3VirtIfTable ((INT4) u4OspfPrevCxtId,
                                                    (INT4 *) &u4ContextId,
                                                    u4PrevOspfVIAreaId,
                                                    &u4OspfVIAreaId,
                                                    u4PrevOspfVINbr,
                                                    &u4OspfVINbr) !=
           SNMP_FAILURE);

    return i1PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowRoutingTableInCxt

  Cli Command   : show ipv6 ospf route
         
  Description   : Displays routes learned by the ospfv3 process 
 
  Input(s)      : cliHandle   - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None

  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowRoutingTableInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    UINT4               u4ShowAllCxt = FALSE;
    INT4                i4RetVal = 0;
    UINT4               u4RoutePfxLength = 0;
    UINT4               u4CurrentRoutePfxLength = 0;
    INT4                i4RouteIfIndex = OSPFV3_INIT_VAL;
    UINT4               u4CurrentVcId = VCM_INVALID_VC;

    UINT1               au1RouteDest[OSPFV3_MAX_ADDR_LEN];
    UINT1               au1CurrentRouteDest[OSPFV3_MAX_ADDR_LEN];
    UINT1               au1RouteNextHop[OSPFV3_MAX_ADDR_LEN];
    UINT1               au1CurrentRouteNextHop[OSPFV3_MAX_ADDR_LEN];

    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RouteDestType = 0;
    INT4                i4CurrentRouteDestType = 0;

    INT4                i4RouteNextHopType = 0;
    INT4                i4CurrentRouteNextHopType = 0;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    UINT1              *pu1String = NULL;
    UINT1               au1RtType[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4RouteType = 0;
    INT4                i4Cost = 0;
    UINT4               u4RouteAreaId = 0;
    UINT1               u1IfName[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1String[OSPFV3_TWO * OSPFV3_CLI_MAX_STRING_LEN];

    tSNMP_OCTET_STRING_TYPE RouteDest;
    tSNMP_OCTET_STRING_TYPE CurrentRouteDest;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OCTET_STRING_TYPE CurrentRouteNextHop;

    MEMSET (au1RouteDest, ZERO, OSPFV3_MAX_ADDR_LEN);
    MEMSET (au1CurrentRouteDest, ZERO, OSPFV3_MAX_ADDR_LEN);
    MEMSET (au1RouteNextHop, ZERO, OSPFV3_MAX_ADDR_LEN);
    MEMSET (au1CurrentRouteNextHop, ZERO, OSPFV3_MAX_ADDR_LEN);
    MEMSET (u1IfName, ZERO, OSPFV3_CLI_MAX_STRING_LEN);

    RouteDest.pu1_OctetList = au1RouteDest;
    CurrentRouteDest.pu1_OctetList = au1CurrentRouteDest;
    RouteNextHop.pu1_OctetList = au1RouteNextHop;
    CurrentRouteNextHop.pu1_OctetList = au1CurrentRouteNextHop;

    RouteDest.i4_Length = 0;
    CurrentRouteDest.i4_Length = 0;
    RouteNextHop.i4_Length = 0;
    CurrentRouteNextHop.i4_Length = 0;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        i4RetVal =
            nmhGetFirstIndexFsMIOspfv3RoutingTable ((INT4 *) &u4ContextId,
                                                    &i4RouteDestType,
                                                    &RouteDest,
                                                    &u4RoutePfxLength,
                                                    &i4RouteNextHopType,
                                                    &RouteNextHop);
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        if (V3UtilSetContext (u4ContextId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4RetVal =
            nmhGetFirstIndexFutOspfv3RoutingTable (&i4RouteDestType,
                                                   &RouteDest,
                                                   &u4RoutePfxLength,
                                                   &i4RouteNextHopType,
                                                   &RouteNextHop);

        V3UtilReSetContext ();
    }

    CliPrintf (cliHandle, "OSPFV3 Process Routing Table  \r\n");

    while (i4RetVal == SNMP_SUCCESS)
    {
        if (u4CurrentVcId != u4ContextId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n%-40s     %-40s       %s\r\n",
                       "Dest/Prefix-Length", "NextHop/IfIndex",
                       "Cost/Rt.Type/Area");
        }

        nmhGetFsMIOspfv3RouteAreaId (u4ContextId,
                                     i4RouteDestType,
                                     &RouteDest,
                                     u4RoutePfxLength,
                                     i4RouteNextHopType,
                                     &RouteNextHop, &u4RouteAreaId);

        OSPFV3_CLI_AREAID_TO_STR (pu1String, u4RouteAreaId);

        nmhGetFsMIOspfv3RouteType (u4ContextId,
                                   i4RouteDestType,
                                   &RouteDest,
                                   u4RoutePfxLength,
                                   i4RouteNextHopType,
                                   &RouteNextHop, &i4RouteType);

        if (i4RouteType == OSPFV3_TYPE_2_EXT)
        {
            nmhGetFsMIOspfv3RouteType2Cost ((INT4) u4ContextId,
                                            i4RouteDestType,
                                            &RouteDest,
                                            u4RoutePfxLength,
                                            i4RouteNextHopType,
                                            &RouteNextHop, &i4Cost);
        }
        else
        {
            nmhGetFsMIOspfv3RouteCost ((INT4) u4ContextId,
                                       i4RouteDestType,
                                       &RouteDest,
                                       u4RoutePfxLength,
                                       i4RouteNextHopType,
                                       &RouteNextHop, &i4Cost);
        }

        nmhGetFsMIOspfv3RouteInterfaceIndex ((INT4) u4ContextId,
                                             i4RouteDestType,
                                             &RouteDest,
                                             u4RoutePfxLength,
                                             i4RouteNextHopType,
                                             &RouteNextHop, &i4RouteIfIndex);

        V3OspfGetInterfaceNameFromIndex (i4RouteIfIndex, u1IfName);

        MEMSET (au1String, ZERO, (OSPFV3_TWO * OSPFV3_CLI_MAX_STRING_LEN));

        SNPRINTF ((CHR1 *) au1String, (OSPFV3_TWO * OSPFV3_CLI_MAX_STRING_LEN),
                  "%s/%d", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                         RouteDest.pu1_OctetList),
                  u4RoutePfxLength);
        u4PagingStatus = CliPrintf (cliHandle, "%-45s", au1String);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        MEMSET (au1String, ZERO, (OSPFV3_TWO * OSPFV3_CLI_MAX_STRING_LEN));

        SNPRINTF ((CHR1 *) au1String, (OSPFV3_TWO * OSPFV3_CLI_MAX_STRING_LEN),
                  "%s/ %s", Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                          RouteNextHop.pu1_OctetList),
                  u1IfName);
        u4PagingStatus = CliPrintf (cliHandle, "%-44s", au1String);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        u4PagingStatus = CliPrintf (cliHandle, "   %d", i4Cost);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        switch (i4RouteType)
        {
            case OSPFV3_INTRA_AREA:
                STRCPY (au1RtType, "intraArea");
                break;
            case OSPFV3_INTER_AREA:
                STRCPY (au1RtType, "interArea");
                break;
            case OSPFV3_TYPE_1_EXT:
                STRCPY (au1RtType, "type1Ext");
                break;
            case OSPFV3_TYPE_2_EXT:
                STRCPY (au1RtType, "type2Ext");
                break;
            default:
                break;
        }
        CliPrintf (cliHandle, "%s", "/");

        u4PagingStatus = CliPrintf (cliHandle, "%s", au1RtType);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        CliPrintf (cliHandle, "%s", "/");

        u4PagingStatus = CliPrintf (cliHandle, "%s\r\n", pu1String);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        u4CurrentVcId = u4ContextId;
        i4CurrentRouteDestType = i4RouteDestType;
        MEMCPY (CurrentRouteDest.pu1_OctetList, RouteDest.pu1_OctetList,
                RouteDest.i4_Length);
        u4CurrentRoutePfxLength = u4RoutePfxLength;
        i4CurrentRouteNextHopType = i4RouteNextHopType;
        MEMCPY (CurrentRouteNextHop.pu1_OctetList, RouteNextHop.pu1_OctetList,
                RouteNextHop.i4_Length);

        i4RetVal =
            nmhGetNextIndexFsMIOspfv3RoutingTable ((INT4) u4CurrentVcId,
                                                   (INT4 *) &u4ContextId,
                                                   i4CurrentRouteDestType,
                                                   &i4RouteDestType,
                                                   &CurrentRouteDest,
                                                   &RouteDest,
                                                   u4CurrentRoutePfxLength,
                                                   &u4RoutePfxLength,
                                                   i4CurrentRouteNextHopType,
                                                   &i4RouteNextHopType,
                                                   &CurrentRouteNextHop,
                                                   &RouteNextHop);

        if ((u4CurrentVcId != u4ContextId) && (u4ShowAllCxt == FALSE))
        {
            /*Finished displaying entries belonging to one VRF */
            break;
        }

    }

    return (INT1) u4PagingStatus;

}

/*******************************************************************************
  Function Name: V3OspfCliPrintRxmtLSAInfo 
  
  Description  : This Routine will dump all Link State Advertise  
                 ment's details  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 pNeighbor - Pointer to structure tV3OsNeighbor

  Output(s)    : None             
                 
  Return Values: CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliPrintRxmtLSAInfo (tCliHandle cliHandle, tV3OsNeighbor * pNeighbor)
{

    UINT1              *pu1NbrId = NULL;
    UINT1              *pu1LsId = NULL;
    UINT1              *pu1RtrId = NULL;
    UINT4               u4RxmtQueueLen = OSPFV3_INIT_VAL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsRxmtNode      *pRxmtNode = NULL;
    tShowLsaRxmtInfo    lsaRxmtInfo;
    INT4                i4PageStatus = CLI_SUCCESS;

    OSPFV3_RTR_ID_COPY (&(lsaRxmtInfo.nbrRtrId), pNeighbor->nbrRtrId);
    OSPFV3_IP6_ADDR_COPY (lsaRxmtInfo.nbrIpv6Addr, pNeighbor->nbrIpv6Addr);

    u4RxmtQueueLen = pNeighbor->lsaRxmtDesc.u4LsaRxmtCount;

    if (u4RxmtQueueLen != OSPFV3_INIT_VAL)
    {

        OSPFV3_CLI_RTRID_TO_STR (pu1NbrId, OSPFV3_BUFFER_DWFROMPDU
                                 (pNeighbor->nbrRtrId));
        i4PageStatus = CliPrintf (cliHandle,
                                  "\r\nNeighborId: %-12s Nbr Address: %s",
                                  pu1NbrId,
                                  Ip6PrintAddr (&(lsaRxmtInfo.nbrIpv6Addr)));
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        i4PageStatus = CliPrintf (cliHandle,
                                  "\r\n%-6s    %-11s%-11s%-14s%-8s%s\r\n",
                                  "Type", "LsId", "AdvRtr", "SeqNo", "Age",
                                  "Checksum");
        if (i4PageStatus == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        TMO_SLL_Scan (&(pNeighbor->lsaRxmtLst), pRxmtNode, tV3OsRxmtNode *)
        {
            pLsaInfo = pRxmtNode->pLsaInfo;

            OSPFV3_LINK_STATE_ID_COPY (&(lsaRxmtInfo.lsHeader.linkStateId),
                                       pLsaInfo->lsaId.linkStateId);
            OSPFV3_CLI_RTRID_TO_STR (pu1LsId,
                                     OSPFV3_BUFFER_DWFROMPDU (lsaRxmtInfo.
                                                              lsHeader.
                                                              linkStateId));

            OSPFV3_RTR_ID_COPY (&(lsaRxmtInfo.lsHeader.advRtrId),
                                pLsaInfo->lsaId.advRtrId);
            OSPFV3_CLI_RTRID_TO_STR (pu1RtrId,
                                     OSPFV3_BUFFER_DWFROMPDU
                                     (lsaRxmtInfo.lsHeader.advRtrId));

            lsaRxmtInfo.lsHeader.u2LsaType = pLsaInfo->lsaId.u2LsaType;
            lsaRxmtInfo.lsHeader.u2LsaAge = pLsaInfo->u2LsaAge;
            lsaRxmtInfo.lsHeader.lsaSeqNum = pLsaInfo->lsaSeqNum;
            lsaRxmtInfo.lsHeader.u2LsaChksum = pLsaInfo->u2LsaChksum;

            i4PageStatus = CliPrintf (cliHandle,
                                      "0x%04x    %-11s%-11s0x%-12x%-8d0x%x \r\n",
                                      lsaRxmtInfo.lsHeader.u2LsaType,
                                      pu1LsId, pu1RtrId,
                                      OSIX_NTOHL (lsaRxmtInfo.lsHeader.
                                                  lsaSeqNum),
                                      OSIX_NTOHS (lsaRxmtInfo.lsHeader.
                                                  u2LsaAge),
                                      OSIX_NTOHS (lsaRxmtInfo.lsHeader.
                                                  u2LsaChksum));

            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }
        }
    }
    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowRxmtListInCxt 

  Cli Command   : (a) show ipv6 ospf { retrans-list } [ <Neighbor-RouterID> ]
         
  Description   : This Routine will display all Link State Advertise
                   ments waiting to be retransmitted
 
  Input(s)      : cliHandle   - Used by CliPrintf 
                  u4ContextId - Context Id
                  u1ShowLsaOption - LSA Option
                  u4NbrId     - Neighbour Id
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowRxmtListInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                            UINT1 u1ShowLsaOption, UINT4 u4IfIndex,
                            UINT4 u4NbrId)
{
    tV3OsNeighbor      *pNeighbor = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4AdminStat;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        /*Checking the optional arguments in the command */
        switch (u1ShowLsaOption)
        {
            case OSPFV3_CLI_SHOW_TRANS_LSA_ALL:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

                /*print LSA information for all neighbor structure */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                     nextSortNbr, pLstNode);

                    if (V3OspfCliPrintRxmtLSAInfo (cliHandle,
                                                   pNeighbor) == CLI_FAILURE)
                    {
                        break;
                    }
                }
                break;
            case OSPFV3_CLI_SHOW_TRANS_LSA_NBR:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                                     pLstNode);
                    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);
                    if (V3UtilRtrIdComp (nbrId, pNeighbor->nbrRtrId) ==
                        OSPFV3_EQUAL)
                    {
                        V3OspfCliPrintRxmtLSAInfo (cliHandle, pNeighbor);
                        return CLI_SUCCESS;
                    }
                }
                return CLI_FAILURE;
            case OSPFV3_CLI_SHOW_TRANS_LSA_NBR_INTF:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\nContext Name : %s \r\n",
                           au1OspfCxtName);
                CliPrintf (cliHandle, "--------------\r\n");

                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                                     pLstNode);
                    if (pNeighbor->pInterface->u4InterfaceId == u4IfIndex)
                    {
                        if (u4NbrId == 0)
                        {
                            if (V3OspfCliPrintRxmtLSAInfo (cliHandle, pNeighbor)
                                == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                        else
                        {
                            OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);
                            if (V3UtilRtrIdComp (nbrId, pNeighbor->nbrRtrId) ==
                                OSPFV3_EQUAL)
                            {
                                if (V3OspfCliPrintRxmtLSAInfo
                                    (cliHandle, pNeighbor) == CLI_FAILURE)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                break;
            default:
                CliPrintf (cliHandle, "\r\nInvalid Options entered");
                break;
        }
        /* If u4ShowAllCxt is set to OSPF_TRUE then all context
         * must be displayed */
        if (u4ShowAllCxt == OSPFV3_FALSE)
        {
            break;
        }
    }
    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    return CLI_SUCCESS;
}

/*******************************************************************************
   Function Name:  V3OspfCliPrintLsaReqInfo
   
   Description  : This Routine will dump all Link State Advertisement's details  
   
   Input(s)     : cliHandle - Used by CliPrintf 
                  pNeighbor - Pointer to structure tV3OsNeighbor

   Output(s)    : None             
                  
   Return Values: CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
PRIVATE INT4
V3OspfCliPrintLsaReqInfo (tCliHandle cliHandle, tV3OsNeighbor * pNeighbor)
{

    UINT1              *pu1NbrId = NULL;
    tV3OsLsaReqNode    *pLsaReqNode = NULL;
    tShowLsaReqInfo     lsaReqInfo;
    UINT1              *pu1LsId = NULL;
    UINT1              *pu1RtrId = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT1               au1IfName[CFA_CLI_MAX_IF_NAME_LEN];

    MEMSET (&lsaReqInfo, 0, sizeof (tShowLsaReqInfo));
    OSPFV3_RTR_ID_COPY (&(lsaReqInfo.nbrRtrId), pNeighbor->nbrRtrId);
    OSPFV3_IP6_ADDR_COPY (lsaReqInfo.nbrIpv6Addr, pNeighbor->nbrIpv6Addr);

    CfaCliGetIfName (pNeighbor->pInterface->u4InterfaceId, (INT1 *) au1IfName);
    OSPFV3_CLI_RTRID_TO_STR (pu1NbrId, OSPFV3_BUFFER_DWFROMPDU
                             (pNeighbor->nbrRtrId));
    i4PageStatus =
        CliPrintf (cliHandle,
                   "\r\nNeighborId : %s; Address : %s; Interface: %s\r\n",
                   pu1NbrId, Ip6PrintAddr (&(lsaReqInfo.nbrIpv6Addr)),
                   au1IfName);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4PageStatus = CliPrintf (cliHandle, "\r\n%s    %s        %s"
                              "            %s        %s    %s",
                              "Type", "LSID", "ADVRTR", "SeqNo", "Age",
                              "Checksum");
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    TMO_SLL_Scan (&(pNeighbor->lsaReqDesc.lsaReqLst), pLsaReqNode,
                  tV3OsLsaReqNode *)
    {
        V3UtilExtractLsHeaderFromLbuf
            ((UINT1 *) &(pLsaReqNode->lsHeader),
             (tV3OsLsHeader *) & (lsaReqInfo.lsHeader));

        OSPFV3_CLI_RTRID_TO_STR (pu1LsId,
                                 OSPFV3_BUFFER_DWFROMPDU (lsaReqInfo.
                                                          lsHeader.
                                                          linkStateId));
        OSPFV3_CLI_RTRID_TO_STR (pu1RtrId,
                                 OSPFV3_BUFFER_DWFROMPDU (lsaReqInfo.
                                                          lsHeader.advRtrId));

        i4PageStatus = CliPrintf (cliHandle,
                                  "\r\n0x%x    %s    %s    0x%x    %d    0x%x\r\n",
                                  lsaReqInfo.lsHeader.u2LsaType,
                                  pu1LsId, pu1RtrId,
                                  OSIX_NTOHL (lsaReqInfo.lsHeader.
                                              lsaSeqNum),
                                  OSIX_NTOHS (lsaReqInfo.lsHeader.u2LsaAge),
                                  OSIX_NTOHS (lsaReqInfo.lsHeader.u2LsaChksum));
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowRequestListInCxt 

  Cli Command   : (a) show ipv6 ospf { request-list } [ <num_str>] 
         
  Description   : This Routine will display all Link State Advertise
                   ments requested by the router
 
  Input(s)      : cliHandle   - Used by CliPrintf 
                  u4ContextId - Context Id
                  u1ShowLsaOption - LSA Option
                  u4NbrId     - Neighbour Id

  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowRequestListInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                               UINT1 u1ShowLsaOption, UINT4 u4IfIndex,
                               UINT4 u4NbrId)
{
    tV3OsNeighbor      *pNeighbor = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    UINT1              *pu1RtrId = NULL;
    INT4                i4AdminStat;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;

        if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }
        /*Checking the optional arguments in the command */
        switch (u1ShowLsaOption)
        {
            case OSPFV3_CLI_SHOW_REQ_LSA_ALL:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

                OSPFV3_CLI_RTRID_TO_STR (pu1RtrId,
                                         OSPFV3_BUFFER_DWFROMPDU (pV3OspfCxt->
                                                                  rtrId));
                CliPrintf (cliHandle, "\r\nOspfv3 Router with Id: %s\r\n",
                           pu1RtrId);
                /*print LSA information for all neighbor structure */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                     nextSortNbr, pLstNode);

                    if (V3OspfCliPrintLsaReqInfo (cliHandle, pNeighbor) ==
                        CLI_FAILURE)
                    {
                        break;
                    }
                }
                break;

            case OSPFV3_CLI_SHOW_REQ_LSA_NBR:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

                OSPFV3_CLI_RTRID_TO_STR (pu1RtrId,
                                         OSPFV3_BUFFER_DWFROMPDU (pV3OspfCxt->
                                                                  rtrId));
                CliPrintf (cliHandle, "\r\n Ospfv3 Router with Id: %s\r\n",
                           pu1RtrId);

                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                     nextSortNbr, pLstNode);

                    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);

                    if (V3UtilRtrIdComp (nbrId, pNeighbor->nbrRtrId) ==
                        OSPFV3_EQUAL)
                    {
                        if (V3OspfCliPrintLsaReqInfo (cliHandle, pNeighbor) ==
                            CLI_FAILURE)
                        {
                            break;
                        }
                        return CLI_SUCCESS;
                    }
                }

                return CLI_FAILURE;
            case OSPFV3_CLI_SHOW_REQ_LSA_NBR_INT:

                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "\r\nContext Name : %s \r\n",
                           au1OspfCxtName);
                CliPrintf (cliHandle, "--------------\r\n");

                OSPFV3_CLI_RTRID_TO_STR (pu1RtrId,
                                         OSPFV3_BUFFER_DWFROMPDU (pV3OspfCxt->
                                                                  rtrId));
                CliPrintf (cliHandle, "\r\n Ospfv3 Router with Id: %s\r\n",
                           pu1RtrId);
                CliPrintf (cliHandle, "----------------------\r\n");

                /*find all the neighbor structure for a given neighbor ID */
                TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                     nextSortNbr, pLstNode);
                    if (pNeighbor->pInterface->u4InterfaceId == u4IfIndex)
                    {
                        if (u4NbrId == 0)
                        {
                            if (V3OspfCliPrintLsaReqInfo (cliHandle, pNeighbor)
                                == CLI_FAILURE)
                            {
                                break;
                            }
                        }
                        else
                        {

                            OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);
                            if (V3UtilRtrIdComp (nbrId, pNeighbor->nbrRtrId) ==
                                OSPFV3_EQUAL)
                            {
                                if (V3OspfCliPrintLsaReqInfo
                                    (cliHandle, pNeighbor) == CLI_FAILURE)
                                {
                                    break;
                                }
                            }

                        }
                    }

                }

                break;
            default:
                CliPrintf (cliHandle, "\r\nInvalid Options entered\r\n");
                break;
        }
        /* If u4ShowAllCxt is set to OSPF_TRUE then all context
         * must be displayed */
        if (u4ShowAllCxt == OSPFV3_FALSE)
        {
            break;
        }
    }
    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    return CLI_SUCCESS;
}

/*************************************************************************
  Function Name : V3OspfCliShowOspfInfoInCxt

  Cli Command   : show ipv6 ospf 
         
  Description   : Displays general information about ospfv3 routing process
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values: CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowOspfInfoInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tV3OsArea          *pArea = NULL;
    tShowOspfv3Info     showOspfv3Info;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1String[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1              *pu1String = NULL;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4AreasCount = OSPFV3_INIT_VAL;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;
    INT4                i4InterfaceCount = 0;
    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;
        i4AreasCount = OSPFV3_INIT_VAL;

        if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }
        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        OSPFV3_RTR_ID_COPY (showOspfv3Info.u4RouterId, pV3OspfCxt->rtrId);

        MEMSET (showOspfv3Info.u1AbrType, 0, sizeof (showOspfv3Info.u1AbrType));

        if (pV3OspfCxt->u1ABRType < OSPFV3_MAX_AREA_TYPE)
        {
            MEMCPY (showOspfv3Info.u1AbrType,
                    gau1Os3DbgAbrType[pV3OspfCxt->u1ABRType],
                    STRLEN (gau1Os3DbgAbrType[pV3OspfCxt->u1ABRType]));
        }

        showOspfv3Info.u4SpfInterval = pV3OspfCxt->u4SpfInterval;

        showOspfv3Info.u4SpfHoldTimeInterval =
            pV3OspfCxt->u4SpfHoldTimeInterval;

        showOspfv3Info.u4ExitOverflowInterval =
            pV3OspfCxt->u4ExitOverflowInterval;

        showOspfv3Info.u4RefBw = pV3OspfCxt->u4RefBw;

        showOspfv3Info.i4ExtLsdbLimit = pV3OspfCxt->i4ExtLsdbLimit;

        showOspfv3Info.u4TraceValue = pV3OspfCxt->u4TraceValue;

        showOspfv3Info.u4AsScopeLsaCount = pV3OspfCxt->u4AsScopeLsaCount;

        showOspfv3Info.u4AsScopeLsaChksumSum =
            pV3OspfCxt->u4AsScopeLsaChksumSum;

        showOspfv3Info.bDemandExtension = pV3OspfCxt->bDemandExtension;

        showOspfv3Info.bDefaultPassiveInterface =
            pV3OspfCxt->bDefaultPassiveInterface;

        showOspfv3Info.bNssaAsbrDefRtTrans = pV3OspfCxt->bNssaAsbrDefRtTrans;

        showOspfv3Info.bAreaBdrRtr = pV3OspfCxt->bAreaBdrRtr;

        showOspfv3Info.bAsBdrRtr = pV3OspfCxt->bAsBdrRtr;

        showOspfv3Info.u4AreasCount = pV3OspfCxt->areasLst.u4_Count;

        showOspfv3Info.pAreaInfo = gaOspfv3AreaInfo;

        if (showOspfv3Info.u4AreasCount != OSPFV3_INIT_VAL)
        {
            MEMSET (showOspfv3Info.pAreaInfo, 0,
                    showOspfv3Info.u4AreasCount * sizeof (tOspfv3AreaInfo));
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                i4InterfaceCount = TMO_SLL_Count (&(pArea->ifsInArea));
                if (i4InterfaceCount != 0)
                {
                    i4AreasCount++;
                }
                else
                {
                    continue;
                }

                showOspfv3Info.pAreaInfo[i4AreasCount].i4IfCount =
                    TMO_SLL_Count (&(pArea->ifsInArea));
                OSPFV3_AREA_ID_COPY (&
                                     (showOspfv3Info.pAreaInfo[i4AreasCount].
                                      u4AreaId), pArea->areaId);
                showOspfv3Info.pAreaInfo[i4AreasCount].u4AreaScopeLsaCount =
                    pArea->u4AreaScopeLsaCount;
                showOspfv3Info.pAreaInfo[i4AreasCount].u4AreaScopeLsaChksumSum =
                    pArea->u4AreaScopeLsaChksumSum;
                showOspfv3Info.pAreaInfo[i4AreasCount].u4DcBitResetLsaCount =
                    pArea->u4DcBitResetLsaCount +
                    pArea->u4DcBitResetSelfOrgLsaCount;
                showOspfv3Info.pAreaInfo[i4AreasCount].u4IndicationLsaCount =
                    pArea->u4IndicationLsaCount;
                showOspfv3Info.pAreaInfo[i4AreasCount].i4SpfCount =
                    pArea->u4SpfRuns;
            }
            showOspfv3Info.u4AreasCount = i4AreasCount;
        }
        else
        {
            return CLI_FAILURE;
        }

        CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

        OSPFV3_CLI_RTRID_TO_STR (pu1String,
                                 OSPFV3_BUFFER_DWFROMPDU (showOspfv3Info.
                                                          u4RouterId));
        CliPrintf (cliHandle, "\r\n Router Id: %-20s", pu1String);

        CliPrintf (cliHandle, "ABR Type:  %s", showOspfv3Info.u1AbrType);

        CliPrintf (cliHandle, "\r\n SPF schedule delay: %d secs     "
                   "Hold time between two SPFs: %d secs",
                   showOspfv3Info.u4SpfInterval,
                   showOspfv3Info.u4SpfHoldTimeInterval);
        CliPrintf (cliHandle, "\r\n Exit Overflow Interval: %-11u"
                   "Ref BW: %-14u Ext Lsdb Limit: %d",
                   showOspfv3Info.u4ExitOverflowInterval,
                   showOspfv3Info.u4RefBw, showOspfv3Info.i4ExtLsdbLimit);
        CliPrintf (cliHandle, "\r\n Trace Value: 0x%08x        "
                   "As Scope Lsa: %-8d Checksum Sum: 0x%x",
                   showOspfv3Info.u4TraceValue,
                   showOspfv3Info.u4AsScopeLsaCount,
                   showOspfv3Info.u4AsScopeLsaChksumSum);
        (showOspfv3Info.bDemandExtension == OSIX_TRUE) ?
            STRCPY (au1String, "Enable") : STRCPY (au1String, "Disable");
        CliPrintf (cliHandle, "\r\n Demand Circuit: %-15s", au1String);

        (showOspfv3Info.bDefaultPassiveInterface == OSIX_TRUE) ?
            STRCPY (au1String, "Enable") : STRCPY (au1String, "Disable");
        CliPrintf (cliHandle, "    Passive Interface: %s", au1String);

        (showOspfv3Info.bNssaAsbrDefRtTrans == OSIX_TRUE) ?
            STRCPY (au1String, "Enable") : STRCPY (au1String, "Disable");
        CliPrintf (cliHandle,
                   "\r\n Nssa Asbr Default Route Translation: %s", au1String);

        if (showOspfv3Info.bAreaBdrRtr == OSIX_TRUE)
        {
            CliPrintf (cliHandle, " \r\n It is an Area Border Router");
        }

        if (showOspfv3Info.bAsBdrRtr == OSIX_TRUE)
        {
            CliPrintf (cliHandle, "\r\n Autonomous System Boundary Router");
        }

        CliPrintf (cliHandle, " \r\n Number of Areas in this router  %d ",
                   showOspfv3Info.u4AreasCount);
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            CliPrintf (cliHandle,
                       " \r\n Route calculation staggering is enabled ");
        }
        else
        {
            CliPrintf (cliHandle,
                       " \r\n Route calculation staggering is disabled ");
        }
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            CliPrintf (cliHandle,
                       " \r\n Route calculation staggering interval is %d seconds ",
                       pV3OspfCxt->u4RTStaggeringInterval);
        }

        if (pV3OspfCxt->u1RestartSupport != OSPFV3_RESTART_NONE)
        {
            CliPrintf (cliHandle, "\r\n IETF Non-Stop Forwarding enabled");
            if (pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_BOTH)
            {
                CliPrintf (cliHandle, " (Planned & Unplanned)");
            }
            else
            {
                CliPrintf (cliHandle, " (Planned Only)");
            }
            CliPrintf (cliHandle, "\r\n    restart-interval limit: %d sec",
                       pV3OspfCxt->u4GracePeriod);
            CliPrintf (cliHandle, "\r\n    restart exit reason: ");
            switch (pV3OspfCxt->u1RestartExitReason)
            {
                case OSPFV3_RESTART_INPROGRESS:
                    CliPrintf (cliHandle, "in progress");
                    break;
                case OSPFV3_RESTART_COMPLETED:
                    CliPrintf (cliHandle, "completed");
                    break;
                case OSPFV3_RESTART_TIMEDOUT:
                    CliPrintf (cliHandle, "timed out");
                    break;
                case OSPFV3_RESTART_TOP_CHG:
                    CliPrintf (cliHandle, "topology change");
                    break;
                default:
                    CliPrintf (cliHandle, "none");
            }
        }
        else
        {
            CliPrintf (cliHandle, "\r\n IETF Non-Stop Forwarding disabled");
        }
        if (pV3OspfCxt->u1HelperSupport != OSPFV3_GRACE_NO_HELPER)
        {
            CliPrintf (cliHandle, "\r\n IETF NSF Helper support enabled for");
            if (pV3OspfCxt->u1HelperSupport & OSPFV3_GRACE_UNKNOWN)
            {
                CliPrintf (cliHandle, "\r\n\t Unknown");
            }
            if (pV3OspfCxt->u1HelperSupport & OSPFV3_GRACE_SW_RESTART)
            {
                CliPrintf (cliHandle, "\r\n\t S/W Restart");
            }
            if (pV3OspfCxt->u1HelperSupport & OSPFV3_GRACE_SW_RELOADUPGRADE)
            {
                CliPrintf (cliHandle, "\r\n\t S/W Upgrade");
            }
            if (pV3OspfCxt->u1HelperSupport & OSPFV3_GRACE_SW_REDUNDANT)
            {
                CliPrintf (cliHandle, "\r\n\t Switch to Redundant");
            }
            if (pV3OspfCxt->u4HelperGrTimeLimit != OSPFV3_INIT_VAL)
            {
                CliPrintf (cliHandle,
                           "\r\n IETF NSF Helper Grace-interval limit: %d sec",
                           pV3OspfCxt->u4HelperGrTimeLimit);
            }
            CliPrintf (cliHandle, "\r\n IETF NSF Helper Strict Lsa Check:");
            if (pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED)
            {
                CliPrintf (cliHandle, " Enabled");
            }
            else
            {
                CliPrintf (cliHandle, " Disabled");
            }
        }
        else
        {
            CliPrintf (cliHandle, "\r\n IETF NSF helper support disabled");
        }

        if (pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_ENABLED)
        {
            CliPrintf (cliHandle, "\r\n BFD: Enabled");
        }
        else
        {
            CliPrintf (cliHandle, "\r\n BFD: Disabled");
        }

        for (i4AreasCount = OSPFV3_ONE;
             i4AreasCount <= (INT4) showOspfv3Info.u4AreasCount; i4AreasCount++)
        {
            OSPFV3_CLI_AREAID_TO_STR (pu1String, OSPFV3_BUFFER_DWFROMPDU
                                      (showOspfv3Info.pAreaInfo[i4AreasCount].
                                       u4AreaId));
            i1PageStatus = (INT1)
                CliPrintf (cliHandle, " \r\n            Area      %s",
                           pu1String);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }

            i1PageStatus = (INT1) CliPrintf (cliHandle,
                                             " \r\n    Number of interfaces in this area "
                                             "is  %d",
                                             showOspfv3Info.
                                             pAreaInfo[i4AreasCount].i4IfCount);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }

            CliPrintf (cliHandle,
                       " \r\n    Number of Area Scope Lsa: %-6d",
                       showOspfv3Info.pAreaInfo[i4AreasCount].
                       u4AreaScopeLsaCount);

            i1PageStatus = (INT1) CliPrintf (cliHandle, " Checksum Sum: 0x%x",
                                             showOspfv3Info.
                                             pAreaInfo[i4AreasCount].
                                             u4AreaScopeLsaChksumSum);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }

            CliPrintf (cliHandle, " \r\n    Number of DcBitReset Lsa's: %-6d",
                       showOspfv3Info.
                       pAreaInfo[i4AreasCount].u4DcBitResetLsaCount);

            i1PageStatus =
                (INT1) CliPrintf (cliHandle, "Number of Indication Lsa: %-6d",
                                  showOspfv3Info.pAreaInfo[i4AreasCount].
                                  u4IndicationLsaCount);

            i1PageStatus = (INT1) CliPrintf (cliHandle,
                                             " SPF algorithm executed:  "
                                             "%d times\r\n",
                                             showOspfv3Info.
                                             pAreaInfo[i4AreasCount].
                                             i4SpfCount);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }
        }
        /* If u4ShowAllCxt is set to OSPF_TRUE then all context
         * must be displayed */
        if (u4ShowAllCxt == OSPFV3_FALSE)
        {
            break;
        }
    }
    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    CliPrintf (cliHandle, "\r\n");

    return i1PageStatus;
}

/*************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayRouterLsa                            
                                                                        
  DESCRIPTION      : This function displays the Router Lsa contents     
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                 pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE
                                                                        
*************************************************************************/
PRIVATE INT4
V3OspfCliDisplayRouterLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa,
                           UINT2 u2LsaPktLen)
{
    UINT1               u1TmpData = OSPFV3_INIT_VAL;
    UINT4               u4TmpData = OSPFV3_INIT_VAL;
    UINT2               u2Metric = OSPFV3_INIT_VAL;
    UINT4               u4IfId = OSPFV3_INIT_VAL;
    UINT4               u4RtrId = OSPFV3_INIT_VAL;
    UINT2               u2LnkCnt = OSPFV3_INIT_VAL;
    UINT2               u2LinkLen = OSPFV3_INIT_VAL;
    UINT1              *pu1String = NULL;
    UINT2               u2NoOfLinks = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);

    if (OSPFV3_IS_V_BIT_SET (u1TmpData))
    {
        i4PageStatus = CliPrintf (cliHandle,
                                  "Router is End of Virtual Link\r\n");
    }
    if (OSPFV3_IS_E_BIT_SET (u1TmpData))
    {
        i4PageStatus =
            CliPrintf (cliHandle, "Router is an AS Boundary Router\r\n");
    }
    if (OSPFV3_IS_B_BIT_SET (u1TmpData))
    {
        i4PageStatus =
            CliPrintf (cliHandle, "Router is an Area Border Router\r\n");
    }

    /*  # links */
    u2NoOfLinks = (UINT2) ((u2LsaPktLen - OSPFV3_ROUTER_LSA_HDR_LEN) /
                           OSPFV3_RTR_LSA_LINK_LEN);
    CliPrintf (cliHandle, "Number of Links: %-11hd", u2NoOfLinks);

    u4TmpData = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "Options: 0x%02x\r\n", u4TmpData);

    if (i4PageStatus != CLI_FAILURE)
    {
        for (u2LnkCnt = OSPFV3_INIT_VAL, u2LinkLen = OSPFV3_INIT_VAL;
             ((u2LnkCnt < u2NoOfLinks) && (u2LinkLen < u2LsaPktLen));
             u2LnkCnt++)
        {
            /* Check for malformed Router link tuples */
            if (u2LsaPktLen < (u2LinkLen + OSPFV3_RTR_LSA_LINK_LEN))
            {
                break;
            }

            /* Link Type */
            u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);

            if (u1TmpData == OSPFV3_PTOP_LINK)
            {
                i4PageStatus = CliPrintf (cliHandle,
                                          "Link connected to: another router "
                                          "(point-to-point)\r\n");
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }
            }
            else if (u1TmpData == OSPFV3_TRANSIT_LINK)
            {
                i4PageStatus = CliPrintf (cliHandle,
                                          "Link connected to : transit network\r\n");
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }

            }
            else if (u1TmpData == OSPFV3_VIRTUAL_LINK)
            {
                i4PageStatus =
                    CliPrintf (cliHandle, "Link is Virtual Link\r\n");
                if (i4PageStatus == CLI_FAILURE)
                {
                    break;
                }

            }

            pu1DisplayLsa++;

            u2Metric = OSPFV3_LGET2BYTE (pu1DisplayLsa);
            CliPrintf (cliHandle, "Metric: 0x%-18hx", u2Metric);

            u4IfId = OSPFV3_LGET4BYTE (pu1DisplayLsa);
            i4PageStatus =
                CliPrintf (cliHandle, "Interface Id: %-d\r\n", u4IfId);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            u4IfId = OSPFV3_LGET4BYTE (pu1DisplayLsa);
            CliPrintf (cliHandle, "Nbr Interface Id: %-10d", u4IfId);

            u4RtrId = OSPFV3_LGET4BYTE (pu1DisplayLsa);
            OSPFV3_CLI_RTRID_TO_STR (pu1String, u4RtrId);
            i4PageStatus = CliPrintf (cliHandle,
                                      "Nbr Router Id: %s\r\n", pu1String);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            /* Update the Bytes read so far */
            u2LinkLen += OSPFV3_RTR_LSA_LINK_LEN;

        }
    }
    return i4PageStatus;
}

/***********************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayNetworkLsa                           
                                                                        
  DESCRIPTION      : This function displays the Router Lsa contents     
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                  pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE                                               
                                                                        
***********************************************************************/
PRIVATE INT4
V3OspfCliDisplayNetworkLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa,
                            UINT2 u2LsaPktLen)
{
    UINT4               u4TmpData = OSPFV3_INIT_VAL;
    UINT4               u4Options = OSPFV3_INIT_VAL;
    UINT4               u4RtrId = OSPFV3_INIT_VAL;
    UINT1              *pu1String = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;

    u4TmpData = OSPFV3_NETWORK_LSA_HDR_LEN;
    pu1DisplayLsa++;

    u4Options = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "Options: 0x%02hx\r\n", u4Options);

    if (i4PageStatus != CLI_FAILURE)
    {
        while (u4TmpData < u2LsaPktLen)
        {
            /* Check for malformed Router link tuples */
            if (u2LsaPktLen < (u4TmpData + OSPFV3_RTR_ID_LEN))
            {
                break;
            }

            u4RtrId = OSPFV3_LGET4BYTE (pu1DisplayLsa);

            /* Attached Router */
            OSPFV3_CLI_RTRID_TO_STR (pu1String, u4RtrId);
            i4PageStatus =
                CliPrintf (cliHandle, "Attached Router: %s\r\n", pu1String);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            u4TmpData += OSPFV3_RTR_ID_LEN;
        }
    }
    return i4PageStatus;
}

/*******************************************************************************
                                                                       
  FUNCTION NAME    : V3OspfCliDisInterAreaPrefixLsa                 
                                                                       
  DESCRIPTION      : This function displays the Inter Area  Prefix Lsa contents     
                                                                       
  INPUT(s)         : cliHandle - Used by CliPrintf 
                 pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                       
  RETURNS          : CLI_SUCCESS or CLI_FAILURE                                               
*******************************************************************************/
PRIVATE INT4
V3OspfCliDisInterAreaPrefixLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa)
{
    UINT1               u1TmpData = OSPFV3_INIT_VAL;
    UINT1               u1PfxLen = OSPFV3_INIT_VAL;
    UINT4               u4Metric = OSPFV3_INIT_VAL;
    tIp6Addr            ip6Addr;
    INT4                i4PageStatus = CLI_SUCCESS;

    /* point to Metric field */
    pu1DisplayLsa++;

    u4Metric = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Metric: 0x%hx\r\n", u4Metric);

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Prefix Length: %-14u", u1TmpData);

    u1PfxLen = u1TmpData;
    u1PfxLen = (UINT1) (u1PfxLen / OSPFV3_BYTE_SIZE);
    if ((u1TmpData % OSPFV3_BYTE_SIZE) != OSPFV3_INIT_VAL)
    {
        u1PfxLen++;
    }

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Prefix Options: 0x%02x\r\n", u1TmpData);

    if (u1PfxLen != OSPFV3_INIT_VAL)
    {
        MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
        pu1DisplayLsa += OSPFV3_TWO_BYTES;
        MEMCPY (&ip6Addr, pu1DisplayLsa, u1PfxLen);
        i4PageStatus = CliPrintf (cliHandle, "Prefix: %s\r\n",
                                  Ip6PrintAddr (&ip6Addr));
    }
    else
    {
        i4PageStatus = CliPrintf (cliHandle, "Default Route\r\n");
    }

    return i4PageStatus;
}

/*******************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayInterAreaRtrLsa                
                                                                        
  DESCRIPTION      : This function displays the Intra Area Router  Lsa contents     
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                     pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE                                               
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayInterAreaRtrLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa)
{
    UINT4               u4TmpData = OSPFV3_INIT_VAL;
    UINT4               u4Metric = OSPFV3_INIT_VAL;
    UINT1              *pu1String = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;

    /* point to Option field */
    pu1DisplayLsa++;

    u4TmpData = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    /* point to Metric field */
    pu1DisplayLsa += OSPFV3_ONE_BYTE;
    u4Metric = OSPFV3_LGET3BYTE (pu1DisplayLsa);

    CliPrintf (cliHandle, "Metric: 0x%-18x", u4Metric);
    i4PageStatus = CliPrintf (cliHandle, "Options: 0x%02x\r\n", u4TmpData);

    OSPFV3_CLI_RTRID_TO_STR (pu1String,
                             OSPFV3_BUFFER_DWFROMPDU (pu1DisplayLsa));
    i4PageStatus = CliPrintf (cliHandle,
                              "Destination Router Id: %s\r\n", pu1String);

    return i4PageStatus;
}

/*******************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayExternalLsa                        
                                                                        
  DESCRIPTION      : This function dispplays the As-External Lsa contents           
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                     pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 

******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayExternalLsa (tCliHandle cliHandle,
                             UINT1 *pu1DisplayLsa, UINT2 u2LsaPktLen)
{
    UINT1               u1TmpData = OSPFV3_INIT_VAL;
    UINT2               u2TmpData = OSPFV3_INIT_VAL;
    UINT4               u4Metric = OSPFV3_INIT_VAL;
    UINT1               u1PfxLen = OSPFV3_INIT_VAL;
    UINT1               u1FieldBits = OSPFV3_INIT_VAL;
    UINT4               u4ExtRouteTag = OSPFV3_INIT_VAL;
    tIp6Addr            ip6Addr;
    tIp6Addr            ip6FwdAddr;
    UINT1              *pTemp = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;

    pTemp = pu1DisplayLsa;
    u1FieldBits = OSPFV3_LGET1BYTE (pu1DisplayLsa);

    MEMSET (&ip6FwdAddr, 0, sizeof (tIp6Addr));

    if (u1FieldBits & OSPFV3_EXT_E_BIT_MASK)
    {
        CliPrintf (cliHandle, "Metric Type: %-15s", "Ext Type2");
    }
    else
    {
        CliPrintf (cliHandle, "Metric Type: %-15s", "Ext Type1");
    }

    u4Metric = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "Metric: 0x%hx\r\n", u4Metric);

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Prefix Length: %-14u", u1TmpData);

    u1PfxLen = u1TmpData;
    u1PfxLen = (UINT1) (u1PfxLen / OSPFV3_BYTE_SIZE);
    if ((u1TmpData % OSPFV3_BYTE_SIZE) != OSPFV3_INIT_VAL)
    {
        u1PfxLen++;
    }

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle,
                              "Prefix Options: 0x%02x\r\n", u1TmpData);

    u2TmpData = OSPFV3_LGET2BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle,
                              "Referenced LS Type: %hd\r\n", u2TmpData);

    if (u1PfxLen != OSPFV3_INIT_VAL)
    {
        MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
        MEMCPY (&ip6Addr, pu1DisplayLsa, u1PfxLen);
        pu1DisplayLsa += u1PfxLen;
        i4PageStatus = CliPrintf (cliHandle, "Address Prefix: %s\r\n",
                                  Ip6PrintAddr (&ip6Addr));
    }

    /* Extracting forwarding address */
    /* If F-Bit is set, then forwarding address field is present 
     * with non zero value */
    if (u1FieldBits & OSPFV3_EXT_F_BIT_MASK)
    {
        if (pTemp + u2LsaPktLen < pu1DisplayLsa + OSPFV3_IPV6_ADDR_LEN)
        {
            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "F-Bit is set, but no forwarding "
                            "address is included in external LSA\n");
            return CLI_FAILURE;
        }

        OSPFV3_LGETSTR (pu1DisplayLsa, &ip6FwdAddr, OSPFV3_IPV6_ADDR_LEN);
    }

    i4PageStatus = CliPrintf (cliHandle, "Fw Addres Prefix: %s\r\n",
                              Ip6PrintAddr (&ip6FwdAddr));

    /* Extracting route tag information */
    /* If T-Bit is set, then external route tag filed is present */
    if ((u1FieldBits) & (OSPFV3_EXT_T_BIT_MASK))
    {
        if (pTemp + u2LsaPktLen < pu1DisplayLsa + OSPFV3_RTR_ID_LEN)
        {
            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "T-Bit is set, but no route tag "
                            "information is included in external LSA\n");
            return CLI_FAILURE;
        }
        u4ExtRouteTag = OSPFV3_LGET4BYTE (pu1DisplayLsa);
    }
    i4PageStatus = CliPrintf (cliHandle, "External Route Tag: %-9d\r\n\r\n",
                              u4ExtRouteTag);

    return i4PageStatus;
}

/*******************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayNssaLsa                        
                                                                        
  DESCRIPTION      : This function dispplays the As-Nssa Lsa contents           
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                   pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                        
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayNssaLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa,
                         UINT2 u2LsaPktLen)
{
    UINT1               u1FieldBits = OSPFV3_INIT_VAL;
    UINT1               u1PrefixLength = OSPFV3_INIT_VAL;
    UINT1               u1PrefLen = OSPFV3_INIT_VAL;
    UINT1               u1PrefixOpt = OSPFV3_INIT_VAL;
    UINT2               u2TmpData = OSPFV3_INIT_VAL;
    UINT4               u4Metric = OSPFV3_INIT_VAL;
    UINT1              *pTemp = NULL;
    tIp6Addr            addrPrefix;
    tIp6Addr            fwdAddr;
    UINT4               u4ExtRouteTag = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    u1FieldBits = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    MEMSET (&addrPrefix, 0, sizeof (tIp6Addr));

    /* Extracting the metric information */
    u4Metric = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "Metric: 0x%x\r\n", u4Metric);

    /* If E Bit is set then the metric specified is Type 2 else Type 1 */
    if (u1FieldBits & OSPFV3_EXT_E_BIT_MASK)
    {
        CliPrintf (cliHandle, "Metric Type: %-15s", "Ext Type2");
    }
    else
    {
        CliPrintf (cliHandle, "Metric Type: %-15s", "Ext Type1");
    }

    /* Extracting Prefix information */
    u1PrefixLength = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Prefix Length: %-14u", u1PrefixLength);

    u1PrefixOpt = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    i4PageStatus =
        CliPrintf (cliHandle, "Prefix Options: 0x%02x\r\n", u1PrefixOpt);

    u2TmpData = OSPFV3_LGET2BYTE (pu1DisplayLsa);
    i4PageStatus =
        CliPrintf (cliHandle, "Referenced LS Type: %hd\r\n", u2TmpData);

    u1PrefLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLength));
    OSPFV3_LGETSTR (pu1DisplayLsa, &addrPrefix, u1PrefLen);
    i4PageStatus = CliPrintf (cliHandle, "Address Prefix: %s\r\n",
                              Ip6PrintAddr (&addrPrefix));

    /* Extracting forwarding address */
    /* If F-Bit is set, then forwarding address field is present 
     * with non zero value */
    if (u1FieldBits & OSPFV3_EXT_F_BIT_MASK)
    {
        if (pTemp + u2LsaPktLen < pu1DisplayLsa + OSPFV3_IPV6_ADDR_LEN)
        {
            OSPFV3_GBL_TRC (OSPFV3_NSSA_TRC,
                            "F-Bit is set, but no forwarding "
                            "address is included in external LSA\n");

        }
        else
        {
            OSPFV3_LGETSTR (pu1DisplayLsa, &fwdAddr, OSPFV3_IPV6_ADDR_LEN);
            i4PageStatus = CliPrintf (cliHandle, "Fw Addres Prefix: %s\r\n",
                                      Ip6PrintAddr (&fwdAddr));
        }
    }

    /* Extracting route tag information */
    /* If T-Bit is set, then external route tag filed is present */
    if ((u1FieldBits) & (OSPFV3_EXT_T_BIT_MASK))
    {
        if (pTemp + u2LsaPktLen < pu1DisplayLsa + OSPFV3_RTR_ID_LEN)
        {
            OSPFV3_GBL_TRC (OSPFV3_NSSA_TRC,
                            "T-Bit is set, but no route tag "
                            "information is included in external LSA\n");
        }
        else
        {
            u4ExtRouteTag = OSPFV3_LGET4BYTE (pu1DisplayLsa);
            i4PageStatus =
                CliPrintf (cliHandle, "External Route Tag: %-9d\r\n\r\n",
                           u4ExtRouteTag);
        }
    }

    return i4PageStatus;
}

/***********************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisplayLinkLsa                        
                                                                        
  DESCRIPTION      : This function displays the Link Lsa contents           
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                 pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                        
*************************************************************************/
PRIVATE INT4
V3OspfCliDisplayLinkLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa)
{
    UINT1               u1TmpData = OSPFV3_INIT_VAL;
    UINT1               u1PfxLen = OSPFV3_INIT_VAL;
    UINT4               u4TmpData = OSPFV3_INIT_VAL;
    tIp6Addr            ip6Addr;
    UINT4               u4Index = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "Router Priority: %-11u", u1TmpData);

    /* point to Options field */
    u4TmpData = OSPFV3_LGET3BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "Options: 0x%02hx\r\n", u4TmpData);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    i4PageStatus =
        CliPrintf (cliHandle, "Prefix: %s\r\n",
                   Ip6PrintAddr ((tIp6Addr *) (VOID *) pu1DisplayLsa));
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pu1DisplayLsa += OSPFV3_IPV6_ADDR_LEN;

    u4TmpData = OSPFV3_LGET4BYTE (pu1DisplayLsa);
    i4PageStatus = CliPrintf (cliHandle, "#Prefixes: %hu\r\n", u4TmpData);
    if (i4PageStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    for (u4Index = OSPFV3_INIT_VAL; u4Index < u4TmpData; u4Index++)
    {
        u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
        u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
        i4PageStatus =
            CliPrintf (cliHandle, "Prefix Length (Bytes): %-5u", u1PfxLen);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

        u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
        i4PageStatus =
            CliPrintf (cliHandle, "Prefix Options: 0x%02x\r\n", u1TmpData);
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

        MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
        pu1DisplayLsa += OSPFV3_TWO_BYTES;
        MEMCPY (&ip6Addr, pu1DisplayLsa, u1PfxLen);

        i4PageStatus = CliPrintf (cliHandle, "Prefix: %s\r\n",
                                  Ip6PrintAddr (&ip6Addr));
        if (i4PageStatus == CLI_FAILURE)
        {
            break;
        }

        pu1DisplayLsa += u1PfxLen;

    }
    return i4PageStatus;
}

/*******************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfCliDisIntraAreaPrefixLsa                      
                                                                        
  DESCRIPTION      : This function displays the Intra Area Prefix Lsa contents           
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                 pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                        
******************************************************************************/
PRIVATE INT4
V3OspfCliDisIntraAreaPrefixLsa (tCliHandle cliHandle, UINT1 *pu1DisplayLsa)
{
    UINT1               u1TmpData = OSPFV3_INIT_VAL;
    UINT2               u2TmpData = OSPFV3_INIT_VAL;
    tIp6Addr            ip6Addr;
    UINT2               u2NoOfPfxes = OSPFV3_INIT_VAL;
    UINT1               u1PfxLen = OSPFV3_INIT_VAL;
    UINT4               u4TmpData = OSPFV3_INIT_VAL;
    UINT1              *pu1String = NULL;
    UINT4               u4Index = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    u2NoOfPfxes = OSPFV3_LGET2BYTE (pu1DisplayLsa);
    CliPrintf (cliHandle, "#Prefixes: %-17hu", u2NoOfPfxes);

    u2TmpData = OSPFV3_LGET2BYTE (pu1DisplayLsa);
    if (u2TmpData == OSPFV3_REF_ROUTER_LSA)
    {
        i4PageStatus = CliPrintf (cliHandle,
                                  "Referenced LS Type: Router Lsa\r\n");
    }
    else
    {
        i4PageStatus = CliPrintf (cliHandle,
                                  "Referenced LS Type: Network Lsa\r\n");
    }

    u4TmpData = OSPFV3_LGET4BYTE (pu1DisplayLsa);
    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4TmpData);
    CliPrintf (cliHandle, "Ref Link State Id: %-10s", pu1String);

    u4TmpData = OSPFV3_LGET4BYTE (pu1DisplayLsa);
    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4TmpData);
    i4PageStatus = CliPrintf (cliHandle, "Ref Adv Router: %s\r\n", pu1String);

    if (i4PageStatus != CLI_FAILURE)
    {
        for (u4Index = OSPFV3_INIT_VAL; u4Index < u2NoOfPfxes; u4Index++)
        {
            u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
            u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
            CliPrintf (cliHandle, "Prefix Length (Bytes): %-6u", u1PfxLen);

            u1TmpData = OSPFV3_LGET1BYTE (pu1DisplayLsa);
            i4PageStatus =
                CliPrintf (cliHandle, "Prefix Options: 0x%02x\r\n", u1TmpData);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            u2TmpData = OSPFV3_LGET2BYTE (pu1DisplayLsa);
            i4PageStatus =
                CliPrintf (cliHandle, "Metric: 0x%hx\r\n", u2TmpData);
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            MEMCPY (&ip6Addr, pu1DisplayLsa, u1PfxLen);

            i4PageStatus = CliPrintf (cliHandle, "Prefix: %s\r\n",
                                      Ip6PrintAddr (&ip6Addr));
            if (i4PageStatus == CLI_FAILURE)
            {
                break;
            }

            pu1DisplayLsa += u1PfxLen;

        }
    }
    return i4PageStatus;
}

/******************************************************************************
                                                                        
  FUNCTION NAME    : V3OspfPrintAdvertisement                           
                                                                        
  DESCRIPTION      : This function prints the link state advertisement  
                                                                        
  INPUT(s)         : cliHandle - Used by CliPrintf 
                        pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                        
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                        
*******************************************************************************/

PRIVATE INT4
V3OspfPrintAdvertisement (tCliHandle cliHandle, UINT1 *pu1Data, UINT2 u2LsaLen)
{
    UINT2               u2Length = OSPFV3_INIT_VAL;
    UINT4               u4Index = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    while (u2Length < u2LsaLen)
    {
        for (u4Index = OSPFV3_INIT_VAL; (u4Index + 3) <= OSPFV3_DISPLAY_STR_LEN;
             u4Index += 3)
        {
            i4PageStatus = CliPrintf (cliHandle, "%02x ", *pu1Data);
            if (i4PageStatus == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            pu1Data++;
            u2Length++;
            if (u2Length == u2LsaLen)
            {
                break;
            }
        }
    }

    i4PageStatus = CliPrintf (cliHandle, "\r\n");
    return i4PageStatus;
}

/*******************************************************************************
                                                                       
  FUNCTION NAME    : V3OspfCliDisplayLsa                        
                                                                       
  DESCRIPTION      : This function prints the link state advertisement  
                                                                       
  INPUT(s)         : cliHandle - Used by CliPrintf 
                  pLsaInfo - Pointer to tV3OsLsaInfo Structure
                 u2BitMask - indicates type of Lsa and Displaying 
                 information

  OUTPUT(s)        : NONE             
                                                                       
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                       
*******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayLsa (tCliHandle cliHandle, tV3OsLsaInfo * pLsaInfo,
                     UINT2 u2BitMask)
{
    UINT2               u2LsaType = OSPFV3_INIT_VAL;
    UINT1               au1LsaType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1              *pu1String = NULL;
    UINT1              *pShowLsaInfo = NULL;
    UINT1               au1DispLsa[OSPFV3_MAX_LSA_SIZE];
    UINT1              *pu1DispLsa = NULL;
    UINT2               u2LsaAge = OSPFV3_INIT_VAL;
    UINT4               u4SeqNum = OSPFV3_INIT_VAL;
    UINT2               u2LsaChksum = OSPFV3_INIT_VAL;
    UINT2               u2LsaLen = OSPFV3_INIT_VAL;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    INT4                i4PageStatus = CLI_SUCCESS;

    pu1DispLsa = au1DispLsa;

    MEMCPY (pu1DispLsa, pLsaInfo->pLsa, pLsaInfo->u2LsaLen);
    pShowLsaInfo = pu1DispLsa;

    u2LsaLen = pLsaInfo->u2LsaLen;

    if ((u2BitMask & OSPFV3_CLI_DB_DETAIL) != OSPFV3_INIT_VAL)
    {
        u2LsaAge = OSPFV3_LGET2BYTE (pShowLsaInfo);
        OSPFV3_GET_LSA_AGE (pLsaInfo, &u2LsaAge);
        i4PageStatus = CliPrintf (cliHandle, "\r\nAge: %-4hd%-19s",
                                  u2LsaAge, "Seconds");

        u2LsaType = OSPFV3_LGET2BYTE (pShowLsaInfo);
        switch (u2LsaType)
        {
            case OSPFV3_ROUTER_LSA:
                STRCPY (au1LsaType, "Router Lsa");
                break;
            case OSPFV3_NETWORK_LSA:
                STRCPY (au1LsaType, "Network Lsa");
                break;
            case OSPFV3_INTER_AREA_PREFIX_LSA:
                STRCPY (au1LsaType, "Inter Area Prefix Lsa");
                break;
            case OSPFV3_INTER_AREA_ROUTER_LSA:
                STRCPY (au1LsaType, "Inter Area Router Lsa");
                break;
            case OSPFV3_NSSA_LSA:
                STRCPY (au1LsaType, "Type-7 Lsa");
                break;
            case OSPFV3_AS_EXT_LSA:
                STRCPY (au1LsaType, "External Lsa");
                break;
            case OSPFV3_LINK_LSA:
                STRCPY (au1LsaType, "Link Lsa");
                break;
            case OSPFV3_INTRA_AREA_PREFIX_LSA:
                STRCPY (au1LsaType, "Intra Area Prefix Lsa");
                break;
            case OSPFV3_GRACE_LSA:
                STRCPY (au1LsaType, "Grace Lsa");
                break;
            default:
                break;
        }

        i4PageStatus = CliPrintf (cliHandle, "LS Type: %s\r\n", au1LsaType);

        OSPFV3_LGETSTR (pShowLsaInfo, linkStateId, sizeof (tV3OsLinkStateId));
        OSPFV3_CLI_LINKSTATEID_TO_STR (pu1String,
                                       OSPFV3_BUFFER_DWFROMPDU (linkStateId));
        CliPrintf (cliHandle, "Link State Id: %-13s", pu1String);

        OSPFV3_LGETSTR (pShowLsaInfo, advRtrId, sizeof (tV3OsRouterId));
        OSPFV3_CLI_RTRID_TO_STR (pu1String, OSPFV3_BUFFER_DWFROMPDU (advRtrId));
        i4PageStatus = CliPrintf (cliHandle, "Adv Rtr Id: %s\r\n", pu1String);

        u4SeqNum = OSPFV3_LGET4BYTE (pShowLsaInfo);
        CliPrintf (cliHandle, "Sequence: 0x%-16x", u4SeqNum);

        u2LsaChksum = OSPFV3_LGET2BYTE (pShowLsaInfo);
        CliPrintf (cliHandle, "Checksum: 0x%04x", u2LsaChksum);

        u2LsaLen = OSPFV3_LGET2BYTE (pShowLsaInfo);
        i4PageStatus = CliPrintf (cliHandle, "    Length: %d\r\n", u2LsaLen);

        /* Adjust the packet length with bytes read */
        if (u2LsaLen >= OSPFV3_LS_HEADER_SIZE)
        {
            u2LsaLen -= OSPFV3_LS_HEADER_SIZE;
        }
        else
        {
            u2LsaLen = OSPFV3_INIT_VAL;
        }

        /* parse the LSA contents */

        if (i4PageStatus != CLI_FAILURE)
        {
            if ((u2LsaType == OSPFV3_ROUTER_LSA) &&
                (u2LsaLen >= OSPFV3_ROUTER_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayRouterLsa (cliHandle, pShowLsaInfo,
                                               u2LsaLen);
            }
            else if ((u2LsaType == OSPFV3_NETWORK_LSA) &&
                     (u2LsaLen >= OSPFV3_NETWORK_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayNetworkLsa (cliHandle,
                                                pShowLsaInfo, u2LsaLen);
            }
            else if ((u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA) &&
                     (u2LsaLen >= OSPFV3_INTER_AREA_PFX_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisInterAreaPrefixLsa (cliHandle, pShowLsaInfo);
            }
            else if ((u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) &&
                     (u2LsaLen >= OSPFV3_INTER_AREA_RTR_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayInterAreaRtrLsa (cliHandle, pShowLsaInfo);
            }
            else if ((u2LsaType == OSPFV3_AS_EXT_LSA) &&
                     (u2LsaLen >= OSPFV3_AS_EXTERN_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayExternalLsa (cliHandle,
                                                 pShowLsaInfo, u2LsaLen);
            }
            else if ((u2LsaType == OSPFV3_NSSA_LSA) &&
                     (u2LsaLen >= OSPFV3_NSSA_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayNssaLsa (cliHandle, pShowLsaInfo, u2LsaLen);

            }
            else if ((u2LsaType == OSPFV3_LINK_LSA) &&
                     (u2LsaLen >= OSPFV3_LINK_LSA_HDR_LEN))
            {
                i4PageStatus =
                    V3OspfCliDisplayLinkLsa (cliHandle, pShowLsaInfo);
            }
            else if ((u2LsaType == OSPFV3_INTRA_AREA_PREFIX_LSA) &&
                     (u2LsaLen >= OSPFV3_INTRA_AREA_PFX_LSA_HDR_LEN))
            {
                i4PageStatus = V3OspfCliDisIntraAreaPrefixLsa (cliHandle,
                                                               pShowLsaInfo);
            }
        }
    }
    else
    {
        i4PageStatus =
            V3OspfPrintAdvertisement (cliHandle, pShowLsaInfo, u2LsaLen);
    }

    return i4PageStatus;
}

/*******************************************************************************
                                                                       
  FUNCTION NAME    : V3OspfCliDisplayLsaSummary                        
                                                                       
  DESCRIPTION      : This function prints the link state advertisement header 
                                                                       
  INPUT(s)         : cliHandle - Used by CliPrintf 
                  pLsaInfo - Pointer to tV3OsLsaInfo Structure

  OUTPUT(s)        : NONE             
                                                                       
  RETURNS          : CLI_SUCCESS or CLI_FAILURE 
                                                                       
*******************************************************************************/

PRIVATE INT4
V3OspfCliDisplayLsaSummary (tCliHandle cliHandle,
                            tV3OsLsaInfo * pLsaInfo, UINT2 u2BitMask)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       rtrId;
    UINT1               au1LsaType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1              *pu1AreaId = NULL;
    UINT1              *pu1AdvRtrId = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;
    UINT2               u2LsaAge;

    if ((pLsaInfo->lsaId.u2LsaType != OSPFV3_LINK_LSA) &&
        (pLsaInfo->pArea != NULL))
    {
        if (pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
        {
            OSPFV3_AREA_ID_COPY (areaId, OSPFV3_BACKBONE_AREAID);
        }
        else
        {
            OSPFV3_AREA_ID_COPY (areaId, pLsaInfo->pArea->areaId);
        }
    }
    else if (pLsaInfo->lsaId.u2LsaType == OSPFV3_LINK_LSA)
    {
        OSPFV3_AREA_ID_COPY (areaId, pLsaInfo->pInterface->pArea->areaId);
    }
    else
    {
        OSPFV3_AREA_ID_COPY (areaId, OSPFV3_BACKBONE_AREAID);
    }

    OSPFV3_RTR_ID_COPY (rtrId, pLsaInfo->lsaId.advRtrId);

    OSPFV3_CLI_AREAID_TO_STR (pu1AreaId, OSPFV3_BUFFER_DWFROMPDU (areaId));
    CliPrintf (cliHandle, "%-17s", pu1AreaId);
    OSPFV3_CLI_RTRID_TO_STR (pu1AdvRtrId, OSPFV3_BUFFER_DWFROMPDU (rtrId));
    CliPrintf (cliHandle, "%-17s", pu1AdvRtrId);

    if ((u2BitMask & OSPFV3_CLI_DISP_MASK) == OSPFV3_INIT_VAL)
    {
        switch (pLsaInfo->lsaId.u2LsaType)
        {
            case OSPFV3_ROUTER_LSA:
                STRCPY (au1LsaType, "Router Lsa");
                break;
            case OSPFV3_NETWORK_LSA:
                STRCPY (au1LsaType, "Network Lsa");
                break;
            case OSPFV3_INTER_AREA_PREFIX_LSA:
                STRCPY (au1LsaType, "Inter Area Prefix Lsa");
                break;
            case OSPFV3_INTER_AREA_ROUTER_LSA:
                STRCPY (au1LsaType, "Inter Area Router Lsa");
                break;
            case OSPFV3_NSSA_LSA:
                STRCPY (au1LsaType, "Type-7 Lsa");
                break;
            case OSPFV3_AS_EXT_LSA:
                STRCPY (au1LsaType, "External Lsa");
                break;
            case OSPFV3_LINK_LSA:
                STRCPY (au1LsaType, "Link Lsa");
                break;
            case OSPFV3_INTRA_AREA_PREFIX_LSA:
                STRCPY (au1LsaType, "Intra Area Prefix Lsa");
                break;
            case OSPFV3_GRACE_LSA:
                STRCPY (au1LsaType, "Grace Lsa");
                break;
            default:
                STRCPY (au1LsaType, "Unknown Lsa");
                break;
        }
        if (STRCMP (au1LsaType, "Unknown Lsa") == OSPFV3_INIT_VAL)
        {
            CliPrintf (cliHandle, "0x%04hx  ", pLsaInfo->lsaId.u2LsaType);
        }
        else
        {
            CliPrintf (cliHandle, "%-22s", au1LsaType);
        }
    }

    OSPFV3_GET_LSA_AGE (pLsaInfo, &u2LsaAge);
    CliPrintf (cliHandle, "%-6hd", u2LsaAge);

    CliPrintf (cliHandle, "0x%-12x ", pLsaInfo->lsaSeqNum);

    CliPrintf (cliHandle, "0x%-10hx", pLsaInfo->u2LsaChksum);
    i4PageStatus = CliPrintf (cliHandle, "\r\n");

    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowRedistRouteCfgTblInCxt

  Cli Command   : show ipv6 ospf redist-config 
         
  Description   : Display the configuraiton information to be applied to 
                  the routes learnt from the RTM
 
 Input(s)       : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
 Output(s)      : None
          
 Return Values  : CLI_SUCCESS or CLI_FAILURE 
**************************************************************************/
PRIVATE INT1
V3OspfCliShowRedistRouteCfgTblInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tIp6Addr            ip6Addr;
    tIp6Addr            nextIp6Addr;
    tSNMP_OCTET_STRING_TYPE futOspfv3RedistRouteDest;
    tSNMP_OCTET_STRING_TYPE nextFutOspfv3RedistRouteDest;
    UINT4               u4NextFutOspfv3RedistRoutePfxLength = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    UINT1               au1MetricType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1TagType[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4RetValFutOspfv3RedistRouteMetricType =
        OSPFV3_INIT_VAL;
    INT4                i4RetValFutOspfv3RedistRouteMetric = OSPFV3_INIT_VAL;
    INT4                i4RetValFutOspfv3RedistRouteTagType = OSPFV3_INIT_VAL;
    INT4                i4RetValFutOspfv3RedistRouteTag = OSPFV3_INIT_VAL;
    INT4                i4PrefixType = OSPFV3_INET_ADDR_TYPE;
    INT4                i4PfxType = OSPFV3_INIT_VAL;
    INT4                i4FutOspfv3RedistRoutePfxLength = OSPFV3_INIT_VAL;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;

    futOspfv3RedistRouteDest.pu1_OctetList = (UINT1 *) &ip6Addr;
    nextFutOspfv3RedistRouteDest.pu1_OctetList = (UINT1 *) &nextIp6Addr;
    futOspfv3RedistRouteDest.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    nextFutOspfv3RedistRouteDest.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    MEMSET (&ip6Addr, 0, OSPFV3_IPV6_ADDR_LEN);
    MEMSET (&nextIp6Addr, 0, OSPFV3_IPV6_ADDR_LEN);

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIOspfv3RedistRouteCfgTable
            ((INT4 *) &u4ContextId, &i4PfxType, &nextFutOspfv3RedistRouteDest,
             &u4NextFutOspfv3RedistRoutePfxLength) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4OspfPrevCxtId = u4ContextId;
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable
            ((INT4) u4OspfPrevCxtId, (INT4 *) &u4ContextId, i4PrefixType,
             &i4PfxType, &futOspfv3RedistRouteDest,
             &nextFutOspfv3RedistRouteDest, i4FutOspfv3RedistRoutePfxLength,
             &u4NextFutOspfv3RedistRoutePfxLength) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4ShowAllCxt == OSPFV3_TRUE) || (u4ContextId == u4OspfPrevCxtId))
    {

        CliPrintf (cliHandle,
                   "\r\nOspfv3  External Summary Address"
                   "Configuration Information \r\n");

        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n%-28s%-12s%-12s%-9s%-10s%s\r\n",
                   "Address Prefix", "PfxLength", "MetricType",
                   "Metric", "TagType", "TagValue");
    }

    do
    {
        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }
        else if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n%-28s%-12s%-12s%-9s%-10s%s\r\n",
                       "Address Prefix", "PfxLength", "MetricType",
                       "Metric", "TagType", "TagValue");
        }

        u4OspfPrevCxtId = u4ContextId;

        if (futOspfv3RedistRouteDest.i4_Length > OSPFV3_IPV6_ADDR_LEN)
        {
            return SNMP_FAILURE;
        }
        else
        {
            MEMCPY (futOspfv3RedistRouteDest.pu1_OctetList,
                    nextFutOspfv3RedistRouteDest.pu1_OctetList,
                    OSPFV3_IPV6_ADDR_LEN);

            i4FutOspfv3RedistRoutePfxLength =
                u4NextFutOspfv3RedistRoutePfxLength;

            if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                               &i4AdminStat) == SNMP_FAILURE) ||
                (i4AdminStat == OSPFV3_DISABLED) ||
                (V3UtilOspfGetCxt (u4ContextId) == NULL))
            {
                if (u4ShowAllCxt == OSPFV3_FALSE)
                {
                    /* Invalid context id or admin status of the
                     * context is disabled */
                    break;
                }
                continue;
            }

            if (nmhGetFsMIOspfv3RedistRouteMetric ((INT4) u4ContextId,
                                                   i4PrefixType,
                                                   &futOspfv3RedistRouteDest,
                                                   i4FutOspfv3RedistRoutePfxLength,
                                                   &i4RetValFutOspfv3RedistRouteMetric)
                == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "\r\nnmhGetFutOspfv3RedistRouteMetric:"
                           " Failure\r\n");
                return CLI_FAILURE;
            }

            if (nmhGetFsMIOspfv3RedistRouteMetricType ((INT4) u4ContextId,
                                                       i4PrefixType,
                                                       &futOspfv3RedistRouteDest,
                                                       i4FutOspfv3RedistRoutePfxLength,
                                                       &i4RetValFutOspfv3RedistRouteMetricType)
                == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "\r\nnmhGetFutOspfv3RedistRouteMetricType:"
                           " Failure\r\n");
                return CLI_FAILURE;
            }
            else
            {
                if (i4RetValFutOspfv3RedistRouteMetricType ==
                    OSPFV3_TYPE_1_METRIC)
                {
                    STRCPY (au1MetricType, "asExtType1");
                }
                else
                {
                    STRCPY (au1MetricType, "asExtType2");
                }
            }

            if (nmhGetFsMIOspfv3RedistRouteTagType ((INT4) u4ContextId,
                                                    i4PrefixType,
                                                    &futOspfv3RedistRouteDest,
                                                    i4FutOspfv3RedistRoutePfxLength,
                                                    &i4RetValFutOspfv3RedistRouteTagType)
                == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "\r\nnmhGetFutOspfv3RedistRouteTagType:"
                           " Failure\r\n");
                return CLI_FAILURE;
            }
            else
            {
                if (i4RetValFutOspfv3RedistRouteTagType == OSPFV3_AUTOMATIC_TAG)
                {
                    STRCPY (au1TagType, "automatic");
                }
                else
                {
                    STRCPY (au1TagType, "manual");
                }
            }

            if (nmhGetFsMIOspfv3RedistRouteTag
                ((INT4) u4ContextId, i4PrefixType, &futOspfv3RedistRouteDest,
                 i4FutOspfv3RedistRoutePfxLength,
                 &i4RetValFutOspfv3RedistRouteTag) == SNMP_FAILURE)
            {
                CliPrintf (cliHandle,
                           "\r\nnmhGetFutOspfv3RedistRouteTag:" " Failure\r\n");
                return CLI_FAILURE;
            }

            if (u4ContextId != u4OspfPrevCxtId)
            {
                V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

                CliPrintf (cliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

                CliPrintf (cliHandle, "\r\n%-28s%-12s%-12s%-9s%-10s%s\r\n",
                           "Address Prefix", "PfxLength", "MetricType",
                           "Metric", "TagType", "TagValue");
            }

            i1PageStatus = (INT1) CliPrintf (cliHandle,
                                             "%-30s%-10d%-14s%-7d%-12s%d\r\n",
                                             Ip6PrintAddr ((tIp6Addr *) (VOID *)
                                                           futOspfv3RedistRouteDest.
                                                           pu1_OctetList),
                                             i4FutOspfv3RedistRoutePfxLength,
                                             au1MetricType,
                                             i4RetValFutOspfv3RedistRouteMetric,
                                             au1TagType,
                                             i4RetValFutOspfv3RedistRouteTag);
            if (i1PageStatus == CLI_FAILURE)
            {
                break;
            }
        }
    }
    while (nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable ((INT4) u4OspfPrevCxtId,
                                                         (INT4 *) &u4ContextId,
                                                         i4PrefixType,
                                                         &i4PfxType,
                                                         &futOspfv3RedistRouteDest,
                                                         &nextFutOspfv3RedistRouteDest,
                                                         i4FutOspfv3RedistRoutePfxLength,
                                                         &u4NextFutOspfv3RedistRoutePfxLength)
           != SNMP_FAILURE);

    return i1PageStatus;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayBR
  
  Description  : This routine prints Border Router Table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3BRInfo -  Holds display information

  Output(s)    : None             
                 
  Return Values : CLI_SUCCESS or CLI_FAILURE 
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayBR (tCliHandle cliHandle, tShowOspfv3BRInfo ospfv3BRInfo)
{
    UINT1              *pu1String = NULL;
    UINT1               au1RtrType[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1RtType[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;

    OSPFV3_CLI_RTRID_TO_STR (pu1String, ospfv3BRInfo.u4OspfBRIpAddr);
    CliPrintf (cliHandle, "%-15s", pu1String);

    switch (ospfv3BRInfo.i4OspfBRRtrType)
    {
        case OSPFV3_DEST_AREA_BORDER:
            STRCPY (au1RtrType, "ABR");
            break;
        case OSPFV3_DEST_AS_BOUNDARY:
            STRCPY (au1RtrType, "ASBR");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, "%-7s", au1RtrType);

    CliPrintf (cliHandle, "%-28s",
               Ip6PrintAddr (&(ospfv3BRInfo.ospfBRNextHop)));

    CliPrintf (cliHandle, "%-6d", ospfv3BRInfo.i4OspfBRCost);

    switch (ospfv3BRInfo.i4OspfBRRtType)
    {
        case OSPFV3_INTRA_AREA:
            STRCPY (au1RtType, "intraArea");
            break;
        case OSPFV3_INTER_AREA:
            STRCPY (au1RtType, "interArea");
            break;
        case OSPFV3_TYPE_1_EXT:
            STRCPY (au1RtType, "type1Ext");
            break;
        case OSPFV3_TYPE_2_EXT:
            STRCPY (au1RtType, "type2Ext");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, "%-12s", au1RtType);

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3BRInfo.u4OspfBRArea);
    i4PageStatus = CliPrintf (cliHandle, "%s\r\n", pu1String);

    return i4PageStatus;

}

/*************************************************************************
  Function Name : V3OspfCliShowBorderRouterInCxt 

  Cli Command   : show ipv6 ospf border-routers 
         
  Description   : Displays the internal ospfv3 routing table entries 
                  to an ABR/ASBR
 
 Input(s)       : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
 Output(s)      : None
          
 Return Values  : CLI_SUCCESS or CLI_FAILURE 
**************************************************************************/
PRIVATE INT1
V3OspfCliShowBorderRouterInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3BRInfo   showOspfv3BRInfo;
    tIp6Addr            ospfBRNextHop;
    tSNMP_OCTET_STRING_TYPE prevFutOspfBRNextHop;
    tSNMP_OCTET_STRING_TYPE futOspfBRNextHop;
    UINT4               u4FutOspfRouteIpAddr = OSPFV3_INIT_VAL;
    UINT4               u4RetValFutOspfRouteAreaId = OSPFV3_INIT_VAL;
    UINT4               u4PrevFutOspfRouteIpAddr = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4FutOspfRouteDestType = OSPFV3_INIT_VAL;
    INT4                i4RetValFutOspfRouteCost = OSPFV3_INIT_VAL;
    INT4                i4PrevFutOspfRouteDestType = OSPFV3_INIT_VAL;
    INT4                i4RetValFutOspfRouteType = OSPFV3_INIT_VAL;
    INT4                i4NextHopType;
    INT4                i4AdminStat;
    INT1                i1PageStatus = CLI_SUCCESS;

    MEMSET (&prevFutOspfBRNextHop, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    prevFutOspfBRNextHop.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    futOspfBRNextHop.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    futOspfBRNextHop.pu1_OctetList = (UINT1 *) &ospfBRNextHop;

    CliPrintf (cliHandle,
               "\r\n        Ospfv3 Process Border Router Information\r\n");

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIOspfv3BRRouteTable ((INT4 *) &u4ContextId,
                                                    &u4FutOspfRouteIpAddr,
                                                    &i4NextHopType,
                                                    &futOspfBRNextHop,
                                                    &i4FutOspfRouteDestType) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        u4OspfPrevCxtId = u4ContextId;
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIOspfv3BRRouteTable (u4OspfPrevCxtId,
                                                   (INT4 *) &u4ContextId,
                                                   u4PrevFutOspfRouteIpAddr,
                                                   &u4FutOspfRouteIpAddr,
                                                   OSPFV3_INET_ADDR_TYPE,
                                                   &i4NextHopType,
                                                   &prevFutOspfBRNextHop,
                                                   &futOspfBRNextHop,
                                                   i4PrevFutOspfRouteDestType,
                                                   &i4FutOspfRouteDestType) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

    CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

    CliPrintf (cliHandle, "\r\n%-15s%-18s%-15s%-8s%-12s%s\r\n",
               "Destination", "Type", "NextHop", "Cost", "Rt Type", "Area Id");

    do
    {
        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }
        else if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n%-15s%-18s%-15s%-8s%-12s%s\r\n",
                       "Destination", "Type", "NextHop", "Cost", "Rt Type",
                       "Area Id");
        }

        u4OspfPrevCxtId = u4ContextId;
        u4PrevFutOspfRouteIpAddr = u4FutOspfRouteIpAddr;
        prevFutOspfBRNextHop.pu1_OctetList = futOspfBRNextHop.pu1_OctetList;
        i4PrevFutOspfRouteDestType = i4FutOspfRouteDestType;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        if (nmhGetFsMIOspfv3BRRouteCost ((INT4) u4ContextId,
                                         u4FutOspfRouteIpAddr,
                                         OSPFV3_INET_ADDR_TYPE,
                                         &futOspfBRNextHop,
                                         i4FutOspfRouteDestType,
                                         &i4RetValFutOspfRouteCost) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure For Route Cost \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIOspfv3BRRouteType ((INT4) u4ContextId,
                                         u4FutOspfRouteIpAddr,
                                         OSPFV3_INET_ADDR_TYPE,
                                         &futOspfBRNextHop,
                                         i4FutOspfRouteDestType,
                                         &i4RetValFutOspfRouteType) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure for Route Type \r\n");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIOspfv3BRRouteAreaId ((INT4) u4ContextId,
                                           u4FutOspfRouteIpAddr,
                                           OSPFV3_INET_ADDR_TYPE,
                                           &futOspfBRNextHop,
                                           i4FutOspfRouteDestType,
                                           &u4RetValFutOspfRouteAreaId) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure for Area  \r\n");
            return CLI_FAILURE;
        }

        showOspfv3BRInfo.u4OspfBRIpAddr = u4FutOspfRouteIpAddr;
        OSPFV3_IP6_ADDR_COPY (showOspfv3BRInfo.ospfBRNextHop,
                              *(futOspfBRNextHop.pu1_OctetList));
        showOspfv3BRInfo.i4OspfBRRtrType = i4FutOspfRouteDestType;
        showOspfv3BRInfo.i4OspfBRCost = i4RetValFutOspfRouteCost;
        showOspfv3BRInfo.i4OspfBRRtType = i4RetValFutOspfRouteType;
        showOspfv3BRInfo.u4OspfBRArea = u4RetValFutOspfRouteAreaId;

        i1PageStatus = (INT1) V3OspfCliDisplayBR (cliHandle, showOspfv3BRInfo);

        if (i1PageStatus == CLI_FAILURE)
        {
            break;
        }

        /* prepare to get the next indices */

    }
    while (nmhGetNextIndexFsMIOspfv3BRRouteTable (u4OspfPrevCxtId,
                                                  (INT4 *) &u4ContextId,
                                                  u4PrevFutOspfRouteIpAddr,
                                                  &u4FutOspfRouteIpAddr,
                                                  OSPFV3_INET_ADDR_TYPE,
                                                  &i4NextHopType,
                                                  &prevFutOspfBRNextHop,
                                                  &futOspfBRNextHop,
                                                  i4PrevFutOspfRouteDestType,
                                                  &i4FutOspfRouteDestType) !=
           SNMP_FAILURE);
    return i1PageStatus;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplaySA
  
  Description  : This routine prints Area Aggregation Table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3SAInfo -  Holds display information
                 
  Output(s)    : None             
                 
  Return Values : CLI_SUCCESS or CLI_FAILURE 
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplaySA (tCliHandle cliHandle, tShowOspfv3SAInfo ospfv3SAInfo)
{
    UINT1              *pu1String = NULL;
    UINT1               au1SAEffect[OSPFV3_CLI_MAX_STRING_LEN];
    UINT1               au1SALsaType[OSPFV3_CLI_MAX_STRING_LEN];
    INT4                i4PageStatus = CLI_SUCCESS;

    CliPrintf (cliHandle, "%-28s", Ip6PrintAddr (&(ospfv3SAInfo.ospfSAPrefix)));

    CliPrintf (cliHandle, "%-7d", ospfv3SAInfo.u4OspfSAPfxLen);

    switch (ospfv3SAInfo.i4OspfSALsaType)
    {
        case OSPFV3_INTER_AREA_PREFIX_LSA:
            STRCPY (au1SALsaType, "Summary");
            break;
        case OSPFV3_NSSA_LSA:
            STRCPY (au1SALsaType, "Type7");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, "%-10s", au1SALsaType);

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3SAInfo.u4OspfSAArea);
    CliPrintf (cliHandle, "%-12s", pu1String);

    switch (ospfv3SAInfo.i4OspfSAEffect)
    {
        case OSPFV3_ADVERTISE_MATCHING:
            STRCPY (au1SAEffect, "advertise");
            break;
        case OSPFV3_DO_NOT_ADVERTISE_MATCHING:
            STRCPY (au1SAEffect, "doNotAdvertise");
            break;
        default:
            break;
    }
    CliPrintf (cliHandle, "%-15s", au1SAEffect);

    i4PageStatus = CliPrintf (cliHandle, "%d\r\n", ospfv3SAInfo.i4OspfSATag);

    return i4PageStatus;

}

/*************************************************************************
  Function Name : V3OspfCliShowSummaryAddressInCxt 

  Cli Command   : show ipv6 ospf { area-range } 
         
  Description   : Displays summary address information
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values : CLI_SUCCESS or CLI_FAILURE 
**************************************************************************/
PRIVATE INT1
V3OspfCliShowSummaryAddressInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3SAInfo   showOspfv3SAInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            prevIp6Addr;
    tSNMP_OCTET_STRING_TYPE ospfSANet;
    tSNMP_OCTET_STRING_TYPE prevOspfSANet;
    UINT4               u4OspfSAAreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfSAPfxLen = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfSAAreaId = OSPFV3_INIT_VAL;
    UINT4               u4PrevOspfSAPfxLen = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4OspfSALsaType = OSPFV3_INIT_VAL;
    INT4                i4PrevOspfSALsaType = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfSAEffect = OSPFV3_INIT_VAL;
    INT4                i4RetValOspfSATag = OSPFV3_INIT_VAL;
    INT4                i4PfxType = OSPFV3_INIT_VAL;
    INT1                i1PageStatus = CLI_SUCCESS;
    INT4                i4AdminStat;

    MEMSET (&prevOspfSANet, OSPFV3_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ospfSANet, OSPFV3_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    ospfSANet.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    prevOspfSANet.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    MEMSET (&ip6Addr, OSPFV3_ZERO, OSPFV3_IPV6_ADDR_LEN);
    MEMSET (&prevIp6Addr, OSPFV3_ZERO, OSPFV3_IPV6_ADDR_LEN);

    ospfSANet.pu1_OctetList = (UINT1 *) &ip6Addr;
    prevOspfSANet.pu1_OctetList = (UINT1 *) &prevIp6Addr;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIStdOspfv3AreaAggregateTable
            ((INT4 *) &u4ContextId, &u4OspfSAAreaId, &i4OspfSALsaType,
             &i4PfxType, &ospfSANet, &u4OspfSAPfxLen) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4OspfPrevCxtId = u4ContextId;
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIStdOspfv3AreaAggregateTable (u4OspfPrevCxtId,
                                                            (INT4 *)
                                                            &u4ContextId,
                                                            u4PrevOspfSAAreaId,
                                                            &u4OspfSAAreaId,
                                                            i4PrevOspfSALsaType,
                                                            &i4OspfSALsaType,
                                                            i4PfxType,
                                                            &i4PfxType,
                                                            &prevOspfSANet,
                                                            &ospfSANet,
                                                            u4PrevOspfSAPfxLen,
                                                            &u4OspfSAPfxLen) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if ((u4ShowAllCxt == OSPFV3_TRUE) || (u4ContextId == u4OspfPrevCxtId))
    {
        CliPrintf (cliHandle,
                   "\r\nOspfv3  Summary Address Configuration Information\r\n");

        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n%-24s%-11s%-10s%-12s%-15s%s\r\n",
                   "Network", "PfxLength", "LSA Type", "Area", "Effect", "Tag");

    }

    do
    {
        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }
        else if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n  Vrf  %s\r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n%-24s%-11s%-10s%-12s%-15s%s\r\n",
                       "Network", "PfxLength", "LSA Type", "Area", "Effect",
                       "Tag");
        }

        u4OspfPrevCxtId = u4ContextId;
        MEMCPY (prevOspfSANet.pu1_OctetList, ospfSANet.pu1_OctetList,
                OSPFV3_IPV6_ADDR_LEN);
        u4PrevOspfSAPfxLen = u4OspfSAPfxLen;
        u4PrevOspfSAAreaId = u4OspfSAAreaId;
        i4PrevOspfSALsaType = i4OspfSALsaType;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        if (nmhGetFsMIStdOspfv3AreaAggregateEffect ((INT4) u4ContextId,
                                                    u4OspfSAAreaId,
                                                    i4OspfSALsaType, i4PfxType,
                                                    &ospfSANet, u4OspfSAPfxLen,
                                                    &i4RetValOspfSAEffect) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure For SummaryEffect \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3AreaAggregateRouteTag ((INT4) u4ContextId,
                                                      u4OspfSAAreaId,
                                                      i4OspfSALsaType,
                                                      i4PfxType, &ospfSANet,
                                                      u4OspfSAPfxLen,
                                                      &i4RetValOspfSATag) ==
            SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure For SummaryTag \r\n ");
            return CLI_FAILURE;
        }

        OSPFV3_IP6_ADDR_COPY (showOspfv3SAInfo.ospfSAPrefix,
                              *(ospfSANet.pu1_OctetList));
        showOspfv3SAInfo.u4OspfSAPfxLen = u4OspfSAPfxLen;
        showOspfv3SAInfo.u4OspfSAArea = u4OspfSAAreaId;
        showOspfv3SAInfo.i4OspfSALsaType = i4OspfSALsaType;
        showOspfv3SAInfo.i4OspfSAEffect = i4RetValOspfSAEffect;
        showOspfv3SAInfo.i4OspfSATag = i4RetValOspfSATag;

        if (u4ContextId != u4OspfPrevCxtId)
        {
            V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

            CliPrintf (cliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

            CliPrintf (cliHandle, "\r\n%-24s%-11s%-10s%-12s%-15s%s\r\n",
                       "Network", "PfxLength", "LSA Type", "Area", "Effect",
                       "Tag");
        }

        i1PageStatus = (INT1) V3OspfCliDisplaySA (cliHandle, showOspfv3SAInfo);

        if (i1PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while (nmhGetNextIndexFsMIStdOspfv3AreaAggregateTable (u4OspfPrevCxtId,
                                                           (INT4 *)
                                                           &u4ContextId,
                                                           u4PrevOspfSAAreaId,
                                                           &u4OspfSAAreaId,
                                                           i4PrevOspfSALsaType,
                                                           &i4OspfSALsaType,
                                                           i4PfxType,
                                                           &i4PfxType,
                                                           &prevOspfSANet,
                                                           &ospfSANet,
                                                           u4PrevOspfSAPfxLen,
                                                           &u4OspfSAPfxLen) !=
           SNMP_FAILURE);

    return i1PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowDB 

  Description   : This Routine display Lsa Info
 
  Input(s)      : cliHandle - CLI Handle
                  pLsaInfo  - Pointer to tV3OsLsaInfo structure
                  u2BitMask - Display bit mask
         
  Output(s)     : None
          
  Return Values : CLI_SUCCESS or CLI_FAILURE 
**************************************************************************/
PRIVATE INT4
V3OspfCliShowDB (tV3OsLsaInfo * pLsaInfo, tCliHandle cliHandle, UINT2 u2BitMask)
{
    INT4                i4PageStatus = CLI_SUCCESS;

    if ((u2BitMask & OSPFV3_CLI_DB_SUMMARY) != OSPFV3_INIT_VAL)
    {
        i4PageStatus =
            V3OspfCliDisplayLsaSummary (cliHandle, pLsaInfo, u2BitMask);
    }
    else
    {
        i4PageStatus = V3OspfCliDisplayLsa (cliHandle, pLsaInfo, u2BitMask);
    }

    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfDbRBTreeDisplay 

  Description   : Function for Displaying the LSA in the RB Tree,
                  Display DB Information
 
  Input(s)      : cliHandle - CLI Handle
                  pLsaInfo  - Pointer to LSA info
                  u2DbBitMask  -  Display Bit Mask
                                           
  Output(s)     : None
          
  Return Values : CLI_SUCCESS / CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfDbRBTreeDisplay (tCliHandle cliHandle, tV3OsLsaInfo * pLsaInfo,
                       UINT2 u2DbBitMask)
{
    UINT2               u2LsaType = OSPFV3_INIT_VAL;
    INT4                i4PageStatus = CLI_SUCCESS;

    if (pLsaInfo != NULL)
    {
        u2LsaType = (u2DbBitMask & OSPFV3_CLI_DISP_MASK);

        if ((pLsaInfo->lsaId.u2LsaType == u2LsaType)
            || (u2LsaType == OSPFV3_INIT_VAL))
        {
            i4PageStatus = V3OspfCliShowDB (pLsaInfo, cliHandle, u2DbBitMask);
            if (i4PageStatus == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayHeading
  
  Description  : This routine prints Lsa Heading information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 u2LsaType - Lsa Type
                 
  Output(s)    : None             
                 
  Return Values: None
******************************************************************************/
PRIVATE VOID
V3OspfCliDisplayHeading (tCliHandle cliHandle, UINT2 u2LsaType)
{
    switch (u2LsaType)
    {
        case OSPFV3_ROUTER_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Router Lsa\r\n");
            break;
        case OSPFV3_NETWORK_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Network Lsa\r\n");
            break;
        case OSPFV3_INTER_AREA_PREFIX_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Inter Area "
                       "Prefix Lsa\r\n");
            break;
        case OSPFV3_INTER_AREA_ROUTER_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Inter Area "
                       "Router Lsa\r\n");
            break;
        case OSPFV3_NSSA_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying NSSA Lsa\r\n");
            break;
        case OSPFV3_AS_EXT_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying As-External Lsa\r\n");
            break;
        case OSPFV3_LINK_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Link Lsa\r\n");
            break;
        case OSPFV3_INTRA_AREA_PREFIX_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Intra Area Lsa\r\n");
            break;
        case OSPFV3_GRACE_LSA:
            CliPrintf (cliHandle, "\r\nDisplaying Grace Lsa\r\n");
            break;
        default:
            break;
    }

    CliPrintf (cliHandle, "\r\n%-17s%-17s", "AreaId", "RtrId");

    if ((u2LsaType & OSPFV3_CLI_DISP_MASK) == OSPFV3_INIT_VAL)
    {
        CliPrintf (cliHandle, "%-22s%-6s%-12s%-10s",
                   "LsaType", "Age", "Seq#", "Checksum");
    }
    else
    {
        CliPrintf (cliHandle, "%-6s%-12s%-10s", "Age", "Seq#", "Checksum");
    }

    if (u2LsaType == OSPFV3_ROUTER_LSA)
    {
        CliPrintf (cliHandle, "%s\r\n", "#Links");
    }
    else
    {
        CliPrintf (cliHandle, "\r\n");
    }
}

/*************************************************************************
  Function Name : V3OspfCliShowDatabaseInCxt

  Cli Command   : show ipv6 ospf [area <area-id>] database [router|network|
          as-external|inter-prefix|inter-router|intra-prefix|link|nssa] 
        [detail|HEX] 
         
  Description   :  Displays the LSA information 
 
  Input(s)      : cliHandle   - Used by CliPrintf 
                  u4ContextId - Context Id
                  u4ShowLsaType, u4ShowType, u1AreaFlag
                  u4AreaId    - Area Id
         
  Output(s)     : None
          
  Return Values : CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowDatabaseInCxt (tCliHandle cliHandle, UINT4 u4ContextId,
                            UINT4 u4ShowLsaType, UINT4 u4ShowType,
                            UINT1 u1AreaFlag, UINT4 u4AreaId,
                            UINT4 u4LsaDispType)
{
    UINT2               u2DbBitMask = OSPFV3_INIT_VAL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface      currIf;
    tV3OsArea          *pArea = NULL;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4AdminStat;
    tLsdbCounter        LsdbCounter;
    UINT4               u4PrevAreaId;
    INT4                i4CxtId;
    INT4                i4PrevCxtId;
    UINT4               u4LsTypeTotal = 0;
    UINT4               u4LsTypeMaxAgedTotal = 0;
    UINT1               u1LsType;
    INT4                i4RetVal = CLI_FAILURE;

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        /* Display all context detail */
        /* Get the first context id */
        if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
        {
            return CLI_SUCCESS;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        MEMSET (&LsdbCounter, 0, sizeof (tLsdbCounter));
        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        CliPrintf (cliHandle, "\r\n  Vrf  %s \r\n", au1OspfCxtName);

        if ((u4LsaDispType == 0) || (u4LsaDispType == OSPFV3_LSA_AREA_DATABASE))
        {
            if (u4ShowLsaType != OSPFV3_CLI_DB_ALL_LSA)
            {
                if (!IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (u4ShowLsaType))
                {
                    if (u4ShowAllCxt == OSPFV3_FALSE)
                    {
                        break;
                    }
                    continue;
                }
            }

            if (u4ShowType == OSPFV3_CLI_DB_SUMMARY)
            {
                V3OspfCliDisplayHeading (cliHandle, (UINT2) u4ShowLsaType);
            }

            u2DbBitMask = (UINT2) u4ShowType;
            u2DbBitMask |= (UINT2) u4ShowLsaType;

            OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);

            /* Print Link Scope Lsas */
            if ((u4ShowLsaType == OSPFV3_CLI_DB_LINK) ||
                (u4ShowLsaType == OSPFV3_CLI_DB_ALL_LSA))
            {
                if (u1AreaFlag == OSIX_TRUE)
                {
                    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);
                    if (pArea != NULL)
                    {
                        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode,
                                      tTMO_SLL_NODE *)
                        {
                            pInterface =
                                OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                                     nextIfInArea, pLstNode);
                            pLsaInfo =
                                (tV3OsLsaInfo *) RBTreeGetFirst (pInterface->
                                                                 pLinkScopeLsaRBRoot);

                            do
                            {
                                if (V3OspfDbRBTreeDisplay (cliHandle, pLsaInfo,
                                                           u2DbBitMask)
                                    == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                            }
                            while ((pLsaInfo = (tV3OsLsaInfo *)
                                    RBTreeGetNext (pInterface->
                                                   pLinkScopeLsaRBRoot,
                                                   (tRBElem *) pLsaInfo,
                                                   NULL)) != NULL);

                        }
                    }
                }
                else
                {
                    if ((pInterface = (tV3OsInterface *)
                         RBTreeGetFirst (gV3OsRtr.pIfRBRoot)) == NULL)
                    {
                        return CLI_FAILURE;
                    }
                    do
                    {
                        currIf.u4InterfaceId = pInterface->u4InterfaceId;

                        if (pInterface->pArea->pV3OspfCxt->u4ContextId !=
                            u4ContextId)
                        {
                            continue;
                        }

                        pLsaInfo = (tV3OsLsaInfo *)
                            RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot);

                        do
                        {
                            if (V3OspfDbRBTreeDisplay (cliHandle, pLsaInfo,
                                                       u2DbBitMask) ==
                                CLI_FAILURE)
                            {
                                return CLI_FAILURE;
                            }
                        }
                        while ((pLsaInfo = (tV3OsLsaInfo *)
                                RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                               (tRBElem *) pLsaInfo, NULL))
                               != NULL);
                    }
                    while ((pInterface =
                            (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.
                                                              pIfRBRoot,
                                                              (tRBElem *) &
                                                              currIf,
                                                              NULL)) != NULL);
                }

                if (u4ShowLsaType == OSPFV3_CLI_DB_LINK)
                {
                    if (u4ShowAllCxt == OSPFV3_FALSE)
                    {
                        break;
                    }
                    continue;
                }

            }

            /* Print Area Scope Lsas */
            if (u4ShowLsaType != OSPFV3_CLI_DB_EXTERNAL)
            {
                if (u1AreaFlag == OSIX_TRUE)
                {
                    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
                    {
                        if (V3UtilAreaIdComp (areaId, pArea->areaId) ==
                            OSPFV3_EQUAL)
                        {
                            pLsaInfo = (tV3OsLsaInfo *)
                                RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

                            do
                            {
                                if (V3OspfDbRBTreeDisplay (cliHandle, pLsaInfo,
                                                           u2DbBitMask)
                                    == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                            }
                            while ((pLsaInfo = (tV3OsLsaInfo *)
                                    RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                                   (tRBElem *) pLsaInfo, NULL))
                                   != NULL);

                            break;
                        }
                    }
                }
                else
                {
                    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
                    {
                        pLsaInfo = (tV3OsLsaInfo *)
                            RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

                        do
                        {
                            if (V3OspfDbRBTreeDisplay (cliHandle, pLsaInfo,
                                                       u2DbBitMask) ==
                                CLI_FAILURE)
                            {
                                return CLI_FAILURE;
                            }
                        }
                        while ((pLsaInfo = (tV3OsLsaInfo *)
                                RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                               (tRBElem *) pLsaInfo, NULL))
                               != NULL);
                    }
                }

                if (u4ShowLsaType != OSPFV3_CLI_DB_ALL_LSA)
                {
                    if (u4ShowAllCxt == OSPFV3_FALSE)
                    {
                        break;
                    }
                    continue;
                }
            }

            if ((u1AreaFlag == OSIX_TRUE) &&
                (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) !=
                 OSPFV3_EQUAL))
            {
                if (u4ShowAllCxt == OSPFV3_FALSE)
                {
                    break;
                }
                continue;
            }
            /* Print As Scope External Lsas */
            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

            do
            {
                if (V3OspfDbRBTreeDisplay (cliHandle, pLsaInfo,
                                           u2DbBitMask) == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            while ((pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                                   (tRBElem *) pLsaInfo, NULL)) != NULL);

            /* If u4ShowAllCxt is set to OSPF_TRUE then all context
             * must be displayed */
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                break;
            }

        }

        if (u4LsaDispType == OSPFV3_LSA_DB_SUMMARY)
        {
            if (u1AreaFlag == OSPFV3_TRUE)
            {
                if (V3OspfCliShowAreaDataBaseSummaryInCxt (cliHandle,
                                                           u4ContextId,
                                                           u4AreaId,
                                                           &LsdbCounter) ==
                    CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (nmhGetFirstIndexFsMIStdOspfv3AreaTable (&i4CxtId, &u4AreaId)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                do
                {
                    if (u4ContextId == ((UINT4) i4CxtId))
                    {
                        if (V3OspfCliShowAreaDataBaseSummaryInCxt (cliHandle,
                                                                   u4ContextId,
                                                                   u4AreaId,
                                                                   &LsdbCounter)
                            == CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }
                    }
                    i4PrevCxtId = i4CxtId;
                    u4PrevAreaId = u4AreaId;
                }
                while (nmhGetNextIndexFsMIStdOspfv3AreaTable
                       (i4PrevCxtId, &i4CxtId,
                        u4PrevAreaId, &u4AreaId) != SNMP_FAILURE);

                /* Display Total Process database summary */
                CliPrintf (cliHandle,
                           "\r\nOSPFV3 Process database summary\r\n");
                CliPrintf (cliHandle, "-----------------------------\r\n");
                CliPrintf (cliHandle, "  %-20s %-10s %-10s\r\n",
                           "LSA Type", "Count", "Maxage");
                CliPrintf (cliHandle, "  %-20s %-10s %-10s\r\n",
                           "--------", "-----", "------");

                for (u1LsType = OSPFV3_CLI_ROUTER_LSA;
                     u1LsType <= OSPFV3_CLI_INTRA_AREA_PREFIX_LSA; u1LsType++)
                {
                    switch (u1LsType)
                    {
                        case OSPFV3_CLI_ROUTER_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Router",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_NETWORK_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Network",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_INTER_AREA_PREFIX_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Inter Area Prefix",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_INTER_AREA_ROUTER_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Inter Area Router",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_AS_EXT_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "As External",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_NSSA_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Type-7 Ext",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_LINK_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Link",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        case OSPFV3_CLI_INTRA_AREA_PREFIX_LSA:
                            i4RetVal =
                                CliPrintf (cliHandle,
                                           "  %-20s %-10u %-10u\r\n",
                                           "Intra Area Prefix",
                                           LsdbCounter.au4LsTypeCnt[u1LsType -
                                                                    1],
                                           LsdbCounter.
                                           au4LsTypeMaxAgedCnt[u1LsType - 1]);
                            break;
                        default:
                            break;
                    }
                    u4LsTypeTotal += LsdbCounter.au4LsTypeCnt[u1LsType - 1];
                    u4LsTypeMaxAgedTotal +=
                        LsdbCounter.au4LsTypeMaxAgedCnt[u1LsType - 1];

                    if (i4RetVal == CLI_FAILURE)
                        break;
                }
                CliPrintf (cliHandle, "  %-20s %-10u %-10u\r\n\r\n",
                           "Total", u4LsTypeTotal, u4LsTypeMaxAgedTotal);

            }

        }

        if ((u4LsaDispType == OSPFV3_LSA_SELF_ORG)
            || (u4LsaDispType == OSPFV3_LSA_ADV_ROUTER))
        {
            if (u1AreaFlag == OSPFV3_TRUE)
            {
                if (V3OspfCliShowFilterSummaryInfo (cliHandle,
                                                    u4ContextId,
                                                    u4AreaId,
                                                    u4LsaDispType) ==
                    CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (nmhGetFirstIndexFsMIStdOspfv3AreaTable (&i4CxtId, &u4AreaId)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                do
                {
                    if (u4ContextId == ((UINT4) i4CxtId))
                    {
                        if (V3OspfCliShowFilterSummaryInfo (cliHandle,
                                                            u4ContextId,
                                                            u4AreaId,
                                                            u4LsaDispType) ==
                            CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }
                    }
                    i4PrevCxtId = i4CxtId;
                    u4PrevAreaId = u4AreaId;
                }
                while (nmhGetNextIndexFsMIStdOspfv3AreaTable
                       (i4PrevCxtId, &i4CxtId,
                        u4PrevAreaId, &u4AreaId) != SNMP_FAILURE);
            }
        }

    }

    while (V3UtilOspfGetNextCxtId (u4OspfPrevCxtId, &u4ContextId) !=
           OSIX_FAILURE);

    return CLI_SUCCESS;
}

/*******************************************************************************
  Function Name: V3OspfCliDisplayHost
  
  Description  : This routine prints host table information  
  
  Input(s)     : cliHandle - Used by CliPrintf 
                 ospfv3HostInfo -  Holds display information
                 
  Output(s)    : None             
                 
  Return Values : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliDisplayHost (tCliHandle cliHandle, tShowOspfv3HostInfo ospfv3HostInfo)
{
    UINT1              *pu1String = NULL;
    INT4                i4PageStatus = CLI_SUCCESS;

    CliPrintf (cliHandle, "%-40s",
               Ip6PrintAddr (&(ospfv3HostInfo.hostIp6Addr)));

    OSPFV3_CLI_AREAID_TO_STR (pu1String, ospfv3HostInfo.u4Ospfv3HostAreaId);
    CliPrintf (cliHandle, "%-20s", pu1String);

    i4PageStatus = CliPrintf (cliHandle, "%d\r\n",
                              ospfv3HostInfo.i4Ospfv3HostMetric);
    return i4PageStatus;
}

/*************************************************************************
  Function Name : V3OspfCliShowHostInCxt

  Cli Command   :  show ipv6 ospf host [{<RouterID>}] 
         
  Description   :  Display Host information connected to a router
 
  Input(s)      : cliHandle - Used by CliPrintf 
                  u4ContextId - Context Id
         
  Output(s)     : None
          
  Return Values : CLI_SUCCESS or CLI_FAILURE
**************************************************************************/
PRIVATE INT1
V3OspfCliShowHostInCxt (tCliHandle cliHandle, UINT4 u4ContextId)
{
    tShowOspfv3HostInfo showOspfv3HostInfo;
    tIp6Addr            ip6Addr;
    tIp6Addr            prevIp6Addr;
    tSNMP_OCTET_STRING_TYPE ospfv3HostAddr;
    tSNMP_OCTET_STRING_TYPE prevOspfv3HostAddr;
    UINT4               u4Ospfv3HostAreaId = OSPFV3_INIT_VAL;
    UINT4               u4OspfPrevCxtId;
    UINT4               u4ShowAllCxt = OSPFV3_FALSE;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    INT4                i4Ospfv3HostMetric = OSPFV3_INIT_VAL;
    INT4                i4HostAddressType = OSPFV3_INIT_VAL;
    INT1                i1PageStatus = CLI_SUCCESS;
    INT4                i4AdminStat;

    MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&prevIp6Addr, 0, sizeof (tIp6Addr));

    ospfv3HostAddr.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    prevOspfv3HostAddr.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    ospfv3HostAddr.pu1_OctetList = (UINT1 *) &ip6Addr;
    prevOspfv3HostAddr.pu1_OctetList = (UINT1 *) &prevIp6Addr;

    CliPrintf (cliHandle, "\r\n OSPFV3  HOST  CONFIGURATION Information \r\n");

    CliPrintf (cliHandle, "%-40s%-20s%s\r\n",
               "Address", "AreaId", "StubMetric");

    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        if (nmhGetFirstIndexFsMIStdOspfv3HostTable ((INT4 *) &u4ContextId,
                                                    &i4HostAddressType,
                                                    &ospfv3HostAddr) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        u4ShowAllCxt = OSPFV3_TRUE;
    }
    else
    {
        u4OspfPrevCxtId = u4ContextId;

        if (nmhGetNextIndexFsMIStdOspfv3HostTable (u4OspfPrevCxtId,
                                                   (INT4 *) &u4ContextId,
                                                   i4HostAddressType,
                                                   &i4HostAddressType,
                                                   &prevOspfv3HostAddr,
                                                   &ospfv3HostAddr) !=
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        u4OspfPrevCxtId = u4ContextId;
        prevOspfv3HostAddr.pu1_OctetList = ospfv3HostAddr.pu1_OctetList;

        if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId,
                                           &i4AdminStat) == SNMP_FAILURE) ||
            (i4AdminStat == OSPFV3_DISABLED) ||
            (V3UtilOspfGetCxt (u4ContextId) == NULL))
        {
            if (u4ShowAllCxt == OSPFV3_FALSE)
            {
                /* Invalid context id or admin status of the
                 * context is disabled */
                break;
            }
            continue;
        }

        if ((u4ContextId != u4OspfPrevCxtId) && (u4ShowAllCxt == OSPFV3_FALSE))
        {
            break;
        }

        V3UtilGetCxtName (u4ContextId, au1OspfCxtName);

        if (nmhGetFsMIStdOspfv3HostAreaID
            ((INT4) u4ContextId, i4HostAddressType, &ospfv3HostAddr,
             &u4Ospfv3HostAreaId) == SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure For HostAreaId \r\n ");
            return CLI_FAILURE;
        }

        if (nmhGetFsMIStdOspfv3HostMetric
            ((INT4) u4ContextId, i4HostAddressType, &ospfv3HostAddr,
             &i4Ospfv3HostMetric) == SNMP_FAILURE)
        {
            CliPrintf (cliHandle, " \r\n Get Failure For HostAMetric\r\n ");
            return CLI_FAILURE;
        }

        OSPFV3_IP6_ADDR_COPY (showOspfv3HostInfo.hostIp6Addr,
                              *(ospfv3HostAddr.pu1_OctetList));

        showOspfv3HostInfo.u4Ospfv3HostAreaId = u4Ospfv3HostAreaId;

        showOspfv3HostInfo.i4Ospfv3HostMetric = i4Ospfv3HostMetric;

        CliPrintf (cliHandle, "  Vrf  %s \r\n", au1OspfCxtName);

        i1PageStatus =
            (INT1) V3OspfCliDisplayHost (cliHandle, showOspfv3HostInfo);

        if (i1PageStatus == CLI_FAILURE)
        {
            break;
        }
    }
    while (nmhGetNextIndexFsMIStdOspfv3HostTable (u4OspfPrevCxtId,
                                                  (INT4 *) &u4ContextId,
                                                  i4HostAddressType,
                                                  &i4HostAddressType,
                                                  &prevOspfv3HostAddr,
                                                  &ospfv3HostAddr) !=
           SNMP_FAILURE);
    return i1PageStatus;
}

/*******************************************************************************
  Function Name: V3OspfCliShowRedStatus

  Description  : This routine prints Redundany status information for OspfV3

  Input(s)     : cliHandle - Used by CliPrintf

  Output(s)    : None

  Return Values : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
V3OspfCliShowRedStatus (tCliHandle cliHandle)
{
    INT4                i4HsAdminStatus = OSPFV3_ZERO;
    INT4                i4HsState = OSPFV3_ZERO;
    INT4                i4DynBulkUpdatStatus = OSPFV3_ZERO;
    UINT4               u4HelloSynCount = OSPFV3_ZERO;
    UINT4               u4LsaSyncCount = OSPFV3_ZERO;

    if (nmhGetFutOspfv3HotStandbyAdminStatus (&i4HsAdminStatus) != SNMP_SUCCESS)
    {
        CliPrintf (cliHandle, "\r\nCannot get Hot Standby Admin Status!");
        return CLI_FAILURE;
    }

    if (nmhGetFutOspfv3HotStandbyState (&i4HsState) != SNMP_SUCCESS)
    {
        CliPrintf (cliHandle, "\r\nCannot get Hot Standby State!");
        return CLI_FAILURE;
    }

    if (nmhGetFutOspfv3DynamicBulkUpdStatus (&i4DynBulkUpdatStatus)
        != SNMP_SUCCESS)
    {
        CliPrintf (cliHandle,
                   "\r\nCannot get Hot Standby Dynamis Bulk Update Status!");
        return CLI_FAILURE;
    }

    if (nmhGetFutOspfv3StandbyHelloSyncCount (&u4HelloSynCount) != SNMP_SUCCESS)
    {
        CliPrintf (cliHandle, "\r\nCannot get Hot Standby Hello Sync Count!");
        return CLI_FAILURE;
    }

    if (nmhGetFutOspfv3StandbyLsaSyncCount (&u4LsaSyncCount) != SNMP_SUCCESS)
    {
        CliPrintf (cliHandle, "\r\nCannot get Hot Standby Lsa Sync Count!");
        return CLI_FAILURE;
    }

    CliPrintf (cliHandle, "\r\n OSPFv3 Hot Standby Admin Status : ");
    if (i4HsAdminStatus == OSPFV3_ENABLED)
    {
        CliPrintf (cliHandle, "Enabled\r\n");
    }
    else
    {
        CliPrintf (cliHandle, "Disabled\r\n");
    }

    CliPrintf (cliHandle, " OSPFv3 Hot Standby State        : ");
    if (i4HsState == OSPFV3_HA_STATE_INIT)
    {
        CliPrintf (cliHandle, "Init \r\n");
    }
    else if (i4HsState == OSPFV3_HA_STATE_ACTIVE_STANDBYUP)
    {
        CliPrintf (cliHandle, "Active - Standby Up\r\n");
    }
    else if (i4HsState == OSPFV3_HA_STATE_ACTIVE_STANDBYDOWN)
    {
        CliPrintf (cliHandle, "Active - Standby Down\r\n");
    }
    else if (i4HsState == OSPFV3_HA_STATE_STANDBY)
    {
        CliPrintf (cliHandle, "Standby\r\n");
    }

    CliPrintf (cliHandle, " OSPFv3 Hot Standby Dynamic Bulk Update Status : ");
    if (i4DynBulkUpdatStatus == OSPFV3_RED_DYN_BULK_NOT_STARTED)
    {
        CliPrintf (cliHandle, "Not Started\r\n");
    }
    else if (i4DynBulkUpdatStatus == OSPFV3_RED_DYN_BULK_IN_PROGRESS)
    {
        CliPrintf (cliHandle, "In Progress\r\n");
    }
    else if (i4DynBulkUpdatStatus == OSPFV3_RED_DYN_BULK_ABORTED)
    {
        CliPrintf (cliHandle, "Aborted\r\n");
    }
    else if (i4DynBulkUpdatStatus == OSPFV3_RED_DYN_BULK_COMPLETED)
    {
        CliPrintf (cliHandle, "Completed\r\n");
    }

    CliPrintf (cliHandle, " OSPFv3 Hot Standby Hello Sync count : %d\r\n",
               u4HelloSynCount);
    CliPrintf (cliHandle, " OSPFv3 Hot Standby LSA Sync count   : %d\r\n",
               u4LsaSyncCount);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : Ospfv3GetRouterCfgPrompt                               */
/*                                                                            */
/* Description       : This function validates the given pi1ModeName          */
/*                     and returns the prompt in pi1DispStr if valid,         */
/*                                                                            */
/* Input Parameters  : pi1ModeName - Mode Name to validate                    */
/*                     pi1DispStr - Display string to be returned.            */
/*                                                                            */
/* Output Parameters : pi1DispStr - Display string.                           */
/*                                                                            */
/* Return Value      : TRUE/FALSE                                             */
/******************************************************************************/

INT1
Ospfv3GetRouterCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;
    UINT4               u4Index;

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (CLI_MODE_ROUTER_OSPF3);
    if (STRNCMP (pi1ModeName, CLI_MODE_ROUTER_OSPF3, u4Len) != OSPFV3_INIT_VAL)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_CXT_ID (u4Index);

    STRCPY (pi1DispStr, "(config-router)#");
    return TRUE;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ospfv3ShowRunningConfigInCxt                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Ospfv3 configuration*/
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4Module - Specified Module (Ospfv3/all)           */
/*                        u4OspfCxtId - ospf3 context id                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Ospfv3ShowRunningConfigInCxt (tCliHandle CliHandle, UINT4 u4Module,
                              UINT4 u4ContextId)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4RetVal = OSPFV3_INIT_VAL;

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if ((pV3OspfCxt == NULL) ||
        ((IssGetModuleSystemControl (OSPF3_MODULE_ID) == MODULE_SHUTDOWN) &&
         (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS)))
    {
        return CLI_FAILURE;
    }
    CliRegisterLock (CliHandle, V3OspfLock, V3OspfUnLock);
    V3OspfLock ();

    if ((nmhGetFsMIStdOspfv3AdminStat (u4ContextId, &i4RetVal) == SNMP_FAILURE)
        || (V3UtilOspfGetCxt (u4ContextId) == NULL))
    {
        V3OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    if (i4RetVal == OSPFV3_ENABLED)
    {

        if (V3UtilSetContext (u4ContextId) == SNMP_FAILURE)
        {
            V3OspfUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }

        FilePrintf (CliHandle, "!\r\n");

        Ospfv3ShowRunningConfigScalars (CliHandle);

        Ospfv3ShowRunningConfigTables (CliHandle, u4ContextId);

        CliPrintf (CliHandle, "!\r\n");
        FilePrintf (CliHandle, "!\r\n");

        if (u4Module == ISS_OSPF3_SHOW_RUNNING_CONFIG)
        {
            Ospfv3ShowRunningConfigInterface (CliHandle);
        }

        V3UtilReSetContext ();
    }

    V3OspfUnLock ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ospfv3ShowRunningConfigScalars                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Ospfv3 configuration*/
/*                        for scalars objects                                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Ospfv3ShowRunningConfigScalars (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE HelperSupport;
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1OspfCxtName[OSPFV3_CXT_MAX_LEN];
    UINT1              *pu1String = NULL;
    UINT4               u4RetVal = OSPFV3_INIT_VAL;
    UINT4               u4RetVal2 = OSPFV3_INIT_VAL;
    INT4                i4RetVal = OSPFV3_INIT_VAL;
    INT4                i4Distance = OSPFV3_INIT_VAL;
    INT4                i4RouterIdStatus = 0;
    UINT1               u1HelperSupport = OSPFV3_INIT_VAL;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + OSPFV3_ONE];
    CHR1                ac1Redist[OSPFV3_CXT_MAX_LEN + OSPFV3_ONE];

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    HelperSupport.pu1_OctetList = &u1HelperSupport;
    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + OSPFV3_ONE);
    MEMSET (ac1Redist, 0, OSPFV3_CXT_MAX_LEN + OSPFV3_ONE);
    RouteMapName.pu1_OctetList = au1RMapName;

    nmhGetOspfv3AdminStat (&i4RetVal);
    if (i4RetVal == OSPFV3_ENABLED)
    {
        if (pV3OspfCxt->u4ContextId == OSPFV3_DEFAULT_CXT_ID)
        {
            CliPrintf (CliHandle, "ipv6 router ospf\r\n");
            FilePrintf (CliHandle, "ipv6 router ospf\r\n");
        }
        else
        {
            V3UtilGetCxtName (pV3OspfCxt->u4ContextId, au1OspfCxtName);
            CliPrintf (CliHandle, "ipv6 router ospf vrf %s \r\n",
                       au1OspfCxtName);
            FilePrintf (CliHandle, "ipv6 router ospf vrf %s \r\n",
                        au1OspfCxtName);
        }
    }

    nmhGetOspfv3RouterId (&u4RetVal);
    if (u4RetVal)
    {
        nmhGetFutOspfv3RouterIdPermanence (&i4RouterIdStatus);
        if (i4RouterIdStatus == OSPFV3_ROUTERID_PERMANENT)
        {
            OSPFV3_CLI_RTRID_TO_STR (pu1String, u4RetVal);
            CliPrintf (CliHandle, "router-id %s\r\n", pu1String);
            FilePrintf (CliHandle, "router-id %s\r\n", pu1String);
        }
    }

    nmhGetFutOspfv3SpfDelay (&i4RetVal);
    nmhGetFutOspfv3SpfHoldTime ((INT4 *) &u4RetVal2);

    if (i4RetVal != OSPFV3_DEF_SPF_INT || u4RetVal2 != OSPFV3_DEF_HOLD_TIME_INT)
    {
        CliPrintf (CliHandle, "timers spf %d %d\r\n", i4RetVal, u4RetVal2);
        FilePrintf (CliHandle, "timers spf %d %d\r\n", i4RetVal, u4RetVal2);
    }

    nmhGetFutOspfv3ABRType (&i4RetVal);
    if (i4RetVal == OSPFV3_CLI_CISCO_ABR)
    {
        CliPrintf (CliHandle, "abr-type cisco\r\n");
        FilePrintf (CliHandle, "abr-type cisco\r\n");
    }
    else if (i4RetVal == OSPFV3_CLI_IBM_ABR)
    {
        CliPrintf (CliHandle, "abr-type ibm\r\n");
        FilePrintf (CliHandle, "abr-type ibm\r\n");
    }

    nmhGetOspfv3ASBdrRtrStatus (&i4RetVal);
    if (i4RetVal == OSPFV3_ENABLED)
    {
        CliPrintf (CliHandle, "ASBR Router\r\n");
        FilePrintf (CliHandle, "ASBR Router\r\n");
    }

    nmhGetFutOspfv3DefaultPassiveInterface (&i4RetVal);
    if (i4RetVal == OSPFV3_ENABLED)
    {
        CliPrintf (CliHandle, "passive-interface\r\n");
        FilePrintf (CliHandle, "passive-interface\r\n");
    }

    nmhGetFutOspfv3NssaAsbrDefRtTrans (&i4RetVal);
    if (i4RetVal == OSPFV3_ENABLED)
    {
        CliPrintf (CliHandle, "nssaAsbrDfRtTrans\r\n");
        FilePrintf (CliHandle, "nssaAsbrDfRtTrans\r\n");
    }
    nmhGetOspfv3ExtAreaLsdbLimit (&i4RetVal);
    if (i4RetVal != OSPFV3_NO_LIMIT)
    {
        CliPrintf (CliHandle, "as-external lsdb-limit %d\r\n", i4RetVal);
        FilePrintf (CliHandle, "as-external lsdb-limit %d\r\n", i4RetVal);
    }

    nmhGetOspfv3ExitOverflowInterval (&u4RetVal);
    if (u4RetVal != OSPFV3_DONT_LEAVE_OVERFLOW_STATE)
    {
        CliPrintf (CliHandle, "exit-overflow-interval %d\r\n", u4RetVal);
        FilePrintf (CliHandle, "exit-overflow-interval %d\r\n", u4RetVal);
    }

    nmhGetOspfv3DemandExtensions (&i4RetVal);
    if (i4RetVal != OSPFV3_ENABLED)
    {
        CliPrintf (CliHandle, "no demand-extensions\r\n");
        FilePrintf (CliHandle, "no demand-extensions\r\n");
    }

    nmhGetOspfv3ReferenceBandwidth (&u4RetVal);
    if (u4RetVal != OSPFV3_DEF_REF_BW)
    {
        CliPrintf (CliHandle, "reference-bandwidth %u\r\n", u4RetVal);
        FilePrintf (CliHandle, "reference-bandwidth %u\r\n", u4RetVal);
    }

    i4RetVal = 0;
    nmhGetFutOspfv3RRDSrcProtoMask (&i4RetVal);
    nmhGetFutOspfv3RRDRouteMapName (&RouteMapName);

    RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = OSPFV3_ZERO;

    if (RouteMapName.i4_Length != 0)
    {
        SNPRINTF (ac1Redist, OSPFV3_CXT_MAX_LEN, "route-map %s",
                  RouteMapName.pu1_OctetList);
    }

    if (i4RetVal & RTM6_STATIC_MASK)
    {
        CliPrintf (CliHandle, "redistribute static %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute static %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_STATIC);
    }
    if (i4RetVal & RTM6_DIRECT_MASK)
    {
        CliPrintf (CliHandle, "redistribute connected %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute connected %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle,
                                           OSPFV3_REDISTRUTE_CONNECTED);
    }
    if (i4RetVal & RTM6_RIP_MASK)
    {
        CliPrintf (CliHandle, "redistribute ripng %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute ripng %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_RIP);
    }
    if (i4RetVal & RTM6_BGP_MASK)
    {
        CliPrintf (CliHandle, "redistribute bgp %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute bgp %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_BGP);
    }
    if ((i4RetVal & RTM6_ISISL1L2_MASK) == RTM6_ISISL1L2_MASK)
    {
        CliPrintf (CliHandle, "redistribute isis level-1-2 %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute isis level-1-2 %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_ISIS);
    }
    else if ((i4RetVal & RTM6_ISISL1L2_MASK) == RTM6_ISISL1_MASK)
    {
        CliPrintf (CliHandle, "redistribute isis level-1 %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute isis level-1 %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_ISIS);
    }
    else if ((i4RetVal & RTM6_ISISL1L2_MASK) == RTM6_ISISL2_MASK)
    {
        CliPrintf (CliHandle, "redistribute isis level-2 %s\r\n", ac1Redist);
        FilePrintf (CliHandle, "redistribute isis level-2 %s\r\n", ac1Redist);
        Ospfv3CliPrintMetricAndMetricType (CliHandle, OSPFV3_REDISTRUTE_ISIS);
    }

    nmhGetOspfv3AdminStat (&i4RetVal);
    if (OSPFV3_ENABLED == i4RetVal)
    {

        nmhGetFutOspfv3RTStaggeringStatus (&i4RetVal);
        if (OSPFV3_STAGGERING_DISABLED == i4RetVal)
        {
            CliPrintf (CliHandle, "no route-calculation staggering\r\n");
            FilePrintf (CliHandle, "no route-calculation staggering\r\n");
        }
        nmhGetFutOspfv3RTStaggeringInterval (&i4RetVal);
        if (i4RetVal != (OSPFV3_DEF_STAG_INTERVAL))
        {
            CliPrintf (CliHandle, "route-calculation staggering-interval "
                       "%d\r\n", i4RetVal);
            FilePrintf (CliHandle, "route-calculation staggering-interval "
                        "%d\r\n", i4RetVal);
        }
    }
    /* Restart support */
    if (nmhGetOspfv3RestartSupport (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal == OSPFV3_RESTART_PLANNED)
        {
            CliPrintf (CliHandle, "nsf ietf plannedOnly \r\n");
            FilePrintf (CliHandle, "nsf ietf plannedOnly \r\n");
        }
        else if (i4RetVal == OSPFV3_RESTART_BOTH)
        {
            CliPrintf (CliHandle, "nsf ietf \r\n");
            FilePrintf (CliHandle, "nsf ietf \r\n");
        }
    }
    /* Restart Interval */
    if (nmhGetOspfv3RestartInterval (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != OSPFV3_GR_DEF_INTERVAL)
        {
            CliPrintf (CliHandle, "nsf ietf restart-interval %d \r\n",
                       i4RetVal);
            FilePrintf (CliHandle, "nsf ietf restart-interval %d \r\n",
                        i4RetVal);
        }
    }
    /* Restarter Ack State */
    if (nmhGetFutOspfv3RestartAckState (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != OSPFV3_ENABLED)
        {
            CliPrintf (CliHandle, "no nsf ietf grace lsa ack required \r\n");
            FilePrintf (CliHandle, "no nsf ietf grace lsa ack required \r\n");
        }
    }
    /* Grace LSA Retransmission Count */
    if (nmhGetFutOspfv3GraceLsaRetransmitCount (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != OSPFV3_DEF_GRACE_LSA_SENT)
        {
            CliPrintf (CliHandle, "nsf ietf grace lsa retransmit-count %d \r\n",
                       i4RetVal);
            FilePrintf (CliHandle,
                        "nsf ietf grace lsa retransmit-count %d \r\n",
                        i4RetVal);
        }
    }
    /* Graceful Restart Reason */
    if (nmhGetFutOspfv3RestartReason (&i4RetVal) == SNMP_SUCCESS)
    {
        switch (i4RetVal)
        {
            case OSPFV3_GR_SW_RESTART:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason softwareRestart\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason softwareRestart\r\n");
                break;
            case OSPFV3_GR_SW_UPGRADE:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason swReloadUpgrade\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason swReloadUpgrade\r\n");
                break;
            case OSPFV3_GR_SW_RED:
                CliPrintf (CliHandle,
                           "nsf ietf restart-reason switchToRedundant\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf restart-reason switchToRedundant\r\n");
                break;
        }
    }

    /* Strict LSA checking */
    if (nmhGetFutOspfv3RestartStrictLsaChecking (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal == OSIX_TRUE)
        {
            CliPrintf (CliHandle, "nsf ietf helper strict-lsa-checking \r\n");
            FilePrintf (CliHandle, "nsf ietf helper strict-lsa-checking \r\n");
        }
    }
    /* Helper Grace Time Limit */
    if (nmhGetFutOspfv3HelperGraceTimeLimit (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != OSPFV3_INIT_VAL)
        {
            CliPrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                       i4RetVal);
            FilePrintf (CliHandle, "nsf ietf helper gracetimelimit %d \r\n",
                        i4RetVal);
        }
    }

    /* Helper support */
    if (nmhGetFutOspfv3HelperSupport (&HelperSupport) == SNMP_SUCCESS)
    {
        if (HelperSupport.i4_Length != 0)
        {
            if (!(HelperSupport.pu1_OctetList[0] & OSPFV3_GRACE_UNKNOWN))
            {
                CliPrintf (CliHandle, "nsf ietf helper disable unknown \r\n");
                FilePrintf (CliHandle, "nsf ietf helper disable unknown \r\n");
            }
            if (!(HelperSupport.pu1_OctetList[0] & OSPFV3_GRACE_SW_RESTART))
            {
                CliPrintf (CliHandle,
                           "nsf ietf helper disable softwareRestart \r\n");
                FilePrintf (CliHandle,
                            "nsf ietf helper disable softwareRestart \r\n");
            }
            if (!
                (HelperSupport.
                 pu1_OctetList[0] & OSPFV3_GRACE_SW_RELOADUPGRADE))
            {
                CliPrintf (CliHandle,
                           "nsf ietf helper disable swReloadUpgrade \r\n");
                FilePrintf (CliHandle,
                            "nsf ietf helper disable swReloadUpgrade \r\n");
            }
            if (!(HelperSupport.pu1_OctetList[0] & OSPFV3_GRACE_SW_REDUNDANT))
            {
                CliPrintf (CliHandle,
                           "nsf ietf helper disable switchToRedundant\r\n");
                FilePrintf (CliHandle,
                            "nsf ietf helper disable switchToRedundant\r\n");
            }
        }
    }

    /* Distance */
    if (nmhGetFutOspf3PreferenceValue (&i4Distance) == SNMP_SUCCESS)
    {
        if (i4Distance != IP6_PREFERENCE_OSPF)
        {
            CliPrintf (CliHandle, "distance %d\r\n", i4Distance);
            FilePrintf (CliHandle, "distance %d\r\n", i4Distance);
        }
    }

    if (nmhGetFutOspfv3BfdStatus (&i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal == OSPFV3_BFD_ENABLED)
        {
            CliPrintf (CliHandle, " enable bfd \r\n");
            FilePrintf (CliHandle, " enable bfd \r\n");
        }

        nmhGetFutOspfv3BfdAllIfState (&i4RetVal);
        if (i4RetVal == OSPFV3_BFD_ENABLED)
        {
            CliPrintf (CliHandle, " bfd all-interface \r\n");
            FilePrintf (CliHandle, " bfd all-interface \r\n");
        }
    }

    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : Ospfv3CliPrintMetricAndMetricType                   */
/*                                                                        */
/*  Description     : prints the matched metric value and its metric type */
/*                    for the given input                                 */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                    i4ProtoIndex - Index for the array corresponds      */
/*                    Protocol -                                          */
/*                                                                        */
/*  Output(s)       : None                                                */
/*                                                                        */
/*  Returns         : NONE                                                */
/**************************************************************************/
PRIVATE VOID
Ospfv3CliPrintMetricAndMetricType (tCliHandle CliHandle, INT4 i4ProtoIndex)
{
    INT4                i4ObjectValue = 0;

    if (i4ProtoIndex == 0)
    {
        return;
    }
    nmhGetFutOspfv3RRDMetricValue (i4ProtoIndex, &i4ObjectValue);
    if (i4ObjectValue != OSPFV3_AS_EXT_DEF_METRIC)
    {
        CliPrintf (CliHandle, "metric %d ", i4ObjectValue);
        FilePrintf (CliHandle, "metric %d ", i4ObjectValue);
    }
    nmhGetFutOspfv3RRDMetricType (i4ProtoIndex, &i4ObjectValue);
    if (i4ObjectValue != OSPFV3_TYPE_2_METRIC)
    {
        CliPrintf (CliHandle, "metric-type asExttype1");
        FilePrintf (CliHandle, "metric-type asExttype1");
    }

    CliPrintf (CliHandle, "\r\n");
    FilePrintf (CliHandle, "\r\n");
}

/******************************************************************************/
/* Function Name     : V3OspfIfAuthInfoInCxt                                  */
/*                                                                            */
/* Description       : This function prints the authentication key commands   */
/*                                                                            */
/* Input Parameters  : CliHandle        -  CliContext ID                      */
/*                     i4IfIndex        -  Interface Index                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfIfAuthInfoInCxt (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4IfIndex);
    if (pInterface == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        return CLI_SUCCESS;
    }

    switch (pInterface->u2CryptoAuthType)
    {
        case OSPFV3_AUTH_SHA1:

            CliPrintf (CliHandle, "ipv6 ospf authentication sha-1\r\n");
            FilePrintf (CliHandle, "ipv6 ospf authentication sha-1\r\n");
            break;
        case OSPFV3_AUTH_SHA2_256:

            CliPrintf (CliHandle, "ipv6 ospf authentication sha-256\r\n");
            FilePrintf (CliHandle, "ipv6 ospf authentication sha-256\r\n");
            break;
        case OSPFV3_AUTH_SHA2_384:

            CliPrintf (CliHandle, "ipv6 ospf authentication sha-384\r\n");
            FilePrintf (CliHandle, "ipv6 ospf authentication sha-384\r\n");
            break;
        case OSPFV3_AUTH_SHA2_512:

            CliPrintf (CliHandle, "ipv6 ospf authentication sha-512\r\n");
            FilePrintf (CliHandle, "ipv6 ospf authentication sha-512\r\n");
            break;
        case OSPFV3_AUTH_NO_TYPE:

            break;
        default:

            CliPrintf (CliHandle, "Unknown Cryptographic authentication \r\n");
            break;
    }

    if (pInterface->u1AuthMode != OSPFV3_AUTH_MODE_DEF)
    {
        CliPrintf (CliHandle, "ipv6 ospf authentication-mode transition\r\n");
        FilePrintf (CliHandle, "ipv6 ospf authentication-mode transition\r\n");
    }
    return CLI_SUCCESS;
}

VOID
V3PrintKeyData (tCliHandle CliHandle, tAuthkeyInfo * pAuthkeyInfo)
{
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN];

    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN);

    CliPrintf (CliHandle, "ipv6 ospf authentication-key %d \r\n",
               pAuthkeyInfo->u2AuthkeyId);

    FilePrintf (CliHandle, "ipv6 ospf authentication-key %d %s \r\n",
                pAuthkeyInfo->u2AuthkeyId, pAuthkeyInfo->au1AuthKey);

    if (pAuthkeyInfo->u4KeyStartGenerate > 0)
    {
        V3OspfPrintKeyTime (pAuthkeyInfo->u4KeyStartGenerate,
                            au1KeyConstantTime);
        CliPrintf (CliHandle, "ipv6 ospf key %d start-generate %s",
                   pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);

        FilePrintf (CliHandle, "ipv6 ospf key %d start-generate %s",
                    pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);
    }

    if (pAuthkeyInfo->u4KeyStartAccept > 0)
    {
        V3OspfPrintKeyTime (pAuthkeyInfo->u4KeyStartAccept, au1KeyConstantTime);
        CliPrintf (CliHandle, "ipv6 ospf key %d start-accept %s",
                   pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);

        FilePrintf (CliHandle, "ipv6 ospf key %d start-accept %s",
                    pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);
    }

    if (pAuthkeyInfo->u4KeyStopGenerate != OSPFV3_MAX_TIME)
    {
        V3OspfPrintKeyTime (pAuthkeyInfo->u4KeyStopGenerate,
                            au1KeyConstantTime);
        CliPrintf (CliHandle, "ipv6 ospf key %d stop-generate %s",
                   pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);

        FilePrintf (CliHandle,
                    "ipv6 ospf key %d stop-generate %s",
                    pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);
    }

    if (pAuthkeyInfo->u4KeyStopAccept != OSPFV3_MAX_TIME)
    {
        V3OspfPrintKeyTime (pAuthkeyInfo->u4KeyStopAccept, au1KeyConstantTime);
        CliPrintf (CliHandle, "ipv6 ospf key %d stop-accept %s",
                   pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);

        FilePrintf (CliHandle, "ipv6 ospf key %d stop-accept %s",
                    pAuthkeyInfo->u2AuthkeyId, au1KeyConstantTime);
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ospfv3ShowRunningConfigTables                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays the current Ospfv3          */
/*                        configuration for tables                           */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
Ospfv3ShowRunningConfigTables (tCliHandle CliHandle, UINT4 u4Ospfv3AuthCxtId)
{
    tIp6Addr            ip6Addr;
    UINT4               u4NextRetVal = OSPFV3_INIT_VAL;
    UINT4               u4CurrRetVal = OSPFV3_INIT_VAL;
    UINT4               u4NextRetVal2 = OSPFV3_INIT_VAL;
    UINT4               u4CurrRetVal2 = OSPFV3_INIT_VAL;
    UINT4               u4RetVal = OSPFV3_INIT_VAL;
    UINT4               u4RetValType = OSPFV3_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT1              *pu1String = NULL;
    UINT1               u1isShowAll = TRUE;
    INT4                i4NextRetVal2 = OSPFV3_INIT_VAL;
    INT4                i4CurrRetVal2 = OSPFV3_INIT_VAL;
    INT4                i4NextRetVal3 = OSPFV3_INIT_VAL;
    INT4                i4CurrRetVal3 = OSPFV3_INIT_VAL;
    INT4                i4NextRetVal4 = OSPFV3_INIT_VAL;
    INT4                i4CurrRetVal4 = OSPFV3_INIT_VAL;
    INT4                i4RetVal = OSPFV3_INIT_VAL;
#ifdef ROUTEMAP_WANTED
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4Value = OSPFV3_INIT_VAL;
    INT4                i4FilterType = OSPFV3_INIT_VAL;
    INT4                i4NextFilterType = OSPFV3_INIT_VAL;
#endif

    INT1                i1AreaStatus = OSPFV3_INIT_VAL;
    INT1                i1OutCome = OSPFV3_INIT_VAL;
    UINT4               u4Ospfv3NextCxtId = OSPFV3_INIT_VAL;
    INT4                i4CryptoAuthType = -1;
    INT4                i4AuthKeyId = -1;
    INT4                i4NextAuthKeyId = -1;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN];
    INT4                i4NextHopDestType = 0;
    INT4                i4RouteNextHopType = 0;

    tIp6Addr            ip6NexthopAddr;
    tIp6Addr            ip6DefaultAddr;
    tSNMP_OCTET_STRING_TYPE RouteNextHop;
    tSNMP_OCTET_STRING_TYPE NextHop;
    tSNMP_OCTET_STRING_TYPE ExtDefRoute;

    tSNMP_OCTET_STRING_TYPE ospfSANet;
    tSNMP_OCTET_STRING_TYPE prevOspfSANet;

    tV3OsRouterId       nbrId;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pOspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;
    tAuthkeyInfo       *pAuthkeyInfo;

    ospfSANet.pu1_OctetList = (UINT1 *) &ip6Addr;
    NextHop.pu1_OctetList = (UINT1 *) &ip6NexthopAddr;
    MEMSET (&RouteNextHop, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    if (nmhGetFirstIndexOspfv3AreaTable (&u4NextRetVal) == SNMP_SUCCESS)
    {
        do
        {
            i1AreaStatus =
                nmhGetOspfv3AreaStatus (u4NextRetVal, (INT4 *) &u4RetVal);

            if ((i1AreaStatus != SNMP_FAILURE)
                && (u4RetVal == OSPFV3_CLI_STATUS_ACTIVE))
            {
                OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);

                nmhGetOspfv3ImportAsExtern (u4NextRetVal, (INT4 *) &u4RetVal);
                if (u4RetVal != OSPFV3_NORMAL_AREA)
                {
                    CliPrintf (CliHandle, "area %s", pu1String);
                    FilePrintf (CliHandle, "area %s", pu1String);
                    if (u4RetVal == OSPFV3_STUB_AREA)
                    {
                        CliPrintf (CliHandle, " stub");
                        FilePrintf (CliHandle, " stub");
                    }
                    else if (u4RetVal == OSPFV3_NSSA_AREA)
                    {
                        CliPrintf (CliHandle, " nssa");
                        FilePrintf (CliHandle, " nssa");
                    }
                    nmhGetOspfv3AreaSummary (u4NextRetVal, (INT4 *) &u4RetVal);

                    if (u4RetVal != OSPFV3_SEND_AREA_SUMMARY)
                    {
                        i4PagingStatus =
                            CliPrintf (CliHandle, " no-summary\r\n");
                        FilePrintf (CliHandle, " no-summary\r\n");
                    }
                    nmhGetOspfv3AreaDfInfOriginate (u4NextRetVal,
                                                    (INT4 *) &u4RetVal);
                    if (u4RetVal != OSPFV3_NO_DEFAULT_INFO_ORIGINATE)
                    {

                        CliPrintf (CliHandle,
                                   " default-information-originate ");
                        FilePrintf (CliHandle,
                                    " default-information-originate ");

                        nmhGetOspfv3StubMetric (u4NextRetVal,
                                                (INT4 *) &u4RetVal);
                        if (u4RetVal != OSPFV3_AS_EXT_DEF_METRIC)
                        {
                            CliPrintf (CliHandle, "metric %d ", u4RetVal);
                            FilePrintf (CliHandle, "metric %d ", u4RetVal);
                        }
                        nmhGetOspfv3AreaStubMetricType (u4NextRetVal,
                                                        (INT4 *) &u4RetVal);
                        if ((u4RetVal != OSPFV3_TYPE1EXT_METRIC)
                            && (u4RetVal != OSPFV3_METRIC))
                        {
                            CliPrintf (CliHandle, "metric-type %d", u4RetVal);
                            FilePrintf (CliHandle, "metric-type %d", u4RetVal);
                        }
                        CliPrintf (CliHandle, "\r\n");
                        FilePrintf (CliHandle, "\r\n");
                    }

                    else
                    {
                        i4PagingStatus = CliPrintf (CliHandle, "\r\n");
                        FilePrintf (CliHandle, "\r\n");
                    }
                }
            }

            nmhGetOspfv3AreaNssaTranslatorRole (u4NextRetVal,
                                                (INT4 *) &u4RetVal);
            if (u4RetVal == OSPFV3_CLI_NSSA_TRANS_ALWAYS)
            {
                CliPrintf (CliHandle, "area %s translation-role", pu1String);
                FilePrintf (CliHandle, "area %s translation-role", pu1String);
                i4PagingStatus = CliPrintf (CliHandle, " always\r\n");
                FilePrintf (CliHandle, " always\r\n");
            }

            nmhGetOspfv3StubMetric (u4NextRetVal, (INT4 *) &u4RetVal);
            if (u4RetVal != OSPFV3_METRIC)
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "area %s default-metric %d\r\n",
                                            pu1String, u4RetVal);
                FilePrintf (CliHandle, "area %s default-metric %d\r\n",
                            pu1String, u4RetVal);
            }
            nmhGetOspfv3AreaStubMetricType (u4NextRetVal, (INT4 *) &u4RetVal);
            nmhGetOspfv3ImportAsExtern (u4NextRetVal, (INT4 *) &u4RetValType);
            if ((u4RetValType == OSPFV3_NSSA_AREA)
                && (u4RetVal != OSPFV3_TYPE1EXT_METRIC)
                && (u4RetVal != OSPFV3_METRIC))
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "area %s default-metric type %d\r\n",
                                            pu1String, u4RetVal);
                FilePrintf (CliHandle, "area %s default-metric type %d\r\n",
                            pu1String, u4RetVal);
            }
            if ((u4RetValType != OSPFV3_NSSA_AREA)
                && (u4RetVal != OSPFV3_METRIC))
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "area %s default-metric type %d\r\n",
                                            pu1String, u4RetVal);
                FilePrintf (CliHandle, "area %s default-metric type %d\r\n",
                            pu1String, u4RetVal);
            }
            nmhGetOspfv3AreaNssaTranslatorStabilityInterval (u4NextRetVal,
                                                             &u4RetVal);
            if (u4RetVal != OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL)
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "area %s stability-interval %u\r\n",
                                            pu1String, u4RetVal);
                FilePrintf (CliHandle, "area %s stability-interval %u\r\n",
                            pu1String, u4RetVal);
            }
            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            u4CurrRetVal = u4NextRetVal;

            if (nmhGetNextIndexOspfv3AreaTable (u4CurrRetVal, &u4NextRetVal)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    u1isShowAll = TRUE;

    if (nmhGetFirstIndexOspfv3AreaAggregateTable
        (&u4NextRetVal, &i4NextRetVal2, &i4NextRetVal3, &ospfSANet,
         (UINT4 *) &i4NextRetVal4) == SNMP_SUCCESS)
    {
        do
        {
            OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
            CliPrintf (CliHandle, "area %s range %s", pu1String,
                       Ip6PrintAddr (&ip6Addr));
            FilePrintf (CliHandle, "area %s range %s", pu1String,
                        Ip6PrintAddr (&ip6Addr));
            CliPrintf (CliHandle, " %d", i4NextRetVal4);
            FilePrintf (CliHandle, " %d", i4NextRetVal4);
            i4RetVal = 0;
            nmhGetOspfv3AreaAggregateEffect (u4NextRetVal, i4NextRetVal2,
                                             i4NextRetVal3, &ospfSANet,
                                             i4NextRetVal4, &i4RetVal);

            if (i4RetVal != OSPFV3_ADVERTISE_MATCHING)
            {
                CliPrintf (CliHandle, " not-advertise");
                FilePrintf (CliHandle, " not-advertise");
            }

            switch (i4NextRetVal2)
            {
                case OSPFV3_INTER_AREA_PREFIX_LSA:
                    CliPrintf (CliHandle, " summary");
                    FilePrintf (CliHandle, " summary");
                    break;
                case OSPFV3_NSSA_LSA:
                    CliPrintf (CliHandle, " Type7");
                    FilePrintf (CliHandle, " Type7");
                    break;
            }

            i4RetVal = OSPFV3_INIT_VAL;
            nmhGetOspfv3AreaAggregateRouteTag (u4NextRetVal, i4NextRetVal2,
                                               i4NextRetVal3, &ospfSANet,
                                               i4NextRetVal4, &i4RetVal);
            if (i4RetVal != OSPFV3_INIT_VAL)
            {
                CliPrintf (CliHandle, " tag %d", i4RetVal);
                FilePrintf (CliHandle, " tag %d", i4RetVal);
            }

            i4PagingStatus = CliPrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");

            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;
            prevOspfSANet.i4_Length = ospfSANet.i4_Length;
            u4CurrRetVal = u4NextRetVal;
            i4CurrRetVal2 = i4NextRetVal2;
            i4CurrRetVal3 = i4NextRetVal3;
            i4CurrRetVal4 = i4NextRetVal4;

            if (nmhGetNextIndexOspfv3AreaAggregateTable (u4CurrRetVal,
                                                         &u4NextRetVal,
                                                         i4CurrRetVal2,
                                                         &i4NextRetVal2,
                                                         i4CurrRetVal3,
                                                         &i4NextRetVal3,
                                                         &prevOspfSANet,
                                                         &ospfSANet,
                                                         i4CurrRetVal4,
                                                         (UINT4 *)
                                                         &i4NextRetVal4) ==
                SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    u1isShowAll = TRUE;

    if (nmhGetFirstIndexFutOspfv3AsExternalAggregationTable (&i4NextRetVal2,
                                                             &ospfSANet,
                                                             (UINT4 *)
                                                             &i4NextRetVal3,
                                                             &u4NextRetVal)
        == SNMP_SUCCESS)
    {
        do
        {
            OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
            CliPrintf (CliHandle, "area %s summary-prefix %s",
                       pu1String, Ip6PrintAddr (&ip6Addr));
            FilePrintf (CliHandle, "area %s summary-prefix %s",
                        pu1String, Ip6PrintAddr (&ip6Addr));
            CliPrintf (CliHandle, " %d", i4NextRetVal3);
            FilePrintf (CliHandle, " %d", i4NextRetVal3);
            i4RetVal = OSPFV3_INIT_VAL;
            nmhGetFutOspfv3AsExternalAggregationEffect (i4NextRetVal2,
                                                        &ospfSANet,
                                                        i4NextRetVal3,
                                                        u4NextRetVal,
                                                        &i4RetVal);

            if (((V3UtilAreaIdComp
                  ((UINT1 *) (&u4NextRetVal),
                   OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)
                 && (i4RetVal != OSPFV3_RAG_ADVERTISE))
                ||
                ((V3UtilAreaIdComp
                  ((UINT1 *) (&u4NextRetVal),
                   OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
                 && (i4RetVal != OSPFV3_RAG_ALLOW_ALL)))
            {
                switch (i4RetVal)
                {
                    case OSPFV3_RAG_ADVERTISE:
                        CliPrintf (CliHandle, " advertise");
                        FilePrintf (CliHandle, " advertise");
                        break;
                    case OSPFV3_RAG_DO_NOT_ADVERTISE:
                        CliPrintf (CliHandle, " not-advertise");
                        FilePrintf (CliHandle, " not-advertise");
                        break;
                    case OSPFV3_RAG_ALLOW_ALL:
                        CliPrintf (CliHandle, " allowAll");
                        FilePrintf (CliHandle, " allowAll");
                        break;
                    case OSPFV3_RAG_DENY_ALL:
                        CliPrintf (CliHandle, " denyAll");
                        FilePrintf (CliHandle, " denyAll");
                        break;
                }
            }
            i4RetVal = OSPFV3_INIT_VAL;
            nmhGetFutOspfv3AsExternalAggregationTranslation (i4NextRetVal2,
                                                             &ospfSANet,
                                                             i4NextRetVal3,
                                                             u4NextRetVal,
                                                             &i4RetVal);
            if (i4RetVal != OSPFV3_ENABLED)
            {
                CliPrintf (CliHandle, " Translation disabled");
                FilePrintf (CliHandle, " Translation disabled");
            }

            i4PagingStatus = CliPrintf (CliHandle, "\r\n");
            FilePrintf (CliHandle, "\r\n");

            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;
            i4CurrRetVal2 = i4NextRetVal2;
            i4CurrRetVal3 = i4NextRetVal3;
            u4CurrRetVal = u4NextRetVal;

            if (nmhGetNextIndexFutOspfv3AsExternalAggregationTable
                (i4CurrRetVal2, &i4NextRetVal2, &prevOspfSANet, &ospfSANet,
                 (UINT4) i4CurrRetVal3, (UINT4 *) &i4NextRetVal3,
                 u4CurrRetVal, &u4NextRetVal) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    u1isShowAll = TRUE;

    if (nmhGetFirstIndexOspfv3HostTable (&i4NextRetVal2, &ospfSANet)
        == SNMP_SUCCESS)
    {
        do
        {
            nmhGetOspfv3HostStatus (i4NextRetVal2, &ospfSANet,
                                    (INT4 *) &u4RetVal);
            if (u4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
            {
                CliPrintf (CliHandle, "host %s", Ip6PrintAddr (&ip6Addr));
                FilePrintf (CliHandle, "host %s", Ip6PrintAddr (&ip6Addr));
                nmhGetOspfv3HostMetric (i4NextRetVal2, &ospfSANet,
                                        (INT4 *) &u4RetVal);
                CliPrintf (CliHandle, " metric %d", u4RetVal);
                FilePrintf (CliHandle, " metric %d", u4RetVal);

                nmhGetOspfv3HostAreaID (i4NextRetVal2, &ospfSANet, &u4RetVal);
                OSPFV3_CLI_RTRID_TO_STR (pu1String, u4RetVal);
                i4PagingStatus =
                    CliPrintf (CliHandle, " area-id %s\r\n", pu1String);
                FilePrintf (CliHandle, " area-id %s\r\n", pu1String);
            }
            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            i4CurrRetVal2 = i4NextRetVal2;
            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;

            if (nmhGetNextIndexOspfv3HostTable
                (i4CurrRetVal2, &i4NextRetVal2, &prevOspfSANet,
                 &ospfSANet) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    u1isShowAll = TRUE;

    if (nmhGetFirstIndexFutOspfv3RedistRouteCfgTable
        (&i4NextRetVal2, &ospfSANet, (UINT4 *) &i4NextRetVal3) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFutOspfv3RedistRouteStatus (i4NextRetVal2, &ospfSANet,
                                              i4NextRetVal3, &i4RetVal);
            if (i4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
            {
                CliPrintf (CliHandle, "redist-config %s",
                           Ip6PrintAddr (&ip6Addr));
                FilePrintf (CliHandle, "redist-config %s",
                            Ip6PrintAddr (&ip6Addr));
                CliPrintf (CliHandle, " %d", i4NextRetVal3);
                FilePrintf (CliHandle, " %d", i4NextRetVal3);

                nmhGetFutOspfv3RedistRouteMetric (i4NextRetVal2, &ospfSANet,
                                                  i4NextRetVal3, &i4RetVal);
                if (i4RetVal != OSPFV3_AS_EXT_DEF_METRIC)
                {
                    CliPrintf (CliHandle, " metric-value %d", i4RetVal);
                    FilePrintf (CliHandle, " metric-value %d", i4RetVal);
                }

                nmhGetFutOspfv3RedistRouteMetricType (i4NextRetVal2,
                                                      &ospfSANet,
                                                      i4NextRetVal3, &i4RetVal);

                if (i4RetVal != OSPFV3_TYPE_2_METRIC)
                {
                    CliPrintf (CliHandle, " metric-type asExttype1");
                    FilePrintf (CliHandle, " metric-type asExttype1");
                }

                nmhGetFutOspfv3RedistRouteTag (i4NextRetVal2, &ospfSANet,
                                               i4NextRetVal3, &i4RetVal);

                if (i4RetVal != OSPFV3_INIT_VAL)
                {
                    CliPrintf (CliHandle, " tag %d", i4RetVal);
                    FilePrintf (CliHandle, " tag %d", i4RetVal);
                }

                i4PagingStatus = CliPrintf (CliHandle, "\r\n");
                FilePrintf (CliHandle, "\r\n");

            }
            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            i4CurrRetVal2 = i4NextRetVal2;
            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;
            i4CurrRetVal3 = i4NextRetVal3;

            if (nmhGetNextIndexFutOspfv3RedistRouteCfgTable (i4CurrRetVal2,
                                                             &i4NextRetVal2,
                                                             &prevOspfSANet,
                                                             &ospfSANet,
                                                             i4CurrRetVal3,
                                                             (UINT4 *)
                                                             &i4NextRetVal3)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }
    MEMSET (&ip6DefaultAddr, 0, sizeof (tIp6Addr));
    OSPFV3_IP6_ADDR_COPY (ip6DefaultAddr, gV3OsNullIp6Addr);

    ExtDefRoute.pu1_OctetList = (UINT1 *) &ip6DefaultAddr;
    ExtDefRoute.i4_Length = OSPFV3_IPV6_ADDR_LEN;

/* displaying default-information configurations*/

    if (nmhGetFirstIndexFutOspfv3ExtRouteTable
        (&i4NextRetVal2, &ospfSANet,
         (UINT4 *) &i4NextRetVal3,
         &i4RouteNextHopType, &NextHop) == SNMP_SUCCESS)
    {
        do
        {
            nmhGetFutOspfv3ExtRouteStatus (i4NextRetVal2, &ospfSANet,
                                           i4NextRetVal3, i4RouteNextHopType,
                                           &NextHop, &i4RetVal);

            if ((i4RetVal == ACTIVE) && (i4NextRetVal2 == OSPFV3_INET_ADDR_TYPE)
                && (i4RouteNextHopType == OSPFV3_INET_ADDR_TYPE)
                && (i4NextRetVal3 == 0)
                &&
                (MEMCMP
                 (ospfSANet.pu1_OctetList, ExtDefRoute.pu1_OctetList,
                  OSPFV3_IPV6_ADDR_LEN) == 0)
                &&
                (MEMCMP
                 (NextHop.pu1_OctetList, ExtDefRoute.pu1_OctetList,
                  OSPFV3_IPV6_ADDR_LEN) == 0))

            {
                CliPrintf (CliHandle, "default-information originate always ");
                FilePrintf (CliHandle, "default-information originate always ");
                /*Route metric */
                nmhGetFutOspfv3ExtRouteMetric (i4NextRetVal2, &ospfSANet,
                                               i4NextRetVal3,
                                               i4RouteNextHopType, &NextHop,
                                               &i4RetVal);
                if (i4RetVal != OSPFV3_CLI_AS_EXT_DEF_METRIC)
                {
                    CliPrintf (CliHandle, "metric %d ", i4RetVal);
                    FilePrintf (CliHandle, "metric %d ", i4RetVal);
                }
                /*Route metric type */
                nmhGetFutOspfv3ExtRouteMetricType (i4NextRetVal2, &ospfSANet,
                                                   i4NextRetVal3,
                                                   i4RouteNextHopType, &NextHop,
                                                   &i4RetVal);
                if (i4RetVal != OSPFV3_TYPE_2_METRIC)
                {
                    CliPrintf (CliHandle, "metric-type 1\r\n");
                    FilePrintf (CliHandle, "metric-type 1\r\n");
                }
                i4PagingStatus = CliPrintf (CliHandle, "\r\n");
                FilePrintf (CliHandle, "\r\n");

            }
            if (i4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            i4CurrRetVal2 = i4NextRetVal2;
            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;
            i4CurrRetVal3 = i4NextRetVal3;
            i4NextHopDestType = i4RouteNextHopType;
            NextHop.pu1_OctetList = RouteNextHop.pu1_OctetList;

            if (nmhGetNextIndexFutOspfv3ExtRouteTable (i4CurrRetVal2,
                                                       &i4NextRetVal2,
                                                       &prevOspfSANet,
                                                       &ospfSANet,
                                                       (UINT4)
                                                       i4CurrRetVal3,
                                                       (UINT4 *)
                                                       &i4NextRetVal3,
                                                       i4NextHopDestType,
                                                       &i4RouteNextHopType,
                                                       &NextHop,
                                                       &RouteNextHop)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }
    u1isShowAll = TRUE;

    if (nmhGetFirstIndexOspfv3VirtIfTable (&u4NextRetVal, &u4NextRetVal2)
        == SNMP_SUCCESS)
    {
        i1OutCome =
            nmhGetNextIndexFsMIOspfv3VirtIfAuthTable ((INT4) u4Ospfv3AuthCxtId,
                                                      (INT4 *)
                                                      &u4Ospfv3NextCxtId,
                                                      u4NextRetVal, &u4RetVal,
                                                      u4NextRetVal2, &u4RetVal,
                                                      (-1), &i4NextAuthKeyId);
        do
        {
            /* Reseting the context to back */
            if (V3UtilSetContext (u4Ospfv3AuthCxtId) == SNMP_FAILURE)
            {
                V3OspfUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            nmhGetOspfv3VirtIfStatus (u4NextRetVal, u4NextRetVal2, &i4RetVal);
            if (i4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
            {
                OSPFV3_BUFFER_DWTOPDU (areaId, u4NextRetVal);
                OSPFV3_BUFFER_DWTOPDU (nbrId, u4NextRetVal2);

                pOspfCxt = V3UtilOspfGetCxt (u4Ospfv3AuthCxtId);
                if (pOspfCxt == NULL)
                {
                    return CLI_FAILURE;
                }

                pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
                if (pInterface == NULL)
                {
                    return CLI_FAILURE;
                }

                OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                CliPrintf (CliHandle, "area %s", pu1String);
                FilePrintf (CliHandle, "area %s", pu1String);
                OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                CliPrintf (CliHandle, " virtual-link %s", pu1String);
                FilePrintf (CliHandle, " virtual-link %s", pu1String);

                nmhGetFsMIStdOspfv3VirtIfIndex ((INT4) u4Ospfv3AuthCxtId,
                                                u4NextRetVal, u4NextRetVal2,
                                                &i4RetVal);
                CliPrintf (CliHandle, " %d", i4RetVal);
                FilePrintf (CliHandle, " %d", i4RetVal);
                nmhGetFsMIStdOspfv3VirtIfHelloInterval ((INT4)
                                                        u4Ospfv3AuthCxtId,
                                                        u4NextRetVal,
                                                        u4NextRetVal2,
                                                        &i4RetVal);
                if (i4RetVal != OSPFV3_DEF_HELLO_INTERVAL)
                {
                    CliPrintf (CliHandle, " hello-interval %d", i4RetVal);
                    FilePrintf (CliHandle, " hello-interval %d", i4RetVal);
                }
                nmhGetFsMIStdOspfv3VirtIfRetransInterval ((INT4)
                                                          u4Ospfv3AuthCxtId,
                                                          u4NextRetVal,
                                                          u4NextRetVal2,
                                                          &i4RetVal);
                if (i4RetVal != OSPFV3_DEF_RXMT_INTERVAL)
                {
                    CliPrintf (CliHandle, " retransmit-interval %d", i4RetVal);
                    FilePrintf (CliHandle, " retransmit-interval %d", i4RetVal);
                }
                nmhGetFsMIStdOspfv3VirtIfTransitDelay ((INT4) u4Ospfv3AuthCxtId,
                                                       u4NextRetVal,
                                                       u4NextRetVal2,
                                                       &i4RetVal);
                if (i4RetVal != OSPFV3_DEF_IF_TRANS_DELAY)
                {
                    CliPrintf (CliHandle, " transmit-delay %d", i4RetVal);
                    FilePrintf (CliHandle, " transmit-delay %d", i4RetVal);
                }
                nmhGetFsMIStdOspfv3VirtIfRtrDeadInterval ((INT4)
                                                          u4Ospfv3AuthCxtId,
                                                          u4NextRetVal,
                                                          u4NextRetVal2,
                                                          &i4RetVal);
                if (i4RetVal != OSPFV3_DEF_VIRT_IF_RTR_DEAD_INTERVAL)
                {
                    CliPrintf (CliHandle, " dead-interval %d", i4RetVal);
                    FilePrintf (CliHandle, " dead-interval %d", i4RetVal);
                }

                if (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH)
                {
                    nmhGetFsMIOspfv3VirtIfCryptoAuthType ((INT4)
                                                          u4Ospfv3AuthCxtId,
                                                          u4NextRetVal,
                                                          u4NextRetVal2,
                                                          &i4CryptoAuthType);
                    if (i4CryptoAuthType != OSPFV3_DEF_AUTH_TYPE)
                    {
                        switch (i4CryptoAuthType)
                        {
                            case OSPFV3_AUTH_SHA1:
                                CliPrintf (CliHandle, " authentication sha-1");
                                FilePrintf (CliHandle, " authentication sha-1");
                                break;

                            case OSPFV3_AUTH_SHA2_256:
                                CliPrintf (CliHandle, " authentication");
                                FilePrintf (CliHandle, " authentication");
                                break;

                            case OSPFV3_AUTH_SHA2_384:
                                CliPrintf (CliHandle,
                                           " authentication sha-384");
                                FilePrintf (CliHandle,
                                            " authentication sha-384");
                                break;

                            case OSPFV3_AUTH_SHA2_512:
                                CliPrintf (CliHandle,
                                           " authentication sha-512");
                                FilePrintf (CliHandle,
                                            " authentication sha-512");
                                break;

                            default:
                                break;
                        }
                    }
                }

                CliPrintf (CliHandle, "\r\n");
                FilePrintf (CliHandle, "\r\n");

                if (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH)
                {
                    /* In Case of SHA Authentication Type the key value is stored in
                     * the object  futOspfv3VirtIfAuthKey .This information however
                     * will not be displayed for security reasons*/
                    if (pInterface->u1AuthMode == OSPFV3_AUTH_MODE_TRANS)
                    {
                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                        CliPrintf (CliHandle, "area %s", pu1String);
                        FilePrintf (CliHandle, "area %s", pu1String);
                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                        CliPrintf (CliHandle, " virtual-link %s", pu1String);
                        FilePrintf (CliHandle, " virtual-link %s", pu1String);

                        nmhGetFsMIStdOspfv3VirtIfIndex ((INT4)
                                                        u4Ospfv3AuthCxtId,
                                                        u4NextRetVal,
                                                        u4NextRetVal2,
                                                        &i4RetVal);

                        CliPrintf (CliHandle, " %d", i4RetVal);
                        FilePrintf (CliHandle, " %d", i4RetVal);
                        CliPrintf (CliHandle,
                                   " authentication-mode transition\r\n");
                        FilePrintf (CliHandle,
                                    " authentication-mode transition\r\n");
                    }
                }
            }

            do
            {
                pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                                                &areaId, &nbrId,
                                                                (UINT2)
                                                                i4NextAuthKeyId);

                if (pAuthkeyInfo != NULL)
                {
                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                    CliPrintf (CliHandle, "area %s", pu1String);
                    FilePrintf (CliHandle, "area %s", pu1String);
                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                    CliPrintf (CliHandle, " virtual-link %s", pu1String);
                    FilePrintf (CliHandle, " virtual-link %s", pu1String);

                    nmhGetFsMIStdOspfv3VirtIfIndex ((INT4) u4Ospfv3AuthCxtId,
                                                    u4NextRetVal, u4NextRetVal2,
                                                    &i4RetVal);
                    CliPrintf (CliHandle, " %d", i4RetVal);
                    FilePrintf (CliHandle, " %d", i4RetVal);

                    CliPrintf (CliHandle, " authentication-key %d\r\n",
                               i4NextAuthKeyId);
                    FilePrintf (CliHandle, " authentication-key %d %s\r\n",
                                i4NextAuthKeyId, pAuthkeyInfo->au1AuthKey);

                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                    CliPrintf (CliHandle, "area %s", pu1String);
                    FilePrintf (CliHandle, "area %s", pu1String);

                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                    CliPrintf (CliHandle, " virtual-link %s", pu1String);
                    FilePrintf (CliHandle, " virtual-link %s", pu1String);

                    CliPrintf (CliHandle, " key %d", i4NextAuthKeyId);
                    FilePrintf (CliHandle, " key %d", i4NextAuthKeyId);

                    V3OspfPrintKeyTime ((UINT4) pAuthkeyInfo->
                                        u4KeyStartGenerate, au1KeyConstantTime);
                    CliPrintf (CliHandle, " start-generate %s",
                               au1KeyConstantTime);
                    FilePrintf (CliHandle, " start-generate %s",
                                au1KeyConstantTime);

                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                    CliPrintf (CliHandle, "area %s", pu1String);
                    FilePrintf (CliHandle, "area %s", pu1String);

                    OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                    CliPrintf (CliHandle, " virtual-link %s", pu1String);
                    FilePrintf (CliHandle, " virtual-link %s", pu1String);

                    CliPrintf (CliHandle, " key %d", i4NextAuthKeyId);
                    FilePrintf (CliHandle, " key %d", i4NextAuthKeyId);

                    V3OspfPrintKeyTime (pAuthkeyInfo->u4KeyStartAccept,
                                        au1KeyConstantTime);
                    CliPrintf (CliHandle, " start-accept %s",
                               au1KeyConstantTime);
                    FilePrintf (CliHandle, " start-accept %s",
                                au1KeyConstantTime);

                    if (pAuthkeyInfo->u4KeyStopGenerate != OSPFV3_MAX_TIME)
                    {
                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                        CliPrintf (CliHandle, "area %s", pu1String);
                        FilePrintf (CliHandle, "area %s", pu1String);

                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                        CliPrintf (CliHandle, " virtual-link %s", pu1String);
                        FilePrintf (CliHandle, " virtual-link %s", pu1String);

                        CliPrintf (CliHandle, " key %d", i4NextAuthKeyId);
                        FilePrintf (CliHandle, " key %d", i4NextAuthKeyId);

                        V3OspfPrintKeyTime ((UINT4) pAuthkeyInfo->
                                            u4KeyStopGenerate,
                                            au1KeyConstantTime);
                        CliPrintf (CliHandle, " stop-generate %s",
                                   au1KeyConstantTime);
                        FilePrintf (CliHandle, " stop-generate %s",
                                    au1KeyConstantTime);
                    }

                    if (pAuthkeyInfo->u4KeyStopAccept != OSPFV3_MAX_TIME)
                    {
                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal);
                        CliPrintf (CliHandle, "area %s", pu1String);
                        FilePrintf (CliHandle, "area %s", pu1String);

                        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4NextRetVal2);
                        CliPrintf (CliHandle, " virtual-link %s", pu1String);
                        FilePrintf (CliHandle, " virtual-link %s", pu1String);

                        CliPrintf (CliHandle, " key %d", i4NextAuthKeyId);
                        FilePrintf (CliHandle, " key %d", i4NextAuthKeyId);

                        V3OspfPrintKeyTime ((UINT4) pAuthkeyInfo->
                                            u4KeyStopAccept,
                                            au1KeyConstantTime);
                        CliPrintf (CliHandle, " stop-accept %s",
                                   au1KeyConstantTime);
                        FilePrintf (CliHandle, " stop-accept %s",
                                    au1KeyConstantTime);
                    }
                }
                i4PagingStatus = CliPrintf (CliHandle, "\r\n", i4RetVal);
                FilePrintf (CliHandle, "\r\n", i4RetVal);
                if (i4PagingStatus == CLI_FAILURE)
                {
                    u1isShowAll = FALSE;
                }

                u4CurrRetVal = u4NextRetVal;
                u4CurrRetVal2 = u4NextRetVal2;
                i4AuthKeyId = i4NextAuthKeyId;

                i1OutCome =
                    nmhGetNextIndexFsMIOspfv3VirtIfAuthTable ((INT4)
                                                              u4Ospfv3AuthCxtId,
                                                              (INT4 *)
                                                              &u4Ospfv3NextCxtId,
                                                              u4CurrRetVal,
                                                              &u4NextRetVal,
                                                              u4CurrRetVal2,
                                                              &u4NextRetVal2,
                                                              i4AuthKeyId,
                                                              &i4NextAuthKeyId);

            }
            while ((i1OutCome != SNMP_FAILURE)
                   && (u4Ospfv3NextCxtId == u4Ospfv3AuthCxtId));

            if ((u4CurrRetVal != u4NextRetVal)
                || (u4CurrRetVal2 != u4NextRetVal2))
            {
                u1isShowAll = FALSE;
            }

            if (nmhGetNextIndexOspfv3VirtIfTable
                (u4CurrRetVal, &u4NextRetVal, u4CurrRetVal2,
                 &u4NextRetVal2) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
        /* Reseting the context to back */
        if (V3UtilSetContext (u4Ospfv3AuthCxtId) == SNMP_FAILURE)
        {
            V3OspfUnLock ();
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
        }
    }

#ifdef ROUTEMAP_WANTED
    /* Filters */
    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    RouteMapName.pu1_OctetList = au1RMapName;

    i1OutCome = nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable (&RouteMapName,
                                                                 &i4FilterType);
    while (i1OutCome != SNMP_FAILURE)
    {
        switch (i4FilterType)
        {
            case FILTERING_TYPE_DISTANCE:
                if (nmhGetFutOspfv3DistInOutRouteMapValue
                    (&RouteMapName, i4FilterType, &i4Value) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "distance %d route-map %s \r\n",
                               i4Value, RouteMapName.pu1_OctetList);
                    FilePrintf (CliHandle, "distance %d route-map %s \r\n",
                                i4Value, RouteMapName.pu1_OctetList);
                }
                break;
            case FILTERING_TYPE_DISTRIB_IN:
                CliPrintf (CliHandle, "distribute-list route-map %s in\r\n",
                           RouteMapName.pu1_OctetList);
                FilePrintf (CliHandle, "distribute-list route-map %s in\r\n",
                            RouteMapName.pu1_OctetList);
                break;
            case FILTERING_TYPE_DISTRIB_OUT:
                CliPrintf (CliHandle,
                           "distribute-list route-map %s out\r\n",
                           RouteMapName.pu1_OctetList);
                FilePrintf (CliHandle, "distribute-list route-map %s out\r\n",
                            RouteMapName.pu1_OctetList);
                break;
        }

        MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
        NextRouteMapName.pu1_OctetList = au1NextRMapName;

        i1OutCome =
            nmhGetNextIndexFutOspfv3DistInOutRouteMapTable (&RouteMapName,
                                                            &NextRouteMapName,
                                                            i4FilterType,
                                                            &i4NextFilterType);

        i4FilterType = i4NextFilterType;

        MEMCPY (RouteMapName.pu1_OctetList, NextRouteMapName.pu1_OctetList,
                RMAP_MAX_NAME_LEN + 4);
        RouteMapName.i4_Length = NextRouteMapName.i4_Length;
    }
#endif
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ospfv3ShowRunningConfigInterface                   */
/*                                                                           */
/*     DESCRIPTION      : This function displays current Ospfv3 configuration*/
/*                        for interfaces                                     */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Ospfv3ShowRunningConfigInterface (tCliHandle CliHandle)
{
    INT4                i4NextIndex = OSPFV3_INIT_VAL;
    INT4                i4CurrIndex = OSPFV3_INIT_VAL;
    UINT1               u1isShowAll = TRUE;
    UINT4               u4RetVal = OSPFV3_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    UINT4               u4ContextId = 0;

    if (nmhGetFirstIndexOspfv3IfTable (&i4NextIndex) == SNMP_SUCCESS)
    {
        do
        {
            u4RetVal = V3OspfGetCxtIdFromCfaIndex (i4NextIndex, &u4ContextId);

            if ((u4RetVal == OSIX_SUCCESS)
                && (u4ContextId == gV3OsRtr.pV3OspfCxt->u4ContextId))
            {
                if (nmhGetOspfv3IfStatus (i4NextIndex, (INT4 *) &u4RetVal)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (u4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
                {
                    V3OspfUnLock ();
                    CliUnRegisterLock (CliHandle);

                    Ospfv3ShowRunningConfigInterfaceDetails (CliHandle,
                                                             i4NextIndex);

                    CliRegisterLock (CliHandle, V3OspfLock, V3OspfUnLock);
                    V3OspfLock ();

                    u4PagingStatus = CliPrintf (CliHandle, "\r\n");
                    FilePrintf (CliHandle, "!\r\n");
                }
                if (u4PagingStatus == CLI_FAILURE)
                {
                    u1isShowAll = FALSE;
                }
            }
            i4CurrIndex = i4NextIndex;

            if (nmhGetNextIndexOspfv3IfTable (i4CurrIndex, &i4NextIndex)
                == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : Ospfv3ShowRunningConfigInterfaceDetails            */
/*                                                                           */
/*     DESCRIPTION      :This function displays current Ospfv3 configurations*/
/*                        for a particular interface                         */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4Index - Specified interface index,               */
/*                                  for configuration                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
Ospfv3ShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    tIp6Addr            ip6Addr;
    tNetIpv6IfInfo      ipv6IfInfo;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT1              *pu1String = NULL;
    UINT4               u4Metric = OSPFV3_INIT_VAL;
    UINT4               u4RetVal = OSPFV3_INIT_VAL;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4CxtId = OSPFV3_INIT_VAL;
    UINT1               u1isShowAll = TRUE;
    UINT1               u1IfType = OSPFV3_INIT_VAL;
    INT4                i4NextIndex = OSPFV3_INIT_VAL;
    INT4                i4CurrIndex = OSPFV3_INIT_VAL;
    INT4                i4NextIndex2 = OSPFV3_INIT_VAL;
    INT4                i4CurrIndex2 = OSPFV3_INIT_VAL;
    INT4                i4RetVal = OSPFV3_INIT_VAL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1BangStatus = FALSE;
    UINT4               u4OspfPrevCxtId = 0;
    INT4                i4AuthKeyId = 0;
    INT4                i4PrevAuthKeyId = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4PrevAuthIfIndex = 0;
    INT1                i1OutCome;
    INT4                i4RetValue = OSPFV3_INIT_VAL;
    tAuthkeyInfo       *pAuthkeyInfo;

    tSNMP_OCTET_STRING_TYPE ospfSANet;
    tSNMP_OCTET_STRING_TYPE prevOspfSANet;

    ospfSANet.pu1_OctetList = (UINT1 *) &ip6Addr;

    if (V3OspfGetCxtIdFromCfaIndex (i4IfIndex, &u4CxtId) == OSIX_SUCCESS)
    {
        pV3OspfCxt = V3UtilOspfGetCxt (u4CxtId);
        if ((pV3OspfCxt == NULL) ||
            ((IssGetModuleSystemControl (OSPF3_MODULE_ID) == MODULE_SHUTDOWN) &&
             (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS)))
        {
            return CLI_FAILURE;
        }
    }
    if (NetIpv6GetIfInfo ((UINT4) i4IfIndex, &ipv6IfInfo) == NETIPV6_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliRegisterLock (CliHandle, V3OspfLock, V3OspfUnLock);
    V3OspfLock ();

    if (nmhGetOspfv3IfStatus (i4IfIndex, (INT4 *) &u4RetVal) == SNMP_FAILURE)
    {
        V3OspfUnLock ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    if (u4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
    {
        CfaCliConfGetIfName (i4IfIndex, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
        u1BangStatus = TRUE;
        FilePrintf (CliHandle, "interface %s\r\n", au1NameStr);
        nmhGetOspfv3IfAreaId (i4IfIndex, &u4RetVal);
        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4RetVal);
        if ((nmhGetOspfv3IfInstId (i4IfIndex, &i4RetVal) == SNMP_SUCCESS)
            && (i4RetVal != OSPFV3_INIT_VAL))
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf area %s instance %d \r\n",
                                        pu1String, i4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf area %s instance %d \r\n",
                        pu1String, i4RetVal);
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf area %s\r\n", pu1String);
            FilePrintf (CliHandle, "ipv6 ospf area %s\r\n", pu1String);
        }

        nmhGetOspfv3IfDemand (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal == OSPFV3_ENABLED)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf demand-circuit\r\n");
            FilePrintf (CliHandle, "ipv6 ospf demand-circuit\r\n");
        }

        nmhGetOspfv3IfRetransInterval (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_RXMT_INTERVAL)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf retransmit-interval %d\r\n",
                                        u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf retransmit-interval %d\r\n",
                        u4RetVal);
        }

        nmhGetOspfv3IfTransitDelay (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_IF_TRANS_DELAY)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf transmit-delay %d\r\n",
                                        u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf transmit-delay %d\r\n", u4RetVal);
        }

        nmhGetOspfv3IfRtrPriority (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_RTR_PRIORITY)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf priority %d\r\n", u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf priority %d\r\n", u4RetVal);
        }

        nmhGetOspfv3IfHelloInterval (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_HELLO_INTERVAL)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf hello-interval %d\r\n",
                                        u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf hello-interval %d\r\n", u4RetVal);
        }

        nmhGetOspfv3IfRtrDeadInterval (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_RTR_DEAD_INTERVAL)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf dead-interval %d\r\n",
                                        u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf dead-interval %d\r\n", u4RetVal);
        }

        nmhGetOspfv3IfPollInterval (i4IfIndex, &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_POLL_INTERVAL)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf poll-interval %d\r\n",
                                        u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf poll-interval %d\r\n", u4RetVal);
        }

        nmhGetOspfv3IfMetricValue (i4IfIndex, (INT4 *) &u4RetVal);
        /* Get IfSpeed */

        if (V3OspfGetCxtIdFromCfaIndex (i4IfIndex, &u4CxtId) == OSIX_SUCCESS)
        {
            pV3OspfCxt = V3UtilOspfGetCxt (u4CxtId);
            if (pV3OspfCxt != NULL)
            {
                u4Metric = V3UtilIfGetMetricFromSpeedInCxt (pV3OspfCxt,
                                                            ipv6IfInfo.
                                                            u4IfSpeed,
                                                            ipv6IfInfo.
                                                            u4IfHighSpeed);
            }
        }
        if (u4RetVal != u4Metric)
        {
            u4PagingStatus = CliPrintf (CliHandle,
                                        "ipv6 ospf metric %d\r\n", u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf metric %d\r\n", u4RetVal);
        }

        /* Get Default IfType */
        nmhGetOspfv3IfType (i4IfIndex, (INT4 *) &u4RetVal);
        V3MapIfType (ipv6IfInfo.u4InterfaceType, &u1IfType);
        switch (u4RetVal)
        {
            case OSPFV3_IF_NBMA:
            {
                u4PagingStatus =
                    CliPrintf (CliHandle,
                               "ipv6 ospf network non-broadcast\r\n");
                FilePrintf (CliHandle, "ipv6 ospf network non-broadcast\r\n");
            }
                break;
            case OSPFV3_IF_PTOMP:
            {
                u4PagingStatus =
                    CliPrintf (CliHandle,
                               "ipv6 ospf network point-to-multipoint\r\n");
                FilePrintf (CliHandle,
                            "ipv6 ospf network point-to-multipoint\r\n");
            }
                break;
            case OSPFV3_IF_PTOP:
            {
                u4PagingStatus =
                    CliPrintf (CliHandle,
                               "ipv6 ospf network point-to-point\r\n");
                FilePrintf (CliHandle, "ipv6 ospf network point-to-point\r\n");
            }
                break;
        }

        nmhGetOspfv3IfDemandNbrProbe (i4IfIndex, (INT4 *) &u4RetVal);
        if (u4RetVal == OSPFV3_ENABLED)
        {
            u4PagingStatus =
                CliPrintf (CliHandle, "ipv6 ospf neighbor probing\r\n");
            FilePrintf (CliHandle, "ipv6 ospf neighbor probing\r\n");
        }

        nmhGetOspfv3IfDemandNbrProbeRetxLimit (i4IfIndex, &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_NBR_PROBE_RXMT_LIMIT)
        {
            u4PagingStatus =
                CliPrintf (CliHandle,
                           "ipv6 ospf neighbor-probe retransmit-limit %d\r\n",
                           u4RetVal);
            FilePrintf (CliHandle,
                        "ipv6 ospf neighbor-probe retransmit-limit %d\r\n",
                        u4RetVal);
        }

        nmhGetOspfv3IfDemandNbrProbeInterval (i4IfIndex, &u4RetVal);
        if (u4RetVal != OSPFV3_DEF_NBR_PROBE_INTERVAL)
        {
            u4PagingStatus =
                CliPrintf (CliHandle,
                           "ipv6 ospf neighbor-probe interval %d\r\n",
                           u4RetVal);
            FilePrintf (CliHandle, "ipv6 ospf neighbor-probe interval %d\r\n",
                        u4RetVal);
        }

        nmhGetFutOspfv3IfPassive (i4IfIndex, &i4RetVal);
        nmhGetFutOspfv3DefaultPassiveInterface (&i4RetValue);
        if (i4RetVal == OSPFV3_ENABLED)
        {
            u4PagingStatus =
                CliPrintf (CliHandle, "ipv6 ospf passive-interface\r\n");
            FilePrintf (CliHandle, "ipv6 ospf passive-interface\r\n");
        }

        else if (i4RetValue == OSPFV3_ENABLED && i4RetVal != OSPFV3_ENABLED)
        {
            u4PagingStatus =
                CliPrintf (CliHandle, "no ipv6 ospf passive-interface\r\n");
            FilePrintf (CliHandle, "no ipv6 ospf passive-interface\r\n");
        }

        nmhGetFutOspfv3IfLinkLSASuppression (i4IfIndex, &i4RetVal);
        if (i4RetVal == OSPFV3_ENABLED)
        {
            u4PagingStatus =
                CliPrintf (CliHandle, "ipv6 ospf LinkLSASuppression\r\n");
            FilePrintf (CliHandle, "ipv6 ospf LinkLSASuppression\r\n");
        }

        nmhGetFutOspfv3IfBfdState (i4IfIndex, &i4RetVal);
        if (i4RetVal == OSPFV3_BFD_ENABLED)
        {
            CliPrintf (CliHandle, " ipv6 ospf bfd\r\n");
            FilePrintf (CliHandle, " ipv6 ospf bfd\r\n");
        }

        if (V3OspfIfAuthInfoInCxt (CliHandle, i4IfIndex) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* futOspfV3IfAuthTable */
        u4OspfPrevCxtId = u4CxtId;

        i1OutCome =
            nmhGetNextIndexFsMIOspfv3IfAuthTable ((INT4) u4OspfPrevCxtId,
                                                  (INT4 *) &u4CxtId, i4IfIndex,
                                                  &i4NextIfIndex, (-1),
                                                  &i4AuthKeyId);

        while (i1OutCome == SNMP_SUCCESS)
        {
            if (i4IfIndex == i4NextIfIndex)
            {

                pAuthkeyInfo =
                    V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4NextIfIndex,
                                                 (UINT2) i4AuthKeyId);

                if (pAuthkeyInfo != NULL)
                {
                    V3PrintKeyData (CliHandle, pAuthkeyInfo);
                }
            }
            i4PrevAuthIfIndex = i4NextIfIndex;
            i4PrevAuthKeyId = i4AuthKeyId;

            i1OutCome = nmhGetNextIndexFsMIOspfv3IfAuthTable ((INT4)
                                                              u4OspfPrevCxtId,
                                                              (INT4 *) &u4CxtId,
                                                              i4PrevAuthIfIndex,
                                                              &i4NextIfIndex,
                                                              i4PrevAuthKeyId,
                                                              &i4AuthKeyId);

            if ((u4CxtId != u4OspfPrevCxtId) ||
                (i4PrevAuthIfIndex != i4NextIfIndex))
            {
                u4CxtId = u4OspfPrevCxtId;
                break;
            }
        }
        if (V3UtilSetContext ((UINT4) u4OspfPrevCxtId) == SNMP_FAILURE)
        {
            CliUnRegisterLock (CliHandle);
            V3OspfUnLock ();
            return CLI_FAILURE;
        }
    }

    if (nmhGetFirstIndexOspfv3NbmaNbrTable (&i4NextIndex, &i4NextIndex2,
                                            &ospfSANet) == SNMP_SUCCESS)
    {
        do
        {
            if (i4NextIndex == i4IfIndex)
            {
                nmhGetOspfv3NbmaNbrStatus (i4NextIndex, i4NextIndex2,
                                           &ospfSANet, &i4RetVal);

                if (i4RetVal == OSPFV3_CLI_STATUS_ACTIVE)
                {
                    CliPrintf (CliHandle, "ipv6 ospf neighbor %s",
                               Ip6PrintAddr (&ip6Addr));
                    FilePrintf (CliHandle, "ipv6 ospf neighbor %s",
                                Ip6PrintAddr (&ip6Addr));
                    nmhGetOspfv3NbmaNbrPriority (i4NextIndex, i4NextIndex2,
                                                 &ospfSANet, &i4RetVal);
                    CliPrintf (CliHandle, " priority %d", i4RetVal);
                    FilePrintf (CliHandle, " priority %d", i4RetVal);
                    u4PagingStatus = CliPrintf (CliHandle, "\r\n", i4RetVal);
                    FilePrintf (CliHandle, "\r\n", i4RetVal);

                }
            }
            if (u4PagingStatus == CLI_FAILURE)
            {
                u1isShowAll = FALSE;
            }

            i4CurrIndex = i4NextIndex;
            i4CurrIndex2 = i4NextIndex2;
            prevOspfSANet.pu1_OctetList = ospfSANet.pu1_OctetList;

            if (nmhGetNextIndexOspfv3NbmaNbrTable
                (i4CurrIndex, &i4NextIndex, i4CurrIndex2, &i4NextIndex2,
                 &prevOspfSANet, &ospfSANet) == SNMP_FAILURE)
            {
                u1isShowAll = FALSE;
            }
        }
        while (u1isShowAll);
    }
    if (u1BangStatus == TRUE)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    V3OspfUnLock ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetStaggeringStatus                           */
/*                                                                            */
/* Description       : This Routine will set OSPF route table calculation     */
/*                     staggering status                                      */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4StaggingStatus - Staggering status                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
PRIVATE INT4
V3OspfCliSetStaggeringStatus (tCliHandle CliHandle, UINT4 u4StaggingStatus)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    if (nmhTestv2FutOspfv3RTStaggeringStatus (&(u4ErrorCode),
                                              (INT4) u4StaggingStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong staggering status\r\n");
        return CLI_FAILURE;
    }

    nmhSetFutOspfv3RTStaggeringStatus ((INT4) u4StaggingStatus);
    /*  Check whether router calculation staggering interval is added earlier.
     *  If added also reset the staggering interval to default value */

    if (OSPFV3_STAGGERING_DISABLED == u4StaggingStatus)

    {
        nmhSetFutOspfv3RTStaggeringInterval (OSPFV3_DEF_STAG_INTERVAL);
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetStaggeringInterval                         */
/*                                                                            */
/* Description       : This Routine will set OSPF route table calculation     */
/*                     staggering interval                                    */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4StaggingStatus - Staggering status                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
PRIVATE INT4
V3OspfCliSetStaggeringInterval (tCliHandle CliHandle, UINT4 u4StaggingInterval)
{
    UINT4               u4ErrorCode = OSPFV3_INIT_VAL;

    if ((gV3OsRtr.u4RTStaggeringStatus != OSPFV3_STAGGERING_ENABLED))
    {
        CliPrintf (CliHandle,
                   "\r%% OSPFv3 route calculation staggering must be enabled\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2FutOspfv3RTStaggeringInterval
        (&(u4ErrorCode), (INT4) u4StaggingInterval) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong staggering interval\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RTStaggeringInterval ((INT4) u4StaggingInterval) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef ROUTEMAP_WANTED
/***************************************************************************/
/*   Function Name : V3OspfSetRouteDistance                                */
/*                                                                         */
/*   Description   :  Sets route distance                                  */
/*                                                                         */
/*   Input(s)      :  1. CliHandle                                         */
/*                    2. i4Distance - Distance value                       */
/*                    3. pu1RMapName - route map name                      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
V3OspfSetRouteDistance (tCliHandle CliHandle, INT4 i4Distance,
                        UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = OSPFV3_CLI_INV_ASSOC_RMAP;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (nmhGetFutOspfv3DistInOutRouteMapRowStatus (&RouteMapName,
                                                       FILTERING_TYPE_DISTANCE,
                                                       &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
            {
                return SNMP_FAILURE;
            }
            if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                    (&RouteMapName, FILTERING_TYPE_DISTANCE,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    u4ErrorCode = SNMP_ERR_NO_ERROR;
                }
            }
            else
            {
                u4CliError = OSPFV3_CLI_RMAP_ASSOC_FAILED;
            }
        }
        else
        {
            u4ErrorCode = SNMP_ERR_NO_ERROR;
        }

        if (SNMP_ERR_NO_ERROR == u4ErrorCode)
        {
            if (nmhTestv2FutOspfv3DistInOutRouteMapValue
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 i4Distance) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfv3DistInOutRouteMapValue (&RouteMapName,
                                                           FILTERING_TYPE_DISTANCE,
                                                           i4Distance) ==
                    SNMP_SUCCESS)
                {
                    if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                        (&u4ErrorCode, &RouteMapName,
                         FILTERING_TYPE_DISTANCE, ACTIVE) == SNMP_SUCCESS)
                    {
                        if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                            (&RouteMapName, FILTERING_TYPE_DISTANCE,
                             ACTIVE) == SNMP_SUCCESS)
                        {
                            return CLI_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FutOspf3PreferenceValue (&u4ErrorCode, i4Distance) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFutOspf3PreferenceValue (i4Distance) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4CliError = OSPFV3_CLI_INV_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : V3OspfSetNoRouteDistance                              */
/*                                                                         */
/*   Description   :                                                       */
/*                                                                         */
/*   Input(s)      :  1. CliHandle                                         */
/*                    2. pu1RMapName - route map name                      */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
V3OspfSetNoRouteDistance (tCliHandle CliHandle, UINT1 *pu1RMapName)
{
    UINT4               u4CliError = OSPFV3_CLI_INV_DISASSOC_RMAP;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (nmhGetFutOspfv3DistInOutRouteMapRowStatus (&RouteMapName,
                                                       FILTERING_TYPE_DISTANCE,
                                                       &i4RowStatus) ==
            SNMP_SUCCESS)
        {
            if (RouteMapName.i4_Length > RMAP_MAX_NAME_LEN)
            {
                return SNMP_FAILURE;
            }
            if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFutOspfv3DistInOutRouteMapRowStatus
                    (&RouteMapName, FILTERING_TYPE_DISTANCE,
                     DESTROY) == SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
            }
        }
        else
        {
            u4CliError = OSPFV3_CLI_INV_ASSOC_RMAP;
        }
    }
    else
    {                            /* 0 distance will set default value */
        if (nmhTestv2FutOspf3PreferenceValue (&u4ErrorCode, 0) == SNMP_SUCCESS)
        {
            if (nmhSetFutOspf3PreferenceValue (0) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4CliError = OSPFV3_CLI_INV_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}
#endif

/******************************************************************************/
/* Function Name     : V3OspfCliGetContext                                    */
/*                                                                            */
/* Description       : This function calls a routine to set the               */
/*                     context id. The function sets a flag for show routines */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext ID                         */
/*                     u4Command      - CLI command                           */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId   - Ospf Context ID                       */
/*                     pu4IfIndex     - Interface If Index                    */
/*                     pu2ShowCmdFlag - Flag to indicate the show command     */
/*                                                                            */
/* Return Value      : OSPF_SUCCESS/OSPF_FAILURE                              */
/******************************************************************************/

INT4
V3OspfCliGetContext (tCliHandle CliHandle, UINT4 u4Command,
                     UINT4 *pu4V3OspfCxtId, UINT4 *pu4IfIndex,
                     UINT2 *pu2ShowCmdFlag)
{
    INT4                i4IfIndex;

    *pu2ShowCmdFlag = OSPFV3_FALSE;

    /* Get the context Id from pCliContext structure */
    *pu4V3OspfCxtId = CLI_GET_CXT_ID ();

    /* Invalid context Id is obtained in Interface and Global mode */
    if (*pu4V3OspfCxtId == (UINT4) CLI_ERROR)
    {
        i4IfIndex = CLI_GET_IFINDEX ();

        /* CLI_GET_IFINDEX returns -1 for non-interface mode */
        if (i4IfIndex != CLI_ERROR)
        {
            *pu4IfIndex = (UINT4) i4IfIndex;

            /* Get the context Id from the port */
            if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4IfIndex, pu4V3OspfCxtId)
                == OSIX_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Interface not mapped to any context\r\n");
                return OSIX_FAILURE;
            }
        }
        else
        {
            /* This is the Global mode
             * Show flag is set for exec mode*/
            if ((u4Command == OSPFV3_CLI_ADMINSTAT))
            {
                *pu2ShowCmdFlag = OSPFV3_FALSE;
            }
            else
            {
                *pu2ShowCmdFlag = OSPFV3_TRUE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name : V3OspfShowCounters                                        */
/*                                                                           */
/* CLI Command   : show ospf debug failure counters                          */
/*                                                                           */
/* Description   : Display the ospf counter values                           */
/*                                                                           */
/* Input(s)      : Ospfv3 CLI Configuration parameter                        */
/*                                                                           */
/* Output(s)     : ospf counter values                                       */
/*                                                                           */
/* Return Values : None                                                      */
/*****************************************************************************/
VOID
V3OspfShowCounters (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "OSPFv3 Counters : \n");
    CliPrintf (CliHandle,
               "-------------------------------------------------- \n");
    CliPrintf (CliHandle, "gu4V3OspfLockFail                         : %d\r\n",
               gu4V3OspfLockFail);
    CliPrintf (CliHandle, "gu4V3UtilOspfIsValidCxtIdFail             : %d\r\n",
               gu4V3UtilOspfIsValidCxtIdFail);
    CliPrintf (CliHandle, "gu4V3RtmTxRedistributeMsgInCxtFail        : %d\r\n",
               gu4V3RtmTxRedistributeMsgInCxtFail);
    CliPrintf (CliHandle, "gu4V3AuthSendPktFail                      : %d\r\n",
               gu4V3AuthSendPktFail);
    CliPrintf (CliHandle, "gu4V3OspfSendPktFail                      : %d\r\n",
               gu4V3OspfSendPktFail);
    CliPrintf (CliHandle, "gu4V3SendPktOnUnNumberedIntfFail          : %d\r\n",
               gu4V3SendPktOnUnNumberedIntfFail);
    CliPrintf (CliHandle, "gu4O3GrExitHelperFail                     : %d\r\n",
               gu4O3GrExitHelperFail);
    CliPrintf (CliHandle, "gu4V3OspfJoinMcastGroupFail               : %d\r\n",
               gu4V3OspfJoinMcastGroupFail);
    CliPrintf (CliHandle, "gu4V3LsuDeleteLsaFromDatabaseFai          : %d\r\n",
               gu4V3LsuDeleteLsaFromDatabaseFail);
    CliPrintf (CliHandle, "gu4V3SignalLsaRegenInCxtFail              : %d\r\n",
               gu4V3SignalLsaRegenInCxtFail);
    CliPrintf (CliHandle, "gu4V3RtcProcessRtEntryChangesInCxtFail    : %d\r\n",
               gu4V3RtcProcessRtEntryChangesInCxtFail);
    CliPrintf (CliHandle, "gu4V3RtcGetExtLsaLinkFail                 : %d\r\n",
               gu4V3RtcGetExtLsaLinkFail);
    CliPrintf (CliHandle, "gu4V3RtcCheckAndGenAggrLsaInCxtFail       : %d\r\n",
               gu4V3RtcCheckAndGenAggrLsaInCxtFail);
    CliPrintf (CliHandle, "gu4O3RedDynConstructAndSendLsaFail        : %d\r\n",
               gu4O3RedDynConstructAndSendLsaFail);
    CliPrintf (CliHandle, "gu4V3OspfEnqMsgToRmFail                   : %d\r\n",
               gu4V3OspfEnqMsgToRmFail);
    CliPrintf (CliHandle, "gu4V3AuthProcPktFail                      : %d\r\n",
               gu4V3AuthProcPktFail);
    CliPrintf (CliHandle, "gu4V3LsuAddToLsuFail                      : %d\r\n",
               gu4V3LsuAddToLsuFail);
    CliPrintf (CliHandle, "gu4O3RedDynSendIntfInfoToStandbyFail      : %d\r\n",
               gu4O3RedDynSendIntfInfoToStandbyFail);
    CliPrintf (CliHandle, "gu4V3RagChkLsaFallInPrvRngFail            : %d\r\n",
               gu4V3RagChkLsaFallInPrvRngFail);
    CliPrintf (CliHandle, "gu4V3UtilIsMatchedPrefHashNodeFail        : %d\r\n",
               gu4V3UtilIsMatchedPrefHashNodeFail);
    CliPrintf (CliHandle, "gu4V3RtcIsLsaValidForRtCalcFail           : %d\r\n",
               gu4V3RtcIsLsaValidForRtCalcFail);
    CliPrintf (CliHandle, "gu4V3ApplyInFilterInCxtFail               : %d\r\n",
               gu4V3ApplyInFilterInCxtFail);
    CliPrintf (CliHandle, "gu4V3AuthGetATposFail                     : %d\r\n",
               gu4V3AuthGetATposFail);
    CliPrintf (CliHandle, "gu4V3WriteCryptoHighSeqFail               : %d\r\n",
               gu4V3WriteCryptoHighSeqFail);
    CliPrintf (CliHandle, "gu4V3AuthRmCryptSeqMsgToRmFail            : %d\r\n",
               gu4V3AuthRmCryptSeqMsgToRmFail);
    CliPrintf (CliHandle, "gu4V3LsuAddOrDelLsaInRBTreeFail           : %d\r\n",
               gu4V3LsuAddOrDelLsaInRBTreeFail);
    CliPrintf (CliHandle, "gu4V3IfPrefixAddFail                      : %d\r\n",
               gu4V3IfPrefixAddFail);
    CliPrintf (CliHandle, "gu4V3OspfSendEventToRmFail                : %d\r\n",
               gu4V3OspfSendEventToRmFail);
    CliPrintf (CliHandle, "gu4O3RedIfSendIntfInfoFail                : %d\r\n",
               gu4O3RedIfSendIntfInfoFail);
    CliPrintf (CliHandle, "gu4O3RedUtlSendBulkUpdateFail             : %d\r\n",
               gu4O3RedUtlSendBulkUpdateFail);
    CliPrintf (CliHandle, "gu4O3RedAdjAllocateBulkUpdatePktFail      : %d\r\n",
               gu4O3RedAdjAllocateBulkUpdatePktFail);
    CliPrintf (CliHandle, "gu4O3RedAdjSendBulkNbrInfoFail            : %d\r\n",
               gu4O3RedAdjSendBulkNbrInfoFail);
    CliPrintf (CliHandle, "gu4O3RedTmrAddStabilityInfoFail           : %d\r\n",
               gu4O3RedTmrAddStabilityInfoFail);
    CliPrintf (CliHandle, "gu4O3RedTmrSendTimerInfoFail              : %d\r\n",
               gu4O3RedTmrSendTimerInfoFail);
    CliPrintf (CliHandle, "gu4V3OspfUnLockFail                       : %d\r\n",
               gu4V3OspfUnLockFail);
    CliPrintf (CliHandle, "gu4V3HpRcvHelloFail                       : %d\r\n",
               gu4V3HpRcvHelloFail);
    CliPrintf (CliHandle, "gu4O3RedAdjSendBulkVirtNbrInfoFail        : %d\r\n",
               gu4O3RedAdjSendBulkVirtNbrInfoFail);
    CliPrintf (CliHandle, "gu4O3RedAdjSendVirtNbrInfoFail            : %d\r\n",
               gu4O3RedAdjSendVirtNbrInfoFail);
    CliPrintf (CliHandle, "gu4O3RedAdjProcessBulkHelloFail           : %d\r\n",
               gu4O3RedAdjProcessBulkHelloFail);
    CliPrintf (CliHandle, "gu4O3RedAdjProcessNbrStateInfoFail        : %d\r\n",
               gu4O3RedAdjProcessNbrStateInfoFail);
    CliPrintf (CliHandle, "gu4O3RedIfProcessIntfInfoFail             : %d\r\n",
               gu4O3RedIfProcessIntfInfoFail);
    CliPrintf (CliHandle, "gu4O3RedLsuGetLsuFromUpdtFail             : %d\r\n",
               gu4O3RedLsuGetLsuFromUpdtFail);
    CliPrintf (CliHandle, "gu4O3RedLsuProcessLsuFail                 : %d\r\n",
               gu4O3RedLsuProcessLsuFail);
    CliPrintf (CliHandle, "gu4V3RagGenExtLsaInCxtFail                : %d\r\n",
               gu4V3RagGenExtLsaInCxtFail);
    CliPrintf (CliHandle, "gu4V3IsEligibleToGenAseLsaInCxtFail       : %d\r\n",
               gu4V3IsEligibleToGenAseLsaInCxtFail);
    CliPrintf (CliHandle, "gu4V3IsFunctionalityEqvLsaFail            : %d\r\n",
               gu4V3IsFunctionalityEqvLsaFail);
}

/******************************************************************************/
/* Function Name     : V3OspfCliGetCxtIdFromCxtName                           */
/*                                                                            */
/* Description       : This function calls a VCM module to get                */
/*                     the context id with respect to Vrf name                */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     pu1OspfCxtName - vrf name                              */
/*                                                                            */
/* Output Parameters : pu4V3OspfCxtId    - v3Ospf Context ID                  */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliGetCxtIdFromCxtName (tCliHandle CliHandle,
                              UINT1 *pu1OspfCxtName, UINT4 *pu4V3OspfCxtId)
{
    UNUSED_PARAM (CliHandle);

    /* Call the vcm module to get the context id
     * with respect to the vrf name */

    if (V3OspfIsVcmSwitchExist (pu1OspfCxtName, pu4V3OspfCxtId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliGetCxtIdFromIfIndex                           */
/*                                                                            */
/* Description       : This function returns the context id                   */
/*                     with respect to the interface                          */
/*                                                                            */
/* Input Parameters  : CliHandle      - CliContext                            */
/*                     u4IfIndex      - Interface index                       */
/*                                                                            */
/* Output Parameters : pu4OspfCxtId    - V3Ospf Context ID                    */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliGetCxtIdFromIfIndex (tCliHandle CliHandle, UINT4 u4IfIndex,
                              UINT4 *pu4OspfCxtId)
{
    UINT4               u4CxtId = OSPFV3_INIT_VAL;

    UNUSED_PARAM (CliHandle);

    if (V3OspfGetSystemMode (OSPF_PROTOCOL_ID) == OSPFV3_MI_MODE)
    {
        if (V3OspfGetCxtIdFromCfaIndex (u4IfIndex, &u4CxtId) == OSIX_FAILURE)
        {
            /* No Context available for given interface */
            return CLI_FAILURE;
        }

        *pu4OspfCxtId = u4CxtId;
    }
    else
    {
        *pu4OspfCxtId = (INT4) OSPFV3_DEFAULT_CXT_ID;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigRestartSupport                          */
/*                                                                            */
/* Description       : This function sets the restart support for the         */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4RestartSupport -   Restart support                   */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigRestartSupport (tCliHandle CliHandle, INT4 i4RestartSupport)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Ospfv3RestartSupport (&u4ErrCode, i4RestartSupport)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfv3RestartSupport (i4RestartSupport) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigGracePeriod                             */
/*                                                                            */
/* Description       : This function sets the grace period                       */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GracePeriod    -   Grace period                      */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigGracePeriod (tCliHandle CliHandle, INT4 i4GracePeriod)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Ospfv3RestartInterval (&u4ErrCode, i4GracePeriod)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetOspfv3RestartInterval (i4GracePeriod) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigGrAckState                              */
/*                                                                            */
/* Description       : This function enables/disables Grace LSA Ack State     */
/*                     in helper                                              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrAckState  -   Grace LSA Acknowledegement state     */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigGrAckState (tCliHandle CliHandle, INT4 i4GrAckState)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfv3RestartAckState (&u4ErrCode, i4GrAckState)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RestartAckState (i4GrAckState) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigGraceRetransCount                       */
/*                                                                            */
/* Description       : This function sets the grace lsa retransmission count  */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrLsaRetransCount  -   Grace Lsa Retransmission Count*/
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigGraceRetransCount (tCliHandle CliHandle,
                                  INT4 i4GrLsaRetransCount)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfv3GraceLsaRetransmitCount
        (&u4ErrCode, i4GrLsaRetransCount) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3GraceLsaRetransmitCount (i4GrLsaRetransCount) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigRestartReason                           */
/*                                                                            */
/* Description       : This function sets the grace lsa retransmission count  */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GrRestartReason  -   Grace Lsa Restart Reason        */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigRestartReason (tCliHandle CliHandle, INT4 i4GrRestartReason)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfv3RestartReason (&u4ErrCode, i4GrRestartReason)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RestartReason (i4GrRestartReason) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigHelperSupport                           */
/*                                                                            */
/* Description       : This function enables/disables the helper support      */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4HelperSupport  -   Helper support                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* pReturB Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigHelperSupport (tCliHandle CliHandle, INT4 i4HelperSupport,
                              UINT1 u1HelperFlag)
{
    tSNMP_OCTET_STRING_TYPE HelperBit;
    UINT4               u4ErrCode = 0;
    UINT4               u4HelperSupport = (UINT4) i4HelperSupport;
    UINT1               au1HelperSupport[4];

    MEMSET (&HelperBit, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    HelperBit.pu1_OctetList = au1HelperSupport;

    nmhGetFutOspfv3HelperSupport (&HelperBit);

    if (u1HelperFlag == CLI_DISABLE)
    {
        u4HelperSupport = (HelperBit.pu1_OctetList[0] & (~(u4HelperSupport)));
    }
    else
    {
        u4HelperSupport = ((HelperBit.pu1_OctetList[0]) | u4HelperSupport);
    }

    HelperBit.pu1_OctetList[0] = (UINT1) u4HelperSupport;
    HelperBit.i4_Length = 1;

    if (nmhTestv2FutOspfv3HelperSupport (&u4ErrCode, &HelperBit) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3HelperSupport (&HelperBit) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigHelperGraceLimitPeriod                  */
/*                                                                            */
/* Description       : This function sets the helper grace limiy              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4GracePeriod    -   Grace period                      */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigHelperGraceLimitPeriod (tCliHandle CliHandle,
                                       INT4 i4GraceLimitPeriod)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfv3HelperGraceTimeLimit (&u4ErrCode, i4GraceLimitPeriod)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3HelperGraceTimeLimit (i4GraceLimitPeriod) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliConfigStrictLsaCheck                          */
/*                                                                            */
/* Description       : This function enables/disables the strict LSA          */
/*                     in helper                                              */
/*                                                                            */
/* Input Parameters  : CliHandle        -   CliContext ID                     */
/*                     i4StrictLsaCheck -   Strict LSA check option           */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliConfigStrictLsaCheck (tCliHandle CliHandle, INT4 i4StrictLsaCheck)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FutOspfv3RestartStrictLsaChecking
        (&u4ErrCode, i4StrictLsaCheck) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3RestartStrictLsaChecking (i4StrictLsaCheck) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
V3OspfCliSetBfdAdmState (tCliHandle CliHandle, INT4 i4BfdAdminStatus)
{
    UINT4               u4ErrorCode = 0;

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3BfdStatus (&u4ErrorCode, i4BfdAdminStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3BfdStatus (i4BfdAdminStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
V3OspfCliSetBfdStatusAllInt (tCliHandle CliHandle, INT4 i4BfdAllIntStatus)
{
    UINT4               u4ErrorCode = 0;
    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3BfdAllIfState (&u4ErrorCode, i4BfdAllIntStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3BfdAllIfState (i4BfdAllIntStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

INT4
V3OspfCliSetBfdStatusSpecificInt (tCliHandle CliHandle, UINT4 u4InterfaceIndex,
                                  INT4 i4BfdSpecificIntStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FutOspfv3IfBfdState
        (&u4ErrorCode, (INT4) u4InterfaceIndex,
         i4BfdSpecificIntStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFutOspfv3IfBfdState
        ((INT4) u4InterfaceIndex, i4BfdSpecificIntStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3UtilOspfconvertToOctet                               */
/*                                                                            */
/* Description       : This function Converts string format time to octet     */
/*                                                                            */
/* Input Parameters  : CliHandle         -  CliContext ID                     */
/*                     pu1StartAcceptTime-  Pointer to the string time        */
/*                     KeyStartTime      -  Pointer to the octet string       */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3UtilOspfconvertToOctet (tCliHandle CliHandle, UINT1 *pu1StartAcceptTime,
                          tSNMP_OCTET_STRING_TYPE * KeyStartTime)
{
    INT4                i4CheckVal = 0;
    INT4                i4Len;
    UINT1               pOspfV3KeyTime[OSPFV3_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (pOspfV3KeyTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);
    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    i4Len = (INT4) STRLEN (pu1StartAcceptTime);
    if (i4Len > OSPFV3_DST_TIME_LEN)    /* Check if date length exceeds format length */
    {
        CliPrintf (CliHandle,
                   "\r%% Enter date in DD-MON-YEAR,HH:MM format \r\n");
        return CLI_FAILURE;
    }

    /* Validates  the time entered by user and fills tm structure */
    i4CheckVal = V3OspfConvertTime (pu1StartAcceptTime, &tm);
    switch (i4CheckVal)
    {
        case OSPFV3_INVALID_HOUR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid hour\r\n");
            return CLI_FAILURE;
        }
        case OSPFV3_INVALID_MIN:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid minute\r\n");
            return CLI_FAILURE;
        }
        case OSPFV3_INVALID_DATE:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid date\r\n");
            return CLI_FAILURE;
        }
        case OSPFV3_INVALID_MONTH:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid month\r\n");
            return CLI_FAILURE;
        }
        case OSPFV3_INVALID_YEAR:
        {
            CliPrintf (CliHandle, "\r%% Please enter valid year\r\n");
            return CLI_FAILURE;
        }
        case OSPFV3_FAILURE:
        {
            CliPrintf (CliHandle, "\r%% Time not valid\r\n");
            return CLI_FAILURE;
        }
        default:
            break;
    }

    tm.tm_year = tm.tm_year % 10000;
    tm.tm_mon = tm.tm_mon % 100;
    tm.tm_mday = tm.tm_mday % 100;
    tm.tm_hour = tm.tm_hour % 100;
    tm.tm_min = tm.tm_min % 100;
    tm.tm_sec = tm.tm_sec % 100;

    /* Converts the DD-MON-YY to YY-MON-DD format */
    SPRINTF ((CHR1 *) pOspfV3KeyTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    MEMCPY (KeyStartTime->pu1_OctetList, pOspfV3KeyTime, OSPFV3_IPV6_ADDR_LEN);
    KeyStartTime->i4_Length = (INT4) STRLEN (pOspfV3KeyTime);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfCryptoAuthType                           */
/*                                                                            */
/* Description       : This function Set the auth Type for an interface       */
/*                                                                            */
/* Input Parameters  : CliHandle         -  CliContext ID                     */
/*                     u4IfIndex         -  Interfae Index                    */
/*                     i4CryptoAuthType  -  Cryptographic authentication type */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliSetIfCryptoAuthType (tCliHandle CliHandle, UINT4 u4IfIndex,
                              INT4 i4CryptoAuthType)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4StatusVal = 0;
    tV3OsInterface     *pInterface = NULL;

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetOspfv3IfStatus ((INT4) u4IfIndex, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% No Free Index\r\n");
            return CLI_FAILURE;
        }
    }

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FutOspfv3IfCryptoAuthType (&u4ErrorCode, (INT4) u4IfIndex,
                                            i4CryptoAuthType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3IfCryptoAuthType ((INT4) u4IfIndex, i4CryptoAuthType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) u4IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Get the row status */

    if (nmhGetOspfv3IfStatus ((INT4) u4IfIndex, &i4StatusVal) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4StatusVal != OSPFV3_CLI_STATUS_ACTIVE)
    {
        if (nmhTestv2Ospfv3IfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                     OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetOspfv3IfStatus
            ((INT4) u4IfIndex, OSPFV3_CLI_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfAuthKeyStartAccept                       */
/*                                                                            */
/* Description       : This function set the StartAccept time for key         */
/*                                                                            */
/* Input Parameters  : CliHandle           -  CliContext ID                   */
/*                     KeyId               -  Authentication Key ID for a key */
/*                     pu1StartAcceptTime  -  Key accept time in string       */
/*                                            format                          */
/*                     u4ContextId         -  Context ID                      */
/*                     u4IfIndex           -  Interfae Index                  */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliSetIfAuthKeyStartAccept (tCliHandle CliHandle, INT4 KeyId,
                                  UINT1 *pu1StartAcceptTime, UINT4 u4OspfCxtId,
                                  UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               au1OspfKeyTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStartTime.pu1_OctetList = au1OspfKeyTime;

    if (V3UtilOspfGetCxt (u4OspfCxtId) == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3GetFindIf (u4IfIndex) == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (CliHandle, pu1StartAcceptTime, &KeyStartTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) u4IfIndex);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3IfAuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3IfAuthKeyStartAccept
            ((INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%nmhSetFsMIOspfv3IfAuthKeyStartAccept Failed\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Authentication Disabled\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfAuthKeyStartGenerate                     */
/*                                                                            */
/* Description       : This function set the StartGenerate time for key       */
/*                                                                            */
/* Input Parameters  : CliHandle           -  CliContext ID                   */
/*                     KeyId               -  Authentication Key ID for a key */
/*                     pu1StartGenerateTime-  Key Generate time in string     */
/*                                            format                          */
/*                     u4ContextId         -  Context ID                      */
/*                     u4IfIndex           -  Interfae Index                  */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliSetIfAuthKeyStartGenerate (tCliHandle CliHandle, INT4 KeyId,
                                    UINT1 *pu1StartGenerateTime,
                                    UINT4 u4OspfCxtId, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               au1OspfKeyTime[OSPFV3_DST_TIME_LEN + 1];
    tV3OspfCxt         *pV3OspfCxt = NULL;

    MEMSET (&KeyStartTime, OSPFV3_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStartTime.pu1_OctetList = au1OspfKeyTime;

    pV3OspfCxt = V3UtilOspfGetCxt (u4OspfCxtId);
    if (pV3OspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3GetFindIf (u4IfIndex) == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet
        (CliHandle, pu1StartGenerateTime, &KeyStartTime) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) u4IfIndex);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3IfAuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3IfAuthKeyStartGenerate
            ((INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStartGenerate failed\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Authentication Disabled\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfAuthKeyStopAccept                        */
/*                                                                            */
/* Description       : This function  set the StopAccept time for key         */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StopAcceptTime  -  Key accept time in string        */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                     u4IfIndex          -  Interfae Index                   */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliSetIfAuthKeyStopAccept (tCliHandle CliHandle, INT4 KeyId,
                                 UINT1 *pu1StopAcceptTime, UINT4 u4OspfCxtId,
                                 UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               au1OspfKeyTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStopTime.pu1_OctetList = au1OspfKeyTime;

    if (V3UtilOspfGetCxt (u4OspfCxtId) == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3GetFindIf (u4IfIndex) == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (CliHandle, pu1StopAcceptTime, &KeyStopTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) u4IfIndex);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3IfAuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3IfAuthKeyStopAccept
            ((INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStopAccept Failed\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Authentication Disabled\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfAuthKeyStopGenerate                      */
/*                                                                            */
/* Description       : This function  set the StopGenerate time for key       */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StopGenerateTime-  Key Generation time in string    */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                     u4IfIndex          -  Interfae Index                   */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT4
V3OspfCliSetIfAuthKeyStopGenerate (tCliHandle CliHandle, INT4 KeyId,
                                   UINT1 *pu1StopGenerateTime,
                                   UINT4 u4OspfCxtId, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               au1OspfKeyTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&KeyStopTime, OSPFV3_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1OspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStopTime.pu1_OctetList = au1OspfKeyTime;

    if (V3UtilOspfGetCxt (u4OspfCxtId) == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3GetFindIf (u4IfIndex) == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (CliHandle, pu1StopGenerateTime, &KeyStopTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) u4IfIndex);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3IfAuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3IfAuthKeyStopGenerate
            ((INT4) u4OspfCxtId, (INT4) u4IfIndex, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStopGenerate Failed\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%% Authentication Disabled\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfAuthKey                                  */
/*                                                                            */
/* Description       : This function create the auth key                      */
/*                                                                            */
/* Input Parameters  : CliHandle         -  CliContext ID                     */
/*                     u4ContextId       -  Context ID                        */
/*                     u4IfIndex         -  Interfae Index                    */
/*                     i4AuthKeyId       -  Authentication Key ID for a key   */
/*                     pu1AuthKey        -  Cryptographic authentication Key  */
/*                     i4KeyStatus       -  Cryptographic auth Key Status     */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliSetIfAuthKey (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4IfIndex, INT4 i4AuthKeyId,
                       UINT1 *pu1AuthKey, INT4 i4KeyStatus)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE *pOctetString = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (pu1AuthKey != NULL)
    {
        pOctetString =
            SNMP_AGT_FormOctetString (pu1AuthKey, (INT4) STRLEN (pu1AuthKey));

        if (pOctetString == NULL)
        {
            return CLI_FAILURE;
        }
    }

    if (i4KeyStatus == AUTHKEY_STATUS_DELETE)
    {
        if (nmhTestv2FsMIOspfv3IfAuthKeyStatus
            (&u4ErrorCode, (INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             DESTROY) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfv3IfAuthKeyStatus
            ((INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStatus Failed\r\n");
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }
    }

    if ((pOctetString != NULL) && (i4KeyStatus == AUTHKEY_STATUS_VALID))
    {
        /* creates the row as create and wait */
        if (nmhTestv2FsMIOspfv3IfAuthKeyStatus
            (&u4ErrorCode, (INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfv3IfAuthKeyStatus
            ((INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStatus Failed\r\n");
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMIOspfv3IfAuthKey
            (&u4ErrorCode, (INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             pOctetString) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfv3IfAuthKey
            ((INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             pOctetString) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% nmhSetFsMIOspfv3IfAuthKey Failed\r\n");
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsMIOspfv3IfAuthKeyStatus
            (&u4ErrorCode, (INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             ACTIVE) == SNMP_FAILURE)
        {
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }

        if (nmhSetFsMIOspfv3IfAuthKeyStatus
            ((INT4) u4ContextId, (INT4) u4IfIndex, i4AuthKeyId,
             ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% nmhSetFsMIOspfv3IfAuthKeyStatus Failed\r\n");
            SNMP_AGT_FreeOctetString (pOctetString);
            return CLI_FAILURE;
        }
    }

    SNMP_AGT_FreeOctetString (pOctetString);

    pInterface = V3GetFindIf (u4IfIndex);
    if (pInterface == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface\r\n");
        return CLI_FAILURE;
    }

    if (TMO_SLL_Count (&(pInterface->sortAuthkeyLst)) == 0)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, (UINT4) OSPFV3_SYSLOG_ID,
                      "V3OspfCliSetIfAuthKey: last key deleted"));
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfCryptoAuthMode                           */
/*                                                                            */
/* Description       : This function Set the auth Type for an interface       */
/*                                                                            */
/* Input Parameters  : CliHandle         -  CliContext ID                     */
/*                     u4IfIndex         -  Interfae Index                    */
/*                     i4CryptoAuthType  -  Cryptographic authentication mode */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT1
V3OspfCliSetIfAuthMode (tCliHandle CliHandle, UINT4 u4IfIndex, INT4 i4AuthMode)
{
    UINT4               u4ErrorCode = 0;

    if (V3GetFindIf (u4IfIndex) == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMIOspfv3IfCryptoAuthMode (&u4ErrorCode,
                                             (INT4) u4IfIndex,
                                             i4AuthMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIOspfv3IfCryptoAuthMode ((INT4) u4IfIndex, i4AuthMode) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfShowAuthPktStats                         */
/*                                                                            */
/* Description       : This function display packet counts for an interface   */
/*                                                                            */
/* Input Parameters  : CliHandle         -  CliContext ID                     */
/*                     u4IfIndex         -  Interfae Index                    */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT1
V3OspfCliSetIfShowAuthPktStats (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf (u4IfIndex);
    if (pInterface == NULL)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface passed\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "%-25s:%-7lu\n%-25s:%-7lu\n%-25s:%-7lu\n\r",
               "AT packets Transmitted",
               pInterface->u4AuthUpdateTxedCount,
               "AT packets Received",
               pInterface->u4AuthUpdateRcvdCount,
               "AT packets Droped", pInterface->u4AuthUpdateDisdCount);

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfVirtAuthKeyStartAccept                   */
/*                                                                            */
/* Description       : This function  set the StartAccept time for key        */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     u4AreaId           -  Aread ID                         */
/*                     u4NbrId            -  Neighbour ID                     */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StartAcceptTime -  Key Accept time in string        */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT1
V3OspfCliSetIfVirtAuthKeyStartAccept (tCliHandle cliHandle, UINT4 u4AreaId,
                                      UINT4 u4NbrId, INT4 KeyId,
                                      UINT1 *pu1StartAcceptTime,
                                      UINT4 u4OspfCxtId)
{

    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = NULL;
    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               pOspfKeyTime[OSPFV3_DST_TIME_LEN + 1];
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;

    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (pOspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStartTime.pu1_OctetList = pOspfKeyTime;

    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);

    pOspfCxt = V3UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (cliHandle, pu1StartAcceptTime, &KeyStartTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStartAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId,
             u4NbrId, KeyId, &KeyStartTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3VirtIfAuthKeyStartAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Authentication is NULL
         * It should not come here */
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfVirtAuthKeyStopAccept                    */
/*                                                                            */
/* Description       : This function  set the StopAccept time for key         */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     u4AreaId           -  Aread ID                         */
/*                     u4NbrId            -  Neighbour ID                     */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StopAcceptTime  -  Key Accept time in string        */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT1
V3OspfCliSetIfVirtAuthKeyStopAccept (tCliHandle cliHandle, UINT4 u4AreaId,
                                     UINT4 u4NbrId, INT4 KeyId,
                                     UINT1 *pu1StopAcceptTime,
                                     UINT4 u4OspfCxtId)
{

    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = NULL;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;

    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               pOspfKeyTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (pOspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStopTime.pu1_OctetList = pOspfKeyTime;

    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);

    pOspfCxt = V3UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (cliHandle, pu1StopAcceptTime, &KeyStopTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStopAccept
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId,
             u4NbrId, KeyId, &KeyStopTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3VirtIfAuthKeyStopAccept
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Authentication is NULL
         * It should not come here */
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfVirtAuthKeyStartGen                      */
/*                                                                            */
/* Description       : This function  set the StartGen time for key           */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     u4AreaId           -  Aread ID                         */
/*                     u4NbrId            -  Neighbour ID                     */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StartGenTime    -  Key Gene time in string          */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT1
V3OspfCliSetIfVirtAuthKeyStartGen (tCliHandle cliHandle, UINT4 u4AreaId,
                                   UINT4 u4NbrId, INT4 KeyId,
                                   UINT1 *pu1StartGenTime, UINT4 u4OspfCxtId)
{

    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = NULL;

    tSNMP_OCTET_STRING_TYPE KeyStartTime;
    UINT1               pOspfKeyTime[OSPFV3_DST_TIME_LEN + 1];
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;

    MEMSET (&KeyStartTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (pOspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStartTime.pu1_OctetList = pOspfKeyTime;

    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);

    pOspfCxt = V3UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (cliHandle, pu1StartGenTime, &KeyStartTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHKEY_STATUS);
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStartGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId,
             u4NbrId, KeyId, &KeyStartTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3VirtIfAuthKeyStartGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStartTime) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Authentication is NULL
         * It should not come here */
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetIfVirtAuthKeyStopGen                       */
/*                                                                            */
/* Description       : This function  set the StopGen time for key            */
/*                                                                            */
/* Input Parameters  : CliHandle          -  CliContext ID                    */
/*                     u4AreaId           -  Aread ID                         */
/*                     u4NbrId            -  Neighbour ID                     */
/*                     KeyId              -  Authentication Key ID for a key  */
/*                     pu1StopGenTime     -  Key Gene time in string          */
/*                                           format                           */
/*                     u4ContextId        -  Context ID                       */
/*                                                                            */
/* Output Parameters : NONE                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
INT1
V3OspfCliSetIfVirtAuthKeyStopGen (tCliHandle cliHandle, UINT4 u4AreaId,
                                  UINT4 u4NbrId, INT4 KeyId,
                                  UINT1 *pu1StopGenTime, UINT4 u4OspfCxtId)
{

    UINT4               u4ErrorCode = SNMP_SUCCESS;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = NULL;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;

    tSNMP_OCTET_STRING_TYPE KeyStopTime;
    UINT1               pOspfKeyTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&KeyStopTime, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (pOspfKeyTime, OSPFV3_ZERO, (OSPFV3_DST_TIME_LEN + 1));
    KeyStopTime.pu1_OctetList = pOspfKeyTime;

    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4NbrId);

    pOspfCxt = V3UtilOspfGetCxt (u4OspfCxtId);
    if (pOspfCxt == NULL)
    {
        return CLI_FAILURE;
    }

    if (V3UtilOspfconvertToOctet (cliHandle, pu1StopGenTime, &KeyStopTime)
        == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        return CLI_FAILURE;
    }

    /* Test routine is called for configuring Key Start Accept value */
    if (pInterface->u2CryptoAuthType != OSPFV3_AUTH_NO_TYPE)
    {
        if (nmhTestv2FsMIOspfv3VirtIfAuthKeyStopGenerate
            (&u4ErrorCode, (INT4) u4OspfCxtId, u4AreaId,
             u4NbrId, KeyId, &KeyStopTime) == SNMP_FAILURE)
        {
            return CLI_FAILURE;

        }
        /* Set the Key Start Accept Value */
        if (nmhSetFsMIOspfv3VirtIfAuthKeyStopGenerate
            ((INT4) u4OspfCxtId, u4AreaId, u4NbrId, KeyId,
             &KeyStopTime) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (cliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssOspf3ShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the OSPF3 debug level         */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
VOID
IssOspf3ShowDebugging (tCliHandle CliHandle)
{
    INT4                i4TraceVal = 0;

    /* Get OSPFV3 Trace Option */
    if (V3UtilSetContext (OSPFV3_DEFAULT_CXT_ID) == SNMP_FAILURE)
    {
        return;
    }
    if (nmhGetFutOspfv3TraceLevel (&i4TraceVal) == SNMP_FAILURE)
    {
        return;
    }

    if (i4TraceVal != 0)
    {
        if ((i4TraceVal & OSPFV3_CLI_DUMP_HGH_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - High Level packet dump debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_DUMP_LOW_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Low Level packet dump debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_DUMP_HEX_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Hex Level packet dump debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_HP_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - Hello debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_DDP_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - DDP debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_LRQ_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Link State Request debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_LSU_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Link State Update debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_LAK_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Link State Acknowledge debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_FN_ENTRY) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Function entry debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_FN_EXIT) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - Function exit debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_CRITICAL_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - Critical debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_MEM_SUCC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Memory Allocation Success debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_MEM_FAIL) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Memory Allocation Failure debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_PPP_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Protocol Packet Processing debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_RTMODULE_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - RTM debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_NSSA_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - NSSA debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_RAG_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Route Aggregation debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_ADJACENCY_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Adjacency Formation debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_LSDB_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Link State Database debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_ISM_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Interface State Machine debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_NSM_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Neighbor State Machine debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_RT_TRC) != 0)
        {
            CliPrintf (CliHandle,
                       "\r\n OSPF3 - Roting Table Calculation debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_INTERFACE_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - Interface debugging is on");
        }
        if ((i4TraceVal & OSPFV3_CLI_CONFIGURATION_TRC) != 0)
        {
            CliPrintf (CliHandle, "\r\n OSPF3 - Configuration debugging is on");
        }
        CliPrintf (CliHandle, "\n");
    }
    else
    {
        UNUSED_PARAM (CliHandle);
    }
    V3UtilReSetContext ();
    return;
}

/*****************************************************************************/
/* Function Name      : V3CliGetShowCmdOutputToFile                          */
/*                                                                           */
/* Description        : This function handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu1FileName - The output of the show cmd is          */
/*                      redirected to this file                              */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPFV3_SUCCESS/OSPFV3_FAILURE                              */
/*****************************************************************************/
INT4
V3CliGetShowCmdOutputToFile (UINT1 *pu1FileName)
{
    INT4                i4Cmd = 0;
    CHR1               *au1OspfShowCmdList[OSPFV3_DYN_MAX_CMDS]
        = { "snmpwalk mib name ospfv3NbrTable >> ",
        "snmpwalk mib name ospfv3VirtNbrTable >> ",
        "snmpwalk mib name ospfv3IfTable >> ",
        "snmpwalk mib name futOspfv3IfTable >> ",
        "snmpwalk mib name ospfv3VirtIfTable >> ",
        "snmpwalk mib name ospfv3AreaLsdbAdvertisement >> ",
        "snmpwalk mib name ospfv3AsLsdbAdvertisement >> ",
        "snmpwalk mib name ospfv3LinkLsdbAdvertisement >> ",
        "show ipv6 ospf retrans-list >> "
    };
    if (FileStat ((const CHR1 *) pu1FileName) == OSIX_SUCCESS)
    {
        if (0 != FileDelete (pu1FileName))
        {
            return OSPFV3_FAILURE;
        }
    }
    for (i4Cmd = 0; i4Cmd < OSPFV3_DYN_MAX_CMDS; i4Cmd++)
    {
        if (CliGetShowCmdOutputToFile (pu1FileName,
                                       (UINT1 *) au1OspfShowCmdList[i4Cmd])
            == CLI_FAILURE)
        {
            return OSPFV3_FAILURE;
        }
    }
    return OSPFV3_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : V3CliCalcSwAudCheckSum                               */
/*                                                                           */
/* Description        : This function handles the Calculation of checksum    */
/*                      for the data in the given input file.                */
/*                                                                           */
/* Input(s)           : pu1FileName - The checksum is calculated for the data*/
/*                      in this file                                         */
/*                                                                           */
/* Output(s)          : pu2ChkSum - The calculated checksum is stored here   */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPFV3_SUCCESS/OSPFV3_FAILURE                              */
/*****************************************************************************/
INT4
V3CliCalcSwAudCheckSum (UINT1 *pu1FileName, UINT2 *pu2ChkSum)
{
    UINT4               u4Sum = 0;
    UINT4               u4Last = 0;
    INT4                i4Fd;
    UINT2               u2CkSum = 0;
    INT2                i2ReadLen;
    INT1                ai1Buf[OSPFV3_CLI_MAX_GROUPS_LINE_LEN + 1];

    i4Fd = FileOpen ((CONST UINT1 *) pu1FileName, OSPFV3_CLI_RDONLY);
    if (i4Fd == -1)
    {
        return OSPFV3_FAILURE;
    }
    MEMSET (ai1Buf, 0, OSPFV3_CLI_MAX_GROUPS_LINE_LEN + 1);
    while (V3CliReadLineFromFile (i4Fd, ai1Buf, OSPFV3_CLI_MAX_GROUPS_LINE_LEN,
                                  &i2ReadLen) != OSPFV3_CLI_EOF)

    {
        if ((i2ReadLen > 0) &&
            (NULL == STRSTR (ai1Buf, "ospfv3NbrOptions")) &&
            (NULL == STRSTR (ai1Buf, "ospfv3NbrEvents")) &&
            (NULL == STRSTR (ai1Buf, "ospfv3VirtNbrOptions")) &&
            (NULL == STRSTR (ai1Buf, "ospfv3VirtNbrEvents")) &&
            (NULL == STRSTR (ai1Buf, "ospfv3IfEvents")) &&
            (NULL == STRSTR (ai1Buf, "ospfv3VirtIfEvents")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfHelloRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfHelloTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfHelloDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfDdpRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfDdpTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfDdpDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLrqRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLrqTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLrqDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLsuRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLsuTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLsuDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLakRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLakTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfLakDisd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfAuthTxed")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfAuthRcvd")) &&
            (NULL == STRSTR (ai1Buf, "futOspfv3IfAuthDisd")))
        {
            UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                              &u2CkSum, (UINT4) i2ReadLen, CKSUM_DATA);
            MEMSET (ai1Buf, '\0', OSPFV3_CLI_MAX_GROUPS_LINE_LEN + 1);
        }
    }
    /*CheckSum for last line */
    if ((u4Sum != 0) || (i2ReadLen != 0))
    {
        UtilCompCheckSum ((UINT1 *) ai1Buf, &u4Sum, &u4Last,
                          &u2CkSum, (UINT4) i2ReadLen, CKSUM_LASTDATA);

    }
    *pu2ChkSum = u2CkSum;
    if (FileClose (i4Fd) < 0)
    {
        return OSPFV3_FAILURE;
    }
    return OSPFV3_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : V3CliReadLineFromFile                               */
/*                                                                           */
/* Description        : It is a utility to read a line from the given file   */
/*                      descriptor.                                          */
/*                                                                           */
/* Input(s)           : i4Fd - File Descriptor for the file                  */
/*                      i2MaxLen - Maximum length that can be read and store */
/*                                                                           */
/* Output(s)          : pi1Buf - Buffer to store the line read from the file */
/*                      pi2ReadLen - the total length of the data read       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPFV3_SUCCESS/OSPFV3_FAILURE                        */
/*****************************************************************************/
INT1
V3CliReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (OSPFV3_CLI_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (OSPFV3_CLI_EOF);
}

/******************************************************************************/
/* Function Name     : V3OspfCliSetDefaultOriginate                           */
/*                                                                            */
/* Description       : This Routine will generate a default external route    */
/*                     in to OSPFV3 routing domain                            */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     i4Metric   - Metric for the route                      */
/*                     i4MetricType - Metric type for the route               */
/*                     u4FutOspfv3ExtRouteDest - Route destination            */
/*                     u4FutOspfv3ExtRoutePfxLength - Prefix length           */
/*                     u1Flag     - OSPFV3_ENABLED/OSPFV3_DISABLED            */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliSetDefaultOriginate (tCliHandle CliHandle, INT4 i4Metric,
                              INT4 i4MetricType, tIp6Addr FutOspfv3ExtRouteDest,
                              UINT4 u4FutOspfv3ExtRoutePfxLength,
                              tIp6Addr FutOspfv3ExtRouteNextHop)
{
    UINT4               u4ErrorCode = 0;

    INT4                i4StatusVal = 0;
    tSNMP_OCTET_STRING_TYPE ExtRouteDest;
    tSNMP_OCTET_STRING_TYPE ExtRouteNextHop;

    ExtRouteDest.pu1_OctetList = (UINT1 *) &FutOspfv3ExtRouteDest;
    ExtRouteDest.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    ExtRouteNextHop.pu1_OctetList = (UINT1 *) &FutOspfv3ExtRouteNextHop;
    ExtRouteNextHop.i4_Length = OSPFV3_IPV6_ADDR_LEN;

    UNUSED_PARAM (CliHandle);
    if (nmhGetFutOspfv3ExtRouteStatus
        (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
         u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
         &i4StatusVal) == SNMP_FAILURE)
    {
        /*Create an row in ospf external routing table using create and wait */
        if (nmhTestv2FutOspfv3ExtRouteStatus
            (&u4ErrorCode, OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
             u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE,
             &ExtRouteNextHop, CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3ExtRouteStatus
            (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
             u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE,
             &ExtRouteNextHop, CREATE_AND_WAIT) == SNMP_FAILURE)

        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FutOspfv3ExtRouteMetric (&u4ErrorCode,
                                          OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                          u4FutOspfv3ExtRoutePfxLength,
                                          OSPFV3_INET_ADDR_TYPE,
                                          &ExtRouteNextHop,
                                          i4Metric) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfv3ExtRouteMetricType (&u4ErrorCode,
                                              OSPFV3_INET_ADDR_TYPE,
                                              &ExtRouteDest,
                                              u4FutOspfv3ExtRoutePfxLength,
                                              OSPFV3_INET_ADDR_TYPE,
                                              &ExtRouteNextHop,
                                              i4MetricType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFutOspfv3ExtRouteMetric (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                       u4FutOspfv3ExtRoutePfxLength,
                                       OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
                                       i4Metric) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFutOspfv3ExtRouteMetricType (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                           u4FutOspfv3ExtRoutePfxLength,
                                           OSPFV3_INET_ADDR_TYPE,
                                           &ExtRouteNextHop,
                                           i4MetricType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfv3ExtRouteStatus
        (&u4ErrorCode, OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
         u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
         ACTIVE) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    /*Set the external route status active */
    if (nmhSetFutOspfv3ExtRouteStatus (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                       u4FutOspfv3ExtRoutePfxLength,
                                       OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
                                       ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************/
/* Function Name     : V3OspfCliDelDefaultOriginate                           */
/*                                                                            */
/* Description       : This Routine will delete the  default external route   */
/*                     in  OSPFV3 routing domain                              */
/*                                                                            */
/* Input Parameters  : CliHandle  - CliContext ID                             */
/*                     FutOspfv3ExtRouteDest - Route destination              */
/*                     u4FutOspfv3ExtRoutePfxLength - Prefix length           */
/*                     FutOspfv3ExtRouteNextHop - Next hop                    */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/

INT4
V3OspfCliDelDefaultOriginate (tCliHandle CliHandle, INT4 i4Metric,
                              INT4 i4MetricType, tIp6Addr FutOspfv3ExtRouteDest,
                              UINT4 u4FutOspfv3ExtRoutePfxLength,
                              tIp6Addr FutOspfv3ExtRouteNextHop)
{
    INT4                i4StatusVal = 0;
    INT4                i4RouteMetric = 0;
    INT4                i4RouteMetricType = 0;
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE ExtRouteDest;
    tSNMP_OCTET_STRING_TYPE ExtRouteNextHop;

    ExtRouteDest.pu1_OctetList = (UINT1 *) &FutOspfv3ExtRouteDest;
    ExtRouteDest.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    ExtRouteNextHop.pu1_OctetList = (UINT1 *) &FutOspfv3ExtRouteNextHop;
    ExtRouteNextHop.i4_Length = OSPFV3_IPV6_ADDR_LEN;
    UNUSED_PARAM (CliHandle);
    if (nmhGetFutOspfv3ExtRouteStatus
        (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
         u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
         &i4StatusVal) == SNMP_FAILURE)
    {

        /* No default routes in OSPFV3 external database.
         * return success without doing anything*/
        return CLI_SUCCESS;
    }
    /* Checks whether the passed parameter value falls within the mib range */
    if (i4Metric != OSPFV3_INVALID)
    {
        nmhGetFutOspfv3ExtRouteMetric (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                       u4FutOspfv3ExtRoutePfxLength,
                                       OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
                                       &i4RouteMetric);
        if (i4RouteMetric == i4Metric)
        {
            i4Metric = OSPFV3_CLI_AS_EXT_DEF_METRIC;
        }
        else
        {
            CLI_SET_ERR (OSPFV3_CLI_MISMATCH_VALUE);
            return CLI_FAILURE;
        }

        if (nmhTestv2FutOspfv3ExtRouteMetric (&u4ErrorCode,
                                              OSPFV3_INET_ADDR_TYPE,
                                              &ExtRouteDest,
                                              u4FutOspfv3ExtRoutePfxLength,
                                              OSPFV3_INET_ADDR_TYPE,
                                              &ExtRouteNextHop,
                                              i4Metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFutOspfv3ExtRouteMetric
            (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
             u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE,
             &ExtRouteNextHop, i4Metric) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (i4MetricType != OSPFV3_INVALID)
    {
        nmhGetFutOspfv3ExtRouteMetricType (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                           u4FutOspfv3ExtRoutePfxLength,
                                           OSPFV3_INET_ADDR_TYPE,
                                           &ExtRouteNextHop,
                                           &i4RouteMetricType);
        if (i4RouteMetricType == i4MetricType)
        {
            i4MetricType = OSPFV3_TYPE_2_METRIC;
        }
        else
        {
            CLI_SET_ERR (OSPFV3_CLI_MISMATCH_VALUE);
            return CLI_FAILURE;
        }
        if (nmhTestv2FutOspfv3ExtRouteMetricType (&u4ErrorCode,
                                                  OSPFV3_INET_ADDR_TYPE,
                                                  &ExtRouteDest,
                                                  u4FutOspfv3ExtRoutePfxLength,
                                                  OSPFV3_INET_ADDR_TYPE,
                                                  &ExtRouteNextHop,
                                                  i4MetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFutOspfv3ExtRouteMetricType
            (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest, u4FutOspfv3ExtRoutePfxLength,
             OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
             i4MetricType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FutOspfv3ExtRouteStatus
        (&u4ErrorCode, OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
         u4FutOspfv3ExtRoutePfxLength, OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
         DESTROY) == SNMP_FAILURE)

    {
        return CLI_FAILURE;
    }
    /*Set the external route status active */
    if (nmhSetFutOspfv3ExtRouteStatus (OSPFV3_INET_ADDR_TYPE, &ExtRouteDest,
                                       u4FutOspfv3ExtRoutePfxLength,
                                       OSPFV3_INET_ADDR_TYPE, &ExtRouteNextHop,
                                       DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

INT4
V3CliUserPromptResponse (tCliHandle cliHandle, INT1 *pi1Prompt)
{
    INT1                ai1UserPrompt[OSPF3_MAX_INPUT_SIZE];
    INT4                i4RetVal = 0;
    MEMSET (ai1UserPrompt, 0, OSPF3_MAX_INPUT_SIZE);

    MGMT_UNLOCK ();
    i4RetVal = CliReadLine (pi1Prompt, ai1UserPrompt, OSPF3_MAX_INPUT_SIZE - 1);
    MGMT_LOCK ();

    CliPrintf (cliHandle, " \r\n");

    if (i4RetVal == CLI_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (((STRNCASECMP ((const char *) ai1UserPrompt, "y", 1) == 0) &&
         (STRLEN (ai1UserPrompt) == 1)) ||
        ((STRNCASECMP ((const char *) ai1UserPrompt, "yes", 3) == 0) &&
         (STRLEN (ai1UserPrompt) == 3)))

    {
        return (CLI_SUCCESS);
    }
    else if (((STRNCASECMP ((const char *) ai1UserPrompt, "n", 1) == 0) &&
              (STRLEN (ai1UserPrompt) == 1)) ||
             ((STRNCASECMP ((const char *) ai1UserPrompt, "no", 2) == 0) &&
              (STRLEN (ai1UserPrompt) == 2)))
    {
        return (CLI_FAILURE);
    }

    else
    {
        CliPrintf (cliHandle, "%%Invalid Input\r\n");
        return (CLI_FAILURE);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OspfHandleOSPFClearProcess                                 */
/*                                                                           */
/* Description  : This procedure handles OSPF memory allocation failures.    */
/*                OSPF router is always in loading state and consume CPU.    */
/*                To Improve ISS CPU utilization ,shut down all the          */
/*                interfaces and clear the database.                         */
/*                So other protocols in the ISS can have the CPU.            */
/*                                                                           */
/* Input        : tOspfCxt  : ospf contex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS / OSPF_FAILURE                                */
/*                                                                           */
/* Comments     :   Actions taken when memory alloc failure(s) detected      */
/*                        i.   Bring down all the OSPF interfaces            */
/*                        ii.  Clear the OSPF database                       */
/*                        iii. Bring up all the OSPF interfaces              */
/*****************************************************************************/
PRIVATE INT1
V3OspfCliClearProcess (tCliHandle cliHandle, UINT4 u4ContextId)
{

    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4ErrorCode;
    INT4                i4AdminStat;

    UNUSED_PARAM (cliHandle);

    if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                       &i4AdminStat) == SNMP_FAILURE) ||
        (i4AdminStat == OSPFV3_DISABLED) ||
        ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FutOspfv3ClearProcess (&u4ErrorCode, OSPFV3_TRUE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*setting the clear flag as true */
    gu4ClearFlag = OSIX_TRUE;
    if (nmhSetFutOspfv3ClearProcess (OSPFV3_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /*resetting the clear flag to false */
    gu4ClearFlag = OSIX_FALSE;

    return CLI_SUCCESS;

}

/******************************************************************************/
/* Function Name     : OspfCliShowAreaDataBaseSummaryInCxt                    */
/*                                                                            */
/* Description       : This Routine will show the Area database summary       */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     u4AreaId      - Area ID                                */
/*                     LsdbCounter   - Counter for LSA database information   */
/*                                     types, aged LSA's...                   */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
PRIVATE INT1
V3OspfCliShowAreaDataBaseSummaryInCxt (tCliHandle CliHandle, UINT4 u4ContextId,
                                       UINT4 u4AreaId,
                                       tLsdbCounter * LsdbCounter)
{

    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo        LsaInfo;
    UINT4               au4LsTypeCnt[11];
    UINT4               au4LsTypeMaxAgedCnt[11];
    UINT4               u4LsTypeTotal = 0;
    UINT4               u4LsTypeMaxAgedTotal = 0;
    UINT1              *pu1String = NULL;
    UINT1               u1LsType;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    INT4                i4AdminStat;
    INT4                i4PageStatus = 0;
    tV3OsAreaId         areaId;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;

    if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                       &i4AdminStat) == SNMP_FAILURE) ||
        (i4AdminStat == OSPFV3_DISABLED) ||
        ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
    {
        return CLI_FAILURE;
    }
    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    MEMSET (au4LsTypeCnt, 0, sizeof (au4LsTypeCnt));
    MEMSET (au4LsTypeMaxAgedCnt, 0, sizeof (au4LsTypeMaxAgedCnt));

    if (pArea != NULL)
    {
        OSPFV3_CLI_RTRID_TO_STR (pu1String, u4AreaId);
        MEMSET (&LsaInfo, 0, sizeof (tV3OsLsaInfo));
        /*Link Scope Lsas count */
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot);
            if (pLsaInfo == NULL)
            {
                continue;
            }

            do
            {
                MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
                if (LsaInfo.lsaId.u2LsaType == OSPFV3_LINK_LSA)
                {
                    au4LsTypeCnt[OSPFV3_CLI_LINK_LSA - 1]++;
                    if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                    {
                        au4LsTypeMaxAgedCnt[OSPFV3_CLI_LINK_LSA - 1]++;
                    }
                }

            }
            while ((pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                   (tRBElem *) pLsaInfo, NULL)) != NULL);

        }
    }

    /*Print Area Scope Lsas */
    pArea = NULL;
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (areaId, pArea->areaId) == OSPFV3_EQUAL)
        {
            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

            if ((pArea->u4AreaType != OSPFV3_STUB_AREA) &&
                (pArea->u4AreaType != OSPFV3_NSSA_AREA))
            {
                do
                {
                    if (pLsaInfo != NULL)
                    {
                        MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
                        if (LsaInfo.lsaId.u2LsaType == OSPFV3_ROUTER_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_ROUTER_LSA - 1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt[OSPFV3_CLI_ROUTER_LSA -
                                                    1]++;
                            }
                        }
                        if (LsaInfo.lsaId.u2LsaType == OSPFV3_NETWORK_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_NETWORK_LSA - 1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt[OSPFV3_CLI_NETWORK_LSA -
                                                    1]++;
                            }
                        }
                        if (LsaInfo.lsaId.u2LsaType ==
                            OSPFV3_INTER_AREA_PREFIX_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_INTER_AREA_PREFIX_LSA -
                                         1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt
                                    [OSPFV3_CLI_INTER_AREA_PREFIX_LSA - 1]++;
                            }
                        }
                        if (LsaInfo.lsaId.u2LsaType ==
                            OSPFV3_INTER_AREA_ROUTER_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_INTER_AREA_ROUTER_LSA -
                                         1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt
                                    [OSPFV3_CLI_INTER_AREA_ROUTER_LSA - 1]++;
                            }
                        }
                        if (LsaInfo.lsaId.u2LsaType == OSPFV3_NSSA_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_NSSA_LSA - 1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt[OSPFV3_CLI_NSSA_LSA - 1]++;
                            }
                        }
                        if (LsaInfo.lsaId.u2LsaType ==
                            OSPFV3_INTRA_AREA_PREFIX_LSA)
                        {
                            au4LsTypeCnt[OSPFV3_CLI_INTRA_AREA_PREFIX_LSA -
                                         1]++;
                            if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                            {
                                au4LsTypeMaxAgedCnt
                                    [OSPFV3_CLI_INTRA_AREA_PREFIX_LSA - 1]++;
                            }
                        }
                    }

                }
                while ((pLsaInfo = (tV3OsLsaInfo *)
                        RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                       (tRBElem *) pLsaInfo, NULL)) != NULL);

                break;
            }
        }

        /* Print As Scope External Lsas */
        pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

        do
        {
            if (pLsaInfo != NULL)
            {
                MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
                if (LsaInfo.lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
                {
                    au4LsTypeCnt[OSPFV3_CLI_AS_EXT_LSA - 1]++;
                    if (LsaInfo.u1FloodFlag == OSPFV3_TRUE)
                    {
                        au4LsTypeMaxAgedCnt[OSPFV3_CLI_AS_EXT_LSA - 1]++;
                    }
                }
            }

        }
        while ((pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                               (tRBElem *) pLsaInfo, NULL)) != NULL);

    }

    if (au4LsTypeCnt[OSPFV3_CLI_LINK_LSA - 1] != 0)
    {
        CliPrintf (CliHandle, "\r\nArea %s database summary\r\n", pu1String);
        CliPrintf (CliHandle, "--------------------------------\r\n");

        CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                   "LSA Type", "Count", "Maxage");
        CliPrintf (CliHandle, "  %-20s %-10s %-10s\r\n",
                   "--------", "-----", "------");

        for (u1LsType = OSPFV3_CLI_ROUTER_LSA;
             u1LsType <= OSPFV3_CLI_INTRA_AREA_PREFIX_LSA; u1LsType++)
        {
            switch (u1LsType)
            {
                case OSPFV3_CLI_ROUTER_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Router", au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_NETWORK_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Network", au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_INTER_AREA_PREFIX_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Inter Area Prefix",
                                   au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_INTER_AREA_ROUTER_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Inter Area Router",
                                   au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_AS_EXT_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "As External", au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_NSSA_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Type 7", au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_LINK_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Link", au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;
                case OSPFV3_CLI_INTRA_AREA_PREFIX_LSA:
                    i4PageStatus =
                        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                                   "Intra Area Prefix",
                                   au4LsTypeCnt[u1LsType - 1],
                                   au4LsTypeMaxAgedCnt[u1LsType - 1]);
                    break;

                default:
                    break;
            }

            LsdbCounter->au4LsTypeCnt[u1LsType - 1] +=
                au4LsTypeCnt[u1LsType - 1];
            LsdbCounter->au4LsTypeMaxAgedCnt[u1LsType - 1] +=
                au4LsTypeMaxAgedCnt[u1LsType - 1];

            u4LsTypeTotal += au4LsTypeCnt[u1LsType - 1];
            u4LsTypeMaxAgedTotal += au4LsTypeMaxAgedCnt[u1LsType - 1];
            if (i4PageStatus == CLI_FAILURE)
                break;
        }

        CliPrintf (CliHandle, "  %-20s %-10u %-10u\r\n",
                   "Subtotal", u4LsTypeTotal, u4LsTypeMaxAgedTotal);

    }

    return i4PageStatus;
}

/******************************************************************************/
/* Function Name     : OspfCliShowFilterSummaryInfo                           */
/*                                                                            */
/* Description       : This Routine will show the Area database summary       */
/*                                                                            */
/* Input Parameters  : CliHandle     - CliContext ID                          */
/*                     pOspfCxt      - pointer to tOspfCxt structure          */
/*                     u4AreaId      - Area ID                                */
/*                                                                            */
/* Output Parameters : None                                                   */
/*                                                                            */
/* Return Value      : CLI_SUCCESS/CLI_FAILURE                                */
/******************************************************************************/
PRIVATE INT1
V3OspfCliShowFilterSummaryInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4AreaId, UINT4 u4LsaDispType)
{

    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo        LsaInfo;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    INT4                i4AdminStat;
    tV3OsAreaId         areaId;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;

    if ((nmhGetFsMIStdOspfv3AdminStat ((INT4) u4ContextId,
                                       &i4AdminStat) == SNMP_FAILURE) ||
        (i4AdminStat == OSPFV3_DISABLED) ||
        ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL))
    {
        return CLI_FAILURE;
    }
    OSPFV3_BUFFER_DWTOPDU (areaId, u4AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea != NULL)
    {
        MEMSET (&LsaInfo, 0, sizeof (tV3OsLsaInfo));
        /*Link Scope Lsas count */
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot);
            if (pLsaInfo == NULL)
            {
                continue;
            }

            do
            {
                MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
                if (LsaInfo.lsaId.u2LsaType == OSPFV3_LINK_LSA)
                {
                    if (u4LsaDispType == OSPFV3_LSA_ADV_ROUTER)
                    {
                        if ((V3UtilRtrIdComp
                             (LsaInfo.lsaId.advRtrId,
                              V3_LS_ADV_ROUTER_ID)) == OSPFV3_EQUAL)
                        {
                            if (V3OspfCliDisplayLsaSummary (CliHandle, pLsaInfo,
                                                            0) == CLI_FAILURE)
                            {
                                return CLI_FAILURE;
                            }

                        }
                    }
                    else if (u4LsaDispType == OSPFV3_LSA_SELF_ORG)
                    {
                        if ((V3UtilRtrIdComp
                             (LsaInfo.lsaId.advRtrId,
                              pV3OspfCxt->rtrId)) == OSPFV3_EQUAL)
                        {
                            if (V3OspfCliDisplayLsaSummary (CliHandle, pLsaInfo,
                                                            0) == CLI_FAILURE)
                            {
                                return CLI_FAILURE;
                            }
                        }
                    }

                }

            }
            while ((pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                   (tRBElem *) pLsaInfo, NULL)) != NULL);

        }
    }
    /*Print Area Scope Lsas */
    pArea = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (areaId, pArea->areaId) == OSPFV3_EQUAL)
        {
            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

            do
            {
                if (pLsaInfo != NULL)
                {
                    MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
                    if ((LsaInfo.lsaId.u2LsaType != OSPFV3_LINK_LSA) &&
                        (LsaInfo.lsaId.u2LsaType != OSPFV3_AS_EXT_LSA))
                    {
                        if (u4LsaDispType == OSPFV3_LSA_ADV_ROUTER)
                        {
                            if ((V3UtilRtrIdComp
                                 (LsaInfo.lsaId.advRtrId,
                                  V3_LS_ADV_ROUTER_ID)) == OSPFV3_EQUAL)
                            {
                                if (V3OspfCliDisplayLsaSummary
                                    (CliHandle, pLsaInfo, 0) == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }

                            }
                        }
                        else if (u4LsaDispType == OSPFV3_LSA_SELF_ORG)
                        {
                            if ((V3UtilRtrIdComp
                                 (LsaInfo.lsaId.advRtrId,
                                  pV3OspfCxt->rtrId)) == OSPFV3_EQUAL)
                            {
                                if (V3OspfCliDisplayLsaSummary
                                    (CliHandle, pLsaInfo, 0) == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                            }
                        }

                    }
                }

            }
            while ((pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                   (tRBElem *) pLsaInfo, NULL)) != NULL);

            break;
        }
    }

    /* Print As Scope External Lsas */
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

    do
    {
        if (pLsaInfo != NULL)
        {
            MEMCPY (&LsaInfo, pLsaInfo, sizeof (tV3OsLsaInfo));
            if (LsaInfo.lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
            {
                if (u4LsaDispType == OSPFV3_LSA_ADV_ROUTER)
                {
                    if ((V3UtilRtrIdComp
                         (LsaInfo.lsaId.advRtrId,
                          V3_LS_ADV_ROUTER_ID)) == OSPFV3_EQUAL)
                    {
                        if (V3OspfCliDisplayLsaSummary (CliHandle, pLsaInfo,
                                                        0) == CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }

                    }
                }
                else if (u4LsaDispType == OSPFV3_LSA_SELF_ORG)
                {
                    if ((V3UtilRtrIdComp
                         (LsaInfo.lsaId.advRtrId,
                          pV3OspfCxt->rtrId)) == OSPFV3_EQUAL)
                    {
                        if (V3OspfCliDisplayLsaSummary (CliHandle, pLsaInfo,
                                                        0) == CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }
                    }
                }

            }
        }

    }
    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                           (tRBElem *) pLsaInfo, NULL)) != NULL);

    return CLI_SUCCESS;
}

#endif /* __OSPF3CLI_C__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  ospf3cli.c                     */
/*-----------------------------------------------------------------------*/
