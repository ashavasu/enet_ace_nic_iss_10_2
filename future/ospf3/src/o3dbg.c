/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3dbg.c,v 1.6 2012/03/27 12:33:47 siva Exp $ 
 *
 * Description: This file contains routines used for debugging
*
 *******************************************************************/

#ifdef TRACE_WANTED
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     :  V3DbgOspfFormatTime                                       */
/*                                                                           */
/* Description  : Convet time value in seconds into HH:MM:SS format          */
/*                                                                           */
/* Input        : u4TimeSec : Time value in sec                              */
/*                                                                           */
/* Output       : pu1String : Formatted time in HH:MM:SS                     */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1      *
V3DbgOspfFormatTime (UINT4 u4TimeSec, UINT1 *pu1String)
{
    UINT4               u4Hour = 0;
    UINT4               u4Minute = 0;
    UINT4               u4Sec = 0;

    u4Hour = u4TimeSec / OSPFV3_SECS_IN_HOUR;
    u4Minute = (u4TimeSec - u4Hour * OSPFV3_SECS_IN_HOUR) / OSPFV3_SECS_IN_MIN;
    u4Sec = u4TimeSec % OSPFV3_SECS_IN_MIN;

    SPRINTF ((CHR1 *) pu1String, "%02u:%02u:%02u", u4Hour, u4Minute, u4Sec);

    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfPrefixOptionsDump                                 */
/*                                                                           */
/* Description  : Mapping the prefix option value to string format         */
/*                                                                           */
/* Input        : u1Options : Prefix otion value                     */
/*                                                                           */
/* Output       : pu1String : pointer to the string                  */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE CHR1       *
V3DbgOspfPrefixOptionsDump (UINT1 u1Options, CHR1 * pu1String)
{
    UINT1               u1Flag = OSIX_FALSE;

    /* P/MC/LA/NU bits */
    if (OSPFV3_BIT (u1Options, OSPFV3_P_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), "P");
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_BIT (u1Options, OSPFV3_LA_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "LA" : "/LA"));
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_BIT (u1Options, OSPFV3_NU_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "NU" : "/NU"));
    }

    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfOptionsDump                                       */
/*                                                                           */
/* Description  : Mapping the three bytes option value from Hello/DD packet  */
/*           or from Router/Network/Inter-area-router/Link Lsa to       */
/*           string format.                            */
/*                                                                           */
/* Input        : options   : Option value                         */
/*                                                                           */
/* Output       : pu1String : pointer to the string                  */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE CHR1       *
V3DbgOspfOptionsDump (tV3OsOptions options, CHR1 * pu1String)
{
    UINT1               u1Flag = OSIX_FALSE;

    if (OSPFV3_OPT_ISSET (options, OSPFV3_DC_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), "DC");
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_OPT_ISSET (options, OSPFV3_R_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "R" : "/R"));
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_OPT_ISSET (options, OSPFV3_N_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "N" : "/N"));
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_OPT_ISSET (options, OSPFV3_MC_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "MC" : "/MC"));
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_OPT_ISSET (options, OSPFV3_E_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "E" : "/E"));
        u1Flag = OSIX_TRUE;
    }
    if (OSPFV3_OPT_ISSET (options, OSPFV3_V6_BIT_MASK))
    {
        SPRINTF (OSPFV3BUF (pu1String), (u1Flag == OSIX_FALSE ? "V6" : "/V6"));
    }
    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfRouterLinkTypeDump                                */
/*                                                                           */
/* Description  : Mapping the Link Type value of Router Lsa to string format */
/*                                                                           */
/* Input        : u1Lnktype : Link Type                          */
/*                                                                           */
/* Output       : pu1String : pointer to the string having name of Link Type*/
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1      *
V3DbgOspfRouterLinkTypeDump (UINT1 u1Lnktype, UINT1 *pu1String)
{
    switch (u1Lnktype)
    {
        case OSPFV3_PTOP_LINK:
            SPRINTF ((CHR1 *) pu1String, "Point-to-Point (%u)", u1Lnktype);
            break;

        case OSPFV3_TRANSIT_LINK:
            SPRINTF ((CHR1 *) pu1String, "Connection to Transit Network (%u)",
                     u1Lnktype);
            break;

        case OSPFV3_VIRTUAL_LINK:
            SPRINTF ((CHR1 *) pu1String, "Virtual Link (%u)", u1Lnktype);
            break;

        default:
            SPRINTF ((CHR1 *) pu1String, "*Unknown OSPF Router Link type (%u)*",
                     u1Lnktype);
            break;
    }
    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfLSTypeString                                      */
/*                                                                           */
/* Description  : Maps the LS Type value to string format.                   */
/*                                                                           */
/* Input        : u2LStype  : Type of the LSA                         */
/*                                                                           */
/* Output       : pu1String : pointer to the string having name of LSA         */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1      *
V3DbgOspfLSTypeString (UINT2 u2LStype, UINT1 *pu1String)
{
    switch (u2LStype)
    {
        case OSPFV3_ROUTER_LSA:
            SPRINTF ((CHR1 *) pu1String, "Router LSA");
            break;
        case OSPFV3_NETWORK_LSA:
            SPRINTF ((CHR1 *) pu1String, "Network LSA");
            break;
        case OSPFV3_INTER_AREA_PREFIX_LSA:
            SPRINTF ((CHR1 *) pu1String, "Inter-Area Prefix LSA ");
            break;
        case OSPFV3_INTER_AREA_ROUTER_LSA:
            SPRINTF ((CHR1 *) pu1String, "Inter-Area Router LSA");
            break;
        case OSPFV3_AS_EXT_LSA:
            SPRINTF ((CHR1 *) pu1String, "AS External LSA");
            break;
        case OSPFV3_NSSA_LSA:
            SPRINTF ((CHR1 *) pu1String, "NSSA AS External LSA");
            break;
        case OSPFV3_LINK_LSA:
            SPRINTF ((CHR1 *) pu1String, "Link LSA");
            break;
        case OSPFV3_INTRA_AREA_PREFIX_LSA:
            SPRINTF ((CHR1 *) pu1String, "Intra-Area Prefix LSA");
            break;
        default:
            SPRINTF ((CHR1 *) pu1String, "*Unknown OSPFV3 LS type 0x%04x*",
                     u2LStype);
            break;
    }

    return pu1String;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfLSAdvtDump                         */
/*                                                                           */
/* Description  : Dumps the contents of the Lsa.                        */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1LsaHdr   : Flag to indicate dumping Lsa header only      */
/*                            or Lsa                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfLSAdvtDump (UINT1 *pBuf,
                     CHR1 * pu1LogBuf, INT4 i4DataLen, UINT1 u1LsaHdr)
{
    tV3OsOptions        options;
    tIp6Addr            ip6Addr;
    UINT1              *pu1Metric = NULL;
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPFV3_MAX_LOG_STR_LEN + 1];
    UINT2               u2LsaPktLen = 0;
    UINT2               u2LsaOffset = 0;
    UINT4               u4HdrCnt = 0;
    UINT2               u2LsType;
    UINT1               u1Flag = OSIX_FALSE;
    UINT1               u1PfxLen = 0;
    UINT1               u1TBitFlag = OSIX_FALSE;
    UINT1               u1FBitFlag = OSIX_FALSE;
    UINT2               u2RefLsType;
    UINT4               u4NoOfPfxs;
    UINT4               u4PfxCnt;
    UINT2               u2LsaLen = 0;
    UINT2               u2DataReadLen = 0;
    UINT1              *pu1TmpPtr = NULL;

    au1TmpBuf[0] = '\0';
    pu1TmpPtr = au1TmpBuf;

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    /* Extract the packet type */
    OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_TYPE_OFFSET, u1TmpData);

    if (u1TmpData == OSPFV3_DD_PKT)
    {
        u2LsaOffset = OSPFV3_DDP_SUMMARY_LST_OFFSET;
    }
    else if (u1TmpData == OSPFV3_LS_UPDATE_PKT)
    {
        u2LsaOffset = OSPFV3_LSU_LSA_OFFSET;
    }
    else if (u1TmpData == OSPFV3_LSA_ACK_PKT)
    {
        u2LsaOffset = OSPFV3_PKT_HEADER_LEN;
    }
    /* Skip the Dump for other packets */
    else
    {
        u2LsaOffset = (UINT2) i4DataLen;
    }

    for (u4HdrCnt = 1; u2LsaOffset < i4DataLen; u4HdrCnt++)
    {
        /* Check for truncated LSA advertisements */
        if (i4DataLen < (u2LsaOffset + OSPFV3_LS_HEADER_SIZE))
        {
            break;
        }

        /* LS age */
        OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2TmpData);

        /* LS type */
        OSPFV3_BUFFER_GET_2_BYTE (pBuf,
                                  (u2LsaOffset + OSPFV3_AGE_SIZE_IN_LS_HEADER),
                                  u2LsType);

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: ---- OSPFV3 Link State Advertisement Header %u ----"
                 "\n"
                 "OSPFV3: LS Age: %u%s seconds\n",
                 u4HdrCnt,
                 u2TmpData, (u2TmpData >= OSPFV3_MAX_AGE) ? " (MaxAge)" : "");

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: LS Type: %s (%u)\n",
                 V3DbgOspfLSTypeString (u2LsType, au1TmpBuf), u2LsType);

        /* Link State ID */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                  (u2LsaOffset + OSPFV3_AGE_SIZE_IN_LS_HEADER +
                                   OSPFV3_LS_TYPE_LEN),
                                  OSPFV3_LINKSTATE_ID_LEN);

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Link State ID: %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Advertising Router */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                  (u2LsaOffset + OSPFV3_AGE_SIZE_IN_LS_HEADER +
                                   OSPFV3_LS_TYPE_LEN +
                                   OSPFV3_LINKSTATE_ID_LEN), OSPFV3_RTR_ID_LEN);

        /* LS sequence number */
        OSPFV3_BUFFER_GET_4_BYTE (pBuf,
                                  (u2LsaOffset +
                                   OSPFV3_LS_SEQ_NUM_OFFSET_IN_LS_HEADER),
                                  u4TmpData);

        /* LS checksum */
        OSPFV3_BUFFER_GET_2_BYTE (pBuf,
                                  (u2LsaOffset +
                                   OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER),
                                  u2TmpData);

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Advertising Router: %d.%d.%d.%d\n"
                 "OSPFV3: LS Sequence Number: 0x%08x\n"
                 "OSPFV3: LS Checksum: 0x%04X\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3],
                 u4TmpData, u2TmpData);

        /* LSA length */
        OSPFV3_BUFFER_GET_2_BYTE (pBuf,
                                  (u2LsaOffset +
                                   OSPFV3_LENGTH_OFFSET_IN_LS_HEADER),
                                  u2LsaPktLen);
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Length: %u bytes\n" "OSPFV3:\n", u2LsaPktLen);

        /* Update the ReadOffset with bytes read */
        u2LsaOffset += OSPFV3_LS_HEADER_SIZE;

        /* Reset buffer after displaying, to avoid buffer space problems */
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
        au1TmpBuf[0] = '\0';

        /* If only LS header is to be dumped, break now */
        if (u1LsaHdr == OSIX_TRUE)
        {
            continue;
        }

        /* Adjust the packet length with bytes read */
        if (u2LsaPktLen >= OSPFV3_LS_HEADER_SIZE)
        {
            u2LsaPktLen -= OSPFV3_LS_HEADER_SIZE;
        }
        else
        {
            u2LsaPktLen = 0;
        }

        if (STRLEN (pu1LogBuf) != 0)
        {
            /* Reset buffer after displaying */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }

        /* parse the LSA contents */

        if ((u2LsType == OSPFV3_ROUTER_LSA) &&
            (i4DataLen >= (u2LsaOffset + OSPFV3_ROUTER_LSA_HDR_LEN)) &&
            (u2LsaPktLen >= OSPFV3_ROUTER_LSA_HDR_LEN))
        {

            /* Flags Nt/-/V/E/B */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;

            u1Flag = OSIX_FALSE;
            if (OSPFV3_BIT (u1TmpData, OSPFV3_NT_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("Nt")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr), "Nt");
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_V_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/V")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF (OSPFV3BUF ((CHR1 *) pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "V" : "/V"));
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_ASBR_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/E")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF (OSPFV3BUF ((CHR1 *) pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "E" : "/E"));
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_ABR_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/B")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF (OSPFV3BUF ((CHR1 *) pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "B" : "/B"));
                }
            }
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Flags: 0x%02x  (%s)\n", u1TmpData, au1TmpBuf);
            au1TmpBuf[0] = '\0';

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, u2LsaOffset,
                                      OSPFV3_OPTION_LEN);
            u2LsaOffset += OSPFV3_OPTION_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Options: 0x%02X"
                     "  (%s)\n", options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));
            au1TmpBuf[0] = '\0';

            /* Reset buffer after displaying */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            u4TmpData = 0;

            for (u2LsaLen = 0; (u2LsaOffset < i4DataLen) &&
                 (u2LsaLen < u2LsaPktLen);)
            {
                /* Check for truncated LSA advertisements */
                if (i4DataLen < (u2LsaOffset + OSPFV3_RTR_LSA_LINK_LEN)
                    || (u2LsaPktLen < (u2LsaLen + OSPFV3_RTR_LSA_LINK_LEN)))
                {
                    break;
                }

                /* Link Type */
                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u2LsaOffset++;
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Link Type: %d (%s)\n",
                         u1TmpData,
                         V3DbgOspfRouterLinkTypeDump (u1TmpData, au1TmpBuf));
                au1TmpBuf[0] = '\0';

                /* Reserved word */
                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u2LsaOffset++;
                SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Reserved: %d \n",
                         u1TmpData);

                /* Metric */
                OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2TmpData);
                u2LsaOffset += OSPFV3_TWO_BYTES;

                /* Extract Interface Id */
                OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                u2LsaOffset += OSPFV3_FOUR_BYTES;
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Metric: %d \n"
                         "OSPFV3: Interface ID: %u\n", u2TmpData, u4TmpData);

                /* Extract Nbr Interface Id */
                OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                u2LsaOffset += OSPFV3_FOUR_BYTES;

                /* Nbr Router Id */
                OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                          OSPFV3_RTR_ID_LEN);
                u2LsaOffset += OSPFV3_RTR_ID_LEN;
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Neighbor Interface ID: %u\n"
                         "OSPFV3: Neighbor Router ID: %d.%d.%d.%d\n", u4TmpData,
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2],
                         au1TmpBuf[3]);

                u2LsaLen += OSPFV3_RTR_LSA_LINK_LEN;

                if (STRLEN (pu1LogBuf) != 0)
                {
                    /* Reset buffer after displaying */
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }

        }
        else if ((u2LsType == OSPFV3_NETWORK_LSA) &&
                 (i4DataLen >= (u2LsaOffset + OSPFV3_NETWORK_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPFV3_NETWORK_LSA_HDR_LEN))
        {
            /* Extract Reserved field */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPF Network Links Advertisement ----\n"
                     "OSPFV3: Reserved: %d\n", u1TmpData);

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, u2LsaOffset,
                                      OSPFV3_OPTION_LEN);
            u2LsaOffset += OSPFV3_OPTION_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Options: 0x%02X" "  (%s)\n",
                     options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));

            for (u4TmpData = OSPFV3_NETWORK_LSA_HDR_LEN;
                 ((u4TmpData < u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u4TmpData += OSPFV3_RTR_ID_LEN,
                 u2LsaOffset += OSPFV3_RTR_ID_LEN)
            {
                /* Check for malformed Router link tuples */
                if ((u2LsaPktLen < (u4TmpData + OSPFV3_RTR_ID_LEN)) ||
                    (i4DataLen < (u2LsaOffset + OSPFV3_RTR_ID_LEN)))
                {
                    break;
                }

                /* Attached Router */
                OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                          u2LsaOffset, OSPFV3_RTR_ID_LEN);

                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Attached Router: %d.%d.%d.%d\n",
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2],
                         au1TmpBuf[3]);

                if (STRLEN (pu1LogBuf) != 0)
                {
                    /* Reset buffer after displaying */
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else if ((u2LsType == OSPFV3_INTER_AREA_PREFIX_LSA)
                 && (i4DataLen >= (u2LsaOffset +
                                   OSPFV3_INTER_AREA_PFX_LSA_HDR_LEN))
                 && (u2LsaPktLen >= OSPFV3_INTER_AREA_PFX_LSA_HDR_LEN))
        {
            /* Reserved word */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPFV3 Inter Area Prefix LSA ----\n"
                     "OSPFV3: Reserved: %d\n", u1TmpData);

            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_BIG_METRIC_SIZE);
            u2LsaOffset += OSPFV3_BIG_METRIC_SIZE;

            /* Extract the 3 byte Metric portion */
            pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
            u4TmpData = OSPFV3_LGET3BYTE (pu1Metric);
            au1TmpBuf[0] = '\0';

            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Metric: %u\n", u4TmpData);

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Prefix Length: %d (Bits)\n", u1TmpData);

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2TmpData);
            u2LsaOffset += OSPFV3_TWO_BYTES;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Prefix Options: 0x%02x (%s)\n"
                     "OSPFV3: Reserved: %d\n", u1TmpData,
                     V3DbgOspfPrefixOptionsDump (u1TmpData, (CHR1 *) au1TmpBuf),
                     u2TmpData);

            if (u1PfxLen != 0)
            {
                MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
                OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset +
                                          OSPFV3_INTER_AREA_PREF_OFFSET,
                                          u1PfxLen);
                u2LsaOffset += u1PfxLen;
                SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Prefix: %s\n",
                         Ip6PrintAddr (&ip6Addr));
            }
            else
            {
                SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Default Route\n");
            }

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }

        }
        else if ((u2LsType == OSPFV3_INTER_AREA_ROUTER_LSA)
                 && (i4DataLen >= (u2LsaOffset +
                                   OSPFV3_INTER_AREA_RTR_LSA_HDR_LEN))
                 && (u2LsaPktLen >= OSPFV3_INTER_AREA_RTR_LSA_HDR_LEN))
        {
            /* Reserved word */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPFV3 Inter Area Router LSA ----\n"
                     "OSPFV3: Reserved: %d\n", u1TmpData);

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, u2LsaOffset,
                                      OSPFV3_OPTION_LEN);
            u2LsaOffset += OSPFV3_OPTION_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Options: 0x%02X"
                     "  (%s)\n", options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));

            /* Reserved word */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Reserved: %d\n",
                     u1TmpData);

            /* Metric */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_BIG_METRIC_SIZE);
            u2LsaOffset += OSPFV3_BIG_METRIC_SIZE;

            /* Extract the 3 byte Metric portion */
            pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
            u4TmpData = OSPFV3_LGET3BYTE (pu1Metric);

            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Metric: %u\n", u4TmpData);

            /* Designated Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_RTR_ID_LEN);
            u2LsaOffset += OSPFV3_RTR_ID_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Destination Router ID: %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }

        }
        else if ((u2LsType == OSPFV3_AS_EXT_LSA) &&
                 (i4DataLen >= (u2LsaOffset + OSPFV3_AS_EXTERN_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPFV3_AS_EXTERN_LSA_HDR_LEN))
        {
            u2DataReadLen = u2LsaOffset;

            /* E/F/T bits */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;

            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_E_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("E")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr), "E");
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_F_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/F")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "F" : "/F"));
                }
                u1Flag = OSIX_TRUE;
                u1FBitFlag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_T_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/T")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF (OSPFV3BUF ((CHR1 *) pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "T" : "/T"));
                }
                u1TBitFlag = OSIX_TRUE;
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPF AS External Link Advertisement ----\n"
                     "OSPFV3: Flags: 0x%02X  (%s)\n", u1TmpData, au1TmpBuf);

            /* Metric */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_BIG_METRIC_SIZE);
            u2LsaOffset += OSPFV3_BIG_METRIC_SIZE;

            /* Extract the 3 byte Metric portion */
            pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
            u4TmpData = OSPFV3_LGET3BYTE (pu1Metric);
            au1TmpBuf[0] = '\0';

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
            u2LsaOffset++;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Metric : %d\n"
                     "OSPFV3: Prefix Length: %d (Bits)\n",
                     u2TmpData, u1TmpData);

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;

            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2RefLsType);
            u2LsaOffset += OSPFV3_LS_TYPE_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Prefix Options: 0x%02x (%s)\n"
                     "OSPFV3: Referenced LS Type: %d\n", u1TmpData,
                     V3DbgOspfPrefixOptionsDump (u1TmpData, (CHR1 *) au1TmpBuf),
                     u2RefLsType);

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset, u1PfxLen);
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Prefix: %s\n",
                     Ip6PrintAddr (&ip6Addr));
            u2LsaOffset += u1PfxLen;

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            if ((u1FBitFlag == OSIX_TRUE) && (u2LsaPktLen >=
                                              (u2LsaOffset - u2DataReadLen +
                                               OSPFV3_IPV6_ADDR_LEN)))
            {
                /* Forwarding address */
                OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset,
                                          OSPFV3_IPV6_ADDR_LEN);
                u2LsaOffset += OSPFV3_IPV6_ADDR_LEN;
            }
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Forwarding address: %s\n",
                     Ip6PrintAddr (&ip6Addr));

            u4TmpData = 0;
            if ((u1TBitFlag == OSIX_TRUE) && (u1FBitFlag == OSIX_TRUE))
            {
                if ((u2LsaPktLen >= (u2LsaOffset - u2DataReadLen +
                                     OSPFV3_FOUR_BYTES)))
                {

                    /* External Route Tag */
                    OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                    u2LsaOffset += OSPFV3_FOUR_BYTES;
                }
            }
            else if (u1TBitFlag == OSIX_TRUE)
            {
                if (u2LsaPktLen >= (u2LsaOffset - u2DataReadLen +
                                    OSPFV3_FOUR_BYTES))
                {

                    /* External Route Tag */
                    OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                    u2LsaOffset += OSPFV3_FOUR_BYTES;
                }
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: External Route Tag: 0x%08x\n", u4TmpData);

            if (u2RefLsType == 0)
            {
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Referenced LS ID: Not Included");
            }
            else
            {
                /* Link State ID */
                OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                          OSPFV3_LINKSTATE_ID_LEN);
                u2LsaOffset += OSPFV3_LINKSTATE_ID_LEN;

                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Link State ID: %d.%d.%d.%d\n",
                         au1TmpBuf[0], au1TmpBuf[1],
                         au1TmpBuf[2], au1TmpBuf[3]);
            }

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
                au1TmpBuf[0] = '\0';
            }

        }
        else if ((u2LsType == OSPFV3_NSSA_LSA) &&
                 (i4DataLen >= (u2LsaOffset + OSPFV3_AS_EXTERN_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPFV3_AS_EXTERN_LSA_HDR_LEN))
        {
            u2DataReadLen = u2LsaOffset;

            /* E/F/T bits */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_E_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("E")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr), "E");
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_F_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/F")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "F" : "/F"));
                }
                u1Flag = OSIX_TRUE;
                u1FBitFlag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_EXT_T_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/T")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "T" : "/T"));
                }
                u1TBitFlag = OSIX_TRUE;
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPFV3 NSSA Link Advertisement ----\n"
                     "OSPFV3: Flags: 0x%02X  (%s)", u1TmpData, au1TmpBuf);

            /* Metric */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_BIG_METRIC_SIZE);
            u2LsaOffset += OSPFV3_BIG_METRIC_SIZE;

            pu1Metric = (UINT1 *) &(au1TmpBuf[1]);
            u4TmpData = OSPFV3_LGET3BYTE (pu1Metric);
            au1TmpBuf[0] = '\0';

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;
            u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Metric : %d\n"
                     "OSPFV3: Prefix Length: %d (Bits)\n",
                     u2TmpData, u1TmpData);

            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;

            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2RefLsType);
            u2LsaOffset += OSPFV3_LS_TYPE_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Prefix Options: 0x%02x (%s)\n"
                     "OSPFV3: Referenced LS Type: %d\n", u1TmpData,
                     V3DbgOspfPrefixOptionsDump (u1TmpData, (CHR1 *) au1TmpBuf),
                     u2RefLsType);

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset, u1PfxLen);
            u2LsaOffset += u1PfxLen;
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Prefix: %s\n",
                     Ip6PrintAddr (&ip6Addr));

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            if ((u1FBitFlag == OSIX_TRUE) && (u2LsaPktLen >=
                                              (u2LsaOffset - u2DataReadLen +
                                               OSPFV3_IPV6_ADDR_LEN)))
            {
                /* Forwarding address */
                OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset,
                                          OSPFV3_IPV6_ADDR_LEN);
                u2LsaOffset += OSPFV3_IPV6_ADDR_LEN;
            }
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Forwarding address: %s\n",
                     Ip6PrintAddr (&ip6Addr));

            u4TmpData = 0;
            if ((u1TBitFlag == OSIX_TRUE) && (u1FBitFlag == OSIX_TRUE))
            {
                if ((u2LsaPktLen >= (u2LsaOffset - u2DataReadLen +
                                     OSPFV3_FOUR_BYTES)))
                {

                    /* External Route Tag */
                    OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                    u2LsaOffset += OSPFV3_FOUR_BYTES;
                }
            }
            else if (u1TBitFlag == OSIX_TRUE)
            {
                if ((u2LsaPktLen >= (u2LsaOffset - u2DataReadLen +
                                     OSPFV3_FOUR_BYTES)))
                {

                    /* External Route Tag */
                    OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4TmpData);
                    u2LsaOffset += OSPFV3_FOUR_BYTES;
                }
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: External Route Tag    : 0x%08x\n", u4TmpData);

            if (u2RefLsType == 0)
            {
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "Referenced LS ID: Not Included");
            }
            else
            {
                /* Link State ID */
                OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                          u2LsaOffset, OSPFV3_LINKSTATE_ID_LEN);
                u2LsaOffset += OSPFV3_LINKSTATE_ID_LEN;

                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Link State ID: %d.%d.%d.%d\n",
                         au1TmpBuf[0], au1TmpBuf[1],
                         au1TmpBuf[2], au1TmpBuf[3]);
            }

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
                au1TmpBuf[0] = '\0';
            }

        }
        else if ((u2LsType == OSPFV3_LINK_LSA) &&
                 (i4DataLen >= (u2LsaOffset + OSPFV3_LINK_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPFV3_LINK_LSA_HDR_LEN))
        {
            /* Router priority */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
            u2LsaOffset++;

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, u2LsaOffset,
                                      OSPFV3_OPTION_LEN);
            u2LsaOffset += OSPFV3_OPTION_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Router Priority: %d\n"
                     "OSPFV3: Options: 0x%02X"
                     "  (%s)\n", u1TmpData, options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));
            au1TmpBuf[0] = '\0';

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset,
                                      OSPFV3_IPV6_ADDR_LEN);
            u2LsaOffset += OSPFV3_IPV6_ADDR_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Link Local Address: %s\n",
                     Ip6PrintAddr (&ip6Addr));

            /* Number of Prefixes */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, u2LsaOffset, u4NoOfPfxs);
            u2LsaOffset += OSPFV3_FOUR_BYTES;
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Number of Prefixes: %u\n",
                     u4NoOfPfxs);

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }

            for (u4PfxCnt = 0; (u4PfxCnt < u4NoOfPfxs) &&
                 (u2LsaOffset < i4DataLen); u4PfxCnt++)
            {
                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u2LsaOffset++;
                u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Prefix Length: %d (Bits)\n", u1TmpData);

                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u2LsaOffset++;

                OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2TmpData);
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Prefix Options: 0x%02x (%s)\n"
                         "OSPFV3: Reserved: %d\n", u1TmpData,
                         V3DbgOspfPrefixOptionsDump (u1TmpData,
                                                     (CHR1 *) au1TmpBuf),
                         u2TmpData);
                u2LsaOffset += OSPFV3_TWO_BYTES;
                au1TmpBuf[0] = '\0';

                MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
                OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset,
                                          u1PfxLen);
                u2LsaOffset += u1PfxLen;
                SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Prefix: %s\n",
                         Ip6PrintAddr (&ip6Addr));

                if (STRLEN (pu1LogBuf) != 0)
                {
                    /* Reset buffer after displaying */
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }

            }

        }
        else if ((u2LsType == OSPFV3_INTRA_AREA_PREFIX_LSA) &&
                 (i4DataLen >= (u2LsaOffset + (UINT2)
                                OSPFV3_INTRA_AREA_PFX_LSA_HDR_LEN)) &&
                 (u2LsaPktLen >= OSPFV3_INTRA_AREA_PFX_LSA_HDR_LEN))
        {
            /* Number of Prefixes */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u4NoOfPfxs);
            u2LsaOffset += OSPFV3_TWO_BYTES;
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Number of Prefixes: %u\n",
                     u4NoOfPfxs);

            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2RefLsType);
            u2LsaOffset += OSPFV3_TWO_BYTES;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Referenced LS Type: 0x%04x (%s)\n", u2RefLsType,
                     V3DbgOspfPrefixOptionsDump ((UINT1) u2RefLsType,
                                                 (CHR1 *) au1TmpBuf));

            /* Link State ID */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2LsaOffset,
                                      OSPFV3_LINKSTATE_ID_LEN);
            u2LsaOffset += OSPFV3_LINKSTATE_ID_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Referenced Link State ID: %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Advertising Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                      u2LsaOffset, OSPFV3_RTR_ID_LEN);
            u2LsaOffset += OSPFV3_RTR_ID_LEN;
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Referenced Advertise Rtr ID: %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);
            au1TmpBuf[0] = '\0';

            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }

            for (u4PfxCnt = 0; (u4PfxCnt < u4NoOfPfxs) &&
                 (u2LsaOffset < i4DataLen); u4PfxCnt++)
            {
                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u1PfxLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1TmpData));
                u2LsaOffset++;
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Prefix Length: %d (Bits)\n", u1TmpData);

                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);
                u2LsaOffset++;

                OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2LsaOffset, u2TmpData);
                u2LsaOffset += OSPFV3_TWO_BYTES;
                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Prefix Options: 0x%02x (%s)\n"
                         "OSPFV3: Metric: %d\n", u1TmpData,
                         V3DbgOspfPrefixOptionsDump (u1TmpData,
                                                     (CHR1 *) au1TmpBuf),
                         u2TmpData);
                au1TmpBuf[0] = '\0';

                MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
                OSPFV3_BUFFER_GET_STRING (pBuf, &ip6Addr, u2LsaOffset,
                                          u1PfxLen);
                u2LsaOffset += u1PfxLen;
                SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Prefix: %s\n",
                         Ip6PrintAddr (&ip6Addr));
                if (STRLEN (pu1LogBuf) != 0)
                {
                    /* Reset buffer after displaying */
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }

            }

        }
        else
        {
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- Unknown OSPF Link State Advertisement ----\n"
                     "OSPFV3:");

            for (u2TmpData = 1;
                 ((u2TmpData <= u2LsaPktLen) && (u2LsaOffset < i4DataLen));
                 u2TmpData++, u2LsaOffset++)
            {
                OSPFV3_BUFFER_GET_1_BYTE (pBuf, u2LsaOffset, u1TmpData);

                SPRINTF (OSPFV3BUF (pu1LogBuf), " %02X", u1TmpData);

                if (!(u2TmpData % OSPFV3_FOUR_BYTES))
                {
                    SPRINTF (OSPFV3BUF (pu1LogBuf), " ");
                }

                if ((!(u2TmpData % OSPFV3_NO_OF_BYTE_PER_LINE)) ||
                    (u2TmpData == u2LsaPktLen))
                {
                    SPRINTF (OSPFV3BUF (pu1LogBuf), "\nOSPFV3:");
                }

                if (STRLEN (pu1LogBuf))
                {
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        SPRINTF (OSPFV3BUF (pu1LogBuf), "\n");

        if (STRLEN (pu1LogBuf) != 0)
        {
            /* Reset buffer after displaying */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }
    }

    if (STRLEN (pu1LogBuf) != 0)
    {
        /* Reset buffer after displaying */
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfHelloDump                                         */
/*                                                                           */
/* Description  : Dumps the contents of the Hello Packet.                    */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1DumpLevel: LOW or HIGH Dump                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfHelloDump (UINT1 *pBuf, CHR1 * pu1LogBuf,
                    INT4 i4DataLen, UINT1 u1DumpLevel)
{
    tV3OsOptions        options;
    UINT1               u1TmpData = 0;
    UINT2               u2TmpData = 0;
    UINT4               u4TmpData = 0;
    UINT1               au1TmpBuf[OSPFV3_MAX_LOG_STR_LEN + 1];

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (i4DataLen >= (OSPFV3_PKT_HEADER_LEN + OSPFV3_HELLO_FIXED_PORTION_SIZE))
    {
        if (u1DumpLevel == OSPFV3_LOW_DUMP)
        {
            /* Extract Interface Id */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, OSPFV3_HP_INTERFACE_ID_OFFSET,
                                      u4TmpData);

            /* Router priority */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_HP_RTR_PRI_OFFSET,
                                      u1TmpData);

            SPRINTF ((OSPFV3BUF (pu1LogBuf)),
                     "OSPFV3: ------ Hello Packet ------\n"
                     "OSPFV3: Interface ID: %u \n"
                     "OSPFV3: Router Priority: %d \n", u4TmpData, u1TmpData);

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, OSPFV3_HP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Options: 0x%02X"
                     "  (%s)\n", options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));

            /* Hello interval */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_HP_HELLO_INTERVAL_OFFSET,
                                      u2TmpData);
            SPRINTF ((OSPFV3BUF (pu1LogBuf)),
                     "OSPFV3: Hello Interval: %d (seconds)\n", u2TmpData);

            /*Router Dead Interval */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_HP_RTR_DEAD_INTERVAL_OFFSET,
                                      u2TmpData);
            SPRINTF ((OSPFV3BUF (pu1LogBuf)),
                     "OSPFV3: Rtr Dead Interval: %d (seconds)\n", u2TmpData);

            /* Designated Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                      OSPFV3_HP_DSG_RTR_OFFSET,
                                      OSPFV3_RTR_ID_LEN);
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Designated Router: %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Backup Designated Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                      OSPFV3_HP_BK_DSG_RTR_OFFSET,
                                      OSPFV3_RTR_ID_LEN);
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Backup Designated Router: %d.%d.%d.%d\n",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Reset buffer after displaying, to avoid buffer space problems */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            au1TmpBuf[0] = '\0';

            for (u2TmpData = OSPFV3_HP_NBR_ID_OFFSET, u4TmpData = 1;
                 u2TmpData < i4DataLen;
                 u2TmpData += OSPFV3_RTR_ID_LEN, u4TmpData++)
            {
                if (i4DataLen < (u2TmpData + OSPFV3_RTR_ID_LEN))
                {
                    break;
                }

                /* Neighbors... */
                OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, u2TmpData,
                                          OSPFV3_RTR_ID_LEN);

                SPRINTF (OSPFV3BUF (pu1LogBuf),
                         "OSPFV3: Active Neighbor: %d.%d.%d.%d\n",
                         au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2],
                         au1TmpBuf[3]);

                if (STRLEN (pu1LogBuf) != 0)
                {
                    OSPFV3_TRC_PRINTF (pu1LogBuf);
                    pu1LogBuf[0] = '\0';
                }
            }
        }
        else                    /* DumpLevel == OSPFV3_HIGH_DUMP */
        {
            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, OSPFV3_HP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);
            SPRINTF (OSPFV3BUF (pu1LogBuf), "(%s) ",
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));

            /* Router priority */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_HP_RTR_PRI_OFFSET,
                                      u1TmpData);

            /* Designated Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                      OSPFV3_HP_DSG_RTR_OFFSET,
                                      OSPFV3_RTR_ID_LEN);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "\nPri:%u "
                     "DR:%d.%d.%d.%d ", u1TmpData,
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Backup Designated Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf,
                                      OSPFV3_HP_BK_DSG_RTR_OFFSET,
                                      OSPFV3_RTR_ID_LEN);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "BDR:%d.%d.%d.%d ",
                     au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

            /* Neighbors... */
            u4TmpData = 0;
            for (u2TmpData = OSPFV3_HP_NBR_ID_OFFSET; u2TmpData < i4DataLen;
                 u2TmpData += OSPFV3_RTR_ID_LEN)
            {
                if (i4DataLen < (u2TmpData + OSPFV3_RTR_ID_LEN))
                {
                    break;
                }
                ++u4TmpData;
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf), "Nbrs:%u ", u4TmpData);
            if (STRLEN (pu1LogBuf) != 0)
            {
                /* Reset buffer after displaying */
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }
        }
    }

    if (STRLEN (pu1LogBuf) != 0)
    {
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfDdpDump                                           */
/*                                                                           */
/* Description  : Dumps the contents of the DD Packet.                       */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1DumpLevel: LOW or HIGH Dump                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3DbgOspfDdpDump (UINT1 *pBuf, CHR1 * pu1LogBuf,
                  INT4 i4DataLen, UINT1 u1DumpLevel)
{
    tV3OsOptions        options;
    UINT1               u1TmpData;
    UINT2               u2TmpData;
    UINT4               u4TmpData;
    UINT1               au1TmpBuf[OSPFV3_MAX_LOG_STR_LEN + 1];
    UINT1               u1Flag = OSIX_FALSE;
    UINT1              *pu1TmpPtr = NULL;

    au1TmpBuf[0] = '\0';
    pu1TmpPtr = au1TmpBuf;

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (i4DataLen >= (OSPFV3_PKT_HEADER_LEN + OSPFV3_DDP_FIXED_PORTION_SIZE))
    {
        if (u1DumpLevel == OSPFV3_LOW_DUMP)
        {
            /* Reserved */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_PKT_HEADER_LEN, u1TmpData);

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, OSPFV3_DDP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPFV3 Database Description ----\n"
                     "OSPFV3: Reserved: %d\n"
                     "OSPFV3: Options: 0x%02X  (%s)\n",
                     u1TmpData, options[OSPFV3_OPT_BYTE_THREE],
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));
            au1TmpBuf[0] = '\0';

            /* Interface MTU */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_DDP_IFACE_MTU_OFFSET,
                                      u2TmpData);
            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Interface MTU: %d (bytes)\n", u2TmpData);

            /* Reserved */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_DDP_RSVD_OFFSET, u1TmpData);
            SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Reserved: %d\n",
                     u1TmpData);

            /* I/M/MS bits */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_DDP_MASK_OFFSET, u1TmpData);
            if (OSPFV3_BIT (u1TmpData, OSPFV3_I_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("I")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr), "I");
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_M_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/M")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "M" : "/M"));
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_MS_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/MS")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "MS" : "/MS"));
                }
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: Flags: 0x%02x  (%s)\n", u1TmpData, au1TmpBuf);

            /* DD Sequence number */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, OSPFV3_DDP_SEQ_NUM_OFFSET,
                                      u4TmpData);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: DD Sequence Number: 0x%08x\n", u4TmpData);

            /* Reset buffer after displaying, to avoid buffer space problems */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
            au1TmpBuf[0] = '\0';

            V3DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSIX_TRUE);
        }
        else                    /* DumpLevel == OSPFV3_HIGH_DUMP */
        {
            /* Interface MTU */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_DDP_IFACE_MTU_OFFSET,
                                      u2TmpData);

            /* Options */
            OSPFV3_BUFFER_GET_STRING (pBuf, options, OSPFV3_DDP_OPTIONS_OFFSET,
                                      OSPFV3_OPTION_LEN);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "MTU:%u \n"
                     "(%s) ", u2TmpData,
                     V3DbgOspfOptionsDump (options, (CHR1 *) au1TmpBuf));
            au1TmpBuf[0] = '\0';

            /* DD Sequence number */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, OSPFV3_DDP_SEQ_NUM_OFFSET,
                                      u4TmpData);

            /* I/M/MS bits */
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_DDP_MASK_OFFSET, u1TmpData);

            if (OSPFV3_BIT (u1TmpData, OSPFV3_I_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("I")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr), "I");
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_M_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/M")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "M" : "/M"));
                }
                u1Flag = OSIX_TRUE;
            }
            if (OSPFV3_BIT (u1TmpData, OSPFV3_MS_BIT_MASK))
            {
                if ((STRLEN (pu1TmpPtr) + STRLEN ("/MS")) <=
                    OSPFV3_MAX_LOG_STR_LEN)
                {
                    SPRINTF ((CHR1 *) OSPFV3BUF (pu1TmpPtr),
                             (u1Flag == OSIX_FALSE ? "MS" : "/MS"));
                }
            }

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "(%s) DD Seq#: 0x%08x", au1TmpBuf, u4TmpData);

            /* Reset buffer after displaying */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            au1TmpBuf[0] = '\0';
            pu1LogBuf[0] = '\0';
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfLSRequestDump                     */
/*                                                                           */
/* Description  : Dumps the contents of the Ls Request Packet.               */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1DumpLevel: LOW or HIGH Dump                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfLSRequestDump (UINT1 *pBuf, CHR1 * pu1LogBuf,
                        INT4 i4DataLen, UINT1 u1DumpLevel)
{
    UINT1               au1TmpBuf[OSPFV3_MAX_LOG_STR_LEN];
    UINT1               au1LsId[OSPFV3_LINKSTATE_ID_LEN + 1];
    UINT1               au1AdvRtr[OSPFV3_RTR_ID_LEN + 1];
    UINT2               u2DataOffset = 0;    /* Data offset in buffer */
    UINT2               u2DataCnt = 0;    /* Pckt Length counter */
    UINT2               u2TmpData = 0;

    au1TmpBuf[0] = '\0';

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (u1DumpLevel == OSPFV3_LOW_DUMP)
    {
        for (u2DataCnt = 1, u2DataOffset = OSPFV3_PKT_HEADER_LEN;
             u2DataOffset < i4DataLen; u2DataCnt++)
        {
            /* Check for malformed packets */
            if (i4DataLen < (u2DataOffset + OSPFV3_LSA_REQ_SIZE))
            {
                break;
            }

            /* Extract Reserved field */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2DataOffset, u2TmpData);
            u2DataOffset += OSPFV3_TWO_BYTES;

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: ---- OSPFV3 Link State Request %d ----\n"
                     "OSPFV3: Reserved: %d\n", u2DataCnt, u2TmpData);

            /* LS Type */
            OSPFV3_BUFFER_GET_2_BYTE (pBuf, u2DataOffset, u2TmpData);
            u2DataOffset += OSPFV3_LS_TYPE_LEN;

            /* Link State ID */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1LsId,
                                      u2DataOffset, OSPFV3_LINKSTATE_ID_LEN);
            u2DataOffset += OSPFV3_LINKSTATE_ID_LEN;

            /* Advertising Router */
            OSPFV3_BUFFER_GET_STRING (pBuf, au1AdvRtr,
                                      u2DataOffset, OSPFV3_RTR_ID_LEN);
            u2DataOffset += OSPFV3_RTR_ID_LEN;

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: LS Type: %s (%d)\n"
                     "OSPFV3: Link State ID: %d.%d.%d.%d\n"
                     "OSPFV3: Advertising Router: %d.%d.%d.%d\n",
                     V3DbgOspfLSTypeString (u2TmpData, au1TmpBuf), u2TmpData,
                     au1LsId[0], au1LsId[1], au1LsId[2], au1LsId[3],
                     au1AdvRtr[0], au1AdvRtr[1], au1AdvRtr[2], au1AdvRtr[3]);

            if (STRLEN (pu1LogBuf) != 0)
            {
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }
        }
    }
    else                        /* DumpLevel == OSPFV3_HIGH_DUMP */
    {
        u2DataCnt = 0;
        for (u2DataOffset = OSPFV3_PKT_HEADER_LEN; u2DataOffset < i4DataLen;
             u2DataOffset += OSPFV3_LSA_REQ_SIZE)
        {
            /* Check for malformed packets */
            if (i4DataLen < (u2DataOffset + OSPFV3_LSA_REQ_SIZE))
            {
                break;
            }
            ++u2DataCnt;
        }

        SPRINTF (OSPFV3BUF (pu1LogBuf), "# LSReq:%d ", u2DataCnt);
    }

    if (STRLEN (pu1LogBuf) != 0)
    {
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfLSUpdateDump                         */
/*                                                                           */
/* Description  : Dumps the contents of the Ls Update Packet.                */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1DumpLevel: LOW or HIGH Dump                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfLSUpdateDump (UINT1 *pBuf, CHR1 * pu1LogBuf,
                       INT4 i4DataLen, UINT1 u1DumpLevel)
{
    UINT4               u4TmpData;

    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    /* Check for malformed packets */
    if (i4DataLen >= (OSPFV3_PKT_HEADER_LEN + OSPFV3_LSU_FIXED_PORTION_SIZE))
    {
        if (u1DumpLevel == OSPFV3_LOW_DUMP)
        {
            /* # LSAs */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, OSPFV3_PKT_HEADER_LEN, u4TmpData);

            SPRINTF (OSPFV3BUF (pu1LogBuf),
                     "OSPFV3: # Advertisements: %u\n", u4TmpData);

            /* Display Content before LSA data */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';

            /* Parse the LSAs */
            V3DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSIX_FALSE);
        }
        else                    /* DumpLevel == OSPFV3_HIGH_DUMP */
        {
            /* # LSAs */
            OSPFV3_BUFFER_GET_4_BYTE (pBuf, OSPFV3_PKT_HEADER_LEN, u4TmpData);
            SPRINTF (OSPFV3BUF (pu1LogBuf), "#LSAs: %u ", u4TmpData);
        }
    }

    if (STRLEN (pu1LogBuf) != 0)
    {
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfLSAckDump                         */
/*                                                                           */
/* Description  : Dumps the contents of the Ls Ack Packet.                   */
/*                                                                           */
/* Input        : pBuf       : pointer to the buffer to be dumped            */
/*          pu1LogBuf  : pointer to the buffer which stores dumped     */
/*                     information                     */
/*                i4DataLen  : length of the information to be dumped        */
/*                u1DumpLevel: LOW or HIGH Dump                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfLSAckDump (UINT1 *pBuf, CHR1 * pu1LogBuf,
                    INT4 i4DataLen, UINT1 u1DumpLevel)
{
    if ((pBuf == NULL) || (pu1LogBuf == NULL))
    {
        return;
    }

    if (u1DumpLevel == OSPFV3_HIGH_DUMP)    /* Nothing to Dump */
    {
        return;
    }

    /* Check for malformed packets */
    if (i4DataLen >= (OSPFV3_PKT_HEADER_LEN + OSPFV3_LS_HEADER_SIZE))
    {
        /* Display Content before LSA data */
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';

        /* Parse the LSAs */
        V3DbgOspfLSAdvtDump (pBuf, pu1LogBuf, i4DataLen, OSIX_TRUE);
    }

    if (STRLEN (pu1LogBuf) != 0)
    {
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfPktAnalyser                                       */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : u1Direction : flag indicating outgoing or incoming message */
/*                u1DumpLevel : HexDump / Parsing / Highlevel                */
/*                pBuf      : pointer to the buffer to be dumped             */
/*                u2BufLen  : length of the information to be dumped         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DbgOspfPktAnalyser (UINT1 u1Direction, UINT1 u1DumpLevel,
                      UINT1 *pBuf, UINT2 u2BufLen)
{
    UINT4               u4ReadOffset = 0;
    INT4                i4DataLen = 0;    /* Eligible Pkt Data Length for dump */
    UINT1               u1TmpData = 0;
    UINT2               u2TmpData = 0;
    UINT1               u1PktType = 0;
    UINT4               u4RtrSysTime = 0;    /* Time elapsed since Router boot */
    UINT4               u4BufLength = 0;
    UINT1               au1TmpBuf[OSPFV3_MAX_LOG_STR_LEN + 1];
    static CHR1         au1LogBuf[OSPFV3_MAX_DUMP_BUF];
    CHR1               *pu1LogBuf = NULL;

    if (pBuf == NULL)
    {
        OSPFV3_TRC_PRINTF ("\nOSPFV3: Either invalid data buffer or"
                           " no data exists\n");
        return;
    }

    pu1LogBuf = au1LogBuf;

    pu1LogBuf[0] = '\0';
    au1TmpBuf[0] = '\0';

    /* Check the packet length */
    OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_PKT_LENGTH_OFFSET, u2TmpData);
    i4DataLen = (INT4) u2TmpData;    /* For avoiding the endian problems */
    u4BufLength = (UINT4) u2BufLen;

    if (u4BufLength < (UINT4) i4DataLen)
    {
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "\nOSPFV3: Warning: Truncated Packet\n");
        i4DataLen = (INT4) u4BufLength;
    }
    else if (u4BufLength > (UINT4) i4DataLen)
    {
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "\nOSPFV3: Warning: Packet length is shorter than "
                 "buffer length\n");
        i4DataLen = (INT4) u4BufLength;
    }

    u4RtrSysTime = OsixGetSysUpTime ();

    /* Extract the packet type */
    OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_TYPE_OFFSET, u1PktType);

    if ((u1DumpLevel == OSPFV3_LOW_DUMP) || (u1DumpLevel == OSPFV3_HEX_DUMP))
    {
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "\n\n"
                 "OSPFV3: Time: %-10s\t **** "
                 "%s\n%s",
                 V3DbgOspfFormatTime (u4RtrSysTime, au1TmpBuf),
                 ((u1Direction ==
                   OSPFV3_DIR_IN) ? "Received Packet" : "Sending Packet"),
                 ((u1DumpLevel ==
                   OSPFV3_HEX_DUMP) ? "" :
                  "OSPFV3: ---------------- OSPFV3 Packet ----------------\n"));

        switch (u1PktType)
        {
            case OSPFV3_HELLO_PKT:
                SPRINTF ((CHR1 *) au1TmpBuf, "OSPFV3 Hello");
                break;

            case OSPFV3_DD_PKT:
                SPRINTF ((CHR1 *) au1TmpBuf, "Database Description");
                break;

            case OSPFV3_LSA_REQ_PKT:
                SPRINTF ((CHR1 *) au1TmpBuf, "Link State Request");
                break;

            case OSPFV3_LS_UPDATE_PKT:
                SPRINTF ((CHR1 *) au1TmpBuf, "Link State Update");
                break;

            case OSPFV3_LSA_ACK_PKT:
                SPRINTF ((CHR1 *) au1TmpBuf, "Link State Acknowledge");
                break;

            default:
                SPRINTF ((CHR1 *) au1TmpBuf, "Unknown Packet");
                break;
        }
    }

    /* OSPFV3 Hex Dumping Only .... */
    if (u1DumpLevel == OSPFV3_HEX_DUMP)
    {
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3:  ---------- %s%s ----------\n"
                 "OSPFV3:", ((u1PktType != OSPFV3_HELLO_PKT) ? "OSPFV3 " : ""),
                 au1TmpBuf);

        for (u4ReadOffset = 1; u4ReadOffset <= u4BufLength; u4ReadOffset++)
        {
            OSPFV3_BUFFER_GET_1_BYTE (pBuf, (u4ReadOffset - 1), u1TmpData);

            SPRINTF (OSPFV3BUF (pu1LogBuf), " %02x", u1TmpData);

            if (!(u4ReadOffset % OSPFV3_FOUR_BYTES))
            {
                SPRINTF (OSPFV3BUF (pu1LogBuf), " ");
            }

            if ((!(u4ReadOffset % OSPFV3_NO_OF_BYTE_PER_LINE)) ||
                (u4ReadOffset == u4BufLength))
            {
                SPRINTF (OSPFV3BUF (pu1LogBuf), "\nOSPFV3:");
            }

            if (STRLEN (pu1LogBuf) != 0)
            {
                OSPFV3_TRC_PRINTF (pu1LogBuf);
                pu1LogBuf[0] = '\0';
            }
        }

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "\t------------------ End OSPFV3 Packet ------------------\n");
    }

    /* OSPFV3 Packet detailed parsing */
    if (u1DumpLevel == OSPFV3_LOW_DUMP)
    {
        OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_VERSION_NO_OFFSET, u1TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Version No: %d\n", u1TmpData);

        /* Display the packet type */
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Message Type: %s (%d)\n", au1TmpBuf, u1PktType);

        OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_PKT_LENGTH_OFFSET, u2TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Packet Length: %d bytes\n", u2TmpData);

        /* Extract Router ID */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, OSPFV3_RTR_ID_OFFSET,
                                  OSPFV3_RTR_ID_LEN);

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Source OSPF Router ID: %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Area ID */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, OSPFV3_AREA_ID_OFFSET,
                                  OSPFV3_AREA_ID_LEN);

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Area ID: %d.%d.%d.%d\n",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Checksum field */
        OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_CHKSUM_OFFSET, u2TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3: Packet Checksum: 0x%04X\n", u2TmpData);

        /* Extract Instance ID field */
        OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_INSTANCE_OFFSET, u1TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Instance ID: %d\n", u1TmpData);

        /* Extract Reserved field */
        OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_RSVD_OFFSET, u1TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: Reserved: %d\n", u1TmpData);

        if (STRLEN (pu1LogBuf) != 0)
        {
            /* Reset buffer after displaying,
               to avoid buffer space problems */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }

        if (i4DataLen > OSPFV3_PKT_HEADER_LEN)
        {
            switch (u1PktType)
            {
                case OSPFV3_HELLO_PKT:    /* Print the OSPF Hello Packet */
                    V3DbgOspfHelloDump (pBuf, pu1LogBuf, i4DataLen,
                                        u1DumpLevel);
                    break;

                case OSPFV3_DD_PKT:
                    V3DbgOspfDdpDump (pBuf, pu1LogBuf, i4DataLen, u1DumpLevel);
                    break;

                case OSPFV3_LSA_REQ_PKT:
                    V3DbgOspfLSRequestDump (pBuf, pu1LogBuf, i4DataLen,
                                            u1DumpLevel);
                    break;

                case OSPFV3_LS_UPDATE_PKT:
                    V3DbgOspfLSUpdateDump (pBuf, pu1LogBuf, i4DataLen,
                                           u1DumpLevel);
                    break;

                case OSPFV3_LSA_ACK_PKT:
                    V3DbgOspfLSAckDump (pBuf, pu1LogBuf, i4DataLen,
                                        u1DumpLevel);
                    break;

                default:

                    SPRINTF (OSPFV3BUF (pu1LogBuf), "\nOSPFV3:");

                    for (u4ReadOffset = OSPFV3_PKT_HEADER_LEN;
                         (u4ReadOffset < u4BufLength); u4ReadOffset++)
                    {
                        OSPFV3_BUFFER_GET_1_BYTE (pBuf, u4ReadOffset,
                                                  u1TmpData);

                        SPRINTF (OSPFV3BUF (pu1LogBuf), " %02X", u1TmpData);

                        if (!(u4ReadOffset % OSPFV3_NO_OF_BYTE_PER_LINE)
                            || ((u4ReadOffset + 1) == u4BufLength))
                        {
                            SPRINTF (OSPFV3BUF (pu1LogBuf), "\nOSPFV3:");
                        }

                        if (STRLEN (pu1LogBuf) != 0)
                        {
                            OSPFV3_TRC_PRINTF (pu1LogBuf);
                            pu1LogBuf[0] = '\0';
                        }
                    }

                    break;
            }
        }

        SPRINTF (OSPFV3BUF (pu1LogBuf),
                 "OSPFV3:\t----------------- End OSPFV3 Packet"
                 " ----------------\n");
    }

    /* One liner packet dumping */
    if (u1DumpLevel == OSPFV3_HIGH_DUMP)
    {
        SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV3: %-10s %s",
                 V3DbgOspfFormatTime (u4RtrSysTime, au1TmpBuf),
                 ((u1Direction == OSPFV3_DIR_IN) ? "< " : "> "));

        OSPFV3_BUFFER_GET_1_BYTE (pBuf, OSPFV3_VERSION_NO_OFFSET, u1TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf), "OSPFV%d-", u1TmpData);

        switch (u1PktType)
        {
            case OSPFV3_HELLO_PKT:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "Hello ");
                break;

            case OSPFV3_DD_PKT:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "DDP ");
                break;

            case OSPFV3_LSA_REQ_PKT:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "LS Req ");
                break;

            case OSPFV3_LS_UPDATE_PKT:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "LS Update ");
                break;

            case OSPFV3_LSA_ACK_PKT:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "LS Ack ");
                break;

            default:
                SPRINTF (OSPFV3BUF (pu1LogBuf), "Unknown ");
                break;
        }

        OSPFV3_BUFFER_GET_2_BYTE (pBuf, OSPFV3_PKT_LENGTH_OFFSET, u2TmpData);
        SPRINTF (OSPFV3BUF (pu1LogBuf), "Len:%d ", u2TmpData);

        /* Extract Router ID */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, OSPFV3_RTR_ID_OFFSET,
                                  OSPFV3_RTR_ID_LEN);

        SPRINTF (OSPFV3BUF (pu1LogBuf), "Rtr:%d.%d.%d.%d ",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        /* Extract Area ID */
        OSPFV3_BUFFER_GET_STRING (pBuf, au1TmpBuf, OSPFV3_AREA_ID_OFFSET,
                                  OSPFV3_AREA_ID_LEN);

        SPRINTF (OSPFV3BUF (pu1LogBuf), "Area:%d.%d.%d.%d ",
                 au1TmpBuf[0], au1TmpBuf[1], au1TmpBuf[2], au1TmpBuf[3]);

        if (STRLEN (pu1LogBuf))
        {
            /* Reset buffer after displaying,
               to avoid buffer space problems */
            OSPFV3_TRC_PRINTF (pu1LogBuf);
            pu1LogBuf[0] = '\0';
        }

        if (i4DataLen > OSPFV3_PKT_HEADER_LEN)
        {
            switch (u1PktType)
            {
                case OSPFV3_HELLO_PKT:    /* Print the OSPF Hello Packet */
                    V3DbgOspfHelloDump (pBuf, pu1LogBuf, i4DataLen,
                                        u1DumpLevel);
                    break;

                case OSPFV3_DD_PKT:
                    V3DbgOspfDdpDump (pBuf, pu1LogBuf, i4DataLen, u1DumpLevel);
                    break;

                case OSPFV3_LSA_REQ_PKT:
                    V3DbgOspfLSRequestDump (pBuf, pu1LogBuf, i4DataLen,
                                            u1DumpLevel);
                    break;

                case OSPFV3_LS_UPDATE_PKT:
                    V3DbgOspfLSUpdateDump (pBuf, pu1LogBuf, i4DataLen,
                                           u1DumpLevel);
                    break;

                case OSPFV3_LSA_ACK_PKT:
                    V3DbgOspfLSAckDump (pBuf, pu1LogBuf, i4DataLen,
                                        u1DumpLevel);
                    break;

                default:
                    SPRINTF (OSPFV3BUF (pu1LogBuf), "Unknown packet");
                    break;
            }
        }

        SPRINTF (OSPFV3BUF (pu1LogBuf), "\n");

    }                            /* end of high level dumping */

    if (STRLEN (pu1LogBuf) != 0)
    {
        OSPFV3_TRC_PRINTF (pu1LogBuf);
        pu1LogBuf[0] = '\0';
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DbgOspfDumpPktData                                       */
/*                                                                           */
/* Description  : Dumps the contents of the linear buffer                    */
/*                                                                           */
/* Input        : u4Trace   : Trace Flags word of the OSPF module            */
/*                u1Direction : flag indicating outgoing or incoming message */
/*                pBuf      : pointer to the buffer to be dumped             */
/*                u2BufLen  : length of the information to be dumped         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3DbgOspfDumpPktData (UINT4 u4Trace, UINT1 u1Direction,
                      UINT1 *pBuf, UINT2 u2BufLen)
{
    UINT1               u1DumpLevel = 0;

    if (!(u4Trace & (OSPFV3_DUMP_HGH_TRC | OSPFV3_DUMP_LOW_TRC |
                     OSPFV3_DUMP_HEX_TRC)))
    {
        /* Dumping messages of this "type" not enabled */
        return;
    }

    if (u4Trace & OSPFV3_DUMP_HGH_TRC)
    {
        u1DumpLevel = OSPFV3_HIGH_DUMP;
    }
    else if (u4Trace & OSPFV3_DUMP_LOW_TRC)
    {
        u1DumpLevel = OSPFV3_LOW_DUMP;
    }
    else if (u4Trace & OSPFV3_DUMP_HEX_TRC)
    {
        u1DumpLevel = OSPFV3_HEX_DUMP;
    }
    else
    {
        return;                    /* Error in trace flag */
    }

    /* Call the packet analyser */
    V3DbgOspfPktAnalyser (u1Direction, u1DumpLevel, pBuf, u2BufLen);

    return;
}

#endif /* TRACE_WANTED */
