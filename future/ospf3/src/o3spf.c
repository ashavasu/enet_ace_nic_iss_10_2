/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3spf.c,v 1.16 2017/12/26 13:34:29 siva Exp $
 *
 * Description: This file contains procedures related to the
 *              intra-area route calculations.
 ********************************************************************/
#include "o3inc.h"

PRIVATE INT4        V3RtcUnMarkSpfNode
PROTO ((tRBElem * pNode, eRBVisit visit, UINT4 level, VOID *arg, VOID *out));
PRIVATE UINT1      *V3RtcGetLinksFromLsa PROTO ((tV3OsCandteNode * pCandteNode,
                                                 UINT1 *pCurrPtr,
                                                 tV3OsLinkNode * pLinkNode,
                                                 tV3OsLsaInfo ** pCurrLsaInfo));
PRIVATE INT4        V3RtcUpdateCandteLst
PROTO ((tV3OsCandteNode * pSpfRoot,
        tV3OsCandteNode * pNewVertex,
        tV3OsLinkNode * pLinkNode, tV3OsArea * pArea, UINT1 u1VertType));

PRIVATE tV3OsCandteNode *V3RtcGetNearestCandteNodeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PRIVATE tV3OsCandteNode *V3RtcUpdateSpfTree
PROTO ((tV3OsCandteNode * pCandteNode, tV3OsArea * pArea));

PRIVATE VOID
    V3RtcAddToSpfTree PROTO ((tV3OsSpf pSpfTree, tV3OsCandteNode * pSpfNode));

PRIVATE VOID
    V3RtcDeleteFromSpfTree PROTO ((tV3OsSpf pSpfTree,
                                   tV3OsCandteNode * pSpfNode));

PRIVATE tV3OsCandteNode *V3RtcCreateCandteNode
PROTO ((tV3OsRouterId * pVertexRtrId, UINT4 u4VertexIfId, UINT1 u1VertType));

PRIVATE tV3OsCandteNode *V3RtcSearchCandteLstInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pVertexRtrId,
        UINT4 u4VertexIfId, UINT1 u1VertType));

PRIVATE UINT4       V3RtcCandteHashFunc
PROTO ((tV3OsRouterId * pVertexRtrId, UINT4 u4VertexIfId, UINT1 u1VertType));
PRIVATE UINT1
    V3RtcSearchLsaLinks PROTO ((UINT1 *pPtr, tV3OsRouterId * pVertexRtrId,
                                UINT4 *pu4VertexIfId,
                                UINT1 u1VertType, UINT1 *pu1ArrayIndex,
                                UINT4 au4IfId[], UINT2 au2IfMetric[]));

PRIVATE VOID        V3RtcSetNextHops
PROTO ((tV3OsCandteNode * pSpfRoot, tV3OsCandteNode * pParent,
        tV3OsCandteNode * pVertex, tV3OsLinkNode * pLinkNode,
        tV3OsArea * pArea, UINT1 u1CountFlag, UINT4 au4IfId[],
        UINT2 au2IfMetric[], UINT4 u4MinCost));

PRIVATE INT4 V3RtcCheckLsaMaxAge PROTO ((UINT1 *pPtr, UINT1 u1VertType));

PRIVATE VOID V3RtcExtractVEBBitOptions PROTO ((tV3OsCandteNode * pVertex));

PRIVATE tV3OsInterface *V3RtcGetFindIf
PROTO ((UINT4 u4InterfaceId, tV3OsArea * pArea));

PRIVATE VOID
    V3RtcUpdateCandteNodeNextHops PROTO ((tV3OsCandteNode * pVertex,
                                          tV3OsCandteNode * pParent));

/*****************************************************************************/
/* Function     : V3RtcBuildSpfTree                                          */
/*                                                                           */
/* Description  : This function builds the SPF Tree for the area and         */
/*                identifies the SPF nodes which are changed                 */
/*                                                                           */
/* Input        : pArea   : AREA whose intra-area routes are to be           */
/*                          calculated                                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if the SPF calculation is                     */
/*                successful otherwise returns OSIX_FAILURE                  */
/*****************************************************************************/
PUBLIC VOID
V3RtcBuildSpfTree (tV3OsArea * pArea)
{
    tV3OsCandteNode    *pSpfRoot = NULL;
    tV3OsCandteNode    *pNewVertex = NULL;
    tV3OsInterface     *pCheckInterface = NULL;
    UINT1              *pCurrPtr = NULL;
    UINT1              *pPtr = NULL;
    tV3OsLsaInfo       *pCurrLsaInfo = NULL;
    tV3OsLinkNode       linkNode;
    UINT1               u1VertType = OSPFV3_INVALID;

    /* Relinquish */
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcBuildSpfTree\n");

    OSPFV3_TRC1 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                 pArea->pV3OspfCxt->u4ContextId,
                 "Intra-Area Route Calculation: Area %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    pArea->u4SpfRuns++;

    /* Search for the Root node of the SPF tree */
    pSpfRoot = V3RtcSearchSpfTree (pArea->pSpf, &(pArea->pV3OspfCxt->rtrId), 0,
                                   OSPFV3_VERT_ROUTER);

    /* Search for the LSA corresponding to this root node */
    if ((pPtr = V3RtcSearchDatabase (OSPFV3_ROUTER_LSA,
                                     &(pArea->pV3OspfCxt->rtrId), 0, pArea))
        == NULL)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "No LSA present for Root\n");
    }

    /* If the SPF root is not present, create the root node 
     * and add it to SPF tree */
    if (pSpfRoot == NULL)
    {
        if ((pSpfRoot = V3RtcCreateCandteNode (&(pArea->pV3OspfCxt->rtrId), 0,
                                               OSPFV3_VERT_ROUTER)) == NULL)
        {
            OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_RTMODULE_TRC,
                        pArea->pV3OspfCxt->u4ContextId,
                        "Candidate Node Allocation Failed for root\n");
            return;
        }

        pSpfRoot->u4Cost = 0;
        /* Adding the created Root node to SPF tree */
        V3RtcAddToSpfTree (pArea->pSpf, pSpfRoot);
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "Root Node is added to SPF Tree\n");
    }
    else
    {
        /* Resetting the SPF node not found flag */
        pSpfRoot->u1Flag -= OSPFV3_NOT_FOUND_MASK;
    }

    if (pPtr == NULL)
    {
        return;
    }

    pSpfRoot->pDbNode = (tV3OsDbNode *) (VOID *) pPtr;
    V3RtcExtractVEBBitOptions (pSpfRoot);

    pNewVertex = pSpfRoot;

    do
    {
        pCurrPtr = NULL;

        if (pNewVertex->u1VertType == OSPFV3_VERT_ROUTER)
        {
            if (pArea->bTransitCapability != OSIX_TRUE)
            {
                /* If V-Bit is set, then set the area as 
                 * transit area */
                if (OSPFV3_IS_V_BIT_SET (pNewVertex->u1Options))
                {
                    pArea->bTransitCapability = OSIX_TRUE;
                }
            }
        }
        /* Check whether we need to relinquish the route calculation
         * to do other OSPFv3 processings */
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than next relinquish time calcuteled 
             * in previous relinuish */
            if (u4CurrentTime >= pArea->pV3OspfCxt->u4StaggeringDelta)
            {
                OSPF3RtcRelinquishInCxt (pArea->pV3OspfCxt);
                /* next relinquish time calc in OSPF3RtcRelinquish() */
            }
        }

        OSPFV3_TRC2 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Links Of Vertex with Vertex Rtr Id = %x \t"
                     "Vertex If Id = %d Considered\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNewVertex->vertexId.
                                              vertexRtrId),
                     pNewVertex->vertexId.u4VertexIfId);

        pCurrLsaInfo = NULL;
        while ((pCurrPtr =
                V3RtcGetLinksFromLsa (pNewVertex, pCurrPtr, &linkNode,
                                      &pCurrLsaInfo)) != NULL)
        {

            if (pNewVertex == pSpfRoot)
            {
                pCheckInterface = V3IfGetPtrtoInterface (pArea,
                                                         linkNode.
                                                         u4InterfaceId);
                if (pCheckInterface != NULL)
                {
                    if (pCheckInterface->u1IsmState == OSPFV3_IFS_STANDBY)
                    {
                        continue;
                    }
                }

            }
            /* 
             * If the new vertex is a router and the link is a transit link 
             * then the next vertex should be a network. Otherwise the next 
             * vertex is a router.
             */
            if ((pNewVertex->u1VertType == OSPFV3_VERT_ROUTER) &&
                (linkNode.u1LinkType == OSPFV3_TRANSIT_LINK))
            {
                u1VertType = OSPFV3_VERT_NETWORK;
            }
            else
            {
                u1VertType = OSPFV3_VERT_ROUTER;
            }

            if ((linkNode.u1LinkType == OSPFV3_VIRTUAL_LINK) &&
                (V3UtilAreaIdComp (pArea->areaId,
                                   OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL))
            {
                OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                            pArea->pV3OspfCxt->u4ContextId,
                            "Router LSA is having "
                            "link as virtual link in non backbone area\n");
                continue;
            }

            /* Add to candidate list if necessary */
            if (V3RtcUpdateCandteLst (pSpfRoot, pNewVertex, &linkNode,
                                      pArea, u1VertType) == OSIX_FAILURE)
            {
                continue;
            }
        }

        /* If candidate list is empty this phase of algorithm is complete
         * otherwise remove the vertex which is nearest to root from candidate
         * list and add it to the spf tree.
         */
        if ((pNewVertex = V3RtcGetNearestCandteNodeInCxt (pArea->pV3OspfCxt))
            == NULL)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                        pArea->pV3OspfCxt->u4ContextId,
                        "Candidate List is Empty\n");
            break;
        }

        /* Delete the node from candiate list */
        TMO_HASH_Delete_Node (pArea->pV3OspfCxt->pCandteLst,
                              &pNewVertex->candteHashNode,
                              V3RtcCandteHashFunc (&
                                                   (pNewVertex->vertexId.
                                                    vertexRtrId),
                                                   pNewVertex->vertexId.
                                                   u4VertexIfId,
                                                   pNewVertex->u1VertType));

        /* If the node is already present update the node 
         * else add the new node to SPF tree */
        pNewVertex = V3RtcUpdateSpfTree (pNewVertex, pArea);

        OSPFV3_TRC2 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Vertex with Vertex Rtr Id = %x \t"
                     "Vertex If Id = %d Considered\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNewVertex->vertexId.
                                              vertexRtrId),
                     pNewVertex->vertexId.u4VertexIfId);
    }
    while (1);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcBuildSpfTree\n");
}

/*****************************************************************************/
/* Function     : V3RtcUnMarkSpfNode                                         */
/*                                                                           */
/* Description  : This function set the all SPF nodes u1Flag to              */
/*                OSPFV3_NOT_FOUND                                           */
/*                                                                           */
/* Input        : pNode - Pointer to SPF Tree Node                           */
/*                visit - Visit order.                                       */
/*                level - Level of the scan                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Return RB_WALK_CONT.                                       */
/*****************************************************************************/
PRIVATE INT4
V3RtcUnMarkSpfNode (tRBElem * pNode, eRBVisit visit, UINT4 level,
                    VOID *arg, VOID *out)
{
    tV3OsCandteNode    *pSpfNode = NULL;

    UNUSED_PARAM (arg);
    UNUSED_PARAM (out);
    UNUSED_PARAM (level);

    if ((visit == leaf) || (visit == postorder))
    {
        if (pNode != NULL)
        {
            pSpfNode = (tV3OsCandteNode *) pNode;
            pSpfNode->u1Flag = OSPFV3_NOT_FOUND;
        }
    }
    return (RB_WALK_CONT);
}

/*****************************************************************************/
/* Function     : V3RtcMarkAllSpfNodes                                       */
/*                                                                           */
/* Description  : This function scans all the SPF nodes to reset the u1Flag  */
/*                                                                           */
/* Input        : pSpf - Pointer to SPF Tree                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcMarkAllSpfNodes (tV3OsSpf pSpf)
{
    RBTreeWalk (pSpf, V3RtcUnMarkSpfNode, 0, (VOID *) NULL);
}

/*****************************************************************************/
/* Function     : V3RtcUpdateCandteLst                                       */
/*                                                                           */
/* Description  : If the vertex specified in the link is already present in  */
/*                the candidate list its contents are updated. Otherwise a   */
/*                new candidate node is created for the vertex.              */
/*                                                                           */
/* Input        : pSpfRoot    : Pointer to Root node of SPF Tree             */
/*                pSpfTree    : Pointer to SPF Tree                          */
/*                pNewVertex  : Pointer to vertex whose link are being       */
/*                              processed.                                   */
/*                pLinkNode   : Pointer to the link that is being processed  */
/*                pArea       : Pointer to the area for which SPF tree is    */
/*                              computed                                     */
/*                u1VertType  : Vertex type of the new vertex being added or */
/*                              modified                                     */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
V3RtcUpdateCandteLst (tV3OsCandteNode * pSpfRoot,
                      tV3OsCandteNode * pNewVertex,
                      tV3OsLinkNode * pLinkNode, tV3OsArea * pArea,
                      UINT1 u1VertType)
{
    tV3OsCandteNode    *pCandteNode = NULL;
    tV3OsCandteNode    *pSpfNode = NULL;
    UINT1              *pPtr = NULL;
    UINT4               u4Cost = 0;
    UINT4               u4MinCost = 1;
    UINT4               u4HashIndex = 0;
    UINT4               u4VertexIfId = 0;
    UINT1               u1CountFlag = 0;
    UINT1               u1IndexCount = 0;
    UINT1               u1TempCountFlag = 0;
    UINT4               au4IfId[OSPFV3_MAX_IF_OVER_LINK];
    UINT2               au2IfMetric[OSPFV3_MAX_IF_OVER_LINK];

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcUpdateCandteLst \n");

    /* Check if cost of link is ls_infinity */
    if (pLinkNode->u2LinkMetric == OSPFV3_LS_INFINITY_16BIT)
    {
        OSPFV3_TRC3 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Link Cost LS Infinity Parent %x"
                     "Vertex RtrId: %x  VertexIfId: %x  Dstn: %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNewVertex->vertexId.vertexRtrId),
                     pNewVertex->vertexId.u4VertexIfId,
                     OSPFV3_BUFFER_DWFROMPDU (pLinkNode->nbrRtrId));
        return OSIX_SUCCESS;
    }

    if (u1VertType == OSPFV3_VERT_ROUTER)
    {
        u4VertexIfId = 0;
    }
    else
    {
        u4VertexIfId = pLinkNode->u4NbrIfId;
    }

    if (((pSpfNode = V3RtcSearchSpfTree (pArea->pSpf, &(pLinkNode->nbrRtrId),
                                         u4VertexIfId, u1VertType)) != NULL)
        && (!(pSpfNode->u1Flag & OSPFV3_NOT_FOUND_MASK)))
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "Node Already Present in SPF Tree\n");
        return OSIX_SUCCESS;
    }

    if ((pPtr =
         V3RtcSearchDatabase ((UINT2)
                              ((u1VertType ==
                                OSPFV3_VERT_ROUTER) ? OSPFV3_ROUTER_LSA :
                               OSPFV3_NETWORK_LSA), &(pLinkNode->nbrRtrId),
                              pLinkNode->u4NbrIfId, pArea)) == NULL)
    {
        OSPFV3_TRC3 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "LSA is not Present Parent %x "
                     "Vertex RtrId: %x  VertexIfId: %x  Dstn: %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNewVertex->vertexId.vertexRtrId),
                     pNewVertex->vertexId.u4VertexIfId,
                     OSPFV3_BUFFER_DWFROMPDU (pLinkNode->nbrRtrId));
        return OSIX_FAILURE;
    }

    if (V3RtcCheckLsaMaxAge (pPtr, u1VertType) == OSIX_TRUE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "All the LSAs of the generated by "
                    "the router are MAX_AGE LSAs\n");
        return OSIX_SUCCESS;
    }

    /* RFC Section 16.1 - Step (2) (b) 
     * Footnotes
     [23]Note that the presence of any link back    to V is sufficient; it
     need not be    the matching half of the link under consideration from V
     to W. This is enough to ensure that, before    data traffic flows
     between a pair of neighboring routers, their link state databases
     will be synchronized. */

    MEMSET (au4IfId, 0, (sizeof (UINT4) * 8));
    MEMSET (au2IfMetric, 0, (sizeof (UINT2) * 8));

    if (V3RtcSearchLsaLinks (pPtr, &(pNewVertex->vertexId.vertexRtrId),
                             &(pNewVertex->vertexId.u4VertexIfId),
                             u1VertType, &u1CountFlag, (au4IfId),
                             (au2IfMetric)) == OSIX_FALSE)
    {
        OSPFV3_TRC3 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Lnk Bk To Parent Not Present Parent "
                     "Vertex RtrId: %x  VertexIfId: %x  Dstn: %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNewVertex->vertexId.vertexRtrId),
                     pNewVertex->vertexId.u4VertexIfId,
                     OSPFV3_BUFFER_DWFROMPDU (pLinkNode->nbrRtrId));
        return OSIX_FAILURE;
    }
    if (u1CountFlag <= 1)
    {
        u1CountFlag = 0;
    }
    else
    {
        u1IndexCount = 0;
        u4MinCost = au2IfMetric[u1IndexCount];
        for (u1IndexCount = 1; ((u1IndexCount < u1CountFlag) &&
                                (u1IndexCount < OSPFV3_MAX_IF_OVER_LINK));
             u1IndexCount++)
        {
            if (u4MinCost > au2IfMetric[u1IndexCount])
            {
                u4MinCost = au2IfMetric[u1IndexCount];
                u1TempCountFlag = 1;

            }
            else if (u4MinCost == au2IfMetric[u1IndexCount])
            {
                u1TempCountFlag++;
            }

        }
        if (u1TempCountFlag == 1)
        {
            u1CountFlag = 0;
        }
    }

    pCandteNode = V3RtcSearchCandteLstInCxt
        (pArea->pV3OspfCxt, &(pLinkNode->nbrRtrId), u4VertexIfId, u1VertType);
    u4Cost = pNewVertex->u4Cost + pLinkNode->u2LinkMetric;

    if ((pCandteNode != NULL) && (u4Cost > pCandteNode->u4Cost))
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "Candidate Lst Not Updated (Existing Cost Less)\n");
        return OSIX_SUCCESS;
    }
    else if ((pCandteNode != NULL) && (u4Cost == pCandteNode->u4Cost))
    {
        V3RtcSetNextHops (pSpfRoot, pNewVertex,
                          pCandteNode, pLinkNode, pArea, 0, NULL, NULL,
                          u4MinCost);
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "Candidate Lst Updated (New Nxt Hops Added)\n");
        return OSIX_SUCCESS;
    }
    else
    {
        if (pCandteNode == NULL)
        {
            if ((pCandteNode = V3RtcCreateCandteNode (&(pLinkNode->nbrRtrId),
                                                      u4VertexIfId,
                                                      u1VertType)) == NULL)
            {
                OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pArea->pV3OspfCxt->u4ContextId,
                            "Failed to Allocate memory for candidate node\n");
                return OSIX_FAILURE;
            }

            pCandteNode->pDbNode = (tV3OsDbNode *) (VOID *) pPtr;

            if (u1VertType == OSPFV3_VERT_ROUTER)
            {
                V3RtcExtractVEBBitOptions (pCandteNode);
            }

            u4HashIndex = (V3RtcCandteHashFunc (&(pCandteNode->
                                                  vertexId.vertexRtrId),
                                                pCandteNode->vertexId.
                                                u4VertexIfId, u1VertType));
            TMO_HASH_Add_Node (pArea->pV3OspfCxt->pCandteLst,
                               &(pCandteNode->candteHashNode), u4HashIndex,
                               NULL);
        }

        /* New cost equal is lesser than the old cost */
        pCandteNode->u4Cost = u4Cost;
        pCandteNode->u1HopCount = 0;
        V3RtcSetNextHops (pSpfRoot, pNewVertex,
                          pCandteNode, pLinkNode, pArea, u1CountFlag,
                          au4IfId, au2IfMetric, u4MinCost);
        OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtcUpdateCandteLst\n");
    }
    KW_FALSEPOSITIVE_FIX (pCandteNode);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcGetNearestCandteNodeInCxt                             */
/*                                                                           */
/* Description  : This function gets the nearest candate node.               */
/*                                                                           */
/* Input        : pV3OspfCxt    -  Context pointer                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the nearest candidate node.                     */
/*****************************************************************************/
PRIVATE tV3OsCandteNode *
V3RtcGetNearestCandteNodeInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsCandteNode    *pLstCandteNode = NULL;
    tV3OsCandteNode    *pNearestCandteNode = NULL;
    tTMO_HASH_NODE     *pNode = NULL;
    UINT4               u4HashIndex = 0;

    TMO_HASH_Scan_Table (pV3OspfCxt->pCandteLst, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pV3OspfCxt->pCandteLst, u4HashIndex,
                              pNode, tTMO_HASH_NODE *)
        {
            pLstCandteNode = OSPFV3_GET_BASE_PTR (tV3OsCandteNode,
                                                  candteHashNode, pNode);
            if (pNearestCandteNode == NULL)
            {
                pNearestCandteNode = pLstCandteNode;
            }
            else if (pLstCandteNode->u4Cost <= pNearestCandteNode->u4Cost)
            {
                /* If costs are Equal then take the Network vertices first */
                if ((pLstCandteNode->u4Cost == pNearestCandteNode->u4Cost)
                    && (pNearestCandteNode->u1VertType == OSPFV3_VERT_ROUTER))
                {
                    pNearestCandteNode = pLstCandteNode;
                }
                /* If the cost is Less then take that as the next 
                   candidate node */
                else if (pLstCandteNode->u4Cost < pNearestCandteNode->u4Cost)
                {
                    pNearestCandteNode = pLstCandteNode;
                }
            }
        }
    }
    return pNearestCandteNode;
}

/*****************************************************************************/
/* Function     : V3RtcUpdateSpfTree                                         */
/*                                                                           */
/* Description  : This function performs the addition of candidate node to   */
/*                SPF Tree.                                                  */
/*                                                                           */
/* Input        : pCandteNode   -   Node to be added/modified                */
/*                pArea         -   Pointer to the area                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE tV3OsCandteNode *
V3RtcUpdateSpfTree (tV3OsCandteNode * pCandteNode, tV3OsArea * pArea)
{
    tV3OsCandteNode    *pOldSpfNode = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    UINT1               u1AbrOptChange = OSIX_FALSE;
    UINT1               u1AsbrOptChange = OSIX_FALSE;
    UINT1               u1HopIndex = 0;

    /* Search for the SPF node in SPF Tree */
    pOldSpfNode = V3RtcSearchSpfTree (pArea->pSpf,
                                      &(pCandteNode->vertexId.vertexRtrId),
                                      pCandteNode->vertexId.u4VertexIfId,
                                      pCandteNode->u1VertType);

    /* If new SPF node is not present in the old SPF tree */
    if (pOldSpfNode == NULL)
    {
        if (pCandteNode->u1VertType == OSPFV3_VERT_ROUTER)
        {
            /* If E-Bit is set, then do the Intra Area ASBR route 
             * calculation */
            if (OSPFV3_IS_E_BIT_SET (pCandteNode->u1Options))
            {
                V3RtcCalculateAsbrRoute (pCandteNode, pArea, OSPFV3_CREATED);
            }

            /* If B-Bit is set, then do the ABR route calculation */
            if (OSPFV3_IS_B_BIT_SET (pCandteNode->u1Options))
            {
                V3RtcCalculateAbrRoute (pCandteNode, pArea, OSPFV3_CREATED);
            }

            /* Stop the special aging of the LSAs generated by the 
             * router described by this vertex */
            V3LsuSplAgingActionInCxt (pArea->pV3OspfCxt,
                                      &(pCandteNode->vertexId.vertexRtrId),
                                      OSPFV3_STOP);
        }

        V3RtcProcIntraPrefLsaOfSpfNode (pCandteNode, pArea, OSPFV3_CREATED);

        V3RtcAddToSpfTree (pArea->pSpf, pCandteNode);
        if (pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR)
        {
            V3RtcProcNonStandABRVirtualLink (pCandteNode, pArea, OSPFV3_IFE_UP);
        }
        return pCandteNode;
    }
    else
    {
        if (pCandteNode->u1VertType == OSPFV3_VERT_ROUTER)
        {
            /* Check for the changes in the option */
            if ((OSPFV3_IS_B_BIT_SET (pOldSpfNode->u1Options)) &&
                (!(OSPFV3_IS_B_BIT_SET (pCandteNode->u1Options))))
            {
                V3RtcCalculateAbrRoute (pCandteNode, pArea, OSPFV3_DELETED);
                u1AbrOptChange = OSIX_TRUE;
            }

            if ((!(OSPFV3_IS_B_BIT_SET (pOldSpfNode->u1Options))) &&
                (OSPFV3_IS_B_BIT_SET (pCandteNode->u1Options)))
            {
                V3RtcCalculateAbrRoute (pCandteNode, pArea, OSPFV3_CREATED);
                u1AbrOptChange = OSIX_TRUE;
            }

            if ((OSPFV3_IS_B_BIT_SET (pOldSpfNode->u1Options)) &&
                (OSPFV3_IS_B_BIT_SET (pCandteNode->u1Options)))
            {
                /* No change in SPF nodes. Get the route and update the flag */
                pRtEntry = V3RtcFindRtEntryInCxt
                    (pArea->pV3OspfCxt,
                     (VOID *) &(pCandteNode->vertexId.vertexRtrId),
                     0, OSPFV3_DEST_AREA_BORDER);

                if (pRtEntry != NULL)
                {
                    pRtEntry->u1Flag = OSPFV3_FOUND;
                }
            }

            if ((OSPFV3_IS_E_BIT_SET (pOldSpfNode->u1Options)) &&
                (!(OSPFV3_IS_E_BIT_SET (pCandteNode->u1Options))))
            {
                V3RtcCalculateAsbrRoute (pCandteNode, pArea, OSPFV3_DELETED);
                u1AsbrOptChange = OSIX_TRUE;
            }

            if ((!OSPFV3_IS_E_BIT_SET (pOldSpfNode->u1Options)) &&
                (OSPFV3_IS_E_BIT_SET (pCandteNode->u1Options)))
            {
                V3RtcCalculateAsbrRoute (pCandteNode, pArea, OSPFV3_CREATED);
                u1AsbrOptChange = OSIX_TRUE;
            }

            if ((OSPFV3_IS_E_BIT_SET (pOldSpfNode->u1Options)) &&
                (OSPFV3_IS_E_BIT_SET (pCandteNode->u1Options)))
            {
                /* No change in SPF nodes. Get the route and update the flag */
                pRtEntry = V3RtcFindRtEntryInCxt
                    (pArea->pV3OspfCxt,
                     (VOID *) &(pCandteNode->vertexId.vertexRtrId),
                     0, OSPFV3_DEST_AS_BOUNDARY);

                if (pRtEntry != NULL)
                {
                    pRtEntry->u1Flag = OSPFV3_FOUND;
                }
            }
        }

        if ((pOldSpfNode->u1Options & OSPFV3_NT_BIT_MASK) !=
            (pCandteNode->u1Options & OSPFV3_NT_BIT_MASK))
        {
            pArea->u4AbrStatFlg = OSIX_TRUE;
        }

        pOldSpfNode->u1Options = pCandteNode->u1Options;

        /* Resetting the SPF node not found flag */
        pOldSpfNode->u1Flag = pCandteNode->u1Flag;
        pOldSpfNode->pDbNode = (tV3OsDbNode *) pCandteNode->pDbNode;
        pOldSpfNode->u1VirtReachable = pCandteNode->u1VirtReachable;

        if (((pArea->pV3OspfCxt->u1VirtRtCal == OSIX_TRUE) &&
             (V3UtilAreaIdComp (pArea->areaId,
                                OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)) ||
            (V3RtcIsChangeInSpfNodes (pOldSpfNode, pCandteNode) == OSIX_TRUE))
        {
            if (pCandteNode->u1VertType == OSPFV3_VERT_ROUTER)
            {
                if ((u1AbrOptChange == OSIX_FALSE) &&
                    (OSPFV3_IS_B_BIT_SET (pCandteNode->u1Options)))
                {
                    V3RtcCalculateAbrRoute (pCandteNode, pArea, OSPFV3_UPDATED);
                }
                if ((u1AsbrOptChange == OSIX_FALSE) &&
                    (OSPFV3_IS_E_BIT_SET (pCandteNode->u1Options)))
                {
                    V3RtcCalculateAsbrRoute (pCandteNode, pArea,
                                             OSPFV3_UPDATED);
                }
            }

            V3RtcProcIntraPrefLsaOfSpfNode (pCandteNode, pArea, OSPFV3_UPDATED);

            pOldSpfNode->u4Cost = pCandteNode->u4Cost;

            for (u1HopIndex = 0; ((u1HopIndex < pCandteNode->u1HopCount) &&
                                  (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
                 u1HopIndex++)
            {
                /* Next hop is not found in Old route entry */
                /* Add the next hop information derived from ABR route entry */
                pOldSpfNode->aNextHops[u1HopIndex].pInterface =
                    pCandteNode->aNextHops[u1HopIndex].pInterface;

                OSPFV3_IP6_ADDR_COPY (pOldSpfNode->aNextHops[u1HopIndex].
                                      nbrIpv6Addr,
                                      pCandteNode->aNextHops[u1HopIndex].
                                      nbrIpv6Addr);
            }
            pOldSpfNode->u1HopCount = pCandteNode->u1HopCount;
            if (pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR)
            {
                V3RtcProcNonStandABRVirtualLink (pOldSpfNode, pArea,
                                                 OSPFV3_VIF_COST_CHANGED);
            }
        }
        OSPFV3_CANDTE_FREE (pCandteNode);
        return pOldSpfNode;
    }
}

/*****************************************************************************/
/* Function     : V3RtcProcessSpfNodes                                       */
/*                                                                           */
/* Description  : This function scans all the SPF nodes and process the      */
/*                SPF node changes                                           */
/*                                                                           */
/* Input        : pArea - Pointer to the area whose SPF node changes are     */
/*                        being processed                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessSpfNodes (tV3OsArea * pArea)
{
    tV3OsCandteNode    *pSpfNode = NULL;
    tV3OsCandteNode    *pTempSpfNode = NULL;

    pSpfNode = (tV3OsCandteNode *) RBTreeGetFirst (pArea->pSpf);

    while (pSpfNode != NULL)
    {
        pTempSpfNode = (tV3OsCandteNode *) RBTreeGetNext (pArea->pSpf,
                                                          (tRBElem *) pSpfNode,
                                                          NULL);
        /* If the SPF node not found flag is set then do some processing 
         * to delete the SPF node */
        if (pSpfNode->u1Flag & OSPFV3_NOT_FOUND_MASK)
        {
            if ((pSpfNode->u1VertType == OSPFV3_VERT_ROUTER) &&
                (V3UtilRtrIdComp (pSpfNode->vertexId.vertexRtrId,
                                  OSPFV3_RTR_ID (pArea->pV3OspfCxt))
                 != OSPFV3_EQUAL))
            {
                if (OSPFV3_IS_B_BIT_SET (pSpfNode->u1Options))
                {
                    V3RtcCalculateAbrRoute (pSpfNode, pArea, OSPFV3_DELETED);
                }
                if (OSPFV3_IS_E_BIT_SET (pSpfNode->u1Options))
                {
                    V3RtcCalculateAsbrRoute (pSpfNode, pArea, OSPFV3_DELETED);
                }
                V3LsuSplAgingActionInCxt (pArea->pV3OspfCxt,
                                          &(pSpfNode->vertexId.vertexRtrId),
                                          OSPFV3_START);
            }

            V3RtcProcIntraPrefLsaOfSpfNode (pSpfNode, pArea, OSPFV3_DELETED);
            if (pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR)
            {
                V3RtcProcNonStandABRVirtualLink (pSpfNode, pArea,
                                                 OSPFV3_IFE_DOWN);
            }
            V3RtcDeleteFromSpfTree (pArea->pSpf, pSpfNode);
        }
        pSpfNode = pTempSpfNode;
    }
}

/*****************************************************************************/
/* Function     : V3RtcGetLinksFromLsa                                       */
/*                                                                           */
/* Description  : This function is to process the Router or Network          */
/*                LSA to get  the next link.                                 */
/*                                                                           */
/* Input        : pCandteNode    : Pointer to the candidate node whose LSAs  */
/*                                 are being processed                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the next link in Router LSA.                    */
/*                Pointer to the next neighbor in Network LSA                */
/*                NULL, if the link no more links are there to process       */
/*****************************************************************************/
PRIVATE UINT1      *
V3RtcGetLinksFromLsa (tV3OsCandteNode * pCandteNode,
                      UINT1 *pCurrPtr, tV3OsLinkNode * pLinkNode,
                      tV3OsLsaInfo ** pCurrLsaInfo)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Found = OSIX_FALSE;

    if (pCurrPtr == NULL)
    {
        /* First time this function is getting called */
        if (pCandteNode->u1VertType == OSPFV3_VERT_ROUTER)
        {
            TMO_SLL_Scan (&(pCandteNode->pDbNode->lsaLst),
                          pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                pLstNode);
                if (!(OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
                {
                    *pCurrLsaInfo = pLsaInfo;
                    break;
                }
            }
        }
        else
        {
            *pCurrLsaInfo = pCandteNode->pOsLsaInfo;
        }

        if (*pCurrLsaInfo == NULL)
        {
            return NULL;
        }

        pCurrPtr = (*pCurrLsaInfo)->pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;
    }

    pLstNode = NULL;

    if (pCandteNode->u1VertType == OSPFV3_VERT_ROUTER)
    {
        /* No more links are there in present LSA  */
        if ((pCurrPtr - (*pCurrLsaInfo)->pLsa) == (*pCurrLsaInfo)->u2LsaLen)
        {
            while ((pLstNode = TMO_SLL_Next (&(pCandteNode->pDbNode->lsaLst),
                                             &((*pCurrLsaInfo)->nextLsaInfo)))
                   != NULL)
            {
                pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                pLstNode);
                *pCurrLsaInfo = pLsaInfo;
                /* If the LSA is MAX_AGE LSA, then consider next */
                if (!(OSPFV3_IS_MAX_AGE ((*pCurrLsaInfo)->u2LsaAge)))
                {
                    pCurrPtr =
                        (*pCurrLsaInfo)->pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;
                    if ((pCurrPtr - (*pCurrLsaInfo)->pLsa) <
                        (*pCurrLsaInfo)->u2LsaLen)
                    {
                        u1Found = OSIX_TRUE;
                        break;
                    }
                }
            }
        }
        else
        {
            u1Found = OSIX_TRUE;
        }

        if (u1Found == OSIX_TRUE)
        {
            pLinkNode->u1LinkType = OSPFV3_LGET1BYTE (pCurrPtr);
            /* Skip the Reserved field */
            pCurrPtr++;
            pLinkNode->u2LinkMetric = OSPFV3_LGET2BYTE (pCurrPtr);
            pLinkNode->u4InterfaceId = OSPFV3_LGET4BYTE (pCurrPtr);
            pLinkNode->u4NbrIfId = OSPFV3_LGET4BYTE (pCurrPtr);
            OSPFV3_LGETSTR (pCurrPtr, pLinkNode->nbrRtrId, OSPFV3_RTR_ID_LEN);
            return pCurrPtr;
        }
    }
    else
    {
        if ((pCurrPtr - (*pCurrLsaInfo)->pLsa) < (*pCurrLsaInfo)->u2LsaLen)
        {
            pLinkNode->u1LinkType = OSPFV3_TRANSIT_LINK;
            pLinkNode->u2LinkMetric = 0;
            pLinkNode->u4InterfaceId = 0;
            pLinkNode->u4NbrIfId =
                OSPFV3_BUFFER_DWFROMPDU ((*pCurrLsaInfo)->lsaId.linkStateId);
            OSPFV3_LGETSTR (pCurrPtr, pLinkNode->nbrRtrId, OSPFV3_RTR_ID_LEN);
            return pCurrPtr;
        }
    }
    /* No more links are there to process */
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcIsChangeInSpfNodes                                    */
/*                                                                           */
/* Description  : This function checks for the change on 2 spf nodes         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE if 2 SPF are different                           */
/*                OSIX_FALSE otherwise                                       */
/*****************************************************************************/
PUBLIC INT4
V3RtcIsChangeInSpfNodes (tV3OsCandteNode * pOldSpfNode,
                         tV3OsCandteNode * pNewSpfNode)
{
    UINT1               u1HopIndex = 0;

    if (pOldSpfNode->u4Cost != pNewSpfNode->u4Cost)
    {
        return OSIX_TRUE;
    }
    else if (pOldSpfNode->u1HopCount != pNewSpfNode->u1HopCount)
    {
        return OSIX_TRUE;
    }
    else
    {
        for (u1HopIndex = 0; ((u1HopIndex < pOldSpfNode->u1HopCount) &&
                              (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1HopIndex++)
        {
            if (V3UtilIp6AddrComp
                (&pOldSpfNode->aNextHops[u1HopIndex].nbrIpv6Addr,
                 &pNewSpfNode->aNextHops[u1HopIndex].nbrIpv6Addr) !=
                OSPFV3_EQUAL)
            {
                return OSIX_TRUE;
            }
        }
        return OSIX_FALSE;
    }
}

/*****************************************************************************/
/* Function     : V3RtcSearchSpfTree                                         */
/*                                                                           */
/* Description  : Searches the SPF Tree and returns the particular           */
/*                candidate node.                                            */
/*                                                                           */
/* Input        : pSpfTree      : Pointer to SPF Tree.                       */
/*                pVertexRtrId  : Pointer vertex router Id.                  */
/*                u4VertexIfId  : Vertex interface identifier                */
/*                u1VertType    : Vertex type.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to SPF node , if found.                            */
/*              : NULL , otherwise.                                          */
/*****************************************************************************/
PUBLIC tV3OsCandteNode *
V3RtcSearchSpfTree (tV3OsSpf pSpfTree, tV3OsRouterId * pVertexRtrId,
                    UINT4 u4VertexIfId, UINT1 u1VertType)
{
    tV3OsCandteNode     spfNode;
    tV3OsCandteNode    *pSpfNode = NULL;

    spfNode.u1VertType = u1VertType;
    MEMCPY (&(spfNode.vertexId.vertexRtrId), pVertexRtrId, OSPFV3_RTR_ID_LEN);
    spfNode.vertexId.u4VertexIfId = u4VertexIfId;

    pSpfNode = ((tV3OsCandteNode *) RBTreeGet (pSpfTree,
                                               (tRBElem *) & spfNode));
    return pSpfNode;
}

/*****************************************************************************/
/* Function     : V3RtcAddToSpfTree                                          */
/*                                                                           */
/* Description  : Add the candidate node to SPF Tree                         */
/*                                                                           */
/* Input        : pSpfTree      : Pointer to SPF Tree Root.                  */
/*                pSpfNode      : Pointer to Candidate node which is to be   */
/*                                added to SPF Tree.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcAddToSpfTree (tV3OsSpf pSpfTree, tV3OsCandteNode * pSpfNode)
{
    if (RBTreeAdd (pSpfTree, (VOID *) pSpfNode) == RB_FAILURE)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "RBTreeAdd Failed\r\n");
    }
}

/*****************************************************************************/
/* Function     : V3RtcDeleteFromSpfTree                                     */
/*                                                                           */
/* Description  : Deletes SPF node from SPF tree                             */
/*                                                                           */
/* Input        : pSpfTree      : Pointer to SPF Tree Root.                  */
/*                pSpfNode      : Pointer to Candidate node which is to be   */
/*                                added to SPF Tree.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcDeleteFromSpfTree (tV3OsSpf pSpfTree, tV3OsCandteNode * pSpfNode)
{
    RBTreeRemove (pSpfTree, (VOID *) pSpfNode);
    OSPFV3_CANDTE_FREE (pSpfNode);
}

/*****************************************************************************/
/* Function     : V3RtcCreateCandteNode                                      */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure allocates a candidate node and initialises  */
/*                the vertexId, vertex type, next hop fields.                */
/*                                                                           */
/* Input        : pVertexRtrId    : Vertex Router Id.                        */
/*                u4VertexIfId    : Vertex Interface Id.                     */
/*                u1VertType      : Vertex type                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to candte node, if node successfully allocated and */
/*                initialized NULL, otherwise                                */
/*****************************************************************************/
PRIVATE tV3OsCandteNode *
V3RtcCreateCandteNode (tV3OsRouterId * pVertexRtrId, UINT4 u4VertexIfId,
                       UINT1 u1VertType)
{
    tV3OsCandteNode    *pCandteNode = NULL;

    /* Allocate memory for candidate node */
    OSPFV3_CANDTE_ALLOC (&(pCandteNode));
    if (NULL == pCandteNode)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_RTMODULE_TRC,
                        "Candidate Node Allocation Failure\n");
        return NULL;
    }

    /* Filling VertexId and Vertex Type information into the created 
     * candidate node */
    OSPFV3_RTR_ID_COPY (&pCandteNode->vertexId.vertexRtrId, pVertexRtrId);
    pCandteNode->vertexId.u4VertexIfId = u4VertexIfId;
    pCandteNode->u1VertType = u1VertType;

    /* Initializing other parameters of candidate node */
    pCandteNode->u4Cost = OSPFV3_LS_INFINITY_16BIT;
    pCandteNode->u1HopCount = 0;
    pCandteNode->u1Flag = OSPFV3_INDIRECT;
    pCandteNode->pDbNode = (tV3OsDbNode *) NULL;
    pCandteNode->u1Options = 0;
    pCandteNode->u1VirtReachable = OSIX_FALSE;

    TMO_HASH_INIT_NODE (&(pCandteNode->candteHashNode));

    OSPFV3_GBL_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    "New Candidate Node is " "Created\n");
    return pCandteNode;
}

/*****************************************************************************/
/* Function     : V3RtcSearchCandteLstInCxt                                  */
/*                                                                           */
/* Description  : Searches the candidate list and returns the candidate node */
/*                which is matching with the given parameters                */
/*                                                                           */
/* Input        : pV3OspfCxt    : Pointer to Context                         */
/*                pVertexRtrId  : Pointer to vertex router Id                */
/*                u4VertexIfId  : Vertex interface identifier                */
/*                u1VertType    : Vertex type.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to candidate node , if found.                      */
/*                NULL , otherwise.                                          */
/*****************************************************************************/
PRIVATE tV3OsCandteNode *V3RtcSearchCandteLstInCxt
    (tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pVertexRtrId,
     UINT4 u4VertexIfId, UINT1 u1VertType)
{
    tV3OsCandteNode    *pCandteNode = NULL;
    tTMO_HASH_NODE     *pNode = NULL;

    /* Candidate nodes are maintained in hash table with vertexId and 
     * vertex type as the key */
    TMO_HASH_Scan_Bucket (pV3OspfCxt->pCandteLst,
                          V3RtcCandteHashFunc (pVertexRtrId, u4VertexIfId,
                                               u1VertType),
                          pNode, tTMO_HASH_NODE *)
    {
        pCandteNode = OSPFV3_GET_BASE_PTR (tV3OsCandteNode,
                                           candteHashNode, pNode);
        if ((V3UtilRtrIdComp
             (pCandteNode->vertexId.vertexRtrId, *pVertexRtrId)
             == OSPFV3_EQUAL) &&
            (pCandteNode->vertexId.u4VertexIfId == u4VertexIfId))
        {
            return pCandteNode;
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcClearSpfTree                                          */
/*                                                                           */
/* Description  : Clears the SPF tree.                                       */
/*                                                                           */
/* Input        : pSpfTree   :  the SPF tree                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcClearSpfTree (tV3OsSpf pSpfTree)
{
    VOID               *pSpfNode = NULL;

    while ((pSpfNode = RBTreeGetFirst (pSpfTree)) != NULL)
    {
        RBTreeRemove (pSpfTree, pSpfNode);
        OSPFV3_SPF_FREE ((tV3OsCandteNode *) pSpfNode);
    }
}

/*****************************************************************************/
/* Function     : V3RtcCandteHashFunc                                        */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : pVertexRtrId  : Pointer to vertex router Id.               */
/*                u4VertexIfId  : Vertex interface identifier                */
/*                u1VertType    : Vertex type.                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash value of the Key.                                     */
/*****************************************************************************/
PRIVATE UINT4
V3RtcCandteHashFunc (tV3OsRouterId * pVertexRtrId, UINT4 u4VertexIfId,
                     UINT1 u1VertType)
{
    UINT1               au1Buf[OSPFV3_CAND_NODE_KEY_LEN];

    au1Buf[0] = u1VertType;

    MEMCPY ((au1Buf + 1), pVertexRtrId, sizeof (tV3OsRouterId));
    MEMCPY ((au1Buf + 1 + sizeof (tV3OsRouterId)), &u4VertexIfId,
            sizeof (UINT4));
    return V3UtilHashGetValue (OSPFV3_CANDTE_HASH_TABLE_SIZE, au1Buf,
                               (OSPFV3_CAND_NODE_KEY_LEN));
}

/*****************************************************************************/
/* Function     : V3RtcSearchLsaLinks                                        */
/*                                                                           */
/* Description  : This function used for following.                          */
/*                RFC 2328 -- Footnotes                                      */
/*                [23]Note that the presence of any link back to V is        */
/*                sufficient; it need not be the matching half of the        */
/*                link under consideration from V to W. This is enough       */
/*                to ensure that, before data traffic flows between a        */
/*                pair of neighboring routers, their link state databases    */
/*                will be synchronized.                                      */
/*                                                                           */
/* Input        : pPtr         : Pointer to the LSA information              */
/*                pVertexRtrId : Pointer to vertex router Id.                */
/*                pVertexRtrIfId: Pointer to Vertex Router's If Id.          */
/*                u1VertType    : Vertex Type                                */
/*                                                                           */
/* Output       : pu1CountFlag  : No.of Sby Links                            */
/*                au4IfId       : Array of Sby Interface Ids                 */
/*                au2IfMetric   : Array of Sby Interface's Metrics           */
/*                                                                           */
/* Returns      : OSIX_TRUE, if found                                        */
/*                OSIX_FALSE, otherwise                                      */
/*****************************************************************************/
PRIVATE UINT1
V3RtcSearchLsaLinks (UINT1 *pPtr, tV3OsRouterId * pVertexRtrId,
                     UINT4 *pVertexRtrIfId, UINT1 u1VertType,
                     UINT1 *pu1CountFlag, UINT4 au4IfId[], UINT2 au2IfMetric[])
{
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1              *pCurrPtr = NULL;
    UINT1              *pLinkPtr = NULL;
    UINT1              *pTempPrt = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsRouterId       nbrRtrId;
    UINT1               u1FoundFlag = OSIX_FALSE;
    UINT1               u1ArrayIndex = 0;
    UINT4               u4NbrIfId = 0;

    if (u1VertType == OSPFV3_VERT_ROUTER)
    {
        pDbHashNode = (tV3OsDbNode *) (VOID *) pPtr;

        TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);
            if (!(OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
            {
                pCurrPtr = pLsaInfo->pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;
                while ((pCurrPtr - pLsaInfo->pLsa) < pLsaInfo->u2LsaLen)
                {
                    pLinkPtr = pCurrPtr;
                    pTempPrt = pCurrPtr;
                    pCurrPtr = pCurrPtr + OSPFV3_RTR_LSA_NBR_RTRID_OFFSET;
                    pTempPrt = pTempPrt + (OSPFV3_RTR_LSA_NBR_RTRID_OFFSET - 4);

                    OSPFV3_LGETSTR (pCurrPtr, nbrRtrId, OSPFV3_RTR_ID_LEN);
                    MEMCPY (&u4NbrIfId, pTempPrt, OSPFV3_RTR_ID_LEN);
                    u4NbrIfId = OSIX_NTOHL (u4NbrIfId);

                    if ((V3UtilRtrIdComp (nbrRtrId, *pVertexRtrId) ==
                         OSPFV3_EQUAL))
                    {
                        if (u4NbrIfId == *pVertexRtrIfId)
                        {        /* Check for the Nbr's If Id - DR's If ID */

                            pLinkPtr = pLinkPtr + OSPFV3_RTR_LSA_METRIC_OFFSET;
                            MEMCPY (&(au2IfMetric[u1ArrayIndex]),
                                    pLinkPtr, OSPFV3_IF_METRIC_LEN);
                            au2IfMetric[u1ArrayIndex] = OSIX_NTOHS
                                (au2IfMetric[u1ArrayIndex]);

                            pLinkPtr = pLinkPtr + OSPFV3_RTR_LSA_IFID_OFFSET;
                            MEMCPY (&(au4IfId[u1ArrayIndex]), pLinkPtr,
                                    OSPFV3_RTR_ID_LEN);
                            au4IfId[u1ArrayIndex] = OSIX_NTOHL
                                (au4IfId[u1ArrayIndex]);

                            u1ArrayIndex++;
                        }
                        u1FoundFlag = OSIX_TRUE;

                    }
                    else
                    {
                        if (u1FoundFlag == OSIX_TRUE)
                        {
                            break;
                        }
                    }
                }
                if (u1FoundFlag == OSIX_TRUE)
                {
                    break;
                }
            }
        }
        if (u1FoundFlag == OSIX_TRUE)
        {                        /* Update the Standby Link Count */

            *pu1CountFlag = u1ArrayIndex;
            return OSIX_TRUE;
        }

    }
    else if (u1VertType == OSPFV3_VERT_NETWORK)
    {
        pLsaInfo = (tV3OsLsaInfo *) (VOID *) pPtr;

        if (!(OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
        {
            pCurrPtr = pLsaInfo->pLsa + OSPFV3_NTWR_LSA_FIXED_PORTION;

            while ((pCurrPtr - pLsaInfo->pLsa) < pLsaInfo->u2LsaLen)
            {
                OSPFV3_LGETSTR (pCurrPtr, nbrRtrId, OSPFV3_RTR_ID_LEN);

                if (V3UtilRtrIdComp (nbrRtrId, *pVertexRtrId) == OSPFV3_EQUAL)
                {
                    return OSIX_TRUE;
                }
            }
        }
    }
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function     : V3RtcSetNextHops                                           */
/*                                                                           */
/* Description  : Reference RFC-2328 Section 16.1.1.                         */
/*                Reference RFC-2740 Section 3.8.1.1.                        */
/*                This function calculates a set of next hops for destination*/
/*                'vertex' and specified parent. The next hops field in the  */
/*                vertex node is appropriately updated.                      */
/*                                                                           */
/* Input        : pSpfRoot    : Pointer to Root node of SPF Tree             */
/*                pParent     : Pointer to parent node                       */
/*                pVertex     : Pointer to the present vertex                */
/*                pLinkNode   : Pointer to link node                         */
/*                pArea       : Pointer to area                              */
/*                u1CountFlag : Flag to indicate whether Standby interface   */
/*                              links to be included as next hop (if metric  */
/*                              is same)                                     */
/*                au4IfId     : Array of Standby Interface ID                */
/*                au2IfMetric : Array of Standby Interface Metric            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcSetNextHops (tV3OsCandteNode * pSpfRoot, tV3OsCandteNode * pParent,
                  tV3OsCandteNode * pVertex, tV3OsLinkNode * pLinkNode,
                  tV3OsArea * pArea, UINT1 u1CountFlag, UINT4 au4IfId[],
                  UINT2 au2IfMetric[], UINT4 u4MinCost)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsLinkNode       TempLinkNode;
    UINT1               u1Index = 0;
    tIp6Addr            nextHop;
    UINT1               u1ArrayIndex;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER :  V3RtcSetNextHops \n");

    /* Already reached maximum hops */
    if (pVertex->u1HopCount >= OSPFV3_MAX_NEXT_HOPS)
    {
        return;
    }

    /* If the parent is ROOT node */
    if (pParent == pSpfRoot)
    {
        if ((pInterface = V3RtcGetFindIf (pLinkNode->u4InterfaceId,
                                          pArea)) == NULL)
        {
            return;
        }

        if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
        {
            /* Setting of the next hop should defer till 16.3 */
            pVertex->u1VirtReachable = OSIX_TRUE;
            return;
        }

        if (pVertex->u1VertType == OSPFV3_VERT_ROUTER)
        {
            if (V3RtcCalculateNextHop
                (pInterface, pVertex, pLinkNode, &nextHop,
                 u1CountFlag) != OSIX_SUCCESS)
            {
                /* Failed to set the next hop */
                return;
            }

            /* Add to the list of next hops already present */
            pVertex->aNextHops[pVertex->u1HopCount].pInterface = pInterface;
            OSPFV3_IP6_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                  nbrIpv6Addr, nextHop);
            pVertex->u1HopCount++;
        }
        else
        {
            MEMSET (&(pVertex->aNextHops[0].nbrIpv6Addr), 0,
                    OSPFV3_IPV6_ADDR_LEN);
            pVertex->aNextHops[0].pInterface = pInterface;
            pVertex->u1HopCount++;
            pVertex->u1Flag |= OSPFV3_DIRECT;
        }
    }
    else
    {
        if ((pParent->u1VertType == OSPFV3_VERT_ROUTER) &&
            (pVertex->u1VertType == OSPFV3_VERT_ROUTER))
        {
            V3RtcUpdateCandteNodeNextHops (pVertex, pParent);
        }
        else if ((pParent->u1VertType == OSPFV3_VERT_ROUTER) &&
                 (pVertex->u1VertType == OSPFV3_VERT_NETWORK))
        {
            if (pVertex->u1HopCount == 0)
            {
                pVertex->u1Flag |= OSPFV3_INDIRECT;
                if (pVertex->u1Flag & OSPFV3_DIRECT)
                {
                    pVertex->u1Flag -= OSPFV3_DIRECT;
                }
            }

            V3RtcUpdateCandteNodeNextHops (pVertex, pParent);
        }
        else if ((pParent->u1VertType == OSPFV3_VERT_NETWORK) &&
                 (pVertex->u1VertType == OSPFV3_VERT_ROUTER) &&
                 (pParent->u1Flag & OSPFV3_DIRECT))
        {
            pInterface = pParent->aNextHops[0].pInterface;
            if (u1CountFlag != 0)
            {                    /*Only if the Router is attached to a link with many Standby Interfaces */

                for (u1ArrayIndex = 0; u1ArrayIndex < u1CountFlag;
                     u1ArrayIndex++)
                {
                    if (au2IfMetric[u1ArrayIndex] == u4MinCost)
                    {            /* Equal Cost */

                        MEMSET (&(TempLinkNode), 0, sizeof (tV3OsLinkNode));
                        TempLinkNode.u4NbrIfId = pLinkNode->u4NbrIfId;
                        MEMCPY (TempLinkNode.nbrRtrId,
                                pLinkNode->nbrRtrId, OSPFV3_RTR_ID_LEN);
                        TempLinkNode.u4InterfaceId = au4IfId[u1ArrayIndex];
                        TempLinkNode.u2LinkMetric = pLinkNode->u2LinkMetric;
                        TempLinkNode.u1LinkType = pLinkNode->u1LinkType;

                        if (V3RtcCalculateNextHop
                            (pInterface, pVertex, &TempLinkNode, &nextHop,
                             u1CountFlag) != OSIX_SUCCESS)
                        {
                            continue;
                        }
                        /* Adding the Link as ECMP */
                        pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                            pInterface;
                        OSPFV3_IP6_ADDR_COPY (pVertex->
                                              aNextHops[pVertex->u1HopCount].
                                              nbrIpv6Addr, nextHop);
                        pVertex->u1HopCount++;

                    }

                }

            }
            else
            {
                if (V3RtcCalculateNextHop
                    (pInterface, pVertex, pLinkNode, &nextHop,
                     u1CountFlag) != OSIX_SUCCESS)
                {
                    /* Failed to set the next hop */
                    return;
                }

                pVertex->aNextHops[pVertex->u1HopCount].pInterface = pInterface;
                OSPFV3_IP6_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                      nbrIpv6Addr, nextHop);
                pVertex->u1HopCount++;
            }

            /* Inherit from the parent */
            for (u1Index = 1; ((u1Index < pParent->u1HopCount) &&
                               (u1Index < OSPFV3_MAX_NEXT_HOPS)); u1Index++)
            {
                /* Check if the number of next hops in node reaches max limit */
                if (pVertex->u1HopCount == OSPFV3_MAX_NEXT_HOPS)
                {
                    break;
                }

                pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                    pParent->aNextHops[u1Index].pInterface;
                OSPFV3_IP6_ADDR_COPY (pVertex->aNextHops[pVertex->u1HopCount].
                                      nbrIpv6Addr,
                                      pParent->aNextHops[u1Index].nbrIpv6Addr);
                pVertex->u1HopCount++;
            }
            pVertex->u1VirtReachable = pParent->u1VirtReachable;
        }
        else
        {
            V3RtcUpdateCandteNodeNextHops (pVertex, pParent);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT :  V3RtcSetNextHops\n");
}

/*****************************************************************************/
/* Function     : V3RtcCalculateNextHop                                      */
/*                                                                           */
/* Description  : This function calculates the next hop to reach a node      */
/*                                                                           */
/* Input        : pInterface - Pointer to interface through which this vertex*/
/*                             node is reachable                             */
/*                pVertex    - Pointer to vertex node                        */
/*                pLinkNode  - Pointer to link node                          */
/*                pNextHop   - Pointer to next hop where the calculcated     */
/*                             next hop is stored                            */
/*                u1CountFlag - No.of Stanby Links                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if found next hop                             */
/*                OSIX_FAILURE otherwise                                     */
/*****************************************************************************/
PUBLIC INT4
V3RtcCalculateNextHop (tV3OsInterface * pInterface, tV3OsCandteNode * pVertex,
                       tV3OsLinkNode * pLinkNode, tIp6Addr * pNextHop,
                       UINT1 u1CountFlag)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pCurrLsaInfo = NULL;
    UINT1              *pCurrPtr = NULL;
    tV3OsLinkNode       linkNode;
    tV3OsLinkStateId    linkStateId;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Found = OSIX_FALSE;

    if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        if (pLinkNode->u1LinkType != OSPFV3_PTOP_LINK)
        {
            return OSIX_FAILURE;
        }

        OSPFV3_BUFFER_DWTOPDU (linkStateId, pLinkNode->u4NbrIfId);

        pLsaInfo = V3LsuSearchDatabase (OSPFV3_LINK_LSA, &linkStateId,
                                        &pLinkNode->nbrRtrId, pInterface, NULL);
        if (pLsaInfo != NULL)
        {
            pCurrPtr = pLsaInfo->pLsa + OSPFV3_LINK_LSA_HDR_LEN;
            MEMCPY (pNextHop, pCurrPtr, OSPFV3_IPV6_ADDR_LEN);
            return OSIX_SUCCESS;
        }
        else
        {                        /* In case of Link LSA Suppression - Get the Next hop from the Hello
                                   Information */

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
            {
                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                            nextNbrInIf, pLstNode);
                if (V3UtilRtrIdComp (pNbr->nbrRtrId,
                                     pLinkNode->nbrRtrId) == OSPFV3_EQUAL)
                {
                    MEMCPY (pNextHop,
                            &(pNbr->nbrIpv6Addr), OSPFV3_IPV6_ADDR_LEN);
                    return OSIX_SUCCESS;
                }
            }
        }
    }
    else
    {
        if ((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ||
            (pInterface->u1NetworkType == OSPFV3_IF_NBMA))
        {
            if (pLinkNode->u1LinkType != OSPFV3_TRANSIT_LINK)
            {
                return OSIX_FAILURE;
            }

            while ((pCurrPtr = V3RtcGetLinksFromLsa (pVertex,
                                                     pCurrPtr,
                                                     &linkNode,
                                                     &pCurrLsaInfo)) != NULL)
            {
                if (linkNode.u1LinkType != OSPFV3_TRANSIT_LINK)
                {
                    continue;
                }
                if ((linkNode.u4NbrIfId == pLinkNode->u4NbrIfId) &&
                    (V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                                      linkNode.nbrRtrId) == OSPFV3_EQUAL))
                {
                    if (u1CountFlag != 0)
                    {
                        if (linkNode.u4InterfaceId == pLinkNode->u4InterfaceId)
                        {
                            u1Found = OSIX_TRUE;
                            break;
                        }

                    }
                    else
                    {
                        u1Found = OSIX_TRUE;
                        break;
                    }
                }
            }

            if (u1Found == OSIX_TRUE)
            {
                OSPFV3_BUFFER_DWTOPDU (linkStateId, linkNode.u4InterfaceId);
                pLsaInfo = V3LsuSearchDatabase (OSPFV3_LINK_LSA,
                                                &linkStateId,
                                                &pVertex->vertexId.vertexRtrId,
                                                pInterface, NULL);
                if (pLsaInfo != NULL)
                {
                    pCurrPtr = pLsaInfo->pLsa + OSPFV3_LINK_LSA_HDR_LEN;
                    MEMCPY (pNextHop, pCurrPtr, OSPFV3_IPV6_ADDR_LEN);
                    return OSIX_SUCCESS;
                }
            }
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function     : V3RtcUpdateCandteNodeNextHops                              */
/*                                                                           */
/* Description  : This function checks for the duplication and updates the   */
/*                vertex node next hops from parent.                         */
/*                                                                           */
/* Input        : pVertex : Pointer to vertex node whose next hops are to    */
/*                          be updated.                                      */
/*                pParent : Pointer to the parent node.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcUpdateCandteNodeNextHops (tV3OsCandteNode * pVertex,
                               tV3OsCandteNode * pParent)
{
    UINT1               u1VertexHopCount = 0;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    UINT1               u1HopPresent = OSIX_FALSE;

    u1VertexHopCount = pVertex->u1HopCount;

    if (pVertex->u1HopCount >= OSPFV3_MAX_NEXT_HOPS)
    {
        return;
    }

    pVertex->u1VirtReachable = pParent->u1VirtReachable;

    for (u1Index1 = 0; ((u1Index1 < pParent->u1HopCount) &&
                        (u1Index1 < OSPFV3_MAX_NEXT_HOPS)); u1Index1++)
    {
        for (u1Index2 = 0; ((u1Index2 < u1VertexHopCount) &&
                            (u1Index2 < OSPFV3_MAX_NEXT_HOPS)); u1Index2++)
        {
            if ((pParent->aNextHops[u1Index1].pInterface ==
                 pVertex->aNextHops[u1Index2].pInterface) &&
                (V3UtilIp6AddrComp
                 (&pParent->aNextHops[u1Index1].nbrIpv6Addr,
                  &pVertex->aNextHops[u1Index2].nbrIpv6Addr) == OSPFV3_EQUAL))
            {
                u1HopPresent = OSIX_TRUE;
                break;
            }
        }

        if (u1HopPresent == OSIX_FALSE)
        {
            pVertex->aNextHops[pVertex->u1HopCount].pInterface =
                pParent->aNextHops[u1Index1].pInterface;
            OSPFV3_IP6_ADDR_COPY (pVertex->
                                  aNextHops[pVertex->u1HopCount].
                                  nbrIpv6Addr,
                                  pParent->aNextHops[u1Index1].nbrIpv6Addr);
            pVertex->u1HopCount++;
            /* Check if the number of next hops in node reaches max limit */
            if (pVertex->u1HopCount == OSPFV3_MAX_NEXT_HOPS)
            {
                return;
            }
        }
        else
        {
            u1HopPresent = OSIX_FALSE;
        }
    }
}

/*****************************************************************************/
/* Function     : V3RtcProcIntraPrefLsaOfSpfNode                             */
/*                                                                           */
/* Description  : This function process the intra area prefix LSA of the     */
/*                spf node.                                                  */
/*                                                                           */
/* Input        : pSpfNode      : Pointer to SPF node.                       */
/*                pArea         : Pointer to area.                           */
/*                u1Status      : Indicating the created/updated/deleted     */
/*                                state of the SPF node.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcIntraPrefLsaOfSpfNode (tV3OsCandteNode * pSpfNode,
                                tV3OsArea * pArea, UINT1 u1Status)
{
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4CurrentTime = 0;
    UINT2               u2RefLsType = OSPFV3_INVALID;

    if (pSpfNode->u1VertType == OSPFV3_VERT_ROUTER)
    {
        u2RefLsType = OSPFV3_REF_ROUTER_LSA;
    }
    else
    {
        u2RefLsType = OSPFV3_REF_NETWORK_LSA;
    }

    pDbHashNode =
        V3RtcSearchIntraAreaPrefixNode (&(pSpfNode->vertexId.
                                          vertexRtrId),
                                        pSpfNode->vertexId.u4VertexIfId,
                                        u2RefLsType, pArea);
    if (pDbHashNode != NULL)
    {
        TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);
            V3RtcProcessIntraAreaPrefLsa (pSpfNode, pLsaInfo, pLsaInfo->pLsa,
                                          pArea, u1Status);
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >= pArea->pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pArea->pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }

        }
    }
    else
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "No Intra area prefix LSA present for " "candidate node\n");
    }
}

/*****************************************************************************/
/* Function     : V3RtcCheckLsaMaxAge                                        */
/*                                                                           */
/* Description  : Function to check the age of the Router LSAs or Network    */
/*                LSAs. This is used during SPF tree building.               */
/*                If all the Router LSAs generated by the vertex router are  */
/*                of MAX_AGE then the Vertex router node is not added to     */
/*                candidate list. Similarly if the network LSA is of         */
/*                MAX_AGE then vertex network corresponding to the LSA is not*/
/*                added to candidate list.                                   */
/*                                                                           */
/* Input        : pPtr       : Pointer to LSA information                    */
/*                u1VertType : Vertex Type                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE if all Router LSAs or Network LSA is MAX_AGE     */
/*                else returns OSIX_FALSE                                    */
/*****************************************************************************/
PRIVATE INT4
V3RtcCheckLsaMaxAge (UINT1 *pPtr, UINT1 u1VertType)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (u1VertType == OSPFV3_VERT_ROUTER)
    {
        pDbHashNode = (tV3OsDbNode *) (VOID *) pPtr;
        TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);
            if (!(OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
            {
                return OSIX_FALSE;
            }
        }
    }
    else
    {
        pLsaInfo = (tV3OsLsaInfo *) (VOID *) pPtr;
        if (!(OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
        {
            return OSIX_FALSE;
        }
    }
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function     : V3RtcExtractVEBBitOptions                                  */
/*                                                                           */
/* Description  : This function extracts VEB bit options from router LSAs    */
/*                                                                           */
/* Input        : pVertex    : Pointer to Vertex node                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcExtractVEBBitOptions (tV3OsCandteNode * pVertex)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (pVertex->pDbNode == NULL)
    {
        return;
    }

    TMO_SLL_Scan (&(pVertex->pDbNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

        if (!OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            pVertex->u1Options = *(pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE);
            return;
        }
    }
}

/*****************************************************************************/
/* Function     : V3RtcGetFindIf                                             */
/*                                                                           */
/* Description  : This function extracts VEB bit options from router LSAs    */
/*                                                                           */
/* Input        : u4InterfaceId : Interface identifier.                      */
/*                pArea         : Pointer to area in which interface is      */
/*                                present.                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to interface if found, NULL otherwise.             */
/*****************************************************************************/
PRIVATE tV3OsInterface *
V3RtcGetFindIf (UINT4 u4InterfaceId, tV3OsArea * pArea)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface      interface;

    interface.u4InterfaceId = u4InterfaceId;

    pInterface = (tV3OsInterface *) RBTreeGet (gV3OsRtr.pIfRBRoot, &interface);

    if (pInterface != NULL)
    {
        return pInterface;
    }

    if (V3UtilAreaIdComp (pArea->areaId, OSPFV3_NULL_RTRID) == OSPFV3_EQUAL)
    {
        TMO_SLL_Scan (&pArea->pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == u4InterfaceId)
            {
                return pInterface;
            }
        }
    }
    return NULL;
}

/*----------------------------------------------------------------------*/
/*                     End of the file o3spf.c                       */
/*----------------------------------------------------------------------*/
