/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lscons.c,v 1.17 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures for generating
 *             LSAs
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE UINT1 V3FillOptionInLsa PROTO ((tV3OsArea * pArea));

PRIVATE UINT1       V3SetEFTBitInAsOrNssaLsa
PROTO ((UINT4 u4MetricType, tIp6Addr * pfwdAddr, UINT4 u4ExtRtTag));

PRIVATE tIp6Addr   *V3SetNssaFwdAddr PROTO ((tV3OsArea * pArea));

PRIVATE VOID        V3ConstructLsHeaderInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pLsa, UINT2 u2Len, UINT2 u2LsaType,
        tV3OsLinkStateId * pLinkStateId, tV3OsLsaSeqNum i4LsaSeqNum));

PRIVATE UINT1       V3FillOptionInNetworkLsa
PROTO ((tV3OsInterface * pInterface));

PRIVATE UINT1      *V3GetFwdAddress PROTO ((UINT1 *pLsa));

PRIVATE UINT4       V3IsFunctionalityEqvLsa
PROTO ((tV3OsMetric * pMetric, tV3OsMetric * pDbMetric,
        tIp6Addr * pfwdaddr, tIp6Addr * pdbfwdaddr));

/****************************************************************************/
/*                                                                          */
/* Function        :  V3ConstructIntraAreaPrefixLsaInCxt                    */
/*                                                                          */
/* Description     :  Constructs Intra Area Prefix LSA.                     */
/*                                                                          */
/* Input           :  tV3OspfCxt : Context pointer                          */
/*                    pPtr       : Pointer to Intra Area Prefix LSA buffer  */
/*                    i4SeqNum   : Sequence number                          */
/*                    pLsId      : Link State Id.                           */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : Pointer to Intra Area Prefix LSA, if successfully      */
/*                   constructed                                            */
/*                   NULL, Otherwise                                        */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT1       *
V3ConstructIntraAreaPrefixLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr,
                                    tV3OsLsaSeqNum i4SeqNum,
                                    tV3OsLinkStateId * pLsId)
{

    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructIntraAreaPrefixLsa\n");

    OSPFV3_BUFFER_GET_2_BYTE (pPtr, OSPFV3_LENGTH_OFFSET_IN_LS_HEADER,
                              u2LsaLen);

    V3ConstructLsHeaderInCxt (pV3OspfCxt, pPtr, u2LsaLen,
                              OSPFV3_INTRA_AREA_PREFIX_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructIntraAreaPrefixLsa\n");

    return pPtr;
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3ConstructNetworkLsa                                 */
/*                                                                          */
/* Description     :  Constructs  Network LSA.                              */
/*                                                                          */
/* Input           :  pInterface : Pointer to interface to which the        */
/*                                 Network LSA is to be generated.          */
/*                    i4SeqNum   : Sequence number                          */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : Pointer to network LSA, if successfully constructed    */
/*                   NULL, Otherwise                                        */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT1       *
V3ConstructNetworkLsa (tV3OsInterface * pInterface, tV3OsLsaSeqNum i4SeqNum)
{

    UINT1              *pNetworkLsa = NULL;
    UINT1              *pCurrent = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT1               u1Option = 0;
    tV3OsLinkStateId    linkStateId;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructNetworkLsa\n");

    OSPFV3_TRC1 (OSPFV3_LSU_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Network LSA to be constructed for interface : %d \n",
                 pInterface->u4InterfaceId);

    OSPFV3_LSA_ALLOC (&(pNetworkLsa));
    if (NULL == pNetworkLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Network LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Network LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pNetworkLsa;

    /* leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    OSPFV3_LADD3BYTE (pCurrent, 0);

    /* Option need to fill here */
    u1Option = V3FillOptionInNetworkLsa (pInterface);

    OSPFV3_LADD1BYTE (pCurrent, u1Option);

    /* router id of this router. */
    OSPFV3_LADDSTR (pCurrent, OSPFV3_RTR_ID (pInterface->pArea->pV3OspfCxt),
                    OSPFV3_RTR_ID_LEN);

    /* router ids of all neighbors who are fully adjacent */
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        if ((pNbr->u1NsmState == OSPFV3_NBRS_FULL) ||
            (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING))
        {
            OSPFV3_LADDSTR (pCurrent, pNbr->nbrRtrId, OSPFV3_RTR_ID_LEN);
        }
    }

    u2LsaLen = (UINT2) (pCurrent - pNetworkLsa);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);

    V3ConstructLsHeaderInCxt (pInterface->pArea->pV3OspfCxt, pNetworkLsa,
                              u2LsaLen, OSPFV3_NETWORK_LSA,
                              &(linkStateId), i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Network LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructNetworkLsa\n");

    return pNetworkLsa;
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3ConstructLinkLsa                                    */
/*                                                                          */
/* Description     :  Constructs Link LSA.                                  */
/*                                                                          */
/* Input           :  pInterface :   pointer to interface to which the      */
/*                                   Link  LSA is to be generated.          */
/*                    i4SeqNum   :   sequence number                        */
/*                                                                          */
/* Output          :  None.                                                 */
/*                                                                          */
/*  Returns        : Pointer to Link LSA, if successfully constructed       */
/*                   NULL, Otherwise                                        */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT1       *
V3ConstructLinkLsa (tV3OsInterface * pInterface,
                    tV3OsLsaSeqNum i4SeqNum, UINT1 *pu1LinkSup)
{

    UINT1              *pLinkLsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               u1Option = 0;
    UINT1               u1PrefOption = 0;
    tV3OsLinkStateId    linkStateId;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    UINT4               u4PrefixCount = 0;
    UINT2               u2LinkLsaSize = 0;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructLinkLsa\n");

    OSPFV3_TRC1 (OSPFV3_LSU_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Link LSA to be constructed for interface : %d \n",
                 pInterface->u4InterfaceId);
    if ((pInterface->bLinkLsaSuppress == OSPFV3_ENABLED) &&
        ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
         (pInterface->u1NetworkType == OSPFV3_IF_PTOMP)))
    {
        *pu1LinkSup = OSPFV3_ONE;
        return NULL;
    }

    u2LinkLsaSize = (UINT2) (OSPFV3_LINK_LSA_SIZE (pInterface));

    OSPFV3_LSA_ALLOC (&(pLinkLsa));
    if (NULL == pLinkLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Link LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Link LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pLinkLsa;

    MEMSET (pLinkLsa, 0, u2LinkLsaSize);

    pCurrent += OSPFV3_LS_HEADER_SIZE;

    OSPFV3_LADD1BYTE (pCurrent, pInterface->u1RtrPriority);
    OSPFV3_LADD2BYTE (pCurrent, 0);

    /* Option need to fill here */
    u1Option = V3FillOptionInLsa (pInterface->pArea);

    OSPFV3_LADD1BYTE (pCurrent, u1Option);

    OSPFV3_LADDSTR (pCurrent, &pInterface->ifIp6Addr, OSPFV3_IPV6_ADDR_LEN);

    u4PrefixCount = TMO_SLL_Count (&(pInterface->ip6AddrLst));

    OSPFV3_LADD4BYTE (pCurrent, u4PrefixCount);
    /* router ids of all neighbors who are fully adjacent */

    TMO_SLL_Scan (&(pInterface->ip6AddrLst), pPrefixNode, tV3OsPrefixNode *)
    {
        u1PrefOption = 0;
        if (pPrefixNode->prefixInfo.u1PrefixLength == OSPFV3_MAX_PREFIX_LEN)
        {
            u1PrefOption |= OSPFV3_LA_BIT_MASK;
        }

        OSPFV3_ADD_LINK_LSA_PREFIX (pCurrent,
                                    pPrefixNode->prefixInfo.u1PrefixLength,
                                    u1PrefOption,
                                    pPrefixNode->prefixInfo.addrPrefix);

    }

    u2LsaLen = (UINT2) (pCurrent - pLinkLsa);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);

    V3ConstructLsHeaderInCxt (pInterface->pArea->pV3OspfCxt, pLinkLsa,
                              u2LsaLen, OSPFV3_LINK_LSA,
                              &(linkStateId), i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Link LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructLinkLsa\n");

    return pLinkLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function       :  V3ConstructInterAreaPrefixLsaInCxt                      */
/*                                                                           */
/* Description    :  Constructs a Inter Area Prefix LSA.                     */
/*                                                                           */
/* Input          :  pV3OspfCxt: Context pointer                             */
/*                   u2Type    : Internal type.                              */
/*                               OSPFV3_COND_INTER_AREA_PREFIX_LSA |         */
/*                               OSPFV3_INTER_AREA_PREFIX_LSA.               */
/*                   pStruct   : Structure associated with it.               */
/*                               pAddrRange / pSummaryParam                  */
/*                   i4SeqNum  : Sequence number.                            */
/*                   pLsId     : Link State Id.                              */
/*                                                                           */
/* Output         :  None.                                                   */
/*                                                                           */
/* Returns        :  Pointer to LSA, if successfully constructed             */
/*                   NULL, Otherwise                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3ConstructInterAreaPrefixLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT2 u2Type,
                                    UINT1 *pStruct, tV3OsLsaSeqNum i4SeqNum,
                                    tV3OsLinkStateId * pLsId)
{
    UINT1              *pInterAreaPrefixLsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructInterAreaPrefixLsaInCxt\n");

    OSPFV3_EXT_LSA_ALLOC (&(pInterAreaPrefixLsa));
    if (NULL == pInterAreaPrefixLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Inter Area Prefix LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Inter Area Prefix LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pInterAreaPrefixLsa;

    /* leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    /* Fill zero for unused */
    OSPFV3_LADD1BYTE (pCurrent, 0);

    if (u2Type == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        OSPFV3_ADD_ADDR_PREFIX (pCurrent,
                                ((tV3OsSummaryParam *) (VOID *)
                                 pStruct)->u4Metric,
                                ((tV3OsSummaryParam *) (VOID *)
                                 pStruct)->prefixInfo.
                                u1PrefixLength,
                                (((tV3OsSummaryParam *) (VOID *) pStruct)->
                                 prefixInfo.addrPrefix));
    }
    else
    {
        OSPFV3_ADD_ADDR_PREFIX (pCurrent,
                                ((tV3OsAddrRange *) (VOID *) pStruct)->metric.
                                u4Metric,
                                ((tV3OsAddrRange *) (VOID *) pStruct)->
                                u1PrefixLength,
                                (((tV3OsAddrRange *) (VOID *) pStruct)->
                                 ip6Addr));
    }

    u2LsaLen = (UINT2) (pCurrent - pInterAreaPrefixLsa);

    V3ConstructLsHeaderInCxt (pV3OspfCxt, pInterAreaPrefixLsa, u2LsaLen,
                              OSPFV3_INTER_AREA_PREFIX_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                "Inter Area Prefix LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructInterAreaPrefixLsaInCxt\n");

    return pInterAreaPrefixLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3ConstructInterAreaRouterLsa                          */
/*                                                                           */
/* Description     :  Constructs a Inter Area Router LSA.                    */
/*                                                                           */
/* Input           :  pArea     : Pointer to the area in which the           */
/*                                Inter Area Router LSA is to be generated.  */
/*                    u2Type    : Internal type.                             */
/*                                OSPFV3_INDICATION_LSA |                    */
/*                                OSPFV3_INTER_AREA_ROUTER_LSA.              */
/*                    pStruct   : Structure associated with it.              */
/*                                pSummaryParam                              */
/*                    i4SeqNum  : Sequence number.                           */
/*                    pLsId     : Link State Id.                             */
/*                                                                           */
/* Output          :  None.                                                  */
/*                                                                           */
/* Returns         :  Pointer to LSA, if successfully constructed            */
/*                    NULL, Otherwise                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3ConstructInterAreaRouterLsa (tV3OsArea * pArea, UINT2 u2Type,
                               UINT1 *pStruct, tV3OsLsaSeqNum i4SeqNum,
                               tV3OsLinkStateId * pLsId)
{

    UINT1              *pInterAreaRouterLsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               u1Option = 0;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructInterAreaRouterLsa\n");

    OSPFV3_EXT_LSA_ALLOC (&(pInterAreaRouterLsa));
    if (NULL == pInterAreaRouterLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Inter Area Router LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "Inter Area Router LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pInterAreaRouterLsa;

    /* leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    /* Fill zero for unused */
    OSPFV3_LADD3BYTE (pCurrent, 0);

    u1Option = V3FillOptionInLsa (pArea);

    if (u2Type == OSPFV3_INDICATION_LSA)
    {
        u1Option &= ~OSPFV3_DC_BIT_MASK;
    }

    OSPFV3_LADD1BYTE (pCurrent, u1Option);

    OSPFV3_LADD1BYTE (pCurrent, 0);

    switch (u2Type)
    {
        case OSPFV3_INTER_AREA_ROUTER_LSA:
            OSPFV3_LADD3BYTE (pCurrent,
                              ((tV3OsSummaryParam *) (VOID *) pStruct)->
                              u4Metric);

            OSPFV3_LADDSTR (pCurrent, pLsId, OSPFV3_RTR_ID_LEN);
            break;

        case OSPFV3_INDICATION_LSA:
            OSPFV3_LADD3BYTE (pCurrent, OSPFV3_LS_INFINITY_24BIT);
            OSPFV3_LADDSTR (pCurrent, pLsId, OSPFV3_RTR_ID_LEN);
            break;

        default:
            break;
    }

    u2LsaLen = (UINT2) (pCurrent - pInterAreaRouterLsa);

    V3ConstructLsHeaderInCxt (pArea->pV3OspfCxt, pInterAreaRouterLsa, u2LsaLen,
                              OSPFV3_INTER_AREA_ROUTER_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                "Inter Area Router LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructInterAreaRouterLsa\n");

    return pInterAreaRouterLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :   V3ConstructRtrLsa                                     */
/*                                                                           */
/* Description     :   Constructs a Router LSA.                              */
/*                                                                           */
/* Input           :   pArea      : Pointer to area to which the router LSA  */
/*                                  is to be generated.                      */
/*                     i4SeqNum   : Sequence number.                         */
/*                     pLsId      : Link State Id.                           */
/*                                                                           */
/* Output          :   None.                                                 */
/*                                                                           */
/* Returns         :   pointer to router LSA, if successfully constructed.   */
/*                     NULL, Otherwise.                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3ConstructRtrLsa (tV3OsArea * pArea, tV3OsLsaSeqNum i4SeqNum,
                   tV3OsLinkStateId * pLsId)
{

    tTMO_SLL_NODE      *pIfNode = NULL;
    tTMO_SLL_NODE      *pStdNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsNeighbor      *pStandbyLst = NULL;
    UINT1              *pRtrLsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT1               u1LinkType = 0;
    UINT1               u1Option = 0;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructRtrLsa\n");

    OSPFV3_TRC1 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "Router LSA to be constructed for area : %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    OSPFV3_LSA_ALLOC (&(pRtrLsa));
    if (NULL == pRtrLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Router LSA\n"));

        OSPFV3_TRC (OSPFV3_ADJACENCY_TRC | OS_RESOURCE_TRC,
                    pArea->pV3OspfCxt->u4ContextId, "Rtr LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pRtrLsa;

    /* Leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    if ((pArea->u4AreaType == OSPFV3_NSSA_AREA)
        && (pArea->u1NssaTrnsltrRole == OSPFV3_TRNSLTR_ROLE_ALWAYS))
    {
        u1Option |= OSPFV3_NT_BIT_MASK;
    }

    if (OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt))
    {
        u1Option |= OSPFV3_ABR_BIT_MASK;
    }

    if ((OSPFV3_IS_NSSA_ABR (pArea->pV3OspfCxt)) ||
        ((OSPFV3_IS_AS_BOUNDARY_RTR (pArea->pV3OspfCxt)) &&
         (!OSPFV3_IS_STUB_AREA (pArea))))
    {
        u1Option |= OSPFV3_ASBR_BIT_MASK;
    }

    /* Setting the V_BIT in the RTR LSA */
    if (pArea->u4FullVirtNbrCount != 0)
    {
        u1Option = u1Option | OSPFV3_V_BIT_MASK;
    }

    OSPFV3_LADD1BYTE (pCurrent, u1Option);

    /* Field reset to be reused */
    u1Option = 0;

    OSPFV3_LADD2BYTE (pCurrent, 0);

    u1Option = V3FillOptionInLsa (pArea);

    OSPFV3_LADD1BYTE (pCurrent, u1Option);

    TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfNode);

        switch (pInterface->u1IsmState)
        {
            case OSPFV3_IFS_DOWN:
                break;

            case OSPFV3_IFS_PTOP:
                if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
                    (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL))
                {
                    if (V3UtilLinkStateIdComp (pInterface->linkStateId,
                                               *pLsId) != OSPFV3_EQUAL)
                    {
                        break;
                    }

                    if ((pNbrNode =
                         TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
                    {
                        pNbr = OSPFV3_GET_BASE_PTR
                            (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                        /* Add PTOP or virtual link */
                        if ((OSPFV3_IS_NBR_FULL (pNbr)) ||
                            (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING))
                        {
                            u1LinkType = (OSPFV3_IS_VIRTUAL_IFACE (pInterface) ?
                                          OSPFV3_VIRTUAL_LINK :
                                          OSPFV3_PTOP_LINK);

                            OSPFV3_ADD_RTR_LSA_LINK (pCurrent, u1LinkType,
                                                     (UINT2) pInterface->
                                                     u4IfMetric,
                                                     pInterface->u4InterfaceId,
                                                     pNbr->u4NbrInterfaceId,
                                                     pNbr->nbrRtrId);
                        }
                    }
                }
                else if (pInterface->u1NetworkType == OSPFV3_IF_PTOMP)
                {
                    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                                  tTMO_SLL_NODE *)
                    {
                        pNbr = OSPFV3_GET_BASE_PTR
                            (tV3OsNeighbor, nextNbrInIf, pNbrNode);
                        if (V3UtilLinkStateIdComp (pNbr->linkStateId,
                                                   *pLsId) != OSPFV3_EQUAL)
                        {
                            continue;
                        }

                        if ((OSPFV3_IS_NBR_FULL (pNbr)) ||
                            (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING))
                        {
                            OSPFV3_ADD_RTR_LSA_LINK (pCurrent, OSPFV3_PTOP_LINK,
                                                     (UINT2) pInterface->
                                                     u4IfMetric,
                                                     pInterface->u4InterfaceId,
                                                     pNbr->u4NbrInterfaceId,
                                                     pNbr->nbrRtrId);

                        }
                    }
                }
                break;

            case OSPFV3_IFS_LOOP_BACK:
                break;
            case OSPFV3_IFS_STANDBY:

                pStdNode = NULL;
                TMO_SLL_Scan (&(pInterface->nbrsInIf), pStdNode,
                              tTMO_SLL_NODE *)
                {
                    pStandbyLst = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                       nextNbrInIf, pStdNode);
                    if (pStandbyLst->u1NbrIfStatus == OSPFV3_ACTIVE_ON_LINK)
                    {
                        break;
                    }
                }

                if (pStandbyLst == NULL)
                {
                    break;
                }

                /* Check the link type of the Active Interface */
                if (V3IsTransitLink (pStandbyLst->pActiveInterface)
                    == OSIX_TRUE)
                {
                    if (V3UtilLinkStateIdComp
                        (pStandbyLst->pActiveInterface->linkStateId,
                         *pLsId) != OSPFV3_EQUAL)
                    {
                        break;
                    }

                    OSPFV3_ADD_RTR_LSA_LINK (pCurrent, OSPFV3_TRANSIT_LINK,
                                             (UINT2) pInterface->u4IfMetric,
                                             pInterface->u4InterfaceId,
                                             pStandbyLst->pActiveInterface->
                                             u4DRInterfaceId,
                                             pStandbyLst->pActiveInterface->
                                             desgRtr);
                }
                break;

            default:
                if (V3IsTransitLink (pInterface) == OSIX_TRUE)
                {
                    if (V3UtilLinkStateIdComp (pInterface->linkStateId,
                                               *pLsId) != OSPFV3_EQUAL)
                    {
                        break;
                    }

                    OSPFV3_ADD_RTR_LSA_LINK (pCurrent, OSPFV3_TRANSIT_LINK,
                                             (UINT2) pInterface->u4IfMetric,
                                             pInterface->u4InterfaceId,
                                             pInterface->u4DRInterfaceId,
                                             pInterface->desgRtr);

                }
                break;
        }
    }

    u2LsaLen = (UINT2) (pCurrent - pRtrLsa);

    V3ConstructLsHeaderInCxt (pArea->pV3OspfCxt, pRtrLsa, u2LsaLen,
                              OSPFV3_ROUTER_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                "Router LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructRtrLsa\n");

    return pRtrLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3AddFwdAddrToLstInCxt                                  */
/*                                                                           */
/* Description     : Inserts Forwarding Address of the LSA in global         */
/*                   Forwarding address list maintained by the Router        */
/*                                                                           */
/* Input           : pV3OspfCxt  :  Context pointer                          */
/*                   fwdAddr     : IPv6 Address                              */
/*                                                                           */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AddFwdAddrToLstInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr fwdAddr)
{
    tV3OsFwdAddr       *pfwdAddrNode = NULL;
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;
    UINT1               u1FwdAddrFound = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3AddFwdAddrToLstInCxt\n");

    OSPFV3_FWD_ADDR_ALLOC (&(pfwdAddrNode));
    if (NULL == pfwdAddrNode)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Forwarding Address\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC | OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                    " Forwarding Address  Alloc Failed\n");
        return;
    }

    OSPFV3_IP6ADDR_CPY (&(pfwdAddrNode->fwdAddr), &(fwdAddr));

    if (TMO_SLL_Count (&(pV3OspfCxt->fwdAddrLst)) == 0)
    {
        TMO_SLL_Add (&(pV3OspfCxt->fwdAddrLst), &(pfwdAddrNode->nextFwdAddr));
        return;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->fwdAddrLst), pScanFwdAddrNode, tV3OsFwdAddr *)
    {
        if (V3UtilIp6AddrComp (&(pScanFwdAddrNode->fwdAddr),
                               &(fwdAddr)) == OSPFV3_EQUAL)
        {
            u1FwdAddrFound = OSIX_TRUE;
            break;
        }
    }
    if (u1FwdAddrFound == OSIX_FALSE)
    {
        TMO_SLL_Add (&(pV3OspfCxt->fwdAddrLst), &(pfwdAddrNode->nextFwdAddr));
    }
    else
    {
        OSPFV3_FWD_ADDR_FREE (pfwdAddrNode);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AddFwdAddrToLstInCxt\n");
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3ConstructAsExtLsaInCxt                                */
/*                                                                           */
/* Description     : Constructs an AS External LSA.                          */
/*                                                                           */
/* Input           : pV3OspfCxt : Context pointer                            */
/*                   pPtr       : Pointer associated with.                   */
/*                                pAsExtRange / pAddrRange / pRtEntry /      */
/*                                pExtRoute                                  */
/*                   u2InternalType  : Internal type of LSA.                 */
/*                   i4SeqNum   : Sequence number.                           */
/*                   pLsId      : Link State Id.                             */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to ASE LSA,    if successfully constructed      */
/*                   NULL,                  otherwise                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3ConstructAsExtLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr,
                          UINT2 u2InternalType, tV3OsLsaSeqNum i4SeqNum,
                          tV3OsLinkStateId * pLsId)
{

    UINT1              *pAsExtLsa = NULL;
    UINT1              *pCurrent = NULL;
    UINT4               u4RangeValue = 0;
    UINT1               u1Option = 0;
    tIp6Addr            fwdAddr;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OsExtLsaLink     extLsaLink;
    INT1                i1RetCode = OSIX_FAILURE;
    tV3OsArea          *pArea = NULL;
    UINT2               u2LsaLen = 0;
    tV3OsInterface     *pInterface = NULL;
    tV3OsPrefixNode    *pPrefInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructAsExtLsa\n");

    OSPFV3_EXT_LSA_ALLOC (&(pAsExtLsa));
    if (NULL == pAsExtLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for External LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pV3OspfCxt->u4ContextId, " Ext LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pAsExtLsa;

    /* leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    if (u2InternalType == OSPFV3_COND_AS_EXT_LSA)
    {
        pAsExtRange = (tV3OsAsExtAddrRange *) (VOID *) pPtr;

        u1Option = V3SetEFTBitInAsOrNssaLsa (pAsExtRange->metric.u4MetricType,
                                             NULL, 0);

        OSPFV3_LADD1BYTE (pCurrent, u1Option);

        OSPFV3_ADD_ADDR_PREFIX (pCurrent, pAsExtRange->metric.u4Metric,
                                pAsExtRange->u1PrefixLength,
                                pAsExtRange->ip6Addr);

    }
    else if (u2InternalType == OSPFV3_AS_TRNSLTD_EXT_LSA)
    {

        pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;

        pPath = OSPFV3_GET_PATH (pRtEntry);

        pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pPath->areaId));

        if ((pArea != NULL) && (pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
            (pArea->u1NssaTrnsltrState == OSPFV3_TRNSLTR_STATE_DISABLED))
        {
            OSPFV3_EXT_LSA_FREE (pAsExtLsa);
            return NULL;
        }

        if ((pPath != NULL) &&
            ((i1RetCode = (INT1) (V3RtcGetExtLsaLink (pPath->pLsaInfo->pLsa,
                                                      pPath->pLsaInfo->u2LsaLen,
                                                      &extLsaLink))) ==
             OSIX_SUCCESS))
        {
            u1Option = V3SetEFTBitInAsOrNssaLsa (extLsaLink.metric.u4MetricType,
                                                 &extLsaLink.fwdAddr,
                                                 extLsaLink.u4ExtRouteTag);

            OSPFV3_LADD1BYTE (pCurrent, u1Option);

            OSPFV3_ADD_ADDR_PREFIX (pCurrent, extLsaLink.metric.u4Metric,
                                    extLsaLink.extRtPrefix.u1PrefixLength,
                                    extLsaLink.extRtPrefix.addrPrefix);

            if (OSPFV3_IS_F_BIT_SET (u1Option))
            {
                OSPFV3_LADDSTR (pCurrent, &extLsaLink.fwdAddr,
                                OSPFV3_IPV6_ADDR_LEN);
            }

            if (extLsaLink.u4ExtRouteTag != 0)
            {
                OSPFV3_LADD4BYTE (pCurrent, extLsaLink.u4ExtRouteTag);
            }
        }
    }
    else if (u2InternalType == OSPFV3_AS_TRNSLTD_RNG_LSA)
    {
        pAddrRange = (tV3OsAddrRange *) (VOID *) pPtr;

        u1Option = V3SetEFTBitInAsOrNssaLsa (pAddrRange->metric.u4MetricType,
                                             NULL,
                                             pAddrRange->u4AddrRangeRtTag);

        OSPFV3_LADD1BYTE (pCurrent, u1Option);

        u4RangeValue = pAddrRange->metric.u4Metric;

        if (pAddrRange->metric.u4MetricType == OSPFV3_TYPE_2_METRIC)
        {
            u4RangeValue++;
        }

        OSPFV3_ADD_ADDR_PREFIX (pCurrent, u4RangeValue,
                                pAddrRange->u1PrefixLength,
                                pAddrRange->ip6Addr);

        if (pAddrRange->u4AddrRangeRtTag != 0)
        {
            OSPFV3_LADD4BYTE (pCurrent, pAddrRange->u4AddrRangeRtTag);
        }
    }
    else
    {
        pExtRoute = (tV3OsExtRoute *) (VOID *) pPtr;

        OSPFV3_IP6_ADDR_COPY (fwdAddr, gV3OsNullIp6Addr);

        pInterface = V3GetFindIf (pExtRoute->u4FwdIfIndex);

        if (pInterface != NULL)
        {

            if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
                (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
            {
                /* Do Nothing with the forwarding address. */
            }
            else if ((!(IS_ADDR_LLOCAL (pExtRoute->fwdAddr))) &&
                     (V3UtilIp6AddrComp (&(pExtRoute->fwdAddr),
                                         &gV3OsNullIp6Addr) != OSPFV3_EQUAL))
            {
                TMO_SLL_Scan (&(pInterface->ip6AddrLst),
                              pPrefInfo, tV3OsPrefixNode *)
                {
                    if (V3UtilIp6PrefixComp (&pPrefInfo->prefixInfo.addrPrefix,
                                             &pExtRoute->fwdAddr,
                                             pPrefInfo->prefixInfo.
                                             u1PrefixLength) == OSPFV3_EQUAL)
                    {
                        OSPFV3_IP6_ADDR_COPY (fwdAddr, pExtRoute->fwdAddr);
                        V3AddFwdAddrToLstInCxt (pV3OspfCxt, fwdAddr);
                        break;
                    }
                }
            }
        }

        u1Option = V3SetEFTBitInAsOrNssaLsa (pExtRoute->metric.u4MetricType,
                                             &fwdAddr, pExtRoute->u4ExtTag);

        OSPFV3_LADD1BYTE (pCurrent, u1Option);

        OSPFV3_ADD_ADDR_PREFIX (pCurrent, pExtRoute->metric.u4Metric,
                                pExtRoute->u1PrefixLength,
                                pExtRoute->ip6Prefix);

        if (OSPFV3_IS_F_BIT_SET (u1Option))
        {
            OSPFV3_LADDSTR (pCurrent, &fwdAddr, OSPFV3_IPV6_ADDR_LEN);
        }
        if (pExtRoute->u4ExtTag != 0)
        {
            OSPFV3_LADD4BYTE (pCurrent, pExtRoute->u4ExtTag);
        }

    }

    u2LsaLen = (UINT2) (pCurrent - pAsExtLsa);

    V3ConstructLsHeaderInCxt (pV3OspfCxt, pAsExtLsa, u2LsaLen,
                              OSPFV3_AS_EXT_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                "AS External LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructAsExtLsa\n");

    return pAsExtLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3FillOptionInLsa                                       */
/*                                                                           */
/* Description     : This function sets the options.                         */
/*                                                                           */
/* Input           : pArea   : Pointer to Area.                              */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : u1Option  option support by Router                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3FillOptionInLsa (tV3OsArea * pArea)
{
    UINT1               u1Option = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3FillOptionInLsa\n");

    if (OSPFV3_IS_NORMAL_AREA (pArea))
    {
        u1Option |= OSPFV3_E_BIT_MASK;
    }

    if (OSPFV3_IS_NSSA_AREA (pArea))
    {
        u1Option |= OSPFV3_N_BIT_MASK;
    }

    if (OSPFV3_IS_SUPPORTING_DC_EXTENSIONS (pArea->pV3OspfCxt))
    {
        u1Option |= OSPFV3_DC_BIT_MASK;
    }

    u1Option |= OSPFV3_R_BIT_MASK;
    u1Option |= OSPFV3_V6_BIT_MASK;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3FillOptionInLsa\n");
    return u1Option;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3SetEFTBitInAsOrNssaLsa                                */
/*                                                                           */
/* Description     : This function sets the E, F, and T bit in LSA.          */
/*                                                                           */
/* Input           : u4MetricType   : Metric type TYPE_1 or TYPE_2           */
/*                   pfwdAddr       : Pointer to forwarding address.         */
/*                   u4ExtRtTag     : External route tag.                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : u1Option                                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3SetEFTBitInAsOrNssaLsa (UINT4 u4MetricType, tIp6Addr * pfwdAddr,
                          UINT4 u4ExtRtTag)
{
    UINT1               u1Option = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3SetEFTBitInAsOrNssaLsa\n");

    if (u4MetricType == OSPFV3_TYPE_2_METRIC)
    {
        u1Option |= OSPFV3_EXT_E_BIT_MASK;
    }
    if ((pfwdAddr != NULL) && (!OSPFV3_IS_FORWARD_ADDR_NULL (pfwdAddr)))
    {
        u1Option |= OSPFV3_EXT_F_BIT_MASK;
    }
    if (u4ExtRtTag != 0)
    {
        u1Option |= OSPFV3_EXT_T_BIT_MASK;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3SetEFTBitInAsOrNssaLsa\n");
    return u1Option;
}

/*****************************************************************************/
/*                                                                           */
/* Function         : V3ConstructNssaLsa                                     */
/*                                                                           */
/* Description      : This function constructs NSSA LSA                      */
/*                                                                           */
/* Input            : pPtr - Pointer to struct associated with LSA           */
/*                  : u2InternalType - LSA Type                              */
/*                  : pArea    - Pointer to NSSA area                        */
/*                  : i4SeqNum - LSA sequence number                         */
/*                  : pLsId - Link State Id.                                 */
/*                                                                           */
/* Output           : None                                                   */
/*                                                                           */
/* Returns          : Pointer to Type 7 LSA if constructed successfully      */
/*                    else NULL                                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3ConstructNssaLsa (UINT1 *pPtr, UINT2 u2InternalType, tV3OsArea * pArea,
                    tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId)
{
    UINT1               u1Option = 0;
    UINT1              *pNssaLsa = NULL;
    UINT1              *pCurrent = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tIp6Addr           *pfwdAddr = NULL;
    tIp6Addr            fwdAddr;
    UINT1               u1PrefixOption = 0;
    UINT2               u2LsaLen = 0;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    UINT1               u1PrefixOptions = 0;
    UINT1               u1PrefixLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ConstructNssaLsa\n");

    OSPFV3_EXT_LSA_ALLOC (&(pNssaLsa));
    if (NULL == pNssaLsa)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for NSSA LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    " NSSA LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pNssaLsa;

    /* leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    if (u2InternalType == OSPFV3_COND_NSSA_LSA)
    {
        pAsExtRange = (tV3OsAsExtAddrRange *) (VOID *) pPtr;

        pfwdAddr = V3SetNssaFwdAddr (pArea);

        u1Option = V3SetEFTBitInAsOrNssaLsa (pAsExtRange->metric.u4MetricType,
                                             pfwdAddr, 0);

        OSPFV3_LADD1BYTE (pCurrent, u1Option);

        pLsaInfo = V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                        pLsId, &(pArea->pV3OspfCxt->rtrId),
                                        NULL, pArea);

        /* forwarding addr */
        /* Set-Clear "P" bit in the Options field */
        if ((pAsExtRange->u1AggTranslation == OSIX_TRUE) && (pLsaInfo == NULL)
            && (pfwdAddr != NULL))
        {
            u1PrefixOption |= OSPFV3_P_BIT_MASK;
        }

        OSPFV3_ADD_NSSA_ADDR_PREFIX (pCurrent, pAsExtRange->metric.u4Metric,
                                     pAsExtRange->u1PrefixLength,
                                     u1PrefixOption, pAsExtRange->ip6Addr);

        if (pfwdAddr != NULL)
        {
            OSPFV3_LADDSTR (pCurrent, pfwdAddr, OSPFV3_IPV6_ADDR_LEN);
        }
    }
    else if (u2InternalType == OSPFV3_NSSA_LSA)
    {
        pExtRoute = (tV3OsExtRoute *) (VOID *) pPtr;

        /* forwarding addr */
        if (OSPFV3_IS_FORWARD_ADDR_NULL (&pExtRoute->fwdAddr))
        {
            pfwdAddr = V3SetNssaFwdAddr (pArea);
        }
        else
        {
            /* fwdAddrUpdate */
            TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
            {
                pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                                  nextIfInArea, pLstNode);

                TMO_SLL_Scan (&pInterface->ip6AddrLst,
                              pPrefixNode, tV3OsPrefixNode *)
                {
                    if ((V3UtilIp6PrefixComp (&pExtRoute->fwdAddr,
                                              &pPrefixNode->
                                              prefixInfo.addrPrefix,
                                              pPrefixNode->prefixInfo.
                                              u1PrefixLength)) == OSPFV3_EQUAL)
                    {
                        pfwdAddr = &pExtRoute->fwdAddr;
                        break;
                    }
                }
                if (pfwdAddr != NULL)
                {
                    break;
                }
            }

            if (pfwdAddr == NULL)
            {
                pfwdAddr = V3SetNssaFwdAddr (pArea);
            }
        }
        if (pfwdAddr == NULL)
        {
            OSPFV3_IP6_ADDR_COPY (fwdAddr, gV3OsNullIp6Addr);
        }
        else
        {
            OSPFV3_IP6_ADDR_COPY (fwdAddr, *pfwdAddr);
            V3AddFwdAddrToLstInCxt (pArea->pV3OspfCxt, fwdAddr);
        }
        u1Option = V3SetEFTBitInAsOrNssaLsa (pExtRoute->metric.u4MetricType,
                                             &fwdAddr, pExtRoute->u4ExtTag);

        OSPFV3_LADD1BYTE (pCurrent, u1Option);

        /* Set P bit in the Options field */
        if (V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA, pLsId,
                                 &(pArea->pV3OspfCxt->rtrId),
                                 NULL, pArea) == NULL)
        {
            if (pfwdAddr != NULL)
            {
                u1PrefixOptions |= OSPFV3_P_BIT_MASK;
            }
        }

        OSPFV3_ADD_NSSA_ADDR_PREFIX (pCurrent, pExtRoute->metric.u4Metric,
                                     pExtRoute->u1PrefixLength,
                                     u1PrefixOptions, pExtRoute->ip6Prefix);

        if (pfwdAddr != NULL)
        {
            OSPFV3_LADDSTR (pCurrent, &fwdAddr, OSPFV3_IPV6_ADDR_LEN);
        }
        if (pExtRoute->u4ExtTag != 0)
        {
            OSPFV3_LADD4BYTE (pCurrent, pExtRoute->u4ExtTag);
        }
    }
    else
    {
        if (OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt))
        {
            u1Option =
                V3SetEFTBitInAsOrNssaLsa (pArea->stubDefaultCost.u4MetricType,
                                          pfwdAddr, 0);

            OSPFV3_LADD1BYTE (pCurrent, u1Option);

            OSPFV3_ADD_NSSA_ADDR_PREFIX (pCurrent,
                                         pArea->stubDefaultCost.u4Metric,
                                         u1PrefixLen, 0, gV3OsNullIp6Addr);

        }
        else
        {
            pExtRoute = V3ExtrtFindRouteInCxt (pArea->pV3OspfCxt,
                                               &gV3OsNullIp6Addr, 0);

            if (pExtRoute == NULL)
            {
                OSPFV3_EXT_LSA_FREE (pNssaLsa);
                return NULL;
            }

            /* forwarding addr */
            if (OSPFV3_IS_FORWARD_ADDR_NULL (&pExtRoute->fwdAddr))
            {
                pfwdAddr = V3SetNssaFwdAddr (pArea);
            }
            else
            {
                /* fwdAddrUpdate */
                TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
                {
                    pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                                      nextIfInArea, pLstNode);

                    TMO_SLL_Scan (&pInterface->ip6AddrLst,
                                  pPrefixNode, tV3OsPrefixNode *)
                    {
                        if ((V3UtilIp6PrefixComp
                             (&pExtRoute->fwdAddr,
                              &pPrefixNode->prefixInfo.addrPrefix,
                              pPrefixNode->prefixInfo.u1PrefixLength)) ==
                            OSPFV3_EQUAL)
                        {
                            pfwdAddr = &pExtRoute->fwdAddr;
                            break;
                        }
                    }
                    if (pfwdAddr != NULL)
                    {
                        break;
                    }
                }
                if (pfwdAddr == NULL)
                {
                    pfwdAddr = V3SetNssaFwdAddr (pArea);
                }
            }

            if (pfwdAddr == NULL)
            {
                OSPFV3_IP6_ADDR_COPY (fwdAddr, gV3OsNullIp6Addr);
            }
            else
            {
                OSPFV3_IP6_ADDR_COPY (fwdAddr, *pfwdAddr);
                V3AddFwdAddrToLstInCxt (pArea->pV3OspfCxt, fwdAddr);
            }

            u1Option = V3SetEFTBitInAsOrNssaLsa (pExtRoute->metric.u4MetricType,
                                                 &fwdAddr, pExtRoute->u4ExtTag);

            OSPFV3_LADD1BYTE (pCurrent, u1Option);

            /* Set P bit in the Options field */
            if ((pArea->pV3OspfCxt->bNssaAsbrDefRtTrans == OSIX_TRUE) &&
                (pfwdAddr != NULL))
            {
                u1PrefixOptions |= OSPFV3_P_BIT_MASK;
            }

            OSPFV3_ADD_NSSA_ADDR_PREFIX (pCurrent, pExtRoute->metric.u4Metric,
                                         pExtRoute->u1PrefixLength,
                                         u1PrefixOptions, pExtRoute->ip6Prefix);

            if (pfwdAddr != NULL)
            {
                OSPFV3_LADDSTR (pCurrent, &fwdAddr, OSPFV3_IPV6_ADDR_LEN);
            }
            if (pExtRoute->u4ExtTag != 0)
            {
                OSPFV3_LADD4BYTE (pCurrent, pExtRoute->u4ExtTag);
            }
        }
    }

    u2LsaLen = (UINT2) (pCurrent - pNssaLsa);
    V3ConstructLsHeaderInCxt (pArea->pV3OspfCxt, pNssaLsa, u2LsaLen,
                              OSPFV3_NSSA_LSA, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                "NSSA LSA Constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3ConstructNssaLsa\n");
    return pNssaLsa;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3ConstructGraceLsa                                        */
/*                                                                           */
/* Description  : This routine contructs the grace LSA packets               */
/*                The TLVs related to the grace LSA are filled               */
/*                RFC 3623                                                   */
/*                                                                           */
/* Input        : pInterface - Interface through which the pkt               */
/*                             is to be transmitted                          */
/*                i4SeqNum   - Sequence number.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to router LSA, if successfully constructed.        */
/*                NULL, Otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1       *
V3ConstructGraceLsa (tV3OsInterface * pInterface, tV3OsLsaSeqNum i4SeqNum)
{
    UINT1              *pGrLsa = NULL;
    UINT1              *pCurrent = NULL;
    tV3OsLinkStateId    linkStateId;
    UINT2               u2LsaLen = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER:V3ConstructGraceLsa\r\n");

    OSPFV3_EXT_LSA_ALLOC (&(pGrLsa));
    if (NULL == pGrLsa)
    {

        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for Grace LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Grace LSA Alloc Failure\n");
        return NULL;
    }

    pCurrent = pGrLsa;

    /* Leave space for header */
    pCurrent += OSPFV3_LS_HEADER_SIZE;

    /* Add the Grace period TLV */
    OSPFV3_LADD2BYTE (pCurrent, OSPFV3_GR_PERIOD_TLV_TYPE);
    OSPFV3_LADD2BYTE (pCurrent, OSPFV3_GR_PERIOD_TLV_LENGTH);
    OSPFV3_LADD4BYTE (pCurrent, pInterface->pArea->pV3OspfCxt->u4GracePeriod);

    /* Add the Restart reason TLV */
    OSPFV3_LADD2BYTE (pCurrent, OSPFV3_GR_REASON_TLV_TYPE);
    OSPFV3_LADD2BYTE (pCurrent, OSPFV3_GR_REASON_TLV_LENGTH);
    OSPFV3_LADD1BYTE (pCurrent, pInterface->pArea->pV3OspfCxt->u1RestartReason);
    OSPFV3_LADD3BYTE (pCurrent, 0);    /* Padding */

    u2LsaLen = (UINT2) (pCurrent - pGrLsa);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);

    V3ConstructLsHeaderInCxt (pInterface->pArea->pV3OspfCxt, pGrLsa,
                              u2LsaLen, OSPFV3_GRACE_LSA, &linkStateId,
                              i4SeqNum);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructGraceLsa\r\n");
    return pGrLsa;
}

/****************************************************************************/
/*                                                                          */
/* Function       : V3SetNssaFwdAddr                                        */
/*                                                                          */
/* Description    : This routine sets Fwd address for NSSA LSA.             */
/*                                                                          */
/* Input          : pArea - Pointer to NSSA area                            */
/*                                                                          */
/* Output         : None                                                    */
/*                                                                          */
/* Returns        : forwarding address.                                     */
/*                                                                          */
/****************************************************************************/
PRIVATE tIp6Addr   *
V3SetNssaFwdAddr (tV3OsArea * pArea)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tIp6Addr           *pIp6Addr = NULL;

    /* This function sets the fwd address to one of the following */
    /* 1) Fwd Address is active NSSA interface in NSSA            */
    /* 2) Active OSPF Stub network address                        */
    /* 3) Fwd address set to 0.0.0.0                              */

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3SetNssaFwdAddr\n");

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);

        /* FwdAddrUpdate */
        if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
        {

            pPrefixNode =
                (tV3OsPrefixNode *) TMO_SLL_First (&pInterface->ip6AddrLst);
            if (pPrefixNode == NULL)
            {
                continue;
            }
            if (V3IsTransitLink (pInterface) != OSIX_TRUE)
            {
                return &pPrefixNode->prefixInfo.addrPrefix;
            }
            else
            {
                pIp6Addr = &pPrefixNode->prefixInfo.addrPrefix;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3SetNssaFwdAddr\n");
    return pIp6Addr;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3BuildSummaryParam                                    */
/*                                                                           */
/* Description     :  builds the summary param structure required for the    */
/*                    construction of the uncondensed summary lsas           */
/*                                                                           */
/* Input           :  pArea    :  area to which the summarization to be      */
/*                                done                                       */
/*                    pRtEntry :  pointer to route entry structure which is  */
/*                                to be summarized                           */
/*                                                                           */
/* Output          :  pSummaryParam : pointer to summary param structure     */
/*                                    which is getting filled up the routine */
/*                                                                           */
/* Returns          :   OSIX_SUCCESS/OSIX_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3BuildSummaryParam (tV3OsArea * pArea, tV3OsRtEntry * pRtEntry,
                     tV3OsSummaryParam * pSummaryParam)
{

    tV3OsPath          *pPath = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3BuildSummaryParam\n");

    if (OSPFV3_IS_DEST_NETWORK (pRtEntry))
    {
        OSPFV3_IP6_ADDR_COPY (pSummaryParam->prefixInfo.addrPrefix,
                              pRtEntry->destRtPrefix);
        pSummaryParam->prefixInfo.u1PrefixLength = pRtEntry->u1PrefixLen;
    }

    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        if (V3UtilAreaIdComp (pPath->areaId, pArea->areaId) == OSPFV3_EQUAL)
        {
            /* path is associated with the same area */
            continue;
        }
        else
        {
            if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea))
            {
                if (pPath->u1HopCount > 0)
                {
                    if (pRtEntry->u1RtType == OSPFV3_DIRECT)
                    {
                        pSummaryParam->u4Metric = pPath->u4Cost;
                        break;
                    }
                    else if (V3UtilAreaIdComp
                             (pPath->aNextHops[0].pInterface->pArea->areaId,
                              pArea->areaId) == OSPFV3_EQUAL)
                    {
                        continue;
                    }
                }
            }
            pSummaryParam->u4Metric = pPath->u4Cost;
            break;
        }
    }
    if (pPath == NULL)
    {
        return OSIX_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3BuildSummaryParam\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function       :  V3RefreshLsaInCxt                                       */
/*                                                                           */
/* Description    :  Refresh the LSA.                                        */
/*                                                                           */
/* Input          :  pV3OspfCxt: Context pointer.                            */
/*                   pLsaDesc  : Pointer to LSA Descriptor.                  */
/*                   i4SeqNum  : Sequence number.                            */
/*                   u2LsaType : LSA type.                                   */
/*                                                                           */
/* Output         :  None.                                                   */
/*                                                                           */
/* Returns        :  Pointer to LSA, if successfully constructed             */
/*                   NULL, Otherwise                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1       *
V3RefreshLsaInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsLsaDesc * pLsaDesc,
                   INT4 i4SeqNum, UINT2 u2LsaType, tV3OsLinkStateId * pLsId)
{
    UINT1              *pLsa = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RefreshLsa\n");
    if (pLsaDesc->pLsaInfo == NULL)
    {
        return NULL;
    }

    OSPFV3_LSA_TYPE_ALLOC (u2LsaType, &(pLsa));
    if (pLsa == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for refreshing LSA\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pV3OspfCxt->u4ContextId, "Refresh LSA Alloc Failure\n");
        return NULL;
    }

    MEMSET (pLsa, 0, sizeof (tV3OsLsHeader));
    MEMCPY (pLsa, pLsaDesc->pLsaInfo->pLsa, pLsaDesc->pLsaInfo->u2LsaLen);
    V3ConstructLsHeaderInCxt (pV3OspfCxt, pLsa, pLsaDesc->pLsaInfo->u2LsaLen,
                              u2LsaType, pLsId, i4SeqNum);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RefreshLsa\n");

    return pLsa;
}

/****************************************************************************/
/*                                                                          */
/* Function        :   V3ConstructLsHeaderInCxt                             */
/*                                                                          */
/* Description     :   constructs the LS header for the given LSA.          */
/*                                                                          */
/* Input           :   pV3OspfCxt     :   pointer to Context                */
/*                     pLsa           :   pointer to LSA                    */
/*                     u2Len          :   length of LSA                     */
/*                     u2LsaType      :   type of LSA                       */
/*                     pLinkStateId   :   LS Id                             */
/*                     i4LsaSeqNum    :   sequence number                   */
/*                                                                          */
/* Output          :   None                                                 */
/*                                                                          */
/* Returns         :   None                                                 */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3ConstructLsHeaderInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pLsa, UINT2 u2Len,
                          UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId,
                          tV3OsLsaSeqNum i4LsaSeqNum)
{

    UINT1              *pCurr = pLsa;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3ConstructLsHeaderInCxt\n");

    if (i4LsaSeqNum == OSPFV3_MAX_SEQ_NUM)
    {
        OSPFV3_LADD2BYTE (pCurr, OSPFV3_MAX_AGE);
    }

    else
    {
        /* age set to 0     */
        OSPFV3_LADD2BYTE (pCurr, 0);
    }

    OSPFV3_LADD2BYTE (pCurr, u2LsaType);

    OSPFV3_LADDSTR (pCurr, pLinkStateId, OSPFV3_LINKSTATE_ID_LEN);

    /* advRtrId       */
    OSPFV3_LADDSTR (pCurr, OSPFV3_RTR_ID (pV3OspfCxt), OSPFV3_RTR_ID_LEN);
    OSPFV3_LADD4BYTE (pCurr, i4LsaSeqNum);

    /* chksum set to 0  */
    OSPFV3_LADD2BYTE (pCurr, 0);
    OSPFV3_LADD2BYTE (pCurr, u2Len);

    V3UtilComputeLsaFletChksum (pLsa, u2Len);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstructLsHeaderInCxt\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :   V3IsTransitLink                                      */
/*                                                                          */
/* Description     :   checks the transit Status of the interface.          */
/*                                                                          */
/* Input           :   pInterface : Interface whose Status is to be checked */
/*                                                                          */
/* Output          :   None                                                 */
/*                                                                          */
/* Returns         :   OSIX_TRUE,  if the link is a transit link            */
/*                     OSIX_FALSE, otherwise                                */
/*                                                                          */
/****************************************************************************/
PUBLIC UINT1
V3IsTransitLink (tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsTransitLink\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (OSPFV3_IS_NBR_FULL (pNbr))
        {
            if (OSPFV3_IS_DR (pInterface))
            {
                return OSIX_TRUE;
            }
            if (V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                                 pNbr->nbrRtrId) == OSPFV3_EQUAL)
            {
                return OSIX_TRUE;
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsTransitLink\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3BuildDefaultSummaryParam                             */
/*                                                                           */
/* Description     :  builds the summary param structure required for the    */
/*                    construction of the Default summary lsas               */
/*                                                                           */
/* Input           :  pArea    :  area to which the summarization to be      */
/*                                done                                       */
/*                                                                           */
/* Output          :  pSummaryParam : pointer to summary param structure     */
/*                                    which is getting filled up the routine */
/*                                                                           */
/* Returns         :   None                                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3BuildDefaultSummaryParam (tV3OsArea * pArea,
                            tV3OsSummaryParam * pSummaryParam)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3BuildDefaultSummaryParam\n");

    MEMSET (pSummaryParam, 0, (sizeof (tV3OsSummaryParam)));

    pSummaryParam->u4Metric = pArea->stubDefaultCost.u4Metric;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3BuildDefaultSummaryParam\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function        :   V3IsEligibleToGenAseLsaInCxt                          */
/*                                                                           */
/* Description     :   the ASE lsa list is scanned to check whether a        */
/*                     functionally equivalent lsa which is generated by a   */
/*                     router with a greater router id exists. If that lsa   */
/*                     exists, then it is ineligible and eligible otherwise. */
/*                                                                           */
/* Input           :   pV3OspfCxt  : Context pointer                         */
/*                     pExtRoute   : Pointer to the external route structure */
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   OSIX_SUCCESS/OSIX_FAILURE                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3IsEligibleToGenAseLsaInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsExtRoute * pExtRoute)
{

    tV3OsExtLsaLink     extLsaLink;
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3IsEligibleToGenAseLsa\n");

    u1PrefixLen = pExtRoute->u1PrefixLength;
    pAddrPrefix = (UINT1 *) &(pExtRoute->ip6Prefix);

    pOsDbNode = V3IsDbNodePresent (pAddrPrefix, u1PrefixLen,
                                   pV3OspfCxt->pExtLsaHashTable,
                                   OSPFV3_AS_EXT_LSA);

    /* If Database node is not present, then return  */
    if (pOsDbNode == NULL)
    {
        return OSIX_SUCCESS;
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
    {
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (OSPFV3_IS_MAX_AGE (pDbLsaInfo->u2LsaAge))
        {
            continue;
        }
        if ((V3UtilRtrIdComp (OSPFV3_RTR_ID (pV3OspfCxt),
                              pDbLsaInfo->lsaId.advRtrId) == OSPFV3_LESS) &&
            (V3RtcFindAsbrPathInCxt (pV3OspfCxt, &(pDbLsaInfo->lsaId.advRtrId),
                                     NULL) != NULL))
        {
            /* check for the equivalent functionality */

            if (V3RtcGetExtLsaLink (pDbLsaInfo->pLsa,
                                    pDbLsaInfo->u2LsaLen,
                                    &extLsaLink) == OSIX_FAILURE)
            {
                continue;
            }

            if (V3IsFunctionalityEqvLsa (&pExtRoute->metric, &extLsaLink.metric,
                                         &pExtRoute->fwdAddr,
                                         &extLsaLink.fwdAddr) == OSIX_SUCCESS)
            {
                pDbLsaInfo->u1FnEqvlFlag = OSIX_TRUE;
                gu4V3IsEligibleToGenAseLsaInCxtFail++;
                return OSIX_FAILURE;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3IsEligibleToGenAseLsa\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3IsEligibleToGenNssaLsa                               */
/*                                                                           */
/* Description     :  Handles functionally equv NSSA LSA while originating   */
/*                    NSSA LSA                                               */
/*                                                                           */
/* Input           :  pLsaInfo  : Pointer to the LSA Info structure.         */
/*                    pArea     : Pointer to the Area structure.             */
/*                                                                           */
/* Output          :  None                                                   */
/*                                                                           */
/* Returns         :  OSIX_TRUE/OSIX_FALSE                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3IsEligibleToGenNssaLsa (tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea)
{

    tV3OsExtLsaLink     extLsaLink;
    tV3OsExtLsaLink     newextLsaLink;
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsEligibleToGenNssaLsa\n");

    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pAddrPrefix = (pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    pOsDbNode = V3IsDbNodePresent (pAddrPrefix, u1PrefixLen,
                                   pArea->pNssaLsaHashTable, OSPFV3_NSSA_LSA);

    /* If Database node is not present, then return  */
    if (pOsDbNode == NULL)
    {
        return OSIX_SUCCESS;
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
    {
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (OSPFV3_IS_MAX_AGE (pDbLsaInfo->u2LsaAge))
        {
            continue;
        }
        if (V3UtilRtrIdComp (OSPFV3_RTR_ID (pArea->pV3OspfCxt),
                             pDbLsaInfo->lsaId.advRtrId) == OSPFV3_EQUAL)
        {
            continue;
        }
        if (V3RtcFindAsbrPathInCxt (pArea->pV3OspfCxt,
                                    &(pDbLsaInfo->lsaId.advRtrId),
                                    NULL) != NULL)
        {
            /* check for the equivalent functionality */
            if (V3RtcGetExtLsaLink (pDbLsaInfo->pLsa,
                                    pDbLsaInfo->u2LsaLen,
                                    &extLsaLink) == OSIX_FAILURE)
            {
                continue;
            }
            if (V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                                    &newextLsaLink) == OSIX_FAILURE)
            {
                continue;
            }

            if (V3IsFunctionalityEqvLsa
                (&newextLsaLink.metric, &extLsaLink.metric,
                 &newextLsaLink.fwdAddr, &extLsaLink.fwdAddr) == OSIX_SUCCESS)
            {
                if (OSPFV3_IS_P_BIT_SET (pLsaInfo->pLsa))
                {
                    if (OSPFV3_IS_P_BIT_SET (pDbLsaInfo->pLsa))
                    {
                        if (V3UtilRtrIdComp
                            (pDbLsaInfo->lsaId.advRtrId,
                             pLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
                        {
                            return OSIX_FAILURE;
                        }
                    }
                }
                else
                {
                    if (OSPFV3_IS_P_BIT_SET (pDbLsaInfo->pLsa))
                    {
                        return OSIX_FAILURE;
                    }
                    else
                    {
                        if (V3UtilRtrIdComp
                            (pDbLsaInfo->lsaId.advRtrId,
                             pLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
                        {
                            return OSIX_FAILURE;
                        }
                    }
                }

            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsEligibleToGenNssaLsa\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3HandleFuncEqvNssaLsa                                  */
/*                                                                           */
/* Description     : This function handles functionally equivalent NSSA LSA. */
/*                                                                           */
/* Input           : pLsa - Pointer to received NSSA LSA                     */
/*                 : pNbr - Nbr who sent this LSA                            */
/*                                                                           */
/* Output           : None                                                   */
/*                                                                           */
/* Returns          : VOID                                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3HandleFuncEqvNssaLsa (UINT1 *pLsa, tV3OsNeighbor * pNbr)
{
    tV3OsLsHeader       lsHeader;
    tV3OsExtLsaLink     extLsaLink;
    tV3OsExtLsaLink     newextLsaLink;
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    UINT1               u1FnEqvlFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3HandleFuncEqvNssaLsa\n");

    V3UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    u1PrefixLen = *((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pAddrPrefix = (pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    pOsDbNode = V3IsDbNodePresent (pAddrPrefix, u1PrefixLen,
                                   pNbr->pInterface->pArea->pNssaLsaHashTable,
                                   OSPFV3_NSSA_LSA);

    /* If Database node is not present, then return  */
    if (pOsDbNode == NULL)
    {
        return u1FnEqvlFlag;
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
    {
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (OSPFV3_IS_MAX_AGE (pDbLsaInfo->u2LsaAge))
        {
            continue;
        }
        if (V3UtilRtrIdComp (OSPFV3_RTR_ID
                             (pNbr->pInterface->pArea->pV3OspfCxt),
                             pDbLsaInfo->lsaId.advRtrId) != OSPFV3_EQUAL)
        {
            continue;
        }

        if (V3RtcGetExtLsaLink (pDbLsaInfo->pLsa,
                                pDbLsaInfo->u2LsaLen,
                                &extLsaLink) == OSIX_FAILURE)
        {
            continue;
        }
        if (V3RtcGetExtLsaLink (pLsa, lsHeader.u2LsaLen,
                                &newextLsaLink) == OSIX_FAILURE)
        {
            continue;
        }

        if (V3IsFunctionalityEqvLsa (&newextLsaLink.metric, &extLsaLink.metric,
                                     &newextLsaLink.fwdAddr,
                                     &extLsaLink.fwdAddr) == OSIX_SUCCESS)
        {
            if (OSPFV3_IS_P_BIT_SET (pDbLsaInfo->pLsa))
            {
                if (OSPFV3_IS_P_BIT_SET (pLsa))
                {
                    if (V3UtilRtrIdComp
                        (lsHeader.advRtrId,
                         pDbLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
                    {
                        u1FnEqvlFlag = OSIX_TRUE;
                        V3AgdFlushOut (pDbLsaInfo);
                    }
                }
            }
            else
            {
                if (OSPFV3_IS_P_BIT_SET (pLsa))
                {
                    u1FnEqvlFlag = OSIX_TRUE;
                    V3AgdFlushOut (pDbLsaInfo);
                }
                else
                {
                    if (V3UtilRtrIdComp
                        (lsHeader.advRtrId,
                         pDbLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
                    {
                        u1FnEqvlFlag = OSIX_TRUE;
                        V3AgdFlushOut (pDbLsaInfo);
                    }
                }
            }

        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HandleFuncEqvNssaLsa\n");
    return u1FnEqvlFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3HandleFuncEqvAsExtLsaInCxt                            */
/*                                                                           */
/* Description     : This function handles functionally equivalent AS EXT LSA*/
/*                                                                           */
/* Input           : pV3OspfCxt   -   Context pointer                        */
/*                   pLsa         -   Pointer to received AS External LSA    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3HandleFuncEqvAsExtLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pLsa)
{
    tV3OsLsHeader       lsHeader;
    tV3OsExtLsaLink     extLsaLink;
    tV3OsExtLsaLink     newextLsaLink;
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    UINT1               u1FnEqvlFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3HandleFuncEqvAsExtLsa\n");

    V3UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    u1PrefixLen = *((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pAddrPrefix = (pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    pOsDbNode = V3IsDbNodePresent (pAddrPrefix, u1PrefixLen,
                                   pV3OspfCxt->pExtLsaHashTable,
                                   OSPFV3_AS_EXT_LSA);

    /* If Database node is not present, then return  */
    if (pOsDbNode == NULL)
    {
        return u1FnEqvlFlag;
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
    {
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (OSPFV3_IS_MAX_AGE (pDbLsaInfo->u2LsaAge))
        {
            continue;
        }
        if (V3UtilRtrIdComp (OSPFV3_RTR_ID (pV3OspfCxt),
                             pDbLsaInfo->lsaId.advRtrId) != OSPFV3_EQUAL)
        {
            continue;
        }
        if (V3RtcGetExtLsaLink (pDbLsaInfo->pLsa,
                                pDbLsaInfo->u2LsaLen,
                                &extLsaLink) == OSIX_FAILURE)
        {
            continue;
        }
        if (V3RtcGetExtLsaLink (pLsa, lsHeader.u2LsaLen,
                                &newextLsaLink) == OSIX_FAILURE)
        {
            continue;
        }

        if (V3IsFunctionalityEqvLsa (&newextLsaLink.metric, &extLsaLink.metric,
                                     &newextLsaLink.fwdAddr,
                                     &extLsaLink.fwdAddr) == OSIX_SUCCESS)
        {
            if (V3UtilRtrIdComp
                (lsHeader.advRtrId,
                 pDbLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
            {
                u1FnEqvlFlag = OSIX_TRUE;
                V3AgdFlushOut (pDbLsaInfo);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3HandleFuncEqvAsExtLsa\n");
    return u1FnEqvlFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3IsDbNodePresent                                       */
/*                                                                           */
/* Description     : This function checks whether the prefix is present in   */
/*                   the pLsaHashTable                                       */
/*                                                                           */
/* Input           : pAddrPrefix  : Pointer to the Address Prefix            */
/*                   u1PrefixLen  : Prefix Length                            */
/*                   pLsaHashTable: Pointer to the LSA Hash Table            */
/*                   u2LsaType    : Lsa Type                                 */
/*                                                                           */
/* Output           : None                                                   */
/*                                                                           */
/* Returns          : Pointer to the Lsdb Node                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsDbNode *
V3IsDbNodePresent (UINT1 *pAddrPrefix, UINT1 u1PrefixLen,
                   tTMO_HASH_TABLE * pLsaHashTable, UINT2 u2LsaType)
{

    UINT4               u4HashIndex = 0;
    tV3OsDbNode        *pOsDbNode = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    UINT1               u1DbPrefixLen = 0;
    UINT1              *pDbAddrPrefix = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IsDbNodePresent\n");
    u4HashIndex = V3LsaAddrPrefixHashFunc (pAddrPrefix, u1PrefixLen, u2LsaType);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        u1DbPrefixLen =
            *((UINT1 *) (pDbLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
        pDbAddrPrefix = (pDbLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

        if ((u1PrefixLen == u1DbPrefixLen) &&
            (V3UtilIp6PrefixComp
             ((tIp6Addr *) (VOID *) pAddrPrefix, (tIp6Addr *) (VOID *)
              pDbAddrPrefix, u1PrefixLen) == OSPFV3_EQUAL))
        {
            return pOsDbNode;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IsDbNodePresent\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3IsFunctionalityEqvLsa                                 */
/*                                                                           */
/* Description     : This function checks whether the received Type-5/Type-7 */
/*                   Lsa is functionally equivalent with the database copy   */
/*                   or not                                                  */
/*                                                                           */
/* Input           : pMetric    : Pointer to the Lsa metric                  */
/*                   pDbMetric  : Pointer to the database Lsa metric         */
/*                   pfwdAddr   : Pointer to the forwarding addr             */
/*                   pDbfwdAddr : Pointer to the database fwd addr           */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_TRUE/OSIX_FALSE                                    */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
V3IsFunctionalityEqvLsa (tV3OsMetric * pMetric, tV3OsMetric * pDbMetric,
                         tIp6Addr * pfwdAddr, tIp6Addr * pDbfwdAddr)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IsFunctionalityEqvLsa\n");

    if ((pMetric->u4Metric != pDbMetric->u4Metric) ||
        (pMetric->u4MetricType != pDbMetric->u4MetricType))
    {
        gu4V3IsFunctionalityEqvLsaFail++;
        return OSIX_FAILURE;
    }

    if ((OSPFV3_IS_FORWARD_ADDR_NULL (pfwdAddr)) ||
        (OSPFV3_IS_FORWARD_ADDR_NULL (pDbfwdAddr)))
    {
        gu4V3IsFunctionalityEqvLsaFail++;
        return OSIX_FAILURE;
    }

    if (V3UtilIp6AddrComp (pfwdAddr, pDbfwdAddr) != OSPFV3_EQUAL)
    {
        gu4V3IsFunctionalityEqvLsaFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IsFunctionalityEqvLsa\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3ModifyNssaAsbrDefRtTransInCxt                          */
/*                                                                           */
/* Description    : Processes change in P bit setting of default  Type 7     */
/*                  LSA generated  by NSSA ASBR (not ABR)                    */
/*                                                                           */
/* Input          : pV3OspfCxt    -    Context pointer                       */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : VOID                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3ModifyNssaAsbrDefRtTransInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ModifyNssaAsbrDefRtTrans\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA's Cannot be (e)orginated in graceful restart mode */
        return OSIX_SUCCESS;
    }

    if ((OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt)) ||
        (!(OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt))))
    {
        return OSIX_SUCCESS;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            if ((pLsaInfo =
                 V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                      &OSPFV3_NULL_LSID,
                                      &(pV3OspfCxt->rtrId), NULL, pArea))
                != NULL)
            {
                V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                       (UINT1 *) pLsaInfo->pLsaDesc);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ModifyNssaAsbrDefRtTrans\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3FillOptionInNetworkLsa                                */
/*                                                                           */
/* Description     : This function generates the options in Network-LSA      */
/*                                                                           */
/* Input           : pInterface : Pointer to interface                       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Options for Network-LSA                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3FillOptionInNetworkLsa (tV3OsInterface * pInterface)
{

    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1               u1Option = 0;
    UINT1               u1NetOption = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3FillOptionInNetworkLsa\n");
    TMO_SLL_Scan (&(pInterface->linkLsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

        OSPFV3_BUFFER_GET_1_BYTE (pLsaInfo->pLsa,
                                  (OSPFV3_LS_HEADER_SIZE + 3), u1Option);
        u1NetOption = u1NetOption | u1Option;

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3FillOptionInNetworkLsa\n");
    return u1NetOption;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3GenNssaLsaWithNonZeroFwdAddr                          */
/*                                                                           */
/* Description     : This routine recalculates Fwd address for those         */
/*                   External LSAs for whom  interface acting as  fwd        */
/*                   address. It is called when there is a prefix addition   */
/*                                                                           */
/* Input           : pArea       - Pointer to Area structure                 */
/*                   pIp6Addr    - IPv6 Prefix which is deleted              */
/*                   u1PrefixLen - Length of the Prefix to be deleted        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenNssaLsaWithNonZeroFwdAddr (tV3OsArea * pArea,
                                tIp6Addr * pIp6Addr, UINT1 u1PrefixLen)
{
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;
    UINT1               u1FwdAddrFound = OSIX_FALSE;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    tV3OsExtLsaLink     nssaLsaLink;
    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenNssaLsaWithNonZeroFwdAddr\n");

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->fwdAddrLst), pScanFwdAddrNode,
                  tV3OsFwdAddr *)
    {
        if (V3UtilIp6PrefixComp (pIp6Addr, &(pScanFwdAddrNode->fwdAddr),
                                 u1PrefixLen) == OSPFV3_EQUAL)

        {
            u1FwdAddrFound = OSIX_TRUE;
            break;
        }
    }

    if (u1FwdAddrFound == OSIX_TRUE)
    {
        inParams.pRoot = pArea->pV3OspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPFV3_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        inParams.Key.pKey = NULL;

        if (TrieGetFirstNode (&inParams,
                              &pTempPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return;
        }
        do
        {
            pExtRoute = (tV3OsExtRoute *) pTempPtr;
            pType7LsaInfo = V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                                 &(pExtRoute->linkStateId),
                                                 &(pArea->pV3OspfCxt->rtrId),
                                                 NULL, pArea);
            if (pType7LsaInfo != NULL)
            {

                V3RtcGetExtLsaLink (pType7LsaInfo->pLsa,
                                    pType7LsaInfo->u2LsaLen, &nssaLsaLink);
                if ((V3UtilIp6AddrComp (&(nssaLsaLink.fwdAddr),
                                        &gV3OsNullIp6Addr)
                     == OSPFV3_EQUAL) ||
                    (V3UtilIp6PrefixComp (pIp6Addr,
                                          &(pExtRoute->fwdAddr), u1PrefixLen)
                     == OSPFV3_EQUAL))
                {
                    V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pType7LsaInfo->pLsaDesc);
                }
            }
            pLeafNode = inParams.pLeafNode;
            OSPFV3_IP6ADDR_CPY (au1Key, &(pExtRoute->ip6Prefix));
            au1Key[sizeof (tIp6Addr)] = pExtRoute->u1PrefixLength;
            inParams.Key.pKey = au1Key;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr,
                                (VOID **) &(inParams.pLeafNode))
               != TRIE_FAILURE);

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenNssaLsaWithNonZeroFwdAddr\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3GenExtLsaWithFwdAddr                                  */
/*                                                                           */
/* Description     : This routine recalculates Fwd address for those         */
/*                   External LSAs for whom  interface acting as  fwd        */
/*                   address. It is called when there is a prefix deletion   */
/*                   on this interface.                                      */
/*                                                                           */
/* Input           : pInterface - Pointer to interface structure             */
/*                   pIp6Addr   - IPv6 Prefix which is deleted               */
/*                   u1PrefixLen - Length of the Prefix to be deleted        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenExtLsaWithFwdAddr (tV3OsInterface * pInterface,
                        tIp6Addr * pIp6Addr, UINT1 u1PrefixLen)
{
    UINT1               u1FwdAddrFound = OSIX_FALSE;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenExtLsaWithFwdAddr\n");

    if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        return;
    }

    TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->fwdAddrLst),
                  pScanFwdAddrNode, tV3OsFwdAddr *)
    {
        if (V3UtilIp6PrefixComp (pIp6Addr, &(pScanFwdAddrNode->fwdAddr),
                                 u1PrefixLen) == OSPFV3_EQUAL)

        {
            u1FwdAddrFound = OSIX_TRUE;
            break;
        }
    }

    if (u1FwdAddrFound == OSIX_TRUE)
    {
        inParams.pRoot = pInterface->pArea->pV3OspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPFV3_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        inParams.Key.pKey = NULL;

        if (TrieGetFirstNode (&inParams,
                              &pTempPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return;
        }
        do
        {
            pExtRoute = (tV3OsExtRoute *) pTempPtr;

            if (V3UtilIp6PrefixComp (pIp6Addr,
                                     &(pExtRoute->fwdAddr), u1PrefixLen)
                == OSPFV3_EQUAL)
            {
                if (pExtRoute->pLsaInfo != NULL)
                {
                    V3SignalLsaRegenInCxt
                        (pInterface->pArea->pV3OspfCxt,
                         OSPFV3_SIG_NEXT_INSTANCE,
                         (UINT1 *) pExtRoute->pLsaInfo->pLsaDesc);
                }

            }

            pLeafNode = inParams.pLeafNode;
            OSPFV3_IP6ADDR_CPY (au1Key, &(pExtRoute->ip6Prefix));
            au1Key[sizeof (tIp6Addr)] = pExtRoute->u1PrefixLength;
            inParams.Key.pKey = au1Key;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr,
                                (VOID **) &(inParams.pLeafNode))
               != TRIE_FAILURE);

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenExtLsaWithFwdAddr\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3GenNssaLsaWithZeroFwdAddr                             */
/*                                                                           */
/* Description     : This routine recalculates Fwd address for those         */
/*                   NSSA  LSAs for whom  interface acting as  fwd           */
/*                   address. It is called when IPv6 prefix is deleted on    */
/*                   on this interface.                                      */
/*                                                                           */
/* Input           : pInterface - Pointer to interface structure             */
/*                   pIp6Addr   - IPv6 Prefix to be deleted                  */
/*                   u1PrefixLen - Prefix length of the IPv6 address to be   */
/*                                 deleted                                   */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenNssaLsaWithZeroFwdAddr (tV3OsArea * pArea,
                             tIp6Addr * pIp6Addr, UINT1 u1PrefixLen)
{
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;
    UINT1              *pFwdAddr = NULL;
    UINT1               u1Option = 0;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;
    UINT1               u1FwdAddrFound = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenNssaLsaWithZeroFwdAddr \n");

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->fwdAddrLst),
                  pScanFwdAddrNode, tV3OsFwdAddr *)
    {
        if (V3UtilIp6PrefixComp (pIp6Addr, &(pScanFwdAddrNode->fwdAddr),
                                 u1PrefixLen) == OSPFV3_EQUAL)

        {
            u1FwdAddrFound = OSIX_TRUE;
            break;
        }
    }
    if (u1FwdAddrFound == OSIX_TRUE)
    {
        TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashKey)
        {
            OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable, u4HashKey,
                                          pOsDbNode, pTmpDbNode, tV3OsDbNode *)
            {
                OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                      tTMO_SLL_NODE *)
                {
                    pLsaInfo =
                        OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                             pLstNode);

                    if (!OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
                    {
                        continue;
                    }

                    u1Option =
                        *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE));

                    if (!OSPFV3_IS_F_BIT_SET (u1Option))
                    {
                        continue;
                    }

                    pFwdAddr = V3GetFwdAddress (pLsaInfo->pLsa);

                    if (V3UtilIp6PrefixComp
                        (pIp6Addr, (tIp6Addr *) (VOID *) pFwdAddr, u1PrefixLen)
                        == OSPFV3_EQUAL)
                    {
                        V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                               OSPFV3_SIG_NEXT_INSTANCE,
                                               (UINT1 *) pLsaInfo->pLsaDesc);
                    }
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenNssaLsaWithZeroFwdAddr \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3ResetNssaFwdAddr                                      */
/*                                                                           */
/* Description     : This routine recalculates Fwd address for those         */
/*                   NSSA  LSAs for whom  interface acting as  fwd           */
/*                   address transitions to down state                       */
/*                                                                           */
/* Input           : pInterface - Pointer to interface structure             */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3ResetNssaFwdAddr (tV3OsInterface * pInterface)
{
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    UINT1              *pFwdAddr = NULL;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    UINT1               u1Option = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ResetNssaFwdAddr\n");
    if (pInterface->pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        return;
    }
    else
    {
        TMO_HASH_Scan_Table (pInterface->pArea->pNssaLsaHashTable, u4HashKey)
        {
            TMO_HASH_Scan_Bucket (pInterface->pArea->pNssaLsaHashTable,
                                  u4HashKey, pOsDbNode, tV3OsDbNode *)
            {
                TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
                {
                    pLsaInfo =
                        OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                             pLstNode);

                    if (!OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
                    {
                        continue;
                    }

                    u1Option =
                        *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE));

                    if (!OSPFV3_IS_F_BIT_SET (u1Option))
                    {
                        continue;
                    }

                    pFwdAddr = V3GetFwdAddress (pLsaInfo->pLsa);

                    TMO_SLL_Scan (&pInterface->ip6AddrLst, pPrefixNode,
                                  tV3OsPrefixNode *)
                    {
                        if (V3UtilIp6PrefixComp
                            (&pPrefixNode->prefixInfo.addrPrefix,
                             (tIp6Addr *) (VOID *) pFwdAddr,
                             pPrefixNode->prefixInfo.u1PrefixLength)
                            == OSPFV3_EQUAL)
                        {
                            V3SignalLsaRegenInCxt
                                (pInterface->pArea->pV3OspfCxt,
                                 OSPFV3_SIG_NEXT_INSTANCE,
                                 (UINT1 *) pLsaInfo->pLsaDesc);
                        }
                    }
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ResetNssaFwdAddr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3GetFwdAddress                                             */
/*                                                                           */
/* Description : This function extract the pointer from the lsa buffer       */
/*                                                                           */
/* Input       : pLsa - Pointer to lsa buffer                                */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : Pointer to the fwd address                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1      *
V3GetFwdAddress (UINT1 *pLsa)
{
    UINT1               u1PrefixLen = 0;
    UINT1              *pFwdAddr = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3GetFwdAddress\n");
    u1PrefixLen = *((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pFwdAddr =
        (pLsa + OSPFV3_ADDR_PREFIX_OFFSET +
         OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen));
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3GetFwdAddress\n");
    return pFwdAddr;
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3SetLsIdInterAreaPrefixLsaInCxt                            */
/*                                                                           */
/* Description : This function generates the link-state id for               */
/*               Inter-Area-Prefix LSA and assigns to the corresponding      */
/*               Rt-Entry or Address range depending on u2Type               */
/*                                                                           */
/* Input       : pV3OspfCxt   -  Context pointer                             */
/*               pPtr         -  Pointer to tV3OsRtEntry if u2Type =         */
/*                               OSPFV3_INTER_AREA_PREFIX_LSA                */
/*                               Otherwise,                                  */
/*                               Pointer to tV3OsAddrRange                   */
/*               u2Type       -  Type of LSA                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3SetLsIdInterAreaPrefixLsaInCxt (tV3OspfCxt * pV3OspfCxt,
                                  UINT1 *pPtr, UINT2 u2Type)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3SetLsIdInterAreaPrefixLsaInCxt\n");
    if (u2Type == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;

        if (pRtEntry->u1PrefixLen == 0)
        {
            MEMSET (pRtEntry->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4InterAreaLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pRtEntry->linkStateId,
                                   pV3OspfCxt->u4InterAreaLsaCounter);
        }
    }
    else
    {
        pAddrRange = (tV3OsAddrRange *) (VOID *) pPtr;

        if (pAddrRange->u1PrefixLength == 0)
        {
            MEMSET (pAddrRange->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4InterAreaLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pAddrRange->linkStateId,
                                   pV3OspfCxt->u4InterAreaLsaCounter);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3SetLsIdInterAreaPrefixLsaInCxt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3SetLsIdAsExtLsaInCxt                                      */
/*                                                                           */
/* Description : This function generates the link-state id for               */
/*               AS-External LSA and assigns to the corresponding AddrRange  */
/*               or ExtRoute or AsExtAddrRange depending on u2Type           */
/*                                                                           */
/* Input       : pV3OspfCxt   - Context pointer                              */
/*               pPtr         - Pointer to tV3OsAddrRange if u2Type =        */
/*                              OSPFV3_NSSA_EXTERNAL_LINK                    */
/*                              Pointer to tV3OsExtRoute if u2Type =         */
/*                              OSPFV3_AS_EXT_LSA                            */
/*                              Otherwise,                                   */
/*                              Pointer to tV3OsAsExtAddrRange               */
/*               u2Type       -  Type of LSA                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3SetLsIdAsExtLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr, UINT2 u2Type)
{
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3SetLsIdAsExtLsaInCxt\n");
    if (u2Type == OSPFV3_AS_EXT_LSA)
    {
        pExtRoute = (tV3OsExtRoute *) (VOID *) pPtr;
        if (pExtRoute->u1PrefixLength == 0)
        {
            MEMSET (pExtRoute->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4AsExtLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pExtRoute->linkStateId,
                                   pV3OspfCxt->u4AsExtLsaCounter);
        }
    }
    else if (u2Type == OSPFV3_AS_TRNSLTD_EXT_LSA)
    {
        pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;

        if (pRtEntry->u1PrefixLen == 0)
        {
            MEMSET (pRtEntry->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4AsExtLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pRtEntry->linkStateId,
                                   pV3OspfCxt->u4AsExtLsaCounter);
        }
    }
    else if (u2Type == OSPFV3_COND_AS_EXT_LSA)
    {
        pAsExtRange = (tV3OsAsExtAddrRange *) (VOID *) pPtr;

        if (pAsExtRange->u1PrefixLength == 0)
        {
            MEMSET (pAsExtRange->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4AsExtLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pAsExtRange->linkStateId,
                                   pV3OspfCxt->u4AsExtLsaCounter);
        }
    }
    else
    {
        pAddrRange = (tV3OsAddrRange *) (VOID *) pPtr;

        if (pAddrRange->u1PrefixLength == 0)
        {
            MEMSET (pAddrRange->linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
        }
        else
        {
            pV3OspfCxt->u4AsExtLsaCounter++;
            OSPFV3_BUFFER_DWTOPDU (pAddrRange->linkStateId,
                                   pV3OspfCxt->u4AsExtLsaCounter);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3SetLsIdAsExtLsaInCxt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3SetRtrLsaLsId                                             */
/*                                                                           */
/* Description : This function set the link-state id value in                */
/*               Interface or neighbour structure from RtrLsaCounter         */
/*               this is used while generating the router lsa.               */
/*                                                                           */
/* Input       : pPtr - Pointer to tV3OsNeighbor if u1Type =                 */
/*                      OSPFV3_CONFIGURED_NBR                                */
/*                      Otherwise,                                           */
/*                      Pointer to tV3OsInterface                            */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3SetRtrLsaLsId (UINT1 *pPtr, UINT1 u1Type, UINT1 u1ChngType)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3SetRtrLsaLsId\n");
    if (u1Type == OSPFV3_CONFIGURED_NBR)
    {
        pNbr = (tV3OsNeighbor *) (VOID *) pPtr;
        if (pNbr->pInterface->pArea->u4rtrLsaLinkSize < OSPFV3_MAX_RTR_LINKS)
        {
            OSPFV3_BUFFER_DWTOPDU (pNbr->linkStateId,
                                   pNbr->pInterface->pArea->u4rtrLsaCounter);
            pNbr->pInterface->pArea->u4rtrLsaLinkSize++;
        }
        else
        {
            pNbr->pInterface->pArea->u4rtrLsaCounter++;
            pNbr->pInterface->pArea->u4rtrLsaLinkSize = 0;
            OSPFV3_BUFFER_DWTOPDU (pNbr->linkStateId,
                                   pNbr->pInterface->pArea->u4rtrLsaCounter);
        }

    }
    else
    {
        pInterface = (tV3OsInterface *) (VOID *) pPtr;
        if (pInterface->pArea->u4rtrLsaLinkSize < OSPFV3_MAX_RTR_LINKS)
        {
            OSPFV3_BUFFER_DWTOPDU (pInterface->linkStateId,
                                   pInterface->pArea->u4rtrLsaCounter);
            pInterface->pArea->u4rtrLsaLinkSize++;
        }
        else
        {
            pInterface->pArea->u4rtrLsaCounter++;
            pInterface->pArea->u4rtrLsaLinkSize = 0;
            OSPFV3_BUFFER_DWTOPDU (pInterface->linkStateId,
                                   pInterface->pArea->u4rtrLsaCounter);
        }

        if ((u1ChngType == OSIX_TRUE)
            && (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
        {
            pInterface->pArea->u4rtrLsaLinkSize =
                pInterface->pArea->u4rtrLsaLinkSize +
                TMO_SLL_Count (&pInterface->nbrsInIf);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3SetRtrLsaLsId\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3ResetRtrLsaLsId                                           */
/*                                                                           */
/* Description : This function Reset the link-state id value in              */
/*               Interface or neighbour structure and update RtrLsaCounter   */
/*               and rtrLsaLinkSize.                                         */
/*                                                                           */
/* Input       : pPtr - Pointer to tV3OsNeighbor if u1Type =                 */
/*                      OSPFV3_CONFIGURED_NBR                                */
/*                      Otherwise,                                           */
/*                      Pointer to tV3OsInterface                            */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3ResetRtrLsaLsId (UINT1 *pPtr, UINT1 u1Type, UINT1 u1ChngType)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT4               u4LinkStateId = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3ResetRtrLsaLsId\n");
    if (u1Type == OSPFV3_CONFIGURED_NBR)
    {
        pNbr = (tV3OsNeighbor *) (VOID *) pPtr;
        u4LinkStateId = OSPFV3_BUFFER_DWFROMPDU (pNbr->linkStateId);
        if (u4LinkStateId != pNbr->pInterface->pArea->u4rtrLsaCounter)
        {
            return;
        }

        if ((pNbr->pInterface->pArea->u4rtrLsaLinkSize == 0) &&
            (pNbr->pInterface->pArea->u4rtrLsaCounter > 0))
        {
            pNbr->pInterface->pArea->u4rtrLsaLinkSize = OSPFV3_MAX_RTR_LINKS;
            pNbr->pInterface->pArea->u4rtrLsaCounter--;
        }
        else
        {
            pNbr->pInterface->pArea->u4rtrLsaLinkSize--;
        }

    }
    else
    {
        pInterface = (tV3OsInterface *) (VOID *) pPtr;
        u4LinkStateId = OSPFV3_BUFFER_DWFROMPDU (pInterface->linkStateId);
        if (u4LinkStateId != pInterface->pArea->u4rtrLsaCounter)
        {
            return;
        }
        if ((pInterface->pArea->u4rtrLsaLinkSize == 0) &&
            (pInterface->pArea->u4rtrLsaCounter > 0))
        {
            pInterface->pArea->u4rtrLsaLinkSize = OSPFV3_MAX_RTR_LINKS;
            pInterface->pArea->u4rtrLsaCounter--;
        }
        else
        {
            pInterface->pArea->u4rtrLsaLinkSize--;
        }

        if ((u1ChngType == OSIX_TRUE)
            && (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
        {
            pInterface->pArea->u4rtrLsaLinkSize =
                pInterface->pArea->u4rtrLsaLinkSize -
                TMO_SLL_Count (&pInterface->nbrsInIf);
        }

    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3ResetRtrLsaLsId\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3ChkFnEqLsaChngOrNot                         */
/*                                                                           */
/* Description : This function checks if received LSA is functionality       */
/*               equivalent flag or not                                      */
/*                                                                           */
/* Input       : Pointer to LSA                                              */
/*          Pointer to LSA info                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : True or False                                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1
V3ChkFnEqLsaChngOrNot (UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo)
{
    tV3OsLsHeader       lsHeader;
    tV3OsExtLsaLink     extLsaLink;
    tV3OsExtLsaLink     newextLsaLink;
    UINT1               u1FnEqvlFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3ChkFnEqLsaChngOrNot\n");

    V3UtilExtractLsHeaderFromLbuf (pLsa, &lsHeader);

    if (V3RtcGetExtLsaLink (pLsaInfo->pLsa,
                            pLsaInfo->u2LsaLen, &extLsaLink) == OSIX_FAILURE)
    {
        return u1FnEqvlFlag;
    }
    if (V3RtcGetExtLsaLink (pLsa, lsHeader.u2LsaLen,
                            &newextLsaLink) == OSIX_FAILURE)
    {
        return u1FnEqvlFlag;
    }

    if (V3IsFunctionalityEqvLsa (&newextLsaLink.metric, &extLsaLink.metric,
                                 &newextLsaLink.fwdAddr,
                                 &extLsaLink.fwdAddr) == OSIX_SUCCESS)
    {
        u1FnEqvlFlag = OSIX_TRUE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3ChkFnEqLsaChngOrNot\n");
    return u1FnEqvlFlag;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lscons.c                      */
/*-----------------------------------------------------------------------*/
