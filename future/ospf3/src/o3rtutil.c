/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtutil.c,v 1.15 2018/02/07 09:32:29 siva Exp $
 *
 * Description:This file contains several utility functions
 *             for Creation Addition and Deletion of Routing Table
 *
 *
 *******************************************************************/
#include "o3inc.h"

PRIVATE VOID V3RtcAddToSortBdrRtLstInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                                 tV3OsRtEntry * pRtEntry));
PRIVATE VOID V3RtcMarkAllInterAreaLsasInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID V3RtcMarkAllNssaLsasInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID V3RtcMarkAllExtLsasInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/*****************************************************************************/
/* Function     : V3RtcCreateRtEntry                                         */
/*                                                                           */
/* Description  : This function creates a new routing entry                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to router entry, if successfully created           */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsRtEntry *
V3RtcCreateRtEntry (VOID)
{
    tV3OsRtEntry       *pRtEntry = NULL;

    pRtEntry = (VOID *) MemAllocMemBlk ((tMemPoolId) OSPFV3_RT_ENTRY_QID);

    if (NULL == pRtEntry)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for route entry\n"));

        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_RTMODULE_TRC,
                        "Memory allocation for route entry failed\n");
        return NULL;
    }
    MEMSET (pRtEntry, 0, sizeof (tV3OsRtEntry));
    TMO_SLL_Init (&(pRtEntry->pathLst));
    TMO_SLL_Init (&(pRtEntry->lsaLst));
    pRtEntry->u1IsRmapAffected = OSIX_FALSE;

    OSPFV3_GBL_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    "New route entry created\n");
    return pRtEntry;
}

/*****************************************************************************/
/* Function     : V3RtcAddRtEntryInCxt                                       */
/*                                                                           */
/* Description  : This function add the route entry to OSPF routing table.   */
/*                                                                           */
/* Input        : pV3OspfCxt: Pointer to Context                             */
/*                pRtEntry  : Pointer to entry to be added                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcAddRtEntryInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    if (OSPFV3_IS_DEST_NETWORK (pRtEntry))
    {
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.u1PrefixLen = pRtEntry->u1PrefixLen;
        inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
        inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        if (TrieAdd (&inParams, (VOID *) pRtEntry, &outParams) != TRIE_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to add the route entry to TRIE\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                        "Failed to add the route entry to TRIE\n");
            return;
        }
    }
    else
    {
        if (pRtEntry != NULL)
        {
            V3RtcAddToSortBdrRtLstInCxt (pV3OspfCxt, pRtEntry);
        }
    }
}

/*****************************************************************************/
/* Function     : V3RtcFindRtEntryInCxt                                      */
/*                                                                           */
/* Description  : This procedure finds the routing entry corresponding to    */
/*                the destination and specified dest type.Used during        */
/*                routing table calculation.                                 */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                pDest      : Destination information                       */
/*                u1PrefixLen: Prefix Length                                 */
/*                u1DestType : OSPFV3_DEST_NETWORK/OSPFV3_DEST_AREA_BORDER/  */
/*                             OSPFV3_DEST_AS_BOUNDARY                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to routing entry, if found                         */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsRtEntry *
V3RtcFindRtEntryInCxt (tV3OspfCxt * pV3OspfCxt, VOID *pDest,
                       UINT1 u1PrefixLen, UINT1 u1DestType)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    if (u1DestType == OSPFV3_DEST_NETWORK)

    {
        MEMCPY (&(au1Key), pDest, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.u1PrefixLen = u1PrefixLen;
        inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
        inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
        inParams.pLeafNode = NULL;
        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        if (TrieSearch (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
        {
            return NULL;
        }
        pRtEntry = (tV3OsRtEntry *) outParams.pAppSpecInfo;
        return pRtEntry;
    }
    else
    {
        TMO_SLL_Scan (&(pV3OspfCxt->ospfV3RtTable.routesList), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pRtEntry =
                OSPFV3_GET_BASE_PTR (tV3OsRtEntry, nextRtEntryNode, pLstNode);
            if ((u1DestType == pRtEntry->u1DestType) &&
                (V3UtilRtrIdComp (*(tV3OsRouterId *) pDest,
                                  pRtEntry->destRtRtrId) == OSPFV3_EQUAL))
            {
                return pRtEntry;
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcDeleteRtEntryInCxt                                    */
/*                                                                           */
/* Description  : This routine deletes the specified routing entry from the  */
/*                routing table.                                             */
/*                                                                           */
/* Input        : pV3OspfCxt  :  Context pointer                             */
/*                pRtEntry    : Pointer to route entry to be deleted         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcDeleteRtEntryInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    if (OSPFV3_IS_DEST_NETWORK (pRtEntry))
    {
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
        inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
        inParams.pLeafNode = NULL;
        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to delete the route entry from trie\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                        "Failed to delete the route entry from trie\n");
        }
    }
    else
    {
        TMO_SLL_Delete (&(pV3OspfCxt->ospfV3RtTable.routesList),
                        &(pRtEntry->nextRtEntryNode));
    }
}

/*****************************************************************************/
/* Function     : V3RtcFreeRtEntry                                           */
/*                                                                           */
/* Description  : This routine frees the specified routing entry.            */
/*                                                                           */
/* Input        : pRtEntry  : Pointer to the route entry to be freied        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcFreeRtEntry (tV3OsRtEntry * pRtEntry)
{
    tV3OsPath          *pPath = NULL;
    tV3OsLsaNode       *pLsaNode = NULL;

    /* Deleting and freeing all the paths of the list */
    while ((pPath = (tV3OsPath *) TMO_SLL_Get (&(pRtEntry->pathLst))) != NULL)
    {
        OSPFV3_PATH_FREE (pPath);
    }

    while ((pLsaNode =
            (tV3OsLsaNode *) TMO_SLL_Get (&(pRtEntry->lsaLst))) != NULL)
    {
        OSPFV3_LSA_NODE_FREE (pLsaNode);
    }

    /* Freeing the route entry */
    OSPFV3_RT_ENTRY_FREE (pRtEntry);
}

/*****************************************************************************/
/* Function     : V3RtcAddToSortBdrRtLstInCxt                                */
/*                                                                           */
/* Description  : This function adds the ABR or ASBR route entries to        */
/*                ABR/ASBR routes list in sorted order.                      */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                pRtEntry : Pointer to route entry to be added to the list  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcAddToSortBdrRtLstInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{
    tV3OsRtEntry       *pListRtEntry = NULL;
    tV3OsRtEntry       *pPrevLstRtEntry = NULL;
    INT1                i1RetVal;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC : V3RtcAddToSortRtLst\n");

    TMO_SLL_Scan (&(pV3OspfCxt->ospfV3RtTable.routesList), pListRtEntry,
                  tV3OsRtEntry *)
    {
        i1RetVal = V3UtilRtrIdComp (pRtEntry->destRtRtrId,
                                    pListRtEntry->destRtRtrId);

        if (i1RetVal == OSPFV3_EQUAL)
        {
            if (pRtEntry->u1DestType < pListRtEntry->u1DestType)
            {
                break;
            }
        }
        else if (i1RetVal == OSPFV3_LESS)
        {
            break;
        }
        pPrevLstRtEntry = pListRtEntry;
    }
    TMO_SLL_Insert (&(pV3OspfCxt->ospfV3RtTable.routesList),
                    &(pPrevLstRtEntry->nextRtEntryNode),
                    &(pRtEntry->nextRtEntryNode));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcAddToSortRtLst\n");
}

/*****************************************************************************/
/* Function     : V3RtcCreatePath                                            */
/*                                                                           */
/* Description  : This function creates a new path entry and sets the        */
/*                specified values for the appropriate fields.               */
/*                                                                           */
/* Input        : pAreaId   : Pointer to areaId                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to the path created, if successfully created       */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsPath   *
V3RtcCreatePath (tV3OsAreaId * pAreaId,
                 UINT1 u1PathType, UINT4 u4Cost, UINT4 u4Type2Cost)
{
    tV3OsPath          *pPath = NULL;

    OSPFV3_PATH_ALLOC (&(pPath));
    if (NULL == pPath)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Path creation failed. Unable to allocate memory\n"));

        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "Path Creation Failure\n");
        return NULL;
    }

    TMO_SLL_Init_Node (&(pPath->nextPath));
    OSPFV3_RTR_ID_COPY (&pPath->areaId, pAreaId);
    pPath->u1PathType = u1PathType;
    pPath->u4Cost = u4Cost;
    pPath->u4Type2Cost = u4Type2Cost;
    pPath->u1HopCount = 0;
    pPath->pLsaInfo = NULL;
    OSPFV3_GBL_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    "New RT Path Created\n");
    return pPath;
}

/*****************************************************************************/
/* Function     : V3RtcAddPath                                               */
/*                                                                           */
/* Description  : Adds the specified path to the specified routing table     */
/*                entry.                                                     */
/*                                                                           */
/* Input        : pRtEntry   : Routing table entry that is to be             */
/*                             updated                                       */
/*                pPath      : Path to be added                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcAddPath (tV3OsRtEntry * pRtEntry, tV3OsPath * pPath)
{
    TMO_SLL_Add (&(pRtEntry->pathLst), &(pPath->nextPath));
}

/*****************************************************************************/
/* Function     : V3RtcDeletePath                                            */
/*                                                                           */
/* Description  : This function finds and delete the path from the route     */
/*                entry for the specified area.                              */
/*                                                                           */
/* Input        : pRtEntry   : Routing table entry from which the path is to */
/*                             be deleted.                                   */
/*                pAreaId    : AreaId of the path which is to be deleted.    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcDeletePath (tV3OsRtEntry * pRtEntry, tV3OsAreaId * pAreaId)
{
    tV3OsPath          *pPath = NULL;

    pPath = V3RtcFindPath (pRtEntry, pAreaId);

    if (pPath != NULL)
    {
        TMO_SLL_Delete (&(pRtEntry->pathLst), &(pPath->nextPath));
        OSPFV3_PATH_FREE (pPath);
    }
}

/*****************************************************************************/
/* Function     : V3RtcRtLookupInCxt                                         */
/*                                                                           */
/* Description  : This function scans the entire routing table and finds the */
/*                most appropriate route for the given destination addr.     */
/*                Used during forwarding.                                    */
/*                                                                           */
/* Input        : pV3OspfCxt   : Pointer to Context                          */
/*                pDestIp6Addr : Pointer to IP6 address                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns the pointer to route entry if found                */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsRtEntry *
V3RtcRtLookupInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pDestIp6Addr)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    MEMCPY (&(au1Key), pDestIp6Addr, OSPFV3_IPV6_ADDR_LEN);
    au1Key[OSPFV3_IPV6_ADDR_LEN] = OSPFV3_IPV6_ADDR_LEN;
    inParams.Key.pKey = (UINT1 *) au1Key;
    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieLookup (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to find the route entry in TRIE\n"));

        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "Failed to find the route entry\n");
        return NULL;
    }
    pRtEntry = (tV3OsRtEntry *) outParams.pAppSpecInfo;
    return pRtEntry;
}

/*****************************************************************************/
/* Function     : V3RtcFindAbrPathInCxt                                      */
/*                                                                           */
/* Description  : This function finds the ABR path through the given area.   */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                pAbrRtrId: Pointer to ABR/ASBR router Id                   */
/*                pAreaId  : Pointer to areaId of the area through which ABR */
/*                           path is requested                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to path, if found                                  */
/*                NULL, Otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsPath   *
V3RtcFindAbrPathInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pAbrRtrId,
                       tV3OsAreaId * pAreaId)
{
    tV3OsPath          *pPath = NULL;
    tV3OsRtEntry       *pAbrRtEntry = NULL;

    if (((pAbrRtEntry =
          V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *) pAbrRtrId, 0,
                                 OSPFV3_DEST_AREA_BORDER)) != NULL) &&
        (pAbrRtEntry->u1Flag == OSPFV3_FOUND))
    {
        /* Find and returns ABR path through the 
         * required area */
        TMO_SLL_Scan (&(pAbrRtEntry->pathLst), pPath, tV3OsPath *)
        {
            if (V3UtilAreaIdComp (*pAreaId, pPath->areaId) == OSPFV3_EQUAL)
            {
                return pPath;
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcFindAsbrPathInCxt                                     */
/*                                                                           */
/* Description  : This funciton finds the routing table corresponding to the */
/*                destination and returns the path of the specified          */
/*                area.                                                      */
/*                                                                           */
/* Input        : pV3OspfCxt  : Context pointer                              */
/*                pAsbrRtrId  : Pointer to ASBR route Id                     */
/*                pArea       : Pointer to area                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to path, if found                                  */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsPath   *
V3RtcFindAsbrPathInCxt (tV3OspfCxt * pV3OspfCxt,
                        tV3OsRouterId * pAsbrRtrId, tV3OsArea * pArea)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OsPath          *pLstPath = NULL;
    tV3OsArea          *pLstArea = NULL;
    tV3OsAreaId         areaId;
    UINT4               u4BestCost = OSPFV3_LS_INFINITY_24BIT;

    OSPFV3_RTR_ID_COPY (&areaId, &OSPFV3_BACKBONE_AREAID);

    if ((pRtEntry =
         V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *) pAsbrRtrId, 0,
                                OSPFV3_DEST_AS_BOUNDARY)) == NULL)
    {
        return NULL;
    }

    if (pArea == NULL)
    {
        TMO_SLL_Scan (&(pRtEntry->pathLst), pLstPath, tV3OsPath *)
        {
            if (pLstPath->u1PathType == OSPFV3_INTER_AREA)
            {
                return pLstPath;
            }

            pLstArea = V3GetFindAreaInCxt (pV3OspfCxt, &pLstPath->areaId);

            if ((pLstArea != NULL) &&
                (pLstArea->u4AreaType == OSPFV3_NSSA_AREA))
            {
                continue;
            }

            if (pLstPath->u4Cost < u4BestCost)
            {
                pPath = pLstPath;
                u4BestCost = pLstPath->u4Cost;
                OSPFV3_RTR_ID_COPY (&areaId, &pLstPath->areaId);
            }
            else if (pLstPath->u4Cost == u4BestCost)
            {
                if (V3UtilAreaIdComp (pLstPath->areaId, areaId)
                    == OSPFV3_GREATER)
                {
                    pPath = pLstPath;
                    OSPFV3_RTR_ID_COPY (&areaId, &pLstPath->areaId);
                }
            }
        }
        return pPath;
    }
    else
    {
        /* If Area is non_null find path through given area */
        TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
        {
            if (V3UtilAreaIdComp (pArea->areaId, pPath->areaId) == OSPFV3_EQUAL)
            {
                return pPath;
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcFindPreferredAsbrPathInCxt                            */
/*                                                                           */
/* Description  : This funciton finds the routing table corresponding to the */
/*                destination and returns the path of the specified          */
/*                area.                                                      */
/*                                                                           */
/* Input        : pV3OspfCxt  : Context pointer                              */
/*                pAsbrRtrId  : Pointer to ASBR route Id                     */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to path, if found                                  */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsPath   *
V3RtcFindPreferredAsbrPathInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tV3OsRouterId * pAsbrRtrId)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pLstPath = NULL;
    tV3OsPath          *pCurrBestPath = NULL;
    tV3OsPath          *pBestPath = NULL;
    tV3OsPath           tmpBestPath;
    UINT1               u1FoundIntraArea = OSPFV3_FALSE;

    tmpBestPath.u4Cost = OSPFV3_LS_INFINITY_24BIT;
    tmpBestPath.u1PathType = OSPFV3_INTER_AREA;
    pCurrBestPath = &tmpBestPath;
    if ((pRtEntry =
         V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *) pAsbrRtrId, 0,
                                OSPFV3_DEST_AS_BOUNDARY)) == NULL)
    {
        return NULL;
    }
    TMO_SLL_Scan (&(pRtEntry->pathLst), pLstPath, tV3OsPath *)
    {
        /* intra-area non-backbone path */
        if ((pLstPath->u1PathType == OSPFV3_INTRA_AREA) &&
            (V3UtilAreaIdComp (pLstPath->areaId, OSPFV3_BACKBONE_AREAID) !=
             OSPFV3_EQUAL))
        {
            /*  prefer this entry (immaterial of cost) if the current best
               path is inter-area OR backbone area
             */
            if (!
                ((pCurrBestPath->u1PathType == OSPFV3_INTRA_AREA)
                 &&
                 (V3UtilAreaIdComp
                  (pCurrBestPath->areaId,
                   OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)))
            {
                pCurrBestPath = pLstPath;
                pBestPath = pLstPath;
                u1FoundIntraArea = OSPFV3_TRUE;
                continue;
            }
            if (pCurrBestPath->u4Cost == pLstPath->u4Cost)
            {
                if (V3UtilAreaIdComp (pLstPath->areaId,
                                      pCurrBestPath->areaId) == OSPFV3_GREATER)
                {
                    pCurrBestPath = pLstPath;
                    pBestPath = pLstPath;
                }
            }
            else if (pCurrBestPath->u4Cost > pLstPath->u4Cost)
            {
                pCurrBestPath = pLstPath;
                pBestPath = pLstPath;
            }
        }
        else
        {
            /* inter-area path OR backbone path */
            if (u1FoundIntraArea == OSPFV3_FALSE)
            {
                if (pCurrBestPath->u4Cost == pLstPath->u4Cost)
                {
                    if (V3UtilAreaIdComp (pLstPath->areaId,
                                          pCurrBestPath->areaId) ==
                        OSPFV3_GREATER)
                    {
                        pCurrBestPath = pLstPath;
                        pBestPath = pLstPath;
                    }
                }
                else if (pCurrBestPath->u4Cost > pLstPath->u4Cost)
                {
                    pCurrBestPath = pLstPath;
                    pBestPath = pLstPath;
                }
            }
        }
    }                            /* end of SLL Scan */
    return pBestPath;
}

/*****************************************************************************/
/* Function     : V3RtcFindPath                                              */
/*                                                                           */
/* Description  : This function path through a particular area from route    */
/*                entry                                                      */
/*                                                                           */
/* Input        : pRtEntry : Pointer to route entry                          */
/*                pAreaId  : Pointer to area id through which the path is    */
/*                           required                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to path, if found                                  */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsPath   *
V3RtcFindPath (tV3OsRtEntry * pRtEntry, tV3OsAreaId * pAreaId)
{
    tV3OsPath          *pPath = NULL;

    /* Scan the path list to find the path corresponding to 
     * given area */
    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        if (V3UtilAreaIdComp (*pAreaId, pPath->areaId) == OSPFV3_EQUAL)
        {
            return pPath;
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcSearchDatabase                                        */
/*                                                                           */
/* Description  : This procedure searches the area database based on lsa_type*/
/*                and linkStateId. This procedure is called during           */
/*                intra-area route calculation to find router lsa and network*/
/*                lsa corresponding to vertices in the SPF tree.             */
/*                                                                           */
/* Input        : u2LsaType     : Type of LSA                                */
/*                pVertexRtrId  : Pointer to vertex router Id.               */
/*                u4VertexIfId  : Vertex interface identifier                */
/*                pArea         : Pointer to area for which the intra-area   */
/*                                route calculation is being done            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA information, if found                       */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC UINT1       *
V3RtcSearchDatabase (UINT2 u2LsaType, tV3OsRouterId * pVertexRtrId,
                     UINT4 u4VertexIfId, tV3OsArea * pArea)
{
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLinkStateId    linkStateId;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4HashIndex = 0;

    /* If LSA Type is ROUTER LSA */
    if (u2LsaType == OSPFV3_ROUTER_LSA)
    {
        u4HashIndex = V3LsaRtrIdHashFunc (pVertexRtrId);
        TMO_HASH_Scan_Bucket (pArea->pRtrLsaHashTable, u4HashIndex,
                              pDbHashNode, tV3OsDbNode *)
        {
            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));
            pLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
            if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId, *pVertexRtrId) ==
                OSPFV3_EQUAL)

            {
                return (UINT1 *) pDbHashNode;
            }
        }
    }
    /* If LSA Type is NETWORK LSA */
    else if (u2LsaType == OSPFV3_NETWORK_LSA)
    {
        OSPFV3_BUFFER_DWTOPDU (linkStateId, u4VertexIfId);
        pLsaInfo =
            V3LsuSearchDatabase (OSPFV3_NETWORK_LSA, &linkStateId,
                                 pVertexRtrId, NULL, pArea);
        return (UINT1 *) pLsaInfo;
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcSearchIntraAreaPrefixNode                             */
/*                                                                           */
/* Description  : This function searches the intra area prefix LSA from area */
/*                database.                                                  */
/*                                                                           */
/* Input        : pVertexRtrId  : Pointer to vertex router Id.               */
/*                u4VertexIfId  : Vertex interface identifier                */
/*                u2RefLsType   : Reference LSA type                         */
/*                pArea         : Pointer to area                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to DB node information, if found                   */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsDbNode *
V3RtcSearchIntraAreaPrefixNode (tV3OsRouterId *
                                pVertexRtrId,
                                UINT4 u4VertexIfId,
                                UINT2 u2RefLsType, tV3OsArea * pArea)
{
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1              *pCurrPtr = NULL;
    UINT4               u4HashIndex = 0;
    UINT2               u2LsaRefLsType = OSPFV3_INVALID;
    UINT4               u4LsaRefLsId = 0;
    tV3OsLinkStateId    refLinkStateId;

    OSPFV3_BUFFER_DWTOPDU (refLinkStateId, u4VertexIfId);

    u4HashIndex =
        V3LsaIntraAreaHashFunc (u2RefLsType, &refLinkStateId, pVertexRtrId);
    TMO_HASH_Scan_Bucket (pArea->pIntraPrefixLsaTable, u4HashIndex,
                          pDbHashNode, tV3OsDbNode *)
    {
        pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
        pCurrPtr = pLsaInfo->pLsa + OSPFV3_REF_LS_TYPE_OFFSET;
        u2LsaRefLsType = OSPFV3_LGET2BYTE (pCurrPtr);
        u4LsaRefLsId = OSPFV3_LGET4BYTE (pCurrPtr);
        if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId, *pVertexRtrId) ==
             OSPFV3_EQUAL) && (u2LsaRefLsType == u2RefLsType)
            && (u4VertexIfId == u4LsaRefLsId))

        {
            return pDbHashNode;
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcSearchLsaHashNode                                     */
/*                                                                           */
/* Description  : This function is to search the database to find the        */
/*                prefix (Inter area, AS external, NSSA ) or inter area      */
/*                router LSAs                                                */
/*                                                                           */
/* Input        : pPtr        : Pointer to address prefix in case of         */
/*                              prefix LSAs (Inter-Area, AS external,NSSA)   */
/*                              Pointer to routerId in case of               */
/*                              inter area router LSA.                       */
/*                u1PrefixLen : Prefix length                                */
/*                u2LsaType   : Pointer to the destination route entry       */
/*                pArea       : Pointer to area in which the search          */
/*                              is needed                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to Database hash node if found else returns NULL   */
/*****************************************************************************/
PUBLIC tV3OsDbNode *
V3RtcSearchLsaHashNode (VOID *pPtr, UINT1 u1PrefixLen,
                        UINT2 u2LsaType, tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_HASH_TABLE    *pHashTable = NULL;
    UINT4               u4HashIndex = 0;

    if (u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        pHashTable = pArea->pInterPrefixLsaTable;
    }
    else if (u2LsaType == OSPFV3_AS_EXT_LSA)
    {
        pHashTable = pArea->pV3OspfCxt->pExtLsaHashTable;
    }
    else if (u2LsaType == OSPFV3_NSSA_LSA)
    {
        pHashTable = pArea->pNssaLsaHashTable;
    }
    else
    {
        pHashTable = pArea->pInterRouterLsaTable;
    }

    if (u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA)
    {
        u4HashIndex = V3LsaRtrIdHashFunc ((tV3OsRouterId *) pPtr);

        TMO_HASH_Scan_Bucket (pArea->pInterRouterLsaTable, u4HashIndex,
                              pDbHashNode, tV3OsDbNode *)
        {
            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));
            pLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
            if (MEMCMP
                (pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_OFFSET, pPtr,
                 OSPFV3_RTR_ID_LEN) == OSPFV3_EQUAL)
            {
                return pDbHashNode;
            }
        }
    }

    else
    {

        if (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen) > OSPFV3_IPV6_ADDR_LEN)
        {
            return NULL;
        }
        else
        {

            u4HashIndex =
                V3LsaAddrPrefixHashFunc ((UINT1 *) pPtr, u1PrefixLen,
                                         u2LsaType);
            TMO_HASH_Scan_Bucket (pHashTable, u4HashIndex, pDbHashNode,
                                  tV3OsDbNode *)
            {
                if (V3UtilIsMatchedPrefHashNode
                    (pDbHashNode, (tIp6Addr *) pPtr, u1PrefixLen) == OSIX_TRUE)
                {
                    return pDbHashNode;
                }
            }
        }

    }

    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcGetExtLsaLink                                         */
/*                                                                           */
/* Description  : This function gets the link information from the AS        */
/*                external LSA.                                              */
/*                                                                           */
/* Input        : pLsaInfo     : Pointer to External LSA                     */
/*                pExtLsaLink  : Pointer to structure to hold external       */
/*                               LSA link information                        */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if LSA contents are proper else               */
/*                returns OSIX_FAILURE                                       */
/*****************************************************************************/
PUBLIC INT4
V3RtcGetExtLsaLink (UINT1 *pLsa, UINT2 u2LsaLen, tV3OsExtLsaLink * pExtLsaLink)
{
    UINT1              *pCurrPtr = NULL;
    UINT1               u1FieldBits = 0;
    UINT1               u1PrefLen = 0;

    MEMSET (pExtLsaLink, 0, sizeof (tV3OsExtLsaLink));

    pCurrPtr = pLsa + OSPFV3_LS_HEADER_SIZE;

    u1FieldBits = OSPFV3_LGET1BYTE (pCurrPtr);

    pExtLsaLink->u1FieldBits = u1FieldBits;
    /* Extracting the metric information */
    pExtLsaLink->metric.u4Metric = OSPFV3_LGET3BYTE (pCurrPtr);

    /* If E Bit is set then the metric specified is Type 2 else Type 1 */
    if (u1FieldBits & OSPFV3_EXT_E_BIT_MASK)
    {
        pExtLsaLink->metric.u4MetricType = OSPFV3_TYPE_2_METRIC;
    }
    else
    {
        pExtLsaLink->metric.u4MetricType = OSPFV3_TYPE_1_METRIC;
    }

    /* Extracting Prefix information */
    pExtLsaLink->extRtPrefix.u1PrefixLength = OSPFV3_LGET1BYTE (pCurrPtr);
    pExtLsaLink->extRtPrefix.u1PrefixOpt = OSPFV3_LGET1BYTE (pCurrPtr);
    pCurrPtr += OSPFV3_EXT_LSA_REF_LSTYPE_OFFSET;    /* Skipping Referenced LS Type filed */
    u1PrefLen = (UINT1)
        (OSPFV3_GET_PREFIX_LEN_IN_BYTE
         (pExtLsaLink->extRtPrefix.u1PrefixLength));
    OSPFV3_LGETSTR (pCurrPtr, &(pExtLsaLink->extRtPrefix.addrPrefix),
                    u1PrefLen);

    /* Extracting forwarding address */
    /* If F-Bit is set, then forwarding address field is present 
     * with non zero value */
    if (u1FieldBits & OSPFV3_EXT_F_BIT_MASK)
    {
        if (pLsa + u2LsaLen < pCurrPtr + OSPFV3_IPV6_ADDR_LEN)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "F-Bit is set, but no forwarding "
                          "address is included in external LSA\n"));

            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "F-Bit is set, but no forwarding "
                            "address is included in external LSA\n");
            gu4V3RtcGetExtLsaLinkFail++;
            return OSIX_FAILURE;
        }
        OSPFV3_LGETSTR (pCurrPtr, &(pExtLsaLink->fwdAddr),
                        OSPFV3_IPV6_ADDR_LEN);
    }

    /* Extracting route tag information */
    /* If T-Bit is set, then external route tag filed is present */
    if ((u1FieldBits) & (OSPFV3_EXT_T_BIT_MASK))
    {
        if (pLsa + u2LsaLen < pCurrPtr + OSPFV3_RTR_ID_LEN)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "T-Bit is set, but no route tag "
                          "information is included in external LSA\n"));
            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "T-Bit is set, but no route tag "
                            "information is included in external LSA\n");
            gu4V3RtcGetExtLsaLinkFail++;
            return OSIX_FAILURE;
        }
        pExtLsaLink->u4ExtRouteTag = OSPFV3_LGET4BYTE (pCurrPtr);
    }
    /* Extracting External LSA information is successful */
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function        : V3RtcGetRtTag                                           */
/*                                                                           */
/* Description     : This function gets the tag value for the current path.  */
/*                                                                           */
/* Input           : pPath    - pointer to the path structure                */
/*                                                                           */
/* Output          : pu4RtTag - pointer to the route tag                     */
/*                                                                           */
/* Returns         : None                                                    */
/*****************************************************************************/
PUBLIC VOID
V3RtcGetRtTag (tV3OsPath * pPath, UINT4 *pu4RtTag)
{
    tV3OsExtLsaLink     extLsaLink;

    if ((pPath->pLsaInfo != NULL) && (pPath->pLsaInfo->pLsa != NULL))
    {
        V3RtcGetExtLsaLink (pPath->pLsaInfo->pLsa, pPath->pLsaInfo->u2LsaLen,
                            &extLsaLink);
        *pu4RtTag = extLsaLink.u4ExtRouteTag;
    }
    else
    {
        *pu4RtTag = 0;
    }
}

/*****************************************************************************/
/* Function        : V3RtcMarkAllPrefixAndAsbrLsasInCxt                      */
/*                                                                           */
/* Description     : Mark all the prefix and intra area router LSA nodes     */
/*                   as not used. Setting this flag is useful during         */
/*                   route calculation.                                      */
/*                                                                           */
/* Input           : pV3OspfCxt   -   Context pointer                        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*****************************************************************************/
PUBLIC VOID
V3RtcMarkAllPrefixAndAsbrLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    V3RtcMarkAllInterAreaLsasInCxt (pV3OspfCxt);
    V3RtcMarkAllExtLsasInCxt (pV3OspfCxt);
    V3RtcMarkAllNssaLsasInCxt (pV3OspfCxt);
}

/*****************************************************************************/
/* Function        : V3RtcMarkAllInterAreaLsasInCxt                          */
/*                                                                           */
/* Description     : Mark all the intra area prefix and inter area router    */
/*                   LSA nodes as not used of all the areas.                 */
/*                                                                           */
/* Input           : pV3OspfCxt   -   Context pointer                        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtcMarkAllInterAreaLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tTMO_HASH_TABLE    *pHashTable = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    UINT4               u4HashIndex = 0;
    UINT1               u1Count = 0;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType != OSPFV3_NORMAL_AREA)
        {
            if (pArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY)
            {
                u1Count = 1;
            }
            else
            {
                continue;
            }
        }
        pHashTable = pArea->pInterPrefixLsaTable;

        for (u1Count = 0; u1Count < 2; u1Count++)
        {
            TMO_HASH_Scan_Table (pHashTable, u4HashIndex)
            {
                TMO_HASH_Scan_Bucket (pHashTable, u4HashIndex, pDbHashNode,
                                      tV3OsDbNode *)
                {
                    pDbHashNode->u1Flag = OSPFV3_NOT_USED;
                }
            }
            pHashTable = pArea->pInterRouterLsaTable;
        }
    }
}

/*****************************************************************************/
/* Function        : V3RtcMarkAllNssaLsasInCxt                               */
/*                                                                           */
/* Description     : Mark all the NSSA LSA nodes of all NSSA areas as        */
/*                   not used.                                               */
/*                                                                           */
/* Input           : pV3OspfCxt   -   Context pointer                        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtcMarkAllNssaLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tTMO_HASH_TABLE    *pHashTable = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    UINT4               u4HashIndex = 0;

    if (pV3OspfCxt->u4NssaAreaCount == 0)
    {
        return;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
        {
            continue;
        }
        pHashTable = pArea->pNssaLsaHashTable;

        TMO_HASH_Scan_Table (pHashTable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pHashTable, u4HashIndex, pDbHashNode,
                                  tV3OsDbNode *)
            {
                pDbHashNode->u1Flag = OSPFV3_NOT_USED;
            }
        }
    }
}

/*****************************************************************************/
/* Function        : V3RtcMarkAllExtLsasInCxt                                */
/*                                                                           */
/* Description     : Mark all the external prefix LSA nodes as not used.     */
/*                                                                           */
/* Input           : pV3OspfCxt  -  Context pointer                          */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtcMarkAllExtLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tTMO_HASH_TABLE    *pHashTable = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    UINT4               u4HashIndex = 0;

    if (pV3OspfCxt->u4AsExtLsaCount != 0)
    {
        pHashTable = pV3OspfCxt->pExtLsaHashTable;

        TMO_HASH_Scan_Table (pHashTable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pHashTable, u4HashIndex, pDbHashNode,
                                  tV3OsDbNode *)
            {
                pDbHashNode->u1Flag = OSPFV3_NOT_USED;
            }
        }
    }
}

/*****************************************************************************/
/* Function     : V3RtcMarkAllRoutesInCxt                                    */
/*                                                                           */
/* Description  : Mark all the routes as not found, so that useful for       */
/*                processing unreachable routes at the end of route          */
/*                calculation.                                               */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcMarkAllRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    /* Mark all ASBR route entries as not found */
    TMO_SLL_Scan (&(pV3OspfCxt->ospfV3RtTable.routesList), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pRtEntry =
            OSPFV3_GET_BASE_PTR (tV3OsRtEntry, nextRtEntryNode, pLstNode);
        pRtEntry->u1Flag = OSPFV3_NOT_FOUND;
    }

    /* Updating the network route entries */
    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "No route entries present in TRIE\n"));

        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "TrieGetFirstNode Failure\n");
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        pRtEntry->u1Flag = OSPFV3_NOT_FOUND;
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);
}

/*****************************************************************************/
/* Function     : V3RtcTrieAddRtEntry                                        */
/*                                                                           */
/* Description  : This is the callback function to add the route entry to    */
/*                OSPF routing table                                         */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  OSPF specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  OSPF specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  OSPF specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieAddRtEntry (VOID *pInputParams, VOID *pOutputParams,
                     tV3OsRtEntry ** ppAppSpecInfo,
                     tV3OsRtEntry * pNewAppSpecInfo)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcTrieDeleteRtEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to delete the route entry    */
/*                from OSPF routing table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  OSPF specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  OSPF specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  OSPF specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieDeleteRtEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                        VOID *pOutputParams, VOID *pNextHop, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (pNextHop);
    UNUSED_PARAM (Key);

    (*ppAppSpecInfo) = NULL;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcTrieDelAllRtEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to delete the all the route  */
/*                entries from OSPF routing table. This is used when the     */
/*                entire routing table needs to be cleared.                  */
/*                                                                           */
/* Input        : pInputParams    - Pointer used pointing to input parameters*/
/*                pOutputParams   - Pointer not used by OSPF                 */
/*                VOID (*)(VOID *)- Dummy pointer to the function for future */
/*                                  use.                                     */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : Pointer to the value which helps in deciding whether to    */
/*                delete the route entry at IP level                         */
/*****************************************************************************/
PUBLIC UINT1       *
V3RtcTrieDelAllRtEntry (tInputParams * pInputParams, VOID *dummy,
                        tOutputParams * pOutputParams)
{
    UINT1              *pInpRtInfo = NULL;
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (dummy);
    pInpRtInfo = (UINT1 *) pInputParams;
    return (pInpRtInfo);
}

/*****************************************************************************/
/* Function     : V3RtcTrieDelRoute                                          */
/*                                                                           */
/* Description  : This is the callback function to delete a particular       */
/*                route from OSPF routing table. This is called before       */
/*                deleting each node from trie when entire routing table     */
/*                is cleared.                                                */
/*                                                                           */
/* Input        : pDelIpRt        - Pointer to the value which decides       */
/*                                  whether IP routes need to be deleted     */
/*                ppAppPtr        - Pointer to the Route Entry to be deleted */
/*                                  given by trie.                           */
/*                key             - Key given to trie                        */
/*                                                                           */
/* Output       : ppAppPtr with NULL value                                   */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieDelRoute (VOID *pInRtInfo, VOID **ppAppPtr, tKey key)
{
    UNUSED_PARAM (pInRtInfo);
    UNUSED_PARAM (ppAppPtr);
    UNUSED_PARAM (key);
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcTrieSearchRtEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to search the route entry    */
/*                from OSPF routing table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by OSPF to store the OSPF   */
/*                                  route entry                              */
/*                pAppSpecInfo    - Pointer to the OSPF specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieSearchRtEntry (tInputParams * pInputParams,
                        tOutputParams * pOutputParams, VOID *pAppSpecInfo)
{
    UNUSED_PARAM (pInputParams);
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcTrieLookUpEntry                                       */
/*                                                                           */
/* Description  : This is the callback function for the lookup of the        */
/*                route entry in OSPF routing table                          */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by OSPF to store the OSPF   */
/*                                  route entry                              */
/*                pAppSpecInfo    - Pointer to the OSPF specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieLookUpEntry (tInputParams * pInputParams,
                      tOutputParams * pOutputParams, VOID *pAppSpecInfo,
                      UINT2 u2KeySize, tKey key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (key);
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : V3RtcTrieFindBestMatch                                    */
/* Description   : This function gets the best route from the given node.    */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pAppSpecInfo   - address of the Application specific      */
/*                                info present in TRIE leaf node             */
/*                 u2Keysize    - size of the key                            */
/*                 Key          - Key Value                                  */
/* Output(s)     : pOutputParams                                             */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
PUBLIC INT4
V3RtcTrieFindBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                        VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);
    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return TRIE_SUCCESS;
}

/************************************************************************/
/* Function Name : V3RtcTrieDelete                                      */
/* Description   : This is a call back function for TRIE Delete         */
/* Input(s)      : pInput - Input Parameter.                            */
/* Output(s)     : None.                                                */
/* Return(s)     : None.                                                */
/************************************************************************/
PUBLIC VOID
V3RtcTrieDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/*****************************************************************************/
/* Function     : V3RtcClearAndDeleteRtFromIpInCxt                           */
/*                                                                           */
/* Description  : This function deletes all ABR/ASBR route entries from      */
/*                OSPF routing table and deletes NETWORK route entries from  */
/*                IP and OSPF routing tables.                                */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcClearAndDeleteRtFromIpInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    VOID               *pTemp = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;
    VOID               *pTempRoute = NULL;
    VOID               *pLeafNode = NULL;

    /* Delete all ABR/ASBR route entries */
    while ((pRtEntry = (tV3OsRtEntry *)
            TMO_SLL_Get (&(pV3OspfCxt->ospfV3RtTable.routesList))) != NULL)
    {
        V3RtcFreeRtEntry (pRtEntry);
    }

    /* Deleting NETWORK route entries */
    pRtEntry = NULL;
    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;
    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)

    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "No routes are present in " "routing table\n");
        return;
    }

    pRtEntry = (tV3OsRtEntry *) pTempPtr;
    while (pRtEntry != NULL)
    {

        pLeafNode = inParams.pLeafNode;

        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.u1PrefixLen = pRtEntry->u1PrefixLen;
        if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                             &pTempPtr, (VOID **)
                             &(inParams.pLeafNode)) != TRIE_SUCCESS)
        {
            pTempRoute = NULL;
        }
        else
        {
            pTempRoute = (tV3OsRtEntry *) pTempPtr;
        }

        /* Deleting from Trie */
        if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to delete the route entry from trie\n"));

            OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                        "Failed to delete the route entry from trie\n");
            pRtEntry = pTempRoute;
            continue;

        }

        /* Deleting paths and Freeing the route entry */
        V3RtcFreeRtEntry (pRtEntry);
        pRtEntry = pTempRoute;
    }
}

/*****************************************************************************/
/* Function     : V3RtcGetRefLsIdInfo                                        */
/*                                                                           */
/* Description  : This function extracts reference LSA information from      */
/*                intra area prefix LSA.                                     */
/*                                                                           */
/* Input        : pVertex    : Pointer to Vertex node                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcGetRefLsIdInfo (UINT1 *pLsa, tV3OsLsaId * pLsaId)
{
    UINT1              *pCurrPtr = NULL;

    pCurrPtr = pLsa + OSPFV3_REF_LS_TYPE_OFFSET;

    pLsaId->u2LsaType = OSPFV3_LGET2BYTE (pCurrPtr);
    OSPFV3_LGETSTR (pCurrPtr, pLsaId->linkStateId, OSPFV3_LINKSTATE_ID_LEN);
    OSPFV3_LGETSTR (pCurrPtr, pLsaId->advRtrId, OSPFV3_RTR_ID_LEN);
}

/*---------------------------------------------------------------------------*/
/*                       End of the file o3rtutil.c                          */
/*---------------------------------------------------------------------------*/
