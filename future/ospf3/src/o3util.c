/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3util.c,v 1.29 2018/01/25 10:04:16 siva Exp $
 *
 * Description:This file contains several Utility Functions
 *
 *******************************************************************/
#include "o3inc.h"
#include "utilrand.h"
#ifdef OSPF3_BCMXNP_HELLO_WANTED
#include "cfanp.h"
#endif
PRIVATE INT4 V3UtilMod255 PROTO ((INT4 i4Sum));
#ifdef OSPF3_BCMXNP_HELLO_WANTED
PRIVATE UINT1       gau1TxPktBuf[OSPFV3_MAX_HELLO_PKT_LEN +
                                 CFA_ENET_V2_HEADER_SIZE + IPV6_HEADER_LEN];
extern INT4         CfaGetIfIvrVlanIdExtended (UINT4 u4IfIndex,
                                               UINT2 *pu2IfIvrVlanId);

extern INT4
        VlanGetPortEtherTypeExtended (UINT4 u4IfIndex, UINT2 *pu2EtherType);

extern UINT1        CfaMbsmNpTxOnStackInterface (UINT1 *pu1Pkt, INT4 i4PktSize);
#endif
/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilExtractLsHeaderFromLbuf                              */
/*                                                                           */
/* Description  : Extracts LSA information from linear buffer                */
/*                                                                           */
/* Input        : pLsa           : Pointer to linear buffer                  */
/*                pLsHeader     : Pointer to lsHeader                        */
/*                                                                           */
/* Output       : Filled lsHeader                                            */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilExtractLsHeaderFromLbuf (UINT1 *pLsa, tV3OsLsHeader * pLsHeader)
{
    pLsHeader->u2LsaAge = OSPFV3_LGET2BYTE (pLsa);
    pLsHeader->u2LsaType = OSPFV3_LGET2BYTE (pLsa);
    OSPFV3_LGETSTR (pLsa, pLsHeader->linkStateId, OSPFV3_LINKSTATE_ID_LEN);
    OSPFV3_LGETSTR (pLsa, pLsHeader->advRtrId, OSPFV3_RTR_ID_LEN);
    pLsHeader->lsaSeqNum = OSPFV3_LGET4BYTE (pLsa);
    pLsHeader->u2LsaChksum = OSPFV3_LGET2BYTE (pLsa);
    pLsHeader->u2LsaLen = OSPFV3_LGET2BYTE (pLsa);
}

INT4
V3OspfConvertTimeForSnmp (UINT1 *pu1TimeStr, tUtlTm * tm)
{

    UINT4               u4Month = 0;
    UINT1               au1Temp[OSPFV3_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;
    UINT4               au4YearDays[] = { 0, 31, 31, 62, 92, 123,
        153, 184, 215, 245, 276, 306
    };
    UINT1               au1MonthDays[] = { 31, 29, 31, 30, 31, 30,
        31, 31, 30, 31, 30, 31
    };

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);

    /* Parsing through the function  for year */
    if (pu1CurrPtr != NULL)
    {
        if ((pu1CurrPtr != NULL) && ((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* Check if year is integer or not */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9'))    /* ISDGIT is creating coverity warning */
            && ((pu1CurrPtr[2] >= '0') && (pu1CurrPtr[2] <= '9'))
            && ((pu1CurrPtr[3] >= '0') && (pu1CurrPtr[3] <= '9')))
        {
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = (UINT4) ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < TM_BASE_YEAR)
            {
                return OSPFV3_INVALID_YEAR;
            }
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_YEAR;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MM-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MM-DD,hh:mm:ss" */
        MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        MEMCPY (au1Temp, pu1BasePtr, 2);
        /* Month Validations  and check if date is valid for respective month */

        if ((au1Temp[0] >= '0') && (au1Temp[0] <= '9')    /* Check if sec is integer or not */
            && (au1Temp[1] >= '0') && (au1Temp[1] <= '9'))    /* ISDIGIT throughing coverity errors */
        {
            u4Month = (UINT4) ATOI (au1Temp);

            if ((u4Month > 0) && (u4Month <= 12))
            {
                tm->tm_mon = u4Month - 1;
                tm->tm_yday += au4YearDays[tm->tm_mon];
            }
            else
            {
                return OSPFV3_INVALID_MONTH;
            }
        }
        else
        {
            return OSPFV3_INVALID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-DD,hh:mm:ss" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "DD,hh:mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);

        if (((au1Temp[0] >= '0') && (au1Temp[0] <= '9'))    /* Check if year is integer or not */
            && ((au1Temp[1] >= '0') && (au1Temp[1] <= '9')))    /* ISDGIT is creating coverity warning */
        {
            tm->tm_mday = (UINT4) ATOI (au1Temp);
            tm->tm_yday += tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < 1) || (tm->tm_mday >= OSPFV3_MAX_MONTH_DAYS + 1))
            {
                return OSPFV3_INVALID_DATE;
            }
            if (tm->tm_mday > au1MonthDays[tm->tm_mon])
            {
                return OSPFV3_INVALID_DATE;
            }
            if ((tm->tm_mon == 1) && (!(IS_LEAP (tm->tm_year)))
                && (tm->tm_mday == OSPFV3_MAX_LEAP_FEB_DAYS))
            {
                return OSPFV3_INVALID_DATE;
            }
            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += OSPFV3_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += OSPFV3_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_DATE;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",hh:mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;
        pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "hh:mm:ss" */
        if ((pu1CurrPtr != NULL) && ((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* Check if year is integer or not */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9')))    /* ISDGIT is creating coverity warning */
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > OSPFV3_MAX_HOUR)
            {
                return OSPFV3_INVALID_HOUR;
            }
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_HOUR;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":mm:ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "mm:ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if (((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* Check if min is integer or not */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9')))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of min */
            if (tm->tm_min > OSPFV3_MAX_MIN)
            {
                return OSPFV3_INVALID_MIN;
            }
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_MIN;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":ss" */

    if (pu1CurrPtr != NULL)
    {
        pu1CurrPtr++;            /* pu1CurrPtr postioned at "ss" */
    }
    if (pu1CurrPtr != NULL)
    {
        if ((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9')    /* Check if sec is integer or not */
            && (pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9'))    /* ISDIGIT throughing coverity errors */
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_sec = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of sec */
            if (tm->tm_sec > 60)
            {
                return OSPFV3_INVALID_SEC;
            }
            MEMSET (au1Temp, 0, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_SEC;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    return OSPFV3_SUCCESS;
}

PUBLIC VOID
V3AuthKeyCopy (UINT1 *pu1Dst, UINT1 *pu1Src, INT4 i4Len)
{
    INT4                i4Count;

    for (i4Count = 0; i4Count < OSPFV3_MAX_AUTHKEY_LEN; i4Count++)
    {
        if (i4Count < i4Len)
            *(pu1Dst + i4Count) = *(pu1Src + i4Count);
        else
            *(pu1Dst + i4Count) = (UINT1) 0;
    }

}

PUBLIC tAuthkeyInfo *
V3AuthKeyInfoCreate ()
{
    tAuthkeyInfo       *pAuthkeyInfo;
    UINT4               u4Ospf1sTimer = 0;
    tUtlTm              tm;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    AUTH_ALLOC (&(pAuthkeyInfo));
    UtlGetTime (&tm);
    u4Ospf1sTimer = V3UtilGetSecondsSinceBase (tm);

    if (pAuthkeyInfo == NULL)
    {
        return NULL;
    }

    pAuthkeyInfo->u4KeyStartAccept = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStartGenerate = u4Ospf1sTimer;
    pAuthkeyInfo->u4KeyStopGenerate = DEF_KEY_STOP_GENERATE;
    pAuthkeyInfo->u4KeyStopAccept = DEF_KEY_STOP_ACCEPT;
    pAuthkeyInfo->u1AuthkeyStatus = NOT_IN_SERVICE;

    return (pAuthkeyInfo);
}

PUBLIC VOID
V3SortInsertAuthKeyInfo (tTMO_SLL * pauthkeyLst, tAuthkeyInfo * pAuthkeyInfo)
{
    UINT2               u2AuthkeyId;
    tTMO_SLL_NODE      *pPrev = NULL;
    tAuthkeyInfo       *pTmpAuthkeyInfo;
    tTMO_SLL_NODE      *pLstNode;

    u2AuthkeyId = pAuthkeyInfo->u2AuthkeyId;
    TMO_SLL_Scan (pauthkeyLst, pLstNode, tTMO_SLL_NODE *)
    {
        pTmpAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
        if (pTmpAuthkeyInfo->u2AuthkeyId < u2AuthkeyId)
            pPrev = pLstNode;
        if (pTmpAuthkeyInfo->u2AuthkeyId > u2AuthkeyId)
            break;
    }
    TMO_SLL_Insert (pauthkeyLst, pPrev, (tTMO_SLL_NODE *) pAuthkeyInfo);
}

/************************************************************************
 *  Function Name   : V3OspfConvertTime
 *  Description     : This function will validate the time
                      by parsing through the string against DD-MON-YEAR,HH:MM
                      format and convert the time in string to structure.
 *  Input           : pu1TimeStr -
 *                    tm - Time structure to set or get time of key constants.
 *  Output          : None
 *  Returns         : OSPF_SUCCESS/OSPF_FAILURE
 ************************************************************************/

INT4
V3OspfConvertTime (UINT1 *pu1TimeStr, tUtlTm * tm)
{
    UINT1               au1Temp[OSPFV3_TMP_NUM_STR];
    UINT1              *pu1CurrPtr = NULL;
    UINT1              *pu1BasePtr = NULL;

    pu1CurrPtr = pu1TimeStr;
    pu1BasePtr = pu1TimeStr;

    MEMSET (au1Temp, OSPFV3_ZERO, sizeof (au1Temp));
    /* Parsing through the function  for date */
    if (pu1CurrPtr != NULL)
    {
        /* Checking for date */
        MEMCPY (au1Temp, pu1BasePtr, 2);
        if ((pu1BasePtr != NULL) && ((pu1BasePtr[0] >= '0') && (pu1BasePtr[0] <= '9'))    /* Check if date is an integer */
            && ((pu1BasePtr[1] >= '0') && (pu1BasePtr[1] <= '9')))
        {
            tm->tm_mday = (UINT4) ATOI (au1Temp);
            tm->tm_yday = tm->tm_mday;
            /* Checking for the validity of Date */
            if ((tm->tm_mday < OSPFV3_ONE)
                || (tm->tm_mday > OSPFV3_MAX_MONTH_DAYS + 1))
            {
                return OSPFV3_INVALID_DATE;
            }
            MEMSET (au1Temp, OSPFV3_ZERO, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_DATE;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-MON-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPFV3_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "MON-YEAR,HH:MM" */
        }
        else
        {
            return OSPFV3_FAILURE;
        }
        MEMSET (au1Temp, OSPFV3_ZERO, sizeof (au1Temp));
        MEMCPY (au1Temp, pu1BasePtr, 3);
        /* Month Validations  and check if date is valid for respective month */
        if (STRCASECMP (au1Temp, "Jan") == 0)
        {
            tm->tm_mon = 0;
        }
        else if (STRCASECMP (au1Temp, "Feb") == 0)
        {

            tm->tm_mon = 1;
            tm->tm_yday += OSPFV3_MAX_MONTH_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Mar") == 0)
        {
            tm->tm_mon = 2;
            tm->tm_yday += OSPFV3_MAX_MONTH_DAYS;

        }
        else if (STRCASECMP (au1Temp, "Apr") == 0)
        {
            if (tm->tm_mday > OSPFV3_MIN_MONTH_DAYS + 1)
            {
                return OSPFV3_INVALID_DATE;
            }
            tm->tm_mon = 3;
            tm->tm_yday += OSPFV3_MAX_MAR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "May") == 0)
        {
            tm->tm_mon = 4;
            tm->tm_yday += OSPFV3_MAX_APR_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jun") == 0)
        {
            if (tm->tm_mday >= OSPFV3_MIN_MONTH_DAYS + 1)
            {
                return OSPFV3_INVALID_DATE;
            }
            tm->tm_mon = 5;
            tm->tm_yday += OSPFV3_MAX_MAY_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Jul") == 0)
        {
            tm->tm_mon = 6;
            tm->tm_yday += OSPFV3_MAX_JUN_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Aug") == 0)
        {
            tm->tm_mon = 7;
            tm->tm_yday += OSPFV3_MAX_JUL_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Sep") == 0)
        {
            if (tm->tm_mday >= OSPFV3_MIN_MONTH_DAYS + 1)
            {
                return OSPFV3_INVALID_DATE;
            }
            tm->tm_mon = 8;
            tm->tm_yday += OSPFV3_MAX_AUG_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Oct") == 0)
        {
            tm->tm_mon = 9;
            tm->tm_yday += OSPFV3_MAX_SEP_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Nov") == 0)
        {
            if (tm->tm_mday >= OSPFV3_MIN_MONTH_DAYS + 1)
            {
                return OSPFV3_INVALID_DATE;
            }
            tm->tm_mon = 10;
            tm->tm_yday += OSPFV3_MAX_OCT_DAYS;
        }
        else if (STRCASECMP (au1Temp, "Dec") == 0)
        {
            tm->tm_mon = 11;
            tm->tm_yday += OSPFV3_MAX_NOV_DAYS;
        }
        else
        {
            return OSPFV3_INVALID_MONTH;
        }
        /* End of Month Validation */
    }
    else
    {
        return OSPFV3_FAILURE;
    }

    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, '-');    /*pu1CurrPtr postioned at "-YEAR,HH:MM" */
    /* Checking for separator */
    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPFV3_ZERO] == '-')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /* pu1CurrPtr postioned at "YEAR,HH:MM" */
        }
        else
        {
            return OSPFV3_FAILURE;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if ((pu1CurrPtr != NULL) && ((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* Check if year is integer or not */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9'))    /* ISDGIT is creating coverity warning */
            && ((pu1CurrPtr[2] >= '0') && (pu1CurrPtr[2] <= '9'))
            && ((pu1CurrPtr[3] >= '0') && (pu1CurrPtr[3] <= '9')))
        {
            MEMSET (au1Temp, OSPFV3_ZERO, OSPFV3_TMP_NUM_STR);
            MEMCPY (au1Temp, pu1CurrPtr, 4);
            tm->tm_year = (UINT4) ATOI (au1Temp);

            /* Checking for the validity of Year */
            if (tm->tm_year < OSPFV3_BASE_YEAR)
            {
                return OSPFV3_INVALID_YEAR;
            }
            if (tm->tm_mon >= 2)
            {
                if (IS_LEAP (tm->tm_year))
                {
                    tm->tm_yday += OSPFV3_MAX_LEAP_FEB_DAYS;

                }
                else
                {
                    tm->tm_yday += OSPFV3_MIN_LEAP_FEB_DAYS;
                }
            }
            MEMSET (au1Temp, OSPFV3_ZERO, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_YEAR;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ',');    /*pu1CurrPtr postioned at ",HH:MM" */

    if (pu1CurrPtr != NULL)
    {
        if (pu1CurrPtr[OSPFV3_ZERO] == ',')
        {
            pu1CurrPtr++;
            pu1BasePtr = pu1CurrPtr;    /*pu1CurrPtr postioned at "HH:MM" */
        }
        else
        {
            return OSPFV3_FAILURE;
        }
        if ((pu1CurrPtr != NULL) && ((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* check if hour is integer */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9')))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_hour = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of Hour */
            if (tm->tm_hour > OSPFV3_MAX_HOUR)
            {
                return OSPFV3_INVALID_HOUR;
            }
            MEMSET (au1Temp, OSPFV3_ZERO, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_HOUR;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    pu1CurrPtr = (UINT1 *) STRCHR (pu1CurrPtr, ':');    /*pu1CurrPtr postioned at ":MM" */

    if (pu1CurrPtr != NULL)
    {
        /* Checking for Separator */
        if (pu1CurrPtr[OSPFV3_ZERO] == ':')
        {
            pu1CurrPtr++;        /* pu1CurrPtr postioned at "MM" */
        }
        else
        {
            return OSPFV3_INVALID_HOUR;
        }
    }

    if (pu1CurrPtr != NULL)
    {
        if (((pu1CurrPtr[0] >= '0') && (pu1CurrPtr[0] <= '9'))    /* Check if min is integer or not */
            && ((pu1CurrPtr[1] >= '0') && (pu1CurrPtr[1] <= '9')))
        {
            MEMCPY (au1Temp, pu1CurrPtr, 2);
            tm->tm_min = (UINT4) ATOI (au1Temp);
            /* Checking for the validity of Min */
            if (tm->tm_min > OSPFV3_MAX_MIN)
            {
                return OSPFV3_INVALID_MIN;
            }
            MEMSET (au1Temp, OSPFV3_ZERO, OSPFV3_TMP_NUM_STR);
        }
        else
        {
            return OSPFV3_INVALID_MIN;
        }
    }
    else
    {
        return OSPFV3_FAILURE;
    }
    return OSPFV3_SUCCESS;
}

INT1
V3ConvertKeyTime (UINT4 u4Secs, tUtlTm * tm)
{
    UINT1               au1Mon[12];
    MEMSET (au1Mon, OSPFV3_ZERO, sizeof (au1Mon));

    UtlGetTimeForSeconds (u4Secs, tm);    /* Tm structure is filled for secs */

    /* Month is filled in array and year_day is reduced to date in this process */
    if (tm->tm_mon == 0)
    {
        STRCPY (au1Mon, "Jan");
    }
    else if (tm->tm_mon == 1)
    {
        STRCPY (au1Mon, "Feb");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_MONTH_DAYS;
    }
    else if (tm->tm_mon == 2)
    {
        STRCPY (au1Mon, "Mar");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_MONTH_DAYS;
    }
    else if (tm->tm_mon == 3)
    {
        STRCPY (au1Mon, "Apr");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_MAR_DAYS;
    }
    else if (tm->tm_mon == 4)
    {
        STRCPY (au1Mon, "May");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_APR_DAYS;
    }
    else if (tm->tm_mon == 5)
    {
        STRCPY (au1Mon, "Jun");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_MAY_DAYS;
    }
    else if (tm->tm_mon == 6)
    {
        STRCPY (au1Mon, "Jul");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_JUN_DAYS;
    }
    else if (tm->tm_mon == 7)
    {
        STRCPY (au1Mon, "Aug");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_JUL_DAYS;
    }
    else if (tm->tm_mon == 8)
    {
        STRCPY (au1Mon, "Sep");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_AUG_DAYS;
    }
    else if (tm->tm_mon == 9)
    {
        STRCPY (au1Mon, "Oct");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_SEP_DAYS;
    }
    else if (tm->tm_mon == 10)
    {
        STRCPY (au1Mon, "Nov");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_OCT_DAYS;
    }
    else if (tm->tm_mon == 11)
    {
        STRCPY (au1Mon, "Dec");
        tm->tm_yday = tm->tm_yday - OSPFV3_MAX_NOV_DAYS;
    }

    if (tm->tm_mon >= 2)        /* In the case of Leap Year for the month of Feb Year_Day is reduced */
    {
        if (IS_LEAP (tm->tm_year))
        {
            tm->tm_yday = tm->tm_yday - OSPFV3_MAX_LEAP_FEB_DAYS;
        }
        else
        {
            tm->tm_yday = tm->tm_yday - OSPFV3_MIN_LEAP_FEB_DAYS;
        }
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : OspfPrintKeyTime
 *  Description     : This function will convert the time in structure to
                      string.
 *  Input           : u4Secs -Seconds
 *  Output          : pOspfKeyTime- Returns the Value in array
 *  Returns         : NONE
 ************************************************************************/
VOID
V3OspfPrintKeyTime (UINT4 u4Secs, UINT1 *pOspfv3KeyTime)
{
    tUtlTm              tm;
    UINT1               au1Mon[12];

    MEMSET (au1Mon, OSPFV3_ZERO, sizeof (au1Mon));
    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    UtlGetTimeForSeconds (u4Secs, &tm);    /* Tm structure is filled for secs */

    /* Month is filled in array and year_day is reduced to date in this process */
    if (tm.tm_mon == 0)
    {
        STRCPY (au1Mon, "Jan");
    }
    else if (tm.tm_mon == 1)
    {
        STRCPY (au1Mon, "Feb");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 2)
    {
        STRCPY (au1Mon, "Mar");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_MONTH_DAYS;
    }
    else if (tm.tm_mon == 3)
    {
        STRCPY (au1Mon, "Apr");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_MAR_DAYS;
    }
    else if (tm.tm_mon == 4)
    {
        STRCPY (au1Mon, "May");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_APR_DAYS;
    }
    else if (tm.tm_mon == 5)
    {
        STRCPY (au1Mon, "Jun");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_MAY_DAYS;
    }
    else if (tm.tm_mon == 6)
    {
        STRCPY (au1Mon, "Jul");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_JUN_DAYS;
    }
    else if (tm.tm_mon == 7)
    {
        STRCPY (au1Mon, "Aug");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_JUL_DAYS;
    }
    else if (tm.tm_mon == 8)
    {
        STRCPY (au1Mon, "Sep");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_AUG_DAYS;
    }
    else if (tm.tm_mon == 9)
    {
        STRCPY (au1Mon, "Oct");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_SEP_DAYS;
    }
    else if (tm.tm_mon == 10)
    {
        STRCPY (au1Mon, "Nov");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_OCT_DAYS;
    }
    else if (tm.tm_mon == 11)
    {
        STRCPY (au1Mon, "Dec");
        tm.tm_yday = tm.tm_yday - OSPFV3_MAX_NOV_DAYS;
    }

    if (tm.tm_mon >= 2)            /* In the case of Leap Year for the month of Feb Year_Day is reduced */
    {
        if (IS_LEAP (tm.tm_year))
        {
            tm.tm_yday = tm.tm_yday - OSPFV3_MAX_LEAP_FEB_DAYS;
        }
        else
        {
            tm.tm_yday = tm.tm_yday - OSPFV3_MIN_LEAP_FEB_DAYS;
        }
    }
    /* Values are printed in the structure */
    SPRINTF ((CHR1 *) pOspfv3KeyTime,
             "%.2u-%s-%4u,%.2u:%.2u",
             tm.tm_yday, au1Mon, tm.tm_year, tm.tm_hour, tm.tm_min);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMod255                                               */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                a modulo 255 operator using rule of 9's which is much      */
/*                faster than generic '%' (C mod operator)                   */
/*                                                                           */
/* Input        : i4Sum            : value whose mod is to be found          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Result of the mod operation                                */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
V3UtilMod255 (INT4 i4Sum)
{
    UINT1              *pu1Ptr = (UINT1 *) &i4Sum;
    INT4                i4Res = 0;
    INT1                i1Neg = OSIX_FALSE;

    if (i4Sum < 0)
    {
        i4Sum = -i4Sum;
        i1Neg = OSIX_TRUE;
    }
    i4Res = *pu1Ptr++;
    i4Res += *pu1Ptr++;
    i4Res += *pu1Ptr++;
    i4Res += *pu1Ptr;

    i4Res = (i4Res & 0xff) + (i4Res >> 8);
    i4Res = (i4Res & 0xff) + (i4Res >> 8);

    if (i4Res == 0xff)
    {
        i4Res = 0;
    }
    if (i1Neg == OSIX_TRUE)
    {
        i4Res = ~i4Res;
    }

    return (i4Res);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilComputeLsaFletChksum                                 */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                Generates a fletcher's checksum and inserts in lsa.This    */
/*                procedure is written specifically for the OSPF lsa format. */
/*                The lsa should be present in a linear buffer.              */
/*                                                                           */
/* Input        : pLsa          : pointer to LSA                             */
/*                u2Len         : length of LSA                              */
/*                                                                           */
/* Output       : LSA updated with the computed fletched checksum            */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilComputeLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{
    INT4                i4C0 = 0;
    INT4                i4C1 = 0;
    INT4                i4Cmul = 0;
    UINT1              *pCurr = NULL;
    UINT1              *pEnd = NULL;
    INT4                i4X = 0;
    INT4                i4Y = 0;
    INT4                i4ModCounter = 1;

    *(pLsa + OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER) = 0;
    *(pLsa + OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER + 1) = 0;

    pEnd = pLsa + u2Len;

    /* age field is exempted from checksum calculation */

    for (pCurr = pLsa + OSPFV3_AGE_SIZE_IN_LS_HEADER; pCurr < pEnd; pCurr++)
    {
        i4C0 += *pCurr;
        i4C1 += i4C0;
        i4ModCounter++;
        if (i4ModCounter == OSPFV3_MAX_MOD_COUNTER_VALUE)
        {
            i4C0 = V3UtilMod255 (i4C0);
            i4C1 = V3UtilMod255 (i4C1);
            i4ModCounter = 1;
        }
    }

    i4C0 = V3UtilMod255 (i4C0);
    i4C1 = V3UtilMod255 (i4C1);

    i4Cmul = (u2Len - OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER) * (long) i4C0;
    i4X = V3UtilMod255 (i4Cmul - i4C0 - i4C1);
    i4Y = V3UtilMod255 (i4C1 - i4Cmul);

    if (i4X == 0)
    {
        i4X = 0xff;
    }
    if (i4Y == 0)
    {
        i4Y = 0xff;
    }

    *(pLsa + OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER) = (UINT1) (i4X & 0xff);
    *(pLsa + OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER + 1) = (UINT1) (i4Y & 0xff);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilVerifyLsaFletChksum                                  */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies the checksum of the lsa. This proc-*/
/*                edure is written specifically for the OSPF lsa format. The */
/*                lsa is assumed to be in a linear buffer. This procedure    */
/*                returns OSIX_TRUE for success and OSIX_FALSE for checksum  */
/*                failure.                                                   */
/*                                                                           */
/* Input        : pLsa         : pointer to LSA                              */
/*                u2Len        : length of LSA                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if checksum is ok                               */
/*                OSIX_FALSE, if checksum is not ok                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3UtilVerifyLsaFletChksum (UINT1 *pLsa, UINT2 u2Len)
{
    UINT4               u4C0 = 0;
    UINT4               u4C1 = 0;
    UINT1              *pCurr = 0;
    INT4                i4ModCounter = 1;

    /* age field is exempted from checksum calculation */

    for (pCurr = pLsa + OSPFV3_AGE_SIZE_IN_LS_HEADER,
         u2Len -= OSPFV3_AGE_SIZE_IN_LS_HEADER; u2Len; pCurr++, u2Len--)
    {
        u4C0 += *pCurr;
        u4C1 += u4C0;
        i4ModCounter++;
        if (i4ModCounter == OSPFV3_MAX_MOD_COUNTER_VALUE)
        {
            u4C0 = V3UtilMod255 (u4C0);
            u4C1 = V3UtilMod255 (u4C1);
            i4ModCounter = 1;
        }
    }

    /* mod 255 adjustment */

    u4C0 = V3UtilMod255 (u4C0);
    u4C1 = V3UtilMod255 (u4C1);

    if ((!u4C0) && (!u4C1))
    {
        return (OSIX_TRUE);
    }
    return (OSIX_FALSE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilConstructHdr                                         */
/*                                                                           */
/* Description  : This procedure constructs the ospfv3 header for packets to */
/*                be transmitted on the specified interface. This procedure  */
/*                will be called after the packet is fully constructed.      */
/*                                                                           */
/* Input        : pInterface       : interface over which the packets are    */
/*                                   transmitted                             */
/*                pBuf             : buffer which holds the header           */
/*                u1Type           : type                                    */
/*                u2Len            : length                                  */
/*                                                                           */
/* Output       : pBuf, buffer which holds the header                        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3UtilConstructHdr (tV3OsInterface * pInterface,
                    UINT1 *pBuf, UINT1 u1Type, UINT2 u2Len)
{
    OSPFV3_BUFFER_ASSIGN_1_BYTE (pBuf, OSPFV3_VERSION_NO_OFFSET,
                                 OSPFV3_VERSION_NO);
    OSPFV3_BUFFER_ASSIGN_1_BYTE (pBuf, OSPFV3_TYPE_OFFSET, u1Type);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pBuf, OSPFV3_PKT_LENGTH_OFFSET, u2Len);
    OSPFV3_BUFFER_APPEND_STRING (pBuf, pInterface->pArea->pV3OspfCxt->rtrId,
                                 OSPFV3_RTR_ID_OFFSET, OSPFV3_RTR_ID_LEN);
    OSPFV3_BUFFER_APPEND_STRING (pBuf, OSPFV3_GET_IF_AREA_ID (pInterface),
                                 OSPFV3_AREA_ID_OFFSET, OSPFV3_AREA_ID_LEN);
    /* Checksum field is set to 0 before calculation */
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pBuf, OSPFV3_CHKSUM_OFFSET, 0);
    OSPFV3_BUFFER_ASSIGN_1_BYTE (pBuf, OSPFV3_INSTANCE_OFFSET,
                                 pInterface->u2InstId);
    OSPFV3_BUFFER_ASSIGN_1_BYTE (pBuf, OSPFV3_RSVD_OFFSET, 0);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilVirtIfIndComp                                        */
/*                                                                           */
/* Description  : compares the two virtual interface indices.                */
/*                                                                           */
/* Input        : areaId1    :  Area Id 1                                    */
/*                nbrId1     :  Neighbor Id 1                                */
/*                areaId2    :  Area Id 2                                    */
/*                nbrId2     :  Neighbor Id 2                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if the couples are equal                     */
/*                OSPFV3_GREATER, if couple1 is greater                      */
/*                OSPFV3_LESS, if couple2 is greater                         */
/*                couple is <Area Id, Neighbor Id>                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3UtilVirtIfIndComp (tV3OsAreaId areaId1, tV3OsRouterId nbrId1,
                     tV3OsAreaId areaId2, tV3OsRouterId nbrId2)
{
    if (areaId1 == NULL)
    {
        return OSPFV3_LESS;
    }

    if (areaId2 == NULL)
    {
        return OSPFV3_GREATER;
    }

    switch (V3UtilAreaIdComp (areaId1, areaId2))
    {
        case OSPFV3_EQUAL:
            if (nbrId1 == NULL)
            {
                return OSPFV3_LESS;
            }
            else if (nbrId2 == NULL)
            {
                return OSPFV3_GREATER;
            }
            else
            {
                return (V3UtilRtrIdComp (nbrId1, nbrId2));
            }

        case OSPFV3_LESS:
            return OSPFV3_LESS;

        default:
            return OSPFV3_GREATER;

    }                            /* end of switch */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilIp6Chksum                                            */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                Routine to calculate IPv6 checksum. The checksum field is  */
/*                the 16 bit one's complement of the one's complement sum of */
/*                all 16 bit words in the header. The routine assumes that   */
/*                the data to calculate checksum begins at the current read  */
/*                offset in the buffer.                                      */
/*                                                                           */
/* Input        : pBuf                         : pointer to message          */
/*                u2Len                        : length of LSA               */
/*                srcAddr                      : Source Address              */
/*                dstAddr                      : Destination Address         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Checksum                                                   */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT2
V3UtilIp6Chksum (UINT1 *pBuf, UINT4 u4Len, tIp6Addr * srcAddr,
                 tIp6Addr * dstAddr)
{
    UINT4               u4Chsum = 0;
    UINT2               u2Tmp = 0;
    UINT4               u4ReadOffset = 0;
    UINT1               u1Proto = OSPFV3_PROTO;
    UINT4               i = 0;
    UINT2               u2OddByte = 0;

    OSPFV3_BUFFER_ASSIGN_2_BYTE (pBuf, OSPFV3_CHKSUM_OFFSET, 0);

    for (i = 0; i < OSPFV3_IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        u4Chsum += OSIX_NTOHS (srcAddr->u2_addr[i]);
    }

    for (i = 0; i < OSPFV3_IP6_ADDR_ACCESS_IN_SHORT_INT; i++)
    {
        u4Chsum += OSIX_NTOHS (dstAddr->u2_addr[i]);
    }

    u4Chsum += u4Len >> 16;
    u4Chsum += u4Len & 0xffff;
    u4Chsum += u1Proto;

    while (u4Len > 1)
    {
        OSPFV3_BUFFER_EXTRACT_2_BYTE (pBuf, u4ReadOffset, u2Tmp);
        u4Chsum += u2Tmp;
        u4Len -= 2;
        u4ReadOffset += 2;
    }

    /* odd byte */
    if (u4Len == 1)
    {
        u2OddByte = 0;
        *((UINT1 *) &u2OddByte) = *(UINT1 *) (pBuf + u4ReadOffset);
        u4Chsum += u2OddByte;
    }

    /* Repeat while overflow bit exists */
    while (u4Chsum >> 16)
    {
        u4Chsum = (u4Chsum >> 16) + (u4Chsum & 0xffff);
    }

    return ((UINT2) (~u4Chsum));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilJitter                                               */
/*                                                                           */
/* Description  : Determines the jitter value based on the jitter percent    */
/*                and adds it to the value to be jittered.                   */
/*                                                                           */
/* Input        : u4Value           :  value to be jittered                  */
/*                u4JitterPcent     :  jitter percent                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Modified value after jittering                             */
/*                                                                           */
/*****************************************************************************/

UINT4
V3UtilJitter (UINT4 u4Value, UINT4 u4JitterPcent)
{
    UINT4               u4Max = 0;
    UINT4               u4Min = 0;

    u4Max = u4Value + ((u4Value * u4JitterPcent) / OSPFV3_MAX_PERCENTAGE);
    u4Min = u4Value - ((u4Value * u4JitterPcent) / OSPFV3_MAX_PERCENTAGE);
    return (OSIX_RAND (u4Min, u4Max));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilRtrIdComp                                            */
/*                                                                           */
/* Description  : This procedure compares two router ids                     */
/*                                                                           */
/* Input        : rtrId1  :   Router Id 1                                    */
/*                rtrId2  :   Router Id 2                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if they are equal                            */
/*                OSPFV3_GREATER, if rtrId 1 is greater                      */
/*                OSPFV3_LESS, if IP rtrId 2 is greater                      */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3UtilRtrIdComp (tV3OsRouterId rtrId1, tV3OsRouterId rtrId2)
{
    UINT4               u4RtrId1 = 0;
    UINT4               u4RtrId2 = 0;

    /* returns OSPFV3_EQUAL if they are equal
     * OSPFV3_GREATER if rtrId1 is greater
     * OSPFV3_LESS if rtrId1 is lesser
     */

    if ((rtrId1 == NULL) && (rtrId2 == NULL))
    {
        return OSPFV3_EQUAL;
    }

    if (rtrId1 == NULL)
    {
        return OSPFV3_LESS;
    }
    if (rtrId2 == NULL)
    {
        return OSPFV3_GREATER;
    }

    u4RtrId1 = OSPFV3_BUFFER_DWFROMPDU (rtrId1);
    u4RtrId2 = OSPFV3_BUFFER_DWFROMPDU (rtrId2);

    if (u4RtrId1 > u4RtrId2)
    {
        return OSPFV3_GREATER;
    }
    else if (u4RtrId1 < u4RtrId2)
    {
        return OSPFV3_LESS;
    }

    return (OSPFV3_EQUAL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilAreaIdComp                                           */
/*                                                                           */
/* Description  : This procedure compares two area ids                       */
/*                                                                           */
/* Input        : area1   :   Area Id 1                                      */
/*                area2   :   Area Id 2                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if they are equal                            */
/*                OSPFV3_GREATER, if area 1 is greater                       */
/*                OSPFV3_LESS, if IP area 2 is greater                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3UtilAreaIdComp (tV3OsAreaId area1, tV3OsAreaId area2)
{
    UINT4               u4Area1 = 0;
    UINT4               u4Area2 = 0;

    /* returns OSPFV3_EQUAL if they are equal
     * OSPFV3_GREATER if area1 is greater
     * OSPFV3_LESS if area1 is lesser
     */

    if ((area1 == NULL) && (area2 == NULL))
    {
        return OSPFV3_EQUAL;
    }

    if (area1 == NULL)
    {
        return OSPFV3_LESS;
    }
    if (area2 == NULL)
    {
        return OSPFV3_GREATER;
    }

    u4Area1 = OSPFV3_BUFFER_DWFROMPDU (area1);
    u4Area2 = OSPFV3_BUFFER_DWFROMPDU (area2);

    if (u4Area1 > u4Area2)
    {
        return OSPFV3_GREATER;
    }
    else if (u4Area1 < u4Area2)
    {
        return OSPFV3_LESS;
    }
    return (OSPFV3_EQUAL);
}

/*****************************************************************************/
/* Function     : V3UtilExtractLsHeaderFromPkt                               */
/*                                                                           */
/* Description  : Extracts the lsHeader information from OSPFv3 packet       */
/*                                                                           */
/* Input        : pPkt          : pointer to linear buffer having            */
/*                                OSPFv3 packet.                             */
/*                pLsHeader     : Pointer to lsHeader                        */
/*                                                                           */
/* Output       : Filled lsHeader                                            */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3UtilExtractLsHeaderFromPkt (UINT1 *pPkt, tV3OsLsHeader * pLsHeader,
                              UINT2 u2OffsetLsaCount, UINT1 u1PktType)
{
    UINT1              *pCurrPtr = NULL;

    pCurrPtr = pPkt + OSPFV3_PKT_HEADER_LEN;

    if (u1PktType == OSPFV3_DD_PKT)
    {
        pCurrPtr += OSPFV3_DDP_FIXED_PORTION_SIZE;
    }

    pCurrPtr += (u2OffsetLsaCount * OSPFV3_LS_HEADER_SIZE);

    V3UtilExtractLsHeaderFromLbuf (pCurrPtr, pLsHeader);
}

/*****************************************************************************/
/* Function     : V3UtilIp6AddrComp                                          */
/*                                                                           */
/* Description  : This procedure compares two Ipv6 addresses                 */
/*                                                                           */
/* Input        : pAddr1   : Pointer to IPv6 address 1                       */
/*                pAddr2   : Pointer to IPv6 address 2                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if the IPv6 addresses are equal              */
/*                OSPFV3_GREATER, if IPv6 address 1 is greater               */
/*                OSPFV3_LESS, if IPv6 address 2 is greater                  */
/*****************************************************************************/
PUBLIC INT1
V3UtilIp6AddrComp (tIp6Addr * pAddr1, tIp6Addr * pAddr2)
{
    INT4                i4Val;

    i4Val = MEMCMP (pAddr1, pAddr2, OSPFV3_IPV6_ADDR_LEN);

    if (i4Val < 0)
    {
        return OSPFV3_LESS;
    }
    else if (i4Val > 0)
    {
        return OSPFV3_GREATER;
    }
    else
    {
        return OSPFV3_EQUAL;
    }
}

/*****************************************************************************/
/* Function     : V3UtilIp6PrefixComp                                        */
/*                                                                           */
/* Description  : This procedure compares two IPv6 addresses after applying  */
/*                the prefix.                                                */
/*                                                                           */
/* Input        : pAddr1    : Pointer to IPv6 address 1                      */
/*                pAddr2    : Pointer to IPv6 address 2                      */
/*                u1PrefixLen : Prefix length to be applied on IPv6          */
/*                              addresses                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPFV3_EQUAL, if the IPv6 prefixes are equal               */
/*                else returns greater than zero value or less than          */
/*                zero value.                                                */
/*****************************************************************************/
PUBLIC INT4
V3UtilIp6PrefixComp (tIp6Addr * pAddr1, tIp6Addr * pAddr2, UINT1 u1PrefixLen)
{
    UINT1               u1Bytes = 0;
    UINT1               u1Bits = 0;
    UINT1               u1Byte1 = 0;
    UINT1               u1Byte2 = 0;
    UINT1               u1Mask = 0;
    INT4                i4RetVal = OSPFV3_EQUAL;

    u1Bytes = (UINT1) (u1PrefixLen / 8);
    u1Bits = (UINT1) (u1PrefixLen % 8);

    if ((u1Bytes <= OSPFV3_IPV6_ADDR_LEN) && (u1Bytes != 0))
    {
        i4RetVal =
            MEMCMP (pAddr1, pAddr2, MEM_MAX_BYTES (sizeof (tIp6Addr), u1Bytes));
        if (i4RetVal != 0)
        {
            return i4RetVal;
        }
    }

    if ((u1Bytes < OSPFV3_IPV6_ADDR_LEN) && (u1Bits != 0))
    {
        u1Mask = (UINT1) (0xff << (8 - u1Bits));
        u1Byte1 = pAddr1->u1_addr[u1Bytes] & u1Mask;
        u1Byte2 = pAddr2->u1_addr[u1Bytes] & u1Mask;
        return (u1Byte1 - u1Byte2);
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : V3UtilIp6PrefixCopy                                        */
/*                                                                           */
/* Description  : This procedure copies IP address from the source to the    */
/*                destination, after prefix is applied on the source address.*/
/*                                                                           */
/* Input        : pDest       : Pointer to destination address               */
/*                pSrc        : Pointer to Source address                    */
/*                u1PrefixLen :  Copy upto the prefix Length                 */
/*                                                                           */
/* Output       : destination address                                        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3UtilIp6PrefixCopy (tIp6Addr * pDest, tIp6Addr * pSrc, UINT1 u1PrefixLen)
{
    UINT1              *pDestination = NULL;
    UINT1              *pSource = NULL;
    UINT1               u1Bits = 0;
    UINT1               u1Bytes = 0;

    pDestination = (UINT1 *) pDest;
    pSource = (UINT1 *) pSrc;

    u1Bytes = (UINT1) (u1PrefixLen / 8);
    u1Bits = (UINT1) (u1PrefixLen % 8);

    MEMSET (pDest, 0, sizeof (tIp6Addr));
    MEMCPY (pDestination, pSource, u1Bytes);

    if (u1Bytes != OSPFV3_IPV6_ADDR_LEN)
    {
        MEMSET ((pDestination + u1Bytes), 0, (OSPFV3_IPV6_ADDR_LEN - u1Bytes));
        *(pDestination + u1Bytes) =
            (UINT1) ((*(pSource + u1Bytes)) & (0xff << (8 - u1Bits)));
    }
}

/*****************************************************************************/
/* Function     : V3UtilHashGetValue                                         */
/*                                                                           */
/* Description  : This procedure gets the hash value for the key             */
/*                                                                           */
/* Input        : u4HashSize    :  Max. no. of buckets                       */
/*                pKey           :  Pointer to information to be hashed      */
/*                u1KeyLen      :  Length of the information that is to      */
/*                                   be hashed.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hash Bucket value                                          */
/*****************************************************************************/
PUBLIC UINT4
V3UtilHashGetValue (UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen)
{
    UINT4               u4H = 0;
    UINT4               u4G;
    UINT1              *pPtr = NULL;

    for (pPtr = pKey; u1KeyLen; pPtr++, u1KeyLen--)
    {
        u4H = (u4H << 4) + (*pPtr);
        u4G = u4H & 0xf0000000;
        if (u4G)
        {
            u4H = u4H ^ (u4G >> 24);
            u4H = u4H ^ u4G;
            u4G = u4H & 0xf0000000;
        }
    }
    return u4H % u4HashSize;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3UtilLsaIdComp                                           */
/*                                                                          */
/* Description  : This procedure compares two LSA IDs                       */
/*                                                                          */
/* Input        : u2LsaType1       :  LSA type 1                            */
/*                linkStateId1     :  Link state Id 1                       */
/*                advRtrId1        :  Advertising Router Id 1               */
/*                u2LsaType2       :  LSA type 2                            */
/*                linkStateId2     :  Link state Id 2                       */
/*                advRtrId2        :  Advertising Router Id 2               */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      :  OSPFV3_EQUAL, if the triplets are equal                  */
/*                 OSPFV3_GREATER, if triplet1 is greater                   */
/*                 OSPFV3_LESS, if triplet2 is greater                      */
/*                 triplet is <Lsa type, Link state Id, Adv. Rtr Id>        */
/*                                                                          */
/****************************************************************************/

PUBLIC INT1
V3UtilLsaIdComp (UINT2 u2LsaType1, tV3OsLinkStateId linkStateId1,
                 tV3OsRouterId advRtrId1, UINT2 u2LsaType2,
                 tV3OsLinkStateId linkStateId2, tV3OsRouterId advRtrId2)
{
    if (u2LsaType1 < u2LsaType2)
    {
        return OSPFV3_LESS;
    }
    else if (u2LsaType1 > u2LsaType2)
    {
        return OSPFV3_GREATER;
    }
    switch (V3UtilLinkStateIdComp (linkStateId1, linkStateId2))
    {
        case OSPFV3_LESS:
            return OSPFV3_LESS;

        case OSPFV3_GREATER:
            return OSPFV3_GREATER;

        default:
            /* default occurs when OSPFV3_EQUAL is returned */
            return (V3UtilRtrIdComp (advRtrId1, advRtrId2));
    }
}

/*****************************************************************************/
/* Function     : V3UtilExtractPrefixFromLsa                                 */
/*                                                                           */
/* Description  : This function is to extract the prefix from LSAs           */
/*                                                                           */
/* Input        : pCurrPtr    : Starting address of the prefix info in LSA   */
/*                pPrefInfo   : Pointer to the prefix structure to where     */
/*                              we want to extract the Prefix information    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the to next field after the prefix.             */
/*****************************************************************************/
PUBLIC UINT1       *
V3UtilExtractPrefixFromLsa (UINT1 *pCurrPtr, tV3OsPrefixInfo * pPrefInfo)
{
    UINT1               u1PrefFieldLen = 0;

    pPrefInfo->u1PrefixLength = OSPFV3_LGET1BYTE (pCurrPtr);
    pPrefInfo->u1PrefixOpt = OSPFV3_LGET1BYTE (pCurrPtr);
    pPrefInfo->u2Metric = OSPFV3_LGET2BYTE (pCurrPtr);

    MEMSET (&(pPrefInfo->addrPrefix), 0, OSPFV3_IPV6_ADDR_LEN);

    u1PrefFieldLen = (UINT1) (OSPFV3_GET_PREFIX_LEN_IN_BYTE
                              (pPrefInfo->u1PrefixLength));

    OSPFV3_LGETSTR (pCurrPtr, &(pPrefInfo->addrPrefix), u1PrefFieldLen);

    return pCurrPtr;
}

/*****************************************************************************/
/* Function     : V3UtilIsMatchedPrefHashNode                                */
/*                                                                           */
/* Description  : This is utility function used to see whether the given     */
/*                DB node is matching with the given prefix and prefix       */
/*                length or not.                                             */
/*                                                                           */
/* Input        : pDbHashNode : Pointer to DB node                           */
/*                pKey        : Pointer to IPv6 prefix                       */
/*                u1PrefixLen : Length of prefix                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Returns OSIX_TRUE, if the DB node contents are matching    */
/*                with given prefix and prefix length.                       */
/*                otherwise returns OSIX_FALSE                               */
/*****************************************************************************/
PUBLIC INT1
V3UtilIsMatchedPrefHashNode (tV3OsDbNode * pDbHashNode, tIp6Addr * pRtPrefix,
                             UINT1 u1PrefixLen)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1              *pCurrPtr = NULL;
    UINT1               u1LsaPrefLen = 0;

    pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

    pLsaInfo =
        (tV3OsLsaInfo *) OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                              pLstNode);

    /* Get the prefix and prefix length from LSA */
    pCurrPtr = pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE +
        OSPFV3_INTER_AREA_PFX_LSA_PFXLEN_OFFSET;

    /* Getting the prefix length from LSA */
    u1LsaPrefLen = *pCurrPtr;

    pCurrPtr += OSPFV3_INTER_AREA_PFX_LSA_PFXLEN_OFFSET;

    if ((u1PrefixLen == u1LsaPrefLen) &&
        (MEMCMP (pRtPrefix, pCurrPtr,
                 OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen)) == OSPFV3_EQUAL))
    {
        return OSIX_TRUE;
    }
    else
    {
        gu4V3UtilIsMatchedPrefHashNodeFail++;
        return OSIX_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilFindIsTransitArea                                    */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure verifies if area is transit are-            */
/*                                                                           */
/* Input        : pArea         : pointer to area                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if transit area                                 */
/*                OSIX_FALSE, if not transit area                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3UtilFindIsTransitArea (tV3OsArea * pArea)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
        if (V3UtilAreaIdComp (pInterface->transitAreaId, pArea->areaId) ==
            OSPFV3_EQUAL)
        {
            return OSIX_TRUE;
        }
    }
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilGetCxtName                                           */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure returns the context name for the id         */
/*                This is called from Trace functions.                       */
/*                                                                           */
/*                If context id is invalid, it is global trace,              */
/*                return global                                              */
/*                                                                           */
/*                If context id is valid, return the context name            */
/*                                                                           */
/* Input        : u4ContextId  : Context identifier                          */
/*                                                                           */
/* Output       : pu1Alias     : Context name                                */
/*                                                                           */
/* Returns      : Context Name or Global                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilGetCxtName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    UINT1               au1CxtName[OSPFV3_CXT_MAX_LEN];

    MEMSET (au1CxtName, 0, OSPFV3_CXT_MAX_LEN);

    /* If the context Id is invalid, return global */
    if (u4ContextId == OSPFV3_INVALID_CXT_ID)
    {
        STRCPY (au1CxtName, "Global");
    }
    else
    {
        if (V3OspfGetSystemModeExt (OSPF3_PROTOCOL_ID) == OSPFV3_SI_MODE)
        {
            /* System is running in SI mode */
            STRCPY (au1CxtName, "default");
        }
        else
        {
            /* Get the context name corresponding to the context id */
            V3OspfGetAliasName (u4ContextId, au1CxtName);
        }
    }

    STRCPY (pu1Alias, au1CxtName);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilSetContext                                           */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure is used to set the context pointer          */
/*                corresponding to the given context id in the global        */
/*                structure. This is used for the management purpose         */
/*                when trying to configure or retrive objects using MI       */
/*                specific low level routines. This function updates the     */
/*                temporary global context pointer                           */
/*                                                                           */
/* Input        : u4ContextId  : Context identifier                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3UtilSetContext (UINT4 u4ContextId)
{
    INT4                i4RetVal = SNMP_SUCCESS;

    /* If the system is operating in SI mode, the global pointer
     * is always set to the default context pointer. This function
     * should act as a dummy function in SI mode
     */
    if (u4ContextId >= OSPFV3_MAX_CONTEXTS_LIMIT)
    {
        return SNMP_FAILURE;
    }

    /* Check whether the context id exists in VCM */
    if (V3OspfVcmIsVcExist (u4ContextId) == OSIX_FAILURE)
    {
        i4RetVal = SNMP_FAILURE;
    }

    /* Check if the context is created in OSPFv3 */
    if ((i4RetVal == SNMP_SUCCESS) &&
        (gV3OsRtr.apV3OspfCxt[u4ContextId] == NULL))
    {
        i4RetVal = SNMP_FAILURE;
    }

    /* If the above condition passes, set the global context pointer */
    if (i4RetVal == SNMP_SUCCESS)
    {
        gV3OsRtr.pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4ContextId];
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilReSetContext                                         */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure is used to reset the context pointer        */
/*                This is used for the management purpose. This is called    */
/*                from MI low level routines after calling the corresponding */
/*                SI routines                                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3UtilReSetContext (VOID)
{
    /* If the system is operating in SI mode, the global pointer
     * is always set to the default context pointer. This function
     * should act as a dummy function in SI mode
     */
    gV3OsRtr.pV3OspfCxt = NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOspfGetFirstCxtId                                    */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure is used to get the first valid context id   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pu4ContextId   -  First valid context id                   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3UtilOspfGetFirstCxtId (UINT4 *pu4ContextId)
{
    UINT4               u4ContextId = 0;

    for (u4ContextId = 0; u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT;
         u4ContextId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4ContextId] != NULL)
        {
            *pu4ContextId = u4ContextId;
            return OSIX_SUCCESS;
        }
    }

    /* No context is created */
    *pu4ContextId = OSPFV3_INVALID_CXT_ID;
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOspfGetNextCxtId                                     */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure is used to get the next valid context id    */
/*                                                                           */
/* Input        : u4ContextId        -  Context Id                           */
/*                                                                           */
/* Output       : pu4NextContextId   -  Next valid context id                */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3UtilOspfGetNextCxtId (UINT4 u4ContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4TmpCxtId = 0;

    for (u4TmpCxtId = u4ContextId + 1;
         u4TmpCxtId < OSPFV3_MAX_CONTEXTS_LIMIT; u4TmpCxtId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4TmpCxtId] != NULL)
        {
            *pu4NextContextId = u4TmpCxtId;
            return OSIX_SUCCESS;
        }
    }

    *pu4NextContextId = OSPFV3_INVALID_CXT_ID;
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOspfIsValidCxtId                                     */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure is used to check the validity of context    */
/*                id                                                         */
/*                                                                           */
/* Input        : i4ContextId        -  Context Id                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3UtilOspfIsValidCxtId (INT4 i4ContextId)
{
    if ((i4ContextId < OSPFV3_DEFAULT_CXT_ID) ||
        ((UINT4) i4ContextId >= OSPFV3_MAX_CONTEXTS_LIMIT))
    {
        /* Invalid context Id passed */
        gu4V3UtilOspfIsValidCxtIdFail++;
        return OSIX_FAILURE;
    }

    if (gV3OsRtr.apV3OspfCxt[i4ContextId] == NULL)
    {
        /* Context not created */
        gu4V3UtilOspfIsValidCxtIdFail++;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOspfGetCxt                                           */
/*                                                                           */
/* Description  : This function finds and returns the pointer to the         */
/*                ospf context structure for the given ospf context ID       */
/*                                                                           */
/* Input        : u4ContextId : Ospfv3 Context Id                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to ospf context structure, if context is found     */
/*                NULL, if not found                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OspfCxt  *
V3UtilOspfGetCxt (UINT4 u4ContextId)
{
    if (V3UtilOspfIsValidCxtId ((INT4) u4ContextId) == OSIX_SUCCESS)
    {
        return (gV3OsRtr.apV3OspfCxt[u4ContextId]);
    }
    else
    {
        return NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForScalarObj                                      */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Scalar Objects                                      */
/*                                                                           */
/* Input        : i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForScalarObj (INT4 i4SetIntVal, UINT4 *pu4ObjectId,
                       UINT4 u4OidLen, UINT1 u1IsRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = OSPFV3_ZERO;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 0;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", i4SetIntVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfTable                                      */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Table                                               */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id                      */
/*                i4SetVal          - Value which should be stored.          */
/*                pSetVal           - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfTable (INT4 i4FsMIStdOspfv3ContextId, INT4 i4SetIntVal,
                       tSNMP_OCTET_STRING_TYPE * pSetVal, UINT4 *pu4ObjectId,
                       UINT4 u4OidLen, CHR1 c1Type, UINT1 u1IsRowStatus,
                       UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    if (pSetVal != NULL)
    {
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s", i4FsMIStdOspfv3ContextId,
                          pSetVal));
    }
    else
    {
        switch (c1Type)
        {
            case 'p':
            {
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p",
                                  i4FsMIStdOspfv3ContextId, i4SetIntVal));
                break;
            }
            case 'i':
            {
                SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                                  i4FsMIStdOspfv3ContextId, i4SetIntVal));
                break;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForStdOspfAreaTable                               */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 Area Table                                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id                      */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForStdOspfAreaTable (INT4 i4FsMIStdOspfv3ContextId,
                              UINT4 u4FsMIStdOspfv3AreaId, INT4 i4SetVal,
                              UINT4 *pu4ObjectId, UINT4 u4OidLen,
                              CHR1 c1Type, UINT1 u1IsRowStatus,
                              UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    switch (c1Type)
    {
        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p",
                              i4FsMIStdOspfv3ContextId,
                              u4FsMIStdOspfv3AreaId, i4SetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i",
                              i4FsMIStdOspfv3ContextId,
                              u4FsMIStdOspfv3AreaId, i4SetVal));
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForStdOspfHostTable                               */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 Host Table                                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id                      */
/*                i4FsMIStdOspfv3HostAddressType - Host Address Type         */
/*                pFsMIStdOspfv3HostAddress - Host Address                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForStdOspfHostTable (INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4FsMIStdOspfv3HostAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsMIStdOspfv3HostAddress, INT4 i4SetVal,
                              UINT4 *pu4ObjectId, UINT4 u4OidLen,
                              CHR1 c1Type, UINT1 u1IsRowStatus,
                              UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    switch (c1Type)
    {
        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %p",
                              i4FsMIStdOspfv3ContextId,
                              i4FsMIStdOspfv3HostAddressType,
                              pFsMIStdOspfv3HostAddress, i4SetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                              i4FsMIStdOspfv3ContextId,
                              i4FsMIStdOspfv3HostAddressType,
                              pFsMIStdOspfv3HostAddress, i4SetVal));
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfIfTable                                    */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Interface Table                                     */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3IfIndex - Interface Index                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfIfTable (INT4 i4FsMIStdOspfv3IfIndex, INT4 i4SetVal,
                         UINT4 *pu4ObjectId, CHR1 c1Type, UINT4 u4OidLen,
                         UINT1 u1IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 1;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    switch (c1Type)
    {
        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p",
                              i4FsMIStdOspfv3IfIndex, i4SetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i",
                              i4FsMIStdOspfv3IfIndex, i4SetVal));
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfIfAuthTable                                */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Auth Table                                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3IfIndex - Interface Index                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfIfAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                             INT4 i4FsMIStdOspfv3IfIndex, INT4 i4KeyId,
                             tSNMP_OCTET_STRING_TYPE * pSetVal, INT4 i4SetVal,
                             UINT4 *pu4ObjectId, CHR1 c1Type, UINT4 u4OidLen,
                             UINT1 u1IsRowStatus, UINT4 u4SeqNum,
                             INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    switch (c1Type)
    {
        case 's':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %s",
                              i4FsMIStdOspfv3ContextId, i4FsMIStdOspfv3IfIndex,
                              i4KeyId, pSetVal));
            break;
        }
        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i %i",
                              i4FsMIStdOspfv3ContextId, i4FsMIStdOspfv3IfIndex,
                              i4KeyId, i4SetVal));
            break;
        }
        default:
        {
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfVirtIfAuthTable                            */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Auth Table                                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3IfIndex - Interface Index                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfVirtIfAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                 UINT4 u4FsMIStdOspfv3AreaId,
                                 UINT4 u4FsMIStdOspfv3NbrId, INT4 i4KeyId,
                                 tSNMP_OCTET_STRING_TYPE * pSetVal,
                                 INT4 i4SetVal, UINT4 *pu4ObjectId, CHR1 c1Type,
                                 UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                 UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    switch (c1Type)
    {
        case 's':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, " %i %p %p %i %s",
                              i4FsMIStdOspfv3ContextId, u4FsMIStdOspfv3AreaId,
                              u4FsMIStdOspfv3NbrId, i4KeyId, pSetVal));
            break;
        }

        case 'p':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %p",
                              i4FsMIStdOspfv3ContextId, u4FsMIStdOspfv3AreaId,
                              u4FsMIStdOspfv3NbrId, i4KeyId, i4SetVal));
            break;
        }

        case 'i':
        {
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i %i",
                              i4FsMIStdOspfv3ContextId, u4FsMIStdOspfv3AreaId,
                              u4FsMIStdOspfv3NbrId, i4KeyId, u1IsRowStatus));
            break;
        }
        default:
        {
            break;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfVirtIfCryptoAuthTable                      */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for           */
/*                Ospfv3 Auth Table                                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3IfIndex - Interface Index                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfVirtIfCryptoAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       UINT4 u4FsMIStdOspfv3NbrId,
                                       INT4 i4SetVal, UINT4 *pu4ObjectId,
                                       UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                       UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i",
                      i4FsMIStdOspfv3ContextId, u4FsMIStdOspfv3AreaId,
                      u4FsMIStdOspfv3NbrId, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForStdOspfVirtIfTable                             */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 Virtual Interface Table                             */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id                      */
/*                u4FsMIStdOspfv3VirtIfAreaId - Virtual Interface Area Id.   */
/*                u4FsMIStdOspfv3VirtIfNeighbor - Virtual Interface Neighbour*/
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForStdOspfVirtIfTable (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                INT4 i4SetVal, UINT4 *pu4ObjectId,
                                UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %p %i", i4FsMIStdOspfv3ContextId,
                      u4FsMIStdOspfv3VirtIfAreaId,
                      u4FsMIStdOspfv3VirtIfNeighbor, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForStdOspfNbmaNbrTable                            */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 NbmaNbr Table                                       */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3NbmaNbrIfIndex - NbmaNbr Interface Index.   */
/*                i4FsMIStdOspfv3NbmaNbrAddressType - NbmaNbr Address Type   */
/*                pFsMIStdOspfv3NbmaNbrAddress - NbmaNbr Address             */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForStdOspfNbmaNbrTable (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                 INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdOspfv3NbmaNbrAddress, INT4 i4SetVal,
                                 UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                 UINT1 u1IsRowStatus,
                                 UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %i",
                      i4FsMIStdOspfv3NbmaNbrIfIndex,
                      i4FsMIStdOspfv3NbmaNbrAddressType,
                      pFsMIStdOspfv3NbmaNbrAddress, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForStdOspfAreaAggregateTable                      */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 Area Aggregate Table                                */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id                      */
/*                u4FsMIStdOspfv3AreaAggregateAreaID                         */
/*                i4FsMIStdOspfv3AreaAggregateAreaLsdbType                   */
/*                i4FsMIStdOspfv3AreaAggregatePrefixType                     */
/*                pFsMIStdOspfv3AreaAggregatePrefix                          */
/*                u4FsMIStdOspfv3AreaAggregatePrefixLength                   */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForStdOspfAreaAggregateTable (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaAggregateAreaID,
                                       INT4
                                       i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                       INT4
                                       i4FsMIStdOspfv3AreaAggregatePrefixType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdOspfv3AreaAggregatePrefix,
                                       UINT4
                                       u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                       INT4 i4SetVal, UINT4 *pu4ObjectId,
                                       UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                       UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 6;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %p %i %i %s %p %i",
                      i4FsMIStdOspfv3ContextId,
                      u4FsMIStdOspfv3AreaAggregateAreaID,
                      i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                      i4FsMIStdOspfv3AreaAggregatePrefixType,
                      pFsMIStdOspfv3AreaAggregatePrefix,
                      u4FsMIStdOspfv3AreaAggregatePrefixLength, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForAsExternalAggregationEffect                    */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 AsExternal Aggregate Table                          */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id.                     */
/*                i4FsMIOspfv3AsExternalAggregationNetType                   */
/*                pFsMIOspfv3AsExternalAggregationNet                        */
/*                u4FsMIOspfv3AsExternalAggregationPfxLength                 */
/*                u4FsMIOspfv3AsExternalAggregationAreaId                    */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForAsExternalAggregationEffect (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         i4FsMIOspfv3AsExternalAggregationNetType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIOspfv3AsExternalAggregationNet,
                                         UINT4
                                         u4FsMIOspfv3AsExternalAggregationPfxLength,
                                         UINT4
                                         u4FsMIOspfv3AsExternalAggregationAreaId,
                                         INT4 i4SetVal, UINT4 *pu4ObjectId,
                                         UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                         UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 5;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %p %p %i",
                      i4FsMIStdOspfv3ContextId,
                      i4FsMIOspfv3AsExternalAggregationNetType,
                      pFsMIOspfv3AsExternalAggregationNet,
                      u4FsMIOspfv3AsExternalAggregationPfxLength,
                      u4FsMIOspfv3AsExternalAggregationAreaId, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForOspfRedistRouteCfgTable                        */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 NbmaNbr Table                                       */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id.                     */
/*                i4FsMIOspfv3RedistRouteDestType                            */
/*                pFsMIOspfv3RedistRouteDest                                 */
/*                u4FsMIOspfv3RedistRoutePfxLength                           */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForOspfRedistRouteCfgTable (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4FsMIOspfv3RedistRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIOspfv3RedistRouteDest,
                                     UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                     INT4 i4SetVal, UINT4 *pu4ObjectId,
                                     UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                     UINT4 u4SeqNum, INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 4;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %p %i",
                      i4FsMIStdOspfv3ContextId, i4FsMIOspfv3RedistRouteDestType,
                      pFsMIOspfv3RedistRouteDest,
                      u4FsMIOspfv3RedistRoutePfxLength, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilMsrForDistInOutRouteMapTable                         */
/*                                                                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 NbmaNbr Table                                       */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3NbmaNbrIfIndex - NbmaNbr Interface Index.   */
/*                i4FsMIStdOspfv3NbmaNbrAddressType - NbmaNbr Address Type   */
/*                pFsMIStdOspfv3NbmaNbrAddress - NbmaNbr Address             */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForDistInOutRouteMapTable (INT4 i4FsMIStdOspfv3ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3DistInOutRouteMapName,
                                    INT4 i4FsMIOspfv3DistInOutRouteMapType,
                                    INT4 i4SetVal, UINT4 *pu4ObjectId,
                                    UINT4 u4OidLen, UINT1 u1IsRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = OSPFV3_ZERO;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    RM_GET_SEQ_NUM (&u4SeqNum);
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 3;
    SnmpNotifyInfo.i1ConfStatus = SNMP_SUCCESS;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %s %i %i", i4FsMIStdOspfv3ContextId,
                      pFsMIOspfv3DistInOutRouteMapName,
                      i4FsMIOspfv3DistInOutRouteMapType, i4SetVal));
}

/*****************************************************************************/
/* Function     : V3UtilMsrForfutOspfExtRouteTable                           */
/* Description  : This function performs Inc Msr functionality for Std       */
/*                Ospfv3 NbmaNbr Table                                       */
/*                                                                           */
/* Input        : i4FsMIStdOspfv3ContextId - Context Id.                     */
/*                i4FsMIOspfv3ExtRouteDestType                               */
/*                pFsMIOspfv3ExtRouteDest                                    */
/*                u4FsMIOspfv3ExtRoutePfxLength                              */
/*                i4FsMIOspfv3ExtRouteNextHopType                            */
/*                pFsMIOspfv3ExtRouteNextHop                                 */
/*                i4SetVal          - Value which should be stored.          */
/*                pu4ObjectId       - Object ID.                             */
/*                u4OidLen          - OID Length                             */
/*                u1IsRowStatus     - Rowstatus (TRUE/FALSE)                 */
/*                u4SeqNum          - RM sequence number                     */
/*                i1ConfStatus      - SNMP_SUCCESS / SNMP_FAILURE            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilMsrForfutOspfExtRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4FsMIOspfv3ExtRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIOspfv3ExtRouteDest,
                                  UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                  INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIOspfv3ExtRouteNextHop, INT4 i4SetVal,
                                  UINT4 *pu4ObjectId, UINT4 u4OidLen,
                                  UINT1 u1IsRowStatus, UINT4 u4SeqNum,
                                  INT1 i1ConfStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    SnmpNotifyInfo.pu4ObjectId = pu4ObjectId;
    SnmpNotifyInfo.u4OidLen = u4OidLen;
    SnmpNotifyInfo.u1RowStatus = u1IsRowStatus;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 6;
    SnmpNotifyInfo.i1ConfStatus = i1ConfStatus;
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %s %u %i %s %i",
                      i4FsMIStdOspfv3ContextId, i4FsMIOspfv3ExtRouteDestType,
                      pFsMIOspfv3ExtRouteDest, u4FsMIOspfv3ExtRoutePfxLength,
                      i4FsMIOspfv3ExtRouteNextHopType,
                      pFsMIOspfv3ExtRouteNextHop, i4SetVal));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilConstRtrLsaRBTree                                    */
/*                                                                           */
/* Description  : This procedure populates router lsa link RBTree for GR     */
/*                                                                           */
/* Input        : pLsa          : pointer to lsa                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3UtilConstRtrLsaRBTree (UINT1 *pLsa)
{
    tV3OsRtrLsaRtInfo  *pRtrLsaRtInfo = NULL;
    UINT1              *pu1CurrPtr = NULL;
    tV3OsLsHeader       lsHeader;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "FUNC: ENTRY V3UtilConstRtrLsaRBTree\r\n");
    pu1CurrPtr = pLsa;
    V3UtilExtractLsHeaderFromLbuf (pu1CurrPtr, &lsHeader);
    /* Skip the LSA header, options fields */
    pu1CurrPtr = pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;

    while ((pu1CurrPtr - pLsa) < lsHeader.u2LsaLen)
    {
        OSPFV3_RTR_LSA_LINK_ALLOC (&(pRtrLsaRtInfo));
        if (pRtrLsaRtInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        MEMSET (pRtrLsaRtInfo, 0, sizeof (tV3OsRtrLsaRtInfo));
        O3GrLsuGetLinksFromLsa (pu1CurrPtr, pRtrLsaRtInfo);

        /*Increment the Pointer for taking next link */
        pu1CurrPtr = pu1CurrPtr + OSPFV3_RTR_LSA_LINK_LEN;

        if (RBTreeAdd (gV3OsRtr.pRtrLsaCheck, (tRBElem *) pRtrLsaRtInfo) ==
            RB_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "FUNC: EXIT V3UtilConstRtrLsaRBTree\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilConstNwLsaRBTree                                     */
/*                                                                           */
/* Description  : This procedure populates network lsa link RBTree for GR    */
/*                                                                           */
/* Input        : pLsa          : pointer to lsa                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3UtilConstNwLsaRBTree (UINT1 *pLsa)
{
    tV3OsNwLsaRtInfo   *pNwLsaRtInfo = NULL;
    UINT1              *pu1CurrPtr = NULL;
    tV3OsLsHeader       lsHeader;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "FUNC: ENTRY V3UtilConstNwLsaRBTree\r\n");

    pu1CurrPtr = pLsa;
    V3UtilExtractLsHeaderFromLbuf (pu1CurrPtr, &lsHeader);

    /* Skip the LSA header, options fields */
    pu1CurrPtr = pLsa + OSPFV3_NTWR_LSA_FIXED_PORTION;

    while ((pu1CurrPtr - pLsa) < lsHeader.u2LsaLen)
    {
        OSPFV3_NW_LSA_LINK_ALLOC (&(pNwLsaRtInfo));
        if (pNwLsaRtInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        MEMSET (pNwLsaRtInfo, 0, sizeof (tV3OsNwLsaRtInfo));
        pNwLsaRtInfo->u4AttachedRtrId = (UINT4) OSPFV3_LGET4BYTE (pu1CurrPtr);

        if (RBTreeAdd (gV3OsRtr.pNwLsaCheck, (tRBElem *) pNwLsaRtInfo) ==
            RB_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "FUNC: EXIT V3UtilConstNwLsaRBTree\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilGetSecondsSinceBase                                  */
/*                                                                           */
/* Description  : This routine calculates the seconds from the obtained      */
/*                time. The seconds is calculated from the BASE year (2000)  */
/*                                                                           */
/* Input        : tUtlTm   -   Time format structure                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : The calculated time in seconds                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
V3UtilGetSecondsSinceBase (tUtlTm utlTm)
{
    UINT4               u4year = utlTm.tm_year;
    UINT4               u4Secs = OSPFV3_INIT_VAL;

    if (u4year == 0)
    {
        return 0;
    }

    --u4year;
    while (u4year >= TM_BASE_YEAR)
    {
        u4Secs += SECS_IN_YEAR (u4year);
        --u4year;
    }
    u4Secs += (utlTm.tm_yday * SECS_IN_DAY);
    u4Secs += (utlTm.tm_hour * SECS_IN_HOUR);
    u4Secs += (utlTm.tm_min * SECS_IN_MINUTE);
    u4Secs += (utlTm.tm_sec);

    return u4Secs;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilSendHello                                            */
/*                                                                           */
/* Description  : Reference : Utility                                        */
/*                This procedure sends the hello packet through the intf     */
/*                This function is called in the following cases             */
/*                -> Hello Timer expiry                                      */
/*                -> Poll Timer expiry                                       */
/*                -> Standby to active                                       */
/*                                                                           */
/* Input        : pInterface    : pointer to interface                       */
/*                u1Flag        : Poll timer / Hello timer                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3UtilSendHello (tV3OsInterface * pInterface, UINT1 u1Flag)
{
    UINT1               u1SendHelloFlag = OSIX_TRUE;
    UINT4               u4HelloTmrInterval = 0;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = (tV3OsNeighbor *) NULL;

    if (u1Flag == OSPFV3_HELLO_TIMER)
    {
        OSPFV3_TRC1 (OS_RESOURCE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Hello Tmr Exp If %s\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)));

        if ((pInterface->pArea->pV3OspfCxt->u1RestartExitReason ==
             OSPFV3_RESTART_INPROGRESS)
            && (pInterface->pArea->pV3OspfCxt->u1RestartStatus ==
                OSPFV3_RESTART_UNPLANNED)
            && (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                OSPFV3_GR_RESTART)
            && (pInterface->pArea->pV3OspfCxt->u1GraceLsaTxCount !=
                pInterface->pArea->pV3OspfCxt->u1GrLsaMaxTxCount))
        {
            /* Incase of unplanned restart, grace LSA should be sent out
               before sending the first Hello Packet */
            u1SendHelloFlag = OSIX_FALSE;
        }
        else if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF
                 (pInterface))
        {
            /* since PTOP_IF, only one neighbour will be in nbrsInIf list */
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if ((pNbr->u1NsmState >= OSPFV3_NBRS_LOADING) &&
                    (OSPFV3_IS_HELLO_SUPPRESSED_NBR (pNbr)))
                {
                    u1SendHelloFlag = OSIX_FALSE;
                }
                else if ((pInterface->u1NetworkType == OSPFV3_IF_PTOMP) &&
                         (pNbr->u1NsmState > OSPFV3_NBRS_DOWN))
                {
                    V3HpSendHello (pInterface, pNbr, OSPFV3_HELLO_TIMER);
                    if (pNbr->u4HelloSuppressPtoMp == OSIX_TRUE)
                    {
                        pNbr->u4HelloSuppressPtoMp = OSIX_FALSE;
                    }
                    u1SendHelloFlag = OSIX_FALSE;
                }
                else if (pNbr->u4HelloSuppressPtoMp == OSIX_TRUE)
                {
                    u1SendHelloFlag = OSIX_FALSE;
                }
            }
        }

        if (u1SendHelloFlag == OSIX_TRUE)
        {
            /* 
             * The second parameter is NULL indicating that the hello pkts
             * are to be sent to all neighbors on the interface 
             */
            V3HpSendHello (pInterface, NULL, OSPFV3_HELLO_TIMER);
        }
        u4HelloTmrInterval =
            OSPFV3_NO_OF_TICKS_PER_SEC * (pInterface->u2HelloInterval);

        OSPFV3_TRC1 (OSPFV3_CLI_RTMODULE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     " set OSPFV3_HELLO_TIMER  %p\n",
                     (void *) &(pInterface->helloTimer));

        V3TmrSetTimer (&(pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                       ((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ?
                        V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                      (pInterface->u2HelloInterval),
                                      OSPFV3_JITTER) : u4HelloTmrInterval));

        OSPFV3_TRC (CONTROL_PLANE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Hello Sent To All Nbrs\n");
    }
    else
    {
        OSPFV3_TRC1 (OS_RESOURCE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Poll Tmr Exp If %s\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)));

        /* 
         * The second parameter is NULL indicating that the hello pkts
         * are to be sent to all neighbors on the interface 
         */

        V3HpSendHello (pInterface, NULL, OSPFV3_POLL_TIMER);
        V3TmrSetTimer (&(pInterface->pollTimer), OSPFV3_POLL_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pInterface->u4PollInterval));

        OSPFV3_TRC (CONTROL_PLANE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Hello Sent To All Nbrs That Are DN\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilGetTraceFlagInCxt                                    */
/*                                                                           */
/* Description  : This routine returns the trace flag. The function returns  */
/*                either normal trace or extended trace                      */
/*                                                                           */
/* Input        : u4ContextId   -    Context Identifier                      */
/*                u1TraceType   -    Normal/Extended Trace                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Trace flag                                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT4
V3UtilGetTraceFlagInCxt (UINT4 u4ContextId, UINT1 u1TraceType)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4TrcVal = OSPFV3_ZERO;

    if (V3UtilOspfIsValidCxtId (u4ContextId) == OSIX_SUCCESS)
    {
        pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

        if (pV3OspfCxt == NULL)
        {
            return 0;
        }

        if (u1TraceType == OSPFV3_CXT_TRACE_TYPE)
        {
            u4TrcVal = pV3OspfCxt->u4TraceValue;
        }
        else
        {
            u4TrcVal = pV3OspfCxt->u4ExtTraceValue;
        }
    }

    return u4TrcVal;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3UtilSetLsaId                                            */
/*                                                                          */
/* Description  : This  function is called to set the LSA Id counter when   */
/*                self-originated LSA is received from the active node or   */
/*                self-originated LSA is received by the restarting router  */
/*                                                                          */
/* Input        : u4LsaId             -     Link state Id                   */
/*                u2InternalLsaType   -     Internal LSA Type               */
/*                pLsaInfo            -     pointer to LSA info             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
V3UtilSetLsaId (UINT4 u4LsaId, UINT2 u2InternalLsaType, tV3OsLsaInfo * pLsaInfo)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: V3UtilSetLsaId\r\n");

    switch (u2InternalLsaType)
    {
        case OSPFV3_ROUTER_LSA:
        {
            if (u4LsaId > pLsaInfo->pArea->u4rtrLsaLinkSize)
            {
                pLsaInfo->pArea->u4rtrLsaCounter = u4LsaId;
            }
            break;
        }
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            if (u4LsaId > pLsaInfo->pV3OspfCxt->u4InterAreaLsaCounter)
            {
                pLsaInfo->pV3OspfCxt->u4InterAreaLsaCounter = u4LsaId;
            }
            break;
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        case OSPFV3_COND_NSSA_LSA:
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (u4LsaId > pLsaInfo->pV3OspfCxt->u4AsExtLsaCounter)
            {
                pLsaInfo->pV3OspfCxt->u4AsExtLsaCounter = u4LsaId;
            }
            break;
        }
        case OSPFV3_INTRA_AREA_PREFIX_LSA:
        {
            if (u4LsaId > pLsaInfo->pArea->u4IntraAreaLsaCounter)
            {
                pLsaInfo->pArea->u4IntraAreaLsaCounter = u4LsaId;
            }
            break;
        }
        default:
        {
            break;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3UtilSetLsaId\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilApplyRMapRule                                        */
/*                                                                           */
/* Description  : This procedure applies routemap rules to route entries that*/
/*                are already dropped by rmap rules                          */
/*                                                                           */
/* Input        : pAddr        : IP address                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilOspfApplyRMapRule (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpNode = NULL;
    tOsixMsg           *pBuf = NULL;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Status = 0;
    UINT2               u2Index = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN];

    TMO_DYN_SLL_Scan (&(pV3OspfCxt->ospfV3RtTable.routesList), pLstNode,
                      pTmpNode, tTMO_SLL_NODE *)
    {
        pRtEntry =
            OSPFV3_GET_BASE_PTR (tV3OsRtEntry, nextRtEntryNode, pLstNode);

        for (; u2Index < MAX_OSPFV3_RMAP_MSGS; u2Index++)
        {
            if (gV3OsRmapMsg[u2Index] != NULL)
            {
                pBuf = gV3OsRmapMsg[u2Index];
                if (CRU_BUF_Copy_FromBufChain
                    (pBuf, (UINT1 *) &u4Status, 0,
                     sizeof (u4Status)) != sizeof (u4Status))
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    gV3OsRmapMsg[u2Index] = NULL;
                    continue;
                }

                MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN));
                if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName,
                                               sizeof (u4Status),
                                               RMAP_MAX_NAME_LEN) !=
                    RMAP_MAX_NAME_LEN)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    gV3OsRmapMsg[u2Index] = NULL;
                    continue;
                }
                if (V3OspfSendRouteMapUpdateMsg (au1RMapName, u4Status)
                    == OSIX_FAILURE)
                {
                    break;
                }
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                gV3OsRmapMsg[u2Index] = NULL;
            }
        }

        if (pRtEntry->u1IsRmapAffected == OSIX_TRUE)
        {
            pRtEntry->u1IsRmapAffected = OSIX_FALSE;
            V3RtmUpdateNewRtInCxt (pV3OspfCxt, pRtEntry);
        }

        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
            {
                OSPF3RtcRelinquishInCxt (pV3OspfCxt);
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilDeleteQueueMsg                                       */
/*                                                                           */
/* Description  : This procedure deleted all the messages in the queue       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3UtilDeleteQueueMsg (VOID)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;

    while (OsixQueRecv
           (OSPF3_Q_ID, (UINT1 *) &pOsixMsg, OSIX_DEF_MSG_LEN,
            (UINT4) 0) == OSIX_SUCCESS)
    {
        pOspfQMsg = (tV3OspfQMsg *) pOsixMsg;

        switch (pOspfQMsg->u4OspfMsgType)
        {
            case OSPFV3_PKT_ARRIVAL_EVENT:
                if (pOspfQMsg->unOspfMsgType.ospfIpIfParam.pChainBuf != NULL)
                {
                    CRU_BUF_Release_MsgBufChain (pOspfQMsg->unOspfMsgType.
                                                 ospfIpIfParam.pChainBuf,
                                                 OSPFV3_NORMAL_RELEASE);
                }
                else
                {
                    V3UtilOsMsgFree (pOspfQMsg->unOspfMsgType.ospfIpIfParam.
                                     pLinearBuf);
                }
                break;

            case OSPFV3_VCM_CHG_EVENT:
            case OSPFV3_IPV6_IF_STAT_CHG_EVENT:
            case OSPFV3_IPV6_IF_ADDR_CHG_EVENT:
                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                break;
#endif

            default:
                break;
        }

        OSPFV3_QMSG_FREE (pOspfQMsg);
    }

    while (OsixQueRecv
           (OSPF3_RM_Q_ID, (UINT1 *) &pOsixMsg, OSIX_DEF_MSG_LEN,
            (UINT4) 0) == OSIX_SUCCESS)
    {
        pOspfQMsg = (tV3OspfQMsg *) pOsixMsg;

        switch (pOspfQMsg->u4OspfMsgType)
        {
            case OSPFV3_RM_MSG_EVENT:
                if (pOspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.pData != NULL)
                {
                    if (pOspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.u1Event ==
                        RM_MESSAGE)
                    {
                        RM_FREE (pOspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.
                                 pData);
                    }
                    else if ((pOspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.
                              u1Event == RM_STANDBY_UP)
                             || (pOspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.
                                 u1Event == RM_STANDBY_DOWN))
                    {
                        V3OspfRmReleaseMemoryForMsg ((UINT1 *) pOspfQMsg->
                                                     unOspfMsgType.
                                                     ospfv3RmCtrlMsg.pData);
                    }
                }
                break;

            default:
                break;
        }

        OSPFV3_RM_QMSG_FREE (pOspfQMsg);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilDwordFromPdu                                         */
/*                                                                           */
/* Description  : This procedure converts network to host byte order         */
/*                                                                           */
/* Input        : pu1PduAddrp        : pointer to the network address        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : host byte order of pu1PduAddrp                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
V3UtilDwordFromPdu (UINT1 *pu1PduAddrp)
{
    UINT4               u4TemData = 0;
    MEMCPY (&u4TemData, pu1PduAddrp, 4);
    return (OSIX_NTOHL (*(&u4TemData)));
}

/******************************************************************************/
/*                                                                            */
/* Function     : V3UtilWordToPdu                                             */
/*                                                                            */
/* Description  : This procedure converts Host to Network (Short) Byte Order  */
/*                                                                            */
/* Input        : pu1PduAddr        : Pointer to the Word                     */
/*            : u4Value        : Host Value to be stored as Word         */
/*                                                                            */
/* Output       : Network Byte Order value                                    */
/*                                                                            */
/* Returns      : VOID                                                        */
/*                                                                            */
/*****************************************************************************/
PUBLIC VOID
V3UtilWordToPdu (UINT1 *pu1PduAddrp, UINT2 u2Value)
{
    UINT2               u2TemData = 0;
    u2TemData = OSIX_HTONS (u2Value);
    MEMCPY (pu1PduAddrp, &u2TemData, 2);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilDwordToPdu                                           */
/*                                                                           */
/* Description  : This procedure converts Host to Network (Long) Byte Order  */
/*                                                                           */
/* Input        : pu1PduAddr        : Pointer to the DoubleWord              */
/*            : u4Value        : Host Value to be stored as DoubleWord  */
/*                                                                           */
/* Output       : Network Byte Order value                                   */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3UtilDwordToPdu (UINT1 *pu1PduAddr, UINT4 u4Value)
{
    UINT4               u4TemData = 0;
    u4TemData = OSIX_HTONL (u4Value);
    MEMCPY (pu1PduAddr, &u4TemData, 4);
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3GetUnnumberedInterface                                   */
/*                                                                           */
/* Description  : This procedure returns ipv6 structure details of           */
/*                associated inferface of unnumbered nterface                */
/*                                                                           */
/*                                                                           */
/* Input        : u4IfIndex    : INterface index                             */
/*             : pNetIpv6IfInfo : pointer to Netipv6 structure               */
/*                                                                           */
/* Output       : Returns a pointer to Netipv6 structure of interface        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3GetUnnumberedInterface (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    INT4                i4RetVal = NETIPV6_FAILURE;

    i4RetVal = NetIpv6GetIfInfo (u4IfIndex, pNetIpv6IfInfo);
    return i4RetVal;

}

#ifdef OSPF3_BCMXNP_HELLO_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : V3NPSendHelloPktInCxt                                      */
/*                                                                           */
/* Description  : This procedure sends hello packet to NP                    */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3NPSendHelloPktInCxt (UINT1 *pPkt, UINT2 u2PktLen,
                       tV3OsInterface * pInterface, tIp6Addr * pDestIp6Addr)
{
    tEnetV2Header       EnetV2Header;
    UINT1              *pu1RawPkt = NULL;
    tIp6Hdr             ip6Hdr;
    UINT1               u1Type;
    UINT1               u1HopLimit = 0;
    tIp6Addr            srcIp6Addr;
    tIp6Addr            dstIp6Addr;
    UINT2               u2CheckSum = 0;
    UINT4               u4OutIfIndex = 0, u4Length = 0;
    UINT1              *pOspfPkt = NULL;
    UINT2               u2ATLen = 0;
    UINT1               u2ATStat = OSPFV3_SUCCESS;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    UINT2               VlanId = 0;
    tPortList          *pTagPorts = NULL;
    tPortList          *pUnTagPorts = NULL;
    tMacAddr            DestAddr;
    tCfaVlanInfo        VlanInfo;
    UINT4               u4OutPort = 0;
    INT4                i4RetVal = 0;
    BOOL1               bIsTag;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1PortFlag;
    UINT1               u1EgrTpidType = VLAN_PORT_EGRESS_PORTBASED;
    INT4                i4RetStat = CFA_SUCCESS;

    MEMSET (&ip6Hdr, 0, sizeof (tIp6Hdr));
    MEMSET (gau1TxPktBuf, 0,
            OSPFV3_MAX_HELLO_PKT_LEN + CFA_ENET_V2_HEADER_SIZE +
            IPV6_HEADER_LEN);

    if (pInterface->u1NetworkType >= OSPFV3_MAX_IF_TYPE)
    {
        return OSIX_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "FUNC: V3NPSendHelloPktInCxt\n");
    pu1RawPkt = gau1TxPktBuf;

    /* Form Ethernet header */

    MEMSET (&EnetV2Header, ZERO, sizeof (tEnetV2Header));
    EnetV2Header.u2LenOrType = OSIX_HTONS (CFA_ENET_IPV6);
    CfaGetSysMacAddress (EnetV2Header.au1SrcAddr);
    EnetV2Header.au1DstAddr[0] = MCAST_MAC_ADDR_OCTET_1;
    EnetV2Header.au1DstAddr[1] = MCAST_MAC_ADDR_OCTET_2;

    if ((V3UtilIp6AddrComp (pDestIp6Addr, &gV3OsAllSpfRtrs) == OSPFV3_EQUAL))
    {
        EnetV2Header.au1DstAddr[5] = 5;
    }
    else if (V3UtilIp6AddrComp (pDestIp6Addr, &gV3OsAllDRtrs) == OSPFV3_EQUAL)
    {
        EnetV2Header.au1DstAddr[5] = 6;
    }

    MEMCPY (pu1RawPkt, &EnetV2Header, CFA_ENET_V2_HEADER_SIZE);

    /* Calculate the OSPFv3 Pkt Pointer */
    pOspfPkt = pPkt + IPV6_HEADER_LEN;

    OSPFV3_TRC4 (OSPFV3_PPP_TRC | CONTROL_PLANE_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Hello Packet to be txed If_Index: %d"
                 " Addr: %s  Network Type: %s"
                 " Len: %d\n",
                 pInterface->u4InterfaceId,
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 gau1Os3DbgIfType[pInterface->u1NetworkType], u2PktLen);

    if ((pInterface->operStatus == OSPFV3_DISABLED) ||
        (pInterface->admnStatus == OSPFV3_DISABLED) ||
        (pInterface->bPassive == OSIX_TRUE))
    {

        /* If the oper status or the admin status is disabled then the
           buffer is released */
        return OSPFV3_FAILURE;
    }
    OSPFV3_BUFFER_GET_1_BYTE (pOspfPkt, OSPFV3_TYPE_OFFSET, u1Type);

    /* Setting Source IPv6 Addr */
    OSPFV3_IP6_ADDR_COPY (srcIp6Addr, pInterface->ifIp6Addr);

    /* Setting Destination IPv6 Addr */
    if (pInterface->u1NetworkType == OSPFV3_IF_PTOP)
    {
        OSPFV3_IP6_ADDR_COPY (dstIp6Addr, gV3OsAllSpfRtrs);
    }
    else
    {
        OSPFV3_IP6_ADDR_COPY (dstIp6Addr, *pDestIp6Addr);
    }

    /* Checking NULL source and destination Addr */
    if ((V3UtilIp6AddrComp (&srcIp6Addr, &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
        || (V3UtilIp6AddrComp (&dstIp6Addr, &gV3OsNullIp6Addr) == OSPFV3_EQUAL))
    {
        return OSPFV3_FAILURE;
    }

    u2ATStat =
        V3AuthSendPkt (pOspfPkt, u2PktLen, pInterface, dstIp6Addr, &u2ATLen);
    if (u2ATStat != OSPFV3_SUCCESS)
    {
        return OSPFV3_FAILURE;
    }

    /* Calculate ospfv3 Checksum */
    if (u2ATLen == 0)
    {
        u2CheckSum =
            V3UtilIp6Chksum (pOspfPkt, u2PktLen, &srcIp6Addr, &dstIp6Addr);

        OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt, OSPFV3_CHKSUM_OFFSET,
                                     u2CheckSum);
    }
    if ((u1Type >= OSPFV3_HELLO_PKT) && (u1Type <= OSPFV3_LSA_ACK_PKT))
    {
        OSPFV3_TRC1 (CONTROL_PLANE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Txmitting %s\n", gau1Os3DbgPktType[u1Type]);
    }

    OSPFV3_OUT_PKT_DUMP
        (pInterface->pArea->pV3OspfCxt->u4ContextId, pOspfPkt, u2PktLen);
    /* Dump the Outgoing Packet */

    /* Calculate the output ifIndex */
    if (OSPFV3_IS_VIRTUAL_IFACE (pInterface))
    {
        u4OutIfIndex = pInterface->u4OutIfIndex;
        u1HopLimit = IP6_DEF_HOP_LIMIT;
    }
    else
    {
        u4OutIfIndex = pInterface->u4InterfaceId;
        u1HopLimit = 1;
    }

    u4Length = (UINT4) (u2PktLen + u2ATLen);

/* Copy IPv6 Header*/

    ip6Hdr.u4Head = IP6_DEFALUT_HDR_VALUE | ((CLI_PING_DEFAULT_TOS) << 20);
    ip6Hdr.u2Len = (UINT2) (CRU_HTONS ((UINT2) (u4Length)));
    ip6Hdr.u1Nh = OSPFV3_PROTO;
    ip6Hdr.u1Hlim = u1HopLimit;
    OSPFV3_IP6ADDR_CPY (&(ip6Hdr.srcAddr), &srcIp6Addr);
    OSPFV3_IP6ADDR_CPY (&(ip6Hdr.dstAddr), &dstIp6Addr);

    MEMCPY (pu1RawPkt + CFA_ENET_V2_HEADER_SIZE, &ip6Hdr, sizeof (tIp6Hdr));

/*Copy Ospf PDU*/

    MEMCPY (pu1RawPkt + (CFA_ENET_V2_HEADER_SIZE + sizeof (tIp6Hdr)), pOspfPkt,
            u4Length);

    u4Length = CFA_ENET_V2_HEADER_SIZE + IPV6_HEADER_LEN + u2PktLen + u2ATLen;

    CfaGetIfBridgedIfaceStatusExtended (u4OutIfIndex, &u1BridgedIfaceStatus);
    {
        /*Router Port */
        if ((u1BridgedIfaceStatus != CFA_ENABLED)
            && (CFA_IS_UPLINK_PORT (u4OutIfIndex)))
        {
            if (CfaNpPortWrite (pu1RawPkt, u4OutIfIndex, u4Length) ==
                CFA_FAILURE)
            {
                return OSPFV3_FAILURE;
            }
        }
        /* Vlan port */
        else
        {
            CfaGetIfIvrVlanIdExtended (u4OutIfIndex, &VlanId);

#ifdef MBSM_WANTED
            /* If the packet belongs to STACK IVR, then send the
             * packet via stack port using ATP*/
            if (VlanId == CFA_DEFAULT_STACK_VLAN_ID)
            {
                if (CfaMbsmNpTxOnStackInterface (pOspfPkt, u4Length) ==
                    FNP_FAILURE)
                {
                    return OSPFV3_FAILURE;
                }
                return OSPFV3_SUCCESS;
            }
#endif
            pTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pTagPorts == NULL)
            {
                return OSPFV3_FAILURE;
            }
            pUnTagPorts = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pUnTagPorts == NULL)
            {
                FsUtilReleaseBitList ((UINT1 *) pTagPorts);
                return OSPFV3_FAILURE;
            }

            VlanInfo.unPortInfo.TxPorts.pTagPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (VlanInfo.unPortInfo.TxPorts.pTagPorts == NULL)
            {
                FsUtilReleaseBitList ((UINT1 *) pTagPorts);
                FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
                return OSPFV3_FAILURE;
            }

            VlanInfo.unPortInfo.TxPorts.pUnTagPorts =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (VlanInfo.unPortInfo.TxPorts.pUnTagPorts == NULL)
            {
                FsUtilReleaseBitList ((UINT1 *) pTagPorts);
                FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
                FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                                 pTagPorts));
                return OSPFV3_FAILURE;
            }
            MEMSET (*pTagPorts, 0, sizeof (tPortList));
            MEMSET (*pUnTagPorts, 0, sizeof (tPortList));
            VlanInfo.u2VlanId = VlanId;
            MEMCPY (DestAddr, EnetV2Header.au1DstAddr, CFA_ENET_ADDR_LEN);
            i4RetVal =
                VlanIvrGetHelloTxPortOrPortListInCxt (pInterface->pArea->
                                                      pV3OspfCxt->u4ContextId,
                                                      DestAddr, VlanId, FALSE,
                                                      &u4OutPort, &bIsTag,
                                                      *pTagPorts, *pUnTagPorts);

            for (u2ByteInd = 0; u2ByteInd < BRG_MAX_PHY_PLUS_LOG_PORTS_EXT;
                 u2ByteInd++)
            {
                if ((*pUnTagPorts)[u2ByteInd] == 0)
                {
                    continue;
                }

                u1PortFlag = (*pUnTagPorts)[u2ByteInd];

                for (u2BitIndex = 0;
                     ((u2BitIndex < BITS_PER_BYTE)
                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & 0x80) != 0)
                    {
                        VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                        VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                            (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        u4OutPort = VlanInfo.unPortInfo.TxUcastPort.u2TxPort;

#ifdef VLAN_WANTED
                        VlanGetPortEtherTypeExtended (u4OutPort, &u2EtherType);
#endif /*Trident2 L3 Porting */
                        VlanInfo.unPortInfo.TxUcastPort.
                            u2EtherType = u2EtherType;

                        VlanInfo.unPortInfo.TxUcastPort.u1Tag = CFA_NP_UNTAGGED;

                        CfaHwL3VlanIntfWrite (pu1RawPkt, u4Length, VlanInfo);
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
            for (u2ByteInd = 0; u2ByteInd < BRG_MAX_PHY_PLUS_LOG_PORTS_EXT;
                 u2ByteInd++)
            {
                if ((*pTagPorts)[u2ByteInd] == 0)
                {
                    continue;
                }

                u1PortFlag = (*pTagPorts)[u2ByteInd];

                for (u2BitIndex = 0;
                     ((u2BitIndex < BITS_PER_BYTE)
                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & 0x80) != 0)
                    {
                        VlanInfo.u2PktType = CFA_NP_KNOWN_UCAST_PKT;

                        VlanInfo.unPortInfo.TxUcastPort.u2TxPort =
                            (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                     u2BitIndex + 1);

                        u4OutPort = VlanInfo.unPortInfo.TxUcastPort.u2TxPort;

#ifdef VLAN_WANTED
                        VlanGetPortEtherTypeExtended (u4OutPort, &u2EtherType);
#endif /*Trident2 L3 Porting */
                        VlanInfo.unPortInfo.TxUcastPort.
                            u2EtherType = u2EtherType;

                        VlanInfo.unPortInfo.TxUcastPort.u1Tag = CFA_NP_TAGGED;

                        CfaHwL3VlanIntfWrite (pu1RawPkt, u4Length, VlanInfo);
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
            MEMSET (VlanInfo.unPortInfo.TxPorts.pTagPorts, 0,
                    sizeof (tPortList));
            VlanInfo.u2PktType = CFA_NP_UNKNOWN_UCAST_PKT;
            /*
             * In 802.1ad Bridges, for untagged ports, the tag ethertype
             * need not be filled in based on the port ethertype.
             * Hence calling the API for packet transmission on L3
             * interfaces directly.
             */
            if (CfaHwL3VlanIntfWrite
                (pu1RawPkt, u4Length, VlanInfo) != FNP_SUCCESS)
            {
                i4RetStat = CFA_FAILURE;
            }

            FsUtilReleaseBitList ((UINT1 *) pTagPorts);
            FsUtilReleaseBitList ((UINT1 *) pUnTagPorts);
            FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                             pTagPorts));
            FsUtilReleaseBitList ((UINT1 *) (VlanInfo.unPortInfo.TxPorts.
                                             pUnTagPorts));
        }

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NPSendHelloPktInCxt\n");
    return OSPFV3_SUCCESS;

}
#endif

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfIsLowPriorityMessage                       */
/*                                                                           */
/*     DESCRIPTION      : This function checks the priority of the           */
/*                        Ospf message type                                  */
/*                                                                           */
/*     INPUT            : u4MsgType    - Ospf message type                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSPFV3_SUCCESS if message type is Low priority          */
/*                        OSPFV3_FAILURE if message type is High priority        */
/*****************************************************************************/
PUBLIC INT4
V3OspfIsLowPriorityMessage (UINT4 u4MsgType)
{
    if ((u4MsgType == OSPFV3_IPV6_IF_STAT_CHG_EVENT)
        || (u4MsgType == OSPFV3_VCM_CHG_EVENT))
    {
        /* this is a low priority message */
        return OSPFV3_SUCCESS;
    }
    /* Note : If priority based Q support is not needed
     *        then classify all the message as High priority Message
     */
    return OSPFV3_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : UtilOspfIsLowPriorityOspfPkt                       */
/*                                                                           */
/*     DESCRIPTION      : This function checks the priority of the           */
/*                        Ospf packets                                       */
/*                                                                           */
/*     INPUT            : u1PakType    - Ospf packet type                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : OSPFV3_SUCCESS if Ospf packet type is Low priority      */
/*                        OSPFV3_FAILURE if Ospf packet type is High priority    */
/*****************************************************************************/
PUBLIC INT4
V3OspfIsLowPriorityOspfPkt (UINT1 u1OspfPktType)
{
    if ((u1OspfPktType == OSPFV3_DD_PKT) ||
        (u1OspfPktType == OSPFV3_LS_UPDATE_PKT)
        || (u1OspfPktType == OSPFV3_LSA_REQ_PKT))
    {
        /* this is a low priority packet */
        return OSPFV3_SUCCESS;
    }
    /* Note : If priority based Q support is not needed
     *        then classify all the packets as High priority packets
     */
    return OSPFV3_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouterIdPermanence
 Input       :  The Indices

                The Object
                retValFutOspfv3RouterIdPermanence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
VOID
UtilGetOspfv3RouterIdPermanence (INT4 *pi4RetValFutOspfv3RouterIdPermanence,
                                 UINT4 u4ContextId)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        return;
    }

    *pi4RetValFutOspfv3RouterIdPermanence = pV3OspfCxt->u1RouterIdStatus;
    return;
}

/*----------------------------------------------------------------------*/
/*                     End of the file o3util.c                         */
/*----------------------------------------------------------------------*/
