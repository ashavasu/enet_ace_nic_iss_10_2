/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3redblk.c,v 1.8 2017/12/26 13:34:28 siva Exp $
 *
 * Description: 
 ********************************************************************/
#include "o3inc.h"

/* Prototypes */

PRIVATE VOID O3RedBlkSendBulkTail PROTO ((VOID));

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkSendBulkUpdate                                    */
/*                                                                          */
/* Description  : This function is called when active node gets a bulk      */
/*                request from the standby node. This functions resets all  */
/*                temporary data used in the previous bulk update process   */
/*                and send the bulk updates                                 */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedBlkSendBulkUpdate (VOID)
{
    tV3OsRedBulkUpdAbortTrapInfo BulkUpdAbortTrapInfo;
    INT4                i4SendUpdate = OSIX_SUCCESS;
    INT4                i4SendSubBulkEvent = OSIX_FAILURE;
    INT4                i4RouterIdStatus = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkSendBulkUpdate\r\n");

    MEMSET (&BulkUpdAbortTrapInfo, 0, sizeof (tV3OsRedBulkUpdAbortTrapInfo));

    /* If none of the instance is disabled, then send the bulk tail message */
    if (gV3OsRtr.u4ActiveCxtCount == 0)
    {
        O3RedBlkSendBulkTail ();
        return;
    }

    /* Send a trap to indicate that the bulk update is in progress */
    OSPFV3_RTR_ID_COPY (&BulkUpdAbortTrapInfo.rtrId,
                        &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->rtrId);
    BulkUpdAbortTrapInfo.i4DynamicBulkUpdStatus =
        OSPFV3_RED_DYN_BULK_IN_PROGRESS;
    BulkUpdAbortTrapInfo.u1BulkUpdateAbortReason = RM_NONE;
    O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                           OSPFV3_RED_BULK_UPD_ABORT_TRAP,
                           &BulkUpdAbortTrapInfo);

    /* Clear the temporary information and start the bulk update process
     * from the beginning
     */
    O3RedUtlResetHsLastInfo ();

    /* Update the bulk update start time */
    gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime = OsixGetSysUpTime ();

    /* Synchronize the router id if routerid is selected dynmically */

    UtilGetOspfv3RouterIdPermanence (&i4RouterIdStatus, OSPFV3_DEFAULT_CXT_ID);
    if (i4RouterIdStatus == OSPFV3_ROUTERID_DYNAMIC)
    {
        i4SendUpdate = O3RedSendRouterIdInfo (&i4SendSubBulkEvent);
    }
    /* Synchronize the last received hello packets from each neighbor
     * to the the adjacency
     */

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        i4SendUpdate = O3RedAdjSendBulkNbrInfo (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Synchronize the hello and nbr state in virtual links */
        i4SendUpdate = O3RedAdjSendBulkVirtNbrInfo (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Synchronize the LSA present in the database */
        i4SendUpdate = O3RedLsuSendLsu (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Synchronize the timers */
        i4SendUpdate = O3RedTmrSendTimerInfo (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Bulk update is completed. Send the bulk tail message */
        O3RedBlkSendBulkTail ();
    }

    if (i4SendSubBulkEvent == OSIX_SUCCESS)
    {
        /* Send out the OSPFV3_RM_SUB_BULK_EVENT,
         * to relinquish the bulk update process */
        OsixEvtSend (OSPF3_TASK_ID, OSPFV3_RM_SUB_BULK_EVENT);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkSendBulkUpdate\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkSendSubBulkUpdate                                 */
/*                                                                          */
/* Description  : This function is called when active node gets a sub       */
/*                bulk update event to itself.                              */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedBlkSendSubBulkUpdate (VOID)
{
    INT4                i4SendUpdate = OSIX_SUCCESS;
    INT4                i4SendSubBulkEvent = OSIX_FAILURE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkSendSubBulkUpdate\r\n");

    /* If the protocol is disabled, then send the bulk tail message */
    if ((i4SendUpdate == OSIX_SUCCESS) &&
        (gV3OsRtr.u4ActiveCxtCount == OSPFV3_INIT_VAL))
    {
        O3RedBlkSendBulkTail ();
        i4SendUpdate = OSIX_SUCCESS;
    }

    /* Update the bulk update start time */
    gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime = OsixGetSysUpTime ();

    if ((i4SendUpdate == OSIX_SUCCESS) &&
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus == OSPFV3_BULK_UPDT_NBR))
    {
        /* Synchronize the last received hello packets from each neighbor
         * and Synchronize the neighbor state
         */
        i4SendUpdate = O3RedAdjSendBulkNbrInfo (&i4SendSubBulkEvent);
    }
    if ((i4SendUpdate == OSIX_SUCCESS) &&
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
         OSPFV3_BULK_UPDT_VIRT_NBR))
    {
        /* Synchronize the last received hello packets from each virtual neighbor
         * and Synchronize the neighbor state
         */
        i4SendUpdate = O3RedAdjSendBulkVirtNbrInfo (&i4SendSubBulkEvent);
    }
    if ((i4SendUpdate == OSIX_SUCCESS) &&
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus == OSPFV3_BULK_UPDT_LSU))
    {
        /* Synchronize the LSA present in the database */
        i4SendUpdate = O3RedLsuSendLsu (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Synchronize the timers */
        i4SendUpdate = O3RedTmrSendTimerInfo (&i4SendSubBulkEvent);
    }

    if (i4SendUpdate == OSIX_SUCCESS)
    {
        /* Bulk update is completed. Send the bulk tail message */
        O3RedBlkSendBulkTail ();
    }

    if (i4SendSubBulkEvent == OSIX_SUCCESS)
    {
        /* Send out the OSPFV3_RM_SUB_BULK_EVENT,
         * to relinquish the bulk update process */
        OsixEvtSend (OSPF3_TASK_ID, OSPFV3_RM_SUB_BULK_EVENT);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkSendSubBulkUpdate\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkSendBulkRequest                                   */
/*                                                                          */
/* Description  : This function is called when standby node receives        */
/*                initiate bulk update. This function creates and sends     */
/*                bulk request message to the active node                   */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedBlkSendBulkRequest (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDT_REQ_MSG;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkSendBulkRequest\r\n");

    u2DataLen = (UINT2) OSPFV3_BULK_REQ_LEN;

    pMsg = RM_ALLOC_TX_BUF (u2DataLen);
#ifdef L2RED_WANTED
    if (pMsg == NULL)
    {
        /* Allocation failure */
        O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
    }
    else
    {
        /* Add the byte to the message and send to RM module */
        OSPFV3_RED_PUT_1_BYTE (pMsg, u2OffSet, u1MsgType);

        if (V3OspfEnqMsgToRm (pMsg, u2DataLen) == OSIX_FAILURE)
        {
            /* Enqueue message to RM failed */
            O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
        }
    }
#else
    UNUSED_PARAM (u2OffSet);
    UNUSED_PARAM (u1MsgType);
    O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkSendBulkRequest\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkSendBulkTail                                      */
/*                                                                          */
/* Description  : This function is called when active node has completed    */
/*                sending all the bulk updates or it has no bulk updates    */
/*                to send to the standby. This function constucts and       */
/*                sends the bulk tail message                               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedBlkSendBulkTail (VOID)
{
    tV3OsRedBulkUpdAbortTrapInfo BulkUpdAbortTrapInfo;
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDT_TAIL_MSG;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkSendBulkTail\r\n");

    MEMSET (&BulkUpdAbortTrapInfo, 0, sizeof (tV3OsRedBulkUpdAbortTrapInfo));

    u2DataLen = (UINT2) OSPFV3_BULK_TAIL_LEN;

    pMsg = RM_ALLOC_TX_BUF (u2DataLen);
#ifdef L2RED_WANTED
    if (pMsg == NULL)
    {
        /* Allocation failure */
        O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
    }
    else
    {
        /* Add the byte to the message and send to RM module */
        OSPFV3_RED_PUT_1_BYTE (pMsg, u2OffSet, u1MsgType);

        /* Set the bulk update status as completed and set the bit in RM
         * structure
         */
        gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_COMPLETED;
        V3OspfRmSetBulkUpdateStatus ();

        if (V3OspfEnqMsgToRm (pMsg, u2DataLen) == OSIX_FAILURE)
        {
            /* Enqueue message to RM failed */
            O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
        }
        else
        {
            /* Send a trap to indicate that the bulk update is in progress */
            OSPFV3_RTR_ID_COPY
                (&BulkUpdAbortTrapInfo.rtrId,
                 &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->rtrId);
            BulkUpdAbortTrapInfo.i4DynamicBulkUpdStatus =
                OSPFV3_RED_DYN_BULK_COMPLETED;
            BulkUpdAbortTrapInfo.u1BulkUpdateAbortReason = RM_NONE;
            O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                                   OSPFV3_RED_BULK_UPD_ABORT_TRAP,
                                   &BulkUpdAbortTrapInfo);
        }
    }
#else
    UNUSED_PARAM (u1MsgType);
    UNUSED_PARAM (u2OffSet);
    O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkSendBulkTail\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkProcessBulkUpdt                                   */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update message. This function gets the message type and   */
/*                calls the function to process each sub bulk updates       */
/*                                                                          */
/* Input        : pRmMsg      -   pointer to RM message                     */
/*                u2DataLen   -   Data Length                               */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedBlkProcessBulkUpdt (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    UINT2               u2OffSet = 0;
    UINT1               u1SubMsgType = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkProcessBulkUpdt\r\n");

    /* Get the sub bulk message type Skip the Type and length of the TLV
     * and get the sub bulk update type
     */
    u2OffSet = sizeof (UINT1) + sizeof (UINT2);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1SubMsgType);

    switch (u1SubMsgType)
    {
        case OSPFV3_RED_BULK_HELLO:
        case OSPFV3_RED_BULK_NBR_STATE:
        case OSPFV3_RED_BULK_INTF:
        {
            /* Process LSU bulk update */
            O3RedAdjProcessNbrInfo (pRmMsg);
            break;
        }
        case OSPFV3_RED_BULK_LSU:
        {
            /* Process LSU bulk update */
            O3RedLsuProcessLsuBlkUpdt (pRmMsg, u2DataLen);
            break;
        }
        case OSPFV3_RED_BULK_DB_TIMER:
        case OSPFV3_RED_BULK_STAB_TIMER:
        {
            /* Process timer info */
            O3RedTmrProcessTimerInfo (pRmMsg);
            break;
        }
        case OSPFV3_RED_BULK_ROUTER_ID:
        {
            /*Process the router id info */
            O3RedProcessRouterIdInfo (pRmMsg);
            break;
        }
        default:
        {
            break;
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkProcessBulkUpdt\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedBlkProcessBulkTail                                   */
/*                                                                          */
/* Description  : This function is called when the standby node receives    */
/*                the bulk tail message. This function starts the route     */
/*                calculation timer and sends an event to RM                */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedBlkProcessBulkTail (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4PrevContextId = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedBlkProcessBulkTail\r\n");
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_COMPLETED;

    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

            if ((pV3OspfCxt == NULL) ||
                (pV3OspfCxt->admnStat == OSPFV3_DISABLED))
            {
                continue;
            }

            V3RtcSetRtTimerInCxt (pV3OspfCxt);

        }
        while (V3UtilOspfGetNextCxtId (u4PrevContextId, &u4ContextId)
               == OSIX_SUCCESS);
    }

    O3RedRmSendEvent (RM_PROTOCOL_BULK_UPDT_COMPLETION, RM_NONE);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedBlkProcessBulkTail\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  O3RedSendRouterIdInfo                                     */
/*                                                                           */
/* Description  : This function constructs and Sends out the bulk update     */
/*                for router id                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pi4SendSubBulkEvent - indicates whether SUBBULK UDATE EVENT*/
/*                                      needs to be sent out or not          */
/*                                      Will be set to OSIX_SUCCESS, when the*/
/*                                      SUBBULK UDATE EVENT needs to sent    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedSendRouterIdInfo (INT4 *pi4SendSubBulkEvent)
{

    tV3OspfCxt         *pV3OspfCxt =
        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID];
    tV3OsRouterId       RtrId;
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT4               u4PktLen = OSPFV3_ZERO;
    UINT1               u1MsgType = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "FUNC: O3RedSendRouterIdInfo\r\n");

    MEMSET (RtrId, OSPFV3_ZERO, OSPFV3_RTR_ID_LEN);
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_ROUTERID;
    *pi4SendSubBulkEvent = OSIX_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return OSIX_FAILURE;
    }

    OSPFV3_RTR_ID_COPY (&RtrId, &pV3OspfCxt->rtrId);

    /* Allocate a New Buffer and Initialize the Buffer */
    if ((pBuf = RM_ALLOC_TX_BUF (OSPFV3_ROUTER_ID_INFO + OSPFV3_ONE)) == NULL)
    {

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "O3RedAdjAllocateBulkUpdatePkt: Cannot "
                      "allocate buffer for sending Nbr Bulk Update"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.
                        apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "Cannot allocate buffer for sending Hello to "
                        "Standby");
        O3RedRmSendEvent ((UINT4) RM_BULK_UPDT_ABORT, (UINT4) RM_MEMALLOC_FAIL);
        pBuf = NULL;
        return OSIX_FAILURE;
    }
    /* Initialize offset */
    u4Offset = OSPFV3_ZERO;
    OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, RM_BULK_UPDATE_MSG);

    /* Increment offset to pass the total packet length which
     * will be filled later 
     */

    u4Offset += sizeof (UINT2);
    u4PktLen = OSPFV3_RED_MSG_HDR_LEN;

    u1MsgType = (UINT1) OSPFV3_RED_BULK_ROUTER_ID;
    OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_N_BYTE (pBuf, RtrId, u4Offset, sizeof (RtrId));
    OSPFV3_RED_PUT_4_BYTE (pBuf, u4Offset, pV3OspfCxt->u4ContextId);

    u4PktLen = u4Offset;

    /* Send out the constructed packet */
    if (O3RedUtlSendBulkUpdate (pBuf, u4PktLen) == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "FUNC: O3RedSendRouterIdInfo \r\n");
        return OSIX_FAILURE;
    }
    /* Router id  Bulk update is completed */
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_NBR;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "FUNC: O3RedSendRouterIdInfo \r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedProcessRouterIdInfo                                   */
/*                                                                           */
/* Description  : This function process the router id info                   */
/*                to syncronize with the standby node.                       */
/*                                                                           */
/* Input        : pRmMsg     -    RM Message                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedProcessRouterIdInfo (tRmMsg * pRmMsg)
{
    UINT1               u1MsgType = 0;    /* Sub message bulk type */
    UINT4               u4Offset = 0;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2PktLen = 0;
    tV3OsRouterId       rtrId;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: O3RedProcessRouterIdInfo \r\n");

    MEMSET (rtrId, OSPFV3_ZERO, OSPFV3_RTR_ID_LEN);
    /* Skip the RM header length 3 */
    u4Offset = sizeof (UINT1);

    OSPFV3_RED_GET_2_BYTE (pRmMsg, u4Offset, u2PktLen);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_GET_N_BYTE (pRmMsg, rtrId, u4Offset, sizeof (rtrId));
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, u4ContextId);

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                         gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                         u4ContextId, "Context : %d "
                         "not available\r\n", u4ContextId);
        return OSIX_FAILURE;
    }

    if (V3UtilRtrIdComp (pV3OspfCxt->rtrId, rtrId) != OSPFV3_EQUAL)
    {
        MEMCPY (pV3OspfCxt->rtrId, rtrId, OSPFV3_RTR_ID_LEN);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedProcessRouterIdInfo \r\n");
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redblk.c                           */
/*-----------------------------------------------------------------------*/
