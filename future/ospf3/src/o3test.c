/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3test.c,v 1.37 2018/01/25 10:04:16 siva Exp $
 *
 * Description:This file contains test Routines for MIB Objects.
 *
 *******************************************************************/

#include "o3inc.h"
#include "snmccons.h"
#include "ospf3lw.h"
#include "fsos3lw.h"
#include "fsmisos3cli.h"
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3RouterId
 Input       :  The Indices

                The Object 
                testValOspfv3RouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3RouterId (UINT4 *pu4ErrorCode, UINT4 u4TestValOspfv3RouterId)
{
    tV3OsRouterId       rtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test Routine For Ospfv3RouterId %x\n",
                 u4TestValOspfv3RouterId);

    OSPFV3_BUFFER_DWTOPDU (rtrId, u4TestValOspfv3RouterId);

    if (V3UtilRtrIdComp (rtrId, OSPFV3_NULL_RTRID) != OSPFV3_EQUAL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3RouterId Success\n");
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3RouterId Failure\n");
    CLI_SET_ERR (OSPFV3_CLI_NO_ACT_INT);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AdminStat
 Input       :  The Indices

                The Object 
                testValOspfv3AdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AdminStat (UINT4 *pu4ErrorCode, INT4 i4TestValOspfv3AdminStat)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test Routine For Ospfv3AdminStat %d\n",
                 i4TestValOspfv3AdminStat);
    if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValOspfv3AdminStat))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3AdminStat Failure\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3ASBdrRtrStatus
 Input       :  The Indices

                The Object 
                testValOspfv3ASBdrRtrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3ASBdrRtrStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValOspfv3ASBdrRtrStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test Routine For Ospfv3ASBdrRtrStatus %d\n",
                 i4TestValOspfv3ASBdrRtrStatus);

    if (OSPFV3_IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfv3ASBdrRtrStatus))
    {
        i4TestValOspfv3ASBdrRtrStatus =
            OSPFV3_MAP_FROM_TRUTH_VALUE (i4TestValOspfv3ASBdrRtrStatus);

        if (((UINT1) i4TestValOspfv3ASBdrRtrStatus == OSIX_TRUE)
            && (OSPFV3_IS_INTERNAL_RTR_IN_STUB_AREA (pV3OspfCxt)))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3ASBdrRtrStatus Failure,"
                        " Router in STUB Area\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_ERR_ASBR);
            return SNMP_FAILURE;
        }
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3ASBdrRtrStatus Success\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "TruthValue is not valid \n");

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3ASBdrRtrStatus Failure\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3ExtAreaLsdbLimit
 Input       :  The Indices

                The Object 
                testValOspfv3ExtAreaLsdbLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3ExtAreaLsdbLimit (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValOspfv3ExtAreaLsdbLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test Routine For Ospfv3ExtLsdbLimit %d\n",
                 i4TestValOspfv3ExtAreaLsdbLimit);

    if (OSPFV3_IS_VALID_EXT_LSDB_LIMIT (i4TestValOspfv3ExtAreaLsdbLimit))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3ExtAreaLsdbLimit Success\n");
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3ExtAreaLsdbLimit Failure\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3MulticastExtensions
 Input       :  The Indices

                The Object 
                testValOspfv3MulticastExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3MulticastExtensions (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValOspfv3MulticastExtensions)
{
    UNUSED_PARAM (*pTestValOspfv3MulticastExtensions);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3MulticastExtensions\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3ExitOverflowInterval
 Input       :  The Indices

                The Object 
                testValOspfv3ExitOverflowInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3ExitOverflowInterval (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValOspfv3ExitOverflowInterval)
{
    UNUSED_PARAM (pu4ErrorCode);
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4TestValOspfv3ExitOverflowInterval);
    OSPFV3_GBL_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test Routine For Ospfv3ExitOverflowInterval %x\n",
                     u4TestValOspfv3ExitOverflowInterval);

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3ExitOverflowInterval Success\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3DemandExtensions
 Input       :  The Indices

                The Object 
                testValOspfv3DemandExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3DemandExtensions (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValOspfv3DemandExtensions)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test Routine For Ospfv3DemandExtensions %d\n",
                 i4TestValOspfv3DemandExtensions);

    if (OSPFV3_IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfv3DemandExtensions))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3DemandExtensions Success\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "TruthValue is not valid \n");

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3DemandExtensions Failure\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3TrafficEngineeringSupport
 Input       :  The Indices

                The Object 
                testValOspfv3TrafficEngineeringSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3TrafficEngineeringSupport (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValOspfv3TrafficEngineeringSupport)
{
    UNUSED_PARAM (i4TestValOspfv3TrafficEngineeringSupport);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3TrafficEngineeringSupport\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3ReferenceBandwidth
 Input       :  The Indices

                The Object 
                testValOspfv3ReferenceBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3ReferenceBandwidth (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValOspfv3ReferenceBandwidth)
{
    UNUSED_PARAM (pu4ErrorCode);
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4TestValOspfv3ReferenceBandwidth);
    OSPFV3_GBL_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test Routine For Ospfv3ReferenceBandwidth %dn",
                     u4TestValOspfv3ReferenceBandwidth);

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3ReferenceBandwidth Success\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3RestartSupport
 Input       :  The Indices

                The Object 
                testValOspfv3RestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3RestartSupport (UINT4 *pu4ErrorCode,
                               INT4 i4TestValOspfv3RestartSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3RestartSupport\n");
    if ((i4TestValOspfv3RestartSupport < OSPFV3_RESTART_NONE) ||
        (i4TestValOspfv3RestartSupport > OSPFV3_RESTART_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3RestartSupport failed - Wrong value\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) &&
        (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3RestartSupport failed - Restart InProgress\n");
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3RestartSupport Success\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3RestartInterval
 Input       :  The Indices

                The Object 
                testValOspfv3RestartInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3RestartInterval (UINT4 *pu4ErrorCode,
                                INT4 i4TestValOspfv3RestartInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3RestartInterval\n");
    if ((i4TestValOspfv3RestartInterval < OSPFV3_GR_MIN_INTERVAL) ||
        (i4TestValOspfv3RestartInterval > OSPFV3_GR_MAX_INTERVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3RestartSupport failed - Wrong Value\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) &&
        (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3RestartSupport failed - Restart InProgress\n");
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3RestartInterval\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3RouterId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3RouterId (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3AdminStat
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3AdminStat (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3ASBdrRtrStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3ASBdrRtrStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3ExtAreaLsdbLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3ExtAreaLsdbLimit (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3MulticastExtensions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3MulticastExtensions (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3ExitOverflowInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3ExitOverflowInterval (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3DemandExtensions
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3DemandExtensions (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3TrafficEngineeringSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3TrafficEngineeringSupport (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3ReferenceBandwidth
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3ReferenceBandwidth (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3RestartSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3RestartSupport (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3RestartInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3RestartInterval (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3AreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3AreaTable
 Input       :  The Indices
                Ospfv3AreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3AreaTable (UINT4 u4Ospfv3AreaId)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Validate Index For Area Table OspfAreaId %x\n",
                 u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);
    if (V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For Area Table Success\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Validate Index For Area Table Failed\n");
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3ImportAsExtern
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3ImportAsExtern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3ImportAsExtern (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                               INT4 i4TestValOspfv3ImportAsExtern)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3ImportAsExtern %d Index AreaId %x \r\n",
                 i4TestValOspfv3ImportAsExtern, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);
    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId))) == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area not found\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    switch (i4TestValOspfv3ImportAsExtern)
    {
        case OSPFV3_NORMAL_AREA:
        case OSPFV3_STUB_AREA:
        case OSPFV3_NSSA_AREA:
            if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) ==
                OSPFV3_EQUAL)
            {
                if (i4TestValOspfv3ImportAsExtern != OSPFV3_NORMAL_AREA)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                                pV3OspfCxt->u4ContextId,
                                "Test For Ospfv3ImportAsExtern "
                                "Failed, Trying to "
                                "set Backbone area\n"
                                "as other than NORMAL area\n");
                    CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if ((pArea->u4AreaType == OSPFV3_NORMAL_AREA) &&
                    ((i4TestValOspfv3ImportAsExtern == OSPFV3_STUB_AREA) ||
                     (i4TestValOspfv3ImportAsExtern == OSPFV3_NSSA_AREA)))
                {
                    if (V3UtilFindIsTransitArea (pArea) == OSIX_TRUE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                                    pV3OspfCxt->u4ContextId,
                                    "Test For Ospfv3ImportAsExtern "
                                    "Failed as Trying to "
                                    "set Transit Area\n"
                                    "to other than Normal Area\n");
                        CLI_SET_ERR (OSPFV3_CLI_INV_NSSA_TRANSIT_CONF);
                        return SNMP_FAILURE;
                    }
                }
            }
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3ImportAsExtern Success\n");
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3ImportAsExtern Failed\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaSummary
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3AreaSummary
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaSummary (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                            INT4 i4TestValOspfv3AreaSummary)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3AreaSummary %d Index AreaId %x \n",
                 i4TestValOspfv3AreaSummary, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (V3GetFindAreaInCxt (pV3OspfCxt, &(areaId)) == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area not found\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        return SNMP_FAILURE;
    }

    if (((i4TestValOspfv3AreaSummary == OSPFV3_NO_AREA_SUMMARY) ||
         (i4TestValOspfv3AreaSummary == OSPFV3_SEND_AREA_SUMMARY)))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3AreaSummary Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3AreaSummary Failed\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaStatus
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3AreaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaStatus (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                           INT4 i4TestValOspfv3AreaStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!IS_VALID_AREA_ID (u4Ospfv3AreaId))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3AreaStatus %d Index AreaId %x \n",
                 i4TestValOspfv3AreaStatus, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (!OSPFV3_IS_RTR_ENABLED (pV3OspfCxt))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Configuration is not allowed on Backbone\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
        return SNMP_FAILURE;
    }

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (i4TestValOspfv3AreaStatus == DESTROY)
    {
        if (pArea == NULL)
        {
            return SNMP_SUCCESS;
        }

        if (TMO_SLL_Count (&(pArea->ifsInArea)) != 0)
        {
            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                        "When some interfaces are configured "
                        "in area, then the area deletion is not allowed.\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_ERR_DEL_AREA);
            return SNMP_FAILURE;
        }

        /* Area deletion is not allowed when some virtual links are configured
           through the area */
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (V3UtilAreaIdComp (pInterface->transitAreaId, areaId) ==
                OSPFV3_EQUAL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ERR_DEL_VAREA);
                return SNMP_FAILURE;
            }
        }
        return SNMP_SUCCESS;
    }

    switch ((UINT1) i4TestValOspfv3AreaStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3AreaStatus Failed\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            if (TMO_SLL_Count (&(pV3OspfCxt->areasLst)) >=
                FsOSPFV3SizingParams[MAX_OSPFV3_AREAS_SIZING_ID].
                u4PreAllocatedUnits)
            {
                CLI_SET_ERR (OSPFV3_CLI_AREA_ALLOC_FAILED);
                return SNMP_FAILURE;
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3AreaStatus Success\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId))) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3AreaStatus Failed\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pArea->areaStatus == NOT_IN_SERVICE) ||
                     (pArea->areaStatus == ACTIVE) ||
                     (pArea->areaStatus == NOT_READY))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3AreaStatus Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3AreaStatus Failed\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;
            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3AreaStatus Failed\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3StubMetric
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3StubMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3StubMetric (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                           INT4 i4TestValOspfv3StubMetric)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3StubMetric %d Index AreaId %d\n",
                 i4TestValOspfv3StubMetric, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3StubMetric Failure,"
                    " Area is Backbone area\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
        return SNMP_FAILURE;
    }

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId))) == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area not found\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if (pArea->u4AreaType != OSPFV3_NSSA_AREA
        && pArea->u4AreaType != OSPFV3_STUB_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_DEF_METRIC);
        return SNMP_FAILURE;
    }

    if (OSPFV3_IS_VALID_BIG_METRIC (i4TestValOspfv3StubMetric)
        && (pArea->u4AreaType != OSPFV3_NORMAL_AREA))
    {

        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3StubMetric Success\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3StubMetric Failure\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaStubMetricType
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3StubMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaStubMetricType (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                                   INT4 i4TestValOspfv3StubMetricType)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3StubMetricType %d Index AreaId %x \n",
                 i4TestValOspfv3StubMetricType, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);
    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId))) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area not found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        return SNMP_FAILURE;
    }

    switch (i4TestValOspfv3StubMetricType)
    {

        case OSPFV3_METRIC:
        case OSPFV3_TYPE1EXT_METRIC:
        case OSPFV3_TYPE2EXT_METRIC:

            if (pArea->u4AreaType == OSPFV3_NORMAL_AREA)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3StubMetricType Failure, "
                            "Area is not NSSA or STUB area\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_AREA_TYPE);
                return SNMP_FAILURE;
            }

            if ((pArea->u4AreaType == OSPFV3_STUB_AREA) &&
                ((i4TestValOspfv3StubMetricType == OSPFV3_TYPE1EXT_METRIC)
                 || (i4TestValOspfv3StubMetricType == OSPFV3_TYPE2EXT_METRIC)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3StubMetricType Failure, "
                            "Area is Stub and\n"
                            "MetricType is other than OSPFV3_METRIC\n");
                CLI_SET_ERR (OSPFV3_CLI_ERR_METRIC_STUB);
                return SNMP_FAILURE;
            }

            if ((pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                (i4TestValOspfv3StubMetricType == OSPFV3_METRIC) &&
                (pArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3StubMetricType Failure,"
                            " Area is NSSA,\n"
                            "MetricType is OSPFV3_METRIC and AreaSummary is "
                            "SEND_AREA_SUMMARY\n");
                CLI_SET_ERR (OSPFV3_CLI_ERR_METRIC_NSSA);
                return SNMP_FAILURE;
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3StubMetricType Success\n");
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3StubMetricType Failure\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
            return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3AreaTable
 Input       :  The Indices
                Ospfv3AreaId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3AreaTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaNssaTranslatorRole
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3AreaNssaTranslatorRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaNssaTranslatorRole (UINT4 *pu4ErrorCode,
                                       UINT4 u4Ospfv3AreaId,
                                       INT4
                                       i4TestValOspfv3AreaNssaTranslatorRole)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3AreaNSSATranslatorRole %d Index AreaId %x \n",
                 i4TestValOspfv3AreaNssaTranslatorRole, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (pV3OspfCxt->admnStat != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3AreaTranslatorRole Failure, OSPFV3 "
                    "is not enabled\n");
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Configuration is not allowed on Backbone\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
        return SNMP_FAILURE;
    }

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId))) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3AreaNSSATranslatorRole Fails\n"
                    "Area not Found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    switch (i4TestValOspfv3AreaNssaTranslatorRole)
    {
        case (OSPFV3_TRNSLTR_ROLE_ALWAYS):
        case (OSPFV3_TRNSLTR_ROLE_CANDIDATE):

            if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3StubMetricType Failure, "
                            "Area is not NSSA area\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_TRNS_ROLE);
                return SNMP_FAILURE;
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3AreaTranslatorRole Success");
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3AreaTranslatorRole Failure\n"
                        "TranslatorRole is not valid\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                testValOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaNssaTranslatorStabilityInterval (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ospfv3AreaId,
                                                    UINT4
                                                    u4TestValOspfv3AreaNssaTranslatorStabilityInterval)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsArea          *pArea;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4TestValOspfv3AreaNssaTranslatorStabilityInterval);

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For OspfAreaNSSATranslatorRole %d Index AreaId %x \n",
                 u4TestValOspfv3AreaNssaTranslatorStabilityInterval,
                 u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Configuration is not allowed on Backbone\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
        return SNMP_FAILURE;
    }

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_NO_AREA);
        return SNMP_FAILURE;
    }

    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_STAB_INT);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : Ospfv3AsLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3AsLsdbTable
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3AsLsdbTable (UINT4 u4Ospfv3AsLsdbType,
                                           UINT4 u4Ospfv3AsLsdbRouterId,
                                           UINT4 u4Ospfv3AsLsdbLsid)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC3 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Valdt Ospfv3LsdbType %d Ospfv3LsdbLsid %x"
                 " Ospfv3LsdbRouterId %x\n",
                 u4Ospfv3AsLsdbType, u4Ospfv3AsLsdbLsid,
                 u4Ospfv3AsLsdbRouterId);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if (V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType, &linkStateId,
                             &advRtrId, NULL, pV3OspfCxt->pBackbone) != NULL)
    {

        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For AsLsdbTable Succeeded\n");
        return SNMP_SUCCESS;
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Validate Index For AsLsdbTable Failed\n");
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3AreaLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3AreaLsdbTable
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3AreaLsdbTable (UINT4
                                             u4Ospfv3AreaLsdbAreaId,
                                             UINT4 u4Ospfv3AreaLsdbType,
                                             UINT4
                                             u4Ospfv3AreaLsdbRouterId,
                                             UINT4 u4Ospfv3AreaLsdbLsid)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC4 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Validate Ospfv3AreaLsdbAreaId %x Ospfv3AreaLsdbType %d \n"
                 "Ospfv3AreaLsdbLsid %x Ospfv3AreaLsdbRouterId %x\n",
                 u4Ospfv3AreaLsdbAreaId, u4Ospfv3AreaLsdbType,
                 u4Ospfv3AreaLsdbLsid, u4Ospfv3AreaLsdbRouterId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For AreaLsdbTable Failed,"
                    " AreaId is not valid\n");
        return SNMP_FAILURE;
    }

    if (V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType, &linkStateId,
                             &advRtrId, NULL, pArea) != NULL)
    {

        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For AreaLsdbTable Succeeded\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Validate Index For AreaLsdbTable Failed\n");
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3LinkLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3LinkLsdbTable
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3LinkLsdbTable (INT4
                                             i4Ospfv3LinkLsdbIfIndex,
                                             UINT4 u4Ospfv3LinkLsdbType,
                                             UINT4
                                             u4Ospfv3LinkLsdbRouterId,
                                             UINT4 u4Ospfv3LinkLsdbLsid)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_GBL_TRC4 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Validate Ospfv3LinkLsdbIfIndex %x Ospfv3LinkLsdbType %d\n"
                     "Ospfv3LinkLsdbLsid %x Ospfv3LinkLsdbRouterId %x\n",
                     i4Ospfv3LinkLsdbIfIndex, u4Ospfv3LinkLsdbType,
                     u4Ospfv3LinkLsdbLsid, u4Ospfv3LinkLsdbRouterId);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) == NULL)
    {
        OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        "Validate Index For LinkLsdbTable Failed, IfIndex is\n"
                        "not valid\n");
        return SNMP_FAILURE;
    }

    if (V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType, &linkStateId,
                             &advRtrId, pInterface, NULL) != NULL)
    {

        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Validate Index For LinkLsdbTable Succeeded\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Validate Index For LinkLsdbTable Failed\n");
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3HostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3HostTable
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3HostTable (INT4 i4Ospfv3HostAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pOspfv3HostAddress)
{
    tIp6Addr            hostIp6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Validate Index For HostTable Ospfv3HostIpAddress %s\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                               pOspfv3HostAddress->pu1_OctetList));

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr.u1_addr,
                          *(pOspfv3HostAddress->pu1_OctetList));

    if (i4Ospfv3HostAddressType == OSPFV3_INET_ADDR_TYPE)
    {
        if (V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr) != NULL)
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Validate Index For HostTable Succeeded\n");
            return SNMP_SUCCESS;
        }
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Validate Index For HostTable Failed\n");
    return SNMP_FAILURE;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3HostMetric
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                testValOspfv3HostMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3HostMetric (UINT4 *pu4ErrorCode,
                           INT4 i4Ospfv3HostAddressType,
                           tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                           INT4 i4TestValOspfv3HostMetric)
{
    tIp6Addr            hostIp6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3HostMetric %d Indices Ospfv3HostIpAddress %s",
                 i4TestValOspfv3HostMetric,
                 Ip6PrintAddr ((tIp6Addr *) pOspfv3HostAddress));

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3HostMetric Failed,"
                    " Invalid HostAddressType\n");
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr.u1_addr,
                          *(pOspfv3HostAddress->pu1_OctetList));

    if (V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr) != NULL)
    {

        if (OSPFV3_IS_VALID_METRIC (i4TestValOspfv3HostMetric))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3HostMetric Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3HostMetric Failure\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3HostStatus
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                testValOspfv3HostStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3HostStatus (UINT4 *pu4ErrorCode,
                           INT4 i4Ospfv3HostAddressType,
                           tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                           INT4 i4TestValOspfv3HostStatus)
{
    tIp6Addr            hostIp6Addr;
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4HostCount = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3HostStatus %d Indices Ospfv3HostIpAddress %s",
                 i4TestValOspfv3HostStatus,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                               pOspfv3HostAddress->pu1_OctetList));

    if (i4TestValOspfv3HostStatus == DESTROY)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3HostStatus Failure, hostStatus "
                    "is DESTROY\n");
        return SNMP_SUCCESS;
    }

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3HostStatus Failed,"
                    " Invalid HostAddressType\n");
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr.u1_addr,
                          *(pOspfv3HostAddress->pu1_OctetList));

    switch ((UINT1) i4TestValOspfv3HostStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            if (V3GetFindHostInCxt (pV3OspfCxt, &(hostIp6Addr)) != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3HostStatus Failed,"
                            " Host entry exists\n");
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            RBTreeCount (pV3OspfCxt->pHostRBRoot, &u4HostCount);
            if ((u4HostCount) >=
                FsOSPFV3SizingParams[MAX_OSPFV3_HOSTS_SIZING_ID].
                u4PreAllocatedUnits)
            {
                CLI_SET_ERR (OSPFV3_CLI_HOST_ALLOC_FAILED);
                return SNMP_FAILURE;
            }
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3HostStatus Success\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pHost =
                 V3GetFindHostInCxt (pV3OspfCxt, &(hostIp6Addr))) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3HostStatus Failed,"
                            " Host entry doesn't exist\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pHost->hostStatus == NOT_IN_SERVICE) ||
                     (pHost->hostStatus == ACTIVE) ||
                     (pHost->hostStatus == NOT_READY))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3HostStatus Success\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3HostStatus Failure\n");
            CLI_SET_ERR (OSPFV3_CLI_ERR_TEST_STATUS_ACTIVE);
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3HostStatus Failure\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3HostAreaID
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                testValOspfv3HostAreaID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3HostAreaID (UINT4 *pu4ErrorCode,
                           INT4 i4Ospfv3HostAddressType,
                           tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                           UINT4 u4TestValOspfv3HostAreaID)
{
    tIp6Addr            hostIp6Addr;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3HostAreaID %d Indices Ospfv3HostAddress %s",
                 u4TestValOspfv3HostAreaID,
                 Ip6PrintAddr ((tIp6Addr *) (VOID *)
                               pOspfv3HostAddress->pu1_OctetList));

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3HostAreaID Failed,"
                    " Invalid HostAddressType\n");
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr.u1_addr,
                          *(pOspfv3HostAddress->pu1_OctetList));
    OSPFV3_BUFFER_DWTOPDU (areaId, u4TestValOspfv3HostAreaID);

    if (V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr) != NULL)
    {
        if (V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL)
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3HostAreaID Success\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3HostAreaID Failure\n");
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3HostTable
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3HostTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3IfTable
 Input       :  The Indices
                Ospfv3IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3IfTable (INT4 i4Ospfv3IfIndex)
{

    if (V3GetFindIf ((UINT4) i4Ospfv3IfIndex) != NULL)
    {
        OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        "Validate Index For IfTable Succeeded\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Validate Index For IfTable Failed\n");
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfAreaId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfAreaId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfAreaId (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                         UINT4 u4TestValOspfv3IfAreaId)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsRouterId       rtrId;

    MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For OspfIfAreaId %x Indices Ospfv3IfIndex %x ",
                 u4TestValOspfv3IfAreaId, i4Ospfv3IfIndex);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4TestValOspfv3IfAreaId);

    if (V3GetFindIf ((UINT4) i4Ospfv3IfIndex) != NULL)
    {
        if (V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL)
        {
            if ((V3UtilRtrIdComp (pV3OspfCxt->rtrId, OSPFV3_NULL_RTRID) !=
                 OSPFV3_EQUAL))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfAreaId Succeeds\n");
                return SNMP_SUCCESS;
            }
            else
            {
                /* Dynamic Rtr ID assignment is allowed only if GR is not in progress */
                if (V3OspfRmGetNodeState () != RM_STANDBY)
                {
                    if ((pV3OspfCxt->u1RestartExitReason !=
                         OSPFV3_RESTART_INPROGRESS)
                        && (pV3OspfCxt->u1RouterIdStatus ==
                            OSPFV3_ROUTERID_DYNAMIC))
                    {
                        if (V3UtilSelectOspfRouterId (pV3OspfCxt->u4ContextId,
                                                      &rtrId) == OSPFV3_SUCCESS)
                        {
                            if (V3UtilRtrIdComp (rtrId, OSPFV3_NULL_RTRID) !=
                                OSPFV3_EQUAL)
                            {
                                OSPFV3_IPV4_ADDR_COPY (&(pV3OspfCxt->rtrId),
                                                       &(rtrId));
                                if (O3RedDynSendRtrIdToStandby
                                    (rtrId,
                                     pV3OspfCxt->u4ContextId) == OSPFV3_FAILURE)
                                {
                                    return SNMP_FAILURE;
                                }
                            }
                        }
                    }
                }
            }

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_RTR_ID_NOT_SET);
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3IfAreaId Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfType
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfType (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                       INT4 i4TestValOspfv3IfType)
{

    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfType %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfType, i4Ospfv3IfIndex);

    if ((pInterface = V3GetFindIf (i4Ospfv3IfIndex)) != NULL)
    {
        if (OSPFV3_IS_VALID_IF_TYPE_VALUE (i4TestValOspfv3IfType))
        {

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if (pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "Configured Neighbours present on this interface,"
                                " If Type not set \n");
                    CLI_SET_ERR (OSPFV3_CLI_ERR_NBR_EXISTS);
                    return SNMP_FAILURE;
                }
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3IfType Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_IFTYPE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }
    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfType Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfAdminStat
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfAdminStat (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                            INT4 i4TestValOspfv3IfAdminStat)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfAdminStat %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfAdminStat, i4Ospfv3IfIndex);
    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_TRUTH_VALUE ((UINT4) i4TestValOspfv3IfAdminStat))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfAdminStat Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfAdminStat Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfRtrPriority
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfRtrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfRtrPriority (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                              INT4 i4TestValOspfv3IfRtrPriority)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfRtrPriority %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfRtrPriority, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_PRIORITY (i4TestValOspfv3IfRtrPriority))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfRtrPriority Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfRtrPriority Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfTransitDelay
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfTransitDelay (UINT4 *pu4ErrorCode,
                               INT4 i4Ospfv3IfIndex,
                               INT4 i4TestValOspfv3IfTransitDelay)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfTransitDely %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfTransitDelay, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_AGE (i4TestValOspfv3IfTransitDelay))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfTransitDelay Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfTransitDelay Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfRetransInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfRetransInterval (UINT4 *pu4ErrorCode,
                                  INT4 i4Ospfv3IfIndex,
                                  INT4 i4TestValOspfv3IfRetransInterval)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfRetransInterval %d"
                     " Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfRetransInterval, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_AGE (i4TestValOspfv3IfRetransInterval))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfRetransInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfRetransInterval Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfHelloInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfHelloInterval (UINT4 *pu4ErrorCode,
                                INT4 i4Ospfv3IfIndex,
                                INT4 i4TestValOspfv3IfHelloInterval)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfHelloInterval %d"
                     " Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfHelloInterval, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_HELLO_RANGE (i4TestValOspfv3IfHelloInterval))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfHelloInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfHelloInterval Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfRtrDeadInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                  INT4 i4Ospfv3IfIndex,
                                  INT4 i4TestValOspfv3IfRtrDeadInterval)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfRtrDeadInterval %d "
                     "Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfRtrDeadInterval, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_DEAD_INTERVAL (i4TestValOspfv3IfRtrDeadInterval))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfRtrDeadInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfRtrDeadInterval Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfPollInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfPollInterval (UINT4 *pu4ErrorCode,
                               INT4 i4Ospfv3IfIndex,
                               UINT4 u4TestValOspfv3IfPollInterval)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfPollInterval %d "
                     "Indices i4Ospfv3IfIndex %d\n",
                     u4TestValOspfv3IfPollInterval, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_POLL_INTERVAL (u4TestValOspfv3IfPollInterval))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfPollInterval Succeeds\n");

            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfPollInterval Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfStatus
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfStatus (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                         INT4 i4TestValOspfv3IfStatus)
{
    tNetIpv6IfInfo      netIp6IfInfo;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4CxtId = OSPFV3_ZERO;
    UINT1               u1IfType = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->admnStat != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Test For Ospfv3IfStatus Fails\n");
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if (i4TestValOspfv3IfStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (V3OspfGetCxtIdFromCfaIndex (i4Ospfv3IfIndex, &u4CxtId) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (u4CxtId != pV3OspfCxt->u4ContextId)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfv3IfStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            pInterface = V3GetFindIf (i4Ospfv3IfIndex);
            if (pInterface != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfStatus Fails,"
                            " Interface exists\n");
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            if (NetIpv6GetIfInfo ((UINT4) i4Ospfv3IfIndex, &netIp6IfInfo) ==
                NETIPV6_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfStatus Fails\n");
                CLI_SET_ERR (OSPFV3_CLI_ERR_IP6);
                return SNMP_FAILURE;
            }
            if (V3MapIfType (netIp6IfInfo.u4InterfaceType, &u1IfType)
                == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "V3MapIfType: Test For Ospfv3 IfType Fails\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_IFTYPE);
                return SNMP_FAILURE;
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3IfStatus Succeeds\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            pInterface = V3GetFindIf (i4Ospfv3IfIndex);
            if (pInterface == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfStatus Fails, Interface "
                            "does not exist\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pInterface->ifStatus == NOT_IN_SERVICE) ||
                     (pInterface->ifStatus == ACTIVE))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            else if (pInterface->ifStatus == NOT_READY)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3IfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3IfStatus Fails\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
            break;
        default:
            break;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3IfStatus Fails\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfMulticastForwarding
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfMulticastForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfMulticastForwarding (UINT4 *pu4ErrorCode,
                                      INT4 i4Ospfv3IfIndex,
                                      INT4 i4TestValOspfv3IfMulticastForwarding)
{
    UNUSED_PARAM (i4Ospfv3IfIndex);
    UNUSED_PARAM (i4TestValOspfv3IfMulticastForwarding);

    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfMulticastForwarding %d "
                     "Indices Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfMulticastForwarding, i4Ospfv3IfIndex);

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfMulticastForwarding Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfDemand
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfDemand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfDemand (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                         INT4 i4TestValOspfv3IfDemand)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3IfDemand %d Indices i4Ospfv3IfIndex %d\n",
                 i4TestValOspfv3IfDemand, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValOspfv3IfDemand))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3IfDemand Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_ERR_DEMAND);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3IfDemand Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfMetricValue
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfMetricValue (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                              INT4 i4TestValOspfv3IfMetricValue)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfMetricValue %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfMetricValue, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_METRIC (i4TestValOspfv3IfMetricValue))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfMetricValue Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfMetricValue Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfInstId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfInstId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfInstId (UINT4 *pu4ErrorCode, INT4 i4Ospfv3IfIndex,
                         INT4 i4TestValOspfv3IfInstId)
{
    OSPFV3_GBL_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                     "Test For Ospfv3IfInstId %d Indices i4Ospfv3IfIndex %d\n",
                     i4TestValOspfv3IfInstId, i4Ospfv3IfIndex);

    if (V3GetFindIf (i4Ospfv3IfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_INSTANCE_ID (i4TestValOspfv3IfInstId))
        {
            OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            "Test For Ospfv3IfInstId Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_GBL_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    "Test For Ospfv3IfInstId Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfDemandNbrProbe
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfDemandNbrProbe
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfDemandNbrProbe (UINT4 *pu4ErrorCode,
                                 INT4 i4Ospfv3IfIndex,
                                 INT4 i4TestValOspfv3IfDemandNbrProbe)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3IfDemandNbrProbe %d "
                 "Indices i4Ospfv3IfIndex %d\n",
                 i4TestValOspfv3IfDemandNbrProbe, i4Ospfv3IfIndex);

    if ((pInterface = V3GetFindIf (i4Ospfv3IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbe Failure, Interface"
                    "not found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pInterface->bDcEndpt == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbe Fails, "
                    "DemandExt or IfDemand is not enabled\n");
        CLI_SET_ERR (OSPFV3_CLI_ERR_DEMAND);
        return SNMP_FAILURE;
    }

    if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValOspfv3IfDemandNbrProbe))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbe Succeeds\n");
        return SNMP_SUCCESS;
    }
    else
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "TruthValue is not valid \n");
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For Ospfv3IfDemandNbrProbe Fails\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit (UINT4 *pu4ErrorCode,
                                          INT4 i4Ospfv3IfIndex,
                                          UINT4
                                          u4TestValOspfv3IfDemandNbrProbeRetxLimit)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4TestValOspfv3IfDemandNbrProbeRetxLimit);
    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3IfDemandNbrProbeRetxLimit "
                 "%d Indices i4Ospfv3IfIndex %d\n",
                 u4TestValOspfv3IfDemandNbrProbeRetxLimit, i4Ospfv3IfIndex);

    if ((pInterface = V3GetFindIf (i4Ospfv3IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbe Failure, Interface"
                    "not found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pInterface->bDcEndpt == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbeRetxLimit Fails, DemandExt "
                    "or IfDeamnd is not enable\n");
        CLI_SET_ERR (OSPFV3_CLI_ERR_DEMAND);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                testValOspfv3IfDemandNbrProbeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3IfDemandNbrProbeInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4Ospfv3IfIndex,
                                         UINT4
                                         u4TestValOspfv3IfDemandNbrProbeInterval)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u4TestValOspfv3IfDemandNbrProbeInterval);

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3IfDemandNbrProbeInterval "
                 "%d Indices i4Ospfv3IfIndex %d\n",
                 u4TestValOspfv3IfDemandNbrProbeInterval, i4Ospfv3IfIndex);

    if ((pInterface = V3GetFindIf (i4Ospfv3IfIndex)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbe Failure, Interface"
                    "not found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pInterface->bDcEndpt == OSIX_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3IfDemandNbrProbeInterval Fails, DemandExt "
                    "or IfDeamnd is not enable\n");
        CLI_SET_ERR (OSPFV3_CLI_ERR_DEMAND);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3IfTable
 Input       :  The Indices
                Ospfv3IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3VirtIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3VirtIfTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3VirtIfTable (UINT4 u4Ospfv3VirtIfAreaId,
                                           UINT4 u4Ospfv3VirtIfNeighbor)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For VirtIfTable Succeeds\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Validate Index For VirtIfTable Fails\n");
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfIndex
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfIndex (UINT4 *pu4ErrorCode,
                            UINT4 u4Ospfv3VirtIfAreaId,
                            UINT4 u4Ospfv3VirtIfNeighbor,
                            INT4 i4TestValOspfv3VirtIfIndex)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValOspfv3VirtIfIndex);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if (!(OSPFV3_IS_VALID_IF_INDEX (i4TestValOspfv3VirtIfIndex)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL) &&
        (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3VirtIfIndex Succeeds\n");
        return SNMP_SUCCESS;
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3VirtIfIndex Fails\n");
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfTransitDelay
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfTransitDelay (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 i4TestValOspfv3VirtIfTransitDelay)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL) &&
        (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL))
    {
        if (OSPFV3_IS_VALID_AGE (i4TestValOspfv3VirtIfTransitDelay))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfTransitDelay Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For Ospfv3VirtIfTransitDealy Fails\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfRetransInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfRetransInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ospfv3VirtIfAreaId,
                                      UINT4 u4Ospfv3VirtIfNeighbor,
                                      INT4 i4TestValOspfv3VirtIfRetransInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL) &&
        (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL))
    {
        if (OSPFV3_IS_VALID_AGE (i4TestValOspfv3VirtIfRetransInterval))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfRetransInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For Ospfv3VirtIfRetransInterval Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfHelloInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfHelloInterval (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ospfv3VirtIfAreaId,
                                    UINT4 u4Ospfv3VirtIfNeighbor,
                                    INT4 i4TestValOspfv3VirtIfHelloInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL) &&
        (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL))
    {
        if (OSPFV3_IS_VALID_HELLO_RANGE (i4TestValOspfv3VirtIfHelloInterval))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfHelloInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3VirtIfHelloInterval Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ospfv3VirtIfAreaId,
                                      UINT4 u4Ospfv3VirtIfNeighbor,
                                      INT4 i4TestValOspfv3VirtIfRtrDeadInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((V3GetFindAreaInCxt (pV3OspfCxt, &areaId) != NULL) &&
        (V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId) != NULL))
    {
        if (OSPFV3_IS_VALID_DEAD_INTERVAL
            (i4TestValOspfv3VirtIfRtrDeadInterval))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfRtrDeadInterval Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                "Test For Ospfv3VirtIfRtrDeadInterval Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3VirtIfStatus
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                testValOspfv3VirtIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3VirtIfStatus (UINT4 *pu4ErrorCode,
                             UINT4 u4Ospfv3VirtIfAreaId,
                             UINT4 u4Ospfv3VirtIfNeighbor,
                             INT4 i4TestValOspfv3VirtIfStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if (i4TestValOspfv3VirtIfStatus == DESTROY)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3VirtIfStatus Succeeds\n");
        return SNMP_SUCCESS;
    }

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area Not Found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        return SNMP_FAILURE;
    }

    if ((pArea->u4AreaType != OSPFV3_NORMAL_AREA) ||
        (V3UtilAreaIdComp (pArea->areaId, OSPFV3_BACKBONE_AREAID) ==
         OSPFV3_EQUAL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA_TYPE);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfv3VirtIfStatus)
    {
        case CREATE_AND_GO:
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfStatus Fails, "
                        "CREATE_AND_GO not valid status\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_ERR_TEST_STATUS_CG);
            return SNMP_FAILURE;

        case CREATE_AND_WAIT:
            pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
            if (pInterface != NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3VirtIfStatus Fails, "
                            "VirtIf Entry Exists\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }

            if (TMO_SLL_Count (&(pV3OspfCxt->virtIfLst)) ==
                OSPFV3_MAX_VIRTUAL_IFS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_NO_FREE_ENTRY);
                return SNMP_FAILURE;

            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfStatus Succeeds\n");
            return SNMP_SUCCESS;

        case ACTIVE:
            pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
            if ((pInterface == NULL) || (pInterface->u4InterfaceId == 0))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3VirtIfStatus Fails, VirtIf Entry "
                            "Does Not Exist\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pInterface->ifStatus == NOT_IN_SERVICE) ||
                     (pInterface->ifStatus == ACTIVE))
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3VirtIfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }
            else if (pInterface->ifStatus == NOT_READY)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For Ospfv3VirtIfStatus Succeeds\n");
                return SNMP_SUCCESS;
            }

            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3VirtIfStatus Fails,Satus is Active\n");

            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Test For Ospfv3VirtIfStatus Fails\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3VirtIfTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3VirtIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3NbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3NbrTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3NbrTable (INT4 i4Ospfv3NbrIfIndex,
                                        UINT4 u4Ospfv3NbrRtrId)
{
    tV3OsRouterId       nbrRtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if (V3GetFindNbrInCxt (pV3OspfCxt, &nbrRtrId,
                           (UINT4) i4Ospfv3NbrIfIndex) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3NbmaNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3NbmaNbrTable
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3NbmaNbrTable (INT4 i4Ospfv3NbmaNbrIfIndex,
                                            INT4
                                            i4Ospfv3NbmaNbrAddressType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pOspfv3NbmaNbrAddress)
{
    tIp6Addr            nbmaNbrAddress;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3NbmaNbrAddressType == OSPFV3_INET_ADDR_TYPE)
    {
        OSPFV3_IP6_ADDR_COPY (nbmaNbrAddress.u1_addr,
                              *(pOspfv3NbmaNbrAddress->pu1_OctetList));
        if (V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbmaNbrAddress,
                                   i4Ospfv3NbmaNbrIfIndex) != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3NbmaNbrPriority
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                testValOspfv3NbmaNbrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3NbmaNbrPriority (UINT4 *pu4ErrorCode,
                                INT4 i4Ospfv3NbmaNbrIfIndex,
                                INT4 i4Ospfv3NbmaNbrAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pOspfv3NbmaNbrAddress,
                                INT4 i4TestValOspfv3NbmaNbrPriority)
{
    tIp6Addr            nbmaNbrAddress;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (nbmaNbrAddress.u1_addr,
                          *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if (V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbmaNbrAddress,
                               (UINT4) i4Ospfv3NbmaNbrIfIndex) != NULL)
    {
        if (OSPFV3_IS_VALID_PRIORITY (i4TestValOspfv3NbmaNbrPriority))
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For nmhTestv2Ospfv3NbmaNbrPriority Succeeds\n");
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For nmhTestv2Ospfv3NbmaNbrPriority Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3NbmaNbrStorageType
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                testValOspfv3NbmaNbrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3NbmaNbrStorageType (UINT4 *pu4ErrorCode,
                                   INT4 i4Ospfv3NbmaNbrIfIndex,
                                   INT4 i4Ospfv3NbmaNbrAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pOspfv3NbmaNbrAddress,
                                   INT4 i4TestValOspfv3NbmaNbrStorageType)
{
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    UNUSED_PARAM (i4Ospfv3NbmaNbrIfIndex);
    UNUSED_PARAM (i4Ospfv3NbmaNbrAddressType);
    UNUSED_PARAM (pOspfv3NbmaNbrAddress);
    UNUSED_PARAM (i4TestValOspfv3NbmaNbrStorageType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3NbmaNbrStatus
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                testValOspfv3NbmaNbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3NbmaNbrStatus (UINT4 *pu4ErrorCode,
                              INT4 i4Ospfv3NbmaNbrIfIndex,
                              INT4 i4Ospfv3NbmaNbrAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pOspfv3NbmaNbrAddress,
                              INT4 i4TestValOspfv3NbmaNbrStatus)
{
    tIp6Addr            nbrAddr;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface     *pLocalInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (nbrAddr.u1_addr,
                          *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if ((V3UtilIp6AddrComp ((tIp6Addr *) (VOID *) nbrAddr.u1_addr,
                            &gV3OsNullIp6Addr) == OSPFV3_EQUAL) ||
        (!IS_ADDR_LLOCAL (nbrAddr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_NBR_ADDRESS);
        return SNMP_FAILURE;
    }

    if (i4TestValOspfv3NbmaNbrStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfv3NbmaNbrStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3NbmaNbrIfIndex))
                != NULL)
            {
                if (((pInterface->u1NetworkType != OSPFV3_IF_NBMA) &&
                     (pInterface->u1NetworkType != OSPFV3_IF_PTOMP)) ||
                    (pInterface->pArea == NULL))
                {
                    /* Nbr can be configured only in NBMA intrfaces */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    CLI_SET_ERR (OSPFV3_CLI_ERR_CONF_NBR);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }

            pLocalInterface =
                (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
            while (pLocalInterface != NULL)
            {
                if ((V3UtilIp6AddrComp ((tIp6Addr *) (VOID *) nbrAddr.u1_addr,
                                        &pLocalInterface->ifIp6Addr) ==
                     OSPFV3_EQUAL)
                    && (pLocalInterface->pArea->pV3OspfCxt->u4ContextId ==
                        pV3OspfCxt->u4ContextId))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    CLI_SET_ERR (OSPFV3_CLI_SELF_LINKLOCAL_ADDR);
                    return SNMP_FAILURE;
                }
                pLocalInterface =
                    (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                      (tRBElem *)
                                                      pLocalInterface, NULL);
            }
            pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrAddr,
                                          i4Ospfv3NbmaNbrIfIndex);
            if (pNbr != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
            pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrAddr,
                                          i4Ospfv3NbmaNbrIfIndex);
            if (pNbr == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            else if ((pNbr->nbrStatus == NOT_IN_SERVICE) ||
                     (pNbr->nbrStatus == ACTIVE) ||
                     (pNbr->nbrStatus == NOT_READY))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ospfv3AreaAggregateTable
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3AreaAggregateTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3VirtNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3VirtNbrTable
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3VirtNbrTable (UINT4 u4Ospfv3VirtNbrArea,
                                            UINT4 u4Ospfv3VirtNbrRtrId)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if (V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId, &nbrId) != NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For VirtNbrTable Succeeds\n");
        return SNMP_SUCCESS;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Validate Index For VirtNbrTable Fails\n");
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3AreaAggregateTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceOspfv3AreaAggregateTable
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceOspfv3AreaAggregateTable (UINT4
                                                  u4Ospfv3AreaAggregateAreaID,
                                                  INT4
                                                  i4Ospfv3AreaAggregateAreaLsdbType,
                                                  INT4
                                                  i4Ospfv3AreaAggregatePrefixType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pOspfv3AreaAggregatePrefix,
                                                  UINT4
                                                  i4Ospfv3AreaAggregatePrefixLength)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Area Not Found\n");
        return SNMP_FAILURE;
    }

    if (V3GetFindAreaAddrRange
        (pArea, (tIp6Addr *) (VOID *) pOspfv3AreaAggregatePrefix->pu1_OctetList,
         (UINT1) i4Ospfv3AreaAggregatePrefixLength,
         (UINT2) i4Ospfv3AreaAggregateAreaLsdbType) != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaAggregateStatus
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                testValOspfv3AreaAggregateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaAggregateStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ospfv3AreaAggregateAreaID,
                                    INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                    INT4 i4Ospfv3AreaAggregatePrefixType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pOspfv3AreaAggregatePrefix,
                                    UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                    INT4 i4TestValOspfv3AreaAggregateStatus)
{
    tV3OsAreaId         areaId;
    tIp6Addr            ip6Addr;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    OSPFV3_IP6_ADDR_COPY (ip6Addr.u1_addr,
                          *(pOspfv3AreaAggregatePrefix->pu1_OctetList));

    if (i4TestValOspfv3AreaAggregateStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValOspfv3AreaAggregateStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, "Area Not Found\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
                return SNMP_FAILURE;
            }

            if (V3GetFindAreaAddrRange
                (pArea, &ip6Addr, (UINT1) i4Ospfv3AreaAggregatePrefixLength,
                 (UINT2) i4Ospfv3AreaAggregateAreaLsdbType) != NULL)

            {
                OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                             " !!!!! Same Address Range is configured in "
                             "Area %08x    !!!!! \n",
                             OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
            if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, "Area Not Found\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
                return SNMP_FAILURE;
            }
            if ((pAddrRange = V3GetFindAreaAddrRange (pArea, &ip6Addr,
                                                      (UINT1)
                                                      i4Ospfv3AreaAggregatePrefixLength,
                                                      (UINT2)
                                                      i4Ospfv3AreaAggregateAreaLsdbType))
                != NULL)
            {
                if ((pAddrRange->areaAggStatus == NOT_IN_SERVICE) ||
                    (pAddrRange->areaAggStatus == ACTIVE))
                {
                    return SNMP_SUCCESS;
                }
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            break;

            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaAggregateEffect
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                testValOspfv3AreaAggregateEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaAggregateEffect (UINT4 *pu4ErrorCode,
                                    UINT4 u4Ospfv3AreaAggregateAreaID,
                                    INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                    INT4 i4Ospfv3AreaAggregatePrefixType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pOspfv3AreaAggregatePrefix,
                                    UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                    INT4 i4TestValOspfv3AreaAggregateEffect)
{
    tV3OsAreaId         areaId;
    tIp6Addr            ip6Addr;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);
    OSPFV3_IP6_ADDR_COPY (ip6Addr.u1_addr,
                          *(pOspfv3AreaAggregatePrefix->pu1_OctetList));

    if (((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
        && (V3GetFindAreaAddrRange (pArea, &ip6Addr,
                                    (UINT1)
                                    i4Ospfv3AreaAggregatePrefixLength,
                                    (UINT2)
                                    i4Ospfv3AreaAggregateAreaLsdbType) != NULL))
    {
        if (OSPFV3_IS_VALID_AGG_EFFECT (i4TestValOspfv3AreaAggregateEffect))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaAggregateRouteTag
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                testValOspfv3AreaAggregateRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaAggregateRouteTag (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ospfv3AreaAggregateAreaID,
                                      INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                      INT4 i4Ospfv3AreaAggregatePrefixType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pOspfv3AreaAggregatePrefix,
                                      UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                      INT4 i4TestValOspfv3AreaAggregateRouteTag)
{
    tV3OsAreaId         areaId;
    tIp6Addr            ip6Addr;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValOspfv3AreaAggregateRouteTag);

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);
    OSPFV3_IP6_ADDR_COPY (ip6Addr.u1_addr,
                          *(pOspfv3AreaAggregatePrefix->pu1_OctetList));

    if (((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
        && (V3GetFindAreaAddrRange (pArea, &ip6Addr,
                                    (UINT1) i4Ospfv3AreaAggregatePrefixLength,
                                    (UINT2)
                                    i4Ospfv3AreaAggregateAreaLsdbType) != NULL))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3TraceLevel
 Input       :  The Indices

                The Object 
                testValFutOspfv3TraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3TraceLevel (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFutOspfv3TraceLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFutOspfv3TraceLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ABRType
 Input       :  The Indices

                The Object 
                testValFutOspfv3ABRType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ABRType (UINT4 *pu4ErrorCode, INT4 i4TestValFutOspfv3ABRType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_ABR_TYPE (i4TestValFutOspfv3ABRType))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                testValFutOspfv3NssaAsbrDefRtTrans
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3NssaAsbrDefRtTrans (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFutOspfv3NssaAsbrDefRtTrans)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValFutOspfv3NssaAsbrDefRtTrans))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "TruthValue is not valid \n");
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3DefaultPassiveInterface
 Input       :  The Indices

                The Object 
                testValFutOspfv3DefaultPassiveInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3DefaultPassiveInterface (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFutOspfv3DefaultPassiveInterface)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValFutOspfv3DefaultPassiveInterface))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "TruthValue is not valid \n");
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3SpfDelay
 Input       :  The Indices

                The Object 
                testValFutOspfv3SpfDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3SpfDelay (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfv3SpfDelay)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_SPF_OR_HOLD_DELAY (i4TestValFutOspfv3SpfDelay))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3SpfHoldTime
 Input       :  The Indices

                The Object 
                testValFutOspfv3SpfHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3SpfHoldTime (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFutOspfv3SpfHoldTime)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_SPF_OR_HOLD_DELAY (i4TestValFutOspfv3SpfHoldTime))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RestartStrictLsaChecking
 Input       :  The Indices

                The Object
                testValFutOspfv3RestartStrictLsaChecking
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RestartStrictLsaChecking (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFutOspfv3RestartStrictLsaChecking)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfv3RestartStrictLsaChecking != OSIX_ENABLED) &&
        (i4TestValFutOspfv3RestartStrictLsaChecking != OSIX_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) &&
        (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For nmhTestv2FutOspfv3RestartStrictLsaChecking"
                    "failed - Restart InProgress\n");
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    if (pV3OspfCxt->u1HelperSupport == OSPFV3_GRACE_NO_HELPER)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_HELPER_DISABLED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3HelperSupport
 Input       :  The Indices

                The Object
                testValFutOspfv3HelperSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3HelperSupport (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValFutOspfv3HelperSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) &&
        (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For nmhTestv2FutOspfv3HelperSupport"
                    "failed - Restart InProgress\n");
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    if ((*pTestValFutOspfv3HelperSupport->pu1_OctetList == 0) ||
        ((*pTestValFutOspfv3HelperSupport->pu1_OctetList &
          OSPFV3_GRACE_UNKNOWN) ||
         (*pTestValFutOspfv3HelperSupport->pu1_OctetList &
          OSPFV3_GRACE_SW_RESTART) ||
         (*pTestValFutOspfv3HelperSupport->pu1_OctetList &
          OSPFV3_GRACE_SW_RELOADUPGRADE) ||
         (*pTestValFutOspfv3HelperSupport->pu1_OctetList &
          OSPFV3_GRACE_SW_REDUNDANT)))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3HelperGraceTimeLimit
 Input       :  The Indices

                The Object
                testValFutOspfv3HelperGraceTimeLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3HelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFutOspfv3HelperGraceTimeLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfv3HelperGraceTimeLimit != OSPFV3_INIT_VAL) &&
        ((i4TestValFutOspfv3HelperGraceTimeLimit < OSPFV3_GR_MIN_INTERVAL) ||
         (i4TestValFutOspfv3HelperGraceTimeLimit > OSPFV3_GR_MAX_INTERVAL)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) &&
        (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For nmhTestv2FutOspfv3HelperGraceTimeLimit"
                    "failed - Restart InProgress\n");
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->u1HelperSupport == OSPFV3_GRACE_NO_HELPER)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_HELPER_DISABLED);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RestartAckState
 Input       :  The Indices

                The Object
                testValFutOspfv3RestartAckState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RestartAckState (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFutOspfv3RestartAckState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfv3RestartAckState != OSPFV3_ENABLED) &&
        (i4TestValFutOspfv3RestartAckState != OSPFV3_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3GraceLsaRetransmitCount
 Input       :  The Indices

                The Object
                testValFutOspfv3GraceLsaRetransmitCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3GraceLsaRetransmitCount (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFutOspfv3GraceLsaRetransmitCount)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfv3GraceLsaRetransmitCount < OSPFV3_INIT_VAL) ||
        (i4TestValFutOspfv3GraceLsaRetransmitCount > OSPFV3_MAX_GRACE_LSA_SENT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RestartReason
 Input       :  The Indices

                The Object
                testValFutOspfv3RestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RestartReason (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfv3RestartReason)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4TestValFutOspfv3RestartReason < OSPFV3_GR_UNKNOWN) ||
        (i4TestValFutOspfv3RestartReason > OSPFV3_GR_SW_RED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS)
        && (IssGetCsrRestoreFlag () != OSIX_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_RESTART_INPROGRESS);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ExtTraceLevel
 Input       :  The Indices

                The Object
                testValFutOspfv3ExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ExtTraceLevel (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfv3GrTraceLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFutOspfv3GrTraceLevel);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3SetTraps
 Input       :  The Indices

                The Object
                testValFutOspfv3SetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3SetTraps (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFutOspfv3SetTraps)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFutOspfv3SetTraps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3BfdStatus
 Input       :  The Indices

                 The Object 
                 testValFutOspfv3BfdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
Error Codes :   The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3BfdStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFutOspfv3BfdStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3BfdStatus != OSPFV3_BFD_ENABLED) &&
        (i4TestValFutOspfv3BfdStatus != OSPFV3_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3BfdAllIfState
 Input       :  The Indices

                The Object
                testValFutOspfv3BfdAllIfState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3BfdAllIfState (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFutOspfv3BfdAllIfState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3BfdAllIfState != OSPFV3_BFD_ENABLED) &&
        (i4TestValFutOspfv3BfdAllIfState != OSPFV3_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_BFD_ADMIN_STATUS_DISABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RouterIdPermanence
 Input       :  The Indices

                The Object
                testValFutOspfv3RouterIdPermanence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RouterIdPermanence (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFutOspfv3RouterIdPermanence)
{

    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3RouterIdPermanence != OSPFV3_ROUTERID_DYNAMIC) &&
        (i4TestValFutOspfv3RouterIdPermanence != OSPFV3_ROUTERID_PERMANENT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3TraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3TraceLevel (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3ABRType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
     SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3ABRType (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3NssaAsbrDefRtTrans
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3NssaAsbrDefRtTrans (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3DefaultPassiveInterface
Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3DefaultPassiveInterface (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3SpfDelay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3SpfDelay (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3SpfHoldTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
            SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3SpfHoldTime (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RestartStrictLsaChecking
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RestartStrictLsaChecking (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3HelperSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3HelperSupport (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3HelperGraceTimeLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3HelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RestartAckState
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RestartAckState (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3GraceLsaRetransmitCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3GraceLsaRetransmitCount (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RestartReason
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RestartReason (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3ExtTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3ExtTraceLevel (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3SetTraps
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3SetTraps (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3BfdStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3BfdStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3BfdAllIfState
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3BfdAllIfState (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RouterIdPermanence
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RouterIdPermanence (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3IfTable
 Input       :  The Indices
                FutOspfv3IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3IfTable (INT4 i4FutOspfv3IfIndex)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3GetFindIf (i4FutOspfv3IfIndex) != NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Validate Index For IfTable Succeeded\n");
        return SNMP_SUCCESS;
    }

    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Validate Index For IfTable Failed\n");
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfPassive
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                testValFutOspfv3IfPassive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfPassive (UINT4 *pu4ErrorCode,
                             INT4 i4FutOspfv3IfIndex,
                             INT4 i4TestValFutOspfv3IfPassive)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);

    if (pV3OspfCxt->bDefaultPassiveInterface == OSPFV3_ENABLED &&
        i4TestValFutOspfv3IfPassive == OSPFV3_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_DEFAULT_PASSIVE_ENABLED);
        return SNMP_FAILURE;
    }

    if (pInterface != NULL)
    {
        if (OSPFV3_IS_VALID_TRUTH_VALUE (i4TestValFutOspfv3IfPassive))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "TruthValue is not valid \n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                testValFutOspfv3IfLinkLSASuppression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfLinkLSASuppression (UINT4 *pu4ErrorCode,
                                        INT4 i4FutOspfv3IfIndex,
                                        INT4
                                        i4TestValFutOspfv3IfLinkLSASuppression)
{
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);

    if (pInterface != NULL)
    {
        if (OSPFV3_IS_VALID_TRUTH_VALUE
            (i4TestValFutOspfv3IfLinkLSASuppression))
        {
            if (i4TestValFutOspfv3IfLinkLSASuppression == OSPFV3_DISABLED)
            {
                return SNMP_SUCCESS;
            }
            if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
                (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
            {
                return SNMP_SUCCESS;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "Enabling LinkLSAsuppression not allowed in b/c & NBMA N/W\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_LINK_IFTYPE);

            }
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "TruthValue is not valid \n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfBfdState
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object
                testValFutOspfv3IfBfdState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfBfdState (UINT4 *pu4ErrorCode,
                              INT4 i4FutOspfv3IfIndex,
                              INT4 i4TestValFutOspfv3IfBfdState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface = NULL;

    if (i4TestValFutOspfv3IfBfdState != OSPFV3_BFD_ENABLED
        && i4TestValFutOspfv3IfBfdState != OSPFV3_BFD_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    /* Checking BFD Admin status */
    if (pV3OspfCxt == NULL
        || pV3OspfCxt->u1BfdAdminStatus != OSPFV3_BFD_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_BFD_ADMIN_STATUS_DISABLED);
        return SNMP_FAILURE;
    }

    if ((pV3OspfCxt->u1BfdStatusInAllIf == OSPFV3_BFD_ENABLED)
        && (i4TestValFutOspfv3IfBfdState == OSPFV3_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_BFD_ALL_INTF_ENABLED);
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);

    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_BFD_INTF_DISABLED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3IfAuthTable
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3IfAuthTable (INT4 i4FutOspfv3IfAuthIfIndex,
                                              INT4 i4FutOspfv3IfAuthKeyId)
{
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyCnt = 0;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId) == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);

    if (pInterface == NULL)
    {
        return (SNMP_FAILURE);
    }

    i4AuthKeyCnt = TMO_SLL_Count (&(pInterface->sortAuthkeyLst));

    if (i4AuthKeyCnt > OSPFV3_MAX_AUTHKEYS_PER_IF)
    {
        return (SNMP_FAILURE);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKey
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKey (UINT4 *pu4ErrorCode,
                             INT4 i4FutOspfv3IfAuthIfIndex,
                             INT4 i4FutOspfv3IfAuthKeyId,
                             tSNMP_OCTET_STRING_TYPE
                             * pTestValFutOspfv3IfAuthKey)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);

        return SNMP_FAILURE;
    }

    /* Auth key should not be created if authentication is disabled */
    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_AUTH_DISABLED);
        return SNMP_FAILURE;
    }

    if ((pTestValFutOspfv3IfAuthKey->i4_Length <= OSPFV3_MAX_AUTHKEY_LEN)
        && (i4FutOspfv3IfAuthKeyId >= OSPFV3_MIN_AUTHKEY_VALUE)
        && (i4FutOspfv3IfAuthKeyId <= OSPFV3_MAX_AUTHKEY_VALUE))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                    "Test For FutOspfIfAuthKey Succeeds\n");
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For FutOspfv3IfAuthKey Fails\n");
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                        INT4 i4FutOspfv3IfAuthIfIndex,
                                        INT4 i4FutOspfv3IfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pTestValFutOspfv3IfAuthKeyStartAccept)
{
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4Ospf31sTimer = OSPFV3_ZERO;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3IfAuthKeyStartAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) V3UtilGetSecondsSinceBase (tm);
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);

    /* Key StartAccept time should be less than StartGenerate Time */
    if ((pAuthkeyInfo != NULL)
        && (u4Ospf31sTimer < (UINT4) i4AuthKeyStartAccept)
        && (pAuthkeyInfo->u4KeyStartGenerate < (UINT4) i4AuthKeyStartAccept))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STARTACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
    {
        pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
        if (pAuthkeyInfo->u2AuthkeyId != (UINT2) i4FutOspfv3IfAuthKeyId)
        {
            if (pAuthkeyInfo->u4KeyStopAccept > (UINT4) i4AuthKeyStartAccept)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (pInterface->sortAuthkeyLst.u4_Count == 1)
        {
            return SNMP_SUCCESS;
        }
    }

    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For FutOspfv3IfAuthKeyStartAccept Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                          INT4 i4FutOspfv3IfAuthIfIndex,
                                          INT4 i4FutOspfv3IfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValFutOspfv3IfAuthKeyStartGenerate)
{
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    UINT4               u4Ospf31sTimer = OSPFV3_ZERO;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3IfAuthKeyStartGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);

    /* Key StartAccept time should be less than StartGenerate Time */
    if ((pAuthkeyInfo != NULL)
        && (u4Ospf31sTimer < (UINT4) i4AuthKeyStartGenerate)
        && (pAuthkeyInfo->u4KeyStartAccept > (UINT4) i4AuthKeyStartGenerate))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STARTACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
    {
        pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
        if (pAuthkeyInfo->u2AuthkeyId != (UINT2) i4FutOspfv3IfAuthKeyId)
        {
            if (pAuthkeyInfo->u4KeyStopGenerate >
                (UINT4) i4AuthKeyStartGenerate)
            {
                return SNMP_SUCCESS;
            }
        }
        else if (pInterface->sortAuthkeyLst.u4_Count == 1)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For FutOspfv3IfAuthKeyStartGenerate Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                         INT4 i4FutOspfv3IfAuthIfIndex,
                                         INT4 i4FutOspfv3IfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFutOspfv3IfAuthKeyStopGenerate)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3IfAuthKeyStopGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStopGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);

    /* Key StopAccept time should be greater than StopGenerate Time */
    if ((pAuthkeyInfo != NULL) &&
        (pAuthkeyInfo->u4KeyStopAccept < (UINT4) i4AuthKeyStopGenerate))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STOPACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                       INT4 i4FutOspfv3IfAuthIfIndex,
                                       INT4 i4FutOspfv3IfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pTestValFutOspfv3IfAuthKeyStopAccept)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;

    MEMSET (&tm, 0, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3IfAuthKeyStopAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStopAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);

    /* Key StopAccept time should be greater than StopGenerate Time */
    if ((pAuthkeyInfo != NULL) &&
        (pAuthkeyInfo->u4KeyStopGenerate > (UINT4) i4AuthKeyStopAccept))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STOPACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                testValFutOspfv3IfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FutOspfv3IfAuthIfIndex,
                                   INT4 i4FutOspfv3IfAuthKeyId,
                                   INT4 i4TestValFutOspfv3IfAuthKeyStatus)
{
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4KeyCnt = 0;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    UNUSED_PARAM (i4FutOspfv3IfAuthKeyId);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
        return SNMP_FAILURE;
    }

    i4KeyCnt = (INT4) TMO_SLL_Count (&(pInterface->sortAuthkeyLst));

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);

    switch (i4TestValFutOspfv3IfAuthKeyStatus)
    {
        case CREATE_AND_WAIT:
            if (pAuthkeyInfo != NULL)
            {
                return SNMP_SUCCESS;
            }

            if ((i4KeyCnt >= 5) || (MemGetFreeUnits (OSPFV3_AUTH_QID)) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (OSPFV3_CLI_MAX_AUTH_KEY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if ((i4KeyCnt < 1) || (pAuthkeyInfo == NULL))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            break;
        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3IfAuthTable
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3IfAuthTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3VirtIfAuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3VirtIfAuthTable
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3VirtIfAuthTable (UINT4
                                                  u4FutOspfv3VirtIfAuthAreaId,
                                                  UINT4
                                                  u4FutOspfv3VirtIfAuthNeighbor,
                                                  INT4
                                                  i4FutOspfv3VirtIfAuthKeyId)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);
    if (V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                         (UINT2) i4FutOspfv3VirtIfAuthKeyId) !=
        NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKey
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKey (UINT4 *pu4ErrorCode,
                                 UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                 UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                 INT4 i4FutOspfv3VirtIfAuthKeyId,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pTestValFutOspfv3VirtIfAuthKey)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
        return SNMP_FAILURE;
    }
    if ((pTestValFutOspfv3VirtIfAuthKey->i4_Length > OSPFV3_MAX_AUTHKEY_LEN)
        || (i4FutOspfv3VirtIfAuthKeyId > OSPFV3_MAX_AUTHKEY_VALUE))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                    "Test For FutOspfVirtIfMD5AuthKey Fails\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CLI_SET_ERR (OSPFV3_CLI_INV_LENGTH);
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For FutOspfVirtIfMD5AuthKey Succeeds\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                            UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                            UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                            INT4 i4FutOspfv3VirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE
                                            *
                                            pTestValFutOspfv3VirtIfAuthKeyStartAccept)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tTMO_SLL_NODE      *pLstNode;
    tUtlTm              tm;
    INT4                i4AuthKeyStartAccept = 0;
    UINT4               u4Ospf31sTimer = OSPFV3_ZERO;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3VirtIfAuthKeyStartAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);

    /* Key StartAccept time should be less than StartGenerate Time */
    if ((pAuthkeyInfo != NULL)
        && (u4Ospf31sTimer < (UINT4) i4AuthKeyStartAccept)
        && (pAuthkeyInfo->u4KeyStartGenerate < (UINT4) i4AuthKeyStartAccept))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STARTACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if ((pInterface != NULL) && (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH))
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u2AuthkeyId != (UINT2) i4FutOspfv3VirtIfAuthKeyId)
            {
                if (pAuthkeyInfo->u4KeyStopAccept >
                    (UINT4) i4AuthKeyStartAccept)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortAuthkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                              UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                              UINT4
                                              u4FutOspfv3VirtIfAuthNeighbor,
                                              INT4 i4FutOspfv3VirtIfAuthKeyId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pTestValFutOspfv3VirtIfAuthKeyStartGenerate)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tTMO_SLL_NODE      *pLstNode;
    tUtlTm              tm;
    INT4                i4AuthKeyStartGenerate = 0;
    UINT4               u4Ospf31sTimer = OSPFV3_ZERO;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3VirtIfAuthKeyStartGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    MEMSET (&tm, 0, sizeof (tUtlTm));
    UtlGetTime (&tm);
    u4Ospf31sTimer = V3UtilGetSecondsSinceBase (tm);

    /* Key StartAccept time should be less than StartGenerate Time */
    if ((pAuthkeyInfo != NULL)
        && (u4Ospf31sTimer < (UINT4) i4AuthKeyStartGenerate)
        && (pAuthkeyInfo->u4KeyStartAccept > (UINT4) i4AuthKeyStartGenerate))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STARTACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if ((pInterface != NULL) && (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH))
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u2AuthkeyId != (UINT2) i4FutOspfv3VirtIfAuthKeyId)
            {
                if (pAuthkeyInfo->u4KeyStartGenerate >
                    (UINT4) i4AuthKeyStartGenerate)
                {
                    return SNMP_SUCCESS;
                }
            }
            else if (pInterface->sortAuthkeyLst.u4_Count == 1)
            {
                return SNMP_SUCCESS;
            }
        }
    }
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                             UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                             UINT4
                                             u4FutOspfv3VirtIfAuthNeighbor,
                                             INT4 i4FutOspfv3VirtIfAuthKeyId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFutOspfv3VirtIfAuthKeyStopGenerate)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tUtlTm              tm;
    INT4                i4AuthKeyStopGenerate = 0;
    tV3OsInterface     *pInterface;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3VirtIfAuthKeyStopGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStopGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    /* Key StopAccept time should be greater than StopGenerate Time */
    if ((pAuthkeyInfo != NULL) &&
        (pAuthkeyInfo->u4KeyStopAccept < (UINT4) i4AuthKeyStopGenerate))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STOPACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                           UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                           UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                           INT4 i4FutOspfv3VirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValFutOspfv3VirtIfAuthKeyStopAccept)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tUtlTm              tm;
    INT4                i4AuthKeyStopAccept = 0;
    tV3OsInterface     *pInterface;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));

    V3OspfConvertTimeForSnmp (pTestValFutOspfv3VirtIfAuthKeyStopAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStopAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId, &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    /* Key StopAccept time should be greater than StopGenerate Time */
    if ((pAuthkeyInfo != NULL) &&
        (pAuthkeyInfo->u4KeyStopGenerate > (UINT4) i4AuthKeyStopAccept))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_STOPACCEPT);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                testValFutOspfv3VirtIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                       UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                       INT4 i4FutOspfv3VirtIfAuthKeyId,
                                       INT4
                                       i4TestValFutOspfv3VirtIfAuthKeyStatus)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    INT4                i4KeyCnt = 0;
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);
    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_AUTH_DISABLED);
        return SNMP_FAILURE;
    }

    i4KeyCnt = (INT4) TMO_SLL_Count (&(pInterface->sortAuthkeyLst));

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &(nbrId),
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    switch (i4TestValFutOspfv3VirtIfAuthKeyStatus)
    {
        case CREATE_AND_WAIT:
            if (pAuthkeyInfo != NULL)
            {
                return SNMP_SUCCESS;
            }

            if ((i4KeyCnt >= 5) || (MemGetFreeUnits (OSPFV3_AUTH_QID)) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (OSPFV3_CLI_MAX_AUTH_KEY);
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if ((i4KeyCnt < 1) || (pAuthkeyInfo == NULL))
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            break;
        default:
        {
            break;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3VirtIfAuthTable
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3VirtIfAuthTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3VirtIfCryptoAuthTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3VirtIfCryptoAuthTable (UINT4
                                                        u4Ospfv3VirtIfAreaId,
                                                        UINT4
                                                        u4Ospfv3VirtIfNeighbor)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);
    if (V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfCryptoAuthType
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                testValFutOspfv3IfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfCryptoAuthType (UINT4 *pu4ErrorCode,
                                    INT4 i4FutOspfv3IfIndex,
                                    INT4 i4TestValFutOspfv3IfCryptoAuthType)
{
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                 "Test For Ospfv3IfCryptoAuthType %d  i4FutOspfv3IfIndex %d\n",
                 i4TestValFutOspfv3IfCryptoAuthType, i4FutOspfv3IfIndex);

    if ((i4TestValFutOspfv3IfCryptoAuthType >= OSPFV3_MIN_CRYPTOTYPE_VALUE) &&
        (i4TestValFutOspfv3IfCryptoAuthType <= OSPFV3_MAX_CRYPTOTYPE_VALUE))
    {
        pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
        if (pInterface)
        {
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pOspfCxt->u4ContextId,
                        "Test For Ospfv3AuthIfCryptoAuthType Succeeds\n");
            return (SNMP_SUCCESS);
        }
    }
    else
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For OspfIfCryptoAuthType Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                testValFutOspfv3IfCryptoAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3IfCryptoAuthMode (UINT4 *pu4ErrorCode,
                                    INT4 i4FutOspfv3IfIndex,
                                    INT4 i4TestValFutOspfv3IfCryptoAuthMode)
{
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                 "Test For OspfIfCryptoAuthType %d  i4FutOspfv3IfIndex %d\n",
                 i4TestValFutOspfv3IfCryptoAuthMode, i4FutOspfv3IfIndex);

    if ((i4TestValFutOspfv3IfCryptoAuthMode != OSPFV3_AUTH_MODE_TRANS) &&
        (i4TestValFutOspfv3IfCryptoAuthMode != OSPFV3_AUTH_MODE_FULL))
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if ((pInterface != NULL) && (pInterface->u2AuthType == OSPFV3_CRYPT_AUTH))
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId,
                    "Test For Ospfv3AuthIfCryptoAuthType Succeeds\n");
        return (SNMP_SUCCESS);
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC, pOspfCxt->u4ContextId,
                "Test For OspfIfCryptoAuthType Fails\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices                                                                                                       Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3VirtIfCryptoAuthTable (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                testValFutOspfv3VirtIfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfCryptoAuthType (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ospfv3VirtIfAreaId,
                                        UINT4 u4Ospfv3VirtIfNeighbor,
                                        INT4
                                        i4TestValFutOspfv3VirtIfCryptoAuthType)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);
    if (i4TestValFutOspfv3VirtIfCryptoAuthType >= OSPFV3_MIN_CRYPTOTYPE_VALUE &&
        i4TestValFutOspfv3VirtIfCryptoAuthType <= OSPFV3_MAX_CRYPTOTYPE_VALUE)

    {
        pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
        if (!pInterface)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (OSPFV3_CLI_INV_VIRT_AREA);
            return SNMP_FAILURE;
        }
        return (SNMP_SUCCESS);
    }
    else
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                testValFutOspfv3VirtIfCryptoAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3VirtIfCryptoAuthMode (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ospfv3VirtIfAreaId,
                                        UINT4 u4Ospfv3VirtIfNeighbor,
                                        INT4
                                        i4TestValFutOspfv3VirtIfCryptoAuthMode)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (OSPFV3_CLI_INV_VIRT_AREA);
        return SNMP_FAILURE;
    }

    if (pInterface->u2AuthType != OSPFV3_CRYPT_AUTH)
    {
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHTYPE);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3VirtIfCryptoAuthMode == OSPFV3_AUTH_MODE_TRANS) ||
        (i4TestValFutOspfv3VirtIfCryptoAuthMode == OSPFV3_AUTH_MODE_FULL))

    {
        return (SNMP_SUCCESS);
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_AUTHMODE);
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3IfTable
 Input       :  The Indices
                FutOspfv3IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
**************************************************************************/
INT1
nmhDepv2FutOspfv3IfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3RoutingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3RoutingTable
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3RoutingTable (INT4 i4FutOspfv3RouteDestType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfv3RouteDest,
                                               UINT4 i4FutOspfv3RoutePfxLength,
                                               INT4 i4FutOspfv3RouteNextHopType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfv3RouteNextHop)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1NextHopCount = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRtEntry = (tV3OsRtEntry *)
         V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                (VOID *) pFutOspfv3RouteDest->pu1_OctetList,
                                (UINT1) i4FutOspfv3RoutePfxLength,
                                (UINT1) OSPFV3_DEST_NETWORK)) != NULL)
    {
        TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
        {
            for (i1NextHopCount = 0; ((i1NextHopCount < pPath->u1HopCount) &&
                                      (i1NextHopCount < OSPFV3_MAX_NEXT_HOPS));
                 i1NextHopCount++)
            {
                if (V3UtilIp6AddrComp ((tIp6Addr *) (VOID *)
                                       pFutOspfv3RouteNextHop->pu1_OctetList,
                                       &(pPath->aNextHops[i1NextHopCount].
                                         nbrIpv6Addr)) == OSPFV3_EQUAL)
                {
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3AsExternalAggregationTable (INT4
                                                             i4FutOspfv3AsExternalAggregationNetType,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pFutOspfv3AsExternalAggregationNet,
                                                             UINT4
                                                             i4FutOspfv3AsExternalAggregationPfxLength,
                                                             UINT4
                                                             u4FutOspfv3AsExternalAggregationAreaId)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    if (V3GetFindAsExtRngInCxt (pV3OspfCxt,
                                (tIp6Addr *) (VOID *)
                                pFutOspfv3AsExternalAggregationNet->
                                pu1_OctetList,
                                (UINT1)
                                i4FutOspfv3AsExternalAggregationPfxLength,
                                areaId) != NULL)
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FutOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                testValFutOspfv3AsExternalAggregationEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3AsExternalAggregationEffect (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FutOspfv3AsExternalAggregationNetType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfv3AsExternalAggregationNet,
                                               UINT4
                                               i4FutOspfv3AsExternalAggregationPfxLength,
                                               UINT4
                                               u4FutOspfv3AsExternalAggregationAreaId,
                                               INT4
                                               i4TestValFutOspfv3AsExternalAggregationEffect)
{
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tIp6Addr            asExtNet;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1AsExtPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (asExtNet.u1_addr,
                          *(pFutOspfv3AsExternalAggregationNet->pu1_OctetList));
    u1AsExtPfxLen = (UINT1) i4FutOspfv3AsExternalAggregationPfxLength;
    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt, &asExtNet,
                                            u1AsExtPfxLen, areaId);

    if (pAsExtAddrRng == NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, " AS Ext AGG Creation Failure\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pAsExtAddrRng->rangeStatus == ACTIVE)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    " AS Ext AGG Active - Make not in Service\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfv3AsExternalAggregationEffect)
    {
        case OSPFV3_RAG_ADVERTISE:
        case OSPFV3_RAG_DO_NOT_ADVERTISE:
            return SNMP_SUCCESS;

        case OSPFV3_RAG_ALLOW_ALL:
        case OSPFV3_RAG_DENY_ALL:

            if (V3UtilAreaIdComp (pAsExtAddrRng->areaId,
                                  OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
            {
                return SNMP_SUCCESS;
            }
            break;

        default:
            break;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                testValFutOspfv3AsExternalAggregationTranslation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3AsExternalAggregationTranslation (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FutOspfv3AsExternalAggregationNetType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFutOspfv3AsExternalAggregationNet,
                                                    UINT4
                                                    i4FutOspfv3AsExternalAggregationPfxLength,
                                                    UINT4
                                                    u4FutOspfv3AsExternalAggregationAreaId,
                                                    INT4
                                                    i4TestValFutOspfv3AsExternalAggregationTranslation)
{
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tIp6Addr            asExtNet;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1AsExtPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (asExtNet.u1_addr,
                          *(pFutOspfv3AsExternalAggregationNet->pu1_OctetList));
    u1AsExtPfxLen = (UINT1) i4FutOspfv3AsExternalAggregationPfxLength;
    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt, &asExtNet,
                                            u1AsExtPfxLen, areaId);

    if (pAsExtAddrRng == NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, " AS Ext AGG Creation Failure\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if (pAsExtAddrRng->rangeStatus == ACTIVE)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    " AS Ext AGG Active - Make not in Service\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
        return SNMP_FAILURE;
    }

    if (((UINT1) i4TestValFutOspfv3AsExternalAggregationTranslation
         == OSPFV3_ENABLED) ||
        ((UINT1) i4TestValFutOspfv3AsExternalAggregationTranslation
         == OSPFV3_DISABLED))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                testValFutOspfv3AsExternalAggregationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3AsExternalAggregationStatus (UINT4 *pu4ErrorCode,
                                               INT4
                                               i4FutOspfv3AsExternalAggregationNetType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfv3AsExternalAggregationNet,
                                               UINT4
                                               i4FutOspfv3AsExternalAggregationPfxLength,
                                               UINT4
                                               u4FutOspfv3AsExternalAggregationAreaId,
                                               INT4
                                               i4TestValFutOspfv3AsExternalAggregationStatus)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tIp6Addr            asExtNet;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1AsExtPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (asExtNet.u1_addr,
                          *(pFutOspfv3AsExternalAggregationNet->pu1_OctetList));
    u1AsExtPfxLen = (UINT1) i4FutOspfv3AsExternalAggregationPfxLength;

    if (i4TestValFutOspfv3AsExternalAggregationStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);
    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA);
        return SNMP_FAILURE;
    }

    /* The external aggregation for non NSSA area is not allowed */
    if ((V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID)
         != OSPFV3_EQUAL) && (pArea->u4AreaType != OSPFV3_NSSA_AREA))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_AREA_TYPE);
        return SNMP_FAILURE;
    }

    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt, &asExtNet,
                                            u1AsExtPfxLen, areaId);

    switch ((UINT1) i4TestValFutOspfv3AsExternalAggregationStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:

            if (pAsExtAddrRng != NULL)
            {
                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " Same AS Ext Aggregation Range Exist\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
            if (pAsExtAddrRng == NULL)
            {
                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " Same AS Ext Aggregation Range Exist\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:
            if (pAsExtAddrRng == NULL)
            {
                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " Same AS Ext Aggregation Range Exist\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            else if (pAsExtAddrRng->rangeStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        default:
            break;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2Ospfv3NbmaNbrTable
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ospfv3NbmaNbrTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3AsExternalAggregationTable (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3BRRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3BRRouteTable
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3BRRouteTable (UINT4 u4FutOspfv3BRRouteDest,
                                               INT4
                                               i4FutOspfv3BRRouteNextHopType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFutOspfv3BRRouteNextHop,
                                               INT4 i4FutOspfv3BRRouteDestType)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tIp6Addr            nextHopIpAddr;
    tV3OsPath          *pPath = NULL;
    tV3OsRouterId       destIpAddr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1NextHopCount = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteDestType == OSPFV3_DEST_NETWORK)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (destIpAddr, u4FutOspfv3BRRouteDest);
    OSPFV3_IP6_ADDR_COPY (nextHopIpAddr.u1_addr,
                          *(pFutOspfv3BRRouteNextHop->pu1_OctetList));
    if (i4FutOspfv3BRRouteNextHopType == OSPFV3_INET_ADDR_TYPE)
    {
        if ((pRtEntry = (tV3OsRtEntry *) V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                                                (VOID *)
                                                                &destIpAddr, 0,
                                                                (UINT1)
                                                                i4FutOspfv3BRRouteDestType))
            != NULL)
        {
            TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
            {
                for (i1NextHopCount = 0;
                     ((i1NextHopCount < pPath->u1HopCount) &&
                      (i1NextHopCount < OSPFV3_MAX_NEXT_HOPS));
                     i1NextHopCount++)
                {
                    if (V3UtilIp6AddrComp (&nextHopIpAddr,
                                           &pPath->aNextHops
                                           [i1NextHopCount].nbrIpv6Addr) ==
                        OSPFV3_EQUAL)
                    {
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfv3RedistRouteCfgTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFutOspfv3RedistRouteCfgTable (INT4
                                                      i4FutOspfv3RedistRouteDestType,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFutOspfv3RedistRouteDest,
                                                      UINT4
                                                      i4FutOspfv3RedistRoutePfxLength)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));
    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;
    if (i4FutOspfv3RedistRouteDestType == OSPFV3_INET_ADDR_TYPE)
    {
        if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                         u1RrdDestPfxLen) != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3NeighborBfdTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3NeighborBfdTable (INT4
                                                   i4Ospfv3NbrIfIndex,
                                                   UINT4 u4Ospfv3NbrRtrId)
{
    tV3OsRouterId       nbrRtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if (V3GetFindNbrInCxt (pV3OspfCxt, &nbrRtrId,
                           (UINT4) i4Ospfv3NbrIfIndex) != NULL)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RedistRouteMetric
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                testValFutOspfv3RedistRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RedistRouteMetric (UINT4 *pu4ErrorCode,
                                     INT4 i4FutOspfv3RedistRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFutOspfv3RedistRouteDest,
                                     UINT4 i4FutOspfv3RedistRoutePfxLength,
                                     INT4 i4TestValFutOspfv3RedistRouteMetric)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_BIG_METRIC (i4TestValFutOspfv3RedistRouteMetric) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));
    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;
    if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                     u1RrdDestPfxLen) != NULL)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RedistRouteMetricType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                testValFutOspfv3RedistRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RedistRouteMetricType (UINT4 *pu4ErrorCode,
                                         INT4 i4FutOspfv3RedistRouteDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFutOspfv3RedistRouteDest,
                                         UINT4 i4FutOspfv3RedistRoutePfxLength,
                                         INT4
                                         i4TestValFutOspfv3RedistRouteMetricType)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (OSPFV3_IS_VALID_EXT_METRIC_TYPE
        (i4TestValFutOspfv3RedistRouteMetricType) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));

    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;

    if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                     u1RrdDestPfxLen) != NULL)
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RedistRouteTagType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                testValFutOspfv3RedistRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RedistRouteTagType (UINT4 *pu4ErrorCode,
                                      INT4 i4FutOspfv3RedistRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3RedistRouteDest,
                                      UINT4 i4FutOspfv3RedistRoutePfxLength,
                                      INT4 i4TestValFutOspfv3RedistRouteTagType)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!OSPFV3_IS_VALID_RRD_TAG_TYPE_VALUE
        (i4TestValFutOspfv3RedistRouteTagType))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));
    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;
    if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                     u1RrdDestPfxLen) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RedistRouteTag
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                testValFutOspfv3RedistRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RedistRouteTag (UINT4 *pu4ErrorCode,
                                  INT4 i4FutOspfv3RedistRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3RedistRouteDest,
                                  UINT4 i4FutOspfv3RedistRoutePfxLength,
                                  INT4 i4TestValFutOspfv3RedistRouteTag)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFutOspfv3RedistRouteTag);
    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_NO_ASB_RTR);
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));
    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                      u1RrdDestPfxLen)) != NULL)
    {
        if (pRrdConfRtInfo->u1RedistrTagType != (UINT1) OSPFV3_MANUAL_TAG)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_TAGTYPE);
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RedistRouteStatus
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                testValFutOspfv3RedistRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RedistRouteStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FutOspfv3RedistRouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFutOspfv3RedistRouteDest,
                                     UINT4 i4FutOspfv3RedistRoutePfxLength,
                                     INT4 i4TestValFutOspfv3RedistRouteStatus)
{
    tIp6Addr            rrdDestIP6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1RrdDestPfxLen = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (rrdDestIP6Addr.u1_addr,
                          *(pFutOspfv3RedistRouteDest->pu1_OctetList));
    u1RrdDestPfxLen = (UINT1) i4FutOspfv3RedistRoutePfxLength;

    if (i4TestValFutOspfv3RedistRouteStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4TestValFutOspfv3RedistRouteStatus)
    {

        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                             u1RrdDestPfxLen) != NULL)
            {
                /* row already exists */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case ACTIVE:
        case NOT_IN_SERVICE:
            if (V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, &rrdDestIP6Addr,
                                             u1RrdDestPfxLen) != NULL)
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (OSPFV3_CLI_INV_STATUS);
            return SNMP_FAILURE;

    }                            /* switch */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RedistRouteCfgTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RRDStatus
 Input       :  The Indices

                The Object 
                testValFutOspfv3RRDStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RRDStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFutOspfv3RRDStatus)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_NO_ASB_RTR);
        return SNMP_FAILURE;
    }

    if (!OSPFV3_IS_VALID_RRD_STATUS_VALUE (i4TestValFutOspfv3RRDStatus))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4TestValFutOspfv3RRDStatus);

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
#endif /* RRD_WANTED */
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RRDSrcProtoMask
 Input       :  The Indices

                The Object 
                testValFutOspfv3RRDSrcProtoMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RRDSrcProtoMask (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFutOspfv3RRDSrcProtoMask)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt))
        || (pV3OspfCxt->redistrAdmnStatus != OSPFV3_ENABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_REDIS_DISABLED);
        return SNMP_FAILURE;
    }

    if (!OSPFV3_IS_VALID_RRD_SRC_PROTO_MASK_VALUE
        (i4TestValFutOspfv3RRDSrcProtoMask))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4TestValFutOspfv3RRDSrcProtoMask);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    return SNMP_FAILURE;
#endif /* RRD_WANTED */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RRDStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RRDStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RRDSrcProtoMask
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RRDSrcProtoMask (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RTStaggeringInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RTStaggeringInterval (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RTStaggeringStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RTStaggeringStatus (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RTStaggeringInterval
 Input       :  The Indices

                The Object
                testValFutOspfv3RTStaggeringInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RTStaggeringInterval (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFutOspfv3RTStaggeringInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (i4TestValFutOspfv3RTStaggeringInterval < OSPFV3_MIN_STAGGERING_INTERVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object
                testValFutOspfv3RTStaggeringStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RTStaggeringStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFutOspfv3RTStaggeringStatus)
{

    if ((OSPFV3_STAGGERING_ENABLED != i4TestValFutOspfv3RTStaggeringStatus) &&
        (OSPFV3_STAGGERING_DISABLED != i4TestValFutOspfv3RTStaggeringStatus))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RRDRouteMapName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RRDRouteMapName (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3RRDMetricTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3RRDMetricTable
 Input       :  The Indices
                FutOspfv3RRDProtocolId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3RRDMetricTable (INT4 i4FutOspfv3RRDProtocolId)
{
    UNUSED_PARAM (i4FutOspfv3RRDProtocolId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RRDMetricValue
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object
                testValFutOspfv3RRDMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RRDMetricValue (UINT4 *pu4ErrorCode,
                                  INT4 i4FutOspfv3RRDProtocolId,
                                  INT4 i4TestValFutOspfv3RRDMetricValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1Return = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#ifdef RRD_WANTED
    if (pV3OspfCxt->redistrAdmnStatus != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_REDIS_DISABLED);
        return SNMP_FAILURE;
    }
#endif
    if ((i4FutOspfv3RRDProtocolId <= 0) || (i4FutOspfv3RRDProtocolId >
                                            OSPFV3_MAX_PROTO_REDISTRUTE_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3RRDMetricValue < OSPFV3_MIN_METRIC_VALUE)
        || (i4TestValFutOspfv3RRDMetricValue > OSPFV3_MAX_METRIC_VALUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RRDMetricType
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object
                testValFutOspfv3RRDMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RRDMetricType (UINT4 *pu4ErrorCode,
                                 INT4 i4FutOspfv3RRDProtocolId,
                                 INT4 i4TestValFutOspfv3RRDMetricType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1Return = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

#ifdef RRD_WANTED

    if (pV3OspfCxt->redistrAdmnStatus != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_REDIS_DISABLED);
        return SNMP_FAILURE;
    }

#endif
    if ((i4FutOspfv3RRDProtocolId <= 0) || (i4FutOspfv3RRDProtocolId >
                                            OSPFV3_MAX_PROTO_REDISTRUTE_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFutOspfv3RRDMetricType != OSPFV3_TYPE_1_METRIC)
        && (i4TestValFutOspfv3RRDMetricType != OSPFV3_TYPE_2_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3RRDRouteMapName
 Input       :  The Indices

                The Object
                testValFutOspfRRDRouteMapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3RRDRouteMapName (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pTestValFutOspfv3RRDRouteMapEnable)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (OSPFV3_CLI_INV_ASSOC_RMAP);
        return SNMP_FAILURE;
    }

    if (pTestValFutOspfv3RRDRouteMapEnable->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->bAsBdrRtr != OSIX_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_NO_ASB_RTR);
        return SNMP_FAILURE;
    }
    if (pV3OspfCxt->redistrAdmnStatus != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_ASSOC_RMAP);
        return SNMP_FAILURE;
    }

    if (pTestValFutOspfv3RRDRouteMapEnable->i4_Length != 0)
    {
        if (STRLEN (pV3OspfCxt->au1RMapName) != 0)
        {
            /* Check for existence of Route Map with different Name.
             * If So, return failure, */
            if ((UINT4) pTestValFutOspfv3RRDRouteMapEnable->i4_Length !=
                (STRLEN (pV3OspfCxt->au1RMapName)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
            if (MEMCMP (pV3OspfCxt->au1RMapName,
                        pTestValFutOspfv3RRDRouteMapEnable->pu1_OctetList,
                        pTestValFutOspfv3RRDRouteMapEnable->i4_Length) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFutOspfv3RRDRouteMapEnable);
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : FutOspfv3DistInOutRouteMapTable. */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                testValFutOspfv3DistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3DistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFutOspfv3DistInOutRouteMapName,
                                          INT4 i4FutOspfv3DistInOutRouteMapType,
                                          INT4
                                          i4TestValFutOspfv3DistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pFutOspfv3DistInOutRouteMapName == NULL
        || pFutOspfv3DistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFutOspfv3DistInOutRouteMapValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistanceFilterRMap,
             pFutOspfv3DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistributeInFilterRMap,
             pFutOspfv3DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFutOspfv3DistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfv3DistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFutOspfv3DistInOutRouteMapValue);
#endif /*ROUTEMAP_WANTED */

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                testValFutOspfv3DistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3DistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFutOspfv3DistInOutRouteMapName,
                                              INT4
                                              i4FutOspfv3DistInOutRouteMapType,
                                              INT4
                                              i4TestValFutOspfv3DistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1Exists = 0;
    INT1                i1Match = 0;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (pFutOspfv3DistInOutRouteMapName == NULL
        || pFutOspfv3DistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)

    {
        if (pV3OspfCxt->pDistanceFilterRMap != NULL)
        {
            i1Exists = 1;
            if (V3OspfCmpFilterRMapName
                (pV3OspfCxt->pDistanceFilterRMap,
                 pFutOspfv3DistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (pV3OspfCxt->pDistributeInFilterRMap != NULL)
        {
            i1Exists = 1;
            if (V3OspfCmpFilterRMapName
                (pV3OspfCxt->pDistributeInFilterRMap,
                 pFutOspfv3DistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFutOspfv3DistInOutRouteMapRowStatus)
    {
        case ACTIVE:
        case DESTROY:
            if (i1Match)
            {
                return SNMP_SUCCESS;
            }
            else if (i1Exists)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            break;
        case CREATE_AND_WAIT:
            if (!i1Exists)
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFutOspfv3DistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfv3DistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFutOspfv3DistInOutRouteMapRowStatus);
#endif /*ROUTEMAP_WANTED */

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhDepv2FutOspfv3RRDMetricTable
 Input       :  The Indices
                FutOspfv3RRDProtocolId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3RRDMetricTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3DistInOutRouteMapTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspf3PreferenceValue
 Input       :  The Indices

                The Object 
                testValFutOspf3PreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspf3PreferenceValue (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFutOspf3PreferenceValue)
{
    if (i4TestValFutOspf3PreferenceValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FutOspf3PreferenceValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspf3PreferenceValue (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ospfv3AreaDfInfOriginate
 Input       :  The Indices
                Ospfv3AreaId

                The Object
                testValOspfv3AreaDfInfOriginate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ospfv3AreaDfInfOriginate (UINT4 *pu4ErrorCode, UINT4 u4Ospfv3AreaId,
                                   INT4 i4TestValOspfv3AreaDfInfOriginate)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    OSPFV3_TRC2 (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Test For Ospfv3AreaDfInfOriginate %d Index AreaId %x \n",
                 i4TestValOspfv3AreaDfInfOriginate, u4Ospfv3AreaId);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (pV3OspfCxt->admnStat != OSPFV3_ENABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For Ospfv3AreaDfInfOriginate Failure, OSPFV3 "
                    "is not enabled\n");
        CLI_SET_ERR (OSPFV3_CLI_RTR_NOT_ENABLED);
        return SNMP_FAILURE;
    }

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Configuration is not allowed on Backbone\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_BACKBONE_CONF);
        return SNMP_FAILURE;
    }

    switch (i4TestValOspfv3AreaDfInfOriginate)
    {

        case (OSPFV3_DEFAULT_INFO_ORIGINATE):
        case (OSPFV3_NO_DEFAULT_INFO_ORIGINATE):
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For Ospfv3AreaDfInfOriginate Failure\n"
                        "DefaultOriginate is not valid\n");
            CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
            return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ClearProcess
 Input       :  The Indices

                The Object
                testValFutOspfv3ClearProcess
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ClearProcess (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFutOspfv3ClearProcess)
{

    if (i4TestValFutOspfv3ClearProcess == OSPFV3_FALSE)
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3ClearProcess
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3ClearProcess (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  o3test.c                       */
/*------------------------------------------------------------------------*/
