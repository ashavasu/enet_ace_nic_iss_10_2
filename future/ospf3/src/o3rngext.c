/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rngext.c,v 1.9 2018/01/02 09:37:09 siva Exp $
 *
 * Description:This file contains procedures for creating and
 *             deleting Extrenal Address Ranges. 
 *             
 *
 *******************************************************************/
#include "o3inc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID
    V3AreaDelBkBoneAddrRangeInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                          tV3OsAsExtAddrRange * pAsExtRange));
PRIVATE VOID
    V3AreaDelNssaAddrRangeInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                        tV3OsAsExtAddrRange * pNssaExtRange));
/************************************************************************/
/*                                                                      */
/* Function       : V3AreaExtAddrRangeAttrChngInCxt                     */
/*                                                                      */
/* Description    : Handles attribute change for configured range       */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pAsExtRange     : Pointer to Extrenal Address Range */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3AreaExtAddrRangeAttrChngInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tV3OsAsExtAddrRange * pAsExtRange)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaExtAddrRangeAttrChng\n");

    if (pAsExtRange->u1AttrMask == 0)
    {
        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    " NO Change in Attributes \n");
        return;
    }
    /* Add the range again  */
    V3AreaAddExtAddrRangeInCxt (pV3OspfCxt, pAsExtRange);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaExtAddrRangeAttrChng\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaAsExtAddDefaultValuesInCxt                    */
/*                                                                      */
/* Description    : Creates As Ext Entry with default                   */
/*                  values                                              */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pIp6Addr        : Pointer to IPv6  Address          */
/*                  u1PrefixLength  : Prefix Length                     */
/*                  u4AreaId        : Area Id associated with Range     */
/*                                                                      */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/*                                                                      */
/* Returns        : As Ext range OR NULL                                */
/*                                                                      */
/*                                                                      */
/************************************************************************/

PUBLIC tV3OsAsExtAddrRange *
V3AreaAsExtAddDefaultValuesInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * ip6Addr,
                                  UINT1 u1PrefixLength, UINT4 u4AreaId)
{
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaAsExtAddDefaultValues\n");

    OSPFV3_AS_EXT_AGG_ALLOC (&(pAsExtAddrRng));
    if (NULL == pAsExtAddrRng)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                    "AS EXT AGG Alloc Failed\n");
        return NULL;
    }

    OSPFV3_IP6_ADDR_COPY ((pAsExtAddrRng->ip6Addr), *ip6Addr);
    pAsExtAddrRng->u1PrefixLength = u1PrefixLength;

    OSPFV3_BUFFER_DWTOPDU (pAsExtAddrRng->areaId, u4AreaId);
    pAsExtAddrRng->u1AggTranslation = OSIX_TRUE;
    if (V3UtilAreaIdComp (pAsExtAddrRng->areaId, OSPFV3_BACKBONE_AREAID)
        != OSPFV3_EQUAL)
    {
        pAsExtAddrRng->u1RangeEffect = OSPFV3_RAG_ADVERTISE;
    }
    else
    {
        pAsExtAddrRng->u1RangeEffect = OSPFV3_RAG_ALLOW_ALL;
    }

    if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
        (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
    {
        O3GrSetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pAsExtAddrRng,
                                  OSPFV3_COND_AS_EXT_LSA);
    }
    else
    {
        V3SetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pAsExtAddrRng,
                                OSPFV3_COND_AS_EXT_LSA);
    }

    pAsExtAddrRng->rangeStatus = CREATE_AND_WAIT;
    pAsExtAddrRng->u1AttrMask = 0;

    (pAsExtAddrRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
    (pAsExtAddrRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

    TMO_SLL_Init (&(pAsExtAddrRng->extRtLst));
    TMO_SLL_Init_Node (&(pAsExtAddrRng->nextAsExtAddrRange));
    TMO_SLL_Init_Node (&(pAsExtAddrRng->nextAsExtAddrRngInRtr));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaAsExtAddDefaultValues\n");
    return pAsExtAddrRng;
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaAddNssaAddrRangeInCxt                         */
/*                                                                      */
/* Description    : Adds the range in NSSA area sorted order            */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pAsExtRange     : Pointer to new Range              */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/*                                                                      */
/************************************************************************/

PRIVATE VOID
V3AreaAddNssaAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                             tV3OsAsExtAddrRange * pAsExtRange)
{

    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tTMO_SLL_NODE      *pPrev = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsAsExtAddrRange *pNxtAsExtRng = NULL;
    tV3OsAsExtAddrRange *pPrvAsExtRng = NULL;
    UINT1               u1NssaLesMtch = OSIX_FALSE;
    UINT1               u1NssaMoreMtch = OSIX_FALSE;
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT1               u1LesSpecBkbone = OSIX_FALSE;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaAddNssaAddrRange\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));

    if ((pArea == NULL) || (pArea->u4AreaType != OSPFV3_NSSA_AREA))
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    " Area type not  NSSA - Invalid addition \n");
        return;
    }

    V3AreaInsertRngInAreaLst (pAsExtRange, pArea);
    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        return;
    }

    pNext = TMO_SLL_Next (&(pArea->type7AddrRangeLst),
                          &(pAsExtRange->nextAsExtAddrRange));

    pNxtAsExtRng = (tV3OsAsExtAddrRange *) (pNext);

    /* If Nxt range in list is less specific than the added range,
       update the new range cost/type by scanning Ext Rt table
       and also update less specific range cost/type by removing 
       Ext Rts now falling in new range 
     */
    if ((pNxtAsExtRng != NULL) &&
        (V3RagCompAddrRng (pAsExtRange, pNxtAsExtRng) == OSPFV3_GREATER))
    {
        V3RagScanRtTableForAddrRangesInCxt (pV3OspfCxt, pAsExtRange,
                                            pNxtAsExtRng);
        V3RagProcessNssaRange (pAsExtRange, pArea);

        /* Take care if less spec has no routes falling 
           in it because of this processing
         */

        V3RagProcessNssaRange (pNxtAsExtRng, pArea);
        u1NssaLesMtch = OSIX_TRUE;
    }
    else if (pNxtAsExtRng != NULL)
    {
        /* Check if the previous range is more specific */
        pPrev = TMO_SLL_Previous (&(pArea->type7AddrRangeLst),
                                  &(pAsExtRange->nextAsExtAddrRange));
        pPrvAsExtRng = (tV3OsAsExtAddrRange *) (pPrev);

        if ((pPrvAsExtRng != NULL) &&
            (V3RagCompAddrRng (pPrvAsExtRng, pAsExtRange) == OSPFV3_GREATER))
        {
            /* Update new list for routes falling in it, but not subsumed 
               by more specific NSSA range 
             */
            V3RagScanRtTableForAddrRangesInCxt (pV3OspfCxt,
                                                pAsExtRange, pNxtAsExtRng);
            V3RagProcessNssaRange (pAsExtRange, pArea);
            u1NssaMoreMtch = OSIX_TRUE;
        }
    }

    /* If there is no more specific NSSA range present 
       update cost/type for range by scanning Ext Rt table
     */

    if (u1NssaMoreMtch == OSIX_FALSE)
    {
        /* Sincee the address range belongs to NSSA,
         *  update cost/type of the range
         * without attaching any Ext Rt
         */
        V3RagUpdtRtLstFromExtRtTableInCxt (pV3OspfCxt, pAsExtRange);
    }
    if (u1NssaLesMtch == OSIX_FALSE)
    {
        /* Scan through Bkbone ranges to find any less specific
           bkbone range
         */
        TMO_SLL_Scan (&(pV3OspfCxt->pBackbone->asExtAddrRangeLst),
                      pScanAsExtRng, tV3OsAsExtAddrRange *)
        {
            /* Check if range is less specific than newly added 
               NSSA range
             */
            if (V3RagCompAddrRng (pAsExtRange, pScanAsExtRng) == OSPFV3_GREATER)
            {
                (pScanAsExtRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
                (pScanAsExtRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

                u1LesSpecBkbone = OSIX_TRUE;

                /* Update cost/type for Bkbone range by 
                   not taking into account
                   Ext Rt falling in newly added NSSA range
                 */

                TMO_SLL_Scan (&(pScanAsExtRng->extRtLst),
                              pExtRtNode, tTMO_SLL_NODE *)
                {
                    pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                                     nextExtRouteInRange,
                                                     pExtRtNode);

                    /* If satisfied, implies Ext Rt falls 
                       in more specific NSSA range
                     */
                    if (V3RagIsExtRtFallInRange (pExtRoute, pAsExtRange)
                        == OSIX_TRUE)
                    {
                        /* Skip this Ext Rt */
                        continue;
                    }
                    else
                    {
                        V3RagUpdatCostTypeForRange (pScanAsExtRng, pExtRoute);
                    }
                }                /* End of TMO_SLL_Scan of Extrenal Range */

            }                    /* less Spec Bkbone Range found */

            if (u1LesSpecBkbone == OSIX_TRUE)
            {
                break;
            }
        }                        /* Scan */

        if (u1LesSpecBkbone == OSIX_TRUE)
        {
            /* Process both the ranges    */
            V3RagProcessNssaRange (pAsExtRange, pArea);
            V3RagProcessNssaRange (pScanAsExtRng, pArea);

            /* Restore backbone range cost/type   */
            V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pScanAsExtRng);
        }
    }

    /* Handles the case when :
     *  No less specific NSSA range is present
     *  No more specific NSSA range is present
     * No less specific Bkbone range is present
     */
    if ((u1LesSpecBkbone == OSIX_FALSE) &&
        (u1NssaLesMtch == OSIX_FALSE) && (u1NssaMoreMtch == OSIX_FALSE))
    {
        V3RagProcessNssaRange (pAsExtRange, pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaAddNssaAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaAddExtAddrRangeInCxt                          */
/*                                                                      */
/* Description    : This procedure handles addition                     */
/*                  of new AS Ext Address Range                         */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pAsExtRange     : Pointer to new Range              */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3AreaAddExtAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                            tV3OsAsExtAddrRange * pAsExtRange)
{

    tV3OsArea          *pArea = NULL;
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT1               u1RngFnd = OSIX_FALSE;
    UINT1               u1MoreLessNssaExist = OSIX_FALSE;
    UINT1               u1LessSpecBkBoneRng = OSIX_FALSE;
    INT1                i1RetStat = OSIX_FALSE;
    tV3OsAsExtAddrRange *pNxtAsExtRng = NULL;
    tV3OsAsExtAddrRange *pNxtNssaRng = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaAddExtAddrRange\n");

    if (V3AreaAddRangeInRtLstInCxt (pV3OspfCxt, pAsExtRange) == OSIX_FALSE)
    {
        OSPFV3_AS_EXT_AGG_FREE (pAsExtRange);
        return;
    }

    /* If range is not active it is not added to its 
       area lst.
     */
    if (pAsExtRange->rangeStatus != ACTIVE)
    {
        return;
    }

    /* If range belongs to Bkbone */

    if (V3UtilAreaIdComp (pAsExtRange->areaId,
                          OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        i1RetStat = V3AreaInsertRngInAreaLst (pAsExtRange,
                                              pV3OspfCxt->pBackbone);
        if (i1RetStat == OSIX_FAILURE)
        {
            return;
        }
        if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
        {
            /* Validation of Less/More specific range and
             * LSA orgination/flush operations will be done at the end of GR */
            return;
        }

        /* Check if the next range in Bkbone link list is less specific 
           than the newly added range
         */

        pNext = TMO_SLL_Next (&(pV3OspfCxt->pBackbone->asExtAddrRangeLst),
                              &(pAsExtRange->nextAsExtAddrRange));
        pNxtAsExtRng = (tV3OsAsExtAddrRange *) (pNext);

        if ((pNxtAsExtRng != NULL)
            && (V3RagCompAddrRng (pAsExtRange, pNxtAsExtRng) == OSPFV3_GREATER))
        {
            /* If next list is less specific, move  
               the routes falling in less specific 
               range, which also falls in newly added 
               range, to the link list of 
               Ext Rt  maintained in new range 
             */
            V3RagUpdtRtLstBtwRangesInCxt (pV3OspfCxt, pAsExtRange,
                                          pNxtAsExtRng, OSIX_TRUE);

            /* Now process  both the Bkbone ranges, taking care of changes
               in Rt cost/Type if any, which should be reflected in Type5 
               LSA
             */
            V3RagProcessBkBoneRangeInCxt (pV3OspfCxt, pAsExtRange);
            V3RagProcessBkBoneRangeInCxt (pV3OspfCxt, pNxtAsExtRng);
            u1LessSpecBkBoneRng = OSIX_TRUE;
        }
        else
        {
            /* Since there is no less specific range, new range is updated 
             * with Ext Rts subsumed by it, by scanning Ext Rt Table
             */
            V3RagUpdtRtLstFromExtRtTableInCxt (pV3OspfCxt, pAsExtRange);

            /* Now process the newly added Bkbone range   */
            V3RagProcessBkBoneRangeInCxt (pV3OspfCxt, pAsExtRange);

        }

        /* NSSA areas are scanned to check if addition 
           of new bkbone requires generation of Type 7
           LSA in NSSA, based on Bkbone range
         */

        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
            {
                TMO_SLL_Scan (&(pArea->type7AddrRangeLst),
                              pScanAsExtRng, tV3OsAsExtAddrRange *)
                {

                    /* Check if there exists a equal NSSA range */
                    if ((V3RagCompAddrRng (pAsExtRange, pScanAsExtRng)
                         == OSPFV3_EQUAL) ||
                        (V3RagCompAddrRng (pAsExtRange,
                                           pScanAsExtRng) == OSPFV3_GREATER))
                    {
                        u1MoreLessNssaExist = OSIX_TRUE;
                        break;
                    }
                }

                /* if Less/Equal NSSA range exists move 
                   on to next NSSA area 
                 */
                if (u1MoreLessNssaExist == OSIX_TRUE)
                {
                    u1MoreLessNssaExist = OSIX_FALSE;
                    continue;
                }

                TMO_SLL_Scan (&(pArea->type7AddrRangeLst),
                              pScanAsExtRng, tV3OsAsExtAddrRange *)
                {

                    /* Get the first more specific NSSA range */
                    if (V3RagCompAddrRng (pScanAsExtRng,
                                          pAsExtRange) == OSPFV3_GREATER)
                    {

                        /* If this condition is satisfied, 
                           it implies next range in NSSA area
                           is also more specific than newly 
                           added Bkbone range    
                         */

                        pNext = TMO_SLL_Next (&(pArea->type7AddrRangeLst),
                                              &(pScanAsExtRng->
                                                nextAsExtAddrRange));
                        pNxtNssaRng = (tV3OsAsExtAddrRange *) (pNext);

                        if ((pNxtNssaRng != NULL) &&
                            (V3RagCompAddrRng (pNxtNssaRng, pAsExtRange)
                             == OSPFV3_GREATER))
                        {
                            continue;
                        }

                        /* Implies we have got the 
                           first more specific range 
                           in NSSA 
                         */
                        else
                        {
                            /* Intialising the cost of Extrenal Range */
                            (pAsExtRange->metric).u4MetricType
                                = OSPFV3_TYPE_1_METRIC;
                            (pAsExtRange->metric).u4Metric
                                = OSPFV3_LS_INFINITY_24BIT;

                            /* Check if any extRoute lie in 
                               more specific NSSA range. If 
                               so don't consider it. Move to 
                               next route.In the end check if 
                               any aggregated LSA is to be generated 
                               based on newRange IF YES take action 
                               based on this new range         
                             */

                            TMO_SLL_Scan (&(pAsExtRange->extRtLst),
                                          pExtRtNode, tTMO_SLL_NODE *)
                            {
                                pExtRoute =
                                    OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                                         nextExtRouteInRange,
                                                         pExtRtNode);

                                /* If satisfied, implies Ext Rt 
                                   falls in more specific NSSA
                                   range
                                 */
                                if (V3RagIsExtRtFallInRange (pExtRoute,
                                                             pScanAsExtRng) ==
                                    OSIX_TRUE)
                                {
                                    /* Skip this Ext Rt */
                                    continue;
                                }

                                /* Update cost/Path Type for 
                                   bkbone range only for Ext Rt 
                                   which are not falling in more 
                                   specific NSSA range
                                 */
                                else
                                {
                                    V3RagUpdatCostTypeForRange (pAsExtRange,
                                                                pExtRoute);
                                }
                            }    /* TMO_SLL_SCAN Route List */

                            u1RngFnd = OSIX_TRUE;
                            V3RagProcessNssaRange (pAsExtRange, pArea);
                            V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt,
                                                             pAsExtRange);
                        }
                    }

                    if (u1RngFnd == OSIX_TRUE)
                    {
                        break;
                    }
                }
                /* Implies there does not exist any
                 * NSSA range which should be considered
                 * before processing NSSA area using this 
                 * bkbone range
                 */
                if ((u1MoreLessNssaExist == OSIX_FALSE) &&
                    (u1RngFnd == OSIX_FALSE))
                {
                    if ((u1LessSpecBkBoneRng == OSIX_TRUE) &&
                        (TMO_SLL_Count (&(pNxtAsExtRng->extRtLst)) == 0))
                    {
                        /* Flush the LSA */
                        V3RagProcessNssaRange (pNxtAsExtRng, pArea);
                    }
                    V3RagProcessNssaRange (pAsExtRange, pArea);
                }
                u1RngFnd = OSIX_FALSE;
            }
        }
    }
    else
    {
        V3AreaAddNssaAddrRangeInCxt (pV3OspfCxt, pAsExtRange);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaAddExtAddrRange\n");
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaDelBkBoneAddrRangeInCxt                       */
/*                                                                      */
/* Description    : Deletes bkbone address range                        */
/*                                                                      */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pAsExtRange     : Pointer to Range                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/

PRIVATE VOID
V3AreaDelBkBoneAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                               tV3OsAsExtAddrRange * pAsExtRange)
{
    tV3OsAsExtAddrRange *pNxtAsExtRng = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    UINT1               u1LesSpecNssa = OSIX_FALSE;
    UINT1               u1FalNssaRng = OSIX_FALSE;
    UINT1               u1LesSpecBkbone = OSIX_FALSE;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDelBkBoneAddrRange\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));

    if (pArea == NULL)
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    " BkBone area not existing \n");
        return;
    }

    /* Flush any Type 5 LSA originated for the range    */
    V3RagFlushBkBoneRangeInCxt (pV3OspfCxt, pAsExtRange, NULL);

    /* Get the next range in the bkbone */

    pNext = TMO_SLL_Next (&(pArea->asExtAddrRangeLst),
                          &(pAsExtRange->nextAsExtAddrRange));

    pNxtAsExtRng = (tV3OsAsExtAddrRange *) (pNext);

    /* Check if it is less specific than range deleted  */

    if ((pNxtAsExtRng != NULL) &&
        (V3RagCompAddrRng (pAsExtRange, pNxtAsExtRng) == OSPFV3_GREATER))
    {
        /* Updates the "pNxtAsExtRng" with routes falling in 
           the deleted range
         */
        V3RagUpdtRtLstBtwRangesInCxt (pV3OspfCxt, pAsExtRange,
                                      pNxtAsExtRng, OSIX_FALSE);
        V3RagProcessBkBoneRangeInCxt (pV3OspfCxt, pNxtAsExtRng);
        u1LesSpecBkbone = OSIX_TRUE;
    }
    else
    {
        /* Ext Rt falling in the range which is deleted are to be 
           re-originated
         */
        TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
        {
            pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                             nextExtRouteInRange, pExtRtNode);

            pExtRoute->pAsExtAddrRange = NULL;
            V3RagOriginateNonAggLsaInCxt (pV3OspfCxt, pExtRoute, NULL);
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            TMO_SLL_Scan (&(pArea->type7AddrRangeLst),
                          pScanAsExtRng, tV3OsAsExtAddrRange *)
            {
                /* Check if there exists any NSSA range that is less/equal
                   to the deleted bkbone range
                 */
                if ((V3RagCompAddrRng (pAsExtRange, pScanAsExtRng)
                     == OSPFV3_GREATER) ||
                    (V3RagCompAddrRng (pAsExtRange, pScanAsExtRng)
                     == OSPFV3_EQUAL))
                {
                    u1LesSpecNssa = OSIX_TRUE;
                    break;
                }
            }                    /* TMO_SLL_Scan Nssa Range Ends */

            if (u1LesSpecNssa == OSIX_FALSE)
            {
                V3RagFlushNssaRange (pAsExtRange, NULL, pArea);

                /* If there is any less specifc Backbone address range 
                 * present, then search for next area list
                 * Otherwise generate Not Aggregated LSA each external
                 * Route falling in this address range.
                 */
                if (u1LesSpecBkbone == OSIX_TRUE)
                {
                    continue;
                }
                else
                {
                    /* Scan all the External Routes of the address Range */
                    TMO_SLL_Scan (&(pAsExtRange->extRtLst),
                                  pExtRtNode, tTMO_SLL_NODE *)
                    {
                        pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                                         nextExtRouteInRange,
                                                         pExtRtNode);

                        /* If Ext Rt is subsumed to any NSSA Range, then do
                         *  nothing otherwise originate Non Aggregated LSA
                         *  for the Extrenal Route.
                         */

                        TMO_SLL_Scan (&(pArea->type7AddrRangeLst),
                                      pScanAsExtRng, tV3OsAsExtAddrRange *)
                        {
                            if (V3RagIsExtRtFallInRange (pExtRoute,
                                                         pScanAsExtRng)
                                == OSIX_TRUE)
                            {
                                u1FalNssaRng = OSIX_TRUE;
                                break;
                            }
                        }        /* TMO_SLL_SCAN NSSA Range Ends */

                        if (u1FalNssaRng == OSIX_FALSE)
                        {
                            V3RagOriginateNonAggLsaInCxt (pV3OspfCxt,
                                                          pExtRoute, pArea);
                            pExtRoute->pAsExtAddrRange = NULL;
                        }
                        else
                        {
                            u1FalNssaRng = OSIX_FALSE;
                        }
                    }            /* TMO_SLL_SCAN external Routes of deleted range */
                }                /* There is no less specific Backbone Range */
            }                    /* If any Less specific range exists */
            else
            {
                u1LesSpecNssa = OSIX_FALSE;
                continue;
            }
        }                        /* If Nssa Area Checks */
    }                            /* TMO_SLL_SCAN Area List ends */

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaDelBkBoneAddrRange \n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3AreaDelNssaAddrRangeInCxt                           */
/*                                                                      */
/* Description  : Deletes NSSA address range                            */
/*                                                                      */
/*                                                                      */
/* Input        : pV3OspfCxt      : Context pointer                     */
/*                pAsExtRange     : Pointer to Nssa Range               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/

PRIVATE VOID
V3AreaDelNssaAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                             tV3OsAsExtAddrRange * pNssaExtRange)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAsExtAddrRange *pScanNssaExtRng = NULL;
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    UINT1               u1LesSpecNssaFnd = OSIX_FALSE;
    UINT1               u1BkboneFnd = OSIX_FALSE;
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT1               u1DelRng = OSIX_FALSE;
    UINT1               u1PrevRng = OSIX_TRUE;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDelNssaAddrRange\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pNssaExtRange->areaId));

    if (pArea == NULL)
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    " BkBone area not existing \n");
        return;
    }
    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    " Area Type not NSSA  \n");
        return;
    }

    /* Flush any Type 7 LSA originated for the range    */
    V3RagFlushNssaRange (pNssaExtRange, NULL, pArea);

    TMO_SLL_Scan (&(pArea->type7AddrRangeLst), pScanNssaExtRng,
                  tV3OsAsExtAddrRange *)
    {
        /* Check if the scanned Range is less specific 
           than the deleted range 
         */
        if (V3RagCompAddrRng (pNssaExtRange, pScanNssaExtRng) == OSPFV3_GREATER)
        {
            /* Updates the "pNxtAsExtRng" with routes falling in 
               the deleted range
             */
            V3RagUpdtRtLstBtwRangesInCxt (pV3OspfCxt, pNssaExtRange,
                                          pScanNssaExtRng, OSIX_FALSE);
            V3RagProcessNssaRange (pScanNssaExtRng, pArea);
            u1LesSpecNssaFnd = OSIX_TRUE;
            break;
        }
    }

    /* If no less specific range exists in NSSA area, check 
       if there exists a bkbone range that is equal/less 
       specific to deleted NSSA range
     */
    if (u1LesSpecNssaFnd == OSIX_FALSE)
    {
        TMO_SLL_Scan (&(pV3OspfCxt->pBackbone->asExtAddrRangeLst),
                      pScanAsExtRng, tV3OsAsExtAddrRange *)
        {
            /* Check if range is less/equal to deleted range */
            if ((V3RagCompAddrRng (pNssaExtRange, pScanAsExtRng)
                 == OSPFV3_GREATER) ||
                (V3RagCompAddrRng (pNssaExtRange, pScanAsExtRng) ==
                 OSPFV3_EQUAL))
            {
                (pScanAsExtRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
                (pScanAsExtRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

                TMO_SLL_Scan (&(pScanAsExtRng->extRtLst),
                              pExtRtNode, tTMO_SLL_NODE *)
                {
                    pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                                     nextExtRouteInRange,
                                                     pExtRtNode);

                    if (V3RagChkExtRtFallInPrvRngInCxt (pV3OspfCxt,
                                                        pNssaExtRange,
                                                        pExtRoute) == OSIX_TRUE)
                    {
                        /* Skip this Ext Rt */
                        continue;
                    }
                    /* Update cost/Path Type for bkbone 
                       range only for Ext Rt 
                       which are not falling in more 
                       specific NSSA range
                     */
                    else
                    {
                        V3RagUpdatCostTypeForRange (pScanAsExtRng, pExtRoute);
                    }
                }
                V3RagProcessNssaRange (pScanAsExtRng, pArea);
                u1BkboneFnd = OSIX_TRUE;
                V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pScanAsExtRng);
                break;
            }
        }
    }

    /* If no less spcific NSSA Range is found and no less specific
     * BackBone Range is found, then originate Non Aggregated LSA
     * for all the External Routes falling in this Address Range
     */

    if ((u1LesSpecNssaFnd == OSIX_FALSE) && (u1BkboneFnd == OSIX_FALSE))
    {
        inParams.pRoot = pV3OspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPFV3_RT_ID;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        inParams.Key.pKey = NULL;

        if (TrieGetFirstNode (&inParams,
                              &pTempPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return;
        }

        do
        {
            pExtRoute = (tV3OsExtRoute *) pTempPtr;
            u1DelRng = V3RagIsExtRtFallInRange (pExtRoute, pNssaExtRange);
            u1PrevRng = V3RagChkExtRtFallInPrvRngInCxt (pV3OspfCxt,
                                                        pNssaExtRange,
                                                        pExtRoute);

            if ((u1DelRng == OSIX_TRUE) && (u1PrevRng == OSIX_FALSE))
            {
                V3RagOriginateNonAggLsaInCxt (pV3OspfCxt, pExtRoute, pArea);
            }
            pLeafNode = inParams.pLeafNode;
            OSPFV3_IP6ADDR_CPY (au1Key, &(pExtRoute->ip6Prefix));
            au1Key[sizeof (tIp6Addr)] = pExtRoute->u1PrefixLength;
            inParams.Key.pKey = au1Key;

        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaDelNssaAddrRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaDelExtAddrRangeInCxt                          */
/*                                                                      */
/* Description    : Deletes the AS EXT External Range                   */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                  pAsExtRange     : Pointer to Range                  */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3AreaDelExtAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                            tV3OsAsExtAddrRange * pAsExtRange, UINT1 u1Flag)
{
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDelExtAddrRangeInCxt\n");

    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_TOP_CHG);
    }

    /* Only active ranges are used for aggregation */
    if (pAsExtRange->rangeStatus != NOT_READY)
    {
        pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));
        if (pArea == NULL)
        {
            return;
        }
        if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
            == OSPFV3_EQUAL)
        {
            V3AreaDelBkBoneAddrRangeInCxt (pV3OspfCxt, pAsExtRange);
        }
        else
        {
            V3AreaDelNssaAddrRangeInCxt (pV3OspfCxt, pAsExtRange);
        }
    }

    if (u1Flag == OSIX_TRUE)
    {
        TMO_SLL_Delete (&(pV3OspfCxt->asExtAddrRangeLst),
                        &(pAsExtRange->nextAsExtAddrRngInRtr));

        /* Only active range is present in 
           respective area Lst
         */
        if ((pAsExtRange->rangeStatus != NOT_READY) &&
            (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
             == OSPFV3_EQUAL))
        {
            TMO_SLL_Delete (&(pArea->asExtAddrRangeLst),
                            &(pAsExtRange->nextAsExtAddrRange));
        }
        else if ((pAsExtRange->rangeStatus != NOT_READY) &&
                 (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
                  != OSPFV3_EQUAL))
        {
            TMO_SLL_Delete (&(pArea->type7AddrRangeLst),
                            &(pAsExtRange->nextAsExtAddrRange));
        }

        gV3OsRtr.pExtAggrTableCache = NULL;

        OSPFV3_AS_EXT_AGG_FREE (pAsExtRange);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaDelExtAddrRangeInCxt\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function       : V3AreaDeleteAllExtAggInCxt                          */
/*                                                                      */
/* Description    : Deletes all AS EXT External Range                   */
/*                                                                      */
/* Input          : pV3OspfCxt      : Context pointer                   */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3AreaDeleteAllExtAggInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDeleteAllExtAggInCxt\n");

    while ((pNode = TMO_SLL_First (&(pV3OspfCxt->asExtAddrRangeLst))) != NULL)
    {
        pAsExtRange = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                           nextAsExtAddrRngInRtr, pNode);

        TMO_SLL_Delete (&(pV3OspfCxt->asExtAddrRangeLst),
                        &(pAsExtRange->nextAsExtAddrRngInRtr));
        pAsExtRange = NULL;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaDeleteAllExtAggInCxt\n");

}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rngext.c                       */
/*----------------------------------------------------------------------*/
