/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3ddp.c,v 1.9 2017/12/26 13:34:26 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             database description process.
 *
 *******************************************************************/

#include "o3inc.h"
#include "fsos3lw.h"
/* Proto types of the functions private to this file only */
PRIVATE VOID        V3DdpProcessDdp
PROTO ((UINT1 *pDdPkt, UINT2 u2Len, tV3OsNeighbor * pNbr));
PRIVATE INT1 V3DdpIsEmptySummary PROTO ((tV3OsNeighbor * pNbr));
PRIVATE VOID V3DdpUpdateSummary PROTO ((tV3OsNeighbor * pNbr));
PRIVATE tV3OsTruthValue V3DdpAddToSummary
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
        UINT1 *pNextLsHeaderStart));
PRIVATE UINT2 V3DdpAddSummay PROTO ((tV3OsNeighbor * pNbr));
PRIVATE VOID V3DdpUpdateLsaLst PROTO ((tV3OsNeighbor * pNbr));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpRcvDdp                                                */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.6                          */
/*                The PPP Module calls it when a DDP packet is received.     */
/*                It processes and validates the received DDP packet and     */
/*                generates the necessary neighbor state machine event.      */
/*                After validating the DDP packet, it calls V3DdpProcessDdp()*/
/*                function to process the LSA headers received from neighbor.*/
/*                                                                           */
/* Input        : pDdPkt            : The ddp packet received                */
/*                u2Len             : The length of the packet               */
/*                pNbr              : The neighbor from whom the pkt was     */
/*                                    received                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3DdpRcvDdp (UINT1 *pDdPkt, UINT2 u2Len, tV3OsNeighbor * pNbr)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;    /* OSPFv3 context Id */
    tV3OsLsaSeqNum      ddSeqNum = 0;
    tV3OsOptions        options;
    UINT2               u2Ifmtu = 0;
    UINT1               u1Flags = 0;
    UINT1               u1NegDone = OSIX_FALSE;
    UINT1               u1DupFlag = OSIX_FALSE;
    INT4                i4TraceVal = OSPFV3_INIT_VAL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpRcvDdp\n");

    nmhGetFutOspfv3TraceLevel (&i4TraceVal);
    /* Store the context pointer associated with the neighbor */
    pV3OspfCxt = pNbr->pInterface->pArea->pV3OspfCxt;

    if (pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE)
    {
        return;
    }

    OSPFV3_TRC3 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Received Packet Type %s From Nbr %x in state %s\n",
                 gau1Os3DbgPktType[OSPFV3_DD_PKT],
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 gau1Os3DbgNbrState[pNbr->u1NsmState]);

    OSPFV3_BUFFER_GET_1_BYTE (pDdPkt, OSPFV3_DDP_MASK_OFFSET, u1Flags);
    OSPFV3_BUFFER_GET_2_BYTE (pDdPkt, OSPFV3_DDP_IFACE_MTU_OFFSET, u2Ifmtu);
    OSPFV3_BUFFER_GET_STRING (pDdPkt, options, OSPFV3_DDP_OPTIONS_OFFSET,
                              OSPFV3_OPTION_LEN);
    OSPFV3_BUFFER_EXTRACT_4_BYTE (pDdPkt, OSPFV3_DDP_SEQ_NUM_OFFSET, ddSeqNum);

    if (((options[OSPFV3_OPT_BYTE_THREE] ==
          pNbr->lastRxDdp.rtrOptions[OSPFV3_OPT_BYTE_THREE])) &&
        (u1Flags == pNbr->lastRxDdp.u1Flags) &&
        (ddSeqNum == pNbr->lastRxDdp.i4Ddseqno))
    {
        u1DupFlag = OSIX_TRUE;
        OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Duplicate DDP\n");
    }

    if (u2Ifmtu > (UINT2) pNbr->pInterface->u4MtuSize)
    {
        OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

        if (((i4TraceVal & OSPFV3_DDP_TRC) != 0) ||
            ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded.Reason : MTU Size Mismatch. Neighbor MTU :%d, Interface MTU :%d",
                          pNbr->pInterface->u4MtuSize, u2Ifmtu));
        }

        OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "DDP Disc MTU Size Mismatch .Neighbor MTU :%d, Interface MTU :%d\n",
                     pNbr->pInterface->u4MtuSize, u2Ifmtu);
        return;
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface))
    {

        /* trying to suppress hellos in demand circuits */
        if (OSPFV3_OPT_ISSET (options, OSPFV3_DC_BIT_MASK))
        {
            pNbr->bHelloSuppression = OSIX_TRUE;
        }
        else
        {
            pNbr->bHelloSuppression = OSIX_FALSE;
        }
    }

    if ((pNbr->u1NsmState == OSPFV3_NBRS_DOWN) ||
        (pNbr->u1NsmState == OSPFV3_NBRS_ATTEMPT))
    {

        OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "DDP Discarded. Reason : Improper Nbr State %s\n",
                      gau1Os3DbgNbrState[pNbr->u1NsmState]));

        OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "DDP Discarded. Reason : Improper Neighbor State %s\n",
                     gau1Os3DbgNbrState[pNbr->u1NsmState]);

        return;
    }

    if (pNbr->u1NsmState == OSPFV3_NBRS_INIT)
    {

        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_2WAY_RCVD);

        /* continue further in the new state */

    }

    if (pNbr->u1NsmState == OSPFV3_NBRS_2WAY)
    {

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "DDP Discarded.Reason : Nbr State Already %s\n",
                      gau1Os3DbgNbrState[pNbr->u1NsmState]));

        OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "DDP Discarded. Reason : Neighbor state is already %s\n",
                     gau1Os3DbgNbrState[pNbr->u1NsmState]);

        OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

        return;
    }

    if (pNbr->u1NsmState == OSPFV3_NBRS_EXSTART)
    {

        OSPFV3_TRC3 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Flag: %d  , Nbr_ID: %x,  Rtr_ID: %x \n",
                     u1Flags,
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     OSPFV3_BUFFER_DWFROMPDU (pV3OspfCxt->rtrId));

        OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "DD seq No: %d,  pNbr.seqNo: %d\n",
                     ddSeqNum, pNbr->dbSummary.seqNum);

        if ((u1Flags & OSPFV3_I_BIT_MASK) &&
            (u1Flags & OSPFV3_M_BIT_MASK) &&
            (u1Flags & OSPFV3_MS_BIT_MASK) &&
            (u2Len == (OSPFV3_HEADER_SIZE + OSPFV3_DDP_FIXED_PORTION_SIZE)) &&
            (V3UtilRtrIdComp (pNbr->nbrRtrId, pV3OspfCxt->rtrId) ==
             OSPFV3_GREATER))
        {

            OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Router %x Slave wrt Nbr %x\n",
                         OSPFV3_BUFFER_DWFROMPDU (OSPFV3_RTR_ID (pV3OspfCxt)),
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

            pNbr->dbSummary.bMaster = OSIX_FALSE;
            u1NegDone = OSIX_TRUE;

        }
        else if ((!(u1Flags & OSPFV3_I_BIT_MASK)) &&
                 (!(u1Flags & OSPFV3_MS_BIT_MASK)) &&
                 (ddSeqNum == pNbr->dbSummary.seqNum) &&
                 (V3UtilRtrIdComp (pNbr->nbrRtrId, pV3OspfCxt->rtrId) ==
                  OSPFV3_LESS))
        {

            OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Router %x Master wrt Nbr %x\n",
                         OSPFV3_BUFFER_DWFROMPDU (OSPFV3_RTR_ID (pV3OspfCxt)),
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
            pNbr->dbSummary.bMaster = OSIX_TRUE;

            /* printf ("\n\n Router is Master wrt Nbr :%d u4InterfaceId:%d ",ddSeqNum,pNbr->pInterface->u4InterfaceId); */
            u1NegDone = OSIX_TRUE;
        }
        else
        {

            OSPFV3_TRC3 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "PktType: %s from nbrId: %x,"
                         "in state:%s discarded.\n",
                         gau1Os3DbgPktType[OSPFV3_DD_PKT],
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);

            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded.Reason : Incorrect flag %d received in DDP packet\n",
                          u1Flags));

            OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP Discarded. Reason : Incorrect flag %d received in DDP packet \n",
                         u1Flags);

            return;
        }

        if (u1NegDone == OSIX_TRUE)
        {

            pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE] =
                options[OSPFV3_OPT_BYTE_THREE];

            /*printf("\n\n before pNbr->u1NsmState :%d u4InterfaceId %d \n",pNbr->u1NsmState,pNbr->pInterface->u4InterfaceId); */
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_NEG_DONE);
            /* 
             * If memory can not be allocated for the db_summary_list, NEG_DONE 
             * event will fail without updating nsm state to EXCHANGE. 
             */

            if (pNbr->u1NsmState == OSPFV3_NBRS_EXCHANGE)
            {

                V3TmrDeleteTimer (&(pNbr->dbSummary.ddTimer));

                pNbr->lastRxDdp.rtrOptions[OSPFV3_OPT_BYTE_THREE] =
                    options[OSPFV3_OPT_BYTE_THREE];

                pNbr->lastRxDdp.u1Flags = u1Flags;
                pNbr->lastRxDdp.i4Ddseqno = ddSeqNum;
                V3DdpProcessDdp (pDdPkt, u2Len, pNbr);
            }
            else
            {

                OSPFV3_OPT_CLEAR_ALL (pNbr->nbrOptions);
                OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "DDP Discarded.Cannot allocate memory for the db_summary_list\n"));

                OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                            pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "DDP Discarded. Reason : DB_SUM_LST Alloc Failure\n");

            }
        }
    }
    else if (pNbr->u1NsmState == OSPFV3_NBRS_EXCHANGE)
    {
        if ((pNbr->dbSummary.bMaster == OSIX_TRUE) && (u1DupFlag == OSIX_TRUE))
        {
            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded.Duplicate Rcvd By Master"
                          "From Nbr In State %s\n",
                          gau1Os3DbgNbrState[pNbr->u1NsmState]));

            OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP Discarded. Reason : Duplicate Received By Master "
                         "From Nbr In State %s\n",
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);

        }
        else if ((pNbr->dbSummary.bMaster == OSIX_FALSE) &&
                 (u1DupFlag == OSIX_TRUE))
        {

            /* duplicate received by slave */

            OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Duplicate DDP Received By Slave From Nbr In State %s"
                         " DDP To Be Retransmitted. \n",
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);

            V3DdpRxmtDdp (pNbr);
        }
        else if (((pNbr->dbSummary.bMaster == OSIX_TRUE) &&
                  (u1Flags & OSPFV3_MS_BIT_MASK)) ||
                 ((pNbr->dbSummary.bMaster == OSIX_FALSE) &&
                  (!(u1Flags & OSPFV3_MS_BIT_MASK))) ||
                 (u1Flags & OSPFV3_I_BIT_MASK) ||
                 (pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE] !=
                  options[OSPFV3_OPT_BYTE_THREE]))
        {

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded because of Options Mismatch."
                          " OldOptions %x%x%x"
                          " NewOptions %x%x%x"
                          " Flags %d \n",
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_ONE],
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_TWO],
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE],
                          options[OSPFV3_OPT_BYTE_ONE],
                          options[OSPFV3_OPT_BYTE_TWO],
                          options[OSPFV3_OPT_BYTE_THREE], u1Flags));

            OSPFV3_TRC7 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP Discarded. Reason : OptionsMismatch OldOptions %x%x%x"
                         " NewOptions %x%x%x"
                         " Flags %d \n",
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_ONE],
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_TWO],
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE],
                         options[OSPFV3_OPT_BYTE_ONE],
                         options[OSPFV3_OPT_BYTE_TWO],
                         options[OSPFV3_OPT_BYTE_THREE], u1Flags);

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_SEQ_NUM_MISMATCH);
            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

        }
        else if (((pNbr->dbSummary.bMaster == OSIX_TRUE) &&
                  (ddSeqNum == pNbr->dbSummary.seqNum)) ||
                 ((pNbr->dbSummary.bMaster == OSIX_FALSE) &&
                  (ddSeqNum == (pNbr->dbSummary.seqNum + 1))))
        {
            pNbr->lastRxDdp.rtrOptions[OSPFV3_OPT_BYTE_THREE] =
                options[OSPFV3_OPT_BYTE_THREE];

            pNbr->lastRxDdp.u1Flags = u1Flags;
            pNbr->lastRxDdp.i4Ddseqno = ddSeqNum;
            V3DdpProcessDdp (pDdPkt, u2Len, pNbr);
        }
        else
        {
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_SEQ_NUM_MISMATCH);
            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

            if (pNbr->u1NsmState < OSPFV3_MAX_NBR_STATE)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "DDP Discarded. Duplicate Rcvd By Master From Nbr %x"
                              " in state %s \n",
                              OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                              gau1Os3DbgNbrState[pNbr->u1NsmState]));

                OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                             pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                             "DDP Discarded.Reason : Duplicate Received By Master From Nbr %x"
                             " in state %s \n",
                             OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                             gau1Os3DbgNbrState[pNbr->u1NsmState]);
            }

        }
    }
    else if ((pNbr->u1NsmState == OSPFV3_NBRS_LOADING) ||
             (pNbr->u1NsmState == OSPFV3_NBRS_FULL))
    {

        if (((pNbr->dbSummary.bMaster == OSIX_TRUE) &&
             (u1Flags & OSPFV3_MS_BIT_MASK)) ||
            ((pNbr->dbSummary.bMaster == OSIX_FALSE) &&
             (!(u1Flags & OSPFV3_MS_BIT_MASK))) ||
            (u1Flags & OSPFV3_I_BIT_MASK) ||
            (pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE] !=
             options[OSPFV3_OPT_BYTE_THREE]))
        {

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded because of Options mismatch."
                          " OldOptions %x%x%x"
                          " Newoptions %x%x%x"
                          " Flags %d \n",
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_ONE],
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_TWO],
                          pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE],
                          options[OSPFV3_OPT_BYTE_ONE],
                          options[OSPFV3_OPT_BYTE_TWO],
                          options[OSPFV3_OPT_BYTE_THREE], u1Flags));

            OSPFV3_TRC7 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP Discarded. Reason : Options mismatch. OldOptions %x%x%x"
                         " Newoptions %x%x%x"
                         " Flags %d \n",
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_ONE],
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_TWO],
                         pNbr->nbrOptions[OSPFV3_OPT_BYTE_THREE],
                         options[OSPFV3_OPT_BYTE_ONE],
                         options[OSPFV3_OPT_BYTE_TWO],
                         options[OSPFV3_OPT_BYTE_THREE], u1Flags);

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_SEQ_NUM_MISMATCH);
            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

        }
        else if ((pNbr->dbSummary.bMaster == OSIX_TRUE) &&
                 (u1DupFlag == OSIX_TRUE))
        {

            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Discarded. Duplicate Rcvd By Master From Nbr %x"
                          " in state %s \n",
                          OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                          gau1Os3DbgNbrState[pNbr->u1NsmState]));

            OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP Discarded. Reason : Duplicate Received By Master From Nbr %x"
                         " in state %s \n",
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);

        }
        else if ((pNbr->dbSummary.bMaster == OSIX_FALSE) &&
                 (u1DupFlag == OSIX_TRUE) &&
                 (pNbr->dbSummary.dbSummaryPkt != NULL))
        {

            /* duplicate received by slave and lask pkt is not freed */

            OSPFV3_TRC2 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "DDP To be retransmitted. Duplicate Received By Slave From Nbr %x"
                         " In State %s \n",
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);

            V3DdpRxmtDdp (pNbr);
        }
        else
        {

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_SEQ_NUM_MISMATCH);
            OSPFV3_INC_DISCARD_DDP_CNT (pNbr->pInterface);

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "DDP Disc Due To SeqNoMisMatch \n"));

            OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "DDP Discarded. Reason :Sequence Number Mismatch \n");

        }
    }

    OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Received DDP Processing Over\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpRcvDdp\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpProcessDdp                                            */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.6                          */
/*                This procedure is invoked when the DDP packet is accepted  */
/*                as next in sequence. This procedure considers the          */
/*                LS headers in the packet and adds them in the link state   */
/*                request (if necessary). Also it calls the V3DdpSendDdp()   */
/*                to send the next sequence of DD packet if necessary.       */
/*                                                                           */
/* Input        : pDdPkt            : The dd packet                          */
/*                u2Len             : The length of the packet               */
/*                pNbr              : The neighbor from whom the pkt was     */
/*                                    received                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3DdpProcessDdp (UINT1 *pDdPkt, UINT2 u2Len, tV3OsNeighbor * pNbr)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsHeader       lsHeader;
    tV3OsLsaSeqNum      ddSeqNum = 0;
    INT4                i4LSACount = 0;
    UINT2               u2LsHdrCount = 0;
    UINT1               u1Flags = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpProcessDdp\n");

    OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "DDP Processing Start\n");

    OSPFV3_BUFFER_GET_1_BYTE (pDdPkt, OSPFV3_DDP_MASK_OFFSET, u1Flags);
    OSPFV3_BUFFER_EXTRACT_4_BYTE (pDdPkt, OSPFV3_DDP_SEQ_NUM_OFFSET, ddSeqNum);

    u2LsHdrCount = (UINT2) ((u2Len - (OSPFV3_HEADER_SIZE +
                                      OSPFV3_DDP_FIXED_PORTION_SIZE))
                            / OSPFV3_LS_HEADER_SIZE);

    while (u2LsHdrCount--)
    {
        V3UtilExtractLsHeaderFromPkt (pDdPkt, &lsHeader, (UINT2) i4LSACount,
                                      OSPFV3_DD_PKT);

        if ((OSPFV3_IS_AS_FLOOD_SCOPE_LSA (lsHeader.u2LsaType) &&
             ((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) ||
              (pNbr->pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA))))

        {
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_SEQ_NUM_MISMATCH);
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "LSType Mismatch Error : Type %d LSA DDP Processing Stopped\n",
                          lsHeader.u2LsaType));

            OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "LSType Mismatch Error : Type %d LSA DDP Processing Stopped\n",
                         lsHeader.u2LsaType);
            return;
        }
        else
        {
            pLsaInfo = V3LsuSearchDatabase (lsHeader.u2LsaType,
                                            &(lsHeader.linkStateId),
                                            &(lsHeader.advRtrId),
                                            pNbr->pInterface,
                                            pNbr->pInterface->pArea);

            if ((pLsaInfo == NULL) ||
                (V3LsuCompLsaInstance (pLsaInfo, &lsHeader) == OSPFV3_RCVD_LSA))
            {

                if (V3LrqAddToLsaReqLst (pNbr, &lsHeader) == OSIX_FALSE)
                {

                    OSPFV3_GENERATE_NBR_EVENT (pNbr,
                                               OSPFV3_NBRE_SEQ_NUM_MISMATCH);
                    SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                  "unable to allocate memory to Lsa Request List\n"));

                    OSPFV3_TRC (OS_RESOURCE_TRC,
                                pNbr->pInterface->pArea->pV3OspfCxt->
                                u4ContextId,
                                "DDP Processing Stopped "
                                "Due To LRQ Alloc Failure\n");
                    return;
                }
            }
        }
        /* Processing of received ddp complete */
        i4LSACount++;            /* FSAP2 */
    }

    /* Send ddp in response */
    if (pNbr->dbSummary.bMaster == OSIX_TRUE)
    {
        /* router is bMaster */
        pNbr->dbSummary.seqNum++;
        V3DdpUpdateSummary (pNbr);
        if ((V3DdpIsEmptySummary (pNbr) == OSIX_TRUE) &&
            (!(u1Flags & OSPFV3_M_BIT_MASK)))
        {
            /* database exchange complete */
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_EXCHANGE_DONE);
            V3DdpClearSummary (pNbr);
        }
        else
        {
            /* Since the reception of a DDP packet with same seq num from the
             * NBR indicates the Ack, the rxmt timer started for that packet is
             * deleted from the list.
             */
            V3TmrDeleteTimer (&(pNbr->dbSummary.ddTimer));
            V3DdpSendDdp (pNbr);
        }

    }
    else
    {

        /* router is slave */

        pNbr->dbSummary.seqNum = ddSeqNum;
        V3DdpUpdateSummary (pNbr);
        V3DdpSendDdp (pNbr);

        if ((V3DdpIsEmptySummary (pNbr) == OSIX_TRUE) &&
            (!(u1Flags & OSPFV3_M_BIT_MASK)))
        {

            /* database exchange complete */

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_EXCHANGE_DONE);
            V3TmrSetTimer (&(pNbr->dbSummary.ddTimer),
                           OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC *
                           (pNbr->pInterface->u2RtrDeadInterval));
        }
    }

    OSPFV3_TRC (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "DDP Contents Processing Over\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpProcessDdp\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpIsEmptySummary                                        */
/*                                                                           */
/* Description  : This procedure returns OSIX_TRUE if the database summary   */
/*                list of this neighbor is empty and OSIX_FALSE otherwise.   */
/*                                                                           */
/* Input        : pNbr          : the nbr whose summary is to be checked     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if summary list is empty                        */
/*                OSIX_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT1
V3DdpIsEmptySummary (tV3OsNeighbor * pNbr)
{
    UINT1               u1RetStatus = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpIsEmptySummary\n");

    if (pNbr->dbSummary.pLsaRBRoot == NULL)
    {
        u1RetStatus = OSIX_TRUE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpIsEmptySummary\n");

    return (u1RetStatus);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpUpdateSummary                                         */
/*                                                                           */
/* Description  : This procedure updates the top of the summary list so that */
/*                the contents of the previously transmitted ddp are removed */
/*                from the summary list. Also the DD pkt previously transmi- */
/*                tted is freed.                                             */
/*                                                                           */
/* Input        : pNbr       : the nbr whose summary is to be updated        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3DdpUpdateSummary (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpUpdateSummary\n");

    OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Updating database summary of neighbor with ID : %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    pNbr->dbSummary.u2LastTxedSummaryLen = 0;
    if (pNbr->dbSummary.dbSummaryPkt != NULL)
    {

        V3UtilOsMsgFree (pNbr->dbSummary.dbSummaryPkt);
        pNbr->dbSummary.dbSummaryPkt = NULL;
    }

    OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Database Summary of neighbor with ID : %x updated\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpUpdateSummary\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpBuildSummary                                          */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                This procedure builds the database summary list for this   */
/*                neighbor. This procedure is invoked whenever the neighbor  */
/*                is ready to enter  exchange state.                         */
/*                                                                           */
/* Input        : pNbr            : the nbr whose summary is to be built     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if summary successfully built                   */
/*                OSIX_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3DdpBuildSummary (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpBuildSummary\n");

    OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Building Database Summary List of Neighbor with ID : %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
    /* printf ("\n\n V3DdpBuildSummary \n\n"); */
    pNbr->dbSummary.pLsaRBRoot = NULL;
    if (pNbr->pInterface->u4LinkScopeLsaCount > 0)
    {
        pNbr->dbSummary.pLsaRBRoot = pNbr->pInterface->pLinkScopeLsaRBRoot;
    }
    else if (pNbr->pInterface->pArea->u4AreaScopeLsaCount > 0)
    {
        pNbr->dbSummary.pLsaRBRoot =
            pNbr->pInterface->pArea->pAreaScopeLsaRBRoot;
    }
    else if (pNbr->pInterface->pArea->pV3OspfCxt->u4AsScopeLsaCount > 0)
    {
        if ((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) ||
            (pNbr->pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) ||
            (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL))
        {
            return (OSIX_FALSE);
        }

        pNbr->dbSummary.pLsaRBRoot =
            pNbr->pInterface->pArea->pV3OspfCxt->pAsScopeLsaRBRoot;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpBuildSummary\n");

    return (OSIX_TRUE);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpAddToSummary                                          */
/*                                                                           */
/* Description  : This routine adds the specified advertisement to the       */
/*                specified neighbour's summary list. If the specified ad is */
/*                of age OSPFV3_MAX_AGE then adds it to the rxmt list and    */
/*                returns OSIX_FALSE. Otherwise, adds the lsHeader of the    */
/*                advertisement to the database summary list at point        */
/*                next_ls_header_start and returns OSIX_TRUE.                */
/*                                                                           */
/* Input        : pNbr               : nbr whose summary is constructed      */
/*                pLsaInfo           : the advt to be added to summary       */
/*                pNextLsHeaderStart : ptr to the summary being built        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_FALSE, if added to rxmt list                          */
/*                OSIX_TRUE, otherwise                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE             tV3OsTruthValue
V3DdpAddToSummary (tV3OsNeighbor * pNbr,
                   tV3OsLsaInfo * pLsaInfo, UINT1 *pNextLsHeaderStart)
{

    UINT2               u2LsaAge = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpAddToSummary\n");

    OSPFV3_GET_LSA_AGE (pLsaInfo, &(u2LsaAge));

    if (OSPFV3_IS_MAX_AGE (u2LsaAge))
    {

        V3LsuAddToRxmtLst (pNbr, pLsaInfo);

        OSPFV3_TRC3 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "MAX AGE Lsa_Type %d Link_State_Id %x Adv_Rtr_Id "
                     " %x Added To Retransmission List\n",
                     pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        OSPFV3_TRC (OSPFV3_FN_EXIT,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3DdpAddToSummary\n");
        return (OSIX_FALSE);
    }
    else
    {

        MEMCPY (pNextLsHeaderStart, pLsaInfo->pLsa, OSPFV3_LS_HEADER_SIZE);

        /* update the age field appropriately in the summary list */

        OSPFV3_BUFFER_ASSIGN_2_BYTE (pNextLsHeaderStart,
                                     OSPFV3_AGE_OFFSET_IN_LS_HEADER, u2LsaAge);

        OSPFV3_TRC3 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Lsa_Type %d Link_State_Id %x Adv_Rtr_Id"
                     " %x Added To Database Summary\n",
                     pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

        OSPFV3_TRC (OSPFV3_FN_EXIT,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3DdpAddToSummary\n");
        return (OSIX_TRUE);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpClearSummary                                          */
/*                                                                           */
/* Description  : This procedure clears the database summary list of the     */
/*                specified neighbour.                                       */
/*                                                                           */
/* Input        : pArg           :  the nbr whose summary is to be cleared   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3DdpClearSummary (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = (tV3OsNeighbor *) pArg;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3DdpClearSummary\n");

    pNbr->dbSummary.pLsaRBRoot = NULL;
    pNbr->dbSummary.lstTxedLsa = NULL;
    pNbr->dbSummary.u2LastTxedSummaryLen = 0;
    if (pNbr->dbSummary.dbSummaryPkt != NULL)
    {
        V3UtilOsMsgFree (pNbr->dbSummary.dbSummaryPkt);
        pNbr->dbSummary.dbSummaryPkt = NULL;
    }
    /* Since the reception of a DDP packet with same seq num from the NBR
     * indicates the Ack, the rxmt timer started for that packet is
     * deleted from the list.
     */
    V3TmrDeleteTimer (&(pNbr->dbSummary.ddTimer));

    OSPFV3_GBL_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                     "Database Summary List of Neighbor with ID %x Cleared\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3DdpClearSummary\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpRxmtDdp                                               */
/*                                                                           */
/* Description  : It is called from the DDP and TMR modules under the        */
/*                following conditions:                                      */
/*                -  Retransmission timer expiry;                            */
/*                -  Slave has received a duplicate DDP from master;         */
/*                In state EXSTART, empty DDP with I, M, MS bits set is      */
/*                sent to the neighbor. In state EXCHANGE, the previous DDP  */
/*                is retransmitted.                                          */
/*                                                                           */
/* Input        : pNbr         : Pointer to the neighbor structure to which  */
/*                               the DDP packet has to be retransmitted.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3DdpRxmtDdp (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpRxmtDdp\n");

    if (pNbr->dbSummary.dbSummaryPkt == NULL)
    {
        return;
    }

    V3PppSendPkt (pNbr->dbSummary.dbSummaryPkt,
                  (UINT2) (OSPFV3_HEADER_SIZE + OSPFV3_DDP_FIXED_PORTION_SIZE +
                           pNbr->dbSummary.u2LastTxedSummaryLen),
                  pNbr->pInterface, &(pNbr->nbrIpv6Addr));

    OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DDP Retransmitted To Neighbor %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpRxmtDdp\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpSendDdp                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.8                          */
/*                It sends the DDP packets. Called by the NSM and DDP        */
/*                modules under the following circumstances:                 */
/*                - When a neighbor state transitions to ExStart, then it    */
/*                  starts sending DD packet until a response DD packet is   */
/*                  received or Router Dead Interval elapses.                */
/*                - Master receives acknowledgement (DDP with the same       */
/*                  sequence number) with M-bit set or master has more       */
/*                  LSA-headers to send.                                     */
/*                - Slave receives a new DDP from the master; it has to send */
/*                  the acknowledgement for the DD packet.                   */
/*                                                                           */
/* Input        : pNbr             : Pointer to the neighbor structure to    */
/*                                   which the DDP packet has to be sent.    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3DdpSendDdp (tV3OsNeighbor * pNbr)
{

    UINT1               u1Flags = 0;
    tV3OsOptions        options = { 0, 0, 0 };
    UINT2               u2Len = 0;
    INT1                i1TimerType = OSPFV3_INVALID_TIMER_TYPE;
    UINT4               u4Interval = 0;
    UINT2               u2DdLen = 0;
    UINT1              *pOspfPkt = NULL;

    /* RFC 2328 */
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3DdpSendDdp\n");

    if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
    {
        /* If the node is stand by, no need to send DDP
         * This will be triggered once it becomes ACTIVE node.
         * Blocking this call would block the Rx timer and avoid
         * adding in the Rx list */
        return;
    }

    options[OSPFV3_OPT_BYTE_THREE] =
        pNbr->pInterface->pArea->pV3OspfCxt->rtrOptions[OSPFV3_OPT_BYTE_THREE] |
        pNbr->pInterface->pArea->areaOptions[OSPFV3_OPT_BYTE_THREE];

    if (pNbr->u1NsmState == OSPFV3_NBRS_EXSTART)
    {

        if ((pNbr->dbSummary.dbSummaryPkt =
             V3UtilOsMsgAlloc (OSPFV3_HEADER_SIZE +
                               OSPFV3_DDP_FIXED_PORTION_SIZE)) == NULL)
        {

            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to Allocate Memory for DDP Tx to Nbr %x\n",
                          OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId)));

            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Cannot allocate buffer for DDP packet Transmission to Neighbor %x\n",
                          OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId)));

            OSPFV3_TRC1 (OS_RESOURCE_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Alloc Failure For DDP Transmission To Neighbor %x\n",
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
            return;
        }

        pNbr->dbSummary.u2LastTxedSummaryLen = 0;
        pOspfPkt = pNbr->dbSummary.dbSummaryPkt + IPV6_HEADER_LEN;

        /*  2328  */
        /* 2 bytes  filled with interface MTU  */
        if (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
        {
            OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                         OSPFV3_DDP_IFACE_MTU_OFFSET, 0);
        }
        else
        {
            OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                         OSPFV3_DDP_IFACE_MTU_OFFSET,
                                         (UINT2) pNbr->pInterface->u4MtuSize);
        }

        if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF
            (pNbr->pInterface))
        {
            options[OSPFV3_OPT_BYTE_THREE] |=
                pNbr->pInterface->ifOptions[OSPFV3_OPT_BYTE_THREE];
        }

        OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt,
                                     options,
                                     OSPFV3_DDP_OPTIONS_OFFSET,
                                     OSPFV3_OPTION_LEN);

        /* To take care of first condition of ddp reception in ex-start
         * RFC 2328 sec - 10.6.
         */
        if (pNbr->dbSummary.bMaster == OSIX_TRUE)
        {
            OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt,
                                         OSPFV3_DDP_MASK_OFFSET,
                                         (OSPFV3_I_BIT_MASK | OSPFV3_M_BIT_MASK
                                          | OSPFV3_MS_BIT_MASK));
        }

        /* To take care of second condition of ddp reception in ex-start
         * RFC 2328 sec - 10.6.
         */
        else
        {
            OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt,
                                         OSPFV3_DDP_MASK_OFFSET,
                                         (OSPFV3_M_BIT_MASK));
        }

        OSPFV3_BUFFER_ASSIGN_4_BYTE (pOspfPkt,
                                     OSPFV3_DDP_SEQ_NUM_OFFSET,
                                     pNbr->dbSummary.seqNum);
        i1TimerType = OSPFV3_DD_INIT_RXMT_TIMER;

        /* Assign ospfv3 packet size */
        u2DdLen = OSPFV3_HEADER_SIZE + OSPFV3_DDP_FIXED_PORTION_SIZE;
    }
    else if (pNbr->u1NsmState == OSPFV3_NBRS_EXCHANGE)
    {
        /* Alloc buffer and construct pkt */
        if ((pNbr->dbSummary.dbSummaryPkt =
             V3UtilOsMsgAlloc (OSPFV3_IFACE_MTU (pNbr->pInterface))) == NULL)
        {

            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to Allocate Memory for DDP Tx to Nbr %x\n",
                          OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId)));

            OSPFV3_TRC1 (OS_RESOURCE_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Alloc Failure For DDP Transmission To Neighbor %x\n",
                         OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));
            return;
        }

        pOspfPkt = pNbr->dbSummary.dbSummaryPkt + IPV6_HEADER_LEN;

        /* 2328 - 2 bytes  filled with interface MTU  */
        if (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
        {
            OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                         OSPFV3_DDP_IFACE_MTU_OFFSET, 0);
        }
        else
        {
            OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                         OSPFV3_DDP_IFACE_MTU_OFFSET,
                                         (UINT2) pNbr->pInterface->u4MtuSize);
        }

        if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF
            (pNbr->pInterface))
        {
            options[OSPFV3_OPT_BYTE_THREE] |=
                pNbr->pInterface->ifOptions[OSPFV3_OPT_BYTE_THREE];
        }

        OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt,
                                     options,
                                     OSPFV3_DDP_OPTIONS_OFFSET,
                                     OSPFV3_OPTION_LEN);
        OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt, OSPFV3_DDP_MASK_OFFSET, u1Flags);
        OSPFV3_BUFFER_ASSIGN_4_BYTE (pOspfPkt,
                                     OSPFV3_DDP_SEQ_NUM_OFFSET,
                                     pNbr->dbSummary.seqNum);

        u2Len = V3DdpAddSummay (pNbr);
        pNbr->dbSummary.u2LastTxedSummaryLen = u2Len;
        if (V3DdpIsEmptySummary (pNbr) == OSIX_TRUE)
        {
            u1Flags = ((pNbr->dbSummary.bMaster == OSIX_TRUE)
                       ? OSPFV3_MS_BIT_MASK : 0);
        }
        else
        {
            u1Flags = (UINT1) (OSPFV3_M_BIT_MASK |
                               ((pNbr->dbSummary.bMaster == OSIX_TRUE) ?
                                OSPFV3_MS_BIT_MASK : 0));
        }
        OSPFV3_BUFFER_ASSIGN_1_BYTE (pOspfPkt, OSPFV3_DDP_MASK_OFFSET, u1Flags);

        if (pNbr->dbSummary.bMaster == OSIX_TRUE)
        {
            /* Since it is the master it starts timer for retransmission of the 
             * DDP Packet.
             */
            i1TimerType = OSPFV3_DD_RXMT_TIMER;
        }

        /* Assign OSPFv3 packet size */
        u2DdLen = (UINT2) (OSPFV3_HEADER_SIZE +
                           OSPFV3_DDP_FIXED_PORTION_SIZE + u2Len);
    }
    else
    {

        /* neighbor in state other than exchange or exstart */
        if (pNbr->u1NsmState < OSPFV3_MAX_NBR_STATE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Procedure Invoked in Improper Nbr State %s\n",
                          gau1Os3DbgNbrState[pNbr->u1NsmState]));

            OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Procedure Invoked in Improper Nbr State %s\n",
                         gau1Os3DbgNbrState[pNbr->u1NsmState]);
        }
        return;
    }

    V3UtilConstructHdr (pNbr->pInterface, pOspfPkt, OSPFV3_DD_PKT, u2DdLen);

    V3PppSendPkt (pNbr->dbSummary.dbSummaryPkt,
                  (UINT2) (OSPFV3_HEADER_SIZE + OSPFV3_DDP_FIXED_PORTION_SIZE +
                           pNbr->dbSummary.u2LastTxedSummaryLen),
                  pNbr->pInterface, &(pNbr->nbrIpv6Addr));

    if (i1TimerType != OSPFV3_INVALID_TIMER_TYPE)
    {
        if (pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA)
        {
            u4Interval = V3UtilJitter (pNbr->pInterface->u2RxmtInterval,
                                       OSPFV3_JITTER);
        }
        else
        {
            u4Interval = pNbr->pInterface->u2RxmtInterval;
        }

        V3TmrSetTimer (&(pNbr->dbSummary.ddTimer), i1TimerType,
                       OSPFV3_NO_OF_TICKS_PER_SEC * u4Interval);
    }

    OSPFV3_TRC1 (OSPFV3_DDP_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DDP Transmitted To Nbr %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3DdpSendDdp\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpAddSummay                                             */
/*                                                                           */
/* Description  : This function builds the summary list for DDP packet       */
/*                                                                           */
/* Input        : pNbr             : Pointer to the neighbor structure to    */
/*                                   which the DDP packet has to be sent.    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Length of the constructed LSA headers                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT2
V3DdpAddSummay (tV3OsNeighbor * pNbr)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT2               u2DdpLen = 0;
    UINT4               u4MaxTxLen = 0;
    UINT1              *pNextLsHeaderStart = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER:V3DdpAddSummay\n");

    /* If all the Lsas are already transmitted, */
    if ((pNbr->dbSummary.lstTxedLsa == NULL) &&
        (pNbr->dbSummary.pLsaRBRoot == NULL))
    {
        return (u2DdpLen);
    }

    /* Maximum length of the ddp headers to be transmitted */
    u4MaxTxLen = (OSPFV3_IFACE_MTU (pNbr->pInterface) -
                  (IPV6_HEADER_LEN + OSPFV3_HEADER_SIZE +
                   OSPFV3_DDP_FIXED_PORTION_SIZE));

    if ((pNbr->dbSummary.lstTxedLsa == NULL) &&
        (pNbr->dbSummary.pLsaRBRoot != NULL))
    {
        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pNbr->dbSummary.pLsaRBRoot);
    }
    else
    {
        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetNext (pNbr->dbSummary.pLsaRBRoot,
                                                   (tRBElem *) pNbr->dbSummary.
                                                   lstTxedLsa, NULL);
    }

    pNextLsHeaderStart = (pNbr->dbSummary.dbSummaryPkt +
                          IPV6_HEADER_LEN + OSPFV3_HEADER_SIZE +
                          OSPFV3_DDP_FIXED_PORTION_SIZE);
    /* Add Lsas */
    do
    {
        if (pLsaInfo != NULL)
        {
            if (V3DdpAddToSummary (pNbr, pLsaInfo,
                                   pNextLsHeaderStart) == OSIX_TRUE)
            {
                pNextLsHeaderStart += OSPFV3_LS_HEADER_SIZE;
                u4MaxTxLen -= OSPFV3_LS_HEADER_SIZE;
                u2DdpLen += OSPFV3_LS_HEADER_SIZE;
            }

            /* If Pkt length is less than header size, then return */
            if (u4MaxTxLen < OSPFV3_LS_HEADER_SIZE)
            {
                break;
            }

            pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetNext (pNbr->dbSummary.pLsaRBRoot,
                               (tRBElem *) pLsaInfo, NULL);
        }

        if (pLsaInfo == NULL)
        {
            if (pNbr->dbSummary.pLsaRBRoot ==
                pNbr->pInterface->pLinkScopeLsaRBRoot)
            {
                pNbr->dbSummary.pLsaRBRoot =
                    pNbr->pInterface->pArea->pAreaScopeLsaRBRoot;
                pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetFirst (pNbr->dbSummary.pLsaRBRoot);
            }
            else if (pNbr->dbSummary.pLsaRBRoot ==
                     pNbr->pInterface->pArea->pAreaScopeLsaRBRoot)
            {
                if ((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) ||
                    (pNbr->pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) ||
                    (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL))
                {
                    pLsaInfo = NULL;
                    pNbr->dbSummary.pLsaRBRoot = NULL;
                    break;
                }

                pNbr->dbSummary.pLsaRBRoot =
                    pNbr->pInterface->pArea->pV3OspfCxt->pAsScopeLsaRBRoot;
                pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetFirst (pNbr->dbSummary.pLsaRBRoot);
            }
            else
            {
                pNbr->dbSummary.pLsaRBRoot = NULL;
                break;
            }
        }
    }
    while ((u4MaxTxLen > OSPFV3_LS_HEADER_SIZE));

    /* Store the last transmitted packet */
    pNbr->dbSummary.lstTxedLsa = pLsaInfo;

    /* If pLsaInfo is the last Lsa in the AreaScope LSA List, then 
     * no need to send more Ddp packet */
    V3DdpUpdateLsaLst (pNbr);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT:V3DdpAddSummay\n");
    return (u2DdpLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DdpUpdateLsaLst                                          */
/*                                                                           */
/* Description  : This function checks and updates the LSA lists whether     */
/*                more DDP packet has to be transmitted or not               */
/*                                                                           */
/* Input        : pNbr             : Pointer to the neighbor structure to    */
/*                                   which the DDP packet has to be sent.    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3DdpUpdateLsaLst (tV3OsNeighbor * pNbr)
{
    tV3OsLsaInfo       *pTmpLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER:V3DdpUpdateLsaLst\n");
    if (pNbr->dbSummary.pLsaRBRoot == pNbr->pInterface->pLinkScopeLsaRBRoot)
    {
        pTmpLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pNbr->dbSummary.pLsaRBRoot,
                           (tRBElem *) pNbr->dbSummary.lstTxedLsa, NULL);
        if ((pTmpLsaInfo == NULL) &&
            (pNbr->pInterface->pArea->u4AreaScopeLsaCount == 0) &&
            (((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) ||
              (pNbr->pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) ||
              (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)) ||
             (pNbr->pInterface->pArea->pV3OspfCxt->u4AsScopeLsaCount == 0)))
        {
            pNbr->dbSummary.pLsaRBRoot = NULL;
            pNbr->dbSummary.lstTxedLsa = NULL;
        }
    }
    else if (pNbr->dbSummary.pLsaRBRoot ==
             pNbr->pInterface->pArea->pAreaScopeLsaRBRoot)
    {
        pTmpLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pNbr->dbSummary.pLsaRBRoot,
                           (tRBElem *) pNbr->dbSummary.lstTxedLsa, NULL);
        if ((pTmpLsaInfo == NULL) &&
            ((pNbr->pInterface->pArea->pV3OspfCxt->u4AsScopeLsaCount == 0) ||
             ((pNbr->pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA) ||
              (pNbr->pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) ||
              (pNbr->pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL))))
        {
            pNbr->dbSummary.pLsaRBRoot = NULL;
            pNbr->dbSummary.lstTxedLsa = NULL;
        }
    }
    else if (pNbr->dbSummary.pLsaRBRoot ==
             pNbr->pInterface->pArea->pV3OspfCxt->pAsScopeLsaRBRoot)
    {
        pTmpLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pNbr->dbSummary.pLsaRBRoot,
                           (tRBElem *) pNbr->dbSummary.lstTxedLsa, NULL);
        if (pTmpLsaInfo == NULL)
        {
            pNbr->dbSummary.pLsaRBRoot = NULL;
            pNbr->dbSummary.lstTxedLsa = NULL;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT:V3DdpUpdateLsaLst\n");
}

/* -----------------------------------------------------------------------*/
/*                        End of the file o3ddp.c                         */
/*------------------------------------------------------------------------*/
