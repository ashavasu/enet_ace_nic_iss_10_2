/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3gr.c,v 1.20 2017/12/26 13:34:26 siva Exp $
 *
 * Description:This file contains the restarting router related
 *             modules during Graceful restart process.
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrInitiateRestart                                        */
/*                                                                           */
/* Description  : This routine triggers the graceful shutdown of OSPF        */
/*                process. If the application is registered, grace LSA is    */
/*                constructed and sent out through all the interfaces        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrInitiateRestart (VOID)
{
    tV3OsInterface     *pInterface = NULL;    /* Interface through which the
                                               LSA is to be transmitted */
    tTMO_SLL_NODE      *pLstNode = NULL;    /* Interface SLL node */
    tV3OspfCxt         *pV3OspfCxt = NULL;    /* pointer to context */
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    UINT4               u4PrevCxtId = OSPFV3_INVALID_CXT_ID;
    UINT1               u1RestartSupport = OSPFV3_INIT_VAL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER :O3GrInitiateRestart\r\n");
    /* Scan all the context */
    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
    {
        /* No context available */
        return OSIX_FAILURE;
    }

    do
    {
        u4PrevCxtId = u4ContextId;
        pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
        if (pV3OspfCxt == NULL)
        {
            return OSIX_FAILURE;
        }

        if ((pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_BOTH) &&
            (pV3OspfCxt->u1RestartReason == OSPFV3_GR_UNKNOWN))
        {
            pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_INPROGRESS;
            pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_NONE;
            /* Store all the GR specific information in non-volatile storage */
            O3GrShutdownProcess ();
            return OSIX_SUCCESS;
        }
        else if (pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_NONE)
        {
            pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_NONE;
            u1RestartSupport = (UINT1) (u1RestartSupport |
                                        (OSPFV3_ONE <<
                                         (pV3OspfCxt->u1RestartStatus - 1)));
        }
        else if (((pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_BOTH) &&
                  (pV3OspfCxt->u1RestartReason != OSPFV3_GR_UNKNOWN)) ||
                 (pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_PLANNED))
        {
            pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_PLANNED;
            u1RestartSupport = (UINT1) (u1RestartSupport |
                                        (OSPFV3_ONE <<
                                         (pV3OspfCxt->u1RestartStatus - 1)));
        }
        /* Initialise the Grace LSA transmission count */
        pV3OspfCxt->u1GraceLsaTxCount = OSPFV3_INIT_VAL;

        /* graceful restart process should not be triggered
         * if Graceful restart support is not configured. */

        if ((pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS) ||
            (pV3OspfCxt->u1RestartSupport == OSPFV3_RESTART_NONE) ||
            ((pV3OspfCxt->u1RestartSupport != OSPFV3_RESTART_BOTH) &&
             (pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_UNPLANNED)))
        {
            continue;
        }

        /* In case of unplanned restart, restart reason should be unknown */
        if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_UNPLANNED) &&
            (pV3OspfCxt->u1RestartReason != OSPFV3_GR_UNKNOWN))
        {
            continue;
        }

        /* Delete the route related timers and trigger the route calculation
         * to ensure that the forwarding table is up-to date.
         * RFC 3623 Section 2.1 Step 1 */
        V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));
        if (pV3OspfCxt->bVrfSpfTmrStatus == OSIX_TRUE)
        {
            TMO_DLL_Delete (&(gV3OsRtr.vrfSpfList),
                            &(pV3OspfCxt->nextVrfSpfNode));
            pV3OspfCxt->bVrfSpfTmrStatus = OSIX_FALSE;
        }

        /* Trigger the route calculation if the flag is set */
        if (pV3OspfCxt->u1RtrNetworkLsaChanged == OSIX_TRUE)
        {
            pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
            gV3OsRtr.u4LastRtCalcTime = OsixGetSysUpTime ();
            pV3OspfCxt->u4LastCalTime = gV3OsRtr.u4LastRtCalcTime;
            V3RtcCalculateRtInCxt (pV3OspfCxt);
        }

        /* Scan all the interfaces attached to the context and construct the
         * grace LSA for the each interface. */
        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        while (pInterface != NULL)
        {
            /* Generate Grace LSA */
            V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_GRACE_LSA_GEN,
                                   (UINT1 *) pInterface);
            pInterface = (tV3OsInterface *) RBTreeGetNext
                (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface, NULL);
        }

        /* Transmit the Grace LSA through all the attached virtual links */
        TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);

            /* Generate Grace LSA */
            V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_GRACE_LSA_GEN,
                                   (UINT1 *) pInterface);
        }

        /* Grace LSA is transmitted through all the interfaces in this
         * instance */
        pV3OspfCxt->u1GraceLsaTxCount++;

        /* Change the restart exit reason to in progress and
         * restart state to shutdown */
        pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_INPROGRESS;
        pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_NONE;

        /* Send Trap notification regarding restart status cheange */
        O3SnmpSendStatusChgTrapInCxt (pV3OspfCxt, NULL,
                                      OSPFV3_RST_STATUS_CHANGE_TRAP);

        /* Indicate the RTM regarding Graceful shutdown */
        V3RtmGrNotifInd (pV3OspfCxt);

        /* Start the grace timer */
        V3TmrSetTimer (&(pV3OspfCxt->graceTimer), OSPFV3_RESTART_GRACE_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC * pV3OspfCxt->u4GracePeriod);
    }
    while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4ContextId) != OSIX_FAILURE);

    /* Delete the global VRF SPF timer */
    V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
    if (u1RestartSupport == OSPFV3_RESTART_NONE)
    {
        V3OspfLock ();
        /* Normal Restart */
        V3OspfModuleShutDown ();
        V3OspfUnLock ();
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
        OsixDeleteQ (SELF, OSPFV3_LNXVRF_QUEUE_NAME);
#endif
        OsixDeleteSem (OSIX_TRUE, OSPFV3_MUT_EXCL_SEM_NAME);
        OsixDeleteSem (OSIX_TRUE, OSPFV3_RTMRT_LIST_SEM);
        OsixDeleteQ (SELF, (const UINT1 *) OSPFV3_QUEUE_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) OSPFV3_RM_QUEUE_NAME);
        OsixDeleteQ (SELF, (const UINT1 *) OSPFV3_LOW_PRIORITY_QUEUE_NAME);
#ifdef OSPF3_BCMXNP_HELLO_WANTED
        OsixDeleteQ (SELF, OSPFV3_NP_HELLO_QUEUE_NAME);
#endif

        /* Delete OSPFv3 Task */
        OsixDeleteTask (SELF, (CONST UINT1 *) "OSV3");
        return OSIX_SUCCESS;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrInitiateRestart\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrDeleteFromRxmtLst                                      */
/*                                                                           */
/* Description  : This routine deletes the given grace LSA from              */
/*                the re-transmission list. If the grace LSA's               */
/*                re-transmission count is 0, then check the re-transmission */
/*                count for all grace LSA to shutdown the OSPF process       */
/*                                                                           */
/* Input        : pNbr             - The neighbor to which grace LSA         */
/*                                   transmission has reached the threshold  */
/*                pGrLsaInfo       - pointer to Grace LSA info structure     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrDeleteFromRxmtLst (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pGrLsaInfo)
{
    tV3OsRxmtNode      *pRxmtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER :O3GrDeleteRxmtLst\r\n");

    pRxmtNode = V3LsuSearchLsaInRxmtLst (pNbr, pGrLsaInfo);

    if (pRxmtNode != NULL)
    {

        TMO_SLL_Delete (&(pNbr->lsaRxmtLst), &(pRxmtNode->nextLsaInfo));

        OSPFV3_RXMT_NODE_FREE (pRxmtNode);

        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbohr */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == OSPFV3_INIT_VAL)
        {
            V3TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }

        if (--pGrLsaInfo->u4RxmtCount == OSPFV3_INIT_VAL)
        {
            O3GrCheckRxmtLstInCxt (pGrLsaInfo->pV3OspfCxt);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrDeleteRxmtLst\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrCheckRxmtLstInCxt                                      */
/*                                                                           */
/* Description  : This routine checks the re-transmission count              */
/*                for all grace LSA in all interfaces. If all the grace LSA  */
/*                are transmitted, OSPF process is shutdown                  */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
O3GrCheckRxmtLstInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1TxFlag = OSIX_FALSE;
    UINT1               u1GrNoAckFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :O3GrCheckRxmtLstInCxt\r\n");

    /* If the Grace LSA re-transmission count is 0, then
     * a. The rest of the neighbors have acknowledged the Grace LSA
     * b. The re-transmission count has reached the threshold for the
     *    neighbors.
     * Since the Grace LSAs are trasnmissted , the router enters restart mode
     */

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    while ((pInterface != NULL) && (u1TxFlag == OSIX_FALSE))
    {
        /* Scan the Link Local scope LSAs of this interface and
         * get the seq num of grace LSA */
        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst
            (pInterface->pLinkScopeLsaRBRoot);
        while (pLsaInfo != NULL)
        {
            if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA) &&
                (pLsaInfo->u4RxmtCount != OSPFV3_INIT_VAL))
            {
                u1TxFlag = OSIX_TRUE;
                break;
            }
            pLsaInfo = (tV3OsLsaInfo *) RBTreeGetNext
                (pInterface->pLinkScopeLsaRBRoot, (tRBElem *) pLsaInfo, NULL);
        }
        /* check grace LSA ack is received */
        if ((pInterface->u1GrLsaAckRcvd == OSIX_FALSE) &&
            (pInterface->u4NbrFullCount != OSPFV3_INIT_VAL))
        {
            u1GrNoAckFlag = OSIX_TRUE;
        }
        pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                       (tRBElem *) pInterface,
                                                       NULL);
    }

    /* Scan all the virtual interfaces  attached to the context and check 
     * grace LSA ack is received */
    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst
            (pInterface->pLinkScopeLsaRBRoot);
        while (pLsaInfo != NULL)
        {
            if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA) &&
                (pLsaInfo->u4RxmtCount != OSPFV3_INIT_VAL))
            {
                u1TxFlag = OSIX_TRUE;
                break;
            }
            pLsaInfo = (tV3OsLsaInfo *) RBTreeGetNext
                (pInterface->pLinkScopeLsaRBRoot, (tRBElem *) pLsaInfo, NULL);
        }
        if ((pInterface->u1GrLsaAckRcvd == OSIX_FALSE) &&
            (pInterface->u4NbrFullCount != OSPFV3_INIT_VAL))
        {
            u1GrNoAckFlag = OSIX_TRUE;
        }
        if (u1TxFlag == OSIX_TRUE)
        {
            break;
        }
    }

    /* Grace LSA has been transmitted to all the neighbors in this
     * instance. Increment the grace LSA transmission count */
    if (u1TxFlag == OSIX_FALSE)
    {
        /* Grace LSA is not present in any of the re-transmission
         * list in this context */
        pV3OspfCxt->u1GraceLsaTxCount++;
    }

    if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
        (u1TxFlag == OSIX_FALSE))
    {
        if ((pV3OspfCxt->u4IsGraceAckReq == OSPFV3_ENABLED) &&
            (u1GrNoAckFlag == OSIX_TRUE))
        {
            pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_NONE;
            pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_SHUTDOWN;
            pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_NONE;
        }
        pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_SHUTDOWN;

        O3GrCheckAndShutdownProcess ();
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:O3GrCheckRxmtLstInCxt\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrCheckAndShutdownProcess                                */
/*                                                                           */
/* Description  : This routine checks the Grace LSA transmission             */
/*                count in all the context. If the transmission count has    */
/*                reached the threshold in all the context or ack received   */
/*                the routing process undergoes graceful shutdown.           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrCheckAndShutdownProcess (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;    /* pointer to context */
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    /* Current context id */
    UINT4               u4PrevCxtId = OSPFV3_INVALID_CXT_ID;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrCheckAndShutdownProcess\r\n");

    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
    {
        return;
    }
    else
    {
        do
        {
            u4PrevCxtId = u4ContextId;
            pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
            if (pV3OspfCxt == NULL)
            {
                return;
            }

            /* If the re-transmission count has reached the threshold,
             * scan the next context */
            if (pV3OspfCxt->u1GraceLsaTxCount < pV3OspfCxt->u1GrLsaMaxTxCount)
            {
                return;
            }

        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4ContextId)
               != OSIX_FAILURE);
    }

    OSPFV3_GBL_TRC (OSPFV3_RESTART_TRC,
                    "Router is shutting down gracefully\r\n");

    /* Store all the GR specific information in non-volatile storage */
    O3GrCfgStoreRestartInfo ();
    O3GrShutdownProcess ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrCheckAndShutdownProcess\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrShutdownProcess                                        */
/*                                                                           */
/* Description  : This routine does all the actions specific to              */
/*                Graceful shutdown and stores all the information           */
/*                necessary during restart. CSR is used to store the         */
/*                graceful restart related information.                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrShutdownProcess (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;    /* pointer to context */
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    /* Current context id */
    UINT4               u4PrevCxtId = OSPFV3_INVALID_CXT_ID;
    /* Current context id */
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:O3GrShutdownProcess\r\n");

    V3OspfUnLock ();
#ifdef CLI_WANTED
    IssCsrSaveCli ((UINT1 *) "OSV3");
#endif
    V3OspfLock ();

    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
    {
        return;
    }
    else
    {
        do
        {
            u4PrevCxtId = u4ContextId;
            pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
            if (pV3OspfCxt == NULL)
            {
                return;
            }
            O3GrApiDeleteCxt (pV3OspfCxt);
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4ContextId)
               != OSIX_FAILURE);
    }

    /* Delete all memory and delete the task */
    O3GrApiTaskDeInit ();

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrShutdownProcess\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrDisableInCxt                                           */
/*                                                                           */
/* Description  : This function is similar to V3RtrDisable                   */
/*                but does not flush the LSA from routing domain and         */
/*                does not indicate the RTM regarding route deletion         */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
O3GrDisableInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;    /* pointer to interface */
    tTMO_SLL_NODE      *pLstNode = NULL;    /* pointer to interface 
                                               SLL node */
    tV3OsArea          *pArea = NULL;    /* pointer ro Area */
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;
    tV3OsFwdAddr       *pTempFwdAddrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :O3GrDisable\r\n");
#ifdef L3_SWITCHING_WANTED
    /* Do the hardware initialisation to receive OSPF packets */
    if (OSPF3_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
    {
        Ospf3FsNpOspf3DeInit ();
    }
#endif /* L3_SWITCHING_WANTED */

    /* Since the process is undergoing graceful shutdown, self originated
     * LSA should not be flushed */
    pV3OspfCxt->u1RtrDisableInProgress = OSIX_TRUE;

    /* Delete all external routes redistributed from RTM */
    V3ExtrtDeleteAllInCxt (pV3OspfCxt);

    OSPFV3_DYNM_SLL_SCAN (&(pV3OspfCxt->fwdAddrLst),
                          pScanFwdAddrNode, pTempFwdAddrNode, tV3OsFwdAddr *)
    {
        TMO_SLL_Delete (&(pV3OspfCxt->fwdAddrLst),
                        &(pScanFwdAddrNode->nextFwdAddr));
        OSPFV3_FWD_ADDR_FREE (pScanFwdAddrNode);
    }
    pV3OspfCxt->admnStat = OSPFV3_DISABLED;

    RBTreeWalk (gV3OsRtr.pIfRBRoot, (tRBWalkFn) O3GrApiIfDisableRBTAction,
                NULL, NULL);

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
        O3GrApiIfDisable (pInterface);
    }

    /* Delete all the LSA from LSDB */
    O3GrApiLsaDeleteAllInCxt (pV3OspfCxt);

    gV3OsRtr.u4ActiveCxtCount--;
#ifdef RAWSOCK_WANTED
#if !(defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED))
    if (gV3OsRtr.u4ActiveCxtCount == 0)
#endif
    {
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
        SelRemoveFd (gV3OsRtr.ai4RawSockId[pV3OspfCxt->u4ContextId]);
#else
        SelRemoveFd (gV3OsRtr.i4RawSockId);
#endif
        /* Socket has to be closed */
        V3OspfCloseSocket (pV3OspfCxt->u4ContextId);
    }
#endif /* End of ifdef RAWSOCK_WANTED */

#ifdef IP6_WANTED
    NetIpv6DeRegisterHigherLayerProtocol (OSPFV3_PROTO);
#endif
    /* Initialise the ISM schedule queue */
    V3IsmInitSchedQueueInCxt (pV3OspfCxt, NULL);

    V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));

    pV3OspfCxt->u4AsScopeLsaChksumSum = 0;
    pV3OspfCxt->u4OriginateNewLsa = 0;
    pV3OspfCxt->u4RcvNewLsa = 0;
    pV3OspfCxt->u4AsExtLsaCount = 0;
    pV3OspfCxt->u4AsScopeLsaCount = 0;
    pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
    pV3OspfCxt->u1AsbSummaryLsaChanged = OSIX_FALSE;
    pV3OspfCxt->bOverflowState = OSIX_FALSE;
    pV3OspfCxt->u4ExitOverflowInterval = 0;
    pV3OspfCxt->u1VirtRtCal = OSIX_FALSE;
    /* Delete all routes from the OSPF routing table */
    O3GrApiRtDeleteAllInCxt (&pV3OspfCxt->ospfV3RtTable);

    /* Delete all the nodes from SPF tree in all areas */
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pLstNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pLstNode);
        V3RtcClearSpfTree (pArea->pSpf);
        pArea->u4AreaBdrRtrCount = OSPFV3_INIT_VAL;
        pArea->u4AsBdrRtrCount = OSPFV3_INIT_VAL;
    }

    V3TmrDeleteTimer (&(pV3OspfCxt->exitOverflowTimer));
    if (pV3OspfCxt->bVrfSpfTmrStatus == OSPFV3_TRUE)
    {
        pV3OspfCxt->bVrfSpfTmrStatus = OSPFV3_FALSE;
        TMO_DLL_Delete (&(gV3OsRtr.vrfSpfList), &(pV3OspfCxt->nextVrfSpfNode));
        if (TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) == 0)
        {
            V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
        }
    }
    V3TmrDeleteTimer (&(gV3OsRtr.g50msTimerNode));
#ifdef RRD_WANTED
    /* Disbale the re-distribution status and send an event
     * to RTM */
    if (pV3OspfCxt->u4RrdSrcProtoBitMask & OSPFV3_RRD_SRC_PROTO_BIT_MASK)
    {
        if (V3RtmRRDSrcProtoDisableInCxt
            (pV3OspfCxt, pV3OspfCxt->u4RrdSrcProtoBitMask) != OSIX_SUCCESS)
        {
            OSPFV3_EXT_TRC (OSPFV3_RESTART_TRC, pV3OspfCxt->u4ContextId,
                            "Re-distribution disable failed\r\n");
            return OSIX_FAILURE;
        }
    }
    pV3OspfCxt->u4RrdSrcProtoBitMask = OSPFV3_INIT_VAL;
#endif
    /* Delete the exit overflow timer */

    /* Graceful process should not de-register from RTM */
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrDisable\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrDeleteLsaFromDatabase                                  */
/*                                                                           */
/* Description  : This routine deletes LSA from the LSDB                     */
/*                The routine deletes the LSA from all rxmt list, deletes    */
/*                all LSA specific timers and clears the memory              */
/*                                                                           */
/* Input        : pLsaInfo - pointer to LSA info structure                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrDeleteLsaFromDatabase (tV3OsLsaInfo * pLsaInfo)
{
    UINT4               u4ContextId;
    u4ContextId = pLsaInfo->pV3OspfCxt->u4ContextId;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId,
                "ENTER : O3GrDeleteLsaFromDatabase\r\n");

    /* Delete the LSA from all the re-trasnmission list.
     * After deleting all the LSA from the rxmt list,
     * LSA rxmt timer in neighbor will be deleted */
    V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_FALSE);

    V3LsuUpdateChksumAndCount (pLsaInfo, OSPFV3_DELETE);

    /* Clear the LSA Descriptor.
     * This will delete the minimum LSA interval timer. */
    if (pLsaInfo->pLsaDesc != NULL)
    {
        V3LsuClearLsaDesc (pLsaInfo->pLsaDesc);
        pLsaInfo->pLsaDesc = NULL;
    }
    /* Clear the LSA. This will delete
     * lsaAgingTimer and dnaLsaSplAgingTimer */
    V3LsuClearLsaInfo (pLsaInfo);
    pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId,
                "EXIT :O3GrDeleteLsaFromDatabase\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrExitGracefulRestartInCxt                               */
/*                                                                           */
/* Description  : This routine does the actions required on exiting          */
/*                Graceful restart                                           */
/*                RFC 3623 Section 2.3                                       */
/*                                                                           */
/* Input        : pV3OspfCxt   - pointer to OSPFv3 Context                   */
/*                u1ExitReason - Reasong for Graceful restart exit           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrExitGracefulRestartInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1ExitReason)
{
    tRtm6RegnId         RegnId;
    tV3OsLinkStateId    linkStateId;
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pAreaNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1               u1AbrFlag = OSPFV3_INIT_VAL;

    MEMSET (&RegnId, OSPFV3_INIT_VAL, sizeof (tRtm6RegnId));
    MEMSET (linkStateId, OSPFV3_INIT_VAL, sizeof (tV3OsLinkStateId));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3GrExitGracefulRestartInCxt\r\n");

    /* Stop the grace timer */
    V3TmrDeleteTimer (&(pV3OspfCxt->graceTimer));
    /* Copy the registration id */
    MEMSET (&RegnId, OSPFV3_INIT_VAL, sizeof (tRtm6RegnId));
    RegnId.u2ProtoId = OSPF6_ID;
    RegnId.u4ContextId = pV3OspfCxt->u4ContextId;
    pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_NONE;
    pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_RESTART;
    pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_NONE;

    /* Send trap notification reagrding the status change.
     * Nbr parameter is required for Neighbor status change
     * when the router is acting as helper */
    O3SnmpSendStatusChgTrapInCxt (pV3OspfCxt, NULL,
                                  OSPFV3_RST_STATUS_CHANGE_TRAP);

    u1AbrFlag = V3RtrCheckABRStatInCxt (pV3OspfCxt);
    pV3OspfCxt->bAreaBdrRtr = u1AbrFlag;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pAreaNode);
        V3GenIntraAreaPrefixLsa (pArea);
    }

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    while (pInterface != NULL)
    {
        /* Re-originate link LSA for all interfaces */
        OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
        V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                       &linkStateId, (UINT1 *) pInterface, OSIX_FALSE);

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);
            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                V3NbrGenerateRtrNetworkIntraLsas (pNbr);
            }
        }
        /* In case of improper exit, If grace lsa is present in database, 
         * no need to originate grace lsa */

        pLsaInfo = V3LsuSearchDatabase (OSPFV3_GRACE_LSA, &linkStateId,
                                        &(pV3OspfCxt->rtrId), pInterface,
                                        pInterface->pArea);

        if ((u1ExitReason != OSPFV3_RESTART_COMPLETED) && (pLsaInfo == NULL))
        {
            O3GrApiConstAndSendGraceLsa (pInterface);
            pInterface->lsUpdate.pLsuPkt = NULL;
        }
        pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                       (tRBElem *) pInterface,
                                                       NULL);
    }

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
        OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
        V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                       &linkStateId, (UINT1 *) pInterface->pArea, OSIX_FALSE);
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);
            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                V3NbrGenerateRtrNetworkIntraLsas (pNbr);
            }
        }

        /* In case of improper exit, If grace lsa is present in database, 
         * no need to originate grace lsa */

        pLsaInfo = V3LsuSearchDatabase (OSPFV3_GRACE_LSA, &linkStateId,
                                        &(pV3OspfCxt->rtrId), pInterface,
                                        pInterface->pArea);

        if ((u1ExitReason != OSPFV3_RESTART_COMPLETED) && (pLsaInfo == NULL))
        {
            O3GrApiConstAndSendGraceLsa (pInterface);
            pInterface->lsUpdate.pLsuPkt = NULL;
        }
    }
    V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));
    /* Step 3 */
    /* Delete all the routes from OSPF  */
    O3GrApiRtDeleteAllInCxt (&pV3OspfCxt->ospfV3RtTable);

    /* Delete all the nodes from SPF tree in all areas */
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pAreaNode);
        V3RtcClearSpfTree (pArea->pSpf);
        pArea->u4AreaBdrRtrCount = OSPFV3_INIT_VAL;
        pArea->u4AsBdrRtrCount = OSPFV3_INIT_VAL;
    }

    /* Schedule route calculation */
    pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
    V3RtcCalculateRtInCxt (pV3OspfCxt);
    gV3OsRtr.u4LastRtCalcTime = OsixGetSysUpTime ();
    pV3OspfCxt->u4LastCalTime = gV3OsRtr.u4LastRtCalcTime;

    /* Generate Type 5 and Type 7 LSA */
    V3ExtrtLsaPolicyHandlerInCxt (pV3OspfCxt);

    /* Step 4 */
    /*To Delete all the invalid routes in RTM6 post an event to RTM6 */
    Rtm6ApiHandleProtocolStatusInCxt (RegnId.u4ContextId,
                                      (UINT1) RegnId.u2ProtoId, RTM6_GR_CLEANUP,
                                      0, 0, 0, 0);

    pV3OspfCxt->u1RestartExitReason = u1ExitReason;

    /* Step 5 */
    /* Flush all unwanted self originated LSAs */
    O3GrApiFlushUnwantedSelfLsa (pV3OspfCxt);

    /*Remove the GR conf file */
    FileDelete (OSPFV3_GR_CONF);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrExitGracefulRestartInCxt\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrChkAndExitGracefulRestart                              */
/*                                                                           */
/* Description  : This routine checks and exits the graceful restart after   */
/*                restoring the pre-restart adjaceny with the neighbors      */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrChkAndExitGracefulRestart (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1AdjFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: O3GrChkAndExitGracefulRestart\r\n");

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
    {
        u1AdjFlag = OSIX_FALSE;
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pNbrNode);
        if (V3NsmToBecomeAdj (pNbr) == OSIX_TRUE)
        {
            /* If neighbor state is not full then GR is not yet completed
               so return */
            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                u1AdjFlag = OSIX_TRUE;
            }
        }
        else
        {
            /* If neighbor state is not two-way then GR is not 
             * yet completed so return */
            if (pNbr->u1NsmState == OSPFV3_NBRS_2WAY)
            {
                u1AdjFlag = OSIX_TRUE;
            }
        }
        if (u1AdjFlag == OSIX_FALSE)
        {
            return;
        }
    }
    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                          pLstNode);
        if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
        {
            u1AdjFlag = OSIX_FALSE;
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
            /* If neighbor state is not full then GR is not yet completed
               so return */
            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                u1AdjFlag = OSIX_TRUE;
            }
            if (u1AdjFlag == OSIX_FALSE)
            {
                return;
            }
        }
    }

    O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_COMPLETED);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrChkAndExitGracefulRestart\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function    : O3GrSetLsIdInterAreaPrefixLsa                               */
/*                                                                           */
/* Description : This function generates the link-state id for               */
/*               Inter-Area-Prefix LSA and assigns to the corresponding      */
/*               Rt-Entry or Address range depending on u2Type               */
/*                                                                           */
/* Input       : pV3OspfCxt   -  Context pointer                             */
/*               pPtr         -  Pointer to tV3OsRtEntry if u2Type =         */
/*                               OSPFV3_INTER_AREA_PREFIX_LSA                */
/*                               Otherwise,                                  */
/*                               Pointer to tV3OsAddrRange                   */
/*               u2Type       -  Type of LSA                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrSetLsIdInterAreaPrefixLsa (tV3OspfCxt * pV3OspfCxt,
                               UINT1 *pPtr, UINT2 u2Type)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsGRLsIdInfo     GRLsIdInfo, *pLsIdEntry = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: O3GrSetLsIdInterAreaPrefixLsa\r\n");
    if (u2Type == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;
        if (pRtEntry->u1PrefixLen == OSPFV3_INIT_VAL)
        {
            MEMSET (pRtEntry->linkStateId, OSPFV3_INIT_VAL,
                    OSPFV3_LINKSTATE_ID_LEN);
            return;
        }
        GRLsIdInfo.u1PrefixLength = pRtEntry->u1PrefixLen;
        OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                     pRtEntry->destRtPrefix,
                                     pRtEntry->u1PrefixLen);
        pLsIdEntry =
            (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                           (tRBElem *) & GRLsIdInfo);
        if (pLsIdEntry != NULL)
        {
            OSPFV3_LINK_STATE_ID_COPY (pRtEntry->linkStateId,
                                       pLsIdEntry->linkStateId);
            pV3OspfCxt->u4InterAreaLsaCounter =
                OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
            RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
            OSPFV3_LSAID_FREE (pLsIdEntry);
            return;
        }
    }
    else
    {
        pAddrRange = (tV3OsAddrRange *) (VOID *) pPtr;
        if (pAddrRange->u1PrefixLength == OSPFV3_INIT_VAL)
        {
            MEMSET (pAddrRange->linkStateId, OSPFV3_INIT_VAL,
                    OSPFV3_LINKSTATE_ID_LEN);
            return;
        }
        GRLsIdInfo.u1PrefixLength = pAddrRange->u1PrefixLength;
        OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                     pAddrRange->ip6Addr,
                                     pAddrRange->u1PrefixLength);
        pLsIdEntry =
            (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                           (tRBElem *) & GRLsIdInfo);
        if (pLsIdEntry != NULL)
        {
            OSPFV3_LINK_STATE_ID_COPY (pAddrRange->linkStateId,
                                       pLsIdEntry->linkStateId);
            pV3OspfCxt->u4InterAreaLsaCounter =
                OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
            RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
            OSPFV3_LSAID_FREE (pLsIdEntry);

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT: O3GrSetLsIdInterAreaPrefixLsa\r\n");
            return;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function    : O3GrSetLsIdAsExtLsaInCxt                                    */
/*                                                                           */
/* Description : This function generates the link-state id for               */
/*               AS-External LSA and assigns to the corresponding AddrRange  */
/*               or ExtRoute or AsExtAddrRange depending on u2Type           */
/*                                                                           */
/* Input       : pV3OspfCxt   - Context pointer                              */
/*               pPtr         - Pointer to tV3OsAddrRange if u2Type =        */
/*                              OSPFV3_NSSA_EXTERNAL_LINK                    */
/*                              Pointer to tV3OsExtRoute if u2Type =         */
/*                              OSPFV3_AS_EXT_LSA                            */
/*                              Otherwise,                                   */
/*                              Pointer to tV3OsAsExtAddrRange               */
/*               u2Type       -  Type of LSA                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrSetLsIdAsExtLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr, UINT2 u2Type)
{
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsGRLsIdInfo     GRLsIdInfo, *pLsIdEntry = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: O3GrSetLsIdAsExtLsaInCxt\r\n");
    switch (u2Type)
    {
        case OSPFV3_AS_EXT_LSA:
            pExtRoute = (tV3OsExtRoute *) (VOID *) pPtr;
            if (pExtRoute->u1PrefixLength == OSPFV3_INIT_VAL)
            {
                MEMSET (pExtRoute->linkStateId, OSPFV3_INIT_VAL,
                        OSPFV3_LINKSTATE_ID_LEN);
            }
            else
            {
                GRLsIdInfo.u1PrefixLength = pExtRoute->u1PrefixLength;
                OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                             pExtRoute->ip6Prefix,
                                             pExtRoute->u1PrefixLength);
                pLsIdEntry =
                    (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                                   (tRBElem *) & GRLsIdInfo);
                if (pLsIdEntry != NULL)
                {
                    OSPFV3_LINK_STATE_ID_COPY (pExtRoute->linkStateId,
                                               pLsIdEntry->linkStateId);
                    pV3OspfCxt->u4AsExtLsaCounter =
                        OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
                    RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
                    OSPFV3_LSAID_FREE (pLsIdEntry);
                    return;
                }

            }
            break;
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
            pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;
            if (pRtEntry->u1PrefixLen == OSPFV3_INIT_VAL)
            {
                MEMSET (pRtEntry->linkStateId, OSPFV3_INIT_VAL,
                        OSPFV3_LINKSTATE_ID_LEN);
            }
            else
            {
                GRLsIdInfo.u1PrefixLength = pRtEntry->u1PrefixLen;
                OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                             pRtEntry->destRtPrefix,
                                             pRtEntry->u1PrefixLen);
                pLsIdEntry =
                    (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                                   (tRBElem *) & GRLsIdInfo);
                if (pLsIdEntry != NULL)
                {
                    OSPFV3_LINK_STATE_ID_COPY (pRtEntry->linkStateId,
                                               pLsIdEntry->linkStateId);
                    pV3OspfCxt->u4AsExtLsaCounter =
                        OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
                    RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
                    OSPFV3_LSAID_FREE (pLsIdEntry);
                    return;
                }
            }
            break;
        case OSPFV3_COND_AS_EXT_LSA:
            pAsExtRange = (tV3OsAsExtAddrRange *) (VOID *) pPtr;
            if (pAsExtRange->u1PrefixLength == OSPFV3_INIT_VAL)
            {
                MEMSET (pAsExtRange->linkStateId, OSPFV3_INIT_VAL,
                        OSPFV3_LINKSTATE_ID_LEN);
            }
            else
            {
                GRLsIdInfo.u1PrefixLength = pAsExtRange->u1PrefixLength;
                OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                             pAsExtRange->ip6Addr,
                                             pAsExtRange->u1PrefixLength);
                pLsIdEntry =
                    (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                                   (tRBElem *) & GRLsIdInfo);
                if (pLsIdEntry != NULL)
                {
                    OSPFV3_LINK_STATE_ID_COPY (pAsExtRange->linkStateId,
                                               pLsIdEntry->linkStateId);
                    pV3OspfCxt->u4AsExtLsaCounter =
                        OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
                    RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
                    OSPFV3_LSAID_FREE (pLsIdEntry);
                    return;

                }
            }
            break;
        default:
            pAddrRange = (tV3OsAddrRange *) (VOID *) pPtr;
            if (pAddrRange->u1PrefixLength == OSPFV3_INIT_VAL)
            {
                MEMSET (pAddrRange->linkStateId, OSPFV3_INIT_VAL,
                        OSPFV3_LINKSTATE_ID_LEN);
            }
            else
            {
                GRLsIdInfo.u1PrefixLength = pAddrRange->u1PrefixLength;
                OSPFV3_IP6_ADDR_PREFIX_COPY (GRLsIdInfo.addrPrefix,
                                             pAddrRange->ip6Addr,
                                             pAddrRange->u1PrefixLength);
                pLsIdEntry =
                    (tV3OsGRLsIdInfo *) RBTreeGet (gV3OsRtr.pGRLsIdInfo,
                                                   (tRBElem *) & GRLsIdInfo);
                if (pLsIdEntry != NULL)
                {
                    OSPFV3_LINK_STATE_ID_COPY (pAddrRange->linkStateId,
                                               pLsIdEntry->linkStateId);
                    pV3OspfCxt->u4AsExtLsaCounter =
                        OSPFV3_BUFFER_DWFROMPDU (pLsIdEntry->linkStateId);
                    RBTreeRemove (gV3OsRtr.pGRLsIdInfo, pLsIdEntry);
                    OSPFV3_LSAID_FREE (pLsIdEntry);

                    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                                "EXIT: O3GrSetLsIdAsExtLsaInCxt\r\n");
                    return;

                }
            }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3gr.c                         */
/*-----------------------------------------------------------------------*/
