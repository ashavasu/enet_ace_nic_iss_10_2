/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtr.c,v 1.31 2018/01/16 10:46:02 siva Exp $
 *
 * Description:This file contains procedures for enabling or 
 *             disabling several Routre Status
 *
 *******************************************************************/

#include "o3inc.h"
#include "fsmisos3cli.h"
#ifdef L3_SWITCHING_WANTED
#include "ip6np.h"
#endif
#ifdef ICCH_WANTED
#include "icch.h"
#endif

PRIVATE VOID V3RtrInitialiseCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID V3RtrSetDefaultValuesInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetProtocolStatus                                     */
/*                                                                           */
/* Description  : Enables the OSPFV3 protocol.                               */
/*                                                                           */
/* Input        : pV3OspfCxt    : Context pointer                            */
/*                u1AdmnStatus  : admin status to be set                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3RtrSetProtocolStatusInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1AdmnStatus)
{
    INT1                i1RetStat = OSIX_FAILURE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetProtocolStatusInCxt\n");

    switch (u1AdmnStatus)
    {
        case OSPFV3_ENABLED:
            if (pV3OspfCxt->admnStat != OSPFV3_ENABLED)
            {
                i1RetStat = V3RtrEnableInCxt (pV3OspfCxt, OSPFV3_FALSE);
            }
            break;

        case OSPFV3_DISABLED:
            if (pV3OspfCxt->admnStat != OSPFV3_DISABLED)
            {
                if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
                {
                    i1RetStat = V3RtrDisableInCxt (pV3OspfCxt, OSPFV3_FALSE);
                }
                else if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART)
                {
                    i1RetStat = O3GrDisableInCxt (pV3OspfCxt);
                }
            }
            break;

        default:
            break;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetProtocolStatusInCxt\n");

    return i1RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetAsbrStatusInCxt                                    */
/*                                                                           */
/* Description  : sets the ASBR status of OSPFV3 router                      */
/*                                                                           */
/* Input        : pV3OspfCxt       : Context pointer                         */
/*                u1AsBdrRtrStatus : OSIX_TRUE  - Makes router as ASBR       */
/*                                       OSIX_FALSE - Non ASBR router        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3RtrSetAsbrStatusInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1AsBdrRtrStatus)
{
    tV3OsArea          *pArea = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetAsbrStatusInCxt\n");

    if (u1AsBdrRtrStatus == OSIX_TRUE)
    {
        /* newly configured as AS boundary router */
        pV3OspfCxt->bAsBdrRtr = OSIX_TRUE;
        if ((OSPFV3_IS_RTR_ENABLED (pV3OspfCxt)) &&
            (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
        {
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                V3GenLowestLsIdRtrLsa (pArea);
            }
            V3GenerateLsasForExtRoutesInCxt (pV3OspfCxt);
        }
    }
    else if (u1AsBdrRtrStatus == OSIX_FALSE)
    {
        /* 
         * This router is no longer AS boundary router.
         * Self originated AS ext LSAs should be flushed from
         * the routing domain.
         */
        pV3OspfCxt->bAsBdrRtr = OSIX_FALSE;
        if ((OSPFV3_IS_RTR_ENABLED (pV3OspfCxt)) &&
            (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
        {
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                V3GenLowestLsIdRtrLsa (pArea);
            }
            V3LsuFlushAllSelfOrgExtLsasInCxt (pV3OspfCxt);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetAsbrStatusInCxt\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3RtrEnterOverflowStateInCxt                            */
/*                                                                           */
/* Description     : actions taken when the router enters overflow state     */
/*                                                                           */
/* Input           : pV3OspfCxt   -  Context pointer                         */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3RtrEnterOverflowStateInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrEnterOverflowStateInCxt\n");

    pV3OspfCxt->bOverflowState = OSIX_TRUE;
    if (OSPFV3_IS_RTR_CFGD_TO_LEAVE_OVFL_STATE (pV3OspfCxt))
    {
        V3TmrSetTimer (&(pV3OspfCxt->exitOverflowTimer),
                       OSPFV3_EXIT_OVERFLOW_TIMER,
                       (INT4) V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                            (pV3OspfCxt->
                                             u4ExitOverflowInterval),
                                            OSPFV3_JITTER));
    }

    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
    {
        V3LsuFlushAllNonDefSelfOrgAseLsaInCxt (pV3OspfCxt);
    }
    else if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* router is trying to enter the overflow state
         * in graceful restart mode */
        O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_TOP_CHG);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrEnterOverflowStateInCxt\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetExtLsdbLimitInCxt                                  */
/*                                                                           */
/* Description  : Set Ext Lsdb Limit if router contains more non default     */
/*                ext Lsas ans database size is limited . it enters into     */
/*                overflow state.                                            */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Contetx pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3RtrSetExtLsdbLimitInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetExtLsdbLimitInCxt\n");

    if (pV3OspfCxt->i4ExtLsdbLimit == OSPFV3_NO_LIMIT)
    {
        V3TmrDeleteTimer (&(pV3OspfCxt->exitOverflowTimer));
        pV3OspfCxt->bOverflowState = OSIX_FALSE;
        V3LsuGenerateNonDefaultAsExtLsasInCxt (pV3OspfCxt);
    }

    if ((!(OSPFV3_IS_IN_OVERFLOW_STATE (pV3OspfCxt))) &&
        (OSPFV3_IS_EXT_LSDB_SIZE_LIMITED (pV3OspfCxt)) &&
        (OSPFV3_GET_NON_DEF_ASE_LSA_COUNT (pV3OspfCxt) >=
         pV3OspfCxt->i4ExtLsdbLimit))
    {

        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_ADJACENCY_TRC,
                    pV3OspfCxt->u4ContextId, "Rtr Enters OvrFlw State\n");
        V3RtrEnterOverflowStateInCxt (pV3OspfCxt);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetExtLsdbLimitInCxt\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrCreateCxt                                             */
/*                                                                           */
/* Description  : Creates an instance in OSPFv3                              */
/*                Called during                                              */
/*                1. Task initialization for default context                 */
/*                2. Low lwvwle routines during context creation             */
/*                                                                           */
/* Input        : u4ContextId -  Context Id                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3RtrCreateCxt (UINT4 u4ContextId)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsAreaId         bBoneAreaId;
    UINT1               u1ProtoIndex;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, u4ContextId,
                 "ENTER : V3RtrCreateCxt for Cxt : %d\n", u4ContextId);

    MEMSET (&bBoneAreaId, 0, sizeof (tV3OsAreaId));
    /* Allocate the context structure from the memory pool */
    OSPFV3_CXT_ALLOC (&pV3OspfCxt);

    if (pV3OspfCxt == NULL)
    {
        /* Context allocation failed */

        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Context memory block allocation failed for cxt: %d\n",
                      u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Context memory block allocation failed for cxt: %d\n",
                     u4ContextId);

        return OSIX_FAILURE;
    }

    MEMSET (pV3OspfCxt, 0, sizeof (tV3OspfCxt));
    pV3OspfCxt->u4ContextId = u4ContextId;
    pV3OspfCxt->admnStat = OSPFV3_DISABLED;

    /* Initialise the SLL and DLL present in the context structure */
    V3RtrInitialiseCxt (pV3OspfCxt);
    V3RtrSetDefaultValuesInCxt (pV3OspfCxt);

    /* Initialise the routine table strucutes */
    if (V3RtcInitRtInCxt (pV3OspfCxt, OSPFV3_RT_ID,
                          OSPFV3_RT_TYPE_IN_CXT (pV3OspfCxt)) == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create context external routing table "
                      "for cxt : %d\n", u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Context routing table creation failed : %d\n",
                     u4ContextId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    /* Initialise the external routine table strucutes */
    if (V3ExtRtInitRtInCxt (pV3OspfCxt, OSPFV3_RT_ID,
                            OSPFV3_EXT_RT_TYPE_IN_CXT (pV3OspfCxt))
        == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create context external routing table "
                      "for cxt : %d\n", u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Context external routing table creation failed "
                     "for cxt : %d\n", u4ContextId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    /* Create the candidate hash table for route calculation */
    pV3OspfCxt->pCandteLst =
        TMO_HASH_Create_Table (OSPFV3_CANDTE_HASH_TABLE_SIZE, NULL, FALSE);

    if (pV3OspfCxt->pCandteLst == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Context candidate hash"
                      "for cxt : %d\n", u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Context candidate hash creation failed for "
                     "cxt : %d\n", u4ContextId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    /* Create the backbone area */
    if (pV3OspfCxt != NULL)
    {
        pV3OspfCxt->pBackbone = V3AreaCreateInCxt (pV3OspfCxt, &bBoneAreaId);

        if (pV3OspfCxt->pBackbone == NULL)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to create Backbone area for  cxt : %d\n",
                          u4ContextId));

            OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                         OSPFV3_CONFIGURATION_TRC, u4ContextId,
                         "Backbone area creation failed for cxt : %d\n",
                         u4ContextId);
            V3RtrDeleteCxt (pV3OspfCxt);
            pV3OspfCxt = NULL;
            return OSIX_FAILURE;
        }
    }

    /* Create the host RB Tree */
    pV3OspfCxt->pHostRBRoot =
        RBTreeCreateEmbedded
        ((OSPFV3_OFFSET (tV3OsHost, RbNode)), V3CompareHost);

    if (pV3OspfCxt->pHostRBRoot == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Host RB Tree for cxt : %d\n",
                      u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Host RB Tree creation failed for cxt : %d\n",
                     u4ContextId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    /* Create the RB Tree and Hash Table for AS LSA */
    pV3OspfCxt->pExtLsaHashTable =
        TMO_HASH_Create_Table (OSPFV3_EXT_LSA_HASH_TABLE_SIZE, NULL, FALSE);
    if (pV3OspfCxt->pExtLsaHashTable == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Ext LSA Hash Table for cxt : %d\n",
                      u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "Ext LSA Hash Table creation failed for cxt : %d\n",
                     u4ContextId);
        KW_FALSEPOSITIVE_FIX1 (pV3OspfCxt->pHostRBRoot->SemId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    pV3OspfCxt->pAsScopeLsaRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsLsaInfo,
                                              rbNode.RbAsScopeNode)),
                              V3CompareLsa);
    if (pV3OspfCxt->pAsScopeLsaRBRoot == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create AS scope LSA RB Tree for cxt : %d\n",
                      u4ContextId));

        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "AS scope LSA RB Tree creation failed for cxt : %d\n",
                     u4ContextId);
        KW_FALSEPOSITIVE_FIX1 (pV3OspfCxt->pHostRBRoot->SemId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    pV3OspfCxt->IfLeakRouteRBTree =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsLeakRoutes, RbNode)),
                              V3OsUpdIfRouteLeakStatus);

    if (pV3OspfCxt->IfLeakRouteRBTree == NULL)
    {
        OSPFV3_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                     OSPFV3_CONFIGURATION_TRC, u4ContextId,
                     "IfLeakRouteRBTree RB Tree creation failed for cxt : %d\n",
                     u4ContextId);
        KW_FALSEPOSITIVE_FIX1 (pV3OspfCxt->pHostRBRoot->SemId);
        V3RtrDeleteCxt (pV3OspfCxt);
        pV3OspfCxt = NULL;
        return OSIX_FAILURE;
    }

    gV3OsRtr.apV3OspfCxt[u4ContextId] = pV3OspfCxt;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
    if (V3OspfCreateSocket (pV3OspfCxt->u4ContextId) == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to create Socket\n"));

        OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                    pV3OspfCxt->u4ContextId, "Socket creation failure\n");
        return OSIX_FAILURE;
    }
    SelAddFd (gV3OsRtr.ai4RawSockId[pV3OspfCxt->u4ContextId], V3SendEvent2Ospf);
#endif

    /* Restore the context specific graceful restart configurations */
    O3GrCfgRestoreRestartInfo (pV3OspfCxt);

    OSPFV3_TRC1 (OSPFV3_CONFIGURATION_TRC, u4ContextId,
                 "Cxt %d created successfully\n", u4ContextId);

    OSPFV3_TRC1 (OSPFV3_FN_EXIT, u4ContextId,
                 "EXIT : V3RtrCreateCxt for cxt : %d\n", u4ContextId);
    KW_FALSEPOSITIVE_FIX (pV3OspfCxt->pAsScopeLsaRBRoot->SemId);
    KW_FALSEPOSITIVE_FIX1 (pV3OspfCxt->pHostRBRoot->SemId);

    for (u1ProtoIndex = 0; u1ProtoIndex < OSPFV3_MAX_PROTO_REDISTRUTE_SIZE;
         u1ProtoIndex++)
    {
        pV3OspfCxt->au4MetricValue[u1ProtoIndex] = OSPFV3_AS_EXT_DEF_METRIC;
        pV3OspfCxt->au4MetricType[u1ProtoIndex] = OSPFV3_TYPE_2_METRIC;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrDeleteCxt                                             */
/*                                                                           */
/* Description  : Deletes an instance in OSPFv3                              */
/*                Called during                                              */
/*                1. Task de-initialization                                  */
/*                2. Context deletion by administrator                       */
/*                3. Context deletion in VCM module                          */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3RtrDeleteCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrDeleteCxt\n");

    u4ContextId = pV3OspfCxt->u4ContextId;

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
                    defined (LNXIP6_WANTED)
    SelRemoveFd (gV3OsRtr.ai4RawSockId[pV3OspfCxt->u4ContextId]);
    /* Socket has to be closed */
    V3OspfCloseSocket (pV3OspfCxt->u4ContextId);
#endif

    /* Disable the context */
    if (pV3OspfCxt->admnStat == OSPFV3_ENABLED)
    {
        V3RtrDisableInCxt (pV3OspfCxt, OSPFV3_FALSE);
    }

    /* Delete the database */
    V3IfDeleteAllInterfaceInCxt (pV3OspfCxt);
    V3VIfDeleteAllInterfaceInCxt (pV3OspfCxt);
    V3AreaDeleteAllAreasInCxt (pV3OspfCxt);
    V3AreaDeleteAllExtAggInCxt (pV3OspfCxt);

    if (pV3OspfCxt->ospfV3RtTable.pOspfV3Root != NULL)
    {
        /* Delete the routing table */
        V3OspfDeleteRtTable (pV3OspfCxt->ospfV3RtTable.pOspfV3Root);
    }

    if (pV3OspfCxt->pExtRtRoot != NULL)
    {
        /* Delete the external root table */
        V3OspfDeleteRtTable (pV3OspfCxt->pExtRtRoot);
    }

    if (pV3OspfCxt->pCandteLst != NULL)
    {
        /* Delete the candidate hash table */
        TMO_HASH_Delete_Table (pV3OspfCxt->pCandteLst, NULL);
    }

    if (pV3OspfCxt->pBackbone != NULL)
    {
        /* Delete the backbone area */
        V3AreaDelete (pV3OspfCxt->pBackbone);
    }

    if (pV3OspfCxt->pHostRBRoot != NULL)
    {
        /* Delete the host RB Tree */
        V3HostDeleteAllHostsInCxt (pV3OspfCxt);
        RBTreeDelete (pV3OspfCxt->pHostRBRoot);
    }

    if (pV3OspfCxt->pExtLsaHashTable != NULL)
    {
        /* Delete the external LSA Hash Table */
        TMO_HASH_Delete_Table (pV3OspfCxt->pExtLsaHashTable, NULL);
    }

    if (pV3OspfCxt->pAsScopeLsaRBRoot != NULL)
    {
        /* Delete the AS scope LSA RB Tree */
        RBTreeDelete (pV3OspfCxt->pAsScopeLsaRBRoot);
    }

    if (pV3OspfCxt->IfLeakRouteRBTree != NULL)
    {
        /* Delete the IfLeakRoute RB Tree */
        RBTreeDelete (pV3OspfCxt->IfLeakRouteRBTree);
    }

    /* De-allocate the memory */
    if (pV3OspfCxt->pDistributeInFilterRMap != NULL)
    {
        OSPFV3_RMAP_MSG_FREE (pV3OspfCxt->pDistributeInFilterRMap);
        pV3OspfCxt->pDistributeInFilterRMap = NULL;
    }

    if (pV3OspfCxt->pDistanceFilterRMap != NULL)
    {
        OSPFV3_RMAP_MSG_FREE (pV3OspfCxt->pDistanceFilterRMap);
        pV3OspfCxt->pDistanceFilterRMap = NULL;
    }

    MEMSET (pV3OspfCxt, 0, sizeof (tV3OspfCxt));
    OSPFV3_CXT_FREE (pV3OspfCxt);
    gV3OsRtr.apV3OspfCxt[u4ContextId] = NULL;

    OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, u4ContextId,
                "Cxt deleted successfully\n");

    OSPFV3_TRC1 (OSPFV3_FN_EXIT, u4ContextId,
                 "EXIT : V3RtrDeleteCxt : %d\n", u4ContextId);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrEnableInCxt                                           */
/*                                                                           */
/* Description  : Enables the OSPFV3 protocol.                               */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3RtrEnableInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1ClearFlag)
{
    tV3OsInterface      curInterface;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4Mask = 0;
    UINT4               u450msTicks = 50;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrEnableInCxt\n");

#ifdef RRD_WANTED
    tRtm6RegnId         rtm6RegnId;
    MEMSET (&rtm6RegnId, OSPFV3_INIT_VAL, sizeof (tRtm6RegnId));
#endif

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                pV3OspfCxt->u4ContextId, "OSPFV3 to be enabled\n");

    MEMSET (&curInterface, 0, sizeof (tV3OsInterface));
#ifdef RRD_WANTED
    if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
    {
        rtm6RegnId.u4ContextId = pV3OspfCxt->u4ContextId;
        rtm6RegnId.u2ProtoId = OSPF6_ID;

        /* Since RTM6 does not support MI, registration to RTM6 is done
         * only once based on the reference count. The count is incremented
         * every time a context is enabled. When count is one registration
         * to RTM6 is done. This check should be removed once MI support is given
         * for RTM6
         */
        /*In case of clear ospf process no need to register with RTM
           as deregistration is not done */
        if (u1ClearFlag == OSPFV3_FALSE)
        {
            if (Rtm6Register (&rtm6RegnId, RTM6_ACK_REQUIRED,
                              V3RtmSendToOspf) != RTM6_SUCCESS)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to register with RTM6\n"));

                OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Failed to register with RTM6\n");
                return OSIX_FAILURE;
            }
        }

        if ((pV3OspfCxt->redistrAdmnStatus == OSPFV3_ENABLED) &&
            (pV3OspfCxt->u4RrdSrcProtoBitMask != OSPFV3_ZERO))
        {
            if (V3RtmTxRedistributeMsgInCxt
                (pV3OspfCxt, pV3OspfCxt->u4RrdSrcProtoBitMask,
                 RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                 pV3OspfCxt->au1RMapName) != OSIX_SUCCESS)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to register protocol bit mask with RTM6\n"));

                OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Failed to register protocol bit mask with RTM6\n");
                return OSIX_FAILURE;
            }
        }
    }

#endif /* RRD_WANTED */

    pV3OspfCxt->u4AsScopeLsaChksumSum = 0;
    pV3OspfCxt->u4OriginateNewLsa = 0;
    pV3OspfCxt->u4RcvNewLsa = 0;
    pV3OspfCxt->u4AsExtLsaCount = 0;
    pV3OspfCxt->u4AsScopeLsaCount = 0;
    pV3OspfCxt->u1AsbSummaryLsaChanged = OSIX_FALSE;
    pV3OspfCxt->bDefaultAseLsaPresenc = OSIX_FALSE;
    pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
    pV3OspfCxt->u1RtrDisableInProgress = OSIX_FALSE;
    pV3OspfCxt->u4AsExtLsaCounter = 0;
    pV3OspfCxt->u4InterAreaLsaCounter = 0;
    pV3OspfCxt->u4ActiveAreaCount = 0;
    pV3OspfCxt->u1VirtRtCal = OSIX_FALSE;

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                pV3OspfCxt->u4ContextId, "Enable All Interfaces\n");

    gV3OsRtr.u4ActiveCxtCount++;
    V3TmrSetTimer (&(gV3OsRtr.g50msTimerNode), OSPFV3_50MS_LSU_LAK_TIMER,
                   u450msTicks);
#ifdef RAWSOCK_WANTED
    /* Raw socket identifier is same for all the contexts present
     * in the router. The socket is opened when the first context is
     * enabled and closed when the last context is disabled based on
     * the current number of active context count
     */
#if !(defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED))
    if (gV3OsRtr.u4ActiveCxtCount == 1)
    {
        if (V3OspfCreateSocket (pV3OspfCxt->u4ContextId) == OSIX_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to create Socket\n"));

            OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                        pV3OspfCxt->u4ContextId, "Socket creation failure\n");
            return OSIX_FAILURE;
        }

        SelAddFd (gV3OsRtr.i4RawSockId, V3SendEvent2Ospf);
    }
#endif
#endif

    u4Mask =
        NETIPV6_APPLICATION_RECEIVE | NETIPV6_ADDRESS_CHANGE |
        NETIPV6_INTERFACE_PARAMETER_CHANGE;

#ifdef IP6_WANTED
    /* Since IP6 does not support MI, registration to IP6 is done
     * only once based on the reference count. The count is incremented
     * every time a context is enabled. When count is one registration
     * to IP6 is done. This check should be removed once MI support is given
     * for IP6
     */
    /* In case of clear ospf process no need to register with IP6
       as registration is already present */
    if (u1ClearFlag == OSPFV3_FALSE)
    {
        NetIpv6RegisterHigherLayerProtocolInCxt
            (pV3OspfCxt->u4ContextId,
             OSPFV3_PROTO, u4Mask, (void *) V3Ipv6OspfInterface);
    }
#endif /* for IP6_WANTED */

    pV3OspfCxt->admnStat = OSPFV3_ENABLED;

    while ((pInterface = RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                        (tRBElem *) & curInterface,
                                        NULL)) != NULL)
    {
        MEMCPY (&curInterface, pInterface, sizeof (tV3OsInterface));
        if (pInterface->pArea->pV3OspfCxt->u4ContextId ==
            pV3OspfCxt->u4ContextId)
        {
            V3IfUp (pInterface);
        }
    }

    /* Calculate all the host routes */
    V3RtcCalculateAllLocalHostRouteInCxt (pV3OspfCxt);

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                pV3OspfCxt->u4ContextId, "OSPF Enabled\n");

#ifdef L3_SWITCHING_WANTED
    /* Do the hardware initialisation to receive OSPFv3 packets */
    /* In case of clear ospf process no need to register with NP as 
       it is already registered */
    if (u1ClearFlag == OSPFV3_FALSE)
    {
        if (OSPF3_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
        {
            Ospf3FsNpOspf3Init ();
        }
    }
#endif /* L3_SWITCHING_WANTED */

#ifndef L3_SWITCHING_WANTED
#ifndef RRD_WANTED
#ifndef IP6_WANTED

    UNUSED_PARAM (u1ClearFlag);

#endif
#endif
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrEnableInCxt\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDemandRBTAction                                        */
/*                                                                           */
/* Description  : Called from low level routine while changing the DC        */
/*                                                                           */
/* Input        : pNode: Pointer to RB Tree Element                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Retun the Number of Intrefaces                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3IfDemandRBTAction (tRBElem * pNode, eRBVisit visit, UINT4 u4Level,
                     VOID *arg, VOID *out)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3IfDemandRBTAction\n");

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (out);

    pV3OspfCxt = (tV3OspfCxt *) arg;
    if ((visit == leaf) || (visit == postorder))
    {
        if (pNode != NULL)
        {
            pInterface = (tV3OsInterface *) pNode;

            if (pInterface->pArea->pV3OspfCxt->u4ContextId ==
                pV3OspfCxt->u4ContextId)
            {
                if (pInterface->bDcEndpt == OSIX_TRUE)
                {
                    if (pInterface->bDemandNbrProbe == OSIX_TRUE)
                    {
                        V3TmrDeleteTimer (&(pInterface->nbrProbeTimer));
                        pInterface->bDemandNbrProbe = OSIX_FALSE;
                    }
                    V3IfSetDemand (pInterface, OSIX_FALSE);
                }
            }
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3IfDemandRBTAction\n");
    return (RB_WALK_CONT);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrDisableInCxt                                          */
/*                                                                           */
/* Description  : Disables the OSPFV3 protocol                               */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                 u1ClearFlag   - Flag to indicate function is called from  */
/*                 Clear ospf process flow                                   */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3RtrDisableInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1ClearFlag)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrDisableInCxt\n");

    tV3OsInterface      curInterface;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsFwdAddr       *pScanFwdAddrNode = NULL;
    tV3OsFwdAddr       *pTempFwdAddrNode = NULL;
    tV3OsInterface     *pVirtInterface = NULL;
    tV3OsInterface     *pOspfInterface = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;

#ifdef RRD_WANTED
    tRtm6RegnId         rtm6RegnId;
#endif
    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                pV3OspfCxt->u4ContextId, "OSPFV3 To Be Disabled\n");

    MEMSET (&curInterface, 0, sizeof (tV3OsInterface));
#ifdef L3_SWITCHING_WANTED
/* In case of clear ospf process no need to deregister with NP*/
    if (u1ClearFlag != OSPFV3_TRUE)
    {
        /* Do the hardware initialisation to receive OSPF packets */
        if (OSPF3_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE)
        {
            Ospf3FsNpOspf3DeInit ();
        }
    }
#endif /* L3_SWITCHING_WANTED */

    if ((pV3OspfCxt->contextStatus != DESTROY)
        && (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* This router is already under going GR shutdown
         * process so no need to disable */
        return OSIX_SUCCESS;
    }

    pV3OspfCxt->u1RtrDisableInProgress = OSIX_TRUE;

    if (u1ClearFlag == OSPFV3_TRUE)
    {
        Rtm6ApiHandleProtocolStatusInCxt (pV3OspfCxt->u4ContextId, OSPF6_ID,
                                          RTM6_PROTOCOL_SHUTDOWN, 0, 0, NULL,
                                          0);

    }

    V3RtcClearAndDeleteRtFromIpInCxt (pV3OspfCxt);

    V3LsuFlushAllSelfOrgLsasInCxt (pV3OspfCxt);

/* Before clearing the process ensure that all packets in queue are
 * cleared */
    pOspfInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    if (pOspfInterface != NULL)
    {
        do
        {
            if (pOspfInterface->delLsAck.u2CurrentLen > 0)
            {

                V3LakSendDelayedAck (pOspfInterface);
            }
            if (pOspfInterface->lsUpdate.u2CurrentLen > 0)
            {
                V3LsuSendLsu (NULL, pOspfInterface);
            }
        }
        while ((pOspfInterface = (tV3OsInterface *)
                RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                               (tRBElem *) pOspfInterface, NULL)) != NULL);

    }

    V3TmrDeleteTimer (&(gV3OsRtr.g50msTimerNode));
    V3ExtrtDeleteAllInCxt (pV3OspfCxt);

#ifdef RRD_WANTED
    /* Disabling Redistribution. It will be enabled in V3RtrEnableInCxt */
    V3RtmRRDSrcProtoDisableInCxt (pV3OspfCxt, pV3OspfCxt->u4RrdSrcProtoBitMask);
#endif
    OSPFV3_DYNM_SLL_SCAN (&(pV3OspfCxt->fwdAddrLst),
                          pScanFwdAddrNode, pTempFwdAddrNode, tV3OsFwdAddr *)
    {
        TMO_SLL_Delete (&(pV3OspfCxt->fwdAddrLst),
                        &(pScanFwdAddrNode->nextFwdAddr));
        OSPFV3_FWD_ADDR_FREE (pScanFwdAddrNode);
    }
    pV3OspfCxt->admnStat = OSPFV3_DISABLED;

    while ((pInterface = RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                        (tRBElem *) & curInterface,
                                        NULL)) != NULL)
    {
        MEMCPY (&curInterface, pInterface, sizeof (tV3OsInterface));
        if (pInterface->pArea->pV3OspfCxt->u4ContextId ==
            pV3OspfCxt->u4ContextId)
        {
            V3IfDisable (pInterface);
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pVirtInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
        V3IfDisable (pVirtInterface);
    }

    V3LsuDeleteAllLsasInCxt (pV3OspfCxt);

    gV3OsRtr.u4ActiveCxtCount--;
#ifdef RAWSOCK_WANTED
#if !(defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED))
    if (gV3OsRtr.u4ActiveCxtCount == 0)
    {
        SelRemoveFd (gV3OsRtr.i4RawSockId);
        /* Socket has to be closed */
        V3OspfCloseSocket (pV3OspfCxt->u4ContextId);

    }
#endif
#endif

#ifdef IP6_WANTED
/* In case of clear ospf process no need to deregister with IP*/
    if (u1ClearFlag != OSPFV3_TRUE)
    {
        NetIpv6DeRegisterHigherLayerProtocolInCxt (pV3OspfCxt->u4ContextId,
                                                   OSPFV3_PROTO);
    }
#endif

    V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));

    V3TmrDeleteTimer (&(pV3OspfCxt->exitOverflowTimer));

    pV3OspfCxt->u4AsScopeLsaChksumSum = 0;
    pV3OspfCxt->u4OriginateNewLsa = 0;
    pV3OspfCxt->u4RcvNewLsa = 0;
    pV3OspfCxt->u4AsExtLsaCount = 0;
    pV3OspfCxt->u4AsScopeLsaCount = 0;
    pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
    pV3OspfCxt->u1AsbSummaryLsaChanged = OSIX_FALSE;
    pV3OspfCxt->bOverflowState = OSIX_FALSE;
    pV3OspfCxt->u1VirtRtCal = OSIX_FALSE;
    pV3OspfCxt->u1RouteLeakStatus = OSIX_FALSE;
    pV3OspfCxt->u4IfDownIndex = OSPFV3_ZERO;
    pV3OspfCxt->u1IfRouteLeakStatus = OSIX_FALSE;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        V3RtcClearSpfTree (pArea->pSpf);
        pArea->u4AreaBdrRtrCount = 0;
        pArea->u4AsBdrRtrCount = 0;

        /* Scan the aggregation list and reset the aggregation flag */
        TMO_SLL_Scan (&(pArea->type3AggrLst), pNode, tTMO_SLL_NODE *)
        {
            pInternalRng = OSPFV3_GET_BASE_PTR (tV3OsAddrRange,
                                                nextAddrRange, pNode);

            pInternalRng->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
            pInternalRng->u4RtCount = 0;
        }

        /* Scan the NSSA aggregation list */
        TMO_SLL_Scan (&(pArea->type7AddrRangeLst), pNode, tTMO_SLL_NODE *)
        {
            pAsExtAddrRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                                 nextAsExtAddrRange, pNode);

            (pAsExtAddrRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
            (pAsExtAddrRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
        }
    }

    /* Reset the AS external aggregation */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst), pNode, tTMO_SLL_NODE *)
    {
        pAsExtAddrRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr, pNode);

        (pAsExtAddrRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
        (pAsExtAddrRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
    }

    /* If the context is present in the global VRF DLL list, delete
     * the context node from the DLL. If the DLL count is 0 i.e. the
     * context is the only node in the DLL, delete the timer
     */
    if (pV3OspfCxt->bVrfSpfTmrStatus == OSPFV3_TRUE)
    {
        pV3OspfCxt->bVrfSpfTmrStatus = OSPFV3_FALSE;
        TMO_DLL_Delete (&(gV3OsRtr.vrfSpfList), &(pV3OspfCxt->nextVrfSpfNode));
        if (TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) == 0)
        {
            V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
        }
    }

#ifdef RRD_WANTED
    rtm6RegnId.u4ContextId = pV3OspfCxt->u4ContextId;
    rtm6RegnId.u2ProtoId = OSPF6_ID;

/* In case of clear ospf process no need to deregister with RTM
   routes alone to be deleted from RTM*/
    if (u1ClearFlag != OSPFV3_TRUE)
    {
        Rtm6Deregister (&rtm6RegnId);
    }

#endif /* RRD_WANTED */

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_RTMODULE_TRC,
                pV3OspfCxt->u4ContextId, "OSPFV3 Disabled\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrDisableInCxt\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetRouterIdInCxt                                      */
/*                                                                           */
/* Description  : Disables the OSPFV3 protocol. Updates the RouterId and then*/
/*                Enables the OSPFV3 protocol.                               */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                rtrId  : RouterID to be updated                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSPV3_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3RtrSetRouterIdInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRouterId rtrId)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetRouterIdInCxt\n");

    if (!(OSPFV3_IS_NULL_RTR_ID (pV3OspfCxt->rtrId)) &&
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* Routerid is changed during graceful restart mode.
         * so exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_TOP_CHG);
        return OSIX_SUCCESS;
    }

    if ((OSPFV3_IS_NULL_RTR_ID (pV3OspfCxt->rtrId)) &&
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        OSPFV3_RTR_ID_COPY (pV3OspfCxt->rtrId, rtrId);
        return OSIX_SUCCESS;
    }

    V3RtrDisableInCxt (pV3OspfCxt, OSPFV3_FALSE);

    OSPFV3_RTR_ID_COPY (pV3OspfCxt->rtrId, rtrId);

    if (V3RtrEnableInCxt (pV3OspfCxt, OSPFV3_FALSE) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetRouterIdInCxt\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetExitOverflowIntervalInCxt                          */
/*                                                                           */
/* Description  : Set Exit overflow interval                                 */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3RtrSetExitOverflowIntervalInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetExitOverflowIntervalInCxt\n");

    if (pV3OspfCxt->bOverflowState == OSIX_TRUE)
    {
        if (OSPFV3_IS_RTR_CFGD_TO_LEAVE_OVFL_STATE (pV3OspfCxt))
        {
            V3TmrSetTimer (&(pV3OspfCxt->exitOverflowTimer),
                           OSPFV3_EXIT_OVERFLOW_TIMER,
                           (INT4) V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                                (pV3OspfCxt->
                                                 u4ExitOverflowInterval),
                                                OSPFV3_JITTER));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetExitOverflowIntervalInCxt\n");
}

/************************************************************************/
/* Function    : V3RtrHandleABRStatChngInCxt                            */
/*                                                                      */
/* Description : Handles Change in ABR status.                          */
/*                                                                      */
/* Input       : pV3OspfCxt - Context pointer                           */
/*               u1AbrFlag  - ABR statis flag                           */
/*                                                                      */
/* Output      : None                                                   */
/*                                                                      */
/* Returns     : VOID                                                   */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RtrHandleABRStatChngInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pTransArea = NULL;
    UINT1               u1AbrFlag = 0;
    tV3OsLsaInfo       *pDefLsaInfo = NULL;
    tV3OsInterface     *pVirtInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrHandleABRStatChngInCxt\n");
    u1AbrFlag = (UINT1) V3RtrCheckABRStatInCxt (pV3OspfCxt);

    /* There is no change in the ABR state of the Router */
    if (pV3OspfCxt->bAreaBdrRtr == u1AbrFlag)
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtrHandleABRStatChngInCxt\n");
        return;
    }

    pV3OspfCxt->bAreaBdrRtr = u1AbrFlag;

    if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            /* Generating Router LSA into attached areas
             * to indicate ABR Status change */
            if (pArea->u4AreaType == OSPFV3_STUB_AREA)
            {
                if (u1AbrFlag == OSIX_FALSE)
                {
                    pDefLsaInfo = V3LsuSearchDatabase
                        (OSPFV3_INTER_AREA_PREFIX_LSA,
                         &(OSPFV3_NULL_LSID), &(pV3OspfCxt->rtrId), NULL,
                         pArea);

                    if (pDefLsaInfo != NULL)
                    {
                        V3AgdFlushOut (pDefLsaInfo);
                    }
                }
                else
                {
                    V3SignalLsaRegenInCxt (pV3OspfCxt,
                                           OSPFV3_SIG_DEFAULT_SUMMARY,
                                           (UINT1 *) pArea);
                }
            }
            if (pArea->u4ActIntCount != 0)
            {
                V3GenLowestLsIdRtrLsa (pArea);
            }
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pVirtInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

        if ((pVirtInterface->u1IsmState == OSPFV3_IFS_DOWN) &&
            (pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE))
        {
            pTransArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                             &pVirtInterface->transitAreaId);
            if (pTransArea != NULL)
            {
                V3VifActivate (pVirtInterface, pTransArea);
            }
        }
        else if ((pVirtInterface->u1IsmState != OSPFV3_IFS_DOWN) &&
                 (pV3OspfCxt->bAreaBdrRtr == OSIX_FALSE))
        {
            OSPFV3_GENERATE_IF_EVENT (pVirtInterface, OSPFV3_IFE_DOWN);
        }
    }

    if (pV3OspfCxt->u4NssaAreaCount > OSPFV3_RAG_NO_CHNG)
    {
        if (u1AbrFlag == OSIX_TRUE)
        {
            /* Rtr has become ABR, so Trigger NSSA
             * Translator Election
             */
            V3AreaNssaFsmTrgrTrnsltrElnInCxt (pV3OspfCxt);
        }
        else
        {
            /* Rtr has lost ABR Status, invoke */
            V3AreaNssaABRStatLostInCxt (pV3OspfCxt);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrHandleABRStatChngInCxt\n");

}

/************************************************************************/
/* Function    : V3RtrSetABRTypeInCxt                                   */
/*                                                                      */
/* Description : This function will set the ABR Type and takes          */
/*               necessary action.                                      */
/*                                                                      */
/* Input       : pV3OspfCxt   -  Context pointer                        */
/*               u4ABRType    -  Type of the ABR to be set.             */
/*                                                                      */
/* Output      : None                                                   */
/*                                                                      */
/* Returns     : VOID                                                   */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RtrSetABRTypeInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4ABRType)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrSetABRTypeInCxt\n");

    pV3OspfCxt->u1ABRType = (UINT1) u4ABRType;

    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtrSetABRTypeInCxt\n");

        return;
    }

    V3RtrHandleABRStatChngInCxt (pV3OspfCxt);

    /* Calculate the Entire Routing Table, because 
     * inter area route calculation will differ for different 
     * types of ABR's */
    if (pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
    {
        V3RtcSetRtTimerInCxt (pV3OspfCxt);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtrSetABRTypeInCxt\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrCheckABRStatInCxt                                     */
/*                                                                           */
/* Description  : Reference : RFC 2328 and RFC 3509                          */
/*                This function checks whether the router is ABR or not.     */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if ABR                                          */
/*                OSIX_FALSE, if not ABR                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3RtrCheckABRStatInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtrCheckABRStatInCxt\n");

    switch (pV3OspfCxt->u1ABRType)
    {
        case OSPFV3_STANDARD_ABR:
            /* Ref: RFC 2328 -- Section 3.3 */
            if (pV3OspfCxt->u4ActiveAreaCount > 1)
            {
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : V3RtrCheckABRStatInCxt\n");

                return OSIX_TRUE;
            }
            break;

        case OSPFV3_CISCO_ABR:
            /* Ref: RFC 3509 -- Section 2.1 */
            if ((pV3OspfCxt->u4ActiveAreaCount > 1) &&
                (OSPFV3_IS_ACTIVELY_ATTACHED_AREA (pV3OspfCxt->pBackbone)))
            {
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : V3RtrCheckABRStatInCxt\n");
                return OSIX_TRUE;
            }
            break;

        case OSPFV3_IBM_ABR:
            /* Ref: RFC 3509 -- Section 2.1 */
            if ((pV3OspfCxt->u4ActiveAreaCount > 1)
                && (OSPFV3_IS_CONFIGURED_BACKBONE (pV3OspfCxt)))
            {
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : V3RtrCheckABRStatInCxt\n");
                return OSIX_TRUE;
            }
            break;

        default:
            return OSIX_FALSE;
    }
    return OSIX_FALSE;
}

#ifdef RAWSOCK_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfCreateSocket                                         */
/*                                                                           */
/* Description  : Create Socket                                              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*                OSIX_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3OspfCreateSocket (UINT4 u4Ospf3ContextId)
{

    INT4                i4SockFd = -1;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4Ospf3ContextId,
                "ENTER : V3OspfCreateSocket\n");

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
    i4SockFd = LnxVrfGetSocketFd (u4Ospf3ContextId, AF_INET6, SOCK_RAW,
                                  OSPFV3_PROTO, LNX_VRF_OPEN_SOCKET);
#else
    i4SockFd = OSPFV3_SOCKET (AF_INET6, SOCK_RAW, OSPFV3_PROTO);
#endif
    if (i4SockFd < 0)
    {
        return OSIX_FAILURE;
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
        defined (LNXIP6_WANTED)
    gV3OsRtr.ai4RawSockId[u4Ospf3ContextId] = i4SockFd;
#else
    UNUSED_PARAM (u4Ospf3ContextId);
    gV3OsRtr.i4RawSockId = i4SockFd;
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, u4Ospf3ContextId,
                "EXIT : V3OspfCreateSocket\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfCloseSocket                                          */
/*                                                                           */
/* Description  : Close Socket                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfCloseSocket (UINT4 u4Ospf3ContextId)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4Ospf3ContextId,
                "ENTER : V3OspfCloseSocket\n");

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
            defined (LNXIP6_WANTED)
    OSPFV3_CLOSE (gV3OsRtr.ai4RawSockId[u4Ospf3ContextId]);
    gV3OsRtr.ai4RawSockId[u4Ospf3ContextId] = -1;
#else
    UNUSED_PARAM (u4Ospf3ContextId);
    OSPFV3_CLOSE (gV3OsRtr.i4RawSockId);
    gV3OsRtr.i4RawSockId = -1;
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4Ospf3ContextId, "EXIT : V3OspfCloseSocket\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3SendEvent2Ospf                                           */
/*                                                                           */
/* Description  : Callback function, invoked whenever Data packets are       */
/*           available on Raw Socket                                         */
/*                                                                           */
/* Input        : i4RawSockId - Socket Fd                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3SendEvent2Ospf (INT4 i4RawSockId)
{

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    tV3OsVrfSock       *pV3OsVrfSktFd;
    pV3OsVrfSktFd = (tV3OsVrfSock *) MemAllocMemBlk (OSPFV3_VRF_SOCK_FD);
    if (pV3OsVrfSktFd == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for the queue message\n"));

        OSPFV3_TRC (CONTROL_PLANE_TRC, i4RawSockId,
                    "Memory allocation for the queue message failed\n");
        return;
    }
    MEMSET (pV3OsVrfSktFd, 0, sizeof (tV3OsVrfSock));
    pV3OsVrfSktFd->i4SockFd = i4RawSockId;

    if (OsixQueSend
        (OSPF3_LNX_Q_ID,
         (UINT1 *) &pV3OsVrfSktFd, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to enqueue to LINUX OSPF for VRF\n"));

        OSPFV3_TRC (CONTROL_PLANE_TRC, i4RawSockId,
                    "Enqueue To LINUX OSPF for VRF Failed\n");
        return;
    }

#else
    UNUSED_PARAM (i4RawSockId);
#endif
    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_PKT_ARRIVAL_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_PKT_ARRIVAL_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_PKT_ARRIVAL_EVENT"));
    }

}
#endif /* RAWSOCK_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrInitialiseCxt                                         */
/*                                                                           */
/* Description  : Initialises the SLL and DLL in the context                 */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3RtrInitialiseCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER : V3RtrInitialiseCxt for cxt : %d\n",
                 pV3OspfCxt->u4ContextId);

    TMO_DLL_Init (&(pV3OspfCxt->lsaDescLst));
    TMO_SLL_Init (&(pV3OspfCxt->areasLst));
    TMO_SLL_Init (&(pV3OspfCxt->fwdAddrLst));
    TMO_SLL_Init (&(pV3OspfCxt->asExtAddrRangeLst));
    TMO_SLL_Init (&(pV3OspfCxt->virtIfLst));
    TMO_SLL_Init (&(pV3OspfCxt->sortNbrLst));
    TMO_SLL_Init (&(pV3OspfCxt->redistrRouteConfigLst));

    /* Initialise the VRF SPF node */
    TMO_SLL_Init_Node (&(pV3OspfCxt->nextVrfSpfNode));

    OSPFV3_TRC1 (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                 "EXIT : V3RtrInitialiseCxt for cxt :\n",
                 pV3OspfCxt->u4ContextId);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrSetDefaultValuesInCxt                                 */
/*                                                                           */
/* Description  : Sets the default value for the variables in the context    */
/*                                                                           */
/* Input        : pV3OspfCxt  -  Context pointer                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3RtrSetDefaultValuesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4CsrRestoreFlag = OSIX_FALSE;
    INT4                i1FileFd = OSPFV3_INIT_VAL;
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER : V3RtrSetDefaultValuesInCxt "
                 "for cxt : %d\n", pV3OspfCxt->u4ContextId);

    pV3OspfCxt->u4TraceValue = OSPFV3_CRITICAL_TRC;
    pV3OspfCxt->u4ExtTraceValue = OSPFV3_CRITICAL_TRC;
    pV3OspfCxt->u4ExitOverflowInterval = OSPFV3_DONT_LEAVE_OVERFLOW_STATE;
    pV3OspfCxt->u4RefBw = OSPFV3_DEF_REF_BW;
    pV3OspfCxt->u4SpfInterval = OSPFV3_DEF_SPF_INT;
    pV3OspfCxt->u4SpfHoldTimeInterval = OSPFV3_DEF_HOLD_TIME_INT;
    pV3OspfCxt->u4StaggeringDelta = OSPFV3_INVALID_STAGGERING_DELTA;
    pV3OspfCxt->u4RTStaggeringInterval = OSPFV3_DEF_STAG_INTERVAL;
    pV3OspfCxt->u4RouteCalcCompleted = OSPFV3_ROUTE_CALC_COMPLETED;
    pV3OspfCxt->i4ExtLsdbLimit = OSPFV3_NO_LIMIT;
#ifdef RRD_WANTED
    pV3OspfCxt->u4RrdSrcProtoBitMask = 0;
    pV3OspfCxt->redistrAdmnStatus = OSPFV3_DISABLED;
#endif
    pV3OspfCxt->bOverflowState = OSIX_FALSE;
    pV3OspfCxt->bDemandExtension = OSIX_TRUE;
    pV3OspfCxt->bDefaultPassiveInterface = OSIX_FALSE;
    pV3OspfCxt->bAreaBdrRtr = OSIX_FALSE;
    pV3OspfCxt->bAsBdrRtr = OSIX_FALSE;
    pV3OspfCxt->bGenType5Flag = OSIX_FALSE;
    pV3OspfCxt->bNssaAsbrDefRtTrans = OSIX_FALSE;
    pV3OspfCxt->rtrOptions[OSPFV3_OPT_BYTE_THREE] |= OSPFV3_R_BIT_MASK;
    pV3OspfCxt->rtrOptions[OSPFV3_OPT_BYTE_THREE] |= OSPFV3_V6_BIT_MASK;
    /* By Default ABR Type supported is STANDARD_ABR */
    pV3OspfCxt->u1ABRType = OSPFV3_STANDARD_ABR;
    pV3OspfCxt->u4NssaAreaCount = 0;
    pV3OspfCxt->pDistributeInFilterRMap = NULL;
    pV3OspfCxt->pDistanceFilterRMap = NULL;
    pV3OspfCxt->u1Distance = IP6_PREFERENCE_OSPF;

    /*Graceful Restart Default Values */
    pV3OspfCxt->u1RestartSupport = OSPFV3_RESTART_NONE;
    pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_NONE;
    pV3OspfCxt->u4GracePeriod = OSPFV3_GR_DEF_INTERVAL;
    pV3OspfCxt->u1StrictLsaCheck = OSIX_DISABLED;
    pV3OspfCxt->u1HelperSupport = OSPFV3_GRACE_HELPER_ALL;
    pV3OspfCxt->u4HelperGrTimeLimit = OSPFV3_INIT_VAL;
    pV3OspfCxt->u4IsGraceAckReq = OSPFV3_ENABLED;
    pV3OspfCxt->u1GrLsaMaxTxCount = OSPFV3_DEF_GRACE_LSA_SENT;
    pV3OspfCxt->u1RestartReason = OSPFV3_GR_UNKNOWN;
    pV3OspfCxt->u1HelperStatus = OSPFV3_GR_NOT_HELPING;
    pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_NONE;

    /* Bfd Default Values */
    pV3OspfCxt->u1BfdAdminStatus = OSPFV3_BFD_DISABLED;
    pV3OspfCxt->u1BfdStatusInAllIf = OSPFV3_BFD_DISABLED;

    pV3OspfCxt->u1RouterIdStatus = OSPFV3_ROUTERID_DYNAMIC;
    pV3OspfCxt->u1RouteLeakStatus = OSIX_FALSE;
    pV3OspfCxt->u4IfDownIndex = OSPFV3_ZERO;
    pV3OspfCxt->u1IfRouteLeakStatus = OSIX_FALSE;
    if (FlashFileExists (OSPF3_CSR_CONFIG_FILE) == ISS_SUCCESS)
    {
        gu4Os3CsrFlag = OSIX_TRUE;
    }

    i1FileFd = FileOpen (OSPFV3_GR_CONF, OSIX_FILE_RO);

    if (i1FileFd > OSPFV3_ZERO)
    {
        gu4Os3RestoreFlag = OSIX_TRUE;
    }
    u4CsrRestoreFlag = (UINT4) IssGetCsrRestoreFlag ();
    if ((gu4Os3CsrFlag == OSIX_TRUE) && (gu4Os3RestoreFlag != OSIX_TRUE) &&
        (u4CsrRestoreFlag == OSIX_TRUE))
    {
        pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_UNPLANNED;
        pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_RESTART;
        pV3OspfCxt->u1RestartReason = OSPFV3_GR_UNKNOWN;
        pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_INPROGRESS;
    }

    OSPFV3_TRC1 (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                 "EXIT : V3RtrSetDefaultValuesInCxt "
                 "for cxt : %d\n", pV3OspfCxt->u4ContextId);
    if (i1FileFd >= OSPFV3_ZERO)
    {
        FileClose (i1FileFd);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfDeleteRtTable                                        */
/*                                                                           */
/* Description  : Delete Trie instance                                       */
/*                                                                           */
/* Input        : pRoot - Trie root pointer.                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfDeleteRtTable (VOID *pRoot)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfDeleteRtTable\n");

    tInputParams        InParams;
    tOutputParams       OutParams;

    InParams.pRoot = pRoot;
    InParams.i1AppId = OSPFV3_RT_ID;
    InParams.pLeafNode = NULL;
    InParams.Key.pKey = NULL;

    if (TrieDel (&InParams, V3RtcTrieDelete, (VOID *) &OutParams)
        == TRIE_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Trie Delete Failed\r\n"));

        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Trie Delete Failed\r\n");
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfDeleteRtTable\n");

}

/*****************************************************************************/
/* Function     : V3UtilSelectOspfRouterId                                   */
/* Description  : This procedure selecting router-id of smallest Ip          */
/*                of interface address.                                      */
/* Input        : None                                                       */
/* Output       : pAddr        : IP address                                  */
/* Returns      : success/failure                                            */
/*****************************************************************************/
PUBLIC INT4
V3UtilSelectOspfRouterId (UINT4 u4OspfCxtId, tV3OsRouterId * prtrId)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4LoopbackAddr = 0xffffffff;
    UINT4               u4Addr = 0xffffffff;
    UINT4               u4IfAddr = 0;
    UINT1               u1LoopbackFlag = FALSE;
    tNetIpv4IfInfo      NetIpIfInfo;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4OspfCxtId,
                "FUNC:V3UtilSelectOspfRouterId RouterId selected \n");

    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetFirstIfInfoInCxt (u4OspfCxtId,
                                    &NetIpIfInfo) == NETIPV4_SUCCESS)
    {
        do
        {
            u4IfIndex = NetIpIfInfo.u4IfIndex;
            u4IfAddr = NetIpIfInfo.u4Addr;
            if ((NetIpIfInfo.u4ContextId == u4OspfCxtId)
                && (NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE))
            {
                if ((NetIpIfInfo.u4IfType == CFA_LOOPBACK)
                    && (u4IfAddr < u4LoopbackAddr) && (u4IfAddr != 0))
                {
                    /* highest priority is lowest loopback
                     * address should be select as ospf router-id */
                    u4LoopbackAddr = u4IfAddr;
                    u1LoopbackFlag = TRUE;
                }
                else
                {
#ifdef ICCH_WANTED
                    /* The Smallest IP Addres will choosen as Router Id.
                     * Incase ICCH Is enabled on the system ICCH IP Address
                     * shouldnt be choosen as RouterId  */

                    if (u4IfAddr != IcchApiGetIcclIpAddr (0))
#endif
                    {
                        if ((u1LoopbackFlag == FALSE)
                            && (u4IfAddr < u4Addr) && (u4IfAddr != 0))
                        {
                            u4Addr = u4IfAddr;
                        }
                    }
                }
            }

            MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
        }
        while (NetIpv4GetNextIfInfoInCxt (u4OspfCxtId,
                                          u4IfIndex,
                                          &NetIpIfInfo) == NETIPV4_SUCCESS);
        if (u1LoopbackFlag == TRUE)
        {
            OSPFV3_BUFFER_DWTOPDU ((UINT1 *) prtrId, u4LoopbackAddr);

        }
        else
        {
            if (u4Addr == 0xffffffff)
            {
                u4Addr = 0;
            }

            OSPFV3_BUFFER_DWTOPDU ((UINT1 *) prtrId, u4Addr);
        }
        OSPFV3_TRC1 (OSPFV3_FN_EXIT, u4OspfCxtId,
                     "FUNC:V3UtilSelectOspfRouterId - RouterId selected :%x \n",
                     u4IfAddr);
        return OSPFV3_SUCCESS;
    }
    return OSPFV3_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RtrProtocolDisableTmrs                                   */
/*                                                                           */
/* Description  : Disables ospfv3 timers in the given context                */
/*                                                                           */
/* Input        : pOspfCxt   -pointer to the ospf context                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3RtrProtocolDisableTmrs (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;

/* In case of GO_STANDBY event below timers are stopped, as they should
** be running only in ACTIVE NODE
**
**            OSPFV3_HELLO_TIMER
**            OSPFV3_POLL_TIMER
**            OSPFV3_WAIT_TIMER
**            OSPFV3_NBR_PROBE_INTERVAL_TIMER
**            OSPFV3_INACTIVITY_TIMER
**            OSPFV3_DD_INIT_RXMT_TIMER
**            OSPFV3_DD_RXMT_TIMER
**            OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER
**            OSPFV3_LSA_RXMT_TIMER
**            OSPFV3_RESTART_GRACE_TIMER
**            OSPFV3_DEL_ACK_TIMER
**            OSPFV3_LSA_REQ_RXMT_TIMER
** In Case of GO_ACTIVE respect timers will be started in the below flow
**            OSPFV3_POLL_TIMER and OSPFV3_HELLO_TIMER  will be started once hello packet is rcvd
**            OSPFV3_WAIT_TIMER will be started in O3RedRmProcessStandbyToActive
**            OSPFV3_INACTIVITY_TIMER and OSPFV3_LSA_RXMT_TIMER  will be 
**            started in O3RedRmProcessStandbyToActive function when O3RedRmCheckNbrs is called
**            OSPFV3_DD_INIT_RXMT_TIMER will be started started in 
**            O3RedRmProcessStandbyToActive->O3RedRmCheckNbrs->V3NsmRestartAdj->V3DdpSendDdp
**            If session is in extract state when force switchover is triggered then session 
**            will be reformed,  OSPFV3_DD_RXMT_TIMER and OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER 
**            will be started at session formation time
**            OSPFV3_DEL_ACK_TIMER , OSPFV3_LSA_REQ_RXMT_TIMER  and OSPFV3_LSA_RXMT_TIMER will be started
**            while processing New LS update
**
*/
    if (pV3OspfCxt != NULL)
    {

        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        while (pInterface != NULL)
        {
            V3IsmDisableIfTimers (pInterface);
            V3LakClearDelayedAck (pInterface);
            pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                           (tRBElem *)
                                                           pInterface, NULL);
        }

        TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
            V3IsmDisableIfTimers (pInterface);
            V3LakClearDelayedAck (pInterface);
        }

        /* stop the inactivity timer for all neighbors */
        TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

            if ((pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR) &&
                (pNbr->u1NsmState == OSPFV3_NBRS_DOWN))
            {
                continue;
            }
            V3DdpClearSummary (pNbr);
            V3LsuClearRxmtLst (pNbr);
            V3LrqClearLsaReqLst (pNbr);
            V3TmrDeleteTimer (&(pNbr->inactivityTimer));
        }
        /* Stop the grace timer */
        V3TmrDeleteTimer (&(pV3OspfCxt->graceTimer));

    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3rtr.c                         */
/*-----------------------------------------------------------------------*/
