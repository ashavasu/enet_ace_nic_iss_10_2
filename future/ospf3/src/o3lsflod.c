
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lsflod.c,v 1.16 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures for flooding 
 *             link state update packets 
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE UINT1       V3LsuFloodASScopeLsa
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
        UINT1 u1LsaChngdFlag, tV3OsLsHeader * pLsHeader));

PRIVATE UINT1       V3LsuFloodAreaScopeLsa
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr, UINT1 u1LsaChngdFlag,
        tV3OsArea * pArea, tV3OsLsHeader * pLsHeader));

PRIVATE UINT1       V3LsuFloodLinkScopeLsa
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr, UINT1 u1LsaChngdFlag,
        tV3OsInterface * pInterface, tV3OsLsHeader * pLsHeader));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddToRxmtLst                                          */
/*                                                                           */
/* Description  : The specified LSA is added to the link state retransmission*/
/*                list of this neighbor.                                     */
/*                                                                           */
/* Input        : pNbr            : Neighbour to whom the LSA is to be       */
/*                                  added.                                   */
/*                pLsaInfo        : the LSA to be added                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuAddToRxmtLst (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo)
{

    tV3OsRxmtNode      *pRxmtNode = NULL;
    UINT4               u4TmrInterval = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddToRxmtLst\n");

    OSPFV3_TRC2 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LSA to be added to rxmt list of Nbr Rtr Id %x"
                 "Nbr Interface Id %d \n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    if (pLsaInfo->u4RxmtCount != 0)
    {
        pRxmtNode = V3LsuSearchLsaInRxmtLst (pNbr, pLsaInfo);
    }

    if (pRxmtNode == NULL)
    {
        OSPFV3_RXMT_NODE_ALLOC (&(pRxmtNode));
        if (NULL == pRxmtNode)
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory to Retarnsmission List node\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "!!! RXMT NODE ALLOC FAILURE !!! \n");
            return;
        }

        pRxmtNode->pLsaInfo = pLsaInfo;
        /* If the Grace LSA is to be added, add it 
         * at the beginning of the list */
        if (pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA)
        {
            TMO_SLL_Insert (&(pNbr->lsaRxmtLst), NULL,
                            &(pRxmtNode->nextLsaInfo));
        }
        else
        {
            TMO_SLL_Add (&(pNbr->lsaRxmtLst), &(pRxmtNode->nextLsaInfo));
        }
        pLsaInfo->u4RxmtCount++;
        pNbr->lsaRxmtDesc.u4LsaRxmtCount++;

        /* The Rxmt timer of neighbohr is being 
           started only if LSA is added on to Rxmt list */

        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 1)
        {
            u4TmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
                (pNbr->pInterface->u2RxmtInterval);
            V3TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer),
                           OSPFV3_LSA_RXMT_TIMER, u4TmrInterval);
        }

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSA Added To Rxmt Lst \n");
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddToRxmtLst\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSearchLsaInRxmtLst                                    */
/*                                                                           */
/* Description  : Check whether this particular LSA is present or not in the */
/*                retransmission list of this neighbor.                      */
/*                                                                           */
/* Input        : pNbr            : neighbour in which the LSA is to be      */
/*                                  checked.                                 */
/*                pLsaInfo        : the LSA to be checked                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pRxmtNode    : Pointer to the Rxmt Node if found           */
/*                NULL         : otherwise                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsRxmtNode *
V3LsuSearchLsaInRxmtLst (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo)
{

    tV3OsRxmtNode      *pRxmtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSearchLsaInRxmtLst\n");

    TMO_SLL_Scan (&(pNbr->lsaRxmtLst), pRxmtNode, tV3OsRxmtNode *)
    {
        if (pRxmtNode->pLsaInfo == pLsaInfo)
        {
            return pRxmtNode;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSearchLsaInRxmtLst\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteFromRxmtLst                                     */
/*                                                                           */
/* Description  : The specified LSA is removed from the neighbor's           */
/*                retransmission list.                                       */
/*                                                                           */
/* Input        : pNbr            : Neighbour from which the LSA is to be    */
/*                                  deleted.                                 */
/*                pLsaInfo        : the LSA to be deleted                    */
/*                u1DelFlag       : If flag is set delete the MAX AGE lsa    */
/*                                  from the database.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteFromRxmtLst (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
                        UINT1 u1DelFlag)
{

    tV3OsRxmtNode      *pRxmtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteFromRxmtLst\n");

    OSPFV3_TRC2 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "LSA to be deleted from rxmt list of Nbr Rtr Id %x"
                 "Nbr Interface Id %d \n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    if ((pLsaInfo == NULL) || (pLsaInfo->u4RxmtCount == 0))
    {
        return;
    }

    /* For Neigbour Probing */
    if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_LINK_LSA) &&
        (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                          pNbr->pInterface->pArea->pV3OspfCxt->rtrId)
         == OSPFV3_EQUAL))
    {
        if ((OSPFV3_IS_DC_EXT_APPLICABLE_IF (pNbr->pInterface)) &&
            (pNbr->pInterface->bDemandNbrProbe == OSIX_TRUE) &&
            (pNbr->u1NsmState == OSPFV3_NBRS_FULL) &&
            (OSPFV3_IS_HELLO_SUPPRESSED_NBR (pNbr)))
        {
            pNbr->u4NbrProbeCount = 0;
        }
    }

    pRxmtNode = V3LsuSearchLsaInRxmtLst (pNbr, pLsaInfo);

    if (pRxmtNode != NULL)
    {

        TMO_SLL_Delete (&(pNbr->lsaRxmtLst), &(pRxmtNode->nextLsaInfo));

        OSPFV3_RXMT_NODE_FREE (pRxmtNode);

        pNbr->lsaRxmtDesc.u4LsaRxmtCount--;

        /* If LSA Rxmt list is empty, stop Rxmt timer on that neighbor */
        if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
        {
            V3TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        }

        /* if lsa is not present in any rxmt list check the LSA age */

        if (--pLsaInfo->u4RxmtCount == 0)
        {
            O3RedDynLsuUpdAckToStandby (pLsaInfo);

            /* if advt age is MAX_AGE remove it from database */
            if ((OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
                && (u1DelFlag == OSIX_TRUE))
            {
                V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_FALSE);
            }
        }
    }
    else
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "LSA Not Present In Rxmt Lst \n");
        return;
    }

    OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "LSA Deleted From Rxmt List \n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteFromRxmtLst\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteFromAllRxmtLst                                  */
/*                                                                           */
/* Description  : The specified LSA is removed from all neighbor's rxmt      */
/*                lists.                                                     */
/*                                                                           */
/* Input        : pLsaInfo        : Pointer to the advertisement             */
/*                u1DelFlag       : If flag is set delete the MAX AGE lsa    */
/*                                  from the database.                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteFromAllRxmtLst (tV3OsLsaInfo * pLsaInfo, UINT1 u1DelFlag)
{

    tV3OsNeighbor      *pLstNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIntfNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteFromAllRxmtLst\n");

    pV3OspfCxt = pLsaInfo->pV3OspfCxt;

    if (pLsaInfo->u4RxmtCount == 0)
    {
        return;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLstNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);
        V3LsuDeleteFromRxmtLst (pLstNbr, pLsaInfo, u1DelFlag);
    }

    /* Delete from the virtual neighbors */
    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pIntfNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pIntfNode);

        pLstNode = TMO_SLL_First (&(pInterface->nbrsInIf));

        if (pLstNbr != NULL)
        {
            pLstNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                           pLstNode);
            V3LsuDeleteFromRxmtLst (pLstNbr, pLsaInfo, u1DelFlag);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteFromAllRxmtLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuClearRxmtLst                                         */
/*                                                                           */
/* Description  : This procedure clears the retransmission list associated   */
/*                with this neighbor.                                        */
/*                                                                           */
/* Input        : pNbr            : neighbour whose rxmt list has to be     */
/*                                   cleared                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuClearRxmtLst (tV3OsNeighbor * pNbr)
{
    tV3OsRxmtNode      *pRxmtNode = NULL;
    tV3OsRxmtNode      *pTempRxmtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuClearRxmtLst\n");

    OSPFV3_DYNM_SLL_SCAN (&(pNbr->lsaRxmtLst), pRxmtNode, pTempRxmtNode,
                          tV3OsRxmtNode *)
    {
        V3LsuDeleteFromRxmtLst (pNbr, pRxmtNode->pLsaInfo, OSIX_TRUE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuClearRxmtLst\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuRxmtLsu                                               */
/*                                                                           */
/* Description  : This procedure is called when the rxmt timer fires for a   */
/*                particular neighbor. This procedure constructs a ls update */
/*                packet from the top of the retransmission list and         */
/*                transmits it to the neighbor.                              */
/*                                                                           */
/* Input        : pNbr            : neighbour to whom the LS update packet   */
/*                                  is to be retransmitted                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuRxmtLsu (tV3OsNeighbor * pNbr)
{
    tV3OsRxmtNode      *pRxmtNode = NULL;
    tV3OsLsaInfo       *pGrLsaInfo = NULL;    /* pointer to Grace LSA
                                               info structure */
    UINT1               u1TxmtFlag = OSIX_FAILURE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuRxmtLsu\n");

    /* check rxmt count for neighbor */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount == 0)
    {
        return OSIX_SUCCESS;
    }

    OSPFV3_TRC2 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Retransmission to be done to Nbr Rtr Id %x"
                 "Nbr Interface Id %d\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 pNbr->u4NbrInterfaceId);

    TMO_SLL_Scan (&(pNbr->lsaRxmtLst), pRxmtNode, tV3OsRxmtNode *)
    {
        if (pRxmtNode->pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA)
        {
            if ((pNbr->pInterface->pArea->pV3OspfCxt->u1RestartStatus
                 == OSPFV3_RESTART_UNPLANNED) && (pNbr->u1GraceLsaTxCount > 0))
            {
                pGrLsaInfo = pRxmtNode->pLsaInfo;
                /* This LSA has been re-tranmitted once */
                continue;
            }
            else if (pNbr->u1GraceLsaTxCount >
                     pNbr->pInterface->pArea->pV3OspfCxt->u1GrLsaMaxTxCount)
            {
                pGrLsaInfo = pRxmtNode->pLsaInfo;
                continue;
            }
            pNbr->u1GraceLsaTxCount++;
        }
        /* For Nbr Probing */
        if ((OSPFV3_IS_DC_EXT_APPLICABLE_IF (pNbr->pInterface)) &&
            (pNbr->pInterface->bDemandNbrProbe == OSIX_TRUE) &&
            (pNbr->u1NsmState == OSPFV3_NBRS_FULL) &&
            (OSPFV3_IS_HELLO_SUPPRESSED_NBR (pNbr)))
        {
            if (pRxmtNode->pLsaInfo->lsaId.u2LsaType == OSPFV3_LINK_LSA)
            {
                if (pNbr->u4NbrProbeCount ==
                    pNbr->pInterface->u4NbrProbeRxmtLimit)
                {
                    OSPFV3_SET_NULL_RTR_ID (pNbr->desgRtr);
                    OSPFV3_SET_NULL_RTR_ID (pNbr->backupDesgRtr);
                    V3NsmResetVariables (pNbr);
                    V3TmrDeleteTimer (&(pNbr->inactivityTimer));
                    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                    return OSIX_FAILURE;
                }

                pNbr->u4NbrProbeCount++;    /* Increment the count */
            }
        }

        u1TxmtFlag =
            V3LsuAddToLsu (pNbr, pRxmtNode->pLsaInfo, pNbr->pInterface);
        if (u1TxmtFlag == OSIX_FAILURE)
        {
            return OSIX_SUCCESS;
        }
    }

    /* The above loop might end without the update packet being transmitted */
    V3LsuSendLsu (pNbr, pNbr->pInterface);

    /* If pGrLsaInfo is not NULL, then the grace LSA to be re-transmitted
     * to this neighbor has reached the threshold */
    if (pGrLsaInfo != NULL)
    {
        O3GrDeleteFromRxmtLst (pNbr, pGrLsaInfo);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuRxmtLsu\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFloodOut                                              */
/*                                                                           */
/* Description  : This procedure takes care of the flooding of a received    */
/*                LSA. The LSA is flooded out on certain interfaces.         */
/*                The pNbr refers to the neighbor from whom this LSA  was    */
/*                received. This paramater is NULL for self-originated LSAs. */
/*                This procedure returns FLOOD_BACK or NO_FLOOD_BACK based   */
/*                on whether the lsa has been flooded out on the receiving   */
/*                interface or not.                                          */
/*                                                                           */
/* Input        : pLsaInfo        :  Pointer to Lsa Info.                    */
/*                pRcvNbr         :  nbr from whom the lsa was received.     */
/*                                   NULL for self originated lsa            */
/*                pFloodArea      :  the area into which the lsa is to be    */
/*                                   flooded                                 */
/*                u1LsaChngdFlag  :  tells whether the contents of the lsa   */
/*                                   is changed or not.                      */
/*                pIface          :  the Interface into which the lsa is to  */
/*                                   be flooded (this field is valid only    */
/*                                   for Link Scope Lsa                      */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1AckFlag.                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuFloodOut (tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
               tV3OsArea * pFloodArea, UINT1 u1LsaChngdFlag,
               tV3OsInterface * pIface)
{

    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;
    UINT1               u1FloodScope = 0;
    tV3OsLsHeader       lsHeader;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFloodOut\n");

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);

    u1FloodScope = V3LsuGetLsaFloodScope (lsHeader.u2LsaType);

    switch (u1FloodScope)
    {
        case OSPFV3_LINK_FLOOD_SCOPE:
            u1AckFlag =
                V3LsuFloodLinkScopeLsa (pLsaInfo, pRcvNbr, u1LsaChngdFlag,
                                        pIface, &lsHeader);
            break;

        case OSPFV3_AREA_FLOOD_SCOPE:
            u1AckFlag =
                V3LsuFloodAreaScopeLsa (pLsaInfo, pRcvNbr, u1LsaChngdFlag,
                                        pFloodArea, &lsHeader);
            break;

        default:
            /* Default occurs for AS scope LSA */
            u1AckFlag =
                V3LsuFloodASScopeLsa (pLsaInfo, pRcvNbr, u1LsaChngdFlag,
                                      &lsHeader);
            break;

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFloodOut\n");

    return u1AckFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFloodASScopeLsa                                       */
/*                                                                           */
/* Description  : This procedure takes care of the flooding of AS scope      */
/*                LSA. The LSA is flooded out on certain interfaces.         */
/*                The pNbr refers to the neighbor from whom this LSA  was    */
/*                received. This paramater is NULL for self-originated LSAs. */
/*                This procedure returns FLOOD_BACK or NO_FLOOD_BACK based   */
/*                on whether the lsa has been flooded out on the receiving   */
/*                interface or not.                                          */
/*                                                                           */
/* Input        : pLsaInfo        : Pointer to Lsa Info.                     */
/*                pRcvNbr         :  nbr from whom the lsa was received.     */
/*                                   NULL for self originated lsa            */
/*                u1LsaChngdFlag  :  tells whether the contents of the lsa   */
/*                                   is changed or not.                      */
/*                pLsHeader       :  Pointer to LSA header                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1AckFlag.                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3LsuFloodASScopeLsa (tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
                      UINT1 u1LsaChngdFlag, tV3OsLsHeader * pLsHeader)
{

    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;
    UINT1               u1RetAckFlag = OSPFV3_UNUSED_ACK_FLAG;
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFloodASScopeLsa\n");

    TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {

        if (pArea->u4AreaType != OSPFV3_NORMAL_AREA)
        {
            continue;
        }

        u1AckFlag =
            V3LsuFloodAreaScopeLsa (pLsaInfo, pRcvNbr, u1LsaChngdFlag, pArea,
                                    pLsHeader);

        if (u1AckFlag != OSPFV3_UNUSED_ACK_FLAG)
        {
            u1RetAckFlag = u1AckFlag;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFloodASScopeLsa\n");
    return u1RetAckFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFloodAreaScopeLsa                                     */
/*                                                                           */
/* Description  : This procedure takes care of the flooding of Area scope    */
/*                LSA. The LSA is flooded out on certain interfaces.         */
/*                The pNbr refers to the neighbor from whom this LSA  was    */
/*                received. This paramater is NULL for self-originated LSAs. */
/*                This procedure returns FLOOD_BACK or NO_FLOOD_BACK based   */
/*                on whether the lsa has been flooded out on the receiving   */
/*                interface or not.                                          */
/*                                                                           */
/* Input        : pLsaInfo        :  Pointer to Lsa Info.                    */
/*                pRcvNbr         :  nbr from whom the lsa was received.     */
/*                                   NULL for self originated lsa            */
/*                u1LsaChngdFlag  :  tells whether the contents of the lsa   */
/*                                   is changed or not.                      */
/*                pArea           :  Pointer to Area                         */
/*                pLsHeader       :  Pointer to LSA header                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1AckFlag.                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3LsuFloodAreaScopeLsa (tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
                        UINT1 u1LsaChngdFlag, tV3OsArea * pArea,
                        tV3OsLsHeader * pLsHeader)
{

    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;
    UINT1               u1RetAckFlag = OSPFV3_UNUSED_ACK_FLAG;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFloodAreaScopeLsa\n");

    TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
        u1AckFlag =
            V3LsuFloodLinkScopeLsa (pLsaInfo, pRcvNbr, u1LsaChngdFlag,
                                    pInterface, pLsHeader);

        if (u1AckFlag != OSPFV3_UNUSED_ACK_FLAG)
        {
            u1RetAckFlag = u1AckFlag;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFloodAreaScopeLsa\n");
    return u1RetAckFlag;
}

/*****************************************************************************/
/* Function     : V3LsuFloodLinkScopeLsa                                     */
/*                                                                           */
/* Description  : This procedure takes care of the flooding of Link  scope   */
/*                LSA. The LSA is flooded out on certain interfaces.         */
/*                The pNbr refers to the neighbor from whom this LSA  was    */
/*                received. This paramater is NULL for self-originated LSAs. */
/*                This procedure returns FLOOD_BACK or NO_FLOOD_BACK based   */
/*                on whether the lsa has been flooded out on the receiving   */
/*                interface or not.                                          */
/*                                                                           */
/* Input        : pLsaInfo        :  Pointer to Lsa Info.                    */
/*                pRcvNbr         :  nbr from whom the lsa was received.     */
/*                                   NULL for self originated lsa            */
/*                u1LsaChngdFlag  :  tells whether the contents of the lsa   */
/*                                   is changed or not.                      */
/*                pInterface      :  Pointer to Interface.                   */
/*                pLsHeader       :  Pointer to LSA header                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u1AckFlag.                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3LsuFloodLinkScopeLsa (tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
                        UINT1 u1LsaChngdFlag, tV3OsInterface * pInterface,
                        tV3OsLsHeader * pLsHeader)
{

    UINT1               u1FloodFlag = OSIX_FALSE;
    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;
    UINT1               u1NbrExited = OSIX_FALSE;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsNeighbor      *pNeighbour = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsInterface     *pRcvInterface = NULL;
    tV3OsLsaReqNode    *pLsaReqNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFloodLinkScopeLsa\n");

    pRcvInterface = (pRcvNbr == NULL) ? NULL : pRcvNbr->pInterface;

    if ((OSPFV3_IS_VIRTUAL_IFACE (pInterface)) &&
        (OSPFV3_IS_AS_FLOOD_SCOPE_LSA (pLsHeader->u2LsaType)))
    {
        return u1AckFlag;
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

        if (pNbr->u1NsmState < OSPFV3_NBRS_EXCHANGE)
        {
            continue;
        }
        else if (pNbr->u1NsmState < OSPFV3_NBRS_FULL)
        {

            if ((pLsaReqNode = V3LrqSearchLsaReqLst (pNbr, pLsHeader)) != NULL)
            {

                switch (OSPFV3_LSU_FIND_RECENT_HDR (pLsHeader,
                                                    &(pLsaReqNode->lsHeader)))
                {
                    case OSPFV3_LSA1:

                        /* rcv ad is more recent */
                        V3LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                        break;

                    case OSPFV3_LSA2:

                        /* nbr copy is more recent, try next nbr */
                        continue;

                    default:

                        /* default occurs when OSPFV3_EQUAL is returned
                         * both are same instance */
                        V3LrqDeleteFromLsaReqLst (pNbr, pLsaReqNode);
                        continue;
                }
            }
        }

        /* Only if lsa is changed flood it over the demand interface */
        if ((OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface)) &&
            (OSPFV3_IS_DNA_LSA_PROCESS_CAPABLE (pInterface->pArea)) &&
            (u1LsaChngdFlag == OSIX_FALSE))
        {
            continue;
        }

        if (pNbr == pRcvNbr)
        {
            continue;
        }
        /*If topology change during helping, come out of helper */
        if ((pNbr->pInterface->pArea->pV3OspfCxt->u1StrictLsaCheck
             == OSIX_ENABLED) &&
            (pNbr->pInterface->pArea->pV3OspfCxt->u1HelperStatus
             == OSPFV3_GR_HELPING))
        {
            TMO_SLL_Scan (&(pNbr->pInterface->pArea->pV3OspfCxt->sortNbrLst),
                          pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbour = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                  nextSortNbr, pNbrNode);
                if ((pLsaInfo->u1LsaRefresh == OSIX_TRUE) &&
                    (pNeighbour->u1NbrHelperStatus == OSPFV3_GR_HELPING))
                {
                    if ((V3UtilRtrIdComp (pNeighbour->nbrRtrId,
                                          pNbr->nbrRtrId) == OSPFV3_EQUAL) &&
                        (pNeighbour->u4NbrInterfaceId ==
                         pNbr->u4NbrInterfaceId))
                    {
                        u1NbrExited = OSIX_TRUE;
                    }
                    O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNeighbour);
                }
            }
            TMO_SLL_Scan (&(pNbr->pInterface->pArea->pV3OspfCxt->virtIfLst),
                          pIfLstNode, tTMO_SLL_NODE *)
            {
                pInterface =
                    OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                         pIfLstNode);
                if ((pNbrNode =
                     TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
                {
                    pNeighbour = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                      nextNbrInIf, pNbrNode);
                    if ((pLsaInfo->u1LsaRefresh == OSIX_TRUE) &&
                        (pNeighbour->u1NbrHelperStatus == OSPFV3_GR_HELPING))
                    {
                        if ((V3UtilRtrIdComp (pNeighbour->nbrRtrId,
                                              pNbr->nbrRtrId) == OSPFV3_EQUAL)
                            && (pNeighbour->u4NbrInterfaceId ==
                                pNbr->u4NbrInterfaceId))
                        {
                            u1NbrExited = OSIX_TRUE;
                        }
                        O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                    }
                }
            }
        }
        if (u1NbrExited == OSIX_FALSE)
        {
            V3LsuAddToRxmtLst (pNbr, pLsaInfo);
            u1FloodFlag = OSIX_TRUE;
        }
    }

    if (u1FloodFlag == OSIX_FALSE)
    {
        if (pInterface == pRcvInterface)
        {
            u1AckFlag = OSPFV3_NO_FLOOD_BACK;
        }
        return u1AckFlag;
    }
    if (pInterface == pRcvInterface)
    {

        if ((V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                              pRcvNbr->nbrRtrId) == OSPFV3_EQUAL) ||
            (V3UtilRtrIdComp (OSPFV3_GET_BDR (pInterface),
                              pRcvNbr->nbrRtrId) == OSPFV3_EQUAL))
        {
            u1AckFlag = OSPFV3_NO_FLOOD_BACK;
            return u1AckFlag;
        }

        if (pInterface->u1IsmState == OSPFV3_IFS_BACKUP)
        {
            u1AckFlag = OSPFV3_NO_FLOOD_BACK;
            return u1AckFlag;
        }

        if ((pInterface->u1IsmState == OSPFV3_IFS_DR) &&
            (V3UtilRtrIdComp (pInterface->backupDesgRtr,
                              pRcvNbr->nbrRtrId) != OSPFV3_EQUAL))
        {
            u1AckFlag = OSPFV3_FLOOD_BACK;
        }
    }

    V3LsuAddToLsu (NULL, pLsaInfo, pInterface);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFloodLinkScopeLsa\n");

    return u1AckFlag;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddToLsu                                              */
/*                                                                           */
/* Description  : This procedure is called when a requested advertisement is */
/*                located in the database. The specified LSA is added to the */
/*                update packet. If the packet becomes full it is            */
/*                transmitted.                                               */
/*                                                                           */
/* Input        : pNbr             : Neighbour to whom the LSU is being      */
/*                                   transmitted                             */
/*                pLsaInfo         : Pointer to advertisement                */
/*                pInterface       : Pointer to Interface.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuAddToLsu (tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
               tV3OsInterface * pInterface)
{

    UINT2               u2LsaLen = 0;
    UINT2               u2AllocLen = 0;
    UINT2               u2LsaAge = 0;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddToLsu\n");
    if (pLsaInfo->pLsa == NULL)
    {
        gu4V3LsuAddToLsuFail++;
        return OSIX_FAILURE;
    }

    u2LsaLen = pLsaInfo->u2LsaLen;

    if ((pInterface->lsUpdate.u2CurrentLen > OSPFV3_IFACE_MTU (pInterface)) ||
        ((pInterface->lsUpdate.u2CurrentLen > 0) &&
         (u2LsaLen > (OSPFV3_IFACE_MTU (pInterface) -
                      pInterface->lsUpdate.u2CurrentLen))))
    {
        V3LsuSendLsu (pNbr, pInterface);
    }

    /* The LSA is added to the LSU */

    if (pInterface->lsUpdate.u2CurrentLen == 0)
    {

        u2AllocLen = (UINT2)
            (u2LsaLen + OSPFV3_HEADER_SIZE + OSPFV3_LSU_FIXED_PORTION_SIZE);

        u2AllocLen = (UINT2) ((u2AllocLen >
                               (UINT2) (OSPFV3_IFACE_MTU (pInterface))) ?
                              u2AllocLen : OSPFV3_IFACE_MTU (pInterface));

        if ((pInterface->lsUpdate.pLsuPkt =
             V3UtilOsMsgAlloc (u2AllocLen)) == NULL)
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for LS Update Packet\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "ALLOC FAILED for LS Update Pkt \n");
            gu4V3LsuAddToLsuFail++;
            return OSIX_FAILURE;
        }
        pInterface->lsUpdate.u2CurrentLen =
            OSPFV3_HEADER_SIZE + OSPFV3_LSU_FIXED_PORTION_SIZE;
        pInterface->lsUpdate.u2LsaCount = 0;
    }

    /* Calculate OSPFv3 Pkt Ptr */
    pOspfPkt = pInterface->lsUpdate.pLsuPkt + IPV6_HEADER_LEN;

    u2LsaAge = V3LsuGetLsaAge (pLsaInfo, pInterface);

    OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt, pLsaInfo->pLsa,
                                 pInterface->lsUpdate.u2CurrentLen, u2LsaLen);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt, pInterface->lsUpdate.u2CurrentLen,
                                 u2LsaAge);

    pInterface->lsUpdate.u2LsaCount++;
    pInterface->lsUpdate.u2CurrentLen += u2LsaLen;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddToLsu \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSendAllLsuInCxt                                       */
/*                                                                           */
/* Description  : This routine send all the LS update packets.               */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuSendAllLsuInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSendAllLsu \n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            if ((pInterface->lsUpdate.u2CurrentLen >
                 OSPFV3_IFACE_MTU (pInterface))
                || ((pInterface->lsUpdate.u2CurrentLen > 0)
                    &&
                    ((OSPFV3_IFACE_MTU (pInterface) -
                      pInterface->lsUpdate.u2CurrentLen) < 200)))
            {

                V3LsuSendLsu (NULL, pInterface);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSendAllLsu\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSendLsu                                               */
/*                                                                           */
/* Description  : This procedure just transmits the ls_update_pkt that was   */
/*                constructed during the processing of a received ls_req_pkt.*/
/*                This procedure is called when the processing of the        */
/*                received ls_req_pkt is over.                               */
/*                                                                           */
/* Input        : pNbr             : neighbour to whom the update packet is  */
/*                                   to be sent                              */
/*                pInterface       : pointer to the Interface.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuSendLsu (tV3OsNeighbor * pNbr, tV3OsInterface * pInterface)
{

    tV3OsNeighbor      *pLstNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4LsaCount = 0;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSendLsu \n");

    if ((pInterface->lsUpdate.pLsuPkt == NULL) ||
        (pInterface->lsUpdate.u2CurrentLen == 0))
    {
        if (pInterface->lsUpdate.pLsuPkt != NULL)
        {
            V3UtilOsMsgFree (pInterface->lsUpdate.pLsuPkt);
        }
        return;
    }

    /* Calculate the OSPFv3 Pkt Ptr */
    pOspfPkt = pInterface->lsUpdate.pLsuPkt + IPV6_HEADER_LEN;
    u4LsaCount = (UINT4) pInterface->lsUpdate.u2LsaCount;

    OSPFV3_BUFFER_ASSIGN_4_BYTE (pOspfPkt, OSPFV3_HEADER_SIZE, u4LsaCount);

    V3UtilConstructHdr (pInterface, pOspfPkt,
                        OSPFV3_LS_UPDATE_PKT,
                        pInterface->lsUpdate.u2CurrentLen);

    if (pNbr != NULL)
    {
        V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                      pInterface->lsUpdate.u2CurrentLen, pInterface,
                      &(pNbr->nbrIpv6Addr));
    }
    else
    {
        if (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)
        {

            if ((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                (pInterface->u1IsmState == OSPFV3_IFS_BACKUP))
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &gV3OsAllSpfRtrs);
            }
            else
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &gV3OsAllDRtrs);
            }
        }
        else
        {
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
            {
                pLstNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

                if (pLstNbr->u1NsmState >= OSPFV3_NBRS_EXCHANGE)
                {

                    V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                                  pInterface->lsUpdate.u2CurrentLen,
                                  pInterface, &(pLstNbr->nbrIpv6Addr));
                }
            }
        }

    }

    V3UtilOsMsgFree (pInterface->lsUpdate.pLsuPkt);
    pInterface->lsUpdate.pLsuPkt = NULL;
    V3LsuClearUpdate (&pInterface->lsUpdate);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSendLsu\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuClearUpdate                                           */
/*                                                                           */
/* Description  : Clears the update packet.                                  */
/*                                                                           */
/* Input        : pUpdate          : the LSU packet that is to be cleared    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuClearUpdate (tV3OsLsUpdate * pUpdate)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuClearUpdate\n");

    pUpdate->u2CurrentLen = 0;
    pUpdate->u2LsaCount = 0;

    if (pUpdate->pLsuPkt != NULL)
    {
        V3UtilOsMsgFree (pUpdate->pLsuPkt);
        pUpdate->pLsuPkt = NULL;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuClearUpdate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGetLsaAge                                             */
/*                                                                           */
/* Description  : This function get the LSA age.                             */
/*                                                                           */
/* Input        : pLsaInfo        : Pointer to the LSA Info.                 */
/*              : pInterface      : Pointer to the Interface.                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u2LsaAge                                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT2
V3LsuGetLsaAge (tV3OsLsaInfo * pLsaInfo, tV3OsInterface * pInterface)
{

    UINT2               u2LsaAge = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGetLsaLsaAge\n");

    OSPFV3_GET_LSA_AGE (pLsaInfo, &(u2LsaAge));

    u2LsaAge = (UINT2) (u2LsaAge + pInterface->u2IfTransDelay);

    if ((OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface)) &&
        (OSPFV3_IS_DNA_LSA_PROCESS_CAPABLE (pInterface->pArea)) &&
        (OSPFV3_IS_MAX_AGE (u2LsaAge)))
    {
        /*set DO_NOT_AGE in the lsa */
        u2LsaAge = OSPFV3_DO_NOT_AGE | OSPFV3_MAX_AGE;
    }
    else
    {
        if (OSPFV3_IS_MAX_AGE (u2LsaAge))
        {
            u2LsaAge = OSPFV3_MAX_AGE;
        }
    }

    if ((OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface)) &&
        (OSPFV3_IS_DNA_LSA_PROCESS_CAPABLE (pInterface->pArea)) &&
        (!OSPFV3_IS_MAX_AGE (u2LsaAge)))
    {
        /*set DO_NOT_AGE in the lsa */
        u2LsaAge |= OSPFV3_DO_NOT_AGE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGetLsaLsaAge\n");
    return u2LsaAge;
}

/*****************************************************************************/
/* Function     : V3LsuProcessAckFlag                                        */
/*                                                                           */
/* Description  : This routine send the LS ack packet.                       */
/*                                                                           */
/* Input        : pNbr          : Pointer to the neighbour                   */
/*                pLsa          : Pointer to LSA                             */
/*                u1AckFlag     : type of acknowledge                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuProcessAckFlag (tV3OsNeighbor * pNbr, UINT1 *pLsa, UINT1 u1AckFlag)
{

    /* process ackFlag  (section 13.5 , Table 19) */
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuProcessAckFlag\n");

    switch (u1AckFlag)
    {

        case OSPFV3_IMPLIED_ACK:

            /* received advt was duplicate and treated as implied ack */
            V3LakAddToDelayedAck (pNbr->pInterface, pLsa);
            break;

        case OSPFV3_NO_IMPLIED_ACK:

            /* received advt was duplicate but not treated as implied ack */
            V3LakAddToDelayedAck (pNbr->pInterface, pLsa);
            break;

        case OSPFV3_FLOOD_BACK:

            /* The lsa is flooded out on receiving interface */
            break;

        case OSPFV3_NO_FLOOD_BACK:

            /* The lsa is not flooded out on receiving interface */
            if ((pNbr->pInterface->u1IsmState != OSPFV3_IFS_BACKUP) ||
                ((pNbr->pInterface->u1IsmState == OSPFV3_IFS_BACKUP) &&
                 (V3UtilRtrIdComp (OSPFV3_GET_DR (pNbr->pInterface),
                                   pNbr->nbrRtrId) == OSPFV3_EQUAL)))
            {
                V3LakAddToDelayedAck (pNbr->pInterface, pLsa);
            }
            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuProcessAckFlag\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lsflod.c                      */
/*-----------------------------------------------------------------------*/
