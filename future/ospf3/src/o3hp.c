/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3hp.c,v 1.20 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             Hello protocol.
 *
 *******************************************************************/

#include "o3inc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID        V3HpSendHelloNbma
PROTO ((UINT1 *pHelloPkt,
        UINT2 u2PktLen, tV3OsInterface * pInterface, UINT1 u1TimerId));
PRIVATE VOID        V3HpSendHelloToAllNbr
PROTO ((UINT1 *pHelloPkt,
        UINT2 u2PktLen, tV3OsInterface * pInterface, UINT1 u1TimerId));
PRIVATE VOID        V3HpSendHelloToEligibleNbr
PROTO ((UINT1 *pHelloPkt,
        UINT2 u2PktLen, tV3OsInterface * pInterface, UINT1 u1TimerId));
PRIVATE UINT2       V3HpConstructHello
PROTO ((tV3OsInterface * pInterface, UINT1 *pHelloPkt));
PRIVATE VOID V3HpStartIneligibleNbr PROTO ((tV3OsInterface * pInterface));
PRIVATE VOID V3HpCheckAllAdj PROTO ((tV3OsInterface * pInterface));
PRIVATE VOID V3HpElectBdrFromLst PROTO ((tV3OsInterface * pInterface));
PRIVATE VOID V3HpElectDrFromLst PROTO ((tV3OsInterface * pInterface));
PRIVATE UINT1       V3HpCheckRtrInRcvdHelloInCxt
PROTO ((tV3OsRouterId rtrId, UINT1 *pHelloPkt, UINT2 u2PktLen));
PRIVATE tV3OsNeighbor *V3HpSearchNbmaNbrLst PROTO ((tIp6Addr * pIp6SrcAddr,
                                                    tV3OsInterface *
                                                    pInterface));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSendHello                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.5                           */
/*                This procedure constructs and sends a hello on the         */
/*                specified interface. If the pNbr field is valid then hello */
/*                packet is sent to that neighbor alone. Otherwise hellos are*/
/*                sent to all neighbors on the interface. If the timer_id is */
/*                OSPFV3_HELLO_TIMER, it indicates that the hello timer has  */
/*                fired. Then hello packets are sent to neighbors in state   */
/*                other than down. If the field is OSPFV3_POLL_TIMER then it */
/*                indicates that the poll timer has fired. In that case hello*/
/*                packets are sent to neighbors in state down only.          */
/*                                                                           */
/* Input        : pInterface       : The interface, on which hello is to be  */
/*                                   sent                                    */
/*                pNbr             : non-null if hello is to be sent to      */
/*                                   single nbr                              */
/*                u1TimerId        : value is OSPFV3_HELLO_TIMER or          */
/*                                   OSPFV3_POLL_TIMER                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3HpSendHello (tV3OsInterface * pInterface, tV3OsNeighbor * pNbr,
               UINT1 u1TimerId)
{

    UINT1              *pHelloPkt = NULL;
    UINT2               u2PktLen = 0;
    UINT1               u1HelloTxed = OSIX_FALSE;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tIp6Addr            destAddr;
    tV3OsNeighbor      *pTmpNbr = NULL;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3HpSendHello: Hello to be sent on interface : %d,",
                 pInterface->u4InterfaceId);

    if ((pHelloPkt = V3UtilOsMsgAlloc (OSPFV3_IFACE_MTU (pInterface))) == NULL)
    {

        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate memory  for Transmission of HP on %d interface\n ",
                      pInterface->u4InterfaceId));
        OSPFV3_TRC1 (OS_RESOURCE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Alloc Failed For Txmission of HP on %d\n",
                     pInterface->u4InterfaceId);
        return;
    }

    /* Calculate the OSPFv3 Pkt Ptr */
    pOspfPkt = pHelloPkt + IPV6_HEADER_LEN;

    /* Construction of Hello Packet */
    u2PktLen = V3HpConstructHello (pInterface, pOspfPkt);

    if (pNbr != NULL)
    {
        OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "HP To Be Sent To Single Neighbor %x.%d\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     pNbr->u4NbrInterfaceId);

        OSPFV3_IP6_ADDR_COPY (destAddr, pNbr->nbrIpv6Addr);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_NBMA)
    {
        OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "HP To Be Sent Over NBMA\n");

        V3HpSendHelloNbma (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        u1HelloTxed = OSIX_TRUE;
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)
    {
        OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "HP To Be Sent To Multicast Address %s\n",
                     Ip6PrintAddr (&(gV3OsAllSpfRtrs)));
        OSPFV3_IP6_ADDR_COPY (destAddr, gV3OsAllSpfRtrs);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
    {
        pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
        pTmpNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "HP To Be Sent To Virtual Neighbor %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pTmpNbr->nbrRtrId));
        OSPFV3_IP6_ADDR_COPY (destAddr, pTmpNbr->nbrIpv6Addr);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_PTOP)
    {

        if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_IF (pInterface))
        {
            pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
            if (pNbrNode == NULL)
            {
                if (u1TimerId == OSPFV3_HELLO_TIMER)
                {
                    u1HelloTxed = OSIX_FALSE;
                }
            }
            else
            {
                pTmpNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                               nextNbrInIf, pNbrNode);

                if (((pTmpNbr->u1NsmState > OSPFV3_NBRS_DOWN) &&
                     (u1TimerId == OSPFV3_HELLO_TIMER)) ||
                    ((pTmpNbr->u1NsmState == OSPFV3_NBRS_DOWN) &&
                     (u1TimerId == OSPFV3_POLL_TIMER)))
                {
                    u1HelloTxed = OSIX_FALSE;
                }
                else
                {
                    u1HelloTxed = OSIX_TRUE;
                }
            }
        }
        OSPFV3_IP6_ADDR_COPY (destAddr, gV3OsAllSpfRtrs);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_PTOMP)
    {
        V3HpSendHelloToAllNbr (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        u1HelloTxed = OSIX_TRUE;
    }

    if (u1HelloTxed == OSIX_FALSE)
    {
#ifdef OSPF3_BCMXNP_HELLO_WANTED
        /*Changes for NPHelloTx.To avoid the time delay during switchover By-passing the CFA  */
        if (V3NPSendHelloPktInCxt (pHelloPkt, u2PktLen, pInterface, &destAddr)
            == OSPFV3_SUCCESS)
        {
            OSPFV3_COUNTER_OP (pInterface->u4HelloTxedCount, 1);
        }
#else
        V3PppSendPkt (pHelloPkt, u2PktLen, pInterface, &destAddr);
#endif

    }

    /* Freeing the memory allocated for Hello Packet */
    V3UtilOsMsgFree (pHelloPkt);

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId, "HP Transmitted\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSendHello\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSendHelloNbma                                          */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.5.1                         */
/*                This procedure takes care of sending hello packets on NBMA */
/*                networks.                                                  */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent               */
/*                u2PktLen          : length of the pkt                      */
/*                pInterface        : the interface to NBMA                  */
/*                u1TimerId         : value is OSPFV3_HELLO_TIMER or         */
/*                                    OSPFV3_ POLL_TIMER                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpSendHelloNbma (UINT1 *pHelloPkt, UINT2 u2PktLen,
                   tV3OsInterface * pInterface, UINT1 u1TimerId)
{

    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3HpSendHelloNbma: HP To Be Sent On NBMA If %s\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    if (pInterface->u1IsmState >= OSPFV3_IFS_WAITING)
    {

        if ((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
            (pInterface->u1IsmState == OSPFV3_IFS_BACKUP))
        {
            V3HpSendHelloToAllNbr (pHelloPkt, u2PktLen, pInterface, u1TimerId);
        }
        else if (pInterface->u1RtrPriority > OSPFV3_INELIGIBLE_RTR_PRIORITY)
        {
            V3HpSendHelloToEligibleNbr (pHelloPkt, u2PktLen, pInterface,
                                        u1TimerId);
        }
        else
        {
            OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP To Be Sent To DR/BDR Alone Interface %s\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)));

            if (!(OSPFV3_IS_NULL_RTR_ID (pInterface->desgRtr)))
            {
                pNbr =
                    V3HpSearchNbrLst (&(pInterface->desgRtr), OSPFV3_ZERO,
                                      pInterface);
                if (pNbr != NULL)
                {
                    V3PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                                  &(pNbr->nbrIpv6Addr));
                }
            }
            if (!(OSPFV3_IS_NULL_RTR_ID (pInterface->backupDesgRtr)))
            {
                pNbr =
                    V3HpSearchNbrLst (&(pInterface->backupDesgRtr), OSPFV3_ZERO,
                                      pInterface);
                if (pNbr != NULL)
                {
                    V3PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                                  &(pNbr->nbrIpv6Addr));
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "HP Transmitted On NBMA\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSendHelloNbma\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSendHelloToAllNbr                                      */
/*                                                                           */
/* Description  : Transmits the hello packet to all neighbors on the speci-  */
/*                fied interface.                                            */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent               */
/*                u2PktLen          : length of the pkt                      */
/*                pInterface        : the interface on which hello is to be  */
/*                                    sent                                   */
/*                u1TimerId         : value is OSPFV3_HELLO_TIMER or         */
/*                                    OSPFV3_POLL_TIMER                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3HpSendHelloToAllNbr (UINT1 *pHelloPkt, UINT2 u2PktLen,
                       tV3OsInterface * pInterface, UINT1 u1TimerId)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpSendHelloToAllNbr\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (((pNbr->u1NsmState > OSPFV3_NBRS_DOWN) &&
             (u1TimerId == OSPFV3_HELLO_TIMER)) ||
            ((pNbr->u1NsmState == OSPFV3_NBRS_DOWN) &&
             (u1TimerId == OSPFV3_POLL_TIMER)) ||
            ((pInterface->u1NetworkType == OSPFV3_IF_PTOMP) &&
             (u1TimerId == OSPFV3_HELLO_TIMER)))
        {
            V3PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                          &(pNbr->nbrIpv6Addr));
        }
    }

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "HP Transmitted To All Nbrs\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSendHelloToAllNbr\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSendHelloToEligibleNbr                                 */
/*                                                                           */
/* Description  : Transmits the hello packet to all neighbors eligible to    */
/*                become DR or BDR on this interface.                        */
/*                                                                           */
/* Input        : pHelloPkt         : the hello pkt to be sent               */
/*                u2PktLen          : length of the pkt                      */
/*                pInterface        : the interface on which hello is to be  */
/*                                    sent                                   */
/*                u1TimerId         : value is OSPFV3_HELLO_TIMER or         */
/*                                    OSPFV3_POLL_TIMER                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpSendHelloToEligibleNbr (UINT1 *pHelloPkt, UINT2 u2PktLen,
                            tV3OsInterface * pInterface, UINT1 u1TimerId)
{

    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpSendHelloToEligibleNbr\n");

    OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "HP To Be Sent To Eligible Neighbors On NBMA If %s TmrId %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)), u1TimerId);

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        if (pNbr->u1NbrRtrPriority > OSPFV3_INELIGIBLE_RTR_PRIORITY)
        {

            if (((pNbr->u1NsmState > OSPFV3_NBRS_DOWN) &&
                 (u1TimerId == OSPFV3_HELLO_TIMER)) ||
                ((pNbr->u1NsmState == OSPFV3_NBRS_DOWN) &&
                 (u1TimerId == OSPFV3_POLL_TIMER)))
            {
                V3PppSendPkt (pHelloPkt, u2PktLen, pInterface,
                              &(pNbr->nbrIpv6Addr));
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSendHelloToEligibleNbr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpConstructHello                                         */
/*                                                                           */
/* Description  : Reference : RFC-2328 section A.3.2                         */
/*                This procedure constructs the hello packet to be transa-   */
/*                mitted on the specified interface.                         */
/*                                                                           */
/* Input        : pInterface       : the interface on which hello is to be   */
/*                                   sent                                    */
/*                pHelloPkt        : the buffer in which hello is to be      */
/*                                   constructed                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : packet length                                              */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT2
V3HpConstructHello (tV3OsInterface * pInterface, UINT1 *pHelloPkt)
{

    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT2               u2NbrCount = 0;
    UINT2               u2PktLen = 0;
    tV3OsOptions        options;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpConstructHello\n");

    OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Hello to be constructed for transmission on interface : %s,"
                 " IfIndex : %d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_OPT_CLEAR_ALL (options);

    OSPFV3_BUFFER_ASSIGN_4_BYTE (pHelloPkt, OSPFV3_HP_INTERFACE_ID_OFFSET,
                                 pInterface->u4InterfaceId);

    OSPFV3_BUFFER_ASSIGN_1_BYTE (pHelloPkt, OSPFV3_HP_RTR_PRI_OFFSET,
                                 pInterface->u1RtrPriority);

    options[OSPFV3_OPT_BYTE_THREE] =
        pInterface->pArea->pV3OspfCxt->rtrOptions[OSPFV3_OPT_BYTE_THREE] |
        pInterface->pArea->areaOptions[OSPFV3_OPT_BYTE_THREE];

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pInterface))
    {
        options[OSPFV3_OPT_BYTE_THREE] =
            options[OSPFV3_OPT_BYTE_THREE] |
            pInterface->ifOptions[OSPFV3_OPT_BYTE_THREE];
    }

    OSPFV3_BUFFER_APPEND_STRING (pHelloPkt, options, OSPFV3_HP_OPTIONS_OFFSET,
                                 OSPFV3_OPTION_LEN);

    OSPFV3_BUFFER_ASSIGN_2_BYTE (pHelloPkt, OSPFV3_HP_HELLO_INTERVAL_OFFSET,
                                 pInterface->u2HelloInterval);

    OSPFV3_BUFFER_ASSIGN_2_BYTE (pHelloPkt, OSPFV3_HP_RTR_DEAD_INTERVAL_OFFSET,
                                 pInterface->u2RtrDeadInterval);

    OSPFV3_BUFFER_APPEND_STRING (pHelloPkt, pInterface->desgRtr,
                                 OSPFV3_HP_DSG_RTR_OFFSET, OSPFV3_RTR_ID_LEN);
    OSPFV3_BUFFER_APPEND_STRING (pHelloPkt, pInterface->backupDesgRtr,
                                 OSPFV3_HP_BK_DSG_RTR_OFFSET,
                                 OSPFV3_RTR_ID_LEN);

    u2NbrCount = 0;
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (pNbr->u1NsmState >= OSPFV3_NBRS_INIT)
        {
            OSPFV3_BUFFER_APPEND_STRING (pHelloPkt, pNbr->nbrRtrId,
                                         (UINT4) (OSPFV3_HP_NBR_ID_OFFSET +
                                                  (OSPFV3_RTR_ID_LEN *
                                                   u2NbrCount)),
                                         (UINT4) OSPFV3_RTR_ID_LEN);
            u2NbrCount++;
        }
    }

    u2PktLen = (UINT2) (OSPFV3_HEADER_SIZE +
                        OSPFV3_HELLO_FIXED_PORTION_SIZE +
                        u2NbrCount * (sizeof (tV3OsRouterId)));
    V3UtilConstructHdr (pInterface, pHelloPkt, OSPFV3_HELLO_PKT, u2PktLen);

    OSPFV3_TRC (OSPFV3_HP_TRC | CONTROL_PLANE_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Hello constructed\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpConstructHello\n");

    return ((UINT2) u2PktLen);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSearchNbrLst                                           */
/*                                                                           */
/* Description  : The list of neighbor structures is searched for a matching */
/*                entry. The key for the search is rtrId.                    */
/*                                                                           */
/* Input        : pRtrId           : the Rtr-Id of the nbr to be searched    */
/*                u4InterfaceId    : the If-id of the nbr to be searched.    */
/*                                   The value will be ZERO for normal OSPFv3*/
/*                                   nbr.                                    */
/*                pInterface       : the interface whose nbr list is         */
/*                                   searched                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to matching neighbour structure, if found          */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsNeighbor *
V3HpSearchNbrLst (tV3OsRouterId * pRtrId,
                  UINT4 u4InterfaceId, tV3OsInterface * pInterface)
{

    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpSearchNbrLst\n");
    OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Search for neighbor with nbrId : %x on interface : %s\n",
                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pRtrId),
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        if (u4InterfaceId != OSPFV3_ZERO)
        {
            if ((V3UtilRtrIdComp (*pRtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL) &&
                (pNbr->u4NbrInterfaceId == u4InterfaceId))
            {
                OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                             pInterface->pArea->pV3OspfCxt->u4ContextId,
                             "Neighbor with ID : %x found\n",
                             OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pRtrId));
                return (pNbr);
            }
        }
        else
        {
            if (V3UtilRtrIdComp (*pRtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL)
            {
                OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                             pInterface->pArea->pV3OspfCxt->u4ContextId,
                             "Neighbor with ID : %x  found\n",
                             OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pRtrId));
                return (pNbr);
            }

        }
    }

    SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                  "Neighbor with ID : %x not found\n",
                  OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pRtrId)));

    OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Neighbor with ID : %x not found\n",
                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pRtrId));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSearchNbrLst\n");
    return (NULL);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpElectDesgRtr                                           */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.4                           */
/*                This procedure elects a designated router and backup       */
/*                designated router for the specified multi-access network.  */
/*                                                                           */
/* Input        : pInterface        : the interface for which DR is to be    */
/*                                    elected                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3HpElectDesgRtr (tV3OsInterface * pInterface)
{

    tV3OsRouterId       prevDr;
    tV3OsRouterId       prevBdr;
    UINT1               u1NewlyDr = 0;
    UINT1               u1NewlyBdr = 0;
    UINT1               u1NoLongerDr = 0;
    UINT1               u1NoLongerBdr = 0;
    UINT1               u1PrevIfState = OSPFV3_IFS_DOWN;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLinkStateId    tmpLinkStateId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpElectDesgRtr\n");

    OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DR/BDR Election Interface %s.%d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    if (((pInterface->pArea->pV3OspfCxt->u1RestartExitReason
          == OSPFV3_RESTART_INPROGRESS)
         && (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState
             == OSPFV3_GR_RESTART)) ||
        (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE))
    {
        OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "DR/BDR Election Over Interface %s.%d\n, Router/Network Lsa not"
                     "modified- Router is in GR State",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);

        V3HpCheckAllAdj (pInterface);

        OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3HpElectDesgRtr\n");
        return;
    }

    /* Save prev DR and BDR value. (RFC-2328, section 9.4 Step 1) */

    OSPFV3_RTR_ID_COPY (prevDr, OSPFV3_GET_DR (pInterface));
    OSPFV3_RTR_ID_COPY (prevBdr, OSPFV3_GET_BDR (pInterface));
    u1PrevIfState = pInterface->u1IsmState;

    /* Elect DR and BDR (RFC-2328, section 9.4 Step 2 and 3) */

    V3HpElectBdrFromLst (pInterface);
    V3HpElectDrFromLst (pInterface);

    if ((u1PrevIfState == OSPFV3_IFS_DR) &&
        (V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                          pInterface->pArea->pV3OspfCxt->rtrId) !=
         OSPFV3_EQUAL))
    {
        u1NoLongerDr = 1;
    }
    else if ((u1PrevIfState != OSPFV3_IFS_DR) &&
             (V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                               pInterface->pArea->pV3OspfCxt->rtrId) ==
              OSPFV3_EQUAL))
    {
        u1NewlyDr = 1;
    }
    else if ((u1PrevIfState == OSPFV3_IFS_BACKUP) &&
             (V3UtilRtrIdComp (OSPFV3_GET_BDR (pInterface),
                               pInterface->pArea->pV3OspfCxt->rtrId) !=
              OSPFV3_EQUAL))
    {
        u1NoLongerBdr = 1;
    }
    else if ((u1PrevIfState != OSPFV3_IFS_BACKUP) &&
             (V3UtilRtrIdComp (OSPFV3_GET_BDR (pInterface),
                               pInterface->pArea->pV3OspfCxt->rtrId) ==
              OSPFV3_EQUAL))
    {
        u1NewlyBdr = 1;
    }

    if ((u1NewlyDr) || (u1NewlyBdr) || (u1NoLongerDr) || (u1NoLongerBdr))
    {
        u1NewlyDr = u1NewlyBdr = 0;
        V3HpElectBdrFromLst (pInterface);
        V3HpElectDrFromLst (pInterface);
        if ((u1PrevIfState != OSPFV3_IFS_DR) &&
            (V3UtilRtrIdComp (OSPFV3_GET_DR (pInterface),
                              pInterface->pArea->pV3OspfCxt->rtrId) ==
             OSPFV3_EQUAL))
        {
            u1NewlyDr = 1;
        }
        if ((u1PrevIfState != OSPFV3_IFS_BACKUP) &&
            (V3UtilRtrIdComp (OSPFV3_GET_BDR (pInterface),
                              pInterface->pArea->pV3OspfCxt->rtrId) ==
             OSPFV3_EQUAL))
        {
            u1NewlyBdr = 1;
        }
    }

    if (V3UtilRtrIdComp
        (OSPFV3_GET_DR (pInterface),
         pInterface->pArea->pV3OspfCxt->rtrId) == OSPFV3_EQUAL)
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_DR);
    }
    else if (V3UtilRtrIdComp (OSPFV3_GET_BDR (pInterface),
                              pInterface->pArea->pV3OspfCxt->rtrId) ==
             OSPFV3_EQUAL)
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_BACKUP);
    }
    else
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_DR_OTHER);
    }

    if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) &&
        ((u1NewlyDr) || (u1NewlyBdr)))
    {
        V3HpStartIneligibleNbr (pInterface);
    }

    V3HpCheckAllAdj (pInterface);

    if ((V3UtilRtrIdComp (prevDr, OSPFV3_GET_DR (pInterface)) != OSPFV3_EQUAL))
    {
        if ((pInterface->u4NbrFullCount > 0) &&
            (u1PrevIfState != OSPFV3_IFS_WAITING))
        {
            V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                                   OSPFV3_SIG_DR_CHANGE, (UINT1 *) pInterface);
        }
    }

    if ((u1PrevIfState == OSPFV3_IFS_DR) &&
        (pInterface->u1IsmState != OSPFV3_IFS_DR))
    {

        OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, pInterface->u4InterfaceId);
        pLsaInfo = V3LsuSearchDatabase (OSPFV3_NETWORK_LSA,
                                        &(tmpLinkStateId),
                                        &(pInterface->pArea->pV3OspfCxt->rtrId),
                                        NULL, pInterface->pArea);

        if (pLsaInfo != NULL)
        {
            V3AgdFlushOut (pLsaInfo);
        }

        V3GenIntraAreaPrefixLsa (pInterface->pArea);
    }

    /* Send the information to the standby node */
    O3RedDynSendIntfInfoToStandby (pInterface, OSIX_FALSE);

    OSPFV3_TRC2 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DR/BDR Election Over If %s.%d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpElectDesgRtr\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpStartIneligibleNbr                                     */
/*                                                                           */
/* Description  : Generates event start for all neighbors ineligible to      */
/*                become DR.                                                 */
/*                                                                           */
/* Input        : pInterface        : interface on which nbrs are to be      */
/*                                    signalled                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpStartIneligibleNbr (tV3OsInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpStartIneligibleNbr\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (pNbr->u1NbrRtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY)
        {
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_START);
        }
    }

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Started generating event start for all InEligible Neighbors\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpStartIneligibleNbr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpCheckAllAdj                                            */
/*                                                                           */
/* Description  : Generates event ADJOK for all neighbors in state atleast   */
/*                2way.                                                      */
/*                                                                           */
/* Input        : pInterface       : interface on which adjacencies are      */
/*                                   checked                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpCheckAllAdj (tV3OsInterface * pInterface)
{
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpCheckAllAdj\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (pNbr->u1NsmState >= OSPFV3_NBRS_2WAY)
        {
            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_ADJ_OK);
        }
    }

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Adjacencies Verified\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpCheckAllAdj\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpElectBdrFromLst                                        */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.4 step 2                    */
/*                Elects a backup designated router from the list of eligible*/
/*                routers.                                                   */
/*                                                                           */
/* Input        : pInterface     : interface on which bdr is to be elected   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpElectBdrFromLst (tV3OsInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT1               u1ChangeFlag = OSIX_FALSE;
    UINT4               u4CurrBdrIfaceId = 0;
    /* priority of the current bdr */
    UINT1               u1CurrBdrPriority = 0;
    UINT1               u1CurrBdrStatus = OSPFV3_ALL_RTRS;

    /*
     * = OSPFV3_ONLY_BDRS if current bdr has advertised itself as bdr.
     * If this field is OSPFV3_ALL_RTRS then current bdr was elected
     * based on router priority alone.
     */

    tV3OsRouterId       newBdrId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpElectBdrFromLst\n");

    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "BDR To Be Elected From Eligible Neighbor List Id %s\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    /* consider this router */

    OSPFV3_SET_NULL_RTR_ID (newBdrId);

    if (pInterface->u1RtrPriority != OSPFV3_INELIGIBLE_RTR_PRIORITY)
    {

        if (V3UtilRtrIdComp (pInterface->pArea->pV3OspfCxt->rtrId,
                             OSPFV3_GET_DR (pInterface)) != OSPFV3_EQUAL)
        {
            u1CurrBdrPriority = pInterface->u1RtrPriority;
            u4CurrBdrIfaceId = pInterface->u4InterfaceId;
            OSPFV3_RTR_ID_COPY (newBdrId, pInterface->pArea->pV3OspfCxt->rtrId);
            if (V3UtilRtrIdComp (pInterface->pArea->pV3OspfCxt->rtrId,
                                 OSPFV3_GET_BDR (pInterface)) == OSPFV3_EQUAL)
            {
                u1CurrBdrStatus = OSPFV3_ONLY_BDRS;
            }
        }
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        /* Eligible neighbors are present at the start of the nbr list */

        if (pNbr->u1NbrRtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY)
        {
            OSPFV3_TRC (OSPFV3_ISM_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "InEligible Neighbor Priority Encountered\n");
            break;
        }

        /* Only nbrs in state >= NBRS_2WAY are considered */

        if (pNbr->u1NsmState < OSPFV3_NBRS_2WAY)
        {

            OSPFV3_TRC (OSPFV3_ISM_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Neighbor State Less Than 2-Way\n");

            continue;
        }

        if (V3UtilRtrIdComp (pNbr->nbrRtrId, pNbr->desgRtr) == OSPFV3_EQUAL)
        {
            OSPFV3_TRC (OSPFV3_ISM_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Neighbor Advertises Itself As DR\n");

            continue;
        }

        u1ChangeFlag = OSIX_FALSE;

        /* CONSIDER ONLY bdrs, if u1CurrBdrStatus ON */

        if ((V3UtilRtrIdComp (pNbr->nbrRtrId,
                              pNbr->backupDesgRtr) != OSPFV3_EQUAL) &&
            (u1CurrBdrStatus == OSPFV3_ONLY_BDRS))
        {
            continue;
        }

        if ((V3UtilRtrIdComp (pNbr->nbrRtrId,
                              pNbr->backupDesgRtr) == OSPFV3_EQUAL) &&
            (u1CurrBdrStatus != OSPFV3_ONLY_BDRS))
        {
            u1CurrBdrStatus = OSPFV3_ONLY_BDRS;
            u1ChangeFlag = OSIX_TRUE;
        }
        else
        {
            /* decision based on priority */

            if (u1CurrBdrPriority < pNbr->u1NbrRtrPriority)
            {
                u1ChangeFlag = OSIX_TRUE;
            }
            else if (u1CurrBdrPriority == pNbr->u1NbrRtrPriority)
            {

                if (V3UtilRtrIdComp (newBdrId, pNbr->nbrRtrId) == OSPFV3_LESS)
                {
                    u1ChangeFlag = OSIX_TRUE;
                }
            }
        }

        if (u1ChangeFlag == OSIX_TRUE)
        {

            u1CurrBdrPriority = pNbr->u1NbrRtrPriority;
            u4CurrBdrIfaceId = pNbr->u4NbrInterfaceId;
            OSPFV3_RTR_ID_COPY (newBdrId, pNbr->nbrRtrId);
        }
    }

    OSPFV3_RTR_ID_COPY (pInterface->backupDesgRtr, newBdrId);
    pInterface->u4BDRInterfaceId = u4CurrBdrIfaceId;

    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "BDR Election Over new BDR %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pInterface->backupDesgRtr));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpElectBdrFromLst\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpElectDrFromLst                                         */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.4 step 3                    */
/*                Elects a designated router from the list of eligible       */
/*                routers.                                                   */
/*                                                                           */
/* Input        : pInterface     : interface on which dr is to be elected    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3HpElectDrFromLst (tV3OsInterface * pInterface)
{

    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsRouterId       newDrId;
    UINT4               newDrIfaceId = 0;
    UINT1               u1ChangeFlag = OSIX_FALSE;

    /* Priority of the current dr */
    UINT1               u1CurrDrPriority = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpElectDrFromLst\n");
    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DR To Be Elected From Eligible Neighbor List Id %s\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    OSPFV3_SET_NULL_RTR_ID (newDrId);

    if (pInterface->u1RtrPriority != OSPFV3_INELIGIBLE_RTR_PRIORITY)
    {

        if (V3UtilRtrIdComp (pInterface->pArea->pV3OspfCxt->rtrId,
                             OSPFV3_GET_DR (pInterface)) == OSPFV3_EQUAL)
        {
            u1CurrDrPriority = pInterface->u1RtrPriority;
            OSPFV3_RTR_ID_COPY (newDrId, pInterface->pArea->pV3OspfCxt->rtrId);
            newDrIfaceId = pInterface->u4InterfaceId;
        }
    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        /* Eligible neighbors are present at the start of the nbr list */
        if (pNbr->u1NbrRtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY)
        {

            OSPFV3_TRC (OSPFV3_ISM_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "InEligible Neighbor Priority Encountered\n");

            break;
        }

        /* Only nbrs in state >= NBRS_2WAY are considered */
        if (pNbr->u1NsmState < OSPFV3_NBRS_2WAY)
        {

            OSPFV3_TRC (OSPFV3_ISM_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Neighbor State Less Than 2-Way\n");

            continue;
        }

        u1ChangeFlag = OSIX_FALSE;
        if (V3UtilRtrIdComp (pNbr->nbrRtrId, pNbr->desgRtr) == OSPFV3_EQUAL)
        {

            if (u1CurrDrPriority < pNbr->u1NbrRtrPriority)
            {
                u1ChangeFlag = OSIX_TRUE;
            }
            else if (u1CurrDrPriority == pNbr->u1NbrRtrPriority)
            {

                if (V3UtilRtrIdComp (newDrId, pNbr->nbrRtrId) == OSPFV3_LESS)
                {
                    u1ChangeFlag = OSIX_TRUE;
                }
            }
        }

        if (u1ChangeFlag == OSIX_TRUE)
        {
            OSPFV3_RTR_ID_COPY (newDrId, pNbr->nbrRtrId);
            newDrIfaceId = pNbr->u4NbrInterfaceId;
            u1CurrDrPriority = pNbr->u1NbrRtrPriority;
        }
    }

    if (OSPFV3_IS_NULL_RTR_ID (newDrId))
    {
        OSPFV3_RTR_ID_COPY (pInterface->desgRtr, pInterface->backupDesgRtr);
        pInterface->u4DRInterfaceId = pInterface->u4BDRInterfaceId;
    }
    else
    {
        OSPFV3_RTR_ID_COPY (pInterface->desgRtr, newDrId);
        pInterface->u4DRInterfaceId = newDrIfaceId;
    }

    OSPFV3_TRC1 (OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DR Election Over New DR %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pInterface->desgRtr));
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpElectDrFromLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpCheckRtrInRcvdHelloInCxt                               */
/*                                                                           */
/* Description  : Checks whether this router id is present as a neighbor id  */
/*                in the received hello packet                               */
/*                                                                           */
/* Input        : rtrId      : Router Id.                                    */
/*                pHelloPkt  : Pointer to hello packet in which the          */
/*                             check has to be performed.                    */
/*                u2PktLen   : Length of Hello packet.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE if found                                         */
/*                OSIX_FALSE Otherwise.                                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT1
V3HpCheckRtrInRcvdHelloInCxt (tV3OsRouterId rtrId, UINT1 *pHelloPkt,
                              UINT2 u2PktLen)
{
    tV3OsRouterId       nbrId;
    UINT2               u2NbrCount = 0;
    UINT1               u1Found = OSIX_FALSE;
    UINT4               u4Offset = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3HpCheckRtrInRcvdHelloInCxt\n");

    u2NbrCount
        = (UINT2) ((u2PktLen -
                    (OSPFV3_HEADER_SIZE + OSPFV3_HELLO_FIXED_PORTION_SIZE)) /
                   sizeof (tV3OsRouterId));

    while (u2NbrCount--)
    {
        OSPFV3_BUFFER_EXTRACT_STRING (pHelloPkt, nbrId,
                                      (OSPFV3_HP_NBR_ID_OFFSET +
                                       (OSPFV3_RTR_ID_LEN * u4Offset)),
                                      OSPFV3_RTR_ID_LEN);
        if (V3UtilRtrIdComp (rtrId, nbrId) == OSPFV3_EQUAL)
        {
            u1Found = OSIX_TRUE;
            break;
        }
        u4Offset++;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3HpCheckRtrInRcvdHelloInCxt\n");
    return u1Found;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpRcvHello                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.5                          */
/*                This procedure processes the received hello packet. The    */
/*                received packet is associated with a neighbor and its      */
/*                contents are processed and the neighbor state machine is   */
/*                invoked with the appropriate event.                        */
/*                                                                           */
/* Input        : pHelloPkt          : the received hello pkt                */
/*                u2PktLen           : length of the pkt                     */
/*                pInterface         : the receiving interface               */
/*                pSrcIp6Addr        : the source ipv6 addr from IPv6 header */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if packet processed successfully                  */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3HpRcvHello (UINT1 *pHelloPkt, UINT2 u2PktLen,
              tV3OsInterface * pInterface, tIp6Addr * pSrcIp6Addr)
{
    tV3OsRouterId       prevDr;
    tV3OsRouterId       prevBdr;
    tV3OsRouterId       headerRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsNeighbor      *pNeighbour = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsInterface     *pSbyInterface = NULL;
    tV3OsInterface     *pDownInterface = NULL;
    UINT4               u4InterfaceId = 0;
    UINT4               u4IfMetric = 0;
    /* Interface id to be stored
     * for hello sync up
     */
    UINT2               u2OldChkSum = 0;    /* Check sum of the hello pkt
                                             * stored in the DB
                                             */
    UINT2               u2NewChkSum = 0;    /* Check sum of the hello packet
                                             * received
                                             */
    INT4                i4IsmSchedFlag = OSPFV3_ISM_NOT_SCHEDULED;
    UINT1               u1OldState = OSPFV3_NBRS_DOWN;
    tV3HelloStaticPortion hello;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3HpRcvHello\n");

    if (pInterface->u1IsmState >= OSPFV3_MAX_IF_STATE)
    {
        gu4V3HpRcvHelloFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_RTR_ID_COPY (prevDr, OSPFV3_GET_DR (pInterface));
    OSPFV3_RTR_ID_COPY (prevBdr, OSPFV3_GET_BDR (pInterface));

    OSPFV3_BUFFER_GET_STRING (pHelloPkt, headerRtrId,
                              OSPFV3_RTR_ID_OFFSET, OSPFV3_RTR_ID_LEN);

    OSPFV3_TRC4 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "HP Received. Interface %s.%d ISM State: %s From Nbr: %x\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId,
                 gau1Os3DbgIfState[pInterface->u1IsmState],
                 OSPFV3_BUFFER_DWFROMPDU (headerRtrId));

    OSPFV3_BUFFER_EXTRACT_2_BYTE (pHelloPkt, OSPFV3_HP_HELLO_INTERVAL_OFFSET,
                                  hello.u2HelloInterval);
    OSPFV3_BUFFER_GET_STRING (pHelloPkt, hello.options,
                              OSPFV3_HP_OPTIONS_OFFSET, OSPFV3_OPTION_LEN);
    OSPFV3_BUFFER_EXTRACT_1_BYTE (pHelloPkt, OSPFV3_HP_RTR_PRI_OFFSET,
                                  hello.u1RtrPriority);
    OSPFV3_BUFFER_EXTRACT_2_BYTE (pHelloPkt, OSPFV3_HP_RTR_DEAD_INTERVAL_OFFSET,
                                  hello.u2RtrDeadInterval);
    OSPFV3_BUFFER_EXTRACT_STRING (pHelloPkt, hello.desgRtr,
                                  OSPFV3_HP_DSG_RTR_OFFSET, OSPFV3_RTR_ID_LEN);
    OSPFV3_BUFFER_EXTRACT_STRING (pHelloPkt, hello.backupDesgRtr,
                                  OSPFV3_HP_BK_DSG_RTR_OFFSET,
                                  OSPFV3_RTR_ID_LEN);
    OSPFV3_BUFFER_EXTRACT_4_BYTE (pHelloPkt, OSPFV3_HP_INTERFACE_ID_OFFSET,
                                  hello.u4InterfaceId);

    if (pInterface->u1IsmState == OSPFV3_IFS_STANDBY)
    {
        if (V3UtilRtrIdComp (headerRtrId,
                             pInterface->pArea->pV3OspfCxt->rtrId)
            != OSPFV3_EQUAL)
        {                        /* Standby Interfaces over a link should process  only Hello 
                                   pkts with Rtr Id same as theirs */

            OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }

    }

    if (V3UtilRtrIdComp (headerRtrId,
                         pInterface->pArea->pV3OspfCxt->rtrId) != OSPFV3_EQUAL)
    {
        if (pInterface->u2HelloInterval != hello.u2HelloInterval)
        {
            OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);

            OSPFV3_TRC4 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP Discarded. Reason : Hello Interval Mismatch If %s If Hello Intv %d"
                         " Src IPv6 Addr %s HP Hello Intv %d\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         pInterface->u2HelloInterval,
                         Ip6PrintAddr (pSrcIp6Addr), hello.u2HelloInterval);
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }

        /* Check for "E" Bit option */
        if (OSPFV3_E_BIT_OPTION_MISMATCH (pInterface, hello.options))
        {
            OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);

            OSPFV3_TRC6 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP Discarded. Reason : E-bit Mismatch If %s Stub Area Status %d"
                         " Src IPv6 Addr %s HP Options %x%x%x\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         pInterface->pArea->u4AreaType,
                         Ip6PrintAddr (pSrcIp6Addr),
                         hello.options[OSPFV3_OPT_BYTE_ONE],
                         hello.options[OSPFV3_OPT_BYTE_TWO],
                         hello.options[OSPFV3_OPT_BYTE_THREE]);
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }

        /* Check for "N" Bit option */
        if (OSPFV3_N_BIT_OPTION_MISMATCH (pInterface, hello.options))
        {
            OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);

            OSPFV3_TRC6 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP Discarded. Reason : N-bit Mismatch If %s Area Type %d"
                         " Src IPv6 Addr %s HP Options %x%x%x\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         pInterface->pArea->u4AreaType,
                         Ip6PrintAddr (pSrcIp6Addr),
                         hello.options[OSPFV3_OPT_BYTE_ONE],
                         hello.options[OSPFV3_OPT_BYTE_TWO],
                         hello.options[OSPFV3_OPT_BYTE_THREE]);
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }

        if (pInterface->u2RtrDeadInterval != hello.u2RtrDeadInterval)
        {
            OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);

            OSPFV3_TRC4 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "HP Discarded. Reason : Rtr Dead Interval Mismatch If %s "
                         "If Rtr Dead Intv %d Src IPv6 Addr %s"
                         " HP Rtr Dead Intv %d\n",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                         (pInterface->u2RtrDeadInterval),
                         Ip6PrintAddr (pSrcIp6Addr), (hello.u2RtrDeadInterval));
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }
    }

    if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        pNbr = V3HpSearchNbmaNbrLst (pSrcIp6Addr, pInterface);
        if (pNbr == NULL)
        {
            if ((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                 OSPFV3_GR_RESTART)
                && (pInterface->pArea->pV3OspfCxt->u1RestartExitReason ==
                    OSPFV3_RESTART_INPROGRESS)
                && (V3HpCheckRtrInRcvdHelloInCxt
                    (pInterface->pArea->pV3OspfCxt->rtrId, pHelloPkt,
                     u2PktLen) != OSIX_TRUE))
            {
                /* Exit GR with exit reason as TOPOLOGY change */
                O3GrExitGracefulRestartInCxt (pInterface->pArea->pV3OspfCxt,
                                              OSPFV3_RESTART_TOP_CHG);
            }
            OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "No NBMA Neighbor Configured for Address %s\n",
                         Ip6PrintAddr (pSrcIp6Addr));
            gu4V3HpRcvHelloFail++;
            return OSIX_FAILURE;
        }
        else
        {
            pNbr->u4NbrInterfaceId = hello.u4InterfaceId;
        }
    }
    else
    {

        pNbr = V3HpSearchNbrLst (&headerRtrId, hello.u4InterfaceId, pInterface);
        if (pNbr == NULL)
        {
            if ((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                 OSPFV3_GR_RESTART)
                && (pInterface->pArea->pV3OspfCxt->u1RestartExitReason ==
                    OSPFV3_RESTART_INPROGRESS)
                && (V3HpCheckRtrInRcvdHelloInCxt
                    (pInterface->pArea->pV3OspfCxt->rtrId,
                     pHelloPkt, u2PktLen) != OSIX_TRUE))
            {
                /* Exit GR with exit reason as TOPOLOGY change */
                O3GrExitGracefulRestartInCxt (pInterface->pArea->pV3OspfCxt,
                                              OSPFV3_RESTART_TOP_CHG);
                gu4V3HpRcvHelloFail++;
                return OSIX_FAILURE;
            }
            if (V3UtilRtrIdComp (headerRtrId,
                                 pInterface->pArea->pV3OspfCxt->rtrId)
                == OSPFV3_EQUAL)
            {                    /*When multiple interfaces on a link is attached */

                pSbyInterface =
                    V3IfGetPtrtoInterface (pInterface->pArea,
                                           hello.u4InterfaceId);

                if (pSbyInterface == NULL)
                {
                    OSPFV3_TRC (OSPFV3_HP_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "Critical error - pInterface is NULL\n");
                    gu4V3HpRcvHelloFail++;
                    return OSIX_FAILURE;

                }

                if ((pNbr = V3NbrCreate (pSrcIp6Addr, &headerRtrId,
                                         pInterface,
                                         OSPFV3_DISCOVERED_SBYIF)) != NULL)
                {

                    pNbr->u4NbrInterfaceId = hello.u4InterfaceId;
                    pNbr->u4NbrIfMetric = pSbyInterface->u4IfMetric;
                    pInterface->u1SdbyActCount++;
                    OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);
                    OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr,
                                        hello.backupDesgRtr);
                    if (OSPFV3_IS_AT_BIT_SET
                        (hello.options[OSPFV3_OPT_BYTE_TWO]))
                    {
                        pNbr->bIsABitSet = OSPFV3_ONE;
                    }
                    V3NbrAddToSortNbrLst (pNbr);
                    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);
                    return OSIX_SUCCESS;
                }
                else
                {
                    if ((pInterface->u1SdbyActCount == OSPFV3_MAX_IF_OVER_LINK))
                    {
                        if (pInterface->u4MultActiveIfId ==
                            pInterface->u4InterfaceId)
                        {
                            pDownInterface =
                                V3IfGetPtrtoInterface (pInterface->pArea,
                                                       hello.u4InterfaceId);
                            if (pDownInterface != NULL)
                            {
                                OSPFV3_GENERATE_IF_EVENT (pDownInterface,
                                                          OSPFV3_IFE_DOWN);

                            }
                        }
                        return OSIX_SUCCESS;
                    }
                    else
                    {
                        OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                                    "Could Not Associate With Neighbor\n");
                        gu4V3HpRcvHelloFail++;
                        return OSIX_FAILURE;
                    }
                }

            }
            else
            {
                if ((pNbr = V3NbrCreate (pSrcIp6Addr, &headerRtrId,
                                         pInterface, OSPFV3_DISCOVERED_NBR))
                    != NULL)
                {
                    V3NbrProcessPriorityChange (pNbr, hello.u1RtrPriority);
                    pNbr->u4NbrIfMetric = 0;
                    pNbr->u4NbrInterfaceId = hello.u4InterfaceId;
                    OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);
                    OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr,
                                        hello.backupDesgRtr);
                    if (OSPFV3_IS_AT_BIT_SET
                        (hello.options[OSPFV3_OPT_BYTE_TWO]))
                    {
                        pNbr->bIsABitSet = OSPFV3_ONE;
                    }

                }
                else
                {
                    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "Could Not Associate With Neighbor\n");
                    gu4V3HpRcvHelloFail++;
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            pNbr->u4NbrInterfaceId = hello.u4InterfaceId;

            if (V3UtilRtrIdComp (headerRtrId,
                                 pInterface->pArea->pV3OspfCxt->rtrId)
                == OSPFV3_EQUAL)
            {
                if (V3IfIsMetricChange (pNbr, &u4IfMetric) == OSIX_TRUE)
                {
                    /* Metric of the Active/Standby Interface 
                     * over the link gets changed */
                    pNbr->u4NbrIfMetric = u4IfMetric;

                    /* Trigger Active/Standby Interface 
                     * election over the link */
                    if (pInterface->u1ActiveDetectTmrFlag ==
                        OSPFV3_MULTIF_ACTIVE_TMR_ON)
                    {
                        TmrStopTimer (gV3OsRtr.timerLstId,
                                      &((&
                                         (pInterface->
                                          MultIfActiveDetectTimer))->
                                        timerNode));

                    }

                    pInterface->u1ActiveDetectTmrFlag =
                        OSPFV3_MULTIF_ACTIVE_TMR_OFF;
                    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UP);
                    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);
                    return OSIX_SUCCESS;

                }
                else
                {
                    OSPFV3_INC_DISCARD_HELLO_CNT (pInterface);
                    gu4V3HpRcvHelloFail++;
                    return OSIX_FAILURE;

                }

            }
        }
    }

    if (gV3OsRtr.ospfRedInfo.u4RmState != OSPFV3_RED_INIT)
    {
        /* If this is a vitual interface, then pass the actual
         * interface index stored in the virtual links's outifindex
         */
        if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
        {
            u4InterfaceId = pInterface->u4OutIfIndex;
        }
        else
        {
            u4InterfaceId = pInterface->u4InterfaceId;
        }

        /* If the Hello sync state in the neighbor is false, then the
         * last hello sync message from this nbr failed. Therefore, send
         * the packet even if there is no change in the hello message
         */
        if ((u2PktLen == pNbr->lastHelloInfo.u2Len) ||
            (pNbr->u1HelloSyncState == OSIX_FALSE))
        {
            /* Get the checksum from the hello packet store in the DB
             * and from the hello packet received from the neighbor
             * If the check sum does not match, then the contents of
             * the hello packet has been changed and the new hello
             * packet is stored in the DB
             */
            OSPFV3_BUFFER_EXTRACT_2_BYTE (pHelloPkt, OSPFV3_CHKSUM_OFFSET,
                                          u2NewChkSum);
            OSPFV3_BUFFER_EXTRACT_2_BYTE (pNbr->lastHelloInfo.au1Pkt,
                                          OSPFV3_CHKSUM_OFFSET, u2OldChkSum);

            if (u2NewChkSum != u2OldChkSum)
            {
                O3RedAdjAddLastHelloDetails (pNbr, u4InterfaceId,
                                             pSrcIp6Addr, pHelloPkt, u2PktLen);
            }
        }
        else
        {
            O3RedAdjAddLastHelloDetails (pNbr, u4InterfaceId,
                                         pSrcIp6Addr, pHelloPkt, u2PktLen);
        }
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pInterface))
    {
        /* suppress hello */
        if (OSPFV3_OPT_ISSET (hello.options, OSPFV3_DC_BIT_MASK) &&
            (V3HpCheckRtrInRcvdHelloInCxt (pInterface->pArea->pV3OspfCxt->rtrId,
                                           pHelloPkt, u2PktLen) == OSIX_TRUE))
        {
            pNbr->bHelloSuppression = OSIX_TRUE;
        }
        else
        {
            /* check for the validity of discovered end point */
            if (!OSPFV3_OPT_ISSET (hello.options, OSPFV3_DC_BIT_MASK) &&
                (V3HpCheckRtrInRcvdHelloInCxt
                 (pInterface->pArea->pV3OspfCxt->rtrId, pHelloPkt,
                  u2PktLen) == OSIX_TRUE))
            {
                pNbr->bHelloSuppression = OSIX_FALSE;
                if (OSPFV3_IS_DISCOVERED_DC_ENDPOINT_IFACE (pInterface))
                {
                    pInterface->bDcEndpt = OSIX_FALSE;
                    OSPFV3_OPT_CLEAR_ALL (pInterface->ifOptions);
                    V3TmrDeleteTimer (&(pInterface->pollTimer));
                }
            }
        }
    }
    else
    {
        /* making other end of the dc link as discovered end point */
        if (((OSPFV3_OPT_ISSET (hello.options, OSPFV3_DC_BIT_MASK)) &&
             ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
              (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL) ||
              (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))) &&
            (OSPFV3_IS_SUPPORTING_DC_EXTENSIONS
             (pInterface->pArea->pV3OspfCxt)))
        {
            pInterface->bDcEndpt = OSIX_TRUE;
            OSPFV3_OPT_SET (pInterface->ifOptions, OSPFV3_DC_BIT_MASK);
            pInterface->u1ConfStatus = OSPFV3_DISCOVERED_ENDPOINT;
            V3TmrRestartTimer (&(pInterface->pollTimer), OSPFV3_POLL_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC *
                               (pInterface->u4PollInterval));
        }
    }

    if ((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ||
        (pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        OSPFV3_RTR_ID_COPY (pNbr->nbrRtrId, headerRtrId);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_PTOP)
    {
        OSPFV3_IP6_ADDR_COPY (pNbr->nbrIpv6Addr, *pSrcIp6Addr);
    }

    u1OldState = pNbr->u1NsmState;

    OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_HELLO_RCVD);

    if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) &&
        (pInterface->u1RtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY) &&
        (pNbr->u1NbrRtrPriority > OSPFV3_INELIGIBLE_RTR_PRIORITY) &&
        (V3UtilRtrIdComp (pNbr->nbrRtrId,
                          OSPFV3_GET_DR (pInterface)) != OSPFV3_EQUAL) &&
        (V3UtilRtrIdComp (pNbr->nbrRtrId, OSPFV3_GET_BDR (pInterface)) !=
         OSPFV3_EQUAL))
    {

        V3HpSendHello (pInterface, pNbr, OSPFV3_INVALID_TIMER);
    }

    /* To avoid Improper Election in the case of NBMA network */
    if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) &&
        ((u1OldState == OSPFV3_NBRS_DOWN) ||
         (u1OldState == OSPFV3_NBRS_ATTEMPT)))
    {
        if ((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState
             == OSPFV3_GR_RESTART) &&
            (pInterface->pArea->pV3OspfCxt->u1RestartExitReason
             == OSPFV3_RESTART_INPROGRESS))
        {

            if (V3UtilRtrIdComp
                (pInterface->pArea->pV3OspfCxt->rtrId,
                 hello.desgRtr) == OSPFV3_EQUAL)
            {
                V3IfUpdateState (pInterface, OSPFV3_IFS_DR);
                pInterface->u4DRInterfaceId = pInterface->u4InterfaceId;
            }
            else if (V3UtilRtrIdComp
                     (pInterface->pArea->pV3OspfCxt->rtrId,
                      hello.backupDesgRtr) == OSPFV3_EQUAL)
            {
                V3IfUpdateState (pInterface, OSPFV3_IFS_BACKUP);
                pInterface->u4BDRInterfaceId = pInterface->u4InterfaceId;
            }
            else
            {
                V3IfUpdateState (pInterface, OSPFV3_IFS_DR_OTHER);
            }

            OSPFV3_RTR_ID_COPY (pInterface->desgRtr, hello.desgRtr);
            OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);

            OSPFV3_RTR_ID_COPY (pInterface->backupDesgRtr, hello.backupDesgRtr);
            OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbour =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
                if (V3UtilRtrIdComp (pNeighbour->nbrRtrId, hello.desgRtr) ==
                    OSPFV3_EQUAL)
                {
                    pInterface->u4DRInterfaceId = pNeighbour->u4NbrInterfaceId;
                }
                if (V3UtilRtrIdComp
                    (pNeighbour->nbrRtrId, hello.backupDesgRtr) == OSPFV3_EQUAL)
                {
                    pInterface->u4BDRInterfaceId = pNeighbour->u4NbrInterfaceId;
                }
            }

            OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_2WAY_RCVD);
        }
        else
        {
            if (V3HpCheckRtrInRcvdHelloInCxt
                (pInterface->pArea->pV3OspfCxt->rtrId, pHelloPkt,
                 u2PktLen) == OSIX_TRUE)
            {
                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_2WAY_RCVD);
            }
            else
            {
                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_1WAY_RCVD);
            }
        }
        return OSIX_SUCCESS;
    }

    /* check if this router is listed in the received hello */
    if (V3HpCheckRtrInRcvdHelloInCxt (pInterface->pArea->pV3OspfCxt->rtrId,
                                      pHelloPkt, u2PktLen) == OSIX_TRUE)
    {
        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_2WAY_RCVD);
    }
    else
    {
        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_1WAY_RCVD);
        return OSIX_SUCCESS;
    }

    /* Added to implement the condition specified in RFC 2328 Section 7.3
     * If the n/w already has a DR & a BDR router elected , the new i/f
     * coming up should accept these as the DR & the BDR routers irrespective
     * of the proiority */

    i4IsmSchedFlag = OSPFV3_ISM_NOT_SCHEDULED;

    if ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY) &&
        ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
         (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)))
    {
        OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);
        OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

        if (hello.u1RtrPriority != pNbr->u1NbrRtrPriority)
        {
            V3NbrProcessPriorityChange (pNbr, hello.u1RtrPriority);
        }

        return OSIX_SUCCESS;
    }

    if (((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
          OSPFV3_GR_RESTART)
         && (pInterface->pArea->pV3OspfCxt->u1RestartExitReason ==
             OSPFV3_RESTART_INPROGRESS))
        && ((pInterface->u1NetworkType == OSPFV3_IF_NBMA)
            || (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)))
    {
        if (V3UtilRtrIdComp
            (pInterface->pArea->pV3OspfCxt->rtrId,
             hello.desgRtr) == OSPFV3_EQUAL)
        {
            V3IfUpdateState (pInterface, OSPFV3_IFS_DR);
            pInterface->u4DRInterfaceId = pInterface->u4InterfaceId;
        }
        else if (V3UtilRtrIdComp
                 (pInterface->pArea->pV3OspfCxt->rtrId,
                  hello.backupDesgRtr) == OSPFV3_EQUAL)
        {
            V3IfUpdateState (pInterface, OSPFV3_IFS_BACKUP);
            pInterface->u4BDRInterfaceId = pInterface->u4InterfaceId;
        }
        else
        {
            V3IfUpdateState (pInterface, OSPFV3_IFS_DR_OTHER);
        }

        OSPFV3_RTR_ID_COPY (pInterface->desgRtr, hello.desgRtr);
        OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);

        OSPFV3_RTR_ID_COPY (pInterface->backupDesgRtr, hello.backupDesgRtr);
        OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNeighbour =
                OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
            if (V3UtilRtrIdComp (pNeighbour->nbrRtrId, hello.desgRtr) ==
                OSPFV3_EQUAL)
            {
                pInterface->u4DRInterfaceId = pNeighbour->u4NbrInterfaceId;
            }
            if (V3UtilRtrIdComp
                (pNeighbour->nbrRtrId, hello.backupDesgRtr) == OSPFV3_EQUAL)
            {
                pInterface->u4BDRInterfaceId = pNeighbour->u4NbrInterfaceId;
            }
        }

        return OSIX_SUCCESS;
    }

    if (hello.u1RtrPriority != pNbr->u1NbrRtrPriority)
    {
        if ((i4IsmSchedFlag = V3NbrProcessPriorityChange (pNbr,
                                                          hello.
                                                          u1RtrPriority)) ==
            OSPFV3_ISM_SCHEDULED)
        {
            V3IsmSchedule (pNbr->pInterface, OSPFV3_IFE_NBR_CHANGE);
        }
    }

    if ((pInterface->u1IsmState == OSPFV3_IFS_WAITING) &&
        (V3UtilRtrIdComp (pNbr->nbrRtrId, hello.desgRtr) == OSPFV3_EQUAL) &&
        (OSPFV3_IS_NULL_RTR_ID (hello.backupDesgRtr)))
    {
        if (i4IsmSchedFlag == OSPFV3_ISM_NOT_SCHEDULED)
        {
            i4IsmSchedFlag = OSPFV3_ISM_SCHEDULED;
            V3IsmSchedule (pInterface, OSPFV3_IFE_BACKUP_SEEN);
        }
    }
    else if (((V3UtilRtrIdComp (pNbr->nbrRtrId,
                                pNbr->desgRtr) != OSPFV3_EQUAL) &&
              (V3UtilRtrIdComp (pNbr->nbrRtrId,
                                hello.desgRtr) == OSPFV3_EQUAL)) ||
             ((V3UtilRtrIdComp (pNbr->nbrRtrId,
                                pNbr->desgRtr) == OSPFV3_EQUAL) &&
              (V3UtilRtrIdComp (pNbr->nbrRtrId, hello.desgRtr) !=
               OSPFV3_EQUAL)))
    {
        if (i4IsmSchedFlag == OSPFV3_ISM_NOT_SCHEDULED)
        {
            OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);
            OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);
            pNbr->u4NbrInterfaceId = hello.u4InterfaceId;

            i4IsmSchedFlag = OSPFV3_ISM_SCHEDULED;
            V3IsmSchedule (pInterface, OSPFV3_IFE_NBR_CHANGE);
        }
    }

    OSPFV3_RTR_ID_COPY (pNbr->desgRtr, hello.desgRtr);

    if ((pInterface->u1IsmState == OSPFV3_IFS_WAITING) &&
        (V3UtilRtrIdComp (pNbr->nbrRtrId, hello.backupDesgRtr) == OSPFV3_EQUAL))
    {
        if (i4IsmSchedFlag == OSPFV3_ISM_NOT_SCHEDULED)
        {
            OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            i4IsmSchedFlag = OSPFV3_ISM_SCHEDULED;
            V3IsmSchedule (pInterface, OSPFV3_IFE_BACKUP_SEEN);
        }
    }
    else if (OSPFV3_NBR_LOSE_OR_GAIN_DR_OR_BDR (pNbr, hello.backupDesgRtr))
    {
        if (i4IsmSchedFlag == OSPFV3_ISM_NOT_SCHEDULED)
        {
            OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

            i4IsmSchedFlag = OSPFV3_ISM_SCHEDULED;
            V3IsmSchedule (pInterface, OSPFV3_IFE_NBR_CHANGE);
        }
    }

    OSPFV3_RTR_ID_COPY (pNbr->backupDesgRtr, hello.backupDesgRtr);

    OSPFV3_TRC (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "HP Processing Over\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpRcvHello\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpSearchNbmaNbrLst                                       */
/*                                                                           */
/* Description  : The list of neighbor structures is searched for a matching */
/*                entry. The key for the search is rtrId.                    */
/*                                                                           */
/* Input        : pIp6SrcAddr      : the IPv6 Src addr of the nbr to be      */
/*                                   searched                                */
/*                pInterface       : the interface whose nbr list is         */
/*                                   searched                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to matching neighbour structure, if found          */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE tV3OsNeighbor *
V3HpSearchNbmaNbrLst (tIp6Addr * pIp6SrcAddr, tV3OsInterface * pInterface)
{

    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3HpSearchNbmaNbrLst\n");
    OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Search for neighbor with src Address : %s\n",
                 Ip6PrintAddr (pIp6SrcAddr));

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {

        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if ((V3UtilIp6AddrComp (pIp6SrcAddr, &pNbr->nbrIpv6Addr) ==
             OSPFV3_EQUAL) && (pInterface == pNbr->pInterface))
        {
            OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Neighbor with ID : %s found\n",
                         Ip6PrintAddr (pIp6SrcAddr));
            return (pNbr);
        }
    }

    OSPFV3_TRC1 (OSPFV3_HP_TRC | OSPFV3_ADJACENCY_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Neighbor with ID %s Not found\n", Ip6PrintAddr (pIp6SrcAddr));
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpSearchNbmaNbrLst\n");
    return (NULL);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpElectActiveOnLink                                      */
/*                                                                           */
/* Description  : Reference : RFC-5340 section 4.9                           */
/*                Elects an active interface from the list of eligible       */
/*                Nbr's which are attached to the same interface.            */
/*                                                                           */
/* Input        : pInterface -  the interface whose nbr list is              */
/*                              searched and compares the metric             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to matching neighbour structure, if found          */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3HpElectActiveOnLink (tV3OsInterface * pInterface)
{
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsNeighbor      *pSbyNbr = NULL;
    tV3OsNeighbor      *pActiveNbr = NULL;
    tV3OsInterface     *pActiveInterface = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT4               u4MultIfActiveId = 0;
    UINT4               u4PreMultIfActiveId = 0;
    UINT4               u4MultIfActiveMetric = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3HpElectActiveOnLink\n");

    /* STEP 1 - Activate the ACTIVATE DETECT TIMER  if it is OFF */

    if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_OFF)
    {                            /* Start the Active Detec timer once */

        V3TmrRestartTimer (&(pInterface->MultIfActiveDetectTimer),
                           OSPFV3_MULTIF_ACTIVE_DETECT_TIMER,
                           (OSPFV3_NO_OF_TICKS_PER_SEC *
                            (pInterface->u2MultIfActiveDetectInterval)));
        pInterface->u1ActiveDetectTmrFlag = OSPFV3_MULTIF_ACTIVE_TMR_ON;

    }

    u4MultIfActiveId = pInterface->u4InterfaceId;
    u4MultIfActiveMetric = pInterface->u4IfMetric;
    pActiveInterface = pInterface;
    u4PreMultIfActiveId = pInterface->u4MultActiveIfId;

    /* STEP 2 - Find out the ACTIVE INTERFACE ID */

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
    {
        pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);

        if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
        {
            if (pSbyNbr->u4NbrIfMetric < u4MultIfActiveMetric)
            {                    /* If Metric is less then update the Active If Id */

                u4MultIfActiveMetric = pSbyNbr->u4NbrIfMetric;
                u4MultIfActiveId = pSbyNbr->u4NbrInterfaceId;
                pActiveNbr = pSbyNbr;
            }
            else if (pSbyNbr->u4NbrIfMetric == u4MultIfActiveMetric)
            {                    /* If Metric is equal choose the Nbr whose If id is large */

                if (pSbyNbr->u4NbrInterfaceId > u4MultIfActiveId)
                {
                    u4MultIfActiveMetric = pSbyNbr->u4NbrIfMetric;
                    u4MultIfActiveId = pSbyNbr->u4NbrInterfaceId;
                    pActiveNbr = pSbyNbr;
                }
            }

        }

    }

    /* STEP 3 - Fetch the Interface Pointer of ACTIVE INTERFACE */

    pNode = NULL;
    if ((pActiveNbr != NULL) && (u4MultIfActiveId != pInterface->u4InterfaceId))
    {
        pActiveInterface =
            V3IfGetPtrtoInterface (pInterface->pArea,
                                   pActiveNbr->u4NbrInterfaceId);
        if (pActiveInterface == NULL)
        {
            return;
        }

    }

    /* STEP 4 - UPDATE the Nbr Status depending on STEP 2's output
     * Provided, After 20 secs (ie. Expiry of Active Detect Timer*/

    pNode = NULL;
    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
    {
        pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);

        if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
        {
            pSbyNbr->pActiveInterface = pActiveInterface;

            if ((pSbyNbr->u4NbrInterfaceId != u4MultIfActiveId) &&
                (pInterface->u1ActiveDetectTmrFlag ==
                 OSPFV3_MULTIF_ACTIVE_TMR_STOP))
            {                    /*Update the status of each Nbr in the list */

                pSbyNbr->u1NbrIfStatus = OSPFV3_STANDBY_ON_LINK;
            }
        }
    }

    if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_STOP)
    {
        pInterface->u4MultActiveIfId = u4MultIfActiveId;

        if (pActiveNbr != NULL)
        {                        /* Update the status of the active Nbr */

            pActiveNbr->u1NbrIfStatus = OSPFV3_ACTIVE_ON_LINK;
        }

    }

    /* STEP 5 - Necessary Actions - 
     * Condition 1 - Lost the Active Status - Elected as Standby */

    if ((u4PreMultIfActiveId == pInterface->u4InterfaceId) &&
        (u4PreMultIfActiveId != u4MultIfActiveId))
    {                            /* New Active takes over */

        if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_STOP)
        {
            /*kill all nbrs if the interface becomes standby */
            V3IfUpdateState (pInterface, OSPFV3_IFS_DOWN);

            V3IfCleanup (pInterface, OSPFV3_IF_CLEANUP_DELETE);

            /* Update the Status os STANDBY only after the expiry
             * of Active Detect Timer  - 20 sec*/

            V3IfUpdateState (pInterface, OSPFV3_IFS_STANDBY);
        }

        /* flush the Link LSA's of the standby interfaces */

        if (pInterface->u1IsmState != OSPFV3_IFS_STANDBY)
        {
            pNode = NULL;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
            {
                pSbyNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);

                if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
                {
                    OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId,
                                           pSbyNbr->u4NbrInterfaceId);

                }
                else
                {
                    continue;
                }

                if ((pLsaInfo =
                     V3LsuSearchDatabase (OSPFV3_LINK_LSA, &(tmpLinkStateId),
                                          &(pInterface->pArea->pV3OspfCxt->
                                            rtrId), pInterface, NULL)) != NULL)
                {

                    V3AgdFlushOut (pLsaInfo);
                }
            }

        }

    }

    /* Condition 2 - Elected as the Active Over the link */

    if (u4MultIfActiveId == pInterface->u4InterfaceId)
    {                            /* Elected as Active interface over the link */

        /* Enable Hello - if it is in STANDBY state , inorder to
         * reveal its presence to the new Interface */

        if ((pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_ON)
            && (pInterface->u1IsmState == OSPFV3_IFS_STANDBY))
        {
            V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);

        }

        /* When the Elected active was not Previously Active , Generate
         * Link LSA for the standby interfaces */

        if (u4PreMultIfActiveId != u4MultIfActiveId)
        {                        /* Generate the Link LSA's for the standby 
                                 * interfaces over the link */

            if (pInterface->u1ActiveDetectTmrFlag
                == OSPFV3_MULTIF_ACTIVE_TMR_STOP)
            {
                V3HpGenerateStandbyLinkLsa (pInterface);
            }

            /* Triggering Event to form new OSPF normal Nbrs */
            if (pInterface->u1IsmState != OSPFV3_IFS_WAITING)
            {
                V3IsmSchedule (pInterface, OSPFV3_IFE_NBR_CHANGE);
            }
        }
        /* Condition 4 - Re-elected as ACTIVE */
        else if ((u4MultIfActiveId & pInterface->u4InterfaceId)
                 == u4PreMultIfActiveId)
        {                        /* if the interface is re-elected as the
                                   active interface over the link */

            if (pInterface->u1ActiveDetectTmrFlag ==
                OSPFV3_MULTIF_ACTIVE_TMR_STOP)
            {                    /* Generate Link LSA's for the standby interfaces of the link */

                V3HpGenerateStandbyLinkLsa (pInterface);

            }
        }

    }

    /* Condition 3 - Re-elected as STANDBY */

    if ((u4MultIfActiveId != pInterface->u4InterfaceId) &&
        (u4PreMultIfActiveId != pInterface->u4InterfaceId))
    {                            /* if the interface is elected as standby once again */

        if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_ON)
        {                        /* send hello for the new interface to find out the 
                                 *  presence of standby interface */
            V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);
        }
        if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_STOP)
        {                        /* disable the interface timers once the 
                                   Active Detect timer expires */

            V3IsmDisableIfTimers (pInterface);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpElectActiveOnLink\n");
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3HpGenerateStandbyLinkLsa                                 */
/*                                                                           */
/* Description  : This function scans the Nbr lst of pInterface              */
/*                and calls the generate lsa ,inorder to generate Link LSA   */
/*                for the Standby Nbrs.                                      */
/*                                                                           */
/* Input        : pInterface -  Pointer to the Active Interface over         */
/*                              the Link                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3HpGenerateStandbyLinkLsa (tV3OsInterface * pInterface)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsNeighbor      *pSbyNbr = NULL;
    tV3OsLinkStateId    tmpLinkStateId;

    if (pInterface == NULL)
    {
        return;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3HpGenerateStandbyLinkLsa\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
    {
        pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);
        if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
        {
            OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, pSbyNbr->u4NbrInterfaceId);
            V3GenerateLsa (pInterface->pArea, OSPFV3_STANDBY_LINK_LSA,
                           &tmpLinkStateId, (UINT1 *) pInterface, OSIX_FALSE);
        }

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3HpGenerateStandbyLinkLsa\n");
    return;

}

/*------------------------------------------------------------------------*/
/*                        End of the file o3hp.c                          */
/*------------------------------------------------------------------------*/
