/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3extrt.c,v 1.14 2017/12/26 13:34:26 siva Exp $
 * 
 *
 * Description:This file contains procedures related to the 
 *             external routes.
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/* Function        : V3ExtrtAddInCxt                                         */
/*                                                                           */
/* Description     : This procedure creates the AS external route entry,     */
/*                   initializes it and adds the specified AS external route */
/*                   to the router structure                                 */
/*                                                                           */
/* Input           : pV3OspfCxt : Context pointer                            */
/*                   pRtmInfo : Pointer to external route information.       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to Added External Route Entry.                  */
/*****************************************************************************/
PUBLIC tV3OsExtRoute *
V3ExtrtAddInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pRtmInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsExtRoute      *pExtRouteOld = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ExtrtAdd\n");

    pExtRouteOld = V3ExtrtFindRouteInCxt (pV3OspfCxt, &pRtmInfo->ip6Prefix,
                                          pRtmInfo->u1PrefixLength);

    if (pExtRouteOld == NULL)
    {
        OSPFV3_EXT_ROUTE_ALLOC (&(pExtRoute));
        if (NULL == pExtRoute)
        {
            OSPFV3_TRC (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                        "EXT ROUTE Alloc Failed\n");
            return NULL;
        }
        MEMSET (pExtRoute, 0, sizeof (tV3OsExtRoute));
        pExtRoute->pAsExtAddrRange = NULL;

        MEMSET (&(pExtRoute->ip6Prefix), 0, sizeof (tIp6Addr));
        OSPFV3_IP6_ADDR_PREFIX_COPY (pExtRoute->ip6Prefix, pRtmInfo->ip6Prefix,
                                     pRtmInfo->u1PrefixLength);

        pExtRoute->u1PrefixLength = pRtmInfo->u1PrefixLength;

        if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
            (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
        {
            O3GrSetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pExtRoute,
                                      OSPFV3_AS_EXT_LSA);
        }
        else
        {
            V3SetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pExtRoute,
                                    OSPFV3_AS_EXT_LSA);
        }

        TMO_SLL_Init_Node (&(pExtRoute->nextExtRouteInRange));

        /* Adding the external route to Trie data structure */
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtmInfo->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.pRoot = pV3OspfCxt->pExtRtRoot;
        inParams.i1AppId = OSPFV3_RT_ID;
        inParams.u1PrefixLen = pRtmInfo->u1PrefixLength;
        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        if (TrieAdd (&inParams, (VOID *) pExtRoute, &outParams) != TRIE_SUCCESS)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                        "Failed to add the route entry to TRIE\n");
            OSPFV3_EXT_ROUTE_FREE (pExtRoute);
            return NULL;
        }
    }
    else
    {
        pExtRoute = pExtRouteOld;
    }

    pExtRoute->u2SrcProto = pRtmInfo->u2SrcProto;
    pExtRoute->metric.u4Metric = pRtmInfo->metric.u4Metric;
    pExtRoute->metric.u4MetricType = pRtmInfo->metric.u4MetricType;
    pExtRoute->rowStatus = pRtmInfo->rowStatus;
    pExtRoute->u4ExtTag = pRtmInfo->u4ExtTag;
    OSPFV3_IP6_ADDR_COPY (pExtRoute->fwdAddr, pRtmInfo->fwdAddr);
    pExtRoute->u4FwdIfIndex = pRtmInfo->u4FwdIfIndex;
    pExtRoute->u1Level = pRtmInfo->u1Level;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId, "EXIT: V3ExtrtAdd\n");
    return pExtRoute;
}

/*****************************************************************************/
/* Function        : V3ExtrtParamChangeInCxt                                 */
/*                                                                           */
/* Description     : This routine generates an LSA if any of the exterior    */
/*                   route parameters change.                                */
/*                                                                           */
/* Input           : pV3OspfCxt:   ptr to the Context                        */
/*                   pExtRoute :   ptr to the external route structure       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC VOID
V3ExtrtParamChangeInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ExtrtParamChangeInCxt\n");
    V3RagDelRouteFromAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
    V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ExtrtParamChangeInCxt\n");

}

/*****************************************************************************/
/* Function        : V3ExtrtLsaPolicyHandlerInCxt                            */
/*                                                                           */
/* Description     : This function decides whether Type 5 LSAs need to be    */
/*                   generated/deleted.                                      */
/*                                                                           */
/* Input           : pV3OspfCxt  -  Context pointer                          */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC VOID
V3ExtrtLsaPolicyHandlerInCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4FlagVal = OSIX_FALSE;
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pTempDbNode = NULL;
    tV3OsLinkStateId    linkStateId;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpLstNode = NULL;
    UINT4               u4HashIndex = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:V3ExtrtLsaPolicyHandlerInCxt\n");

    u4FlagVal = pV3OspfCxt->bGenType5Flag;
    pV3OspfCxt->bGenType5Flag = OSIX_FALSE;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if ((pArea->u4ActIntCount > 0) &&
            (pArea->u4AreaType == OSPFV3_NORMAL_AREA))
        {
            pV3OspfCxt->bGenType5Flag = OSIX_TRUE;
            break;
        }
    }

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        return;
    }

    if ((u4FlagVal == OSIX_FALSE) && (pV3OspfCxt->bGenType5Flag == OSIX_TRUE))
    {
        V3GenerateLsasForExtRoutesInCxt (pV3OspfCxt);
    }
    else if ((u4FlagVal == OSIX_TRUE)
             && (pV3OspfCxt->bGenType5Flag == OSIX_FALSE))
    {
        TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashIndex)
        {
            OSPFV3_DYNM_HASH_BUCKET_SCAN
                (pV3OspfCxt->pExtLsaHashTable, u4HashIndex,
                 pDbHashNode, pTempDbNode, tV3OsDbNode *)
            {
                OSPFV3_DYNM_SLL_SCAN (&(pDbHashNode->lsaLst), pLstNode,
                                      pTmpLstNode, tTMO_SLL_NODE *)
                {
                    pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                    nextLsaInfo, pLstNode);
                    OSPFV3_LINK_STATE_ID_COPY (&linkStateId,
                                               &pLsaInfo->lsaId.linkStateId);

                    V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);

                    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
                    {
                        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
                        {
                            pType7LsaInfo =
                                V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                                     &(linkStateId),
                                                     &(pV3OspfCxt->rtrId), NULL,
                                                     pArea);

                            if (pType7LsaInfo != NULL)
                            {
                                V3SignalLsaRegenInCxt
                                    (pV3OspfCxt,
                                     OSPFV3_SIG_NEXT_INSTANCE,
                                     (UINT1 *) pType7LsaInfo->pLsaDesc);
                            }
                        }
                    }

                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:V3ExtrtLsaPolicyHandlerInCxt\n");
}

/*****************************************************************************/
/* Function        : V3ExtrtFindRouteInCxt                                   */
/*                                                                           */
/* Description     : This function find the external route entry in the TRIE */
/*                   matching with the given prefix and prefix length.       */
/*                                                                           */
/* Input           : pV3OspfCxt   : Pointer to OSPFv3 context                */
/*                   pExtRtPrefix : Pointer to external route prefix         */
/*                   u1PrefixLength : Prefix length                          */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC tV3OsExtRoute *
V3ExtrtFindRouteInCxt (tV3OspfCxt * pV3OspfCxt,
                       tIp6Addr * pExtRtPrefix, UINT1 u1PrefixLength)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :V3ExtrtFindRouteInCxt\n");

    if (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLength) > OSPFV3_TRIE_KEY_SIZE)
    {
        return NULL;
    }
    MEMSET (&(au1Key), 0, sizeof (tIp6Addr));
    MEMCPY (&(au1Key), pExtRtPrefix,
            OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLength));
    au1Key[OSPFV3_IPV6_ADDR_LEN] = u1PrefixLength;
    inParams.Key.pKey = (UINT1 *) au1Key;
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieSearch (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
    {
        return NULL;
    }
    pExtRoute = (tV3OsExtRoute *) outParams.pAppSpecInfo;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:V3ExtrtFindRouteInCxt\n");
    return pExtRoute;
}

/*****************************************************************************/
/* Function        : V3ExtrtDeleteInCxt                                      */
/*                                                                           */
/* Description     : This routine deletes the exterior route.                */
/*                                                                           */
/* Input           : pV3OspfCxt :  ptr to the context                        */
/*                   pExtRoute  :  ptr to the external route structure       */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC VOID
V3ExtrtDeleteInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute)
{
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:V3ExtrtDeleteInCxt\n");

    if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_SHUTDOWN)
    {
        /* Route entry deletion will be taken care during GR shutdown process */
        return;
    }
    /* Delete the route entry from Trie during GR restart process */
    if (!(pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART))
    {
        V3RagDelRouteFromAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
    }

    MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
    au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
    inParams.Key.pKey = (UINT1 *) au1Key;
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    outParams.Key.pKey = NULL;
    outParams.pAppSpecInfo = NULL;

    if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "Failed to delete the route entry" "from trie\n");
    }

    /* Delete the route entry from Trie */
    OSPFV3_EXT_ROUTE_FREE (pExtRoute);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:V3ExtrtDeleteInCxt\n");
}

/*****************************************************************************/
/* Function        : V3ExtrtDeleteAllInCxt                                   */
/*                                                                           */
/* Description     : This function is to delete all external routes          */
/*                                                                           */
/* Input           : pV3OspfCxt  - Context pointer                           */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC VOID
V3ExtrtDeleteAllInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsExtRoute      *pTempExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    VOID               *pTemp = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pTempPtr = NULL;
    tV3OspfRtmNode     *pRtmNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pTmpNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:V3ExtrtDeleteAllInCxt\n");

    /* Delete all the external routes from as external aggregation list */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst), pLstNode, tTMO_SLL_NODE *)
    {
        pAsExtRange = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                           nextAsExtAddrRngInRtr, pLstNode);

        /* Deleting all the elements of the list */
        TMO_SLL_Init (&(pAsExtRange->extRtLst));
    }

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "No External routes are present in "
                    "external routing table\n");
        return;
    }
    pExtRoute = (tV3OsExtRoute *) pTempPtr;
    while (pExtRoute != NULL)
    {
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;

        if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                             &pTempPtr, (VOID **)
                             &(inParams.pLeafNode)) != TRIE_SUCCESS)
        {
            pTempExtRoute = NULL;
        }
        pTempExtRoute = (tV3OsExtRoute *) pTempPtr;
        /* Deleting from Trie */
        if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                        "Failed to delete the external route "
                        "entry from TRIE\n");
        }

        /* Deleting paths and Freeing the route entry */
        OSPFV3_EXT_ROUTE_FREE (pExtRoute);
        pExtRoute = pTempExtRoute;
    }

    /* Delete the route updations from RTM */
    OsixSemTake (OSPF3_RTM_LST_SEM_ID);

    TMO_DYN_SLL_Scan (&(gV3OsRtr.rtmRoutesLst), pNode, pTmpNode,
                      tTMO_SLL_NODE *)
    {
        pRtmNode = OSPFV3_GET_BASE_PTR (tV3OspfRtmNode, nextRtmNode, pNode);

        if (pRtmNode->u1MessageType == RTM6_REGISTRATION_ACK_MESSAGE)
        {
            if (pRtmNode->RegnAckMsg.u4ContextId == pV3OspfCxt->u4ContextId)
            {
                TMO_SLL_Delete (&(gV3OsRtr.rtmRoutesLst), pNode);
                OSPFV3_RTM_ROUTES_FREE (pRtmNode);
            }
        }
        else
        {
            if (pRtmNode->RtInfo.u4ContextId == pV3OspfCxt->u4ContextId)
            {
                TMO_SLL_Delete (&(gV3OsRtr.rtmRoutesLst), pNode);
                OSPFV3_RTM_ROUTES_FREE (pRtmNode);
            }
        }
    }

    OsixSemGive (OSPF3_RTM_LST_SEM_ID);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:V3ExtrtDeleteAllInCxt\n");
}

/*****************************************************************************/
/* Function     : V3ExtRtInitRtInCxt                                         */
/*                                                                           */
/* Description  : Initializes the external routing table.                    */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                u1ApplnId  : Application Id of the external routing table  */
/*                u4Type     : Type                                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE depending the TRIE creation   */
/*****************************************************************************/
PUBLIC INT1
V3ExtRtInitRtInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1ApplnId, UINT4 u4Type)
{
    tTrieCrtParams      extTrieCreateParam;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:V3ExtRtInitRtInCxt\n");
    extTrieCreateParam.u2KeySize = OSPFV3_TRIE_KEY_SIZE;
    extTrieCreateParam.u4Type = u4Type;
    extTrieCreateParam.AppFns = &(V3RtcTrieLibFuncs);
    extTrieCreateParam.u4NumRadixNodes = OSPFV3_MAX_REDIST_EXT_ROUTES + 1;
    extTrieCreateParam.u4NumLeafNodes = OSPFV3_MAX_REDIST_EXT_ROUTES;
    extTrieCreateParam.u4NoofRoutes = OSPFV3_MAX_REDIST_EXT_ROUTES;
    extTrieCreateParam.u1AppId = u1ApplnId;
    extTrieCreateParam.bPoolPerInst = OSIX_FALSE;
    extTrieCreateParam.bSemPerInst = OSIX_FALSE;
    extTrieCreateParam.bValidateType = OSIX_FALSE;

    if ((pV3OspfCxt->pExtRtRoot = TrieCreateInstance (&extTrieCreateParam))
        == NULL)
    {
        return OSIX_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:V3ExtRtInitRtInCxt\n");
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3extrt.c                       */
/*-----------------------------------------------------------------------*/
