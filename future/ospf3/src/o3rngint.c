/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rngint.c,v 1.9 2017/12/26 13:34:29 siva Exp $
 *
 * Description:This file contains procedures for creating and
 *             deleting Internal Address Ranges. 
 *
 *******************************************************************/

#include "o3inc.h"
/* Proto types of the functions private to this file only */

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaDeleteTrnsAddrRng                                  */
/*                                                                           */
/* Description    : Free NSSA Translated Address Range                       */
/*                                                                           */
/* Input          : pArea           : Pointer to Area Structure.             */
/*                  pNssaTrnsAddrRange : Pointer to translated Nssa          */
/*                                       Addr Range.                         */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : None                                                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AreaDeleteTrnsAddrRng (tV3OsArea * pArea, tV3OsAddrRange * pNssaTrnsAddrRange)
{
    tV3OsExtRoute      *pExtRoute = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDeleteTrnsAddrRng\n");

    TMO_SLL_Delete (&(pArea->type7AggrLst),
                    &(pNssaTrnsAddrRange->nextAddrRange));

    if (pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
    {
        OSPFV3_AREA_AGG_FREE (pNssaTrnsAddrRange);
        return;
    }

    if ((pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART) &&
        (pArea->pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pArea->pV3OspfCxt,
                                      OSPFV3_RESTART_TOP_CHG);
    }

    V3RagNssaProcAggRangeInCxt (pArea->pV3OspfCxt);

    pExtRoute = V3ExtrtFindRouteInCxt (pArea->pV3OspfCxt,
                                       &(pNssaTrnsAddrRange->ip6Addr),
                                       pNssaTrnsAddrRange->u1PrefixLength);

    if (pExtRoute != NULL)
    {
        V3RagAddRouteInAggAddrRangeInCxt (pArea->pV3OspfCxt, pExtRoute);
    }
    OSPFV3_AREA_AGG_FREE (pNssaTrnsAddrRange);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaDeleteTrnsAddrRng\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaDeleteInternalAddrRng                              */
/*                                                                           */
/* Description    : Free NSSA Translated Address Range                       */
/*                                                                           */
/* Input          : pArea       : Pointer to Area Structure.                 */
/*                  pInternalRng: Pointer to Internal Address Range          */
/*                                                                           */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : None                                                     */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3AreaDeleteInternalAddrRng (tV3OsArea * pArea, tV3OsAddrRange * pInternalRng)
{
    tV3OsArea          *pLstArea = NULL;
    tV3OsAddrRange     *pScanRng = NULL;
    tInputParams        inParams;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    VOID               *pLeafNode = NULL;
    UINT1               u1MoreSpecificRng = OSIX_FALSE;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaDeleteInternalAddrRng\n");

    TMO_SLL_Delete (&(pArea->type3AggrLst), &(pInternalRng->nextAddrRange));

    if (pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
    {
        OSPFV3_AREA_AGG_FREE (pInternalRng);
        return;
    }

    if ((pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART) &&
        (pArea->pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pArea->pV3OspfCxt,
                                      OSPFV3_RESTART_TOP_CHG);
    }

    /* Scan the areas table and delete the Cond. Inter Area Prefix lsa */
    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea, tV3OsArea *)
    {
        if ((V3UtilAreaIdComp (pArea->areaId,
                               pLstArea->areaId) != OSPFV3_EQUAL) &&
            ((pLstArea->u4AreaType == OSPFV3_NORMAL_AREA) ||
             ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
              (pLstArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY))))
        {

            if ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
                (pInternalRng->u1PrefixLength == 0))
            {
                continue;
            }
            if ((pLsaInfo = V3LsuSearchDatabase
                 (OSPFV3_INTER_AREA_PREFIX_LSA,
                  &(pInternalRng->linkStateId),
                  &(pArea->pV3OspfCxt->rtrId), NULL, pLstArea)) != NULL)

            {
                if (pLsaInfo->pLsaDesc->u2InternalLsaType ==
                    OSPFV3_COND_INTER_AREA_PREFIX_LSA)
                {

                    OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst,
                                          pInternalRng->ip6Addr);
                    NetRtInfo.u1Prefixlen = pInternalRng->u1PrefixLength;
                    MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
                    NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
                    NetRtInfo.u4RowStatus = ACTIVE;
                    NetRtInfo.u4Index = 0;
                    NetRtInfo.u1NullFlag = 0;

                    if (NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetRtInfo) ==
                        NETIPV6_FAILURE)
                    {
                        OSPFV3_TRC (OSPFV3_RAG_TRC,
                                    pArea->pV3OspfCxt->u4ContextId,
                                    " NetIpv6LeakRoute Deletion Failed. \n");
                    }

                    V3AgdFlushOut (pLsaInfo);
                }
            }
        }

    }

    /* 
       If there any address range is configured on the router,
       then it is required to originate aggregated LSAs */

    TMO_SLL_Scan (&(pArea->type3AggrLst), pScanRng, tV3OsAddrRange *)
    {
        if (V3UtilIp6PrefixComp (&(pInternalRng->ip6Addr),
                                 &(pScanRng->ip6Addr),
                                 pScanRng->u1PrefixLength) == OSPFV3_EQUAL)
        {
            u1MoreSpecificRng = OSIX_TRUE;
            break;
        }
    }

    if (u1MoreSpecificRng == OSIX_TRUE)
    {
        V3RagSetAggFlag (pArea);
        V3RagGenerateAggLsa (pArea);
    }
    else
    {
        inParams.pRoot = pArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
        inParams.i1AppId = pArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
        inParams.pLeafNode = NULL;
        inParams.u1PrefixLen = 0;
        inParams.Key.pKey = NULL;

        if (TrieGetFirstNode (&inParams, &pTempPtr,
                              (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            OSPFV3_AREA_AGG_FREE (pInternalRng);
            return;
        }

        do
        {
            pRtEntry = (tV3OsRtEntry *) pTempPtr;
            if (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                     &(pInternalRng->ip6Addr),
                                     pInternalRng->u1PrefixLength) ==
                OSPFV3_EQUAL)
            {

                TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea,
                              tV3OsArea *)
                {
                    if ((V3UtilAreaIdComp (pArea->areaId,
                                           pLstArea->areaId) != OSPFV3_EQUAL) &&
                        ((pLstArea->u4AreaType == OSPFV3_NORMAL_AREA) ||
                         ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
                          (pLstArea->u1SummaryFunctionality
                           == OSPFV3_SEND_AREA_SUMMARY))))
                    {
                        V3GenerateSummary (pLstArea, pRtEntry);
                    }
                }                /* TMO_SLL_Scan */
            }
            pLeafNode = inParams.pLeafNode;
            OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
            au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
            inParams.Key.pKey = au1Key;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr,
                                (VOID **) &(inParams.pLeafNode)) !=
               TRIE_FAILURE);

    }
    OSPFV3_AREA_AGG_FREE (pInternalRng);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaDeleteInternalAddrRange \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaAddAddrRng                                         */
/*                                                                           */
/* Description    : Add The Intrenal address Range in the sorted IPv6        */
/*                  Address and prefix length.                               */
/*                                                                           */
/* Input          : pAddrRngLst     : Pointer to Address Range List          */
/*                  pAddrRng        : Pointer to Internal Address Range      */
/*                                                                           */
/* Return         : None                                                     */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3AreaAddAddrRng (tTMO_SLL * pAddrRngLst, tV3OsAddrRange * pAddrRng)
{
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;
    tV3OsAddrRange     *pScanAddrRng = NULL;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tV3OsAddrRange     *pNxtAddrRng = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3AreaAddAddrRng\n");

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAddrRng->ip6Addr), pAddrRng->u1PrefixLength);

    /* If first node in list  - insert */
    if ((TMO_SLL_Count (pAddrRngLst)) == 0)
    {
        TMO_SLL_Add (pAddrRngLst, &(pAddrRng->nextAddrRange));
    }

    TMO_SLL_Scan (pAddrRngLst, pScanAddrRng, tV3OsAddrRange *)
    {

        V3UtilIp6PrefixCopy (&(tmpRngNet),
                             &(pScanAddrRng->ip6Addr),
                             pScanAddrRng->u1PrefixLength);

        if (V3UtilIp6AddrComp (&tmpRngNet, &newRngNet) == OSPFV3_GREATER)
        {
            /* Check if the next node  is not NULL 
               If next node is NULL, means we have reached 
               the end of list and this node should be inserted as 
               last node
             */
            pNext = TMO_SLL_Next (pAddrRngLst, &(pScanAddrRng->nextAddrRange));

            pNxtAddrRng = (tV3OsAddrRange *) (pNext);

            if (pNxtAddrRng == NULL)
            {
                TMO_SLL_Insert (pAddrRngLst,
                                &(pScanAddrRng->nextAddrRange),
                                &(pAddrRng->nextAddrRange));
                return;
            }
            else
            {
                continue;
            }
        }
        else if ((V3UtilIp6AddrComp (&tmpRngNet, &newRngNet)) == OSPFV3_LESS)
        {
            /* Insert new node before scanned node */
            pPrev = TMO_SLL_Previous (pAddrRngLst,
                                      &(pScanAddrRng->nextAddrRange));

            TMO_SLL_Insert (pAddrRngLst, pPrev, &(pAddrRng->nextAddrRange));

            break;
        }
        else
        {
            if (pAddrRng->u1PrefixLength > pScanAddrRng->u1PrefixLength)
                /* Insert new node before scanned node     */
            {
                pPrev =
                    TMO_SLL_Previous (pAddrRngLst,
                                      &(pScanAddrRng->nextAddrRange));
                TMO_SLL_Insert (pAddrRngLst, pPrev, &(pAddrRng->nextAddrRange));
                break;

            }
            else if (pAddrRng->u1PrefixLength < pScanAddrRng->u1PrefixLength)
                /* Insert new node after scanned node     */
            {

                pNext = TMO_SLL_Next (pAddrRngLst,
                                      &(pScanAddrRng->nextAddrRange));
                pNxtAddrRng = (tV3OsAddrRange *) (pNext);
                if (pNxtAddrRng == NULL)
                {
                    TMO_SLL_Insert (pAddrRngLst,
                                    &(pScanAddrRng->nextAddrRange),
                                    &(pAddrRng->nextAddrRange));
                    return;
                }
                else
                {
                    continue;
                }
            }
            else
            {
                if (V3UtilIp6AddrComp (&pAddrRng->ip6Addr,
                                       &pScanAddrRng->ip6Addr) != OSPFV3_EQUAL)
                {
                    OSPFV3_GBL_TRC (OSPFV3_RAG_TRC, " Invalid Config \n");
                    return;
                }
                OSPFV3_GBL_TRC (OSPFV3_RAG_TRC,
                                "Entry already exist - Must be made active\n");
                break;
            }
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3AreaAddAddrRng\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaCreateAddrRange                                    */
/*                                                                           */
/* Description    : Create an IP address Range and add to the address        */
/*                  range list.                                              */
/*                                                                           */
/* Input          : pArea           :   pointer to the AREA.                 */
/*                  pIp6Addr        :   IPv6 address of the Address range.   */
/*                  u1PrefixLength  : Prefix Length                          */
/*                  bStatus         :   Status of the Address range          */
/*                                                                           */
/* Returns        : New Address Range or NULL                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsAddrRange *
V3AreaCreateAddrRange (tV3OsArea * pArea,
                       tIp6Addr * pIp6Addr,
                       UINT1 u1PrefixLength,
                       UINT2 u2LsdbType, tV3OsRowStatus bStatus)
{

    tV3OsAddrRange     *pNewAddrRange = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaCreateAddrRange\n");

    OSPFV3_AREA_AGG_ALLOC (&(pNewAddrRange));
    if (NULL == pNewAddrRange)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC, pArea->pV3OspfCxt->u4ContextId,
                    " Area Aggregate  Alloc Failed\n");
        return NULL;
    }

    MEMSET (pNewAddrRange, 0, sizeof (tV3OsAddrRange));

    /* Populating New Address Range */
    pNewAddrRange->areaAggStatus = bStatus;
    OSPFV3_IP6ADDR_CPY (&(pNewAddrRange->ip6Addr), pIp6Addr);
    pNewAddrRange->u1PrefixLength = u1PrefixLength;
    pNewAddrRange->u4AddrRangeRtTag = 0;
    pNewAddrRange->u1AggrEffect = OSPFV3_ADVERTISE_MATCHING;
    (pNewAddrRange->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
    pNewAddrRange->u4RtCount = 0;

    if (u2LsdbType == OSPFV3_NSSA_LSA)
    {
        (pNewAddrRange->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
        if ((pArea->pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
            (pArea->pV3OspfCxt->u1RestartExitReason
             == OSPFV3_RESTART_INPROGRESS))
        {
            O3GrSetLsIdAsExtLsaInCxt (pArea->pV3OspfCxt,
                                      (UINT1 *) pNewAddrRange,
                                      OSPFV3_AS_TRNSLTD_RNG_LSA);
        }
        else
        {
            V3SetLsIdAsExtLsaInCxt (pArea->pV3OspfCxt, (UINT1 *) pNewAddrRange,
                                    OSPFV3_AS_TRNSLTD_RNG_LSA);
        }
        V3AreaAddAddrRng (&(pArea->type7AggrLst), pNewAddrRange);
    }
    else
    {
        (pNewAddrRange->metric).u4MetricType = OSPFV3_METRIC;
        if ((pArea->pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
            (pArea->pV3OspfCxt->u1RestartExitReason ==
             OSPFV3_RESTART_INPROGRESS))
        {
            O3GrSetLsIdInterAreaPrefixLsa
                (pArea->pV3OspfCxt, (UINT1 *) pNewAddrRange,
                 OSPFV3_COND_INTER_AREA_PREFIX_LSA);
        }
        else
        {
            V3SetLsIdInterAreaPrefixLsaInCxt
                (pArea->pV3OspfCxt, (UINT1 *) pNewAddrRange,
                 OSPFV3_COND_INTER_AREA_PREFIX_LSA);
        }
        V3AreaAddAddrRng (&(pArea->type3AggrLst), pNewAddrRange);
    }

    pNewAddrRange->pArea = pArea;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaCreateAddrRange\n");
    return pNewAddrRange;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaActiveInternalAddrRng                              */
/*                                                                           */
/* Description    : Activate the configured address range and                */
/*                  do the aggregation of summary Lsas.                      */
/*                                                                           */
/* Input          : pArea           :  Pointer to the AREA.                  */
/*                  pInternalRng    :  Pointer to Internal Addr Range        */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : None                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AreaActiveInternalAddrRng (tV3OsArea * pArea, tV3OsAddrRange * pInternalRng)
{
    tV3OsRtEntry       *pRtEntry = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaActiveInternalAddrRng\n");

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA's cannot be generated in graceful restart mode */
        return;
    }

    if (pArea->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
    {
        return;
    }

    V3RagFlushLsaFallingInAddrRange (pArea, pInternalRng);

    pRtEntry = V3RtcFindRtEntryInCxt (pArea->pV3OspfCxt,
                                      (VOID *) &(pInternalRng->ip6Addr),
                                      pInternalRng->u1PrefixLength,
                                      OSPFV3_DEST_NETWORK);

    if (pRtEntry != NULL)
    {
        V3RtcFlushOutSummaryLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                                 &(pRtEntry->linkStateId));

        V3RtcDeleteRtEntryInCxt (pArea->pV3OspfCxt, pRtEntry);
        V3RtcFreeRtEntry (pRtEntry);
    }

    V3RagSetAggFlag (pArea);
    V3RagGenerateAggLsa (pArea);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaActiveInternalAddrRng\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaActiveTransAddrRngInCxt                            */
/*                                                                           */
/* Description    : Activate the configured translated address range         */
/*                  to originate Aggregated type-5 LSA.                      */
/*                                                                           */
/* Input          : pV3OspfCxt      : Context pointer.                       */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : None                                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AreaActiveTransAddrRngInCxt (tV3OspfCxt * pV3OspfCxt)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaActiveTransAddrRng\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA's cannot be generated in graceful restart mode */
        return;
    }

    if (pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
    {
        return;
    }

    V3RagNssaProcAggRangeInCxt (pV3OspfCxt);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaActiveTransAddrRng\n");
    return;

}

/****************************************************************************/
/*                                                                          */
/* Function       : V3AreaAddRangeInRtLstInCxt                              */
/*                                                                          */
/* Description    : Inserts range in global Rtr Lst in ascending order      */
/*                                                                          */
/* Input          : pV3OspfCxt      : Context pointer.                      */
/*                  pAsExtRange     : Pointer to Range                      */
/*                                                                          */
/* Output         : None                                                    */
/*                                                                          */
/* Returns        : OSIX_TRUE   - If range added in Lst                     */
/*                  OSIX_FALSE  - If addition fails                         */
/*                                                                          */
/****************************************************************************/

PUBLIC UINT1
V3AreaAddRangeInRtLstInCxt (tV3OspfCxt * pV3OspfCxt,
                            tV3OsAsExtAddrRange * pAsExtRange)
{
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pScanAsExtNode = NULL;
    tTMO_SLL_NODE      *pNext = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaAddRangeInRtLst\n");

    /* If first node in list  - insert */
    if (TMO_SLL_Count (&(pV3OspfCxt->asExtAddrRangeLst)) == 0)
    {
        TMO_SLL_Add (&(pV3OspfCxt->asExtAddrRangeLst),
                     &(pAsExtRange->nextAsExtAddrRngInRtr));
        return OSIX_TRUE;
    }

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAsExtRange->ip6Addr), pAsExtRange->u1PrefixLength);

    /* List is in ascending order :
       Starting with lowest area Id range, all nodes
       are arranged in ascending order of IPv6 Address and Prefix Length 
     */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst),
                  pScanAsExtNode, tTMO_SLL_NODE *)
    {
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr,
                                             pScanAsExtNode);

        switch (V3UtilAreaIdComp (pAsExtRange->areaId, pScanAsExtRng->areaId))
        {
            case OSPFV3_GREATER:
                /* Check if the next node  is not NULL 
                   If next node is NULL,, means we have reached 
                   the end of list and this node should be inserted as 
                   last node
                 */
                pNext = TMO_SLL_Next (&(pV3OspfCxt->asExtAddrRangeLst),
                                      &(pScanAsExtRng->nextAsExtAddrRngInRtr));
                if (pNext == NULL)
                {
                    TMO_SLL_Insert (&(pV3OspfCxt->asExtAddrRangeLst),
                                    &(pScanAsExtRng->nextAsExtAddrRngInRtr),
                                    &(pAsExtRange->nextAsExtAddrRngInRtr));

                    gV3OsRtr.pExtAggrTableCache = NULL;
                    return OSIX_TRUE;
                }
                else
                {
                    continue;
                }
            case OSPFV3_LESS:
                /* Insert before this node */
                pPrev = TMO_SLL_Previous (&(pV3OspfCxt->asExtAddrRangeLst),
                                          &(pScanAsExtRng->
                                            nextAsExtAddrRngInRtr));
                TMO_SLL_Insert (&(pV3OspfCxt->asExtAddrRangeLst), pPrev,
                                &(pAsExtRange->nextAsExtAddrRngInRtr));

                gV3OsRtr.pExtAggrTableCache = NULL;
                return OSIX_TRUE;

            default:
                /*For OSPFV3_EQUAL */
                /* There exist range having same area Id:
                   So now inserted in ascending order of 
                   net/mask
                 */
                V3UtilIp6PrefixCopy (&tmpRngNet,
                                     &(pScanAsExtRng->ip6Addr),
                                     pScanAsExtRng->u1PrefixLength);

                switch (V3UtilIp6AddrComp (&tmpRngNet, &newRngNet))
                {
                    case OSPFV3_LESS:
                        pNext = TMO_SLL_Next (&(pV3OspfCxt->asExtAddrRangeLst),
                                              &(pScanAsExtRng->
                                                nextAsExtAddrRngInRtr));
                        if (pNext == NULL)
                        {
                            TMO_SLL_Insert (&(pV3OspfCxt->asExtAddrRangeLst),
                                            &(pScanAsExtRng->
                                              nextAsExtAddrRngInRtr),
                                            &(pAsExtRange->
                                              nextAsExtAddrRngInRtr));

                            gV3OsRtr.pExtAggrTableCache = NULL;
                            return OSIX_TRUE;
                        }
                        else
                        {
                            continue;
                        }

                    case OSPFV3_GREATER:
                        /* Insert before this node */
                        pPrev = TMO_SLL_Previous
                            (&(pV3OspfCxt->asExtAddrRangeLst),
                             &(pScanAsExtRng->nextAsExtAddrRngInRtr));

                        TMO_SLL_Insert (&(pV3OspfCxt->asExtAddrRangeLst),
                                        pPrev,
                                        &(pAsExtRange->nextAsExtAddrRngInRtr));

                        gV3OsRtr.pExtAggrTableCache = NULL;
                        return OSIX_TRUE;

                    default:
                        if (pAsExtRange->u1PrefixLength >
                            pScanAsExtRng->u1PrefixLength)
                        {
                            pNext =
                                TMO_SLL_Next (&(pV3OspfCxt->asExtAddrRangeLst),
                                              &(pScanAsExtRng->
                                                nextAsExtAddrRngInRtr));

                            if (pNext == NULL)
                            {
                                /* Insert after this node */
                                TMO_SLL_Insert
                                    (&(pV3OspfCxt->asExtAddrRangeLst),
                                     &(pScanAsExtRng->
                                       nextAsExtAddrRngInRtr),
                                     &(pAsExtRange->nextAsExtAddrRngInRtr));

                                gV3OsRtr.pExtAggrTableCache = NULL;
                                return OSIX_TRUE;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        else if (pAsExtRange->u1PrefixLength
                                 < pScanAsExtRng->u1PrefixLength)
                        {
                            /* Insert before this node */
                            pPrev =
                                TMO_SLL_Previous
                                (&(pV3OspfCxt->asExtAddrRangeLst),
                                 &(pScanAsExtRng->nextAsExtAddrRngInRtr));

                            TMO_SLL_Insert (&(pV3OspfCxt->asExtAddrRangeLst),
                                            pPrev,
                                            &(pAsExtRange->
                                              nextAsExtAddrRngInRtr));

                            gV3OsRtr.pExtAggrTableCache = NULL;
                            return OSIX_TRUE;
                        }
                        else
                        {
                            if (V3UtilIp6AddrComp (&pAsExtRange->ip6Addr,
                                                   &pScanAsExtRng->ip6Addr)
                                != OSPFV3_EQUAL)
                            {
                                OSPFV3_TRC (OSPFV3_RAG_TRC,
                                            pV3OspfCxt->u4ContextId,
                                            " Invalid Config \n");
                                return OSIX_FALSE;
                            }

                            OSPFV3_TRC (OSPFV3_RAG_TRC,
                                        pV3OspfCxt->u4ContextId,
                                        " Entry exist - Must be made ACTIVE\n");
                            return OSIX_TRUE;
                        }
                }
        }
    }                            /* List Scan */

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaAddRangeInRtLst\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function       : V3AreaInsertRngInAreaLst                                 */
/*                                                                           */
/* Description    : Adds the range in area Lst in sorted order               */
/*                                                                           */
/* Input          : pAsExtRange     : Pointer to new Range                   */
/*                  pArea           : Area pointer                           */
/*                                                                           */
/* Output         : None                                                     */
/*                                                                           */
/* Returns        : OSIX_SUCCESS / OSIX_FAILURE                              */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3AreaInsertRngInAreaLst (tV3OsAsExtAddrRange * pAsExtRange, tV3OsArea * pArea)
{
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;
    tTMO_SLL           *pExtRngLst = NULL;
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tTMO_SLL_NODE      *pPrev = NULL;
    tTMO_SLL_NODE      *pNext = NULL;
    tV3OsAsExtAddrRange *pNxtAsExtRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaInsertRngInAreaLst\n");

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAsExtRange->ip6Addr), pAsExtRange->u1PrefixLength);

    if (V3UtilAreaIdComp ((pAsExtRange->areaId),
                          OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)
    {
        pExtRngLst = &(pArea->type7AddrRangeLst);
    }
    else
    {
        pExtRngLst = &(pArea->asExtAddrRangeLst);
    }

    /* If first node in list  - insert */
    if ((TMO_SLL_Count (pExtRngLst)) == 0)
    {
        TMO_SLL_Add (pExtRngLst, &(pAsExtRange->nextAsExtAddrRange));
        return OSIX_SUCCESS;
    }

    TMO_SLL_Scan (pExtRngLst, pScanAsExtRng, tV3OsAsExtAddrRange *)
    {

        V3UtilIp6PrefixCopy (&tmpRngNet,
                             &(pScanAsExtRng->ip6Addr),
                             pScanAsExtRng->u1PrefixLength);

        if (V3UtilIp6AddrComp (&tmpRngNet, &newRngNet) == OSPFV3_GREATER)
        {
            /* Check if the next node  is not NULL 
               If next node is NULL, means we have reached 
               the end of list and this node should be inserted as 
               last node
             */
            pNext = TMO_SLL_Next (pExtRngLst,
                                  &(pScanAsExtRng->nextAsExtAddrRange));
            pNxtAsExtRng = (tV3OsAsExtAddrRange *) (pNext);

            if (pNxtAsExtRng == NULL)
            {

                TMO_SLL_Insert (pExtRngLst,
                                &(pScanAsExtRng->nextAsExtAddrRange),
                                &(pAsExtRange->nextAsExtAddrRange));
                return OSIX_SUCCESS;
            }
            else
            {
                continue;
            }
        }
        else if (V3UtilIp6AddrComp (&tmpRngNet, &newRngNet) == OSPFV3_LESS)
        {
            /* Insert new node before scanned node */
            pPrev = TMO_SLL_Previous (pExtRngLst,
                                      &(pScanAsExtRng->nextAsExtAddrRange));

            TMO_SLL_Insert (pExtRngLst,
                            pPrev, &(pAsExtRange->nextAsExtAddrRange));

            break;
        }
        else
        {
            if (pAsExtRange->u1PrefixLength > pScanAsExtRng->u1PrefixLength)
                /* Insert new node before scanned node     */
            {
                pPrev =
                    TMO_SLL_Previous (pExtRngLst,
                                      &(pScanAsExtRng->nextAsExtAddrRange));

                TMO_SLL_Insert (pExtRngLst,
                                pPrev, &(pAsExtRange->nextAsExtAddrRange));

                break;

            }
            else if (pAsExtRange->u1PrefixLength
                     < pScanAsExtRng->u1PrefixLength)
                /* Insert new node after scanned node     */
            {

                pNext = TMO_SLL_Next (pExtRngLst,
                                      &(pScanAsExtRng->nextAsExtAddrRange));
                pNxtAsExtRng = (tV3OsAsExtAddrRange *) (pNext);
                if (pNxtAsExtRng == NULL)
                {

                    TMO_SLL_Insert (pExtRngLst,
                                    &(pScanAsExtRng->nextAsExtAddrRange),
                                    &(pAsExtRange->nextAsExtAddrRange));
                    return OSIX_SUCCESS;
                }
                else
                {
                    continue;
                }

            }
            else
            {
                if (V3UtilIp6AddrComp (&pAsExtRange->ip6Addr,
                                       &pScanAsExtRng->ip6Addr) != OSPFV3_EQUAL)
                {
                    OSPFV3_TRC (OSPFV3_RAG_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                " Invalid Config \n");
                    return OSIX_FAILURE;

                }

                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " Entry already exist - Must be made active\n");
                break;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaInsertRngInAreaLst\n");
    return OSIX_SUCCESS;
}

/*---------------------------------------------------------------------*/
/*                     End of the file  o3rngint.c                      */
/*---------------------------------------------------------------------*/
