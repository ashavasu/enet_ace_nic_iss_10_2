/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3intra.c,v 1.11 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures for intra-area prefix
 *             LSAs
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE UINT1 V3ConstIntraAreaPrefixLsaRefRtr PROTO ((tV3OsArea * pArea));

PRIVATE VOID        V3ConstIntraAreaPrefixLsaRefNet
PROTO ((tV3OsInterface * pInterface));

PRIVATE VOID        V3GenIntraAreaPrefixLsaRefNet
PROTO ((tV3OsInterface * pInterface));

PRIVATE VOID        V3GenIARefNetLsa
PROTO ((UINT1 *pCurrent, UINT1 *pIALsa, UINT2 u2PrefixCount,
        tV3OsInterface * pInterface));

PRIVATE VOID V3GenIntraAreaPrefixLsaRefRtr PROTO ((tV3OsArea * pArea));

PRIVATE VOID        V3GenIARefRtrLsa
PROTO ((UINT1 *pCurrent, UINT1 *pIALsa, UINT2 u2PrefixCount,
        tV3OsArea * pArea));

PRIVATE VOID        V3FlushIntraAreaPrefixLsa
PROTO ((tV3OsArea * pArea, UINT4 u4IntraAreaLsaCounter));

PRIVATE UINT4 V3GetIntraAreaLsaLSId PROTO ((tV3OsArea * pArea));

PRIVATE UINT1       V3IsRtrFullyAdj
PROTO ((tV3OsInterface * pInterface, tV3OsRouterId rtrId));

PRIVATE VOID V3IntraLsaLstClear PROTO ((tTMO_SLL * pLsaLst));

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenIntraAreaPrefixLsa                               */
/*                                                                          */
/* Description     :  This function generate Intra Area Prefix LSA.         */
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
V3GenIntraAreaPrefixLsa (tV3OsArea * pArea)
{
    tTMO_SLL_NODE      *pIfNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface     *pVirtInterface = NULL;
    UINT4               u4IntraAreaLsaCounter = 0;
    UINT1               u1VirtFlag = OSIX_FALSE;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLinkStateId    linkStateId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenIntraAreaPrefixLsa\n");

    OSPFV3_TRC1 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "Intra Area Prefix LSA to be constructed for area : %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        u1VirtFlag = V3ConstIntraAreaPrefixLsaRefRtr (pArea);
        V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);

        TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pVirtInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

            if (V3UtilAreaIdComp
                (pVirtInterface->transitAreaId, pArea->areaId) == OSPFV3_EQUAL)
            {
                if ((pVirtInterface->u1IsmState == OSPFV3_IFS_DOWN)
                    && (u1VirtFlag == OSIX_FALSE))
                {
                    V3VifActivate (pVirtInterface, pArea);
                }
                else if ((pVirtInterface->u1IsmState != OSPFV3_IFS_DOWN)
                         && (u1VirtFlag == OSIX_TRUE))
                {
                    OSPFV3_GENERATE_IF_EVENT (pVirtInterface, OSPFV3_IFE_DOWN);
                }
            }
        }
        return;
    }

    MEMSET (&linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
    pLsaInfo = V3LsuSearchDatabase (OSPFV3_INTRA_AREA_PREFIX_LSA,
                                    &linkStateId,
                                    &(pArea->pV3OspfCxt->rtrId), NULL, pArea);

    if ((pLsaInfo != NULL) && (pLsaInfo->pLsaDesc != NULL)
        && (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired != OSIX_TRUE))
    {
        pLsaInfo->pLsaDesc->u1LsaChanged = OSIX_TRUE;
        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "MinLsInterval timer has not fired for this lsa\n");
        return;
    }

    u4IntraAreaLsaCounter = pArea->u4IntraAreaLsaCounter;
    pArea->u4IntraAreaLsaCounter = 0;

    u1VirtFlag = V3ConstIntraAreaPrefixLsaRefRtr (pArea);
    V3GenIntraAreaPrefixLsaRefRtr (pArea);

    TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfNode);

        if ((OSPFV3_IS_DR (pInterface)) && (pInterface->u4NbrFullCount != 0))
        {
            V3ConstIntraAreaPrefixLsaRefNet (pInterface);
            V3GenIntraAreaPrefixLsaRefNet (pInterface);
        }
    }

    if (u4IntraAreaLsaCounter > pArea->u4IntraAreaLsaCounter)
    {
        while (u4IntraAreaLsaCounter != pArea->u4IntraAreaLsaCounter)
        {
            u4IntraAreaLsaCounter--;
            V3FlushIntraAreaPrefixLsa (pArea, u4IntraAreaLsaCounter);
        }
    }

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pVirtInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

        if (V3UtilAreaIdComp
            (pVirtInterface->transitAreaId, pArea->areaId) == OSPFV3_EQUAL)
        {
            if ((pVirtInterface->u1IsmState == OSPFV3_IFS_DOWN)
                && (u1VirtFlag == OSIX_FALSE))
            {
                V3VifActivate (pVirtInterface, pArea);
            }
            else if ((pVirtInterface->u1IsmState != OSPFV3_IFS_DOWN)
                     && (u1VirtFlag == OSIX_TRUE))
            {
                OSPFV3_GENERATE_IF_EVENT (pVirtInterface, OSPFV3_IFE_DOWN);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenIntraAreaPrefixLsa\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3ConstIntraAreaPrefixLsaRefRtr                       */
/*                                                                          */
/* Description     :  This function constructs prefix List for Intra Area   */
/*                    Prefix LSA reference by Router LSA.                   */
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE UINT1
V3ConstIntraAreaPrefixLsaRefRtr (tV3OsArea * pArea)
{

    tV3OsInterface     *pInterface = NULL;
    tV3OsHost          *pHost = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    tTMO_SLL_NODE      *pStdNode = NULL;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pNewPrefixNode = NULL;
    tV3OsNeighbor      *pStandbyNbr = NULL;
    UINT1               u1VirtFlag = OSIX_FALSE;
    tV3OsInterface     *pVirtInterface = NULL;
    UINT1               u1TransFlag = OSIX_TRUE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3ConstIntraAreaPrefixLsaRefRtr\n");

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pVirtInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

        if (V3UtilAreaIdComp (pVirtInterface->transitAreaId, pArea->areaId) ==
            OSPFV3_EQUAL)
        {
            u1VirtFlag = OSIX_TRUE;
            MEMSET (&(pVirtInterface->ifIp6Addr), 0, sizeof (tIp6Addr));
        }
    }

    TMO_SLL_Scan (&(pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfNode);

        if ((u1VirtFlag == OSIX_TRUE)
            && (pInterface->u1IsmState != OSPFV3_IFS_DOWN))
        {
            pPrefixNode =
                (tV3OsPrefixNode *) TMO_SLL_First (&pInterface->ip6AddrLst);

            if (pPrefixNode != NULL)
            {
                OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                if (NULL == pNewPrefixNode)
                {
                    SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Unable to Allocate memory for Prefix Node\n"));

                    OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                    V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                    return u1VirtFlag;
                }

                MEMCPY (&pNewPrefixNode->prefixInfo, &pPrefixNode->prefixInfo,
                        sizeof (tV3OsPrefixInfo));

                pNewPrefixNode->prefixInfo.u1PrefixOpt |= OSPFV3_LA_BIT_MASK;
                pNewPrefixNode->prefixInfo.u1PrefixOpt
                    &= OSPFV3_NU_BIT_CLEAR_MASK;
                pNewPrefixNode->prefixInfo.u1PrefixLength =
                    OSPFV3_MAX_PREFIX_LEN;
                pNewPrefixNode->prefixInfo.u2Metric = 0;
                TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                             &pNewPrefixNode->nextPrefixNode);

                TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode,
                              tTMO_SLL_NODE *)
                {
                    pVirtInterface =
                        OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                             pLstNode);

                    if (V3UtilAreaIdComp
                        (pVirtInterface->transitAreaId,
                         pArea->areaId) == OSPFV3_EQUAL)
                    {
                        OSPFV3_IP6_ADDR_COPY (pVirtInterface->ifIp6Addr,
                                              pPrefixNode->prefixInfo.
                                              addrPrefix);
                    }
                }
                u1VirtFlag = OSIX_FALSE;
                V3RtcCalculateLocalHostRouteInCxt
                    (pArea->pV3OspfCxt,
                     &(pVirtInterface->ifIp6Addr),
                     &(pVirtInterface->transitAreaId), 0, OSPFV3_CREATED);
            }
        }

        switch (pInterface->u1IsmState)
        {
            case OSPFV3_IFS_DOWN:
                break;

            case OSPFV3_IFS_PTOP:
                if (pInterface->u1NetworkType == OSPFV3_IF_PTOP)
                {
                    TMO_SLL_Scan (&pInterface->ip6AddrLst,
                                  pPrefixNode, tV3OsPrefixNode *)
                    {
                        OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                        if (NULL == pNewPrefixNode)
                        {
                            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                          "Unable to Allocate memory for Prefix Node\n"));

                            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                        pArea->pV3OspfCxt->u4ContextId,
                                        "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                            V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                            return u1VirtFlag;
                        }

                        MEMCPY (&pNewPrefixNode->prefixInfo,
                                &pPrefixNode->prefixInfo,
                                sizeof (tV3OsPrefixInfo));
                        if (pNewPrefixNode->prefixInfo.u1PrefixLength ==
                            OSPFV3_MAX_PREFIX_LEN)
                        {
                            pNewPrefixNode->prefixInfo.u2Metric = 0;
                            pNewPrefixNode->prefixInfo.u1PrefixOpt |=
                                OSPFV3_LA_BIT_MASK;
                        }
                        else
                        {
                            pNewPrefixNode->prefixInfo.u2Metric =
                                (UINT2) pInterface->u4IfMetric;
                        }
                        TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                                     &pNewPrefixNode->nextPrefixNode);

                    }
                }
                else if (pInterface->u1NetworkType == OSPFV3_IF_PTOMP)
                {
                    TMO_SLL_Scan (&pInterface->ip6AddrLst,
                                  pPrefixNode, tV3OsPrefixNode *)
                    {
                        OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                        if (NULL == pNewPrefixNode)
                        {
                            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                          "Unable to Allocate memory for Prefix Node\n"));

                            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                        pArea->pV3OspfCxt->u4ContextId,
                                        "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                            V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                            return u1VirtFlag;
                        }

                        MEMCPY (&pNewPrefixNode->prefixInfo,
                                &pPrefixNode->prefixInfo,
                                sizeof (tV3OsPrefixInfo));
                        pNewPrefixNode->prefixInfo.u2Metric = 0;
                        pNewPrefixNode->prefixInfo.u1PrefixLength =
                            OSPFV3_MAX_PREFIX_LEN;
                        pNewPrefixNode->prefixInfo.u1PrefixOpt |=
                            OSPFV3_LA_BIT_MASK;
                        TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                                     &pNewPrefixNode->nextPrefixNode);

                    }
                }
                break;

            case OSPFV3_IFS_LOOP_BACK:
                TMO_SLL_Scan (&pInterface->ip6AddrLst,
                              pPrefixNode, tV3OsPrefixNode *)
            {
                OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                if (NULL == pNewPrefixNode)
                {
                    SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Unable to Allocate memory for Prefix Node\n"));

                    OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                    V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                    return u1VirtFlag;
                }

                MEMCPY (&pNewPrefixNode->prefixInfo,
                        &pPrefixNode->prefixInfo, sizeof (tV3OsPrefixInfo));
                pNewPrefixNode->prefixInfo.u2Metric = 0;
                pNewPrefixNode->prefixInfo.u1PrefixLength =
                    OSPFV3_MAX_PREFIX_LEN;
                pNewPrefixNode->prefixInfo.u1PrefixOpt |= OSPFV3_LA_BIT_MASK;
                TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                             &pNewPrefixNode->nextPrefixNode);
            }
                break;
            case OSPFV3_IFS_STANDBY:
                pStdNode = NULL;

                TMO_SLL_Scan (&pInterface->nbrsInIf, pStdNode, tTMO_SLL_NODE *)
                {
                    pStandbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                       nextNbrInIf, pStdNode);
                    if (pStandbyNbr->u1NbrIfStatus == OSPFV3_ACTIVE_ON_LINK)
                    {
                        break;
                    }
                }

                if (pStandbyNbr == NULL)
                {
                    break;
                }
                /* Check the Active Interface's Link Type */
                u1TransFlag = V3IsTransitLink (pStandbyNbr->pActiveInterface);
                TMO_SLL_Scan (&pInterface->ip6AddrLst,
                              pPrefixNode, tV3OsPrefixNode *)
                {
                    if ((u1TransFlag == OSIX_FALSE) ||
                        (pPrefixNode->prefixInfo.u1PrefixLength ==
                         OSPFV3_MAX_PREFIX_LEN))
                    {

                        OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                        if (NULL == pNewPrefixNode)
                        {
                            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                          "Unable to Allocate memory for Prefix Node\n"));

                            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                        pArea->pV3OspfCxt->u4ContextId,
                                        "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                            V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                            return u1VirtFlag;
                        }

                        MEMCPY (&pNewPrefixNode->prefixInfo,
                                &pPrefixNode->prefixInfo,
                                sizeof (tV3OsPrefixInfo));

                        if (pNewPrefixNode->prefixInfo.u1PrefixLength ==
                            OSPFV3_MAX_PREFIX_LEN)
                        {
                            pNewPrefixNode->prefixInfo.u2Metric = 0;
                            pNewPrefixNode->prefixInfo.u1PrefixOpt |=
                                OSPFV3_LA_BIT_MASK;
                        }
                        else
                        {
                            pNewPrefixNode->prefixInfo.u2Metric =
                                (UINT2) pInterface->u4IfMetric;
                        }
                        TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                                     &pNewPrefixNode->nextPrefixNode);

                    }
                }
                break;

            default:
                u1TransFlag = V3IsTransitLink (pInterface);

                TMO_SLL_Scan (&pInterface->ip6AddrLst,
                              pPrefixNode, tV3OsPrefixNode *)
                {

                    if ((u1TransFlag == OSIX_FALSE) ||
                        (pPrefixNode->prefixInfo.u1PrefixLength ==
                         OSPFV3_MAX_PREFIX_LEN))
                    {
                        OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                        if (NULL == pNewPrefixNode)
                        {
                            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                          "Unable to Allocate memory for Prefix Node\n"));

                            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                        pArea->pV3OspfCxt->u4ContextId,
                                        "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                            V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                            return u1VirtFlag;
                        }

                        MEMCPY (&pNewPrefixNode->prefixInfo,
                                &pPrefixNode->prefixInfo,
                                sizeof (tV3OsPrefixInfo));

                        if (pNewPrefixNode->prefixInfo.u1PrefixLength ==
                            OSPFV3_MAX_PREFIX_LEN)
                        {
                            pNewPrefixNode->prefixInfo.u2Metric = 0;
                            pNewPrefixNode->prefixInfo.u1PrefixOpt |=
                                OSPFV3_LA_BIT_MASK;
                        }
                        else
                        {
                            pNewPrefixNode->prefixInfo.u2Metric =
                                (UINT2) pInterface->u4IfMetric;
                        }
                        TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                                     &pNewPrefixNode->nextPrefixNode);

                    }
                }
                break;
        }
    }

    /* Add host routes */

    pHost = (tV3OsHost *) RBTreeGetFirst (pArea->pV3OspfCxt->pHostRBRoot);

    while (pHost != NULL)
    {
        if (pHost->pArea == pArea)
        {
            if ((pHost->hostStatus == ACTIVE) &&
                (pHost->u4HostMetric < OSPFV3_LS_INFINITY_16BIT))
            {
                OSPFV3_PREFIX_ALLOC (&(pNewPrefixNode));
                if (NULL == pNewPrefixNode)
                {
                    SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Unable to Allocate memory for Prefix Node\n"));

                    OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                    V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
                    return u1VirtFlag;
                }

                OSPFV3_IP6_ADDR_COPY (pNewPrefixNode->prefixInfo.addrPrefix,
                                      pHost->hostIp6Addr);
                pNewPrefixNode->prefixInfo.u2Metric =
                    (UINT2) pHost->u4HostMetric;
                pNewPrefixNode->prefixInfo.u1PrefixLength =
                    OSPFV3_MAX_PREFIX_LEN;
                pNewPrefixNode->prefixInfo.u1PrefixOpt = 0;
                TMO_SLL_Add (&pArea->IARefRtrPrefixLst,
                             &pNewPrefixNode->nextPrefixNode);
            }
        }

        pHost =
            (tV3OsHost *) RBTreeGetNext (pArea->pV3OspfCxt->pHostRBRoot,
                                         (tRBElem *) pHost, NULL);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstIntraAreaPrefixLsaRefRtr\n");
    return u1VirtFlag;
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3ConstIntraAreaPrefixLsaRefNet                       */
/*                                                                          */
/* Description     :  This function constructs prefix List for Intra Area   */
/*                    Prefix LSA reference by Network LSA.                  */
/*                                                                          */
/* Input           :  pInterface : Pointer to Interface.                    */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3ConstIntraAreaPrefixLsaRefNet (tV3OsInterface * pInterface)
{

    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT4               u4NumPrefix = 0;
    UINT2               u2NextPrefixStart = 0;
    UINT1               u1PrefixLength = 0;
    UINT1               u1PrefixOption = 0;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pOldPrefixNode = NULL;
    UINT1               u1Flag = OSIX_FALSE;
    UINT4               u4linkId1 = 0;
    UINT4               u4linkId2 = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3ConstIntraAreaPrefixLsaRefNet\n");

    TMO_SLL_Scan (&(pInterface->linkLsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

        if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                             pInterface->pArea->pV3OspfCxt->rtrId) !=
            OSPFV3_EQUAL)
        {
            if (V3IsRtrFullyAdj (pInterface, pLsaInfo->lsaId.advRtrId) ==
                OSIX_FALSE)
            {
                continue;
            }
        }

        OSPFV3_BUFFER_GET_4_BYTE (pLsaInfo->pLsa,
                                  OSPFV3_LS_PREFIX_NUM_OFFSET, u4NumPrefix);

        u2NextPrefixStart = OSPFV3_LS_PREFIX_NUM_OFFSET +
            OSPFV3_LS_PREFIX_NUM_SIZE;

        while (u4NumPrefix--)
        {
            OSPFV3_BUFFER_GET_1_BYTE (pLsaInfo->pLsa, u2NextPrefixStart,
                                      u1PrefixLength);
            OSPFV3_BUFFER_GET_1_BYTE (pLsaInfo->pLsa,
                                      (u2NextPrefixStart +
                                       OSPFV3_LS_PREFIX_LEN_SIZE),
                                      u1PrefixOption);

            if (!OSPFV3_IS_NU_OR_LA_BIT_SET (u1PrefixOption))
            {
                OSPFV3_PREFIX_ALLOC (&(pPrefixNode));
                if (NULL == pPrefixNode)
                {
                    SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Unable to Allocate memory for Prefix Node\n"));

                    OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "!!! PREFIX NODE ALLOC FAILURE !!! \n");
                    V3IntraLsaLstClear (&pInterface->ip6NetPrefLst);
                    return;
                }

                pPrefixNode->prefixInfo.u1PrefixLength = u1PrefixLength;
                pPrefixNode->prefixInfo.u1PrefixOpt = u1PrefixOption;
                pPrefixNode->prefixInfo.u2Metric = 0;
                OSPFV3_BUFFER_GET_STRING
                    (pLsaInfo->pLsa,
                     &pPrefixNode->prefixInfo.addrPrefix,
                     (u2NextPrefixStart + OSPFV3_LS_LEN_OPT_RES_SIZE),
                     OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLength));

                u4linkId1 = V3UtilDwordFromPdu (pLsaInfo->lsaId.linkStateId);
                u4linkId2 = pInterface->u4InterfaceId;

                TMO_SLL_Scan (&pInterface->ip6NetPrefLst,
                              pOldPrefixNode, tV3OsPrefixNode *)
                {

                    if (pOldPrefixNode->prefixInfo.u1PrefixLength ==
                        u1PrefixLength)
                    {
                        if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                              pInterface->pArea->pV3OspfCxt->
                                              rtrId) == OSPFV3_EQUAL)
                            && (u4linkId1 != u4linkId2))
                        {
                            u1Flag = OSIX_FALSE;
                        }
                        else
                        {

                            if (V3UtilIp6PrefixComp
                                (&pOldPrefixNode->prefixInfo.addrPrefix,
                                 &pPrefixNode->prefixInfo.addrPrefix,
                                 u1PrefixLength) == OSPFV3_EQUAL)
                            {
                                pOldPrefixNode->prefixInfo.u1PrefixOpt |=
                                    u1PrefixOption;
                                u1Flag = OSIX_TRUE;
                            }
                        }
                    }

                }

                if (u1Flag == OSIX_TRUE)
                {
                    u1Flag = OSIX_FALSE;
                    OSPFV3_PREFIX_FREE (pPrefixNode);
                }
                else
                {
                    TMO_SLL_Add (&pInterface->ip6NetPrefLst,
                                 &pPrefixNode->nextPrefixNode);

                }

            }

            u2NextPrefixStart = (UINT2) (u2NextPrefixStart +
                                         OSPFV3_LS_LEN_OPT_RES_SIZE +
                                         OSPFV3_GET_PREFIX_LEN_IN_BYTE
                                         (u1PrefixLength));
        }

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ConstIntraAreaPrefixLsaRefNet\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenIntraAreaPrefixLsaRefNet                         */
/*                                                                          */
/* Description     :  This function constructs Intra Area Prefix LSA        */
/*                    reference by Network LSA.                             */
/*                                                                          */
/* Input           :  pInterface : Pointer to Interface.                    */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3GenIntraAreaPrefixLsaRefNet (tV3OsInterface * pInterface)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pTempPrefixNode = NULL;
    UINT2               u2PrefixCount = 0;
    UINT1              *pCurrent = NULL;
    UINT1              *pIALsa = NULL;
    UINT1               u1TxFlag = OSIX_FALSE;
    UINT1               u1AllocFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GenIntraAreaPrefixLsaRefNet\n");
    if (TMO_SLL_Count (&pInterface->ip6NetPrefLst) == 0)
    {
        return;
    }

    OSPFV3_DYNM_SLL_SCAN (&pInterface->ip6NetPrefLst, pPrefixNode,
                          pTempPrefixNode, tV3OsPrefixNode *)
    {
        if (u2PrefixCount == 0)
        {
            OSPFV3_LSA_ALLOC (&(pIALsa));
            if (NULL == pIALsa)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to Allocate memory for Intra Area Ref Network LSA\n"));

                OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                            pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "Intra Area Ref Network LSA_ALLOC FAILURE\n");
                u1AllocFlag = OSIX_TRUE;
                break;
            }
            pCurrent = pIALsa;
            pCurrent =
                pCurrent + OSPFV3_LS_HEADER_SIZE + OSPFV3_REF_LS_HDR_SIZE;
            u1TxFlag = OSIX_FALSE;
        }
        OSPFV3_ADD_INTRA_AREA_PREFIX (pCurrent, pPrefixNode->prefixInfo);
        u2PrefixCount++;
        if (u2PrefixCount == OSPFV3_MAX_NUM_PREFIX)
        {
            /* Generate Intra Area prefix LSA */
            V3GenIARefNetLsa (pCurrent, pIALsa, u2PrefixCount, pInterface);
            u2PrefixCount = 0;
            u1TxFlag = OSIX_TRUE;
            pIALsa = NULL;
        }

        TMO_SLL_Delete (&pInterface->ip6NetPrefLst,
                        &pPrefixNode->nextPrefixNode);
        OSPFV3_PREFIX_FREE (pPrefixNode);
    }

    if (u1AllocFlag == OSIX_TRUE)
    {
        V3IntraLsaLstClear (&pInterface->ip6NetPrefLst);
        return;
    }

    if ((u1TxFlag == OSIX_FALSE) && (pIALsa != NULL))
    {
        V3GenIARefNetLsa (pCurrent, pIALsa, u2PrefixCount, pInterface);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenIntraAreaPrefixLsaRefNet\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenIARefNetLsa                                      */
/*                                                                          */
/* Description     :  This function generates Intra Area Prefix LSA         */
/*                    reference by Network LSA.                             */
/*                                                                          */
/* Input           :  pInterface : Pointer to Interface.                    */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3GenIARefNetLsa (UINT1 *pCurrent, UINT1 *pIALsa, UINT2 u2PrefixCount,
                  tV3OsInterface * pInterface)
{

    tV3OsLinkStateId    linkStateId;
    UINT2               u2LsaLen = 0;
    UINT1              *pNewCurrent = NULL;
    UINT4               u4LinkStateId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GenIARefNetLsa\n");
    pNewCurrent = pIALsa;

    pNewCurrent = pNewCurrent + OSPFV3_LS_HEADER_SIZE;

    u2LsaLen = (UINT2) (pCurrent - pIALsa);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pIALsa, OSPFV3_LENGTH_OFFSET_IN_LS_HEADER,
                                 u2LsaLen);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
    OSPFV3_LADD2BYTE (pNewCurrent, u2PrefixCount);
    OSPFV3_LADD2BYTE (pNewCurrent, OSPFV3_REF_NETWORK_LSA);
    OSPFV3_LADDSTR (pNewCurrent, &linkStateId, OSPFV3_LINKSTATE_ID_LEN);
    OSPFV3_LADDSTR (pNewCurrent, &pInterface->pArea->pV3OspfCxt->rtrId,
                    OSPFV3_RTR_ID_LEN);

    u4LinkStateId = V3GetIntraAreaLsaLSId (pInterface->pArea);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4LinkStateId);

    V3GenerateLsa (pInterface->pArea, OSPFV3_INTRA_AREA_PREFIX_LSA,
                   &linkStateId, pIALsa, OSIX_FALSE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenIARefNetLsa\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenIntraAreaPrefixLsaRefRtr                         */
/*                                                                          */
/* Description     :  This function constructs Intra Area Prefix LSA        */
/*                    reference by Router  LSA.                             */
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3GenIntraAreaPrefixLsaRefRtr (tV3OsArea * pArea)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pTempPrefixNode = NULL;
    UINT2               u2PrefixCount = 0;
    UINT1              *pCurrent = NULL;
    UINT1              *pIALsa = NULL;
    UINT1               u1TxFlag = OSIX_FALSE;
    UINT1               u1AllocFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GenIntraAreaPrefixLsaRefRtr\n");

    if (TMO_SLL_Count (&pArea->IARefRtrPrefixLst) == 0)
    {
        return;
    }

    OSPFV3_DYNM_SLL_SCAN (&pArea->IARefRtrPrefixLst, pPrefixNode,
                          pTempPrefixNode, tV3OsPrefixNode *)
    {
        if (u2PrefixCount == 0)
        {
            OSPFV3_LSA_ALLOC (&(pIALsa));
            if (NULL == pIALsa)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to Allocate memory for Intra Area Ref Network LSA\n"));

                OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                            pArea->pV3OspfCxt->u4ContextId,
                            "Intra Area Ref Network LSA_ALLOC FAILURE\n");
                u1AllocFlag = OSIX_TRUE;
                break;
            }
            pCurrent = pIALsa;
            pCurrent =
                pCurrent + OSPFV3_LS_HEADER_SIZE + OSPFV3_REF_LS_HDR_SIZE;
            u1TxFlag = OSIX_FALSE;
        }
        OSPFV3_ADD_INTRA_AREA_PREFIX (pCurrent, pPrefixNode->prefixInfo);
        u2PrefixCount++;
        if (u2PrefixCount == OSPFV3_MAX_NUM_PREFIX)
        {
            /* Generate Intra Area prefix LSA */
            V3GenIARefRtrLsa (pCurrent, pIALsa, u2PrefixCount, pArea);
            u2PrefixCount = 0;
            u1TxFlag = OSIX_TRUE;
            pIALsa = NULL;
        }

        TMO_SLL_Delete (&pArea->IARefRtrPrefixLst,
                        &pPrefixNode->nextPrefixNode);
        OSPFV3_PREFIX_FREE (pPrefixNode);
    }

    if (u1AllocFlag == OSIX_TRUE)
    {
        V3IntraLsaLstClear (&pArea->IARefRtrPrefixLst);
        return;
    }

    if ((u1TxFlag == OSIX_FALSE) && (pIALsa != NULL))
    {
        V3GenIARefRtrLsa (pCurrent, pIALsa, u2PrefixCount, pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenIntraAreaPrefixLsaRefRtr\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenIARefRtrLsa                                      */
/*                                                                          */
/* Description     :  This function generates Intra Area Prefix LSA         */
/*                    reference by Router  LSA.                             */
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3GenIARefRtrLsa (UINT1 *pCurrent, UINT1 *pIALsa, UINT2 u2PrefixCount,
                  tV3OsArea * pArea)
{

    tV3OsLinkStateId    linkStateId;
    UINT2               u2LsaLen = 0;
    UINT1              *pNewCurrent = NULL;
    UINT4               u4LinkStateId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GenIARefRtrLsa\n");

    pNewCurrent = pIALsa;

    pNewCurrent = pNewCurrent + OSPFV3_LS_HEADER_SIZE;

    u2LsaLen = (UINT2) (pCurrent - pIALsa);
    OSPFV3_BUFFER_ASSIGN_2_BYTE (pIALsa, OSPFV3_LENGTH_OFFSET_IN_LS_HEADER,
                                 u2LsaLen);

    MEMSET (&linkStateId, 0, OSPFV3_LINKSTATE_ID_LEN);
    OSPFV3_LADD2BYTE (pNewCurrent, u2PrefixCount);
    OSPFV3_LADD2BYTE (pNewCurrent, OSPFV3_REF_ROUTER_LSA);
    OSPFV3_LADDSTR (pNewCurrent, &linkStateId, OSPFV3_LINKSTATE_ID_LEN);
    OSPFV3_LADDSTR (pNewCurrent, &pArea->pV3OspfCxt->rtrId, OSPFV3_RTR_ID_LEN);

    u4LinkStateId = V3GetIntraAreaLsaLSId (pArea);

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4LinkStateId);

    V3GenerateLsa (pArea, OSPFV3_INTRA_AREA_PREFIX_LSA, &linkStateId,
                   pIALsa, OSIX_FALSE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenIARefRtrLsa\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3FlushIntraAreaPrefixLsa                             */
/*                                                                          */
/* Description     :  This function flush a particular Intra Area Prefix LSA*/
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                    u4IntraAreaLsaCounter  : link State Id.               */
/*                                                                          */
/* Output         :  None.                                                  */
/*                                                                          */
/* Returns         : None.                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3FlushIntraAreaPrefixLsa (tV3OsArea * pArea, UINT4 u4IntraAreaLsaCounter)
{

    tV3OsLinkStateId    linkStateId;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3FlushIntraAreaPrefixLsa\n");

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4IntraAreaLsaCounter);

    pLsaInfo = V3LsuSearchDatabase (OSPFV3_INTRA_AREA_PREFIX_LSA, &linkStateId,
                                    &pArea->pV3OspfCxt->rtrId, NULL, pArea);

    if (pLsaInfo != NULL)
    {
        V3AgdFlushOut (pLsaInfo);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3FlushIntraAreaPrefixLsa\n");
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GetIntraAreaLsaLSId                                 */
/*                                                                          */
/* Description     :  This function gives the Link State Id for Intra Area  */
/*                    Prefix LSA.                                           */
/*                                                                          */
/* Input           :  pArea : Pointer to Area.                              */
/*                                                                          */
/* Output          :  None.                                                 */
/*                                                                          */
/* Returns         : Link State Id.                                         */
/*                                                                          */
/****************************************************************************/
PRIVATE UINT4
V3GetIntraAreaLsaLSId (tV3OsArea * pArea)
{
    UINT4               u4LinkStateId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GetIntraAreaLsaLSId\n");
    u4LinkStateId = pArea->u4IntraAreaLsaCounter;
    pArea->u4IntraAreaLsaCounter++;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GetIntraAreaLsaLSId\n");
    return u4LinkStateId;

}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3IsRtrFullyAdj                                       */
/*                                                                          */
/* Description     :  This function checks whether the neighbor router      */
/*                    (rtrId) is in FULL state with this router in the      */
/*                    interface                                             */
/*                                                                          */
/* Input           :  pInterface : Pointer to interface                     */
/*                    rtrId      : Router Id                                */
/*                                                                          */
/* Output          :  None.                                                 */
/*                                                                          */
/* Returns         :  OSIX_TRUE/OSIX_FALSE                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE UINT1
V3IsRtrFullyAdj (tV3OsInterface * pInterface, tV3OsRouterId rtrId)
{

    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsRtrFullyAdj\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (OSPFV3_IS_NBR_FULL (pNbr))
        {
            if (V3UtilRtrIdComp (rtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL)
            {
                return OSIX_TRUE;
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsRtrFullyAdj\n");
    return OSIX_FALSE;
}

/****************************************************************************/
/*                                                                          */
/* Function        :  V3IntraLsaLstClear                                    */
/*                                                                          */
/* Description     :  This function deletes the intra-area prefix node      */
/*                                                                          */
/* Input           :  pLsaLst : TMO_SLL Node                                */
/*                                                                          */
/* Output          :  None.                                                 */
/*                                                                          */
/* Returns         :  None                                                  */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
V3IntraLsaLstClear (tTMO_SLL * pLsaLst)
{

    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pTempPrefixNode = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IntraLsaLstClear\n");
    OSPFV3_DYNM_SLL_SCAN (pLsaLst, pPrefixNode,
                          pTempPrefixNode, tV3OsPrefixNode *)
    {
        TMO_SLL_Delete (pLsaLst, &pPrefixNode->nextPrefixNode);
        OSPFV3_PREFIX_FREE (pPrefixNode);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IntraLsaLstClear\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3intra.c                       */
/*-----------------------------------------------------------------------*/
