/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3miconf.c,v 1.12 2017/12/26 13:34:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "o3inc.h"
# include "fsmisolw.h"
# include "fsmio3lw.h"
# include "ospf3lw.h"
# include "fsos3lw.h"
# include "fsmisos3cli.h"
# include "fsmio3cli.h"

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3RouterId
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3RouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3RouterId (INT4 i4FsMIStdOspfv3ContextId,
                             UINT4 u4SetValFsMIStdOspfv3RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3RouterId (u4SetValFsMIStdOspfv3RouterId);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AdminStat
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3AdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AdminStat (INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4SetValFsMIStdOspfv3AdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3AdminStat (i4SetValFsMIStdOspfv3AdminStat);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3ASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3ASBdrRtrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3ASBdrRtrStatus (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4SetValFsMIStdOspfv3ASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3ASBdrRtrStatus (i4SetValFsMIStdOspfv3ASBdrRtrStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3ExtAreaLsdbLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3ExtAreaLsdbLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3ExtAreaLsdbLimit (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4SetValFsMIStdOspfv3ExtAreaLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3ExtAreaLsdbLimit (i4SetValFsMIStdOspfv3ExtAreaLsdbLimit);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3MulticastExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3MulticastExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3MulticastExtensions (INT4 i4FsMIStdOspfv3ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pSetValFsMIStdOspfv3MulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3MulticastExtensions
        (pSetValFsMIStdOspfv3MulticastExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3ExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3ExitOverflowInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3ExitOverflowInterval (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4
                                         u4SetValFsMIStdOspfv3ExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3ExitOverflowInterval
        (u4SetValFsMIStdOspfv3ExitOverflowInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3DemandExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3DemandExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3DemandExtensions (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4SetValFsMIStdOspfv3DemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3DemandExtensions (i4SetValFsMIStdOspfv3DemandExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3TrafficEngineeringSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3TrafficEngineeringSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3TrafficEngineeringSupport (INT4 i4FsMIStdOspfv3ContextId,
                                              INT4
                                              i4SetValFsMIStdOspfv3TrafficEngineeringSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3TrafficEngineeringSupport
        (i4SetValFsMIStdOspfv3TrafficEngineeringSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3ReferenceBandwidth
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3ReferenceBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3ReferenceBandwidth (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4
                                       u4SetValFsMIStdOspfv3ReferenceBandwidth)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3ReferenceBandwidth
        (u4SetValFsMIStdOspfv3ReferenceBandwidth);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3RestartSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3RestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3RestartSupport (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4SetValFsMIStdOspfv3RestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3RestartSupport (i4SetValFsMIStdOspfv3RestartSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3RestartInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3RestartInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3RestartInterval (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4SetValFsMIStdOspfv3RestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3RestartInterval (i4SetValFsMIStdOspfv3RestartInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3Status
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIStdOspfv3Status
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3Status (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4SetValFsMIStdOspfv3Status)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4SeqNum = 0;

    pV3OspfCxt = V3UtilOspfGetCxt (i4FsMIStdOspfv3ContextId);

    switch (i4SetValFsMIStdOspfv3Status)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if (pV3OspfCxt != NULL)
            {
                return SNMP_FAILURE;
            }
            if (V3RtrCreateCxt ((UINT4) i4FsMIStdOspfv3ContextId) ==
                OSIX_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if ((pV3OspfCxt =
                 V3UtilOspfGetCxt (i4FsMIStdOspfv3ContextId)) == NULL)
            {
                return SNMP_FAILURE;
            }

            if (i4SetValFsMIStdOspfv3Status == CREATE_AND_GO)
            {
                pV3OspfCxt->contextStatus = ACTIVE;
            }
            else
            {
                pV3OspfCxt->contextStatus = NOT_IN_SERVICE;
            }
            break;
        }
        case ACTIVE:
        case NOT_IN_SERVICE:
        {
            if (pV3OspfCxt == NULL)
            {
                return SNMP_FAILURE;
            }
            pV3OspfCxt->contextStatus =
                (tV3OsRowStatus) i4SetValFsMIStdOspfv3Status;
            break;
        }
        case DESTROY:
        {
            if (pV3OspfCxt == NULL)
            {
                return SNMP_SUCCESS;
            }
            pV3OspfCxt->contextStatus = DESTROY;
            V3RtrDeleteCxt (pV3OspfCxt);
            break;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (i4FsMIStdOspfv3ContextId,
                           i4SetValFsMIStdOspfv3Status, NULL,
                           FsMIStdOspfv3Status,
                           (sizeof (FsMIStdOspfv3Status) /
                            sizeof (UINT4)), 'i', OSPFV3_TRUE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3ImportAsExtern
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3ImportAsExtern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3ImportAsExtern (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3AreaId,
                                   INT4 i4SetValFsMIStdOspfv3ImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3ImportAsExtern (u4FsMIStdOspfv3AreaId,
                                    i4SetValFsMIStdOspfv3ImportAsExtern);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaSummary
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3AreaSummary
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaSummary (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3AreaId,
                                INT4 i4SetValFsMIStdOspfv3AreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaSummary (u4FsMIStdOspfv3AreaId,
                                 i4SetValFsMIStdOspfv3AreaSummary);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3StubMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3StubMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3StubMetric (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIStdOspfv3AreaId,
                               INT4 i4SetValFsMIStdOspfv3StubMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3StubMetric (u4FsMIStdOspfv3AreaId,
                                i4SetValFsMIStdOspfv3StubMetric);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaNssaTranslatorRole
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3AreaNssaTranslatorRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaNssaTranslatorRole (INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIStdOspfv3AreaId,
                                           INT4
                                           i4SetValFsMIStdOspfv3AreaNssaTranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaNssaTranslatorRole (u4FsMIStdOspfv3AreaId,
                                            i4SetValFsMIStdOspfv3AreaNssaTranslatorRole);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval (INT4
                                                        i4FsMIStdOspfv3ContextId,
                                                        UINT4
                                                        u4FsMIStdOspfv3AreaId,
                                                        UINT4
                                                        u4SetValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaNssaTranslatorStabilityInterval (u4FsMIStdOspfv3AreaId,
                                                         u4SetValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaStubMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3AreaStubMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaStubMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       INT4
                                       i4SetValFsMIStdOspfv3AreaStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaStubMetricType (u4FsMIStdOspfv3AreaId,
                                        i4SetValFsMIStdOspfv3AreaStubMetricType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                setValFsMIStdOspfv3AreaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaStatus (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIStdOspfv3AreaId,
                               INT4 i4SetValFsMIStdOspfv3AreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3AreaStatus (u4FsMIStdOspfv3AreaId,
                                       i4SetValFsMIStdOspfv3AreaStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3HostMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                setValFsMIStdOspfv3HostMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3HostMetric (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               INT4 i4SetValFsMIStdOspfv3HostMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3HostMetric (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                i4SetValFsMIStdOspfv3HostMetric);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3HostAreaID
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                setValFsMIStdOspfv3HostAreaID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3HostAreaID (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               UINT4 u4SetValFsMIStdOspfv3HostAreaID)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3HostAreaID (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                u4SetValFsMIStdOspfv3HostAreaID);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3HostStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                setValFsMIStdOspfv3HostStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3HostStatus (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               INT4 i4SetValFsMIStdOspfv3HostStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3HostStatus (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                i4SetValFsMIStdOspfv3HostStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfAreaId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfAreaId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfAreaId (INT4 i4FsMIStdOspfv3IfIndex,
                             UINT4 u4SetValFsMIStdOspfv3IfAreaId)
{
    return (nmhSetOspfv3IfAreaId (i4FsMIStdOspfv3IfIndex,
                                  u4SetValFsMIStdOspfv3IfAreaId));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfType
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfType (INT4 i4FsMIStdOspfv3IfIndex,
                           INT4 i4SetValFsMIStdOspfv3IfType)
{
    return (nmhSetOspfv3IfType (i4FsMIStdOspfv3IfIndex,
                                i4SetValFsMIStdOspfv3IfType));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfAdminStat
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfAdminStat (INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 i4SetValFsMIStdOspfv3IfAdminStat)
{
    return (nmhSetOspfv3IfAdminStat (i4FsMIStdOspfv3IfIndex,
                                     i4SetValFsMIStdOspfv3IfAdminStat));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfRtrPriority
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfRtrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfRtrPriority (INT4 i4FsMIStdOspfv3IfIndex,
                                  INT4 i4SetValFsMIStdOspfv3IfRtrPriority)
{
    return (nmhSetOspfv3IfRtrPriority (i4FsMIStdOspfv3IfIndex,
                                       i4SetValFsMIStdOspfv3IfRtrPriority));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfTransitDelay (INT4 i4FsMIStdOspfv3IfIndex,
                                   INT4 i4SetValFsMIStdOspfv3IfTransitDelay)
{
    return (nmhSetOspfv3IfTransitDelay (i4FsMIStdOspfv3IfIndex,
                                        i4SetValFsMIStdOspfv3IfTransitDelay));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfRetransInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4
                                      i4SetValFsMIStdOspfv3IfRetransInterval)
{
    return (nmhSetOspfv3IfRetransInterval (i4FsMIStdOspfv3IfIndex,
                                           i4SetValFsMIStdOspfv3IfRetransInterval));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfHelloInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                    INT4 i4SetValFsMIStdOspfv3IfHelloInterval)
{
    return (nmhSetOspfv3IfHelloInterval (i4FsMIStdOspfv3IfIndex,
                                         i4SetValFsMIStdOspfv3IfHelloInterval));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfRtrDeadInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4
                                      i4SetValFsMIStdOspfv3IfRtrDeadInterval)
{
    return (nmhSetOspfv3IfRtrDeadInterval (i4FsMIStdOspfv3IfIndex,
                                           i4SetValFsMIStdOspfv3IfRtrDeadInterval));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfPollInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfPollInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                   UINT4 u4SetValFsMIStdOspfv3IfPollInterval)
{
    return (nmhSetOspfv3IfPollInterval (i4FsMIStdOspfv3IfIndex,
                                        u4SetValFsMIStdOspfv3IfPollInterval));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfMulticastForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfMulticastForwarding (INT4 i4FsMIStdOspfv3IfIndex,
                                          INT4
                                          i4SetValFsMIStdOspfv3IfMulticastForwarding)
{
    return (nmhSetOspfv3IfMulticastForwarding (i4FsMIStdOspfv3IfIndex,
                                               i4SetValFsMIStdOspfv3IfMulticastForwarding));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfDemand
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfDemand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfDemand (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 i4SetValFsMIStdOspfv3IfDemand)
{
    return (nmhSetOspfv3IfDemand (i4FsMIStdOspfv3IfIndex,
                                  i4SetValFsMIStdOspfv3IfDemand));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfMetricValue
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfMetricValue (INT4 i4FsMIStdOspfv3IfIndex,
                                  INT4 i4SetValFsMIStdOspfv3IfMetricValue)
{
    return (nmhSetOspfv3IfMetricValue (i4FsMIStdOspfv3IfIndex,
                                       i4SetValFsMIStdOspfv3IfMetricValue));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfInstId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfInstId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfInstId (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 i4SetValFsMIStdOspfv3IfInstId)
{
    return (nmhSetOspfv3IfInstId (i4FsMIStdOspfv3IfIndex,
                                  i4SetValFsMIStdOspfv3IfInstId));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfDemandNbrProbe
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfDemandNbrProbe
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbe (INT4 i4FsMIStdOspfv3IfIndex,
                                     INT4 i4SetValFsMIStdOspfv3IfDemandNbrProbe)
{
    return (nmhSetOspfv3IfDemandNbrProbe (i4FsMIStdOspfv3IfIndex,
                                          i4SetValFsMIStdOspfv3IfDemandNbrProbe));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbeRetxLimit (INT4 i4FsMIStdOspfv3IfIndex,
                                              UINT4
                                              u4SetValFsMIStdOspfv3IfDemandNbrProbeRetxLimit)
{
    return (nmhSetOspfv3IfDemandNbrProbeRetxLimit (i4FsMIStdOspfv3IfIndex,
                                                   u4SetValFsMIStdOspfv3IfDemandNbrProbeRetxLimit));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfDemandNbrProbeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbeInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                             UINT4
                                             u4SetValFsMIStdOspfv3IfDemandNbrProbeInterval)
{
    return (nmhSetOspfv3IfDemandNbrProbeInterval (i4FsMIStdOspfv3IfIndex,
                                                  u4SetValFsMIStdOspfv3IfDemandNbrProbeInterval));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3IfStatus
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                setValFsMIStdOspfv3IfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3IfStatus (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 i4SetValFsMIStdOspfv3IfStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3IfStatus (i4FsMIStdOspfv3IfIndex,
                                     i4SetValFsMIStdOspfv3IfStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfIndex (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                INT4 i4SetValFsMIStdOspfv3VirtIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3VirtIfIndex (u4FsMIStdOspfv3VirtIfAreaId,
                                        u4FsMIStdOspfv3VirtIfNeighbor,
                                        i4SetValFsMIStdOspfv3VirtIfIndex);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfTransitDelay (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                       UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                       INT4
                                       i4SetValFsMIStdOspfv3VirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3VirtIfTransitDelay (u4FsMIStdOspfv3VirtIfAreaId,
                                               u4FsMIStdOspfv3VirtIfNeighbor,
                                               i4SetValFsMIStdOspfv3VirtIfTransitDelay);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfRetransInterval (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          i4SetValFsMIStdOspfv3VirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3VirtIfRetransInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                                  u4FsMIStdOspfv3VirtIfNeighbor,
                                                  i4SetValFsMIStdOspfv3VirtIfRetransInterval);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfHelloInterval (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                        UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                        INT4
                                        i4SetValFsMIStdOspfv3VirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3VirtIfHelloInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                                u4FsMIStdOspfv3VirtIfNeighbor,
                                                i4SetValFsMIStdOspfv3VirtIfHelloInterval);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfRtrDeadInterval (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          i4SetValFsMIStdOspfv3VirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3VirtIfRtrDeadInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                                  u4FsMIStdOspfv3VirtIfNeighbor,
                                                  i4SetValFsMIStdOspfv3VirtIfRtrDeadInterval);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3VirtIfStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIStdOspfv3VirtIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3VirtIfStatus (INT4 i4FsMIStdOspfv3ContextId,
                                 UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                 UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                 INT4 i4SetValFsMIStdOspfv3VirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3VirtIfStatus (u4FsMIStdOspfv3VirtIfAreaId,
                                  u4FsMIStdOspfv3VirtIfNeighbor,
                                  i4SetValFsMIStdOspfv3VirtIfStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3NbmaNbrPriority
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                setValFsMIStdOspfv3NbmaNbrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3NbmaNbrPriority (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdOspfv3NbmaNbrAddress,
                                    INT4 i4SetValFsMIStdOspfv3NbmaNbrPriority)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3NbmaNbrPriority (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                            i4FsMIStdOspfv3NbmaNbrAddressType,
                                            pFsMIStdOspfv3NbmaNbrAddress,
                                            i4SetValFsMIStdOspfv3NbmaNbrPriority);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3NbmaNbrStorageType
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                setValFsMIStdOspfv3NbmaNbrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3NbmaNbrStorageType (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                       INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdOspfv3NbmaNbrAddress,
                                       INT4
                                       i4SetValFsMIStdOspfv3NbmaNbrStorageType)
{
    return (nmhSetOspfv3NbmaNbrStorageType (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                            i4FsMIStdOspfv3NbmaNbrAddressType,
                                            pFsMIStdOspfv3NbmaNbrAddress,
                                            i4SetValFsMIStdOspfv3NbmaNbrStorageType));
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3NbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                setValFsMIStdOspfv3NbmaNbrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3NbmaNbrStatus (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                  INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdOspfv3NbmaNbrAddress,
                                  INT4 i4SetValFsMIStdOspfv3NbmaNbrStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetOspfv3NbmaNbrStatus (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                          i4FsMIStdOspfv3NbmaNbrAddressType,
                                          pFsMIStdOspfv3NbmaNbrAddress,
                                          i4SetValFsMIStdOspfv3NbmaNbrStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                setValFsMIStdOspfv3AreaAggregateEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaAggregateEffect (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregateAreaID,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregatePrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdOspfv3AreaAggregatePrefix,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                        INT4
                                        i4SetValFsMIStdOspfv3AreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaAggregateEffect (u4FsMIStdOspfv3AreaAggregateAreaID,
                                         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                         i4FsMIStdOspfv3AreaAggregatePrefixType,
                                         pFsMIStdOspfv3AreaAggregatePrefix,
                                         u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                         i4SetValFsMIStdOspfv3AreaAggregateEffect);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaAggregateRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                setValFsMIStdOspfv3AreaAggregateRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaAggregateRouteTag (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4
                                          u4FsMIStdOspfv3AreaAggregateAreaID,
                                          INT4
                                          i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                          INT4
                                          i4FsMIStdOspfv3AreaAggregatePrefixType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdOspfv3AreaAggregatePrefix,
                                          UINT4
                                          u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                          INT4
                                          i4SetValFsMIStdOspfv3AreaAggregateRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaAggregateRouteTag (u4FsMIStdOspfv3AreaAggregateAreaID,
                                           i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                           i4FsMIStdOspfv3AreaAggregatePrefixType,
                                           pFsMIStdOspfv3AreaAggregatePrefix,
                                           u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                           i4SetValFsMIStdOspfv3AreaAggregateRouteTag);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                setValFsMIStdOspfv3AreaAggregateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaAggregateStatus (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregateAreaID,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregatePrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdOspfv3AreaAggregatePrefix,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                        INT4
                                        i4SetValFsMIStdOspfv3AreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaAggregateStatus (u4FsMIStdOspfv3AreaAggregateAreaID,
                                         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                         i4FsMIStdOspfv3AreaAggregatePrefixType,
                                         pFsMIStdOspfv3AreaAggregatePrefix,
                                         u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                         i4SetValFsMIStdOspfv3AreaAggregateStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/*-------------------------------- fsmioslw.c ------------------------------ */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3GlobalTraceLevel
 Input       :  The Indices

                The Object 
                setValFsMIOspfv3GlobalTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3GlobalTraceLevel (INT4 i4SetValFsMIOspfv3GlobalTraceLevel)
{
    gV3OsRtr.u4GblTrcValue = (UINT4) i4SetValFsMIOspfv3GlobalTraceLevel;
#ifdef SNMP_3_WANTED
    V3UtilMsrForScalarObj (i4SetValFsMIOspfv3GlobalTraceLevel,
                           FsMIOspfv3GlobalTraceLevel,
                           (sizeof (FsMIOspfv3GlobalTraceLevel) /
                            sizeof (UINT4)), OSPFV3_FALSE);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VrfSpfInterval
 Input       :  The Indices

                The Object 
                setValFsMIOspfv3VrfSpfInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VrfSpfInterval (INT4 i4SetValFsMIOspfv3VrfSpfInterval)
{
    gV3OsRtr.u4VrfSpfInterval = (UINT4) i4SetValFsMIOspfv3VrfSpfInterval;
#ifdef SNMP_3_WANTED
    V3UtilMsrForScalarObj (i4SetValFsMIOspfv3VrfSpfInterval,
                           FsMIOspfv3VrfSpfInterval,
                           (sizeof (FsMIOspfv3VrfSpfInterval) /
                            sizeof (UINT4)), OSPFV3_FALSE);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object 
                setValFsMIOspfv3RTStaggeringStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RTStaggeringStatus (INT4 i4SetValFsMIOspfv3RTStaggeringStatus)
{
    nmhSetFutOspfv3RTStaggeringStatus (i4SetValFsMIOspfv3RTStaggeringStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3TraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3TraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3TraceLevel (INT4 i4FsMIStdOspfv3ContextId,
                            INT4 i4SetValFsMIOspfv3TraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3TraceLevel (i4SetValFsMIOspfv3TraceLevel);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ABRType
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3ABRType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ABRType (INT4 i4FsMIStdOspfv3ContextId,
                         INT4 i4SetValFsMIOspfv3ABRType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3ABRType (i4SetValFsMIOspfv3ABRType);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3NssaAsbrDefRtTrans
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3NssaAsbrDefRtTrans (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4SetValFsMIOspfv3NssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3NssaAsbrDefRtTrans
        (i4SetValFsMIOspfv3NssaAsbrDefRtTrans);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3DefaultPassiveInterface
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3DefaultPassiveInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3DefaultPassiveInterface (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         i4SetValFsMIOspfv3DefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3DefaultPassiveInterface
        (i4SetValFsMIOspfv3DefaultPassiveInterface);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3SpfDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3SpfDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3SpfDelay (INT4 i4FsMIStdOspfv3ContextId,
                          INT4 i4SetValFsMIOspfv3SpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3SpfDelay (i4SetValFsMIOspfv3SpfDelay);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3SpfHoldTime
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3SpfHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3SpfHoldTime (INT4 i4FsMIStdOspfv3ContextId,
                             INT4 i4SetValFsMIOspfv3SpfHoldTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3SpfHoldTime (i4SetValFsMIOspfv3SpfHoldTime);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RTStaggeringInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RTStaggeringInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RTStaggeringInterval (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4
                                      i4SetValFsMIOspfv3RTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RTStaggeringInterval
        (i4SetValFsMIOspfv3RTStaggeringInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RestartStrictLsaChecking
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RestartStrictLsaChecking (INT4 i4FsMIStdOspfv3ContextId,
                                          INT4
                                          i4SetValFsMIOspfv3RestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RestartStrictLsaChecking
        (i4SetValFsMIOspfv3RestartStrictLsaChecking);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3HelperSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3HelperSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3HelperSupport (INT4 i4FsMIStdOspfv3ContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMIOspfv3HelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3HelperSupport (pSetValFsMIOspfv3HelperSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3HelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3HelperGraceTimeLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3HelperGraceTimeLimit (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4
                                      i4SetValFsMIOspfv3HelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3HelperGraceTimeLimit
        (i4SetValFsMIOspfv3HelperGraceTimeLimit);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RestartAckState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RestartAckState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RestartAckState (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4SetValFsMIOspfv3RestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RestartAckState (i4SetValFsMIOspfv3RestartAckState);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3GraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3GraceLsaRetransmitCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3GraceLsaRetransmitCount (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         i4SetValFsMIOspfv3GraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3GraceLsaRetransmitCount
        (i4SetValFsMIOspfv3GraceLsaRetransmitCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RestartReason
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RestartReason (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4SetValFsMIOspfv3RestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3RestartReason (i4SetValFsMIOspfv3RestartReason);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3ExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ExtTraceLevel (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4SetValFsMIOspfv3ExtTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3ExtTraceLevel (i4SetValFsMIOspfv3ExtTraceLevel);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3SetTraps
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3SetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3SetTraps (INT4 i4FsMIStdOspfv3ContextId,
                          INT4 i4SetValFsMIOspfv3SetTraps)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3SetTraps (i4SetValFsMIOspfv3SetTraps);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3BfdStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                setValFsMIOspfv3BfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3BfdStatus (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4SetValFsMIOspfv3BfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3BfdStatus (i4SetValFsMIOspfv3BfdStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3BfdAllIfState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                setValFsMIOspfv3BfdAllIfState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3BfdAllIfState (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4SetValFsMIOspfv3BfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3BfdAllIfState (i4SetValFsMIOspfv3BfdAllIfState);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RouterIdPermanence
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                setValFsMIOspfv3RouterIdPermanence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RouterIdPermanence (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4SetValFsMIOspfv3RouterIdPermanence)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RouterIdPermanence
        (i4SetValFsMIOspfv3RouterIdPermanence);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                setValFsMIOspfv3IfLinkLSASuppression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfLinkLSASuppression (INT4 i4FsMIOspfv3IfIndex,
                                      INT4
                                      i4SetValFsMIOspfv3IfLinkLSASuppression)
{

    return (nmhSetFutOspfv3IfLinkLSASuppression
            (i4FsMIOspfv3IfIndex, i4SetValFsMIOspfv3IfLinkLSASuppression));

}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfBfdState
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                setValFsMIOspfv3IfBfdState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfBfdState (INT4 i4FsMIOspfv3IfIndex,
                            INT4 i4SetValFsMIOspfv3IfBfdState)
{

    return (nmhSetFutOspfv3IfBfdState
            (i4FsMIOspfv3IfIndex, i4SetValFsMIOspfv3IfBfdState));

}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfPassive
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                setValFsMIOspfv3IfPassive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfPassive (INT4 i4FsMIOspfv3IfIndex,
                           INT4 i4SetValFsMIOspfv3IfPassive)
{
    return (nmhSetFutOspfv3IfPassive (i4FsMIOspfv3IfIndex,
                                      i4SetValFsMIOspfv3IfPassive));
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                setValFsMIOspfv3AsExternalAggregationEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3AsExternalAggregationEffect (INT4 i4FsMIStdOspfv3ContextId,
                                             INT4
                                             i4FsMIOspfv3AsExternalAggregationNetType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIOspfv3AsExternalAggregationNet,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationPfxLength,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationAreaId,
                                             INT4
                                             i4SetValFsMIOspfv3AsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3AsExternalAggregationEffect
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         i4SetValFsMIOspfv3AsExternalAggregationEffect);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                setValFsMIOspfv3AsExternalAggregationTranslation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3AsExternalAggregationTranslation (INT4 i4FsMIStdOspfv3ContextId,
                                                  INT4
                                                  i4FsMIOspfv3AsExternalAggregationNetType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIOspfv3AsExternalAggregationNet,
                                                  UINT4
                                                  u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                  UINT4
                                                  u4FsMIOspfv3AsExternalAggregationAreaId,
                                                  INT4
                                                  i4SetValFsMIOspfv3AsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3AsExternalAggregationTranslation
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         i4SetValFsMIOspfv3AsExternalAggregationTranslation);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                setValFsMIOspfv3AsExternalAggregationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3AsExternalAggregationStatus (INT4 i4FsMIStdOspfv3ContextId,
                                             INT4
                                             i4FsMIOspfv3AsExternalAggregationNetType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIOspfv3AsExternalAggregationNet,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationPfxLength,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationAreaId,
                                             INT4
                                             i4SetValFsMIOspfv3AsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3AsExternalAggregationStatus
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         i4SetValFsMIOspfv3AsExternalAggregationStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RedistRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                setValFsMIOspfv3RedistRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RedistRouteMetric (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3RedistRouteDest,
                                   UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                   INT4 i4SetValFsMIOspfv3RedistRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RedistRouteMetric (i4FsMIOspfv3RedistRouteDestType,
                                          pFsMIOspfv3RedistRouteDest,
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          i4SetValFsMIOspfv3RedistRouteMetric);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RedistRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                setValFsMIOspfv3RedistRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RedistRouteMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3RedistRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3RedistRouteDest,
                                       UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                       INT4
                                       i4SetValFsMIOspfv3RedistRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RedistRouteMetricType (i4FsMIOspfv3RedistRouteDestType,
                                              pFsMIOspfv3RedistRouteDest,
                                              u4FsMIOspfv3RedistRoutePfxLength,
                                              i4SetValFsMIOspfv3RedistRouteMetricType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RedistRouteTagType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                setValFsMIOspfv3RedistRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RedistRouteTagType (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4FsMIOspfv3RedistRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3RedistRouteDest,
                                    UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                    INT4 i4SetValFsMIOspfv3RedistRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RedistRouteTagType (i4FsMIOspfv3RedistRouteDestType,
                                           pFsMIOspfv3RedistRouteDest,
                                           u4FsMIOspfv3RedistRoutePfxLength,
                                           i4SetValFsMIOspfv3RedistRouteTagType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RedistRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                setValFsMIOspfv3RedistRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RedistRouteTag (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3RedistRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3RedistRouteDest,
                                UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                INT4 i4SetValFsMIOspfv3RedistRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RedistRouteTag (i4FsMIOspfv3RedistRouteDestType,
                                       pFsMIOspfv3RedistRouteDest,
                                       u4FsMIOspfv3RedistRoutePfxLength,
                                       i4SetValFsMIOspfv3RedistRouteTag);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RedistRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                setValFsMIOspfv3RedistRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RedistRouteStatus (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3RedistRouteDest,
                                   UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                   INT4 i4SetValFsMIOspfv3RedistRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RedistRouteStatus (i4FsMIOspfv3RedistRouteDestType,
                                          pFsMIOspfv3RedistRouteDest,
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          i4SetValFsMIOspfv3RedistRouteStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RRDStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RRDStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RRDStatus (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4SetValFsMIOspfv3RRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3RRDStatus (i4SetValFsMIOspfv3RRDStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RRDSrcProtoMask
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RRDSrcProtoMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RRDSrcProtoMask (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4SetValFsMIOspfv3RRDSrcProtoMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RRDSrcProtoMask (i4SetValFsMIOspfv3RRDSrcProtoMask);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RRDRouteMapName
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3RRDRouteMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RRDRouteMapName (INT4 i4FsMIStdOspfv3ContextId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValFsMIOspfv3RRDRouteMapName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RRDRouteMapName (pSetValFsMIOspfv3RRDRouteMapName);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                setValFsMIOspfv3DistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3DistInOutRouteMapValue (INT4 i4FsMIStdOspfv3ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3DistInOutRouteMapName,
                                        INT4 i4FsMIOspfv3DistInOutRouteMapType,
                                        INT4
                                        i4SetValFsMIOspfv3DistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3DistInOutRouteMapValue (pFsMIOspfv3DistInOutRouteMapName,
                                               i4FsMIOspfv3DistInOutRouteMapType,
                                               i4SetValFsMIOspfv3DistInOutRouteMapValue);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                setValFsMIOspfv3DistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3DistInOutRouteMapRowStatus (INT4 i4FsMIStdOspfv3ContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIOspfv3DistInOutRouteMapName,
                                            INT4
                                            i4FsMIOspfv3DistInOutRouteMapType,
                                            INT4
                                            i4SetValFsMIOspfv3DistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3DistInOutRouteMapRowStatus
        (pFsMIOspfv3DistInOutRouteMapName, i4FsMIOspfv3DistInOutRouteMapType,
         i4SetValFsMIOspfv3DistInOutRouteMapRowStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3PreferenceValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                setValFsMIOspfv3PreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3PreferenceValue (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4SetValFsMIOspfv3PreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspf3PreferenceValue (i4SetValFsMIOspfv3PreferenceValue);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfCryptoAuthType
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                setValFsMIOspfv3IfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfCryptoAuthType (INT4 i4FsMIOspfv3IfIndex,
                                  INT4 i4SetValFsMIOspfv3IfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3IfCryptoAuthType ((INT4) i4FsMIOspfv3IfIndex,
                                                i4SetValFsMIOspfv3IfCryptoAuthType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                setValFsMIOspfv3IfCryptoAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfCryptoAuthMode (INT4 i4FsMIOspfv3IfIndex,
                                  INT4 i4SetValFsMIOspfv3IfCryptoAuthMode)
{
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3IfCryptoAuthMode ((INT4) i4FsMIOspfv3IfIndex,
                                                i4SetValFsMIOspfv3IfCryptoAuthMode);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIOspfv3VirtIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfCryptoAuthType (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                      UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                      INT4
                                      i4SetValFsMIOspfv3VirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3VirtIfCryptoAuthType (u4FsMIStdOspfv3VirtIfAreaId,
                                                    u4FsMIStdOspfv3VirtIfNeighbor,
                                                    i4SetValFsMIOspfv3VirtIfCryptoAuthType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                setValFsMIOspfv3VirtIfCryptoAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfCryptoAuthMode (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                      UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                      INT4
                                      i4SetValFsMIOspfv3VirtIfCryptoAuthMode)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3VirtIfCryptoAuthMode (u4FsMIStdOspfv3VirtIfAreaId,
                                                    u4FsMIStdOspfv3VirtIfNeighbor,
                                                    i4SetValFsMIOspfv3VirtIfCryptoAuthMode);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKey (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4FsMIOspfv3IfAuthIfIndex,
                           INT4 i4FsMIOspfv3IfAuthKeyId,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsMIOspfv3IfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKey (i4FsMIOspfv3IfAuthIfIndex,
                                         i4FsMIOspfv3IfAuthKeyId,
                                         pSetValFsMIOspfv3IfAuthKey);

    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKeyStartAccept (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4FsMIOspfv3IfAuthIfIndex,
                                      INT4 i4FsMIOspfv3IfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pSetValFsMIOspfv3IfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKeyStartAccept (i4FsMIOspfv3IfAuthIfIndex,
                                                    i4FsMIOspfv3IfAuthKeyId,
                                                    pSetValFsMIOspfv3IfAuthKeyStartAccept);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKeyStartGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                        INT4 i4FsMIOspfv3IfAuthIfIndex,
                                        INT4 i4FsMIOspfv3IfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValFsMIOspfv3IfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKeyStartGenerate (i4FsMIOspfv3IfAuthIfIndex,
                                                      i4FsMIOspfv3IfAuthKeyId,
                                                      pSetValFsMIOspfv3IfAuthKeyStartGenerate);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKeyStopGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3IfAuthIfIndex,
                                       INT4 i4FsMIOspfv3IfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValFsMIOspfv3IfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKeyStopGenerate (i4FsMIOspfv3IfAuthIfIndex,
                                                     i4FsMIOspfv3IfAuthKeyId,
                                                     pSetValFsMIOspfv3IfAuthKeyStopGenerate);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKeyStopAccept (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4FsMIOspfv3IfAuthIfIndex,
                                     INT4 i4FsMIOspfv3IfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pSetValFsMIOspfv3IfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKeyStopAccept (i4FsMIOspfv3IfAuthIfIndex,
                                                   i4FsMIOspfv3IfAuthKeyId,
                                                   pSetValFsMIOspfv3IfAuthKeyStopAccept);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                setValFsMIOspfv3IfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3IfAuthKeyStatus (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4FsMIOspfv3IfAuthIfIndex,
                                 INT4 i4FsMIOspfv3IfAuthKeyId,
                                 INT4 i4SetValFsMIOspfv3IfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }
    i1Return = nmhSetFutOspfv3IfAuthKeyStatus (i4FsMIOspfv3IfAuthIfIndex,
                                               i4FsMIOspfv3IfAuthKeyId,
                                               i4SetValFsMIOspfv3IfAuthKeyStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIStdOspfv3AreaDfInfOriginate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object
                setValFsMIStdOspfv3AreaDfInfOriginate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIStdOspfv3AreaDfInfOriginate (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       INT4
                                       i4SetValFsMIStdOspfv3AreaDfInfOriginate)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetOspfv3AreaDfInfOriginate (u4FsMIStdOspfv3AreaId,
                                        i4SetValFsMIStdOspfv3AreaDfInfOriginate);
    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKey (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                               UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                               INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                               tSNMP_OCTET_STRING_TYPE
                               * pSetValFsMIOspfv3VirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3VirtIfAuthKey (u4FsMIOspfv3VirtIfAuthAreaId,
                                             u4FsMIOspfv3VirtIfAuthNeighbor,
                                             i4FsMIOspfv3VirtIfAuthKeyId,
                                             pSetValFsMIOspfv3VirtIfAuthKey);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStartAccept (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                          INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pSetValFsMIOspfv3VirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3VirtIfAuthKeyStartAccept (u4FsMIOspfv3VirtIfAuthAreaId,
                                                 u4FsMIOspfv3VirtIfAuthNeighbor,
                                                 i4FsMIOspfv3VirtIfAuthKeyId,
                                                 pSetValFsMIOspfv3VirtIfAuthKeyStartAccept);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStartGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                            UINT4
                                            u4FsMIOspfv3VirtIfAuthNeighbor,
                                            INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValFsMIOspfv3VirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3VirtIfAuthKeyStartGenerate (u4FsMIOspfv3VirtIfAuthAreaId,
                                                   u4FsMIOspfv3VirtIfAuthNeighbor,
                                                   i4FsMIOspfv3VirtIfAuthKeyId,
                                                   pSetValFsMIOspfv3VirtIfAuthKeyStartGenerate);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStopGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                           UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                           INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pSetValFsMIOspfv3VirtIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3VirtIfAuthKeyStopGenerate (u4FsMIOspfv3VirtIfAuthAreaId,
                                                  u4FsMIOspfv3VirtIfAuthNeighbor,
                                                  i4FsMIOspfv3VirtIfAuthKeyId,
                                                  pSetValFsMIOspfv3VirtIfAuthKeyStopGenerate);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStopAccept (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                         UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                         INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValFsMIOspfv3VirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3VirtIfAuthKeyStopAccept (u4FsMIOspfv3VirtIfAuthAreaId,
                                                u4FsMIOspfv3VirtIfAuthNeighbor,
                                                i4FsMIOspfv3VirtIfAuthKeyId,
                                                pSetValFsMIOspfv3VirtIfAuthKeyStopAccept);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                setValFsMIOspfv3VirtIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStatus (INT4 i4FsMIStdOspfv3ContextId,
                                     UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                     UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                     INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                     INT4 i4SetValFsMIOspfv3VirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhSetFutOspfv3VirtIfAuthKeyStatus (u4FsMIOspfv3VirtIfAuthAreaId,
                                                   u4FsMIOspfv3VirtIfAuthNeighbor,
                                                   i4FsMIOspfv3VirtIfAuthKeyId,
                                                   i4SetValFsMIOspfv3VirtIfAuthKeyStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RRDMetricValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                setValFsMIOspfv3RRDMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RRDMetricValue (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3RRDProtocolId,
                                INT4 i4SetValFsMIOspfv3RRDMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RRDMetricValue
        (i4FsMIOspfv3RRDProtocolId, i4SetValFsMIOspfv3RRDMetricValue);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3RRDMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                setValFsMIOspfv3RRDMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3RRDMetricType (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIOspfv3RRDProtocolId,
                               INT4 i4SetValFsMIOspfv3RRDMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3RRDMetricType
        (i4FsMIOspfv3RRDProtocolId, i4SetValFsMIOspfv3RRDMetricType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ClearProcess
 Input       :  The Indices

                The Object
                setValFsMIOspfv3ClearProcess
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ClearProcess (INT4 i4SetValFsMIOspfv3ClearProcess)
{
    nmhSetFutOspfv3ClearProcess (i4SetValFsMIOspfv3ClearProcess);
    return SNMP_SUCCESS;
}
