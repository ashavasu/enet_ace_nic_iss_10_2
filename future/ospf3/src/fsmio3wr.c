/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fsmio3wr.c,v 1.6 2017/12/26 13:34:25 siva Exp $
 * *
 * * Description: Protocol Mib Data base
 * *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmio3lw.h"
# include  "fsmio3wr.h"
# include  "fsmio3db.h"
INT4
FsMIOspfv3GlobalTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3GlobalTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3VrfSpfIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3VrfSpfInterval (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3RTStaggeringStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3RTStaggeringStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3HotStandbyAdminStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3HotStandbyAdminStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3HotStandbyStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3HotStandbyState (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3DynamicBulkUpdStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3DynamicBulkUpdStatus
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3StandbyHelloSyncCountGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3StandbyHelloSyncCount
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsMIOspfv3StandbyLsaSyncCountGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3StandbyLsaSyncCount (&(pMultiData->u4_ULongValue)));
}

INT4
FsMIOspfv3ClearProcessGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMIOspfv3ClearProcess (&(pMultiData->i4_SLongValue)));
}

INT4
FsMIOspfv3GlobalTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfv3GlobalTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3VrfSpfIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfv3VrfSpfInterval (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3RTStaggeringStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfv3RTStaggeringStatus (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3ClearProcessSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMIOspfv3ClearProcess (pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3GlobalTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfv3GlobalTraceLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3ClearProcessTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfv3ClearProcess
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3VrfSpfIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfv3VrfSpfInterval
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3RTStaggeringStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMIOspfv3RTStaggeringStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMIOspfv3GlobalTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3GlobalTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfv3ClearProcessDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3ClearProcess
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfv3VrfSpfIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3VrfSpfInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfv3RTStaggeringStatusDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3RTStaggeringStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3Table (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3Table
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3Table
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3OverFlowStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3OverFlowState (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3TraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3TraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3ABRTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3ABRType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3NssaAsbrDefRtTransGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3NssaAsbrDefRtTrans
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3DefaultPassiveInterfaceGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3DefaultPassiveInterface
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3SpfDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3SpfDelay (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3SpfHoldTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3SpfHoldTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RTStaggeringIntervalGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RTStaggeringInterval
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RestartStrictLsaCheckingGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RestartStrictLsaChecking
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3HelperSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3HelperSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3HelperGraceTimeLimitGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3HelperGraceTimeLimit
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RestartAckStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RestartAckState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3GraceLsaRetransmitCountGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3GraceLsaRetransmitCount
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RestartReasonGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RestartReason (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3ExtTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3ExtTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3SetTrapsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3SetTraps (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3BfdStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3BfdAllIfStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BfdAllIfState (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RouterIdPermanenceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3Table
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouterIdPermanence
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3TraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3TraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ABRTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3ABRType (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3NssaAsbrDefRtTransSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3NssaAsbrDefRtTrans
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3DefaultPassiveInterfaceSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3DefaultPassiveInterface
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3SpfDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3SpfDelay (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3SpfHoldTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3SpfHoldTime (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RTStaggeringIntervalSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RTStaggeringInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RestartStrictLsaCheckingSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RestartStrictLsaChecking
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3HelperSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3HelperSupport (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3HelperGraceTimeLimitSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3HelperGraceTimeLimit
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RestartAckStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RestartAckState
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3GraceLsaRetransmitCountSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3GraceLsaRetransmitCount
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RestartReasonSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RestartReason (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3ExtTraceLevel (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3SetTrapsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3SetTraps (pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3BfdStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3BfdStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3BfdAllIfStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3BfdAllIfState (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RouterIdPermanenceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RouterIdPermanence
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3TraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3TraceLevel (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ABRTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3ABRType (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3NssaAsbrDefRtTransTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3NssaAsbrDefRtTrans (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3DefaultPassiveInterfaceTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3DefaultPassiveInterface (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsMIOspfv3SpfDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3SpfDelay (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3SpfHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3SpfHoldTime (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RTStaggeringIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RTStaggeringInterval (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfv3RestartStrictLsaCheckingTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RestartStrictLsaChecking (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));

}

INT4
FsMIOspfv3HelperSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3HelperSupport (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3HelperGraceTimeLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3HelperGraceTimeLimit (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfv3RestartAckStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RestartAckState (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3GraceLsaRetransmitCountTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3GraceLsaRetransmitCount (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
FsMIOspfv3RestartReasonTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RestartReason (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3ExtTraceLevel (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3SetTrapsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3SetTraps (pu4Error,
                                         pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3BfdStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3BfdStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3BfdAllIfStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3BfdAllIfState (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RouterIdPermanenceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RouterIdPermanence (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3TableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3Table (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3IfTable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3IfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3IfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3IfOperStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfOperState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfPassiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfNbrCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfNbrCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfAdjCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAdjCount (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfHelloRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfHelloRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfHelloTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfHelloTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfHelloDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfHelloDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfDdpRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfDdpRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfDdpTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfDdpTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfDdpDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfDdpDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLrqRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLrqRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLrqTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLrqTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLrqDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLrqDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLsuRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLsuRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLsuTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLsuTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLsuDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLsuDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLakRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLakRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLakTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLakTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfLakDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLakDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfContextId (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfLinkLSASuppressionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfLinkLSASuppression
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfPassiveSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfPassive (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfLinkLSASuppressionSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfLinkLSASuppression
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfBfdStateSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfPassiveTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfPassive (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfLinkLSASuppressionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfLinkLSASuppression (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfv3IfBfdStateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfBfdState (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3IfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3RoutingTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3RoutingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3RoutingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3RouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RouteAreaIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouteAreaId (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         pMultiIndex->pIndex[5].pOctetStrValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3RouteCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouteCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiIndex->pIndex[4].i4_SLongValue,
                                       pMultiIndex->pIndex[5].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RouteType2CostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouteType2Cost
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RouteInterfaceIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RoutingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RouteInterfaceIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfv3AsExternalAggregationTable (tSnmpIndex * pFirstMultiIndex,
                                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3AsExternalAggregationTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].u4_ULongValue,
             &(pNextMultiIndex->pIndex[4].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3AsExternalAggregationEffectGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3AsExternalAggregationEffect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3AsExternalAggregationTranslationGet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3AsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3AsExternalAggregationStatusGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3AsExternalAggregationStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3AsExternalAggregationEffectSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3AsExternalAggregationEffect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationTranslationSet (tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3AsExternalAggregationTranslation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationStatusSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3AsExternalAggregationStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationEffectTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3AsExternalAggregationEffect (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            pOctetStrValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[4].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationTranslationTest (UINT4 *pu4Error,
                                                tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3AsExternalAggregationTranslation (pu4Error,
                                                                 pMultiIndex->
                                                                 pIndex[0].
                                                                 i4_SLongValue,
                                                                 pMultiIndex->
                                                                 pIndex[1].
                                                                 i4_SLongValue,
                                                                 pMultiIndex->
                                                                 pIndex[2].
                                                                 pOctetStrValue,
                                                                 pMultiIndex->
                                                                 pIndex[3].
                                                                 u4_ULongValue,
                                                                 pMultiIndex->
                                                                 pIndex[4].
                                                                 u4_ULongValue,
                                                                 pMultiData->
                                                                 i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationStatusTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3AsExternalAggregationStatus (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[2].
                                                            pOctetStrValue,
                                                            pMultiIndex->
                                                            pIndex[3].
                                                            u4_ULongValue,
                                                            pMultiIndex->
                                                            pIndex[4].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            i4_SLongValue));

}

INT4
FsMIOspfv3AsExternalAggregationTableDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3AsExternalAggregationTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3BRRouteTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3BRRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3BRRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue,
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3BRRouteTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3BRRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BRRouteType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3BRRouteAreaIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3BRRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BRRouteAreaId (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].i4_SLongValue,
                                           pMultiIndex->pIndex[3].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[4].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3BRRouteCostGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3BRRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BRRouteCost (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].i4_SLongValue,
                                         pMultiIndex->pIndex[3].pOctetStrValue,
                                         pMultiIndex->pIndex[4].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3BRRouteInterfaceIndexGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3BRRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3BRRouteInterfaceIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfv3RedistRouteCfgTable (tSnmpIndex * pFirstMultiIndex,
                                           tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3RedistRouteCfgTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3RedistRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RedistRouteMetric
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RedistRouteMetricTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RedistRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RedistRouteTagTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RedistRouteTagType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RedistRouteTagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RedistRouteTag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RedistRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RedistRouteStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RedistRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RedistRouteMetric
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteMetricTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RedistRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteTagTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RedistRouteTagType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteTagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RedistRouteTag
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RedistRouteStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RedistRouteMetric (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RedistRouteMetricType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteTagTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RedistRouteTagType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteTagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RedistRouteTag (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RedistRouteStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[2].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RedistRouteCfgTableDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3RedistRouteCfgTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3RRDRouteTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3RRDRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3RRDRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3RRDStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RRDStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RRDSrcProtoMaskGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RRDSrcProtoMask
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RRDRouteMapNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RRDRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3RRDStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RRDStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDSrcProtoMaskSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RRDSrcProtoMask
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDRouteMapNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RRDRouteMapName
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3RRDStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RRDStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDSrcProtoMaskTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RRDSrcProtoMask (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDRouteMapNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RRDRouteMapName (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3RRDRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3RRDRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3RRDMetricTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3RRDMetricTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3RRDMetricTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3RRDMetricValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RRDMetricTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RRDMetricValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RRDMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3RRDMetricTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3RRDMetricType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3RRDMetricValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RRDMetricValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3RRDMetricType (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDMetricValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RRDMetricValue (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3RRDMetricType (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3RRDMetricTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3RRDMetricTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3DistInOutRouteMapTable (tSnmpIndex * pFirstMultiIndex,
                                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3DistInOutRouteMapValueGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3DistInOutRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3DistInOutRouteMapValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3DistInOutRouteMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3DistInOutRouteMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3DistInOutRouteMapValueSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3DistInOutRouteMapValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3DistInOutRouteMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3DistInOutRouteMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3DistInOutRouteMapValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3DistInOutRouteMapValue (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
FsMIOspfv3DistInOutRouteMapRowStatusTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           pOctetStrValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
FsMIOspfv3DistInOutRouteMapTableDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3DistInOutRouteMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3PreferenceTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3PreferenceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3PreferenceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3PreferenceValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3PreferenceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3PreferenceValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3PreferenceValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3PreferenceValue
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3PreferenceValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3PreferenceValue (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3PreferenceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3PreferenceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3NeighborBfdTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3NeighborBfdTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3NeighborBfdTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3NbrBfdStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3NeighborBfdTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3NbrBfdState (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexFsMIOspfv3IfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3IfAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3IfAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3IfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiIndex->pIndex[2].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKey (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiIndex->pIndex[2].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKeyStartAccept (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[2].
                                                     i4_SLongValue,
                                                     pMultiData->
                                                     pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStartGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKeyStartGenerate (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[1].
                                                       i4_SLongValue,
                                                       pMultiIndex->pIndex[2].
                                                       i4_SLongValue,
                                                       pMultiData->
                                                       pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopGenerateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKeyStopGenerate (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[2].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStopAcceptTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKeyStopAccept (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiData->
                                                    pOctetStrValue));

}

INT4
FsMIOspfv3IfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfAuthKeyStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[2].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3IfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3VirtIfAuthTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3VirtIfAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3VirtIfAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3VirtIfAuthKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartAcceptGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartGenerateGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopGenerateGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopAcceptGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3VirtIfAuthKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartAcceptSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKeyStartAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartGenerateSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKeyStartGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopGenerateSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKeyStopGenerate
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopAcceptSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKeyStopAccept
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfAuthKeyStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKey (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[3].
                                              i4_SLongValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartAcceptTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKeyStartAccept (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[2].
                                                         u4_ULongValue,
                                                         pMultiIndex->pIndex[3].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStartGenerateTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKeyStartGenerate (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[3].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopGenerateTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKeyStopGenerate (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue,
                                                          pMultiIndex->
                                                          pIndex[1].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[2].
                                                          u4_ULongValue,
                                                          pMultiIndex->
                                                          pIndex[3].
                                                          i4_SLongValue,
                                                          pMultiData->
                                                          pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStopAcceptTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKeyStopAccept (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[3].
                                                        i4_SLongValue,
                                                        pMultiData->
                                                        pOctetStrValue));

}

INT4
FsMIOspfv3VirtIfAuthKeyStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3VirtIfAuthTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3VirtIfAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3VirtIfCryptoAuthTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3VirtIfCryptoAuthTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3VirtIfCryptoAuthTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3VirtIfCryptoAuthTypeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfCryptoAuthType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3VirtIfCryptoAuthModeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3VirtIfCryptoAuthTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3VirtIfCryptoAuthMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3VirtIfCryptoAuthTypeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfCryptoAuthType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3VirtIfCryptoAuthModeSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3VirtIfCryptoAuthMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3VirtIfCryptoAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfCryptoAuthType (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfv3VirtIfCryptoAuthModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3VirtIfCryptoAuthMode (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
FsMIOspfv3IfAuthTxedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthTxed (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfAuthRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthRcvd (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3IfAuthDisdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfAuthDisd (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
FsMIOspfv3VirtIfCryptoAuthTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3VirtIfCryptoAuthTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMIOspfv3IfCryptoAuthTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfCryptoAuthType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfCryptoAuthModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3IfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3IfCryptoAuthMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3IfCryptoAuthTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfCryptoAuthType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfCryptoAuthModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3IfCryptoAuthMode
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfCryptoAuthTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfCryptoAuthType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3IfCryptoAuthModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3IfCryptoAuthMode (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

VOID
RegisterFSMIO3 ()
{
    SNMPRegisterMib (&FsMIOspfv3IfAuthTableOID,
                     &FsMIOspfv3IfAuthTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3VirtIfAuthTableOID,
                     &FsMIOspfv3VirtIfAuthTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3VirtIfCryptoAuthTableOID,
                     &FsMIOspfv3VirtIfCryptoAuthTableEntry, SNMP_MSR_TGR_FALSE);

    SNMPRegisterMib (&FsMIOspfv3TableOID, &FsMIOspfv3TableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3IfTableOID, &FsMIOspfv3IfTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3RoutingTableOID, &FsMIOspfv3RoutingTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3AsExternalAggregationTableOID,
                     &FsMIOspfv3AsExternalAggregationTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3BRRouteTableOID, &FsMIOspfv3BRRouteTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3RedistRouteCfgTableOID,
                     &FsMIOspfv3RedistRouteCfgTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3RRDRouteTableOID, &FsMIOspfv3RRDRouteTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3RRDMetricTableOID,
                     &FsMIOspfv3RRDMetricTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3DistInOutRouteMapTableOID,
                     &FsMIOspfv3DistInOutRouteMapTableEntry,
                     SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3PreferenceTableOID,
                     &FsMIOspfv3PreferenceTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3NeighborBfdTableOID,
                     &FsMIOspfv3NeighborBfdTableEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3GlobalTraceLevelOID,
                     &FsMIOspfv3GlobalTraceLevelEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3VrfSpfIntervalOID,
                     &FsMIOspfv3VrfSpfIntervalEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3RTStaggeringStatusOID,
                     &FsMIOspfv3RTStaggeringStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3HotStandbyAdminStatusOID,
                     &FsMIOspfv3HotStandbyAdminStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3HotStandbyStateOID,
                     &FsMIOspfv3HotStandbyStateEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3DynamicBulkUpdStatusOID,
                     &FsMIOspfv3DynamicBulkUpdStatusEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3StandbyHelloSyncCountOID,
                     &FsMIOspfv3StandbyHelloSyncCountEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3StandbyLsaSyncCountOID,
                     &FsMIOspfv3StandbyLsaSyncCountEntry, SNMP_MSR_TGR_FALSE);
    SNMPRegisterMib (&FsMIOspfv3ClearProcessOID, &FsMIOspfv3ClearProcessEntry,
                     SNMP_MSR_TGR_FALSE);

    SNMPAddSysorEntry (&fsmio3OID, (const UINT1 *) "fsmio3");
}

VOID
UnRegisterFSMIO3 ()
{
    SNMPUnRegisterMib (&FsMIOspfv3TableOID, &FsMIOspfv3TableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3IfTableOID, &FsMIOspfv3IfTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3RoutingTableOID,
                       &FsMIOspfv3RoutingTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3AsExternalAggregationTableOID,
                       &FsMIOspfv3AsExternalAggregationTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3BRRouteTableOID,
                       &FsMIOspfv3BRRouteTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3RedistRouteCfgTableOID,
                       &FsMIOspfv3RedistRouteCfgTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3RRDRouteTableOID,
                       &FsMIOspfv3RRDRouteTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3RRDMetricTableOID,
                       &FsMIOspfv3RRDMetricTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3DistInOutRouteMapTableOID,
                       &FsMIOspfv3DistInOutRouteMapTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3PreferenceTableOID,
                       &FsMIOspfv3PreferenceTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3NeighborBfdTableOID,
                       &FsMIOspfv3NeighborBfdTableEntry);
    SNMPUnRegisterMib (&FsMIOspfv3GlobalTraceLevelOID,
                       &FsMIOspfv3GlobalTraceLevelEntry);
    SNMPUnRegisterMib (&FsMIOspfv3VrfSpfIntervalOID,
                       &FsMIOspfv3VrfSpfIntervalEntry);
    SNMPUnRegisterMib (&FsMIOspfv3RTStaggeringStatusOID,
                       &FsMIOspfv3RTStaggeringStatusEntry);
    SNMPUnRegisterMib (&FsMIOspfv3HotStandbyAdminStatusOID,
                       &FsMIOspfv3HotStandbyAdminStatusEntry);
    SNMPUnRegisterMib (&FsMIOspfv3HotStandbyStateOID,
                       &FsMIOspfv3HotStandbyStateEntry);
    SNMPUnRegisterMib (&FsMIOspfv3DynamicBulkUpdStatusOID,
                       &FsMIOspfv3DynamicBulkUpdStatusEntry);
    SNMPUnRegisterMib (&FsMIOspfv3StandbyHelloSyncCountOID,
                       &FsMIOspfv3StandbyHelloSyncCountEntry);
    SNMPUnRegisterMib (&FsMIOspfv3StandbyLsaSyncCountOID,
                       &FsMIOspfv3StandbyLsaSyncCountEntry);
    SNMPUnRegisterMib (&FsMIOspfv3ClearProcessOID,
                       &FsMIOspfv3ClearProcessEntry);

    SNMPDelSysorEntry (&fsmio3OID, (const UINT1 *) "fsmio3");
}
