/********************************************************************
 *       Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *   
 *         $Id: fso3tewr.c,v 1.6 2017/12/26 13:34:26 siva Exp $
 *    
 *          Description:
 *      
 *********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fso3telw.h"
# include  "fso3tewr.h"
# include  "fso3tedb.h"
# include  "o3inc.h"

INT4
GetNextIndexFutOspfv3TestIfTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfv3TestIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfv3TestIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSO3TE ()
{
    SNMPRegisterMibWithContextIdAndLock
        (&fso3teOID, &fso3teEntry, V3OspfLock, V3OspfUnLock,
         V3UtilSetContext, V3UtilReSetContext, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fso3teOID, (const UINT1 *) "futOspfv3TestGroup");
}

VOID
UnRegisterFSO3TE ()
{
    SNMPUnRegisterMib (&fso3teOID, &fso3teEntry);
    SNMPDelSysorEntry (&fso3teOID, (const UINT1 *) "futOspfv3TestGroup");
}

INT4
FutOspfv3TestDemandTrafficGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfv3TestIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfv3TestDemandTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfv3TestDemandTrafficSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfv3TestDemandTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfv3TestDemandTrafficTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfv3TestDemandTraffic (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
FutOspfv3TestIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfv3TestIfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFutOspfv3ExtRouteTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFutOspfv3ExtRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFutOspfv3ExtRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FutOspfv3ExtRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfv3ExtRouteMetric (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfv3ExtRouteMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfv3ExtRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfv3ExtRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFutOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFutOspfv3ExtRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           &(pMultiData->i4_SLongValue)));

}

INT4
FutOspfv3ExtRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfv3ExtRouteMetric (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfv3ExtRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFutOspfv3ExtRouteStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].i4_SLongValue,
                                           pMultiIndex->pIndex[4].
                                           pOctetStrValue,
                                           pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfv3ExtRouteMetric (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[3].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[4].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfv3ExtRouteMetricType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[4].
                                                  pOctetStrValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FutOspfv3ExtRouteStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[2].
                                              u4_ULongValue,
                                              pMultiIndex->pIndex[3].
                                              i4_SLongValue,
                                              pMultiIndex->pIndex[4].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
FutOspfv3ExtRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FutOspfv3ExtRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
