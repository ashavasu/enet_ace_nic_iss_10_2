/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3grapi.c,v 1.13 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains the API used by restarting router
 *              during Graceful restart process.
 *
 *******************************************************************/
#include "o3inc.h"
#include "ospf3wr.h"
#include "fsos3wr.h"
#include "fsmio3wr.h"
#include "fsmisowr.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiIfDelete                                            */
/*                                                                           */
/* Description  : This routine deletes the interface, all the neighbors      */
/*                attached to this interface and clears the memory           */
/*                                                                           */
/* Input        : pInterface  -  pointer to the interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
O3GrApiIfDelete (tV3OsInterface * pInterface)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pTmpPrefNode = NULL;
    UINT4               u4ContextId = 0;

    if (pInterface == NULL)
    {
        return;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: O3GrApiIfDelete\r\n");

    u4ContextId = pInterface->pArea->pV3OspfCxt->u4ContextId;

    /* Disable the interface */
    O3GrApiIfDisable (pInterface);

    /* Delete the interface from all the data strucutres */
    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));

    if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
    {

        TMO_SLL_Delete (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                        &(pInterface->nextVirtIfNode));
    }
    else
    {
        RBTreeRemove (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface);
    }

    /* Delete the Prefix List */
    OSPFV3_DYNM_SLL_SCAN (&pInterface->ip6AddrLst, pPrefixNode, pTmpPrefNode,
                          tV3OsPrefixNode *)
    {
        TMO_SLL_Delete (&(pInterface->ip6AddrLst),
                        &(pPrefixNode->nextPrefixNode));

        OSPFV3_PREFIX_FREE (pPrefixNode);
    }

    RBTreeDelete (pInterface->pLinkScopeLsaRBRoot);
    /* helloTimer, helloTimer pollTimer interface specific timers are stopped 
     * in V3IsmDisableIfTimers
     */

    /* Stopping the remaining timers */
    V3TmrDeleteTimer (&(pInterface->MultIfActiveDetectTimer));
    V3TmrDeleteTimer (&(pInterface->nbrProbeTimer));
    OSPFV3_IF_FREE (pInterface);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: O3GrApiIfDelete\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiIfDeleteAllInCxt                                    */
/*                                                                           */
/* Description  : This routine deletes all the interfaces attached           */
/*                to this instance. The deletion of each interface in turn   */
/*                deletes all the neighbors attached                         */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
O3GrApiIfDeleteAllInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:O3GrApiIfDeleteAllInCxt\r\n");

    while ((pInterface = (tV3OsInterface *)
            RBTreeGetFirst (gV3OsRtr.pIfRBRoot)) != NULL)
    {
        O3GrApiIfDelete (pInterface);
    }

    TMO_DYN_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode,
                      pTmpLstNode, tTMO_SLL_NODE *)
    {
        pInterface =
            OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);
        if (pInterface != NULL)
        {
            O3GrApiIfDelete (pInterface);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrApiIfDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiAreaDelete                                          */
/*                                                                           */
/* Description  : This routine deletes the particular area                   */
/*                                                                           */
/* Input        : pArea    - pointer to OSPF area                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
O3GrApiAreaDelete (tV3OsArea * pArea)
{
    tV3OsHost          *pHost = NULL;
    tV3OsHost          *pTempHost = NULL;
    tV3OsAsExtAddrRange *pScanExtRng = NULL;
    tV3OsAsExtAddrRange *pTempScanRng = NULL;
    tV3OsAddrRange     *pScanIntRng = NULL;
    tV3OsAddrRange     *pTempIntRng = NULL;
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: O3GrApiAreaDelete\r\n");

    u4ContextId = pArea->pV3OspfCxt->u4ContextId;

    OSPFV3_DYNM_SLL_SCAN (&(pArea->asExtAddrRangeLst), pScanExtRng,
                          pTempScanRng, tV3OsAsExtAddrRange *)
    {
        TMO_SLL_Delete (&(pArea->pV3OspfCxt->asExtAddrRangeLst),
                        &(pScanExtRng->nextAsExtAddrRngInRtr));
        /* Only active range is present in
           respective area Lst
         */
        if ((pScanExtRng->rangeStatus != NOT_READY) &&
            (V3UtilAreaIdComp (pScanExtRng->areaId, OSPFV3_BACKBONE_AREAID)
             == OSPFV3_EQUAL))
        {
            TMO_SLL_Delete (&(pArea->asExtAddrRangeLst),
                            &(pScanExtRng->nextAsExtAddrRange));
        }

        gV3OsRtr.pExtAggrTableCache = NULL;

        OSPFV3_AS_EXT_AGG_FREE (pScanExtRng);
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type7AddrRangeLst), pScanExtRng,
                          pTempScanRng, tV3OsAsExtAddrRange *)
    {

        TMO_SLL_Delete (&(pArea->pV3OspfCxt->asExtAddrRangeLst),
                        &(pScanExtRng->nextAsExtAddrRngInRtr));

        if ((pScanExtRng->rangeStatus != NOT_READY) &&
            (V3UtilAreaIdComp (pScanExtRng->areaId, OSPFV3_BACKBONE_AREAID)
             != OSPFV3_EQUAL))
        {
            TMO_SLL_Delete (&(pArea->type7AddrRangeLst),
                            &(pScanExtRng->nextAsExtAddrRange));
        }

    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type3AggrLst), pScanIntRng,
                          pTempIntRng, tV3OsAddrRange *)
    {
        TMO_SLL_Delete (&(pArea->type3AggrLst), &(pScanIntRng->nextAddrRange));
        OSPFV3_AREA_AGG_FREE (pScanIntRng);
    }

    OSPFV3_DYNM_SLL_SCAN (&(pArea->type7AggrLst), pScanIntRng,
                          pTempIntRng, tV3OsAddrRange *)
    {
        TMO_SLL_Delete (&(pArea->type7AggrLst), &(pScanIntRng->nextAddrRange));
        OSPFV3_AREA_AGG_FREE (pScanIntRng);
    }
    /* Delete host routes which falls in this area */
    pHost = (tV3OsHost *) RBTreeGetFirst (pArea->pV3OspfCxt->pHostRBRoot);

    while (pHost != NULL)
    {
        pTempHost =
            (tV3OsHost *) RBTreeGetNext (pArea->pV3OspfCxt->pHostRBRoot,
                                         (tRBElem *) pHost, NULL);
        if (pHost->pArea == pArea)
        {
            V3HostDelete (pHost);
        }
        pHost = pTempHost;
    }

    RBTreeDelete (pArea->pAreaScopeLsaRBRoot);

    TMO_HASH_Delete_Table (pArea->pRtrLsaHashTable, NULL);
    TMO_HASH_Delete_Table (pArea->pIntraPrefixLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pInterPrefixLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pInterRouterLsaTable, NULL);
    TMO_HASH_Delete_Table (pArea->pNssaLsaHashTable, NULL);

    TMO_SLL_Delete (&(pArea->pV3OspfCxt->areasLst), &(pArea->nextArea));

    gV3OsRtr.pAggrTableCache = NULL;

    /* This is done while deleting the back_bone area during
     * shut down operation.
     */
    if (V3UtilAreaIdComp (pArea->areaId, pArea->pV3OspfCxt->pBackbone->areaId)
        == OSPFV3_EQUAL)
    {
        pArea->pV3OspfCxt->pBackbone = NULL;
    }

    if (pArea->pSpf != NULL)
    {
        V3RtcClearSpfTree (pArea->pSpf);
        RBTreeDelete (pArea->pSpf);
    }
    V3TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT:O3GrApiAreaDelete\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiAreaDeleteAllInCxt                                  */
/*                                                                           */
/* Description  : This routine deletes all the areas                         */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
O3GrApiAreaDeleteAllInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3GrApiAreaDeleteAllInCxt\r\n");

    TMO_DYN_SLL_Scan (&(pV3OspfCxt->areasLst), pLstNode,
                      pTmpLstNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pLstNode);
        if (V3UtilAreaIdComp (pArea->areaId, pV3OspfCxt->pBackbone->areaId)
            != OSPFV3_EQUAL)
        {
            /* Delete the area */
            O3GrApiAreaDelete (pArea);
        }
    }

    /* To delete the back_bone area if the router is internal router */
    if (pV3OspfCxt->pBackbone != NULL)
    {
        O3GrApiAreaDelete (pV3OspfCxt->pBackbone);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrApiAreaDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiLsaDeleteAllInCxt                                   */
/*                                                                           */
/* Description  : This routine deletes all the LSA present in this           */
/*                instance. This routine does not flush the LSA from         */
/*                the routing domain                                         */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrApiLsaDeleteAllInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :O3GrApiLsaDeleteAllInCxt\r\n");
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pLstNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pLstNode);
        TMO_SLL_Scan (&(pArea->ifsInArea), pTmpLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pTmpLstNode);

            /* Delete Link Local Scope Lsas */
            while ((pLsaInfo = (tV3OsLsaInfo *)
                    RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot)) != NULL)
            {
                O3GrDeleteLsaFromDatabase (pLsaInfo);
            }
        }

        /* Delete Area Scope Lsas */
        while ((pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot)) != NULL)
        {
            O3GrDeleteLsaFromDatabase (pLsaInfo);
        }
    }

    /* Delete AS Scope Lsas */
    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot)) != NULL)
    {
        O3GrDeleteLsaFromDatabase (pLsaInfo);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :O3GrApiLsaDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiRtDeleteAllInCxt                                    */
/*                                                                           */
/* Description  : This routine deletes all the routes from OSPF              */
/*                trie and SLL. It does not give an indication to RTM        */
/*                regarding the route deletion                               */
/*                                                                           */
/* Input        : pV3OspfRt - pointer to Routing table                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrApiRtDeleteAllInCxt (tV3OspfRt * pV3OspfRt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    VOID               *pTemp = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pTempPtr = NULL;

    MEMSET (&inParams, OSPFV3_INIT_VAL, sizeof (tInputParams));
    MEMSET (&outParams, OSPFV3_INIT_VAL, sizeof (tOutputParams));
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrApiRtDeleteAllInCxt\r\n");

    while ((pRtEntry = (tV3OsRtEntry *)
            TMO_SLL_Get (&(pV3OspfRt->routesList))) != NULL)
    {
        V3RtcFreeRtEntry (pRtEntry);
    }
    /* Deleting NETWORK route entries */
    pRtEntry = NULL;
    inParams.pRoot = pV3OspfRt->pOspfV3Root;
    inParams.i1AppId = pV3OspfRt->i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = OSPFV3_INIT_VAL;
    inParams.Key.pKey = NULL;

    while (TrieGetFirstNode (&inParams, &pTempPtr,
                             (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;

        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.u1PrefixLen = pRtEntry->u1PrefixLen;
        inParams.pLeafNode = NULL;
        outParams.Key.pKey = NULL;
        outParams.pAppSpecInfo = NULL;

        /* Deleting from Trie */
        if (TrieRemove (&inParams, &outParams, pTemp) != TRIE_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to delete the route entry from trie\n"));

            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "Failed to delete the route entry from trie\n");
        }

        /* Deleting paths and Freeing the route entry */
        V3RtcFreeRtEntry (pRtEntry);
        pRtEntry = NULL;
    }
    TMO_SLL_Init (&(pV3OspfRt->routesList));
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, " EXIT :O3GrApiRtDeleteAllInCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiIfDisable                                           */
/*                                                                           */
/* Description  : This routine disables the interface, all the neighbors     */
/*                attached to this interface                                 */
/*                                                                           */
/* Input        : pInterface  -  pointer to the interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrApiIfDisable (tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    UINT4               u4ContextId;

    u4ContextId = pInterface->pArea->pV3OspfCxt->u4ContextId;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId, "ENTER :O3GrApiIfDisable\r\n");

    if ((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
        (pInterface->u1IsmState == OSPFV3_IFS_BACKUP))
    {
        V3OspfLeaveMcastGroup (&gV3OsAllDRtrs, pInterface->u4InterfaceId,
                               pInterface->pArea->pV3OspfCxt->u4ContextId);
    }

    if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
    {
        V3OspfLeaveMcastGroup (&(gV3OsAllSpfRtrs), pInterface->u4InterfaceId,
                               pInterface->pArea->pV3OspfCxt->u4ContextId);
    }
    pInterface->u1IsmState = OSPFV3_IFS_DOWN;

    /* Reset Variables */
    V3IsmResetVariables (pInterface);

    /* Disable Interface Timers */
    V3IsmDisableIfTimers (pInterface);

    /* Scan and delete all the neighbors */
    while ((pNbrNode = TMO_SLL_Next (&(pInterface->nbrsInIf), pPrevNode))
           != NULL)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        pNbr->bIsBfdDisable = OSIX_TRUE;
        /* Delete the neighbor and clear the memory. This deletes
         * the inactivity timer, clears the request list and deletes
         * the request timer, clears the update packets and deletes
         * the rxmt timer, clears the ddp packets and deletes the
         *  */
        V3NbrDelete (pNbr);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: O3GrApiIfDisable\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiDeleteCxt                                           */
/*                                                                           */
/* Description  : This routine deletes all the LSDB, neighbors,              */
/*                interfaces, routes and timers in this context.             */
/*                The module does not flush the LSA throughout the domain,   */
/*                does not de-register from RTM and does not send any        */
/*                indication to RTM during route deletion.                   */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrApiDeleteCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OspfRt          *pV3OspfRt = NULL;
    UINT4               u4ContextId = pV3OspfCxt->u4ContextId;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId, "ENTER :O3GrApiDeleteCxt\r\n");

    /* Disable the context */
    O3GrDisableInCxt (pV3OspfCxt);

    /* Delete all the interfaces (includeing virtual interfaces.
     * This in turn deletes all the neighbors */
    O3GrApiIfDeleteAllInCxt (pV3OspfCxt);

    /* Delete the RRD config table */
    V3RtmRrdConfRtInfoDeleteAllInCxt (pV3OspfCxt);

    /* Delete all the areas attached to the instance */
    O3GrApiAreaDeleteAllInCxt (pV3OspfCxt);

    /* De-initialise the routing table trie instance */
    pV3OspfRt = &pV3OspfCxt->ospfV3RtTable;
    V3OspfDeleteRtTable (pV3OspfRt->pOspfV3Root);
    V3OspfDeleteRtTable (pV3OspfCxt->pExtRtRoot);

    if (pV3OspfCxt->pHostRBRoot != NULL)
    {
        RBTreeDelete (pV3OspfCxt->pHostRBRoot);
        pV3OspfCxt->pHostRBRoot = NULL;
    }
    if (pV3OspfCxt->pCandteLst != NULL)
    {
        TMO_HASH_Delete_Table (pV3OspfCxt->pCandteLst, NULL);
        pV3OspfCxt->pCandteLst = NULL;
    }

    RBTreeDelete (pV3OspfCxt->pAsScopeLsaRBRoot);
    pV3OspfCxt->pAsScopeLsaRBRoot = NULL;

    RBTreeDelete (pV3OspfCxt->IfLeakRouteRBTree);
    pV3OspfCxt->IfLeakRouteRBTree = NULL;

    TMO_HASH_Delete_Table (pV3OspfCxt->pExtLsaHashTable, NULL);
    pV3OspfCxt->pExtLsaHashTable = NULL;
    V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));
    V3TmrDeleteTimer (&(pV3OspfCxt->exitOverflowTimer));
    V3TmrDeleteTimer (&(pV3OspfCxt->graceTimer));
    MEMSET (pV3OspfCxt, OSPFV3_INIT_VAL, sizeof (tV3OspfCxt));
    OSPFV3_CXT_FREE (pV3OspfCxt);
    gV3OsRtr.apV3OspfCxt[u4ContextId] = NULL;

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT :O3GrApiDeleteCxt\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiFlushUnwantedSelfLsa                                */
/*                                                                           */
/* Description  : This routine flushes all the unwanted self                 */
/*                originated LSA                                             */
/*                                                                           */
/* Input        : pV3OspfCxt - pointer to OSPFv3 context                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrApiFlushUnwantedSelfLsa (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pIfLstNode = NULL;
    tV3OsArea          *pArea = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: O3GrApiFlushUnwantedSelfLsa\r\n");

    /* The GR LSA bit in each of the self-orignated LSA will be set
     * if the LSA is not re-originated. Remove those LSA and flush them from
     * the routing domain */
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pLstNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pLstNode);

        TMO_SLL_Scan (&(pArea->ifsInArea), pIfLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfLstNode);

            /* Flush unwanted self originated link scope lsas */
            pLsaInfo =
                (tV3OsLsaInfo *) RBTreeGetFirst
                (pInterface->pLinkScopeLsaRBRoot);

            while (pLsaInfo != NULL)
            {
                pNextLsaInfo =
                    (tV3OsLsaInfo *) RBTreeGetNext
                    (pInterface->pLinkScopeLsaRBRoot,
                     (tRBElem *) pLsaInfo, NULL);

                if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                      OSPFV3_RTR_ID (pV3OspfCxt))
                     == OSPFV3_EQUAL) &&
                    (OSPFV3_IS_LSA_REFRESH_BIT_SET
                     (pLsaInfo, OSPFV3_GR_LSA_BIT) ||
                     (pLsaInfo->lsaId.u2LsaType == OSPFV3_GRACE_LSA)))
                {
                    V3AgdFlushOut (pLsaInfo);
                }
                pLsaInfo = pNextLsaInfo;
            }
        }

        /* Flush unwanted self originated area scope lsas */
        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

        while (pLsaInfo != NULL)
        {
            pNextLsaInfo =
                (tV3OsLsaInfo *) RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                                (tRBElem *) pLsaInfo, NULL);
            if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                  OSPFV3_RTR_ID (pV3OspfCxt))
                 == OSPFV3_EQUAL) &&
                (OSPFV3_IS_LSA_REFRESH_BIT_SET (pLsaInfo, OSPFV3_GR_LSA_BIT)))
            {
                V3AgdFlushOut (pLsaInfo);
            }
            pLsaInfo = pNextLsaInfo;
        }
    }
    /* Flush unwanted self originated AS Scope Lsas */
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);
        if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) &&
            (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                              OSPFV3_RTR_ID (pV3OspfCxt))
             == OSPFV3_EQUAL) &&
            (OSPFV3_IS_LSA_REFRESH_BIT_SET (pLsaInfo, OSPFV3_GR_LSA_BIT)))
        {
            V3AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrApiFlushUnwantedSelfLsa\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiTaskDeInit                                          */
/*                                                                           */
/* Description  : This routine clears all the memory                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrApiTaskDeInit (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrApiTaskDeInit\r\n");

#if  (defined (SNMP_2_WANTED)) || (defined (SNMPV3_WANTED))
    UnRegisterOSPF3 ();
    UnRegisterFSOS3 ();
    UnRegisterFSMIO3 ();
    UnRegisterFSMISO ();
#endif

    /* Delete Timer List */
    V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
    V3TmrDeleteTimer (&(gV3OsRtr.g50msTimerNode));
    TmrDeleteTimerList (gV3OsRtr.timerLstId);

    TmrDeleteTimerList (gV3OsRtr.hellotimerLstId);
    TmrDeleteTimerList (gV3OsRtr.g50msTimerLst);
    if (gV3OsRtr.pIfRBRoot != NULL)
    {
        RBTreeDelete (gV3OsRtr.pIfRBRoot);
    }

    if (gV3OsRtr.pRtrLsaCheck != NULL)
    {
        RBTreeDelete (gV3OsRtr.pRtrLsaCheck);
    }

    if (gV3OsRtr.pNwLsaCheck != NULL)
    {
        RBTreeDelete (gV3OsRtr.pNwLsaCheck);
    }

    V3OspfMemClear ();
    V3OspfUnLock ();
    OsixSemDel (OSPF3_MUL_EXCL_SEM_ID);
    OsixSemDel (OSPF3_RTM_LST_SEM_ID);
    OsixQueDel (OSPF3_Q_ID);
    OsixQueDel (OSPF3_RM_Q_ID);
    OsixQueDel (OSPF3_LOW_PRIO_Q_ID);
#ifdef OSPF3_BCMXNP_HELLO_WANTED
    OsixQueDel (OSPF3_NP_HELLO_Q_ID);
#endif
    /* Delete OSPFv3 Task */
    OsixDeleteTask (SELF, (CONST UINT1 *) "OSV3");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiIfDisableRBTAction                                  */
/*                                                                           */
/* Description  : Disabling  each Intreface which are maintained in          */
/*                in the form of  RB Tree                                    */
/* Input        : pNode: Pintre to RB Tree Element                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Retun the Number of Intrefaces                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
O3GrApiIfDisableRBTAction (tRBElem * pNode, eRBVisit visit, UINT4 u4Level,
                           VOID *arg, VOID *out)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:O3GrApiIfDisableRBTAction\r\n");

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (out);
    UNUSED_PARAM (arg);

    if ((visit == leaf) || (visit == postorder))
    {
        if (pNode != NULL)
        {
            O3GrApiIfDisable ((tV3OsInterface *) pNode);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:O3GrApiIfDisableRBTAction\r\n");

    return (RB_WALK_CONT);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrApiConstAndSendGraceLsa                                */
/*                                                                           */
/* Description  : Contruct and Send Grace Lsa in given interface             */
/* Input        : pInterface                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrApiConstAndSendGraceLsa (tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;
    UINT1              *pLsa = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    UINT1              *pOspfPkt = NULL;
    UINT4               u4LsaCount = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER:O3GrApiConstAndSendGraceLsa\r\n");

    if ((pInterface->lsUpdate.pLsuPkt == NULL) &&
        (pInterface->lsUpdate.pLsuPkt =
         V3UtilOsMsgAlloc (OSPFV3_IFACE_MTU (pInterface))) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for LS Update packet\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Alloc failed for LS Update Pkt \n");
        return;
    }
    pInterface->lsUpdate.u2CurrentLen =
        OSPFV3_HEADER_SIZE + OSPFV3_LSU_FIXED_PORTION_SIZE;
    pInterface->lsUpdate.u2LsaCount = 1;

    /* Calculate OSPFv3 Pkt Ptr */
    pOspfPkt = pInterface->lsUpdate.pLsuPkt + IPV6_HEADER_LEN;

    /* Contruct Grace Lsa with MAX_AGE */
    pLsa = V3ConstructGraceLsa (pInterface, OSPFV3_MAX_SEQ_NUM);
    if (pLsa == NULL)
    {
        V3LsuClearUpdate (&pInterface->lsUpdate);
        pInterface->lsUpdate.pLsuPkt = NULL;
        return;
    }

    OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt, pLsa,
                                 pInterface->lsUpdate.u2CurrentLen,
                                 OSPFV3_GRACE_LSA_SIZE);

    pInterface->lsUpdate.u2CurrentLen += OSPFV3_GRACE_LSA_SIZE;

    u4LsaCount = (UINT4) pInterface->lsUpdate.u2LsaCount;
    OSPFV3_BUFFER_ASSIGN_4_BYTE (pOspfPkt, OSPFV3_HEADER_SIZE, u4LsaCount);

    V3UtilConstructHdr (pInterface, pOspfPkt,
                        OSPFV3_LS_UPDATE_PKT,
                        pInterface->lsUpdate.u2CurrentLen);

    if (pInterface->u1NetworkType != OSPFV3_IF_BROADCAST)
    {
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

            if (pNbr->u1NsmState >= OSPFV3_NBRS_EXCHANGE)
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &(pNbr->nbrIpv6Addr));
            }
        }
    }
    else
    {
        if (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)
        {

            if ((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                (pInterface->u1IsmState == OSPFV3_IFS_BACKUP))
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &gV3OsAllSpfRtrs);
            }
            else
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &gV3OsAllDRtrs);
            }
        }
    }
    V3LsuClearUpdate (&pInterface->lsUpdate);
    pInterface->lsUpdate.pLsuPkt = NULL;
    OSPFV3_EXT_LSA_FREE (pLsa);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT:O3GrApiConstAndSendGraceLsa\r\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3grapi.c                      */
/*-----------------------------------------------------------------------*/
