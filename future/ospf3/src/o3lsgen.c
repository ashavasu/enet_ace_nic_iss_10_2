/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lsgen.c,v 1.24 2018/01/25 10:04:16 siva Exp $
 *
 * Description:This file contains procedures for generating 
 *             all the LSAs
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE INT4        V3IsEligibleToGenLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT2 u2InternalType));

PRIVATE UINT2 V3SetLsaType PROTO ((UINT2 u2InternalType));

PRIVATE INT4 V3GetSeqNum PROTO ((tV3OsLsaDesc * pLsaDesc));

PRIVATE UINT1 V3IsEgblToGenIndicationLsa PROTO ((tV3OsArea * pArea));

/****************************************************************************/
/*                                                                          */
/* Function        :  V3GenerateLsa                                         */
/*                                                                          */
/* Description     :  generates lsa if minimum lsa interval expired, else   */
/*                    updates lsa changed flag.                             */
/*                                                                          */
/* Input           :  pArea          : area to which lsa is generated       */
/*                    u2InternalType : internal type of lsa                 */
/*                    pLsId          : link state id                        */
/*                    pPtr           : pointer to the structure associated  */
/*                                     with the lsa.                        */
/*                    u1RefreshFlag  : Refresh Flag.                        */
/*                                                                          */
/* Output          :   None                                                 */
/*                                                                          */
/* Returns         :   None                                                 */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
V3GenerateLsa (tV3OsArea * pArea, UINT2 u2InternalType,
               tV3OsLinkStateId * pLsId, UINT1 *pPtr, UINT1 u1RefreshFlag)
{

    UINT2               u2LsaType = 0;
    tV3OsLsaDesc       *pLsaDesc = NULL;
    UINT1              *pLsa = NULL;
    tV3OsLsaSeqNum      i4SeqNum = 0;
    UINT1              *pTmpPtr = NULL;
    UINT1               u1LsaChngdFlag = OSIX_TRUE;
    tV3OsArea          *pNssaArea = NULL;
    tV3OsLsaInfo       *pNssaLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface     *pStandbyInterface = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr;
    UINT1              *pOspfPkt = NULL;
    UINT1               u1LinkSup = OSPFV3_INIT_VAL;
    UINT4               u4LsaCount = 0;
    INT4                i4RetCode = OSIX_FAILURE;
    UINT2               u2LsaAge;

    if ((pPtr == NULL) &&
        ((u2InternalType == OSPFV3_AS_EXT_LSA) ||
         (u2InternalType == OSPFV3_NSSA_LSA) ||
         (u2InternalType == OSPFV3_COND_NSSA_LSA) ||
         (u2InternalType == OSPFV3_AS_TRNSLTD_EXT_LSA) ||
         (u2InternalType == OSPFV3_AS_TRNSLTD_RNG_LSA) ||
         (u2InternalType == OSPFV3_COND_AS_EXT_LSA) ||
         (u2InternalType == OSPFV3_INTER_AREA_PREFIX_LSA) ||
         (u2InternalType == OSPFV3_INTER_AREA_ROUTER_LSA) ||
         (u2InternalType == OSPFV3_COND_INTER_AREA_PREFIX_LSA) ||
         (u2InternalType == OSPFV3_NETWORK_LSA) ||
         (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA) ||
         (u2InternalType == OSPFV3_LINK_LSA) ||
         (u2InternalType == OSPFV3_STANDBY_LINK_LSA) ||
         (u2InternalType == OSPFV3_GRACE_LSA)))
    {
        return;
    }

    if (pArea == NULL)
    {
        /* pArea should not be NULL for all cases. In case of generating
         * AS External LSA for which Area parameter is not required, pArea
         * is used to access the context structure
         */
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "No area specified."
                      " New LSA origination failure for : %x,"
                      " linkStateId: %x\n", u2InternalType,
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pLsId)));

        OSPFV3_GBL_TRC2 (OSPFV3_LSU_TRC, "No area specified."
                         " New LSA origination failure for : %x,"
                         " linkStateId: %x\n", u2InternalType,
                         OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pLsId));

        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);
        }

        return;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "Enter: V3GenerateLsa\n");

    OSPFV3_TRC2 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "New LSA to be generated for LSA type: %x,"
                 "linkStateId: %x\n", u2InternalType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsId[0]));

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);
        }

        return;
    }

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA cannot be regenerated during GR restart */
        /* In case of unplanned graceful restart, grace LSA should be sent */
        if (!
            ((pArea->pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_UNPLANNED)
             && (u2InternalType == OSPFV3_GRACE_LSA)))
        {
            if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
            {
                OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);
            }

            return;
        }
    }

    if ((u2InternalType == OSPFV3_ROUTER_LSA) &&
        (TMO_SLL_Count (&(pArea->ifsInArea)) == 0))
    {
        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);
        }

        return;
    }

    if ((i4RetCode = V3IsEligibleToGenLsaInCxt (pArea->pV3OspfCxt,
                                                u2InternalType))
        == OSIX_FAILURE)
    {
        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);

        }

        return;
    }

    u2LsaType = V3SetLsaType (u2InternalType);

    if (u2LsaType == OSPFV3_AS_EXT_LSA)
    {
        pTmpPtr = NULL;
    }
    else if ((u2LsaType == OSPFV3_LINK_LSA) || (u2LsaType == OSPFV3_GRACE_LSA))
    {
        pTmpPtr = pPtr;
        pInterface = (tV3OsInterface *) (VOID *) pPtr;
    }
    else
    {
        pTmpPtr = (UINT1 *) pArea;
    }

    if ((pLsaDesc = V3GetLsaDescInCxt (pArea->pV3OspfCxt,
                                       u2LsaType, pLsId, pTmpPtr)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot find lsa descriptor for the advt.\n"));

        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Cannot find lsa desc for the advt. \n");

        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);

        }

        return;
    }

    if (pLsaDesc->pLsaInfo != NULL)
    {
        V3TmrDeleteTimer (&(pLsaDesc->pLsaInfo->lsaAgingTimer));
    }

    /* If the internal type give as input and the internal type
     * present in the LSA descriptor are not same, then
     * free the allocated summary param present in LSA Desc
     * if the internal type is OSPFV3_INTER_AREA_PREFIX_LSA or
     * OSPFV3_INTER_AREA_ROUTER_LSA
     */
    if (pLsaDesc->u2InternalLsaType != u2InternalType)
    {
        if ((pLsaDesc->u2InternalLsaType == OSPFV3_INTER_AREA_PREFIX_LSA) ||
            (pLsaDesc->u2InternalLsaType == OSPFV3_INTER_AREA_ROUTER_LSA))
        {
            if (pLsaDesc->pAssoPrimitive != NULL)
            {
                OSPFV3_SUM_PARAM_FREE (pLsaDesc->pAssoPrimitive);
            }
        }

        pLsaDesc->pAssoPrimitive = NULL;
    }

    pLsaDesc->u2InternalLsaType = u2InternalType;

    /* implementation specific validity conditions */
    if (pLsaDesc->u1SeqNumWrapAround == OSIX_TRUE)
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "This advt has been flooded out"
                    " for seq number wrap around.\n");

        if (u2InternalType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pPtr);

        }
        if (pLsaDesc != NULL)
        {
            /* FASL POSITIVE, CTRL should not reach here, Free the allocated mem for desc */
            KW_FALSEPOSITIVE_FIX (pLsaDesc);
        }
        return;
    }

    /* add associated primitive */
    if ((u2InternalType == OSPFV3_INTER_AREA_PREFIX_LSA) ||
        (u2InternalType == OSPFV3_INTER_AREA_ROUTER_LSA))
    {
        if ((pLsaDesc->pAssoPrimitive == NULL) &&
            (OSPFV3_SUM_PARAM_ALLOC (&(pLsaDesc->pAssoPrimitive)) == NULL))
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for Summary Param\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Summary Param Alloc Failure\n");
            V3LsuClearLsaDesc (pLsaDesc);
            return;
        }
        else
        {
            MEMCPY (pLsaDesc->pAssoPrimitive, pPtr, sizeof (tV3OsSummaryParam));
        }
    }
    else
    {
        pLsaDesc->pAssoPrimitive = pPtr;
    }

    if (pLsaDesc->u1MinLsaIntervalExpired != OSIX_TRUE)
    {
        pLsaDesc->u1LsaChanged = OSIX_TRUE;

        OSPFV3_TRC (OSPFV3_LSU_TRC,
                    pArea->pV3OspfCxt->u4ContextId,
                    "MinLsInterval timer has not fired for this lsa\n");

        /* Send the LSA to the standby node */
        if (pLsaDesc->pLsaInfo != NULL)
        {
            if (!
                ((pLsaDesc->pLsaInfo->pV3OspfCxt->u1RouteLeakStatus ==
                  OSPFV3_TRUE)
                 || (pLsaDesc->pLsaInfo->pV3OspfCxt->u1IfRouteLeakStatus ==
                     OSPFV3_TRUE)))
            {
                if (O3RedDynConstructAndSendLsa (pLsaDesc->pLsaInfo) ==
                    OSIX_FAILURE)
                {
                    SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                                  "LSA sync-up failed. "
                                  "Lsa originated type: %x, link state id: %x, "
                                  "adv rtr id: %x\n",
                                  pLsaDesc->pLsaInfo->lsaId.u2LsaType,
                                  OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                           lsaId.linkStateId),
                                  OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                           lsaId.advRtrId)));

                    OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                                     pLsaDesc->pLsaInfo->pV3OspfCxt->
                                     u4ContextId,
                                     "LSA sync-up failed. "
                                     "Lsa originated type: %x, link state id: %x, "
                                     "adv rtr id: %x\n",
                                     pLsaDesc->pLsaInfo->lsaId.u2LsaType,
                                     OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->
                                                              pLsaInfo->lsaId.
                                                              linkStateId),
                                     OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->
                                                              pLsaInfo->lsaId.
                                                              advRtrId));
                }
            }
        }
        KW_FALSEPOSITIVE_FIX (pLsaDesc);
        return;
    }

    i4SeqNum = V3GetSeqNum (pLsaDesc);

    if (u1RefreshFlag == OSIX_TRUE)
    {
        pLsa = V3RefreshLsaInCxt (pArea->pV3OspfCxt, pLsaDesc, i4SeqNum,
                                  u2LsaType, pLsId);
    }
    else
    {
        /* Construct LSA */
        switch (u2InternalType)
        {
            case OSPFV3_ROUTER_LSA:
                pLsa = V3ConstructRtrLsa (pArea, i4SeqNum, pLsId);
                break;

            case OSPFV3_NETWORK_LSA:
                pLsa =
                    V3ConstructNetworkLsa ((tV3OsInterface *) (VOID *) pPtr,
                                           i4SeqNum);
                break;

            case OSPFV3_INTER_AREA_PREFIX_LSA:
            case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
                pLsa =
                    V3ConstructInterAreaPrefixLsaInCxt
                    (pArea->pV3OspfCxt, u2InternalType, pPtr, i4SeqNum, pLsId);
                break;

            case OSPFV3_INTER_AREA_ROUTER_LSA:
            case OSPFV3_INDICATION_LSA:
                pLsa =
                    V3ConstructInterAreaRouterLsa (pArea, u2InternalType, pPtr,
                                                   i4SeqNum, pLsId);
                break;

            case OSPFV3_AS_EXT_LSA:
            case OSPFV3_AS_TRNSLTD_EXT_LSA:
            case OSPFV3_AS_TRNSLTD_RNG_LSA:
            case OSPFV3_COND_AS_EXT_LSA:
                pLsa =
                    V3ConstructAsExtLsaInCxt (pArea->pV3OspfCxt, pPtr,
                                              u2InternalType, i4SeqNum, pLsId);
                break;

            case OSPFV3_NSSA_LSA:
            case OSPFV3_COND_NSSA_LSA:
            case OSPFV3_DEFAULT_NSSA_LSA:
                pLsa =
                    V3ConstructNssaLsa (pPtr, u2InternalType, pArea, i4SeqNum,
                                        pLsId);
                break;

            case OSPFV3_LINK_LSA:
                pLsa = V3ConstructLinkLsa ((tV3OsInterface *) (VOID *) pPtr,
                                           i4SeqNum, &u1LinkSup);
                break;
            case OSPFV3_STANDBY_LINK_LSA:

                pStandbyInterface = V3IfGetPtrtoInterface (pInterface->pArea,
                                                           V3UtilDwordFromPdu
                                                           (*pLsId));
                if (pStandbyInterface != NULL)
                {

                    pLsa = V3ConstructLinkLsa (pStandbyInterface,
                                               i4SeqNum, &u1LinkSup);
                }
                break;

            case OSPFV3_INTRA_AREA_PREFIX_LSA:
                pLsa = V3ConstructIntraAreaPrefixLsaInCxt (pArea->pV3OspfCxt,
                                                           pPtr, i4SeqNum,
                                                           pLsId);
                break;

            case OSPFV3_GRACE_LSA:
                pLsa = V3ConstructGraceLsa ((tV3OsInterface *) (VOID *) pPtr,
                                            i4SeqNum);
                break;
            default:
                pLsa = (UINT1 *) NULL;
                break;
        }
    }

    /* Check for failure in lsa construction */
    if (pLsa == NULL)
    {
        if (pLsaDesc->pLsaInfo == NULL)
        {
            V3LsuClearLsaDesc (pLsaDesc);
        }
        if (u1LinkSup == OSPFV3_ONE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Link Lsa construction is suppressed. \n"));

            OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Link Lsa construction is suppressed. \n");
        }
        else
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "LSA construction for LSA type : %x failure. \n",
                          u2InternalType));

            OSPFV3_TRC1 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                         "LSA construction for LSA type : %x failure. \n",
                         u2InternalType);
        }
        KW_FALSEPOSITIVE_FIX (pLsaDesc);
        return;
    }

    /* If new advt allocate lsa_info and add to hash table */
    if (pLsaDesc->pLsaInfo == NULL)
    {
        if ((pLsaDesc->pLsaInfo =
             V3LsuAddToLsdb (pArea, u2LsaType, pLsId,
                             &(OSPFV3_RTR_ID (pArea->pV3OspfCxt)),
                             (tV3OsInterface *) (VOID *) pTmpPtr,
                             pLsa)) == NULL)
        {
            OSPFV3_LSA_TYPE_FREE (u2InternalType, pLsa);

            V3LsuClearLsaDesc (pLsaDesc);

            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to Allocate memory for Lsa info \n"));

            OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Lsa Info allocation failure");
            return;
        }
        pLsaDesc->pLsaInfo->pLsaDesc = pLsaDesc;
        TMO_DLL_Add (&(pArea->pV3OspfCxt->lsaDescLst),
                     &(pLsaDesc->nextLsaDesc));
    }

    if (pLsaDesc->pLsaInfo->pLsa != NULL)
    {
        u1LsaChngdFlag = (UINT1) V3LsuCheckActualChange
            (pLsaDesc->pLsaInfo->pLsa, pLsa);
    }

    V3LsuInstallLsa (pLsa, pLsaDesc->pLsaInfo, OSIX_FALSE);

    if (u2InternalType == OSPFV3_AS_EXT_LSA)
    {
        ((tV3OsExtRoute *) (VOID *) (pLsaDesc->pAssoPrimitive))->pLsaInfo =
            pLsaDesc->pLsaInfo;

    }

    if ((u2InternalType == OSPFV3_NSSA_LSA) ||
        (u2InternalType == OSPFV3_COND_NSSA_LSA))
    {
        /* Handling functionally Equ Type 7 */
        if (V3IsEligibleToGenNssaLsa (pLsaDesc->pLsaInfo,
                                      pLsaDesc->pLsaInfo->pArea) ==
            OSIX_FAILURE)
        {
            /* Already added in the list. Should be removed */
            V3LsuDeleteLsaFromDatabase (pLsaDesc->pLsaInfo, OSIX_TRUE);

            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "can't generate ASE LSA, since"
                          "another one exists with same functionality\n"));

            OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "can't generate ASE LSA, since"
                        "another one exists with same functionality\n");
            return;
        }
    }

    if ((u2InternalType == OSPFV3_AS_EXT_LSA) ||
        (u2InternalType == OSPFV3_COND_AS_EXT_LSA))
    {
        pLsaDesc->pLsaInfo->u1TrnsltType5 = OSPFV3_REDISTRIBUTED_TYPE5;
    }
    else if ((u2InternalType == OSPFV3_AS_TRNSLTD_EXT_LSA) ||
             (u2InternalType == OSPFV3_AS_TRNSLTD_RNG_LSA))
    {
        pLsaDesc->pLsaInfo->u1TrnsltType5 = OSIX_TRUE;
    }

    /* Triggering Translation of self originated Type 7 LSA */

    if ((u2InternalType == OSPFV3_NSSA_LSA) ||
        (u2InternalType == OSPFV3_COND_NSSA_LSA) ||
        (u2InternalType == OSPFV3_DEFAULT_NSSA_LSA))
    {
        V3RagNssaType7LsaTranslation (pLsaDesc->pLsaInfo->pArea,
                                      pLsaDesc->pLsaInfo);
        V3RagNssaAdvertiseType7Rngs (pLsaDesc->pLsaInfo->pArea);
    }

    if ((u2InternalType == OSPFV3_AS_EXT_LSA) ||
        (u2InternalType == OSPFV3_COND_AS_EXT_LSA))
    {
        TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pNssaArea, tV3OsArea *)
        {
            if (pNssaArea->u4AreaType == OSPFV3_NSSA_AREA)
            {
                pNssaLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pLsaDesc->pLsaInfo->lsaId.
                                           linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pNssaArea);
                if (pNssaLsaInfo != NULL)
                {
                    pNssaLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
                    V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pNssaLsaInfo->pLsaDesc);
                }
            }
        }
    }
    /* If the router is in unplanned restart state and the LSA
     * to be transmitted is self-originated grace LSA, then
     * transmit the LSA to All SPF routers */
    if ((pArea->pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_UNPLANNED) &&
        (pInterface != NULL))
    {
        /* Add the LSA to the ls update packet in the interface
         * and transmit to all SPF routers */
        if ((pInterface->lsUpdate.pLsuPkt =
             V3UtilOsMsgAlloc (OSPFV3_IFACE_MTU (pInterface))) != NULL)
        {
            pInterface->lsUpdate.u2CurrentLen =
                OSPFV3_HEADER_SIZE + OSPFV3_LSU_FIXED_PORTION_SIZE;
            pInterface->lsUpdate.u2LsaCount = 1;

            /* Calculate OSPFv3 Pkt Ptr */
            pOspfPkt = pInterface->lsUpdate.pLsuPkt + IPV6_HEADER_LEN;

            u2LsaAge = V3LsuGetLsaAge (pLsaDesc->pLsaInfo, pInterface);

            OSPFV3_BUFFER_ASSIGN_STRING (pOspfPkt, pLsaDesc->pLsaInfo->pLsa,
                                         pInterface->lsUpdate.u2CurrentLen,
                                         pLsaDesc->pLsaInfo->u2LsaLen);
            OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt,
                                         pInterface->lsUpdate.u2CurrentLen,
                                         u2LsaAge);

            pInterface->lsUpdate.u2CurrentLen += pLsaDesc->pLsaInfo->u2LsaLen;

            u4LsaCount = (UINT4) pInterface->lsUpdate.u2LsaCount;
            OSPFV3_BUFFER_ASSIGN_4_BYTE (pOspfPkt, OSPFV3_HEADER_SIZE,
                                         u4LsaCount);

            V3UtilConstructHdr (pInterface, pOspfPkt,
                                OSPFV3_LS_UPDATE_PKT,
                                pInterface->lsUpdate.u2CurrentLen);

            if (pInterface->u1NetworkType != OSPFV3_IF_BROADCAST)
            {
                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNbr =
                        OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                             pNbrNode);

                    if (pNbr->u1NsmState >= OSPFV3_NBRS_EXCHANGE)
                    {
                        V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                                      pInterface->lsUpdate.u2CurrentLen,
                                      pInterface, &(pNbr->nbrIpv6Addr));
                    }
                }
            }
            else
            {
                V3PppSendPkt (pInterface->lsUpdate.pLsuPkt,
                              pInterface->lsUpdate.u2CurrentLen, pInterface,
                              &gV3OsAllSpfRtrs);
            }

            V3LsuClearUpdate (&pInterface->lsUpdate);
            pInterface->lsUpdate.pLsuPkt = NULL;
        }
        else
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for LS Update packet\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Alloc failed for LS Update Pkt \n");
        }
    }
    else
    {
        V3LsuFloodOut (pLsaDesc->pLsaInfo, NULL, pArea, u1LsaChngdFlag,
                       pLsaDesc->pLsaInfo->pInterface);

        V3LsuSendAllLsuInCxt (pArea->pV3OspfCxt);

        /* The LSA is sent to the nbr routers. If the LSA is present
         * in any of the rxmt list, send the LSU setting the rxmt
         * flag to true. If ack has not be received before switchover
         * the new active node will re-transmit the LSA
         */
        if (!
            ((pLsaDesc->pLsaInfo->pV3OspfCxt->u1RouteLeakStatus == OSPFV3_TRUE)
             || (pLsaDesc->pLsaInfo->pV3OspfCxt->u1IfRouteLeakStatus ==
                 OSPFV3_TRUE)))
        {
            if ((pLsaDesc->pLsaInfo->u4RxmtCount > 0) &&
                (O3RedDynConstructAndSendLsa (pLsaDesc->pLsaInfo) ==
                 OSIX_FAILURE))
            {

                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "LSA sync-up failed. "
                              "Lsa originated type: %x, link state id: %x, "
                              "adv rtr id: %x\n",
                              pLsaDesc->pLsaInfo->lsaId.u2LsaType,
                              OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                       lsaId.linkStateId),
                              OSPFV3_BUFFER_DWFROMPDU
                              (pLsaDesc->pLsaInfo->lsaId.advRtrId)));

                OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                                 pLsaDesc->pLsaInfo->pV3OspfCxt->u4ContextId,
                                 "LSA sync-up failed. "
                                 "Lsa originated type: %x, link state id: %x, "
                                 "adv rtr id: %x\n",
                                 pLsaDesc->pLsaInfo->lsaId.u2LsaType,
                                 OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->
                                                          lsaId.linkStateId),
                                 OSPFV3_BUFFER_DWFROMPDU
                                 (pLsaDesc->pLsaInfo->lsaId.advRtrId));
            }
        }
    }
    OSPFV3_COUNTER_OP (pArea->pV3OspfCxt->u4OriginateNewLsa, 1);

    OSPFV3_TRC3 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "Lsa originated type: %x, link state id: %x, adv rtr id: %x\n",
                 pLsaDesc->pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->
                                          lsaId.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (pLsaDesc->pLsaInfo->lsaId.advRtrId));

    OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                "LSA Generation Complete\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenerateLsa\n");
    KW_FALSEPOSITIVE_FIX (pLsaDesc);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsEligibleToGenLsaInCxt                                  */
/*                                                                           */
/* Description  : This function checks whether the router is eligible to     */
/*                generate particular type of LSA or not.                    */
/*                                                                           */
/* Input        : pV3OspfCxt     : Context pointer                           */
/*                u2InternalType : Internal LSA type                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS: If eligible to generate LSA..                */
/*                OSIX_FAILURE : If not eligible to generate LSA.            */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
V3IsEligibleToGenLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT2 u2InternalType)
{
    INT4                i4RetValue = OSIX_SUCCESS;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3IsEligibleToGenLsa\n");

    switch (u2InternalType)
    {
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        case OSPFV3_INDICATION_LSA:

            if (!OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ABR");
                i4RetValue = OSIX_FAILURE;
            }
            break;

        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        case OSPFV3_COND_AS_EXT_LSA:

            if ((!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt)) &&
                (!OSPFV3_IS_NSSA_ABR (pV3OspfCxt)))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ASBR or NSSA ABR");
                i4RetValue = OSIX_FAILURE;
            }

            /* If router has no  interface attached to any Type-5 capable 
             * area, it does not generate Type-5 LSAs for the external 
             * imported routes. */

            if (pV3OspfCxt->bGenType5Flag == OSIX_FALSE)
            {

                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Router has no Type 5 capable "
                              "area - Type 5 not generated"));

                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router has no Type 5 capable "
                            "area - Type 5 not generated");
                i4RetValue = OSIX_FAILURE;
            }
            break;

        case OSPFV3_NSSA_LSA:
        case OSPFV3_COND_NSSA_LSA:
        case OSPFV3_DEFAULT_NSSA_LSA:

            if ((!OSPFV3_IS_AS_BOUNDARY_RTR (pV3OspfCxt)) &&
                (!OSPFV3_IS_NSSA_ABR (pV3OspfCxt)))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ASBR or NSSA ABR");
                i4RetValue = OSIX_FAILURE;
            }
            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3IsEligibleToGenLsa\n");
    return i4RetValue;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3SetLsaType                                               */
/*                                                                           */
/* Description  : This function set the LSA type from Internal LSA type.     */
/*                                                                           */
/* Input        : u2InternalType : Internal LSA type                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u2LsaType.                                                 */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT2
V3SetLsaType (UINT2 u2InternalType)
{

    UINT2               u2LsaType = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3SetLsaType\n");

    u2LsaType = ((u2InternalType == OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA) ||
                 (u2InternalType == OSPFV3_COND_INTER_AREA_PREFIX_LSA)) ?
        OSPFV3_INTER_AREA_PREFIX_LSA : u2InternalType;

    if (u2InternalType == OSPFV3_INDICATION_LSA)
    {
        u2LsaType = OSPFV3_INTER_AREA_ROUTER_LSA;
    }

    if ((u2InternalType == OSPFV3_COND_NSSA_LSA)
        || (u2InternalType == OSPFV3_DEFAULT_NSSA_LSA))
    {
        u2LsaType = OSPFV3_NSSA_LSA;
    }

    if ((u2InternalType == OSPFV3_AS_TRNSLTD_RNG_LSA) ||
        (u2InternalType == OSPFV3_AS_TRNSLTD_EXT_LSA) ||
        (u2InternalType == OSPFV3_COND_AS_EXT_LSA))
    {
        u2LsaType = OSPFV3_AS_EXT_LSA;
    }
    if (u2InternalType == OSPFV3_STANDBY_LINK_LSA)
    {
        u2LsaType = OSPFV3_LINK_LSA;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3SetLsaType\n");
    return u2LsaType;
}

/****************************************************************************/
/*                                                                          */
/* Function        : V3GetLsaDescInCxt                                      */
/*                                                                          */
/* Description     : This is an utility routine returns the ptr to the Lsa  */
/*                   descriptor if the Lsa descriptor does not exist,it is  */
/*                   created. if allocation fails, NULL is returned.        */
/*                                                                          */
/* Input           : pV3OspfCxt       : Context pointer                     */
/*                   u2LsaType        : The LSA Type                        */
/*                   pLinkStateId     : The LinkState ID                    */
/*                   pPtr             : NULL for OSPFV3_AS_EXT_LSA,         */
/*                                      Pointer to the Interface for        */
/*                                      OSPFV3_LINK_LSA,                    */
/*                                      Pointer to Area for other LSA types */
/*                                                                          */
/* Output          : None                                                   */
/*                                                                          */
/* Returns         : Pointer to Lsa Descriptor or NULL                      */
/*                                                                          */
/****************************************************************************/
PUBLIC tV3OsLsaDesc *
V3GetLsaDescInCxt (tV3OspfCxt * pV3OspfCxt, UINT2 u2LsaType,
                   tV3OsLinkStateId * pLinkStateId, UINT1 *pPtr)
{

    tV3OsLsaDesc       *pLsaDesc = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1              *pu1TmpPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3GetLsaDesc\n");

    OSPFV3_TRC2 (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                 "Lsa Type : %x, linkStateId : %x",
                 u2LsaType, OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pLinkStateId));

    /* In case of AS flod scope LSA, pPtr will be NULL. In this case
     * copy the backbone area pointer to the temp pointer so that context
     * pointer can be accessedd
     */
    if (V3LsuGetLsaFloodScope (u2LsaType) == OSPFV3_AS_FLOOD_SCOPE)
    {
        pu1TmpPtr = (UINT1 *) pV3OspfCxt->pBackbone;
    }
    else
    {
        pu1TmpPtr = pPtr;
    }

    pLsaInfo = V3LsuSearchDatabase (u2LsaType, pLinkStateId,
                                    &(OSPFV3_RTR_ID (pV3OspfCxt)),
                                    (tV3OsInterface *) (VOID *) pu1TmpPtr,
                                    (tV3OsArea *) (VOID *) pu1TmpPtr);
    if (pLsaInfo != NULL)
    {
        /* Reset the GR bit in the LSA info structure. This indicates
         * that the LSA has been re-originated */
        OSPFV3_LSA_REFRESH_BIT_RESET (pLsaInfo, OSPFV3_GR_LSA_BIT);
        if ((pLsaDesc = pLsaInfo->pLsaDesc) != NULL)
        {
            return pLsaDesc;
        }
        else
        {
            V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);
        }

    }
    OSPFV3_LSA_DESC_ALLOC (&(pLsaDesc));
    if (NULL == pLsaDesc)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for LSA Descriptor\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                    "LSA Descr Alloc Failure. \n");
        return NULL;
    }

    OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                "LSA Descriptor created. \n");

    TMO_DLL_Init_Node (&(pLsaDesc->nextLsaDesc));
    pLsaDesc->pLsaInfo = NULL;
    pLsaDesc->pAssoPrimitive = NULL;
    pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
    pLsaDesc->u1LsaChanged = OSIX_FALSE;
    pLsaDesc->u1SeqNumWrapAround = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3GetLsaDesc \n");
    return pLsaDesc;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3GetSeqNum                                                */
/*                                                                           */
/* Description  : This function gets the LSA sequence number.                */
/*                                                                           */
/* Input        : pLsaDesc : Pointer to LSA descriptor.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : sequence number.                                           */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
V3GetSeqNum (tV3OsLsaDesc * pLsaDesc)
{

    INT4                i4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3GetSeqNum. \n");

    if (pLsaDesc->pLsaInfo == NULL)
    {
        /* this is the first time that it generates the LSA */
        i4SeqNum = (INT4) OSPFV3_MIN_SEQ_NUM;
    }
    else
    {
        if (pLsaDesc->pLsaInfo->lsaSeqNum == OSPFV3_MAX_SEQ_NUM)
        {
            /* seq num wraps around */
            if (OSPFV3_IS_MAX_AGE (pLsaDesc->pLsaInfo->u2LsaAge))
            {
                /* 
                 * the max_seq_num lsa has been flushed and new instance
                 * has to be generated
                 */
                i4SeqNum = (INT4) OSPFV3_MIN_SEQ_NUM;
            }
            else
            {
                /* the max_seq_num lsa has to be flushed */
                i4SeqNum = pLsaDesc->pLsaInfo->lsaSeqNum;
                pLsaDesc->u1SeqNumWrapAround = OSIX_TRUE;
            }
        }
        else
        {
            i4SeqNum = (pLsaDesc->pLsaInfo->lsaSeqNum + 1);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3GetSeqNum. \n");
    return i4SeqNum;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3SignalLsaRegenInCxt                                  */
/*                                                                           */
/* Description     :  (re)generates an lsa depending on the type of signal   */
/*                                                                           */
/* Input           :  pV3OspfCxt  :  Context pointer                         */
/*                    u1Type      :  signal type                             */
/*                    pPtr        :  pointer to the structure associated     */
/*                                   with the signal/lsa                     */
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   None                                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3SignalLsaRegenInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1Type, UINT1 *pPtr)
{
    tV3OsLsaDesc       *pLsaDesc = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsSummaryParam   defSummaryParam;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsLinkStateId    linkStateId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3SignalLsaRegen\n");

    if (!OSPFV3_IS_RTR_ENABLED (pV3OspfCxt))
    {
        gu4V3SignalLsaRegenInCxtFail++;
        return OSIX_FAILURE;
    }
    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA cannot be (re)generated in GR mode */
        return OSIX_SUCCESS;
    }

    switch (u1Type)
    {

        case OSPFV3_SIG_LS_REFRESH:
            pLsaDesc = (tV3OsLsaDesc *) (VOID *) pPtr;
            V3GenerateLsa (pLsaDesc->pLsaInfo->pArea,
                           pLsaDesc->u2InternalLsaType,
                           &(pLsaDesc->pLsaInfo->lsaId.linkStateId),
                           pLsaDesc->pAssoPrimitive, OSIX_TRUE);
            break;

        case OSPFV3_SIG_NEXT_INSTANCE:
            pLsaDesc = (tV3OsLsaDesc *) (VOID *) pPtr;
            if (pLsaDesc->u2InternalLsaType == OSPFV3_INTRA_AREA_PREFIX_LSA)
            {
                V3GenIntraAreaPrefixLsa (pLsaDesc->pLsaInfo->pArea);
            }
            else
            {
                V3GenerateLsa (pLsaDesc->pLsaInfo->pArea,
                               pLsaDesc->u2InternalLsaType,
                               &(pLsaDesc->pLsaInfo->lsaId.linkStateId),
                               pLsaDesc->pAssoPrimitive, OSIX_FALSE);
            }
            break;

        case OSPFV3_SIG_HOST_ATTACHED:
        case OSPFV3_SIG_HOST_DETACHED:
            pArea = (tV3OsArea *) (VOID *) pPtr;
            V3GenIntraAreaPrefixLsa (pArea);
            break;

        case OSPFV3_SIG_IF_STATE_CHANGE:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            V3GenIntraAreaPrefixLsa (pInterface->pArea);
            break;

        case OSPFV3_SIG_IF_PREFIX_ADDED:
        case OSPFV3_SIG_IF_PREFIX_DELETED:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
            V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA, &linkStateId,
                           (UINT1 *) pInterface, OSIX_FALSE);
            V3GenIntraAreaPrefixLsa (pInterface->pArea);
            break;

        case OSPFV3_SIG_IF_PRIORITY_CHANGE:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
            V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA, &linkStateId,
                           (UINT1 *) pInterface, OSIX_FALSE);
            break;

        case OSPFV3_SIG_VIRT_LINK_COST_CHANGE:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
            V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &pInterface->linkStateId,
                           (UINT1 *) pInterface->pArea, OSIX_FALSE);
            break;

        case OSPFV3_SIG_DR_CHANGE:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &pInterface->linkStateId,
                           (UINT1 *) pInterface->pArea, OSIX_FALSE);

            if (OSPFV3_IS_DR (pInterface))
            {
                OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
                V3GenerateLsa (pInterface->pArea, OSPFV3_NETWORK_LSA,
                               &linkStateId, (UINT1 *) pInterface, OSIX_FALSE);
            }
            V3GenIntraAreaPrefixLsa (pInterface->pArea);

            break;

        case OSPFV3_SIG_SUMMARY_GEN:
            pRtEntry = (tV3OsRtEntry *) (VOID *) pPtr;

            if (!OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ABR\n");
                gu4V3SignalLsaRegenInCxtFail++;
                return OSIX_FAILURE;
            }

            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                if (TMO_SLL_Count (&(pArea->ifsInArea)) != 0)
                {
                    V3GenerateSummary (pArea, pRtEntry);
                }
            }
            break;

        case OSPFV3_SIG_DEFAULT_SUMMARY:
            if (!OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ABR Can not genertate"
                            "Default Intra Area Prefix LSA.\n");
                gu4V3SignalLsaRegenInCxtFail++;
                return OSIX_FAILURE;
            }
            V3BuildDefaultSummaryParam ((tV3OsArea *) (VOID *) pPtr,
                                        &defSummaryParam);
            V3GenerateLsa ((tV3OsArea *) (VOID *) pPtr,
                           OSPFV3_INTER_AREA_PREFIX_LSA,
                           &(OSPFV3_NULL_LSID), (UINT1 *) &defSummaryParam,
                           OSIX_FALSE);
            break;

        case OSPFV3_SIG_INDICATION:
            if (!OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
            {
                OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                            "Router is not an ABR Can not genertate"
                            "Indication LSA.\n");
                gu4V3SignalLsaRegenInCxtFail++;
                return OSIX_FAILURE;
            }
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                if (pArea == (tV3OsArea *) (VOID *) pPtr)
                {
                    continue;
                }
                if ((pArea->u4DcBitResetLsaCount != 0) ||
                    (TMO_SLL_Count (&(pArea->ifsInArea)) == 0))
                {
                    /* area has non indication dc bit reset lsa */
                    /*  OR Area is not connected to any of the 
                     * Interfaces of the Router */
                    continue;
                }

                if (V3IsEgblToGenIndicationLsa (pArea) == OSIX_TRUE)
                {
                    if (pArea->bIndicationLsaPresence == OSIX_FALSE)    /* Dc bit fix */
                    {
                        pArea->bIndicationLsaPresence = OSIX_TRUE;
                        V3GenerateLsa (pArea, OSPFV3_INDICATION_LSA,
                                       &pV3OspfCxt->rtrId, NULL, OSIX_FALSE);
                    }
                }
            }
            break;
        case OSPFV3_SIG_GRACE_LSA_GEN:
            pInterface = (tV3OsInterface *) (VOID *) pPtr;
            if (pInterface != NULL)
            {
                OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
                V3GenerateLsa (pInterface->pArea, OSPFV3_GRACE_LSA,
                               &linkStateId, (UINT1 *) pInterface, OSIX_FALSE);
            }
            break;

        default:
            break;

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3SignalLsaRegen\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :  V3IsEgblToGenIndicationLsa                             */
/*                                                                           */
/* Description     :  This function checks whether router is eligible to     */
/*                    genertate Indication LSA or not.                       */
/*                                                                           */
/* Input           :  pArea  : Pointer to area.                              */
/*                                                                           */
/* Output          :  None                                                   */
/*                                                                           */
/* Returns         :  None                                                   */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT1
V3IsEgblToGenIndicationLsa (tV3OsArea * pArea)
{

    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsEgblToGenIndicationLsa\n");

    if ((OSPFV3_IS_STUB_AREA (pArea)) || (OSPFV3_IS_NSSA_AREA (pArea)))
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3IsEgblToGenIndicationLsa\n");

        return OSIX_FALSE;
    }
    TMO_HASH_Scan_Table (pArea->pInterRouterLsaTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pArea->pInterRouterLsaTable, u4HashKey,
                              pOsDbNode, tV3OsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

                if (pLsaInfo->lsaId.u2LsaType != OSPFV3_INTER_AREA_ROUTER_LSA)
                {
                    continue;
                }

                if ((OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)) &&
                    (V3UtilRtrIdComp
                     (pLsaInfo->lsaId.advRtrId,
                      OSPFV3_RTR_ID (pArea->pV3OspfCxt)) == OSPFV3_GREATER))
                {
                    return OSIX_FALSE;
                }
            }

        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsEgblToGenIndicationLsa\n");
    return OSIX_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsaRtrIdHashFunc                                         */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input          pAdvRtrId     : Advertiser router id.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
V3LsaRtrIdHashFunc (tV3OsRouterId * pAdvRtrId)
{
    UINT4               u4HashIndex = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsaRtrIdHashFunc\n");

    u4HashIndex = V3UtilHashGetValue (OSPFV3_LSA_HASH_TABLE_SIZE,
                                      (UINT1 *) pAdvRtrId,
                                      sizeof (tV3OsRouterId));

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsaRtrIdHashFunc\n");
    return u4HashIndex;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsaAddrPrefixHashFunc                                    */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : pAddrPrefix     : addr Prefix.                             */
/*                u1PrefixLength  : Prefix Length.                           */
/*                u2LsaType       : lsa type                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
V3LsaAddrPrefixHashFunc (UINT1 *pAddrPrefix, UINT1 u1PrefixLength,
                         UINT2 u2LsaType)
{
    UINT1               au1Buf[OSPFV3_ADDR_PREFIX_LSA_HASH_KEY_SIZE];
    UINT4               u4HashTableSize = 0;
    UINT4               u4HashIndex = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsaAddrPrefixHashFunc\n");

    MEMSET (au1Buf, 0, OSPFV3_ADDR_PREFIX_LSA_HASH_KEY_SIZE);

    if (OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLength) > OSPFV3_IPV6_ADDR_LEN)
    {
        return u4HashIndex;
    }

    OSPFV3_IP6_ADDR_PREFIX_COPY (au1Buf, *pAddrPrefix, u1PrefixLength);
    au1Buf[OSPFV3_IPV6_ADDR_LEN] = u1PrefixLength;

    if (u2LsaType == OSPFV3_AS_EXT_LSA)
    {
        u4HashTableSize = OSPFV3_EXT_LSA_HASH_TABLE_SIZE;
    }
    else
    {
        u4HashTableSize = OSPFV3_LSA_HASH_TABLE_SIZE;
    }

    u4HashIndex = V3UtilHashGetValue (u4HashTableSize, au1Buf,
                                      OSPFV3_ADDR_PREFIX_LSA_HASH_KEY_SIZE);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsaAddrPrefixHashFunc\n");
    return u4HashIndex;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsaIntraAreaHashFunc                                     */
/*                                                                           */
/* Description  : This function gives the hash value of the key.             */
/*                                                                           */
/* Input        : u2RefLsaType     : lsa type                                */
/*                pRefLinkStateId  : link state id                           */
/*                pRefAdvRtrId     : Advertiser router id.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Hashed value of the Key.                                   */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
V3LsaIntraAreaHashFunc (UINT2 u2RefLsaType,
                        tV3OsLinkStateId * pRefLinkStateId,
                        tV3OsRouterId * pRefAdvRtrId)
{
    UINT1               au1Buf[OSPFV3_INTRA_AREA_PREFIX_LSA_HASH_KEY_SIZE];
    UINT4               u4HashIndex = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsaIntraAreaHashFunc\n");

    OSPFV3_BUFFER_WTOPDU (au1Buf, u2RefLsaType);
    MEMCPY ((au1Buf + OSPFV3_LS_TYPE_LEN), pRefLinkStateId,
            sizeof (tV3OsLinkStateId));
    MEMCPY ((au1Buf + OSPFV3_LS_TYPE_LEN + sizeof (tV3OsLinkStateId)),
            pRefAdvRtrId, sizeof (tV3OsRouterId));

    u4HashIndex = V3UtilHashGetValue (OSPFV3_LSA_HASH_TABLE_SIZE, au1Buf,
                                      OSPFV3_INTRA_AREA_PREFIX_LSA_HASH_KEY_SIZE);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsaIntraAreaHashFunc\n");
    return u4HashIndex;
}

/*****************************************************************************/
/*                                                                           */
/* Function        :   V3GenerateLsasForExtRoutesInCxt                       */
/*                                                                           */
/* Description     :   generates all AS External LSAs                        */
/*                                                                           */
/* Input           :   pV3OspfCxt   -   Context pointer                      */
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   None                                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenerateLsasForExtRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3GenerateLsasForExtRoutesInCxt\n");

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Retreiving routes from OSPFv3 database failed\n"));

        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "Retreiving routes from OSPFv3 database failed\n");
        return;
    }

    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3GenerateLsasForExtRoutesInCxt\n");
}

/*****************************************************************************/
/* Function        : V3GenerateSummary                                       */
/* Description     : Generates Inter Area Prefix LSA or Inter Area Router LSA*/
/*                   for the given routing table entry into the specified    */
/*                   area.                                                   */
/* Input           : pArea     : Area to which the summarization to be done  */
/*                   pRtEntry  : Ptr to route entry structure which is to    */
/*                               be summarized.                              */
/* Output          : None.                                                   */
/* Returns         : None.                                                   */
/*****************************************************************************/
PUBLIC VOID
V3GenerateSummary (tV3OsArea * pArea, tV3OsRtEntry * pRtEntry)
{

    tV3OsSummaryParam   summaryParam;
    tV3OsPath          *pPath = NULL;
    tV3OsArea          *pTmpArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3GenerateSummary\n");
    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        return;
    }

    /* Proceed only if the stub or NSSA area needs Summary LSAs */
    if ((pArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
        (!OSPFV3_IS_SEND_AREA_SUMMARY (pArea)))
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Area is either STUB or NSSA"
                      " does not allow summarization\n"));

        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Area is either STUB or NSSA"
                    " does not allow summarization\n");
        return;
    }

    /* Inter Area Router  LSAs are not generated in Stub or NSSA area */
    if (OSPFV3_IS_DEST_ASBR (pRtEntry)
        && (pArea->u4AreaType != OSPFV3_NORMAL_AREA))
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Area is not a Normal area - "
                      " does not allow ASBR summarization\n"));

        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Area is not a Normal area - "
                    "does not allow ASBR summarization\n");
        return;
    }

    /* ASBR internal to NSSA are not summarized in other areas */
    if (OSPFV3_IS_DEST_ASBR (pRtEntry))
    {
        pPath = OSPFV3_GET_PATH (pRtEntry);

        if (pPath != NULL)
        {
            if ((pTmpArea = V3GetFindAreaInCxt
                 (pArea->pV3OspfCxt, &(pPath->areaId))) != NULL)
            {
                if (pTmpArea->u4AreaType == OSPFV3_NSSA_AREA)
                {

                    SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Internal NSSA ASBR - does not allow"
                                  "ASBR summarization\n"));

                    OSPFV3_TRC (OSPFV3_LSU_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "Internal NSSA ASBR - does not allow"
                                "ASBR summarization\n");
                    return;
                }
            }
        }
    }

    /* Skip routes to Area Border Routers */
    if (OSPFV3_IS_DEST_ABR (pRtEntry))
    {
        return;
    }

    /* Consider only INTER-AREA and INTRA-AREA paths */

    if ((OSPFV3_IS_TYPE_1_EXT_PATH (pRtEntry)) ||
        (OSPFV3_IS_TYPE_2_EXT_PATH (pRtEntry)))
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Path type is TYPE_1_EXT or TYPE_2_EXT.\n");
        return;
    }

    if ((V3BuildSummaryParam (pArea, pRtEntry, &summaryParam) == OSIX_FAILURE))
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Path or the Next hop is associated with same area.");
        return;
    }

    if (OSPFV3_IS_DEST_ASBR (pRtEntry))
    {
        /* Ref : RFC 3509 Section 2.2 Point 3 */
        if ((pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR) &&
            (!OSPFV3_IS_ACTIVE_BACKBONE_CONNECTION (pArea->pV3OspfCxt)) &&
            (!(OSPFV3_IS_INTRA_AREA_PATH (pRtEntry))))
        {
            return;
        }

        V3GenerateLsa (pArea, OSPFV3_INTER_AREA_ROUTER_LSA,
                       &(pRtEntry->destRtRtrId), (UINT1 *) &summaryParam,
                       OSIX_FALSE);
    }
    else if ((OSPFV3_IS_DEST_NETWORK (pRtEntry)) &&
             (OSPFV3_IS_INTER_AREA_PATH (pRtEntry)))
    {
        /* Ref : RFC 3509 Section 2.2 Point 3 */
        if ((pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR) &&
            (!(OSPFV3_IS_ACTIVE_BACKBONE_CONNECTION (pArea->pV3OspfCxt))))
        {
            return;
        }

        V3GenerateLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                       &pRtEntry->linkStateId, (UINT1 *) &summaryParam,
                       OSIX_FALSE);
    }
    else if ((OSPFV3_IS_DEST_NETWORK (pRtEntry)) &&
             (OSPFV3_IS_INTRA_AREA_PATH (pRtEntry)))
    {
        /* Backbone info is not condensed into transit areas */
        if ((OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea)) &&
            (TMO_SLL_Count (&(pRtEntry->pathLst)) != 0) &&
            (OSPFV3_IS_NULL_IP_ADDR (OSPFV3_GET_PATH_AREA_ID (pRtEntry))))
        {
            V3GenerateLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                           &pRtEntry->linkStateId, (UINT1 *) &summaryParam,
                           OSIX_FALSE);

        }
        else
        {
            if (V3CondenseSummaryInCxt (pArea->pV3OspfCxt, pRtEntry)
                == OSIX_FAILURE)
            {

                /* 
                 * if the condensed summary lsa was not generated
                 * generate inter area prefix lsa for that network alone
                 */
                V3GenerateLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                               &pRtEntry->linkStateId,
                               (UINT1 *) &summaryParam, OSIX_FALSE);

            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenerateSummary\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function        :   V3GenerateSummaryToArea                               */
/*                                                                           */
/* Description     :   originates summary of all routes to the newly         */
/*                     attatched area                                        */
/*                                                                           */
/* Input           :   pArea : area to which the summarization to be done    */
/*                                                                           */
/* Output          :   None                                                  */
/*                                                                           */
/* Returns         :   None                                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenerateSummaryToArea (tV3OsArea * pArea)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenerateSummaryToArea\n");

    OSPFV3_TRC1 (OSPFV3_LSU_TRC, pArea->pV3OspfCxt->u4ContextId,
                 "All routes are to be summarized into area : %x.",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->ospfV3RtTable.routesList), pRtEntry,
                  tV3OsRtEntry *)
    {
        V3GenerateSummary (pArea, pRtEntry);
    }

    inParams.pRoot = pArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Retreiving routes from OSPFv3 database failed\n"));

        OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Retreiving routes from OSPFv3 database failed\n");
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        V3GenerateSummary (pArea, pRtEntry);
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenerateSummaryToArea\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3GenerateDefSumToStubNssaArea                             */
/*                                                                           */
/* Description  : This routine is used to send default summary LSA into      */
/*                stub/Nssa area.                                            */
/*                                                                           */
/* Input        : pArea                 : pointer to Stub Area.              */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenerateDefSumToStubNssaArea (tV3OsArea * pArea)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenerateDefSumToStubNssaArea\n");

    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
    {
        if (pArea->u4AreaType == OSPFV3_STUB_AREA)
        {
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                   OSPFV3_SIG_DEFAULT_SUMMARY, (UINT1 *) pArea);
        }
        else if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            if (pArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY)
            {
                if (pArea->u4DfInfOriginate == OSPFV3_DEFAULT_INFO_ORIGINATE)
                {
                    V3GenerateNssaDfLsa (pArea, OSPFV3_DEFAULT_NSSA_LSA,
                                         &(OSPFV3_NULL_LSID));
                }
            }
            else
            {
                V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                       OSPFV3_SIG_DEFAULT_SUMMARY,
                                       (UINT1 *) pArea);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3GenerateDefSumToStubNssaArea\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3GenerateNssaDfLsa                                     */
/*                                                                           */
/* Description     : Generates NSSA Default LSA in NSSA area                 */
/*                                                                           */
/* Input           : pArea - Pointer to area structure                       */
/*                 : u2InternalType - LSA Type                               */
/*                 : pLsId - Link state ID                                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenerateNssaDfLsa (tV3OsArea * pArea, UINT2 u2InternalType,
                     tV3OsLinkStateId * pLsId)
{
    tV3OsExtRoute      *pExtRoute = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenerateNssaDfLsa\n");

    if (!(OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt))
        && (!(OSPFV3_IS_AS_BOUNDARY_RTR (pArea->pV3OspfCxt))))
    {
        return;
    }

    if (OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt))
    {
        V3GenerateLsa (pArea, u2InternalType, pLsId, (UINT1 *) pArea,
                       OSIX_FALSE);
    }
    else
    {
        /* Check whether route is present in EXT RT table       */
        if ((pExtRoute = V3ExtrtFindRouteInCxt (pArea->pV3OspfCxt,
                                                &gV3OsNullIp6Addr, 0)) != NULL)
        {
            V3GenerateLsa (pArea, u2InternalType, pLsId, (UINT1 *) pExtRoute,
                           OSIX_FALSE);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenerateNssaDfLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function    : V3GenLowestLsIdRtrLsa                                       */
/*                                                                           */
/* Description : This function generates link state id Router-Lsa in the area*/
/*                                                                           */
/* Input       : pArea - Pointer to the area                                 */
/*                                                                           */
/* Output      : None                                                        */
/*                                                                           */
/* Returns     : None                                                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3GenLowestLsIdRtrLsa (tV3OsArea * pArea)
{

    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3GenLowestLsIdRtrLsa\n");

    u4HashIndex = V3LsaRtrIdHashFunc (&pArea->pV3OspfCxt->rtrId);

    TMO_HASH_Scan_Bucket (pArea->pRtrLsaHashTable, u4HashIndex, pOsDbNode,
                          tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (V3UtilRtrIdComp (pArea->pV3OspfCxt->rtrId,
                             pDbLsaInfo->lsaId.advRtrId) == OSPFV3_EQUAL)
        {
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pDbLsaInfo->pLsaDesc);
            return;
        }
    }

    V3GenerateLsa (pArea, OSPFV3_ROUTER_LSA,
                   &(OSPFV3_NULL_ID (pArea->pV3OspfCxt->u4ContextId)),
                   NULL, OSIX_FALSE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3GenLowestLsIdRtrLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3CondenseSummaryInCxt                                  */
/*                                                                           */
/* Description     : This function condenses the info related to the         */
/*                   specified routing table entry into the specified area   */
/*                                                                           */
/* Input           : pV3OspfCxt : Context pointer                            */
/*                   pRtEntry : pointer to route entry structure which is to */
/*                              be summarized                                */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_SUCCESS/OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3CondenseSummaryInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3CondenseSummaryInCxt\n");

    if (V3AreaFindInternalAddrRngInCxt
        (pV3OspfCxt,
         &(pRtEntry->destRtPrefix),
         &(OSPFV3_GET_PATH_AREA_ID (pRtEntry))) == NULL)
    {

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      " Dest addr does not fall into any of the"
                      "configured addr ranges \n"));

        OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                    " Dest addr does not fall into any of the"
                    "configured addr ranges \n");
        return OSIX_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3CondenseSummaryInCxt\n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function     : V3GenerateAggLsa                                      */
/*                                                                      */
/* Description  : This Function Scan the Area List and generate Agg     */
/*                Lsa if Flag is set else flush the Agg Lsa if          */
/*                flag is not set and Agg lsa is present in             */
/*                the database                                          */
/*                                                                      */
/* Input        : pArea :  pointer to the AREA.                         */
/*                pAddrRng : Pointer to the address range               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/************************************************************************/
PUBLIC INT4
V3GenerateAggLsa (tV3OsArea * pArea, tV3OsAddrRange * pAddrRng)
{
    tV3OsArea          *pLstArea = NULL;
    UINT1               u1TranAreaPres = OSIX_FALSE;
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagGenerateAggLsa\n");

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea, tV3OsArea *)
    {
        if ((V3UtilAreaIdComp (pArea->areaId,
                               pLstArea->areaId) != OSPFV3_EQUAL) &&
            ((pLstArea->u4AreaType == OSPFV3_NORMAL_AREA) ||
             ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
              (pLstArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY))))
        {
            if ((OSPFV3_IS_AREA_TRANSIT_CAPABLE (pLstArea)) &&
                (V3UtilAreaIdComp (pArea->areaId, OSPFV3_BACKBONE_AREAID)
                 == OSPFV3_EQUAL))
            {
                /* Backbone Network should not be condensed in Transit Area */
                u1TranAreaPres = OSIX_TRUE;
                continue;
            }
            OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst, pAddrRng->ip6Addr);
            NetRtInfo.u1Prefixlen = pAddrRng->u1PrefixLength;
            MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
            NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
            NetRtInfo.u4RowStatus = ACTIVE;
            NetRtInfo.u4Index = 0;
            NetRtInfo.u1NullFlag = 1;
            NetRtInfo.u4Metric = 0;
            NetRtInfo.u1Preference = IP6_PREFERENCE_OSPF;

            if (NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetRtInfo) ==
                NETIPV6_FAILURE)
            {
                OSPFV3_TRC (OSPFV3_RAG_TRC, pArea->pV3OspfCxt->u4ContextId,
                            " NetIpv6LeakRoute Addition Failed. \n");
            }

            V3GenerateLsa (pLstArea,
                           OSPFV3_COND_INTER_AREA_PREFIX_LSA,
                           &(pAddrRng->linkStateId),
                           (UINT1 *) pAddrRng, OSIX_FALSE);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagGenerateAggLsa \n");
    return u1TranAreaPres;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3ChkAndGenerateSumToNewAreasInCxt                         */
/*                                                                           */
/* Description  : This function checks and generate the summary LSAs into    */
/*                the newly attached areas.                                  */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3ChkAndGenerateSumToNewAreasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3ChkAndGenerateSumToNewAreas\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA's Cannot be (e)orginated in graceful restart mode */
        return;
    }
    if (OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (pArea->bNewlyAttached == OSIX_TRUE)
            {
                if (pArea->u4AreaType != OSPFV3_NORMAL_AREA)
                {
                    V3GenerateDefSumToStubNssaArea (pArea);
                }
                if (TMO_SLL_Count (&(pArea->ifsInArea)) != 0)
                {
                    V3GenerateSummaryToArea (pArea);
                }
                pArea->bNewlyAttached = OSIX_FALSE;
            }
            V3RagGenerateAggLsa (pArea);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3ChkAndGenerateSumToNewAreas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3ResetMinLsaForAllIAPrefixLsa                             */
/*                                                                           */
/* Description  : This function Resets the Min LSA interval related flags    */
/*                for all the Intra area prefix LSAs.                        */
/*                the newly attached areas.                                  */
/*                                                                           */
/* Input        : Area pointer                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3ResetMinLsaForAllIAPrefixLsa (tV3OsArea * pArea)
{
    UINT4               u4HashKey;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3ResetMinLsaForAllIAPrefixLsa \n");

    TMO_HASH_Scan_Table (pArea->pIntraPrefixLsaTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pArea->pIntraPrefixLsaTable,
                              u4HashKey, pOsDbNode, tV3OsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

                if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                      pArea->pV3OspfCxt->rtrId)
                     == OSPFV3_EQUAL) && (pLsaInfo->pLsaDesc != NULL))
                {
                    pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
                    pLsaInfo->pLsaDesc->u1LsaChanged = OSIX_FALSE;
                    V3TmrDeleteTimer (&pLsaInfo->pLsaDesc->minLsaIntervalTimer);
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3ResetMinLsaForAllIAPrefixLsa \n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lsgen.c                       */
/*-----------------------------------------------------------------------*/
