/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3redtmr.c,v 1.6 2018/02/01 11:02:57 siva Exp $
 *
 * Description: This file contains procedures for redundancy related timers. 
 *
 *******************************************************************/
#include "o3inc.h"

/* Prototypes */

PRIVATE INT4
    O3RedTmrAddStabilityInfo PROTO ((tRmMsg ** ppRmMsg, tV3OspfCxt * pV3OspfCxt,
                                     UINT4 *pu4Offset));

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedTmrSendTimerInfo                                      */
/*                                                                           */
/* Description  : This function constructs the timers related info           */
/*                to syncronize with the standby node. This is called only   */
/*                during bulk update as in dynamic updates, it will be       */
/*                started automatically                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pi4SendSubBulkEvent - Sub bulk update status               */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedTmrSendTimerInfo (INT4 *pi4SendSubBulkEvent)
{
    tRmMsg             *pRmMsg = NULL;    /* bulk Update packet */
    UINT4               u4PktLen = 0;    /* Current packet Length */
    UINT4               u4Offset = 0;    /* Current offset */
    UINT4               u4ContextId = 0;    /* Context Id */
    UINT4               u4Time = 0;    /* Remaining time */
    UINT1               u1MsgType = 0;    /* Sub message bulk type */
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedTmrSendTimerInfo\n");

    *pi4SendSubBulkEvent = OSIX_FAILURE;

    for (u4ContextId = 0; u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT;
         u4ContextId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4ContextId] == NULL)
        {
            continue;
        }

        u4Time = 0;
        TmrGetRemainingTime (gV3OsRtr.timerLstId,
                             &(gV3OsRtr.apV3OspfCxt[u4ContextId]->
                               exitOverflowTimer.timerNode), &u4Time);

        if (u4Time == 0)
        {
            /* Scan all the areas in context and add the stability interval */
            O3RedTmrAddStabilityInfo (&pRmMsg,
                                      gV3OsRtr.apV3OspfCxt[u4ContextId],
                                      &u4Offset);
            u4PktLen = u4Offset;
            continue;
        }

        u4Time = u4Time / OSPFV3_NO_OF_TICKS_PER_SEC;

        if ((pRmMsg != NULL) &&
            ((u4PktLen + OSPFV3_DB_TIMER_INFO) > OSPFV3_RED_MTU_SIZE))
        {
            if (O3RedUtlSendBulkUpdate (pRmMsg, u4PktLen) == OSIX_FAILURE)
            {
                gu4O3RedTmrSendTimerInfoFail++;
                return OSIX_FAILURE;
            }

            pRmMsg = NULL;
        }

        if (pRmMsg == NULL)
        {
            /* Allocate the New Buffer for bulk update */
            if (O3RedAdjAllocateBulkUpdatePkt (&pRmMsg, &u4Offset, &u4PktLen)
                == OSIX_FAILURE)
            {
                gu4O3RedTmrSendTimerInfoFail++;
                return OSIX_FAILURE;
            }
        }

        u1MsgType = (UINT1) OSPFV3_RED_BULK_DB_TIMER;

        OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
        OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, u4ContextId);
        OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, u4Time);

        /* Scan all the areas in context and add the stability interval */
        O3RedTmrAddStabilityInfo (&pRmMsg, gV3OsRtr.apV3OspfCxt[u4ContextId],
                                  &u4Offset);

        u4PktLen = u4Offset;
    }

    if (pRmMsg != NULL)
    {
        if (O3RedUtlSendBulkUpdate (pRmMsg, u4PktLen) == OSIX_FAILURE)
        {
            gu4O3RedTmrSendTimerInfoFail++;
            return OSIX_FAILURE;
        }

        pRmMsg = NULL;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedTmrSendTimerInfo\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedTmrAddStabilityInfo                                   */
/*                                                                           */
/* Description  : This function scans all the areas in the context and       */
/*                add the remanining stability interval in each area         */
/*                                                                           */
/* Input        : ppRmMsg    -    RM Message                                 */
/*                pV3OspfCxt   -    OSPF context                             */
/*                                                                           */
/* Output       : pu4Offset  -    Offset updated after addition              */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
O3RedTmrAddStabilityInfo (tRmMsg ** ppRmMsg, tV3OspfCxt * pV3OspfCxt,
                          UINT4 *pu4Offset)
{
    tRmMsg             *pRmMsg = *ppRmMsg;
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4PktLen = *pu4Offset;
    UINT4               u4Time = 0;
    UINT1               u1MsgType = 0;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedTmrAddStabilityInfo\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pNode);

        u4Time = 0;

        TmrGetRemainingTime (gV3OsRtr.timerLstId,
                             &(pArea->nssaStbltyIntrvlTmr.timerNode), &u4Time);

        if (u4Time == 0)
        {
            continue;
        }

        u4Time = u4Time / OSPFV3_NO_OF_TICKS_PER_SEC;

        if ((pRmMsg != NULL) &&
            ((u4PktLen + OSPFV3_STAB_TIMER_INFO) > OSPFV3_RED_MTU_SIZE))
        {
            if (O3RedUtlSendBulkUpdate (pRmMsg, u4PktLen) == OSIX_FAILURE)
            {
                gu4O3RedTmrAddStabilityInfoFail++;
                return OSIX_FAILURE;
            }

            pRmMsg = NULL;
        }

        if (pRmMsg == NULL)
        {
            /* Allocate the New Buffer for bulk update */
            if (O3RedAdjAllocateBulkUpdatePkt (&pRmMsg, &u4Offset, &u4PktLen)
                == OSIX_FAILURE)
            {
                gu4O3RedTmrAddStabilityInfoFail++;
                return OSIX_FAILURE;
            }
        }

        u1MsgType = (UINT1) OSPFV3_RED_BULK_STAB_TIMER;

        OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
        OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, pV3OspfCxt->u4ContextId);
        OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, u4Time);
        OSPFV3_RED_PUT_N_BYTE (pRmMsg, pArea->areaId, u4Offset,
                               sizeof (pArea->areaId));

        u4PktLen = u4Offset;
    }

    *ppRmMsg = pRmMsg;
    *pu4Offset = u4Offset;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : O3RedTmrAddStabilityInfo\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedTmrProcessTimerInfo                                   */
/*                                                                           */
/* Description  : This function process the timers related info              */
/*                to syncronize with the standby node. This is called only   */
/*                during bulk update as in dynamic updates, it will be       */
/*                started automatically                                      */
/*                                                                           */
/* Input        : pRmMsg     -    RM Message                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedTmrProcessTimerInfo (tRmMsg * pRmMsg)
{
    tV3OsArea          *pArea = NULL;
    UINT4               u4ContextId = 0;    /* Context Id */
    UINT4               u4Time = 0;    /* Remaining time */
    tV3OsAreaId         areaId;    /* area Id for stability intervale */
    UINT2               u2PktLen = 0;    /* Pkt length */
    UINT2               u2ReadLen = 0;    /* Bytes read */
    UINT1               u1MsgType = 0;    /* Sub message bulk type */
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedTmrProcessTimerInfo\n");

    /* Skip the bulk update type */
    u2ReadLen = sizeof (UINT1);

    OSPFV3_RED_GET_2_BYTE (pRmMsg, u2ReadLen, u2PktLen);

    while (u2ReadLen < u2PktLen)
    {
        OSPFV3_RED_GET_1_BYTE (pRmMsg, u2ReadLen, u1MsgType);

        OSPFV3_RED_GET_4_BYTE (pRmMsg, u2ReadLen, u4ContextId);
        OSPFV3_RED_GET_4_BYTE (pRmMsg, u2ReadLen, u4Time);

        if (V3UtilOspfIsValidCxtId (u4ContextId) == OSIX_FAILURE)
        {
            if (u1MsgType == (UINT1) OSPFV3_RED_BULK_STAB_TIMER)
            {
                u2ReadLen += sizeof (areaId);
            }
            u4ContextId = 0;
            continue;
        }

        if (u1MsgType == (UINT1) OSPFV3_RED_BULK_DB_TIMER)
        {
            V3TmrSetTimer (&
                           (gV3OsRtr.apV3OspfCxt[u4ContextId]->
                            exitOverflowTimer), OSPFV3_EXIT_OVERFLOW_TIMER,
                           (u4Time * OSPFV3_NO_OF_TICKS_PER_SEC));
        }
        else
        {
            OSPFV3_RED_GET_N_BYTE (pRmMsg, areaId, u2ReadLen, sizeof (areaId));

            pArea = V3GetFindAreaInCxt (gV3OsRtr.apV3OspfCxt[u4ContextId],
                                        &areaId);

            if (pArea == NULL)
            {
                continue;
            }

            V3TmrSetTimer (&(pArea->nssaStbltyIntrvlTmr),
                           OSPFV3_EXIT_OVERFLOW_TIMER,
                           (u4Time * OSPFV3_NO_OF_TICKS_PER_SEC));
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedTmrProcessTimerInfo\n");

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redtmr.c                           */
/*-----------------------------------------------------------------------*/
