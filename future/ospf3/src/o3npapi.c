
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: o3npapi.c,v 1.1 2014/04/10 13:36:14 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of OSPF3 module.
 ******************************************************************************/

#ifndef __O3_NPAPI_C__
#define __O3_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : Ospf3FsNpOspf3Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspf3Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspf3Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ospf3FsNpOspf3Init ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_OSPF3_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

/***************************************************************************
 *                                                                          
 *    Function Name       : Ospf3FsNpOspf3DeInit                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpOspf3DeInit
 *                                                                          
 *    Input(s)            : Arguments of FsNpOspf3DeInit
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ospf3FsNpOspf3DeInit ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_OSPF3_DE_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : Ospf3FsNpMbsmOspf3Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmOspf3Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmOspf3Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Ospf3FsNpMbsmOspf3Init (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmOspf3Init *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_OSPF3_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmOspf3Init;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}

#endif /* MBSM_WANTED */
#endif /* __O3_NPAPI_C__ */
