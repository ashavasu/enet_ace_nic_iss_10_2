/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3conf.c,v 1.39 2018/02/07 09:32:29 siva Exp $
 *
 * Description: This file contains procedures related to set Ospfv3
 *         MIB Objects. 
 *
 *******************************************************************/
#include "o3inc.h"
#include "ospf3lw.h"
#include "fsos3lw.h"
#include "fsmisos3cli.h"
#include "fsmio3cli.h"

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  V3MapIfType
 
 Description : This function converts IfType provided by NetIp6 to Ospfv3
              IfType

 Input       :  u4NetIp6IfType - Interface type provided by NetIp6
         

 Output      :  pu4IfType - Interface type used in Ospfv3 
               
 Returns     :  OSIX_FAILURE or OSIX_SUCCESS
****************************************************************************/
PUBLIC INT1
V3MapIfType (UINT4 u4NetIp6IfType, UINT1 *pu1IfType)
{
    if ((u4NetIp6IfType == CRU_ENET_PHYS_INTERFACE_TYPE) ||
        (u4NetIp6IfType == CFA_L3IPVLAN) || (u4NetIp6IfType == CFA_LAGG))
    {
        *pu1IfType = OSPFV3_IF_BROADCAST;
    }

    else if (u4NetIp6IfType == CRU_FRMRL_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = OSPFV3_IF_NBMA;
    }
    else if (u4NetIp6IfType == CRU_X25_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = OSPFV3_IF_PTOP;
    }
    else if (u4NetIp6IfType == CRU_PPP_PHYS_INTERFACE_TYPE)
    {
        *pu1IfType = OSPFV3_IF_PTOP;
    }
    /* LoopBack interface support with respect to OSPF protocol is available in
     * the product. Trigger has to be made from this place, if the interface type 
     * is loopback. Identification of interface type depends on the IP/Interface 
     * module to which OSPF is integrated. Since FS IP does not support this type, 
     * the trigger is not made here. But with different IP, if loopback support is 
     * available, *pu1IfType can be set to IF_LOOPBACK */

    else if (u4NetIp6IfType == IP6_TUNNEL_INTERFACE_TYPE)
    {
        *pu1IfType = OSPFV3_IF_PTOP;
    }
    else if (u4NetIp6IfType == CFA_PSEUDO_WIRE)
    {
        *pu1IfType = OSPFV3_IF_PTOP;
    }
    else if (u4NetIp6IfType == CFA_LOOPBACK)
    {
        *pu1IfType = OSPFV3_IF_LOOPBACK;
    }
    else
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3RouterId
 Input       :  The Indices

                The Object 
                setValOspfv3RouterId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3RouterId (UINT4 u4SetValOspfv3RouterId)
{
    tV3OsRouterId       rtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetOspfv3RouterId\n");

    OSPFV3_BUFFER_DWTOPDU (rtrId, u4SetValOspfv3RouterId);

    /*
     * If the router id of a router changes the protocol is brought down and
     * restarted after setting the new router id.
     */

    if (V3UtilRtrIdComp (pV3OspfCxt->rtrId, rtrId) != OSPFV3_EQUAL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (pV3OspfCxt->admnStat == OSPFV3_ENABLED)
        {
            i1RetVal = V3RtrSetRouterIdInCxt (pV3OspfCxt, rtrId);
            if (i1RetVal == OSIX_SUCCESS)
            {
#ifdef SNMP_3_WANTED
                V3UtilMsrForOspfTable ((INT4) pV3OspfCxt->u4ContextId,
                                       (INT4) u4SetValOspfv3RouterId, NULL,
                                       FsMIStdOspfv3RouterId,
                                       (sizeof (FsMIStdOspfv3RouterId) /
                                        sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                       u4SeqNum, SNMP_SUCCESS);
#endif
            }
            else
            {
#ifdef SNMP_3_WANTED
                V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                       (INT4) u4SetValOspfv3RouterId, NULL,
                                       FsMIStdOspfv3RouterId,
                                       (sizeof (FsMIStdOspfv3RouterId) /
                                        sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                       u4SeqNum, SNMP_FAILURE);
#endif

                return SNMP_FAILURE;
            }
        }
        else
        {
            MEMCPY (pV3OspfCxt->rtrId, rtrId, OSPFV3_RTR_ID_LEN);

#ifdef SNMP_3_WANTED
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   (INT4) u4SetValOspfv3RouterId, NULL,
                                   FsMIStdOspfv3RouterId,
                                   (sizeof (FsMIStdOspfv3RouterId) /
                                    sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_SUCCESS);
#endif
        }
        MEMCPY (pV3OspfCxt->ConfRtrId, pV3OspfCxt->rtrId, OSPFV3_RTR_ID_LEN);
    }
    /* Previously router-id is selected dynamically
     * now routerid status is updated as permanent */
    /* update routerid status and notify to snmp */
    pV3OspfCxt->u1RouterIdStatus = OSPFV3_ROUTERID_PERMANENT;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           OSPFV3_ROUTERID_PERMANENT, NULL,
                           FsMIOspfv3RouterIdPermanence,
                           (sizeof (FsMIOspfv3RouterIdPermanence) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE, u4SeqNum,
                           SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetOspfv3RouterId\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AdminStat
 Input       :  The Indices

                The Object 
                setValOspfv3AdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AdminStat (INT4 i4SetValOspfv3AdminStat)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = 0;
    tV3OsRouterId       rtrId;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER : nmhSetOspfv3AdminStat Ospfv3AdminStat :%d\n",
                 i4SetValOspfv3AdminStat);

    if (pV3OspfCxt->admnStat != (UINT1) i4SetValOspfv3AdminStat)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        /* If the node status is idle, then this function is called
         * through the MSR restoration while syncing static configuration
         * to the standby node. In this case, if the admin stat is enabled
         * do not enable OSPF bu update a flag to enable OSPF after config
         * restoration completed is received
         */
        if (V3OspfRmGetNodeState () != RM_STANDBY)
        {
            if (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS)
            {
                if (pV3OspfCxt->u1RouterIdStatus == OSPFV3_ROUTERID_DYNAMIC)
                {
                    MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));
                    if (V3UtilSelectOspfRouterId (pV3OspfCxt->u4ContextId,
                                                  &rtrId) == OSPFV3_SUCCESS)
                    {
                        OSPFV3_IPV4_ADDR_COPY (&(pV3OspfCxt->rtrId), &(rtrId));
                        if (O3RedDynSendRtrIdToStandby
                            (rtrId, pV3OspfCxt->u4ContextId) == OSPFV3_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }

        if ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_INIT) &&
            (pV3OspfCxt->admnStat == OSPFV3_ENABLED))
        {
            i1RetVal = OSIX_SUCCESS;
            pV3OspfCxt->u1IsOspfEnabled = OSPFV3_TRUE;
        }
        else
        {
            i1RetVal = (INT1) V3RtrSetProtocolStatusInCxt
                (pV3OspfCxt, (UINT1) i4SetValOspfv3AdminStat);
        }

        if (i1RetVal == OSIX_SUCCESS)
        {
#ifdef SNMP_3_WANTED
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValOspfv3AdminStat, NULL,
                                   FsMIStdOspfv3AdminStat,
                                   (sizeof (FsMIStdOspfv3AdminStat) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_SUCCESS);
#endif
            return SNMP_SUCCESS;
        }
        else
        {
#ifdef SNMP_3_WANTED
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValOspfv3AdminStat, NULL,
                                   FsMIStdOspfv3AdminStat,
                                   (sizeof (FsMIStdOspfv3AdminStat) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_FAILURE);
#endif

            return SNMP_FAILURE;
        }
    }
    else
    {
        if (V3OspfRmGetNodeState () != RM_STANDBY)
        {
            if (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS)
            {
                if (pV3OspfCxt->u1RouterIdStatus == OSPFV3_ROUTERID_DYNAMIC)
                {
                    MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));
                    if (V3UtilSelectOspfRouterId (pV3OspfCxt->u4ContextId,
                                                  &rtrId) == OSPFV3_SUCCESS)
                    {
                        OSPFV3_IPV4_ADDR_COPY (&(pV3OspfCxt->rtrId), &(rtrId));
                        if (O3RedDynSendRtrIdToStandby
                            (rtrId, pV3OspfCxt->u4ContextId) == OSPFV3_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetOspfv3AdminStat\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3ASBdrRtrStatus
 Input       :  The Indices

                The Object 
                setValOspfv3ASBdrRtrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3ASBdrRtrStatus (INT4 i4SetValOspfv3ASBdrRtrStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetOspfv3ASBdrRtrStatus OspfASBdrRtrStatus :%d\n",
                 i4SetValOspfv3ASBdrRtrStatus);

    i4SetValOspfv3ASBdrRtrStatus =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValOspfv3ASBdrRtrStatus);

    if (pV3OspfCxt->bAsBdrRtr != (UINT1) i4SetValOspfv3ASBdrRtrStatus)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        V3RtrSetAsbrStatusInCxt (pV3OspfCxt,
                                 (UINT1) i4SetValOspfv3ASBdrRtrStatus);
#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValOspfv3ASBdrRtrStatus, NULL,
                               FsMIStdOspfv3ASBdrRtrStatus,
                               (sizeof (FsMIStdOspfv3ASBdrRtrStatus) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3ASBdrRtrStatus : SNMP_SUCCESS \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3ExtAreaLsdbLimit
 Input       :  The Indices

                The Object 
                setValOspfv3ExtAreaLsdbLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3ExtAreaLsdbLimit (INT4 i4SetValOspfv3ExtAreaLsdbLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetOspfv3ExtAreaLsdbLimit "
                 "OspfExtAreaLsdbLimit :%d\n", i4SetValOspfv3ExtAreaLsdbLimit);

    if (pV3OspfCxt->i4ExtLsdbLimit != i4SetValOspfv3ExtAreaLsdbLimit)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pV3OspfCxt->i4ExtLsdbLimit = i4SetValOspfv3ExtAreaLsdbLimit;
        V3RtrSetExtLsdbLimitInCxt (pV3OspfCxt);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValOspfv3ExtAreaLsdbLimit, NULL,
                               FsMIStdOspfv3ExtAreaLsdbLimit,
                               (sizeof (FsMIStdOspfv3ExtAreaLsdbLimit) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3ExtAreaLsdbLimit : SNMP_SUCCESS \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3MulticastExtensions
 Input       :  The Indices

                The Object 
                setValOspfv3MulticastExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3MulticastExtensions (tSNMP_OCTET_STRING_TYPE
                                 * pSetValOspfv3MulticastExtensions)
{
    UNUSED_PARAM (*pSetValOspfv3MulticastExtensions);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfv3ExitOverflowInterval
 Input       :  The Indices

                The Object 
                setValOspfv3ExitOverflowInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3ExitOverflowInterval (UINT4 u4SetValOspfv3ExitOverflowInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3ExitOverflowInterval "
                 "OspfExitOverflowInterval :%d\n",
                 u4SetValOspfv3ExitOverflowInterval);

    if (pV3OspfCxt->u4ExitOverflowInterval !=
        (UINT4) u4SetValOspfv3ExitOverflowInterval)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pV3OspfCxt->u4ExitOverflowInterval = u4SetValOspfv3ExitOverflowInterval;
        V3RtrSetExitOverflowIntervalInCxt (pV3OspfCxt);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               (INT4) u4SetValOspfv3ExitOverflowInterval,
                               NULL, FsMIStdOspfv3ExitOverflowInterval,
                               (sizeof (FsMIStdOspfv3ExitOverflowInterval) /
                                sizeof (UINT4)), 'p', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3ExitOverflowInterval \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3DemandExtensions
 Input       :  The Indices

                The Object 
                setValOspfv3DemandExtensions
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3DemandExtensions (INT4 i4SetValOspfv3DemandExtensions)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetOspfv3DemandExtensions "
                 "Ospfv3DemandExtensions :%d\n",
                 i4SetValOspfv3DemandExtensions);

    i4SetValOspfv3DemandExtensions =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValOspfv3DemandExtensions);

    if (pV3OspfCxt->bDemandExtension != i4SetValOspfv3DemandExtensions)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pV3OspfCxt->bDemandExtension = (UINT1) i4SetValOspfv3DemandExtensions;

        if (pV3OspfCxt->admnStat == OSPFV3_ENABLED)
        {
            if (i4SetValOspfv3DemandExtensions != OSIX_TRUE)
            {
                RBTreeWalk (gV3OsRtr.pIfRBRoot, (tRBWalkFn) V3IfDemandRBTAction,
                            pV3OspfCxt, NULL);
            }
            V3LsuGenAllSelfOrgLsasInCxt (pV3OspfCxt);
        }
#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValOspfv3DemandExtensions, NULL,
                               FsMIStdOspfv3DemandExtensions,
                               (sizeof (FsMIStdOspfv3DemandExtensions) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3DemandExtensions \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3TrafficEngineeringSupport
 Input       :  The Indices

                The Object 
                setValOspfv3TrafficEngineeringSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3TrafficEngineeringSupport (INT4
                                       i4SetValOspfv3TrafficEngineeringSupport)
{
    UNUSED_PARAM (i4SetValOspfv3TrafficEngineeringSupport);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfv3ReferenceBandwidth
 Input       :  The Indices

                The Object 
                setValOspfv3ReferenceBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3ReferenceBandwidth (UINT4 u4SetValOspfv3ReferenceBandwidth)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3ReferenceBandwidth "
                 "Ospfv3ReferenceBandwidth :%d\n",
                 u4SetValOspfv3ReferenceBandwidth);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4RefBw = u4SetValOspfv3ReferenceBandwidth;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           (INT4) u4SetValOspfv3ReferenceBandwidth, NULL,
                           FsMIStdOspfv3ReferenceBandwidth,
                           (sizeof (FsMIStdOspfv3ReferenceBandwidth) /
                            sizeof (UINT4)), 'p', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3ReferenceBandwidth\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3RestartSupport
 Input       :  The Indices

                The Object 
                setValOspfv3RestartSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3RestartSupport (INT4 i4SetValOspfv3RestartSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3RestartSupport "
                 "Ospfv3RestartSupport :%d\n", i4SetValOspfv3RestartSupport);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1RestartSupport = (UINT1) i4SetValOspfv3RestartSupport;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValOspfv3RestartSupport, NULL,
                           FsMIStdOspfv3RestartSupport,
                           (sizeof (FsMIStdOspfv3RestartSupport)
                            / sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3RestartSupport\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3RestartInterval
 Input       :  The Indices

                The Object 
                setValOspfv3RestartInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3RestartInterval (INT4 i4SetValOspfv3RestartInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3RestartInterval "
                 "Ospfv3RestartInterval :%d\n", i4SetValOspfv3RestartInterval);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4GracePeriod = (UINT4) i4SetValOspfv3RestartInterval;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValOspfv3RestartInterval, NULL,
                           FsMIStdOspfv3RestartInterval,
                           (sizeof (FsMIStdOspfv3RestartInterval)
                            / sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3RestartInterval\n");
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetOspfv3ImportAsExtern
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3ImportAsExtern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3ImportAsExtern (UINT4 u4Ospfv3AreaId,
                            INT4 i4SetValOspfv3ImportAsExtern)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3ImportAsExtern OspfAreaId :%u "
                 "Ospfv3ImportAsExtern %d\n",
                 u4Ospfv3AreaId, i4SetValOspfv3ImportAsExtern);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)
    {
        pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId));

        if (pArea == NULL)
        {
            return SNMP_FAILURE;
        }

        if (pArea->u4AreaType != (UINT4) i4SetValOspfv3ImportAsExtern)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);

            if (TMO_SLL_Count (&(pArea->ifsInArea)) == 0)
            {
                /* Since no interface is attached, just set the 
                 * area Type and return */
                pArea->u4AreaType = i4SetValOspfv3ImportAsExtern;

                if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
                {
                    OSPFV3_OPT_CLEAR (pArea->areaOptions, OSPFV3_E_BIT_MASK);
                    OSPFV3_OPT_SET (pArea->areaOptions, OSPFV3_N_BIT_MASK);
                    pArea->pV3OspfCxt->u4NssaAreaCount++;
                    OSPFV3_TRC1 (OSPFV3_NSSA_TRC,
                                 pV3OspfCxt->u4ContextId,
                                 "Area %x configured as NSSA area.\n",
                                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pArea->
                                                          areaId));

                }

                else if (pArea->u4AreaType == OSPFV3_NORMAL_AREA)
                {
                    OSPFV3_OPT_CLEAR (pArea->areaOptions, OSPFV3_N_BIT_MASK);
                    OSPFV3_OPT_SET (pArea->areaOptions, OSPFV3_E_BIT_MASK);
                }
                else
                {
                    OSPFV3_OPT_CLEAR_ALL (pArea->areaOptions);
                }
#ifdef SNMP_3_WANTED
                V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId,
                                              u4Ospfv3AreaId,
                                              i4SetValOspfv3ImportAsExtern,
                                              FsMIStdOspfv3ImportAsExtern,
                                              (sizeof
                                               (FsMIStdOspfv3ImportAsExtern) /
                                               sizeof (UINT4)), 'i',
                                              OSPFV3_FALSE,
                                              u4SeqNum, SNMP_SUCCESS);
#endif
                return SNMP_SUCCESS;
            }

            V3AreaChangeAreaType (pArea, i4SetValOspfv3ImportAsExtern);

#ifdef SNMP_3_WANTED
            V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId,
                                          u4Ospfv3AreaId,
                                          i4SetValOspfv3ImportAsExtern,
                                          FsMIStdOspfv3ImportAsExtern,
                                          (sizeof
                                           (FsMIStdOspfv3ImportAsExtern) /
                                           sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                          u4SeqNum, SNMP_SUCCESS);
#endif
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3ImportAsExtern\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaSummary
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3AreaSummary
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaSummary (UINT4 u4Ospfv3AreaId, INT4 i4SetValOspfv3AreaSummary)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3AreaSummary OspfAreaId :%u "
                 "Ospfv3AreaSummary %d\n",
                 u4Ospfv3AreaId, i4SetValOspfv3AreaSummary);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pArea->u1SummaryFunctionality != (UINT1) i4SetValOspfv3AreaSummary)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pArea->u1SummaryFunctionality = (UINT1) i4SetValOspfv3AreaSummary;
        V3AreaSendSummaryStatusChange (pArea);

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId, u4Ospfv3AreaId,
                                      i4SetValOspfv3AreaSummary,
                                      FsMIStdOspfv3AreaSummary,
                                      (sizeof (FsMIStdOspfv3AreaSummary) /
                                       sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3AreaSummary\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaStatus
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3AreaStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaStatus (UINT4 u4Ospfv3AreaId, INT4 i4SetValOspfv3AreaStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER:nmhSetOspfv3AreaStatus "
                 "Ospfv3AreaId :%u Ospfv3AreaStatus: %d\n",
                 u4Ospfv3AreaId, i4SetValOspfv3AreaStatus);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    switch ((UINT1) i4SetValOspfv3AreaStatus)
    {

        case CREATE_AND_GO:
            if ((pArea = (tV3OsArea *) V3AreaCreateInCxt (pV3OspfCxt,
                                                          &areaId)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create area\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, "Area Create Failed\n");
                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);
            pArea->areaStatus = ACTIVE;
            break;
        case CREATE_AND_WAIT:
            if ((pArea = (tV3OsArea *) V3AreaCreateInCxt (pV3OspfCxt,
                                                          &areaId)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create area\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetOspfv3AreaStatus: Area Create Failure\n");
                return SNMP_FAILURE;
            }
            pArea->areaStatus = NOT_IN_SERVICE;
            RM_GET_SEQ_NUM (&u4SeqNum);
            break;
        case ACTIVE:
            pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(areaId));

            if (pArea == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pArea->areaStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pArea->areaStatus = ACTIVE;
            RM_GET_SEQ_NUM (&u4SeqNum);
            break;
        case DESTROY:
            if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);
                V3AreaDelete (pArea);
                pArea = NULL;
            }
            break;
        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId,
                                  u4Ospfv3AreaId, i4SetValOspfv3AreaStatus,
                                  FsMIStdOspfv3AreaStatus,
                                  (sizeof (FsMIStdOspfv3AreaStatus) /
                                   sizeof (UINT4)), 'i', OSPFV3_TRUE,
                                  u4SeqNum, SNMP_SUCCESS);
#endif

    if (pArea != NULL)
    {

        KW_FALSEPOSITIVE_FIX (pArea);
        KW_FALSEPOSITIVE_FIX1 (pArea->pAreaScopeLsaRBRoot->SemId);
        KW_FALSEPOSITIVE_FIX2 (pArea->pSpf->SemId);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetOspfv3StubMetric
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3StubMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3StubMetric (UINT4 u4Ospfv3AreaId, INT4 i4SetValOspfv3StubMetric)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3StubMetric\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pArea->stubDefaultCost.u4Metric != (UINT4) i4SetValOspfv3StubMetric)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pArea->stubDefaultCost.u4Metric = (UINT4) i4SetValOspfv3StubMetric;
        V3AreaParamChange (pArea);
#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId, u4Ospfv3AreaId,
                                      i4SetValOspfv3StubMetric,
                                      FsMIStdOspfv3StubMetric,
                                      (sizeof (FsMIStdOspfv3StubMetric) /
                                       sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3StubMetric\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaStubMetricType
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3StubMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaStubMetricType (UINT4 u4Ospfv3AreaId,
                                INT4 i4SetValOspfv3StubMetricType)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3StubMetricType\n");
    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pArea->stubDefaultCost.u4MetricType !=
        (UINT4) i4SetValOspfv3StubMetricType)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pArea->stubDefaultCost.u4MetricType =
            (UINT4) i4SetValOspfv3StubMetricType;
        V3AreaParamChange (pArea);

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId, u4Ospfv3AreaId,
                                      i4SetValOspfv3StubMetricType,
                                      FsMIStdOspfv3AreaStubMetricType,
                                      (sizeof (FsMIStdOspfv3AreaStubMetricType)
                                       / sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3StubMetricType\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaNssaTranslatorRole
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3AreaNssaTranslatorRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaNssaTranslatorRole (UINT4 u4Ospfv3AreaId,
                                    INT4 i4SetValOspfv3AreaNssaTranslatorRole)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTRY: nmhSetOspfv3AreaNssaTranslatorRole\n");
    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pArea->u1NssaTrnsltrRole !=
        (UINT1) i4SetValOspfv3AreaNssaTranslatorRole)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pArea->u1NssaTrnsltrRole = (UINT1) i4SetValOspfv3AreaNssaTranslatorRole;
        V3AreaNssaTrnsltrRlChng (pArea);

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId,
                                      u4Ospfv3AreaId,
                                      i4SetValOspfv3AreaNssaTranslatorRole,
                                      FsMIStdOspfv3AreaNssaTranslatorRole,
                                      (sizeof
                                       (FsMIStdOspfv3AreaNssaTranslatorRole) /
                                       sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: nmhSetOspfv3AreaNssaTranslatorRole\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                setValOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaNssaTranslatorStabilityInterval (UINT4 u4Ospfv3AreaId,
                                                 UINT4
                                                 u4SetValOspfv3AreaNssaTranslatorStabilityInterval)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId, "ENTRY: "
                "nmhSetOspfv3AreaNssaTranslatorStabilityInterval\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pArea->u4NssaTrnsltrStbltyInterval =
        u4SetValOspfv3AreaNssaTranslatorStabilityInterval;

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfAreaTable (pV3OspfCxt->u4ContextId,
                                  u4Ospfv3AreaId,
                                  (INT4)
                                  u4SetValOspfv3AreaNssaTranslatorStabilityInterval,
                                  FsMIStdOspfv3AreaNssaTranslatorStabilityInterval,
                                  (sizeof
                                   (FsMIStdOspfv3AreaNssaTranslatorStabilityInterval)
                                   / sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                  u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:" "nmhSetOspfv3AreaNssaTranslatorStabilityInterval\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3HostMetric
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                setValOspfv3HostMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3HostMetric (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        INT4 i4SetValOspfv3HostMetric)
{
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Ospfv3HostAddressType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3HostMetric\n");

    pHost = V3GetFindHostInCxt (pV3OspfCxt,
                                (tIp6Addr *) (VOID *) pOspfv3HostAddress->
                                pu1_OctetList);

    if (pHost->u4HostMetric != (UINT4) i4SetValOspfv3HostMetric)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pHost->u4HostMetric = i4SetValOspfv3HostMetric;

        if (pHost->hostStatus == ACTIVE)
        {
            V3GenIntraAreaPrefixLsa (pHost->pArea);
            V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                               &(pHost->hostIp6Addr),
                                               &(pHost->pArea->areaId),
                                               pHost->u4HostMetric,
                                               OSPFV3_UPDATED);
        }
#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfHostTable (pV3OspfCxt->u4ContextId,
                                      i4Ospfv3HostAddressType,
                                      pOspfv3HostAddress,
                                      i4SetValOspfv3HostMetric,
                                      FsMIStdOspfv3HostMetric,
                                      (sizeof (FsMIStdOspfv3HostMetric) /
                                       sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3HostMetric\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3HostStatus
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                setValOspfv3HostStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3HostStatus (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        INT4 i4SetValOspfv3HostStatus)
{
    tV3OsHost          *pHost = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Ospfv3HostAddressType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: nmhSetOspfv3HostStatus\n");

    switch ((UINT1) i4SetValOspfv3HostStatus)
    {
        case CREATE_AND_GO:
            if ((pHost = V3HostAddInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                         pOspfv3HostAddress->pu1_OctetList)) ==
                NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create host\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, " Host Create Failed\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pHost->hostStatus = ACTIVE;
            V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_HOST_ATTACHED,
                                   (UINT1 *) pHost->pArea);
            V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                               &(pHost->hostIp6Addr),
                                               &(pHost->pArea->areaId),
                                               pHost->u4HostMetric,
                                               OSPFV3_CREATED);
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3HostStatus\n");
            break;

        case CREATE_AND_WAIT:
            if ((pHost = V3HostAddInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                         pOspfv3HostAddress->pu1_OctetList)) ==
                NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create host\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetOspfv3HostStatus: Host Create Failed\n");
                return SNMP_FAILURE;
            }
            pHost->hostStatus = NOT_READY;
            RM_GET_SEQ_NUM (&u4SeqNum);

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3HostStatus\n");
            break;

        case ACTIVE:
            pHost = V3GetFindHostInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                        pOspfv3HostAddress->pu1_OctetList);
            pHost->hostStatus = ACTIVE;

            RM_GET_SEQ_NUM (&u4SeqNum);

            V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_HOST_ATTACHED,
                                   (UINT1 *) pHost->pArea);
            V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                               &(pHost->hostIp6Addr),
                                               &(pHost->pArea->areaId),
                                               pHost->u4HostMetric,
                                               OSPFV3_CREATED);
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3HostStatus\n");
            break;

        case DESTROY:
            pHost = V3GetFindHostInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                        pOspfv3HostAddress->pu1_OctetList);
            if (pHost != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                pArea = pHost->pArea;
                V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                                   &(pHost->hostIp6Addr),
                                                   &(pHost->pArea->areaId),
                                                   pHost->u4HostMetric,
                                                   OSPFV3_DELETED);
                V3HostDelete (pHost);

                V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_HOST_DETACHED,
                                       (UINT1 *) pArea);
            }
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3HostStatus\n");
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId, "SNMP Failure\n");
            return SNMP_FAILURE;
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfHostTable (pV3OspfCxt->u4ContextId,
                                  i4Ospfv3HostAddressType, pOspfv3HostAddress,
                                  i4SetValOspfv3HostStatus,
                                  FsMIStdOspfv3HostStatus,
                                  (sizeof (FsMIStdOspfv3HostStatus) /
                                   sizeof (UINT4)), 'i', OSPFV3_TRUE,
                                  u4SeqNum, SNMP_SUCCESS);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3HostAreaID
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                setValOspfv3HostAreaID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3HostAreaID (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        UINT4 u4SetValOspfv3HostAreaID)
{
    tV3OsHost          *pHost = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pTmpArea = NULL;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Ospfv3HostAddressType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3HostAreaId\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4SetValOspfv3HostAreaID);

    pHost =
        V3GetFindHostInCxt (pV3OspfCxt,
                            (tIp6Addr *) (VOID *) pOspfv3HostAddress->
                            pu1_OctetList);

    if (V3UtilAreaIdComp (pHost->pArea->areaId, areaId) != OSPFV3_EQUAL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (pHost->hostStatus == ACTIVE)
        {
            V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                               &(pHost->hostIp6Addr),
                                               &(pHost->pArea->areaId),
                                               pHost->u4HostMetric,
                                               OSPFV3_DELETED);
        }

        pTmpArea = pHost->pArea;

        pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

        if (pArea == NULL)
        {
#ifdef SNMP_3_WANTED
            V3UtilMsrForStdOspfHostTable (pV3OspfCxt->u4ContextId,
                                          i4Ospfv3HostAddressType,
                                          pOspfv3HostAddress,
                                          (INT4) u4SetValOspfv3HostAreaID,
                                          FsMIStdOspfv3HostAreaID,
                                          (sizeof (FsMIStdOspfv3HostAreaID) /
                                           sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                          u4SeqNum, SNMP_FAILURE);
#endif

            return SNMP_FAILURE;
        }

        pHost->pArea = pArea;

        if (pHost->hostStatus == ACTIVE)
        {
            V3GenIntraAreaPrefixLsa (pTmpArea);
            V3GenIntraAreaPrefixLsa (pHost->pArea);
            V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                               &(pHost->hostIp6Addr),
                                               &(pHost->pArea->areaId),
                                               pHost->u4HostMetric,
                                               OSPFV3_CREATED);
        }

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfHostTable (pV3OspfCxt->u4ContextId,
                                      i4Ospfv3HostAddressType,
                                      pOspfv3HostAddress,
                                      (INT4) u4SetValOspfv3HostAreaID,
                                      FsMIStdOspfv3HostAreaID,
                                      (sizeof (FsMIStdOspfv3HostAreaID) /
                                       sizeof (UINT4)), 'p', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3HostAreaId\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfAreaId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfAreaId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfAreaId (INT4 i4Ospfv3IfIndex, UINT4 u4SetValOspfv3IfAreaId)
{
    tV3OsAreaId         areaId;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfAreaId\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4SetValOspfv3IfAreaId);

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if ((pInterface->pArea != NULL) &&
        (V3UtilAreaIdComp (pInterface->pArea->areaId, areaId)) != OSPFV3_EQUAL)
    {
        V3IfSetAreaId (pInterface, &areaId);

    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             (INT4) u4SetValOspfv3IfAreaId,
                             FsMIStdOspfv3IfAreaId, 'p',
                             (sizeof (FsMIStdOspfv3IfAreaId) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfAreaId\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfType
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfType (INT4 i4Ospfv3IfIndex, INT4 i4SetValOspfv3IfType)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfType\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pInterface->u1NetworkType != (UINT1) i4SetValOspfv3IfType)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        V3IfSetType (pInterface, (UINT1) i4SetValOspfv3IfType);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfType,
                                 FsMIStdOspfv3IfType, 'i',
                                 (sizeof (FsMIStdOspfv3IfType) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfType\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfAdminStat
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfAdminStat
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfAdminStat (INT4 i4Ospfv3IfIndex, INT4 i4SetValOspfv3IfAdminStat)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfAdminStat\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pInterface->admnStatus != (UINT1) i4SetValOspfv3IfAdminStat)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        V3IfSetAdmnStat (pInterface, (UINT1) i4SetValOspfv3IfAdminStat);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                                 i4SetValOspfv3IfAdminStat,
                                 FsMIStdOspfv3IfAdminStat, 'i',
                                 (sizeof (FsMIStdOspfv3IfAdminStat) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfAdminStat\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfRtrPriority
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfRtrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfRtrPriority (INT4 i4Ospfv3IfIndex,
                           INT4 i4SetValOspfv3IfRtrPriority)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfRtrPriority\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pInterface->u1RtrPriority != (UINT1) i4SetValOspfv3IfRtrPriority)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pInterface->u1RtrPriority = (UINT1) i4SetValOspfv3IfRtrPriority;
        V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                               OSPFV3_SIG_IF_PRIORITY_CHANGE,
                               (UINT1 *) pInterface);
        V3IsmSchedule (pInterface, OSPFV3_IFE_NBR_CHANGE);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                                 i4SetValOspfv3IfRtrPriority,
                                 FsMIStdOspfv3IfRtrPriority, 'i',
                                 (sizeof (FsMIStdOspfv3IfRtrPriority) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfRtrPriority\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfTransitDelay
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfTransitDelay (INT4 i4Ospfv3IfIndex,
                            INT4 i4SetValOspfv3IfTransitDelay)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfTransitDealay\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u2IfTransDelay = (UINT2) i4SetValOspfv3IfTransitDelay;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfTransitDelay,
                             FsMIStdOspfv3IfTransitDelay, 'i',
                             (sizeof (FsMIStdOspfv3IfTransitDelay) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfTransitDealay\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfRetransInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfRetransInterval (INT4 i4Ospfv3IfIndex,
                               INT4 i4SetValOspfv3IfRetransInterval)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfRetransInterval\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u2RxmtInterval = (UINT2) i4SetValOspfv3IfRetransInterval;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             i4SetValOspfv3IfRetransInterval,
                             FsMIStdOspfv3IfRetransInterval, 'i',
                             (sizeof (FsMIStdOspfv3IfRetransInterval) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfRetransInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfHelloInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfHelloInterval (INT4 i4Ospfv3IfIndex,
                             INT4 i4SetValOspfv3IfHelloInterval)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;
    tV3OsNeighbor      *pNbr;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfHelloInterval\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u2HelloInterval = (UINT2) i4SetValOspfv3IfHelloInterval;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        pNbr->u4LastHelloRcvd = OSPFV3_LESS;
    }

    V3TmrRestartTimer (&(pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pInterface->u2HelloInterval));

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             i4SetValOspfv3IfHelloInterval,
                             FsMIStdOspfv3IfHelloInterval, 'i',
                             (sizeof (FsMIStdOspfv3IfHelloInterval) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfHelloInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfRtrDeadInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfRtrDeadInterval (INT4 i4Ospfv3IfIndex,
                               INT4 i4SetValOspfv3IfRtrDeadInterval)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;
    tV3OsNeighbor      *pNbr;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfRtrDeadInterval\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u2RtrDeadInterval = (UINT2) i4SetValOspfv3IfRtrDeadInterval;

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        pNbr->u4LastHelloRcvd = OSPFV3_LESS;
    }

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             i4SetValOspfv3IfRtrDeadInterval,
                             FsMIStdOspfv3IfRtrDeadInterval, 'i',
                             (sizeof (FsMIStdOspfv3IfRtrDeadInterval) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfRtrDeadInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfPollInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfPollInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfPollInterval (INT4 i4Ospfv3IfIndex,
                            UINT4 u4SetValOspfv3IfPollInterval)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfPollInterval\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u4PollInterval = u4SetValOspfv3IfPollInterval;
    V3TmrRestartTimer (&(pInterface->pollTimer), OSPFV3_POLL_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pInterface->u4PollInterval));

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, u4SetValOspfv3IfPollInterval,
                             FsMIStdOspfv3IfPollInterval, 'p',
                             (sizeof (FsMIStdOspfv3IfPollInterval) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfPollInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfStatus
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setVal/Fur
        Ospfv3IfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfStatus (INT4 i4Ospfv3IfIndex, INT4 i4SetValOspfv3IfStatus)
{
    tV3OsInterface     *pInterface = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    INT4                i4RetVal = NETIPV6_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3IfStatus\n");

    switch ((UINT1) i4SetValOspfv3IfStatus)
    {
        case CREATE_AND_GO:
            i4RetVal =
                NetIpv6GetIfInfo ((UINT4) i4Ospfv3IfIndex, &netIp6IfInfo);
            if (i4RetVal == NETIPV6_FAILURE)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to find ipv6 interface for given interface index.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "NetIpv6GetIfInfo failed for given if \n");
                return (SNMP_FAILURE);
            }
            if ((pInterface = V3IfCreateInCxt (pV3OspfCxt,
                                               &netIp6IfInfo)) == NULL)

            {
                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, "if create Failure \n");
                return SNMP_FAILURE;
            }
            pInterface->ifStatus = ACTIVE;

            RM_GET_SEQ_NUM (&u4SeqNum);

            V3IfActivate (pInterface);

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3IfStatus\n");
            break;

        case CREATE_AND_WAIT:
            i4RetVal =
                NetIpv6GetIfInfo ((UINT4) i4Ospfv3IfIndex, &netIp6IfInfo);
            if (i4RetVal == NETIPV6_FAILURE)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to find ipv6 interface for given interface index.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "NetIpv6GetIfInfo failed for given if \n");
                return (SNMP_FAILURE);
            }

            if ((pInterface = V3IfCreateInCxt (pV3OspfCxt,
                                               &netIp6IfInfo)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create interface\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId, "if create Failure \n");
                return SNMP_FAILURE;
            }
            pInterface->ifStatus = NOT_READY;

            RM_GET_SEQ_NUM (&u4SeqNum);

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3IfStatus\n");
            break;

        case ACTIVE:
            pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
            if (pInterface == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to find given V3 interface\n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindIf failed for given if \n");
                return (SNMP_FAILURE);
            }
            if (pInterface->ifStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            pInterface->ifStatus = ACTIVE;
            RM_GET_SEQ_NUM (&u4SeqNum);

            V3IfActivate (pInterface);

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3IfStatus\n");
            break;

        case DESTROY:
            pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
            if (pInterface != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                V3IfDelete (pInterface);
            }

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT:nmhSetOspfv3IfStatus\n");
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfStatus,
                             FsMIStdOspfv3IfStatus, 'i',
                             (sizeof (FsMIStdOspfv3IfStatus) /
                              sizeof (UINT4)), OSPFV3_TRUE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfMulticastForwarding
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfMulticastForwarding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfMulticastForwarding (INT4 i4Ospfv3IfIndex,
                                   INT4 i4SetValOspfv3IfMulticastForwarding)
{
    UNUSED_PARAM (i4Ospfv3IfIndex);
    UNUSED_PARAM (i4SetValOspfv3IfMulticastForwarding);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfDemand
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfDemand
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfDemand (INT4 i4Ospfv3IfIndex, INT4 i4SetValOspfv3IfDemand)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfDemand\n");

    i4SetValOspfv3IfDemand =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValOspfv3IfDemand);

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (pInterface->bDcEndpt != (UINT1) i4SetValOspfv3IfDemand)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        V3IfSetDemand (pInterface, (UINT1) i4SetValOspfv3IfDemand);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfDemand,
                                 FsMIStdOspfv3IfDemand, 'i',
                                 (sizeof (FsMIStdOspfv3IfDemand) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfDemand\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfMetricValue
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfMetricValue (INT4 i4Ospfv3IfIndex,
                           INT4 i4SetValOspfv3IfMetricValue)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfMetricValue\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (i4SetValOspfv3IfMetricValue != (INT4) pInterface->u4IfMetric)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pInterface->u4IfMetric = (UINT4) i4SetValOspfv3IfMetricValue;

        V3IfParamChange (pInterface, OSPFV3_UPDATE_METRIC);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfMetricValue,
                                 FsMIStdOspfv3IfMetricValue, 'i',
                                 (sizeof (FsMIStdOspfv3IfMetricValue) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfMetricValue\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfInstId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfInstId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfInstId (INT4 i4Ospfv3IfIndex, INT4 i4SetValOspfv3IfInstId)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfInstId\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (i4SetValOspfv3IfInstId != (INT2) pInterface->u2InstId)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pInterface->u2InstId = (UINT2) i4SetValOspfv3IfInstId;

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                                 i4SetValOspfv3IfInstId,
                                 FsMIStdOspfv3IfInstId, 'i',
                                 (sizeof (FsMIStdOspfv3IfInstId) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfInstId\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfDemandNbrProbe
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfDemandNbrProbe
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfDemandNbrProbe (INT4 i4Ospfv3IfIndex,
                              INT4 i4SetValOspfv3IfDemandNbrProbe)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetOspfv3IfDemandNbrProbe\n");

    i4SetValOspfv3IfDemandNbrProbe =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValOspfv3IfDemandNbrProbe);

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->bDemandNbrProbe = (UINT1) i4SetValOspfv3IfDemandNbrProbe;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex, i4SetValOspfv3IfDemandNbrProbe,
                             FsMIStdOspfv3IfDemandNbrProbe, 'i',
                             (sizeof (FsMIStdOspfv3IfDemandNbrProbe) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetOspfv3IfDemandNbrProbe\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfDemandNbrProbeRetxLimit (INT4 i4Ospfv3IfIndex,
                                       UINT4
                                       u4SetValOspfv3IfDemandNbrProbeRetxLimit)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER:nmhSetOspfv3IfDemandNbrProbeRetxLimit\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u4NbrProbeRxmtLimit = u4SetValOspfv3IfDemandNbrProbeRetxLimit;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             (INT4) u4SetValOspfv3IfDemandNbrProbeRetxLimit,
                             FsMIStdOspfv3IfDemandNbrProbeRetxLimit, 'p',
                             (sizeof (FsMIStdOspfv3IfDemandNbrProbeRetxLimit) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT:nmhSetOspfv3IfDemandNbrProbeRetxLimit\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                setValOspfv3IfDemandNbrProbeInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3IfDemandNbrProbeInterval (INT4 i4Ospfv3IfIndex,
                                      UINT4
                                      u4SetValOspfv3IfDemandNbrProbeInterval)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER:nmhSetOspfv3IfDemandNbrProbeInterval\n");

    pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find given V3 interface\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pInterface->u4NbrProbeInterval = u4SetValOspfv3IfDemandNbrProbeInterval;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4Ospfv3IfIndex,
                             (INT4) u4SetValOspfv3IfDemandNbrProbeInterval,
                             FsMIStdOspfv3IfDemandNbrProbeInterval, 'p',
                             (sizeof (FsMIStdOspfv3IfDemandNbrProbeInterval) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT:nmhSetOspfv3IfDemandNbrProbeInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfIndex
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfIndex (UINT4 u4Ospfv3VirtIfAreaId,
                         UINT4 u4Ospfv3VirtIfNeighbor,
                         INT4 i4SetValOspfv3VirtIfIndex)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3VirtIfIndex\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find V3 Virtual interface \n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindVirtIfInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    pInterface->u4InterfaceId = (UINT4) i4SetValOspfv3VirtIfIndex;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfIndex,
                                    FsMIStdOspfv3VirtIfIndex,
                                    (sizeof (FsMIStdOspfv3VirtIfIndex) /
                                     sizeof (UINT4)), OSPFV3_FALSE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3VirtIfIndex\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfTransitDelay
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfTransitDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfTransitDelay (UINT4 u4Ospfv3VirtIfAreaId,
                                UINT4 u4Ospfv3VirtIfNeighbor,
                                INT4 i4SetValOspfv3VirtIfTransitDelay)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3VirtIfTransitDealay\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find V3 Virtual interface \n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindVirtIfInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    pInterface->u2IfTransDelay = (UINT2) i4SetValOspfv3VirtIfTransitDelay;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfTransitDelay,
                                    FsMIStdOspfv3VirtIfTransitDelay,
                                    (sizeof (FsMIStdOspfv3VirtIfTransitDelay) /
                                     sizeof (UINT4)), OSPFV3_FALSE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3VirtIfTransitDealay\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfRetransInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfRetransInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfRetransInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 i4SetValOspfv3VirtIfRetransInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3VirtIfRetransInterval\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find V3 Virtual interface \n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindVirtIfInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    pInterface->u2RxmtInterval = (UINT2) i4SetValOspfv3VirtIfRetransInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfRetransInterval,
                                    FsMIStdOspfv3VirtIfRetransInterval,
                                    (sizeof (FsMIStdOspfv3VirtIfRetransInterval)
                                     / sizeof (UINT4)), OSPFV3_FALSE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3VirtIfRetransInterval\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfHelloInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfHelloInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfHelloInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                 UINT4 u4Ospfv3VirtIfNeighbor,
                                 INT4 i4SetValOspfv3VirtIfHelloInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3VirtIfHelloInterval\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find V3 Virtual interface \n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindVirtIfInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    pInterface->u2HelloInterval = (UINT2) i4SetValOspfv3VirtIfHelloInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfHelloInterval,
                                    FsMIStdOspfv3VirtIfHelloInterval,
                                    (sizeof (FsMIStdOspfv3VirtIfHelloInterval) /
                                     sizeof (UINT4)), OSPFV3_FALSE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3VirtIfHelloInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfRtrDeadInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfRtrDeadInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 i4SetValOspfv3VirtIfRtrDeadInterval)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetOspfv3VirtIfRtrDeadInterval\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to find V3 Virtual interface \n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindVirtIfInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    pInterface->u2RtrDeadInterval = (UINT2) i4SetValOspfv3VirtIfRtrDeadInterval;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfRtrDeadInterval,
                                    FsMIStdOspfv3VirtIfRtrDeadInterval,
                                    (sizeof (FsMIStdOspfv3VirtIfRtrDeadInterval)
                                     / sizeof (UINT4)), OSPFV3_FALSE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3VirtIfRtrDeadInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3VirtIfStatus
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                setValOspfv3VirtIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3VirtIfStatus (UINT4 u4Ospfv3VirtIfAreaId,
                          UINT4 u4Ospfv3VirtIfNeighbor,
                          INT4 i4SetValOspfv3VirtIfStatus)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OsArea          *pTransitArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER nmhSetOspfv3VirtIfStatus:\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    switch ((UINT1) i4SetValOspfv3VirtIfStatus)
    {
        case CREATE_AND_WAIT:
            if (V3VifCreateInCxt (pV3OspfCxt,
                                  &areaId, &nbrId, NOT_IN_SERVICE) == NULL)
            {
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            break;

        case ACTIVE:
            pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
            if (pInterface == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Unable to find V3 Virtual interface \n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindVirtIfInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }
            pInterface->ifStatus = ACTIVE;
            pTransitArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                               &pInterface->transitAreaId);

            if (pTransitArea == NULL)
            {
                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);

            V3GenIntraAreaPrefixLsa (pTransitArea);
            V3VifActivate (pInterface, pTransitArea);
            break;

        case DESTROY:
            pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &areaId, &nbrId);
            if (pInterface != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                V3IfDelete (pInterface);
            }
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfVirtIfTable (pV3OspfCxt->u4ContextId,
                                    u4Ospfv3VirtIfAreaId,
                                    u4Ospfv3VirtIfNeighbor,
                                    i4SetValOspfv3VirtIfStatus,
                                    FsMIStdOspfv3VirtIfStatus,
                                    (sizeof (FsMIStdOspfv3VirtIfStatus) /
                                     sizeof (UINT4)), OSPFV3_TRUE,
                                    u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: nmhSetOspfv3VirtIfStatus:\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3NbmaNbrPriority
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                setValOspfv3NbmaNbrPriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3NbmaNbrPriority (INT4 i4Ospfv3NbmaNbrIfIndex,
                             INT4 i4Ospfv3NbmaNbrAddressType,
                             tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                             INT4 i4SetValOspfv3NbmaNbrPriority)
{
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4IsmSchedFlag = 0;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER nmhSetOspfv3NbmaNbrPriority:\n");

    UNUSED_PARAM (i4Ospfv3NbmaNbrAddressType);

    if ((pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                       pOspfv3NbmaNbrAddress->pu1_OctetList,
                                       (UINT4) i4Ospfv3NbmaNbrIfIndex)) != NULL)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        pNbr->u1ConfiguredNbrRtrPriority =
            (UINT1) i4SetValOspfv3NbmaNbrPriority;

        if (pNbr->u1NbrRtrPriority != (UINT1) i4SetValOspfv3NbmaNbrPriority)
        {
            if ((i4IsmSchedFlag =
                 V3NbrProcessPriorityChange (pNbr, (UINT1)
                                             i4SetValOspfv3NbmaNbrPriority)) ==
                OSPFV3_ISM_SCHEDULED)
            {
                V3IsmSchedule (pNbr->pInterface, OSPFV3_IFE_NBR_CHANGE);
            }
        }
#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfNbmaNbrTable (pV3OspfCxt->u4ContextId,
                                         i4Ospfv3NbmaNbrAddressType,
                                         pOspfv3NbmaNbrAddress,
                                         i4SetValOspfv3NbmaNbrPriority,
                                         FsMIStdOspfv3NbmaNbrPriority,
                                         (sizeof (FsMIStdOspfv3NbmaNbrPriority)
                                          / sizeof (UINT4)), OSPFV3_FALSE,
                                         u4SeqNum, SNMP_SUCCESS);
#endif
        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : nmhSetOspfv3NbmaNbrPriority:\n");

        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfv3NbmaNbrStorageType
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                setValOspfv3NbmaNbrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3NbmaNbrStorageType (INT4 i4Ospfv3NbmaNbrIfIndex,
                                INT4 i4Ospfv3NbmaNbrAddressType,
                                tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                                INT4 i4SetValOspfv3NbmaNbrStorageType)
{
    UNUSED_PARAM (i4Ospfv3NbmaNbrIfIndex);
    UNUSED_PARAM (i4Ospfv3NbmaNbrAddressType);
    UNUSED_PARAM (pOspfv3NbmaNbrAddress);
    UNUSED_PARAM (i4SetValOspfv3NbmaNbrStorageType);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetOspfv3NbmaNbrStatus
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                setValOspfv3NbmaNbrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3NbmaNbrStatus (INT4 i4Ospfv3NbmaNbrIfIndex,
                           INT4 i4Ospfv3NbmaNbrAddressType,
                           tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                           INT4 i4SetValOspfv3NbmaNbrStatus)
{
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsRouterId       defNbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4Ospfv3NbmaNbrAddressType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER nmhSetOspfv3NbmaNbrStatus:\n");

    switch ((UINT1) i4SetValOspfv3NbmaNbrStatus)
    {
        case CREATE_AND_GO:
            /* find the interface on which new neighbor is to be created */
            pInterface = V3GetFindIf ((UINT4) i4Ospfv3NbmaNbrIfIndex);
            if (pInterface == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get V3 Interface on which new nbr is to be created.\n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindIf failed for given if \n");
                return (SNMP_FAILURE);
            }
            OSPFV3_BUFFER_DWTOPDU (defNbrId,
                                   OSPFV3_BUFFER_DWFROMPDU (OSPFV3_NULL_RTRID));
            pNbr = V3NbrCreate ((tIp6Addr *) (VOID *)
                                pOspfv3NbmaNbrAddress->pu1_OctetList,
                                &defNbrId, pInterface, OSPFV3_CONFIGURED_NBR);

            if (pNbr == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create neighbor.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC |
                            OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "Nbr Create Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pNbr->nbrStatus = ACTIVE;
            /* If the interface is Passive interface 
             * no need to Activate the neighbor */
            if (pInterface->bPassive != OSIX_TRUE)
            {
                V3NbrActivate (pNbr);
            }
            break;

        case CREATE_AND_WAIT:
            /* find the interface on which new neighbor is to be created */
            pInterface = V3GetFindIf ((UINT4) i4Ospfv3NbmaNbrIfIndex);
            if (pInterface == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get V3 Interface on which new nbr is created.\n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindIf failed for given if \n");
                return (SNMP_FAILURE);
            }

            OSPFV3_BUFFER_DWTOPDU (defNbrId,
                                   OSPFV3_BUFFER_DWFROMPDU (OSPFV3_NULL_RTRID));
            pNbr = V3NbrCreate ((tIp6Addr *) (VOID *)
                                pOspfv3NbmaNbrAddress->pu1_OctetList,
                                &defNbrId, pInterface, OSPFV3_CONFIGURED_NBR);
            if (pNbr == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create Neighbor.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC |
                            OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "Nbr Create Failure\n");
                return SNMP_FAILURE;
            }
            pNbr->nbrStatus = NOT_IN_SERVICE;
            RM_GET_SEQ_NUM (&u4SeqNum);

            break;

        case ACTIVE:
            pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                          pOspfv3NbmaNbrAddress->pu1_OctetList,
                                          (UINT4) i4Ospfv3NbmaNbrIfIndex);
            if (pNbr == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get Nbma neighbor for given interface.\n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "V3GetFindNbmaNbrInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }
            pNbr->nbrStatus = ACTIVE;

            RM_GET_SEQ_NUM (&u4SeqNum);

            /* If the interface is Passive interface 
             * no need to Activate the neighbor */
            if (pNbr->pInterface->bPassive != OSIX_TRUE)
            {
                V3NbrActivate (pNbr);
            }
            break;

        case DESTROY:
            pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                          pOspfv3NbmaNbrAddress->pu1_OctetList,
                                          (UINT4) i4Ospfv3NbmaNbrIfIndex);
            if (pNbr != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                V3NbrInactivate (pNbr);
                V3NbrDelete (pNbr);
            }
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end switch */
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfNbmaNbrTable (i4Ospfv3NbmaNbrIfIndex,
                                     i4Ospfv3NbmaNbrAddressType,
                                     pOspfv3NbmaNbrAddress,
                                     i4SetValOspfv3NbmaNbrStatus,
                                     FsMIStdOspfv3NbmaNbrStatus,
                                     (sizeof (FsMIStdOspfv3NbmaNbrStatus) /
                                      sizeof (UINT4)), OSPFV3_TRUE,
                                     u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT nmhSetOspfv3NbmaNbrStatus:\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaAggregateStatus
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                setValOspfv3AreaAggregateStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaAggregateStatus (UINT4 u4Ospfv3AreaAggregateAreaID,
                                 INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                 INT4 i4Ospfv3AreaAggregatePrefixType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pOspfv3AreaAggregatePrefix,
                                 UINT4 u4Ospfv3AreaAggregatePrefixLength,
                                 INT4 i4SetValOspfv3AreaAggregateStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: nmhSetOspfv3AreaAggregateStatus:\n");

    UNUSED_PARAM (i4Ospfv3AreaAggregatePrefixType);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        if (i4SetValOspfv3AreaAggregateStatus == DESTROY)
        {
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    switch ((UINT1) i4SetValOspfv3AreaAggregateStatus)
    {
        case CREATE_AND_GO:
            if ((pAddrRange = V3AreaCreateAddrRange (pArea,
                                                     (tIp6Addr *) (VOID *)
                                                     pOspfv3AreaAggregatePrefix->
                                                     pu1_OctetList,
                                                     (UINT1)
                                                     u4Ospfv3AreaAggregatePrefixLength,
                                                     (UINT2)
                                                     i4Ospfv3AreaAggregateAreaLsdbType,
                                                     ACTIVE)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create Area Address Range.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " Area Address Range Create Failed\n");
                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);

            if (i4Ospfv3AreaAggregateAreaLsdbType == OSPFV3_NSSA_LSA)
            {
                V3AreaActiveTransAddrRngInCxt (pV3OspfCxt);
            }
            else
            {
                V3AreaActiveInternalAddrRng (pArea, pAddrRange);
            }
            break;

        case CREATE_AND_WAIT:
            if ((pAddrRange = V3AreaCreateAddrRange (pArea,
                                                     (tIp6Addr *) (VOID *)
                                                     pOspfv3AreaAggregatePrefix->
                                                     pu1_OctetList,
                                                     (UINT1)
                                                     u4Ospfv3AreaAggregatePrefixLength,
                                                     (UINT2)
                                                     i4Ospfv3AreaAggregateAreaLsdbType,
                                                     NOT_IN_SERVICE)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create Area Address Range.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Area Address Range  Create Failed\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            break;

        case ACTIVE:
            pAddrRange = V3GetFindAreaAddrRange (pArea,
                                                 (tIp6Addr *) (VOID *)
                                                 pOspfv3AreaAggregatePrefix->
                                                 pu1_OctetList,
                                                 (UINT1)
                                                 u4Ospfv3AreaAggregatePrefixLength,
                                                 (UINT2)
                                                 i4Ospfv3AreaAggregateAreaLsdbType);

            if (pAddrRange == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to find Area Address Range.\n"));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "V3GetFindAreaAddrRange failed for given if \n");
                return (SNMP_FAILURE);
            }

            if (pAddrRange->areaAggStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);

            pAddrRange->areaAggStatus = ACTIVE;

            if (i4Ospfv3AreaAggregateAreaLsdbType == OSPFV3_NSSA_LSA)
            {
                V3AreaActiveTransAddrRngInCxt (pV3OspfCxt);
            }
            else
            {
                V3AreaActiveInternalAddrRng (pArea, pAddrRange);
            }
            break;

        case DESTROY:

            pAddrRange = V3GetFindAreaAddrRange (pArea,
                                                 (tIp6Addr *) (VOID *)
                                                 pOspfv3AreaAggregatePrefix->
                                                 pu1_OctetList,
                                                 (UINT1)
                                                 u4Ospfv3AreaAggregatePrefixLength,
                                                 (UINT2)
                                                 i4Ospfv3AreaAggregateAreaLsdbType);
            if (pAddrRange != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                if (i4Ospfv3AreaAggregateAreaLsdbType == OSPFV3_NSSA_LSA)
                {
                    V3AreaDeleteTrnsAddrRng (pArea, pAddrRange);
                }
                else
                {
                    V3AreaDeleteInternalAddrRng (pArea, pAddrRange);
                }
            }
            break;

        case NOT_IN_SERVICE:
            /* NOT_IN_SERVICE is not supported
             */
            /* CASE falls through
             */
        default:
            return SNMP_FAILURE;
            /* end of switch */
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfAreaAggregateTable (pV3OspfCxt->u4ContextId,
                                           u4Ospfv3AreaAggregateAreaID,
                                           i4Ospfv3AreaAggregateAreaLsdbType,
                                           i4Ospfv3AreaAggregatePrefixType,
                                           pOspfv3AreaAggregatePrefix,
                                           u4Ospfv3AreaAggregatePrefixLength,
                                           i4SetValOspfv3AreaAggregateStatus,
                                           FsMIStdOspfv3AreaAggregateStatus,
                                           (sizeof
                                            (FsMIStdOspfv3AreaAggregateStatus) /
                                            sizeof (UINT4)), OSPFV3_TRUE,
                                           u4SeqNum, SNMP_SUCCESS);
#endif
    KW_FALSEPOSITIVE_FIX (pAddrRange);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetOspfv3AreaAggregateStatus\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaAggregateEffect
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                setValOspfv3AreaAggregateEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaAggregateEffect (UINT4 u4Ospfv3AreaAggregateAreaID,
                                 INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                 INT4 i4Ospfv3AreaAggregatePrefixType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pOspfv3AreaAggregatePrefix,
                                 UINT4 u4Ospfv3AreaAggregatePrefixLength,
                                 INT4 i4SetValOspfv3AreaAggregateEffect)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = (tV3OsArea *) NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetOspfv3AreaAggregateEffect\n");

    UNUSED_PARAM (i4Ospfv3AreaAggregatePrefixType);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    pAddrRange = V3GetFindAreaAddrRange (pArea, (tIp6Addr *) (VOID *)
                                         pOspfv3AreaAggregatePrefix->
                                         pu1_OctetList,
                                         (UINT1)
                                         u4Ospfv3AreaAggregatePrefixLength,
                                         (UINT2)
                                         i4Ospfv3AreaAggregateAreaLsdbType);
    if (pAddrRange == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get  Area Address Range.\n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindAreaAddrRange failed for given if \n");
        return (SNMP_FAILURE);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (pAddrRange->u1AggrEffect != (UINT1) i4SetValOspfv3AreaAggregateEffect)
    {
        if (i4Ospfv3AreaAggregateAreaLsdbType == OSPFV3_NSSA_LSA)
        {
            V3AreaNssaSetAggrEffect (pArea, pAddrRange,
                                     (UINT1) i4SetValOspfv3AreaAggregateEffect);
        }
        else
        {
            V3AreaNormalSetAggrEffect (pArea, pAddrRange,
                                       (UINT1)
                                       i4SetValOspfv3AreaAggregateEffect);
        }
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForStdOspfAreaAggregateTable (pV3OspfCxt->u4ContextId,
                                           u4Ospfv3AreaAggregateAreaID,
                                           i4Ospfv3AreaAggregateAreaLsdbType,
                                           i4Ospfv3AreaAggregatePrefixType,
                                           pOspfv3AreaAggregatePrefix,
                                           u4Ospfv3AreaAggregatePrefixLength,
                                           i4SetValOspfv3AreaAggregateEffect,
                                           FsMIStdOspfv3AreaAggregateEffect,
                                           (sizeof
                                            (FsMIStdOspfv3AreaAggregateEffect) /
                                            sizeof (UINT4)), OSPFV3_FALSE,
                                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetOspfv3AreaAggregateEffect\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaAggregateRouteTag
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                setValOspfv3AreaAggregateRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaAggregateRouteTag (UINT4 u4Ospfv3AreaAggregateAreaID,
                                   INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                   INT4 i4Ospfv3AreaAggregatePrefixType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pOspfv3AreaAggregatePrefix,
                                   UINT4 u4Ospfv3AreaAggregatePrefixLength,
                                   INT4 i4SetValOspfv3AreaAggregateRouteTag)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetOspfv3AreaAggregateRouteTag\n");

    UNUSED_PARAM (i4Ospfv3AreaAggregatePrefixType);
    UNUSED_PARAM (i4SetValOspfv3AreaAggregateRouteTag);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    pAddrRange = V3GetFindAreaAddrRange (pArea, (tIp6Addr *) (VOID *)
                                         pOspfv3AreaAggregatePrefix->
                                         pu1_OctetList,
                                         (UINT1)
                                         u4Ospfv3AreaAggregatePrefixLength,
                                         (UINT2)
                                         i4Ospfv3AreaAggregateAreaLsdbType);
    if (pAddrRange == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get Area Address Range.\n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindAreaAddrRange failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pAddrRange->u4AddrRangeRtTag !=
        (UINT4) i4SetValOspfv3AreaAggregateRouteTag)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pAddrRange->u4AddrRangeRtTag =
            (UINT4) i4SetValOspfv3AreaAggregateRouteTag;

        V3AreaSetAddrRangeTag (pArea, pAddrRange);

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaAggregateTable
            (pV3OspfCxt->u4ContextId,
             u4Ospfv3AreaAggregateAreaID,
             i4Ospfv3AreaAggregateAreaLsdbType,
             i4Ospfv3AreaAggregatePrefixType,
             pOspfv3AreaAggregatePrefix,
             u4Ospfv3AreaAggregatePrefixLength,
             i4SetValOspfv3AreaAggregateRouteTag,
             FsMIStdOspfv3AreaAggregateRouteTag,
             (sizeof
              (FsMIStdOspfv3AreaAggregateRouteTag)
              / sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetOspfv3AreaAggregateRouteTag\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3TraceLevel
 Input       :  The Indices

                The Object 
                setValFutOspfv3TraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3TraceLevel (INT4 i4SetValFutOspfv3TraceLevel)
{
#ifdef TRACE_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4TraceValue = i4SetValFutOspfv3TraceLevel;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3TraceLevel, NULL,
                           FsMIOspfv3TraceLevel,
                           (sizeof (FsMIOspfv3TraceLevel) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFutOspfv3TraceLevel);
    return SNMP_FAILURE;
#endif /* TRACE_WANTED */
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3ABRType
 Input       :  The Indices

                The Object 
                setValFutOspfv3ABRType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3ABRType (INT4 i4SetValFutOspfv3ABRType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3ABRType\n");

    /* If setting value is same as the present value 
     * no need to post any message. */
    if (pV3OspfCxt->u1ABRType != (UINT1) i4SetValFutOspfv3ABRType)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if (pV3OspfCxt->admnStat == OSPFV3_DISABLED)
        {
            pV3OspfCxt->u1ABRType = (UINT1) i4SetValFutOspfv3ABRType;
#ifdef SNMP_3_WANTED
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValFutOspfv3ABRType, NULL,
                                   FsMIOspfv3ABRType,
                                   (sizeof (FsMIOspfv3ABRType) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_SUCCESS);
#endif

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3ABRType\n");

            return SNMP_SUCCESS;
        }

        V3RtrSetABRTypeInCxt (pV3OspfCxt, (UINT4) i4SetValFutOspfv3ABRType);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3ABRType, NULL,
                               FsMIOspfv3ABRType,
                               (sizeof (FsMIOspfv3ABRType) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3ABRType\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                setValFutOspfv3NssaAsbrDefRtTrans
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3NssaAsbrDefRtTrans (INT4 i4SetValFutOspfv3NssaAsbrDefRtTrans)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3NssaAsbrDefRtTrans\n");

    i4SetValFutOspfv3NssaAsbrDefRtTrans =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValFutOspfv3NssaAsbrDefRtTrans);

    if (pV3OspfCxt->bNssaAsbrDefRtTrans != (UINT1)
        i4SetValFutOspfv3NssaAsbrDefRtTrans)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pV3OspfCxt->bNssaAsbrDefRtTrans =
            (UINT1) i4SetValFutOspfv3NssaAsbrDefRtTrans;

        V3ModifyNssaAsbrDefRtTransInCxt (pV3OspfCxt);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3NssaAsbrDefRtTrans, NULL,
                               FsMIOspfv3NssaAsbrDefRtTrans,
                               (sizeof (FsMIOspfv3NssaAsbrDefRtTrans) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3NssaAsbrDefRtTrans\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3DefaultPassiveInterface
 Input       :  The Indices

                The Object 
                setValFutOspfv3DefaultPassiveInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3DefaultPassiveInterface (INT4
                                        i4SetValFutOspfv3DefaultPassiveInterface)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3DefaultPassiveInterface\n");

    i4SetValFutOspfv3DefaultPassiveInterface =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValFutOspfv3DefaultPassiveInterface);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->bDefaultPassiveInterface =
        (UINT1) i4SetValFutOspfv3DefaultPassiveInterface;

    /* Scan all the interfaces and set the passive value configured */
    V3IfAllPassive ((UINT1) i4SetValFutOspfv3DefaultPassiveInterface);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3DefaultPassiveInterface, NULL,
                           FsMIOspfv3DefaultPassiveInterface,
                           (sizeof (FsMIOspfv3DefaultPassiveInterface) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3DefaultPassiveInterface\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3SpfDelay
 Input       :  The Indices

                The Object 
                setValFutOspfv3SpfDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3SpfDelay (INT4 i4SetValFutOspfv3SpfDelay)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetOspfv3FutOspfv3SpfDelay "
                 "i4SetValFutOspfv3SpfDelay :%d\n", i4SetValFutOspfv3SpfDelay);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4SpfInterval = (UINT4) i4SetValFutOspfv3SpfDelay;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId, i4SetValFutOspfv3SpfDelay,
                           NULL, FsMIOspfv3SpfDelay,
                           (sizeof (FsMIOspfv3SpfDelay) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3SpfDelay\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3SpfHoldTime
 Input       :  The Indices

                The Object 
                setValFutOspfv3SpfHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3SpfHoldTime (INT4 i4SetValFutOspfv3SpfHoldTime)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetFutOspfv3SpfHoldTime FutOspfv3SpfHoldTime :%d\n",
                 i4SetValFutOspfv3SpfHoldTime);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4SpfHoldTimeInterval = (UINT4) i4SetValFutOspfv3SpfHoldTime;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3SpfHoldTime, NULL,
                           FsMIOspfv3SpfHoldTime,
                           (sizeof (FsMIOspfv3SpfHoldTime) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3SpfHoldTime\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfPassive
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                setValFutOspfv3IfPassive
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfPassive (INT4 i4FutOspfv3IfIndex,
                          INT4 i4SetValFutOspfv3IfPassive)
{
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER:nmhSetFutOspfv3IfPassive\n");

    i4SetValFutOspfv3IfPassive =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValFutOspfv3IfPassive);

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get V3 Interface for given interface index.\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (pInterface->bPassive != (UINT1) i4SetValFutOspfv3IfPassive)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        if ((pInterface->ifStatus != ACTIVE) ||
            (pInterface->admnStatus != OSPFV3_ENABLED))
        {
            pInterface->bPassive = (UINT1) i4SetValFutOspfv3IfPassive;
#ifdef SNMP_3_WANTED
            V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex,
                                     i4SetValFutOspfv3IfPassive,
                                     FsMIOspfv3IfPassive, 'i',
                                     (sizeof (FsMIOspfv3IfPassive) /
                                      sizeof (UINT4)), OSPFV3_FALSE,
                                     u4SeqNum, SNMP_SUCCESS);
#endif

            return SNMP_SUCCESS;
        }

        pInterface->bPassive = (UINT1) i4SetValFutOspfv3IfPassive;
        V3IfSetPassive (pInterface);
    }

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex, i4SetValFutOspfv3IfPassive,
                             FsMIOspfv3IfPassive, 'i',
                             (sizeof (FsMIOspfv3IfPassive) /
                              sizeof (UINT4)), OSPFV3_FALSE,
                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:nmhSetFutOspfv3IfPassive\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                setValFutOspfv3IfLinkLSASuppression
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfLinkLSASuppression (INT4 i4FutOspfv3IfIndex,
                                     INT4 i4SetValFutOspfv3IfLinkLSASuppression)
{

    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER:nmhSetFutOspfv3IfLinkLSASuppression\n");

    i4SetValFutOspfv3IfLinkLSASuppression =
        OSPFV3_MAP_FROM_TRUTH_VALUE (i4SetValFutOspfv3IfLinkLSASuppression);

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get V3 Interface for given interface index.\n"));

        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (pInterface->bLinkLsaSuppress !=
        (UINT1) i4SetValFutOspfv3IfLinkLSASuppression)
    {

        RM_GET_SEQ_NUM (&u4SeqNum);
        pInterface->bLinkLsaSuppress =
            (UINT1) i4SetValFutOspfv3IfLinkLSASuppression;
        V3IfSetLinkLSASuppression (pInterface);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex,
                                 OSPFV3_MAP_TO_TRUTH_VALUE
                                 (i4SetValFutOspfv3IfLinkLSASuppression),
                                 FsMIOspfv3IfLinkLSASuppression, 'i',
                                 (sizeof (FsMIOspfv3IfLinkLSASuppression) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif

    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT:nmhSetFutOspfv3IfLinkLSASuppression\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfBfdState
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object  setValFutOspfv3IfBfdState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfBfdState (INT4 i4FutOspfv3IfIndex,
                           INT4 i4SetValFutOspfv3IfBfdState)
{
    tV3OsInterface     *pInterface;
    tV3OsNeighbor      *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    UINT4               u4SeqNum;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);

    if (pInterface != NULL)
    {
        OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "ENTER :nmhSetFutOspfv3IfBfdState\n");

        pInterface->u1BfdIfStatus = (UINT1) i4SetValFutOspfv3IfBfdState;

        RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex,
                                 i4SetValFutOspfv3IfBfdState,
                                 FsMIOspfv3IfBfdState, 'i',
                                 (sizeof (FsMIOspfv3IfBfdState) /
                                  sizeof (UINT4)), OSPFV3_FALSE,
                                 u4SeqNum, SNMP_SUCCESS);
#endif

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

            if (pInterface->u1BfdIfStatus == OSPFV3_BFD_ENABLED)
            {
                if ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
                    (pNbr->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST))
                {
                    if ((pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                        (pNbr->pInterface->u1IsmState == OSPFV3_IFS_BACKUP) ||
                        (pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR_OTHER))
                    {
                        if ((((pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                              (pNbr->pInterface->u1IsmState ==
                               OSPFV3_IFS_BACKUP))
                             && (pNbr->u1NsmState == OSPFV3_NBRS_FULL))
                            ||
                            (((pNbr->pInterface->u1IsmState != OSPFV3_IFS_DR)
                              && (pNbr->pInterface->u1IsmState !=
                                  OSPFV3_IFS_BACKUP))
                             && (pNbr->u1NsmState == OSPFV3_NBRS_2WAY)))
                        {
                            if (OSIX_SUCCESS != IfOspfv3BfdRegister (pNbr))
                            {
                                V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                            }
                        }
                    }
                }
            }
            if (pInterface->u1BfdIfStatus != OSPFV3_BFD_ENABLED &&
                pNbr->u1NbrBfdState == OSPFV3_BFD_ENABLED)
            {
                IfOspfv3BfdDeRegister (pNbr, OSIX_FALSE);
            }
        }

        OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT :nmhSetFutOspfv3IfBfdState\n");

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKey
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKey (INT4 i4FutOspfv3IfAuthIfIndex,
                          INT4 i4FutOspfv3IfAuthKeyId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFutOspfv3IfAuthKey)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum;
    UNUSED_PARAM (i4FutOspfv3IfAuthKeyId);

    if ((pOspfCxt == NULL) || (pSetValFutOspfv3IfAuthKey == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3IfAuthKey\n");

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The entry already exists, The Authentication key is changed */
    V3AuthKeyCopy (pAuthkeyInfo->au1AuthKey,
                   pSetValFutOspfv3IfAuthKey->pu1_OctetList,
                   pSetValFutOspfv3IfAuthKey->i4_Length);

    pAuthkeyInfo->u1KeyLen = (UINT1) pSetValFutOspfv3IfAuthKey->i4_Length;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId,
                                 pSetValFutOspfv3IfAuthKey, 0,
                                 FsMIOspfv3IfAuthKey, 's',
                                 (sizeof (FsMIOspfv3IfAuthKey) /
                                  sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3IfAuthKey\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKeyStartAccept (INT4 i4FutOspfv3IfAuthIfIndex,
                                     INT4 i4FutOspfv3IfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pSetValFutOspfv3IfAuthKeyStartAccept)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3IfAuthKeyStartAccept\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3IfAuthKeyStartAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);

    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo->u4KeyStartAccept = ((UINT4) i4AuthKeyStartAccept);

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId,
                                 pSetValFutOspfv3IfAuthKeyStartAccept, 0,
                                 FsMIOspfv3IfAuthKeyStartAccept, 's',
                                 (sizeof (FsMIOspfv3IfAuthKeyStartAccept) /
                                  sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3IfAuthKeyStartAccept\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKeyStartGenerate (INT4 i4FutOspfv3IfAuthIfIndex,
                                       INT4 i4FutOspfv3IfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pSetValFutOspfv3IfAuthKeyStartGenerate)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTRY : nmhSetFutOspfv3IfAuthKeyStartGenerate\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3IfAuthKeyStartGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);

    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo->u4KeyStartGenerate = ((UINT4) i4AuthKeyStartGenerate);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId,
                                 pSetValFutOspfv3IfAuthKeyStartGenerate, 0,
                                 FsMIOspfv3IfAuthKeyStartGenerate, 's',
                                 (sizeof (FsMIOspfv3IfAuthKeyStartGenerate) /
                                  sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3IfAuthKeyStartGenerate\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKeyStopGenerate (INT4 i4FutOspfv3IfAuthIfIndex,
                                      INT4 i4FutOspfv3IfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pSetValFutOspfv3IfAuthKeyStopGenerate)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3IfAuthKeyStopGenerate\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3IfAuthKeyStopGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStopGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo->u4KeyStopGenerate = ((UINT4) i4AuthKeyStopGenerate);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId,
                                 pSetValFutOspfv3IfAuthKeyStopGenerate, 0,
                                 FsMIOspfv3IfAuthKeyStopGenerate, 's',
                                 (sizeof (FsMIOspfv3IfAuthKeyStopGenerate) /
                                  sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3IfAuthKeyStopGenerate\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKeyStopAccept (INT4 i4FutOspfv3IfAuthIfIndex,
                                    INT4 i4FutOspfv3IfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pSetValFutOspfv3IfAuthKeyStopAccept)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3IfAuthKeyStopAccept\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3IfAuthKeyStopAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStopAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);

    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo->u4KeyStopAccept = ((UINT4) i4AuthKeyStopAccept);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId,
                                 pSetValFutOspfv3IfAuthKeyStopAccept, 0,
                                 FsMIOspfv3IfAuthKeyStopAccept, 's',
                                 (sizeof (FsMIOspfv3IfAuthKeyStopAccept) /
                                  sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3IfAuthKeyStopAccept\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                setValFutOspfv3IfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfAuthKeyStatus (INT4 i4FutOspfv3IfAuthIfIndex,
                                INT4 i4FutOspfv3IfAuthKeyId,
                                INT4 i4SetValFutOspfv3IfAuthKeyStatus)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3IfAuthKeyStatus\n");

    pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                i4FutOspfv3IfAuthIfIndex,
                                                (UINT2) i4FutOspfv3IfAuthKeyId);

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex)) == NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "If find Failure\n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFutOspfv3IfAuthKeyStatus)
    {
        case CREATE_AND_WAIT:
            if (pAuthkeyInfo != NULL)
            {
                return SNMP_SUCCESS;
            }
            /* A new entry needs to be created , allocate memory , assign def vals */
            if ((pAuthkeyInfo = V3AuthKeyInfoCreate ()) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for SHA AuthKeyInfo.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pOspfCxt->u4ContextId,
                            "SHA AuthKeyInfo Allocation Failure\n");
                return SNMP_FAILURE;
            }
            pAuthkeyInfo->u2AuthkeyId = (UINT2) i4FutOspfv3IfAuthKeyId;
            V3SortInsertAuthKeyInfo (&(pInterface->sortAuthkeyLst),
                                     pAuthkeyInfo);
            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfv3IfAuthKeyStatus;

            KW_FALSEPOSITIVE_FIX (pAuthkeyInfo);
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
            if (pAuthkeyInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfv3IfAuthKeyStatus;
            break;

        case DESTROY:
            if (pInterface->pLastAuthkey == pAuthkeyInfo)
                pInterface->pLastAuthkey = NULL;

            TMO_SLL_Delete (&(pInterface->sortAuthkeyLst),
                            &(pAuthkeyInfo->nextSortKey));
            AUTH_FREE (pAuthkeyInfo);
            break;
        default:
            /*   It will never happen   */
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                 i4FutOspfv3IfAuthIfIndex,
                                 i4FutOspfv3IfAuthKeyId, NULL,
                                 i4SetValFutOspfv3IfAuthKeyStatus,
                                 FsMIOspfv3IfAuthKeyStatus, 'i',
                                 (sizeof (FsMIOspfv3IfAuthKeyStatus) /
                                  sizeof (UINT4)), OSPFV3_TRUE, u4SeqNum,
                                 SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT: nmhSetFutOspfv3IfAuthKeyStatus \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKey
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKey (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                              UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                              INT4 i4FutOspfv3VirtIfAuthKeyId,
                              tSNMP_OCTET_STRING_TYPE
                              * pSetValFutOspfv3VirtIfAuthKey)
{
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tAuthkeyInfo       *pCurrentAuthkeyInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum;

    if ((pOspfCxt == NULL) || (pSetValFutOspfv3VirtIfAuthKey == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER: nmhSetFutOspfv3VirtIfAuthKey \n");

    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&nbrId, OSPFV3_ZERO, sizeof (tV3OsRouterId));

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);
    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    if ((pInterface == NULL) || (pAuthkeyInfo == NULL))
    {
        return SNMP_FAILURE;

    }

    /* The entry already exists, The Authentication key is Need to be updated */
    V3AuthKeyCopy (pAuthkeyInfo->au1AuthKey,
                   pSetValFutOspfv3VirtIfAuthKey->pu1_OctetList,
                   pSetValFutOspfv3VirtIfAuthKey->i4_Length);

    pAuthkeyInfo->u1KeyLen = (UINT1) pSetValFutOspfv3VirtIfAuthKey->i4_Length;

    /*If the current key in use is changed, make the virtual interface down and up if it is already up */
    pCurrentAuthkeyInfo = V3GetAuthkeyTouse (pInterface);

    if ((pCurrentAuthkeyInfo == pAuthkeyInfo)
        && (pInterface->u1IsmState != OSPFV3_IFS_DOWN))
    {
        V3IfDown (pInterface);
        V3IfUp (pInterface);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId,
                                     pSetValFutOspfv3VirtIfAuthKey, 0,
                                     FsMIOspfv3VirtIfAuthKey, 's',
                                     (sizeof (FsMIOspfv3VirtIfAuthKey) /
                                      sizeof (UINT4)), OSPFV3_FALSE,
                                     u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT: nmhSetFutOspfv3VirtIfAuthKey\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKeyStartAccept (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                         UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                         INT4 i4FutOspfv3VirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pSetValFutOspfv3VirtIfAuthKeyStartAccept)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStartAccept = 0;
    tUtlTm              tm;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    UINT4               u4SeqNum;

    if ((pOspfCxt == NULL) ||
        (pSetValFutOspfv3VirtIfAuthKeyStartAccept == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER: nmhSetFutOspfv3VirtIfAuthKeyStartAccept \n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&nbrId, OSPFV3_ZERO, sizeof (tV3OsRouterId));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3VirtIfAuthKeyStartAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStartAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pAuthkeyInfo->u4KeyStartAccept = ((UINT4) i4AuthKeyStartAccept);
    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId,
                                     pSetValFutOspfv3VirtIfAuthKeyStartAccept,
                                     0, FsMIOspfv3VirtIfAuthKeyStartAccept, 's',
                                     (sizeof
                                      (FsMIOspfv3VirtIfAuthKeyStartAccept) /
                                      sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                     SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStartAccept \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKeyStartGenerate (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                           UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                           INT4 i4FutOspfv3VirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pSetValFutOspfv3VirtIfAuthKeyStartGenerate)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStartGenerate = 0;
    tUtlTm              tm;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    UINT4               u4SeqNum;

    if ((pOspfCxt == NULL) ||
        (pSetValFutOspfv3VirtIfAuthKeyStartGenerate == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3VirtIfAuthKeyStartGenerate\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&nbrId, OSPFV3_ZERO, sizeof (tV3OsRouterId));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3VirtIfAuthKeyStartGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStartGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pAuthkeyInfo->u4KeyStartGenerate = ((UINT4) i4AuthKeyStartGenerate);

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId,
                                     pSetValFutOspfv3VirtIfAuthKeyStartGenerate,
                                     0, FsMIOspfv3VirtIfAuthKeyStartGenerate,
                                     's',
                                     (sizeof
                                      (FsMIOspfv3VirtIfAuthKeyStartGenerate) /
                                      sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                     SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStartGenerate\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKeyStopGenerate (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                          UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                          INT4 i4FutOspfv3VirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pSetValFutOspfv3VirtIfAuthKeyStopGenerate)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStopGenerate = 0;
    tUtlTm              tm;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    UINT4               u4SeqNum;

    if ((pOspfCxt == NULL) ||
        (pSetValFutOspfv3VirtIfAuthKeyStopGenerate == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3VirtIfAuthKeyStopGenerate\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&nbrId, OSPFV3_ZERO, sizeof (tV3OsRouterId));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3VirtIfAuthKeyStopGenerate->
                              pu1_OctetList, &tm);
    i4AuthKeyStopGenerate = (INT4) V3UtilGetSecondsSinceBase (tm);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pAuthkeyInfo->u4KeyStopGenerate = ((UINT4) i4AuthKeyStopGenerate);

    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId,
                                     pSetValFutOspfv3VirtIfAuthKeyStopGenerate,
                                     0, FsMIOspfv3VirtIfAuthKeyStopGenerate,
                                     's',
                                     (sizeof
                                      (FsMIOspfv3VirtIfAuthKeyStopGenerate) /
                                      sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                     SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStopGenerate\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKeyStopAccept (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                        UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                        INT4 i4FutOspfv3VirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pSetValFutOspfv3VirtIfAuthKeyStopAccept)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    INT4                i4AuthKeyStopAccept = 0;
    tUtlTm              tm;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    UINT4               u4SeqNum;

    if ((pOspfCxt == NULL) || (pSetValFutOspfv3VirtIfAuthKeyStopAccept == NULL))
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3VirtIfAuthKeyStopAccept\n");

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&nbrId, OSPFV3_ZERO, sizeof (tV3OsRouterId));
    V3OspfConvertTimeForSnmp (pSetValFutOspfv3VirtIfAuthKeyStopAccept->
                              pu1_OctetList, &tm);
    i4AuthKeyStopAccept = (INT4) V3UtilGetSecondsSinceBase (tm);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo->u4KeyStopAccept = ((UINT4) i4AuthKeyStopAccept);

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId,
                                     pSetValFutOspfv3VirtIfAuthKeyStopAccept, 0,
                                     FsMIOspfv3VirtIfAuthKeyStopAccept, 's',
                                     (sizeof (FsMIOspfv3VirtIfAuthKeyStopAccept)
                                      / sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                                     SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStopAccept\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                setValFutOspfv3VirtIfAuthKeyStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfAuthKeyStatus (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                    UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                    INT4 i4FutOspfv3VirtIfAuthKeyId,
                                    INT4 i4SetValFutOspfv3VirtIfAuthKeyStatus)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3VirtIfAuthKeyStatus\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &areaId,
                                                    &nbrId,
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &(nbrId));
    if (pInterface == NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pOspfCxt->u4ContextId, "Interface Not Found \n");
        return SNMP_FAILURE;
    }

    switch (i4SetValFutOspfv3VirtIfAuthKeyStatus)
    {

        case CREATE_AND_WAIT:
            if (pAuthkeyInfo != NULL)
            {

                OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                            "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStatus\n");
                return SNMP_SUCCESS;
            }

            /*A new entry needs to be created , allocate memory , assign def vals */
            if ((pAuthkeyInfo = V3AuthKeyInfoCreate ()) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for AuthKeyInfo.\n"));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pOspfCxt->u4ContextId,
                            "AuthKeyInfo Allocation Failure\n");
                return SNMP_FAILURE;
            }

            pAuthkeyInfo->u2AuthkeyId = (UINT2) i4FutOspfv3VirtIfAuthKeyId;
            V3SortInsertAuthKeyInfo ((tTMO_SLL *) &
                                     (pInterface->sortAuthkeyLst),
                                     pAuthkeyInfo);

            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfv3VirtIfAuthKeyStatus;

            KW_FALSEPOSITIVE_FIX (pAuthkeyInfo);
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
            if (pAuthkeyInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            pAuthkeyInfo->u1AuthkeyStatus =
                (UINT1) i4SetValFutOspfv3VirtIfAuthKeyStatus;
            break;

        case DESTROY:
            if (pAuthkeyInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            if (pInterface->pLastAuthkey == pAuthkeyInfo)
                pInterface->pLastAuthkey = NULL;
            TMO_SLL_Delete (&(pInterface->sortAuthkeyLst),
                            &(pAuthkeyInfo->nextSortKey));
            AUTH_FREE (pAuthkeyInfo);
            break;

        default:
            /*   It will never happen   */
            return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfAuthTable ((INT4) pOspfCxt->u4ContextId,
                                     u4FutOspfv3VirtIfAuthAreaId,
                                     u4FutOspfv3VirtIfAuthNeighbor,
                                     i4FutOspfv3VirtIfAuthKeyId, NULL,
                                     i4SetValFutOspfv3VirtIfAuthKeyStatus,
                                     FsMIOspfv3VirtIfAuthKeyStatus, 'i',
                                     (sizeof (FsMIOspfv3VirtIfAuthKeyStatus) /
                                      sizeof (UINT4)), OSPFV3_TRUE,
                                     u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfAuthKeyStatus\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfCryptoAuthType
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                setValFutOspfv3IfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfCryptoAuthType (INT4 i4FutOspfv3IfIndex,
                                 INT4 i4SetValFutOspfv3IfCryptoAuthType)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = OSPFV3_ZERO;
    tAuthkeyInfo       *pScanAuthKeyNode = NULL;
    tAuthkeyInfo       *pTempAuthKeyNode = NULL;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3IfCryptoAuthType\n");

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface->u2CryptoAuthType = (UINT2) i4SetValFutOspfv3IfCryptoAuthType;
    if (pInterface->u2CryptoAuthType == OSPFV3_AUTH_NO_TYPE)
    {
        pInterface->u2AuthType = OSPFV3_NO_AUTH;
        pInterface->u1AuthMode = OSPFV3_AUTH_MODE_NONE;
    }
    else
    {
        pInterface->u2AuthType = OSPFV3_CRYPT_AUTH;
        if (pInterface->u1AuthMode == OSPFV3_AUTH_MODE_NONE)
        {
            pInterface->u1AuthMode = OSPFV3_AUTH_MODE_FULL;
        }
    }
    pInterface->pLastAuthkey = NULL;

    if (pInterface->u2CryptoAuthType == OSPFV3_AUTH_NO_TYPE)
    {
        OSPFV3_DYNM_SLL_SCAN (&(pInterface->sortAuthkeyLst), pScanAuthKeyNode,
                              pTempAuthKeyNode, tAuthkeyInfo *)
        {
            TMO_SLL_Delete (&(pInterface->sortAuthkeyLst),
                            &(pScanAuthKeyNode->nextSortKey));
            AUTH_FREE (pScanAuthKeyNode);
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex,
                             i4SetValFutOspfv3IfCryptoAuthType,
                             FsMIOspfv3IfCryptoAuthType, 'i',
                             (sizeof (FsMIOspfv3IfCryptoAuthType) /
                              sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                             SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3IfCryptoAuthType\n");

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                setValFutOspfv3IfCryptoAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3IfCryptoAuthMode (INT4 i4FutOspfv3IfIndex,
                                 INT4 i4SetValFutOspfv3IfCryptoAuthMode)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = OSPFV3_ZERO;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER: nmhSetFutOspfv3IfCryptoAuthMode\n");

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }
    pInterface->u1AuthMode = (UINT1) i4SetValFutOspfv3IfCryptoAuthMode;

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfIfTable (i4FutOspfv3IfIndex,
                             i4SetValFutOspfv3IfCryptoAuthMode,
                             FsMIOspfv3IfCryptoAuthMode, 'i',
                             (sizeof (FsMIOspfv3IfCryptoAuthMode) /
                              sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum,
                             SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3IfCryptoAuthMode\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                setValFutOspfv3VirtIfCryptoAuthType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfCryptoAuthType (UINT4 u4Ospfv3VirtIfAreaId,
                                     UINT4 u4Ospfv3VirtIfNeighbor,
                                     INT4 i4SetValFutOspfv3VirtIfCryptoAuthType)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pScanAuthKeyNode = NULL;
    tAuthkeyInfo       *pTempAuthKeyNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = OSPFV3_ZERO;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3VirtIfCryptoAuthType\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) == NULL)
    {
        return SNMP_FAILURE;
    }
    pInterface->u2CryptoAuthType =
        (UINT2) i4SetValFutOspfv3VirtIfCryptoAuthType;
    if (pInterface->u2CryptoAuthType == OSPFV3_AUTH_NO_TYPE)
    {
        pInterface->u2AuthType = OSPFV3_NO_AUTH;
        pInterface->u1AuthMode = OSPFV3_AUTH_MODE_NONE;
    }
    else
    {
        pInterface->u2AuthType = OSPFV3_CRYPT_AUTH;
        if (pInterface->u1AuthMode == OSPFV3_AUTH_MODE_NONE)
        {
            pInterface->u1AuthMode = OSPFV3_AUTH_MODE_FULL;
        }
    }
    pInterface->pLastAuthkey = NULL;

    if (pInterface->u2CryptoAuthType == OSPFV3_AUTH_NO_TYPE)
    {
        OSPFV3_DYNM_SLL_SCAN (&(pInterface->sortAuthkeyLst), pScanAuthKeyNode,
                              pTempAuthKeyNode, tAuthkeyInfo *)
        {
            TMO_SLL_Delete (&(pInterface->sortAuthkeyLst),
                            &(pScanAuthKeyNode->nextSortKey));
            AUTH_FREE (pScanAuthKeyNode);
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfCryptoAuthTable ((INT4) pOspfCxt->u4ContextId,
                                           u4Ospfv3VirtIfAreaId,
                                           u4Ospfv3VirtIfNeighbor,
                                           i4SetValFutOspfv3VirtIfCryptoAuthType,
                                           FsMIOspfv3VirtIfCryptoAuthType,
                                           (sizeof
                                            (FsMIOspfv3VirtIfCryptoAuthType) /
                                            sizeof (UINT4)), OSPFV3_FALSE,
                                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfCryptoAuthType\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                setValFutOspfv3VirtIfCryptoAuthMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3VirtIfCryptoAuthMode (UINT4 u4Ospfv3VirtIfAreaId,
                                     UINT4 u4Ospfv3VirtIfNeighbor,
                                     INT4 i4SetValFutOspfv3VirtIfCryptoAuthMode)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsAreaId         areaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4SeqNum = OSPFV3_ZERO;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pOspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3VirtIfCryptoAuthMode\n");

    RM_GET_SEQ_NUM (&u4SeqNum);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &areaId, &nbrId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface->u1AuthMode = (UINT1) i4SetValFutOspfv3VirtIfCryptoAuthMode;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfVirtIfCryptoAuthTable ((INT4) pOspfCxt->u4ContextId,
                                           u4Ospfv3VirtIfAreaId,
                                           u4Ospfv3VirtIfNeighbor,
                                           i4SetValFutOspfv3VirtIfCryptoAuthMode,
                                           FsMIOspfv3VirtIfCryptoAuthMode,
                                           (sizeof
                                            (FsMIOspfv3VirtIfCryptoAuthMode) /
                                            sizeof (UINT4)), OSPFV3_FALSE,
                                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pOspfCxt->u4ContextId,
                "EXIT:nmhSetFutOspfv3VirtIfCryptoAuthMode\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                setValFutOspfv3AsExternalAggregationEffect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3AsExternalAggregationEffect (INT4
                                            i4FutOspfv3AsExternalAggregationNetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFutOspfv3AsExternalAggregationNet,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationPfxLength,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationAreaId,
                                            INT4
                                            i4SetValFutOspfv3AsExternalAggregationEffect)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3AsExternalAggregationNetType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3AsExternalAggregationEffect\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);
    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt,
                                            (tIp6Addr *) (VOID *)
                                            pFutOspfv3AsExternalAggregationNet->
                                            pu1_OctetList, (UINT1)
                                            u4FutOspfv3AsExternalAggregationPfxLength,
                                            areaId);
    if (pAsExtAddrRng == NULL)
    {
        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindAsExtRngInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pAsExtAddrRng->u1RangeEffect !=
        (UINT1) i4SetValFutOspfv3AsExternalAggregationEffect)
    {
        pAsExtAddrRng->u1RangeEffect =
            (UINT1) i4SetValFutOspfv3AsExternalAggregationEffect;

        switch ((UINT1) i4SetValFutOspfv3AsExternalAggregationEffect)
        {
            case OSPFV3_RAG_ADVERTISE:
                pAsExtAddrRng->u1AttrMask =
                    pAsExtAddrRng->u1AttrMask | OSPFV3_RAG_ADV_MASK;
                break;

            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                pAsExtAddrRng->u1AttrMask =
                    pAsExtAddrRng->u1AttrMask | OSPFV3_RAG_DONOTADV_MASK;
                break;

            case OSPFV3_RAG_ALLOW_ALL:
                pAsExtAddrRng->u1AttrMask =
                    pAsExtAddrRng->u1AttrMask | OSPFV3_RAG_ALLOW_MASK;
                break;

            case OSPFV3_RAG_DENY_ALL:
                pAsExtAddrRng->u1AttrMask =
                    pAsExtAddrRng->u1AttrMask | OSPFV3_RAG_DENY_MASK;
                break;

            default:
                break;
        }
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForAsExternalAggregationEffect ((INT4) pV3OspfCxt->u4ContextId,
                                             i4FutOspfv3AsExternalAggregationNetType,
                                             pFutOspfv3AsExternalAggregationNet,
                                             u4FutOspfv3AsExternalAggregationPfxLength,
                                             u4FutOspfv3AsExternalAggregationAreaId,
                                             i4SetValFutOspfv3AsExternalAggregationEffect,
                                             FsMIOspfv3AsExternalAggregationEffect,
                                             (sizeof
                                              (FsMIOspfv3AsExternalAggregationEffect)
                                              / sizeof (UINT4)), OSPFV3_FALSE,
                                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3AsExternalAggregationEffect\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                setValFutOspfv3AsExternalAggregationTranslation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3AsExternalAggregationTranslation (INT4
                                                 i4FutOspfv3AsExternalAggregationNetType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFutOspfv3AsExternalAggregationNet,
                                                 UINT4
                                                 u4FutOspfv3AsExternalAggregationPfxLength,
                                                 UINT4
                                                 u4FutOspfv3AsExternalAggregationAreaId,
                                                 INT4
                                                 i4SetValFutOspfv3AsExternalAggregationTranslation)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3AsExternalAggregationNetType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3AsExternalAggregationTranslation\n");
    i4SetValFutOspfv3AsExternalAggregationTranslation =
        OSPFV3_MAP_FROM_TRUTH_VALUE
        (i4SetValFutOspfv3AsExternalAggregationTranslation);

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);
    pAsExtAddrRng = V3GetFindAsExtRngInCxt (pV3OspfCxt,
                                            (tIp6Addr *) (VOID *)
                                            pFutOspfv3AsExternalAggregationNet->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3AsExternalAggregationPfxLength,
                                            areaId);
    if (pAsExtAddrRng == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get External Range for given interface\n"));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindAsExtRngInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pAsExtAddrRng->u1AggTranslation !=
        (UINT1) i4SetValFutOspfv3AsExternalAggregationTranslation)
    {
        pAsExtAddrRng->u1AggTranslation =
            (UINT1) i4SetValFutOspfv3AsExternalAggregationTranslation;

        pAsExtAddrRng->u1AttrMask = pAsExtAddrRng->u1AttrMask |
            OSPFV3_RAG_TRANS_MASK;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForAsExternalAggregationEffect ((INT4) pV3OspfCxt->u4ContextId,
                                             i4FutOspfv3AsExternalAggregationNetType,
                                             pFutOspfv3AsExternalAggregationNet,
                                             u4FutOspfv3AsExternalAggregationPfxLength,
                                             u4FutOspfv3AsExternalAggregationAreaId,
                                             i4SetValFutOspfv3AsExternalAggregationTranslation,
                                             FsMIOspfv3AsExternalAggregationTranslation,
                                             (sizeof
                                              (FsMIOspfv3AsExternalAggregationTranslation)
                                              / sizeof (UINT4)), OSPFV3_FALSE,
                                             u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3AsExternalAggregationTranslation\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                setValFutOspfv3AsExternalAggregationStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3AsExternalAggregationStatus (INT4
                                            i4FutOspfv3AsExternalAggregationNetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFutOspfv3AsExternalAggregationNet,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationPfxLength,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationAreaId,
                                            INT4
                                            i4SetValFutOspfv3AsExternalAggregationStatus)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3AsExternalAggregationNetType);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3AsExternalAggregationStatus\n");

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    switch ((UINT1) i4SetValFutOspfv3AsExternalAggregationStatus)
    {
        case CREATE_AND_GO:
            pAsExtAddrRng =
                V3AreaAsExtAddDefaultValuesInCxt (pV3OspfCxt, (tIp6Addr *)
                                                  (VOID *)
                                                  pFutOspfv3AsExternalAggregationNet->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3AsExternalAggregationPfxLength,
                                                  u4FutOspfv3AsExternalAggregationAreaId);
            if (pAsExtAddrRng == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create AS External Aggregation "));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " AS Ext AGG Creation Failure\n");
                return SNMP_FAILURE;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);

            pAsExtAddrRng->rangeStatus = ACTIVE;
            V3AreaAddExtAddrRangeInCxt (pV3OspfCxt, pAsExtAddrRng);
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3AsExternalAggregationStatus\n");
            break;

        case CREATE_AND_WAIT:

            pAsExtAddrRng =
                V3AreaAsExtAddDefaultValuesInCxt (pV3OspfCxt, (tIp6Addr *)
                                                  (VOID *)
                                                  pFutOspfv3AsExternalAggregationNet->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3AsExternalAggregationPfxLength,
                                                  u4FutOspfv3AsExternalAggregationAreaId);
            if (pAsExtAddrRng == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to create AS External Aggregation "));

                OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            " AS Ext AGG Creation Failure\n");
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pAsExtAddrRng->rangeStatus = NOT_READY;
            V3AreaAddExtAddrRangeInCxt (pV3OspfCxt, pAsExtAddrRng);
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3AsExternalAggregationStatus\n");
            break;

        case NOT_IN_SERVICE:

            pAsExtAddrRng =
                V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                        pFutOspfv3AsExternalAggregationNet->
                                        pu1_OctetList,
                                        (UINT1)
                                        u4FutOspfv3AsExternalAggregationPfxLength,
                                        areaId);
            if (pAsExtAddrRng == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get AS external range for given interface "));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindAsExtRngInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }

            if (pAsExtAddrRng->rangeStatus == NOT_IN_SERVICE)
            {
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : "
                            "nmhSetFutOspfv3AsExternalAggregationStatus\n");
                return SNMP_SUCCESS;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pAsExtAddrRng->rangeStatus = NOT_IN_SERVICE;
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3AsExternalAggregationStatus\n");
            break;

        case ACTIVE:

            pAsExtAddrRng =
                V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                        pFutOspfv3AsExternalAggregationNet->
                                        pu1_OctetList,
                                        (UINT1)
                                        u4FutOspfv3AsExternalAggregationPfxLength,
                                        areaId);
            if (pAsExtAddrRng == NULL)
            {

                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get AS external range for given interface "));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindAsExtRngInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }

            if (pAsExtAddrRng->rangeStatus == ACTIVE)
            {
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : "
                            "nmhSetFutOspfv3AsExternalAggregationStatus\n");
                return SNMP_SUCCESS;
            }

            RM_GET_SEQ_NUM (&u4SeqNum);

            if (pAsExtAddrRng->rangeStatus == NOT_IN_SERVICE)
            {
                pAsExtAddrRng->rangeStatus = ACTIVE;
                V3AreaExtAddrRangeAttrChngInCxt (pV3OspfCxt, pAsExtAddrRng);
            }
            else
            {
                pAsExtAddrRng->rangeStatus = ACTIVE;
                V3AreaAddExtAddrRangeInCxt (pV3OspfCxt, pAsExtAddrRng);
            }

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3AsExternalAggregationStatus\n");
            break;

        case DESTROY:

            pAsExtAddrRng =
                V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                        pFutOspfv3AsExternalAggregationNet->
                                        pu1_OctetList,
                                        (UINT1)
                                        u4FutOspfv3AsExternalAggregationPfxLength,
                                        areaId);

            if (pAsExtAddrRng != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                V3AreaDelExtAddrRangeInCxt (pV3OspfCxt, pAsExtAddrRng,
                                            OSIX_TRUE);
            }

            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : nmhSetFutOspfv3AsExternalAggregationStatus\n");
            break;
        default:
            return SNMP_FAILURE;
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForAsExternalAggregationEffect ((INT4) pV3OspfCxt->u4ContextId,
                                             i4FutOspfv3AsExternalAggregationNetType,
                                             pFutOspfv3AsExternalAggregationNet,
                                             u4FutOspfv3AsExternalAggregationPfxLength,
                                             u4FutOspfv3AsExternalAggregationAreaId,
                                             i4SetValFutOspfv3AsExternalAggregationStatus,
                                             FsMIOspfv3AsExternalAggregationStatus,
                                             (sizeof
                                              (FsMIOspfv3AsExternalAggregationStatus)
                                              / sizeof (UINT4)), OSPFV3_TRUE,
                                             u4SeqNum, SNMP_SUCCESS);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RedistRouteMetric
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                setValFutOspfv3RedistRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RedistRouteMetric (INT4 i4FutOspfv3RedistRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3RedistRouteDest,
                                  UINT4 u4FutOspfv3RedistRoutePfxLength,
                                  INT4 i4SetValFutOspfv3RedistRouteMetric)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3RedistRouteMetric\n");

    UNUSED_PARAM (i4FutOspfv3RedistRouteDestType);

    pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *)
                                                  (VOID *)
                                                  pFutOspfv3RedistRouteDest->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3RedistRoutePfxLength);
    if (pRrdConfRtInfo == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get redistributed route configuration information \n "));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pRrdConfRtInfo->u4RrdRouteMetricValue !=
        (UINT4) i4SetValFutOspfv3RedistRouteMetric)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pRrdConfRtInfo->u4RrdRouteMetricValue =
            (UINT4) i4SetValFutOspfv3RedistRouteMetric;

        if (pRrdConfRtInfo->rrdConfigRecStatus == ACTIVE)
        {
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
        }

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfRedistRouteCfgTable
            ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3RedistRouteDestType,
             pFutOspfv3RedistRouteDest, u4FutOspfv3RedistRoutePfxLength,
             i4SetValFutOspfv3RedistRouteMetric,
             FsMIOspfv3RedistRouteMetric,
             (sizeof (FsMIOspfv3RedistRouteMetric) /
              sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3RedistRouteMetric\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RedistRouteMetricType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                setValFutOspfv3RedistRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RedistRouteMetricType (INT4 i4FutOspfv3RedistRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3RedistRouteDest,
                                      UINT4 u4FutOspfv3RedistRoutePfxLength,
                                      INT4
                                      i4SetValFutOspfv3RedistRouteMetricType)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3RedistRouteMetricType \n");

    UNUSED_PARAM (i4FutOspfv3RedistRouteDestType);

    pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *)
                                                  (VOID *)
                                                  pFutOspfv3RedistRouteDest->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3RedistRoutePfxLength);
    if (pRrdConfRtInfo == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get redistributed route configuration information \n "));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }

    if (pRrdConfRtInfo->u1RrdRouteMetricType !=
        (UINT1) i4SetValFutOspfv3RedistRouteMetricType)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pRrdConfRtInfo->u1RrdRouteMetricType =
            (UINT1) i4SetValFutOspfv3RedistRouteMetricType;
        if (pRrdConfRtInfo->rrdConfigRecStatus == ACTIVE)
        {
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
        }

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfRedistRouteCfgTable
            ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3RedistRouteDestType,
             pFutOspfv3RedistRouteDest, u4FutOspfv3RedistRoutePfxLength,
             i4SetValFutOspfv3RedistRouteMetricType,
             FsMIOspfv3RedistRouteMetricType,
             (sizeof
              (FsMIOspfv3RedistRouteMetricType) /
              sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3RedistRouteMetricType\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RedistRouteTagType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                setValFutOspfv3RedistRouteTagType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RedistRouteTagType (INT4 i4FutOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3RedistRouteDest,
                                   UINT4 u4FutOspfv3RedistRoutePfxLength,
                                   INT4 i4SetValFutOspfv3RedistRouteTagType)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3RedistRouteTagType \n");

    UNUSED_PARAM (i4FutOspfv3RedistRouteDestType);

    pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *)
                                                  (VOID *)
                                                  pFutOspfv3RedistRouteDest->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3RedistRoutePfxLength);
    if (pRrdConfRtInfo == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get redistributed route configuration information \n "));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (pRrdConfRtInfo->u1RedistrTagType !=
        (UINT1) i4SetValFutOspfv3RedistRouteTagType)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pRrdConfRtInfo->u1RedistrTagType =
            (UINT1) i4SetValFutOspfv3RedistRouteTagType;
        if (pRrdConfRtInfo->rrdConfigRecStatus == ACTIVE)
        {
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
        }

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfRedistRouteCfgTable
            ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3RedistRouteDestType,
             pFutOspfv3RedistRouteDest, u4FutOspfv3RedistRoutePfxLength,
             i4SetValFutOspfv3RedistRouteTagType,
             FsMIOspfv3RedistRouteTagType,
             (sizeof (FsMIOspfv3RedistRouteTagType)
              / sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RedistRouteTagType \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RedistRouteTag
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                setValFutOspfv3RedistRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RedistRouteTag (INT4 i4FutOspfv3RedistRouteDestType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3RedistRouteDest,
                               UINT4 u4FutOspfv3RedistRoutePfxLength,
                               INT4 i4SetValFutOspfv3RedistRouteTag)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3RedistRouteTag\n");

    UNUSED_PARAM (i4FutOspfv3RedistRouteDestType);

    pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt,
                                                  (tIp6Addr *) (VOID *)
                                                  pFutOspfv3RedistRouteDest->
                                                  pu1_OctetList,
                                                  (UINT1)
                                                  u4FutOspfv3RedistRoutePfxLength);
    if (pRrdConfRtInfo == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get redistributed route configuration information \n "));

        OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                    "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
        return (SNMP_FAILURE);
    }
    if (pRrdConfRtInfo->u4ManualTagValue !=
        (UINT4) i4SetValFutOspfv3RedistRouteTag)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pRrdConfRtInfo->u4ManualTagValue =
            (UINT4) i4SetValFutOspfv3RedistRouteTag;
        if (pRrdConfRtInfo->rrdConfigRecStatus == ACTIVE)
        {
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
        }

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfRedistRouteCfgTable
            (pV3OspfCxt->u4ContextId, i4FutOspfv3RedistRouteDestType,
             pFutOspfv3RedistRouteDest, u4FutOspfv3RedistRoutePfxLength,
             i4SetValFutOspfv3RedistRouteTag, FsMIOspfv3RedistRouteTag,
             (sizeof (FsMIOspfv3RedistRouteTag) /
              sizeof (UINT4)), OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RedistRouteTag\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RedistRouteStatus
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                setValFutOspfv3RedistRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RedistRouteStatus (INT4 i4FutOspfv3RedistRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3RedistRouteDest,
                                  UINT4 u4FutOspfv3RedistRoutePfxLength,
                                  INT4 i4SetValFutOspfv3RedistRouteStatus)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3RedistRouteStatus\n");

    UNUSED_PARAM (i4FutOspfv3RedistRouteDestType);

    switch ((UINT1) i4SetValFutOspfv3RedistRouteStatus)
    {
        case CREATE_AND_GO:
            if ((pRrdConfRtInfo = V3RrdConfRtInfoCreateInCxt (pV3OspfCxt,
                                                              (tIp6Addr *)
                                                              (VOID *)
                                                              pFutOspfv3RedistRouteDest->
                                                              pu1_OctetList,
                                                              (UINT1)
                                                              u4FutOspfv3RedistRoutePfxLength))
                == NULL)
            {
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pRrdConfRtInfo->rrdConfigRecStatus = ACTIVE;
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
            break;

        case CREATE_AND_WAIT:
            if ((pRrdConfRtInfo =
                 V3RrdConfRtInfoCreateInCxt (pV3OspfCxt,
                                             (tIp6Addr *) (VOID *)
                                             pFutOspfv3RedistRouteDest->
                                             pu1_OctetList,
                                             (UINT1)
                                             u4FutOspfv3RedistRoutePfxLength))
                == NULL)
            {
                return SNMP_FAILURE;
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pRrdConfRtInfo->rrdConfigRecStatus = NOT_IN_SERVICE;
            break;

        case ACTIVE:
            pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt,
                                                          (tIp6Addr *) (VOID *)
                                                          pFutOspfv3RedistRouteDest->
                                                          pu1_OctetList,
                                                          (UINT1)
                                                          u4FutOspfv3RedistRoutePfxLength);
            if (pRrdConfRtInfo == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get redistributed route configuration information \n "));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            if (pRrdConfRtInfo->rrdConfigRecStatus == ACTIVE)
            {
#ifdef SNMP_3_WANTED
                V3UtilMsrForOspfRedistRouteCfgTable (pV3OspfCxt->u4ContextId,
                                                     i4FutOspfv3RedistRouteDestType,
                                                     pFutOspfv3RedistRouteDest,
                                                     u4FutOspfv3RedistRoutePfxLength,
                                                     i4SetValFutOspfv3RedistRouteStatus,
                                                     FsMIOspfv3RedistRouteStatus,
                                                     (sizeof
                                                      (FsMIOspfv3RedistRouteStatus)
                                                      / sizeof (UINT4)),
                                                     OSPFV3_TRUE,
                                                     u4SeqNum, SNMP_SUCCESS);
#endif
                return SNMP_SUCCESS;
            }
            pRrdConfRtInfo->rrdConfigRecStatus = ACTIVE;
            V3RtmSetRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
            break;

        case NOT_IN_SERVICE:
            pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt,
                                                          (tIp6Addr *) (VOID *)
                                                          pFutOspfv3RedistRouteDest->
                                                          pu1_OctetList,
                                                          (UINT1)
                                                          u4FutOspfv3RedistRoutePfxLength);
            if (pRrdConfRtInfo == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to get redistributed route configuration information \n "));

                OSPFV3_TRC (OSPFV3_CONFIGURATION_TRC, pV3OspfCxt->u4ContextId,
                            "V3GetFindRrdConfRtInfoInCxt failed for given if \n");
                return (SNMP_FAILURE);
            }
            RM_GET_SEQ_NUM (&u4SeqNum);

            pRrdConfRtInfo->rrdConfigRecStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            pRrdConfRtInfo = V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt,
                                                          (tIp6Addr *) (VOID *)
                                                          pFutOspfv3RedistRouteDest->
                                                          pu1_OctetList,
                                                          (UINT1)
                                                          u4FutOspfv3RedistRoutePfxLength);
            if (pRrdConfRtInfo != NULL)
            {
                RM_GET_SEQ_NUM (&u4SeqNum);

                V3RtmDelRRDConfigRecordInCxt (pV3OspfCxt, pRrdConfRtInfo);
            }
            break;
    }                            /* switch */
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfRedistRouteCfgTable (pV3OspfCxt->u4ContextId,
                                         i4FutOspfv3RedistRouteDestType,
                                         pFutOspfv3RedistRouteDest,
                                         u4FutOspfv3RedistRoutePfxLength,
                                         i4SetValFutOspfv3RedistRouteStatus,
                                         FsMIOspfv3RedistRouteStatus,
                                         (sizeof (FsMIOspfv3RedistRouteStatus) /
                                          sizeof (UINT4)), OSPFV3_TRUE,
                                         u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RedistRouteStatus\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RRDStatus
 Input       :  The Indices

                The Object 
                setValFutOspfv3RRDStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RRDStatus (INT4 i4SetValFutOspfv3RRDStatus)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3RRDStatus\n");

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFutOspfv3RRDStatus == ((INT4) pV3OspfCxt->redistrAdmnStatus))
    {
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3RRDStatus, NULL,
                               FsMIOspfv3RRDStatus,
                               (sizeof (FsMIOspfv3RRDStatus) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
        return SNMP_SUCCESS;
    }

    if (i4SetValFutOspfv3RRDStatus == OSPFV3_ENABLED)
    {
        pV3OspfCxt->redistrAdmnStatus = OSPFV3_ENABLED;

        if (pV3OspfCxt->u4RrdSrcProtoBitMask != 0)
        {
            /* EnQ redis enable message to RTM6 */
            if (V3RtmTxRedistributeMsgInCxt (pV3OspfCxt,
                                             pV3OspfCxt->u4RrdSrcProtoBitMask,
                                             RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                                             pV3OspfCxt->au1RMapName) !=
                OSIX_SUCCESS)
            {
                V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                       i4SetValFutOspfv3RRDStatus, NULL,
                                       FsMIOspfv3RRDStatus,
                                       (sizeof (FsMIOspfv3RRDStatus) /
                                        sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                       u4SeqNum, SNMP_FAILURE);

                return SNMP_FAILURE;
            }
        }
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3RRDStatus, NULL,
                               FsMIOspfv3RRDStatus,
                               (sizeof (FsMIOspfv3RRDStatus) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
        return SNMP_SUCCESS;
    }

    if (pV3OspfCxt->u4RrdSrcProtoBitMask != 0)
    {
        if (V3RtmRRDSrcProtoDisableInCxt (pV3OspfCxt,
                                          pV3OspfCxt->u4RrdSrcProtoBitMask)
            != OSIX_SUCCESS)
        {
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValFutOspfv3RRDStatus, NULL,
                                   FsMIOspfv3RRDStatus,
                                   (sizeof (FsMIOspfv3RRDStatus) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_FAILURE);

            return SNMP_FAILURE;
        }
    }
    pV3OspfCxt->redistrAdmnStatus = OSPFV3_DISABLED;

    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RRDStatus, NULL,
                           FsMIOspfv3RRDStatus,
                           (sizeof (FsMIOspfv3RRDStatus) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RRDStatus\n");
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFutOspfv3RRDStatus);
    return SNMP_FAILURE;
#endif /* RRD_WANTED */
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RRDSrcProtoMask
 Input       :  The Indices

                The Object 
                setValFutOspfv3RRDSrcProtoMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RRDSrcProtoMask (INT4 i4SetValFutOspfv3RRDSrcProtoMask)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    UINT4               u4NewSrcProtoMaskEnable = 0;
    UINT4               u4NewSrcProtoMaskDisable = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3RRDSrcProtoMask\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    /*
     * Transition in bit value from 0->1 means that redistribution from the 
     * corresponding protocol is to be enabled
     */

    if (pV3OspfCxt->u4RrdSrcProtoBitMask ==
        (UINT4) i4SetValFutOspfv3RRDSrcProtoMask)
    {
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3RRDSrcProtoMask, NULL,
                               FsMIOspfv3RRDSrcProtoMask,
                               (sizeof (FsMIOspfv3RRDSrcProtoMask) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);

        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT :nmhSetFutOspfv3RRDSrcProtoMask\n");
        return SNMP_SUCCESS;
    }

    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_DIRECT_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_DIRECT_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_DIRECT_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_DIRECT_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_DIRECT_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_DIRECT_MASK;
    }

    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_STATIC_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_STATIC_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_STATIC_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_STATIC_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_STATIC_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_STATIC_MASK;
    }

    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_RIP_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_RIP_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_RIP_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_RIP_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_RIP_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_RIP_MASK;
    }

    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_BGP_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_BGP_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_BGP_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_BGP_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_BGP_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_BGP_MASK;
    }
    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_ISISL1_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_ISISL1_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_ISISL1_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_ISISL1_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_ISISL1_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_ISISL1_MASK;
    }
    if ((!(pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_ISISL2_MASK)) &&
        (i4SetValFutOspfv3RRDSrcProtoMask & RTM6_ISISL2_MASK))
    {
        u4NewSrcProtoMaskEnable |= RTM6_ISISL2_MASK;
    }
    else if ((pV3OspfCxt->u4RrdSrcProtoBitMask & RTM6_ISISL2_MASK) &&
             (!(i4SetValFutOspfv3RRDSrcProtoMask & RTM6_ISISL2_MASK)))
    {
        u4NewSrcProtoMaskDisable |= RTM6_ISISL2_MASK;
    }

    pV3OspfCxt->u4RrdSrcProtoBitMask = i4SetValFutOspfv3RRDSrcProtoMask;

    if (u4NewSrcProtoMaskEnable != 0)
    {
        /* EnQ redis enable message to RTM6 */
        if (V3RtmTxRedistributeMsgInCxt (pV3OspfCxt, u4NewSrcProtoMaskEnable,
                                         RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                                         pV3OspfCxt->au1RMapName) !=
            OSIX_SUCCESS)
        {
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValFutOspfv3RRDSrcProtoMask, NULL,
                                   FsMIOspfv3RRDSrcProtoMask,
                                   (sizeof (FsMIOspfv3RRDSrcProtoMask) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_FAILURE);

            return SNMP_FAILURE;
        }
    }

    if (u4NewSrcProtoMaskDisable != 0)
    {
        if (V3RtmRRDSrcProtoDisableInCxt (pV3OspfCxt,
                                          u4NewSrcProtoMaskDisable) !=
            OSIX_SUCCESS)
        {
            V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                                   i4SetValFutOspfv3RRDSrcProtoMask, NULL,
                                   FsMIOspfv3RRDSrcProtoMask,
                                   (sizeof (FsMIOspfv3RRDSrcProtoMask) /
                                    sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                   u4SeqNum, SNMP_FAILURE);

            return SNMP_FAILURE;
        }
    }

    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RRDSrcProtoMask, NULL,
                           FsMIOspfv3RRDSrcProtoMask,
                           (sizeof (FsMIOspfv3RRDSrcProtoMask) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RRDSrcProtoMask\n");
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFutOspfv3RRDSrcProtoMask);
    return SNMP_FAILURE;
#endif /* RRD_WANTED */
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object
                setValFutOspfv3RTStaggeringStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RTStaggeringStatus (INT4 i4SetValFutOspfv3RTStaggeringStatus)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : nmhSetFutOspfv3RTStaggeringStatus\n");

    if (gV3OsRtr.u4RTStaggeringStatus !=
        (UINT4) i4SetValFutOspfv3RTStaggeringStatus)
    {
        gV3OsRtr.u4RTStaggeringStatus =
            (UINT4) i4SetValFutOspfv3RTStaggeringStatus;
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForScalarObj (i4SetValFutOspfv3RTStaggeringStatus,
                           FsMIOspfv3RTStaggeringStatus,
                           (sizeof (FsMIOspfv3RTStaggeringStatus) /
                            sizeof (UINT4)), OSPFV3_FALSE);
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT : nmhSetFutOspfv3RTStaggeringStatus\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RTStaggeringInterval
 Input       :  The Indices

                The Object
                setValFutOspfv3RTStaggeringInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RTStaggeringInterval (INT4 i4SetValFutOspfv3RTStaggeringInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : nmhSetFutOspfv3RTStaggeringInterval\n");

    /* Convert milliseconds to seconds,
     * because u4SetValFutOspfRTStaggeringInterval in milisec
     */
    if (pV3OspfCxt->u4RTStaggeringInterval !=
        (UINT4) (i4SetValFutOspfv3RTStaggeringInterval))
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pV3OspfCxt->u4RTStaggeringInterval =
            (UINT4) (i4SetValFutOspfv3RTStaggeringInterval);

#ifdef SNMP_3_WANTED
        V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                               i4SetValFutOspfv3RTStaggeringInterval, NULL,
                               FsMIOspfv3RTStaggeringInterval,
                               (sizeof (FsMIOspfv3RTStaggeringInterval) /
                                sizeof (UINT4)), 'i', OSPFV3_FALSE,
                               u4SeqNum, SNMP_SUCCESS);
#endif
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : nmhSetFutOspfv3RTStaggeringInterval\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RestartStrictLsaChecking
 Input       :  The Indices

                The Object 
                setValFutOspfv3RestartStrictLsaChecking
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3RestartStrictLsaChecking
    (INT4 i4SetValFutOspfv3RestartStrictLsaChecking)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetFutOspfv3RestartStrictLsaChecking "
                 "Ospfv3RestartStrictLsaChecking %d\n",
                 i4SetValFutOspfv3RestartStrictLsaChecking);

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1StrictLsaCheck =
        (UINT1) i4SetValFutOspfv3RestartStrictLsaChecking;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RestartStrictLsaChecking, NULL,
                           FsMIOspfv3RestartStrictLsaChecking,
                           (sizeof (FsMIOspfv3RestartStrictLsaChecking) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RestartStrictLsaChecking\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3HelperSupport
 Input       :  The Indices

                The Object 
                setValFutOspfv3HelperSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3HelperSupport
    (tSNMP_OCTET_STRING_TYPE * pSetValFutOspfv3HelperSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC1 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "ENTER :nmhSetFutOspfv3HelperSupport "
                 "Ospfv3HelperSupport %d\n",
                 ((UINT1) *pSetValFutOspfv3HelperSupport->pu1_OctetList));

    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1HelperSupport =
        ((UINT1) *pSetValFutOspfv3HelperSupport->pu1_OctetList);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           OSPFV3_FALSE, pSetValFutOspfv3HelperSupport,
                           FsMIOspfv3HelperSupport,
                           (sizeof (FsMIOspfv3HelperSupport) / sizeof (UINT4)),
                           'i', OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3HelperSupport\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3HelperGraceTimeLimit
 Input       :  The Indices

                The Object 
                setValFutOspfv3HelperGraceTimeLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3HelperGraceTimeLimit
    (INT4 i4SetValFutOspfv3HelperGraceTimeLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3HelperGraceTimeLimit\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4HelperGrTimeLimit =
        (UINT4) i4SetValFutOspfv3HelperGraceTimeLimit;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3HelperGraceTimeLimit, NULL,
                           FsMIOspfv3HelperGraceTimeLimit,
                           (sizeof (FsMIOspfv3HelperGraceTimeLimit) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3HelperGraceTimeLimit\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RestartAckState
 Input       :  The Indices

                The Object 
                setValFutOspfv3RestartAckState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3RestartAckState
    (INT4 i4SetValFutOspfv3RestartAckState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3RestartAckState\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u4IsGraceAckReq = (UINT4) i4SetValFutOspfv3RestartAckState;

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RestartAckState, NULL,
                           FsMIOspfv3RestartAckState,
                           (sizeof (FsMIOspfv3RestartAckState) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RestartAckState\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3GraceLsaRetransmitCount
 Input       :  The Indices

                The Object 
                setValFutOspfv3GraceLsaRetransmitCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3GraceLsaRetransmitCount
    (INT4 i4SetValFutOspfv3GraceLsaRetransmitCount)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3GraceLsaRetransmitCount\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1GrLsaMaxTxCount =
        (UINT1) i4SetValFutOspfv3GraceLsaRetransmitCount;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3GraceLsaRetransmitCount, NULL,
                           FsMIOspfv3GraceLsaRetransmitCount,
                           (sizeof (FsMIOspfv3GraceLsaRetransmitCount) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3GraceLsaRetransmitCount\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RestartReason
 Input       :  The Indices

                The Object 
                setValFutOspfv3RestartReason
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3RestartReason
    (INT4 i4SetValFutOspfv3RestartReason)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3RestartReason\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1RestartReason = (UINT1) i4SetValFutOspfv3RestartReason;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RestartReason, NULL,
                           FsMIOspfv3RestartReason,
                           (sizeof (FsMIOspfv3RestartReason) / sizeof (UINT4)),
                           'i', OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RestartReason\n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3ExtTraceLevel
 Input       :  The Indices

                The Object 
                setValFutOspfv3ExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhSetFutOspfv3ExtTraceLevel
    (INT4 i4SetValFutOspfv3ExtTraceLevel)
{
#ifdef TRACE_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3ExtTraceLevel\n");

    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFutOspfv3ExtTraceLevel != 0)
    {
        pV3OspfCxt->u4ExtTraceValue = (UINT4) i4SetValFutOspfv3ExtTraceLevel;
    }
    else
    {
        pV3OspfCxt->u4ExtTraceValue = 0;
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3ExtTraceLevel, NULL,
                           FsMIOspfv3ExtTraceLevel,
                           (sizeof (FsMIOspfv3ExtTraceLevel) / sizeof (UINT4)),
                           'i', OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3ExtTraceLevel\n");
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (i4SetValFutOspfv3ExtTraceLevel);
    return SNMP_FAILURE;
#endif
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3SetTraps
 Input       :  The Indices

                The Object 
                setValFutOspfv3SetTraps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3SetTraps (INT4 i4SetValFutOspfv3SetTraps)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3SetTraps\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (i4SetValFutOspfv3SetTraps != 0)
    {
        pV3OspfCxt->u4TrapControl = (pV3OspfCxt->u4TrapControl |
                                     (1 << (i4SetValFutOspfv3SetTraps - 1)));
    }
    else
    {
        pV3OspfCxt->u4TrapControl = 0;
    }
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3SetTraps, NULL,
                           FsMIOspfv3SetTraps,
                           (sizeof (FsMIOspfv3SetTraps) / sizeof (UINT4)),
                           'i', OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#endif
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3SetTraps\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3BfdStatus
 Input       :  The Indices

                The Object
                setValFutOspfv3BfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3BfdStatus (INT4 i4SetValFutOspfv3BfdStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tV3OsNeighbor      *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3BfdStatus\n");
    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3BfdStatus, NULL,
                           FsMIOspfv3BfdStatus,
                           (sizeof (FsMIOspfv3BfdStatus) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    pV3OspfCxt->u1BfdAdminStatus = (UINT1) i4SetValFutOspfv3BfdStatus;

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);

    if (pInterface != NULL)
    {
        do
        {
            if (pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_DISABLED)
            {
                pInterface->u1BfdIfStatus = OSPFV3_BFD_DISABLED;
            }

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if (pNbr->u1NbrBfdState == OSPFV3_BFD_ENABLED &&
                    pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_DISABLED)
                {
                    pInterface->u1BfdIfStatus = OSPFV3_BFD_DISABLED;
                    pV3OspfCxt->u1BfdStatusInAllIf = OSPFV3_BFD_DISABLED;
                    IfOspfv3BfdDeRegister (pNbr, OSIX_FALSE);
                }
                if (pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_ENABLED)
                {
                    if ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
                        (pNbr->pInterface->u1NetworkType ==
                         OSPFV3_IF_BROADCAST))
                    {
                        if ((pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                            (pNbr->pInterface->u1IsmState == OSPFV3_IFS_BACKUP)
                            || (pNbr->pInterface->u1IsmState ==
                                OSPFV3_IFS_DR_OTHER))
                        {
                            if ((((pNbr->pInterface->u1IsmState ==
                                   OSPFV3_IFS_DR)
                                  || (pNbr->pInterface->u1IsmState ==
                                      OSPFV3_IFS_BACKUP))
                                 && (pNbr->u1NsmState == OSPFV3_NBRS_FULL))
                                ||
                                (((pNbr->pInterface->u1IsmState !=
                                   OSPFV3_IFS_DR)
                                  && (pNbr->pInterface->u1IsmState !=
                                      OSPFV3_IFS_BACKUP))
                                 && (pNbr->u1NsmState == OSPFV3_NBRS_2WAY)))
                            {
                                if (OSIX_SUCCESS !=
                                    (IfOspfv3BfdRegister (pNbr)))
                                {
                                    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                                }
                            }
                        }
                    }
                }
            }
        }
        while (((pInterface =
                 (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                   (tRBElem *) pInterface,
                                                   NULL)) != NULL));
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3BfdStatus\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3BfdAllIfState
 Input       :  The Indices
                The Object
                setValFutOspfv3BfdAllIfState
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3BfdAllIfState (INT4 i4SetValFutOspfv3BfdAllIfState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tV3OsInterface     *pInterface;
    tV3OsNeighbor      *pNbr;
    tTMO_SLL_NODE      *pNbrNode;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3BfdAllIfState\n");
    /* Checking for Global BFD Admin status */
    if (pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_DISABLED)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable ((INT4) pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3BfdAllIfState, NULL,
                           FsMIOspfv3BfdAllIfState,
                           (sizeof (FsMIOspfv3BfdAllIfState) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#else
    UNUSED_PARAM (u4SeqNum);
#endif

    pV3OspfCxt->u1BfdStatusInAllIf = (UINT1) i4SetValFutOspfv3BfdAllIfState;

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);

    if (pInterface != NULL)
    {
        do
        {
            if (pV3OspfCxt->u1BfdStatusInAllIf == OSPFV3_BFD_ENABLED)
            {
                pInterface->u1BfdIfStatus = OSPFV3_BFD_ENABLED;

                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNbr =
                        OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                             pNbrNode);

                    if ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
                        (pNbr->pInterface->u1NetworkType ==
                         OSPFV3_IF_BROADCAST))
                    {
                        if ((pNbr->pInterface->u1IsmState == OSPFV3_IFS_DR) ||
                            (pNbr->pInterface->u1IsmState == OSPFV3_IFS_BACKUP)
                            || (pNbr->pInterface->u1IsmState ==
                                OSPFV3_IFS_DR_OTHER))
                        {
                            if ((((pNbr->pInterface->u1IsmState ==
                                   OSPFV3_IFS_DR)
                                  || (pNbr->pInterface->u1IsmState ==
                                      OSPFV3_IFS_BACKUP))
                                 && (pNbr->u1NsmState == OSPFV3_NBRS_FULL))
                                ||
                                (((pNbr->pInterface->u1IsmState !=
                                   OSPFV3_IFS_DR)
                                  && (pNbr->pInterface->u1IsmState !=
                                      OSPFV3_IFS_BACKUP))
                                 && (pNbr->u1NsmState == OSPFV3_NBRS_2WAY)))
                            {
                                if (OSIX_SUCCESS != IfOspfv3BfdRegister (pNbr))
                                {
                                    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                pInterface->u1BfdIfStatus = OSPFV3_BFD_DISABLED;

                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNbr =
                        OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                             pNbrNode);

                    if (pNbr->u1NbrBfdState == OSPFV3_BFD_ENABLED)
                    {
                        IfOspfv3BfdDeRegister (pNbr, OSIX_FALSE);
                    }
                }
            }
        }
        while ((pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface,
                                                  NULL)) != NULL);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3BfdAllIfState\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RouterIdPermanence
 Input       :  The Indices

                The Object
                setValFutOspfv3RouterIdPermanence
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RouterIdPermanence (INT4 i4SetValFutOspfv3RouterIdPermanence)
{

    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    tV3OsRouterId       rtrId;

    MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (V3OspfRmGetNodeState () != RM_STANDBY)
    {
        if (V3UtilSelectOspfRouterId (pV3OspfCxt->u4ContextId,
                                      &rtrId) == OSPFV3_SUCCESS)
        {
            OSPFV3_IPV4_ADDR_COPY (&(pV3OspfCxt->rtrId), &(rtrId));
            if (O3RedDynSendRtrIdToStandby (rtrId, pV3OspfCxt->u4ContextId)
                == OSPFV3_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    pV3OspfCxt->u1RouterIdStatus = (UINT1) i4SetValFutOspfv3RouterIdPermanence;
#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable (pV3OspfCxt->u4ContextId,
                           i4SetValFutOspfv3RouterIdPermanence, NULL,
                           FsMIOspfv3RouterIdPermanence,
                           (sizeof (FsMIOspfv3RouterIdPermanence) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE, u4SeqNum,
                           SNMP_SUCCESS);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RRDRouteMapName
 Input       :  The Indices

                The Object
                setValFutOspfRRDRouteMapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RRDRouteMapName (tSNMP_OCTET_STRING_TYPE
                                * pSetValFutOspfv3RRDRouteMapEnable)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3RRDRouteMapName\n");
    if (pSetValFutOspfv3RRDRouteMapEnable->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->bAsBdrRtr != OSIX_TRUE)
    {
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->redistrAdmnStatus != OSPFV3_ENABLED)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFutOspfv3RRDRouteMapEnable->i4_Length != 0)
    {
        if (STRCMP (pV3OspfCxt->au1RMapName, "") == 0)
        {
            MEMCPY (pV3OspfCxt->au1RMapName,
                    pSetValFutOspfv3RRDRouteMapEnable->pu1_OctetList,
                    pSetValFutOspfv3RRDRouteMapEnable->i4_Length);
            pV3OspfCxt->au1RMapName[pSetValFutOspfv3RRDRouteMapEnable->
                                    i4_Length] = '\0';
        }
        else if (MEMCMP (pV3OspfCxt->au1RMapName,
                         pSetValFutOspfv3RRDRouteMapEnable->pu1_OctetList,
                         pSetValFutOspfv3RRDRouteMapEnable->i4_Length) != 0)
        {
            /* Check for existence of Route Map with different Name.
             * If So, return failure, */
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Resetting the Route Map Name Associated */
        MEMSET (pV3OspfCxt->au1RMapName, 0, RMAP_MAX_NAME_LEN);
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    /* EnQ redis enable message to RTM6 */
    V3RtmTxRedistributeMsgInCxt (pV3OspfCxt, pV3OspfCxt->u4RrdSrcProtoBitMask,
                                 RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                                 pV3OspfCxt->au1RMapName);

    V3UtilMsrForOspfTable ((INT4) pV3OspfCxt->u4ContextId,
                           OSPFV3_FALSE, pSetValFutOspfv3RRDRouteMapEnable,
                           FsMIOspfv3RRDRouteMapName,
                           (sizeof (FsMIOspfv3RRDRouteMapName) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3RRDRouteMapName\n");
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pSetValFutOspfv3RRDRouteMapEnable);
    return SNMP_FAILURE;
#endif
}

/* LOW LEVEL Routines for Table : FutOspfv3RRDMetricTable. */

/****************************************************************************
 Function    :  nmhSetFutOspfv3RRDMetricValue
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object 
                setValFutOspfv3RRDMetricValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RRDMetricValue (INT4 i4FutOspfv3RRDProtocolId,
                               INT4 i4SetValFutOspfv3RRDMetricValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4SeqNum = 0;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfv3RRDMetricValue;
    SnmpNotifyInfo.u4OidLen =
        sizeof (FsMIOspfv3RRDMetricValue) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
#endif
    pV3OspfCxt->au4MetricValue[i4FutOspfv3RRDProtocolId - 1] =
        (UINT4) i4SetValFutOspfv3RRDMetricValue;
#ifdef SNMP_3_WANTED
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", pV3OspfCxt->u4ContextId,
                      i4FutOspfv3RRDProtocolId,
                      i4SetValFutOspfv3RRDMetricValue));
#endif

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3RRDMetricType
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object 
                setValFutOspfv3RRDMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3RRDMetricType (INT4 i4FutOspfv3RRDProtocolId,
                              INT4 i4SetValFutOspfv3RRDMetricType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
#ifdef SNMP_3_WANTED
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
#endif
    UINT4               u4SeqNum = 0;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

#ifdef SNMP_3_WANTED
    SnmpNotifyInfo.pu4ObjectId = FsMIOspfv3RRDMetricType;
    SnmpNotifyInfo.u4OidLen = sizeof (FsMIOspfv3RRDMetricType) / sizeof (UINT4);
    SnmpNotifyInfo.u4SeqNum = u4SeqNum;
    SnmpNotifyInfo.u1RowStatus = FALSE;
    SnmpNotifyInfo.pLockPointer = V3OspfLock;
    SnmpNotifyInfo.pUnLockPointer = V3OspfUnLock;
    SnmpNotifyInfo.u4Indices = 2;
    SnmpNotifyInfo.i1ConfStatus = i1RetVal;
#endif

    pV3OspfCxt->au4MetricType[i4FutOspfv3RRDProtocolId - 1] =
        (UINT4) i4SetValFutOspfv3RRDMetricType;

#ifdef SNMP_3_WANTED
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", pV3OspfCxt->u4ContextId,
                      i4FutOspfv3RRDProtocolId,
                      i4SetValFutOspfv3RRDMetricType));
#endif

    return i1RetVal;

}

/* LOW LEVEL Routines for Table : FutOspfv3DistInOutRouteMapTable. */

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                setValFutOspfv3DistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3DistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3DistInOutRouteMapName,
                                       INT4 i4FutOspfv3DistInOutRouteMapType,
                                       INT4
                                       i4SetValFutOspfv3DistInOutRouteMapValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:nmhSetFutOspfv3DistInOutRouteMapValue\n");
    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        pV3OspfCxt->pDistanceFilterRMap->u1RMapDistance = (UINT1)
            i4SetValFutOspfv3DistInOutRouteMapValue;
        V3ApplyDistance (pV3OspfCxt);
    }

#ifdef SNMP_3_WANTED
    V3UtilMsrForDistInOutRouteMapTable ((INT4) pV3OspfCxt->u4ContextId,
                                        pFutOspfv3DistInOutRouteMapName,
                                        i4FutOspfv3DistInOutRouteMapType,
                                        i4SetValFutOspfv3DistInOutRouteMapValue,
                                        FsMIOspfv3DistInOutRouteMapValue,
                                        (sizeof
                                         (FsMIOspfv3DistInOutRouteMapValue) /
                                         sizeof (UINT4)), OSPFV3_FALSE);
#else
    UNUSED_PARAM (pFutOspfv3DistInOutRouteMapName);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3DistInOutRouteMapValue\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                setValFutOspfv3DistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3DistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                           pFutOspfv3DistInOutRouteMapName,
                                           INT4
                                           i4FutOspfv3DistInOutRouteMapType,
                                           INT4
                                           i4SetValFutOspfv3DistInOutRouteMapRowStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4ErrCode = 0;

#ifdef ROUTEMAP_WANTED
    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspfv3DistInOutRouteMapRowStatus\n");
    if (nmhTestv2FutOspfv3DistInOutRouteMapRowStatus (&u4ErrCode,
                                                      pFutOspfv3DistInOutRouteMapName,
                                                      i4FutOspfv3DistInOutRouteMapType,
                                                      i4SetValFutOspfv3DistInOutRouteMapRowStatus)
        == SNMP_SUCCESS)
    {
        switch (i4SetValFutOspfv3DistInOutRouteMapRowStatus)
        {
            case CREATE_AND_WAIT:
            {
                tFilteringRMap     *pFilter = NULL;
                UINT4               u4Status = FILTERNIG_STAT_DEFAULT;
                OSPFV3_RMAP_MSG_ALLOC (&pFilter);
                if (pFilter != NULL)
                {
                    MEMSET (pFilter, 0, sizeof (tFilteringRMap));
                    pFilter->u1RowStatus = (UINT1)
                        i4SetValFutOspfv3DistInOutRouteMapRowStatus;
                    if (pFutOspfv3DistInOutRouteMapName->i4_Length >
                        RMAP_MAX_NAME_LEN)
                    {
                        OSPFV3_RMAP_MSG_FREE (pFilter);
                        return SNMP_FAILURE;
                    }
                    MEMCPY (pFilter->au1DistInOutFilterRMapName,
                            pFutOspfv3DistInOutRouteMapName->pu1_OctetList,
                            MEM_MAX_BYTES (RMAP_MAX_NAME_LEN,
                                           pFutOspfv3DistInOutRouteMapName->
                                           i4_Length));

                    u4Status =
                        RMapGetInitialMapStatus
                        (pFutOspfv3DistInOutRouteMapName->pu1_OctetList);
                    if (u4Status != 0)
                    {
                        pFilter->u1Status = FILTERNIG_STAT_ENABLE;
                    }
                    else
                    {
                        pFilter->u1Status = FILTERNIG_STAT_DISABLE;
                    }

                    if (i4FutOspfv3DistInOutRouteMapType ==
                        FILTERING_TYPE_DISTANCE)
                    {
                        pV3OspfCxt->pDistanceFilterRMap = pFilter;
                    }
                    else if (i4FutOspfv3DistInOutRouteMapType ==
                             FILTERING_TYPE_DISTRIB_IN)
                    {
                        pV3OspfCxt->pDistributeInFilterRMap = pFilter;
                    }
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
                break;
            case DESTROY:
                if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    OSPFV3_RMAP_MSG_FREE (pV3OspfCxt->pDistanceFilterRMap);
                    pV3OspfCxt->pDistanceFilterRMap = NULL;
                }
                else if (i4FutOspfv3DistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    OSPFV3_RMAP_MSG_FREE (pV3OspfCxt->pDistributeInFilterRMap);
                    pV3OspfCxt->pDistributeInFilterRMap = NULL;
                }
                break;
            default:
                if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
                {
                    pV3OspfCxt->pDistanceFilterRMap->u1RowStatus = (UINT1)
                        i4SetValFutOspfv3DistInOutRouteMapRowStatus;
                }
                else if (i4FutOspfv3DistInOutRouteMapType ==
                         FILTERING_TYPE_DISTRIB_IN)
                {
                    pV3OspfCxt->pDistributeInFilterRMap->u1RowStatus = (UINT1)
                        i4SetValFutOspfv3DistInOutRouteMapRowStatus;
                }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
    V3UtilMsrForDistInOutRouteMapTable ((INT4) pV3OspfCxt->u4ContextId,
                                        pFutOspfv3DistInOutRouteMapName,
                                        i4FutOspfv3DistInOutRouteMapType,
                                        i4SetValFutOspfv3DistInOutRouteMapRowStatus,
                                        FsMIOspfv3DistInOutRouteMapRowStatus,
                                        (sizeof
                                         (FsMIOspfv3DistInOutRouteMapRowStatus)
                                         / sizeof (UINT4)), OSPFV3_TRUE);

#else
    UNUSED_PARAM (pFutOspfv3DistInOutRouteMapName);
    UNUSED_PARAM (i4FutOspfv3DistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFutOspfv3DistInOutRouteMapRowStatus);
    UNUSED_PARAM (u4ErrCode);
#endif /*ROUTEMAP_WANTED */

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspfv3DistInOutRouteMapRowStatus\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspf3PreferenceValue
 Input       :  The Indices
                The Object
                setValFutOspf3PreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspf3PreferenceValue (INT4 i4SetValFutOspf3PreferenceValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER :nmhSetFutOspf3PreferenceValue\n");
    if (i4SetValFutOspf3PreferenceValue == 0)
    {
        pV3OspfCxt->u1Distance = IP6_PREFERENCE_OSPF;
    }
    else
    {
        pV3OspfCxt->u1Distance = (UINT1) i4SetValFutOspf3PreferenceValue;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
    V3ApplyDistance (pV3OspfCxt);

#ifdef SNMP_3_WANTED
    V3UtilMsrForOspfTable ((INT4) pV3OspfCxt->u4ContextId,
                           i4SetValFutOspf3PreferenceValue, NULL,
                           FsMIOspfv3PreferenceValue,
                           (sizeof (FsMIOspfv3PreferenceValue) /
                            sizeof (UINT4)), 'i', OSPFV3_FALSE,
                           u4SeqNum, SNMP_SUCCESS);
#endif

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT :nmhSetFutOspf3PreferenceValue\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetOspfv3AreaDfInfOriginate
 Input       :  The Indices
                Ospfv3AreaId

                The Object
                retValOspfv3AreaDfInfOriginate
 Output      :  The Set Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetOspfv3AreaDfInfOriginate (UINT4 u4Ospfv3AreaId,
                                INT4 i4SetValOspfv3AreaDfInfOriginate)
{

    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC:nmhSetOspfv3AreaDfInfOriginate\n");
    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pArea->u4DfInfOriginate != (UINT4) i4SetValOspfv3AreaDfInfOriginate)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);

        pArea->u4DfInfOriginate = (UINT4) i4SetValOspfv3AreaDfInfOriginate;
        V3AreaParamChange (pArea);

#ifdef SNMP_3_WANTED
        V3UtilMsrForStdOspfAreaTable ((INT4) pV3OspfCxt->u4ContextId,
                                      u4Ospfv3AreaId,
                                      i4SetValOspfv3AreaDfInfOriginate,
                                      FsMIStdOspfv3AreaDfInfOriginate,
                                      (sizeof (FsMIStdOspfv3AreaDfInfOriginate)
                                       / sizeof (UINT4)), 'i', OSPFV3_FALSE,
                                      u4SeqNum, SNMP_SUCCESS);
#endif
    }
    if (i4SetValOspfv3AreaDfInfOriginate == OSPFV3_DEFAULT_INFO_ORIGINATE)
    {
        V3GenerateNssaDfLsa (pArea, OSPFV3_DEFAULT_NSSA_LSA,
                             &(OSPFV3_NULL_LSID));
    }
    else
    {
        pArea->u4DfInfOriginate = (UINT4) i4SetValOspfv3AreaDfInfOriginate;
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            pLsaInfo = V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                            &(OSPFV3_NULL_LSID),
                                            &(pArea->pV3OspfCxt->rtrId), NULL,
                                            pArea);
            if (pLsaInfo != NULL)
            {
                V3AgdFlushOut (pLsaInfo);
            }
            pArea->stubDefaultCost.u4MetricType = OSPFV3_METRIC;
        }

    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT:nmhSetOspfv3AreaDfInfOriginate\n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfv3ClearProcess
 Input       :  The Indices

                The Object
                setValFutOspfv3ClearProcess
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3ClearProcess (INT4 i4SetValFutOspfv3ClearProcess)
{

    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4ContextId = 0;

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC : nmhSetFutOspfv3ClearProcess\n");

    gu4ClearFlag = (UINT4) i4SetValFutOspfv3ClearProcess;
    if (i4SetValFutOspfv3ClearProcess == OSIX_TRUE)
    {

        /* post event to OSPF3 to handle clear process */
        if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_CLEAR_EVENT) != OSIX_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "Event send failed for OSPFV3_CLEAR_EVENT\r\n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Event send failed for OSPFV3_CLEAR_EVENT"));
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "FUNC : nmhSetFutOspfv3ClearProcess\n");

    return SNMP_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3conf.c                     */
/*-----------------------------------------------------------------------*/
