/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtc.c,v 1.23 2017/12/26 13:34:29 siva Exp $
 *
 * Description: This file contains procedures for adding deleting 
 *              and  modifying Routing Table Entry. 
 *
 *******************************************************************/
#include "o3inc.h"

PRIVATE INT1 V3RtcIsSameNextHop PROTO ((tV3OsPath * pDestRtPath,
                                        tV3OsPath * pAreaBdrPath,
                                        UINT1 u1HopIndex));
PRIVATE VOID V3RtcCheckTranslationInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID        V3RtcCheckAddrRangeFlagInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pAddrPrefix,
        tV3OsAreaId * pNewAreaId, tV3OsAreaId * pOldAreaId));
PRIVATE UINT1       V3RtcCheckAndGenAggrLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry, UINT1 u1Status));
PRIVATE VOID
    V3RtcFindUpdateAddrRangeCost PROTO ((tV3OsArea * pArea,
                                         tV3OsAddrRange * pAddrRange));

PRIVATE VOID V3RtcChkAndFlushFnEqvLsa PROTO ((tV3OsPath * pPath));

PRIVATE VOID V3RtcChkAndGenFnEqvLsa PROTO ((tV3OsPath * pPath));
/*****************************************************************************/
/* Function     : V3RtcInitRtInCxt                                           */
/*                                                                           */
/* Description  : Initializes the routing table.                             */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                u1ApplnId  : Application Id                                */
/*                u4Type     : Type                                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS or FAILURE                                         */
/*****************************************************************************/
PUBLIC INT1
V3RtcInitRtInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1ApplnId, UINT4 u4Type)
{
    tTrieCrtParams      ospfCreate;
    tV3OspfRt          *pV3OspfRt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcInitRt\n");

    pV3OspfRt = &pV3OspfCxt->ospfV3RtTable;

    pV3OspfRt->pOspfV3Root = NULL;
    ospfCreate.u2KeySize = OSPFV3_TRIE_KEY_SIZE;
    ospfCreate.u4Type = u4Type;
    ospfCreate.AppFns = &(V3RtcTrieLibFuncs);
    ospfCreate.u4NumRadixNodes = OSPFV3_MAX_ROUTES + 1;
    ospfCreate.u4NumLeafNodes = OSPFV3_MAX_ROUTES;
    ospfCreate.u4NoofRoutes = OSPFV3_MAX_ROUTES;
    ospfCreate.u1AppId = u1ApplnId;
    ospfCreate.bPoolPerInst = OSIX_FALSE;
    ospfCreate.bSemPerInst = OSIX_FALSE;
    ospfCreate.bValidateType = OSIX_FALSE;

    if ((pV3OspfRt->pOspfV3Root = TrieCreateInstance (&ospfCreate)) == NULL)
    {
        return OSIX_FAILURE;
    }

    pV3OspfRt->i1OspfId = u1ApplnId;

    TMO_SLL_Init (&(pV3OspfRt->routesList));
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcInitRt\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcCalculateRtInCxt                                      */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.                           */
/*                Reference : RFC-2740 Section 3.8.                          */
/*                This procedure calculates the entire routing table. If     */
/*                there is any change in any routing table entry appropriate */
/*                LSAs are originated.                                       */
/*                                                                           */
/* Input        : pV3OspfCxt   -    Context pointer                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateRtInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    UINT1               u1NonNssaAbrChgFlag = OSIX_FALSE;
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateRt\n");

    pV3OspfCxt->u4RouteCalcCompleted = OSPFV3_ROUTE_CALC_IN_PROGRESS;

    /* Check if route-calculation staggering enabled,
     * if enabled set next relinquish time/
     */
    if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        pV3OspfCxt->u4StaggeringDelta =
            (u4CurrentTime + (pV3OspfCxt->u4RTStaggeringInterval / 1000));
    }

    V3RtcMarkAllPrefixAndAsbrLsasInCxt (pV3OspfCxt);
    V3RtcMarkAllRoutesInCxt (pV3OspfCxt);

    /* Step-1: Preform Intra Area route calculation for all attached areas */
    /* Reference : RFC-2328 Section 16.1 */
    /* Reference : RFC-2740 Section 3.8.1  */
    pArea = pV3OspfCxt->pBackbone;
    while ((pArea = (tV3OsArea *)
            TMO_SLL_Next (&(pV3OspfCxt->areasLst), &(pArea->nextArea))) != NULL)
    {
        V3RtcCalculateIntraAreaRoute (pArea);
    }

    if (pV3OspfCxt->pBackbone != NULL)
    {
        V3RtcCalculateIntraAreaRoute (pV3OspfCxt->pBackbone);
    }

    /* Step-2: Perform Inter Area route calculation */
    /* Reference : RFC-2328 Section 16.2 and 16.3 */
    /* Reference : RFC-2740 Section 3.8.2 and 3.8.3 */
    V3RtcCalAllInterAreaRtsInCxt (pV3OspfCxt);

    /* Step-3: Perform NSSA route calculation */
    /* Step-4: Perform External route calculation */
    /* Reference : RFC-2328 Section 16.4 */
    /* Reference : RFC-2740 Section 3.8.4  */
    /* Reference : RFC-3101 Section 2.5 */
    V3RtcCalculateASExtRoutesInCxt (pV3OspfCxt);

    V3ChkAndGenerateSumToNewAreasInCxt (pV3OspfCxt);
    V3RtcProcessUnreachableRoutesInCxt (pV3OspfCxt);

    V3RagNssaType7To5TranslatorInCxt (pV3OspfCxt);
    V3RtcCheckTranslationInCxt (pV3OspfCxt);

    if (OSPFV3_STAGGERING_ENABLED == gV3OsRtr.u4RTStaggeringStatus)
    {
        u4CurrentTime = OsixGetSysUpTime ();
        if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
        {
            OSPF3RtcRelinquishInCxt (pV3OspfCxt);
        }
    }

    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* Translator election and
         * external LSA translation cannot be done in GR mode */
        pV3OspfCxt->u4RouteCalcCompleted = OSPFV3_ROUTE_CALC_COMPLETED;
        return;
    }

    if (pV3OspfCxt->u4NssaAreaCount > 0)
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if ((pArea->u4AreaType != OSPFV3_NSSA_AREA) &&
                (pArea->u4AbrStatFlg == OSIX_TRUE))
            {
                u1NonNssaAbrChgFlag = OSIX_TRUE;
                break;
            }
        }

        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if ((pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                ((pArea->u4AbrStatFlg == OSIX_TRUE) ||
                 (u1NonNssaAbrChgFlag == OSIX_TRUE)))
            {
                V3AreaNssaFsmFindTranslator (pArea);
                pArea->u4AbrStatFlg = OSIX_FALSE;
            }
        }
    }
    /* Indicate that current route calculation is
     * completed(checks in o3tmr.c, when route recalc
     * timer is trigered)
     */
    pV3OspfCxt->u4RouteCalcCompleted = OSPFV3_ROUTE_CALC_COMPLETED;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateRt\n");
}

/*****************************************************************************/
/* Function     : V3RtcProcessRtEntryChangesInCxt                            */
/*                                                                           */
/* Description  : This function process the routing entry changes by         */
/*                comparing new route entry with old route entry.            */
/*                                                                           */
/* Input        : pV3OspfCxt    : Pointer to Context                         */
/*                pNewRtEntry   : Pointer to new routing entry               */
/*                pOldRtEntry   : Pointer to old routing entry               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC INT4
V3RtcProcessRtEntryChangesInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tV3OsRtEntry * pNewRtEntry,
                                 tV3OsRtEntry * pOldRtEntry)
{
    tV3OsPath          *pNewRtPath = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pScanArea = NULL;
    tV3OsRtEntry       *pCurrRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    UINT1               u1RtChanged = OSIX_FALSE;
    UINT1               u1NewPathCount = 0;
    UINT1               u1FallInAggr = OSIX_FALSE;
    UINT1               u1Status = 0;
    tV3OsLsaNode       *pLsaNode = NULL;
    UINT1               u1HopIndex = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessRtEntryChanges\n");

    u1NewPathCount = (UINT1) (TMO_SLL_Count (&(pNewRtEntry->pathLst)));

    if ((pOldRtEntry == NULL) && (u1NewPathCount == 0))
    {
        gu4V3RtcProcessRtEntryChangesInCxtFail++;
        return OSIX_FALSE;
    }
    else if ((pOldRtEntry == NULL) && (u1NewPathCount != 0))
    {
        pRtEntry = V3RtcCreateRtEntry ();
        if (pRtEntry == NULL)
        {
            while ((pPath = (tV3OsPath *)
                    TMO_SLL_Get (&(pNewRtEntry->pathLst))) != NULL)
            {
                OSPFV3_PATH_FREE (pPath);
            }
            gu4V3RtcProcessRtEntryChangesInCxtFail++;
            return OSIX_FALSE;
        }

        MEMCPY (pRtEntry, pNewRtEntry, sizeof (tV3OsRtEntry));
        TMO_SLL_Init (&(pRtEntry->pathLst));

        while ((pPath = (tV3OsPath *)
                TMO_SLL_Get (&(pNewRtEntry->pathLst))) != NULL)
        {
            TMO_SLL_Add (&(pRtEntry->pathLst), &(pPath->nextPath));
        }

        TMO_SLL_Init (&(pRtEntry->lsaLst));
        while ((pLsaNode = (tV3OsLsaNode *)
                TMO_SLL_Get (&(pNewRtEntry->lsaLst))) != NULL)
        {
            TMO_SLL_Add (&(pRtEntry->lsaLst), &(pLsaNode->nextLsaInfo));
        }

        pRtEntry->u1Flag = OSPFV3_FOUND;
        V3RtcAddRtEntryInCxt (pV3OspfCxt, pRtEntry);

        pPath = OSPFV3_GET_PATH (pRtEntry);

        if ((pPath != NULL) &&
            ((pPath->u1PathType == OSPFV3_INTRA_AREA) ||
             (pPath->u1PathType == OSPFV3_INTER_AREA)))
        {
            if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
                (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
            {
                O3GrSetLsIdInterAreaPrefixLsa (pV3OspfCxt,
                                               (UINT1 *) pRtEntry,
                                               OSPFV3_INTER_AREA_PREFIX_LSA);
            }
            else
            {
                V3SetLsIdInterAreaPrefixLsaInCxt (pV3OspfCxt,
                                                  (UINT1 *) pRtEntry,
                                                  OSPFV3_INTER_AREA_PREFIX_LSA);
            }
        }
        else
        {
            if ((pPath != NULL) &&
                (pPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA))
            {
                if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
                    (pV3OspfCxt->u1RestartExitReason ==
                     OSPFV3_RESTART_INPROGRESS))
                {
                    O3GrSetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pRtEntry,
                                              OSPFV3_AS_TRNSLTD_EXT_LSA);
                }
                else
                {
                    V3SetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pRtEntry,
                                            OSPFV3_AS_TRNSLTD_EXT_LSA);
                }
            }
        }
        V3RtmUpdateNewRtInCxt (pV3OspfCxt, pRtEntry);
        u1FallInAggr = V3RtcCheckAndGenAggrLsaInCxt (pV3OspfCxt, pRtEntry,
                                                     OSPFV3_CREATED);

        pCurrRtEntry = pRtEntry;
        if (pPath != NULL)
        {
            V3RtcChkAndFlushFnEqvLsa (pPath);
        }
        if (u1FallInAggr != OSIX_TRUE)
        {
            u1RtChanged = OSIX_TRUE;
        }
    }
    else if ((pOldRtEntry != NULL) && (u1NewPathCount == 0))
    {
        V3RtmDeleteRtInCxt (pV3OspfCxt, pOldRtEntry);
        V3RtcProcessUnreachableRouteInCxt (pV3OspfCxt, pOldRtEntry);
        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);
        if (pOldRtPath != NULL)
        {
            V3RtcChkAndGenFnEqvLsa (pOldRtPath);
        }
        V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pOldRtEntry);
        V3RtcCheckAndGenAggrLsaInCxt (pV3OspfCxt, pOldRtEntry, OSPFV3_DELETED);
        V3RtcFreeRtEntry (pOldRtEntry);
        return OSIX_TRUE;
    }
    else
    {
        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);
        pNewRtPath = OSPFV3_GET_PATH (pNewRtEntry);

        if ((pOldRtPath == NULL) || (pNewRtPath == NULL))
        {
            return u1RtChanged;
        }

        pCurrRtEntry = pNewRtEntry;

        u1RtChanged = V3RtcProcessNextHopChangesInCxt (pV3OspfCxt, pNewRtEntry,
                                                       pNewRtPath,
                                                       pOldRtEntry, pOldRtPath);

        if (pNewRtPath->u1HopCount != pOldRtPath->u1HopCount)
        {
            u1RtChanged = OSIX_TRUE;
        }

        if (pNewRtEntry->u1RtType != pOldRtEntry->u1RtType)
        {
            if (pNewRtEntry->u1RtType != OSPFV3_DIRECT)
            {
                for (u1HopIndex = 0;
                     u1HopIndex < pNewRtPath->u1HopCount; u1HopIndex++)
                {
                    V3RtmLeakRouteInCxt (pV3OspfCxt, pNewRtEntry, pNewRtPath,
                                         u1HopIndex, NETIPV6_ADD_ROUTE);
                }
            }

            if (pOldRtEntry->u1RtType != OSPFV3_DIRECT)
            {
                for (u1HopIndex = 0;
                     u1HopIndex < pOldRtPath->u1HopCount; u1HopIndex++)
                {
                    V3RtmLeakRouteInCxt (pV3OspfCxt, pOldRtEntry, pOldRtPath,
                                         u1HopIndex, NETIPV6_DELETE_ROUTE);
                }
            }

            pOldRtEntry->u1RtType = pNewRtEntry->u1RtType;
            u1RtChanged = OSIX_TRUE;
        }

        if (pNewRtPath->u1PathType != pOldRtPath->u1PathType)
        {
            if ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) &&
                (pNewRtPath->u1PathType == OSPFV3_INTER_AREA))
            {
                if ((V3UtilAreaIdComp (pOldRtPath->areaId,
                                       pV3OspfCxt->pBackbone->areaId)
                     == OSPFV3_EQUAL) &&
                    (V3UtilAreaIdComp (pNewRtPath->areaId,
                                       pV3OspfCxt->pBackbone->areaId)
                     != OSPFV3_EQUAL))
                {
                    V3RtcFlushOutSummaryFromAllAreasInCxt
                        (pV3OspfCxt, OSPFV3_INTER_AREA_PREFIX_LSA, pOldRtEntry);
                }
                else
                {
                    V3RtcFlushOutSummaryLsa (pV3OspfCxt->pBackbone,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pOldRtEntry->linkStateId));
                }

                if (pNewRtPath->pLsaInfo != NULL)
                {
                    pArea = pNewRtPath->pLsaInfo->pArea;
                }

                if ((pArea != NULL) && (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea)))
                {
                    V3RtcFlushOutSummaryLsa (pArea,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pOldRtEntry->linkStateId));
                }

                while ((pLsaNode = (tV3OsLsaNode *)
                        TMO_SLL_Get (&(pOldRtEntry->lsaLst))) != NULL)
                {
                    OSPFV3_LSA_NODE_FREE (pLsaNode);
                }
            }

            if ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) &&
                (pNewRtPath->u1PathType != OSPFV3_INTER_AREA))
            {
                TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pScanArea, tV3OsArea *)
                {
                    /* If the associated area is the same area then skip route */
                    if (V3UtilAreaIdComp (pOldRtPath->areaId,
                                          pScanArea->areaId) == OSPFV3_EQUAL)
                    {
                        continue;
                    }

                    V3RtcFlushOutSummaryLsa (pScanArea,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pOldRtEntry->linkStateId));
                }
            }

            if (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)
            {
                if (V3UtilAreaIdComp (pNewRtPath->areaId,
                                      OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)
                {
                    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pScanArea,
                                  tV3OsArea *)
                    {
                        V3RtcFlushOutSummaryLsa (pScanArea,
                                                 OSPFV3_INTER_AREA_PREFIX_LSA,
                                                 &(pOldRtEntry->linkStateId));
                    }
                }
            }

            if (pOldRtPath->u1PathType == OSPFV3_INTRA_AREA)
            {
                u1Status = OSPFV3_DELETED;
            }

            if (pNewRtPath->u1PathType == OSPFV3_INTRA_AREA)
            {
                u1Status = OSPFV3_CREATED;
            }
            u1RtChanged = OSIX_TRUE;
        }
        else if ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) &&
                 (pOldRtPath->u4Cost != pNewRtPath->u4Cost))
        {
            u1Status = OSPFV3_UPDATED;
        }

        if ((pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR) &&
            (pNewRtPath->u1PathType == OSPFV3_INTER_AREA) &&
            (pOldRtPath->u1PathType == OSPFV3_INTER_AREA) &&
            (V3UtilAreaIdComp (pOldRtPath->areaId,
                               OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL) &&
            (V3UtilAreaIdComp (pNewRtPath->areaId,
                               OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL))
        {
            u1RtChanged = OSIX_TRUE;
        }

        if ((pNewRtPath->u4Cost != pOldRtPath->u4Cost) ||
            (pNewRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost))
        {
            u1RtChanged = OSIX_TRUE;
        }

        if (!((pNewRtPath->u1PathType == OSPFV3_TYPE_2_EXT) &&
              (pNewRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost)))
        {
            if (V3UtilAreaIdComp (pNewRtPath->areaId, pOldRtPath->areaId)
                != OSPFV3_EQUAL)
            {
                V3RtcCheckAddrRangeFlagInCxt (pV3OspfCxt,
                                              &(pNewRtEntry->destRtPrefix),
                                              &(pNewRtPath->areaId),
                                              &(pOldRtPath->areaId));
                u1RtChanged = OSIX_TRUE;
            }
        }

        if ((pNewRtPath->u1PathType == OSPFV3_INTER_AREA) &&
            (pOldRtPath->u1PathType == OSPFV3_INTER_AREA))
        {
            if (V3UtilAreaIdComp (pNewRtPath->areaId, pOldRtPath->areaId)
                != OSPFV3_EQUAL)
            {

                pScanArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                                &pNewRtPath->areaId);
                if (pScanArea != NULL)
                {
                    V3RtcFlushOutSummaryLsa (pScanArea,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pOldRtEntry->linkStateId));
                }
                u1RtChanged = OSIX_TRUE;
            }
        }

        if (((pNewRtPath->u1PathType == OSPFV3_TYPE_1_EXT) ||
             (pNewRtPath->u1PathType == OSPFV3_TYPE_2_EXT)))
        {
            if (((pOldRtPath->pLsaInfo != NULL) &&
                 (pOldRtPath->pLsaInfo->lsaId.u2LsaType != OSPFV3_NSSA_LSA)) &&
                (pNewRtPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA))
            {
                if ((pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED) &&
                    (pV3OspfCxt->u1RestartExitReason ==
                     OSPFV3_RESTART_INPROGRESS))
                {
                    O3GrSetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pOldRtEntry,
                                              OSPFV3_AS_TRNSLTD_EXT_LSA);
                }
                else
                {
                    V3SetLsIdAsExtLsaInCxt (pV3OspfCxt, (UINT1 *) pOldRtEntry,
                                            OSPFV3_AS_TRNSLTD_EXT_LSA);
                }
            }
            u1RtChanged = OSIX_TRUE;
        }

        pOldRtEntry->u1Flag = OSPFV3_FOUND;

        if (u1RtChanged == OSIX_TRUE)
        {
            TMO_SLL_Delete (&(pOldRtEntry->pathLst), &pOldRtPath->nextPath);
            OSPFV3_PATH_FREE (pOldRtPath);
            TMO_SLL_Delete (&(pCurrRtEntry->pathLst), &pNewRtPath->nextPath);
            V3RtcAddPath (pOldRtEntry, pNewRtPath);
            pCurrRtEntry = pOldRtEntry;

            if (u1Status != 0)
            {
                V3RtcCheckAndGenAggrLsaInCxt (pV3OspfCxt, pCurrRtEntry,
                                              u1Status);
            }
        }
        else
        {
            TMO_SLL_Delete (&(pCurrRtEntry->pathLst), &pNewRtPath->nextPath);
            OSPFV3_PATH_FREE (pNewRtPath);
        }
    }

    if (u1RtChanged == OSIX_TRUE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pV3OspfCxt->u4ContextId, "Route Changed\n");
        pPath = OSPFV3_GET_PATH (pCurrRtEntry);

        if ((pPath != NULL) &&
            ((pPath->u1PathType == OSPFV3_INTRA_AREA) ||
             (pPath->u1PathType == OSPFV3_INTER_AREA)) &&
            (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
        {
            if (pPath->u1HopCount != 0)
            {
                OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Summary LSA Generated Due To Route Change\n");
                V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_SUMMARY_GEN,
                                       (UINT1 *) pCurrRtEntry);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcProcessRtEntryChanges\n");
    return u1RtChanged;
}

/*****************************************************************************/
/* Function     : V3RtcProcessASBRRtEntryChangesInCxt                        */
/*                                                                           */
/* Description  : This function process the ASBR routing entry changes by    */
/*                comparing new route entry with old route entry.            */
/*                                                                           */
/* Input        : pV3OspfCxt    : Pointer to Context                         */
/*                pNewRtEntry   : Pointer to new routing entry               */
/*                pOldRtEntry   : Pointer to old routing entry               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC INT4
V3RtcProcessASBRRtEntryChangesInCxt (tV3OspfCxt * pV3OspfCxt,
                                     tV3OsRtEntry * pNewRtEntry,
                                     tV3OsRtEntry * pOldRtEntry)
{
    tV3OsPath          *pNewRtPath = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsRtEntry       *pCurrRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    UINT1               u1RtChanged = OSIX_FALSE;
    UINT4               u4NewPathCount = 0;
    tV3OsArea          *pOldArea = NULL;
    tV3OsArea          *pNewArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessASBRRtEntryChanges\n");

    u4NewPathCount = TMO_SLL_Count (&(pNewRtEntry->pathLst));

    if ((pOldRtEntry == NULL) && (u4NewPathCount == 0))
    {
        return OSIX_FALSE;
    }
    else if ((pOldRtEntry == NULL) && (u4NewPathCount != 0))
    {
        if ((pRtEntry = V3RtcCreateRtEntry ()) == NULL)
        {
            while ((pPath = (tV3OsPath *)
                    TMO_SLL_Get (&(pNewRtEntry->pathLst))) != NULL)
            {
                OSPFV3_PATH_FREE (pPath);
            }
            return OSIX_FALSE;
        }

        MEMCPY (pRtEntry, pNewRtEntry, sizeof (tV3OsRtEntry));
        TMO_SLL_Init (&(pRtEntry->pathLst));

        while ((pPath = (tV3OsPath *)
                TMO_SLL_Get (&(pNewRtEntry->pathLst))) != NULL)
        {
            TMO_SLL_Add (&(pRtEntry->pathLst), &(pPath->nextPath));
        }

        pRtEntry->u1Flag = OSPFV3_FOUND;
        V3RtcAddRtEntryInCxt (pV3OspfCxt, pRtEntry);

        OSPFV3_LINK_STATE_ID_COPY (pRtEntry->linkStateId,
                                   pRtEntry->destRtRtrId);
        pCurrRtEntry = pRtEntry;
        u1RtChanged = OSIX_TRUE;
    }
    else if ((pOldRtEntry != NULL) && (u4NewPathCount == 0))
    {
        pOldRtPath = (tV3OsPath *) TMO_SLL_First (&(pOldRtEntry->pathLst));
        pOldArea = V3GetFindAreaInCxt (pV3OspfCxt, &pOldRtPath->areaId);

        if ((pOldArea != NULL) && (pOldArea->u4AreaType != OSPFV3_NSSA_AREA))
        {
            V3RtcProcessUnreachableRouteInCxt (pV3OspfCxt, pOldRtEntry);
            TMO_SLL_Delete (&(pOldRtEntry->pathLst), &(pOldRtPath->nextPath));
            OSPFV3_PATH_FREE (pOldRtPath);
        }
        if (TMO_SLL_Count (&(pOldRtEntry->pathLst)) == 0)
        {
            V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pOldRtEntry);
            V3RtcFreeRtEntry (pOldRtEntry);
        }
        return OSIX_TRUE;
    }
    else
    {
        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);
        pNewRtPath = OSPFV3_GET_PATH (pNewRtEntry);

        if ((pOldRtPath == NULL) || (pNewRtPath == NULL))
        {
            return u1RtChanged;
        }
        pOldArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pOldRtPath->areaId));
        pNewArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pNewRtPath->areaId));

        if ((pOldArea != NULL) && (pNewArea != NULL) &&
            (pOldArea->u4AreaType == OSPFV3_NORMAL_AREA) &&
            (pOldRtPath->u4Cost == pNewRtPath->u4Cost) &&
            (pNewArea->u4AreaType == OSPFV3_NORMAL_AREA) &&
            (V3UtilAreaIdComp (pOldRtPath->areaId, pNewRtPath->areaId) !=
             OSPFV3_EQUAL))
        {
            /* Add new path to the ASBR */
            TMO_SLL_Add (&(pOldRtEntry->pathLst), &(pNewRtPath->nextPath));
            pOldRtEntry->u1Flag = OSPFV3_FOUND;
            return OSIX_FALSE;

        }

        else if ((pOldArea != NULL) && (pNewArea != NULL) &&
                 (pOldArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                 (pNewArea->u4AreaType != OSPFV3_NSSA_AREA))
        {
            TMO_SLL_Insert (&(pOldRtEntry->pathLst), NULL,
                            &(pNewRtPath->nextPath));

            pOldRtEntry->u1Flag = OSPFV3_FOUND;
            pCurrRtEntry = pOldRtEntry;
            u1RtChanged = OSIX_TRUE;
        }
        else if ((pOldArea != NULL) && (pNewArea != NULL) &&
                 (pOldArea->u4AreaType != OSPFV3_NSSA_AREA) &&
                 (pNewArea->u4AreaType == OSPFV3_NSSA_AREA))
        {
            V3RtcProcessUnreachableRouteInCxt (pV3OspfCxt, pOldRtEntry);
            TMO_SLL_Delete (&(pOldRtEntry->pathLst), &(pOldRtPath->nextPath));
            OSPFV3_PATH_FREE (pOldRtPath);
            pOldRtEntry->u1Flag = OSPFV3_FOUND;
            return OSIX_TRUE;
        }
        else if ((pOldArea != NULL) && (pNewArea != NULL) &&
                 (pOldArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                 (pNewArea->u4AreaType == OSPFV3_NSSA_AREA))
        {
            pOldRtEntry->u1Flag = OSPFV3_FOUND;
            return OSIX_FALSE;
        }
        else
        {
            pCurrRtEntry = pNewRtEntry;

            u1RtChanged = V3RtcProcessNextHopChangesInCxt
                (pV3OspfCxt, pNewRtEntry, pNewRtPath, pOldRtEntry, pOldRtPath);

            if (pNewRtPath->u1HopCount != pOldRtPath->u1HopCount)
            {
                u1RtChanged = OSIX_TRUE;
            }

            if (pOldRtPath->pLsaInfo != pNewRtPath->pLsaInfo)
            {
                u1RtChanged = OSIX_TRUE;
            }

            if (pNewRtPath->u1PathType != pOldRtPath->u1PathType)
            {
                if ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) &&
                    (pNewRtPath->u1PathType == OSPFV3_INTER_AREA))
                {
                    if ((V3UtilAreaIdComp (pOldRtPath->areaId,
                                           pV3OspfCxt->pBackbone->areaId)
                         == OSPFV3_EQUAL) &&
                        (V3UtilAreaIdComp (pNewRtPath->areaId,
                                           pV3OspfCxt->pBackbone->areaId)
                         != OSPFV3_EQUAL))
                    {
                        V3RtcFlushOutSummaryFromAllAreasInCxt
                            (pV3OspfCxt, OSPFV3_INTER_AREA_ROUTER_LSA,
                             pOldRtEntry);
                    }
                    else
                    {
                        V3RtcFlushOutSummaryLsa (pV3OspfCxt->pBackbone,
                                                 OSPFV3_INTER_AREA_ROUTER_LSA,
                                                 &(pOldRtEntry->linkStateId));
                    }

                    pArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                                &pNewRtPath->areaId);

                    if ((pArea != NULL)
                        && (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea)))
                    {
                        V3RtcFlushOutSummaryLsa (pArea,
                                                 OSPFV3_INTER_AREA_ROUTER_LSA,
                                                 &(pOldRtEntry->linkStateId));
                    }
                }
                u1RtChanged = OSIX_TRUE;
            }

            if (V3UtilAreaIdComp (pNewRtPath->areaId,
                                  pOldRtPath->areaId) != OSPFV3_EQUAL)
            {
                if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt,
                                                 &(pOldRtPath->areaId)))
                    != NULL)
                {
                    V3RtcFlushOutSummaryLsa (pArea,
                                             OSPFV3_INTER_AREA_ROUTER_LSA,
                                             &(pOldRtEntry->linkStateId));
                }
                u1RtChanged = OSIX_TRUE;
            }

            if ((pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR) &&
                (pNewRtPath->u1PathType == OSPFV3_INTER_AREA) &&
                (pOldRtPath->u1PathType == OSPFV3_INTER_AREA) &&
                (V3UtilAreaIdComp (pOldRtPath->areaId,
                                   OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL) &&
                (V3UtilAreaIdComp (pNewRtPath->areaId,
                                   OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL))
            {
                u1RtChanged = OSIX_TRUE;
            }

            if (pNewRtPath->u4Cost != pOldRtPath->u4Cost)
            {
                u1RtChanged = OSIX_TRUE;
            }

            pOldRtEntry->u1Flag = OSPFV3_FOUND;

            if (u1RtChanged == OSIX_TRUE)
            {
                TMO_SLL_Delete (&(pOldRtEntry->pathLst), &pOldRtPath->nextPath);
                OSPFV3_PATH_FREE (pOldRtPath);
                TMO_SLL_Delete (&(pCurrRtEntry->pathLst),
                                &pNewRtPath->nextPath);
                TMO_SLL_Insert (&(pOldRtEntry->pathLst), NULL,
                                &(pNewRtPath->nextPath));
                pCurrRtEntry = pOldRtEntry;
            }
            else
            {
                TMO_SLL_Delete (&(pCurrRtEntry->pathLst),
                                &pNewRtPath->nextPath);
                OSPFV3_PATH_FREE (pNewRtPath);
            }
        }
    }

    if (u1RtChanged == OSIX_TRUE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pV3OspfCxt->u4ContextId, "Route Changed\n");
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Summary LSA Generated Due To Route Change\n");
        V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_SUMMARY_GEN,
                               (UINT1 *) pCurrRtEntry);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcProcessASBRRtEntryChanges\n");
    return u1RtChanged;
}

/****************************************************************************/
/* Function      : V3RtcIsLsaValidForRtCalc                                 */
/*                                                                          */
/* Description   : This function checks whether the LSA is MAX_AGE or       */
/*                 Self originated LSA.                                     */
/*                                                                          */
/* Input         : pLsaInfo - Pointer to the LSA which is checked for       */
/*                            the eligibility for route calculation         */
/*                                                                          */
/* Output        : None                                                     */
/*                                                                          */
/* Returns       : OSIX_TRUE, if eligible for route calculation             */
/*                 else returns OSIX_FALSE                                  */
/****************************************************************************/
PUBLIC INT1
V3RtcIsLsaValidForRtCalc (tV3OsLsaInfo * pLsaInfo)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcIsLsaValidForRtCalc\n");

    if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "MAX_AGE LSA is not "
                    "not considered in route calculation\n");
        gu4V3RtcIsLsaValidForRtCalcFail++;
        return OSIX_FALSE;
    }

    if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                         pLsaInfo->pV3OspfCxt->rtrId) == OSPFV3_EQUAL)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId, "Self Originated LSA "
                    "is not considered in route calculation\n");
        gu4V3RtcIsLsaValidForRtCalcFail++;
        return OSIX_FALSE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcIsLsaValidForRtCalc\n");
    return OSIX_TRUE;
}

/*****************************************************************************/
/* Function     : V3RtcProcessNextHopChangesInCxt                            */
/*                                                                           */
/* Description  : This function process and updates common routing table     */
/*                of the changes in the route entry                          */
/*                                                                           */
/* Input        : pV3OspfCxt   : Context pointer                             */
/*                pNewRtEntry  : Pointer to new route entry                  */
/*                pNewRtPath   : Pointer to new path                         */
/*                pOldRtEntry  : Pointer to old route entry                  */
/*                pOldRtPath   : Pointer to old path                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC UINT1
V3RtcProcessNextHopChangesInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tV3OsRtEntry * pNewRtEntry,
                                 tV3OsPath * pNewRtPath,
                                 tV3OsRtEntry * pOldRtEntry,
                                 tV3OsPath * pOldRtPath)
{
    UINT1               u1OldHopIndex = 0;
    UINT1               u1NewHopIndex = 0;
    tV3OsTruthValue     bNextHopFound = OSIX_FALSE;
    UINT1               u1Flag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessNextHopChanges\n");

    for (u1OldHopIndex = 0; ((u1OldHopIndex < pOldRtPath->u1HopCount) &&
                             (u1OldHopIndex < OSPFV3_MAX_NEXT_HOPS));
         u1OldHopIndex++)
    {
        for (u1NewHopIndex = 0; ((u1NewHopIndex < pNewRtPath->u1HopCount) &&
                                 (u1NewHopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1NewHopIndex++)
        {
            if (V3UtilIp6AddrComp
                (&pOldRtPath->aNextHops[u1OldHopIndex].nbrIpv6Addr,
                 &pNewRtPath->aNextHops[u1NewHopIndex].nbrIpv6Addr)
                == OSPFV3_EQUAL)
            {
                bNextHopFound = OSIX_TRUE;

                if ((pOldRtPath->u4Cost != pNewRtPath->u4Cost) ||
                    ((pNewRtPath->u1PathType == OSPFV3_TYPE_2_EXT) &&
                     (pNewRtPath->u4Type2Cost != pOldRtPath->u4Type2Cost)))
                {
                    u1Flag = OSIX_TRUE;

                    if (OSPFV3_IS_DEST_NETWORK (pNewRtEntry))
                    {
                        V3RtmLeakRouteInCxt (pV3OspfCxt, pNewRtEntry,
                                             pNewRtPath, u1NewHopIndex,
                                             NETIPV6_MODIFY_ROUTE);
                    }
                }
                break;
            }
        }
        if (bNextHopFound == OSIX_FALSE)
        {
            u1Flag = OSIX_TRUE;

            if (OSPFV3_IS_DEST_NETWORK (pNewRtEntry))
            {
                V3RtmLeakRouteInCxt (pV3OspfCxt, pOldRtEntry, pOldRtPath,
                                     u1OldHopIndex, NETIPV6_DELETE_ROUTE);
            }
        }
        else
        {
            bNextHopFound = OSIX_FALSE;
        }
    }

    for (u1NewHopIndex = 0; ((u1NewHopIndex < pNewRtPath->u1HopCount) &&
                             (u1NewHopIndex < OSPFV3_MAX_NEXT_HOPS));
         u1NewHopIndex++)
    {
        for (u1OldHopIndex = 0; ((u1OldHopIndex < pOldRtPath->u1HopCount) &&
                                 (u1OldHopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1OldHopIndex++)
        {
            if (V3UtilIp6AddrComp
                (&pOldRtPath->aNextHops[u1OldHopIndex].nbrIpv6Addr,
                 &pNewRtPath->aNextHops[u1NewHopIndex].nbrIpv6Addr)
                == OSPFV3_EQUAL)
            {
                bNextHopFound = OSIX_TRUE;
                break;
            }
        }
        if (bNextHopFound == OSIX_FALSE)
        {
            u1Flag = OSIX_TRUE;

            if (OSPFV3_IS_DEST_NETWORK (pNewRtEntry))
            {
                V3RtmLeakRouteInCxt (pV3OspfCxt, pNewRtEntry, pNewRtPath,
                                     u1NewHopIndex, NETIPV6_ADD_ROUTE);
            }
        }
        else
        {
            bNextHopFound = OSIX_FALSE;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcProcessNextHopChanges\n");
    return u1Flag;
}

/*****************************************************************************/
/* Function     : V3RtcFlushOutSummaryFromAllAreasInCxt                      */
/*                                                                           */
/* Description  : This function flushes summary LSAs correspoding to         */
/*                the given route entry from all areas except Backbone       */
/*                                                                           */
/* Input        : pV3OspfCxt   : Context pointer                             */
/*                u2LsaType    : Type of the LSA                             */
/*                pLinkStateId : Pointer to link state Id of the route to be */
/*                               flushed out.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcFlushOutSummaryFromAllAreasInCxt (tV3OspfCxt * pV3OspfCxt,
                                       UINT2 u2LsaType, tV3OsRtEntry * pRtEntry)
{
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcFlushOutSummaryFromAllAreas\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA cannot be Flushed in GR mode */
        return;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (pArea->areaId, OSPFV3_BACKBONE_AREAID)
            == OSPFV3_EQUAL)
        {
            continue;
        }
        V3RtcFlushOutSummaryLsa (pArea, u2LsaType, &(pRtEntry->linkStateId));
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcFlushOutSummaryFromAllAreas\n");
}

/*****************************************************************************/
/* Function     : V3RtcFlushOutSummaryLsa                                    */
/*                                                                           */
/* Description  : This routine gets the LSA info based on LSA type and then  */
/*                flushes the LSA.                                           */
/*                                                                           */
/* Input        : pArea        : Pointer to area from where the is to        */
/*                               be flushed                                  */
/*                u2LsaType    : Type of LSA                                 */
/*                pLinkStateId : Pointer to link state Id of the LSA         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcFlushOutSummaryLsa (tV3OsArea * pArea,
                         UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcFlushOutSummaryLsa\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA cannot be Flushed in GR mode */
        return;
    }

    pLsaInfo = V3LsuSearchDatabase (u2LsaType, pLinkStateId,
                                    &(pArea->pV3OspfCxt->rtrId), NULL, pArea);
    if (pLsaInfo != NULL)
    {
        V3AgdFlushOut (pLsaInfo);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcFlushOutSummaryLsa\n");

}

/*****************************************************************************/
/* Function     : V3RtcProcessUnreachableRouteInCxt                          */
/*                                                                           */
/* Description  : This function flushes all the unreachable route.           */
/*                                                                           */
/* Input        : pV3OspfCxt: Pointer to Context                             */
/*                pRtEntry  : Pointer to route entry which is unreachable    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessUnreachableRouteInCxt (tV3OspfCxt * pV3OspfCxt,
                                   tV3OsRtEntry * pRtEntry)
{
    tV3OsArea          *pArea = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OsPath          *pNssaPath = NULL;
    tV3OsArea          *pNssaArea = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsExtLsaLink     nssaLsaLink;
    tV3OsLsaInfo       *pFlushLsaInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessUnreachableRoute\n");

    pPath = OSPFV3_GET_PATH (pRtEntry);

    if (pPath == NULL)
    {
        return;
    }

    /* If the route is external route */
    if ((pPath->u1PathType == OSPFV3_TYPE_1_EXT) ||
        (pPath->u1PathType == OSPFV3_TYPE_2_EXT))
    {
        /* Handling when Type 7 route is lost and LSA was translated to  */
        /* Type 5  */

        if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
        {
            /* LSA cannot be Flushed in GR mode */
            return;
        }

        pNssaPath = pPath;
        if (pNssaPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA)
        {
            pNssaArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pNssaPath->areaId));
            if ((pNssaArea != NULL) &&
                (pNssaArea->u4AreaType == OSPFV3_NSSA_AREA))
            {
                pNssaPath->pLsaInfo->u1TrnsltType5 = OSPFV3_FLUSHED_LSA;

                V3RagNssaType7LsaTranslation (pNssaArea, pNssaPath->pLsaInfo);
                V3RagNssaAdvertiseType7Rngs (pNssaArea);

                /* Backbone area pointer is passed as the last parameter in
                 * search database. This is used to get the context pointer
                 * to scan the external LSA
                 */
                pFlushLsaInfo = V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                                     &(pRtEntry->linkStateId),
                                                     &(pV3OspfCxt->rtrId), NULL,
                                                     pV3OspfCxt->pBackbone);

                if ((pFlushLsaInfo != NULL) &&
                    (pFlushLsaInfo->pLsaDesc->u2InternalLsaType ==
                     OSPFV3_AS_TRNSLTD_EXT_LSA))
                {
                    V3AgdFlushOut (pFlushLsaInfo);
                }

                /* Functionally equivalent TYPE 7 LSA                  */
                /* If router has entry for same network in Ext Rt      */
                /* Table, it meant it previously flushed its Type7 LSA */
                /* because of being less preferred. That Type 7 LSA    */
                /* is now re-originated                                */
                V3RtcGetExtLsaLink (pPath->pLsaInfo->pLsa,
                                    pPath->pLsaInfo->u2LsaLen, &nssaLsaLink);

                if ((pExtRoute =
                     V3ExtrtFindRouteInCxt
                     (pV3OspfCxt,
                      &(nssaLsaLink.extRtPrefix.addrPrefix),
                      nssaLsaLink.extRtPrefix.u1PrefixLength)) != NULL)
                {
                    V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
                }
            }
        }
        return;
    }

    /* If the route entry is of INTRA Area or INTER Area */
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        /* If the associated area is the same area then skip route */
        if (V3UtilAreaIdComp (OSPFV3_GET_PATH_AREA_ID (pRtEntry),
                              pArea->areaId) == OSPFV3_EQUAL)
        {
            continue;
        }
        /* If destination is AS boundary router flush out type 4 lsa */
        if (OSPFV3_IS_DEST_ASBR (pRtEntry) &&
            (pArea->u4AreaType == OSPFV3_NORMAL_AREA))
        {
            V3RtcFlushOutSummaryLsa (pArea, OSPFV3_INTER_AREA_ROUTER_LSA,
                                     &(pRtEntry->linkStateId));
        }
        else if ((OSPFV3_IS_DEST_NETWORK (pRtEntry)) &&
                 (pPath->u1PathType == OSPFV3_INTER_AREA))
        {
            V3RtcFlushOutSummaryLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                                     &(pRtEntry->linkStateId));
        }
        else if ((OSPFV3_IS_DEST_NETWORK (pRtEntry)) &&
                 (pPath->u1PathType == OSPFV3_INTRA_AREA))
        {
            /* Backbone info is not condensed into  transit areas */
            if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pArea) &&
                (V3UtilAreaIdComp (OSPFV3_GET_PATH_AREA_ID (pRtEntry),
                                   pV3OspfCxt->pBackbone->areaId)
                 == OSPFV3_EQUAL))
            {
                V3RtcFlushOutSummaryLsa (pArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                                         &(pRtEntry->linkStateId));
            }
            else
            {
                if (V3AreaFindInternalAddrRngInCxt
                    (pV3OspfCxt,
                     &(pRtEntry->destRtPrefix),
                     &(OSPFV3_GET_PATH_AREA_ID (pRtEntry))) == NULL)
                {
                    V3RtcFlushOutSummaryLsa (pArea,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pRtEntry->linkStateId));
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcessUnreachableRoute\n");
}

/*****************************************************************************/
/* Function     : V3RtcProcessVirtualLinkInCxt                               */
/*                                                                           */
/* Description  : This function updates the state of the virtual link        */
/*                based on the event.                                        */
/*                                                                           */
/* Input        : pV3OspfCxt: Pointer to Context                             */
/*                pRtEntry  : Pointer to ABR route entry                     */
/*                u1Event   : Indicating event                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessVirtualLinkInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsRtEntry * pAbrRtEntry,
                              tV3OsPath * pAbrPath, UINT1 u1Event)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessVirtualLink\n");

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        if ((V3UtilRtrIdComp (pAbrRtEntry->destRtRtrId,
                              pInterface->destRtrId) != OSPFV3_EQUAL) ||
            (V3UtilAreaIdComp (pAbrPath->areaId,
                               pInterface->transitAreaId) != OSPFV3_EQUAL))
        {
            continue;
        }

        V3VifSetVifValues (pInterface, pAbrRtEntry);
        OSPFV3_TRC2 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pV3OspfCxt->u4ContextId,
                     "Transit AreaId %x NbrId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pInterface->transitAreaId),
                     OSPFV3_BUFFER_DWFROMPDU (pInterface->destRtrId));

        pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if ((pInterface->u1IsmState != OSPFV3_IFS_DOWN) &&
            (u1Event == OSPFV3_IFE_DOWN))
        {
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_DOWN);
        }
        else if ((pInterface->u1IsmState == OSPFV3_IFS_DOWN) &&
                 (u1Event == OSPFV3_IFE_UP))
        {
            if (V3UtilIp6AddrComp (&pNbr->nbrIpv6Addr,
                                   &gV3OsNullIp6Addr) != OSPFV3_EQUAL)
            {
                pInterface->operStatus = OSPFV3_ENABLED;
                V3IfUp (pInterface);
            }
        }
        else if (u1Event == OSPFV3_VIF_COST_CHANGED)
        {
            if ((pInterface->u1IsmState != OSPFV3_IFS_DOWN) &&
                (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
            {
                V3SignalLsaRegenInCxt
                    (pV3OspfCxt, OSPFV3_SIG_VIRT_LINK_COST_CHANGE,
                     (UINT1 *) pInterface);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcProcessVirtualLink\n");
}

/*****************************************************************************/
/* Function     : V3RtcProcNonStandABRVirtualLink                            */
/*                                                                           */
/* Description  : This function updates the state of the virtual link        */
/*                based on the event.                                        */
/*                                                                           */
/* Input        : pSpfNode : Pointer to ABR route entry                      */
/*                pArea    : Pointer to ABR route entry                      */
/*                u1Event  : Indicating event                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcNonStandABRVirtualLink (tV3OsCandteNode * pSpfNode,
                                 tV3OsArea * pArea, UINT1 u1Event)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcNonStandABRVirtualLink\n");

    if (pSpfNode->u1VertType == OSPFV3_VERT_NETWORK)
    {
        return;
    }

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        if ((V3UtilRtrIdComp (pSpfNode->vertexId.vertexRtrId,
                              pInterface->destRtrId) != OSPFV3_EQUAL) ||
            (V3UtilAreaIdComp (pArea->areaId,
                               pInterface->transitAreaId) != OSPFV3_EQUAL))
        {
            continue;
        }

        V3VifSetVLValues (pInterface, pSpfNode);

        OSPFV3_TRC2 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                     pArea->pV3OspfCxt->u4ContextId,
                     "Transit AreaId %x NbrId %x\n",
                     OSPFV3_BUFFER_DWFROMPDU (pInterface->transitAreaId),
                     OSPFV3_BUFFER_DWFROMPDU (pInterface->destRtrId));

        pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if ((pInterface->u1IsmState != OSPFV3_IFS_DOWN) &&
            (u1Event == OSPFV3_IFE_DOWN))
        {
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_DOWN);
        }
        else if ((pInterface->u1IsmState == OSPFV3_IFS_DOWN) &&
                 (u1Event == OSPFV3_IFE_UP))
        {
            if (V3UtilIp6AddrComp (&pNbr->nbrIpv6Addr,
                                   &gV3OsNullIp6Addr) != OSPFV3_EQUAL)
            {
                pInterface->operStatus = OSPFV3_ENABLED;
                V3IfUp (pInterface);
            }
        }
        else if (u1Event == OSPFV3_VIF_COST_CHANGED)
        {
            if ((pInterface->u1IsmState != OSPFV3_IFS_DOWN) &&
                (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                (pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
            {
                V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                       OSPFV3_SIG_VIRT_LINK_COST_CHANGE,
                                       (UINT1 *) pInterface);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcProcNonStandABRVirtualLink\n");
}

/*****************************************************************************/
/* Function     : V3RtcUpdateNextHops                                        */
/*                                                                           */
/* Description  : This function updated the next hops of the path.           */
/*                                                                           */
/* Input        : pDestRtPath : Pointer to destination path, whose next      */
/*                              are to be updated.                           */
/*                pRtrPath    : Pointer to the path from where the next      */
/*                              hops are to be updated.                      */
/*                pLsaInfo    : Pointer to LSA.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcUpdateNextHops (tV3OsPath * pDestRtPath,
                     tV3OsPath * pRtrPath, tV3OsLsaInfo * pLsaInfo)
{
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    INT1                i1PathExists = OSIX_TRUE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcUpdateNextHops\n");

    for (u1Index1 = 0, u1Index2 = pDestRtPath->u1HopCount;
         (u1Index1 < pRtrPath->u1HopCount) &&
         (u1Index1 < OSPFV3_MAX_NEXT_HOPS) &&
         (u1Index2 < OSPFV3_MAX_NEXT_HOPS); u1Index1++)
    {
        i1PathExists = V3RtcIsSameNextHop (pDestRtPath, pRtrPath, u1Index1);
        if (i1PathExists == OSIX_FALSE)
        {
            /* Next hop is not found in Old route entry */
            /* Add the next hop information derived from ABR route entry */
            MEMCPY (&pDestRtPath->aNextHops[u1Index2],
                    &pRtrPath->aNextHops[u1Index1], sizeof (tV3OsRouteNextHop));
            OSPFV3_RTR_ID_COPY (&pDestRtPath->aNextHops[u1Index2].advRtrId,
                                &pLsaInfo->lsaId.advRtrId);
            u1Index2++;
        }
    }
    pDestRtPath->u1HopCount = u1Index2;
    pDestRtPath->pLsaInfo = pLsaInfo;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcUpdateNextHops\n");
}

/*****************************************************************************/
/* Function     : V3RtcIsSameNextHop                                         */
/*                                                                           */
/* Description  : Checks if the Next Hop present in the 'pAbrPath' with      */
/*                the Hop Index 'u1HopIndex' is present in the               */
/*                'pDestRtPath'.                                             */
/*                                                                           */
/* Input        : pDestRtPath : Path in which the existence of ABR Path is   */
/*                               to be checked                               */
/*                pAreaBdrPath : ABR Path Entry whose next Hop denotd by     */
/*                               'u1HopIndex' is to be checked if            */
/*                               present in pDestRtPath                      */
/*                u1HopIndex   : Index of the ECMP whose path is to be       */
/*                               checked                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if the ABR Path's Next Hop is present in the    */
/*                given 'pDestRtPath' OSIX_FALSE, otherwise.                 */
/*****************************************************************************/
PRIVATE INT1
V3RtcIsSameNextHop (tV3OsPath * pDestRtPath, tV3OsPath * pAreaBdrPath,
                    UINT1 u1HopIndex)
{
    UINT1               u1Index = 0;
    INT1                i1HopExists = OSIX_FALSE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcIsSameNextHop\n");

    for (u1Index = 0; ((u1Index < pDestRtPath->u1HopCount) &&
                       (u1Index < OSPFV3_MAX_NEXT_HOPS)); u1Index++)
    {
        if (V3UtilIp6AddrComp (&pDestRtPath->aNextHops[u1Index].nbrIpv6Addr,
                               &pAreaBdrPath->aNextHops[u1HopIndex].
                               nbrIpv6Addr) == OSPFV3_EQUAL)
        {
            i1HopExists = OSIX_TRUE;
            break;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcIsSameNextHop\n");
    return i1HopExists;
}

/*****************************************************************************/
/* Function     : V3RtcCalculateRouteInCxt                                   */
/*                                                                           */
/* Description  : This function is to calculate the route for a prefix       */
/*                                                                           */
/* Input        : pV3OspfCxt  : Pointer to Context                           */
/*                pNewRtEntry : Pointer to New routing entry                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateRouteInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateRouteInCxt\n");

    V3RtcCalculateInterAreaRtInCxt (pV3OspfCxt, pNewRtEntry);

    /* If the intera area route calculation fails, then do external route 
     * calculation for OSPFV3_DEST_NETWORK route entries */
    if ((TMO_SLL_Count (&(pNewRtEntry->pathLst)) == 0) &&
        (pNewRtEntry->u1DestType == OSPFV3_DEST_NETWORK))
    {
        V3RtcCalculateExtRtInCxt (pV3OspfCxt, pNewRtEntry, NULL);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateRouteInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCheckTranslationInCxt                                 */
/*                                                                           */
/* Description  : Check for the translation                                  */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCheckTranslationInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsExtLsaLink     extLsaLink;
    UINT4               u4HashIndex = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCheckTranslationInCxt\n");

    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA cannot be Flushed in GR mode */
        return;
    }

    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (pV3OspfCxt->pExtLsaHashTable, u4HashIndex,
                              pDbHashNode, tV3OsDbNode *)
        {
            TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
            {
                pLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
                if ((pLsaInfo->u1TrnsltType5 == OSIX_FALSE) &&
                    (V3UtilRtrIdComp (pV3OspfCxt->rtrId,
                                      pLsaInfo->lsaId.advRtrId) ==
                     OSPFV3_EQUAL))
                {
                    /* While flushing check what all clean up is required */
                    V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                                        &extLsaLink);

                    V3AgdFlushOut (pLsaInfo);

                    if ((pExtRoute =
                         V3ExtrtFindRouteInCxt
                         (pV3OspfCxt, &(extLsaLink.extRtPrefix.addrPrefix),
                          extLsaLink.extRtPrefix.u1PrefixLength)) != NULL)
                    {
                        V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt,
                                                          pExtRoute);
                    }
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCheckTranslationInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcSetVirtNextHopInCxt                                   */
/*                                                                           */
/* Description  : This function find the vitual next hop from intra area     */
/*                prefix LSA.                                                */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                pTranAreaId  -  Transit area Id                            */
/*                pDestRtrId   -  Destination router id                      */
/*                pVirtNextHop -  Virtual next hop                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE if virtual next hop is found                     */
/*                OSIX_FALSE otherwise.                                      */
/*****************************************************************************/
PUBLIC INT4
V3RtcSetVirtNextHopInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTranAreaId,
                          tV3OsRouterId * pDestRtrId, tIp6Addr * pVirtNextHop)
{
    tV3OsArea          *pTransitArea = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    UINT1              *pCurrPtr = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsPrefixInfo     prefixInfo;
    UINT2               u2Counter = 0;
    UINT2               u2NoPrefix = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcSetVirtNextHopInCxt\n");

    pTransitArea = V3GetFindAreaInCxt (pV3OspfCxt, pTranAreaId);

    if (pTransitArea != NULL)
    {
        pDbHashNode = V3RtcSearchIntraAreaPrefixNode (pDestRtrId, 0,
                                                      OSPFV3_REF_ROUTER_LSA,
                                                      pTransitArea);
    }

    if (pDbHashNode != NULL)
    {
        TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
            pCurrPtr = pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE;
            u2NoPrefix = OSPFV3_LGET2BYTE (pCurrPtr);

            pCurrPtr += 10;

            /* Processing all the prefixes */
            for (u2Counter = 0; u2Counter < u2NoPrefix; u2Counter++)
            {
                pCurrPtr = V3UtilExtractPrefixFromLsa (pCurrPtr, &prefixInfo);

                /* If LA bit is set in the prefix, then it is the virtul 
                 * link information */
                if (OSPFV3_IS_LA_BIT_SET (prefixInfo.u1PrefixOpt))
                {
                    OSPFV3_IP6_ADDR_COPY (*pVirtNextHop, prefixInfo.addrPrefix);
                    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                                "EXIT : V3RtcSetVirtNextHopInCxt\n");

                    return OSIX_TRUE;
                }
            }
        }
    }
    return OSIX_FALSE;
}

/*****************************************************************************/
/* Function     : V3RtcCheckAddrRangeFlagInCxt                               */
/*                                                                           */
/* Description  : This function checks and takes the necessary action if the */
/*                address range is not present now.                          */
/*                                                                           */
/* Input        : pV3OspfCxt   : Pointer to Context                          */
/*              : pAddrPrefix  : Pointer to address prefix.                  */
/*              : pNewAreaId   : Pointer to old area Id.                     */
/*              : pOldAreaId   : Pointer to new area Id.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCheckAddrRangeFlagInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pAddrPrefix,
                              tV3OsAreaId * pNewAreaId,
                              tV3OsAreaId * pOldAreaId)
{
    tV3OsAddrRange     *pNewAddrRange = NULL;
    tV3OsAddrRange     *pOldAddrRange = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCheckAddrRangeFlagInCxt\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE))
    {
        /* LSA cannot be Flushed in GR mode */
        return;
    }

    pNewAddrRange = V3AreaFindInternalAddrRngInCxt (pV3OspfCxt, pAddrPrefix,
                                                    pNewAreaId);
    pOldAddrRange = V3AreaFindInternalAddrRngInCxt (pV3OspfCxt, pAddrPrefix,
                                                    pOldAreaId);

    if ((pOldAddrRange != NULL) && (pOldAddrRange->u4RtCount == 0)
        && (pNewAddrRange == NULL))
    {
        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (V3UtilAreaIdComp (pArea->areaId, *pOldAreaId) == OSPFV3_EQUAL)
            {
                continue;
            }

            pLsaInfo = V3LsuSearchDatabase (OSPFV3_INTER_AREA_PREFIX_LSA,
                                            &pOldAddrRange->linkStateId,
                                            &(pV3OspfCxt->rtrId), NULL, pArea);
            if (pLsaInfo != NULL)
            {
                V3AgdFlushOut (pLsaInfo);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCheckAddrRangeFlagInCxt\n");
}

/***************************************************************************/
/* Function     : V3RtcCheckAndGenAggrLsaInCxt                             */
/*                                                                         */
/* Description  : This Function checks and generate the aggregated LSA     */
/*                into the other areas if the cost of the route entry      */
/*                changes.                                                 */
/*                                                                         */
/* Input        : pV3OspfCxt      :  pointer to the Context                */
/*                pRtEntry        :  pointer to the RtEntry                */
/*                u1Status        :  Status indicating whether the route   */
/*                                   entry information is created/updated/ */
/*                                   deleted information.                  */
/*                                                                         */
/* Output       : None                                                     */
/*                                                                         */
/* Returns      : Returns OSIX_TRUE if the route is aggregated else        */
/*                Returns OSIX_FALSE                                       */
/***************************************************************************/
PRIVATE UINT1
V3RtcCheckAndGenAggrLsaInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsRtEntry * pRtEntry, UINT1 u1Status)
{
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OsArea          *pArea = NULL;
    UINT1               u1PrefixLength = 0;
    UINT1               u1TranAreaPres = OSIX_FALSE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCheckAndGenAggrLsaInCxt\n");
    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA cannot be generated in GR mode */
        return OSIX_TRUE;
    }

    pPath = OSPFV3_GET_PATH (pRtEntry);

    if (pPath == NULL)
    {
        gu4V3RtcCheckAndGenAggrLsaInCxtFail++;
        return OSIX_FALSE;
    }

    if ((u1Status != OSPFV3_DELETED)
        && (pPath->u1PathType != OSPFV3_INTRA_AREA))
    {
        gu4V3RtcCheckAndGenAggrLsaInCxtFail++;
        return OSIX_FALSE;
    }

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pPath->areaId))) == NULL)
    {
        gu4V3RtcCheckAndGenAggrLsaInCxtFail++;
        return OSIX_FALSE;
    }

    TMO_SLL_Scan (&(pArea->type3AggrLst), pAddrRange, tV3OsAddrRange *)
    {
        if (pAddrRange->areaAggStatus == OSPFV3_INVALID)
        {
            continue;
        }

        u1PrefixLength = pAddrRange->u1PrefixLength;

        if ((pRtEntry->u1PrefixLen >= u1PrefixLength) &&
            (V3UtilIp6PrefixComp (&pRtEntry->destRtPrefix, &pAddrRange->ip6Addr,
                                  u1PrefixLength) == OSPFV3_EQUAL))
        {
            if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
            {
                if (u1Status == OSPFV3_CREATED)
                {
                    pAddrRange->u4RtCount++;

                    if ((pAddrRange->metric.u4Metric ==
                         OSPFV3_LS_INFINITY_24BIT)
                        || (pAddrRange->metric.u4Metric < pPath->u4Cost))
                    {
                        pAddrRange->metric.u4Metric = pPath->u4Cost;
                        if (pAddrRange->u1AggrEffect !=
                            OSPFV3_DO_NOT_ADVERTISE_MATCHING)
                        {
                            u1TranAreaPres = (UINT1) V3GenerateAggLsa
                                (pArea, pAddrRange);
                        }
                    }

                }
                else
                {

                    /* If the new route entry which is falling in the address range 
                       is having more cost, then condensed summary needs to be 
                       regenerated */

                    if (pAddrRange->metric.u4Metric != pPath->u4Cost)
                    {
                        pAddrRange->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
                        pAddrRange->u4RtCount = 0;
                        V3RtcFindUpdateAddrRangeCost (pArea, pAddrRange);

                        if (pAddrRange->u1AggrEffect !=
                            OSPFV3_DO_NOT_ADVERTISE_MATCHING)
                        {
                            u1TranAreaPres = (UINT1) V3GenerateAggLsa
                                (pArea, pAddrRange);
                        }
                    }
                }
            }
            else
            {
                pAddrRange->u4RtCount--;

                /* If no routes are falling in the address range, then flush 
                   summary LSAs generated */
                if (pAddrRange->u4RtCount == 0)
                {
                    V3RtcFlushOutAggrLsa (pArea, pAddrRange);
                    pAddrRange->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
                }
                else if ((pAddrRange->metric.u4Metric == pPath->u4Cost) ||
                         (pPath->u1PathType == OSPFV3_INTER_AREA))
                {
                    /* Need to see any other route entry is falling in the range
                       is available or not 
                       1. If available, then regenerate the LSA
                       2. Else flushout the condensed summary LSA */
                    pAddrRange->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
                    pAddrRange->u4RtCount = 0;
                    V3RtcFindUpdateAddrRangeCost (pArea, pAddrRange);
                    if (pAddrRange->u1AggrEffect !=
                        OSPFV3_DO_NOT_ADVERTISE_MATCHING)
                    {
                        u1TranAreaPres = (UINT1) V3GenerateAggLsa
                            (pArea, pAddrRange);
                    }
                }
            }
            break;
        }
    }

    /* Route entry is not falling into any address range */
    if (pAddrRange == NULL)
    {
        /* Indicating that the route is not falling in any address range */
        gu4V3RtcCheckAndGenAggrLsaInCxtFail++;
        return OSIX_FALSE;
    }

    if (pAddrRange->u1AggrEffect == OSPFV3_DO_NOT_ADVERTISE_MATCHING)
    {
        /* Indicating that the route need not be advertised indivisually to other 
           areas as the route is falling in address range */
        return OSIX_TRUE;
    }

    if (u1TranAreaPres == OSIX_TRUE)
    {
        gu4V3RtcCheckAndGenAggrLsaInCxtFail++;
        return OSIX_FALSE;
    }
    else
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtcCheckAndGenAggrLsaInCxt\n");

        return OSIX_TRUE;
    }
}

/***************************************************************************/
/* Function     : V3RtcFindUpdateAddrRangeCost                             */
/*                                                                         */
/* Description  : This Function update the cost of the aggregated entry    */
/*                                                                         */
/* Input        : pArea     :  Pointer to the area in which the address    */
/*                             range is configured.                        */
/*                pAddrRange:  Pointer to the address range.               */
/*                                                                         */
/* Output       : None                                                     */
/*                                                                         */
/* Returns      : None                                                     */
/***************************************************************************/
PRIVATE VOID
V3RtcFindUpdateAddrRangeCost (tV3OsArea * pArea, tV3OsAddrRange * pAddrRange)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tInputParams        inParams;
    VOID               *pLeafNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    tIp6Addr           *pIp6Addr = NULL;
    UINT1               u1PrefixLength = 0;

    VOID               *pTempPtr = NULL;

    pIp6Addr = &(pAddrRange->ip6Addr);
    u1PrefixLength = pAddrRange->u1PrefixLength;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcFindUpdateAddrRangeCost\n");

    inParams.pRoot = pArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        pPath = OSPFV3_GET_PATH (pRtEntry);
        if ((pPath != NULL) &&
            (V3UtilAreaIdComp (pPath->areaId, pArea->areaId) == OSPFV3_EQUAL) &&
            (OSPFV3_GET_PATH_TYPE (pRtEntry) == OSPFV3_INTRA_AREA))
        {
            /* If the route entry is not falling in the range, then consider 
               the next route entry */
            if (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                     pIp6Addr, u1PrefixLength) == OSPFV3_EQUAL)
            {
                if ((pAddrRange->metric.u4Metric == OSPFV3_LS_INFINITY_24BIT) ||
                    (pAddrRange->metric.u4Metric < pPath->u4Cost))
                {
                    pAddrRange->metric.u4Metric = pPath->u4Cost;
                }
                pAddrRange->u4RtCount++;
            }
        }                        /* End TMO_SLL_Scan of pArea */
        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
        au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcFindUpdateAddrRangeCost\n");
    return;
}

/************************************************************************/
/* Function     : V3RtcFlushOutAggrLsa                                  */
/*                                                                      */
/* Description  : This Function flush out the aggregated LSA from all   */
/*                other areas.                                          */
/*                                                                      */
/* Input        : pArea     :  Pointer to the area in which the address */
/*                             range is configured.                     */
/*                pAddrRange:  Pointer to the address range.            */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns     : None                                                   */
/************************************************************************/
PUBLIC VOID
V3RtcFlushOutAggrLsa (tV3OsArea * pArea, tV3OsAddrRange * pAddrRng)
{
    tV3OsArea          *pLstArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcFlushOutAggrLsa\n");

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        return;
    }

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea, tV3OsArea *)
    {
        if ((V3UtilAreaIdComp (pArea->areaId,
                               pLstArea->areaId) != OSPFV3_EQUAL) &&
            ((pLstArea->u4AreaType == OSPFV3_NORMAL_AREA) ||
             ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
              (pLstArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY))))
        {
            if ((OSPFV3_IS_AREA_TRANSIT_CAPABLE (pLstArea)) &&
                (V3UtilAreaIdComp (pArea->areaId, OSPFV3_BACKBONE_AREAID)
                 == OSPFV3_EQUAL))
            {
                /* Backbone Network area not condensed in Transit Area */
                continue;
            }

            V3RtcFlushOutSummaryLsa (pLstArea, OSPFV3_INTER_AREA_PREFIX_LSA,
                                     &(pAddrRng->linkStateId));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcFlushOutAggrLsa\n");
}

/*****************************************************************************/
/* Function     : V3RtcProcessUnreachableRoutesInCxt                         */
/*                                                                           */
/* Description  : Mark all the routes as not found, so that useful for       */
/*                processing unreachable routes at the end of route          */
/*                calculation.                                               */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessUnreachableRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsRtEntry       *pTempRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTmpNode = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;
    UINT4               u4CurrentTime = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessUnreachableRoutesInCxt\n");

    /* Mark all ASBR route entries as not found */
    OSPFV3_DYNM_SLL_SCAN (&(pV3OspfCxt->ospfV3RtTable.routesList),
                          pLstNode, pTmpNode, tTMO_SLL_NODE *)
    {
        pRtEntry = OSPFV3_GET_BASE_PTR (tV3OsRtEntry, nextRtEntryNode,
                                        pLstNode);
        if (pRtEntry->u1DestType == OSPFV3_DEST_AS_BOUNDARY)
        {
            if (pRtEntry->u1Flag == OSPFV3_NOT_FOUND)
            {
                if (OSPFV3_GET_PATH_TYPE (pRtEntry) != OSPFV3_INTRA_AREA)
                {
                    V3RtcProcessUnreachableRouteInCxt (pV3OspfCxt, pRtEntry);
                    V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pRtEntry);
                    V3RtcFreeRtEntry (pRtEntry);
                }
            }
            else
            {
                pRtEntry->u1Flag = OSPFV3_NOT_FOUND;
                pPath = OSPFV3_GET_PATH (pRtEntry);
                if ((pPath != NULL) && (pPath->u1HopCount == 0))
                {
                    V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pRtEntry);
                    V3RtcFreeRtEntry (pRtEntry);
                }
            }

            /* Check whether we need to relinquish the route calculation
             * to do other OSPFv3 processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
    }

    /* Updating the network route entries */
    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "TrieGetFirstNode Failure\n");
        return;
    }
    pRtEntry = (tV3OsRtEntry *) pTempPtr;
    while (pRtEntry != NULL)
    {
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;

        if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                             &pTempPtr, (VOID **)
                             &(inParams.pLeafNode)) != TRIE_SUCCESS)
        {
            pTempRtEntry = NULL;
        }
        pTempRtEntry = (tV3OsRtEntry *) pTempPtr;
        if (pRtEntry->u1Flag == OSPFV3_NOT_FOUND)
        {
            if (OSPFV3_GET_PATH_TYPE (pRtEntry) != OSPFV3_INTRA_AREA)
            {
                V3RtcProcessUnreachableRouteInCxt (pV3OspfCxt, pRtEntry);
                V3RtmDeleteRtInCxt (pV3OspfCxt, pRtEntry);
                V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pRtEntry);
                V3RtcFreeRtEntry (pRtEntry);
            }
        }
        else
        {
            pRtEntry->u1Flag = OSPFV3_NOT_FOUND;
            pPath = OSPFV3_GET_PATH (pRtEntry);
            if ((pPath != NULL) && (pPath->u1HopCount == 0))
            {
                V3RtcDeleteRtEntryInCxt (pV3OspfCxt, pRtEntry);
                V3RtcFreeRtEntry (pRtEntry);
            }
        }
        pRtEntry = pTempRtEntry;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcessUnreachableRoutesInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcSetRtTimerInCxt                                       */
/*                                                                           */
/* Description  : Function to start the timer to do the route calculation.   */
/*                                                                           */
/* Input        : pV3OspfCxt   -   Context pointer                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcSetRtTimerInCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4RtInterval = 0;
    UINT4               u4CurrTime = 0;
    INT4                i4Temp = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcSetRtTimerInCxt\n");

    if (pV3OspfCxt->admnStat == OSPFV3_DISABLED)
    {
        return;
    }

    if ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY) &&
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus
         != OSPFV3_BULK_UPDT_COMPLETED))
    {
        /* No need to start the route calculation timer during bulk
         * update process in standby node, The route calculation will
         * be started after the bulk update is completed
         */
        return;
    }

    u4CurrTime = OsixGetSysUpTime ();

    i4Temp = (INT4) (pV3OspfCxt->u4LastCalTime) - (INT4) (u4CurrTime) +
        (INT4) (pV3OspfCxt->u4SpfHoldTimeInterval);

    if (i4Temp < 0)
    {
        u4RtInterval = pV3OspfCxt->u4SpfInterval;
    }
    else if ((UINT4) i4Temp > pV3OspfCxt->u4SpfInterval)
    {
        u4RtInterval = i4Temp;
    }
    else
    {
        u4RtInterval = pV3OspfCxt->u4SpfInterval;
    }

    pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_TRUE;
    V3TmrSetTimer (&(pV3OspfCxt->runRtTimer), OSPFV3_RUN_RT_TIMER,
                   (OSPFV3_NO_OF_TICKS_PER_SEC * u4RtInterval));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcSetRtTimerInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateLocalRoutes                                  */
/*                                                                           */
/* Description  : This function calculates all the local routes of the       */
/*                interface.                                                 */
/*                                                                           */
/* Input        : pInterface - Pointer to interface                          */
/*                u1Status   - Indicating the deletion/updation/creation     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateLocalRoutes (tV3OsInterface * pInterface, UINT1 u1Status)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateLocalRoutes\n");

    tV3OsPrefixNode    *pPrefixNode = NULL;

    TMO_SLL_Scan (&(pInterface->ip6AddrLst), pPrefixNode, tV3OsPrefixNode *)
    {
        V3RtcCalculateLocalRoute (pInterface, pPrefixNode, u1Status);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateLocalRoutes\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateLocalRoute                                   */
/*                                                                           */
/* Description  : This function calculates the route entry from the prefix   */
/*                information.                                               */
/*                                                                           */
/* Input        : pInterface - Pointer to interface                          */
/*                pPrefixNode - Prefix information for which the route is    */
/*                to be calculated.                                          */
/*                u1Status   - Indicating the deletion/updation/creation     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateLocalRoute (tV3OsInterface * pInterface,
                          tV3OsPrefixNode * pPrefixNode, UINT1 u1Status)
{
    tV3OsPath          *pNewPath = NULL;
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pOldRtEntry;
    UINT4               u4Metric = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateLocalRoute\n");

    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
    newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;

    TMO_SLL_Init (&(newRtEntry.pathLst));

    if (pInterface->u1NetworkType == OSPFV3_IF_PTOMP)
    {
        newRtEntry.u1PrefixLen = OSPFV3_MAX_PREFIX_LEN;
    }
    else
    {
        newRtEntry.u1PrefixLen = pPrefixNode->prefixInfo.u1PrefixLength;
    }

    V3UtilIp6PrefixCopy (&newRtEntry.destRtPrefix,
                         &pPrefixNode->prefixInfo.addrPrefix,
                         newRtEntry.u1PrefixLen);

    newRtEntry.u1RtType = OSPFV3_DIRECT;
    pOldRtEntry =
        V3RtcFindRtEntryInCxt
        (pInterface->pArea->pV3OspfCxt,
         (VOID *) &(newRtEntry.destRtPrefix),
         newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);

    if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
    {
        if (newRtEntry.u1PrefixLen != OSPFV3_MAX_PREFIX_LEN)
        {
            u4Metric = pInterface->u4IfMetric;
        }

        if ((pNewPath = V3RtcCreatePath (&(pInterface->pArea->areaId),
                                         OSPFV3_INTRA_AREA,
                                         u4Metric, 0)) == NULL)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Path creation fails\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Path creation fails\n");
            return;
        }

        MEMSET (&(pNewPath->aNextHops[0].nbrIpv6Addr), 0, sizeof (tIp6Addr));
        OSPFV3_RTR_ID_COPY (pNewPath->aNextHops[0].advRtrId,
                            pInterface->pArea->pV3OspfCxt->rtrId);
        pNewPath->aNextHops[0].pInterface = pInterface;
        pNewPath->u1HopCount = 1;

        V3RtcAddPath (&newRtEntry, pNewPath);
    }
    else
    {
        if ((pOldRtEntry != NULL)
            && (TMO_SLL_Count (&(pOldRtEntry->lsaLst)) != 0))
        {
            pNewPath =
                V3RtcCreatePath (&(pInterface->pArea->areaId),
                                 OSPFV3_INTRA_AREA, 0xffff, 0);

            if (pNewPath == NULL)
            {
                return;
            }
            V3RtcCalculateIntraPrefRoute (pOldRtEntry,
                                          pNewPath, NULL,
                                          pInterface->pArea,
                                          &pPrefixNode->prefixInfo);

            if (pNewPath->u4Cost != 0xffff)
            {
                V3RtcAddPath (&newRtEntry, pNewPath);
                newRtEntry.u1RtType = OSPFV3_INDIRECT;
            }
            else
            {
                OSPFV3_PATH_FREE (pNewPath);
                pNewPath = NULL;
            }
        }
    }
    V3RtcProcessRtEntryChangesInCxt (pInterface->pArea->pV3OspfCxt,
                                     &newRtEntry, pOldRtEntry);
    KW_FALSEPOSITIVE_FIX (pNewPath);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateLocalRoute\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateAllLocalHostRouteInCxt                       */
/*                                                                           */
/* Description  : This function calculates the route entry for the all       */
/*                hosts in context                                           */
/*                                                                           */
/* Input        : pV3OspfCxt - Pointer to Context                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
V3RtcCalculateAllLocalHostRouteInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsHost          *pHost = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateAllLocalHostRouteInCxt\n");

    pHost = (tV3OsHost *) RBTreeGetFirst (pV3OspfCxt->pHostRBRoot);

    while (pHost != NULL)
    {
        V3RtcCalculateLocalHostRouteInCxt (pV3OspfCxt,
                                           &(pHost->hostIp6Addr),
                                           &(pHost->pArea->areaId),
                                           pHost->u4HostMetric, OSPFV3_CREATED);

        pHost = (tV3OsHost *) RBTreeGetNext (pV3OspfCxt->pHostRBRoot,
                                             (tRBElem *) pHost, NULL);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateAllLocalHostRouteInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateLocalHostRouteInCxt                          */
/*                                                                           */
/* Description  : This function calculates the route entry for the local     */
/*                host                                                       */
/*                                                                           */
/* Input        : pV3OspfCxt - Pointer to Context                            */
/*                pHostAddr  - Pointer to host address.                      */
/*                pAreaId    - Pointer to the areaId to which the route      */
/*                             belongs.                                      */
/*                u4HostMetric - Host route metric value.                    */
/*                u1Status   - Indicating the deletion/updation/creation     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateLocalHostRouteInCxt (tV3OspfCxt * pV3OspfCxt,
                                   tIp6Addr * pHostAddr,
                                   tV3OsAreaId * pAreaId,
                                   UINT4 u4HostMetric, UINT1 u1Status)
{
    tV3OsPath          *pNewPath = NULL;
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pOldRtEntry;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateLocalHostRouteInCxt\n");

    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
    newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;

    TMO_SLL_Init (&(newRtEntry.pathLst));

    newRtEntry.u1PrefixLen = OSPFV3_MAX_PREFIX_LEN;

    OSPFV3_IP6_ADDR_COPY (newRtEntry.destRtPrefix, *pHostAddr);

    newRtEntry.u1RtType = OSPFV3_DIRECT;

    pOldRtEntry =
        V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *) &(newRtEntry.destRtPrefix),
                               newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);

    if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
    {
        if ((pNewPath = V3RtcCreatePath (pAreaId,
                                         OSPFV3_INTRA_AREA,
                                         u4HostMetric, 0)) == NULL)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Path creation fails\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                        "Path creation fails\n");
            return;
        }

        MEMSET (&(pNewPath->aNextHops[0].nbrIpv6Addr), 0, sizeof (tIp6Addr));
        OSPFV3_RTR_ID_COPY (pNewPath->aNextHops[0].advRtrId, pV3OspfCxt->rtrId);
        pNewPath->aNextHops[0].pInterface = NULL;
        pNewPath->u1HopCount = 1;

        V3RtcAddPath (&newRtEntry, pNewPath);
    }
    V3RtcProcessRtEntryChangesInCxt (pV3OspfCxt, &newRtEntry, pOldRtEntry);
    KW_FALSEPOSITIVE_FIX (pNewPath);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateLocalHostRouteInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcChkAndFlushFnEqvLsa                                   */
/*                                                                           */
/* Description  : This function Check for Functionality Equivalent Ext Lsa   */
/*                and flush the LSA when we learn the route entry from other */
/*                router.                                                    */
/*                                                                           */
/* Input        : pPath  - Pointer to Path.                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcChkAndFlushFnEqvLsa (tV3OsPath * pPath)
{
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsArea          *pNssaArea = NULL;
    tV3OsLsaInfo       *pNssaLsaInfo = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcChkAndFlushFnEqvLsa\n");

    if (pPath->pLsaInfo != NULL)
    {
        if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
            (pPath->pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState
             != OSPFV3_GR_NONE))
        {
            /* LSA cannot be Flushed in GR mode */
            return;
        }
        if (pPath->pLsaInfo->u1FnEqvlFlag == OSIX_TRUE)
        {
            u1PrefixLen =
                *((UINT1 *) (pPath->pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
            pAddrPrefix = (pPath->pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);
            pExtRoute = V3ExtrtFindRouteInCxt
                (pPath->pLsaInfo->pV3OspfCxt,
                 (tIp6Addr *) (VOID *) pAddrPrefix, u1PrefixLen);
            if (pPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
            {
                if ((pExtRoute != NULL) && (pExtRoute->pLsaInfo != NULL))
                {
                    V3AgdFlushOut (pExtRoute->pLsaInfo);
                }
            }
            else if ((pPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA)
                     && (pExtRoute != NULL))
            {

                pNssaArea = V3GetFindAreaInCxt (pPath->pLsaInfo->pV3OspfCxt,
                                                &(pPath->areaId));
                pNssaLsaInfo =
                    V3LsuSearchDatabase (pPath->pLsaInfo->lsaId.u2LsaType,
                                         &pExtRoute->linkStateId,
                                         &pPath->pLsaInfo->pV3OspfCxt->rtrId,
                                         NULL, pNssaArea);
                if (pNssaLsaInfo != NULL)
                {
                    V3AgdFlushOut (pNssaLsaInfo);
                }

            }
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcChkAndFlushFnEqvLsa\n");

}

/*****************************************************************************/
/* Function     : V3RtcChkAndGenFnEqvLsa                                     */
/*                                                                           */
/* Description  : This function Check for Functionality Equivalent Ext Lsa   */
/*                and Generate the LSA when we loose the route entry from    */
/*               other router.                                               */
/*                                                                           */
/* Input        : pPath  - Pointer to Path.                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcChkAndGenFnEqvLsa (tV3OsPath * pPath)
{

    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcChkAndGenFnEqvLsa\n");

    if ((pPath->pLsaInfo != NULL) &&
        (pPath->pLsaInfo->u1FnEqvlFlag == OSIX_TRUE))
    {
        u1PrefixLen =
            *((UINT1 *) (pPath->pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
        pAddrPrefix = (pPath->pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);
        pExtRoute = V3ExtrtFindRouteInCxt (pPath->pLsaInfo->pV3OspfCxt,
                                           (tIp6Addr *) (VOID *) pAddrPrefix,
                                           u1PrefixLen);
        if (pExtRoute != NULL)
        {
            V3RagAddRouteInAggAddrRangeInCxt (pPath->pLsaInfo->pV3OspfCxt,
                                              pExtRoute);
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcChkAndGenFnEqvLsa\n");

}

/*---------------------------------------------------------------------------*/
/*                       End of the file o3rtc.c                             */
/*---------------------------------------------------------------------------*/
