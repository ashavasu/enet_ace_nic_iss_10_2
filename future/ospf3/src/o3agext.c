/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agext.c,v 1.8 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to aggregation of 
 *         external address range.
 *
 *******************************************************************/
#include "o3inc.h"

/************************************************************************/
/*                                                                      */
/* Function        : V3RagUpdtBboneRngFrmExtLstInCxt                    */
/*                                                                      */
/* Description     : Updates Bkbone Range cost/type for routes          */
/*                   maintained in its link lst                         */
/*                                                                      */
/* Input           : pV3OspfCxt  -Pointer to Context                    */
/*                   pAsExtRange -Pointer to new Range                  */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagUpdtBboneRngFrmExtLstInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tV3OsAsExtAddrRange * pAsExtRange)
{
    tV3OsArea          *pArea = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagUpdtBboneRngFrmExtLst\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));

    if (pArea == NULL)
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    " BackBone area not existing \n");
        return;
    }

    (pAsExtRange->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
    (pAsExtRange->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;

    TMO_SLL_Scan (&(pAsExtRange->extRtLst), pExtRtNode, tTMO_SLL_NODE *)
    {
        pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                         nextExtRouteInRange, pExtRtNode);

        V3RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagUpdtBboneRngFrmExtLst\n");
    return;

}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagFlushBkBoneRangeInCxt                            */
/*                                                                      */
/* Description  : Flushes the Type 5 LSA originated for bkbone range    */
/*                                                                      */
/* Input        : pV3OspfCxt  -Pointer to Context                       */
/*                pAsExtRange -Pointer to Range                         */
/*                pExtRoute - Pointer to Ext Rt                         */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagFlushBkBoneRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                            tV3OsAsExtAddrRange * pAsExtRange,
                            tV3OsExtRoute * pExtRoute)
{
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlushBkBoneRange \n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));

    /* If area does not exist   */
    if (pArea == NULL)
    {
        return;
    }

    /* If areaId is not 0.0.0.0 OR area Type not NORMAL */
    if ((V3UtilAreaIdComp (pArea->areaId, OSPFV3_BACKBONE_AREAID)
         != OSPFV3_EQUAL) || (pArea->u4AreaType != OSPFV3_NORMAL_AREA))
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    "Area Id is not 0.0.0.0 or area Type is not normal\n");
        return;
    }

    /* Case will occur when As Ext Range is deleted */
    if (pExtRoute == NULL)
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_DENY_ALL:
            case OSPFV3_RAG_DO_NOT_ADVERTISE:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                            " No flushing required  \n");
                break;

            case OSPFV3_RAG_ADVERTISE:
            case OSPFV3_RAG_ALLOW_ALL:
                /* In case of AS external LSA, the last parameter (pArea)
                 * is passed to get the context pointer from the Area
                 */
                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pArea);

                if ((pLsaInfo != NULL) &&
                    (pLsaInfo->u1TrnsltType5 == OSPFV3_REDISTRIBUTED_TYPE5))
                {
                    OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst,
                                          pAsExtRange->ip6Addr);
                    NetRtInfo.u1Prefixlen = pAsExtRange->u1PrefixLength;
                    MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
                    NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
                    NetRtInfo.u4RowStatus = ACTIVE;
                    NetRtInfo.u4Index = 0;
                    NetRtInfo.u1NullFlag = 0;

                    if (NetIpv6LeakRoute (NETIPV6_DELETE_ROUTE, &NetRtInfo) ==
                        NETIPV6_FAILURE)
                    {
                        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                                    " NetIpv6LeakRoute Deletion Failed. \n");
                    }

                    V3AgdFlushOut (pLsaInfo);
                }

                break;

            default:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                            "BackBone Range - Invalid Range effect  \n");

                break;
        }
    }

    /* Case will occur when Ext Rt is deleted from
     * range it falls in
     */
    else
    {
        switch (pAsExtRange->u1RangeEffect)
        {
            case OSPFV3_RAG_DENY_ALL:
            case OSPFV3_RAG_DO_NOT_ADVERTISE:

                /* Ext Rt is removed from link list maintained in 
                   in range
                 */
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));
                break;

            case OSPFV3_RAG_ADVERTISE:
            case OSPFV3_RAG_ALLOW_ALL:

                /* Ext Rt is removed from link list maintained in 
                   in range
                 */
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                V3RagUpdtBboneRngFrmExtLstInCxt (pArea->pV3OspfCxt,
                                                 pAsExtRange);

                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                         &(pAsExtRange->linkStateId),
                                         &(pArea->pV3OspfCxt->rtrId),
                                         NULL, pArea);

                if (pLsaInfo == NULL)
                {
                    return;
                }

                if (pLsaInfo->u1TrnsltType5 != OSPFV3_REDISTRIBUTED_TYPE5)
                {
                    return;
                }
                /* If currently no Ext Rt falls in Rng - Flush it */
                if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
                {
                    V3RagFlushBkBoneRangeInCxt (pArea->pV3OspfCxt, pAsExtRange,
                                                NULL);
                    return;
                }

                if (V3RagCompRngAndLsa (pAsExtRange, pLsaInfo) == OSIX_TRUE)
                {
                    /* Re-Originate Type 5 LSA      */
                    V3SignalLsaRegenInCxt (pArea->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pLsaInfo->pLsaDesc);

                }
                break;

            default:
                OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                            " BackBone Range - Invalid Range effect  \n");
                break;
        }

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlushBkBoneRange\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagGenExtLsaInCxt                                */
/*                                                                      */
/* Description     : Checks whether Ext Lsa can be generated            */
/*                                                                      */
/* Input           : pV3OspfCxt   -  Context pointer                    */
/*                   u1Type       - Internal LSA Type                   */
/*                   pPtr         - Struct associated with LSA          */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : OSIX_TRUE - Generate Ext Lsa                       */
/*                   OSIX_FALSE - Do Not generate                       */
/*                                                                      */
/************************************************************************/

PUBLIC UINT1
V3RagGenExtLsaInCxt (tV3OspfCxt * pV3OspfCxt, UINT2 u2Type, UINT1 *pPtr)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAsExtAddrRange *pAsExtRange = NULL;
    UINT1               u1PrefixLength = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagGenExtLsa\n");

    if ((pV3OspfCxt->bAsBdrRtr == OSIX_FALSE) &&
        ((pV3OspfCxt->bAreaBdrRtr == OSIX_FALSE) ||
         (pV3OspfCxt->u4NssaAreaCount == 0)))
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                    "Router is not an ASBR\n");
        gu4V3RagGenExtLsaInCxtFail++;
        return OSIX_FALSE;
    }

    if (u2Type == OSPFV3_AS_EXT_LSA)
    {
        pExtRoute = (tV3OsExtRoute *) (VOID *) pPtr;
        u1PrefixLength = pExtRoute->u1PrefixLength;
        if (V3IsEligibleToGenAseLsaInCxt (pV3OspfCxt, pExtRoute)
            == OSIX_FAILURE)
        {

            OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                        "cant generate ASE LSA, since"
                        "another one exists with same functionality\n");
            gu4V3RagGenExtLsaInCxtFail++;
            return OSIX_FALSE;
        }
    }
    else
    {
        pAsExtRange = (tV3OsAsExtAddrRange *) (VOID *) pPtr;
        u1PrefixLength = pAsExtRange->u1PrefixLength;
    }

    if (u1PrefixLength != 0)
    {
        if (pV3OspfCxt->bOverflowState == OSIX_TRUE)
        {

            OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                        "no non-default ASE LSA is"
                        "originated in overflow state\n");
            gu4V3RagGenExtLsaInCxtFail++;
            return OSIX_FALSE;
        }
        if ((OSPFV3_IS_EXT_LSDB_SIZE_LIMITED (pV3OspfCxt)) &&
            (OSPFV3_GET_NON_DEF_ASE_LSA_COUNT (pV3OspfCxt)
             == (pV3OspfCxt->i4ExtLsdbLimit - 1)))
        {

            OSPFV3_TRC (OSPFV3_LSU_TRC, pV3OspfCxt->u4ContextId,
                        "Router enters the overflow state"
                        "when trying to orgnte a non-def lsa\n");
            V3RtrEnterOverflowStateInCxt (pV3OspfCxt);
            gu4V3RagGenExtLsaInCxtFail++;
            return OSIX_FALSE;
        }
    }
    else
    {
        /* Set the Flag indicating that Default AS_EXT lsa is Present */
        pV3OspfCxt->bDefaultAseLsaPresenc = OSIX_TRUE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagGenExtLsa\n");
    return OSIX_TRUE;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagInsertRouteInBkboneAggRngInCxt                */
/*                                                                      */
/* Description     : Inserts the Ext Rt in SLL maintained in the list   */
/*                                                                      */
/* Input           : pV3OspfCxt  -Pointer to Context                    */
/*                   pAsExtRange -Pointer to Range                      */
/*                   pExtRoute -Pointer to Ext Rt                       */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/*                                                                      */
/* Returns         : None                                               */
/*                                                                      */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagInsertRouteInBkboneAggRngInCxt (tV3OspfCxt * pV3OspfCxt,
                                     tV3OsAsExtAddrRange * pAsExtRange,
                                     tV3OsExtRoute * pExtRoute)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagInsertRouteInBkboneAggRng\n");

    /* Check if Ext Rt already present in List */
    if ((TMO_SLL_Find (&(pAsExtRange->extRtLst),
                       &(pExtRoute->nextExtRouteInRange))) ==
        OSPFV3_RAG_NO_CHNG)
    {
        /* Add Ext Rt in List */
        TMO_SLL_Add (&(pAsExtRange->extRtLst),
                     &(pExtRoute->nextExtRouteInRange));
        pExtRoute->pAsExtAddrRange = pAsExtRange;
    }
    else
    {
        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                    "External Route already present in Range List\n");
    }
    V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pAsExtRange);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RagInsertRouteInBkboneAggRng\n");
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagProcessBkBoneRangeInCxt                          */
/*                                                                      */
/* Description  : Processes the Bkbone AS Ext Range                     */
/*                Originates Type 5 LSA based on range.                 */
/*                                                                      */
/*                                                                      */
/* Input        : pV3OspfCxt  - Context Pointer                         */
/*                pAsExtRange - Pointer to External Range               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/*                                                                      */
/* Returns      : None                                                  */
/*                                                                      */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagProcessBkBoneRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsAsExtAddrRange * pAsExtRange)
{

    tV3OsLsaInfo       *pType5LsaInfo = NULL;
    tV3OsLsaInfo       *pEquLsaInfo = NULL;
    UINT1               u1LsaFallInRng = OSPFV3_NOT_EQUAL;
    UINT1               u1Change = OSIX_FALSE;
    UINT1               u1EquLsa = OSIX_FALSE;
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;
    tNetIpv6RtInfo      NetRtInfo;

    MEMSET (&NetRtInfo, 0, sizeof (tNetIpv6RtInfo));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagProcessBkBoneRange\n");

    /* If currently no Ext Rt falls in Rng - Flush it */
    if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        V3RagFlushBkBoneRangeInCxt (pV3OspfCxt, pAsExtRange, NULL);
        return;
    }
    if ((pAsExtRange->u1RangeEffect == OSPFV3_RAG_DO_NOT_ADVERTISE) ||
        (pAsExtRange->u1RangeEffect == OSPFV3_RAG_DENY_ALL))
    {
        u1EquLsa = OSIX_TRUE;
    }

    /* SCAN thru Type 5 LSA Lst */
    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashKey)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pV3OspfCxt->pExtLsaHashTable, u4HashKey,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType5LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);

                /* Not Self - originated  : Continue */
                if (V3UtilRtrIdComp ((pType5LsaInfo->lsaId).advRtrId,
                                     pV3OspfCxt->rtrId) != OSPFV3_EQUAL)
                {
                    continue;
                }

                /* Not originated as a result of route import - Continue    */
                if (pType5LsaInfo->u1TrnsltType5 != OSPFV3_REDISTRIBUTED_TYPE5)
                {
                    continue;
                }

                /* If LSA Falls in previous ACTIVE range - continue */
                if (V3RagChkLsaFallInPrvRng (pAsExtRange, pType5LsaInfo)
                    == OSIX_TRUE)
                {
                    continue;
                }

                /* Chk if LSA falls in Range    */
                u1LsaFallInRng = V3RagChkLsaFallInRng (pAsExtRange,
                                                       pType5LsaInfo);

                switch (pAsExtRange->u1RangeEffect)
                {

                    case OSPFV3_RAG_ADVERTISE:
                    case OSPFV3_RAG_ALLOW_ALL:
                        if (u1LsaFallInRng == OSPFV3_GREATER)
                        {
                            V3AgdFlushOut (pType5LsaInfo);
                        }

                        if (u1LsaFallInRng == OSPFV3_EQUAL)
                        {
                            u1EquLsa = OSIX_TRUE;
                            pEquLsaInfo = pType5LsaInfo;
                            u1Change = V3RagCompRngAndLsa (pAsExtRange,
                                                           pType5LsaInfo);
                        }
                        break;

                    case OSPFV3_RAG_DO_NOT_ADVERTISE:
                    case OSPFV3_RAG_DENY_ALL:
                        if ((u1LsaFallInRng == OSPFV3_GREATER) ||
                            (u1LsaFallInRng == OSPFV3_EQUAL))
                        {
                            V3AgdFlushOut (pType5LsaInfo);
                        }
                        break;

                    default:
                        OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                                    " Invalid Range effect \n");
                        return;

                }
            }
        }
    }                            /* Scan Type 5 LSAs */

    /* If LSA matching the range is present.
       Plus there is some update in range cost/Path Type 
     */
    if ((u1EquLsa == OSIX_TRUE) && (u1Change == OSIX_TRUE))
    {
        /* Re-Originate Type 5 LSA      */
        /* LSA should be re-originated using
         * tAsExtAddrRange.
         * Case : LSA equal to the range is
         * present and it is non-aggregated, then
         * pAssoc should point to tAsExtAddrRange
         * from this time onwards. */

        pEquLsaInfo->pLsaDesc->u2InternalLsaType = OSPFV3_COND_AS_EXT_LSA;

        pEquLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pAsExtRange;
        V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                               (UINT1 *) pEquLsaInfo->pLsaDesc);
    }

    /* If currently there is no LSA originated for
       the range
     */

    else if (u1EquLsa == OSIX_FALSE)
    {
        /* Originate new Type 5 LSA */
        if (V3RagGenExtLsaInCxt (pV3OspfCxt, OSPFV3_COND_AS_EXT_LSA,
                                 (UINT1 *) pAsExtRange) == OSIX_TRUE)
        {

            OSPFV3_IP6_ADDR_COPY (NetRtInfo.Ip6Dst, pAsExtRange->ip6Addr);
            NetRtInfo.u1Prefixlen = pAsExtRange->u1PrefixLength;
            MEMSET (&NetRtInfo.NextHop, 0, sizeof (tIp6Addr));
            NetRtInfo.i1Proto = IP6_OSPF_PROTOID;
            NetRtInfo.u4RowStatus = ACTIVE;
            NetRtInfo.u4Index = 0;
            NetRtInfo.u1NullFlag = 1;
            NetRtInfo.u4Metric = 0;
            NetRtInfo.u1Preference = IP6_PREFERENCE_OSPF;

            if (NetIpv6LeakRoute (NETIPV6_ADD_ROUTE, &NetRtInfo) ==
                NETIPV6_FAILURE)
            {
                OSPFV3_TRC (OSPFV3_RAG_TRC, pV3OspfCxt->u4ContextId,
                            " NetIpv6LeakRoute Addition Failed. \n");
            }

            /* For AS external LSA, backbone area pointer is passed as the
             * first parameter. This pointer ir used to retrive the context
             * pointer
             */
            V3GenerateLsa (pV3OspfCxt->pBackbone,
                           OSPFV3_COND_AS_EXT_LSA,
                           &(pAsExtRange->linkStateId),
                           (UINT1 *) pAsExtRange, OSIX_FALSE);

        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagProcessBkBoneRange\n");
    return;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agext.c                      */
/*-----------------------------------------------------------------------*/
