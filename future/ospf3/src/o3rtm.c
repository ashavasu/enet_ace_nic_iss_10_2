/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtm.c,v 1.19 2018/01/25 10:04:16 siva Exp $
 *
 * Description:This file contains procedures for updating Routing Table
 *             and orginating LSAs as par as Extrenal Route entries 
 *             configured through RTM    
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE tV3OsRedistrConfigRouteInfo *V3RtmFindBestRrdConfRtInfoInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pRrdDestPrefix, UINT1 u1PrefixLen));

PRIVATE tV3OsRedistrConfigRouteInfo *V3RtmFindNextBestRrdConfRtInfoInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pRrdDestPrefix, UINT1 u1PrefixLen,
        tV3OsRedistrConfigRouteInfo * pRrdConfRtInfo));

#ifdef ROUTEMAP_WANTED
PRIVATE INT1 V3ApplyInFilterInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                          tNetIpv6RtInfo * pIp6RtInfo));
#endif

#ifdef RRD_WANTED
PRIVATE VOID V3RtmProcessRegAckMsgInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                                tRtm6RegnAckMsg * pRegnAck));

PRIVATE VOID        V3RtmProcessRtUpdateMsgInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pRtInfo));

PRIVATE VOID        V3RtmProcessRtChgNtfMsgInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pRtInfo));
#endif

/*****************************************************************************/
/* Function     : V3RtmUpdateNewRtInCxt                                      */
/*                                                                           */
/* Description  : This function adds IPv6 route entry to the common          */
/*                routing table for each next hop                            */
/*                                                                           */
/* Input        : pV3OspfCxt   : Pointer to OSPFv3 context                   */
/*                pRtEntry     : Pointer to route entry whose details are to */
/*                               be added to common routing table            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtmUpdateNewRtInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{
    tV3OsPath          *pRtPath = NULL;
    UINT1               u1HopCount = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmUpdateNewRtInCxt\n");

    pRtPath = OSPFV3_GET_PATH (pRtEntry);

    if (pRtPath != NULL)
    {
        for (u1HopCount = 0; ((u1HopCount < pRtPath->u1HopCount) &&
                              (u1HopCount < OSPFV3_MAX_NEXT_HOPS));
             u1HopCount++)
        {
            V3RtmLeakRouteInCxt (pV3OspfCxt, pRtEntry, pRtPath,
                                 u1HopCount, NETIPV6_ADD_ROUTE);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmUpdateNewRtInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtmDeleteRtInCxt                                         */
/*                                                                           */
/* Description  : This function deletes IPv6 route entry from the common     */
/*                routing table for each next hop                            */
/*                                                                           */
/* Input        : pV3OspfCxt   : Pointer to OSPFv3 context                   */
/*                pRtEntry     : Pointer to route entry whose details are to */
/*                               be deleted from common routing table        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtmDeleteRtInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry)
{
    tV3OsPath          *pRtPath = NULL;
    UINT1               u1HopCount = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmDeleteRtInCxt\n");

    pRtPath = OSPFV3_GET_PATH (pRtEntry);

    if (pRtPath != NULL)
    {
        for (u1HopCount = 0; ((u1HopCount < pRtPath->u1HopCount) &&
                              (u1HopCount < OSPFV3_MAX_NEXT_HOPS));
             u1HopCount++)
        {
            V3RtmLeakRouteInCxt (pV3OspfCxt, pRtEntry, pRtPath,
                                 u1HopCount, NETIPV6_DELETE_ROUTE);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmDeleteRtInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtmLeakRouteInCxt                                        */
/*                                                                           */
/* Description  : This function updates IPv6 route entry to the common       */
/*                routing table                                              */
/*                                                                           */
/* Input        : pV3OspfCxt   : Pointer to OSPFv3 context                   */
/*                pRtEntry     : Pointer to route entry                      */
/*                pPath        : Pointer to path entry                       */
/*                u1HopIndex   : Hop index                                   */
/*                u1CmdType    : Indicates addition/modification/deletion    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtmLeakRouteInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry,
                     tV3OsPath * pPath, UINT1 u1HopIndex, UINT1 u1CmdType)
{
    tNetIpv6RtInfo      ip6RtInfo;
    tNetIpv6RtInfo      Rmapip6RtInfo;
    tV3OsLeakRoutes     IfLeakRoutes;
    tTMO_SLL_NODE      *pLstNode;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsArea          *pV3OsArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmLeakRouteInCxt\n");
    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* If this router is performing GR,
         * then route updates should not be given to RTM6 */
        return;
    }

    if (pRtEntry->u1RtType == OSPFV3_DIRECT)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "Local routes are not updated\n");
        return;
    }

    if (u1CmdType == NETIPV6_DELETE_ROUTE)
    {
        MEMSET (&IfLeakRoutes, 0, sizeof (tV3OsLeakRoutes));
        if (pPath->aNextHops[u1HopIndex].pInterface != NULL)
        {
            IfLeakRoutes.u4IfIndex =
                pPath->aNextHops[u1HopIndex].pInterface->u4InterfaceId;

            if (RBTreeGet (pV3OspfCxt->IfLeakRouteRBTree, &IfLeakRoutes) !=
                NULL)
            {
                return;
            }
        }
    }

    MEMSET (&ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&Rmapip6RtInfo, 0, sizeof (tNetIpv6RtInfo));

    ip6RtInfo.u4ContextId = pV3OspfCxt->u4ContextId;
    OSPFV3_IP6_ADDR_COPY (ip6RtInfo.Ip6Dst, pRtEntry->destRtPrefix);
    OSPFV3_IP6_ADDR_COPY (ip6RtInfo.NextHop,
                          pPath->aNextHops[u1HopIndex].nbrIpv6Addr);

    if (pPath->aNextHops[u1HopIndex].pInterface != NULL)
    {
        ip6RtInfo.u4Index =
            pPath->aNextHops[u1HopIndex].pInterface->u4InterfaceId;
    }
    else
    {
        return;
    }

    if (pPath->u1PathType != OSPFV3_TYPE_2_EXT)
    {
        ip6RtInfo.u4Metric = pPath->u4Cost;
    }
    else
    {
        ip6RtInfo.u4Metric = pPath->u4Type2Cost;
    }

    if ((pV3OsArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pPath->areaId))) == NULL)
    {
        return;
    }
    if ((pPath->u1PathType == OSPFV3_TYPE_1_EXT)
        || (pPath->u1PathType == OSPFV3_TYPE_2_EXT))
    {
        if (pV3OsArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            /* When the area type is NSSA area then the pathtype is
             * incremented by 2 to inform ip that the ospf route is
             * type NSSA external type1/type2 route.
             */
            ip6RtInfo.u1MetricType = pPath->u1PathType + 2;
        }
        else
        {
            ip6RtInfo.u1MetricType = pPath->u1PathType;
        }
    }
    else
    {
        ip6RtInfo.u1MetricType = pPath->u1PathType;
    }

    if (u1CmdType == NETIPV6_DELETE_ROUTE)
    {
        ip6RtInfo.u4RowStatus = IP6FWD_DESTROY;
    }
    else
    {
        ip6RtInfo.u4RowStatus = IP6FWD_ACTIVE;
    }

    if ((pPath->u1PathType == OSPFV3_INTRA_AREA) ||
        (pPath->u1PathType == OSPFV3_INTER_AREA))
    {
        ip6RtInfo.u4RouteTag = 0;
    }
    else
    {
        V3RtcGetRtTag (pPath, &(ip6RtInfo.u4RouteTag));
    }

    if (V3UtilIp6AddrComp (&pPath->aNextHops[u1HopIndex].nbrIpv6Addr,
                           &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
    {
        ip6RtInfo.i1Type = IP6_ROUTE_TYPE_DIRECT;
    }
    else
    {
        ip6RtInfo.i1Type = IP6_ROUTE_TYPE_INDIRECT;
    }
    ip6RtInfo.u1Prefixlen = pRtEntry->u1PrefixLen;
    ip6RtInfo.i1Proto = IP6_OSPF_PROTOID;

    MEMCPY (&Rmapip6RtInfo, &ip6RtInfo, sizeof (tNetIpv6RtInfo));
#ifdef ROUTEMAP_WANTED
    if (u1CmdType == NETIPV6_ADD_ROUTE || u1CmdType == NETIPV6_MODIFY_ROUTE)
    {
        if (OSIX_FAILURE == V3ApplyInFilterInCxt (pV3OspfCxt, &ip6RtInfo))
        {
            pRtEntry->u1IsRmapAffected = OSIX_TRUE;
            /* Stop processing this route. */
            return;
        }
    }
    if (MEMCMP (&Rmapip6RtInfo, &ip6RtInfo, sizeof (tNetIpv6RtInfo)
                != OSPFV3_ZERO))
    {
        pRtEntry->u1IsRmapAffected = OSIX_TRUE;
    }
#endif
    ip6RtInfo.u1Preference = V3FilterRouteSourceInCxt (pV3OspfCxt, &ip6RtInfo);

    if ((pV3OspfCxt->u1PrevOspfv3RestartState == OSPFV3_GR_RESTART) &&
        (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
    {
        /* To Indicate RTM that this route update is given
         * at the end of graceful restart exit */
        ip6RtInfo.u2ChgBit |= IP6_BIT_GR;
    }

    if (NetIpv6LeakRoute (u1CmdType, &ip6RtInfo) == NETIPV6_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "Failed to add/delete route " "to common routing table\n");
        return;
    }

    /* If this router is a helper to GR router, and the nexthop of the
       Route added is the IP addr of the GR router, then we should exit
       the helper.  */
    if ((u1CmdType == NETIPV6_ADD_ROUTE) &&
        ((pPath->aNextHops[u1HopIndex].pInterface != NULL) &&
         (pPath->aNextHops[u1HopIndex].pInterface->u1GRRouterPresent ==
          OSIX_TRUE)) && (pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED))
    {
        TMO_SLL_Scan (&(pPath->aNextHops[u1HopIndex].pInterface->nbrsInIf),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

            if ((pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING) &&
                (V3UtilIp6AddrComp (&pNbr->nbrIpv6Addr,
                                    &pPath->aNextHops[u1HopIndex].
                                    nbrIpv6Addr) == OSPFV3_EQUAL))
            {
                O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                break;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "Added/Deleted route to Common routing table "
                "successfully\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmLeakRouteInCxt\n");

    return;
}

/*****************************************************************************/
/* Function        : V3RrdConfRtInfoCreateInCxt                              */
/*                                                                           */
/* Description     : This function creates the config record and add the     */
/*                   created config record to sorted list.                   */
/*                                                                           */
/* Input           : pV3OspfCxt        : Context pointer                     */
/*                   pRrdDestIPAddr    : Dest IP Address                     */
/*                   pRrdDestAddrMask  : Dest IP Addr Mask                   */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the new entry, if created successfully       */
/*                   NULL, otherwise                                         */
/*****************************************************************************/
PUBLIC tV3OsRedistrConfigRouteInfo *
V3RrdConfRtInfoCreateInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pRrdDestIPAddr,
                            UINT1 u1PrefixLen)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OsRedistrConfigRouteInfo *pLstRrdConfRtInfo = NULL;
    tV3OsRedistrConfigRouteInfo *pPrvRrdConfRtInfo = NULL;
    UINT1               u1FoundFlag = OSIX_FALSE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RrdConfRtInfoCreateInCxt\n");

    OSPFV3_REDISTR_CONFIG_INFO_ALLOC (&pRrdConfRtInfo);
    if (NULL == pRrdConfRtInfo)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for RRD config record\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC, pV3OspfCxt->u4ContextId,
                    "Failed to allocate memory for RRD config record\n");
        return NULL;
    }

    OSPFV3_IP6_ADDR_COPY (pRrdConfRtInfo->rrdConfigPrefix, *pRrdDestIPAddr);
    pRrdConfRtInfo->u1PrefixLength = u1PrefixLen;

    /* Initialize to the default values */
    pRrdConfRtInfo->u4RrdRouteMetricValue = OSPFV3_AS_EXT_DEF_METRIC;
    pRrdConfRtInfo->u1RrdRouteMetricType = OSPFV3_TYPE_2_METRIC;
    pRrdConfRtInfo->u1RedistrTagType = OSPFV3_MANUAL_TAG;
    pRrdConfRtInfo->u4ManualTagValue = 0;

    /* Add to the SLL in ascending order */
    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pLstRrdConfRtInfo,
                  tV3OsRedistrConfigRouteInfo *)
    {
        switch (V3UtilIp6AddrComp (&pRrdConfRtInfo->rrdConfigPrefix,
                                   &pLstRrdConfRtInfo->rrdConfigPrefix))
        {
            case OSPFV3_LESS:
                u1FoundFlag = OSIX_TRUE;
                break;

            case OSPFV3_EQUAL:
                if (pRrdConfRtInfo->u1PrefixLength <
                    pLstRrdConfRtInfo->u1PrefixLength)
                {
                    u1FoundFlag = OSIX_TRUE;
                }
                else if (pRrdConfRtInfo->u1PrefixLength >
                         pLstRrdConfRtInfo->u1PrefixLength)
                {
                    pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                }
                break;

            default:
                pPrvRrdConfRtInfo = pLstRrdConfRtInfo;
                break;
        }

        if (u1FoundFlag == OSIX_TRUE)
        {
            break;
        }
    }
    TMO_SLL_Insert (&(pV3OspfCxt->redistrRouteConfigLst),
                    &(pPrvRrdConfRtInfo->nextRrdConfigRoute),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    gV3OsRtr.pRedistCfgTableCache = NULL;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RrdConfRtInfoCreateInCxt\n");

    return pRrdConfRtInfo;
}

/*****************************************************************************/
/* Function        : V3RtmSetRRDConfigRecordInCxt                            */
/*                                                                           */
/* Description     : This function regenarates the Type 5 LSA for each       */
/*                   External route which is matching with the configured    */
/*                   config record.                                          */
/*                                                                           */
/* Input           : pV3OspfCxt     - Pointer to Context                     */
/*                   pRrdConfRtInfo - Pointer to config record prefix        */
/*                                    which is to be enabled.                */
/*                                                                           */
/* Output          : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*****************************************************************************/
PUBLIC VOID
V3RtmSetRRDConfigRecordInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsRedistrConfigRouteInfo * pRrdConfRtInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    tV3OsRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmSetRRDConfigRecordInCxt\n");

    VOID               *pTempPtr = NULL;

    /* Updating the network route entries */
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tV3OsExtRoute *) pTempPtr;
            pBestRrdConfRtInfo = V3RtmFindBestRrdConfRtInfoInCxt
                (pV3OspfCxt, &(pExtRoute->ip6Prefix),
                 pExtRoute->u1PrefixLength);

            /* If the configured record is the best match for this 
             * external route */
            if (pRrdConfRtInfo == pBestRrdConfRtInfo)
            {
                pExtRoute->metric.u4Metric =
                    pRrdConfRtInfo->u4RrdRouteMetricValue;
                pExtRoute->metric.u4MetricType =
                    pRrdConfRtInfo->u1RrdRouteMetricType;
                if (pRrdConfRtInfo->u1RedistrTagType == OSPFV3_MANUAL_TAG)
                {
                    pExtRoute->u4ExtTag =
                        (pRrdConfRtInfo->
                         u4ManualTagValue & OSPFV3_MANUAL_TAG_MASK);
                }
                V3ExtrtParamChangeInCxt (pV3OspfCxt, pExtRoute);
            }

            pLeafNode = inParams.pLeafNode;
            MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
            au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
            inParams.Key.pKey = (UINT1 *) au1Key;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr, (VOID **)
                                &(inParams.pLeafNode)) != TRIE_FAILURE);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmSetRRDConfigRecordInCxt\n");

}

/*****************************************************************************/
/* Function        : V3RtmDelRRDConfigRecordInCxt                            */
/*                                                                           */
/* Description     : This function regenarates the Type 5 Lsa for each       */
/*                   External route which is matching with the deleted       */
/*                   config record.                                          */
/*                                                                           */
/* Input           : pV3OspfCxt     - Pointer to Context                     */
/*                   pRrdConfRtInfo - Pointer to config record which is      */
/*                                    to be deleted.                         */
/*                                                                           */
/* Output          : NONE                                                    */
/*                                                                           */
/* Returns         : NONE                                                    */
/*****************************************************************************/
PUBLIC VOID
V3RtmDelRRDConfigRecordInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsRedistrConfigRouteInfo * pRrdConfRtInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;
    tV3OsRedistrConfigRouteInfo *pNextBestRrdConfRtInfo = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmDelRRDConfigRecordInCxt\n");

    /* Updating the network route entries */
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE)
    {
        do
        {
            pExtRoute = (tV3OsExtRoute *) pTempPtr;
            pBestRrdConfRtInfo = V3RtmFindBestRrdConfRtInfoInCxt
                (pV3OspfCxt, &(pExtRoute->ip6Prefix),
                 pExtRoute->u1PrefixLength);

            /* If the deleting record is the best match for this 
             * external route, then Lsa should be regenarated  */
            if (pRrdConfRtInfo == pBestRrdConfRtInfo)
            {
                pNextBestRrdConfRtInfo = V3RtmFindNextBestRrdConfRtInfoInCxt
                    (pV3OspfCxt, &pExtRoute->ip6Prefix,
                     pExtRoute->u1PrefixLength, pBestRrdConfRtInfo);

                /* If there is no next best config record, then 
                 * the external route parameters should take default values */
                if (pNextBestRrdConfRtInfo == NULL)
                {
                    /* Fill With Default values */
                    pExtRoute->metric.u4Metric = OSPFV3_AS_EXT_DEF_METRIC;
                    pExtRoute->metric.u4MetricType = OSPFV3_TYPE_2_METRIC;
                    pExtRoute->u4ExtTag = 0;
                }
                else
                {
                    pExtRoute->metric.u4Metric =
                        pNextBestRrdConfRtInfo->u4RrdRouteMetricValue;
                    pExtRoute->metric.u4MetricType =
                        pNextBestRrdConfRtInfo->u1RrdRouteMetricType;
                    if (pNextBestRrdConfRtInfo->u1RedistrTagType
                        == OSPFV3_MANUAL_TAG)
                    {
                        pExtRoute->u4ExtTag =
                            (pNextBestRrdConfRtInfo->u4ManualTagValue
                             & OSPFV3_MANUAL_TAG_MASK);
                    }
                }
                V3ExtrtParamChangeInCxt (pV3OspfCxt, pExtRoute);
            }

            pLeafNode = inParams.pLeafNode;
            MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
            au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
            inParams.Key.pKey = (UINT1 *) au1Key;
        }
        while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                                &pTempPtr, (VOID **)
                                &(inParams.pLeafNode)) != TRIE_FAILURE);
    }

    TMO_SLL_Delete (&(pV3OspfCxt->redistrRouteConfigLst),
                    &(pRrdConfRtInfo->nextRrdConfigRoute));

    gV3OsRtr.pRedistCfgTableCache = NULL;

    OSPFV3_REDISTR_CONFIG_INFO_FREE (pRrdConfRtInfo);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmDelRRDConfigRecordInCxt\n");

}

/*****************************************************************************/
/* Function        : V3RtmFindBestRrdConfRtInfoInCxt                         */
/*                                                                           */
/* Description     : This procedure scans the redistrRouteConfigLst list and */
/*                   returns the entry that subsumes (or is equal to) the    */
/*                   one received from RTM                                   */
/*                   It is assumed that the masks are contiguous             */
/*                                                                           */
/* Input           : pV3OspfCxt     - Pointer to Context                     */
/*                   pRrdDestPrefix - Pointer to config record prefix        */
/*                   u1PrefixLen - Prefix length                             */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the matching entry, if present               */
/*                   NULL, otherwise                                         */
/*****************************************************************************/
PRIVATE tV3OsRedistrConfigRouteInfo *
V3RtmFindBestRrdConfRtInfoInCxt (tV3OspfCxt * pV3OspfCxt,
                                 tIp6Addr * pRrdDestPrefix, UINT1 u1PrefixLen)
{
    tV3OsRedistrConfigRouteInfo *pLstRrdConfRtInfo = NULL;
    tV3OsRedistrConfigRouteInfo *pBestRrdConfRtInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmFindBestRrdConfRtInfoInCxt\n");

    /* Finding the most specific config record */
    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pLstRrdConfRtInfo,
                  tV3OsRedistrConfigRouteInfo *)
    {
        /* If the config record is not ACTIVE, it can not be 
         * best coonfig record. */
        if (pLstRrdConfRtInfo->rrdConfigRecStatus != ACTIVE)
        {
            continue;
        }

        /* This is the least preferred route */
        if (u1PrefixLen < pLstRrdConfRtInfo->u1PrefixLength)
        {
            continue;
        }

        if (V3UtilIp6PrefixComp (pRrdDestPrefix,
                                 &pLstRrdConfRtInfo->rrdConfigPrefix,
                                 pLstRrdConfRtInfo->u1PrefixLength) ==
            OSPFV3_EQUAL)
        {
            pBestRrdConfRtInfo = pLstRrdConfRtInfo;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmFindBestRrdConfRtInfoInCxt\n");

    return pBestRrdConfRtInfo;
}

/*****************************************************************************/
/* Function        : V3RtmFindNextBestRrdConfRtInfoInCxt                     */
/*                                                                           */
/* Description     : This procedure scans the redistrRouteConfigLst list and */
/*                   returns the entry that is the next best match for the   */
/*                   given destination network.                              */
/*                                                                           */
/* Input           : pV3OspfCxt     - Pointer to Context                     */
/*                   pRrdDestPrefix - Pointer to config record prefix        */
/*                   u1PrefixLen - Prefix length                             */
/*                   pRrdConfRtInfo - Pointer to best config record          */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the next best config entry, if present       */
/*                   NULL, otherwise                                         */
/*****************************************************************************/
PRIVATE tV3OsRedistrConfigRouteInfo *
V3RtmFindNextBestRrdConfRtInfoInCxt (tV3OspfCxt * pV3OspfCxt,
                                     tIp6Addr * pRrdDestPrefix,
                                     UINT1 u1PrefixLen,
                                     tV3OsRedistrConfigRouteInfo *
                                     pRrdConfRtInfo)
{
    tV3OsRedistrConfigRouteInfo *pLstRrdConfRtInfo = NULL;
    tV3OsRedistrConfigRouteInfo *pNextBestRrdConfRtInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmFindNextBestRrdConfRtInfoInCxt\n");

    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pLstRrdConfRtInfo,
                  tV3OsRedistrConfigRouteInfo *)
    {
        /* Best Config record should not be considered for 
         * finding next best config record */
        if (pLstRrdConfRtInfo == pRrdConfRtInfo)
        {
            continue;
        }
        /* If the config record is not ACTIVE, it can not be 
         * best coonfig record. */
        if (pLstRrdConfRtInfo->rrdConfigRecStatus != ACTIVE)
        {
            continue;
        }

        if (pLstRrdConfRtInfo->u1PrefixLength == u1PrefixLen)
        {
            continue;
        }

        if (V3UtilIp6PrefixComp (pRrdDestPrefix,
                                 &pLstRrdConfRtInfo->rrdConfigPrefix,
                                 pLstRrdConfRtInfo->u1PrefixLength) ==
            OSPFV3_EQUAL)
        {
            pNextBestRrdConfRtInfo = pLstRrdConfRtInfo;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmFindNextBestRrdConfRtInfoInCxt\n");

    return pNextBestRrdConfRtInfo;
}

#ifdef ROUTEMAP_WANTED
/*****************************************************************************/
/* Function        : V3ApplyInFilterInCxt                                    */
/*                                                                           */
/* Description     : This function will check whether the route can be added */
/*                   or dropped. If route is permitted then returned         */
/*                   OSIX_SUCCESS else return OSIX_FAILURE.                  */
/*                                                                           */
/* Input           : pV3OspfCxt     - ptr to the Context                     */
/*                   pIp6RtInfoOut  - ptr to the routing update              */
/*                                                                           */
/* Output          : None.                                                   */
/*                                                                           */
/* Returns         : OSIX_SUCCESS/OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT1
V3ApplyInFilterInCxt (tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pIp6RtInfo)
{
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3ApplyInFilterInCxt\n");

    tFilteringRMap     *pFilterRMap = pV3OspfCxt->pDistributeInFilterRMap;

    if ((pFilterRMap == NULL) || (pIp6RtInfo == NULL))
    {
        return OSIX_SUCCESS;
    }
    /* If status of route map is disable then this route should not be droped */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return OSIX_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV6 (RtInfoIn.DstXAddr,
                           pIp6RtInfo->Ip6Dst.ip6_addr_u.u4WordAddr);
    IPVX_ADDR_INIT_FROMV6 (RtInfoIn.SrcXAddr,
                           pIp6RtInfo->NextHop.ip6_addr_u.u4WordAddr);
    IPVX_ADDR_INIT_FROMV6 (RtInfoIn.NextHopXAddr,
                           pIp6RtInfo->NextHop.ip6_addr_u.u4WordAddr);
    RtInfoIn.u2DstPrefixLen = (UINT2) pIp6RtInfo->u1Prefixlen;
    RtInfoIn.u1MetricType = pIp6RtInfo->u1MetricType;
    RtInfoIn.i2RouteType = (INT2) pIp6RtInfo->i1Type;
    RtInfoIn.u4RouteTag = pIp6RtInfo->u4RouteTag;
    RtInfoIn.i4Metric = (INT4) pIp6RtInfo->u4Metric;
    RtInfoIn.u2RtProto = (UINT2) pIp6RtInfo->i1Proto;
    RtInfoIn.u4IfIndex = pIp6RtInfo->u4Index;
    /* Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_DENY != RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                          pFilterRMap->
                                          au1DistInOutFilterRMapName))
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : V3ApplyInFilterInCxt\n");

        return OSIX_SUCCESS;
    }
    else
    {
        gu4V3ApplyInFilterInCxtFail++;
        return OSIX_FAILURE;
    }
}
#endif /*ROUTEMAP_WANTED */

#ifdef RRD_WANTED

/*****************************************************************************/
/* Function        : V3OspfProcessRtmRts                                     */
/*                                                                           */
/* Description     : This routine process all the external routes received   */
/*                   from RTM after receving event from Main loop.           */
/*                                                                           */
/* Input           : None                                                    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessRtmRts (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OspfRtmNode     *pRtmNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4ContextId = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfProcessRtmRts\n");

    OsixSemTake (OSPF3_RTM_LST_SEM_ID);

    while ((pNode = TMO_SLL_Get (&(gV3OsRtr.rtmRoutesLst))) != NULL)
    {
        pRtmNode = OSPFV3_GET_BASE_PTR (tV3OspfRtmNode, nextRtmNode, pNode);

        /* Get the context pointer from the context id present in the RTM
         * header. If the system is operating in SI mode, get the default
         * context pointer else get the context pointer from the context id
         */
        if (pRtmNode->u1MessageType == RTM6_REGISTRATION_ACK_MESSAGE)
        {
            u4ContextId = pRtmNode->RegnAckMsg.u4ContextId;
        }
        else
        {
            u4ContextId = pRtmNode->RtInfo.u4ContextId;
        }

        if (V3OspfGetSystemMode (OSPF3_PROTOCOL_ID) == OSPFV3_MI_MODE)
        {
            if (V3UtilOspfIsValidCxtId ((INT4) u4ContextId) == OSIX_SUCCESS)
            {
                pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4ContextId];
            }
        }
        else
        {
            pV3OspfCxt = gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID];
        }

        if (pV3OspfCxt == NULL)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Context Id : %u " "not available\r\n", u4ContextId));

            OSPFV3_GBL_TRC1 (OSPFV3_RT_TRC | OSPFV3_CRITICAL_TRC,
                             " Context Id : %u "
                             "not available\r\n", u4ContextId);
            OSPFV3_RTM_ROUTES_FREE (pRtmNode);
            continue;
        }

        switch (pRtmNode->u1MessageType)
        {
            case RTM6_REGISTRATION_ACK_MESSAGE:
            {
                V3RtmProcessRegAckMsgInCxt (pV3OspfCxt,
                                            &(pRtmNode->RegnAckMsg));
                break;
            }

            case RTM6_ROUTE_UPDATE_MESSAGE:
            {
                if (pV3OspfCxt->redistrAdmnStatus == OSPFV3_ENABLED)
                {
                    V3RtmProcessRtUpdateMsgInCxt (pV3OspfCxt,
                                                  &(pRtmNode->RtInfo));
                }
                break;
            }

            case RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE:
            {
                if (pV3OspfCxt->redistrAdmnStatus == OSPFV3_ENABLED)
                {
                    V3RtmProcessRtChgNtfMsgInCxt (pV3OspfCxt,
                                                  &(pRtmNode->RtInfo));
                }
                break;
            }

            default:
            {
                break;
            }
        }

        OSPFV3_RTM_ROUTES_FREE (pRtmNode);
    }

    OsixSemGive (OSPF3_RTM_LST_SEM_ID);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfProcessRtmRts\n");

    return;
}

/*****************************************************************************/
/* Function        : V3RtmSendToOspf                                         */
/*                                                                           */
/* Description     : This is the call back function provided to RTM to       */
/*                   receive messages from RTM. Depending on the received    */
/*                   message type this function takes the necessary action   */
/*                                                                           */
/* Input           : pRespInfo   : Pointer to the response message received  */
/*                                 from RTM.                                 */
/*                   pRtm6Header : Pointer to RTM message header.            */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_SUCCESS if the received message is processed       */
/*                   correctly, OSIX_FAILURE otherwise.                      */
/*****************************************************************************/
PUBLIC INT4
V3RtmSendToOspf (tRtm6RespInfo * pRespInfo, tRtm6MsgHdr * pRtm6Header)
{
    tV3OspfRtmNode     *pRtmNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtmSendToOspf\n");

    OsixSemTake (OSPF3_RTM_LST_SEM_ID);

    OSPFV3_RTM_ROUTES_ALLOC (&pRtmNode);

    if (pRtmNode == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to Allocate memory to RTM Node\n"));

        OSPFV3_GBL_TRC (OSPFV3_RT_TRC | OSPFV3_CRITICAL_TRC,
                        "V3RtmSendToOspf : RTM Node allocation failed\r\n");
        OsixSemGive (OSPF3_RTM_LST_SEM_ID);
        return OSIX_FAILURE;
    }

    MEMSET (pRtmNode, 0, sizeof (tV3OspfRtmNode));

    pRtmNode->u1MessageType = pRtm6Header->u1MessageType;

    if (pRtm6Header->u1MessageType == RTM6_REGISTRATION_ACK_MESSAGE)
    {
        MEMCPY (&(pRtmNode->RegnAckMsg), pRespInfo->pRegnAck,
                sizeof (tRtm6RegnAckMsg));
    }
    else
    {
        MEMCPY (&(pRtmNode->RtInfo), pRespInfo->pRtInfo,
                sizeof (tNetIpv6RtInfo));
    }

    TMO_SLL_Init_Node (&(pRtmNode->nextRtmNode));
    TMO_SLL_Add (&(gV3OsRtr.rtmRoutesLst), &(pRtmNode->nextRtmNode));

    OsixSemGive (OSPF3_RTM_LST_SEM_ID);

    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_RTM_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Send Evt for OSPFV3_RTM_EVENT failed\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Send event for OSPFV3_RTM_EVENT failed"));
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtmSendToOspf\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function        : V3RtmProcessRegAckMsgInCxt                              */
/*                                                                           */
/* Description     : This function is for processing the acknowledgement     */
/*                   message received from RTM6.                             */
/*                                                                           */
/* Input           : pV3OspfCxt : Pointer to OSPFv3 context                  */
/*                   pRegnAck : Pointer to acknowledgement message           */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtmProcessRegAckMsgInCxt (tV3OspfCxt * pV3OspfCxt, tRtm6RegnAckMsg * pRegnAck)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmProcessRegAckMsgInCxt\n");

    UNUSED_PARAM (pRegnAck);
    if (pV3OspfCxt->u4RrdSrcProtoBitMask != 0)
    {
        /* Redistribute enable message to RTM */
        V3RtmTxRedistributeMsgInCxt (pV3OspfCxt,
                                     pV3OspfCxt->u4RrdSrcProtoBitMask,
                                     RTM6_REDISTRIBUTE_ENABLE_MESSAGE,
                                     pV3OspfCxt->au1RMapName);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmProcessRegAckMsgInCxt\n");

}

/*****************************************************************************/
/* Function        : V3RtmProcessRtUpdateMsgInCxt                            */
/*                                                                           */
/* Description     : This function is for processing the route update        */
/*                   message received from RTM6.                             */
/*                                                                           */
/* Input           : pV3OspfCxt  :  Context pointer                          */
/*                   pRtInfo : Pointer to route update message               */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtmProcessRtUpdateMsgInCxt (tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pRtInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    UINT4               u4Metric;
    UINT4               u4MetricType;
    UINT4               u4RouteTag;
    UINT4               u4RowStatus;
    UINT4               u4ProtoIndex = OSPFV3_MAX_PROTO_REDISTRUTE_SIZE;
    tV3OsExtRoute       extRoute;

    if (pRtInfo->i1Proto == IP6_BGP_PROTOID)
    {
        u4ProtoIndex = OSPFV3_REDISTRUTE_BGP;
    }
    else if (pRtInfo->i1Proto == IP6_RIP_PROTOID)
    {
        u4ProtoIndex = OSPFV3_REDISTRUTE_RIP;
    }
    else if (pRtInfo->i1Proto == IP6_LOCAL_PROTOID)
    {
        u4ProtoIndex = OSPFV3_REDISTRUTE_CONNECTED;
    }
    else if (pRtInfo->i1Proto == IP6_NETMGMT_PROTOID)
    {
        u4ProtoIndex = OSPFV3_REDISTRUTE_STATIC;
    }
    else if (pRtInfo->i1Proto == IP6_ISIS_PROTOID)
    {
        u4ProtoIndex = OSPFV3_REDISTRUTE_ISIS;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmProcessRtUpdateMsgInCxt\n");

    /*
     * Get the configured metric & metric-type from RedistrRouteConfigLst if 
     * update from RTM is subsumed by the corresponding entry in the list
     * Otherwise, use default values                                   
     */
    pRrdConfRtInfo = V3RtmFindBestRrdConfRtInfoInCxt
        (pV3OspfCxt, &(pRtInfo->Ip6Dst), pRtInfo->u1Prefixlen);

    OSPFV3_IP6_ADDR_COPY (extRoute.fwdAddr, gV3OsNullIp6Addr);

    if (pRrdConfRtInfo != NULL)
    {
        u4Metric = pRrdConfRtInfo->u4RrdRouteMetricValue;
        u4MetricType = pRrdConfRtInfo->u1RrdRouteMetricType;
        if (pRrdConfRtInfo->u1RedistrTagType == OSPFV3_MANUAL_TAG)
        {
            u4RouteTag = pRrdConfRtInfo->u4ManualTagValue;
            u4RouteTag &= OSPFV3_MANUAL_TAG_MASK;
        }
        else
        {
            u4RouteTag = pRtInfo->u4RouteTag;
        }
    }
    else
    {
        if (pRtInfo->i4RMapFlag != 0)
        {
            u4Metric = (UINT4) pRtInfo->u4Metric;
        }
        else
        {
            u4Metric = pV3OspfCxt->au4MetricValue[u4ProtoIndex - 1];
        }
        if (pV3OspfCxt->au4MetricType[u4ProtoIndex - 1] != 0)
        {
            u4MetricType = pV3OspfCxt->au4MetricType[u4ProtoIndex - 1];
        }
        else
        {
            u4MetricType = OSPFV3_TYPE_2_METRIC;
        }
        u4RouteTag = pRtInfo->u4RouteTag;
    }

    u4RowStatus = pRtInfo->u4RowStatus;
    OSPFV3_IP6_ADDR_COPY (extRoute.fwdAddr, pRtInfo->NextHop);
    /*
     * forwarding-address setting
     * If OSPF is enabled on the common-network on which the next hop is 
     * present, next hop addr is the forwarding address. Else set it to NULL  
     */

    OSPFV3_IP6_ADDR_COPY (extRoute.ip6Prefix, pRtInfo->Ip6Dst);
    extRoute.u1PrefixLength = pRtInfo->u1Prefixlen;
    extRoute.metric.u4Metric = u4Metric;
    extRoute.metric.u4MetricType = u4MetricType;
    extRoute.u4ExtTag = u4RouteTag;
    extRoute.u4FwdIfIndex = pRtInfo->u4Index;
    extRoute.u2SrcProto = (UINT2) pRtInfo->i1Proto;
    extRoute.u1Level = (UINT1) pRtInfo->u1MetricType;
    extRoute.rowStatus = (UINT1) u4RowStatus;

    pExtRoute = V3ExtrtFindRouteInCxt
        (pV3OspfCxt, &pRtInfo->Ip6Dst, pRtInfo->u1Prefixlen);

    if (u4RowStatus == DESTROY)
    {
        if (pExtRoute != NULL)
        {
            V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
        }

        return;
    }

    if ((pExtRoute = V3ExtrtAddInCxt (pV3OspfCxt, &extRoute)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to add route to Import List\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Route Addition to Import List Failed\n");
        return;
    }

    if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
    {
        V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
    }

    return;
}

/*****************************************************************************/
/* Function        : V3RtmProcessRtChgNtfMsgInCxt                            */
/*                                                                           */
/* Description     : This function is for processing the route change        */
/*                   notification message received from RTM6.                */
/*                                                                           */
/* Input           : pV3OspfCxt : Context pointer                            */
/*                   pRtInfo  : Pointer to changed route information         */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*****************************************************************************/
PRIVATE VOID
V3RtmProcessRtChgNtfMsgInCxt (tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pRtInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT2               u2RtBitMask;
    UINT4               u4RowStatus;

    u2RtBitMask = pRtInfo->u2ChgBit;
    u4RowStatus = pRtInfo->u4RowStatus;

    if ((pExtRoute = V3ExtrtFindRouteInCxt
         (pV3OspfCxt, &pRtInfo->Ip6Dst, pRtInfo->u1Prefixlen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to get Import List\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Import Lst GET Failed\n");
        return;
    }

    if ((u2RtBitMask & RTM6_ROW_STATUS_MASK) && (u4RowStatus == DESTROY))
    {
        V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
    }
    else if (u2RtBitMask & RTM6_IFINDEX_MASK)
    {
        if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
        {
            V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3ApplyDistance                                        */
/*                                                                           */
/* Description  : This procedure applies Distance  to route entries that*/
/*                are already dropped by rmap rules                          */
/*                                                                           */
/* Input        : pAddr        : IP address                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3ApplyDistance (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    UINT4               u4CurrentTime = 0;
    UINT1               u1HopCount = 0;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;
    VOID               *pTempRoute = NULL;
    VOID               *pLeafNode = NULL;

    MEMSET (&inParams, 0, sizeof (tInputParams));
    MEMSET (&outParams, 0, sizeof (tOutputParams));
    /* Scan the Routing table */
    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)

    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "No routes are present in " "routing table\n");
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "No routes are present in " "routing table\n"));

        return;
    }
    pRtEntry = (tV3OsRtEntry *) pTempPtr;
    while (pRtEntry != NULL)
    {
        TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
        {
            for (u1HopCount = 0; ((u1HopCount < pPath->u1HopCount) &&
                                  (u1HopCount < OSPFV3_MAX_NEXT_HOPS));
                 u1HopCount++)
            {
                V3RtmLeakRouteInCxt (pV3OspfCxt, pRtEntry,
                                     pPath, u1HopCount, NETIPV6_MODIFY_ROUTE);

            }
        }

        pLeafNode = inParams.pLeafNode;

        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
        inParams.u1PrefixLen = pRtEntry->u1PrefixLen;
        if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                             &pTempPtr, (VOID **)
                             &(inParams.pLeafNode)) != TRIE_SUCCESS)
        {
            pTempRoute = NULL;
        }
        else
        {
            pTempRoute = (tV3OsRtEntry *) pTempPtr;
        }
        pRtEntry = pTempRoute;
        /* Check whether we need to relinquish the route calculation
         * to do other OSPF processings */
        if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
        {
            u4CurrentTime = OsixGetSysUpTime ();
            /* current time greater than relinquished interval */
            if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
            {
                OSPF3RtcRelinquishInCxt (pV3OspfCxt);
            }
        }

    }
}

/*****************************************************************************/
/* Function        : V3RtmTxRedistributeMsg                                  */
/*                                                                           */
/* Description     : This function is used to send the proto mask enable     */
/*                   or disable information to RTM6.                         */
/*                                                                           */
/* Input           : pV3OspfCxt        : Context pointer                     */
/*                   u4SrcProtoBitMask : Source protocol mask information    */
/*                   u1Rrd6MsgType     : Indicates enable or disable         */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_SUCCESS if the received message is send to RTM6    */
/*                   correctly, OSIX_FAILURE otherwise.                      */
/*****************************************************************************/
PUBLIC INT4
V3RtmTxRedistributeMsgInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4SrcProtoBitMask,
                             UINT1 u1Rrd6MsgType, UINT1 *pu1RMapName)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtm6MsgHdr        *pRtmMsgHdr = NULL;
    UINT2               u2DestProtoMask;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];    /*trailing zero */

    /**prepare intermediate buf for route-map,
        ensure no garbage after map name */
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    if (NULL != pu1RMapName)
    {
        STRNCPY (au1RMapName, pu1RMapName, RMAP_MAX_NAME_LEN);
    }

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtm6MsgHdr) +
           sizeof (UINT2) + RMAP_MAX_NAME_LEN), 0)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Buffer Allocation failed\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Buffer Allocation Failure\n");
        gu4V3RtmTxRedistributeMsgInCxtFail++;
        return OSIX_FAILURE;
    }

    pRtmMsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pRtmMsg);

    /* Fill the RTM6 message header */
    pRtmMsgHdr->RegnId.u4ContextId = pV3OspfCxt->u4ContextId;
    pRtmMsgHdr->RegnId.u2ProtoId = OSPF6_ID;
    pRtmMsgHdr->u1MessageType = u1Rrd6MsgType;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT2) + RMAP_MAX_NAME_LEN;

    u2DestProtoMask = (UINT2) u4SrcProtoBitMask;

    /*put protocol-mask in packet on offset 0 */
    IP6_COPY_TO_BUF (pRtmMsg, &u2DestProtoMask, 0, sizeof (UINT2));

    /*put route-map in packet on offset 2 */
    IP6_COPY_TO_BUF (pRtmMsg, au1RMapName, sizeof (UINT2), RMAP_MAX_NAME_LEN);

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm6 (pRtmMsg) != RTM6_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failure in sending message to RTM6\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Failure in sending message to RTM6\n");
        CRU_BUF_Release_MsgBufChain (pRtmMsg, OSPFV3_NORMAL_RELEASE);
        gu4V3RtmTxRedistributeMsgInCxtFail++;
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function        : V3RtmRRDSrcProtoDisableInCxt                            */
/*                                                                           */
/* Description     : This function deletes the routes of the learned from    */
/*                   the protocol whose mask is disabled.                    */
/*                                                                           */
/* Input           : pV3OspfCxt        : Context pointer                     */
/*                   u4SrcProtoBitMask : Source protocol mask information    */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_SUCCESS if deletion is successfull                 */
/*                   OSIX_FAILURE otherwise.                                 */
/*****************************************************************************/
PUBLIC INT4
V3RtmRRDSrcProtoDisableInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4SrcProtoBitMask)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsExtRoute      *pTempExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmRRDSrcProtoDisableInCxt\n");
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "No External routes are present in "
                      "external routing table\n"));

        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "No External routes are present in "
                    "external routing table\n");
    }
    pExtRoute = (tV3OsExtRoute *) pTempPtr;
    while (pExtRoute != NULL)
    {
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;

        if (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                             &pTempPtr, (VOID **)
                             &(inParams.pLeafNode)) != TRIE_SUCCESS)
        {
            pTempExtRoute = NULL;
        }
        pTempExtRoute = (tV3OsExtRoute *) pTempPtr;
        switch (pExtRoute->u2SrcProto)
        {
            case IP6_LOCAL_PROTOID:
                if (u4SrcProtoBitMask & RTM6_DIRECT_MASK)
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                break;

            case IP6_NETMGMT_PROTOID:
                if (u4SrcProtoBitMask & RTM6_STATIC_MASK)
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                break;

            case IP6_RIP_PROTOID:
                if (u4SrcProtoBitMask & RTM6_RIP_MASK)
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                break;

            case IP6_BGP_PROTOID:
                if (u4SrcProtoBitMask & RTM6_BGP_MASK)
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                break;
            case IP6_ISIS_PROTOID:
                if ((u4SrcProtoBitMask & RTM6_ISISL1_MASK) &&
                    (pExtRoute->u1Level == IP_ISIS_LEVEL1))
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                else if ((u4SrcProtoBitMask & RTM6_ISISL2_MASK) &&
                         (pExtRoute->u1Level == IP_ISIS_LEVEL2))
                {
                    V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
                }
                break;

            default:
                break;
        }
        pExtRoute = pTempExtRoute;
    }

    /* send redistribution disable message to RTM */
    if (V3RtmTxRedistributeMsgInCxt
        (pV3OspfCxt, u4SrcProtoBitMask,
         RTM6_REDISTRIBUTE_DISABLE_MESSAGE,
         pV3OspfCxt->au1RMapName) != OSIX_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to send redistribution disable message"
                      " to RTM6\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Redistribution Disable Message Transmit "
                    "to RTM6 Failure\n");
        return OSIX_FAILURE;
    }

    if (u4SrcProtoBitMask & (RTM6_DIRECT_MASK | RTM6_STATIC_MASK |
                             RTM6_RIP_MASK | RTM6_OSPF_MASK | RTM6_BGP_MASK))
    {
        MEMSET (&pV3OspfCxt->au1RMapName, 0, RMAP_MAX_NAME_LEN);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmRRDSrcProtoDisableInCxt\n");

    return OSIX_SUCCESS;
}

#endif
/**************************************************************************/
/* Function Name : V3FilterRouteSourceInCxt                               */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : pV3OspfCxt - Context pointer                           */
/*                 pSrcAddr   - source IPv6 address                       */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
V3FilterRouteSourceInCxt (tV3OspfCxt * pV3OspfCxt, tNetIpv6RtInfo * pIp6RtInfo)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3FilterRouteSourceInCxt\n");

#ifdef ROUTEMAP_WANTED
    UINT1               u1Distance = pV3OspfCxt->u1Distance;
    if (pV3OspfCxt->pDistanceFilterRMap != NULL
        && pV3OspfCxt->pDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
    {
        tRtMapInfo          MapInfo;
        MEMSET (&MapInfo, 0, sizeof (tRtMapInfo));
        IPVX_ADDR_INIT_FROMV6 (MapInfo.DstXAddr,
                               pIp6RtInfo->Ip6Dst.ip6_addr_u.u4WordAddr);

        IPVX_ADDR_INIT_FROMV6 (MapInfo.SrcXAddr,
                               pIp6RtInfo->NextHop.ip6_addr_u.u4WordAddr);

        IPVX_ADDR_INIT_FROMV6 (MapInfo.NextHopXAddr,
                               pIp6RtInfo->NextHop.ip6_addr_u.u4WordAddr);

        MapInfo.u2DstPrefixLen = (UINT2) pIp6RtInfo->u1Prefixlen;
        MapInfo.u1MetricType = pIp6RtInfo->u1MetricType;
        MapInfo.i2RouteType = (INT2) pIp6RtInfo->i1Type;
        MapInfo.i4Metric = (INT4) pIp6RtInfo->u4Metric;
        MapInfo.u4RouteTag = pIp6RtInfo->u4RouteTag;
        MapInfo.u2RtProto = (UINT2) pIp6RtInfo->i1Proto;
        MapInfo.u4IfIndex = pIp6RtInfo->u4Index;

        if (RMapApplyRule2 (&MapInfo,
                            pV3OspfCxt->pDistanceFilterRMap->
                            au1DistInOutFilterRMapName) == RMAP_ENTRY_MATCHED)
        {
            u1Distance = pV3OspfCxt->pDistanceFilterRMap->u1RMapDistance;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3FilterRouteSourceInCxt\n");

    return u1Distance;
#else
    UNUSED_PARAM (pV3OspfCxt);
    UNUSED_PARAM (pIp6RtInfo);
    return 0;
#endif /*ROUTEMAP_WANTED */
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3RtmGrNotifInd                                         */
/*                                                                           */
/* Description     : This routine sends an event to RTM indicating the       */
/*                   Grace period                                            */
/*                                                                           */
/* Input           : pV3OspfCxt         -   pointer to OSPFv3 context        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : OSIX_SUCCESS / OSIX_FAILURE                             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3RtmGrNotifInd (tV3OspfCxt * pV3OspfCxt)
{
    tOsixMsg           *pRtmMsg = NULL;
    tRtm6MsgHdr        *pRtmMsgHdr = NULL;

    if ((pRtmMsg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtm6MsgHdr) + sizeof (UINT4)), 0)) == NULL)

    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Buffer Allocation Failure\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Buffer Allocation Failure\n");
        return OSIX_FAILURE;
    }

    pRtmMsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pRtmMsg);

    /* fill the message header */
    pRtmMsgHdr->u1MessageType = RTM6_GR_NOTIFY_MSG;
    pRtmMsgHdr->RegnId.u2ProtoId = OSPF6_ID;
    pRtmMsgHdr->RegnId.u4ContextId = pV3OspfCxt->u4ContextId;
    pRtmMsgHdr->u2MsgLen = sizeof (UINT4);
    pRtmMsgHdr->RegnId.u4ContextId = pV3OspfCxt->u4ContextId;

    IP6_COPY_TO_BUF (pRtmMsg, &(pV3OspfCxt->u4GracePeriod), 0, sizeof (UINT4));

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm6 (pRtmMsg) != RTM6_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to send message to RTM6\n"));

        OSPFV3_TRC (OSPFV3_RTMODULE_TRC, pV3OspfCxt->u4ContextId,
                    "Failure in sending message to RTM6\n");
        CRU_BUF_Release_MsgBufChain (pRtmMsg, OSPFV3_NORMAL_RELEASE);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3RtmRrdConfRtInfoDeleteAllInCxt                        */
/*                                                                           */
/* Description     : This procedure deallocates memory for                   */
/*                   tV3OsRedistrConfigRouteInfo                             */
/*                                                                           */
/* Input           : pV3OspfCxt         -   pointer to OSPFv3 context        */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3RtmRrdConfRtInfoDeleteAllInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsRedistrConfigRouteInfo *pLstRrdConfRtInfo = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtmRrdConfRtInfoDeleteAllInCxt\n");

    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pLstNode,
                  tTMO_SLL_NODE *)
    {
        pLstRrdConfRtInfo = OSPFV3_GET_BASE_PTR
            (tV3OsRedistrConfigRouteInfo, nextRrdConfigRoute, pLstNode);

        if (pLstRrdConfRtInfo != NULL)
        {
            TMO_SLL_Delete (&(pV3OspfCxt->redistrRouteConfigLst),
                            &(pLstRrdConfRtInfo->nextRrdConfigRoute));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtmRrdConfRtInfoDeleteAllInCxt\n");

}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3rtm.c                         */
/*-----------------------------------------------------------------------*/
