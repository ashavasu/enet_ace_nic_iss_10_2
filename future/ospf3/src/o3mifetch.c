/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3mifetch.c,v 1.9 2018/02/01 11:02:57 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "o3inc.h"
# include "fsmisolw.h"
# include "fsmio3lw.h"
# include "ospf3lw.h"
# include "fsos3lw.h"

/* LOW LEVEL Routines for Table : FsMIStdOspfv3Table. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3Table (INT4 *pi4FsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3Table (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 *pi4NextFsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RouterId
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RouterId (INT4 i4FsMIStdOspfv3ContextId,
                             UINT4 *pu4RetValFsMIStdOspfv3RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3RouterId (pu4RetValFsMIStdOspfv3RouterId);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AdminStat
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3AdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AdminStat (INT4 i4FsMIStdOspfv3ContextId,
                              INT4 *pi1RetValFsMIStdOspfv3AdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3AdminStat (pi1RetValFsMIStdOspfv3AdminStat);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VersionNumber
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3VersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VersionNumber (INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 *pi1RetValFsMIStdOspfv3VersionNumber)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3VersionNumber (pi1RetValFsMIStdOspfv3VersionNumber);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3AreaBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaBdrRtrStatus (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3AreaBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaBdrRtrStatus (pi1RetValFsMIStdOspfv3AreaBdrRtrStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3ASBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ASBdrRtrStatus (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 *pi1RetValFsMIStdOspfv3ASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3ASBdrRtrStatus (pi1RetValFsMIStdOspfv3ASBdrRtrStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsScopeLsaCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3AsScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsScopeLsaCount (INT4 i4FsMIStdOspfv3ContextId,
                                    UINT4
                                    *pu4RetValFsMIStdOspfv3AsScopeLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsScopeLsaCount (pu4RetValFsMIStdOspfv3AsScopeLsaCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsScopeLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3AsScopeLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsScopeLsaCksumSum (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3AsScopeLsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsScopeLsaCksumSum
        (pi1RetValFsMIStdOspfv3AsScopeLsaCksumSum);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3OriginateNewLsas
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3OriginateNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3OriginateNewLsas (INT4 i4FsMIStdOspfv3ContextId,
                                     UINT4
                                     *pu4RetValFsMIStdOspfv3OriginateNewLsas)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3OriginateNewLsas (pu4RetValFsMIStdOspfv3OriginateNewLsas);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RxNewLsas
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RxNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RxNewLsas (INT4 i4FsMIStdOspfv3ContextId,
                              UINT4 *pu4RetValFsMIStdOspfv3RxNewLsas)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3RxNewLsas (pu4RetValFsMIStdOspfv3RxNewLsas);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ExtLsaCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3ExtLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ExtLsaCount (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 *pu4RetValFsMIStdOspfv3ExtLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3ExtLsaCount (pu4RetValFsMIStdOspfv3ExtLsaCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ExtAreaLsdbLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3ExtAreaLsdbLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ExtAreaLsdbLimit (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3ExtAreaLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3ExtAreaLsdbLimit (pi1RetValFsMIStdOspfv3ExtAreaLsdbLimit);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3MulticastExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3MulticastExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3MulticastExtensions (INT4 i4FsMIStdOspfv3ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIStdOspfv3MulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3MulticastExtensions
        (pRetValFsMIStdOspfv3MulticastExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3ExitOverflowInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ExitOverflowInterval (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4
                                         *pu4RetValFsMIStdOspfv3ExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3ExitOverflowInterval
        (pu4RetValFsMIStdOspfv3ExitOverflowInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3DemandExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3DemandExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3DemandExtensions (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3DemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3DemandExtensions (pi1RetValFsMIStdOspfv3DemandExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3TrafficEngineeringSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3TrafficEngineeringSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3TrafficEngineeringSupport (INT4 i4FsMIStdOspfv3ContextId,
                                              INT4
                                              *pi1RetValFsMIStdOspfv3TrafficEngineeringSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3TrafficEngineeringSupport
        (pi1RetValFsMIStdOspfv3TrafficEngineeringSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ReferenceBandwidth
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3ReferenceBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ReferenceBandwidth (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4
                                       *pu4RetValFsMIStdOspfv3ReferenceBandwidth)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3ReferenceBandwidth
        (pu4RetValFsMIStdOspfv3ReferenceBandwidth);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RestartSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RestartSupport (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 *pi1RetValFsMIStdOspfv3RestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3RestartSupport (pi1RetValFsMIStdOspfv3RestartSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RestartInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RestartInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RestartInterval (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 *pi1RetValFsMIStdOspfv3RestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3RestartInterval (pi1RetValFsMIStdOspfv3RestartInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RestartStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RestartStatus (INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 *pi1RetValFsMIStdOspfv3RestartStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3RestartStatus (pi1RetValFsMIStdOspfv3RestartStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RestartAge
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RestartAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RestartAge (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 *pi1RetValFsMIStdOspfv3RestartAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3RestartAge (pi1RetValFsMIStdOspfv3RestartAge);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3RestartExitReason
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3RestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3RestartExitReason (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3RestartExitReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3RestartExitReason (pi1RetValFsMIStdOspfv3RestartExitReason);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3Status
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIStdOspfv3Status
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3Status (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 *pi1RetValFsMIStdOspfv3Status)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    pV3OspfCxt = gV3OsRtr.apV3OspfCxt[i4FsMIStdOspfv3ContextId];

    *pi1RetValFsMIStdOspfv3Status = pV3OspfCxt->contextStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3AreaTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                        UINT4 *pu4FsMIStdOspfv3AreaId)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal = nmhGetFirstIndexOspfv3AreaTable (pu4FsMIStdOspfv3AreaId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3AreaTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId
                nextFsMIStdOspfv3AreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3AreaTable (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 *pi4NextFsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       UINT4 *pu4NextFsMIStdOspfv3AreaId)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexOspfv3AreaTable (u4FsMIStdOspfv3AreaId,
                                                   pu4NextFsMIStdOspfv3AreaId);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AreaTable (pu4NextFsMIStdOspfv3AreaId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3ImportAsExtern
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3ImportAsExtern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3ImportAsExtern (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3AreaId,
                                   INT4 *pi1RetValFsMIStdOspfv3ImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3ImportAsExtern (u4FsMIStdOspfv3AreaId,
                                    pi1RetValFsMIStdOspfv3ImportAsExtern);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaSpfRuns
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaSpfRuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaSpfRuns (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3AreaId,
                                UINT4 *pu4RetValFsMIStdOspfv3AreaSpfRuns)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaSpfRuns (u4FsMIStdOspfv3AreaId,
                                 pu4RetValFsMIStdOspfv3AreaSpfRuns);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaBdrRtrCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaBdrRtrCount (INT4 i4FsMIStdOspfv3ContextId,
                                    UINT4 u4FsMIStdOspfv3AreaId,
                                    UINT4
                                    *pu4RetValFsMIStdOspfv3AreaBdrRtrCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaBdrRtrCount (u4FsMIStdOspfv3AreaId,
                                     pu4RetValFsMIStdOspfv3AreaBdrRtrCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaAsBdrRtrCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaAsBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaAsBdrRtrCount (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3AreaId,
                                      UINT4
                                      *pu4RetValFsMIStdOspfv3AreaAsBdrRtrCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaAsBdrRtrCount (u4FsMIStdOspfv3AreaId,
                                       pu4RetValFsMIStdOspfv3AreaAsBdrRtrCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaScopeLsaCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaScopeLsaCount (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3AreaId,
                                      UINT4
                                      *pu4RetValFsMIStdOspfv3AreaScopeLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaScopeLsaCount (u4FsMIStdOspfv3AreaId,
                                       pu4RetValFsMIStdOspfv3AreaScopeLsaCount);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaScopeLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaScopeLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaScopeLsaCksumSum (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3AreaId,
                                         INT4
                                         *pi1RetValFsMIStdOspfv3AreaScopeLsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaScopeLsaCksumSum (u4FsMIStdOspfv3AreaId,
                                          pi1RetValFsMIStdOspfv3AreaScopeLsaCksumSum);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaSummary
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaSummary
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaSummary (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3AreaId,
                                INT4 *pi1RetValFsMIStdOspfv3AreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaSummary (u4FsMIStdOspfv3AreaId,
                                 pi1RetValFsMIStdOspfv3AreaSummary);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3StubMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3StubMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3StubMetric (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIStdOspfv3AreaId,
                               INT4 *pi1RetValFsMIStdOspfv3StubMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3StubMetric (u4FsMIStdOspfv3AreaId,
                                pi1RetValFsMIStdOspfv3StubMetric);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaNssaTranslatorRole
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaNssaTranslatorRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorRole (INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIStdOspfv3AreaId,
                                           INT4
                                           *pi1RetValFsMIStdOspfv3AreaNssaTranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaNssaTranslatorRole (u4FsMIStdOspfv3AreaId,
                                            pi1RetValFsMIStdOspfv3AreaNssaTranslatorRole);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaNssaTranslatorState
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaNssaTranslatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorState (INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIStdOspfv3AreaId,
                                            INT4
                                            *pi1RetValFsMIStdOspfv3AreaNssaTranslatorState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaNssaTranslatorState (u4FsMIStdOspfv3AreaId,
                                             pi1RetValFsMIStdOspfv3AreaNssaTranslatorState);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval (INT4
                                                        i4FsMIStdOspfv3ContextId,
                                                        UINT4
                                                        u4FsMIStdOspfv3AreaId,
                                                        UINT4
                                                        *pu4RetValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaNssaTranslatorStabilityInterval (u4FsMIStdOspfv3AreaId,
                                                         pu4RetValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaNssaTranslatorEvents
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaNssaTranslatorEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorEvents (INT4 i4FsMIStdOspfv3ContextId,
                                             UINT4 u4FsMIStdOspfv3AreaId,
                                             UINT4
                                             *pu4RetValFsMIStdOspfv3AreaNssaTranslatorEvents)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaNssaTranslatorEvents (u4FsMIStdOspfv3AreaId,
                                              pu4RetValFsMIStdOspfv3AreaNssaTranslatorEvents);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaStubMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaStubMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaStubMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3AreaStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaStubMetricType (u4FsMIStdOspfv3AreaId,
                                        pi1RetValFsMIStdOspfv3AreaStubMetricType);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                retValFsMIStdOspfv3AreaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaStatus (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIStdOspfv3AreaId,
                               INT4 *pi1RetValFsMIStdOspfv3AreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaStatus (u4FsMIStdOspfv3AreaId,
                                pi1RetValFsMIStdOspfv3AreaStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3AsLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3AsLsdbTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                          UINT4 *pu4FsMIStdOspfv3AsLsdbType,
                                          UINT4 *pu4FsMIStdOspfv3AsLsdbRouterId,
                                          UINT4 *pu4FsMIStdOspfv3AsLsdbLsid)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AsLsdbTable (pu4FsMIStdOspfv3AsLsdbType,
                                                   pu4FsMIStdOspfv3AsLsdbRouterId,
                                                   pu4FsMIStdOspfv3AsLsdbLsid);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3AsLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                nextFsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                nextFsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid
                nextFsMIStdOspfv3AsLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3AsLsdbTable (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4 *pi4NextFsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3AsLsdbType,
                                         UINT4 *pu4NextFsMIStdOspfv3AsLsdbType,
                                         UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                                         UINT4
                                         *pu4NextFsMIStdOspfv3AsLsdbRouterId,
                                         UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                                         UINT4 *pu4NextFsMIStdOspfv3AsLsdbLsid)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexOspfv3AsLsdbTable (u4FsMIStdOspfv3AsLsdbType,
                                                     pu4NextFsMIStdOspfv3AsLsdbType,
                                                     u4FsMIStdOspfv3AsLsdbRouterId,
                                                     pu4NextFsMIStdOspfv3AsLsdbRouterId,
                                                     u4FsMIStdOspfv3AsLsdbLsid,
                                                     pu4NextFsMIStdOspfv3AsLsdbLsid);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AsLsdbTable
                (pu4NextFsMIStdOspfv3AsLsdbType,
                 pu4NextFsMIStdOspfv3AsLsdbRouterId,
                 pu4NextFsMIStdOspfv3AsLsdbLsid);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsLsdbSequence
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid

                The Object 
                retValFsMIStdOspfv3AsLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsLsdbSequence (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3AsLsdbType,
                                   UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                                   UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                                   INT4 *pi1RetValFsMIStdOspfv3AsLsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsLsdbSequence (u4FsMIStdOspfv3AsLsdbType,
                                    u4FsMIStdOspfv3AsLsdbRouterId,
                                    u4FsMIStdOspfv3AsLsdbLsid,
                                    pi1RetValFsMIStdOspfv3AsLsdbSequence);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsLsdbAge
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid

                The Object 
                retValFsMIStdOspfv3AsLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsLsdbAge (INT4 i4FsMIStdOspfv3ContextId,
                              UINT4 u4FsMIStdOspfv3AsLsdbType,
                              UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                              UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                              INT4 *pi1RetValFsMIStdOspfv3AsLsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsLsdbAge (u4FsMIStdOspfv3AsLsdbType,
                               u4FsMIStdOspfv3AsLsdbRouterId,
                               u4FsMIStdOspfv3AsLsdbLsid,
                               pi1RetValFsMIStdOspfv3AsLsdbAge);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsLsdbChecksum
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid

                The Object 
                retValFsMIStdOspfv3AsLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsLsdbChecksum (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3AsLsdbType,
                                   UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                                   UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                                   INT4 *pi1RetValFsMIStdOspfv3AsLsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsLsdbChecksum (u4FsMIStdOspfv3AsLsdbType,
                                    u4FsMIStdOspfv3AsLsdbRouterId,
                                    u4FsMIStdOspfv3AsLsdbLsid,
                                    pi1RetValFsMIStdOspfv3AsLsdbChecksum);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsLsdbAdvertisement
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid

                The Object 
                retValFsMIStdOspfv3AsLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsLsdbAdvertisement (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4 u4FsMIStdOspfv3AsLsdbType,
                                        UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                                        UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pRetValFsMIStdOspfv3AsLsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsLsdbAdvertisement (u4FsMIStdOspfv3AsLsdbType,
                                         u4FsMIStdOspfv3AsLsdbRouterId,
                                         u4FsMIStdOspfv3AsLsdbLsid,
                                         pRetValFsMIStdOspfv3AsLsdbAdvertisement);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AsLsdbTypeKnown
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid

                The Object 
                retValFsMIStdOspfv3AsLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AsLsdbTypeKnown (INT4 i4FsMIStdOspfv3ContextId,
                                    UINT4 u4FsMIStdOspfv3AsLsdbType,
                                    UINT4 u4FsMIStdOspfv3AsLsdbRouterId,
                                    UINT4 u4FsMIStdOspfv3AsLsdbLsid,
                                    INT4 *pi1RetValFsMIStdOspfv3AsLsdbTypeKnown)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AsLsdbTypeKnown (u4FsMIStdOspfv3AsLsdbType,
                                     u4FsMIStdOspfv3AsLsdbRouterId,
                                     u4FsMIStdOspfv3AsLsdbLsid,
                                     pi1RetValFsMIStdOspfv3AsLsdbTypeKnown);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3AreaLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaLsdbTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                            UINT4
                                            *pu4FsMIStdOspfv3AreaLsdbAreaId,
                                            UINT4 *pu4FsMIStdOspfv3AreaLsdbType,
                                            UINT4
                                            *pu4FsMIStdOspfv3AreaLsdbRouterId,
                                            UINT4 *pu4FsMIStdOspfv3AreaLsdbLsid)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AreaLsdbTable
                (pu4FsMIStdOspfv3AreaLsdbAreaId, pu4FsMIStdOspfv3AreaLsdbType,
                 pu4FsMIStdOspfv3AreaLsdbRouterId,
                 pu4FsMIStdOspfv3AreaLsdbLsid);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3AreaLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                nextFsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                nextFsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                nextFsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid
                nextFsMIStdOspfv3AreaLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3AreaLsdbTable (INT4 i4FsMIStdOspfv3ContextId,
                                           INT4 *pi4NextFsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3AreaLsdbAreaId,
                                           UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3AreaLsdbType,
                                           UINT4
                                           u4FsMIStdOspfv3AreaLsdbRouterId,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3AreaLsdbRouterId,
                                           UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3AreaLsdbLsid)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3AreaLsdbTable (u4FsMIStdOspfv3AreaLsdbAreaId,
                                                pu4NextFsMIStdOspfv3AreaLsdbAreaId,
                                                u4FsMIStdOspfv3AreaLsdbType,
                                                pu4NextFsMIStdOspfv3AreaLsdbType,
                                                u4FsMIStdOspfv3AreaLsdbRouterId,
                                                pu4NextFsMIStdOspfv3AreaLsdbRouterId,
                                                u4FsMIStdOspfv3AreaLsdbLsid,
                                                pu4NextFsMIStdOspfv3AreaLsdbLsid);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AreaLsdbTable
                (pu4NextFsMIStdOspfv3AreaLsdbAreaId,
                 pu4NextFsMIStdOspfv3AreaLsdbType,
                 pu4NextFsMIStdOspfv3AreaLsdbRouterId,
                 pu4NextFsMIStdOspfv3AreaLsdbLsid);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaLsdbSequence
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid

                The Object 
                retValFsMIStdOspfv3AreaLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaLsdbSequence (INT4 i4FsMIStdOspfv3ContextId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbRouterId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3AreaLsdbSequence)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaLsdbSequence (u4FsMIStdOspfv3AreaLsdbAreaId,
                                      u4FsMIStdOspfv3AreaLsdbType,
                                      u4FsMIStdOspfv3AreaLsdbRouterId,
                                      u4FsMIStdOspfv3AreaLsdbLsid,
                                      pi1RetValFsMIStdOspfv3AreaLsdbSequence);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaLsdbAge
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid

                The Object 
                retValFsMIStdOspfv3AreaLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaLsdbAge (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                UINT4 u4FsMIStdOspfv3AreaLsdbRouterId,
                                UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                INT4 *pi1RetValFsMIStdOspfv3AreaLsdbAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaLsdbAge (u4FsMIStdOspfv3AreaLsdbAreaId,
                                 u4FsMIStdOspfv3AreaLsdbType,
                                 u4FsMIStdOspfv3AreaLsdbRouterId,
                                 u4FsMIStdOspfv3AreaLsdbLsid,
                                 pi1RetValFsMIStdOspfv3AreaLsdbAge);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaLsdbChecksum
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid

                The Object 
                retValFsMIStdOspfv3AreaLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaLsdbChecksum (INT4 i4FsMIStdOspfv3ContextId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbRouterId,
                                     UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3AreaLsdbChecksum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaLsdbChecksum (u4FsMIStdOspfv3AreaLsdbAreaId,
                                      u4FsMIStdOspfv3AreaLsdbType,
                                      u4FsMIStdOspfv3AreaLsdbRouterId,
                                      u4FsMIStdOspfv3AreaLsdbLsid,
                                      pi1RetValFsMIStdOspfv3AreaLsdbChecksum);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaLsdbAdvertisement
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid

                The Object 
                retValFsMIStdOspfv3AreaLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaLsdbAdvertisement (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                          UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                          UINT4 u4FsMIStdOspfv3AreaLsdbRouterId,
                                          UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMIStdOspfv3AreaLsdbAdvertisement)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaLsdbAdvertisement (u4FsMIStdOspfv3AreaLsdbAreaId,
                                           u4FsMIStdOspfv3AreaLsdbType,
                                           u4FsMIStdOspfv3AreaLsdbRouterId,
                                           u4FsMIStdOspfv3AreaLsdbLsid,
                                           pRetValFsMIStdOspfv3AreaLsdbAdvertisement);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaLsdbTypeKnown
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid

                The Object 
                retValFsMIStdOspfv3AreaLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaLsdbTypeKnown (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3AreaLsdbAreaId,
                                      UINT4 u4FsMIStdOspfv3AreaLsdbType,
                                      UINT4 u4FsMIStdOspfv3AreaLsdbRouterId,
                                      UINT4 u4FsMIStdOspfv3AreaLsdbLsid,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3AreaLsdbTypeKnown)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaLsdbTypeKnown (u4FsMIStdOspfv3AreaLsdbAreaId,
                                       u4FsMIStdOspfv3AreaLsdbType,
                                       u4FsMIStdOspfv3AreaLsdbRouterId,
                                       u4FsMIStdOspfv3AreaLsdbLsid,
                                       pi1RetValFsMIStdOspfv3AreaLsdbTypeKnown);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3LinkLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3LinkLsdbTable (INT4
                                            *pi4FsMIStdOspfv3LinkLsdbIfIndex,
                                            UINT4 *pu4FsMIStdOspfv3LinkLsdbType,
                                            UINT4
                                            *pu4FsMIStdOspfv3LinkLsdbRouterId,
                                            UINT4 *pu4FsMIStdOspfv3LinkLsdbLsid)
{
    return (nmhGetFirstIndexOspfv3LinkLsdbTable
            (pi4FsMIStdOspfv3LinkLsdbIfIndex, pu4FsMIStdOspfv3LinkLsdbType,
             pu4FsMIStdOspfv3LinkLsdbRouterId, pu4FsMIStdOspfv3LinkLsdbLsid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3LinkLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                nextFsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                nextFsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                nextFsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid
                nextFsMIStdOspfv3LinkLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3LinkLsdbTable (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                           INT4
                                           *pi4NextFsMIStdOspfv3LinkLsdbIfIndex,
                                           UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3LinkLsdbType,
                                           UINT4
                                           u4FsMIStdOspfv3LinkLsdbRouterId,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3LinkLsdbRouterId,
                                           UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                           UINT4
                                           *pu4NextFsMIStdOspfv3LinkLsdbLsid)
{
    return (nmhGetNextIndexOspfv3LinkLsdbTable (i4FsMIStdOspfv3LinkLsdbIfIndex,
                                                pi4NextFsMIStdOspfv3LinkLsdbIfIndex,
                                                u4FsMIStdOspfv3LinkLsdbType,
                                                pu4NextFsMIStdOspfv3LinkLsdbType,
                                                u4FsMIStdOspfv3LinkLsdbRouterId,
                                                pu4NextFsMIStdOspfv3LinkLsdbRouterId,
                                                u4FsMIStdOspfv3LinkLsdbLsid,
                                                pu4NextFsMIStdOspfv3LinkLsdbLsid));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbSequence
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbSequence (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3LinkLsdbSequence)
{
    return (nmhGetOspfv3LinkLsdbSequence
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid,
             pi1RetValFsMIStdOspfv3LinkLsdbSequence));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbAge
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbAge (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                INT4 *pi1RetValFsMIStdOspfv3LinkLsdbAge)
{
    return (nmhGetOspfv3LinkLsdbAge
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid,
             pi1RetValFsMIStdOspfv3LinkLsdbAge));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbChecksum
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbChecksum (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                     UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3LinkLsdbChecksum)
{
    return (nmhGetOspfv3LinkLsdbChecksum
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid,
             pi1RetValFsMIStdOspfv3LinkLsdbChecksum));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbAdvertisement
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbAdvertisement (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                          UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                          UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                          UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsMIStdOspfv3LinkLsdbAdvertisement)
{
    return (nmhGetOspfv3LinkLsdbAdvertisement
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid,
             pRetValFsMIStdOspfv3LinkLsdbAdvertisement));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbTypeKnown
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbTypeKnown (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3LinkLsdbTypeKnown)
{
    return (nmhGetOspfv3LinkLsdbTypeKnown
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid,
             pi1RetValFsMIStdOspfv3LinkLsdbTypeKnown));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3LinkLsdbContextId
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid

                The Object 
                retValFsMIStdOspfv3LinkLsdbContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3LinkLsdbContextId (INT4 i4FsMIStdOspfv3LinkLsdbIfIndex,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbType,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbRouterId,
                                      UINT4 u4FsMIStdOspfv3LinkLsdbLsid,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3LinkLsdbContextId)
{
    tV3OsInterface     *pInterface = NULL;

    UNUSED_PARAM (u4FsMIStdOspfv3LinkLsdbType);
    UNUSED_PARAM (u4FsMIStdOspfv3LinkLsdbRouterId);
    UNUSED_PARAM (u4FsMIStdOspfv3LinkLsdbLsid);

    if ((pInterface =
         V3GetFindIf ((UINT4) i4FsMIStdOspfv3LinkLsdbIfIndex)) != NULL)
    {
        *pi1RetValFsMIStdOspfv3LinkLsdbContextId = (INT4)
            pInterface->pArea->pV3OspfCxt->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3HostTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3HostTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                        INT4 *pi4FsMIStdOspfv3HostAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdOspfv3HostAddress)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3HostTable
                (pi4FsMIStdOspfv3HostAddressType, pFsMIStdOspfv3HostAddress);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3HostTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                nextFsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress
                nextFsMIStdOspfv3HostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3HostTable (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 *pi4NextFsMIStdOspfv3ContextId,
                                       INT4 i4FsMIStdOspfv3HostAddressType,
                                       INT4
                                       *pi4NextFsMIStdOspfv3HostAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdOspfv3HostAddress,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMIStdOspfv3HostAddress)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3HostTable (i4FsMIStdOspfv3HostAddressType,
                                            pi4NextFsMIStdOspfv3HostAddressType,
                                            pFsMIStdOspfv3HostAddress,
                                            pNextFsMIStdOspfv3HostAddress);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3HostTable
                (pi4NextFsMIStdOspfv3HostAddressType,
                 pNextFsMIStdOspfv3HostAddress);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3HostMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                retValFsMIStdOspfv3HostMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3HostMetric (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               INT4 *pi1RetValFsMIStdOspfv3HostMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3HostMetric (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                pi1RetValFsMIStdOspfv3HostMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3HostAreaID
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                retValFsMIStdOspfv3HostAreaID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3HostAreaID (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               UINT4 *pu4RetValFsMIStdOspfv3HostAreaID)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3HostAreaID (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                pu4RetValFsMIStdOspfv3HostAreaID);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3HostStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                retValFsMIStdOspfv3HostStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3HostStatus (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIStdOspfv3HostAddressType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIStdOspfv3HostAddress,
                               INT4 *pi1RetValFsMIStdOspfv3HostStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3HostStatus (i4FsMIStdOspfv3HostAddressType,
                                pFsMIStdOspfv3HostAddress,
                                pi1RetValFsMIStdOspfv3HostStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3IfTable
 Input       :  The Indices
                FsMIStdOspfv3IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3IfTable (INT4 *pi4FsMIStdOspfv3IfIndex)
{
    return (nmhGetFirstIndexOspfv3IfTable (pi4FsMIStdOspfv3IfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3IfTable
 Input       :  The Indices
                FsMIStdOspfv3IfIndex
                nextFsMIStdOspfv3IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3IfTable (INT4 i4FsMIStdOspfv3IfIndex,
                                     INT4 *pi4NextFsMIStdOspfv3IfIndex)
{
    return (nmhGetNextIndexOspfv3IfTable (i4FsMIStdOspfv3IfIndex,
                                          pi4NextFsMIStdOspfv3IfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfAreaId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfAreaId (INT4 i4FsMIStdOspfv3IfIndex,
                             UINT4 *pu4RetValFsMIStdOspfv3IfAreaId)
{
    return (nmhGetOspfv3IfAreaId
            (i4FsMIStdOspfv3IfIndex, pu4RetValFsMIStdOspfv3IfAreaId));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfType
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfType (INT4 i4FsMIStdOspfv3IfIndex,
                           INT4 *pi1RetValFsMIStdOspfv3IfType)
{
    return (nmhGetOspfv3IfType
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfType));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfAdminStat
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfAdminStat (INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 *pi1RetValFsMIStdOspfv3IfAdminStat)
{
    return (nmhGetOspfv3IfAdminStat
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfAdminStat));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfRtrPriority
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfRtrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfRtrPriority (INT4 i4FsMIStdOspfv3IfIndex,
                                  INT4 *pi1RetValFsMIStdOspfv3IfRtrPriority)
{
    return (nmhGetOspfv3IfRtrPriority
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfRtrPriority));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfTransitDelay (INT4 i4FsMIStdOspfv3IfIndex,
                                   INT4 *pi1RetValFsMIStdOspfv3IfTransitDelay)
{
    return (nmhGetOspfv3IfTransitDelay
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfTransitDelay));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfRetransInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3IfRetransInterval)
{
    return (nmhGetOspfv3IfRetransInterval
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfRetransInterval));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfHelloInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                    INT4 *pi1RetValFsMIStdOspfv3IfHelloInterval)
{
    return (nmhGetOspfv3IfHelloInterval
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfHelloInterval));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfRtrDeadInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3IfRtrDeadInterval)
{
    return (nmhGetOspfv3IfRtrDeadInterval
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfRtrDeadInterval));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfPollInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfPollInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                   UINT4 *pu4RetValFsMIStdOspfv3IfPollInterval)
{
    return (nmhGetOspfv3IfPollInterval
            (i4FsMIStdOspfv3IfIndex, pu4RetValFsMIStdOspfv3IfPollInterval));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfState
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfState (INT4 i4FsMIStdOspfv3IfIndex,
                            INT4 *pi1RetValFsMIStdOspfv3IfState)
{
    return (nmhGetOspfv3IfState
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfState));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfDesignatedRouter
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfDesignatedRouter (INT4 i4FsMIStdOspfv3IfIndex,
                                       UINT4
                                       *pu4RetValFsMIStdOspfv3IfDesignatedRouter)
{
    return (nmhGetOspfv3IfDesignatedRouter
            (i4FsMIStdOspfv3IfIndex, pu4RetValFsMIStdOspfv3IfDesignatedRouter));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfBackupDesignatedRouter
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfBackupDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfBackupDesignatedRouter (INT4 i4FsMIStdOspfv3IfIndex,
                                             UINT4
                                             *pu4RetValFsMIStdOspfv3IfBackupDesignatedRouter)
{
    return (nmhGetOspfv3IfBackupDesignatedRouter
            (i4FsMIStdOspfv3IfIndex,
             pu4RetValFsMIStdOspfv3IfBackupDesignatedRouter));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfEvents
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfEvents (INT4 i4FsMIStdOspfv3IfIndex,
                             UINT4 *pu4RetValFsMIStdOspfv3IfEvents)
{
    return (nmhGetOspfv3IfEvents
            (i4FsMIStdOspfv3IfIndex, pu4RetValFsMIStdOspfv3IfEvents));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfMulticastForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfMulticastForwarding (INT4 i4FsMIStdOspfv3IfIndex,
                                          INT4
                                          *pi1RetValFsMIStdOspfv3IfMulticastForwarding)
{
    return (nmhGetOspfv3IfMulticastForwarding
            (i4FsMIStdOspfv3IfIndex,
             pi1RetValFsMIStdOspfv3IfMulticastForwarding));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfDemand
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfDemand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfDemand (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 *pi1RetValFsMIStdOspfv3IfDemand)
{
    return (nmhGetOspfv3IfDemand
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfDemand));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfMetricValue
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfMetricValue (INT4 i4FsMIStdOspfv3IfIndex,
                                  INT4 *pi1RetValFsMIStdOspfv3IfMetricValue)
{
    return (nmhGetOspfv3IfMetricValue
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfMetricValue));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfLinkScopeLsaCount
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfLinkScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfLinkScopeLsaCount (INT4 i4FsMIStdOspfv3IfIndex,
                                        UINT4
                                        *pu4RetValFsMIStdOspfv3IfLinkScopeLsaCount)
{
    return (nmhGetOspfv3IfLinkScopeLsaCount
            (i4FsMIStdOspfv3IfIndex,
             pu4RetValFsMIStdOspfv3IfLinkScopeLsaCount));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfLinkLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfLinkLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfLinkLsaCksumSum (INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4
                                      *pi1RetValFsMIStdOspfv3IfLinkLsaCksumSum)
{
    return (nmhGetOspfv3IfLinkLsaCksumSum
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfLinkLsaCksumSum));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfInstId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfInstId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfInstId (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 *pi1RetValFsMIStdOspfv3IfInstId)
{
    return (nmhGetOspfv3IfInstId
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfInstId));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfDemandNbrProbe
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfDemandNbrProbe
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbe (INT4 i4FsMIStdOspfv3IfIndex,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3IfDemandNbrProbe)
{
    return (nmhGetOspfv3IfDemandNbrProbe
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfDemandNbrProbe));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbeRetxLimit (INT4 i4FsMIStdOspfv3IfIndex,
                                              UINT4
                                              *pu4RetValFsMIStdOspfv3IfDemandNbrProbeRetxLimit)
{
    return (nmhGetOspfv3IfDemandNbrProbeRetxLimit
            (i4FsMIStdOspfv3IfIndex,
             pu4RetValFsMIStdOspfv3IfDemandNbrProbeRetxLimit));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfDemandNbrProbeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbeInterval (INT4 i4FsMIStdOspfv3IfIndex,
                                             UINT4
                                             *pu4RetValFsMIStdOspfv3IfDemandNbrProbeInterval)
{
    return (nmhGetOspfv3IfDemandNbrProbeInterval
            (i4FsMIStdOspfv3IfIndex,
             pu4RetValFsMIStdOspfv3IfDemandNbrProbeInterval));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfContextId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfContextId (INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 *pi1RetValFsMIStdOspfv3IfContextId)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4FsMIStdOspfv3IfIndex)) != NULL)
    {
        *pi1RetValFsMIStdOspfv3IfContextId = (INT4)
            pInterface->pArea->pV3OspfCxt->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3IfStatus
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                retValFsMIStdOspfv3IfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3IfStatus (INT4 i4FsMIStdOspfv3IfIndex,
                             INT4 *pi1RetValFsMIStdOspfv3IfStatus)
{
    return (nmhGetOspfv3IfStatus
            (i4FsMIStdOspfv3IfIndex, pi1RetValFsMIStdOspfv3IfStatus));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3VirtIfTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3VirtIfTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                          UINT4 *pu4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 *pu4FsMIStdOspfv3VirtIfNeighbor)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3VirtIfTable (pu4FsMIStdOspfv3VirtIfAreaId,
                                                   pu4FsMIStdOspfv3VirtIfNeighbor);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3VirtIfTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                nextFsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
                nextFsMIStdOspfv3VirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3VirtIfTable (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4 *pi4NextFsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                         UINT4
                                         *pu4NextFsMIStdOspfv3VirtIfAreaId,
                                         UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                         UINT4
                                         *pu4NextFsMIStdOspfv3VirtIfNeighbor)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3VirtIfTable (u4FsMIStdOspfv3VirtIfAreaId,
                                              pu4NextFsMIStdOspfv3VirtIfAreaId,
                                              u4FsMIStdOspfv3VirtIfNeighbor,
                                              pu4NextFsMIStdOspfv3VirtIfNeighbor);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3VirtIfTable
                (pu4NextFsMIStdOspfv3VirtIfAreaId,
                 pu4NextFsMIStdOspfv3VirtIfNeighbor);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfIndex (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                INT4 *pi1RetValFsMIStdOspfv3VirtIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfIndex (u4FsMIStdOspfv3VirtIfAreaId,
                                 u4FsMIStdOspfv3VirtIfNeighbor,
                                 pi1RetValFsMIStdOspfv3VirtIfIndex);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfTransitDelay (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                       UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3VirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfTransitDelay (u4FsMIStdOspfv3VirtIfAreaId,
                                        u4FsMIStdOspfv3VirtIfNeighbor,
                                        pi1RetValFsMIStdOspfv3VirtIfTransitDelay);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfRetransInterval (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          *pi1RetValFsMIStdOspfv3VirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfRetransInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                           u4FsMIStdOspfv3VirtIfNeighbor,
                                           pi1RetValFsMIStdOspfv3VirtIfRetransInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfHelloInterval (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                        UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                        INT4
                                        *pi1RetValFsMIStdOspfv3VirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfHelloInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                         u4FsMIStdOspfv3VirtIfNeighbor,
                                         pi1RetValFsMIStdOspfv3VirtIfHelloInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfRtrDeadInterval (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          *pi1RetValFsMIStdOspfv3VirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfRtrDeadInterval (u4FsMIStdOspfv3VirtIfAreaId,
                                           u4FsMIStdOspfv3VirtIfNeighbor,
                                           pi1RetValFsMIStdOspfv3VirtIfRtrDeadInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfState
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfState (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                INT4 *pi1RetValFsMIStdOspfv3VirtIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfState (u4FsMIStdOspfv3VirtIfAreaId,
                                 u4FsMIStdOspfv3VirtIfNeighbor,
                                 pi1RetValFsMIStdOspfv3VirtIfState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfEvents
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfEvents (INT4 i4FsMIStdOspfv3ContextId,
                                 UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                 UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                 UINT4 *pu4RetValFsMIStdOspfv3VirtIfEvents)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfEvents (u4FsMIStdOspfv3VirtIfAreaId,
                                  u4FsMIStdOspfv3VirtIfNeighbor,
                                  pu4RetValFsMIStdOspfv3VirtIfEvents);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfLinkScopeLsaCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfLinkScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfLinkScopeLsaCount (INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                            UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                            UINT4
                                            *pu4RetValFsMIStdOspfv3VirtIfLinkScopeLsaCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfLinkScopeLsaCount (u4FsMIStdOspfv3VirtIfAreaId,
                                             u4FsMIStdOspfv3VirtIfNeighbor,
                                             pu4RetValFsMIStdOspfv3VirtIfLinkScopeLsaCount);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfLinkLsaCksumSum
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfLinkLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfLinkLsaCksumSum (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          *pi1RetValFsMIStdOspfv3VirtIfLinkLsaCksumSum)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfLinkLsaCksumSum (u4FsMIStdOspfv3VirtIfAreaId,
                                           u4FsMIStdOspfv3VirtIfNeighbor,
                                           pi1RetValFsMIStdOspfv3VirtIfLinkLsaCksumSum);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtIfStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIStdOspfv3VirtIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtIfStatus (INT4 i4FsMIStdOspfv3ContextId,
                                 UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                 UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                 INT4 *pi1RetValFsMIStdOspfv3VirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtIfStatus (u4FsMIStdOspfv3VirtIfAreaId,
                                  u4FsMIStdOspfv3VirtIfNeighbor,
                                  pi1RetValFsMIStdOspfv3VirtIfStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3NbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3NbrTable (INT4 *pi4FsMIStdOspfv3NbrIfIndex,
                                       UINT4 *pu4FsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3NbrTable (pi4FsMIStdOspfv3NbrIfIndex,
                                                pu4FsMIStdOspfv3NbrRtrId);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3NbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                nextFsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
                nextFsMIStdOspfv3NbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3NbrTable (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                      INT4 *pi4NextFsMIStdOspfv3NbrIfIndex,
                                      UINT4 u4FsMIStdOspfv3NbrRtrId,
                                      UINT4 *pu4NextFsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexOspfv3NbrTable (i4FsMIStdOspfv3NbrIfIndex,
                                                  pi4NextFsMIStdOspfv3NbrIfIndex,
                                                  u4FsMIStdOspfv3NbrRtrId,
                                                  pu4NextFsMIStdOspfv3NbrRtrId);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4V3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3NbrTable (pi4NextFsMIStdOspfv3NbrIfIndex,
                                                pu4NextFsMIStdOspfv3NbrRtrId);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4V3ContextId = (INT4) u4CxtId;
        }
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrAddressType
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrAddressType (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                   UINT4 u4FsMIStdOspfv3NbrRtrId,
                                   INT4 *pi1RetValFsMIStdOspfv3NbrAddressType)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrAddressType (i4FsMIStdOspfv3NbrIfIndex,
                                           u4FsMIStdOspfv3NbrRtrId,
                                           pi1RetValFsMIStdOspfv3NbrAddressType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrAddress
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrAddress (INT4 i4FsMIStdOspfv3NbrIfIndex,
                               UINT4 u4FsMIStdOspfv3NbrRtrId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIStdOspfv3NbrAddress)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrAddress (i4FsMIStdOspfv3NbrIfIndex,
                                       u4FsMIStdOspfv3NbrRtrId,
                                       pRetValFsMIStdOspfv3NbrAddress);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrOptions
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrOptions (INT4 i4FsMIStdOspfv3NbrIfIndex,
                               UINT4 u4FsMIStdOspfv3NbrRtrId,
                               INT4 *pi1RetValFsMIStdOspfv3NbrOptions)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrOptions (i4FsMIStdOspfv3NbrIfIndex,
                                       u4FsMIStdOspfv3NbrRtrId,
                                       pi1RetValFsMIStdOspfv3NbrOptions);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrPriority
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrPriority (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                UINT4 u4FsMIStdOspfv3NbrRtrId,
                                INT4 *pi1RetValFsMIStdOspfv3NbrPriority)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrPriority (i4FsMIStdOspfv3NbrIfIndex,
                                        u4FsMIStdOspfv3NbrRtrId,
                                        pi1RetValFsMIStdOspfv3NbrPriority);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrState
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrState (INT4 i4FsMIStdOspfv3NbrIfIndex,
                             UINT4 u4FsMIStdOspfv3NbrRtrId,
                             INT4 *pi1RetValFsMIStdOspfv3NbrState)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrState (i4FsMIStdOspfv3NbrIfIndex,
                                     u4FsMIStdOspfv3NbrRtrId,
                                     pi1RetValFsMIStdOspfv3NbrState);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrEvents
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrEvents (INT4 i4FsMIStdOspfv3NbrIfIndex,
                              UINT4 u4FsMIStdOspfv3NbrRtrId,
                              UINT4 *pu4RetValFsMIStdOspfv3NbrEvents)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrEvents (i4FsMIStdOspfv3NbrIfIndex,
                                      u4FsMIStdOspfv3NbrRtrId,
                                      pu4RetValFsMIStdOspfv3NbrEvents);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrLsRetransQLen
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrLsRetransQLen (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                     UINT4 u4FsMIStdOspfv3NbrRtrId,
                                     UINT4
                                     *pu4RetValFsMIStdOspfv3NbrLsRetransQLen)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrLsRetransQLen (i4FsMIStdOspfv3NbrIfIndex,
                                             u4FsMIStdOspfv3NbrRtrId,
                                             pu4RetValFsMIStdOspfv3NbrLsRetransQLen);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrHelloSuppressed
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrHelloSuppressed (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                       UINT4 u4FsMIStdOspfv3NbrRtrId,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3NbrHelloSuppressed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrHelloSuppressed (i4FsMIStdOspfv3NbrIfIndex,
                                               u4FsMIStdOspfv3NbrRtrId,
                                               pi1RetValFsMIStdOspfv3NbrHelloSuppressed);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrIfId
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrIfId (INT4 i4FsMIStdOspfv3NbrIfIndex,
                            UINT4 u4FsMIStdOspfv3NbrRtrId,
                            INT4 *pi1RetValFsMIStdOspfv3NbrIfId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrIfId (i4FsMIStdOspfv3NbrIfIndex,
                                    u4FsMIStdOspfv3NbrRtrId,
                                    pi1RetValFsMIStdOspfv3NbrIfId);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrRestartHelperStatus
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrRestartHelperStatus (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                           UINT4 u4FsMIStdOspfv3NbrRtrId,
                                           INT4
                                           *pi1RetValFsMIStdOspfv3NbrRestartHelperStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrRestartHelperStatus
        (i4FsMIStdOspfv3NbrIfIndex, u4FsMIStdOspfv3NbrRtrId,
         pi1RetValFsMIStdOspfv3NbrRestartHelperStatus);

    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrRestartHelperAge
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrRestartHelperAge (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                        UINT4 u4FsMIStdOspfv3NbrRtrId,
                                        INT4
                                        *pi1RetValFsMIStdOspfv3NbrRestartHelperAge)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrRestartHelperAge
        (i4FsMIStdOspfv3NbrIfIndex, u4FsMIStdOspfv3NbrRtrId,
         pi1RetValFsMIStdOspfv3NbrRestartHelperAge);

    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrRestartHelperExitReason
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrRestartHelperExitReason (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                               UINT4 u4FsMIStdOspfv3NbrRtrId,
                                               INT4
                                               *pi1RetValFsMIStdOspfv3NbrRestartHelperExitReason)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbrRestartHelperExitReason
        (i4FsMIStdOspfv3NbrIfIndex, u4FsMIStdOspfv3NbrRtrId,
         pi1RetValFsMIStdOspfv3NbrRestartHelperExitReason);

    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbrContextId
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object 
                retValFsMIStdOspfv3NbrContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbrContextId (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                 UINT4 u4FsMIStdOspfv3NbrRtrId,
                                 INT4 *pi1RetValFsMIStdOspfv3NbrContextId)
{
    tV3OsInterface     *pInterface = NULL;

    UNUSED_PARAM (u4FsMIStdOspfv3NbrRtrId);

    if ((pInterface = V3GetFindIf ((UINT4) i4FsMIStdOspfv3NbrIfIndex)) != NULL)
    {
        *pi1RetValFsMIStdOspfv3NbrContextId = (INT4)
            pInterface->pArea->pV3OspfCxt->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3NbmaNbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3NbmaNbrTable (INT4 *pi4FsMIStdOspfv3NbmaNbrIfIndex,
                                           INT4
                                           *pi4FsMIStdOspfv3NbmaNbrAddressType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdOspfv3NbmaNbrAddress)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3NbmaNbrTable
                (pi4FsMIStdOspfv3NbmaNbrIfIndex,
                 pi4FsMIStdOspfv3NbmaNbrAddressType,
                 pFsMIStdOspfv3NbmaNbrAddress);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3NbmaNbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                nextFsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                nextFsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress
                nextFsMIStdOspfv3NbmaNbrAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3NbmaNbrTable (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                          INT4
                                          *pi4NextFsMIStdOspfv3NbmaNbrIfIndex,
                                          INT4
                                          i4FsMIStdOspfv3NbmaNbrAddressType,
                                          INT4
                                          *pi4NextFsMIStdOspfv3NbmaNbrAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdOspfv3NbmaNbrAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMIStdOspfv3NbmaNbrAddress)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4V3ContextId, &u4CxtId) == OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3NbmaNbrTable (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                               pi4NextFsMIStdOspfv3NbmaNbrIfIndex,
                                               i4FsMIStdOspfv3NbmaNbrAddressType,
                                               pi4NextFsMIStdOspfv3NbmaNbrAddressType,
                                               pFsMIStdOspfv3NbmaNbrAddress,
                                               pNextFsMIStdOspfv3NbmaNbrAddress);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4V3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3NbmaNbrTable
                (pi4NextFsMIStdOspfv3NbmaNbrIfIndex,
                 pi4NextFsMIStdOspfv3NbmaNbrAddressType,
                 pNextFsMIStdOspfv3NbmaNbrAddress);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4V3ContextId = (INT4) u4CxtId;
        }
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrPriority
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrPriority (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIStdOspfv3NbmaNbrAddress,
                                    INT4 *pi1RetValFsMIStdOspfv3NbmaNbrPriority)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbmaNbrPriority (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                            i4FsMIStdOspfv3NbmaNbrAddressType,
                                            pFsMIStdOspfv3NbmaNbrAddress,
                                            pi1RetValFsMIStdOspfv3NbmaNbrPriority);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrRtrId
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrRtrId (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                 INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdOspfv3NbmaNbrAddress,
                                 UINT4 *pu4RetValFsMIStdOspfv3NbmaNbrRtrId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbmaNbrRtrId (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                         i4FsMIStdOspfv3NbmaNbrAddressType,
                                         pFsMIStdOspfv3NbmaNbrAddress,
                                         pu4RetValFsMIStdOspfv3NbmaNbrRtrId);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrState
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrState (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                 INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsMIStdOspfv3NbmaNbrAddress,
                                 INT4 *pi1RetValFsMIStdOspfv3NbmaNbrState)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbmaNbrState (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                         i4FsMIStdOspfv3NbmaNbrAddressType,
                                         pFsMIStdOspfv3NbmaNbrAddress,
                                         pi1RetValFsMIStdOspfv3NbmaNbrState);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrStorageType
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrStorageType (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                       INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdOspfv3NbmaNbrAddress,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3NbmaNbrStorageType)
{
    return (nmhGetOspfv3NbmaNbrStorageType
            (i4FsMIStdOspfv3NbmaNbrIfIndex, i4FsMIStdOspfv3NbmaNbrAddressType,
             pFsMIStdOspfv3NbmaNbrAddress,
             pi1RetValFsMIStdOspfv3NbmaNbrStorageType));
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrContextId
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrContextId (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                     INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdOspfv3NbmaNbrAddress,
                                     INT4
                                     *pi1RetValFsMIStdOspfv3NbmaNbrContextId)
{
    tV3OsInterface     *pInterface = NULL;

    UNUSED_PARAM (i4FsMIStdOspfv3NbmaNbrAddressType);
    UNUSED_PARAM (pFsMIStdOspfv3NbmaNbrAddress);

    if ((pInterface =
         V3GetFindIf ((UINT4) i4FsMIStdOspfv3NbmaNbrIfIndex)) != NULL)
    {
        *pi1RetValFsMIStdOspfv3NbmaNbrContextId = (INT4)
            pInterface->pArea->pV3OspfCxt->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3NbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                retValFsMIStdOspfv3NbmaNbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3NbmaNbrStatus (INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                  INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdOspfv3NbmaNbrAddress,
                                  INT4 *pi1RetValFsMIStdOspfv3NbmaNbrStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetOspfv3NbmaNbrStatus (i4FsMIStdOspfv3NbmaNbrIfIndex,
                                          i4FsMIStdOspfv3NbmaNbrAddressType,
                                          pFsMIStdOspfv3NbmaNbrAddress,
                                          pi1RetValFsMIStdOspfv3NbmaNbrStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3VirtNbrTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3VirtNbrTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                           UINT4 *pu4FsMIStdOspfv3VirtNbrArea,
                                           UINT4 *pu4FsMIStdOspfv3VirtNbrRtrId)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3VirtIfTable (pu4FsMIStdOspfv3VirtNbrArea,
                                                   pu4FsMIStdOspfv3VirtNbrRtrId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3VirtNbrTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                nextFsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId
                nextFsMIStdOspfv3VirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3VirtNbrTable (INT4 i4FsMIStdOspfv3ContextId,
                                          INT4 *pi4NextFsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                          UINT4
                                          *pu4NextFsMIStdOspfv3VirtNbrArea,
                                          UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                          UINT4
                                          *pu4NextFsMIStdOspfv3VirtNbrRtrId)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3VirtNbrTable (u4FsMIStdOspfv3VirtNbrArea,
                                               pu4NextFsMIStdOspfv3VirtNbrArea,
                                               u4FsMIStdOspfv3VirtNbrRtrId,
                                               pu4NextFsMIStdOspfv3VirtNbrRtrId);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3VirtIfTable
                (pu4NextFsMIStdOspfv3VirtNbrArea,
                 pu4NextFsMIStdOspfv3VirtNbrRtrId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrIfIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrIfIndex (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                   UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                   INT4 *pi1RetValFsMIStdOspfv3VirtNbrIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrIfIndex (u4FsMIStdOspfv3VirtNbrArea,
                                    u4FsMIStdOspfv3VirtNbrRtrId,
                                    pi1RetValFsMIStdOspfv3VirtNbrIfIndex);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrAddressType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrAddressType (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                       UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                       INT4
                                       *pi1RetValFsMIStdOspfv3VirtNbrAddressType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrAddressType (u4FsMIStdOspfv3VirtNbrArea,
                                        u4FsMIStdOspfv3VirtNbrRtrId,
                                        pi1RetValFsMIStdOspfv3VirtNbrAddressType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrAddress
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrAddress (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                   UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMIStdOspfv3VirtNbrAddress)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrAddress (u4FsMIStdOspfv3VirtNbrArea,
                                    u4FsMIStdOspfv3VirtNbrRtrId,
                                    pRetValFsMIStdOspfv3VirtNbrAddress);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrOptions
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrOptions (INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                   UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                   INT4 *pi1RetValFsMIStdOspfv3VirtNbrOptions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrOptions (u4FsMIStdOspfv3VirtNbrArea,
                                    u4FsMIStdOspfv3VirtNbrRtrId,
                                    pi1RetValFsMIStdOspfv3VirtNbrOptions);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrState
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrState (INT4 i4FsMIStdOspfv3ContextId,
                                 UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                 UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                 INT4 *pi1RetValFsMIStdOspfv3VirtNbrState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrState (u4FsMIStdOspfv3VirtNbrArea,
                                  u4FsMIStdOspfv3VirtNbrRtrId,
                                  pi1RetValFsMIStdOspfv3VirtNbrState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrEvents
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrEvents (INT4 i4FsMIStdOspfv3ContextId,
                                  UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                  UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                  UINT4 *pu4RetValFsMIStdOspfv3VirtNbrEvents)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrEvents (u4FsMIStdOspfv3VirtNbrArea,
                                   u4FsMIStdOspfv3VirtNbrRtrId,
                                   pu4RetValFsMIStdOspfv3VirtNbrEvents);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrLsRetransQLen
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrLsRetransQLen (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                         UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                         UINT4
                                         *pu4RetValFsMIStdOspfv3VirtNbrLsRetransQLen)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrLsRetransQLen (u4FsMIStdOspfv3VirtNbrArea,
                                          u4FsMIStdOspfv3VirtNbrRtrId,
                                          pu4RetValFsMIStdOspfv3VirtNbrLsRetransQLen);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrHelloSuppressed
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrHelloSuppressed (INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                           UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                           INT4
                                           *pi1RetValFsMIStdOspfv3VirtNbrHelloSuppressed)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrHelloSuppressed (u4FsMIStdOspfv3VirtNbrArea,
                                            u4FsMIStdOspfv3VirtNbrRtrId,
                                            pi1RetValFsMIStdOspfv3VirtNbrHelloSuppressed);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrIfId
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrIfId (INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                INT4 *pi1RetValFsMIStdOspfv3VirtNbrIfId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrIfId (u4FsMIStdOspfv3VirtNbrArea,
                                 u4FsMIStdOspfv3VirtNbrRtrId,
                                 pi1RetValFsMIStdOspfv3VirtNbrIfId);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrRestartHelperStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperStatus (INT4 i4FsMIStdOspfv3ContextId,
                                               UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                               UINT4
                                               u4FsMIStdOspfv3VirtNbrRtrId,
                                               INT4
                                               *pi1RetValFsMIStdOspfv3VirtNbrRestartHelperStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrRestartHelperStatus (u4FsMIStdOspfv3VirtNbrArea,
                                                u4FsMIStdOspfv3VirtNbrRtrId,
                                                pi1RetValFsMIStdOspfv3VirtNbrRestartHelperStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrRestartHelperAge
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperAge (INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIStdOspfv3VirtNbrArea,
                                            UINT4 u4FsMIStdOspfv3VirtNbrRtrId,
                                            INT4
                                            *pi1RetValFsMIStdOspfv3VirtNbrRestartHelperAge)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrRestartHelperAge (u4FsMIStdOspfv3VirtNbrArea,
                                             u4FsMIStdOspfv3VirtNbrRtrId,
                                             pi1RetValFsMIStdOspfv3VirtNbrRestartHelperAge);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3VirtNbrRestartHelperExitReason
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId

                The Object 
                retValFsMIStdOspfv3VirtNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperExitReason (INT4
                                                   i4FsMIStdOspfv3ContextId,
                                                   UINT4
                                                   u4FsMIStdOspfv3VirtNbrArea,
                                                   UINT4
                                                   u4FsMIStdOspfv3VirtNbrRtrId,
                                                   INT4
                                                   *pi1RetValFsMIStdOspfv3VirtNbrRestartHelperExitReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3VirtNbrRestartHelperExitReason (u4FsMIStdOspfv3VirtNbrArea,
                                                    u4FsMIStdOspfv3VirtNbrRtrId,
                                                    pi1RetValFsMIStdOspfv3VirtNbrRestartHelperExitReason);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIStdOspfv3AreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaAggregateTable (INT4
                                                 *pi4FsMIStdOspfv3ContextId,
                                                 UINT4
                                                 *pu4FsMIStdOspfv3AreaAggregateAreaID,
                                                 INT4
                                                 *pi4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                                 INT4
                                                 *pi4FsMIStdOspfv3AreaAggregatePrefixType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIStdOspfv3AreaAggregatePrefix,
                                                 UINT4
                                                 *pu4FsMIStdOspfv3AreaAggregatePrefixLength)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AreaAggregateTable
                (pu4FsMIStdOspfv3AreaAggregateAreaID,
                 pi4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                 pi4FsMIStdOspfv3AreaAggregatePrefixType,
                 pFsMIStdOspfv3AreaAggregatePrefix,
                 pu4FsMIStdOspfv3AreaAggregatePrefixLength);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIStdOspfv3AreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                nextFsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                nextFsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                nextFsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                nextFsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength
                nextFsMIStdOspfv3AreaAggregatePrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIStdOspfv3AreaAggregateTable (INT4 i4FsMIStdOspfv3ContextId,
                                                INT4
                                                *pi4NextFsMIStdOspfv3ContextId,
                                                UINT4
                                                u4FsMIStdOspfv3AreaAggregateAreaID,
                                                UINT4
                                                *pu4NextFsMIStdOspfv3AreaAggregateAreaID,
                                                INT4
                                                i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                                INT4
                                                *pi4NextFsMIStdOspfv3AreaAggregateAreaLsdbType,
                                                INT4
                                                i4FsMIStdOspfv3AreaAggregatePrefixType,
                                                INT4
                                                *pi4NextFsMIStdOspfv3AreaAggregatePrefixType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIStdOspfv3AreaAggregatePrefix,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFsMIStdOspfv3AreaAggregatePrefix,
                                                UINT4
                                                u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                                UINT4
                                                *pu4NextFsMIStdOspfv3AreaAggregatePrefixLength)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexOspfv3AreaAggregateTable
            (u4FsMIStdOspfv3AreaAggregateAreaID,
             pu4NextFsMIStdOspfv3AreaAggregateAreaID,
             i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
             pi4NextFsMIStdOspfv3AreaAggregateAreaLsdbType,
             i4FsMIStdOspfv3AreaAggregatePrefixType,
             pi4NextFsMIStdOspfv3AreaAggregatePrefixType,
             pFsMIStdOspfv3AreaAggregatePrefix,
             pNextFsMIStdOspfv3AreaAggregatePrefix,
             u4FsMIStdOspfv3AreaAggregatePrefixLength,
             pu4NextFsMIStdOspfv3AreaAggregatePrefixLength);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexOspfv3AreaAggregateTable
                (pu4NextFsMIStdOspfv3AreaAggregateAreaID,
                 pi4NextFsMIStdOspfv3AreaAggregateAreaLsdbType,
                 pi4NextFsMIStdOspfv3AreaAggregatePrefixType,
                 pNextFsMIStdOspfv3AreaAggregatePrefix,
                 pu4NextFsMIStdOspfv3AreaAggregatePrefixLength);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                retValFsMIStdOspfv3AreaAggregateEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaAggregateEffect (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregateAreaID,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregatePrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdOspfv3AreaAggregatePrefix,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                        INT4
                                        *pi1RetValFsMIStdOspfv3AreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaAggregateEffect (u4FsMIStdOspfv3AreaAggregateAreaID,
                                         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                         i4FsMIStdOspfv3AreaAggregatePrefixType,
                                         pFsMIStdOspfv3AreaAggregatePrefix,
                                         u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                         pi1RetValFsMIStdOspfv3AreaAggregateEffect);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaAggregateRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                retValFsMIStdOspfv3AreaAggregateRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaAggregateRouteTag (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4
                                          u4FsMIStdOspfv3AreaAggregateAreaID,
                                          INT4
                                          i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                          INT4
                                          i4FsMIStdOspfv3AreaAggregatePrefixType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdOspfv3AreaAggregatePrefix,
                                          UINT4
                                          u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                          INT4
                                          *pi1RetValFsMIStdOspfv3AreaAggregateRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaAggregateRouteTag (u4FsMIStdOspfv3AreaAggregateAreaID,
                                           i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                           i4FsMIStdOspfv3AreaAggregatePrefixType,
                                           pFsMIStdOspfv3AreaAggregatePrefix,
                                           u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                           pi1RetValFsMIStdOspfv3AreaAggregateRouteTag);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                retValFsMIStdOspfv3AreaAggregateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaAggregateStatus (INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregateAreaID,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                        INT4
                                        i4FsMIStdOspfv3AreaAggregatePrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIStdOspfv3AreaAggregatePrefix,
                                        UINT4
                                        u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                        INT4
                                        *pi1RetValFsMIStdOspfv3AreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaAggregateStatus (u4FsMIStdOspfv3AreaAggregateAreaID,
                                         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                         i4FsMIStdOspfv3AreaAggregatePrefixType,
                                         pFsMIStdOspfv3AreaAggregatePrefix,
                                         u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                         pi1RetValFsMIStdOspfv3AreaAggregateStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/*-------------------------------- fsmioslw.c ------------------------------ */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3GlobalTraceLevel
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3GlobalTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3GlobalTraceLevel (INT4 *pi1RetValFsMIOspfv3GlobalTraceLevel)
{
    *pi1RetValFsMIOspfv3GlobalTraceLevel = gV3OsRtr.u4GblTrcValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VrfSpfInterval
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3VrfSpfInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VrfSpfInterval (INT4 *pi1RetValFsMIOspfv3VrfSpfInterval)
{
    *pi1RetValFsMIOspfv3VrfSpfInterval = gV3OsRtr.u4VrfSpfInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3RTStaggeringStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RTStaggeringStatus (INT4 *pi1RetValFsMIOspfv3RTStaggeringStatus)
{
    *pi1RetValFsMIOspfv3RTStaggeringStatus = gV3OsRtr.u4RTStaggeringStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3HotStandbyAdminStatus
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3HotStandbyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3HotStandbyAdminStatus (INT4
                                       *pi1RetValFsMIOspfv3HotStandbyAdminStatus)
{
    return (nmhGetFutOspfv3HotStandbyAdminStatus
            (pi1RetValFsMIOspfv3HotStandbyAdminStatus));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3HotStandbyState
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3HotStandbyState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3HotStandbyState (INT4 *pi1RetValFsMIOspfv3HotStandbyState)
{
    return (nmhGetFutOspfv3HotStandbyState
            (pi1RetValFsMIOspfv3HotStandbyState));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3DynamicBulkUpdStatus
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3DynamicBulkUpdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3DynamicBulkUpdStatus (INT4
                                      *pi1RetValFsMIOspfv3DynamicBulkUpdStatus)
{
    return (nmhGetFutOspfv3DynamicBulkUpdStatus
            (pi1RetValFsMIOspfv3DynamicBulkUpdStatus));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3StandbyHelloSyncCount
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3StandbyHelloSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3StandbyHelloSyncCount (UINT4
                                       *pu4RetValFsMIOspfv3StandbyHelloSyncCount)
{
    return (nmhGetFutOspfv3StandbyHelloSyncCount
            (pu4RetValFsMIOspfv3StandbyHelloSyncCount));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3StandbyLsaSyncCount
 Input       :  The Indices

                The Object 
                retValFsMIOspfv3StandbyLsaSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3StandbyLsaSyncCount (UINT4
                                     *pu4RetValFsMIOspfv3StandbyLsaSyncCount)
{
    return (nmhGetFutOspfv3StandbyLsaSyncCount
            (pu4RetValFsMIOspfv3StandbyLsaSyncCount));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3Table (INT4 *pi4FsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3Table (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 *pi4NextFsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3OverFlowState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3OverFlowState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3OverFlowState (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 *pi1RetValFsMIOspfv3OverFlowState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3OverFlowState (pi1RetValFsMIOspfv3OverFlowState);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3TraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3TraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3TraceLevel (INT4 i4FsMIStdOspfv3ContextId,
                            INT4 *pi1RetValFsMIOspfv3TraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3TraceLevel (pi1RetValFsMIOspfv3TraceLevel);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ABRType
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3ABRType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ABRType (INT4 i4FsMIStdOspfv3ContextId,
                         INT4 *pi1RetValFsMIOspfv3ABRType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3ABRType (pi1RetValFsMIOspfv3ABRType);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3NssaAsbrDefRtTrans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3NssaAsbrDefRtTrans (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 *pi1RetValFsMIOspfv3NssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3NssaAsbrDefRtTrans
        (pi1RetValFsMIOspfv3NssaAsbrDefRtTrans);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3DefaultPassiveInterface
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3DefaultPassiveInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3DefaultPassiveInterface (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         *pi1RetValFsMIOspfv3DefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3DefaultPassiveInterface
        (pi1RetValFsMIOspfv3DefaultPassiveInterface);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3SpfDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3SpfDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3SpfDelay (INT4 i4FsMIStdOspfv3ContextId,
                          INT4 *pi1RetValFsMIOspfv3SpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3SpfDelay (pi1RetValFsMIOspfv3SpfDelay);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3SpfHoldTime
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3SpfHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3SpfHoldTime (INT4 i4FsMIStdOspfv3ContextId,
                             INT4 *pi1RetValFsMIOspfv3SpfHoldTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3SpfHoldTime (pi1RetValFsMIOspfv3SpfHoldTime);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RTStaggeringInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RTStaggeringInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RTStaggeringInterval (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4
                                      *pi1RetValFsMIOspfv3RTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RTStaggeringInterval
        (pi1RetValFsMIOspfv3RTStaggeringInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RestartStrictLsaChecking
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RestartStrictLsaChecking (INT4 i4FsMIStdOspfv3ContextId,
                                          INT4
                                          *pi1RetValFsMIOspfv3RestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RestartStrictLsaChecking
        (pi1RetValFsMIOspfv3RestartStrictLsaChecking);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3HelperSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3HelperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3HelperSupport (INT4 i4FsMIStdOspfv3ContextId,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsMIOspfv3HelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3HelperSupport (pRetValFsMIOspfv3HelperSupport);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3HelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3HelperGraceTimeLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3HelperGraceTimeLimit (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4
                                      *pi1RetValFsMIOspfv3HelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3HelperGraceTimeLimit
        (pi1RetValFsMIOspfv3HelperGraceTimeLimit);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RestartAckState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RestartAckState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RestartAckState (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 *pi1RetValFsMIOspfv3RestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RestartAckState (pi1RetValFsMIOspfv3RestartAckState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3GraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3GraceLsaRetransmitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3GraceLsaRetransmitCount (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         *pi1RetValFsMIOspfv3GraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3GraceLsaRetransmitCount
        (pi1RetValFsMIOspfv3GraceLsaRetransmitCount);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RestartReason
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RestartReason (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 *pi1RetValFsMIOspfv3RestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3RestartReason (pi1RetValFsMIOspfv3RestartReason);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3ExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ExtTraceLevel (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 *pi1RetValFsMIOspfv3ExtTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3ExtTraceLevel (pi1RetValFsMIOspfv3ExtTraceLevel);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3SetTraps
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3SetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3SetTraps (INT4 i4FsMIStdOspfv3ContextId,
                          INT4 *pi1RetValFsMIOspfv3SetTraps)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3SetTraps (pi1RetValFsMIOspfv3SetTraps);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BfdStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                retValFsMIOspfv3BfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BfdStatus (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 *pi4RetValFsMIOspfv3BfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3BfdStatus (pi4RetValFsMIOspfv3BfdStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BfdAllIfState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                retValFsMIOspfv3BfdAllIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BfdAllIfState (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 *pi4RetValFsMIOspfv3BfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3BfdAllIfState (pi4RetValFsMIOspfv3BfdAllIfState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouterIdPermanence
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                retValFsMIOspfv3RouterIdPermanence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouterIdPermanence (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 *pi4RetValFsMIOspfv3RouterIdPermanence)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouterIdPermanence
        (pi4RetValFsMIOspfv3RouterIdPermanence);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3IfTable
 Input       :  The Indices
                FsMIOspfv3IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3IfTable (INT4 *pi4FsMIOspfv3IfIndex)
{
    return (nmhGetFirstIndexOspfv3IfTable (pi4FsMIOspfv3IfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3IfTable
 Input       :  The Indices
                FsMIOspfv3IfIndex
                nextFsMIOspfv3IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3IfTable (INT4 i4FsMIOspfv3IfIndex,
                                  INT4 *pi4NextFsMIOspfv3IfIndex)
{
    return (nmhGetNextIndexOspfv3IfTable (i4FsMIOspfv3IfIndex,
                                          pi4NextFsMIOspfv3IfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfOperState
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfOperState (INT4 i4FsMIOspfv3IfIndex,
                             INT4 *pi1RetValFsMIOspfv3IfOperState)
{
    return (nmhGetFutOspfv3IfOperState
            (i4FsMIOspfv3IfIndex, pi1RetValFsMIOspfv3IfOperState));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfPassive
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfPassive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfPassive (INT4 i4FsMIOspfv3IfIndex,
                           INT4 *pi1RetValFsMIOspfv3IfPassive)
{
    return (nmhGetFutOspfv3IfPassive
            (i4FsMIOspfv3IfIndex, pi1RetValFsMIOspfv3IfPassive));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfNbrCount
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfNbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfNbrCount (INT4 i4FsMIOspfv3IfIndex,
                            UINT4 *pu4RetValFsMIOspfv3IfNbrCount)
{
    return (nmhGetFutOspfv3IfNbrCount
            (i4FsMIOspfv3IfIndex, pu4RetValFsMIOspfv3IfNbrCount));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAdjCount
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfAdjCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAdjCount (INT4 i4FsMIOspfv3IfIndex,
                            UINT4 *pu4RetValFsMIOspfv3IfAdjCount)
{
    return (nmhGetFutOspfv3IfAdjCount
            (i4FsMIOspfv3IfIndex, pu4RetValFsMIOspfv3IfAdjCount));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfHelloRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfHelloRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfHelloRcvd (INT4 i4FsMIOspfv3IfIndex,
                             UINT4 *pu4RetValFsMIOspfv3IfHelloRcvd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfHelloRcvd (i4FsMIOspfv3IfIndex,
                                           pu4RetValFsMIOspfv3IfHelloRcvd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfHelloTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfHelloTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfHelloTxed (INT4 i4FsMIOspfv3IfIndex,
                             UINT4 *pu4RetValFsMIOspfv3IfHelloTxed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfHelloTxed (i4FsMIOspfv3IfIndex,
                                           pu4RetValFsMIOspfv3IfHelloTxed);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfHelloDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfHelloDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfHelloDisd (INT4 i4FsMIOspfv3IfIndex,
                             UINT4 *pu4RetValFsMIOspfv3IfHelloDisd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfHelloDisd (i4FsMIOspfv3IfIndex,
                                           pu4RetValFsMIOspfv3IfHelloDisd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfDdpRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfDdpRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfDdpRcvd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfDdpRcvd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfDdpRcvd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfDdpRcvd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfDdpTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfDdpTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfDdpTxed (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfDdpTxed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfDdpTxed (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfDdpTxed);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfDdpDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfDdpDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfDdpDisd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfDdpDisd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfDdpDisd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfDdpDisd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLrqRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLrqRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLrqRcvd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLrqRcvd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLrqRcvd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLrqRcvd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLrqTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLrqTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLrqTxed (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLrqTxed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLrqTxed (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLrqTxed);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLrqDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLrqDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLrqDisd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLrqDisd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLrqDisd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLrqDisd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLsuRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLsuRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLsuRcvd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLsuRcvd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLsuRcvd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLsuRcvd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLsuTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLsuTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLsuTxed (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLsuTxed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLsuTxed (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLsuTxed);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLsuDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLsuDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLsuDisd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLsuDisd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLsuDisd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLsuDisd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLakRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLakRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLakRcvd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLakRcvd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLakRcvd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLakRcvd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLakTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLakTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLakTxed (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLakTxed)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLakTxed (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLakTxed);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLakDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLakDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLakDisd (INT4 i4FsMIOspfv3IfIndex,
                           UINT4 *pu4RetValFsMIOspfv3IfLakDisd)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfLakDisd (i4FsMIOspfv3IfIndex,
                                         pu4RetValFsMIOspfv3IfLakDisd);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfLinkLSASuppression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfLinkLSASuppression (INT4 i4FsMIOspfv3IfIndex,
                                      INT4
                                      *pi4RetValFsMIOspfv3IfLinkLSASuppression)
{

    return (nmhGetFutOspfv3IfLinkLSASuppression
            (i4FsMIOspfv3IfIndex, pi4RetValFsMIOspfv3IfLinkLSASuppression));

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfBfdState
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                retValFsMIOspfv3IfBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfBfdState (INT4 i4FsMIOspfv3IfIndex,
                            INT4 *pi4RetValFsMIOspfv3IfBfdState)
{
    return (nmhGetFutOspfv3IfBfdState
            (i4FsMIOspfv3IfIndex, pi4RetValFsMIOspfv3IfBfdState));

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfContextId
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfContextId (INT4 i4FsMIOspfv3IfIndex,
                             INT4 *pi1RetValFsMIOspfv3IfContextId)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4FsMIOspfv3IfIndex)) != NULL)
    {
        *pi1RetValFsMIOspfv3IfContextId = (INT4)
            pInterface->pArea->pV3OspfCxt->u4ContextId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3RoutingTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3RoutingTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                        INT4 *pi4FsMIOspfv3RouteDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3RouteDest,
                                        UINT4 *pu4FsMIOspfv3RoutePfxLength,
                                        INT4 *pi4FsMIOspfv3RouteNextHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3RouteNextHop)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3RoutingTable
                (pi4FsMIOspfv3RouteDestType, pFsMIOspfv3RouteDest,
                 pu4FsMIOspfv3RoutePfxLength, pi4FsMIOspfv3RouteNextHopType,
                 pFsMIOspfv3RouteNextHop);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3RoutingTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                nextFsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                nextFsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                nextFsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                nextFsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop
                nextFsMIOspfv3RouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3RoutingTable (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 *pi4NextFsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3RouteDestType,
                                       INT4 *pi4NextFsMIOspfv3RouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3RouteDest,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMIOspfv3RouteDest,
                                       UINT4 u4FsMIOspfv3RoutePfxLength,
                                       UINT4 *pu4NextFsMIOspfv3RoutePfxLength,
                                       INT4 i4FsMIOspfv3RouteNextHopType,
                                       INT4 *pi4NextFsMIOspfv3RouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3RouteNextHop,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMIOspfv3RouteNextHop)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3RoutingTable (i4FsMIOspfv3RouteDestType,
                                                  pi4NextFsMIOspfv3RouteDestType,
                                                  pFsMIOspfv3RouteDest,
                                                  pNextFsMIOspfv3RouteDest,
                                                  u4FsMIOspfv3RoutePfxLength,
                                                  pu4NextFsMIOspfv3RoutePfxLength,
                                                  i4FsMIOspfv3RouteNextHopType,
                                                  pi4NextFsMIOspfv3RouteNextHopType,
                                                  pFsMIOspfv3RouteNextHop,
                                                  pNextFsMIOspfv3RouteNextHop);
        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3RoutingTable
                (pi4NextFsMIOspfv3RouteDestType, pNextFsMIOspfv3RouteDest,
                 pu4NextFsMIOspfv3RoutePfxLength,
                 pi4NextFsMIOspfv3RouteNextHopType,
                 pNextFsMIOspfv3RouteNextHop);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouteType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop

                The Object 
                retValFsMIOspfv3RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouteType (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4FsMIOspfv3RouteDestType,
                           tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteDest,
                           UINT4 u4FsMIOspfv3RoutePfxLength,
                           INT4 i4FsMIOspfv3RouteNextHopType,
                           tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteNextHop,
                           INT4 *pi1RetValFsMIOspfv3RouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouteType (i4FsMIOspfv3RouteDestType,
                                  pFsMIOspfv3RouteDest,
                                  u4FsMIOspfv3RoutePfxLength,
                                  i4FsMIOspfv3RouteNextHopType,
                                  pFsMIOspfv3RouteNextHop,
                                  pi1RetValFsMIOspfv3RouteType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouteAreaId
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop

                The Object 
                retValFsMIOspfv3RouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouteAreaId (INT4 i4FsMIStdOspfv3ContextId,
                             INT4 i4FsMIOspfv3RouteDestType,
                             tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteDest,
                             UINT4 u4FsMIOspfv3RoutePfxLength,
                             INT4 i4FsMIOspfv3RouteNextHopType,
                             tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteNextHop,
                             UINT4 *pu4RetValFsMIOspfv3RouteAreaId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouteAreaId (i4FsMIOspfv3RouteDestType,
                                    pFsMIOspfv3RouteDest,
                                    u4FsMIOspfv3RoutePfxLength,
                                    i4FsMIOspfv3RouteNextHopType,
                                    pFsMIOspfv3RouteNextHop,
                                    pu4RetValFsMIOspfv3RouteAreaId);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouteCost
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop

                The Object 
                retValFsMIOspfv3RouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouteCost (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4FsMIOspfv3RouteDestType,
                           tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteDest,
                           UINT4 u4FsMIOspfv3RoutePfxLength,
                           INT4 i4FsMIOspfv3RouteNextHopType,
                           tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteNextHop,
                           INT4 *pi1RetValFsMIOspfv3RouteCost)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouteCost (i4FsMIOspfv3RouteDestType,
                                  pFsMIOspfv3RouteDest,
                                  u4FsMIOspfv3RoutePfxLength,
                                  i4FsMIOspfv3RouteNextHopType,
                                  pFsMIOspfv3RouteNextHop,
                                  pi1RetValFsMIOspfv3RouteCost);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouteType2Cost
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop

                The Object 
                retValFsMIOspfv3RouteType2Cost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouteType2Cost (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3RouteDestType,
                                tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RouteDest,
                                UINT4 u4FsMIOspfv3RoutePfxLength,
                                INT4 i4FsMIOspfv3RouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3RouteNextHop,
                                INT4 *pi1RetValFsMIOspfv3RouteType2Cost)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouteType2Cost (i4FsMIOspfv3RouteDestType,
                                       pFsMIOspfv3RouteDest,
                                       u4FsMIOspfv3RoutePfxLength,
                                       i4FsMIOspfv3RouteNextHopType,
                                       pFsMIOspfv3RouteNextHop,
                                       pi1RetValFsMIOspfv3RouteType2Cost);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RouteInterfaceIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop

                The Object 
                retValFsMIOspfv3RouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RouteInterfaceIndex (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4FsMIOspfv3RouteDestType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIOspfv3RouteDest,
                                     UINT4 u4FsMIOspfv3RoutePfxLength,
                                     INT4 i4FsMIOspfv3RouteNextHopType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIOspfv3RouteNextHop,
                                     INT4
                                     *pi1RetValFsMIOspfv3RouteInterfaceIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RouteInterfaceIndex (i4FsMIOspfv3RouteDestType,
                                            pFsMIOspfv3RouteDest,
                                            u4FsMIOspfv3RoutePfxLength,
                                            i4FsMIOspfv3RouteNextHopType,
                                            pFsMIOspfv3RouteNextHop,
                                            pi1RetValFsMIOspfv3RouteInterfaceIndex);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3AsExternalAggregationTable (INT4
                                                      *pi4FsMIStdOspfv3ContextId,
                                                      INT4
                                                      *pi4FsMIOspfv3AsExternalAggregationNetType,
                                                      tSNMP_OCTET_STRING_TYPE *
                                                      pFsMIOspfv3AsExternalAggregationNet,
                                                      UINT4
                                                      *pu4FsMIOspfv3AsExternalAggregationPfxLength,
                                                      UINT4
                                                      *pu4FsMIOspfv3AsExternalAggregationAreaId)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3AsExternalAggregationTable
                (pi4FsMIOspfv3AsExternalAggregationNetType,
                 pFsMIOspfv3AsExternalAggregationNet,
                 pu4FsMIOspfv3AsExternalAggregationPfxLength,
                 pu4FsMIOspfv3AsExternalAggregationAreaId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                nextFsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                nextFsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                nextFsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId
                nextFsMIOspfv3AsExternalAggregationAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable (INT4
                                                     i4FsMIStdOspfv3ContextId,
                                                     INT4
                                                     *pi4NextFsMIStdOspfv3ContextId,
                                                     INT4
                                                     i4FsMIOspfv3AsExternalAggregationNetType,
                                                     INT4
                                                     *pi4NextFsMIOspfv3AsExternalAggregationNetType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsMIOspfv3AsExternalAggregationNet,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pNextFsMIOspfv3AsExternalAggregationNet,
                                                     UINT4
                                                     u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                     UINT4
                                                     *pu4NextFsMIOspfv3AsExternalAggregationPfxLength,
                                                     UINT4
                                                     u4FsMIOspfv3AsExternalAggregationAreaId,
                                                     UINT4
                                                     *pu4NextFsMIOspfv3AsExternalAggregationAreaId)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3AsExternalAggregationTable
            (i4FsMIOspfv3AsExternalAggregationNetType,
             pi4NextFsMIOspfv3AsExternalAggregationNetType,
             pFsMIOspfv3AsExternalAggregationNet,
             pNextFsMIOspfv3AsExternalAggregationNet,
             u4FsMIOspfv3AsExternalAggregationPfxLength,
             pu4NextFsMIOspfv3AsExternalAggregationPfxLength,
             u4FsMIOspfv3AsExternalAggregationAreaId,
             pu4NextFsMIOspfv3AsExternalAggregationAreaId);

        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3AsExternalAggregationTable
                (pi4NextFsMIOspfv3AsExternalAggregationNetType,
                 pNextFsMIOspfv3AsExternalAggregationNet,
                 pu4NextFsMIOspfv3AsExternalAggregationPfxLength,
                 pu4NextFsMIOspfv3AsExternalAggregationAreaId);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                retValFsMIOspfv3AsExternalAggregationEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3AsExternalAggregationEffect (INT4 i4FsMIStdOspfv3ContextId,
                                             INT4
                                             i4FsMIOspfv3AsExternalAggregationNetType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIOspfv3AsExternalAggregationNet,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationPfxLength,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationAreaId,
                                             INT4
                                             *pi1RetValFsMIOspfv3AsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3AsExternalAggregationEffect
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         pi1RetValFsMIOspfv3AsExternalAggregationEffect);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                retValFsMIOspfv3AsExternalAggregationTranslation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3AsExternalAggregationTranslation (INT4 i4FsMIStdOspfv3ContextId,
                                                  INT4
                                                  i4FsMIOspfv3AsExternalAggregationNetType,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIOspfv3AsExternalAggregationNet,
                                                  UINT4
                                                  u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                  UINT4
                                                  u4FsMIOspfv3AsExternalAggregationAreaId,
                                                  INT4
                                                  *pi1RetValFsMIOspfv3AsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3AsExternalAggregationTranslation
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         pi1RetValFsMIOspfv3AsExternalAggregationTranslation);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                retValFsMIOspfv3AsExternalAggregationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3AsExternalAggregationStatus (INT4 i4FsMIStdOspfv3ContextId,
                                             INT4
                                             i4FsMIOspfv3AsExternalAggregationNetType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIOspfv3AsExternalAggregationNet,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationPfxLength,
                                             UINT4
                                             u4FsMIOspfv3AsExternalAggregationAreaId,
                                             INT4
                                             *pi1RetValFsMIOspfv3AsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3AsExternalAggregationStatus
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId,
         pi1RetValFsMIOspfv3AsExternalAggregationStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3BRRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3BRRouteTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                        UINT4 *pu4FsMIOspfv3BRRouteDest,
                                        INT4 *pi4FsMIOspfv3BRRouteNextHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3BRRouteNextHop,
                                        INT4 *pi4FsMIOspfv3BRRouteDestType)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3BRRouteTable (pu4FsMIOspfv3BRRouteDest,
                                                       pi4FsMIOspfv3BRRouteNextHopType,
                                                       pFsMIOspfv3BRRouteNextHop,
                                                       pi4FsMIOspfv3BRRouteDestType);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3BRRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                nextFsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                nextFsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                nextFsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType
                nextFsMIOspfv3BRRouteDestType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3BRRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 *pi4NextFsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIOspfv3BRRouteDest,
                                       UINT4 *pu4NextFsMIOspfv3BRRouteDest,
                                       INT4 i4FsMIOspfv3BRRouteNextHopType,
                                       INT4
                                       *pi4NextFsMIOspfv3BRRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3BRRouteNextHop,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFsMIOspfv3BRRouteNextHop,
                                       INT4 i4FsMIOspfv3BRRouteDestType,
                                       INT4 *pi4NextFsMIOspfv3BRRouteDestType)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3BRRouteTable (u4FsMIOspfv3BRRouteDest,
                                                  pu4NextFsMIOspfv3BRRouteDest,
                                                  i4FsMIOspfv3BRRouteNextHopType,
                                                  pi4NextFsMIOspfv3BRRouteNextHopType,
                                                  pFsMIOspfv3BRRouteNextHop,
                                                  pNextFsMIOspfv3BRRouteNextHop,
                                                  i4FsMIOspfv3BRRouteDestType,
                                                  pi4NextFsMIOspfv3BRRouteDestType);

        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3BRRouteTable
                (pu4NextFsMIOspfv3BRRouteDest,
                 pi4NextFsMIOspfv3BRRouteNextHopType,
                 pNextFsMIOspfv3BRRouteNextHop,
                 pi4NextFsMIOspfv3BRRouteDestType);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BRRouteType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType

                The Object 
                retValFsMIOspfv3BRRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BRRouteType (INT4 i4FsMIStdOspfv3ContextId,
                             UINT4 u4FsMIOspfv3BRRouteDest,
                             INT4 i4FsMIOspfv3BRRouteNextHopType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIOspfv3BRRouteNextHop,
                             INT4 i4FsMIOspfv3BRRouteDestType,
                             INT4 *pi1RetValFsMIOspfv3BRRouteType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3BRRouteType (u4FsMIOspfv3BRRouteDest,
                                    i4FsMIOspfv3BRRouteNextHopType,
                                    pFsMIOspfv3BRRouteNextHop,
                                    i4FsMIOspfv3BRRouteDestType,
                                    pi1RetValFsMIOspfv3BRRouteType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BRRouteAreaId
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType

                The Object 
                retValFsMIOspfv3BRRouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BRRouteAreaId (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIOspfv3BRRouteDest,
                               INT4 i4FsMIOspfv3BRRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFsMIOspfv3BRRouteNextHop,
                               INT4 i4FsMIOspfv3BRRouteDestType,
                               UINT4 *pu4RetValFsMIOspfv3BRRouteAreaId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3BRRouteAreaId (u4FsMIOspfv3BRRouteDest,
                                      i4FsMIOspfv3BRRouteNextHopType,
                                      pFsMIOspfv3BRRouteNextHop,
                                      i4FsMIOspfv3BRRouteDestType,
                                      pu4RetValFsMIOspfv3BRRouteAreaId);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BRRouteCost
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType

                The Object 
                retValFsMIOspfv3BRRouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BRRouteCost (INT4 i4FsMIStdOspfv3ContextId,
                             UINT4 u4FsMIOspfv3BRRouteDest,
                             INT4 i4FsMIOspfv3BRRouteNextHopType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsMIOspfv3BRRouteNextHop,
                             INT4 i4FsMIOspfv3BRRouteDestType,
                             INT4 *pi1RetValFsMIOspfv3BRRouteCost)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3BRRouteCost (u4FsMIOspfv3BRRouteDest,
                                    i4FsMIOspfv3BRRouteNextHopType,
                                    pFsMIOspfv3BRRouteNextHop,
                                    i4FsMIOspfv3BRRouteDestType,
                                    pi1RetValFsMIOspfv3BRRouteCost);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3BRRouteInterfaceIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType

                The Object 
                retValFsMIOspfv3BRRouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3BRRouteInterfaceIndex (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIOspfv3BRRouteDest,
                                       INT4 i4FsMIOspfv3BRRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3BRRouteNextHop,
                                       INT4 i4FsMIOspfv3BRRouteDestType,
                                       INT4
                                       *pi1RetValFsMIOspfv3BRRouteInterfaceIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3BRRouteInterfaceIndex (u4FsMIOspfv3BRRouteDest,
                                              i4FsMIOspfv3BRRouteNextHopType,
                                              pFsMIOspfv3BRRouteNextHop,
                                              i4FsMIOspfv3BRRouteDestType,
                                              pi1RetValFsMIOspfv3BRRouteInterfaceIndex);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3RedistRouteCfgTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                               INT4
                                               *pi4FsMIOspfv3RedistRouteDestType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsMIOspfv3RedistRouteDest,
                                               UINT4
                                               *pu4FsMIOspfv3RedistRoutePfxLength)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3RedistRouteCfgTable
                (pi4FsMIOspfv3RedistRouteDestType, pFsMIOspfv3RedistRouteDest,
                 pu4FsMIOspfv3RedistRoutePfxLength);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                nextFsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                nextFsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength
                nextFsMIOspfv3RedistRoutePfxLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable (INT4 i4FsMIStdOspfv3ContextId,
                                              INT4
                                              *pi4NextFsMIStdOspfv3ContextId,
                                              INT4
                                              i4FsMIOspfv3RedistRouteDestType,
                                              INT4
                                              *pi4NextFsMIOspfv3RedistRouteDestType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsMIOspfv3RedistRouteDest,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pNextFsMIOspfv3RedistRouteDest,
                                              UINT4
                                              u4FsMIOspfv3RedistRoutePfxLength,
                                              UINT4
                                              *pu4NextFsMIOspfv3RedistRoutePfxLength)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3RedistRouteCfgTable
            (i4FsMIOspfv3RedistRouteDestType,
             pi4NextFsMIOspfv3RedistRouteDestType, pFsMIOspfv3RedistRouteDest,
             pNextFsMIOspfv3RedistRouteDest, u4FsMIOspfv3RedistRoutePfxLength,
             pu4NextFsMIOspfv3RedistRoutePfxLength);

        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3RedistRouteCfgTable
                (pi4NextFsMIOspfv3RedistRouteDestType,
                 pNextFsMIOspfv3RedistRouteDest,
                 pu4NextFsMIOspfv3RedistRoutePfxLength);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RedistRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                retValFsMIOspfv3RedistRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RedistRouteMetric (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3RedistRouteDest,
                                   UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                   INT4 *pi1RetValFsMIOspfv3RedistRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RedistRouteMetric (i4FsMIOspfv3RedistRouteDestType,
                                          pFsMIOspfv3RedistRouteDest,
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          pi1RetValFsMIOspfv3RedistRouteMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RedistRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                retValFsMIOspfv3RedistRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RedistRouteMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3RedistRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3RedistRouteDest,
                                       UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                       INT4
                                       *pi1RetValFsMIOspfv3RedistRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RedistRouteMetricType (i4FsMIOspfv3RedistRouteDestType,
                                              pFsMIOspfv3RedistRouteDest,
                                              u4FsMIOspfv3RedistRoutePfxLength,
                                              pi1RetValFsMIOspfv3RedistRouteMetricType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RedistRouteTagType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                retValFsMIOspfv3RedistRouteTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RedistRouteTagType (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4FsMIOspfv3RedistRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3RedistRouteDest,
                                    UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                    INT4 *pi1RetValFsMIOspfv3RedistRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RedistRouteTagType (i4FsMIOspfv3RedistRouteDestType,
                                           pFsMIOspfv3RedistRouteDest,
                                           u4FsMIOspfv3RedistRoutePfxLength,
                                           pi1RetValFsMIOspfv3RedistRouteTagType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RedistRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                retValFsMIOspfv3RedistRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RedistRouteTag (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3RedistRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3RedistRouteDest,
                                UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                INT4 *pi1RetValFsMIOspfv3RedistRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RedistRouteTag (i4FsMIOspfv3RedistRouteDestType,
                                       pFsMIOspfv3RedistRouteDest,
                                       u4FsMIOspfv3RedistRoutePfxLength,
                                       pi1RetValFsMIOspfv3RedistRouteTag);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RedistRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                retValFsMIOspfv3RedistRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RedistRouteStatus (INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3RedistRouteDest,
                                   UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                   INT4 *pi1RetValFsMIOspfv3RedistRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RedistRouteStatus (i4FsMIOspfv3RedistRouteDestType,
                                          pFsMIOspfv3RedistRouteDest,
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          pi1RetValFsMIOspfv3RedistRouteStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3RRDRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3RRDRouteTable (INT4 *pi4FsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3RRDRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3RRDRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                        INT4 *pi4NextFsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RRDStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RRDStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RRDStatus (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 *pi1RetValFsMIOspfv3RRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3RRDStatus (pi1RetValFsMIOspfv3RRDStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RRDSrcProtoMask
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RRDSrcProtoMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RRDSrcProtoMask (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 *pi1RetValFsMIOspfv3RRDSrcProtoMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RRDSrcProtoMask (pi1RetValFsMIOspfv3RRDSrcProtoMask);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RRDRouteMapName
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3RRDRouteMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RRDRouteMapName (INT4 i4FsMIStdOspfv3ContextId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValFsMIOspfv3RRDRouteMapName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RRDRouteMapName (pRetValFsMIOspfv3RRDRouteMapName);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable (INT4
                                                  *pi4FsMIStdOspfv3ContextId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pFsMIOspfv3DistInOutRouteMapName,
                                                  INT4
                                                  *pi4FsMIOspfv3DistInOutRouteMapType)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable
                (pFsMIOspfv3DistInOutRouteMapName,
                 pi4FsMIOspfv3DistInOutRouteMapType);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                nextFsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType
                nextFsMIOspfv3DistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable (INT4 i4FsMIStdOspfv3ContextId,
                                                 INT4
                                                 *pi4NextFsMIStdOspfv3ContextId,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIOspfv3DistInOutRouteMapName,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pNextFsMIOspfv3DistInOutRouteMapName,
                                                 INT4
                                                 i4FsMIOspfv3DistInOutRouteMapType,
                                                 INT4
                                                 *pi4NextFsMIOspfv3DistInOutRouteMapType)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3DistInOutRouteMapTable
            (pFsMIOspfv3DistInOutRouteMapName,
             pNextFsMIOspfv3DistInOutRouteMapName,
             i4FsMIOspfv3DistInOutRouteMapType,
             pi4NextFsMIOspfv3DistInOutRouteMapType);

        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable
                (pNextFsMIOspfv3DistInOutRouteMapName,
                 pi4NextFsMIOspfv3DistInOutRouteMapType);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                retValFsMIOspfv3DistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3DistInOutRouteMapValue (INT4 i4FsMIStdOspfv3ContextId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3DistInOutRouteMapName,
                                        INT4 i4FsMIOspfv3DistInOutRouteMapType,
                                        INT4
                                        *pi1RetValFsMIOspfv3DistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3DistInOutRouteMapValue (pFsMIOspfv3DistInOutRouteMapName,
                                               i4FsMIOspfv3DistInOutRouteMapType,
                                               pi1RetValFsMIOspfv3DistInOutRouteMapValue);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                retValFsMIOspfv3DistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3DistInOutRouteMapRowStatus (INT4 i4FsMIStdOspfv3ContextId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFsMIOspfv3DistInOutRouteMapName,
                                            INT4
                                            i4FsMIOspfv3DistInOutRouteMapType,
                                            INT4
                                            *pi1RetValFsMIOspfv3DistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3DistInOutRouteMapRowStatus
        (pFsMIOspfv3DistInOutRouteMapName, i4FsMIOspfv3DistInOutRouteMapType,
         pi1RetValFsMIOspfv3DistInOutRouteMapRowStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3PreferenceTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3PreferenceTable (INT4 *pi4FsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3PreferenceTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3PreferenceTable (INT4 i4FsMIStdOspfv3ContextId,
                                          INT4 *pi4NextFsMIStdOspfv3ContextId)
{
    UINT4               u4CxtId;

    if (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3NeighborBfdTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3NeighborBfdTable (INT4 *pi4FsMIStdOspfv3NbrIfIndex,
                                            UINT4 *pu4FsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3NeighborBfdTable
                (pi4FsMIStdOspfv3NbrIfIndex, pu4FsMIStdOspfv3NbrRtrId);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3NeighborBfdTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                nextFsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
                nextFsMIStdOspfv3NbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3NeighborBfdTable (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                           INT4 *pi4NextFsMIStdOspfv3NbrIfIndex,
                                           UINT4 u4FsMIStdOspfv3NbrRtrId,
                                           UINT4 *pu4NextFsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIStdOspfv3NbrIfIndex, &u4CxtId)
        == OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal = nmhGetNextIndexFutOspfv3NeighborBfdTable
            (i4FsMIStdOspfv3NbrIfIndex,
             pi4NextFsMIStdOspfv3NbrIfIndex,
             u4FsMIStdOspfv3NbrRtrId, pu4NextFsMIStdOspfv3NbrRtrId);
        V3UtilReSetContext ();
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4V3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            if (V3UtilSetContext ((UINT4) (INT4) u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3NeighborBfdTable
                (pi4NextFsMIStdOspfv3NbrIfIndex, pu4NextFsMIStdOspfv3NbrRtrId);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4V3ContextId = (INT4) u4CxtId;
        }
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3PreferenceValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                retValFsMIOspfv3PreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3PreferenceValue (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 *pi1RetValFsMIOspfv3PreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspf3PreferenceValue (pi1RetValFsMIOspfv3PreferenceValue);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3NbrBfdState
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId

                The Object
                retValFsMIOspfv3NbrBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3NbrBfdState (INT4 i4FsMIStdOspfv3NbrIfIndex,
                             UINT4 u4FsMIStdOspfv3NbrRtrId,
                             INT4 *pi4RetValFsMIOspfv3NbrBfdState)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIStdOspfv3NbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3NbrBfdState (i4FsMIStdOspfv3NbrIfIndex,
                                           u4FsMIStdOspfv3NbrRtrId,
                                           pi4RetValFsMIOspfv3NbrBfdState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfCryptoAuthType
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfCryptoAuthType (INT4 i4FsMIOspfv3IfIndex,
                                  INT4 *pi4RetValFsMIOspfv3IfCryptoAuthType)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfCryptoAuthType (i4FsMIOspfv3IfIndex,
                                                pi4RetValFsMIOspfv3IfCryptoAuthType);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                retValFsMIOspfv3IfCryptoAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfCryptoAuthMode (INT4 i4FsMIOspfv3IfIndex,
                                  INT4 *pi4RetValFsMIOspfv3IfCryptoAuthMode)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3IfCryptoAuthMode (i4FsMIOspfv3IfIndex,
                                                pi4RetValFsMIOspfv3IfCryptoAuthMode);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthTxed
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                retValFsMIOspfv3IfAuthTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthTxed (INT4 i4FsMIOspfv3IfIndex,
                            UINT4 *pu4RetValFsMIOspfv3IfAuthTxed)
{
    return (nmhGetFutOspfv3IfAuthTxed (i4FsMIOspfv3IfIndex,
                                       pu4RetValFsMIOspfv3IfAuthTxed));

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthRcvd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                retValFsMIOspfv3IfAuthRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthRcvd (INT4 i4FsMIOspfv3IfIndex,
                            UINT4 *pu4RetValFsMIOspfv3IfAuthRcvd)
{
    return (nmhGetFutOspfv3IfAuthRcvd (i4FsMIOspfv3IfIndex,
                                       pu4RetValFsMIOspfv3IfAuthRcvd));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthDisd
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                retValFsMIOspfv3IfAuthDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthDisd (INT4 i4FsMIOspfv3IfIndex,
                            UINT4 *pu4RetValFsMIOspfv3IfAuthDisd)
{
    return (nmhGetFutOspfv3IfAuthDisd (i4FsMIOspfv3IfIndex,
                                       pu4RetValFsMIOspfv3IfAuthDisd));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3VirtIfCryptoAuthTable (INT4
                                                 *pi4FsMIStdOspfv3ContextId,
                                                 UINT4
                                                 *pu4FsMIStdOspfv3VirtIfAreaId,
                                                 UINT4
                                                 *pu4FsMIStdOspfv3VirtIfNeighbor)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return i4RetVal;
    }
    do
    {
        if (V3UtilSetContext ((UINT4) *pi4FsMIStdOspfv3ContextId)
            == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfv3VirtIfCryptoAuthTable
            (pu4FsMIStdOspfv3VirtIfAreaId, pu4FsMIStdOspfv3VirtIfNeighbor);
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }

        V3UtilReSetContext ();
    }
    while (V3UtilOspfGetNextCxtId ((UINT4) *pi4FsMIStdOspfv3ContextId,
                                   (UINT4 *) pi4FsMIStdOspfv3ContextId)
           != OSPFV3_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                nextFsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
                nextFsMIStdOspfv3VirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3VirtIfCryptoAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                                INT4
                                                *pi4NextFsMIStdOspfv3ContextId,
                                                UINT4
                                                u4FsMIStdOspfv3VirtIfAreaId,
                                                UINT4
                                                *pu4NextFsMIStdOspfv3VirtIfAreaId,
                                                UINT4
                                                u4FsMIStdOspfv3VirtIfNeighbor,
                                                UINT4
                                                *pu4NextFsMIStdOspfv3VirtIfNeighbor)
{
    INT1                i4RetVal = SNMP_FAILURE;
    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSPFV3_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfv3VirtIfCryptoAuthTable
            (u4FsMIStdOspfv3VirtIfAreaId, pu4NextFsMIStdOspfv3VirtIfAreaId,
             u4FsMIStdOspfv3VirtIfNeighbor, pu4NextFsMIStdOspfv3VirtIfNeighbor);
        V3UtilReSetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        (UINT4 *)
                                        pi4NextFsMIStdOspfv3ContextId)) ==
               OSPFV3_SUCCESS)
        {
            if (V3UtilSetContext (*pu4NextFsMIStdOspfv3VirtIfNeighbor) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal = nmhGetFirstIndexFutOspfv3VirtIfCryptoAuthTable
                (&u4FsMIStdOspfv3VirtIfAreaId, &u4FsMIStdOspfv3VirtIfNeighbor);
            V3UtilReSetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfv3ContextId =
                (INT4) *pu4NextFsMIStdOspfv3VirtIfNeighbor;
        }
    }
    else
    {
        *pu4NextFsMIStdOspfv3VirtIfNeighbor = (UINT4) i4FsMIStdOspfv3ContextId;
    }
    return i4RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIOspfv3VirtIfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfCryptoAuthType (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                      UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                      INT4
                                      *pi4RetValFsMIOspfv3VirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfCryptoAuthType (u4FsMIStdOspfv3VirtIfAreaId,
                                             u4FsMIStdOspfv3VirtIfNeighbor,
                                             pi4RetValFsMIOspfv3VirtIfCryptoAuthType);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                retValFsMIOspfv3VirtIfCryptoAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfCryptoAuthMode (INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                      UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                      INT4
                                      *pi4RetValFsMIOspfv3VirtIfCryptoAuthMode)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfCryptoAuthMode (u4FsMIStdOspfv3VirtIfAreaId,
                                             u4FsMIStdOspfv3VirtIfNeighbor,
                                             pi4RetValFsMIOspfv3VirtIfCryptoAuthMode);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3IfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3IfAuthTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                       INT4 *pi4FsMIOspfv3IfAuthIfIndex,
                                       INT4 *pi4FsMIOspfv3IfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfAuthContextId;
    if (V3UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIOspfIfAuthContextId = *pi4FsMIStdOspfv3ContextId;
        if (V3UtilSetContext ((UINT4) *pi4FsMIStdOspfv3ContextId) ==
            SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfv3IfAuthTable
            (pi4FsMIOspfv3IfAuthIfIndex, pi4FsMIOspfv3IfAuthKeyId);

        V3UtilReSetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfAuthContextId,
                                   (UINT4 *) pi4FsMIStdOspfv3ContextId) !=
           OSPFV3_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3IfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                nextFsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId
                nextFsMIOspfv3IfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3IfAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 *pi4NextFsMIStdOspfv3ContextId,
                                      INT4 i4FsMIOspfv3IfAuthIfIndex,
                                      INT4 *pi4NextFsMIOspfv3IfAuthIfIndex,
                                      INT4 i4FsMIOspfv3IfAuthKeyId,
                                      INT4 *pi4NextFsMIOspfv3IfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    if (V3UtilOspfIsValidCxtId ((INT4) i4FsMIStdOspfv3ContextId) ==
        OSPFV3_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfv3IfAuthTable (i4FsMIOspfv3IfAuthIfIndex,
                                                 pi4NextFsMIOspfv3IfAuthIfIndex,
                                                 i4FsMIOspfv3IfAuthKeyId,
                                                 pi4NextFsMIOspfv3IfAuthKeyId);
        V3UtilReSetContext ();
    }

    if (i4RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        (UINT4 *)
                                        pi4NextFsMIStdOspfv3ContextId)) ==
               OSPFV3_SUCCESS)
        {
            if (V3UtilSetContext ((UINT4) *pi4NextFsMIStdOspfv3ContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal = nmhGetFirstIndexFutOspfv3IfAuthTable
                (pi4NextFsMIOspfv3IfAuthIfIndex, pi4NextFsMIOspfv3IfAuthKeyId);
            V3UtilReSetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfv3ContextId = *pi4NextFsMIStdOspfv3ContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKey (INT4 i4FsMIStdOspfv3ContextId,
                           INT4 i4FsMIOspfv3IfAuthIfIndex,
                           INT4 i4FsMIOspfv3IfAuthKeyId,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMIOspfv3IfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKey (i4FsMIOspfv3IfAuthIfIndex,
                                  i4FsMIOspfv3IfAuthKeyId,
                                  pRetValFsMIOspfv3IfAuthKey);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKeyStartAccept (INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4FsMIOspfv3IfAuthIfIndex,
                                      INT4 i4FsMIOspfv3IfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFsMIOspfv3IfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKeyStartAccept (i4FsMIOspfv3IfAuthIfIndex,
                                             i4FsMIOspfv3IfAuthKeyId,
                                             pRetValFsMIOspfv3IfAuthKeyStartAccept);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKeyStartGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                        INT4 i4FsMIOspfv3IfAuthIfIndex,
                                        INT4 i4FsMIOspfv3IfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValFsMIOspfv3IfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKeyStartGenerate (i4FsMIOspfv3IfAuthIfIndex,
                                               i4FsMIOspfv3IfAuthKeyId,
                                               pRetValFsMIOspfv3IfAuthKeyStartGenerate);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKeyStopGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3IfAuthIfIndex,
                                       INT4 i4FsMIOspfv3IfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValFsMIOspfv3IfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKeyStopGenerate (i4FsMIOspfv3IfAuthIfIndex,
                                              i4FsMIOspfv3IfAuthKeyId,
                                              pRetValFsMIOspfv3IfAuthKeyStopGenerate);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKeyStopAccept (INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4FsMIOspfv3IfAuthIfIndex,
                                     INT4 i4FsMIOspfv3IfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValFsMIOspfv3IfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKeyStopAccept (i4FsMIOspfv3IfAuthIfIndex,
                                            i4FsMIOspfv3IfAuthKeyId,
                                            pRetValFsMIOspfv3IfAuthKeyStopAccept);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                retValFsMIOspfv3IfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3IfAuthKeyStatus (INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4FsMIOspfv3IfAuthIfIndex,
                                 INT4 i4FsMIOspfv3IfAuthKeyId,
                                 INT4 *pi4RetValFsMIOspfv3IfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3IfAuthKeyStatus (i4FsMIOspfv3IfAuthIfIndex,
                                        i4FsMIOspfv3IfAuthKeyId,
                                        pi4RetValFsMIOspfv3IfAuthKeyStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3IfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3IfAuthTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3VirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3VirtIfAuthTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                           UINT4 *pu4FsMIOspfv3VirtIfAuthAreaId,
                                           UINT4
                                           *pu4FsMIOspfv3VirtIfAuthNeighbor,
                                           INT4 *pi4FsMIOspfv3VirtIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;
    INT4                i4FsMIOspfIfAuthContextId;
    if (V3UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return i4RetVal;
    }

    do
    {
        i4FsMIOspfIfAuthContextId = *pi4FsMIStdOspfv3ContextId;
        if (V3UtilSetContext ((UINT4) *pi4FsMIStdOspfv3ContextId) ==
            SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetFirstIndexFutOspfv3VirtIfAuthTable
            (pu4FsMIOspfv3VirtIfAuthAreaId, pu4FsMIOspfv3VirtIfAuthNeighbor,
             pi4FsMIOspfv3VirtIfAuthKeyId);

        V3UtilReSetContext ();
        if (i4RetVal == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
    }
    while (V3UtilOspfGetNextCxtId ((UINT4) i4FsMIOspfIfAuthContextId,
                                   (UINT4 *) pi4FsMIStdOspfv3ContextId) !=
           OSPFV3_FAILURE);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3VirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                nextFsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                nextFsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId
                nextFsMIOspfv3VirtIfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3VirtIfAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                          INT4 *pi4NextFsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                          UINT4
                                          *pu4NextFsMIOspfv3VirtIfAuthAreaId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                          UINT4
                                          *pu4NextFsMIOspfv3VirtIfAuthNeighbor,
                                          INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                          INT4
                                          *pi4NextFsMIOspfv3VirtIfAuthKeyId)
{
    INT1                i4RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId ((INT4) i4FsMIStdOspfv3ContextId) ==
        OSPFV3_SUCCESS)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i4RetVal;
        }
        i4RetVal =
            nmhGetNextIndexFutOspfv3VirtIfAuthTable
            (u4FsMIOspfv3VirtIfAuthAreaId, pu4NextFsMIOspfv3VirtIfAuthAreaId,
             u4FsMIOspfv3VirtIfAuthNeighbor,
             pu4NextFsMIOspfv3VirtIfAuthNeighbor, i4FsMIOspfv3VirtIfAuthKeyId,
             pi4NextFsMIOspfv3VirtIfAuthKeyId);
        V3UtilReSetContext ();
    }
    if (i4RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        (UINT4 *)
                                        pi4NextFsMIStdOspfv3ContextId))
               == OSPFV3_SUCCESS)
        {
            if (V3UtilSetContext ((UINT4) *pi4NextFsMIStdOspfv3ContextId) ==
                SNMP_FAILURE)
            {
                break;
            }
            i4RetVal =
                nmhGetFirstIndexFutOspfv3VirtIfAuthTable
                (pu4NextFsMIOspfv3VirtIfAuthAreaId,
                 pu4NextFsMIOspfv3VirtIfAuthNeighbor,
                 pi4NextFsMIOspfv3VirtIfAuthKeyId);
            V3UtilReSetContext ();

            if (i4RetVal == SNMP_SUCCESS)
            {
                break;
            }
            i4FsMIStdOspfv3ContextId = *pi4NextFsMIStdOspfv3ContextId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i4RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKey (INT4 i4FsMIStdOspfv3ContextId,
                               UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                               UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                               INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValFsMIOspfv3VirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3VirtIfAuthKey (u4FsMIOspfv3VirtIfAuthAreaId,
                                             u4FsMIOspfv3VirtIfAuthNeighbor,
                                             i4FsMIOspfv3VirtIfAuthKeyId,
                                             pRetValFsMIOspfv3VirtIfAuthKey);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStartAccept (INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                          UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                          INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pRetValFsMIOspfv3VirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfAuthKeyStartAccept (u4FsMIOspfv3VirtIfAuthAreaId,
                                                 u4FsMIOspfv3VirtIfAuthNeighbor,
                                                 i4FsMIOspfv3VirtIfAuthKeyId,
                                                 pRetValFsMIOspfv3VirtIfAuthKeyStartAccept);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStartGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                            UINT4
                                            u4FsMIOspfv3VirtIfAuthNeighbor,
                                            INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValFsMIOspfv3VirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfAuthKeyStartGenerate (u4FsMIOspfv3VirtIfAuthAreaId,
                                                   u4FsMIOspfv3VirtIfAuthNeighbor,
                                                   i4FsMIOspfv3VirtIfAuthKeyId,
                                                   pRetValFsMIOspfv3VirtIfAuthKeyStartGenerate);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStopGenerate (INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                           UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                           INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValFsMIOspfv3VirtIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfAuthKeyStopGenerate (u4FsMIOspfv3VirtIfAuthAreaId,
                                                  u4FsMIOspfv3VirtIfAuthNeighbor,
                                                  i4FsMIOspfv3VirtIfAuthKeyId,
                                                  pRetValFsMIOspfv3VirtIfAuthKeyStopGenerate);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStopAccept (INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                         UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                         INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValFsMIOspfv3VirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3VirtIfAuthKeyStopAccept (u4FsMIOspfv3VirtIfAuthAreaId,
                                                u4FsMIOspfv3VirtIfAuthNeighbor,
                                                i4FsMIOspfv3VirtIfAuthKeyId,
                                                pRetValFsMIOspfv3VirtIfAuthKeyStopAccept);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                retValFsMIOspfv3VirtIfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStatus (INT4 i4FsMIStdOspfv3ContextId,
                                     UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                     UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                     INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                     INT4
                                     *pi4RetValFsMIOspfv3VirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3VirtIfAuthKeyStatus (u4FsMIOspfv3VirtIfAuthAreaId,
                                                   u4FsMIOspfv3VirtIfAuthNeighbor,
                                                   i4FsMIOspfv3VirtIfAuthKeyId,
                                                   pi4RetValFsMIOspfv3VirtIfAuthKeyStatus);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3VirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3VirtIfAuthTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIStdOspfv3AreaDfInfOriginate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object
                retValFsMIStdOspfv3AreaDfInfOriginate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIStdOspfv3AreaDfInfOriginate (INT4 i4FsMIStdOspfv3ContextId,
                                       UINT4 u4FsMIStdOspfv3AreaId,
                                       INT4
                                       *pi4RetValFsMIStdOspfv3AreaDfInfOriginate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetOspfv3AreaDfInfOriginate (u4FsMIStdOspfv3AreaId,
                                        pi4RetValFsMIStdOspfv3AreaDfInfOriginate);

    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3RRDMetricTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3RRDMetricTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                          INT4 *pi4FsMIOspfv3RRDProtocolId)
{
    if (V3UtilOspfGetFirstCxtId ((UINT4 *) pi4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4FsMIOspfv3RRDProtocolId = OSPFV3_REDISTRUTE_BGP;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3RRDMetricTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId
                nextFsMIOspfv3RRDProtocolId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3RRDMetricTable (INT4 i4FsMIStdOspfv3ContextId,
                                         INT4 *pi4NextFsMIStdOspfv3ContextId,
                                         INT4 i4FsMIOspfv3RRDProtocolId,
                                         INT4 *pi4NextFsMIOspfv3RRDProtocolId)
{
    INT4                i4Count = i4FsMIOspfv3RRDProtocolId;
    INT4                i4ProtocolId = 0;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSPFV3_SUCCESS)
    {
        if (i4Count < OSPFV3_MAX_PROTO_REDISTRUTE_SIZE)
        {
            i4ProtocolId = i4Count + 1;
            *pi4NextFsMIOspfv3RRDProtocolId = i4ProtocolId;
            return SNMP_SUCCESS;
        }
    }
    while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                    (UINT4 *)
                                    pi4NextFsMIStdOspfv3ContextId)) ==
           OSPFV3_SUCCESS)
    {
        *pi4NextFsMIOspfv3RRDProtocolId = OSPFV3_REDISTRUTE_BGP;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RRDMetricValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                retValFsMIOspfv3RRDMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RRDMetricValue (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3RRDProtocolId,
                                INT4 *pi4RetValFsMIOspfv3RRDMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RRDMetricValue
        (i4FsMIOspfv3RRDProtocolId, pi4RetValFsMIOspfv3RRDMetricValue);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3RRDMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                retValFsMIOspfv3RRDMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3RRDMetricType (INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4FsMIOspfv3RRDProtocolId,
                               INT4 *pi4RetValFsMIOspfv3RRDMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3RRDMetricType
        (i4FsMIOspfv3RRDProtocolId, pi4RetValFsMIOspfv3RRDMetricType);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ClearProcess
 Input       :  The Indices

                The Object
                retValFsMIOspfv3ClearProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ClearProcess (INT4 *pi4RetValFsMIOspfv3ClearProcess)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) OSPFV3_DEFAULT_CXT_ID) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return = nmhGetFutOspfv3ClearProcess (pi4RetValFsMIOspfv3ClearProcess);

    V3UtilReSetContext ();

    return i1Return;

}
