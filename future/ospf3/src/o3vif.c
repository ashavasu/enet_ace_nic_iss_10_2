/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3vif.c,v 1.11 2017/12/26 13:34:29 siva Exp $
 *
 * Description: This file contains procedures for configuration
 *              of virtual interfaces
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE VOID V3VifAddToSortVifLst PROTO ((tV3OsInterface * pInterface));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3VifCreateInCxt                                           */
/*                                                                           */
/* Description  : Creates a virtual interface and initializes it.            */
/*                                                                           */
/* Input        : pV3OspfCxt       : Context pointer                         */
/*                pTransitAreaId   : Area identifier                         */
/*                pNbrId           : Neighbor Router Id                      */
/*                rowStatus        : Row status value associated with the    */
/*                                   virtual interface.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the created virtual interface, on successful    */
/*                creation                                                   */
/*                NULL, Otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsInterface *
V3VifCreateInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTransitAreaId,
                  tV3OsRouterId * pNbrId, tV3OsRowStatus rowStatus)
{
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC : V3VifCreate\n");

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_CONFIGURATION_TRC,
                 pV3OspfCxt->u4ContextId,
                 "Virt If Creation TransAreaId %x NbrId %x\n",
                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pTransitAreaId),
                 OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pNbrId));

    if (V3GetFindAreaInCxt (pV3OspfCxt, pTransitAreaId) == NULL)
    {
        return NULL;
    }

    /* allocate buffer for interface data structure */
    OSPFV3_IF_ALLOC (&(pInterface));
    if (NULL == pInterface)
    {

        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "If Alloc Failure\n");
        return NULL;
    }

    /* initialize variables */
    V3IfSetDefaultValues (pInterface);

    /* Create RB Tree for this interface */
    pInterface->pLinkScopeLsaRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsLsaInfo,
                                              rbNode.RbLinkScopeNode)),
                              V3CompareLsa);
    if (pInterface->pLinkScopeLsaRBRoot == NULL)
    {
        OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                    "Link-Scope LSA RB Tree Creation Failed\n");
        OSPFV3_IF_FREE (pInterface);
        return NULL;
    }

    pInterface->bDcEndpt = OSIX_TRUE;
    OSPFV3_OPT_SET (pInterface->ifOptions, OSPFV3_DC_BIT_MASK);
    pInterface->u2RtrDeadInterval = OSPFV3_DEF_VIRT_IF_RTR_DEAD_INTERVAL;
    pInterface->u2RxmtInterval = OSPFV3_VIRTUAL_IF_RXMT_INTERVAL;
    pInterface->u1NetworkType = OSPFV3_IF_VIRTUAL;
    pInterface->ifStatus = rowStatus;
    pInterface->pArea = pV3OspfCxt->pBackbone;

    if (V3NbrCreate (&gV3OsNullIp6Addr, pNbrId, pInterface,
                     OSPFV3_CONFIGURED_NBR) == NULL)
    {

        OSPFV3_IF_FREE (pInterface);

        OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Virt Nbr Creation Failure\n");

        RBTreeDelete (pInterface->pLinkScopeLsaRBRoot);
        OSPFV3_IF_FREE (pInterface);
        return NULL;
    }

    TMO_SLL_Add (&(pV3OspfCxt->pBackbone->ifsInArea),
                 &(pInterface->nextIfInArea));

    OSPFV3_AREA_ID_COPY (pInterface->transitAreaId, pTransitAreaId);
    OSPFV3_RTR_ID_COPY (pInterface->destRtrId, pNbrId);

    V3VifAddToSortVifLst (pInterface);

    /* The operational status of the interface set to DISABLED */
    pInterface->operStatus = OSPFV3_DISABLED;
    V3SetRtrLsaLsId ((UINT1 *) pInterface, 0, OSIX_FALSE);

    OSPFV3_TRC (CONTROL_PLANE_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId, "Virt If Created\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3VifCreate\n");

    return pInterface;
}

/*****************************************************************************/
/* Function     : V3VifActivate                                              */
/*                                                                           */
/* Description  : Activates the interface                                    */
/*                                                                           */
/* Input        : pInterface     :  The pointer to the interface to be       */
/*                                  activated                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3VifActivate (tV3OsInterface * pInterface, tV3OsArea * pTransitArea)
{
    tIp6Addr            ip6Addr;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsCandteNode    *pSpfNode = NULL;
    UINT1               u1NextEndFound = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "FUNC : V3VifActivate\n");

    /* 
     * search routing table to find cost and interface to be associated with
     * the virtual link 
     */

    if (pInterface->pArea->pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
    {
        if (((pRtEntry = V3RtcFindRtEntryInCxt
              (pInterface->pArea->pV3OspfCxt,
               (VOID *) &pInterface->destRtrId, 0,
               OSPFV3_DEST_AREA_BORDER)) != NULL) &&
            (pRtEntry->u1Flag == OSPFV3_FOUND))
        {
            V3VifSetVifValues (pInterface, pRtEntry);
            u1NextEndFound = OSIX_TRUE;
        }
    }
    else
    {
        if ((pSpfNode = V3RtcSearchSpfTree (pTransitArea->pSpf,
                                            &pInterface->destRtrId, 0,
                                            OSPFV3_VERT_ROUTER)) != NULL)
        {
            V3VifSetVLValues (pInterface, pSpfNode);
            u1NextEndFound = OSIX_TRUE;
        }
    }

    if ((V3RtcSetVirtNextHopInCxt
         (pInterface->pArea->pV3OspfCxt,
          &(pInterface->transitAreaId),
          &(pInterface->destRtrId), &ip6Addr)) == OSIX_TRUE)
    {
        pNbr =
            V3HpSearchNbrLst (&(pInterface->destRtrId), OSPFV3_ZERO,
                              pInterface);
        if (pNbr == NULL)
        {
            return;
        }
        OSPFV3_IP6_ADDR_COPY (pNbr->nbrIpv6Addr, ip6Addr);

        if (u1NextEndFound == OSIX_TRUE)
        {
            pInterface->operStatus = OSPFV3_ENABLED;

            if (V3UtilIp6AddrComp (&pInterface->ifIp6Addr, &gV3OsNullIp6Addr) !=
                OSPFV3_EQUAL)
            {
                V3IfUp (pInterface);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3VifActivate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3VifAddToSortVifLst                                       */
/*                                                                           */
/* Description  : The specified virtual interface is added to the list of    */
/*                virtual interfaces in the appropriate position which is    */
/*                found based on the transit area id and the destination     */
/*                router id.                                                 */
/*                                                                           */
/* Input        : pInterface        : Interface to be added                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3VifAddToSortVifLst (tV3OsInterface * pInterface)
{

    tV3OsInterface     *pLstInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "FUNC : V3VifAddToSortVifLst\n");

    pPrevNode = NULL;

    TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                  pLstNode, tTMO_SLL_NODE *)
    {
        pLstInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                             nextVirtIfNode, pLstNode);

        if (V3UtilVirtIfIndComp (pLstInterface->transitAreaId,
                                 pLstInterface->destRtrId,
                                 pInterface->transitAreaId,
                                 pInterface->destRtrId) == OSPFV3_GREATER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    TMO_SLL_Insert (&(pInterface->pArea->pV3OspfCxt->virtIfLst), pPrevNode,
                    &(pInterface->nextVirtIfNode));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3VifAddToSortVifLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3VifSetVifValues                                          */
/*                                                                           */
/* Description  : Initializes the virtual interface.                         */
/*                                                                           */
/* Input        : pVirtIf           : The virtual interface whose fields     */
/*                                    are to be set                          */
/*                pRtEntry          : The RT entry to the other end point    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3VifSetVifValues (tV3OsInterface * pVirtIf, tV3OsRtEntry * pRtEntry)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsPath          *pPath = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3VifSetVifValues\n");

    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        if (V3UtilAreaIdComp (pVirtIf->transitAreaId,
                              pPath->areaId) == OSPFV3_EQUAL)
        {
            if (pPath->u1HopCount > 0)
            {
                pInterface = pPath->aNextHops[0].pInterface;
            }

            if (pInterface != NULL)
            {
                pVirtIf->admnStatus = OSPFV3_ENABLED;
                /* MTU Size of the Virtual interface is set to the Next hop 
                 * Interfaces MTU Size.
                 */
                pVirtIf->u4MtuSize = pInterface->u4MtuSize;
                /* The virtual interface assigned end pt status as the
                   associated interface */
                pVirtIf->u4OutIfIndex = pInterface->u4InterfaceId;
                OSPFV3_OPT_SET (pVirtIf->ifOptions, OSPFV3_DC_BIT_MASK);
                pVirtIf->u4IfMetric = pPath->u4Cost;
            }
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3VifSetVifValues\n");
}

/*****************************************************************************/
/* Function     : V3VifSetVLValues                                           */
/*                                                                           */
/* Description  : Initializes the virtual interface.                         */
/*                                                                           */
/* Input        : pVirtIf           : The virtual interface whose fields     */
/*                                    are to be set                          */
/*                pSpfNode          : The candicate node                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3VifSetVLValues (tV3OsInterface * pVirtIf, tV3OsCandteNode * pSpfNode)
{
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3VifSetVLValues\n");

    if (pSpfNode->u1HopCount > 0)
    {
        pInterface = pSpfNode->aNextHops[0].pInterface;
        pVirtIf->admnStatus = OSPFV3_ENABLED;
        /* MTU Size of the Virtual interface is set to the Next hop 
         * Interfaces MTU Size.  */
        pVirtIf->u4MtuSize = pInterface->u4MtuSize;

        /* The virtual interface assigned end pt status as the
           associated interface */
        pVirtIf->u4OutIfIndex = pInterface->u4InterfaceId;
        OSPFV3_OPT_SET (pVirtIf->ifOptions, OSPFV3_DC_BIT_MASK);
        pVirtIf->u4IfMetric = pSpfNode->u4Cost;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3VifSetVLValues\n");
}

/*****************************************************************************/
/* Function     : V3VIfDeleteAllInterfaceInCxt                               */
/*                                                                           */
/* Description  : This utility scans and deletes all the interfaces in the   */
/*                context.                                                   */
/*                                                                           */
/* Input        : pV3OspfCxt  - Context pointer                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3VIfDeleteAllInterfaceInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3VIfDeleteAllInterfaceInCxt\n");

    /* Delete the virtual interfaces */
    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR
            (tV3OsInterface, nextVirtIfNode, pLstNode);

        V3IfDelete (pInterface);
        pInterface = NULL;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3VIfDeleteAllInterfaceInCxt\n");
}

/*-----------------------------------------------------------------------*/
/*                   End of the file o3vif.c                             */
/*-----------------------------------------------------------------------*/
