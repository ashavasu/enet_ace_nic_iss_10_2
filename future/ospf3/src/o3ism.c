/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3ism.c,v 1.10 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             interface state machine.
 *
 *******************************************************************/

#include "o3inc.h"
#include "o3ism.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmRunIsm                                                */
/*                                                                           */
/* Description  : It is called whenever an interface state change event      */
/*                occurs. The state event table is maintained as a           */
/*                two-dimensional array of integers. Using the event and     */
/*                the state of the interface as indices to this array, an    */
/*                integer is obtained. This is used as an index to an array  */
/*                of function pointers to obtain the actual action-function  */
/*                to be executed.                                            */
/*                                                                           */
/* Input        : pInterface      :  Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                u1IfEvent       :  The event that has occurred             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmRunIsm (tV3OsInterface * pInterface, UINT1 u1IfEvent)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmRunIsm\n");

    if ((pInterface->u1IsmState >= OSPFV3_MAX_IF_STATE) ||
        (pInterface->u1NetworkType >= OSPFV3_MAX_IF_TYPE) ||
        (u1IfEvent >= OSPFV3_MAX_IF_EVENT))
    {
        return;
    }

    OSPFV3_TRC4 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ISM If %s N/W Type %s CS %s Evt %s\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 gau1Os3DbgIfType[pInterface->u1NetworkType],
                 gau1Os3DbgIfState[pInterface->u1IsmState],
                 gau1Os3DbgIfEvent[u1IfEvent]);

    /* SEM Function is being executed now */
    (*gau1Os3IsmFunc[gau1Os3IsmTable[u1IfEvent][pInterface->u1IsmState]])
        (pInterface);

    if (pInterface->u1IsmState < OSPFV3_MAX_IF_STATE)
    {
        OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "ISM If %s NS %s\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     gau1Os3DbgIfState[pInterface->u1IsmState]);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmRunIsm\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmInvalid                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                This function is invoked when an invalid event occurs in   */
/*                any state. An error message is generated.                  */
/*                                                                           */
/* Input        : pInterface        : Pointer to the interface whose state   */
/*                                    is to be changed                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmInvalid (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmInvalid\n");

    OSPFV3_COUNTER_OP (pInterface->u4IfEvents, 1);

    OSPFV3_TRC1 (OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s Invalid Event\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmInvalid\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmIfUp                                                  */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                It starts the hello timer for sending the periodic hello   */
/*                packets.                                                   */
/*                If the attached network is a physical point-to-point       */
/*                network, then it sets the interface state as               */
/*                point_to_point.                                            */
/*                If the attached network is a virtual link, then it sets    */
/*                the interface state to point_to_point.                     */
/*                If the router is ineligible to become DR, it sets the      */
/*                interface state to DR_other.                               */
/*                Otherwise the interface state is waiting and the           */
/*                wait-timer is started. In addition, if the attached        */
/*                network is NBMA, neighbor event, start is generated for    */
/*                all the eligible neighbors.                                */
/*                                                                           */
/* Input        : pInterface        : Pointer to the interface whose state   */
/*                                    is to be changed                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmIfUp (tV3OsInterface * pInterface)
{

    UINT2               u2HelloInterval = 0;
    UINT1               u1PollFlag = OSIX_FALSE;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsLinkStateId    linkStateId;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER: V3IsmIfUp : If %s \n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    u2HelloInterval = pInterface->u2HelloInterval;

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pInterface))
    {
        /* Since PTOP_IF, only one neighbour will be in nbrsInIf list */
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        }

        if ((pNbr == NULL) ||
            ((pNbr != NULL) && (pNbr->u1NsmState >= OSPFV3_NBRS_INIT)))
        {
            V3IfUpdateState (pInterface, OSPFV3_IFS_PTOP);
        }
        else
        {
            u1PollFlag = OSIX_TRUE;
        }
    }
    else if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
             (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_PTOP);
    }
    else if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_PTOP);
    }
    else if (pInterface->u1RtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY)
    {
        V3IfUpdateState (pInterface, OSPFV3_IFS_DR_OTHER);
    }
    else
    {

        /* multi-access */

        V3IfUpdateState (pInterface, OSPFV3_IFS_WAITING);
        V3TmrSetTimer (&(pInterface->waitTimer), OSPFV3_WAIT_TIMER,
                       (OSPFV3_NO_OF_TICKS_PER_SEC *
                        (pInterface->u2RtrDeadInterval)));
        if (pInterface->u1NetworkType == OSPFV3_IF_NBMA)
        {
            u2HelloInterval = (UINT2) V3UtilJitter
                (u2HelloInterval, OSPFV3_JITTER);

            u1PollFlag = OSIX_TRUE;
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {

                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                            nextNbrInIf, pNbrNode);

                /* Eligible neighbors are present at the start of the nbr
                 * list */

                if (pNbr->u1NbrRtrPriority == OSPFV3_INELIGIBLE_RTR_PRIORITY)
                {
                    break;
                }

                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_START);
            }
        }
    }

    if ((pInterface->bPassive == OSIX_FALSE) ||
        (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL))
    {
        if ((u1PollFlag == OSIX_TRUE) ||
            (OSPFV3_IS_DC_EXT_APPLICABLE_PTOMP_IF (pInterface)))
        {
            V3TmrRestartTimer (&(pInterface->pollTimer), OSPFV3_POLL_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC *
                               (pInterface->u4PollInterval));
        }

        V3TmrRestartTimer (&(pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC * (u2HelloInterval));

    }

    /* If the instance is in unplanned restart state, send the grace LSA */
    if (pInterface->pArea->pV3OspfCxt->u1RestartStatus ==
        OSPFV3_RESTART_UNPLANNED)
    {
        pInterface->pArea->pV3OspfCxt->u1RestartReason = OSPFV3_GR_UNKNOWN;

        OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
        V3GenerateLsa (pInterface->pArea, OSPFV3_GRACE_LSA,
                       &linkStateId, (UINT1 *) pInterface, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmIfUp \n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmElectDr                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                It invokes the function to calculate the BDR and the DR    */
/*                for the attached network. Based on the result the next     */
/*                state is DR_other, Backup or DR.                           */
/*                                                                           */
/* Input        : pInterface       : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmElectDr (tV3OsInterface * pInterface)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmElectDr\n");

    V3HpElectDesgRtr (pInterface);

    OSPFV3_TRC3 (OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "DR/BDR Elected. If %s DR %x BDR %x \n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 OSPFV3_BUFFER_DWFROMPDU (pInterface->desgRtr),
                 OSPFV3_BUFFER_DWFROMPDU (pInterface->backupDesgRtr));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmElectDr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmIfReset                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                All interface variables are reset and the interface timers */
/*                are disabled. All the discovered neighbor connections are  */
/*                destroyed.  Next state of the interface is down.           */
/*                                                                           */
/* Input        : pInterface       : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmIfReset (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmIfReset\n");

    V3IfUpdateState (pInterface, OSPFV3_IFS_DOWN);
    V3IfCleanup (pInterface, OSPFV3_IF_CLEANUP_DOWN);

    /* In this case, the following scenario occurs
     * 1. Static configuration of interface down may be sync-ed to the standby
     *    node. In this case, Reset will be called and DR/BDR will be cleared
     * 2. Second Update state sync-up will be sent to the standby node. In
     *    this case, BDR will be present in thee interface structure and
     *    will be updated in the standby node.
     *
     * Since Clean up is done after update state, sync-up is sent again
     * to the standby node to handle this scenario
     */
    O3RedDynSendIntfInfoToStandby (pInterface, OSIX_FALSE);

    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s Reset\n", Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmIfReset\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmIfDown                                                */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                No action is taken. Next state of the interface is set     */
/*                to DOWN.                                                   */
/*                                                                           */
/* Input        : pInterface       : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmIfDown (tV3OsInterface * pInterface)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmIfDown\n");

    V3IfUpdateState (pInterface, OSPFV3_IFS_DOWN);

    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s DN\n", Ip6PrintAddr (&(pInterface->ifIp6Addr)));
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmIfDown\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmIfLoop                                                */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 9.3                           */
/*                All interface variables are reset and the interface timers */
/*                are disabled. All the discovered neighbor connections      */
/*                are destroyed. Next state of the interface is loopback.    */
/*                                                                           */
/* Input        : pInterface       : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmIfLoop (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmIfLoop\n");

    V3IfUpdateState (pInterface, OSPFV3_IFS_LOOP_BACK);
    V3IfCleanup (pInterface, OSPFV3_IF_CLEANUP_DOWN);

    OSPFV3_TRC1 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s LoopBack\n", Ip6PrintAddr (&(pInterface->ifIp6Addr)));
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmIfLoop\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmElectActive                                           */
/*                                                                           */
/* Description  : Reference : RFC-5340 section 4.9                           */
/*                                                                           */
/* Input        : pInterface       : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmElectActive (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmElectActive\n");

    V3HpElectActiveOnLink (pInterface);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmElectActive\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmResetVariables                                        */
/*                                                                           */
/* Description  : All variables in the interface structure are reset.        */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmResetVariables (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmResetVariables\n");

    OSPFV3_SET_NULL_RTR_ID (pInterface->desgRtr);
    OSPFV3_SET_NULL_RTR_ID (pInterface->backupDesgRtr);
    V3LakClearDelayedAck (pInterface);
    V3LsuClearUpdate (&pInterface->lsUpdate);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmResetVariables\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmDisableIfTimers                                       */
/*                                                                           */
/* Description  : The helloTimer, pollTimer and waitTimer associated with    */
/*                this interface are disabled.                               */
/*                                                                           */
/* Input        : pInterface       : pointer to interface                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmDisableIfTimers (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmDisableIfTimers\n");

    V3TmrDeleteTimer (&(pInterface->helloTimer));
    V3TmrDeleteTimer (&(pInterface->pollTimer));
    V3TmrDeleteTimer (&(pInterface->waitTimer));
    if (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface))
    {
        V3TmrDeleteTimer (&(pInterface->nbrProbeTimer));
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmDisableIfTimers\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmInitSchedQueueInCxt                                   */
/*                                                                           */
/* Description  : Initializes the schedule queue.                            */
/*                                                                           */
/* Input        : pV3OspfCxt    -   Context pointer                          */
/*                pInterface    -   Interface pointer                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmInitSchedQueueInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsInterface * pInterface)
{

    UINT1               u1Index = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IsmInitSchedQueue\n");

    if (pV3OspfCxt != NULL)
    {
        /* Ospfv3Cxt is deleted */
        for (u1Index = 0; ((u1Index < gV3OsRtr.ismSchedQueue.u1FreeEntry) &&
                           (u1Index < OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE));
             u1Index++)
        {
            if (gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface->
                pArea->pV3OspfCxt == pV3OspfCxt)
            {
                gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                    = OSPFV3_FALSE;
            }
        }
    }
    else if (pInterface != NULL)
    {
        /* Interface is deleted */
        for (u1Index = 0; ((u1Index < gV3OsRtr.ismSchedQueue.u1FreeEntry) &&
                           (u1Index < OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE));
             u1Index++)
        {
            if (gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface
                == pInterface)
            {
                gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
                    = OSPFV3_FALSE;
            }
        }
    }
    else
    {
        for (u1Index = 0; u1Index < OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE; u1Index++)
        {
            gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].pInterface = NULL;
            gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].u1Event = 0;
        }
        gV3OsRtr.ismSchedQueue.u1FreeEntry = 0;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IsmInitSchedQueue\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmProcessSchedQueue                                     */
/*                                                                           */
/* Description  : This procedure scans the schedule queue and schedules the  */
/*                ism with the appropriate events.                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmProcessSchedQueue (VOID)
{

    UINT1               u1Index = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IsmProcessSchedQueue\n");

    for (u1Index = 0; ((u1Index < gV3OsRtr.ismSchedQueue.u1FreeEntry) &&
                       (u1Index < OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE)); u1Index++)
    {
        if (gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid
            == OSPFV3_TRUE)
        {

            OSPFV3_GENERATE_IF_EVENT ((gV3OsRtr.ismSchedQueue.
                                       aIfEvents[u1Index].pInterface),
                                      (gV3OsRtr.ismSchedQueue.
                                       aIfEvents[u1Index].u1Event));
            gV3OsRtr.ismSchedQueue.aIfEvents[u1Index].u1IsNodeValid =
                OSPFV3_FALSE;
        }
    }
    gV3OsRtr.ismSchedQueue.u1FreeEntry = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IsmProcessSchedQueue\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmSchedule                                              */
/*                                                                           */
/* Description  : This procedure adds the specified interface and event to   */
/*                the schedule queue and updates the next free entry field in*/
/*                the queue.                                                 */
/*                                                                           */
/* Input        : pInterface       : the interface whose state is to be      */
/*                                   updated                                 */
/*                u1Event          : the interface event that has occurred   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmSchedule (tV3OsInterface * pInterface, UINT1 u1Event)
{

    tV3OsIsmSchedNode  *pSchedNode = NULL;
    UINT1               u1FreeEntry = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmSchedule\n");

    if (gV3OsRtr.ismSchedQueue.u1FreeEntry < OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE)
    {

        u1FreeEntry = gV3OsRtr.ismSchedQueue.u1FreeEntry;
        pSchedNode = &(gV3OsRtr.ismSchedQueue.aIfEvents[u1FreeEntry]);
        pSchedNode->pInterface = pInterface;
        pSchedNode->u1Event = u1Event;
        pSchedNode->u1IsNodeValid = OSPFV3_TRUE;
        gV3OsRtr.ismSchedQueue.u1FreeEntry++;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmSchedule\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmActiveDown                                            */
/*                                                                           */
/* Description  : This procedure takes necessary actions when the active     */
/*                interfaces over the link is made DOWN. When pInterface     */
/*                points to the Active interface itself, it scans            */
/*                Nbrs of all the interfaces of the area and if Active intf  */
/*                is a Nbr, call the same function for the corresponding     */
/*                interface.                                                 */
/*                                                                           */
/* Input        : pInterface       : Pointer to the Active/Standby Interface */
/*                                   over the link                           */
/*                u4DownIfId       : Interface index of the Active which     */
/*                                   went DOWN. The value will be 0 if       */
/*                                   pInterface is equal to the Active Inf   */
/*                                   which is to be made DOWN.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmActiveDown (tV3OsInterface * pInterface, UINT4 u4DownIfId)
{
    tV3OsInterface     *pStandbyInterface = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsNeighbor      *pSbyNbr = NULL;
    tV3OsNeighbor      *pActiveNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmActiveDown\n");
    if (u4DownIfId == OSPFV3_DOWN_ON_LINK)
    {                            /* pInterface is the Standby Interface which is made DOWN */

        TMO_SLL_Scan (&(pInterface->pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
        {
            pStandbyInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfNode);
            if (pStandbyInterface == pInterface)
            {
                continue;
            }
            else
            {                    /* Interfaces other than pInterface itself */
                pNode = NULL;
                TMO_SLL_Scan (&(pStandbyInterface->nbrsInIf),
                              pNode, tTMO_SLL_NODE *)
                {                /* scan the Nbr list of each interface */

                    pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                   nextNbrInIf, pNode);
                    if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
                    {
                        if ((pSbyNbr->u4NbrInterfaceId
                             == pInterface->u4InterfaceId) &&
                            (pSbyNbr->u1NbrIfStatus == OSPFV3_ACTIVE_ON_LINK))
                        {        /* pInterface is a Nbr to pSbyNbr */

                            V3IsmActiveDown (pStandbyInterface,
                                             pInterface->u4InterfaceId);
                            break;
                        }
                    }
                }                /* Nbr Scan */

            }
        }                        /* Interface Scan */

    }
    else
    {                            /* Standby Interfaces */

        pInterface->u4MultActiveIfId = pInterface->u4InterfaceId;
        pNode = NULL;
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
        {
            pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);
            if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
            {
                if (pSbyNbr->u4NbrInterfaceId == u4DownIfId)
                {
                    pActiveNbr = pSbyNbr;
                }
                pSbyNbr->pActiveInterface = NULL;
            }
        }
        OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_DOWN);
        pInterface->u1SdbyActCount = OSPFV3_DEF_IFOVERLINK_COUNT;
        /* Delete the Active Interface from the Nbr List */
        if (pActiveNbr != NULL)
        {
            V3NbrDelete (pActiveNbr);
        }
        OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UP);
        OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmActiveDown\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IsmStandbyDown                                           */
/*                                                                           */
/* Description  : This procedure takes necessary actions when one of the     */
/*                standby interfaces over the link is made DOWN. When        */
/*                pInterface points to the standby interface itself, it scans*/
/*                Nbrs of all the interfaces of the area and if standby intf */
/*                is a Nbr, call the same function for the corresponding     */
/*                interface.                                                 */
/*                                                                           */
/* Input        : pInterface       : Pointer to the Active/Standby Interface */
/*                                   over the link                           */
/*                u4DownIfId       : Interface index of the Standby which    */
/*                                   went DOWN. The value will be 0 if       */
/*                                   pInterface is equal to the Standby Inf  */
/*                                   which is to be made DOWN.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IsmStandbyDown (tV3OsInterface * pInterface, UINT4 u4DownIfId)
{
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsInterface     *pStandbyInterface = NULL;
    tTMO_SLL_NODE      *pIfNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsNeighbor      *pSbyNbr = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IsmStandbyDown\n");
    if (u4DownIfId == OSPFV3_DOWN_ON_LINK)
    {                            /* pInterface is the Standby Interface which is made DOWN */

        TMO_SLL_Scan (&(pInterface->pArea->ifsInArea), pIfNode, tTMO_SLL_NODE *)
        {
            pStandbyInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pIfNode);
            if (pStandbyInterface == pInterface)
            {
                continue;
            }
            else
            {                    /* Interfaces other than pInterface itself */

                pNode = NULL;
                TMO_SLL_Scan (&(pStandbyInterface->nbrsInIf), pNode,
                              tTMO_SLL_NODE *)
                {                /* scan the Nbr list of each interface */

                    pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf,
                                                   pNode);
                    if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
                    {
                        if (pSbyNbr->u4NbrInterfaceId ==
                            pInterface->u4InterfaceId)
                        {        /* pInterface is a Nbr to pSbyNbr */

                            V3IsmStandbyDown (pStandbyInterface,
                                              pInterface->u4InterfaceId);
                            break;
                        }
                    }
                }                /* Nbr Scan */

            }
        }                        /* Interface Scan */

    }
    else
    {                            /* Interfaces other than the Standby Interface which is made DOWN */

        pNode = NULL;
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
        {
            pSbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);
            if (pSbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
            {
                if (pSbyNbr->u4NbrInterfaceId == u4DownIfId)
                {                /* u4DownIfId is the Inf Id of the Standby 
                                   Interface which is brought DOWN */

                    break;
                }
            }
        }

        if (pInterface->u4InterfaceId == pInterface->u4MultActiveIfId)
        {
            V3OspfSendMultIfOnLink (pInterface, IPV6_STANDBY_MODIFY,
                                    u4DownIfId);
        }
        /* Delete the Standby Interface from the Nbr List */
        if (pSbyNbr != NULL)
        {
            V3NbrDelete (pSbyNbr);
        }

        if (pInterface->u4InterfaceId == pInterface->u4MultActiveIfId)
        {                        /* If the  interface is the Active Interface over the link */

            /* Decrement the Standby Nbr Count */
            pInterface->u1SdbyActCount--;
            OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, u4DownIfId);
            if ((pLsaInfo =
                 V3LsuSearchDatabase (OSPFV3_LINK_LSA, &(tmpLinkStateId),
                                      &(pInterface->pArea->pV3OspfCxt->rtrId),
                                      pInterface, NULL)) != NULL)
            {                    /* Flush out the Link LSA of the Standby 
                                   Interface which went DOWN */

                V3AgdFlushOut (pLsaInfo);
            }

            /* Generate Router and Intra Area Prefix LSA */
            V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &(pInterface->linkStateId), NULL, OSIX_FALSE);
            V3GenIntraAreaPrefixLsa (pInterface->pArea);

        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IsmStandbyDown\n");
    return;

}

/*------------------------------------------------------------------------*/
/*                        End of the file  o3ism.c                        */
/*------------------------------------------------------------------------*/
