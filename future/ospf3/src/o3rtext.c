/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtext.c,v 1.12 2017/12/26 13:34:29 siva Exp $
 *
 * Description: This file contains procedures for creating and
 *              deleting Extrenal Routes.
 ********************************************************************/
#include "o3inc.h"

PRIVATE VOID V3RtcCalculateAllExtRoutesInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID        V3RtcCalculateAllNssaExtRoutesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));
PRIVATE VOID V3RtcCalculateNssaAreaRoutes PROTO ((tV3OsArea * pArea));
PRIVATE VOID        V3RtcCalculateExtRoute
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsRtEntry * pDestRtEntry));
PRIVATE VOID        V3RtcCalculateNssaRoute
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsRtEntry * pDestRtEntry));

/*****************************************************************************/
/* Function     : V3RtcCalculateASExtRoutesInCxt                             */
/*                                                                           */
/* Description  : This function invokes calculation of External routes       */
/*                using AS external and NSSA LSAs                            */
/*                                                                           */
/* Input        : pV3OspfCxt   -  Context pointer                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateASExtRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateASExtRoutes\n");

    /* If any external LSAs are present, then only do external route 
     * calculation */
    if (pV3OspfCxt->u4AsExtLsaCount > 0)
    {
        V3RtcCalculateAllExtRoutesInCxt (pV3OspfCxt);
    }

    /* If some NSSA areas are present, then only do following */
    if (pV3OspfCxt->u4NssaAreaCount > 0)
    {
        V3RtcCalculateAllNssaExtRoutesInCxt (pV3OspfCxt);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcCalculateASExtRoutes\n");
}

/*****************************************************************************/
/* Function     : V3RtcCalculateAllExtRoutesInCxt                            */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.4.                         */
/*                Reference : RFC-3101 Section 3.8.4.                        */
/*                This procedure calculates routes to destinations external  */
/*                to the AS by considering all the as external LSAs.         */
/*                                                                           */
/* Input        : pV3OspfCxt - Context pointer                               */
/*                u1Flag - Flag indicating the presence of NSSA areas        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCalculateAllExtRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    UINT4               u4HashIndex = 0;
    tV3OsRtEntry        newRtEntry;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    tV3OsArea          *pArea = NULL;

    UINT4               u4CurrentTime = 0;
    UINT1               u1PrefixOpt = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateAllExtRoutesInCxt\n");

    /* Scan the global hash table to do external route calculation */
    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashIndex)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pV3OspfCxt->pExtLsaHashTable, u4HashIndex,
                                      pDbHashNode, pTmpDbNode, tV3OsDbNode *)
        {
            /* Initializing the route entry */
            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);

            MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
            newRtEntry.u1PrefixLen =
                *(pLsaInfo->pLsa + OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET);
            u1PrefixOpt = *(pLsaInfo->pLsa +
                            OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET + OSPFV3_ONE);
            if (OSPFV3_BIT (u1PrefixOpt, OSPFV3_NU_BIT_MASK))
            {
                /* Prefixes with NU bit set should be ignored */
                continue;
            }

            MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
            OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                         *(pLsaInfo->pLsa +
                                           OSPFV3_AS_EXT_LSA_PREF_OFFSET),
                                         newRtEntry.u1PrefixLen);

            TMO_SLL_Init (&(newRtEntry.pathLst));
            newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
            newRtEntry.u1Flag = 0;

            pOldRtEntry =
                V3RtcFindRtEntryInCxt
                (pV3OspfCxt, (VOID *) &(newRtEntry.destRtPrefix),
                 newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);

            if (pOldRtEntry != NULL)
            {
                pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                if ((pOldRtPath != NULL) &&
                    ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) ||
                     (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)))
                {
                    /* Already intra area or inter area route is present. */
                    continue;
                }
            }

            V3RtcProcessExtLsaDbNode (pDbHashNode, &newRtEntry);

            /* Process all the NSSA LSAs of the same prefix */
            TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                /* If the area is not NSSA, consider the next 
                 * area */
                if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
                {
                    continue;
                }

                if ((pDbNssaNode =
                     V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.destRtPrefix,
                                             newRtEntry.u1PrefixLen,
                                             OSPFV3_NSSA_LSA, pArea)) != NULL)
                {
                    V3RtcProcessNssaLsaDbNode (pDbNssaNode, &newRtEntry);
                }
            }
            /* Process the route entry changes */
            V3RtcProcessRtEntryChangesInCxt
                (pV3OspfCxt, &newRtEntry, pOldRtEntry);

            /* Check whether we need to relinquish the route calculation
             * to do other OSPFv3 processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled 
                 * in previous relinuish */
                if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateAllExtRoutesInCxt\n");

}

/****************************************************************************/
/* Function     : V3RtcCalculateAllNssaExtRoutesInCxt                       */
/*                                                                          */
/* Description  : Reference : RFC-3101 Section 2.5.                         */
/*                This function invokes calculation of External routes      */
/*                by scanning NSSA areas                                    */
/*                                                                          */
/* Input        : pV3OspfCxt  -  Context pointer                            */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/****************************************************************************/
PRIVATE VOID
V3RtcCalculateAllNssaExtRoutesInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateAllNssaExtRoutesInCxt\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            V3RtcCalculateNssaAreaRoutes (pArea);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT | OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateAllNssaExtRoutesInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateNssaAreaRoutes                               */
/*                                                                           */
/* Description  : This function calculate the NSSA routes for remaining Type */
/*                7 LSAs.                                                    */
/*                                                                           */
/* Input        : pArea : Pointer to NSSA Area                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCalculateNssaAreaRoutes (tV3OsArea * pArea)
{
    tV3OsRtEntry        newRtEntry;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pNextArea = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    UINT4               u4HashIndex = 0;
    tV3OsDbNode        *pTmpDbNode = NULL;
    UINT1               u1PrefixOpt = 0;
    UINT4               u4CurrentTime = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY | OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateNssaAreaRoutes\n");

    TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashIndex)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable, u4HashIndex,
                                      pDbHashNode, pTmpDbNode, tV3OsDbNode *)
        {
            if (pDbHashNode->u1Flag != OSPFV3_NOT_USED)
            {
                continue;
            }

            pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

            pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                            pLstNode);

            MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
            newRtEntry.u1PrefixLen =
                *(pLsaInfo->pLsa + OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET);
            u1PrefixOpt = *(pLsaInfo->pLsa +
                            OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET + OSPFV3_ONE);
            if (OSPFV3_BIT (u1PrefixOpt, OSPFV3_NU_BIT_MASK))
            {
                /* Prefixes with NU bit set should be ignored */
                continue;
            }

            MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
            OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                         *(pLsaInfo->pLsa +
                                           OSPFV3_AS_EXT_LSA_PREF_OFFSET),
                                         newRtEntry.u1PrefixLen);

            TMO_SLL_Init (&(newRtEntry.pathLst));
            newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
            newRtEntry.u1Flag = 0;

            pOldRtEntry =
                V3RtcFindRtEntryInCxt
                (pArea->pV3OspfCxt, (VOID *) &(newRtEntry.destRtPrefix),
                 newRtEntry.u1PrefixLen, OSPFV3_DEST_NETWORK);
            if (pOldRtEntry != NULL)
            {
                pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                if ((pOldRtPath != NULL) &&
                    ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) ||
                     (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)))
                {
                    /* Already intra area or inter area route is present. */
                    continue;
                }
            }

            V3RtcProcessNssaLsaDbNode (pDbHashNode, &newRtEntry);

            /* Process NSSA LSAs of other areas */
            pNextArea = pArea;

            while ((pNextArea = (tV3OsArea *)
                    TMO_SLL_Next (&(pArea->pV3OspfCxt->areasLst),
                                  &pNextArea->nextArea)) != NULL)
            {
                if (pNextArea->u4AreaType != OSPFV3_NSSA_AREA)
                {
                    continue;
                }
                if ((pDbNssaNode =
                     V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                             destRtPrefix,
                                             newRtEntry.u1PrefixLen,
                                             OSPFV3_NSSA_LSA,
                                             pNextArea)) != NULL)
                {
                    V3RtcProcessNssaLsaDbNode (pDbNssaNode, &newRtEntry);
                }
            }

            /* Processing the route entry changes */
            V3RtcProcessRtEntryChangesInCxt
                (pArea->pV3OspfCxt, &newRtEntry, pOldRtEntry);

            /* Check whether we need to relinquish the route calculation
             * to do other OSPF processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled 
                 * in previous relinuish */
                if (u4CurrentTime >= pArea->pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pArea->pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT | OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateNssaAreaRoutes\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateExtRoute                                     */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.4.                         */
/*                Reference : RFC-2740 Section 3.8.4                         */
/*                This procedure updates the route to the destination        */
/*                described by the specified LSA.                            */
/*                                                                           */
/* Input        : pLsaInfo     : Pointer to AS-External LSA                  */
/*                pDestRtEntry : Pointer to destination network              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCalculateExtRoute (tV3OsLsaInfo * pLsaInfo, tV3OsRtEntry * pDestRtEntry)
{
    tV3OsArea          *pArea = NULL;
    tV3OsPath          *pDestRtPath = NULL;
    tV3OsPath          *pDestNewPath = NULL;
    tV3OsPath          *pFwdAddrPath = NULL;
    tV3OsRtEntry       *pFwdAddrRt = NULL;
    tV3OsExtLsaLink     extLsaLink;
    UINT1               u1NewPath = OSPFV3_INVALID;
    UINT4               u4Metric = OSPFV3_LS_INFINITY_24BIT;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateExtRoute\n");

    if (V3RtcIsLsaValidForRtCalc (pLsaInfo) == OSIX_FALSE)
    {
        return;
    }

    if (V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                            &extLsaLink) == OSIX_FAILURE)
    {
        return;
    }

    if (extLsaLink.metric.u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        return;
    }
    if (OSPFV3_BIT (extLsaLink.extRtPrefix.u1PrefixOpt, OSPFV3_NU_BIT_MASK))
    {
        return;
    }

    if (extLsaLink.u1FieldBits & OSPFV3_EXT_F_BIT_MASK)
    {
        if ((pFwdAddrRt = V3RtcRtLookupInCxt (pLsaInfo->pV3OspfCxt,
                                              &(extLsaLink.fwdAddr))) == NULL)
        {
            return;
        }

        if (pFwdAddrRt != NULL)
        {
            pFwdAddrPath = OSPFV3_GET_PATH (pFwdAddrRt);
            if (pFwdAddrPath == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Fwd Addr Path Not Present\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                            pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Fwd Addr Path Not Present\n");
                return;
            }

            pArea = V3GetFindAreaInCxt (pLsaInfo->pV3OspfCxt,
                                        &(pFwdAddrPath->areaId));
            if ((pArea == NULL) || (pArea->u4AreaType == OSPFV3_NSSA_AREA))
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Forwarding address is not reachable "
                              "through non NSSA area\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                            pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Forwarding address is not reachable "
                            "through non NSSA area\n");
                return;
            }
        }

        if ((pFwdAddrPath->u1PathType != OSPFV3_INTRA_AREA) &&
            (pFwdAddrPath->u1PathType != OSPFV3_INTER_AREA))
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Forwaring address is not "
                          "internal route. So ignore the LSA\n"));

            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "Forwaring address is not "
                        "internal route. So ignore the LSA\n");
            return;
        }
    }
    else
    {
        if ((pFwdAddrPath =
             V3RtcFindPreferredAsbrPathInCxt (pLsaInfo->pV3OspfCxt,
                                              &(pLsaInfo->lsaId.advRtrId))) ==
            NULL)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "No path exists for ASBR\n"));

            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "No path exists for ASBR\n");
            return;
        }
    }

    if (extLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC)
    {
        u4Metric = pFwdAddrPath->u4Cost + extLsaLink.metric.u4Metric;
    }
    else
    {
        u4Metric = extLsaLink.metric.u4Metric;
    }

    pDestRtPath = OSPFV3_GET_PATH (pDestRtEntry);

    pDestNewPath = pDestRtPath;
    /* If no old path is present. */
    if (pDestRtPath == NULL)
    {
        u1NewPath = OSPFV3_NEW_PATH;
    }
    else if ((pDestRtPath->u1PathType == OSPFV3_TYPE_2_EXT) &&
             (extLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC))

    {
        pDestRtPath->u1PathType = OSPFV3_TYPE_1_EXT;
        pDestRtPath->u4Cost = u4Metric;
        pDestRtPath->u4Type2Cost = 0;
        u1NewPath = OSPFV3_REPLACE_PATH;
    }
    else if ((pDestRtPath->u1PathType == OSPFV3_TYPE_1_EXT) &&
             (extLsaLink.metric.u4MetricType == OSPFV3_TYPE_2_METRIC))
    {
        /* If existing entry is of preferred path type do nothing */
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "Existing Path Type Preferred\n");
        return;
    }
    else
    {
        if (pDestRtPath->u1PathType == OSPFV3_TYPE_1_EXT)
        {
            if (u4Metric < pDestRtPath->u4Cost)
            {
                pDestRtPath->u4Cost = u4Metric;
                u1NewPath = OSPFV3_REPLACE_PATH;
            }
            else if (u4Metric == pDestRtPath->u4Cost)
            {
                u1NewPath = OSPFV3_ADD_HOPS;
            }
            else
            {
                return;
            }
        }
        else
        {
            if (u4Metric < pDestRtPath->u4Type2Cost)
            {
                pDestRtPath->u4Type2Cost = u4Metric;
                pDestRtPath->u4Cost = pFwdAddrPath->u4Cost;
                u1NewPath = OSPFV3_REPLACE_PATH;
            }
            else if (u4Metric == pDestRtPath->u4Type2Cost)
            {
                if (pFwdAddrPath->u4Cost < pDestRtPath->u4Cost)
                {
                    pDestRtPath->u4Cost = pFwdAddrPath->u4Cost;
                    u1NewPath = OSPFV3_REPLACE_PATH;
                }
                else if (pFwdAddrPath->u4Cost == pDestRtPath->u4Cost)
                {
                    u1NewPath = OSPFV3_ADD_HOPS;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }

    pDestNewPath = pDestRtPath;
    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        if (extLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC)
        {
            if ((pDestNewPath =
                 V3RtcCreatePath (&(pFwdAddrPath->areaId),
                                  OSPFV3_TYPE_1_EXT, u4Metric, 0)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for Type 1 External path\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC | OS_RESOURCE_TRC,
                            pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Failed to allocate memory for Type 1 External path\n");
                return;
            }
        }
        else
        {
            if ((pDestNewPath =
                 V3RtcCreatePath (&(pFwdAddrPath->areaId),
                                  OSPFV3_TYPE_2_EXT,
                                  pFwdAddrPath->u4Cost, u4Metric)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for Type 2 External path\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Failed to allocate memory for Type 2 External path\n");
                return;
            }
        }
    }
    else if (u1NewPath == OSPFV3_REPLACE_PATH)
    {
        pDestNewPath->u1HopCount = 0;
    }

    /* Update the next hop information */
    V3RtcUpdateNextHops (pDestNewPath, pFwdAddrPath, pLsaInfo);

    if ((pDestNewPath->u1HopCount > 0) &&
        (pDestNewPath->u1HopCount < OSPFV3_MAX_NEXT_HOPS) &&
        (V3UtilIp6AddrComp (&pFwdAddrPath->aNextHops[0].nbrIpv6Addr,
                            &gV3OsNullIp6Addr) == OSPFV3_EQUAL))
    {
        MEMCPY (&pDestNewPath->aNextHops[pDestNewPath->u1HopCount - 1].
                nbrIpv6Addr, &extLsaLink.fwdAddr, OSPFV3_IPV6_ADDR_LEN);
    }

    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        V3RtcAddPath (pDestRtEntry, pDestNewPath);
    }
    pDestNewPath->pLsaInfo = pLsaInfo;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcCalculateExtRoute\n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
}

/*****************************************************************************/
/* Function     : V3RtcCalculateNssaRoute                                    */
/*                                                                           */
/* Description  : Reference : RFC-3101 Section 2.5                           */
/*                This procedure updates the route to the destination        */
/*                described by the Type 7 LSA.                               */
/*                                                                           */
/* Input        : pLsaInfo     : Pointer to NSSA LSA                         */
/*                pDestRtEntry : Pointer to destination network              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcCalculateNssaRoute (tV3OsLsaInfo * pLsaInfo, tV3OsRtEntry * pDestRtEntry)
{
    tV3OsExtLsaLink     nssaLsaLink;
    tV3OsExtLsaLink     tmpNssaLsaLink;
    tV3OsPath          *pAsbrPath = NULL;
    tV3OsRtEntry       *pFwdAddrRt = NULL;
    tV3OsPath          *pFwdAddrPath = NULL;
    tV3OsPath          *pDestRtPath = NULL;
    tV3OsPath          *pDestNewPath = NULL;
    tV3OsArea          *pArea = NULL;
    UINT4               u4Metric = OSPFV3_LS_INFINITY_24BIT;
    UINT1               u1NewPath = OSPFV3_INVALID;
    UINT1               u1NonZeroFwdAddr = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateNssaRoute\n");

    if (V3RtcIsLsaValidForRtCalc (pLsaInfo) == OSIX_FALSE)
    {
        return;
    }

    if (V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                            &nssaLsaLink) == OSIX_FAILURE)
    {
        return;
    }

    if (nssaLsaLink.metric.u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        return;
    }
    if (OSPFV3_BIT (nssaLsaLink.extRtPrefix.u1PrefixOpt, OSPFV3_NU_BIT_MASK))
    {
        return;
    }
    /* RFC-3101 Section 2.5 
       o  The calculating router is a border router and the LSA has
       its P-bit clear.  Appendix E describes a technique
       whereby an NSSA border router installs a Type-7 default
       LSA without propagating it.
       o  The calculating router is a border router and is
       suppressing the import of summary routes as Type-3
       summary-LSAs.
     */
    if (V3UtilIp6AddrComp (&pDestRtEntry->destRtPrefix,
                           &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
    {
        if (OSPFV3_IS_AREA_BORDER_RTR (pLsaInfo->pV3OspfCxt))
        {
            if ((!(OSPFV3_IS_P_BIT_SET (pLsaInfo->pLsa))) ||
                (pLsaInfo->pArea->u1SummaryFunctionality
                 == OSPFV3_NO_AREA_SUMMARY))
            {
                return;
            }
        }
    }

    if ((pAsbrPath = V3RtcFindAsbrPathInCxt
         (pLsaInfo->pV3OspfCxt,
          &pLsaInfo->lsaId.advRtrId, pLsaInfo->pArea)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "No path exists through non NSSA %x area for ASBR\n",
                      OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pLsaInfo->pArea->
                                               areaId)));

        OSPFV3_TRC1 (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                     "No path exists through non NSSA %x area for ASBR\n",
                     OSPFV3_BUFFER_DWFROMPDU ((UINT1 *) pLsaInfo->pArea->
                                              areaId));
        return;
    }

    if (nssaLsaLink.u1FieldBits & OSPFV3_EXT_F_BIT_MASK)
    {
        u1NonZeroFwdAddr = OSIX_TRUE;
        if ((pFwdAddrRt = V3RtcRtLookupInCxt (pLsaInfo->pV3OspfCxt,
                                              &(nssaLsaLink.fwdAddr))) == NULL)
        {
            return;
        }

        if (pFwdAddrRt != NULL)
        {
            pFwdAddrPath = OSPFV3_GET_PATH (pFwdAddrRt);
            if (pFwdAddrPath == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Fwd Addr Path Not Present\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                            pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Fwd Addr Path Not Present\n");
                return;
            }

            pArea = V3GetFindAreaInCxt (pLsaInfo->pV3OspfCxt,
                                        &(pFwdAddrPath->areaId));
            if ((pArea == NULL) || (pArea->u4AreaType != OSPFV3_NSSA_AREA))
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Forwarding address is not reachable "
                              "through NSSA area\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Forwarding address is not reachable "
                            "through NSSA area\n");
                return;
            }
        }

        if ((pFwdAddrPath->u1PathType != OSPFV3_INTRA_AREA) &&
            (pFwdAddrPath->u1PathType != OSPFV3_INTER_AREA))
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Forwarding address is not internal route."
                          " So ignore the LSA\n"));

            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "Forwaring address is not "
                        "internal route. So ignore the LSA\n");
            return;
        }
    }
    else
    {
        pFwdAddrPath = pAsbrPath;
    }

    if (nssaLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC)
    {
        u4Metric = pFwdAddrPath->u4Cost + nssaLsaLink.metric.u4Metric;
    }
    else
    {
        u4Metric = nssaLsaLink.metric.u4Metric;
    }

    pDestRtPath = OSPFV3_GET_PATH (pDestRtEntry);

    pDestNewPath = pDestRtPath;
    /* If no old path is present. */
    if (pDestRtPath == NULL)
    {
        u1NewPath = OSPFV3_NEW_PATH;
    }
    else if ((pDestRtPath->u1PathType == OSPFV3_TYPE_2_EXT) &&
             (nssaLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC))
    {
        pDestRtPath->u1PathType = OSPFV3_TYPE_1_EXT;
        pDestRtPath->u4Cost = u4Metric;
        pDestRtPath->u4Type2Cost = 0;
        u1NewPath = OSPFV3_REPLACE_PATH;
    }
    else if ((pDestRtPath->u1PathType == OSPFV3_TYPE_1_EXT) &&
             (nssaLsaLink.metric.u4MetricType == OSPFV3_TYPE_2_METRIC))
    {
        /* If existing entry is of preferred path type do nothing */
        OSPFV3_TRC (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "Existing Path Type Preferred\n");
        return;
    }
    else
    {
        if (pDestRtPath->u1PathType == OSPFV3_TYPE_1_EXT)
        {
            if (u4Metric < pDestRtPath->u4Cost)
            {
                pDestRtPath->u4Cost = u4Metric;
                u1NewPath = OSPFV3_REPLACE_PATH;
            }
            else if (u4Metric == pDestRtPath->u4Cost)
            {
                u1NewPath = OSPFV3_ADD_HOPS;
            }
            else
            {
                return;
            }
        }
        else
        {
            if (u4Metric < pDestRtPath->u4Type2Cost)
            {
                pDestRtPath->u4Type2Cost = u4Metric;
                pDestRtPath->u4Cost = pFwdAddrPath->u4Cost;
                u1NewPath = OSPFV3_REPLACE_PATH;
            }
            else if (u4Metric == pDestRtPath->u4Type2Cost)
            {
                if (pFwdAddrPath->u4Cost < pDestRtPath->u4Cost)
                {
                    pDestRtPath->u4Cost = pFwdAddrPath->u4Cost;
                    u1NewPath = OSPFV3_REPLACE_PATH;
                }
                else if (pFwdAddrPath->u4Cost == pDestRtPath->u4Cost)
                {
                    u1NewPath = OSPFV3_ADD_HOPS;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }

    pDestNewPath = pDestRtPath;
    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        if (nssaLsaLink.metric.u4MetricType == OSPFV3_TYPE_1_METRIC)
        {
            /* Creating Type1 external path */
            if ((pDestNewPath =
                 V3RtcCreatePath (&(pLsaInfo->pArea->areaId),
                                  OSPFV3_TYPE_1_EXT, u4Metric, 0)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for Nssa Type 1 external path\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC | OS_RESOURCE_TRC,
                            pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Failed to allocate memory for Nssa Type 1 external path\n");
                return;
            }
        }
        else
        {
            /* Creating Type2 external path */
            if ((pDestNewPath =
                 V3RtcCreatePath (&(pLsaInfo->pArea->areaId),
                                  OSPFV3_TYPE_2_EXT,
                                  pFwdAddrPath->u4Cost, u4Metric)) == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for Nssa Type 2 external path\n"));

                OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                            "Failed to allocate memory for path\n");
                return;
            }
        }
    }
    else if (u1NewPath == OSPFV3_REPLACE_PATH)
    {
        pDestNewPath->u1HopCount = 0;
    }
    else
    {
        /* Reference - RFC 3101 Section 2.5 */
        if (u1NonZeroFwdAddr == 0)
        {
            V3RtcGetExtLsaLink (pDestRtPath->pLsaInfo->pLsa,
                                pDestRtPath->pLsaInfo->u2LsaLen,
                                &tmpNssaLsaLink);

            if ((pDestRtPath->pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) &&
                (V3UtilIp6AddrComp (&tmpNssaLsaLink.fwdAddr,
                                    &nssaLsaLink.fwdAddr) == OSPFV3_EQUAL))
            {
                if (OSPFV3_IS_P_BIT_SET (pLsaInfo->pLsa))
                {
                    u1NewPath = OSPFV3_ADD_HOPS;
                }
                else
                {
                    return;
                }
            }
        }
    }

    /* Update the next hop information */
    V3RtcUpdateNextHops (pDestNewPath, pFwdAddrPath, pLsaInfo);

    if ((pDestNewPath->u1HopCount > 0) &&
        (pDestNewPath->u1HopCount < OSPFV3_MAX_NEXT_HOPS) &&
        (V3UtilIp6AddrComp (&pFwdAddrPath->aNextHops[0].nbrIpv6Addr,
                            &gV3OsNullIp6Addr) == OSPFV3_EQUAL))
    {
        MEMCPY (&pDestNewPath->aNextHops[pDestNewPath->u1HopCount - 1].
                nbrIpv6Addr, &nssaLsaLink.fwdAddr, OSPFV3_IPV6_ADDR_LEN);
    }

    pDestNewPath->pLsaInfo = pLsaInfo;

    if (u1NewPath == OSPFV3_NEW_PATH)
    {
        V3RtcAddPath (pDestRtEntry, pDestNewPath);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateNssaRoute\n");
    KW_FALSEPOSITIVE_FIX (pDestNewPath);
}

/*****************************************************************************/
/* Function     : V3RtcCalculateExtRtInCxt                                   */
/*                                                                           */
/* Description  : This function performs external route calculation for a    */
/*                prefix                                                     */
/*                                                                           */
/* Input        : pV3OspfCxt  : Pointer to Context                           */
/*                pNewRtEntry : Pointer to New route entry                   */
/*                pDbExtLsaNode : Pointer to External LSA DB node            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateExtRtInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry,
                          tV3OsDbNode * pDbExtLsaNode)
{
    tV3OsArea          *pArea = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateExtRtInCxt\n");

    if (pDbExtLsaNode == NULL)
    {
        /* Backbone area pointer is used to get the context pointer */
        pDbExtLsaNode =
            V3RtcSearchLsaHashNode ((VOID *) &pNewRtEntry->destRtPrefix,
                                    pNewRtEntry->u1PrefixLen, OSPFV3_AS_EXT_LSA,
                                    pV3OspfCxt->pBackbone);
    }

    /* Process all the AS external LSAs of the same 
     * prefix */
    if (pDbExtLsaNode != NULL)
    {
        V3RtcProcessExtLsaDbNode (pDbExtLsaNode, pNewRtEntry);
    }

    /* Process all the NSSA LSAs of the same prefix */
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        /* If the area is not NSSA, consider the next 
         * area */
        if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
        {
            continue;
        }

        if ((pDbNssaNode =
             V3RtcSearchLsaHashNode ((VOID *) &pNewRtEntry->destRtPrefix,
                                     pNewRtEntry->u1PrefixLen,
                                     OSPFV3_NSSA_LSA, pArea)) != NULL)
        {
            V3RtcProcessNssaLsaDbNode (pDbNssaNode, pNewRtEntry);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateExtRtInCxt\n");

}

/************************************************************************/
/* Function       : V3RtcProcessExtLsaDbNode                            */
/*                                                                      */
/* Description    : This function process the Inter area prefix         */
/*                  or router LSAs of the DB node                       */
/*                                                                      */
/* Input          : pDbHashNode: DB node of the whose LSA are to be     */
/*                               processed.                             */
/*                  pRtEntry   : Pointer to route entry.                */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/************************************************************************/
PUBLIC VOID
V3RtcProcessExtLsaDbNode (tV3OsDbNode * pDbHashNode,
                          tV3OsRtEntry * pDestRtEntry)
{
    tV3OsLsaInfo       *pExtLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcProcessExtLsaDbNode\n");

    TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pExtLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
        V3RtcCalculateExtRoute (pExtLsaInfo, pDestRtEntry);
    }
    pDbHashNode->u1Flag = OSPFV3_USED;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcProcessExtLsaDbNode\n");

}

/************************************************************************/
/* Function       : V3RtcProcessNssaLsaDbNode                           */
/*                                                                      */
/* Description    : This function process the Inter area prefix         */
/*                  or router LSAs of the DB node                       */
/*                                                                      */
/* Input          : pDbHashNode: DB node of the whose LSA are to be     */
/*                               processed.                             */
/*                  pRtEntry   : Pointer to route entry.                */
/*                                                                      */
/* Output         : None                                                */
/*                                                                      */
/* Returns        : VOID                                                */
/************************************************************************/
PUBLIC VOID
V3RtcProcessNssaLsaDbNode (tV3OsDbNode * pDbHashNode,
                           tV3OsRtEntry * pDestRtEntry)
{
    tV3OsLsaInfo       *pNssaLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcProcessNssaLsaDbNode\n");

    TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNssaLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                            nextLsaInfo, pLstNode);
        V3RtcCalculateNssaRoute (pNssaLsaInfo, pDestRtEntry);
    }
    pDbHashNode->u1Flag = OSPFV3_USED;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcProcessNssaLsaDbNode\n");

}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rtext.c                        */
/*----------------------------------------------------------------------*/
