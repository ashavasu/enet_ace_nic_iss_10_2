/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3mitest.c,v 1.9 2017/12/26 13:34:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "o3inc.h"
# include "fsmisolw.h"
# include "fsmio3lw.h"
# include "ospf3lw.h"
# include "fsos3lw.h"

/* LOW LEVEL Routines for Table : FsMIStdOspfv3Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3Table (INT4 i4FsMIStdOspfv3ContextId)
{
    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3RouterId
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3RouterId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3RouterId (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3ContextId,
                                UINT4 u4TestValFsMIStdOspfv3RouterId)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3RouterId (pu4ErrorCode, u4TestValFsMIStdOspfv3RouterId);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AdminStat
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3AdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AdminStat (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMIStdOspfv3ContextId,
                                 INT4 i4TestValFsMIStdOspfv3AdminStat)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AdminStat (pu4ErrorCode,
                                  i4TestValFsMIStdOspfv3AdminStat);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3ASBdrRtrStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3ASBdrRtrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3ASBdrRtrStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4TestValFsMIStdOspfv3ASBdrRtrStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3ASBdrRtrStatus (pu4ErrorCode,
                                       i4TestValFsMIStdOspfv3ASBdrRtrStatus);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3ExtAreaLsdbLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3ExtAreaLsdbLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3ExtAreaLsdbLimit (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfv3ContextId,
                                        INT4
                                        i4TestValFsMIStdOspfv3ExtAreaLsdbLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3ExtAreaLsdbLimit (pu4ErrorCode,
                                         i4TestValFsMIStdOspfv3ExtAreaLsdbLimit);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3MulticastExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3MulticastExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3MulticastExtensions (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pTestValFsMIStdOspfv3MulticastExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3MulticastExtensions (pu4ErrorCode,
                                            pTestValFsMIStdOspfv3MulticastExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3ExitOverflowInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3ExitOverflowInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3ExitOverflowInterval (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4
                                            u4TestValFsMIStdOspfv3ExitOverflowInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3ExitOverflowInterval (pu4ErrorCode,
                                             u4TestValFsMIStdOspfv3ExitOverflowInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3DemandExtensions
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3DemandExtensions
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3DemandExtensions (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfv3ContextId,
                                        INT4
                                        i4TestValFsMIStdOspfv3DemandExtensions)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3DemandExtensions (pu4ErrorCode,
                                         i4TestValFsMIStdOspfv3DemandExtensions);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3TrafficEngineeringSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3TrafficEngineeringSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3TrafficEngineeringSupport (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsMIStdOspfv3ContextId,
                                                 INT4
                                                 i4TestValFsMIStdOspfv3TrafficEngineeringSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3TrafficEngineeringSupport (pu4ErrorCode,
                                                  i4TestValFsMIStdOspfv3TrafficEngineeringSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3ReferenceBandwidth
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3ReferenceBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3ReferenceBandwidth (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4
                                          u4TestValFsMIStdOspfv3ReferenceBandwidth)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3ReferenceBandwidth (pu4ErrorCode,
                                           u4TestValFsMIStdOspfv3ReferenceBandwidth);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3RestartSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3RestartSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3RestartSupport (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4TestValFsMIStdOspfv3RestartSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3RestartSupport (pu4ErrorCode,
                                       i4TestValFsMIStdOspfv3RestartSupport);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3RestartInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3RestartInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3RestartInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3ContextId,
                                       INT4
                                       i4TestValFsMIStdOspfv3RestartInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3RestartInterval (pu4ErrorCode,
                                        i4TestValFsMIStdOspfv3RestartInterval);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3Status
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIStdOspfv3Status
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3Status (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4TestValFsMIStdOspfv3Status)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if (i4TestValFsMIStdOspfv3Status == DESTROY)
    {
        return SNMP_SUCCESS;
    }
    if (V3OspfVcmIsVcExist (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return i1RetVal;
    }

    if ((OSPFV3_MAX_CONTEXTS_LIMIT > SYS_DEF_MAX_NUM_CONTEXTS) &&
        ((i4FsMIStdOspfv3ContextId + 1) > SYS_DEF_MAX_NUM_CONTEXTS))
    {
        /* This check is added to verify that no of contexts supported by OSPF
         * should be lesser than the no of contexts that is defined in system.
         * In system.size file, MAX_OSPFV3_CONTEXTS value should be less than
         * the value of SYS_DEF_MAX_NUM_CONTEXTS.*/
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_CONTEXTS_EXCEED_SYSTEM);
        return i1RetVal;
    }

    if ((UINT4) (i4FsMIStdOspfv3ContextId + 1) > OSPFV3_MAX_CONTEXTS_LIMIT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_CONTEXTS_EXCEED_MAX);
        return i1RetVal;
    }

    pV3OspfCxt = V3UtilOspfGetCxt (i4FsMIStdOspfv3ContextId);

    switch (i4TestValFsMIStdOspfv3Status)
    {
        case NOT_READY:
        {
            if (pV3OspfCxt == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            break;
        }
        case NOT_IN_SERVICE:
        case ACTIVE:
        {
            if (pV3OspfCxt == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
            break;
        }
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
        {
            if (pV3OspfCxt != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            }
            else
            {
                i1RetVal = SNMP_SUCCESS;
            }
            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        }
    }
    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3AreaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3AreaTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaTable (INT4 i4FsMIStdOspfv3ContextId,
                                                UINT4 u4FsMIStdOspfv3AreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceOspfv3AreaTable (u4FsMIStdOspfv3AreaId);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3ImportAsExtern
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3ImportAsExtern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3ImportAsExtern (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3ContextId,
                                      UINT4 u4FsMIStdOspfv3AreaId,
                                      INT4 i4TestValFsMIStdOspfv3ImportAsExtern)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3ImportAsExtern (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                       i4TestValFsMIStdOspfv3ImportAsExtern);

    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaSummary
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3AreaSummary
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaSummary (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3AreaId,
                                   INT4 i4TestValFsMIStdOspfv3AreaSummary)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaSummary (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                    i4TestValFsMIStdOspfv3AreaSummary);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3StubMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3StubMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3StubMetric (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  UINT4 u4FsMIStdOspfv3AreaId,
                                  INT4 i4TestValFsMIStdOspfv3StubMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3StubMetric (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                   i4TestValFsMIStdOspfv3StubMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaNssaTranslatorRole
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3AreaNssaTranslatorRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaNssaTranslatorRole (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIStdOspfv3ContextId,
                                              UINT4 u4FsMIStdOspfv3AreaId,
                                              INT4
                                              i4TestValFsMIStdOspfv3AreaNssaTranslatorRole)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaNssaTranslatorRole (pu4ErrorCode,
                                               u4FsMIStdOspfv3AreaId,
                                               i4TestValFsMIStdOspfv3AreaNssaTranslatorRole);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaNssaTranslatorStabilityInterval (UINT4 *pu4ErrorCode,
                                                           INT4
                                                           i4FsMIStdOspfv3ContextId,
                                                           UINT4
                                                           u4FsMIStdOspfv3AreaId,
                                                           UINT4
                                                           u4TestValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaNssaTranslatorStabilityInterval (pu4ErrorCode,
                                                            u4FsMIStdOspfv3AreaId,
                                                            u4TestValFsMIStdOspfv3AreaNssaTranslatorStabilityInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaStubMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3AreaStubMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaStubMetricType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3AreaId,
                                          INT4
                                          i4TestValFsMIStdOspfv3AreaStubMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaStubMetricType (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                           i4TestValFsMIStdOspfv3AreaStubMetricType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object 
                testValFsMIStdOspfv3AreaStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  UINT4 u4FsMIStdOspfv3AreaId,
                                  INT4 i4TestValFsMIStdOspfv3AreaStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaStatus (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                   i4TestValFsMIStdOspfv3AreaStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3AsLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3AsLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AsLsdbType
                FsMIStdOspfv3AsLsdbRouterId
                FsMIStdOspfv3AsLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3AsLsdbTable (INT4 i4FsMIStdOspfv3ContextId,
                                                  UINT4
                                                  u4FsMIStdOspfv3AsLsdbType,
                                                  UINT4
                                                  u4FsMIStdOspfv3AsLsdbRouterId,
                                                  UINT4
                                                  u4FsMIStdOspfv3AsLsdbLsid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3AsLsdbTable (u4FsMIStdOspfv3AsLsdbType,
                                                   u4FsMIStdOspfv3AsLsdbRouterId,
                                                   u4FsMIStdOspfv3AsLsdbLsid);
    V3UtilReSetContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3AreaLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3AreaLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaLsdbAreaId
                FsMIStdOspfv3AreaLsdbType
                FsMIStdOspfv3AreaLsdbRouterId
                FsMIStdOspfv3AreaLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaLsdbTable (INT4
                                                    i4FsMIStdOspfv3ContextId,
                                                    UINT4
                                                    u4FsMIStdOspfv3AreaLsdbAreaId,
                                                    UINT4
                                                    u4FsMIStdOspfv3AreaLsdbType,
                                                    UINT4
                                                    u4FsMIStdOspfv3AreaLsdbRouterId,
                                                    UINT4
                                                    u4FsMIStdOspfv3AreaLsdbLsid)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3AreaLsdbTable
        (u4FsMIStdOspfv3AreaLsdbAreaId, u4FsMIStdOspfv3AreaLsdbType,
         u4FsMIStdOspfv3AreaLsdbRouterId, u4FsMIStdOspfv3AreaLsdbLsid);
    V3UtilReSetContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3LinkLsdbTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3LinkLsdbTable
 Input       :  The Indices
                FsMIStdOspfv3LinkLsdbIfIndex
                FsMIStdOspfv3LinkLsdbType
                FsMIStdOspfv3LinkLsdbRouterId
                FsMIStdOspfv3LinkLsdbLsid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3LinkLsdbTable (INT4
                                                    i4FsMIStdOspfv3LinkLsdbIfIndex,
                                                    UINT4
                                                    u4FsMIStdOspfv3LinkLsdbType,
                                                    UINT4
                                                    u4FsMIStdOspfv3LinkLsdbRouterId,
                                                    UINT4
                                                    u4FsMIStdOspfv3LinkLsdbLsid)
{

    return (nmhValidateIndexInstanceOspfv3LinkLsdbTable
            (i4FsMIStdOspfv3LinkLsdbIfIndex, u4FsMIStdOspfv3LinkLsdbType,
             u4FsMIStdOspfv3LinkLsdbRouterId, u4FsMIStdOspfv3LinkLsdbLsid));
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3HostTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3HostTable (INT4 i4FsMIStdOspfv3ContextId,
                                                INT4
                                                i4FsMIStdOspfv3HostAddressType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIStdOspfv3HostAddress)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3HostTable (i4FsMIStdOspfv3HostAddressType,
                                                 pFsMIStdOspfv3HostAddress);
    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3HostMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                testValFsMIStdOspfv3HostMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3HostMetric (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4FsMIStdOspfv3HostAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdOspfv3HostAddress,
                                  INT4 i4TestValFsMIStdOspfv3HostMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3HostMetric (pu4ErrorCode, i4FsMIStdOspfv3HostAddressType,
                                   pFsMIStdOspfv3HostAddress,
                                   i4TestValFsMIStdOspfv3HostMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3HostAreaID
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                testValFsMIStdOspfv3HostAreaID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3HostAreaID (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4FsMIStdOspfv3HostAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdOspfv3HostAddress,
                                  UINT4 u4TestValFsMIStdOspfv3HostAreaID)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3HostAreaID (pu4ErrorCode, i4FsMIStdOspfv3HostAddressType,
                                   pFsMIStdOspfv3HostAddress,
                                   u4TestValFsMIStdOspfv3HostAreaID);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3HostStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3HostAddressType
                FsMIStdOspfv3HostAddress

                The Object 
                testValFsMIStdOspfv3HostStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3HostStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4FsMIStdOspfv3HostAddressType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsMIStdOspfv3HostAddress,
                                  INT4 i4TestValFsMIStdOspfv3HostStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3HostStatus (pu4ErrorCode, i4FsMIStdOspfv3HostAddressType,
                                   pFsMIStdOspfv3HostAddress,
                                   i4TestValFsMIStdOspfv3HostStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3IfTable
 Input       :  The Indices
                FsMIStdOspfv3IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3IfTable (INT4 i4FsMIStdOspfv3IfIndex)
{
    return (nmhValidateIndexInstanceOspfv3IfTable (i4FsMIStdOspfv3IfIndex));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfAreaId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfAreaId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfAreaId (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3IfIndex,
                                UINT4 u4TestValFsMIStdOspfv3IfAreaId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfAreaId (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
                                        u4TestValFsMIStdOspfv3IfAreaId);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfType
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfType (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfv3IfIndex,
                              INT4 i4TestValFsMIStdOspfv3IfType)
{
    return (nmhTestv2Ospfv3IfType
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfType));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfAdminStat
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfAdminStat
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfAdminStat (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3IfIndex,
                                   INT4 i4TestValFsMIStdOspfv3IfAdminStat)
{
    return (nmhTestv2Ospfv3IfAdminStat
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfAdminStat));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfRtrPriority
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfRtrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfRtrPriority (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdOspfv3IfIndex,
                                     INT4 i4TestValFsMIStdOspfv3IfRtrPriority)
{
    return (nmhTestv2Ospfv3IfRtrPriority
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfRtrPriority));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfTransitDelay (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3IfIndex,
                                      INT4 i4TestValFsMIStdOspfv3IfTransitDelay)
{
    return (nmhTestv2Ospfv3IfTransitDelay
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfTransitDelay));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfRetransInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3IfIndex,
                                         INT4
                                         i4TestValFsMIStdOspfv3IfRetransInterval)
{
    return (nmhTestv2Ospfv3IfRetransInterval
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfRetransInterval));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfHelloInterval (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3IfIndex,
                                       INT4
                                       i4TestValFsMIStdOspfv3IfHelloInterval)
{
    return (nmhTestv2Ospfv3IfHelloInterval
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfHelloInterval));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3IfIndex,
                                         INT4
                                         i4TestValFsMIStdOspfv3IfRtrDeadInterval)
{
    return (nmhTestv2Ospfv3IfRtrDeadInterval
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfRtrDeadInterval));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfPollInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfPollInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfPollInterval (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3IfIndex,
                                      UINT4
                                      u4TestValFsMIStdOspfv3IfPollInterval)
{
    return (nmhTestv2Ospfv3IfPollInterval
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             u4TestValFsMIStdOspfv3IfPollInterval));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfMulticastForwarding
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfMulticastForwarding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfMulticastForwarding (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3IfIndex,
                                             INT4
                                             i4TestValFsMIStdOspfv3IfMulticastForwarding)
{
    return (nmhTestv2Ospfv3IfMulticastForwarding
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfMulticastForwarding));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfDemand
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfDemand
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfDemand (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 i4TestValFsMIStdOspfv3IfDemand)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfDemand (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
                                        i4TestValFsMIStdOspfv3IfDemand);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfMetricValue
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfMetricValue (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdOspfv3IfIndex,
                                     INT4 i4TestValFsMIStdOspfv3IfMetricValue)
{
    return (nmhTestv2Ospfv3IfMetricValue
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfMetricValue));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfInstId
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfInstId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfInstId (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 i4TestValFsMIStdOspfv3IfInstId)
{
    return (nmhTestv2Ospfv3IfInstId
            (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
             i4TestValFsMIStdOspfv3IfInstId));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfDemandNbrProbe
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfDemandNbrProbe
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbe (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfv3IfIndex,
                                        INT4
                                        i4TestValFsMIStdOspfv3IfDemandNbrProbe)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfDemandNbrProbe (pu4ErrorCode,
                                                i4FsMIStdOspfv3IfIndex,
                                                i4TestValFsMIStdOspfv3IfDemandNbrProbe);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbeRetxLimit (UINT4 *pu4ErrorCode,
                                                 INT4 i4FsMIStdOspfv3IfIndex,
                                                 UINT4
                                                 u4TestValFsMIStdOspfv3IfDemandNbrProbeRetxLimit)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit (pu4ErrorCode,
                                                         i4FsMIStdOspfv3IfIndex,
                                                         u4TestValFsMIStdOspfv3IfDemandNbrProbeRetxLimit);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfDemandNbrProbeInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbeInterval (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIStdOspfv3IfIndex,
                                                UINT4
                                                u4TestValFsMIStdOspfv3IfDemandNbrProbeInterval)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfDemandNbrProbeInterval (pu4ErrorCode,
                                                        i4FsMIStdOspfv3IfIndex,
                                                        u4TestValFsMIStdOspfv3IfDemandNbrProbeInterval);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3IfStatus
 Input       :  The Indices
                FsMIStdOspfv3IfIndex

                The Object 
                testValFsMIStdOspfv3IfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3IfStatus (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3IfIndex,
                                INT4 i4TestValFsMIStdOspfv3IfStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3IfStatus (pu4ErrorCode, i4FsMIStdOspfv3IfIndex,
                                        i4TestValFsMIStdOspfv3IfStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3VirtIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3VirtIfTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3VirtIfTable (INT4 i4FsMIStdOspfv3ContextId,
                                                  UINT4
                                                  u4FsMIStdOspfv3VirtIfAreaId,
                                                  UINT4
                                                  u4FsMIStdOspfv3VirtIfNeighbor)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3VirtIfTable (u4FsMIStdOspfv3VirtIfAreaId,
                                                   u4FsMIStdOspfv3VirtIfNeighbor);

    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfIndex
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfIndex (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                   UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                   INT4 i4TestValFsMIStdOspfv3VirtIfIndex)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfIndex (pu4ErrorCode, u4FsMIStdOspfv3VirtIfAreaId,
                                    u4FsMIStdOspfv3VirtIfNeighbor,
                                    i4TestValFsMIStdOspfv3VirtIfIndex);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfTransitDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfTransitDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfTransitDelay (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                          UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                          INT4
                                          i4TestValFsMIStdOspfv3VirtIfTransitDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfTransitDelay (pu4ErrorCode,
                                           u4FsMIStdOspfv3VirtIfAreaId,
                                           u4FsMIStdOspfv3VirtIfNeighbor,
                                           i4TestValFsMIStdOspfv3VirtIfTransitDelay);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfRetransInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfRetransInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfRetransInterval (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3ContextId,
                                             UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                             UINT4
                                             u4FsMIStdOspfv3VirtIfNeighbor,
                                             INT4
                                             i4TestValFsMIStdOspfv3VirtIfRetransInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfRetransInterval (pu4ErrorCode,
                                              u4FsMIStdOspfv3VirtIfAreaId,
                                              u4FsMIStdOspfv3VirtIfNeighbor,
                                              i4TestValFsMIStdOspfv3VirtIfRetransInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfHelloInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfHelloInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfHelloInterval (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                           UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                           INT4
                                           i4TestValFsMIStdOspfv3VirtIfHelloInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfHelloInterval (pu4ErrorCode,
                                            u4FsMIStdOspfv3VirtIfAreaId,
                                            u4FsMIStdOspfv3VirtIfNeighbor,
                                            i4TestValFsMIStdOspfv3VirtIfHelloInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfRtrDeadInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfRtrDeadInterval (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3ContextId,
                                             UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                             UINT4
                                             u4FsMIStdOspfv3VirtIfNeighbor,
                                             INT4
                                             i4TestValFsMIStdOspfv3VirtIfRtrDeadInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfRtrDeadInterval (pu4ErrorCode,
                                              u4FsMIStdOspfv3VirtIfAreaId,
                                              u4FsMIStdOspfv3VirtIfNeighbor,
                                              i4TestValFsMIStdOspfv3VirtIfRtrDeadInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3VirtIfStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIStdOspfv3VirtIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3VirtIfStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                    UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                    INT4 i4TestValFsMIStdOspfv3VirtIfStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3VirtIfStatus (pu4ErrorCode, u4FsMIStdOspfv3VirtIfAreaId,
                                     u4FsMIStdOspfv3VirtIfNeighbor,
                                     i4TestValFsMIStdOspfv3VirtIfStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3NbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3NbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3NbrTable (INT4 i4FsMIStdOspfv3NbrIfIndex,
                                               UINT4 u4FsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex (i4FsMIStdOspfv3NbrIfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3NbrTable (i4FsMIStdOspfv3NbrIfIndex,
                                                u4FsMIStdOspfv3NbrRtrId);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3NbmaNbrTable
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3NbmaNbrTable (INT4
                                                   i4FsMIStdOspfv3NbmaNbrIfIndex,
                                                   INT4
                                                   i4FsMIStdOspfv3NbmaNbrAddressType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMIStdOspfv3NbmaNbrAddress)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex
        ((UINT4) i4FsMIStdOspfv3NbmaNbrIfIndex, &u4CxtId) == OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3NbmaNbrTable
        (i4FsMIStdOspfv3NbmaNbrIfIndex, i4FsMIStdOspfv3NbmaNbrAddressType,
         pFsMIStdOspfv3NbmaNbrAddress);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3NbmaNbrPriority
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                testValFsMIStdOspfv3NbmaNbrPriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3NbmaNbrPriority (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                       INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIStdOspfv3NbmaNbrAddress,
                                       INT4
                                       i4TestValFsMIStdOspfv3NbmaNbrPriority)
{
    INT1                i1Return = SNMP_FAILURE;
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3NbmaNbrPriority (pu4ErrorCode,
                                               i4FsMIStdOspfv3NbmaNbrIfIndex,
                                               i4FsMIStdOspfv3NbmaNbrAddressType,
                                               pFsMIStdOspfv3NbmaNbrAddress,
                                               i4TestValFsMIStdOspfv3NbmaNbrPriority);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3NbmaNbrStorageType
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                testValFsMIStdOspfv3NbmaNbrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3NbmaNbrStorageType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                          INT4
                                          i4FsMIStdOspfv3NbmaNbrAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIStdOspfv3NbmaNbrAddress,
                                          INT4
                                          i4TestValFsMIStdOspfv3NbmaNbrStorageType)
{
    return (nmhTestv2Ospfv3NbmaNbrStorageType
            (pu4ErrorCode, i4FsMIStdOspfv3NbmaNbrIfIndex,
             i4FsMIStdOspfv3NbmaNbrAddressType, pFsMIStdOspfv3NbmaNbrAddress,
             i4TestValFsMIStdOspfv3NbmaNbrStorageType));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3NbmaNbrStatus
 Input       :  The Indices
                FsMIStdOspfv3NbmaNbrIfIndex
                FsMIStdOspfv3NbmaNbrAddressType
                FsMIStdOspfv3NbmaNbrAddress

                The Object 
                testValFsMIStdOspfv3NbmaNbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3NbmaNbrStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIStdOspfv3NbmaNbrIfIndex,
                                     INT4 i4FsMIStdOspfv3NbmaNbrAddressType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMIStdOspfv3NbmaNbrAddress,
                                     INT4 i4TestValFsMIStdOspfv3NbmaNbrStatus)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIStdOspfv3NbmaNbrIfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2Ospfv3NbmaNbrStatus (pu4ErrorCode,
                                             i4FsMIStdOspfv3NbmaNbrIfIndex,
                                             i4FsMIStdOspfv3NbmaNbrAddressType,
                                             pFsMIStdOspfv3NbmaNbrAddress,
                                             i4TestValFsMIStdOspfv3NbmaNbrStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIStdOspfv3VirtNbrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3VirtNbrTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtNbrArea
                FsMIStdOspfv3VirtNbrRtrId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3VirtNbrTable (INT4
                                                   i4FsMIStdOspfv3ContextId,
                                                   UINT4
                                                   u4FsMIStdOspfv3VirtNbrArea,
                                                   UINT4
                                                   u4FsMIStdOspfv3VirtNbrRtrId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3VirtNbrTable (u4FsMIStdOspfv3VirtNbrArea,
                                                    u4FsMIStdOspfv3VirtNbrRtrId);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIStdOspfv3AreaAggregateTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaAggregateTable (INT4
                                                         i4FsMIStdOspfv3ContextId,
                                                         UINT4
                                                         u4FsMIStdOspfv3AreaAggregateAreaID,
                                                         INT4
                                                         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                                         INT4
                                                         i4FsMIStdOspfv3AreaAggregatePrefixType,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFsMIStdOspfv3AreaAggregatePrefix,
                                                         UINT4
                                                         u4FsMIStdOspfv3AreaAggregatePrefixLength)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceOspfv3AreaAggregateTable
        (u4FsMIStdOspfv3AreaAggregateAreaID,
         i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
         i4FsMIStdOspfv3AreaAggregatePrefixType,
         pFsMIStdOspfv3AreaAggregatePrefix,
         u4FsMIStdOspfv3AreaAggregatePrefixLength);

    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaAggregateEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                testValFsMIStdOspfv3AreaAggregateEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaAggregateEffect (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4
                                           u4FsMIStdOspfv3AreaAggregateAreaID,
                                           INT4
                                           i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                           INT4
                                           i4FsMIStdOspfv3AreaAggregatePrefixType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdOspfv3AreaAggregatePrefix,
                                           UINT4
                                           u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                           INT4
                                           i4TestValFsMIStdOspfv3AreaAggregateEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaAggregateEffect (pu4ErrorCode,
                                            u4FsMIStdOspfv3AreaAggregateAreaID,
                                            i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                            i4FsMIStdOspfv3AreaAggregatePrefixType,
                                            pFsMIStdOspfv3AreaAggregatePrefix,
                                            u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                            i4TestValFsMIStdOspfv3AreaAggregateEffect);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaAggregateRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                testValFsMIStdOspfv3AreaAggregateRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaAggregateRouteTag (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3ContextId,
                                             UINT4
                                             u4FsMIStdOspfv3AreaAggregateAreaID,
                                             INT4
                                             i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                             INT4
                                             i4FsMIStdOspfv3AreaAggregatePrefixType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFsMIStdOspfv3AreaAggregatePrefix,
                                             UINT4
                                             u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                             INT4
                                             i4TestValFsMIStdOspfv3AreaAggregateRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaAggregateRouteTag (pu4ErrorCode,
                                              u4FsMIStdOspfv3AreaAggregateAreaID,
                                              i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                              i4FsMIStdOspfv3AreaAggregatePrefixType,
                                              pFsMIStdOspfv3AreaAggregatePrefix,
                                              u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                              i4TestValFsMIStdOspfv3AreaAggregateRouteTag);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaAggregateStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaAggregateAreaID
                FsMIStdOspfv3AreaAggregateAreaLsdbType
                FsMIStdOspfv3AreaAggregatePrefixType
                FsMIStdOspfv3AreaAggregatePrefix
                FsMIStdOspfv3AreaAggregatePrefixLength

                The Object 
                testValFsMIStdOspfv3AreaAggregateStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaAggregateStatus (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           UINT4
                                           u4FsMIStdOspfv3AreaAggregateAreaID,
                                           INT4
                                           i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                           INT4
                                           i4FsMIStdOspfv3AreaAggregatePrefixType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIStdOspfv3AreaAggregatePrefix,
                                           UINT4
                                           u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                           INT4
                                           i4TestValFsMIStdOspfv3AreaAggregateStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaAggregateStatus (pu4ErrorCode,
                                            u4FsMIStdOspfv3AreaAggregateAreaID,
                                            i4FsMIStdOspfv3AreaAggregateAreaLsdbType,
                                            i4FsMIStdOspfv3AreaAggregatePrefixType,
                                            pFsMIStdOspfv3AreaAggregatePrefix,
                                            u4FsMIStdOspfv3AreaAggregatePrefixLength,
                                            i4TestValFsMIStdOspfv3AreaAggregateStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/*-------------------------------- fsmioslw.c ------------------------------ */

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3GlobalTraceLevel
 Input       :  The Indices

                The Object 
                testValFsMIOspfv3GlobalTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3GlobalTraceLevel (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMIOspfv3GlobalTraceLevel)
{
    UNUSED_PARAM (i4TestValFsMIOspfv3GlobalTraceLevel);
    UNUSED_PARAM (pu4ErrorCode);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VrfSpfInterval
 Input       :  The Indices

                The Object 
                testValFsMIOspfv3VrfSpfInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VrfSpfInterval (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMIOspfv3VrfSpfInterval)
{
    if (i4TestValFsMIOspfv3VrfSpfInterval > 1000 ||
        i4TestValFsMIOspfv3VrfSpfInterval < 10)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object 
                testValFsMIOspfv3RTStaggeringStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RTStaggeringStatus (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValFsMIOspfv3RTStaggeringStatus)
{
    UNUSED_PARAM (i4TestValFsMIOspfv3RTStaggeringStatus);
    UNUSED_PARAM (pu4ErrorCode);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3Table. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3Table
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3Table (INT4 i4FsMIStdOspfv3ContextId)
{
    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3TraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3TraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3TraceLevel (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIStdOspfv3ContextId,
                               INT4 i4TestValFsMIOspfv3TraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3TraceLevel (pu4ErrorCode,
                                      i4TestValFsMIOspfv3TraceLevel);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ABRType
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3ABRType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ABRType (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfv3ContextId,
                            INT4 i4TestValFsMIOspfv3ABRType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3ABRType (pu4ErrorCode, i4TestValFsMIOspfv3ABRType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3NssaAsbrDefRtTrans
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3NssaAsbrDefRtTrans (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3ContextId,
                                       INT4
                                       i4TestValFsMIOspfv3NssaAsbrDefRtTrans)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3NssaAsbrDefRtTrans (pu4ErrorCode,
                                              i4TestValFsMIOspfv3NssaAsbrDefRtTrans);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3DefaultPassiveInterface
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3DefaultPassiveInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3DefaultPassiveInterface (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIStdOspfv3ContextId,
                                            INT4
                                            i4TestValFsMIOspfv3DefaultPassiveInterface)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3DefaultPassiveInterface (pu4ErrorCode,
                                                   i4TestValFsMIOspfv3DefaultPassiveInterface);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3SpfDelay
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3SpfDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3SpfDelay (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfv3ContextId,
                             INT4 i4TestValFsMIOspfv3SpfDelay)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3SpfDelay (pu4ErrorCode, i4TestValFsMIOspfv3SpfDelay);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3SpfHoldTime
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3SpfHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3SpfHoldTime (UINT4 *pu4ErrorCode,
                                INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4TestValFsMIOspfv3SpfHoldTime)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3SpfHoldTime (pu4ErrorCode,
                                       i4TestValFsMIOspfv3SpfHoldTime);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RTStaggeringInterval
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RTStaggeringInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RTStaggeringInterval (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         i4TestValFsMIOspfv3RTStaggeringInterval)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RTStaggeringInterval (pu4ErrorCode,
                                                i4TestValFsMIOspfv3RTStaggeringInterval);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RestartStrictLsaChecking
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RestartStrictLsaChecking
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RestartStrictLsaChecking (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3ContextId,
                                             INT4
                                             i4TestValFsMIOspfv3RestartStrictLsaChecking)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RestartStrictLsaChecking (pu4ErrorCode,
                                                    i4TestValFsMIOspfv3RestartStrictLsaChecking);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3HelperSupport
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3HelperSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3HelperSupport (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMIOspfv3HelperSupport)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3HelperSupport (pu4ErrorCode,
                                         pTestValFsMIOspfv3HelperSupport);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3HelperGraceTimeLimit
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3HelperGraceTimeLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3HelperGraceTimeLimit (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3ContextId,
                                         INT4
                                         i4TestValFsMIOspfv3HelperGraceTimeLimit)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3HelperGraceTimeLimit (pu4ErrorCode,
                                                i4TestValFsMIOspfv3HelperGraceTimeLimit);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RestartAckState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RestartAckState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RestartAckState (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4TestValFsMIOspfv3RestartAckState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RestartAckState (pu4ErrorCode,
                                           i4TestValFsMIOspfv3RestartAckState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3GraceLsaRetransmitCount
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3GraceLsaRetransmitCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3GraceLsaRetransmitCount (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIStdOspfv3ContextId,
                                            INT4
                                            i4TestValFsMIOspfv3GraceLsaRetransmitCount)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3GraceLsaRetransmitCount (pu4ErrorCode,
                                                   i4TestValFsMIOspfv3GraceLsaRetransmitCount);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RestartReason
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RestartReason
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RestartReason (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4TestValFsMIOspfv3RestartReason)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RestartReason (pu4ErrorCode,
                                         i4TestValFsMIOspfv3RestartReason);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ExtTraceLevel
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3ExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ExtTraceLevel (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4TestValFsMIOspfv3ExtTraceLevel)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3ExtTraceLevel (pu4ErrorCode,
                                         i4TestValFsMIOspfv3ExtTraceLevel);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3SetTraps
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3SetTraps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3SetTraps (UINT4 *pu4ErrorCode, INT4 i4FsMIStdOspfv3ContextId,
                             INT4 i4TestValFsMIOspfv3SetTraps)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3SetTraps (pu4ErrorCode, i4TestValFsMIOspfv3SetTraps);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3BfdStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                testValFsMIOspfv3BfdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3BfdStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4TestValFsMIOspfv3BfdStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3BfdStatus (pu4ErrorCode,
                                     i4TestValFsMIOspfv3BfdStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3BfdAllIfState
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                testValFsMIOspfv3BfdAllIfState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3BfdAllIfState (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4TestValFsMIOspfv3BfdAllIfState)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3BfdAllIfState (pu4ErrorCode,
                                         i4TestValFsMIOspfv3BfdAllIfState);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RouterIdPermanence
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object
                testValFsMIOspfv3RouterIdPermanence
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RouterIdPermanence (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3ContextId,
                                       INT4
                                       i4TestValFsMIOspfv3RouterIdPermanence)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RouterIdPermanence (pu4ErrorCode,
                                              i4TestValFsMIOspfv3RouterIdPermanence);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3IfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3IfTable
 Input       :  The Indices
                FsMIOspfv3IfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3IfTable (INT4 i4FsMIOspfv3IfIndex)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex, &u4CxtId) ==
        OSIX_FAILURE)
    {
        return i1RetVal;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfv3IfTable (i4FsMIOspfv3IfIndex);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                testValFsMIOspfv3IfLinkLSASuppression
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfLinkLSASuppression (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIOspfv3IfIndex,
                                         INT4
                                         i4TestValFsMIOspfv3IfLinkLSASuppression)
{
    return (nmhTestv2FutOspfv3IfLinkLSASuppression
            (pu4ErrorCode, i4FsMIOspfv3IfIndex,
             i4TestValFsMIOspfv3IfLinkLSASuppression));
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfBfdState
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object
                testValFsMIOspfv3IfBfdState
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfBfdState (UINT4 *pu4ErrorCode,
                               INT4 i4FsMIOspfv3IfIndex,
                               INT4 i4TestValFsMIOspfv3IfBfdState)
{
    return (nmhTestv2FutOspfv3IfBfdState (pu4ErrorCode,
                                          i4FsMIOspfv3IfIndex,
                                          i4TestValFsMIOspfv3IfBfdState));

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfPassive
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                testValFsMIOspfv3IfPassive
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfPassive (UINT4 *pu4ErrorCode, INT4 i4FsMIOspfv3IfIndex,
                              INT4 i4TestValFsMIOspfv3IfPassive)
{
    return (nmhTestv2FutOspfv3IfPassive
            (pu4ErrorCode, i4FsMIOspfv3IfIndex, i4TestValFsMIOspfv3IfPassive));
}

/* LOW LEVEL Routines for Table : FsMIOspfv3RoutingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3RoutingTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RouteDestType
                FsMIOspfv3RouteDest
                FsMIOspfv3RoutePfxLength
                FsMIOspfv3RouteNextHopType
                FsMIOspfv3RouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3RoutingTable (INT4 i4FsMIStdOspfv3ContextId,
                                                INT4 i4FsMIOspfv3RouteDestType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfv3RouteDest,
                                                UINT4
                                                u4FsMIOspfv3RoutePfxLength,
                                                INT4
                                                i4FsMIOspfv3RouteNextHopType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfv3RouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3RoutingTable
        (i4FsMIOspfv3RouteDestType, pFsMIOspfv3RouteDest,
         u4FsMIOspfv3RoutePfxLength, i4FsMIOspfv3RouteNextHopType,
         pFsMIOspfv3RouteNextHop);

    V3UtilReSetContext ();

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3AsExternalAggregationTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable (INT4
                                                              i4FsMIStdOspfv3ContextId,
                                                              INT4
                                                              i4FsMIOspfv3AsExternalAggregationNetType,
                                                              tSNMP_OCTET_STRING_TYPE
                                                              *
                                                              pFsMIOspfv3AsExternalAggregationNet,
                                                              UINT4
                                                              u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                              UINT4
                                                              u4FsMIOspfv3AsExternalAggregationAreaId)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3AsExternalAggregationTable
        (i4FsMIOspfv3AsExternalAggregationNetType,
         pFsMIOspfv3AsExternalAggregationNet,
         u4FsMIOspfv3AsExternalAggregationPfxLength,
         u4FsMIOspfv3AsExternalAggregationAreaId);

    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                testValFsMIOspfv3AsExternalAggregationEffect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3AsExternalAggregationEffect (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIStdOspfv3ContextId,
                                                INT4
                                                i4FsMIOspfv3AsExternalAggregationNetType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfv3AsExternalAggregationNet,
                                                UINT4
                                                u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                UINT4
                                                u4FsMIOspfv3AsExternalAggregationAreaId,
                                                INT4
                                                i4TestValFsMIOspfv3AsExternalAggregationEffect)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3AsExternalAggregationEffect (pu4ErrorCode,
                                                       i4FsMIOspfv3AsExternalAggregationNetType,
                                                       pFsMIOspfv3AsExternalAggregationNet,
                                                       u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                       u4FsMIOspfv3AsExternalAggregationAreaId,
                                                       i4TestValFsMIOspfv3AsExternalAggregationEffect);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                testValFsMIOspfv3AsExternalAggregationTranslation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3AsExternalAggregationTranslation (UINT4 *pu4ErrorCode,
                                                     INT4
                                                     i4FsMIStdOspfv3ContextId,
                                                     INT4
                                                     i4FsMIOspfv3AsExternalAggregationNetType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFsMIOspfv3AsExternalAggregationNet,
                                                     UINT4
                                                     u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                     UINT4
                                                     u4FsMIOspfv3AsExternalAggregationAreaId,
                                                     INT4
                                                     i4TestValFsMIOspfv3AsExternalAggregationTranslation)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3AsExternalAggregationTranslation (pu4ErrorCode,
                                                            i4FsMIOspfv3AsExternalAggregationNetType,
                                                            pFsMIOspfv3AsExternalAggregationNet,
                                                            u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                            u4FsMIOspfv3AsExternalAggregationAreaId,
                                                            i4TestValFsMIOspfv3AsExternalAggregationTranslation);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3AsExternalAggregationNetType
                FsMIOspfv3AsExternalAggregationNet
                FsMIOspfv3AsExternalAggregationPfxLength
                FsMIOspfv3AsExternalAggregationAreaId

                The Object 
                testValFsMIOspfv3AsExternalAggregationStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3AsExternalAggregationStatus (UINT4 *pu4ErrorCode,
                                                INT4 i4FsMIStdOspfv3ContextId,
                                                INT4
                                                i4FsMIOspfv3AsExternalAggregationNetType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfv3AsExternalAggregationNet,
                                                UINT4
                                                u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                UINT4
                                                u4FsMIOspfv3AsExternalAggregationAreaId,
                                                INT4
                                                i4TestValFsMIOspfv3AsExternalAggregationStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3AsExternalAggregationStatus (pu4ErrorCode,
                                                       i4FsMIOspfv3AsExternalAggregationNetType,
                                                       pFsMIOspfv3AsExternalAggregationNet,
                                                       u4FsMIOspfv3AsExternalAggregationPfxLength,
                                                       u4FsMIOspfv3AsExternalAggregationAreaId,
                                                       i4TestValFsMIOspfv3AsExternalAggregationStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3BRRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3BRRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3BRRouteDest
                FsMIOspfv3BRRouteNextHopType
                FsMIOspfv3BRRouteNextHop
                FsMIOspfv3BRRouteDestType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3BRRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                                UINT4 u4FsMIOspfv3BRRouteDest,
                                                INT4
                                                i4FsMIOspfv3BRRouteNextHopType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFsMIOspfv3BRRouteNextHop,
                                                INT4
                                                i4FsMIOspfv3BRRouteDestType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3BRRouteTable (u4FsMIOspfv3BRRouteDest,
                                                       i4FsMIOspfv3BRRouteNextHopType,
                                                       pFsMIOspfv3BRRouteNextHop,
                                                       i4FsMIOspfv3BRRouteDestType);
    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable (INT4
                                                       i4FsMIStdOspfv3ContextId,
                                                       INT4
                                                       i4FsMIOspfv3RedistRouteDestType,
                                                       tSNMP_OCTET_STRING_TYPE *
                                                       pFsMIOspfv3RedistRouteDest,
                                                       UINT4
                                                       u4FsMIOspfv3RedistRoutePfxLength)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3RedistRouteCfgTable
        (i4FsMIOspfv3RedistRouteDestType, pFsMIOspfv3RedistRouteDest,
         u4FsMIOspfv3RedistRoutePfxLength);
    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RedistRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                testValFsMIOspfv3RedistRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RedistRouteMetric (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4FsMIOspfv3RedistRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIOspfv3RedistRouteDest,
                                      UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                      INT4 i4TestValFsMIOspfv3RedistRouteMetric)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RedistRouteMetric (pu4ErrorCode,
                                             i4FsMIOspfv3RedistRouteDestType,
                                             pFsMIOspfv3RedistRouteDest,
                                             u4FsMIOspfv3RedistRoutePfxLength,
                                             i4TestValFsMIOspfv3RedistRouteMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RedistRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                testValFsMIOspfv3RedistRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RedistRouteMetricType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          INT4 i4FsMIOspfv3RedistRouteDestType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMIOspfv3RedistRouteDest,
                                          UINT4
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          INT4
                                          i4TestValFsMIOspfv3RedistRouteMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RedistRouteMetricType (pu4ErrorCode,
                                                 i4FsMIOspfv3RedistRouteDestType,
                                                 pFsMIOspfv3RedistRouteDest,
                                                 u4FsMIOspfv3RedistRoutePfxLength,
                                                 i4TestValFsMIOspfv3RedistRouteMetricType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RedistRouteTagType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                testValFsMIOspfv3RedistRouteTagType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RedistRouteTagType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3RedistRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3RedistRouteDest,
                                       UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                       INT4
                                       i4TestValFsMIOspfv3RedistRouteTagType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RedistRouteTagType (pu4ErrorCode,
                                              i4FsMIOspfv3RedistRouteDestType,
                                              pFsMIOspfv3RedistRouteDest,
                                              u4FsMIOspfv3RedistRoutePfxLength,
                                              i4TestValFsMIOspfv3RedistRouteTagType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RedistRouteTag
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                testValFsMIOspfv3RedistRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RedistRouteTag (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3RedistRouteDest,
                                   UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                   INT4 i4TestValFsMIOspfv3RedistRouteTag)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RedistRouteTag (pu4ErrorCode,
                                          i4FsMIOspfv3RedistRouteDestType,
                                          pFsMIOspfv3RedistRouteDest,
                                          u4FsMIOspfv3RedistRoutePfxLength,
                                          i4TestValFsMIOspfv3RedistRouteTag);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RedistRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RedistRouteDestType
                FsMIOspfv3RedistRouteDest
                FsMIOspfv3RedistRoutePfxLength

                The Object 
                testValFsMIOspfv3RedistRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RedistRouteStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIStdOspfv3ContextId,
                                      INT4 i4FsMIOspfv3RedistRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsMIOspfv3RedistRouteDest,
                                      UINT4 u4FsMIOspfv3RedistRoutePfxLength,
                                      INT4 i4TestValFsMIOspfv3RedistRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RedistRouteStatus (pu4ErrorCode,
                                             i4FsMIOspfv3RedistRouteDestType,
                                             pFsMIOspfv3RedistRouteDest,
                                             u4FsMIOspfv3RedistRoutePfxLength,
                                             i4TestValFsMIOspfv3RedistRouteStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3RRDRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable (INT4 i4FsMIStdOspfv3ContextId)
{
    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RRDStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RRDStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RRDStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4TestValFsMIOspfv3RRDStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RRDStatus (pu4ErrorCode,
                                     i4TestValFsMIOspfv3RRDStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RRDSrcProtoMask
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RRDSrcProtoMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RRDSrcProtoMask (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4TestValFsMIOspfv3RRDSrcProtoMask)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RRDSrcProtoMask (pu4ErrorCode,
                                           i4TestValFsMIOspfv3RRDSrcProtoMask);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RRDRouteMapName
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3RRDRouteMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RRDRouteMapName (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValFsMIOspfv3RRDRouteMapName)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RRDRouteMapName (pu4ErrorCode,
                                           pTestValFsMIOspfv3RRDRouteMapName);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3RRDMetricTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3RRDMetricTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3RRDMetricTable (INT4 i4FsMIStdOspfv3ContextId,
                                                  INT4
                                                  i4FsMIOspfv3RRDProtocolId)
{
    if (nmhValidateIndexInstanceFsMIStdOspfv3Table (i4FsMIStdOspfv3ContextId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMIOspfv3RRDProtocolId <= 0) || (i4FsMIOspfv3RRDProtocolId >
                                             OSPFV3_MAX_PROTO_REDISTRUTE_SIZE))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RRDMetricValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                testValFsMIOspfv3RRDMetricValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RRDMetricValue (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3RRDProtocolId,
                                   INT4 i4TestValFsMIOspfv3RRDMetricValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RRDMetricValue
        (pu4ErrorCode, i4FsMIOspfv3RRDProtocolId,
         i4TestValFsMIOspfv3RRDMetricValue);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3RRDMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId

                The Object 
                testValFsMIOspfv3RRDMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3RRDMetricType (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  INT4 i4FsMIOspfv3RRDProtocolId,
                                  INT4 i4TestValFsMIOspfv3RRDMetricType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3RRDMetricType
        (pu4ErrorCode, i4FsMIOspfv3RRDProtocolId,
         i4TestValFsMIOspfv3RRDMetricType);
    V3UtilReSetContext ();

    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3RRDMetricTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3RRDProtocolId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3RRDMetricTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3DistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3DistInOutRouteMapTable (INT4
                                                          i4FsMIStdOspfv3ContextId,
                                                          tSNMP_OCTET_STRING_TYPE
                                                          *
                                                          pFsMIOspfv3DistInOutRouteMapName,
                                                          INT4
                                                          i4FsMIOspfv3DistInOutRouteMapType)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3DistInOutRouteMapTable
        (pFsMIOspfv3DistInOutRouteMapName, i4FsMIOspfv3DistInOutRouteMapType);
    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                testValFsMIOspfv3DistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3DistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMIOspfv3DistInOutRouteMapName,
                                           INT4
                                           i4FsMIOspfv3DistInOutRouteMapType,
                                           INT4
                                           i4TestValFsMIOspfv3DistInOutRouteMapValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3DistInOutRouteMapValue (pu4ErrorCode,
                                                  pFsMIOspfv3DistInOutRouteMapName,
                                                  i4FsMIOspfv3DistInOutRouteMapType,
                                                  i4TestValFsMIOspfv3DistInOutRouteMapValue);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3DistInOutRouteMapName
                FsMIOspfv3DistInOutRouteMapType

                The Object 
                testValFsMIOspfv3DistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                               INT4 i4FsMIStdOspfv3ContextId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pFsMIOspfv3DistInOutRouteMapName,
                                               INT4
                                               i4FsMIOspfv3DistInOutRouteMapType,
                                               INT4
                                               i4TestValFsMIOspfv3DistInOutRouteMapRowStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3DistInOutRouteMapRowStatus (pu4ErrorCode,
                                                      pFsMIOspfv3DistInOutRouteMapName,
                                                      i4FsMIOspfv3DistInOutRouteMapType,
                                                      i4TestValFsMIOspfv3DistInOutRouteMapRowStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3PreferenceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3PreferenceTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3PreferenceTable (INT4
                                                   i4FsMIStdOspfv3ContextId)
{
    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3NeighborBfdTable
 Input       :  The Indices
                FsMIStdOspfv3NbrIfIndex
                FsMIStdOspfv3NbrRtrId
 Output       :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3NeighborBfdTable (INT4
                                                    i4FsMIStdOspfv3NbrIfIndex,
                                                    UINT4
                                                    u4FsMIStdOspfv3NbrRtrId)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIStdOspfv3NbrIfIndex, &u4CxtId)
        == OSIX_FAILURE)
    {
        return i1RetVal;
    }
    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilOspfIsValidCxtId (i4V3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3NeighborBfdTable
        (i4FsMIStdOspfv3NbrIfIndex, u4FsMIStdOspfv3NbrRtrId);

    V3UtilReSetContext ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3PreferenceValue
 Input       :  The Indices
                FsMIStdOspfv3ContextId

                The Object 
                testValFsMIOspfv3PreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3PreferenceValue (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4TestValFsMIOspfv3PreferenceValue)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspf3PreferenceValue (pu4ErrorCode,
                                          i4TestValFsMIOspfv3PreferenceValue);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfCryptoAuthType
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                testValFsMIOspfv3IfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfCryptoAuthType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfv3IfIndex,
                                     INT4 i4TestValFsMIOspfv3IfCryptoAuthType)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3IfCryptoAuthType (pu4ErrorCode, i4FsMIOspfv3IfIndex,
                                            i4TestValFsMIOspfv3IfCryptoAuthType);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FsMIOspfv3IfIndex

                The Object 
                testValFsMIOspfv3IfCryptoAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfCryptoAuthMode (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMIOspfv3IfIndex,
                                     INT4 i4TestValFsMIOspfv3IfCryptoAuthMode)
{
    UINT4               u4CxtId = 0;
    INT4                i4V3ContextId = 0;
    INT1                i1Return = SNMP_FAILURE;

    if (V3OspfGetCxtIdFromCfaIndex ((UINT4) i4FsMIOspfv3IfIndex,
                                    &u4CxtId) == OSIX_FAILURE)
    {
        return i1Return;
    }

    i4V3ContextId = (INT4) u4CxtId;

    if (V3UtilSetContext ((UINT4) i4V3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3IfCryptoAuthMode (pu4ErrorCode, i4FsMIOspfv3IfIndex,
                                            i4TestValFsMIOspfv3IfCryptoAuthMode);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIOspfv3VirtIfCryptoAuthType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfCryptoAuthType (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                         UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                         INT4
                                         i4TestValFsMIOspfv3VirtIfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3VirtIfCryptoAuthType (pu4ErrorCode,
                                                u4FsMIStdOspfv3VirtIfAreaId,
                                                u4FsMIStdOspfv3VirtIfNeighbor,
                                                i4TestValFsMIOspfv3VirtIfCryptoAuthType);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor

                The Object 
                testValFsMIOspfv3VirtIfCryptoAuthMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfCryptoAuthMode (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3ContextId,
                                         UINT4 u4FsMIStdOspfv3VirtIfAreaId,
                                         UINT4 u4FsMIStdOspfv3VirtIfNeighbor,
                                         INT4
                                         i4TestValFsMIOspfv3VirtIfCryptoAuthMode)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3VirtIfCryptoAuthMode (pu4ErrorCode,
                                                u4FsMIStdOspfv3VirtIfAreaId,
                                                u4FsMIStdOspfv3VirtIfNeighbor,
                                                i4TestValFsMIOspfv3VirtIfCryptoAuthMode);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3VirtIfCryptoAuthTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3VirtIfAreaId
                FsMIStdOspfv3VirtIfNeighbor
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3VirtIfCryptoAuthTable (INT4
                                                         i4FsMIStdOspfv3ContextId,
                                                         UINT4
                                                         u4FsMIStdOspfv3VirtIfAreaId,
                                                         UINT4
                                                         u4FsMIStdOspfv3VirtIfNeighbor)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = (INT4) OSPFV3_MIN (OSPFV3_MAX_CONTEXTS_LIMIT,
                                  FsOSPFV3SizingParams
                                  [MAX_OSPFV3_CONTEXTS_SIZING_ID].
                                  u4PreAllocatedUnits);

    if ((i4FsMIStdOspfv3ContextId < OSPFV3_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfv3ContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (V3OspfVcmIsVcExist ((UINT4) i4FsMIStdOspfv3ContextId) == OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilOspfIsValidCxtId ((INT4) i4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfv3VirtIfCryptoAuthTable
        (u4FsMIStdOspfv3VirtIfAreaId, u4FsMIStdOspfv3VirtIfNeighbor);

    V3UtilReSetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3IfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3IfAuthTable (INT4 i4FsMIStdOspfv3ContextId,
                                               INT4 i4FsMIOspfv3IfAuthIfIndex,
                                               INT4 i4FsMIOspfv3IfAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = (INT4) OSPFV3_MIN (OSPFV3_MAX_CONTEXTS_LIMIT,
                                  FsOSPFV3SizingParams
                                  [MAX_OSPFV3_CONTEXTS_SIZING_ID].
                                  u4PreAllocatedUnits);

    if ((i4FsMIStdOspfv3ContextId < OSPFV3_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfv3ContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }
    if (V3OspfVcmIsVcExist ((UINT4) i4FsMIStdOspfv3ContextId) == OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilOspfIsValidCxtId ((INT4) i4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfv3IfAuthTable
        (i4FsMIOspfv3IfAuthIfIndex, i4FsMIOspfv3IfAuthKeyId);

    V3UtilReSetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKey (UINT4 *pu4ErrorCode,
                              INT4 i4FsMIStdOspfv3ContextId,
                              INT4 i4FsMIOspfv3IfAuthIfIndex,
                              INT4 i4FsMIOspfv3IfAuthKeyId,
                              tSNMP_OCTET_STRING_TYPE
                              * pTestValFsMIOspfv3IfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3IfAuthKey (pu4ErrorCode, i4FsMIOspfv3IfAuthIfIndex,
                                     i4FsMIOspfv3IfAuthKeyId,
                                     pTestValFsMIOspfv3IfAuthKey);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                         INT4 i4FsMIStdOspfv3ContextId,
                                         INT4 i4FsMIOspfv3IfAuthIfIndex,
                                         INT4 i4FsMIOspfv3IfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFsMIOspfv3IfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3IfAuthKeyStartAccept (pu4ErrorCode,
                                                       i4FsMIOspfv3IfAuthIfIndex,
                                                       i4FsMIOspfv3IfAuthKeyId,
                                                       pTestValFsMIOspfv3IfAuthKeyStartAccept);
    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                           INT4 i4FsMIStdOspfv3ContextId,
                                           INT4 i4FsMIOspfv3IfAuthIfIndex,
                                           INT4 i4FsMIOspfv3IfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pTestValFsMIOspfv3IfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3IfAuthKeyStartGenerate (pu4ErrorCode,
                                                         i4FsMIOspfv3IfAuthIfIndex,
                                                         i4FsMIOspfv3IfAuthKeyId,
                                                         pTestValFsMIOspfv3IfAuthKeyStartGenerate);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          INT4 i4FsMIOspfv3IfAuthIfIndex,
                                          INT4 i4FsMIOspfv3IfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pTestValFsMIOspfv3IfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3IfAuthKeyStopGenerate (pu4ErrorCode,
                                                        i4FsMIOspfv3IfAuthIfIndex,
                                                        i4FsMIOspfv3IfAuthKeyId,
                                                        pTestValFsMIOspfv3IfAuthKeyStopGenerate);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfv3ContextId,
                                        INT4 i4FsMIOspfv3IfAuthIfIndex,
                                        INT4 i4FsMIOspfv3IfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pTestValFsMIOspfv3IfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3IfAuthKeyStopAccept (pu4ErrorCode,
                                                      i4FsMIOspfv3IfAuthIfIndex,
                                                      i4FsMIOspfv3IfAuthKeyId,
                                                      pTestValFsMIOspfv3IfAuthKeyStopAccept);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3IfAuthIfIndex
                FsMIOspfv3IfAuthKeyId

                The Object 
                testValFsMIOspfv3IfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3IfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4FsMIOspfv3IfAuthIfIndex,
                                    INT4 i4FsMIOspfv3IfAuthKeyId,
                                    INT4 i4TestValFsMIOspfv3IfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3IfAuthKeyStatus (pu4ErrorCode,
                                                  i4FsMIOspfv3IfAuthIfIndex,
                                                  i4FsMIOspfv3IfAuthKeyId,
                                                  i4TestValFsMIOspfv3IfAuthKeyStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable (INT4
                                                   i4FsMIStdOspfv3ContextId,
                                                   UINT4
                                                   u4FsMIOspfv3VirtIfAuthAreaId,
                                                   UINT4
                                                   u4FsMIOspfv3VirtIfAuthNeighbor,
                                                   INT4
                                                   i4FsMIOspfv3VirtIfAuthKeyId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4MaxCxt = 0;

    i4MaxCxt = (INT4) OSPFV3_MIN (OSPFV3_MAX_CONTEXTS_LIMIT,
                                  FsOSPFV3SizingParams
                                  [MAX_OSPFV3_CONTEXTS_SIZING_ID].
                                  u4PreAllocatedUnits);

    if ((i4FsMIStdOspfv3ContextId < OSPFV3_DEFAULT_CXT_ID) ||
        (i4FsMIStdOspfv3ContextId >= i4MaxCxt))
    {
        return SNMP_FAILURE;
    }

    if (V3OspfVcmIsVcExist ((UINT4) i4FsMIStdOspfv3ContextId) == OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilOspfIsValidCxtId ((INT4) i4FsMIStdOspfv3ContextId) ==
        OSPFV3_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal = nmhValidateIndexInstanceFutOspfv3VirtIfAuthTable
        (u4FsMIOspfv3VirtIfAuthAreaId, u4FsMIOspfv3VirtIfAuthNeighbor,
         i4FsMIOspfv3VirtIfAuthKeyId);

    V3UtilReSetContext ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKey
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKey (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMIStdOspfv3ContextId,
                                  UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                  UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                  INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                  tSNMP_OCTET_STRING_TYPE
                                  * pTestValFsMIOspfv3VirtIfAuthKey)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKey (pu4ErrorCode,
                                                u4FsMIOspfv3VirtIfAuthAreaId,
                                                u4FsMIOspfv3VirtIfAuthNeighbor,
                                                i4FsMIOspfv3VirtIfAuthKeyId,
                                                pTestValFsMIOspfv3VirtIfAuthKey);

    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStartAccept (UINT4 *pu4ErrorCode,
                                             INT4 i4FsMIStdOspfv3ContextId,
                                             UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                             UINT4
                                             u4FsMIOspfv3VirtIfAuthNeighbor,
                                             INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsMIOspfv3VirtIfAuthKeyStartAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKeyStartAccept (pu4ErrorCode,
                                                           u4FsMIOspfv3VirtIfAuthAreaId,
                                                           u4FsMIOspfv3VirtIfAuthNeighbor,
                                                           i4FsMIOspfv3VirtIfAuthKeyId,
                                                           pTestValFsMIOspfv3VirtIfAuthKeyStartAccept);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStartGenerate (UINT4 *pu4ErrorCode,
                                               INT4 i4FsMIStdOspfv3ContextId,
                                               UINT4
                                               u4FsMIOspfv3VirtIfAuthAreaId,
                                               UINT4
                                               u4FsMIOspfv3VirtIfAuthNeighbor,
                                               INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValFsMIOspfv3VirtIfAuthKeyStartGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKeyStartGenerate (pu4ErrorCode,
                                                             u4FsMIOspfv3VirtIfAuthAreaId,
                                                             u4FsMIOspfv3VirtIfAuthNeighbor,
                                                             i4FsMIOspfv3VirtIfAuthKeyId,
                                                             pTestValFsMIOspfv3VirtIfAuthKeyStartGenerate);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStopGenerate (UINT4 *pu4ErrorCode,
                                              INT4 i4FsMIStdOspfv3ContextId,
                                              UINT4
                                              u4FsMIOspfv3VirtIfAuthAreaId,
                                              UINT4
                                              u4FsMIOspfv3VirtIfAuthNeighbor,
                                              INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pTestValFsMIOspfv3VirtIfAuthKeyStopGenerate)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKeyStopGenerate (pu4ErrorCode,
                                                            u4FsMIOspfv3VirtIfAuthAreaId,
                                                            u4FsMIOspfv3VirtIfAuthNeighbor,
                                                            i4FsMIOspfv3VirtIfAuthKeyId,
                                                            pTestValFsMIOspfv3VirtIfAuthKeyStopGenerate);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStopAccept (UINT4 *pu4ErrorCode,
                                            INT4 i4FsMIStdOspfv3ContextId,
                                            UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                            UINT4
                                            u4FsMIOspfv3VirtIfAuthNeighbor,
                                            INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pTestValFsMIOspfv3VirtIfAuthKeyStopAccept)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKeyStopAccept (pu4ErrorCode,
                                                          u4FsMIOspfv3VirtIfAuthAreaId,
                                                          u4FsMIOspfv3VirtIfAuthNeighbor,
                                                          i4FsMIOspfv3VirtIfAuthKeyId,
                                                          pTestValFsMIOspfv3VirtIfAuthKeyStopAccept);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3VirtIfAuthAreaId
                FsMIOspfv3VirtIfAuthNeighbor
                FsMIOspfv3VirtIfAuthKeyId

                The Object 
                testValFsMIOspfv3VirtIfAuthKeyStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMIStdOspfv3ContextId,
                                        UINT4 u4FsMIOspfv3VirtIfAuthAreaId,
                                        UINT4 u4FsMIOspfv3VirtIfAuthNeighbor,
                                        INT4 i4FsMIOspfv3VirtIfAuthKeyId,
                                        INT4
                                        i4TestValFsMIOspfv3VirtIfAuthKeyStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return = nmhTestv2FutOspfv3VirtIfAuthKeyStatus (pu4ErrorCode,
                                                      u4FsMIOspfv3VirtIfAuthAreaId,
                                                      u4FsMIOspfv3VirtIfAuthNeighbor,
                                                      i4FsMIOspfv3VirtIfAuthKeyId,
                                                      i4TestValFsMIOspfv3VirtIfAuthKeyStatus);

    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIStdOspfv3AreaDfInfOriginate
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIStdOspfv3AreaId

                The Object
                testValFsMIStdOspfv3AreaDfInfOriginate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIStdOspfv3AreaDfInfOriginate (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMIStdOspfv3ContextId,
                                          UINT4 u4FsMIStdOspfv3AreaId,
                                          INT4
                                          i4TestValFsMIStdOspfv3AreaDfInfOriginate)
{

    INT1                i1Return = SNMP_FAILURE;
    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2Ospfv3AreaDfInfOriginate (pu4ErrorCode, u4FsMIStdOspfv3AreaId,
                                           i4TestValFsMIStdOspfv3AreaDfInfOriginate);
    V3UtilReSetContext ();
    return i1Return;

}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ClearProcess
 Input       :  The Indices

                The Object
                testValFsMIOspfv3ClearProcess
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ClearProcess (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMIOspfv3ClearProcess)
{

    UNUSED_PARAM (i4TestValFsMIOspfv3ClearProcess);
    UNUSED_PARAM (pu4ErrorCode);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3ClearProcess
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3ClearProcess (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
