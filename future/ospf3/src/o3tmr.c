
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3tmr.c,v 1.17 2018/01/02 09:37:09 siva Exp $
 *
 * Description:This file contains procedures related to starting
 *             and resetting of timers and also actions routines
 *             for handling timer expiry.
 *
 *******************************************************************/

#include "o3inc.h"

/* Proto types of the functions private to this file only */

PRIVATE VOID V3TmrHelloTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrPollTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrWaitTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrInactivityTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrDdInitRxmtTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrDdRxmtTimer PROTO ((VOID *pAgr));
PRIVATE VOID V3TmrLsaNormalAgingTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrMinLsaIntervalTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrLsaRxmtTimer PROTO ((VOID *pAgr));
PRIVATE VOID V3TmrRunRtTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrExitOverflowTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrNssaFsmStbltyTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrNbrProbeTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrVrfSpfTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrRestartGraceTimer PROTO ((VOID *pArg));
PRIVATE VOID V3TmrHelperGraceTimer PROTO ((VOID *pArg));

/* 
 * The following data structure contains function pointers for timer handling
 * routines and the offsets used to identify the data structure containing the 
 * timer block. The timer id is used to index into the following array.
 */

/*****************************************************************************/
/* Function     : V3TmrInitTimerDesc                                         */
/*                                                                           */
/* Description  : Initializes the timer descriptor structure.                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
V3TmrInitTimerDesc (VOID)
{

    gV3OsRtr.aTimerDesc[OSPFV3_HELLO_TIMER].pTimerExpFunc = V3TmrHelloTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_HELLO_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, helloTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_POLL_TIMER].pTimerExpFunc = V3TmrPollTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_POLL_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, pollTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_WAIT_TIMER].pTimerExpFunc = V3TmrWaitTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_WAIT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, waitTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_INACTIVITY_TIMER].pTimerExpFunc =
        V3TmrInactivityTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_INACTIVITY_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, inactivityTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_DD_INIT_RXMT_TIMER].pTimerExpFunc =
        V3TmrDdInitRxmtTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_DD_INIT_RXMT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, dbSummary.ddTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_DD_RXMT_TIMER].pTimerExpFunc = V3TmrDdRxmtTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_DD_RXMT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, dbSummary.ddTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER].pTimerExpFunc =
        V3DdpClearSummary;
    gV3OsRtr.aTimerDesc[OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, dbSummary.ddTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_LSA_REQ_RXMT_TIMER].pTimerExpFunc =
        V3LrqRxmtLsaReq;
    gV3OsRtr.aTimerDesc[OSPFV3_LSA_REQ_RXMT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, lsaReqDesc.lsaReqRxmtTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_LSA_NORMAL_AGING_TIMER].pTimerExpFunc =
        V3TmrLsaNormalAgingTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_LSA_NORMAL_AGING_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsLsaInfo, lsaAgingTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_MIN_LSA_INTERVAL_TIMER].pTimerExpFunc =
        V3TmrMinLsaIntervalTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_MIN_LSA_INTERVAL_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsLsaDesc, minLsaIntervalTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_LSA_RXMT_TIMER].pTimerExpFunc =
        V3TmrLsaRxmtTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_LSA_RXMT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, lsaRxmtDesc.lsaRxmtTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_DEL_ACK_TIMER].pTimerExpFunc =
        V3LakSendDelayedAck;
    gV3OsRtr.aTimerDesc[OSPFV3_DEL_ACK_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, delLsAck.delAckTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_RUN_RT_TIMER].pTimerExpFunc = V3TmrRunRtTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_RUN_RT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OspfCxt, runRtTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_EXIT_OVERFLOW_TIMER].pTimerExpFunc =
        V3TmrExitOverflowTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_EXIT_OVERFLOW_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OspfCxt, exitOverflowTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_NSSA_STBLTY_INTERVAL_TIMER].pTimerExpFunc =
        V3TmrNssaFsmStbltyTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_NSSA_STBLTY_INTERVAL_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsArea, nssaStbltyIntrvlTmr));

    gV3OsRtr.aTimerDesc[OSPFV3_NBR_PROBE_INTERVAL_TIMER].pTimerExpFunc =
        V3TmrNbrProbeTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_NBR_PROBE_INTERVAL_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, nbrProbeTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_VRF_SPF_TIMER].pTimerExpFunc = V3TmrVrfSpfTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_VRF_SPF_TIMER].i2Offset = -1;
    gV3OsRtr.aTimerDesc[OSPFV3_RESTART_GRACE_TIMER].pTimerExpFunc =
        V3TmrRestartGraceTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_RESTART_GRACE_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OspfCxt, graceTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_HELPER_GRACE_TIMER].pTimerExpFunc =
        V3TmrHelperGraceTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_HELPER_GRACE_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsNeighbor, helperGraceTimer));

    gV3OsRtr.aTimerDesc[OSPFV3_MULTIF_ACTIVE_DETECT_TIMER].pTimerExpFunc =
        V3TmrMultIfActiveDetectTimer;
    gV3OsRtr.aTimerDesc[OSPFV3_MULTIF_ACTIVE_DETECT_TIMER].i2Offset =
        (INT2) (OSPFV3_OFFSET (tV3OsInterface, MultIfActiveDetectTimer));

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC | INIT_SHUT_TRC, "TMR Desc Initialized\n");

}

/*****************************************************************************/
/* Function     : V3TmrSetTimer                                              */
/*                                                                           */
/* Description  : Given timer id is set to time specified by i4NrTicks       */
/*                                                                           */
/* Input        : pTimer      : pointer to timer structure                   */
/*                u1TimerId   : the id ofthe timer to be started             */
/*                i4NrTicks   : the value to which the timer is to be set    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TMR_SUCCESS                                                */
/*                TMR_FAILURE                                                */
/*****************************************************************************/
PUBLIC INT4
V3TmrSetTimer (tV3OspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks)
{

    tTimerListId        timerList = gV3OsRtr.timerLstId;

    if (gu1O3HelloSwitchOverFlag == 0)
    {
        if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
        {
            /* If the node state is STANDBY, Then do not start  
             *  1. Hello Timer
             *  2. Poll Timer
             *  3. Inactivity Timer
             *  4. DD Init Retransmit timer
             *  5. DD Retransmit timer
             *  6. LRQ Retransmit timer
             *  7. LSA Retransmit timer
             *  8. Delayed Ack Timer
             *  9. DDP last packet life time timer
             * 10. Neighbour probe Interval timer 
             * 11. Helper Grace Timeer
             */

            switch (u1TimerId)
            {
                case OSPFV3_HELLO_TIMER:
                case OSPFV3_POLL_TIMER:
                case OSPFV3_WAIT_TIMER:
                case OSPFV3_INACTIVITY_TIMER:
                case OSPFV3_DD_INIT_RXMT_TIMER:
                case OSPFV3_DD_RXMT_TIMER:
                case OSPFV3_LSA_REQ_RXMT_TIMER:
                case OSPFV3_LSA_RXMT_TIMER:
                case OSPFV3_DEL_ACK_TIMER:
                case OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER:
                case OSPFV3_NBR_PROBE_INTERVAL_TIMER:
                case OSPFV3_RESTART_GRACE_TIMER:
                    return TMR_SUCCESS;
                default:
                    break;
            }
        }
    }

    pTimer->u1TimerId = u1TimerId;

    if (u1TimerId == OSPFV3_HELLO_TIMER)
    {
        timerList = gV3OsRtr.hellotimerLstId;
    }
    if (u1TimerId == OSPFV3_50MS_LSU_LAK_TIMER)
    {
        timerList = gV3OsRtr.g50msTimerLst;
    }
    if (u1TimerId == OSPFV3_HELLO_TIMER)
    {
        if (TmrStartTimer (timerList, &(pTimer->timerNode),
                           (UINT4) i4NrTicks) != TMR_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Timer Link Failure\n"));

            OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Tmr Link Failure\n");
            return ((INT4) TMR_FAILURE);
        }
    }

    if (!
        ((gu1O3HelloSwitchOverFlag == 1)
         && (u1TimerId == OSPFV3_INACTIVITY_TIMER)))
    {

        if (TmrStartTimer (timerList, &(pTimer->timerNode),
                           (UINT4) i4NrTicks) != TMR_SUCCESS)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Timer Link Failure\n"));

            OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Tmr Link Failure\n");
            return ((INT4) TMR_FAILURE);
        }
    }
    return TMR_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3TmrRestartTimer                                          */
/*                                                                           */
/* Description  : Restarts the timer with the specified number of ticks.     */
/*                                                                           */
/* Input        : pTimer      : pointer to timer structure                   */
/*                u1TimerId   : the id ofthe timer to be started             */
/*                i4NrTicks   : the value to which the timer is to be set    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3TmrRestartTimer (tV3OspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks)
{
    OSPFV3_GBL_TRC3 (OS_RESOURCE_TRC,
                     "Restarting the TMR BLK %x TMR ID %s NoOfTicks %d\n",
                     pTimer, gau1Os3DbgTimerId[u1TimerId], i4NrTicks);
    if (pTimer == NULL)
    {
        return;
    }

    if (u1TimerId == OSPFV3_HELLO_TIMER)
    {
        TmrStopTimer (gV3OsRtr.hellotimerLstId, &(pTimer->timerNode));
    }
    else if (u1TimerId == OSPFV3_50MS_LSU_LAK_TIMER)
    {
        TmrStopTimer (gV3OsRtr.g50msTimerLst, &(pTimer->timerNode));
    }
    else
    {
        TmrStopTimer (gV3OsRtr.timerLstId, &(pTimer->timerNode));
    }
    V3TmrSetTimer (pTimer, u1TimerId, i4NrTicks);

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "TMR Restarted\n");
}

/*****************************************************************************/
/* Function     : V3TmrDeleteTimer                                           */
/*                                                                           */
/* Description  : Deletes the timer from the timer list.                     */
/*                                                                           */
/* Input        : pTimer      : pointer to timer structure                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3TmrDeleteTimer (tV3OspfTimer * pTimer)
{
    OSPFV3_GBL_TRC2 (OS_RESOURCE_TRC, "Deleting the TMR BLK %x TMR ID %s\n",
                     pTimer, gau1Os3DbgTimerId[pTimer->u1TimerId]);
    if (pTimer->u1TimerId == OSPFV3_HELLO_TIMER)
    {
        TmrStopTimer (gV3OsRtr.hellotimerLstId, &(pTimer->timerNode));
    }
    else if (pTimer->u1TimerId == OSPFV3_50MS_LSU_LAK_TIMER)
    {
        TmrStopTimer (gV3OsRtr.g50msTimerLst, &(pTimer->timerNode));
    }
    else
    {
        TmrStopTimer (gV3OsRtr.timerLstId, &(pTimer->timerNode));
    }
    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "TMR Deleted\n");
}

/*****************************************************************************/
/* Function     : V3TmrHandleExpiry                                          */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a timer*/
/*                expiry occurs. This procedure finds the expired timers and */
/*                invokes the corresponding timer routines.                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3TmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Tmr Expiry To Be Handled\n");
    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gV3OsRtr.timerLstId)) != NULL)
    {
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                         "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        u1TimerId = ((tV3OspfTimer *) pExpiredTimers)->u1TimerId;

        if (u1TimerId >= OSPFV3_MAX_TIMERS)
        {
            continue;
        }

        i2Offset = gV3OsRtr.aTimerDesc[u1TimerId].i2Offset;

        OSPFV3_GBL_TRC2 (OS_RESOURCE_TRC,
                         "TmrId %s Offset %d \n", gau1Os3DbgTimerId[u1TimerId],
                         i2Offset);

        if (gV3OsRtr.bIgnoreExpiredTimers == OSIX_FALSE)
        {
            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter */
                (*(gV3OsRtr.aTimerDesc[u1TimerId].pTimerExpFunc)) (NULL);
            }
            else
            {
                (*(gV3OsRtr.aTimerDesc[u1TimerId].pTimerExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    gV3OsRtr.bIgnoreExpiredTimers = OSIX_FALSE;
    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Tmr Expiry Processing Over\n");
}

/*****************************************************************************/
/* Function     : V3TmrHandleHelloExpiry                                     */
/*                                                                           */
/* Description  : This procedure is invoked when the event indicating a timer*/
/*                expiry occurs. This procedure finds the expired timers and */
/*                invokes the corresponding timer routines.                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3TmrHandleHelloExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Hello Tmr Expiry To Be Handled\n");
    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gV3OsRtr.hellotimerLstId)) != NULL)
    {
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                         "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        u1TimerId = ((tV3OspfTimer *) pExpiredTimers)->u1TimerId;

        if (u1TimerId != OSPFV3_HELLO_TIMER)
        {
            continue;
        }

        i2Offset = gV3OsRtr.aTimerDesc[u1TimerId].i2Offset;

        OSPFV3_GBL_TRC2 (OS_RESOURCE_TRC,
                         "TmrId %s Offset %d \n", gau1Os3DbgTimerId[u1TimerId],
                         i2Offset);

        if (gV3OsRtr.bIgnoreExpiredTimers == OSIX_FALSE)
        {
            if (i2Offset == -1)
            {
                /* The timer function does not take any parameter */
                (*(gV3OsRtr.aTimerDesc[u1TimerId].pTimerExpFunc)) (NULL);
            }
            else
            {
                (*(gV3OsRtr.aTimerDesc[u1TimerId].pTimerExpFunc))
                    ((UINT1 *) pExpiredTimers - i2Offset);
            }
        }
    }
    gV3OsRtr.bIgnoreExpiredTimers = OSIX_FALSE;
    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "Hello Tmr Expiry Processing Over\n");
}

/*****************************************************************************/
/* Function     : V3TmrHelloTimer                                            */
/*                                                                           */
/* Description  : This procedure starts the timer and invokes the procedure  */
/*                to send hello on this interface.                           */
/*                                                                           */
/* Input        : pArg        : the interface whose hello timer has          */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrHelloTimer (VOID *pArg)
{
    tV3OsInterface     *pInterface = pArg;

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
        && (gu1O3HelloSwitchOverFlag == 0))
    {
        return;
    }
    V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);
}

/*****************************************************************************/
/* Function     : V3TmrPollTimer                                             */
/*                                                                           */
/* Description  : This procedure starts the timer and invokes the procedure  */
/*                to send hello on this interface.                           */
/*                                                                           */
/* Input        : pArg        : the interface whose poll timer has           */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrPollTimer (VOID *pArg)
{
    tV3OsInterface     *pInterface = pArg;

    V3UtilSendHello (pInterface, OSPFV3_POLL_TIMER);
}

/*****************************************************************************/
/* Function     : V3TmrWaitTimer                                             */
/*                                                                           */
/* Description  : The interface state machine is invoked with event          */
/*                OSPFV3_IFE_WAIT_TIMER.                                     */
/*                                                                           */
/* Input        : pArg        : the interface whose wait timer has           */
/*                                    fired                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrWaitTimer (VOID *pArg)
{
    tV3OsInterface     *pInterface = pArg;
    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "Wait Tmr Exp If %s\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)));

    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_WAIT_TIMER);

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "ISM Scheduled\n");
}

/*****************************************************************************/
/* Function     : V3TmrInactivityTimer                                       */
/*                                                                           */
/* Description  : The firing of the inactivity timer indicates that the      */
/*                neighbor can be considered dead. The neighbor state machine*/
/*                is invoked with the event OSPFV3_NBRE_INACTIVITY_TIMER.    */
/*                                                                           */
/* Input        : pArg        : nbr whose inactivity timer has fired         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrInactivityTimer (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = pArg;
    UINT4               u4CurrentTime = OsixGetSysUpTime ();

    if (((INT4) (pNbr->u4LastHelloRcvd) != OSPFV3_LESS) &&
        ((UINT4) (pNbr->pInterface->u2RtrDeadInterval) >
         (u4CurrentTime - pNbr->u4LastHelloRcvd)))
    {
        return;
    }

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF (pNbr->pInterface)
        && (pNbr->bHelloSuppression == OSIX_TRUE)
        && (pNbr->u1NsmState >= OSPFV3_NBRS_LOADING))
    {
        return;
    }

    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "Inactivity Tmr Exp Nbr %s\n",
                     Ip6PrintAddr (&(pNbr->nbrIpv6Addr)));

    /* If helper is down then graceful restart should exit */
    if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
        OSPFV3_GR_RESTART)
    {
        O3GrExitGracefulRestartInCxt
            (pNbr->pInterface->pArea->pV3OspfCxt, OSPFV3_RESTART_TOP_CHG);
    }
    /* if the neighbor is in graceful restart process, 
     * don't make the neighbor state as down */
    if (pNbr->u1NbrHelperStatus != OSPFV3_GR_HELPING)
    {
        OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_INACTIVITY_TIMER);
    }

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Nbr Evt Generated\n");
}

/*****************************************************************************/
/* Function     : V3TmrDdInitRxmtTimer                                       */
/*                                                                           */
/* Description  : The timer is started and the procedure to send empty DDP   */
/*                is invoked.                                                */
/*                                                                           */
/* Input        : pAgr            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrDdInitRxmtTimer (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = pArg;
    UINT4               u4RxmtTmrInterval = 0;

    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "InitRxmt Tmr Exp Nbr %s\n",
                     Ip6PrintAddr (&(pNbr->nbrIpv6Addr)));

    V3DdpRxmtDdp (pNbr);
    u4RxmtTmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
        (pNbr->pInterface->u2RxmtInterval);

    V3TmrSetTimer (&(pNbr->dbSummary.ddTimer), OSPFV3_DD_INIT_RXMT_TIMER,
                   ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ?
                    V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                  (pNbr->pInterface->u2RxmtInterval),
                                  OSPFV3_JITTER) : u4RxmtTmrInterval));

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, " DDP Sent To Nbr\n");
}

/*****************************************************************************/
/* Function     : V3TmrDdRxmtTimer                                           */
/*                                                                           */
/* Description  : The timer is started and the procedure to retransmit the   */
/*                previous DDP is invoked.                                   */
/*                                                                           */
/* Input        : pAgr            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrDdRxmtTimer (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = pArg;
    UINT4               u4RxmtTmrInterval = 0;

    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "DDP Rxmt Tmr Exp Nbr %s\n",
                     Ip6PrintAddr (&(pNbr->nbrIpv6Addr)));

    V3DdpRxmtDdp (pNbr);
    u4RxmtTmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
        (pNbr->pInterface->u2RxmtInterval);

    V3TmrSetTimer (&(pNbr->dbSummary.ddTimer), OSPFV3_DD_RXMT_TIMER,
                   ((pNbr->pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ?
                    V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                  (pNbr->pInterface->u2RxmtInterval),
                                  OSPFV3_JITTER) : u4RxmtTmrInterval));

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "DDP Rxmt\n");
}

/*****************************************************************************/
/* Function     :  V3TmrLsaNormalAgingTimer                                  */
/*                                                                           */
/* Description  : The timer is started and the procedure to retransmit       */
/*                request packet is invoked.                                 */
/*                                                                           */
/* Input        : pArg              : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrLsaNormalAgingTimer (VOID *pArg)
{
    tV3OsLsaInfo       *pLsaInfo = pArg;
    UINT4               u4RemGraceTime = 0;
    UINT1               u1Type = 0;

    if (!(IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (pLsaInfo->lsaId.u2LsaType)))
    {
        return;
    }

    /* Get the last byte from the LSA type. The LSA type contains 1 to 9
     * in the lower order byte. This is taken and used as array index for
     * trace function
     */
    u1Type = (UINT1) OSPFV3_GET_LOWER_LSA_TYPE_BYTE (pLsaInfo->lsaId.u2LsaType);

    if (u1Type < 10)
    {

        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                         "LSA Normal Aging Tmr Exp LSType %s\n",
                         gau1Os3DbgLsaType[u1Type]);
    }

    if ((!OSPFV3_IS_DNA_LSA (pLsaInfo)) ||
        (OSPFV3_IS_DNA_LSA (pLsaInfo)
         && (pLsaInfo->u1dnaLsaSplAgeFlag == OSPFV3_START)))
    {
        pLsaInfo->u2LsaAge +=
            OSPFV3_CHECK_AGE - (pLsaInfo->u2LsaAge % OSPFV3_CHECK_AGE);
    }

    if ((OSPFV3_IS_DNA_LSA (pLsaInfo)) &&
        (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)))
    {
        /*set DO_NOT_AGE in the lsa */
        pLsaInfo->u2LsaAge = OSPFV3_DO_NOT_AGE | OSPFV3_MAX_AGE;
    }
    else
    {
        if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            pLsaInfo->u2LsaAge = OSPFV3_MAX_AGE;
        }
    }

    if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        if (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
        {
            V3AgdFlushOut (pLsaInfo);
        }
        else
        {
            /* During graceful restart operation LSA's cannot be flushed */
            pLsaInfo->lsaAgingTimer.u1TimerId = OSPFV3_LSA_NORMAL_AGING_TIMER;
            TmrGetRemainingTime
                (gV3OsRtr.timerLstId, (tTmrAppTimer *)
                 & (pLsaInfo->pV3OspfCxt->graceTimer.timerNode),
                 &u4RemGraceTime);
            V3TmrSetTimer (&(pLsaInfo->lsaAgingTimer),
                           OSPFV3_LSA_NORMAL_AGING_TIMER,
                           (INT4) u4RemGraceTime);
        }
    }
    else
    {
        /* Check sum of the LSA is checked every  CHECK_AGE Interval */
        if (V3AgdCheckChksum (pLsaInfo) == OSIX_FAILURE)
        {
            gV3OsRtr.bIgnoreExpiredTimers = OSIX_TRUE;
            return;
        }

        pLsaInfo->lsaAgingTimer.u1TimerId = OSPFV3_LSA_NORMAL_AGING_TIMER;
        V3TmrSetTimer (&(pLsaInfo->lsaAgingTimer),
                       OSPFV3_LSA_NORMAL_AGING_TIMER,
                       (INT4) OSPFV3_NO_OF_TICKS_PER_SEC * OSPFV3_CHECK_AGE);

        /* 
         * In case of Self originated LSA's if age of the LSA is equal to
         * OSPFV3_LSA_REFRESH_INTERVAL & LSA does not describe any unreachable 
         * Destination than that lsa is reoriginated . 
         * REF Point 1 of LSA origination RFC 2178. 
         */

        if (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
        {
            if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo) &&
                (pLsaInfo->u2LsaAge ==
                 (OSPFV3_LSA_REFRESH_INTERVAL / OSPFV3_NO_OF_TICKS_PER_SEC)))
            {
                pLsaInfo->u1LsaRefresh = OSIX_FALSE;

                if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
                {
                    V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                                           OSPFV3_SIG_LS_REFRESH,
                                           (UINT1 *) pLsaInfo->pLsaDesc);
                }
            }
        }
        else
        {
            /* Graceful restart is in progress, LSA can be reoriginated
             * only after exiting graceful restart */
            pLsaInfo->lsaAgingTimer.u1TimerId = OSPFV3_LSA_NORMAL_AGING_TIMER;
            TmrGetRemainingTime (gV3OsRtr.timerLstId, (tTmrAppTimer *)
                                 & (pLsaInfo->pV3OspfCxt->graceTimer.timerNode),
                                 &u4RemGraceTime);
            V3TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                               OSPFV3_LSA_NORMAL_AGING_TIMER, u4RemGraceTime);
        }
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Chk Aged And Refreshed\n");
    }
}

/*****************************************************************************/
/* Function     : V3TmrMinLsaIntervalTimer                                   */
/*                                                                           */
/* Description  : The min_lsa_interval_exp flag is set. If the lsa_changed   */
/*                flag is set a new instance of the advertisement is         */
/*                originated.                                                */
/*                                                                           */
/* Input        : pArg               : the lsa_desc of the associated lsa    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrMinLsaIntervalTimer (VOID *pArg)
{
    tV3OsLsaDesc       *pLsaDesc = pArg;
    UINT1               u1Type = 0;

    if (!(IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED
          (pLsaDesc->pLsaInfo->lsaId.u2LsaType)))
    {
        return;
    }

    /* Get the last byte from the LSA type. The LSA type contains 1 to 9
     * in the lower order byte. This is taken and used as array index for
     * trace function
     */
    u1Type = (UINT1) OSPFV3_GET_LOWER_LSA_TYPE_BYTE
        (pLsaDesc->pLsaInfo->lsaId.u2LsaType);
    if (u1Type < 10)
    {
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC, "MinLSAInt Exp LSType %s\n",
                         gau1Os3DbgLsaType[u1Type]);
    }
    pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;

    if (pLsaDesc->u1LsaChanged == OSIX_TRUE)
    {
        if (pLsaDesc->u2InternalLsaType == OSPFV3_INTRA_AREA_PREFIX_LSA)
        {
            V3ResetMinLsaForAllIAPrefixLsa (pLsaDesc->pLsaInfo->pArea);
        }
        else
        {
            pLsaDesc->u1LsaChanged = OSIX_FALSE;
        }
        V3SignalLsaRegenInCxt (pLsaDesc->pLsaInfo->pV3OspfCxt,
                               OSPFV3_SIG_NEXT_INSTANCE, (UINT1 *) pLsaDesc);
    }
}

/*****************************************************************************/
/* Function     : V3TmrLsaRxmtTimer                                          */
/*                                                                           */
/* Description  : The timer is restarted and the procedure to construct and  */
/*                send a LSU pkt from the rxmt list is invoked.              */
/*                                                                           */
/* Input        : pArg            : the nbr associated with the timer        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrLsaRxmtTimer (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = pArg;
    UINT4               u4RxmtTmrInterval = 0;

    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "LSA Rxmt Exp Nbr %s\n",
                     Ip6PrintAddr (&(pNbr->nbrIpv6Addr)));

    /* start timer if nbr rxmt count present */
    if (pNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
    {
        if (V3LsuRxmtLsu (pNbr) == OSIX_FAILURE)
        {
            return;
        }

        /* Neighbor LSA Rxmtcount can become 0 in case of re-transmitting
         * Grace LSA. If the re-transmission count has reached the
         * threshold, Grace LSA will be deleted from the re-transmission
         * list without re-transmitting to the neighbor.
         * If the router is undergoing graceful restart, then re-transmit
         * the LSA with 1s interval */
        if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
            OSPFV3_GR_SHUTDOWN)
        {
            u4RxmtTmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC;
        }
        else
        {
            u4RxmtTmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
                (pNbr->pInterface->u2RxmtInterval);
        }
        V3TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer), OSPFV3_LSA_RXMT_TIMER,
                       ((pNbr->pInterface->u1NetworkType ==
                         OSPFV3_IF_BROADCAST) ? V3UtilJitter (u4RxmtTmrInterval,
                                                              OSPFV3_JITTER) :
                        u4RxmtTmrInterval));
    }
    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "LSU Rxmt\n");
}

/*****************************************************************************/
/* Function     : V3TmrRunRtTimer                                            */
/*                                                                           */
/* Description  : The router_network_lsa_changed flag is checked. If it is   */
/*                set then the procedure to calculate the entire routing     */
/*                table is invoked. The flag is also reset.                  */
/*                                                                           */
/* Input        : pArg   -   Context pointer                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrRunRtTimer (VOID *pArg)
{
    tV3OspfCxt         *pV3OspfCxt = (tV3OspfCxt *) pArg;
    UINT4               u4CurrentTime = 0;
    UINT4               u4Interval = 0;
    UINT4               u4Duration = OSPFV3_INIT_VAL;
    tV3OsTruthValue     bCalculateRt = OSPFV3_TRUE;

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "RT Tmr Exp\n");

    /* Get the current system timer */
    u4CurrentTime = OsixGetSysUpTime ();

    if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
    {
        /* Staggered route calculation support is enabled. If any of the
         * context is in the middle of route calculation process and a
         * route timer has been fired for another context, then add this
         * context to the global DLL of SPF list. If the timer is not running
         * start the timer
         */
        if (gV3OsRtr.u4RTStaggeredCtxId != OSPFV3_INVALID_CXT_ID)
        {
            /* Current route calculation not completed,
             * so we need to defer the route calculation
             */
            if (pV3OspfCxt->bVrfSpfTmrStatus == OSPFV3_FALSE)
            {
                /* The context is not present in the DLL. Add the context to
                 * the DLL. If the current DLL count is not 0, the timer must
                 * be running else start the timer
                 */
                TMO_DLL_Add (&(gV3OsRtr.vrfSpfList),
                             &(pV3OspfCxt->nextVrfSpfNode));
                pV3OspfCxt->bVrfSpfTmrStatus = OSPFV3_TRUE;

                if (TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) == 1)
                {
                    u4Duration =
                        ((SYS_TIME_TICKS_IN_A_SEC * gV3OsRtr.u4VrfSpfInterval)
                         / OSPFV3_MSEC_IN_SEC);
                    if (u4Duration == OSPFV3_ZERO)
                    {
                        u4Duration = OSPFV3_ONE;
                    }
                    V3TmrSetTimer (&(gV3OsRtr.vrfSpfTimer),
                                   OSPFV3_VRF_SPF_TIMER, u4Duration);
                }
            }
            bCalculateRt = OSPFV3_FALSE;
        }
    }

    /* Two consecute context can perform route calculation only with a
     * spacing interval of u4VrfSpfInterval. If the minimum interval is
     * not there, then the context is added to the global DLL of VRF SPF
     * list. VRF SPF timer is started if it is not running
     */
    if ((TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) != OSPFV3_ZERO) ||
        ((((u4CurrentTime - gV3OsRtr.u4LastRtCalcTime) * OSPFV3_MSEC_IN_SEC) <
          (gV3OsRtr.u4VrfSpfInterval)) &&
         (V3OspfGetSystemMode (OSPF3_PROTOCOL_ID) == OSPFV3_MI_MODE)))
    {
        /* Already some other context is present in the DLL. So, add this
         * context to the exising DLL or time difference is less than
         * spacing interval
         */
        if (pV3OspfCxt->bVrfSpfTmrStatus == OSPFV3_FALSE)
        {
            TMO_DLL_Init_Node (&(pV3OspfCxt->nextVrfSpfNode));
            TMO_DLL_Add (&(gV3OsRtr.vrfSpfList), &(pV3OspfCxt->nextVrfSpfNode));
            pV3OspfCxt->bVrfSpfTmrStatus = OSPFV3_TRUE;

            if (TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) == OSPFV3_ONE)
            {
                /* u4CurrentTime - gV3OsRtr.u4LastRtCalcTime <
                 * gV3OsRtr.u4VrfSpfInterval. So the timer value to be used
                 * to start the VRF timer should be the
                 * remaining time required to get the minimum interval
                 * (gV3OsRtr.u4VrfSpfInterval)
                 */
                u4Interval = gV3OsRtr.u4VrfSpfInterval -
                    ((u4CurrentTime -
                      gV3OsRtr.u4LastRtCalcTime) * OSPFV3_MSEC_IN_SEC);
                if (u4Interval < OSPFV3_DEF_VRF_SPF_INTERVAL)
                {
                    u4Interval = OSPFV3_DEF_VRF_SPF_INTERVAL;
                }
                u4Duration =
                    ((SYS_TIME_TICKS_IN_A_SEC * u4Interval) /
                     OSPFV3_MSEC_IN_SEC);

                if (u4Duration == OSPFV3_ZERO)
                {
                    u4Duration = OSPFV3_ONE;
                }
                V3TmrSetTimer (&(gV3OsRtr.vrfSpfTimer),
                               OSPFV3_VRF_SPF_TIMER, u4Duration);
            }
        }
        bCalculateRt = OSPFV3_FALSE;
    }

    if (bCalculateRt == OSPFV3_TRUE)
    {
        /* Perform the whole route calculation */
        pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;
        V3RtcCalculateRtInCxt (pV3OspfCxt);
        gV3OsRtr.u4LastRtCalcTime = OsixGetSysUpTime ();
        pV3OspfCxt->u4LastCalTime = gV3OsRtr.u4LastRtCalcTime;
    }

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Entire RT Calculated\n");
}

/****************************************************************************/
/* Function     : V3TmrNssaFsmStbltyTimer                                   */
/*                                                                          */
/* Description  : This routine handles NSSA Stblty Timer timeout            */
/*                                                                          */
/* Input        : pArg                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/****************************************************************************/
PRIVATE VOID
V3TmrNssaFsmStbltyTimer (VOID *pArg)
{
    tV3OsArea          *pArea = pArg;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "FUNC_ENTRY: TmrNssaFsmStbltyTimer\n");
    switch (pArea->u1NssaTrnsltrState)
    {
        case OSPFV3_TRNSLTR_STATE_ENAELCTD:
            V3AreaNssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                           OSPFV3_TRNSLTR_STATE_DISABLED);
            break;

        case OSPFV3_TRNSLTR_STATE_DISABLED:
            OSPFV3_GBL_TRC2 (OSPFV3_NSSA_TRC, "Error : StbltyIntrvlTimeout Event in \
                Invalid State %d in Area %x\n", pArea->u1NssaTrnsltrState,
                             pArea->areaId);
            break;

        default:
            OSPFV3_GBL_TRC2 (OSPFV3_NSSA_TRC,
                             "Error : StbltyIntrvlTimeout Event in"
                             " Invalid State %d in Area %x\n",
                             pArea->u1NssaTrnsltrState, pArea->areaId);
            break;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "FUNC_EXIT: TmrNssaFsmStbltyTimer\n");
}

/*****************************************************************************/
/* Function     : V3TmrExitOverflowTimer                                     */
/*                                                                           */
/* Description  : in this procedure, the router attempts to leave the        */
/*                overflow state                                             */
/*                                                                           */
/* Input        : pArg   -  Context pointer                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrExitOverflowTimer (VOID *pArg)
{
    tV3OspfCxt         *pV3OspfCxt = (tV3OspfCxt *) pArg;
    tInputParams        inParams;
    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    VOID               *pTempPtr = NULL;
    UINT4               u4ExtRtCount = 0;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "ExitOvrFlw Tmr Exp\n");

    /* Count the non-default external routes */
    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_RT_TRC, "TrieGetFirstNode Failure\n");
        return;
    }
    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        if (pExtRoute->u1PrefixLength != 0)
        {
            u4ExtRtCount++;
        }
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    /* router leaves the overflow state */
    if ((pV3OspfCxt->u4NonDefAsLsaCount + u4ExtRtCount) <
        ((UINT4) pV3OspfCxt->i4ExtLsdbLimit))
    {
        pV3OspfCxt->bOverflowState = OSIX_FALSE;
        V3LsuGenerateNonDefaultAsExtLsasInCxt (pV3OspfCxt);
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Rtr Leaves OverFlw State\n");
    }
    else
    {
        /* router re-enters the overflow state */
        if (OSPFV3_IS_RTR_CFGD_TO_LEAVE_OVFL_STATE (pV3OspfCxt))
        {
            V3TmrSetTimer (&(pV3OspfCxt->exitOverflowTimer),
                           OSPFV3_EXIT_OVERFLOW_TIMER,
                           V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                         (pV3OspfCxt->u4ExitOverflowInterval),
                                         OSPFV3_JITTER));
        }
    }

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Rtr Leaves OverFlw State\n");
}

/*****************************************************************************/
/* Function     : V3TmrNbrProbeTimer                                         */
/*                                                                           */
/* Description  : This procedure starts the neighbor probe timer and start   */
/*                sending LSU packet on this interface.                      */
/*                                                                           */
/* Input        : pArg        : the interface whose probe timer has          */
/*                              fired                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrNbrProbeTimer (VOID *pArg)
{
    tV3OsInterface     *pInterface = (tV3OsInterface *) pArg;
    UINT1               u1SendLsuFlag = OSIX_FALSE;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsLinkStateId    linkStateId;
    tV3OsNeighbor      *pNbr = NULL;

    if (pInterface->bDemandNbrProbe == OSIX_TRUE)
    {
        OSPFV3_BUFFER_DWTOPDU (linkStateId, pInterface->u4InterfaceId);
        pLsaInfo = V3LsuSearchDatabase
            (OSPFV3_LINK_LSA,
             &(linkStateId),
             &(OSPFV3_RTR_ID (pInterface->pArea->pV3OspfCxt)),
             pInterface, NULL);

        if (pLsaInfo == NULL)
        {
            return;
        }

        if (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface))
        {
            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
                if ((pNbr != NULL) &&
                    (pNbr->u1NsmState == OSPFV3_NBRS_FULL) &&
                    (OSPFV3_IS_HELLO_SUPPRESSED_NBR (pNbr)))
                {

                    /* Add the LSA on the neighbor's retransmission list */
                    V3LsuAddToRxmtLst (pNbr, pLsaInfo);

                    u1SendLsuFlag = OSIX_TRUE;

                }                /* end of if */
            }                    /* end of for */
        }

        /* Flood the lsa */
        if (u1SendLsuFlag == OSIX_TRUE)
        {
            V3LsuAddToLsu (NULL, pLsaInfo, pInterface);
            V3LsuSendLsu (NULL, pInterface);
        }
    }

    V3TmrSetTimer (&(pInterface->nbrProbeTimer),
                   OSPFV3_NBR_PROBE_INTERVAL_TIMER,
                   (OSPFV3_NO_OF_TICKS_PER_SEC *
                    (pInterface->u4NbrProbeInterval)));
}

/*****************************************************************************/
/* Function     : V3TmrVrfSpfTimer                                           */
/*                                                                           */
/* Description  : This timer is started in the following scenarios when      */
/*                Route calculation timer is fired for any context           */
/*                                                                           */
/*                1. Any other context is present in global VRF DLL          */
/*                2. Currently any context is in staggered state             */
/*                3. Route calculation triggered for another context         */
/*                   before the minimum subsequent route calculation time    */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrVrfSpfTimer (VOID *pArg)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    UINT4               u4Duration = OSPFV3_INIT_VAL;

    UNUSED_PARAM (pArg);

    /* If any context is in staggered route calculation state, restart the
     * timer
     */
    if (gV3OsRtr.u4RTStaggeredCtxId == OSPFV3_INVALID_CXT_ID)
    {
        /* Get the first Context node from the Global DLL list and perform
         * route calculation for the context
         */
        pNode = TMO_DLL_Get (&(gV3OsRtr.vrfSpfList));
        pV3OspfCxt = OSPFV3_GET_BASE_PTR (tV3OspfCxt, nextVrfSpfNode, pNode);
        pV3OspfCxt->bVrfSpfTmrStatus = OSPFV3_FALSE;

        if (pV3OspfCxt->u1RtrNetworkLsaChanged == OSIX_TRUE)
        {
            pV3OspfCxt->u1RtrNetworkLsaChanged = OSIX_FALSE;

            if (pV3OspfCxt->admnStat == OSPFV3_ENABLED)
            {
                V3RtcCalculateRtInCxt (pV3OspfCxt);
            }
            gV3OsRtr.u4LastRtCalcTime = OsixGetSysUpTime ();
            pV3OspfCxt->u4LastCalTime = gV3OsRtr.u4LastRtCalcTime;
        }
    }

    if (TMO_DLL_Count (&(gV3OsRtr.vrfSpfList)) != OSPFV3_ZERO)
    {
        u4Duration = ((SYS_TIME_TICKS_IN_A_SEC * gV3OsRtr.u4VrfSpfInterval)
                      / OSPFV3_MSEC_IN_SEC);
        if (u4Duration == OSPFV3_ZERO)
        {
            u4Duration = OSPFV3_ONE;
        }
        V3TmrSetTimer (&(gV3OsRtr.vrfSpfTimer),
                       OSPFV3_VRF_SPF_TIMER, u4Duration);
    }
}

/*****************************************************************************/
/* Function     : V3TmrRestartGraceTimer                                     */
/*                                                                           */
/* Description  : This routine exits the graceful restart process for        */
/*                this instance                                              */
/*                                                                           */
/* Input        : pArg                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*****************************************************************************/
PRIVATE VOID
V3TmrRestartGraceTimer (VOID *pArg)
{
    tV3OspfCxt         *pV3OspfCxt = pArg;

    /* Reset the trap count value to 0 to start a new window */
    OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "O3Grace Tmr Exp\n");

    O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_TIMEDOUT);
}

/*****************************************************************************/
/* Function     : V3TmrHelperGraceTimer                                      */
/*                                                                           */
/* Description  :  Timer expiry handler for grace timer in helper module     */
/*                 If this timer expires, router should exit the helper mode */
/*                                                                           */
/* Input        : pArg          : pointer to the neighbor stucture           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3TmrHelperGraceTimer (VOID *pArg)
{
    tV3OsNeighbor      *pNbr = pArg;

    OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC,
                     "Grace timer expired in helper for the neighbor %s\n",
                     pNbr->nbrRtrId);

    O3GrExitHelper (OSPFV3_HELPER_GRACE_TIMEDOUT, pNbr);

}

/*****************************************************************************/
/* Function     : V3TmrMultIfActiveDetectTimer                               */
/*                                                                           */
/* Description  :  Timer expiry handler for Active Detect timer.             */
/*                 If this timer expires, Interface should Elect the         */
/*                 Active/Standby interfaces over the link                   */
/*                                                                           */
/* Input        : pArg          : pointer to the Interface stucture          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/

PUBLIC VOID
V3TmrMultIfActiveDetectTimer (VOID *pArg)
{
    tV3OsInterface     *pInterface = (tV3OsInterface *) pArg;

    /* Update the Active Detect tmr Flag */
    pInterface->u1ActiveDetectTmrFlag = OSPFV3_MULTIF_ACTIVE_TMR_STOP;

    /* Generate event for Active/Standby interface election over the link */
    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);

}

#ifdef OSPFV3_RELQ_TEST_WANTED
/*Global pointer to function. To testing purpose in ospf3test1.c file*/
VOID                (*gpTimerFunctionRunEmulation) (VOID *pArg) =
    &V3TmrRunRtTimer;
#endif /* OSPFV3_RELQ_TEST_WANTED */

/*---------------------------------------------------------------------------*/
/*                         End of file o3tmr.c                             */
/*---------------------------------------------------------------------------*/
