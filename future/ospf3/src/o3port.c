/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3port.c,v 1.41 2018/02/01 11:01:42 siva Exp $
 *
 * Description: This file contains procedures to be ported for integrating in
 *            customer environment. 
 *
 *******************************************************************/
#include "o3inc.h"
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>    /* for NF_ACCEPT */
#include <errno.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#endif /* LNXIP6_WANTED */
#endif /* RAWSOCK_HELLO_PKT_WANTED */

#ifdef LNXIP6_WANTED
    /* changes for unnumbered-interface */
#ifdef RAWSOCK_HELLO_PKT_WANTED
PRIVATE INT4        V3OSPFNLHelloProcessPkt
PROTO ((struct nfq_data * pNfqData));
PRIVATE INT4        V3OSPFNLHelloCallBack
PROTO ((struct nfq_q_handle * pQHandle,
        struct nfgenmsg * nfMsg, struct nfq_data * nfData, void *data));
struct nfq_handle  *gp3Handle;
struct nfq_q_handle *gpQ3Handle;
PRIVATE UINT1       gau1RxPktBuf[OSPFV3_MAX_MTU_SIZE];
#endif /* RAWSOCK_HELLO_PKT_WANTED */

PRIVATE INT4        V3SendPktOnUnNumberedIntf (UINT1 *pu1RawPkt, UINT4 u4Length,
                                               UINT4 u4Port,
                                               tIp6Addr * pSrcIp6Addr,
                                               tIp6Addr * pDstIp6Addr,
                                               UINT4 u4ContextId);
#endif /* LNXIP6_WANTED */

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DemandProbeOpen                                          */
/*                                                                           */
/* Description  : This function will be called when the lower layer gets the */
/*                indication that the demand circuit interface has been      */
/*                opened                                                     */
/*                                                                           */
/* Input        : u4IfIndex    : Interface Index for the interface           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3DemandProbeOpen (UINT4 u4IfIndex)
{
    tV3OsInterface     *pInterface = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3DemandProbeOpen\n");
    if ((pInterface = V3GetFindIf (u4IfIndex)) != NULL)
    {
        V3IfDemandProbeOpen (pInterface);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3DemandProbeOpen\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3DemandProbeClose                                         */
/*                                                                           */
/* Description  : This function will be called when the lower layer gets the */
/*                indication that the demand circuit interface has been      */
/*                clsoed                                                     */
/*                                                                           */
/* Input        : u4IfIndex    : Interface Index for the interface           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3DemandProbeClose (UINT4 u4IfIndex)
{
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3DemandProbeClose\n");

    if ((pInterface = V3GetFindIf (u4IfIndex)) != NULL)
    {
        V3IfDemandProbeClose (pInterface);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3DemandProbeClose\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : V3Ipv6OspfInterface                                 */
/*                                                                         */
/*     Description   : This function is provided to IPv6 as a callback     */
/*                     function. This is called by IPv6 when OSPFV3 pkts   */
/*                     are received or IPv6 Address status chenged or      */
/*                     Interface status changed.                           */
/*                                                                         */
/*     Input(s)      :  pIp6HliParams : Pointer to tIp6HliParams           */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  VOIDV.                                             */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
V3Ipv6OspfInterface (tNetIpv6HliParams * pIp6HliParams)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3Ipv6OspfInterface\n");

    switch (pIp6HliParams->u4Command)
    {
        case NETIPV6_APPLICATION_RECEIVE:
            V3OspfRcvPkt (pIp6HliParams->unIpv6HlCmdType.AppRcv.pBuf,
                          NULL,
                          pIp6HliParams->unIpv6HlCmdType.AppRcv.u4PktLength,
                          pIp6HliParams->unIpv6HlCmdType.AppRcv.u4Index);
            break;

        case NETIPV6_ADDRESS_CHANGE:
            V3OspfIfAddrChg (&(pIp6HliParams->unIpv6HlCmdType.AddrChange));
            break;

        case NETIPV6_INTERFACE_PARAMETER_CHANGE:
            V3OspfIfStatChg (&(pIp6HliParams->unIpv6HlCmdType.IfStatChange));
            break;

        default:
            break;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3Ipv6OspfInterface\n");

}

/***************************************************************************/
/*                                                                         */
/*     Function Name : V3OspfSendPkt                                       */
/*                                                                         */
/*     Description   : This function sends the OSPFV3 pkt using functional */
/*                    interface to  FutureIP6 or through Raw Scokets       */
/*                                                                         */
/*     Input(s)      :  pu1DataPkt    : Pointer to the buffer.             */
/*                      u4Length      : Length of the OSPFV3 pdu.          */
/*                      u4IfIndex     : Interfce Index                     */
/*                      pSrcIp6Addr  : Source IPv6 Addr.                   */
/*                      pDstIp6Addr : Destination IPv6 Addr.               */
/*                                                                         */
/*     Output(s)     :  None.                                              */
/*                                                                         */
/*     Returns       :  OSIX_SUCCESS or OSIX_FAILURE.                      */
/*                                                                         */
/***************************************************************************/

PUBLIC INT4
V3OspfSendPkt (UINT4 u4ContextId, UINT1 *pu1DataPkt,
               UINT4 u4Length,
               UINT4 u4IfIndex, tIp6Addr * pSrcIp6Addr,
               tIp6Addr * pDstIp6Addr, UINT1 u1HopLimit)
{
#ifndef RAWSOCK_WANTED
    tHlToIp6Params      params;
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
#else
    struct sockaddr_in6 destAddr;
    INT4                i4SendBytes;
    INT4                i4RawSockId = -1;
    struct in6_pktinfo  pktInfo;
    UINT4               u4AddrLen;
    struct msghdr       msg;
    struct iovec        iov[1];
    struct cmsghdr     *cmsg;
    UINT4               u4IfaceIndex = u4IfIndex;
    UINT1               buf[256], *pu1OspfPkt = NULL;
#ifdef SLI_WANTED
    INT4                i4OptEnable = 1;
#endif
#endif
#ifdef LNXIP6_WANTED
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSendPkt\n");

    pInterface = V3GetFindIf (u4IfaceIndex);
    if (pInterface == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "V3GetFindIf failed for given if \n");
        gu4V3OspfSendPktFail++;
        return OSIX_FAILURE;
    }
    /* changes for unnumbered-interface */
    if (pInterface->u4AddrlessIf != 0)
    {
        if (V3SendPktOnUnNumberedIntf
            (pu1DataPkt, u4Length, u4IfaceIndex, pSrcIp6Addr,
             pDstIp6Addr, u4ContextId) == OSIX_FAILURE)
        {
            gu4V3OspfSendPktFail++;
            return OSIX_FAILURE;

        }
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendPkt\n");

        return OSIX_SUCCESS;
    }
#endif

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendPkt\n");

        return OSIX_SUCCESS;
    }

#ifndef RAWSOCK_WANTED

    params.u1Cmd = IP6_LAYER4_DATA;
    params.u1Hlim = u1HopLimit;
    params.u1Proto = OSPFV3_PROTO;
    params.u4Index = u4IfIndex;
    params.u4Len = u4Length;

    OSPFV3_IP6ADDR_CPY (&(params.Ip6SrcAddr), pSrcIp6Addr);
    OSPFV3_IP6ADDR_CPY (&(params.Ip6DstAddr), pDstIp6Addr);

    if ((pPkt = CRU_BUF_Allocate_MsgBufChain (u4Length, 0)) == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : Unable to allocate buf\n");
        gu4V3OspfSendPktFail++;
        return OSIX_FAILURE;
    }

    CRU_BUF_Copy_OverBufChain (pPkt, pu1DataPkt + IPV6_HEADER_LEN, 0, u4Length);

    if (Ip6RcvFromHl (pPkt, &params) == IP6_FAILURE)
    {
        /* In failure scenario, the buffer will be released by IPv6 module */
        gu4V3OspfSendPktFail++;
        return OSIX_FAILURE;
    }
    UNUSED_PARAM (u4ContextId);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendPkt\n");

    return OSIX_SUCCESS;
#else
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    i4RawSockId = gV3OsRtr.ai4RawSockId[u4ContextId];
#else
    i4RawSockId = gV3OsRtr.i4RawSockId;
#endif
    UNUSED_PARAM (u1HopLimit);
    MEMSET (&(destAddr), 0, sizeof (struct sockaddr_in6));
    destAddr.sin6_family = AF_INET6;
    OSPFV3_IP6ADDR_CPY (&(destAddr.sin6_addr), pDstIp6Addr);

#ifdef LNXIP6_WANTED
    u4IfaceIndex = CfaGetIfIpPort (u4IfIndex);
#endif

    if ((V3UtilIp6AddrComp (pDstIp6Addr, &gV3OsAllSpfRtrs) ==
         OSPFV3_EQUAL)
        || (V3UtilIp6AddrComp (pDstIp6Addr, &gV3OsAllDRtrs) == OSPFV3_EQUAL))
    {

        if (OSPFV3_SET_SOCK_OPT
            ((i4RawSockId), IPPROTO_IPV6, IPV6_MULTICAST_IF,
             (INT4 *) &(u4IfaceIndex), sizeof (INT4)) < 0)
        {
            gu4V3OspfSendPktFail++;
            return OSIX_FAILURE;
        }
    }

#ifdef SLI_WANTED
    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_PKTINFO, &(i4OptEnable),
         sizeof (INT4)) < 0)
    {
        gu4V3OspfSendPktFail++;
        return OSIX_FAILURE;
    }
#endif

    pu1OspfPkt = (UINT1 *) (pu1DataPkt + IPV6_HEADER_LEN);
    u4AddrLen = sizeof (struct sockaddr_in6);
    msg.msg_name = ((void *) &destAddr);
    msg.msg_namelen = u4AddrLen;
    iov[0].iov_base = (VOID *) pu1OspfPkt;
    iov[0].iov_len = u4Length;
    msg.msg_iov = (void *) iov;
    msg.msg_iovlen = 1;
    memset (buf, 0, sizeof (buf));
    cmsg = (struct cmsghdr *) (VOID *) buf;
    msg.msg_control = cmsg;
    msg.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));
    cmsg->cmsg_len = CMSG_LEN (sizeof (pktInfo));
    cmsg->cmsg_level = IPPROTO_IPV6;
    cmsg->cmsg_type = IPV6_PKTINFO;
    memcpy (&(pktInfo.ipi6_addr), pSrcIp6Addr, (sizeof (struct in6_addr)));
    pktInfo.ipi6_ifindex = u4IfaceIndex;
    memcpy (CMSG_DATA (cmsg), &pktInfo, sizeof (pktInfo));
    msg.msg_flags = 0;

    i4SendBytes = sendmsg (i4RawSockId, &msg, 0);
    if (i4SendBytes == -1)
    {
        perror ("sendmsg");
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendPkt\n");
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfJoinMcastGroup                                       */
/*                                                                           */
/* Description  : Set socket option to join Multicast group on an Interface  */
/*                                                                           */
/* Input        : pMcastGroupAddr : multicast group address                  */
/*                u4IfIndex       :interface index                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3OspfJoinMcastGroup (tIp6Addr * pMcastGroupAddr, UINT4 u4IfIndex,
                      UINT4 u4ContextId)
{
#ifdef RAWSOCK_WANTED
    INT4                i4RawSockId = -1;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    i4RawSockId = gV3OsRtr.ai4RawSockId[u4ContextId];
#else
    UNUSED_PARAM (u4ContextId);
    i4RawSockId = gV3OsRtr.i4RawSockId;
#endif
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;

    OSPFV3_IP6ADDR_CPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr);
#ifdef LNXIP6_WANTED
    ip6Mreq.ipv6mr_interface = CfaGetIfIpPort (u4IfIndex);
#else
    ip6Mreq.ipv6mr_interface = u4IfIndex;
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfJoinMcastGroup\n");

    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_JOIN_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        gu4V3OspfJoinMcastGroupFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfJoinMcastGroup\n");
    return OSIX_SUCCESS;

#else
    tSliIpv6Mreq        ip6Mreq;

    OSPFV3_IP6ADDR_CPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr);
    ip6Mreq.ipv6mr_ifindex = u4IfIndex;

    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_JOIN_GROUP,
         &(ip6Mreq), sizeof (tSliIpv6Mreq)) < 0)
    {
        gu4V3OspfJoinMcastGroupFail++;
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfJoinMcastGroup\n");

    return OSIX_SUCCESS;

#endif
#else
    UNUSED_PARAM (u4ContextId);
    if (NetIpv6OspfMcastJoin (u4IfIndex, pMcastGroupAddr) != NETIPV6_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to join in to multicast"
                      " group for the interface with if index = %d, %s\n",
                      u4IfIndex, Ip6PrintAddr (pMcastGroupAddr)));

        OSPFV3_GBL_TRC2 (OSPFV3_CRITICAL_TRC, "Failed to join in to multicast"
                         " group for the interface with if index = %d, %s\n",
                         u4IfIndex, Ip6PrintAddr (pMcastGroupAddr));
        gu4V3OspfJoinMcastGroupFail++;
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfJoinMcastGroup\n");
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfLeaveMcastGroup                                      */
/*                                                                           */
/* Description  : Set socket option to leave given multicase group           */
/*                                                                           */
/* Input        :  pMcastGroupAddr : multicast group address                 */
/*                 u4IfIndex       :interface index                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3OspfLeaveMcastGroup (tIp6Addr * pMcastGroupAddr, UINT4 u4IfIndex,
                       UINT4 u4ContextId)
{
#ifdef RAWSOCK_WANTED
    INT4                i4RawSockId = -1;
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    i4RawSockId = gV3OsRtr.ai4RawSockId[u4ContextId];
#else
    UNUSED_PARAM (u4ContextId);
    i4RawSockId = gV3OsRtr.i4RawSockId;
#endif

#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;

    OSPFV3_IP6ADDR_CPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr);
#ifdef LNXIP6_WANTED
    ip6Mreq.ipv6mr_interface = CfaGetIfIpPort (u4IfIndex);
#else
    ip6Mreq.ipv6mr_interface = u4IfIndex;
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfLeaveMcastGroup\n");

    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_LEAVE_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfLeaveMcastGroup\n");
    return OSIX_SUCCESS;

#else
    tSliIpv6Mreq        ip6Mreq;

    OSPFV3_IP6ADDR_CPY (&(ip6Mreq.ipv6mr_multiaddr), pMcastGroupAddr);
    ip6Mreq.ipv6mr_ifindex = u4IfIndex;

    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_LEAVE_GROUP,
         &(ip6Mreq), sizeof (tSliIpv6Mreq)) < 0)
    {
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfLeaveMcastGroup\n");
    return OSIX_SUCCESS;
#endif
#else
    UNUSED_PARAM (u4ContextId);
    if (NetIpv6OspfMcastLeave (u4IfIndex, pMcastGroupAddr) != NETIPV6_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to leave from multicast"
                      " group for the interface with if index = %d, %s\n",
                      u4IfIndex, Ip6PrintAddr (pMcastGroupAddr)));

        OSPFV3_GBL_TRC2 (OSPFV3_CRITICAL_TRC, "Failed to leave from multicast"
                         " group for the interface with if index = %d, %s\n",
                         u4IfIndex, Ip6PrintAddr (pMcastGroupAddr));
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfLeaveMcastGroup\n");
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OSPFSetIfOperStat                                        */
/*                                                                           */
/* Description  : This function is invoked when an indication from lower     */
/*                levels is received for the OPER STATUS change for the      */
/*                interface.                                                 */
/*                                                                           */
/* Input        : u4IfIndex    -   Interface Index                           */
/*                u1Status    -  Interface state(UP/DOWN)                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OSPFSetIfOperStat (UINT4 u4IfIndex, UINT1 u1Status)
{
    tV3OsInterface     *pInterface = NULL;
    UINT1               u1OspfStatus;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OSPFSetIfOperStat \n");

    if ((pInterface = V3GetFindIf (u4IfIndex)) != NULL)
    {
        u1OspfStatus = pInterface->operStatus;

        if (u1Status == NETIPV6_IF_UP)
        {
            u1OspfStatus = OSPFV3_ENABLED;
        }
        else if (u1Status == NETIPV6_IF_DOWN)
        {
            u1OspfStatus = OSPFV3_DISABLED;
        }

        if (pInterface->operStatus != u1OspfStatus)
        {
            V3IfSetOperStat (pInterface, u1OspfStatus);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OSPFSetIfOperStat \n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : V3OspfIfStatusChgHdlr                               */
/*                                                                         */
/*     Description   : Function for handling interface attribute changes.  */
/*                     Whenever IPv6 senses change in Iface configurations,*/
/*                     it intimates OSPFv3 by calling this function.       */
/*                                                                         */
/*     Input(s)      : pIfStatusChange   -  Pointer to the                 */
/*                     tNetIpv6IfStatChange                                */
/*                     structure, which contains all the interface state   */
/*                     change information.                                 */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
PUBLIC VOID
V3OspfIfStatusChgHdlr (tNetIpv6IfStatChange * pIfStatusChange)
{
    tV3OsInterface     *pInterface = NULL;
    tNetIpv6IfInfo      netIp6IfInfo;
    tNetIpv6AddrInfo    ip6AddrInfo;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    UINT4               u4AddrlessIf = 0;
    MEMSET (&netIp6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfIfStatusChgHdlr\n");

    switch (pIfStatusChange->u4Mask)
    {

        case NETIPV6_INTERFACE_STATUS_CHANGE:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Up-Down Notification\n");

            if (pIfStatusChange->u4IfStat == NETIPV6_IF_DELETE)
            {
                if ((pInterface =
                     V3GetFindIf (pIfStatusChange->u4Index)) != NULL)
                {
                    pInterface->pArea->pV3OspfCxt->u1IfRouteLeakStatus =
                        OSPFV3_TRUE;
                    pInterface->pArea->pV3OspfCxt->u4IfDownIndex =
                        pInterface->u4InterfaceId;
                    /* Send the information to the standby node */
                    O3RedDynSendIntfInfoToStandby (pInterface, OSIX_TRUE);

                    V3IfDelete (pInterface);
                    pInterface->pArea->pV3OspfCxt->u1IfRouteLeakStatus =
                        OSPFV3_FALSE;
                    pInterface->pArea->pV3OspfCxt->u4IfDownIndex = OSPFV3_ZERO;
                    pInterface = NULL;
                }
            }
            else
            {                    /*changes for unnumbered interface */
                V3GetUnnumberedInterface (pIfStatusChange->u4Index,
                                          &netIp6IfInfo);
                pInterface = V3GetFindIf (pIfStatusChange->u4Index);
                if (pInterface != (tV3OsInterface *) NULL)
                {
                    pInterface->pArea->pV3OspfCxt->u1IfRouteLeakStatus =
                        OSPFV3_TRUE;
                    pInterface->pArea->pV3OspfCxt->u4IfDownIndex =
                        pInterface->u4InterfaceId;
                    u4AddrlessIf = netIp6IfInfo.u4AddressLessIf;
                    /*On changing the associated interface of an unnumbered interface */
                    if (((pInterface->u4AddrlessIf) != u4AddrlessIf)
                        && (u4AddrlessIf != 0))
                    {
                        pInterface->u4AddrlessIf = u4AddrlessIf;
                        if (NetIpv6GetFirstIfAddr (u4AddrlessIf, &(ip6AddrInfo))
                            != NETIPV6_SUCCESS)
                        {
                            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                            "Get first interface address failed\n");
                        }

                        TMO_SLL_Scan (&pInterface->ip6AddrLst,
                                      pPrefixNode, tV3OsPrefixNode *)
                        {
                            if (pPrefixNode->prefixInfo.u1PrefixLength ==
                                OSPFV3_MAX_PREFIX_LEN)
                            {
                                TMO_SLL_Delete (&(pInterface->ip6AddrLst),
                                                &(pPrefixNode->nextPrefixNode));

                            }

                        }
                        V3IfPrefixAdd (pInterface, &(ip6AddrInfo.Ip6Addr),
                                       (UINT1) ip6AddrInfo.u4PrefixLength);

                    }
                    /*On un-mapping associated interface on unnumbere interface */
                    else if (((pInterface->u4AddrlessIf) != 0)
                             && (u4AddrlessIf == 0))
                    {
                        pInterface->u4AddrlessIf = u4AddrlessIf;
                        TMO_SLL_Scan (&pInterface->ip6AddrLst,
                                      pPrefixNode, tV3OsPrefixNode *)
                        {
                            if (pPrefixNode->prefixInfo.u1PrefixLength ==
                                OSPFV3_MAX_PREFIX_LEN)
                            {
                                TMO_SLL_Delete (&(pInterface->ip6AddrLst),
                                                &(pPrefixNode->nextPrefixNode));

                            }

                        }
                    }

                }

                V3OSPFSetIfOperStat (pIfStatusChange->u4Index,
                                     (UINT1) pIfStatusChange->u4IfStat);
                if (pInterface != (tV3OsInterface *) NULL)
                {
                    pInterface->pArea->pV3OspfCxt->u1IfRouteLeakStatus =
                        OSPFV3_FALSE;
                    pInterface->pArea->pV3OspfCxt->u4IfDownIndex = OSPFV3_ZERO;
                }

            }
            break;

        case NETIPV6_INTERFACE_MTU_CHANGE:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Iface MTU Chg Notification\n");

            V3IfSetMtuSize (pIfStatusChange->u4Index, pIfStatusChange->u4Mtu);
            break;

        case NETIPV6_INTERFACE_SPEED_CHANGE:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                            "Iface Speed Chg Notification\n");

            if ((pInterface = V3GetFindIf (pIfStatusChange->u4Index)) != NULL)
            {
                pInterface->u4IfMetric =
                    V3IfGetMetricFromSpeedInCxt
                    (pInterface->pArea->pV3OspfCxt,
                     pIfStatusChange->u4IfSpeed, 0);
                V3IfParamChange (pInterface, OSPFV3_UPDATE_METRIC);
            }
            break;

        case NETIPV6_INTERFACE_HIGH_SPEED_CHANGE:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                            "Iface High Speed Chg Notification\n");

            if ((pInterface = V3GetFindIf (pIfStatusChange->u4Index)) != NULL)
            {
                pInterface->u4IfMetric =
                    V3IfGetMetricFromSpeedInCxt
                    (pInterface->pArea->pV3OspfCxt, OSPFV3_MAX_IF_SPEED,
                     pIfStatusChange->u4IfSpeed);
                V3IfParamChange (pInterface, OSPFV3_UPDATE_METRIC);
            }
            break;

        default:
            break;

    }

    if (pInterface != NULL)
    {
        /* Send the information to the standby node
         * In case of interface deletion, indication would have
         * been sent before deletion
         */
        O3RedDynSendIntfInfoToStandby (pInterface, OSIX_FALSE);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfIfStatusChgHdlr\n");
}

#ifdef RAWSOCK_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetPkt                                               */
/*                                                                           */
/* Description  : This procedure gets the pkt from socket and calls          */
/*                V3PppRcvPkt.                                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfGetPkt (INT4 i4RawSockId)
{
    INT4                i4RecvBytes = 0;
    UINT1              *pu1RecvPkt = NULL;
    INT4                i4OptEnable = 1;
    tV3OsInterface     *pInterface = NULL;
    tIp6Addr            srcIp6Addr;
    tIp6Addr            dstIp6Addr;
    struct sockaddr_in6 v6Addr;
    struct in6_pktinfo *pIn6PktInfo = NULL;
    UINT4               u4AddrLen;
    struct msghdr       msg;
    struct iovec        iov[1];
    union controlUnion  unControl;
    struct cmsghdr     *cMsg;
    INT1                flags = 16;
    UINT4               u4IfIndex = 0;
    UINT2               u2OspfPktLen = 0;
    UINT1              *pOspfPkt = NULL;
    UINT4               u4LowPriMessageCount = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfGetPkt\n");

    if ((pu1RecvPkt = V3UtilOsMsgAlloc (OSPFV3_MAX_MTU_SIZE)) == NULL)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC,
                        "Unable to allocate memory for recv pkt\n");
        return;
    }
#ifdef LNXIP6_WANTED
    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_RECVPKTINFO,
         &(i4OptEnable), sizeof (i4OptEnable)) < 0)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
        return;
    }
#else
    if (OSPFV3_SET_SOCK_OPT
        ((i4RawSockId), IPPROTO_IPV6, IPV6_PKTINFO,
         &(i4OptEnable), sizeof (i4OptEnable)) < 0)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
        return;
    }
#endif
    u4AddrLen = sizeof (v6Addr);
    msg.msg_name = &v6Addr;
    msg.msg_namelen = u4AddrLen;
    iov[0].iov_base = (VOID *) pu1RecvPkt;
    iov[0].iov_len = OSPFV3_MAX_MTU_SIZE;
    msg.msg_iov = iov;
    msg.msg_iovlen = 1;
    msg.msg_control = unControl.control;
    msg.msg_controllen = sizeof (unControl.control) + sizeof (struct sockaddr);
    msg.msg_flags = 0;

    OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4LowPriMessageCount);

    i4RecvBytes = recvmsg (i4RawSockId, &msg, flags);
    if (i4RecvBytes > 0)
    {
        if ((msg.msg_controllen < sizeof (struct cmsghdr)) ||
            (msg.msg_flags & MSG_CTRUNC))
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
            return;
        }
        else
        {
            for (cMsg = CMSG_FIRSTHDR (&msg); cMsg != NULL;
                 cMsg = CMSG_NXTHDR (&msg, cMsg))
            {
                if ((cMsg->cmsg_level == IPPROTO_IPV6)
                    && (cMsg->cmsg_type == IPV6_PKTINFO))
                {
                    pIn6PktInfo =
                        (struct in6_pktinfo *) (VOID *) CMSG_DATA (cMsg);
                    u4IfIndex = pIn6PktInfo->ipi6_ifindex;
                    MEMCPY (&dstIp6Addr, &pIn6PktInfo->
                            ipi6_addr, sizeof (struct in6_addr));
                }
            }
        }
    }
    else if ((u4LowPriMessageCount != 0))
    {
        V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
    }

    MEMCPY (&srcIp6Addr, &v6Addr.sin6_addr, sizeof (struct in6_addr));

#ifdef LNXIP6_WANTED
    NetIpv4GetCfaIfIndexFromPort (u4IfIndex, &u4IfIndex);
#endif

    if ((pInterface = V3GetFindIf (u4IfIndex)) == NULL)
    {
        V3UtilOsMsgFree (pu1RecvPkt);
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
        return;
    }

    if (V3UtilIp6AddrComp (&srcIp6Addr, &pInterface->ifIp6Addr) == OSPFV3_EQUAL)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                        "Pkt Disc: dropping self-originated PDU\n ");
        V3UtilOsMsgFree (pu1RecvPkt);
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
        return;
    }

    u2OspfPktLen = i4RecvBytes;
    pOspfPkt = pu1RecvPkt;
    V3PppRcvPkt (pOspfPkt, u2OspfPktLen, u4IfIndex, &srcIp6Addr, &dstIp6Addr);

    if (pu1RecvPkt != NULL)
    {
        V3UtilOsMsgFree (pu1RecvPkt);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetPkt\n");
    return;
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfRcvPkt                                               */
/*                                                                           */
/* Description  : When FutureIPv6 receives an OSPFv3 Packet it calls one     */
/*                callback function, V3IPv6OSPFInterface(). This callback    */
/*                function calls V3OspfRcvPkt.                               */
/*                                                                           */
/* Input        : pChainBuf        - Pointer to the buffer. If Data buffer   */
/*                                     (pLinearBuf) is taken from a          */
/*                                     CRU buffer                            */
/*                                     chain, then the CRU buffer needs to   */
/*                                     be freed completely. Hence the pointer*/
/*                                     of CRU Buffer is passed (This is      */
/*                                     useful in Functional interface with   */
/*                                     FutureSoft IPV6)                      */
/*                pLinearBuf           - Pointer to the buffer               */
/*                u4Length           - Length of the OSPFv3 PDU.             */
/*                u4IfIndex          - Interface Index.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3OspfRcvPkt (tCRU_BUF_CHAIN_HEADER * pChainBuf, UINT1 *pLinearBuf,
              UINT4 u4Length, UINT4 u4IfIndex)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfRcvPkt\n");

    OSPFV3_QMSG_ALLOC (&(pOspfQMsg));
    if (NULL == pOspfQMsg)
    {
        if (pChainBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pChainBuf, OSPFV3_NORMAL_RELEASE);
        }
        if (pLinearBuf != NULL)
        {
            V3UtilOsMsgFree (pLinearBuf);
        }

        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG allocation failed for"
                        "  message received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG allocation failed for"
                      " message received from IPv6"));
        return;
    }

    pOspfQMsg->u4OspfMsgType = OSPFV3_PKT_ARRIVAL_EVENT;
    pOspfQMsg->unOspfMsgType.ospfIpIfParam.u4IfIndex = u4IfIndex;
    pOspfQMsg->unOspfMsgType.ospfIpIfParam.u4PktLen = u4Length;

    if (pChainBuf != NULL)
    {
        pOspfQMsg->unOspfMsgType.ospfIpIfParam.pChainBuf = pChainBuf;
    }
    else
    {
        pOspfQMsg->unOspfMsgType.ospfIpIfParam.pLinearBuf = pLinearBuf;
    }

    /* Enqueue the buffer to the OSPF Task */
    if (OsixQueSend (OSPF3_Q_ID,
                     (UINT1 *) &pOspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for message "
                        "received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for message "
                      "received from IPv6"));
        OSPFV3_QMSG_FREE (pOspfQMsg);
        if (pChainBuf != NULL)
        {
            CRU_BUF_Release_MsgBufChain (pChainBuf, OSPFV3_NORMAL_RELEASE);
        }
        if (pLinearBuf != NULL)
        {
            V3UtilOsMsgFree (pLinearBuf);
        }
        return;
    }

    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_PKT_ARRIVAL_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_PKT_ARRIVAL_EVENT"));
    }
    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Pkt Enqueued To OSPFv3\n");
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfRcvPkt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfDeqPkt                                               */
/*                                                                           */
/* Description  : This function processes the packet received from queue and */
/*                calls V3PppRcvPkt to pass the packet to the respective     */
/*                module.                                                    */
/*                                                                           */
/* Input        : pV3OspfIp6Pkt      -  Pointer to the ospfv3 packet buffer  */
/*                                      structure.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3OspfDeqPkt (tV3OspfIp6Pkt * pV3OspfIp6Pkt)
{

    UINT1              *pPkt = NULL;
    tIp6Addr            srcIp6Addr;
    tIp6Addr            destIp6Addr;
    UINT1              *pOspfPkt = NULL;
    UINT1               u1LinBufFree = OSIX_TRUE;
    UINT2               u2PayLoadLen = 0;
    UINT2               u2TotalLen = 0;
    UINT4               u4Ip6HdrLen = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfDeqPkt\n");

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_INTERFACE_TRC,
                    "OSPFv3 Pkt Rcvd\n");

    /* Convert the CRU BUffer to linear buffer */
    if (pV3OspfIp6Pkt->pChainBuf != NULL)
    {
        /* Get the payload length and copy payload + IPv6 Header */
        OSPFV3_CRU_GET_2_BYTE (pV3OspfIp6Pkt->pChainBuf,
                               IPV6_OFF_PAYLOAD_LEN, u2PayLoadLen);

        u2TotalLen = (UINT2) (u2PayLoadLen + IPV6_HEADER_LEN);

        if ((pPkt = CRU_BUF_Get_DataPtr_IfLinear (pV3OspfIp6Pkt->pChainBuf, 0,
                                                  u2TotalLen)) == NULL)
        {
            if ((pPkt = V3UtilOsMsgAlloc (u2PayLoadLen)) == NULL)
            {
                OSPFV3_GBL_TRC (OS_RESOURCE_TRC, " Alloc Failure\n");
                CRU_BUF_Release_MsgBufChain (pV3OspfIp6Pkt->pChainBuf,
                                             OSPFV3_NORMAL_RELEASE);
                return;
            }
            CRU_BUF_Copy_FromBufChain (pV3OspfIp6Pkt->pChainBuf, pPkt, 0,
                                       pV3OspfIp6Pkt->u4PktLen +
                                       IPV6_HEADER_LEN);
        }
        else
        {
            u1LinBufFree = OSIX_FALSE;
        }
    }
    else
    {
        pPkt = pV3OspfIp6Pkt->pLinearBuf;
    }

    /* Calculate the IPv6 Hdr Length (including extn hdr), source address, 
     * destination header */

    V3ProcessIp6Header (pPkt, &u4Ip6HdrLen, &srcIp6Addr, &destIp6Addr);

    if (u4Ip6HdrLen != 0)
    {
        pOspfPkt = pPkt + u4Ip6HdrLen;
        V3PppRcvPkt (pOspfPkt, (UINT2) pV3OspfIp6Pkt->u4PktLen,
                     pV3OspfIp6Pkt->u4IfIndex, &srcIp6Addr, &destIp6Addr);
    }

    if (u1LinBufFree == OSIX_TRUE)
    {
        V3UtilOsMsgFree (pPkt);
    }

    if (pV3OspfIp6Pkt->pChainBuf != NULL)
    {
        CRU_BUF_Release_MsgBufChain (pV3OspfIp6Pkt->pChainBuf,
                                     OSPFV3_NORMAL_RELEASE);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfDeqPkt \n");

}

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OSPFNLHelloProcessPkt                                      */
/*                                                                           */
/* Description  : This procedure processes the Hello packet received         */
/*                from netfilter queue                                       */
/*                                                                           */
/* Input        : Pointer to nfq_data                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Packet id                                                  */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
V3OSPFNLHelloProcessPkt (struct nfq_data *pNfqData)
{
    INT4                i4Id = 0;
    struct nfqnl_msg_packet_hdr *pHdr;
    UINT1              *pu1Data;
    UINT1              *pPkt = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;
    tIp6Addr            SrcIp6Addr;
    tIp6Addr            DestIp6Addr;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = 0;
    INT4                i4RecvBytes = 0;
    UINT2               u2OspfPktLen = 0;
    UINT1              *pOspfPkt = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OSPFNLHelloProcessPkt\n");

    MEMSET (&SrcIp6Addr, OSPFV3_ZERO, OSPFV3_IPV6_ADDR_LEN);
    MEMSET (&DestIp6Addr, OSPFV3_ZERO, OSPFV3_IPV6_ADDR_LEN);

    pHdr = nfq_get_msg_packet_hdr (pNfqData);

    if (pHdr != NULL)
    {
        i4Id = OSIX_NTOHL (pHdr->packet_id);
    }

    i4RecvBytes = nfq_get_payload (pNfqData, &pu1Data);

    if ((pPkt = V3UtilOsMsgAlloc (OSPFV3_MAX_MTU_SIZE)) == NULL)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC,
                        "Unable to allocate memory for recv pkt\n");
        V3UtilOsMsgFree (pPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }
    if ((pOspfPkt = V3UtilOsMsgAlloc (OSPFV3_MAX_MTU_SIZE)) == NULL)
    {
        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }

    MEMCPY (pPkt, pu1Data, i4RecvBytes);

    u4IfIndex = nfq_get_indev (pNfqData);

    OSPFV3_BUFFER_GET_STRING (pPkt, &SrcIp6Addr,
                              IP6_OFFSET_FOR_SRCADDR_FIELD, IP6_ADDR_SIZE);

    OSPFV3_BUFFER_GET_STRING (pPkt, &DestIp6Addr,
                              IP6_OFFSET_FOR_DESTADDR_FIELD, IP6_ADDR_SIZE);

#ifdef LNXIP6_WANTED
    NetIpv6GetCfaIfIndexFromPort (u4IfIndex, &u4IfIndex);
#endif

    NetIpv6GetCxtId (u4IfIndex, &u4ContextId);

    if ((pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId)) == NULL)
    {
        OSPFV3_TRC (CONTROL_PLANE_TRC,
                    u4ContextId, "Pkt Disc: Invalid Context \n ");
        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }

    if ((pInterface = V3GetFindIf (u4IfIndex)) == NULL)
    {
        OSPFV3_TRC (CONTROL_PLANE_TRC,
                    u4ContextId, "OSPF Pkt Disc Associated If Not Found\n");

        pV3OspfCxt->u4OspfPktsDisd++;
        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }

    if (V3UtilIp6AddrComp (&SrcIp6Addr, &pInterface->ifIp6Addr) == OSPFV3_EQUAL)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                        "Pkt Disc: dropping self-originated PDU\n ");

        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }
    /* If the interface is Passive interface
     * No need to process any OSPF protocol packet
     */
    if (pInterface->bPassive == OSPFV3_TRUE)
    {
        OSPFV3_TRC (CONTROL_PLANE_TRC,
                    u4ContextId,
                    "Pkt Disc: dropping Packets received on passive interfaece \n ");
        pV3OspfCxt->u4OspfPktsDisd++;
        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
        MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);
        return i4Id;
    }

    OSPFV3_COUNTER_OP (pInterface->u4HelloRcvdCount, 1);
    u2OspfPktLen = i4RecvBytes - 40;
    pPkt = pPkt + u2OspfPktLen;

    MEMCPY (pOspfPkt, pPkt, u2OspfPktLen);

    V3PppRcvPkt (pOspfPkt, u2OspfPktLen, u4IfIndex, &SrcIp6Addr, &DestIp6Addr);
    if (pPkt != NULL)
    {
        V3UtilOsMsgFree (pPkt);
        V3UtilOsMsgFree (pOspfPkt);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OSPFNLHelloProcessPkt\n");
    return i4Id;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OSPFNLHelloCallBack                                      */
/*                                                                           */
/* Description  : This procedure is the callback function for the            */
/*                net filter queue for processing hello packets              */
/*                                                                           */
/* Input        : pointers to nfq_q_handle, nfgenmsg, nfq_data and           */
/*                data pointer                                               */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 on error; >= 0 otherwise                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
V3OSPFNLHelloCallBack (struct nfq_q_handle *pQHandle,
                       struct nfgenmsg *nfMsg,
                       struct nfq_data *nfData, void *data)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OSPFNLHelloCallBack\n");
    UNUSED_PARAM (nfMsg);
    UNUSED_PARAM (data);
    UINT4               u4Id = V3OSPFNLHelloProcessPkt (nfData);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OSPFNLHelloCallBack\n");
    return nfq_set_verdict (pQHandle, u4Id, NF_DROP, 0, NULL);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfCreateNLHelloSocket                                    */
/*                                                                           */
/* Description  : This procedure creates the Netlink Hello socket            */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfCreateNLHelloSocket (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTER, "ENTRY : V3OspfCreateNLHelloSocket\n");

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Opening library handle\n");

    gp3Handle = nfq_open ();
    if (gp3Handle == NULL)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "EXIT : error during nfq_open()\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Unbinding existing nf_queue handler for AF_INET6 (if any)\n");
    if (nfq_unbind_pf (gp3Handle, AF_INET6) < 0)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "Error during nfq_unbind_pf()\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Binding nfnetlink_queue as nf_queue handler for AF_INET6\n");
    if (nfq_bind_pf (gp3Handle, AF_INET6) < 0)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "Error during nfq_bind_pf()\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Binding this socket to queue '1'\n");
    gpQ3Handle =
        nfq_create_queue (gp3Handle, OSPF3_NF_QUEUE, &V3OSPFNLHelloCallBack,
                          NULL);
    if (gpQ3Handle == NULL)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "Error during nfq_create_queue()\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Setting copy_packet mode\n");
    if (nfq_set_mode (gpQ3Handle, NFQNL_COPY_PACKET, 0xffff) < 0)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "Can't set packet_copy mode\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    gV3OsRtr.i4NLHelloSockId = nfq_fd (gp3Handle);

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gV3OsRtr.i4NLHelloSockId, V3OspfPacketOnNLHelloSocket) !=
        OSIX_SUCCESS)
    {
        nfq_close (gp3Handle);
        gV3OsRtr.i4NLHelloSockId = -1;
        return OSPFV3_FAILURE;
    }

    if (V3OspfSetNLHelloSockOptions () == OSPFV3_FAILURE)
    {
        /* Socket has to be closed */
        V3OspfCloseNLHelloSocket ();
        return OSPFV3_FAILURE;
    }

    if (gpQ3Handle == NULL)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "Error during nfq_create_queue() \n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "Setting copy_packet mode\n");
    if (nfq_set_mode (gpQ3Handle, NFQNL_COPY_PACKET, 0xffff) < 0)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_FN_EXIT, "\n Can't set packet_copy mode\n",
                         OSPFV3_INVALID_CXT_ID);
        return OSPFV3_FAILURE;
    }

    gV3OsRtr.i4NLHelloSockId = nfq_fd (gp3Handle);

    /* Add the Socket Descriptor to Select utility for Packet Reception */
    if (SelAddFd (gV3OsRtr.i4NLHelloSockId, V3OspfPacketOnNLHelloSocket) !=
        OSIX_SUCCESS)
    {
        nfq_close (gp3Handle);
        gV3OsRtr.i4NLHelloSockId = -1;
        return OSPFV3_FAILURE;
    }

    if (V3OspfSetNLHelloSockOptions () == OSPFV3_FAILURE)
    {
        /* Socket has to be closed */
        V3OspfCloseNLHelloSocket ();
        return OSPFV3_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfCreateNLHelloSocket\n");
    return OSPFV3_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Ip6ifGetNLHelloPkt                                    */
/*                                                                           */
/* Description  : This procedure gets the pkt from socket and calls          */
/*                nfq_handle_packet which processes the packet received.     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
Ip6ifGetNLHelloPkt (VOID)
{
    INT4                i4RecvBytes = 0;
    char               *pRecvPkt = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : Ip6ifGetNLHelloPkt\n");
    MEMSET (gau1RxPktBuf, OSPFV3_ZERO, OSPFV3_MAX_MTU_SIZE);

    pRecvPkt = (char *) gau1RxPktBuf;
    while ((i4RecvBytes =
            recv (gV3OsRtr.i4NLHelloSockId, pRecvPkt, OSPFV3_MAX_MTU_SIZE,
                  0)) > 0)
    {
        nfq_handle_packet (gp3Handle, pRecvPkt, i4RecvBytes);
        continue;
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, OSPFV3_INVALID_CXT_ID,
                "closing library handle\n");

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : Ip6ifGetNLHelloPkt\n");
}

#endif
#endif

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfSetNLHelloSockOptions                                  */
/*                                                                           */
/* Description  : Function to set socket options                             */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSPF_SUCCESS                                               */
/*                OSPF_FAILURE                                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfSetNLHelloSockOptions (VOID)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSetNLHelloSockOptions\n");
    if ((OSPFV3_FCNTL ((gV3OsRtr.i4NLHelloSockId), F_SETFL, O_NONBLOCK)) < 0)
    {
        return OSPFV3_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSetNLHelloSockOptions\n");
    return OSPFV3_SUCCESS;
}
#endif
#endif

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : OspfCloseNLHelloSocket                                     */
/*                                                                           */
/* Description  : Close NL Hello Socket                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfCloseNLHelloSocket (VOID)
{
    /* Remove the Socket Descriptor added to Select utility 
     * for Packet Reception */

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfCloseNLHelloSocket\n");
    SelRemoveFd (gV3OsRtr.i4NLHelloSockId);

    nfq_close (gp3Handle);
    gV3OsRtr.i4NLHelloSockId = -1;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfCloseNLHelloSocket\n");

}
#endif
#endif
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP4_WANTED
/*****************************************************************************/
/* Function     : OspfPacketOnNLHelloSocket                                  */
/*                                                                           */
/* Description  : Indicates OSPF Task about Packet received on the socket    */
/*                                                                           */
/* Input        : OSPF Socket Descriptor                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
V3OspfPacketOnNLHelloSocket (INT4 i4SockFd)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfPacketOnNLHelloSocket\n");
    UNUSED_PARAM (i4SockFd);

    /* Notify OSPF about Packet Reception */
    OsixEvtSend (OSPF3_TASK_ID, OSPFV3_NLH_PKT_ARRIVAL_EVENT);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfPacketOnNLHelloSocket\n");

}

#endif
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : V3ProcessIp6Header                                         */
/*                                                                           */
/* Description  : This function process the IPv6 Header and returns the      */
/*                length, source and destination of the IPv6 header          */
/*                                                                           */
/* Input        : pPkt           - Pointer to the IPv6 packet received       */
/*                                                                           */
/* Output       : u4Ip6HdrLen    - Pointer to length                         */
/*                pSrcIp6Addr    - Pointer to the source address             */
/*                pDestIp6Addr   - Pointer to the destination address        */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3ProcessIp6Header (UINT1 *pPkt, UINT4 *u4Ip6HdrLen,
                    tIp6Addr * pSrcIp6Addr, tIp6Addr * pDestIp6Addr)
{
    tIp6Hdr            *pIp6 = NULL;
    UINT1               u1NextHdr = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3ProcessIp6Header\n");

    OSPFV3_BUFFER_GET_STRING (pPkt, pSrcIp6Addr,
                              IP6_OFFSET_FOR_SRCADDR_FIELD, IP6_ADDR_SIZE);

    OSPFV3_BUFFER_GET_STRING (pPkt, pDestIp6Addr,
                              IP6_OFFSET_FOR_DESTADDR_FIELD, IP6_ADDR_SIZE);

    pIp6 = (tIp6Hdr *) (VOID *) pPkt;
    u1NextHdr = pIp6->u1Nh;
    UNUSED_PARAM (u1NextHdr);
    *u4Ip6HdrLen = IPV6_HEADER_LEN;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3ProcessIp6Header\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOsMsgAlloc                                           */
/*                                                                           */
/* Description  : This procedure allocates memory for the size               */
/*                u4Size + size of the IPv6 header                           */
/*                                                                           */
/* Input        : u4Size   : size of the buffer required.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the UINT1 in case of successful                 */
/*                allocation.                                                */
/*                NULL, otherwise.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC UINT1       *
V3UtilOsMsgAlloc (UINT4 u4Size)
{
    UINT1              *pMsg = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3UtilOsMsgAlloc\n");

    if ((u4Size + IPV6_HEADER_LEN) > OSPFV3_MAX_MSG_SIZE)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory\n"));

        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "Alloc Failure\r\n");
        return NULL;
    }

    OSPFV3_OSMSG_ALLOC (&(pMsg));
    if (pMsg != NULL)
    {
        MEMSET (pMsg, 0, OSPFV3_MAX_MTU_SIZE);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3UtilOsMsgAlloc\n");
    return pMsg;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilOsMsgFree                                            */
/*                                                                           */
/* Description  : This procedure releases memory                             */
/*                                                                           */
/* Input        : msg                : pointer to the UINT1                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3UtilOsMsgFree (UINT1 *msg)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3UtilOsMsgFree\n");
    OSPFV3_OSMSG_FREE (msg);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3UtilOsMsgFree\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfIfAddrChg                                            */
/*                                                                           */
/* Description  : Whenever IPv6 senses changes in Iface address              */
/*                configurations, it calls the callback function,            */
/*                V3IPv6OSPFInterface() which in turn calls V3OspfIfAddrChg. */
/*                                                                           */
/* Input        : pChainBuf        - Pointer to the buffer. If Data buffer   */
/*                                     (pLinearBuf) is taken from a          */
/*                                      CRU buffer                           */
/*                                     chain, then the CRU buffer needs to   */
/*                                     be freed completely. Hence the pointer*/
/*                                     of CRU Buffer is passed (This is      */
/*                                     useful in Functional interface with   */
/*                                     FutureSoft IPV6)                      */
/*                pLinearBuf           - Pointer to the buffer               */
/*                u4Length           - Length of the OSPFv3 PDU.             */
/*                u4IfIndex          - Interface Index.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3OspfIfAddrChg (tNetIpv6AddrChange * pAddrChange)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfIfAddrChg\n");

    OSPFV3_QMSG_ALLOC (&(pOspfQMsg));
    if (NULL == pOspfQMsg)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG allocation failed for interface IP address change "
                        "notification received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG allocation failed for interface IP address change "
                      "notification received from IPv6"));
        return;
    }

    pOspfQMsg->u4OspfMsgType = OSPFV3_IPV6_IF_ADDR_CHG_EVENT;
    pOspfQMsg->unOspfMsgType.ospfIpIfAddrParam.Ipv6AddrInfo
        = pAddrChange->Ipv6AddrInfo;
    pOspfQMsg->unOspfMsgType.ospfIpIfAddrParam.u4Index = pAddrChange->u4Index;
    pOspfQMsg->unOspfMsgType.ospfIpIfAddrParam.u4Mask = pAddrChange->u4Mask;

    /* Enqueue the buffer to the OSPF Task */
    if (OsixQueSend
        (OSPF3_Q_ID, (UINT1 *) &pOspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {

        OSPFV3_QMSG_FREE (pOspfQMsg);
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for interface IP address "
                        "change notification received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for interface IP address "
                      "change notification received from IPv6"));
        return;
    }

    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_IPV6_IF_ADDR_CHG_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_IPV6_IF_ADDR_CHG_EVENT"));
    }
    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Msg Enqueued To OSPFv3\n");
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfIfAddrChg\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfIfStatChg                                            */
/*                                                                           */
/* Description  : Whenever IPv6 senses changes in Iface address              */
/*                configurations, it calls the callback function,            */
/*                V3IPv6OSPFInterface() which in turn calls V3OspfIfStatChg. */
/*                                                                           */
/* Input        : pChainBuf        - Pointer to the buffer. If Data buffer   */
/*                                     (pLinearBuf) is taken from a          */
/*                                     CRU buffer                            */
/*                                     chain, then the CRU buffer needs to   */
/*                                     be freed completely. Hence the pointer*/
/*                                     of CRU Buffer is passed (This is      */
/*                                     useful in Functional interface with   */
/*                                     FutureSoft IPV6)                      */
/*                pLinearBuf           - Pointer to the buffer               */
/*                u4Length           - Length of the OSPFv3 PDU.             */
/*                u4IfIndex          - Interface Index.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfIfStatChg (tNetIpv6IfStatChange * pIfStatusChange)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfIfStatChg\n");

    OSPFV3_QMSG_ALLOC (&(pOspfQMsg));
    if (NULL == pOspfQMsg)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG allocation failed for interface state change "
                        "notification received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG allocation failed for interface state change "
                      "notification received from IPv6"));
        return;
    }

    pOspfQMsg->u4OspfMsgType = OSPFV3_IPV6_IF_STAT_CHG_EVENT;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Index
        = pIfStatusChange->u4Index;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mtu = pIfStatusChange->u4Mtu;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfSpeed
        = pIfStatusChange->u4IfSpeed;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfStat
        = pIfStatusChange->u4IfStat;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4OperStatus
        = pIfStatusChange->u4OperStatus;
    pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mask = pIfStatusChange->u4Mask;

    /* Enqueue the buffer to the OSPF Task */
    if (OsixQueSend
        (OSPF3_Q_ID, (UINT1 *) &pOspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {

        OSPFV3_QMSG_FREE (pOspfQMsg);
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for interface state change "
                        "notification received from IPv6\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for interface state change"
                      "notification received from IPv6"));
        return;
    }

    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_IPV6_IF_STAT_CHG_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_IPV6_IF_STAT_CHG_EVENT"));
    }
    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Msg Enqueued To OSPFv3\n");
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3OspfIfStatChg\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetAliasName                                         */
/*                                                                           */
/* Description  : This function will return the alias name of the            */
/*                given context.                                             */
/*                                                                           */
/* Input        : u4ContextId    - Context-Id.                               */
/*                                                                           */
/* Output       : pu1Alias       - Switch Alias Name.                        */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfGetAliasName\n");

    if (V3OspfGetSystemMode (OSPF_PROTOCOL_ID) == OSPFV3_MI_MODE)
    {
        if (VcmGetAliasName (u4ContextId, pu1Alias) == VCM_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetAliasName\n");
            return OSIX_SUCCESS;
        }
    }
    else
    {
        if (u4ContextId == OSPFV3_DEFAULT_CXT_ID)
        {
            STRCPY (pu1Alias, "default");
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetAliasName\n");
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfVcmIsVcExist                                         */
/*                                                                           */
/* Description  : This function check whether the given context exist or not */
/*                                                                           */
/* Input        : u4ContextId    - Context-Id.                               */
/*                                                                           */
/* Output       : none.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfVcmIsVcExist (UINT4 u4OspfCxtId)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfVcmIsVcExist\n");

    if (V3OspfGetSystemMode (OSPF3_PROTOCOL_ID) == OSPFV3_MI_MODE)
    {
        if (VcmIsL3VcExist (u4OspfCxtId) == VCM_TRUE)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfVcmIsVcExist\n");
            return OSIX_SUCCESS;
        }
    }
    else
    {
        if (u4OspfCxtId == OSPFV3_DEFAULT_CXT_ID)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfVcmIsVcExist\n");
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfIsVcmSwitchExist                                     */
/*                                                                           */
/* Description  : This function check whether the entry is present for       */
/*                the correponding Switch-name in VCM. if yes it will return */
/*                the Context-Id of the Switch.                              */
/*                                                                           */
/* Input        : pu1Alias   - Name of the Switch.                           */
/*                                                                           */
/* Output       : pu4VcNum   - Context-Id of the switch.                     */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfIsVcmSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfIsVcmSwitchExist\n");

    if (V3OspfGetSystemMode (OSPF_PROTOCOL_ID) == OSPFV3_MI_MODE)
    {
        if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_TRUE)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfIsVcmSwitchExist\n");
            return OSIX_SUCCESS;
        }
    }
    else
    {
        if (STRCMP (pu1Alias, "default") == 0)
        {
            *pu4VcNum = OSPFV3_DEFAULT_CXT_ID;
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfIsVcmSwitchExist\n");
            return OSIX_SUCCESS;
        }
    }

    *pu4VcNum = OSPFV3_INVALID_CXT_ID;
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetSystemMode                                        */
/*                                                                           */
/* Description  : This function calls the VCM Module to get the              */
/*                mode of the system (SI / MI).                              */
/*                                                                           */
/* Input        : u2ProtocolId - Protocol Identifier                         */
/*                                                                           */
/* Output       : pu1Alias       - Switch Alias Name.                        */
/*                                                                           */
/* Returns      : OSPFV3_MI_MODE / OSPFV3_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetSystemModeExt                                        */
/*                                                                           */
/* Description  : This function calls the VCM Module to get the              */
/*                mode of the system (SI / MI).                              */
/*                                                                           */
/* Input        : u2ProtocolId - Protocol Identifier                         */
/*                                                                           */
/* Output       : pu1Alias       - Switch Alias Name.                        */
/*                                                                           */
/* Returns      : OSPFV3_MI_MODE / OSPFV3_SI_MODE                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfRegisterWithVcm                                      */
/*                                                                           */
/* Description  : This function de-registers the context with  the           */
/*                VCM module                                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfRegisterWithVcm (VOID)
{
    tVcmRegInfo         VcmRegInfo;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfRegisterWithVcm\n");

    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    VcmRegInfo.pIfMapChngAndCxtChng = V3OspfVcmCallback;
    VcmRegInfo.u1InfoMask |= (VCM_IF_MAP_CHG_REQ | VCM_CXT_STATUS_CHG_REQ);
    VcmRegInfo.u1ProtoId = OSPF3_PROTOCOL_ID;

    if (VcmRegisterHLProtocol (&VcmRegInfo) == VCM_FAILURE)
    {
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfRegisterWithVcm\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfDeRegisterWithVcm                                    */
/*                                                                           */
/* Description  : This function registers the context with  the              */
/*                VCM module                                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfDeRegisterWithVcm (VOID)
{
    VcmDeRegisterHLProtocol (OSPF3_PROTOCOL_ID);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfVcmCallback                                          */
/*                                                                           */
/* Description  : This function is called from VCM module during context     */
/*                deletion                                                   */
/*                                                                           */
/* Input        : u4IfIndex   - Ip Interface Index                           */
/*                u4ContextId - Context Id                                   */
/*                u1BitMap    - Bit Map to identify the change               */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfVcmCallback (UINT4 u4IpIfIndex, UINT4 u4ContextId, UINT1 u1BitMap)
{
    tV3OspfQMsg        *pV3OspfQMsg = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfVcmCallback\n");

    OSPFV3_QMSG_ALLOC (&(pV3OspfQMsg));

    if (pV3OspfQMsg != NULL)
    {
        MEMSET (pV3OspfQMsg, 0, sizeof (tV3OspfQMsg));
        pV3OspfQMsg->u4OspfMsgType = OSPFV3_VCM_CHG_EVENT;
        pV3OspfQMsg->unOspfMsgType.ospfVcmInfo.u4IpIfIndex = u4IpIfIndex;
        pV3OspfQMsg->unOspfMsgType.ospfVcmInfo.u4VcmCxtId = u4ContextId;
        pV3OspfQMsg->unOspfMsgType.ospfVcmInfo.u1BitMap = u1BitMap;
    }

    else
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG allocation failed for"
                        " context deletion event received from VCM\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG allocation failed for"
                      " context deletion event received from VCM"));
    }
    if ((pV3OspfQMsg != NULL) &&
        (OsixQueSend (OSPF3_Q_ID,
                      (UINT1 *) &pV3OspfQMsg, OSIX_DEF_MSG_LEN)
         != OSIX_SUCCESS))
    {
        OSPFV3_QMSG_FREE (pV3OspfQMsg);
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for"
                        " context deletion event received from VCM\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for"
                      " context deletion event received from VCM"));
        pV3OspfQMsg = NULL;
    }

    if ((pV3OspfQMsg != NULL) &&
        (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS))
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_VCM_CHG_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_VCM_CHG_EVENT"));
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfVcmCallback\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetCxtIdFromCfaIndex                                 */
/*                                                                           */
/* Description  : This function gets the context id associated with the      */
/*                the given CFA Interface Index                              */
/*                                                                           */
/* Input        : u4IfIndex   - CFA If index                                 */
/*                                                                           */
/* Output       : *pu4CxtId   - Context id of the interface having           */
/*                              port number as u4IfIndex                     */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfGetCxtIdFromCfaIndex (UINT4 u4IfIndex, UINT4 *pu4CxtId)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfGetCxtIdFromCfaIndex\n");

    if (V3OspfGetSystemModeExt (OSPF_PROTOCOL_ID) == OSPFV3_SI_MODE)
    {
        *pu4CxtId = OSPFV3_DEFAULT_CXT_ID;
    }
    else
    {
        if (VcmGetContextIdFromCfaIfIndex (u4IfIndex, pu4CxtId) == VCM_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetCxtIdFromCfaIndex\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfGetMinFilterRMap                                     */
/*                                                                           */
/* Description  : This function calls the RMAP API to get the minimum        */
/*                filter                                                     */
/*                                                                           */
/* Input        : pFilterRMap1  -   pointer to first RMAP filter             */
/*                i1Type1       -   First RMAP type                          */
/*                pFilterRMap2  -   pointer to second RMAP filter            */
/*                i1Type2       -   Second RMAP type                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to RMAP filter or NULL                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tFilteringRMap *
V3OspfGetMinFilterRMap (tFilteringRMap * pFilterRMap1, INT1 i1Type1,
                        tFilteringRMap * pFilterRMap2, INT1 i1Type2)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfGetMinFilterRMap\n");

#ifdef ROUTEMAP_WANTED
    INT4                i4RetVal = 0;
    if (pFilterRMap1 == NULL && pFilterRMap2 != NULL)
    {
        return pFilterRMap2;
    }

    if (pFilterRMap1 != NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (pFilterRMap1 == NULL && pFilterRMap2 == NULL)
    {
        return pFilterRMap1;
    }

    if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) >
        STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap2;
    }
    else if (STRLEN (pFilterRMap1->au1DistInOutFilterRMapName) <
             STRLEN (pFilterRMap2->au1DistInOutFilterRMapName))
    {
        return pFilterRMap1;
    }
    else
    {
        i4RetVal = MEMCMP (pFilterRMap1->au1DistInOutFilterRMapName,
                           pFilterRMap2->au1DistInOutFilterRMapName,
                           STRLEN (pFilterRMap1->au1DistInOutFilterRMapName));
        if (i4RetVal < 0)
        {
            return pFilterRMap1;
        }
        else if (i4RetVal > 0)
        {
            return pFilterRMap2;
        }
    }

    if (i1Type1 > i1Type2)
    {
        return pFilterRMap2;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetMinFilterRMap\n");

    return pFilterRMap1;
#else
    UNUSED_PARAM (pFilterRMap1);
    UNUSED_PARAM (pFilterRMap2);
    UNUSED_PARAM (i1Type1);
    UNUSED_PARAM (i1Type2);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfGetMinFilterRMap\n");

    return NULL;
#endif
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfCmpFilterRMapName                                    */
/*                                                                           */
/* Description  : This function calls the RMAP API to get the filter status  */
/*                                                                           */
/* Input        : pFilterRMap   -   pointer to first RMAP filter             */
/*                pRMapName     -   RMAP name                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Comparison result                                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfCmpFilterRMapName (tFilteringRMap * pFilterRMap,
                         tSNMP_OCTET_STRING_TYPE * pRMapName)
{
    INT4                i1RetVal = 1;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfCmpFilterRMapName\n");

#ifdef ROUTEMAP_WANTED
    if (pFilterRMap != NULL
        && pRMapName != NULL
        && (UINT4) pRMapName->i4_Length ==
        STRLEN (pFilterRMap->au1DistInOutFilterRMapName))
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfCmpFilterRMapName\n");
        return (INT4) (STRNCMP (pFilterRMap->au1DistInOutFilterRMapName,
                                pRMapName->pu1_OctetList,
                                pRMapName->i4_Length));
    }
#else
    UNUSED_PARAM (pFilterRMap);
    UNUSED_PARAM (pRMapName);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfCmpFilterRMapName\n");
    return i1RetVal;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRegisterWithRM                                      */
/*                                                                          */
/* Description  : This  function calls RM api to register with the RM       */
/*                module                                                    */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfRegisterWithRM\n");

    MEMSET (&RmRegParams, 0, sizeof (tRmRegParams));
    RmRegParams.u4EntId = RM_OSPFV3_APP_ID;
    RmRegParams.pFnRcvPkt = V3OspfRcvPktFromRM;

#ifdef RM_WANTED
    if (RmRegisterProtocols (&RmRegParams) != RM_SUCCESS)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "OSPFv3 Registration to RM module " "failed\n"));

        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "OSPFv3 Registration to RM module "
                        "failed\n");
        return OSIX_FAILURE;
    }

    gV3OsRtr.ospfRedInfo.i4HsAdminStatus = OSPFV3_ENABLED;
#else
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_DOWN;
    O3RedUtlResetHsVariables ();
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfRegisterWithRM\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfDeRegisterWithRM                                    */
/*                                                                          */
/* Description  : This  function calls RM api to de-register from the RM    */
/*                module                                                    */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfDeRegisterWithRM (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfDeRegisterWithRM\n");

#ifdef RM_WANTED
    if (RmDeRegisterProtocols (RM_OSPFV3_APP_ID) != RM_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "OSPFv3 De-Registration to RM module failed\n");
        return OSIX_FAILURE;
    }
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfDeRegisterWithRM\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfEnqMsgToRm                                          */
/*                                                                          */
/* Description  : This  function is called by the protocol to send messages */
/*                to RM module                                              */
/*                                                                          */
/* Input        : pRmMsg     -     Message to be sent to RM module          */
/*                u2Len      -     Message Length                           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2Len)
{
#ifdef RM_WANTED
    UINT4               u4SrcEntId = (UINT4) RM_OSPFV3_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_OSPFV3_APP_ID;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfEnqMsgToRm\n");

    if (RmEnqMsgToRmFromAppl (pRmMsg, u2Len, u4SrcEntId, u4DestEntId)
        == RM_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to send message to RM\n"));

        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "Failed to send message to RM\n");
        RM_FREE (pRmMsg);
        gu4V3OspfEnqMsgToRmFail++;
        return OSIX_FAILURE;
    }
#else

    if (pRmMsg != NULL)
    {
        RM_FREE (pRmMsg);
    }

    UNUSED_PARAM (u2Len);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfEnqMsgToRm\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRmGetNodeState                                      */
/*                                                                          */
/* Description  : This  function is called by the protocol to get the       */
/*                current state in the RM module                            */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : RM node state                                             */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfRmGetNodeState (VOID)
{
    INT4                i4RmState = RM_ACTIVE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfRmGetNodeState\n");
#ifdef RM_WANTED
    i4RmState = RmRetrieveNodeState ();
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfRmGetNodeState\n");

    return i4RmState;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfSendEventToRm                                       */
/*                                                                          */
/* Description  : This  function is called by the protocol to send an       */
/*                event to RM module                                        */
/*                                                                          */
/* Input        : pEvt     -     Pointer to tRmProtoEvt structure           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfSendEventToRm (tRmProtoEvt * pEvt)
{
    /* The following are the event sent by the protocols to RM module
     * RM_PROTOCOL_BULK_UPDT_COMPLETION
     * RM_INITIATE_BULK_UPDATE
     * RM_BULK_UPDT_ABORT
     * RM_STANDBY_EVT_PROCESSED
     * RM_IDLE_TO_ACTIVE_EVT_PROCESSED
     * RM_STANDBY_TO_ACTIVE_EVT_PROCESSED
     * RM_PROTOCOL_SEND_EVENT
     * The following are the error message for bulk update abort
     * RM_MEMALLOC_FAIL
     * RM_SENDTO_FAIL
     * RM_PROCESS_FAIL
     * RM_NONE
     */

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSendEventToRm\n");
    pEvt->u4AppId = RM_OSPFV3_APP_ID;
#ifdef RM_WANTED
    if (RmApiHandleProtocolEvent (pEvt) == RM_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to send event %d to RM\n", pEvt->u4Event));

        OSPFV3_GBL_TRC1 (OSPFV3_CRITICAL_TRC,
                         "Failed to send event %d to RM\n", pEvt->u4Event);
        gu4V3OspfSendEventToRmFail++;
        return OSIX_FAILURE;
    }
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendEventToRm\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRmSetBulkUpdateStatus                               */
/*                                                                          */
/* Description  : This  function is called by the protocol to set the bulk  */
/*                update status to indicate the bulk update completion      */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
V3OspfRmSetBulkUpdateStatus (VOID)
{
#ifdef RM_WANTED
    RmSetBulkUpdatesStatus (RM_OSPFV3_APP_ID);
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRmReleaseMemoryForMsg                               */
/*                                                                          */
/* Description  : This function calls the RM API to release the memory      */
/*                allocated for the RM message by the RM module to the      */
/*                memory pool                                               */
/*                                                                          */
/* Input        : pu1RmMsg     -    Pointer to th RM message received       */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : RM_SUCCESS / RM_FAILURE                                   */
/*                                                                          */
/****************************************************************************/

PUBLIC UINT4
V3OspfRmReleaseMemoryForMsg (UINT1 *pu1RmMsg)
{
    UINT4               u4RetVal = RM_SUCCESS;

#ifdef RM_WANTED
    u4RetVal = RmReleaseMemoryForMsg (pu1RmMsg);
#else
    UNUSED_PARAM (pu1RmMsg);
#endif

    return u4RetVal;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRcvPktFromRM                                        */
/*                                                                          */
/* Description  : This function is the call back function registered with   */
/*                the RM module. RM calls this function to send event or    */
/*                messages to OSPFv3                                        */
/*                                                                          */
/* Input        : u1Event      -    Event type given by RM module           */
/*                pRmMsg       -    Pointer to th RM message received       */
/*                u2DataLen    -    Data length                             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
V3OspfRcvPktFromRM (UINT1 u1Event, tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tV3OspfQMsg        *pV3OspfQMsg = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfRcvPktFromRM\n");

    /* Events other than the necessary event are ignored */
    if ((u1Event != RM_MESSAGE) && (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) && (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) && (u1Event != L2_INITIATE_BULK_UPDATES)
        && (u1Event != RM_CONFIG_RESTORE_COMPLETE)
        && (u1Event != RM_INIT_HW_AUDIT) && (u1Event != RM_DYNAMIC_SYNCH_AUDIT))
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                        "V3OspfRcvPktFromRM : Invalid event received\r\n");
        return OSIX_FAILURE;
    }

    /* pRmMsg should not be NULL for some event */
    if ((pRmMsg == NULL) &&
        ((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)))
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "pRmMsg is NULL for event %d\r\n", u1Event));

        OSPFV3_GBL_TRC1 (CONTROL_PLANE_TRC | OSPFV3_CRITICAL_TRC,
                         "pRmMsg is NULL for event %d\r\n", u1Event);
        return OSIX_FAILURE;
    }

    /* Allocate the message for the queue structure and post an event */
    OSPFV3_RM_QMSG_ALLOC (&(pV3OspfQMsg));

    if (pV3OspfQMsg == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG allocation failed for"
                        " message received from RM\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG allocation failed for"
                      " message received from RM"));
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pRmMsg);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            V3OspfRmReleaseMemoryForMsg ((UINT1 *) pRmMsg);
        }

        return OSIX_FAILURE;
    }

    /* Send to the Queue */
    MEMSET (pV3OspfQMsg, 0, sizeof (tV3OspfQMsg));
    pV3OspfQMsg->u4OspfMsgType = OSPFV3_RM_MSG_EVENT;
    pV3OspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.pData = pRmMsg;
    pV3OspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.u2DataLen = u2DataLen;
    pV3OspfQMsg->unOspfMsgType.ospfv3RmCtrlMsg.u1Event = u1Event;

    if (OsixQueSend (OSPF3_RM_Q_ID,
                     (UINT1 *) &pV3OspfQMsg, OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for"
                        " message received from RM\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for"
                      " message received from RM"));
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pRmMsg);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            V3OspfRmReleaseMemoryForMsg ((UINT1 *) pRmMsg);
        }

        OSPFV3_QMSG_FREE (pV3OspfQMsg);
        pV3OspfQMsg = NULL;
        return OSIX_FAILURE;
    }

    /* Send an event to OSPFv3 task */
    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_RMQ_EVENT) == OSIX_FAILURE)
    {
        OSPFV3_QMSG_FREE (pV3OspfQMsg);
        pV3OspfQMsg = NULL;
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_RM_MSG_EVENT \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for  OSPFV3_RM_MSG_EVENT"));
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfRcvPktFromRM\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfRmGetStandbyNodeCount                               */
/*                                                                          */
/* Description  : This function calls the RM API to get the current no      */
/*                standby peers                                             */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : No of peers attached to this active node                  */
/*                                                                          */
/****************************************************************************/

PUBLIC UINT1
V3OspfRmGetStandbyNodeCount (VOID)
{
    UINT1               u1StandbyCount = 0;

#ifdef RM_WANTED
    u1StandbyCount = RmGetStandbyNodeCount ();
#endif

    return u1StandbyCount;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfSendProtoAckToRM                                    */
/*                                                                          */
/* Description  : This function sends ack to RM module                      */
/*                                                                          */
/* Input        : u4SeqNum  -  Seq num for which ack needs to be sent       */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
V3OspfSendProtoAckToRM (UINT4 u4SeqNum)
{
    tRmProtoAck         ProtoAck;

    MEMSET (&ProtoAck, OSPFV3_ZERO, sizeof (ProtoAck));
    ProtoAck.u4AppId = RM_OSPFV3_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;

#ifdef RM_WANTED
    RmApiSendProtoAckToRM (&ProtoAck);
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfApiModuleStart                                      */
/*                                                                          */
/* Description  : This function call starts the ospfv3 module               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC UINT4
V3OspfApiModuleStart (VOID)
{
    UINT4               u4RetVal = OSIX_SUCCESS;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfApiModuleStart\n");
    V3OspfLock ();

    gV3OsRtr.ospfRedInfo.u4PrevRmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_INIT;

    if (V3OspfRegisterWithRM () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC, "RM Registration failed\n");
        u4RetVal = OSIX_FAILURE;
    }

    /* Register with VCM module to handle the context deletion indication */
    if ((u4RetVal == OSIX_SUCCESS) &&
        (V3OspfRegisterWithVcm () == OSIX_FAILURE))
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "VCM registration failed\r\n");
        u4RetVal = OSIX_FAILURE;
    }

    /* Create the default context */
    if ((u4RetVal == OSIX_SUCCESS) &&
        (V3RtrCreateCxt (OSPFV3_DEFAULT_CXT_ID) == OSIX_FAILURE))
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Default context creation failed\n");
        u4RetVal = OSIX_FAILURE;
    }

    V3OspfUnLock ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfApiModuleStart\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfApiModuleShutDown                                   */
/*                                                                          */
/* Description  : This function call starts the ospfv3 module               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
V3OspfApiModuleShutDown (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OspfRtmNode     *pRtmNode = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    UINT4               u4ContextId = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfApiModuleShutDown\n");

    V3OspfLock ();

    for ((u4ContextId = (OSPFV3_DEFAULT_CXT_ID + 1));
         u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT; u4ContextId++)
    {
        pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4ContextId];

        if (pV3OspfCxt != NULL)
        {
            V3RtrDeleteCxt (pV3OspfCxt);
        }
    }

    /* Delete the default context */
    if (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID] != NULL)
    {
        V3RtrDeleteCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]);
    }

    /* Delete all the messages from the RTM */
    OsixSemTake (OSPF3_RTM_LST_SEM_ID);

    while ((pNode = TMO_SLL_Get (&(gV3OsRtr.rtmRoutesLst))) != NULL)
    {
        pRtmNode = OSPFV3_GET_BASE_PTR (tV3OspfRtmNode, nextRtmNode, pNode);
        OSPFV3_RTM_ROUTES_FREE (pRtmNode);
    }

    OsixSemGive (OSPF3_RTM_LST_SEM_ID);

    /* Delete all the messages in the queue */
    V3UtilDeleteQueueMsg ();

    /* De-register from the RM module */
    V3OspfDeRegisterWithRM ();

    /* De-register from VCM module */
    V3OspfDeRegisterWithVcm ();

    /* Delete the VRF SPF timer */
    V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
    gV3OsRtr.ospfRedInfo.u4PrevRmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_INIT;

    gV3OsRtr.ospfRedInfo.i4HsAdminStatus = OSPFV3_DISABLED;

    V3OspfUnLock ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfApiModuleShutDown\n");

}

/****************************************************************************/
/*                                                                          */
/* Function     : V3OspfSendMultIfOnLink                                    */
/*                                                                          */
/* Description  : This function call starts the Ipv6 module  to intimate    */
/*                the Addition or deletion of Multiple Interfaces over the  */
/*                link                                                      */
/*                                                                          */
/* Input        : pInterface  - Pointer to the Active Interface of the link */
/*                u1Operation -IPV6_ACTIVE_ADD -  Add the multiple infs     */
/*                            -IPV6_ACTIVE_DELETE - Delete the Multiple infs*/
/*                            -IPV6_STANDBY_MODIFY - resets the standby inf */
/*               u4DownIfId - Valid only when u1Operation is                */
/*               -IPV6_STANDBY_MODIFY . This will hold the standby inf index*/
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
V3OspfSendMultIfOnLink (tV3OsInterface * pInterface, UINT2 u2Operation,
                        UINT4 u4DownIfId)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsNeighbor      *pSdbyNbr = NULL;
    tIp6MultIfToIp6Link IfOverLinkInfo;
    UINT4               u4LinkId;
    UINT1               u1MultIfCount = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSendMultIfOnLink\n");

    MEMSET (&IfOverLinkInfo, 0, sizeof (tIp6MultIfToIp6Link));

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tIp6MultIfToIp6Link),
                                       0)) == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : Unable to allocate buf\n");
        return;
    }

#ifdef LNXIP6_WANTED
    IfOverLinkInfo.u4OnLinkActiveIfId =
        CfaGetIfIpPort (pInterface->u4InterfaceId);
#else
    IfOverLinkInfo.u4OnLinkActiveIfId = pInterface->u4InterfaceId;
#endif

    IfOverLinkInfo.u2OnLinkIfCount = pInterface->u1SdbyActCount;

    IfOverLinkInfo.u2LinkOperation = u2Operation;

    if (u2Operation == IPV6_STANDBY_MODIFY)
    {
        if (u1MultIfCount < IPV6_MAX_IF_OVER_LINK)
        {
            IfOverLinkInfo.apIp6If[u1MultIfCount] = u4DownIfId;
        }
        IfOverLinkInfo.u2OnLinkIfCount = OSPFV3_DEF_IFOVERLINK_COUNT;
    }
    else
    {

        pNode = NULL;

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNode, tTMO_SLL_NODE *)
        {                        /* Scan the Standby Nbrs of the Active Interface */

            pSdbyNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNode);

            if (pSdbyNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
            {

#ifdef LNXIP6_WANTED
                u4LinkId = CfaGetIfIpPort (pSdbyNbr->u4NbrInterfaceId);
#else
                u4LinkId = pSdbyNbr->u4NbrInterfaceId;
#endif
                if (u1MultIfCount < IPV6_MAX_IF_OVER_LINK)
                {
                    IfOverLinkInfo.apIp6If[u1MultIfCount] = u4LinkId;
                    u1MultIfCount++;
                }
            }
        }
        if (u1MultIfCount >= IPV6_MAX_IF_OVER_LINK)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
        }
        else
        {
            IfOverLinkInfo.apIp6If[u1MultIfCount] =
                IfOverLinkInfo.u4OnLinkActiveIfId;
        }
    }

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) (VOID *) (&IfOverLinkInfo),
                               0, sizeof (tIp6MultIfToIp6Link));

#ifdef IP6_WANTED
    NetIpv6NotifySelfIfOnLink (IP6_OSPF_PROTOID, (tIp6MultIfToIp6Link *) pBuf);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendMultIfOnLink\n");
    return;

}

#ifdef LNXIP6_WANTED
/***************************************************************************/
/*                                                                         */
/*     Function Name : V3SendPktOnUnNumberedIntf                           */
/*                                                                         */
/*     Description   : This module opens a new packet socket and sends the */
/*                     pkt in the link layer level.                        */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/
INT4
V3SendPktOnUnNumberedIntf (UINT1 *pu1RawPkt, UINT4 u4Length, UINT4 u4Index,
                           tIp6Addr * pSrcIp6Addr, tIp6Addr * pDstIp6Addr,
                           UINT4 u4ContextId)
{

    UINT4               u4HdrSize;
    tIp6Hdr            *pIp6 = NULL;
    INT4                i4Length;
    UINT4               u4Port;
    struct sockaddr_ll  DestAddrUnNumbIntf;
    tMacAddr            DestMacAddr = { 0x33, 0x33, 0x5e, 0x0, 0x0, 0x5 };
    INT4                i4UnNumPfSock = -1;
    INT4                i4SendBytes = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3SendPktOnUnNumberedIntf\n");

    i4Length = u4Length;
    u4HdrSize = sizeof (tIp6Hdr);
    i4Length += u4HdrSize;

    NetIpv4GetPortFromIfIndex (u4Index, &u4Port);

    pIp6 = (tIp6Hdr *) (void *) pu1RawPkt;

    pIp6->u4Head = IPV6_OSPF_DEF_HDR_VAL;
    pIp6->u2Len = (UINT2) (CRU_HTONS ((UINT2) (u4Length)));
    pIp6->u1Nh = OSPFV3_PROTO;

    /*
     * Set the hop limit to the value specified in the parameter
     * if it is non-zero, otherwise set to default hop limit value
     */

    pIp6->u1Hlim = 1;

    Ip6AddrCopy (&pIp6->srcAddr, pSrcIp6Addr);
    Ip6AddrCopy (&pIp6->dstAddr, pDstIp6Addr);

    MEMSET (&DestAddrUnNumbIntf, 0, sizeof (struct sockaddr_ll));

    /* Opens a Packet Socket for sending the pkt */
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    i4UnNumPfSock = LnxVrfGetSocketFd (u4ContextId, PF_PACKET, SOCK_DGRAM,
                                       IP_HTONS (ETH_P_ALL),
                                       LNX_VRF_OPEN_SOCKET);
#else
    i4UnNumPfSock = socket (PF_PACKET, SOCK_DGRAM, IP_HTONS (ETH_P_ALL));
    UNUSED_PARAM (u4ContextId);
#endif
    if (i4UnNumPfSock < 0)
    {
        gu4V3SendPktOnUnNumberedIntfFail++;
        return OSIX_FAILURE;
    }

    DestAddrUnNumbIntf.sll_family = PF_PACKET;
    DestAddrUnNumbIntf.sll_ifindex = (UINT2) u4Port;
    DestAddrUnNumbIntf.sll_protocol = IP_HTONS (ETH_P_IPV6);
    DestAddrUnNumbIntf.sll_halen = CFA_ENET_ADDR_LEN;
    MEMCPY (DestAddrUnNumbIntf.sll_addr, DestMacAddr, CFA_ENET_ADDR_LEN);
    if ((i4SendBytes = sendto ((i4UnNumPfSock), pu1RawPkt,
                               (i4Length), 0,
                               ((struct sockaddr *) &DestAddrUnNumbIntf),
                               (sizeof (struct sockaddr_ll)))) == OSIX_FAILURE)
    {
        close (i4UnNumPfSock);
        gu4V3SendPktOnUnNumberedIntfFail++;
        return OSIX_FAILURE;
    }
    close (i4UnNumPfSock);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3SendPktOnUnNumberedIntf\n");

    return OSIX_SUCCESS;
}
#endif

/*----------------------------------------------------------------------*/
/*                     End of the file o3port.c                         */
/*----------------------------------------------------------------------*/
