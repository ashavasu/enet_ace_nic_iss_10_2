/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agutil.c,v 1.10 2018/01/25 10:04:15 siva Exp $
 *
 * Description: This file contains utility functions for address range.
 *
 *******************************************************************/
#include "o3inc.h"

/************************************************************************/
/*                                                                      */
/* Function     : V3RagCompAddrRng                                      */
/*                                                                      */
/* Description  : Compare two Address ranges                            */
/*                                                                      */
/*                                                                      */
/* Input        : pAsExtRange1 -Pointer to first Range                  */
/*                pAsExtRange2 -Pointer to second Range                 */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : OSPFV3_GREATER - Range1 is more specific              */
/*              : OSPFV3_EQUAL   - Both ranges equal                    */
/*              : OSPFV3_LESS - Range1 is less specific                 */
/*              : NOT_EQUAL - Ranges are in different                   */
/*                                                                      */
/************************************************************************/

PUBLIC INT1
V3RagCompAddrRng (tV3OsAsExtAddrRange * pAsExtRange1,
                  tV3OsAsExtAddrRange * pAsExtRange2)
{
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;
    INT1                i1RetVal;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3RagCompAddrRng\n");

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAsExtRange1->ip6Addr),
                         pAsExtRange1->u1PrefixLength);

    V3UtilIp6PrefixCopy (&tmpRngNet,
                         &(pAsExtRange2->ip6Addr),
                         pAsExtRange2->u1PrefixLength);

    switch (V3UtilIp6AddrComp (&tmpRngNet, &newRngNet))
    {
        case OSPFV3_LESS:
            if ((V3UtilIp6PrefixComp (&(pAsExtRange1->ip6Addr),
                                      &(pAsExtRange2->ip6Addr),
                                      pAsExtRange2->u1PrefixLength))
                == OSPFV3_EQUAL)
            {
                i1RetVal = OSPFV3_GREATER;
            }
            else
            {
                i1RetVal = OSPFV3_NOT_EQUAL;
            }
            break;

        case OSPFV3_GREATER:
            if ((V3UtilIp6PrefixComp (&(pAsExtRange1->ip6Addr),
                                      &(pAsExtRange2->ip6Addr),
                                      pAsExtRange1->u1PrefixLength))
                == OSPFV3_EQUAL)
            {
                i1RetVal = OSPFV3_LESS;
            }
            else
            {
                i1RetVal = OSPFV3_NOT_EQUAL;
            }
            break;

        default:
            if (pAsExtRange1->u1PrefixLength > pAsExtRange2->u1PrefixLength)
            {
                i1RetVal = OSPFV3_GREATER;
            }
            else if (pAsExtRange1->u1PrefixLength <
                     pAsExtRange2->u1PrefixLength)
            {
                i1RetVal = OSPFV3_LESS;
            }
            else
            {
                i1RetVal = OSPFV3_EQUAL;
            }
            break;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3RagCompAddrRng\n");
    return (i1RetVal);
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagIsExtRtFallInRange                               */
/*                                                                      */
/* Description  : Checks whether Ext Rt falls in range                  */
/*                                                                      */
/*                                                                      */
/* Input        : pExtRoute - Pointer to Ext Rt                         */
/*                pAsExtRange -Pointer to Range                         */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : OSIX_TRUE  - If the extrenal Route falls              */
/*                OSIX_FALSE - If the extrenal Route does not fall      */
/************************************************************************/

PUBLIC UINT1
V3RagIsExtRtFallInRange (tV3OsExtRoute * pExtRoute,
                         tV3OsAsExtAddrRange * pAsExtRange)
{
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3RagIsExtRtFallInRange\n");

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAsExtRange->ip6Addr), pAsExtRange->u1PrefixLength);

    V3UtilIp6PrefixCopy (&tmpRngNet,
                         &(pExtRoute->ip6Prefix), pAsExtRange->u1PrefixLength);

    if ((V3UtilIp6AddrComp (&newRngNet, &tmpRngNet) == OSPFV3_EQUAL) &&
        (pAsExtRange->u1PrefixLength <= pExtRoute->u1PrefixLength))
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3RagIsExtRtFallInRange\n");
        return OSIX_TRUE;
    }
    return OSIX_FALSE;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagFindMatChngBkBoneRangeInCxt                      */
/*                                                                      */
/* Description  : Finds Bkbone range that subsumes the Ext Rt           */
/*                                                                      */
/* Input        : pV3OspfCxt - Context pointer                          */
/*                pExtRoute  - Pointer to Ext Rt                        */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : Bkbone range that subsumes the Ext Rt or NULL         */
/*                                                                      */
/************************************************************************/

PUBLIC tV3OsAsExtAddrRange *
V3RagFindMatChngBkBoneRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                                  tV3OsExtRoute * pExtRoute)
{
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFindMatChngBkBoneRange\n");

    TMO_SLL_Scan (&(pV3OspfCxt->pBackbone->asExtAddrRangeLst), pScanAsExtRng,
                  tV3OsAsExtAddrRange *)
    {
        /* Check if Ext Rt falls in range */
        if (V3RagIsExtRtFallInRange (pExtRoute, pScanAsExtRng) == OSIX_TRUE)
        {
            /* Insert Ext Rt in Range */
            V3RagInsertRouteInBkboneAggRngInCxt (pV3OspfCxt, pScanAsExtRng,
                                                 pExtRoute);

            /* Process the range   */
            V3RagProcessBkBoneRangeInCxt (pV3OspfCxt, pScanAsExtRng);
            return pScanAsExtRng;
        }
    }

    /*  No Mtchng Bkbone range found : So originate 
       non-aggregated Type 5 LSA
     */
    V3RagOriginateNonAggLsaInCxt (pV3OspfCxt, pExtRoute, NULL);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFindMatChngBkBoneRange\n");
    return NULL;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagChkLsaFallInRng                                  */
/*                                                                      */
/* Description  : Finds whether LSA is subsumed by AS Ext Range         */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range                         */
/*                pLsaInfo - Pointer to LSA                             */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/*                                                                      */
/* Returns      : OSPFV3_EQUAL - LSA net equals  range                  */
/*                OSPFV3_GREATER - LSA falls in range                   */
/*                OSPFV3_LESS - LSA does not fall in range              */
/*                                                                      */
/************************************************************************/

PUBLIC INT1
V3RagChkLsaFallInRng (tV3OsAsExtAddrRange * pAsExtRange,
                      tV3OsLsaInfo * pLsaInfo)
{
    tIp6Addr            newRngNet;
    tIp6Addr            tmpRngNet;
    tIp6Addr            lsaRngNet;
    UINT1               u1PrefixLen = 0;
    INT1                i1RetVal = OSPFV3_LESS;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagChkLsaFallInRng\n");

    V3UtilIp6PrefixCopy (&newRngNet,
                         &(pAsExtRange->ip6Addr), pAsExtRange->u1PrefixLength);

    MEMSET (&lsaRngNet, 0, sizeof (tIp6Addr));

    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    OSPFV3_IP6_ADDR_PREFIX_COPY (lsaRngNet,
                                 *(pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET),
                                 u1PrefixLen);

    V3UtilIp6PrefixCopy (&tmpRngNet, &lsaRngNet, pAsExtRange->u1PrefixLength);

    if (V3UtilIp6AddrComp (&newRngNet, &tmpRngNet) == OSPFV3_EQUAL)
    {
        if (u1PrefixLen == pAsExtRange->u1PrefixLength)
        {
            i1RetVal = OSPFV3_EQUAL;
        }
        else if (u1PrefixLen > pAsExtRange->u1PrefixLength)
        {
            i1RetVal = OSPFV3_GREATER;
        }
        else if (u1PrefixLen < pAsExtRange->u1PrefixLength)
        {
            i1RetVal = OSPFV3_LESS;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagChkLsaFallInRng\n");

    return i1RetVal;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagCompRngAndLsa                                    */
/*                                                                      */
/* Description  : Finds whether cost/Path advertised in Range is        */
/*                different from that in LSA                            */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range                         */
/*                pLsaInfo - Pointer to LSA                             */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : OSIX_TRUE -Change present                             */
/*                OSIX_FALSE -No change                                 */
/*                                                                      */
/************************************************************************/
PUBLIC UINT1
V3RagCompRngAndLsa (tV3OsAsExtAddrRange * pAsExtRange, tV3OsLsaInfo * pLsaInfo)
{
    tV3OsExtLsaLink     extLsaLink;
    UINT1               u1LsaPbit = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagCompRngAndLsa\n");

    V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, &extLsaLink);

    if (((pAsExtRange->metric).u4MetricType !=
         (extLsaLink.metric).u4MetricType) ||
        ((pAsExtRange->metric).u4Metric != (extLsaLink.metric).u4Metric))
    {
        return OSIX_TRUE;
    }

    if (OSPFV3_IS_P_BIT_SET (pLsaInfo->pLsa) != OSPFV3_RAG_NO_CHNG)
    {
        u1LsaPbit = OSIX_TRUE;
    }

    if (u1LsaPbit != pAsExtRange->u1AggTranslation)
    {
        return OSIX_TRUE;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagCompRngAndLsa\n");

    return OSIX_FALSE;
}

/***********************************************************************/
/*                                                                     */
/* Function        : V3RagChkLsaFallInPrvRng                           */
/*                                                                     */
/* Description     : Finds whether LSA is subsumed by                  */
/*                   previous AS Ext Range                             */
/*                                                                     */
/* Input           : pAsExtRange -Pointer to Range                     */
/*                   pLsaInfo - Pointer to LSA                         */
/*                                                                     */
/* Output          : None                                              */
/*                                                                     */
/* Returns         : OSIX_TRUE if Lsa falls in the Range               */
/*                   OSIX_FALSE if Lsa does not fall in the Range      */
/*                                                                     */
/************************************************************************/
PUBLIC UINT1
V3RagChkLsaFallInPrvRng (tV3OsAsExtAddrRange * pAsExtRange,
                         tV3OsLsaInfo * pLsaInfo)
{

    tTMO_SLL_NODE      *pPrev = NULL;
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagChkLsaFallInPrvRng\n");

    pArea = V3GetFindAreaInCxt (pLsaInfo->pV3OspfCxt, &(pAsExtRange->areaId));

    if (pArea == NULL)
    {
        gu4V3RagChkLsaFallInPrvRngFail++;
        return OSIX_FALSE;
    }
    if (V3UtilAreaIdComp (pAsExtRange->areaId,
                          OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
    {
        pPrev = TMO_SLL_Previous (&(pArea->asExtAddrRangeLst),
                                  &(pAsExtRange->nextAsExtAddrRange));
    }
    else
    {
        pPrev = TMO_SLL_Previous (&(pArea->type7AddrRangeLst),
                                  &(pAsExtRange->nextAsExtAddrRange));
    }
    pAsExtRange = (tV3OsAsExtAddrRange *) (pPrev);

    if (pAsExtRange != NULL)
    {
        if ((V3RagChkLsaFallInRng (pAsExtRange, pLsaInfo) == OSPFV3_GREATER) ||
            (V3RagChkLsaFallInRng (pAsExtRange, pLsaInfo) == OSPFV3_EQUAL))
        {

            return OSIX_TRUE;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagChkLsaFallInPrvRng\n");
    return OSIX_FALSE;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagUpdatCostTypeForRange                            */
/*                                                                      */
/* Description  : Updates Cost/Path Type for the range                  */
/*                if affected by Ext Rt                                 */
/*                                                                      */
/* Input        : pAsExtRange -Pointer to Range                         */
/*                pExtRoute - Pointer to Ext Rt                         */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : OSIX_TRUE - Update Cost/Type                          */
/*                OSIX_FALSE - No update                                */
/*                                                                      */
/************************************************************************/

PUBLIC UINT1
V3RagUpdatCostTypeForRange (tV3OsAsExtAddrRange * pAsExtRange,
                            tV3OsExtRoute * pExtRoute)
{

    UINT1               u1Change = OSIX_FALSE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3RagUpdatCostTypeForRange\n");

    if ((pExtRoute->metric).u4MetricType == (pAsExtRange->metric).u4MetricType)
    {
        if (((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT) ||
            ((pExtRoute->metric).u4Metric > (pAsExtRange->metric).u4Metric))
        {
            (pAsExtRange->metric).u4Metric = (pExtRoute->metric).u4Metric;
            u1Change = OSIX_TRUE;
        }
    }
    else if ((pExtRoute->metric).u4MetricType == OSPFV3_TYPE_2_METRIC)
    {
        (pAsExtRange->metric).u4MetricType = (pExtRoute->metric).u4MetricType;
        (pAsExtRange->metric).u4Metric = (pExtRoute->metric).u4Metric;
        u1Change = OSIX_TRUE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3RagUpdatCostTypeForRange\n");
    return u1Change;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagUpdtRtLstBtwRangesInCxt                       */
/*                                                                      */
/* Description     : Updates the ranges for cost/ Path Type             */
/*                   to be advertised                                   */
/*                                                                      */
/* Input           : pV3OspfCxt - Pointer to Context                    */
/*                   pAsExtRange- Pointer to Range                      */
/*                   pSpecLessRange - Pointer to less spec range        */
/*                   u1Flag     - If u1Flag is True then addition       */
/*                                else deletion                         */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagUpdtRtLstBtwRangesInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsAsExtAddrRange * pAsExtRange,
                              tV3OsAsExtAddrRange * pSpecLessRange,
                              UINT1 u1Flag)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tTMO_SLL_NODE      *pExtRtNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagUpdtRtLstBtwRanges\n");

    if (u1Flag == OSIX_TRUE)
    {
        OSPFV3_DYNM_SLL_SCAN (&(pSpecLessRange->extRtLst),
                              pExtRtNode, pTempNode, tTMO_SLL_NODE *)
        {
            pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                             nextExtRouteInRange, pExtRtNode);
            if (V3RagIsExtRtFallInRange (pExtRoute, pAsExtRange) == OSIX_TRUE)
            {
                /* If Ext Rt falls in more specific newly added range:
                   Delete it from less specific range and
                   Add it to new range
                 */
                TMO_SLL_Delete (&(pSpecLessRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                TMO_SLL_Add (&(pAsExtRange->extRtLst),
                             &(pExtRoute->nextExtRouteInRange));
                pExtRoute->pAsExtAddrRange = pAsExtRange;
            }
        }

        /* Now update both the ranges */
        V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pAsExtRange);
        V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pSpecLessRange);
        return;
    }
    else
    {
        /* Denotes pAsExtRange is deleted */
        if (V3UtilAreaIdComp (pAsExtRange->areaId,
                              OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
        {
            OSPFV3_DYNM_SLL_SCAN (&(pAsExtRange->extRtLst),
                                  pExtRtNode, pTempNode, tTMO_SLL_NODE *)
            {
                pExtRoute = OSPFV3_GET_BASE_PTR (tV3OsExtRoute,
                                                 nextExtRouteInRange,
                                                 pExtRtNode);
                TMO_SLL_Delete (&(pAsExtRange->extRtLst),
                                &(pExtRoute->nextExtRouteInRange));

                TMO_SLL_Add (&(pSpecLessRange->extRtLst),
                             &(pExtRoute->nextExtRouteInRange));
                pExtRoute->pAsExtAddrRange = pSpecLessRange;
            }
            V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pSpecLessRange);
            return;
        }
        else
        {
            if ((pAsExtRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
            {
                return;
            }

            if ((pAsExtRange->metric).u4MetricType ==
                (pSpecLessRange->metric).u4MetricType)
            {
                if ((pAsExtRange->metric).u4Metric >
                    (pSpecLessRange->metric).u4Metric)
                {
                    (pSpecLessRange->metric).u4Metric =
                        (pAsExtRange->metric).u4Metric;
                }
            }
            else if ((pAsExtRange->metric).u4MetricType == OSPFV3_TYPE_2_METRIC)
            {
                (pSpecLessRange->metric).u4MetricType =
                    (pAsExtRange->metric).u4MetricType;
                (pSpecLessRange->metric).u4Metric =
                    (pAsExtRange->metric).u4Metric;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagUpdtRtLstBtwRanges\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagUpdtRtLstFromExtRtTableInCxt                  */
/*                                                                      */
/* Description     : Updates the range for cost/ Path Type by scanning  */
/*                   Ext Rt Table                                       */
/*                                                                      */
/* Input           : pV3OspfCxt  -Pointer to Context                    */
/*                   pAsExtRange -Pointer to Range                      */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagUpdtRtLstFromExtRtTableInCxt (tV3OspfCxt * pV3OspfCxt,
                                   tV3OsAsExtAddrRange * pAsExtRange)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tInputParams        inParams;
    tV3OsAsExtAddrRange *pRtRange = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    VOID               *pLeafNode = NULL;
    tV3OsAsExtAddrRange *pPrvAsExtRng = NULL;
    tTMO_SLL_NODE      *pPrev = NULL;
    tV3OsArea          *pArea = NULL;

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagUpdtRtLstFromExtRtTable\n");
    pArea = V3GetFindAreaInCxt (pV3OspfCxt, &(pAsExtRange->areaId));
    if (pArea == NULL)
    {
        OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                    "V3GetFindAreaInCxt Failed\n");
        return;
    }
    if (V3UtilAreaIdComp (pAsExtRange->areaId,
                          OSPFV3_BACKBONE_AREAID) != OSPFV3_EQUAL)
    {
        (pAsExtRange->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
        (pAsExtRange->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
    }
    /* External routes are stored in Trie */

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        /* Bkbone range */
        if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
            == OSPFV3_EQUAL)
        {
            if (V3RagIsExtRtFallInRange (pExtRoute, pAsExtRange) == OSIX_TRUE)
            {
                if (pExtRoute->pAsExtAddrRange == NULL)
                {
                    TMO_SLL_Add (&(pAsExtRange->extRtLst),
                                 &(pExtRoute->nextExtRouteInRange));
                    pExtRoute->pAsExtAddrRange = pAsExtRange;
                }
                else
                {
                    pRtRange = pExtRoute->pAsExtAddrRange;

                    /* If this range is more specific than continue */
                    if (V3RagCompAddrRng (pRtRange, pAsExtRange)
                        == OSPFV3_GREATER)
                    {
                        /* Ext Rt is attached to more specific range 
                         * So we should do nothing */
                        /* Do Nothing */

                    }
                    else
                    {
                        /* Remove the Ext Rt from and add to new range */
                        TMO_SLL_Delete (&(pRtRange->extRtLst),
                                        &(pExtRoute->nextExtRouteInRange));

                        TMO_SLL_Add (&(pAsExtRange->extRtLst),
                                     &(pExtRoute->nextExtRouteInRange));
                        pExtRoute->pAsExtAddrRange = pAsExtRange;
                    }
                }
            }
        }

        /* NSSA range */
        else
        {
            /* Check if the previous range is more specific */
            pPrev = TMO_SLL_Previous (&(pArea->type7AddrRangeLst),
                                      &(pAsExtRange->nextAsExtAddrRange));
            pPrvAsExtRng = (tV3OsAsExtAddrRange *) (pPrev);

            if ((pPrvAsExtRng != NULL) &&
                (V3RagIsExtRtFallInRange (pExtRoute, pPrvAsExtRng)
                 == OSIX_TRUE))
            {
                /* Since the Route falls in previous More Specific NSSA
                 * Address Range So we should do nothing */
                /* Do Nothing */
            }
            else
            {
                if (V3RagIsExtRtFallInRange (pExtRoute, pAsExtRange)
                    == OSIX_TRUE)
                {
                    V3RagUpdatCostTypeForRange (pAsExtRange, pExtRoute);
                }
            }
        }
        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pExtRoute->ip6Prefix));
        au1Key[sizeof (tIp6Addr)] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = au1Key;

    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    if (V3UtilAreaIdComp (pAsExtRange->areaId, OSPFV3_BACKBONE_AREAID)
        == OSPFV3_EQUAL)
    {
        V3RagUpdtBboneRngFrmExtLstInCxt (pV3OspfCxt, pAsExtRange);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagUpdtRtLstFromExtRtTable\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagOriginateNonAggLsaInCxt                       */
/*                                                                      */
/* Description     : Originates non-aggregated Type 5 or Type 7 LSA for */
/*                   the Ext Rt passed                                  */
/*                                                                      */
/* Input           : pV3OspfCxt - Context pointer                       */
/*                   pExtRoute  - Pointer to Ext Rt                     */
/*                 : pArea      - Pointer to Area                       */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagOriginateNonAggLsaInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsExtRoute * pExtRoute, tV3OsArea * pArea)
{
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagOriginateNonAggLsa\n");

    /* Area passed is NULL - Originate Type 5 LSA   
     * Otherwise generate Type 7 NSSA LSA  
     */

    /*
       AS Ext LSA should not be generated for connected interface on which
       OSPFv3 is enabled
     */
    pInterface = V3GetFindIf (pExtRoute->u4FwdIfIndex);

    if (pInterface != NULL && (pExtRoute->u2SrcProto == IP6_LOCAL_PROTOID))
    {
        return;
    }

    if (pArea == NULL)
    {

        /* Originate new Type 5 LSA */
        if (V3RagGenExtLsaInCxt (pV3OspfCxt, OSPFV3_AS_EXT_LSA,
                                 (UINT1 *) pExtRoute) == OSIX_TRUE)
        {
            /* For AS external LSA, backbone area pointer is passed as the
             * first parameter. This pointer ir used to retrive the context
             * pointer
             */
            V3GenerateLsa (pV3OspfCxt->pBackbone,
                           OSPFV3_AS_EXT_LSA,
                           &(pExtRoute->linkStateId),
                           (UINT1 *) pExtRoute, OSIX_FALSE);
        }
    }

    /* Area passed is NSSA - Originate Type 7       */

    else if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
    {
        if (pExtRoute->u1PrefixLength == 0)
        {
            V3GenerateLsa (pArea,
                           OSPFV3_DEFAULT_NSSA_LSA,
                           &(pExtRoute->linkStateId),
                           (UINT1 *) pExtRoute, OSIX_FALSE);
        }
        else
        {
            V3GenerateLsa (pArea,
                           OSPFV3_NSSA_LSA,
                           &(pExtRoute->linkStateId),
                           (UINT1 *) pExtRoute, OSIX_FALSE);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagOriginateNonAggLsa\n");
}

/************************************************************************/
/*                                                                      */
/* Function         : V3RagAddRouteInAggAddrRangeInCxt                  */
/*                                                                      */
/* Description      : This procedure adds the newly added external      */
/*                    route in the address range it falls in            */
/*                                                                      */
/* Input            : pV3OspfCxt - Pointer to OSPFv3 context            */
/*                    pExtRoute  - Pointer to Ext Rt                    */
/*                                                                      */
/* Output           : None                                              */
/*                                                                      */
/* Returns          : VOID                                              */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagAddRouteInAggAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                                  tV3OsExtRoute * pExtRoute)
{
    tV3OsAsExtAddrRange *pBkBoneRng = NULL;
    tV3OsAsExtAddrRange *pTmpNssaRng = NULL;
    tV3OsAsExtAddrRange *pType7Rng = NULL;
    tV3OsArea          *pArea = NULL;
    UINT1               u1ChngFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagAddRouteInAggAddrRange\n");

    if (pV3OspfCxt->pBackbone != NULL)
    {
        /* Find the most specific bkbone address range  
           that subsumes the route                      
         */
        pBkBoneRng = V3RagFindMatChngBkBoneRangeInCxt (pV3OspfCxt, pExtRoute);
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            /* Find the most specific NSSA range that subsumes the 
               route    
             */
            pTmpNssaRng = V3RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);
            if (pTmpNssaRng == NULL)
            {
                /* If no NSSA range subsumes the added Ext Rt,    
                   Bkbone range that subsumes the Ext Rt is used
                   for aggregation in NSSA                    
                 */
                pType7Rng = pBkBoneRng;
                if (pType7Rng != NULL)
                {
                    V3RagProcessNssaRange (pType7Rng, pArea);
                }
            }
            else
            {
                pType7Rng = pTmpNssaRng;
                u1ChngFlag = V3RagUpdatCostTypeForRange (pType7Rng, pExtRoute);
                if (u1ChngFlag == OSIX_TRUE)
                {
                    /* NSSA range, if affected (cost,type) by new    
                       Ext Rt is used for aggregation in NSSA        
                     */
                    V3RagProcessNssaRange (pType7Rng, pArea);
                }
            }

            if (pType7Rng == NULL)
            {
                /* If Ext Rt is not subsumed by any range   
                   (both bkbone and NSSA), originate non-agg    
                   Type 7 in NSSA                       
                 */
                V3RagOriginateNonAggLsaInCxt (pV3OspfCxt, pExtRoute, pArea);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagAddRouteInAggAddrRange\n");
}

/************************************************************************/
/*                                                                      */
/* Function        : V3RagDelRouteFromAggAddrRangeInCxt                 */
/*                                                                      */
/* Description     : This procedure deletes the Ext Rt from any address */
/*                   range it falls in                                  */
/*                                                                      */
/* Input           : pV3OspfCxt - Context pointer                       */
/*                   pExtRoute -Pointer to Ext Rt                       */
/*                                                                      */
/* Output          : None                                               */
/*                                                                      */
/* Returns         : VOID                                               */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagDelRouteFromAggAddrRangeInCxt (tV3OspfCxt * pV3OspfCxt,
                                    tV3OsExtRoute * pExtRoute)
{

    tV3OsLsaInfo       *pType5LsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsAsExtAddrRange *pTmpNssaRng = NULL;
    tV3OsLsaInfo       *pType7LsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagDelRouteFromAggAddrRange\n");

    /* Check if the deleted Ext Rt is subsumed by any Bkbone Range */
    if (pExtRoute->pAsExtAddrRange != NULL)
    {
        /* If YES, removes the Ext Rt from Bkbone range 
           and takes necessary measures 
         */
        V3RagFlushBkBoneRangeInCxt (pV3OspfCxt, pExtRoute->pAsExtAddrRange,
                                    pExtRoute);
    }
    else
    {
        /* If NO, check if this Ext Rt has caused non-aggregated Type 5 
         * LSA generation
         * Backbone pointer is passed to get the context info
         */
        pType5LsaInfo =
            V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                 &(pExtRoute->linkStateId),
                                 &(pV3OspfCxt->rtrId), NULL,
                                 pV3OspfCxt->pBackbone);

        /* Flush the non-aggregated Type 5 LSA  */
        if ((pType5LsaInfo != NULL) &&
            (pType5LsaInfo->u1TrnsltType5 == OSPFV3_REDISTRIBUTED_TYPE5))
        {
            if (pType5LsaInfo->pLsaDesc != NULL)
            {
                pType5LsaInfo->pLsaDesc->pAssoPrimitive = NULL;
            }
            V3LsuDeleteFromAllRxmtLst (pType5LsaInfo, OSIX_FALSE);
            V3AgdFlushOut (pType5LsaInfo);
        }
    }

    if ((pExtRoute->u1PrefixLength == 0) &&
        (OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt)))
    {
        return;
    }

    /* Check if any of the NSSA area of the router 
       has Type 7 LSAs because of this Ext Rt
     */

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            /* Find the range in NSSA area that best 
               subsumes this Ext Rt     
             */
            pTmpNssaRng = V3RagFindAggAddrRangeInNssaForRt (pExtRoute, pArea);

            if (pTmpNssaRng != NULL)
            {
                /* If range is found, take necessary measures by invoking 
                   below function
                 */
                V3RagFlushNssaRange (pTmpNssaRng, pExtRoute, pArea);
            }
            else
            {
                /* If there is no NSSA range that subsumes this Ext Rt,
                   aggregation in area might have been done on the 
                   basis of Bkbone range that subsumes the Ext Rt
                 */
                if (pExtRoute->pAsExtAddrRange != NULL)
                {
                    /* If YES, take necessary measure by invoking below
                       function
                     */
                    V3RagFlushNssaRange (pExtRoute->pAsExtAddrRange,
                                         pExtRoute, pArea);
                }
                else
                {
                    /* This Ext Rt must have been originated as non- aggregated 
                       Type 7 LSA
                     */
                    pType7LsaInfo =
                        V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                             &(pExtRoute->linkStateId),
                                             &(pV3OspfCxt->rtrId), NULL, pArea);

                    if (pType7LsaInfo != NULL)
                    {
                        /* If so, flush the Type 7 LSA   */
                        if (pType7LsaInfo->pLsaDesc != NULL)
                        {
                            pType7LsaInfo->pLsaDesc->pAssoPrimitive = NULL;
                        }
                        V3AgdFlushOut (pType7LsaInfo);
                    }

                }
            }
        }
    }
    pExtRoute->pAsExtAddrRange = NULL;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagDelRouteFromAggAddrRange\n");
    return;
}

/***********************************************************************/
/*                                                                     */
/* Function        : V3RagGetNssaAddrRange                             */
/*                                                                     */
/* Description     : Gets the NSSA address range configured in the     */
/*                   NSSA area                                         */
/*                                                                     */
/* Input           : pArea        -    Pointer to Area                 */
/*                   pIp6Addr     -    Address range prefix            */
/*                   u1PrefixLen  -    Prefix length                   */
/*                                                                     */
/* Output          : None                                              */
/*                                                                     */
/* Returns         : Pointer to address range or NULL                  */
/*                                                                     */
/************************************************************************/
tV3OsAsExtAddrRange *
V3RagGetNssaAddrRange (tV3OsArea * pArea, tIp6Addr * pIp6Addr,
                       UINT1 u1PrefixLen)
{
    tV3OsAsExtAddrRange *pAddrRange = NULL;
    tTMO_SLL_NODE      *pNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagGetNssaAddrRange \n");

    TMO_SLL_Scan (&(pArea->type7AddrRangeLst), pNode, tTMO_SLL_NODE *)
    {
        pAddrRange =
            OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange, nextAsExtAddrRange,
                                 pNode);

        if ((V3UtilIp6AddrComp (&(pAddrRange->ip6Addr), pIp6Addr)
             == OSPFV3_EQUAL) && (pAddrRange->u1PrefixLength == u1PrefixLen))
        {
            return pAddrRange;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RagGetNssaAddrRange \n");

    return NULL;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agutil.c                    */
/*-----------------------------------------------------------------------*/
