/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agd.c,v 1.9 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to aging
 *         the database. 
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AgdFlushOut                                              */
/*                                                                           */
/* Description  : The advertisements age is set to MAX_AGE and it is flushed */
/*                out.                                                       */
/*                                                                           */
/* Input        : pLsaInfo    : pointer to the advertisement                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3AgdFlushOut (tV3OsLsaInfo * pLsaInfo)
{
    UINT4               u4ContextId = OSPFV3_ZERO;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3AgdFlushOut\n");

    u4ContextId = pLsaInfo->pV3OspfCxt->u4ContextId;

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_CLI_LSDB_TRC, u4ContextId,
                 "Flushing  LSA Type %d Link_state_ID %x Adv_rtr_ID %x\n",
                 pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    /* set the age field to MAX_AGE */
    pLsaInfo->u2LsaAge = OSPFV3_MAX_AGE;

    OSPFV3_TRC4 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC, u4ContextId,
                 "Flushing LSA LSType %d Link_state_ID = %x LsaAge = %d seqNum = %d\n",
                 pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 pLsaInfo->u2LsaAge, pLsaInfo->lsaSeqNum);

    OSPFV3_BUFFER_WTOPDU ((pLsaInfo->pLsa + OSPFV3_AGE_OFFSET_IN_LS_HEADER),
                          OSPFV3_MAX_AGE);

    if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
    {
        if ((pLsaInfo->pLsaDesc != NULL) &&
            (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired != OSIX_TRUE))
        {
            V3TmrDeleteTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer));
            pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
        }
    }

    if (pLsaInfo->u1FloodFlag == OSIX_FALSE)
    {
        V3LsuFloodOut (pLsaInfo, NULL, pLsaInfo->pArea, OSIX_TRUE,
                       pLsaInfo->pInterface);

        V3LsuSendAllLsuInCxt (pLsaInfo->pV3OspfCxt);
    }

    if (pLsaInfo->u4RxmtCount == 0)
    {
        V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_FALSE);
    }
    else
    {
        /* Send the LSA to the standby node */
        if (!((pLsaInfo->pV3OspfCxt->u1RouteLeakStatus == OSPFV3_TRUE) ||
              (pLsaInfo->pV3OspfCxt->u1IfRouteLeakStatus == OSPFV3_TRUE)))
        {
            if (O3RedDynConstructAndSendLsa (pLsaInfo) == OSIX_FAILURE)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "V3AgdFlushOut: LSA sync-up failed "
                              "Lsa originated type: %x, link state id: %x, "
                              "adv rtr id: %x\n",
                              pLsaInfo->lsaId.u2LsaType,
                              OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                                       lsaId.linkStateId),
                              OSPFV3_BUFFER_DWFROMPDU
                              (pLsaInfo->lsaId.advRtrId)));

                OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                                 pLsaInfo->pV3OspfCxt->u4ContextId,
                                 " LSA sync-up failed "
                                 "Lsa originated type: %x, link state id: %x, "
                                 "advertising router id: %x\n",
                                 pLsaInfo->lsaId.u2LsaType,
                                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                                          lsaId.linkStateId),
                                 OSPFV3_BUFFER_DWFROMPDU
                                 (pLsaInfo->lsaId.advRtrId));
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT : V3AgdFlushOut\n");

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3AgdCheckChksum                                           */
/*                                                                           */
/* Description  : The checksum of the specified advertisement is verified. If*/
/*                it is found to be incorrect then the OSPF is  restarted.   */
/*                                                                           */
/* Input        : pLsaInfo    : pointer to the advertisement whose           */
/*                                      checksum is to be verified           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS, if checksum is correct                            */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3AgdCheckChksum (tV3OsLsaInfo * pLsaInfo)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3AgdCheckChksum\n");

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_CLI_LSDB_TRC,
                 pLsaInfo->pV3OspfCxt->u4ContextId,
                 "LSA chksum Verification  LSA Type %d"
                 "Link_state_ID %x Advertising router ID %x\n",
                 pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    if (V3UtilVerifyLsaFletChksum (pLsaInfo->pLsa, pLsaInfo->u2LsaLen)
        == OSIX_FALSE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Checksum Failed for  LSA Type %d"
                      "Link_state_ID %x. OSPF restarted.\n",
                      pLsaInfo->lsaId.u2LsaType,
                      OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId)));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OSPFV3_CLI_PPP_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "ChkSum Failure OSPF Restarted\n");

        if (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
        {
            V3RtrDisableInCxt (pLsaInfo->pV3OspfCxt, OSPFV3_FALSE);
        }
        else if (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState
                 == OSPFV3_GR_RESTART)
        {
            O3GrDisableInCxt (pLsaInfo->pV3OspfCxt);
        }
        else
        {
            /* OSPF Process is undergoing GR shutdown */
            return OSIX_FAILURE;
        }
        V3RtrEnableInCxt (pLsaInfo->pV3OspfCxt, OSPFV3_FALSE);

        return OSIX_FAILURE;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3AgdCheckChksum\n");

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agd.c                        */
/*-----------------------------------------------------------------------*/
