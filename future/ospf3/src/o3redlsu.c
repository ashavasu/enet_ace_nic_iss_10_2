/********************************************************************
 *  * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *  *
 *  * $Id: o3redlsu.c,v 1.14 2017/12/26 13:34:28 siva Exp $
 *  *
 *
 *  *
 *********************************************************************/

#include "o3inc.h"

/* Prototypes */

PRIVATE tV3OsLsaInfo *O3RedLsuSendBulkUpdatesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId areaId, tV3OsLsaId lsaId));

PRIVATE tV3OsLsaInfo *O3RedLsuSendLinkScopeUpdatesInArea
PROTO ((tV3OsArea * pArea, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

PRIVATE tV3OsLsaInfo *O3RedLsuSendVirtualLsuInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

PRIVATE tV3OsLsaInfo *O3RedLsuSendAreaScopeUpdatesInArea
PROTO ((tV3OsArea * pArea, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

PRIVATE tV3OsLsaInfo *O3RedLsuSendAsScopeUpdatesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

PRIVATE INT4        O3RedLsuConstructAndSendBulkUpdt
PROTO ((tV3OsLsaInfo * pLsaInfo, tRmMsg ** ppRmMsg, UINT2 *pu2Len));

PRIVATE UINT2       O3RedLsuGetAssoPrimitiveLength
PROTO ((tV3OsLsaInfo * pLsaInfo));

PRIVATE INT4
     
     
     
     O3RedLsuUpdateAssoProimitive
PROTO ((tV3OspfCxt * pV3OspfCxt, tRmMsg * pRmMsg, UINT2 *pu2Len,
        tV3OsRmLsdbInfo * pLsdbInfo));

PRIVATE INT4
    O3RedLsuAddAssoPrimitive PROTO ((tRmMsg * pRmMsg, UINT2 *pu2Len,
                                     tV3OsLsaInfo * pLsaInfo));

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendLsu                                           */
/*                                                                          */
/* Description  : This function is called when active node get the bulk     */
/*                request message or sub bulk update event. This function   */
/*                gets the next LSA corresponding to the one stored in the  */
/*                temporary data and sends to the standby node.             */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : pi4SendSubBulkEvent-OSIX_SUCCESS id sunb bulk event       */
/*                                    is to be sent, else OSIX_FAILURE      */
/*                                                                          */
/* Returns      : OSPFV3_TRUE / OSPFV3_FALSE                                */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedLsuSendLsu (INT4 *pi4SendSubBulkEvent)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4SendUpdate = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedLsuSendLsu\r\n");

    /* Get the context id from the global variables stored during the
     * previous bulk update. Scan from the current context id and send
     * the bulk updates from the current context id
     */

    if (gV3OsRtr.ospfRedInfo.lastLsaInfo.lsaId.u2LsaType == OSPFV3_ZERO)
    {
        gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_LSU;
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, " Bulk Update started for LSU\r\n");
    }

    for (u4ContextId = gV3OsRtr.ospfRedInfo.u4LastCxtId;
         u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT; u4ContextId++)
    {
        pLsaInfo = NULL;

        pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

        if (pV3OspfCxt == NULL)
        {
            OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                            gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                            u4ContextId,
                            "OSPFv3 Associated Context Not Found\r\n");
            continue;
        }

        if (u4ContextId != gV3OsRtr.ospfRedInfo.u4LastCxtId)
        {
            /* Scanning the new context, clear the area id and lsa id */
            MEMSET (gV3OsRtr.ospfRedInfo.lastLsaInfo.areaId, 0,
                    sizeof (gV3OsRtr.ospfRedInfo.lastLsaInfo.areaId));
            MEMSET (&gV3OsRtr.ospfRedInfo.lastLsaInfo.lsaId, 0,
                    sizeof (tV3OsLsaId));
        }

        pLsaInfo = O3RedLsuSendBulkUpdatesInCxt
            (pV3OspfCxt, gV3OsRtr.ospfRedInfo.lastLsaInfo.areaId,
             gV3OsRtr.ospfRedInfo.lastLsaInfo.lsaId);
    }

    if (pLsaInfo != NULL)
    {
        /* In this case, sub bulk update event needs to be sent and
         * pLsaInfo contains the last sent LSU update
         */
        gV3OsRtr.ospfRedInfo.u4LastCxtId = pLsaInfo->pV3OspfCxt->u4ContextId;
        MEMCPY (gV3OsRtr.ospfRedInfo.lastLsaInfo.areaId,
                pLsaInfo->pArea->areaId, sizeof (tV3OsAreaId));
        MEMCPY (&(gV3OsRtr.ospfRedInfo.lastLsaInfo.lsaId),
                &(pLsaInfo->lsaId), sizeof (tV3OsLsaId));

        i4SendUpdate = OSPFV3_FAILURE;
        *pi4SendSubBulkEvent = OSIX_SUCCESS;
    }
    else
    {
        i4SendUpdate = OSPFV3_SUCCESS;
        *pi4SendSubBulkEvent = OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedLsuSendLsu\r\n");
    return i4SendUpdate;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendBulkUpdatesInCxt                              */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This scans the LSA in the context     */
/*                and constructs the bulk update packet                     */
/*                                                                          */
/* Input        : pV3OspfCxt    -  Pointer to Context                       */
/*                areaId        -  area id of last tx area scope LSA        */
/*                lsaId         -  LSA identifier                           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : pointer to last tx LSU / NULL                             */
/*                                                                          */
/****************************************************************************/

PRIVATE tV3OsLsaInfo *
O3RedLsuSendBulkUpdatesInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId areaId,
                              tV3OsLsaId lsaId)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pAreaNode = NULL;
    tV3OsArea          *pArea = NULL;
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2PktLen = 0;
    UINT1               u1FloodScope = 0;
    INT1                i1AreaComp = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendBulkUpdatesInCxt\n");

    /* This functions contructs and transmits the LSU bulk updates in the
     * given context. The following order is used for bulk update
     * construction
     *
     * 1. Scan all areas in context.
     * 2. If the scan area id is less than given area id, get the next area
     * 3. If the LSA type is not link scope, then the interfaces in this area
     *    are not scanned. Scan all the interfaces and send bulk updates.
     * 4. If LSA type is link scope, then link scope LSA are scanned in this
     *    area. Scan all the area scope LSA and send bulk updates.
     * 5. If LSA type is AS scope, scan all the AS scope LSA in context.
     */

    u1FloodScope = V3LsuGetLsaFloodScope (lsaId.u2LsaType);

    if (u1FloodScope == OSPFV3_AS_FLOOD_SCOPE)
    {
        /* If the last tx LSU is AS scope, then all the link scope and
         * area scope LSU in this context are sent to the standby node.
         * Reture the LSA Info directly
         */

        pLsaInfo = O3RedLsuSendAsScopeUpdatesInCxt (pV3OspfCxt, &pRmMsg,
                                                    &u2PktLen);

        /* At this stage, RM Msg can be present. This occurs when all the LSA
         * are added to the RM Msg but the Msg is not full
         * Send the RM Msg to the standby
         */
        if (pRmMsg != NULL)
        {
            if (O3RedUtlSendBulkUpdate (pRmMsg, u2PktLen) == OSIX_FAILURE)
            {
                return NULL;
            }
        }

        OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                    "EXIT : O3RedLsuSendBulkUpdatesInCxt\n");

        return pLsaInfo;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pAreaNode);

        /* If the area id is less than the given area id, skip the area
         */
        i1AreaComp = V3UtilAreaIdComp (areaId, pArea->areaId);

        if (i1AreaComp == OSPFV3_GREATER)
        {
            continue;
        }
        else if (i1AreaComp == OSPFV3_EQUAL)
        {
            /* The bulk update stops after scanning all link scope LSA
             * in this area or after scanning all area scope LSA in 
             * this area.
             */
            if (u1FloodScope == OSPFV3_AREA_FLOOD_SCOPE)
            {
                /* In this case, area id is equal and the last LSU
                 * sent is an area scope LSA. Since all area scope
                 * LSU will be sent at a stretch in an area, this area
                 * would have been completed. Get the next area
                 */
                continue;
            }
            else
            {
                /* In this case, area id is equal and either LSU type is 0
                 * or the last LSU sent is a link scope LSA. Since all
                 * link scope LSU will be sent at a stretch in an area,
                 * this area's link scope LSU would have been completed.
                 * Scan the area scope LSU. So, set the flood scope as
                 * area scope
                 */
                if (lsaId.u2LsaType == 0)
                {
                    u1FloodScope = OSPFV3_LINK_FLOOD_SCOPE;
                }
                else
                {
                    u1FloodScope = OSPFV3_AREA_FLOOD_SCOPE;
                }
            }
        }
        else
        {
            /* New area. So, we have to scan the link scope LSA */
            u1FloodScope = OSPFV3_LINK_FLOOD_SCOPE;
        }

        /* During bulk update, relinquish happens after sending all the link
         * scope LSA from all the interfaces in this area or after sending all
         * area scope LSA in this area. If the flood scope of last LSA
         * is link level, then scan all interfaces in this area, else scan
         * all area scope LSA
         */
        if (u1FloodScope == OSPFV3_LINK_FLOOD_SCOPE)
        {
            pLsaInfo = O3RedLsuSendLinkScopeUpdatesInArea
                (pArea, &pRmMsg, &u2PktLen);

            if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus
                == OSPFV3_BULK_UPDT_ABORTED)
            {
                pRmMsg = NULL;
                u2PktLen = 0;
                return NULL;
            }

            if (O3RedUtlBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                if (O3RedUtlSendBulkUpdate (pRmMsg, u2PktLen) == OSIX_FAILURE)
                {
                    return NULL;
                }

                pRmMsg = NULL;
                u2PktLen = 0;
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : O3RedLsuSendBulkUpdatesInCxt\n");

                return pLsaInfo;
            }
        }

        pLsaInfo = O3RedLsuSendAreaScopeUpdatesInArea
            (pArea, &pRmMsg, &u2PktLen);

        if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus
            == OSPFV3_BULK_UPDT_ABORTED)
        {
            pRmMsg = NULL;
            u2PktLen = 0;
            return NULL;
        }

        if (O3RedUtlBulkUpdtRelinquish () == OSIX_SUCCESS)
        {
            if (O3RedUtlSendBulkUpdate (pRmMsg, u2PktLen) == OSIX_FAILURE)
            {
                return NULL;
            }

            pRmMsg = NULL;
            u2PktLen = 0;
            OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                        "EXIT : O3RedLsuSendBulkUpdatesInCxt\n");

            return pLsaInfo;
        }
    }

    /* Send the link scope LSA for virtual links */
    if (O3RedLsuSendVirtualLsuInCxt (pV3OspfCxt, &pRmMsg, &u2PktLen) == NULL)
    {
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC, pV3OspfCxt->u4ContextId,
                        "has returned NULL\r\n");
    }
    if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus == OSPFV3_BULK_UPDT_ABORTED)
    {
        pRmMsg = NULL;
        u2PktLen = 0;
        return NULL;
    }

    pLsaInfo = O3RedLsuSendAsScopeUpdatesInCxt (pV3OspfCxt, &pRmMsg, &u2PktLen);

    /* At this stage, RM Msg can be present. This occurs when all the LSA
     * are added to the RM Msg but the Msg is not full
     * Send the RM Msg to the standby
     */
    if (pRmMsg != NULL)
    {
        if (O3RedUtlSendBulkUpdate (pRmMsg, u2PktLen) == OSIX_FAILURE)
        {
            return NULL;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuSendBulkUpdatesInCxt\n");

    return pLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendLinkScopeUpdatesInArea                        */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This scans the link scope LSA in the  */
/*                area and constructs the bulk update packet                */
/*                                                                          */
/* Input        : pArea         -  Pointer to Area structure                */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to last tx LSU / NULL                             */
/*                                                                          */
/****************************************************************************/

PRIVATE tV3OsLsaInfo *
O3RedLsuSendLinkScopeUpdatesInArea (tV3OsArea * pArea, tRmMsg ** ppRmMsg,
                                    UINT2 *pu2Len)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pTmpLsaInfo = NULL;    /* Any LSA Info to be returned */
    INT4                i4RetVal = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendLinkScopeUpdatesInArea\n");

    /* Scan all the interface in the given area. In each interface, scan
     * all the link scope LSA. Construct and send the LSU bulk update
     */

    TMO_SLL_Scan (&(pArea->ifsInArea), pNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pNode);

        MEMSET (&(lsaInfo.lsaId), 0, sizeof (tV3OsLsaId));

        while ((pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                               (tRBElem *) & lsaInfo, NULL)) != NULL)
        {
            if (pTmpLsaInfo == NULL)
            {
                pTmpLsaInfo = pLsaInfo;
            }

            i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                         pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Problem occurred during bulk update transmission */
                return NULL;
            }

            /* If RM msg is NULL, then bulk update would have been sent to the
             * standby and the last LSU is too large to be appended to the pkt.
             */
            if (*ppRmMsg == NULL)
            {
                i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                             pu2Len);

                if (i4RetVal == OSIX_FAILURE)
                {
                    /* Problem occurred during bulk update transmission */
                    return NULL;
                }
            }

            /* pLsaInfo is added to update */
            MEMCPY (&(lsaInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tV3OsLsaId));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuSendLinkScopeUpdatesInArea\n");
    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendAreaScopeUpdatesInArea                        */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This scans the area scope LSA in the  */
/*                area and constructs the bulk update packet                */
/*                                                                          */
/* Input        : pArea         -  Pointer to area structure                */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to last tx LSU / NULL                             */
/*                                                                          */
/****************************************************************************/

PRIVATE tV3OsLsaInfo *
O3RedLsuSendAreaScopeUpdatesInArea (tV3OsArea * pArea, tRmMsg ** ppRmMsg,
                                    UINT2 *pu2Len)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pTmpLsaInfo = NULL;    /* Any LSA Info to be returned */
    INT4                i4RetVal = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendAreaScopeUpdatesInArea\n");
    /* Scan all the interface in the given area. In each interface, scan
     * all the link scope LSA. Construct and send the LSU bulk update
     */

    MEMSET (&(lsaInfo.lsaId), 0, sizeof (tV3OsLsaId));

    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                           (tRBElem *) & lsaInfo, NULL)) != NULL)
    {
        if (pTmpLsaInfo == NULL)
        {
            pTmpLsaInfo = pLsaInfo;
        }

        i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg, pu2Len);

        if (i4RetVal == OSIX_FAILURE)
        {
            /* Problem occurred during bulk update transmission */
            return NULL;
        }

        /* If RM msg is NULL, then bulk update would have been sent to the
         * standby and the last LSU is too large to be appended to the pkt.
         */
        if (*ppRmMsg == NULL)
        {
            i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                         pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Problem occurred during bulk update transmission */
                return NULL;
            }
        }

        /* pLsaInfo is added to update */
        MEMCPY (&(lsaInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tV3OsLsaId));
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuSendAreaScopeUpdatesInArea\n");
    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendVirtualLsuInCxt                               */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This scans the link scope LSA in the  */
/*                virtual links in the given context area and constructs    */
/*                the bulk update packet                                    */
/*                                                                          */
/* Input        : pV3OspfCxt    -  Pointer to Context structure             */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to last tx LSU / NULL                             */
/*                                                                          */
/****************************************************************************/

PRIVATE tV3OsLsaInfo *
O3RedLsuSendVirtualLsuInCxt (tV3OspfCxt * pV3OspfCxt, tRmMsg ** ppRmMsg,
                             UINT2 *pu2Len)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pTmpLsaInfo = NULL;    /* Any LSA Info to be returned */
    INT4                i4RetVal = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendVirtualLsuInCxt\n");
    /* Scan all the interface in the given area. In each interface, scan
     * all the link scope LSA. Construct and send the LSU bulk update
     */

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                          pNode);

        MEMSET (&(lsaInfo.lsaId), 0, sizeof (tV3OsLsaId));

        while ((pLsaInfo = (tV3OsLsaInfo *)
                RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                               (tRBElem *) & lsaInfo, NULL)) != NULL)
        {
            if (pTmpLsaInfo == NULL)
            {
                pTmpLsaInfo = pLsaInfo;
            }

            i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                         pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Problem occurred during bulk update transmission */
                return NULL;
            }

            /* If RM msg is NULL, then bulk update would have been sent to the
             * standby and the last LSU is too large to be appended to the pkt.
             */
            if (*ppRmMsg == NULL)
            {
                i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                             pu2Len);

                if (i4RetVal == OSIX_FAILURE)
                {
                    /* Problem occurred during bulk update transmission */
                    return NULL;
                }
            }

            MEMCPY (&(lsaInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tV3OsLsaId));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuSendVirtualLsuInCxt\n");

    return pTmpLsaInfo;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendAsScopeUpdatesInCxt                           */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This scans the AS scope LSA in the    */
/*                context and constructs the bulk update packet             */
/*                                                                          */
/* Input        : pV3OspfCxt    -  Pointer to context                       */
/*                pRmMsg        -  Pointer to RM message                    */
/*                pu2Len        -  Length of the RM message                 */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : pointer to last tx LSU / NULL                             */
/*                                                                          */
/****************************************************************************/

PRIVATE tV3OsLsaInfo *
O3RedLsuSendAsScopeUpdatesInCxt (tV3OspfCxt * pV3OspfCxt, tRmMsg ** ppRmMsg,
                                 UINT2 *pu2Len)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pPrevLsaInfo = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT1               u1LsaCount = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendAsScopeUpdatesInCxt\n");

    MEMCPY (&(lsaInfo.lsaId), &(gV3OsRtr.ospfRedInfo.lastLsaInfo.lsaId),
            sizeof (tV3OsLsaId));

    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                           &(lsaInfo), NULL)) != NULL)
    {
        i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg, pu2Len);

        if (i4RetVal == OSIX_FAILURE)
        {
            /* Problem occurred during bulk update transmission */
            return NULL;
        }

        /* If RM msg is NULL, then bulk update would have been sent to the
         * standby and the last LSU is too large to be appended to the pkt.
         */
        if (*ppRmMsg == NULL)
        {
            i4RetVal = O3RedLsuConstructAndSendBulkUpdt (pLsaInfo, ppRmMsg,
                                                         pu2Len);

            if (i4RetVal == OSIX_FAILURE)
            {
                /* Problem occurred during bulk update transmission */
                return NULL;
            }
        }

        /* pLsaInfo is added to update */
        pPrevLsaInfo = pLsaInfo;
        MEMCPY (&(lsaInfo.lsaId), &(pLsaInfo->lsaId), sizeof (tV3OsLsaId));
        u1LsaCount++;

        /* If the count reaches a minimum, check for the time stamp. This
         * count is used to avoid checking the time stamp after sending each
         * LSA
         */
        if (u1LsaCount == OSPFV3_MAX_AS_LSA_PER_BULK_UPDT)
        {
            /* Get the system time and check the time stamp. If time stamp
             * exceeds, send the LSA
             */
            if (O3RedUtlBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                if (O3RedUtlSendBulkUpdate (*ppRmMsg, *pu2Len) == OSIX_FAILURE)
                {
                    *ppRmMsg = NULL;
                    *pu2Len = 0;
                    return NULL;
                }
                *ppRmMsg = NULL;
                *pu2Len = 0;
                OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                            "EXIT : O3RedLsuSendAsScopeUpdatesInCxt\n");

                return pPrevLsaInfo;
            }

            u1LsaCount = 0;
        }
    }

    /* All AS scope LSA are sent to the standby node */
    return NULL;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuConstructAndSendBulkUpdt                          */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                by the active node. This functiosn contructs and sends    */
/*                the LSU bulk update                                       */
/*                                                                          */
/* Input        : pLsaInfo      -  Pointer to LSA                           */
/*                pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Output       : pRmMsg        -  Address of pointer to RM message         */
/*                pu2Len        -  Address of pointer to length             */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
O3RedLsuConstructAndSendBulkUpdt (tV3OsLsaInfo * pLsaInfo, tRmMsg ** ppRmMsg,
                                  UINT2 *pu2Len)
{
    INT4                i4SendUpdate = OSIX_TRUE;    /* Indicates whether bulk
                                                     * update needs to be sent
                                                     * or not
                                                     */
    UINT1               u1MsgType = (UINT1) RM_BULK_UPDATE_MSG;
    UINT1               u1MsgSubType = (UINT1) OSPFV3_RED_BULK_LSU;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuConstructAndSendBulkUpdt\n");

    /* Allocate the RM message and fill the header */
    if (*ppRmMsg == NULL)
    {
        *pu2Len = 0;

        *ppRmMsg = RM_ALLOC_TX_BUF ((UINT2) OSPFV3_RED_MTU_SIZE);

        if (*ppRmMsg == NULL)
        {
            OSPFV3_EXT_TRC (OSPFV3_RM_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                            "LSU bulk update allocation failed");
            O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_MEMALLOC_FAIL);
            return OSIX_FAILURE;
        }

        OSPFV3_RED_PUT_1_BYTE (*ppRmMsg, *pu2Len, u1MsgType);
        OSPFV3_RED_PUT_2_BYTE (*ppRmMsg, *pu2Len, 0);
        /* Add the sub bulk update message type */
        OSPFV3_RED_PUT_1_BYTE (*ppRmMsg, *pu2Len, u1MsgSubType);
    }

    /* If it is a self-originated LSA and LSA Desc is NULL, then do
     * not send the LSU. This occurs when an invalid self-originated
     * LSA is received from the neighbor and the LSA is about to be
     * flushed
     */
    if ((OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) &&
        (pLsaInfo->pLsaDesc == NULL))
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "EXIT : O3RedLsuConstructAndSendBulkUpdt\n");

        return OSIX_SUCCESS;
    }

    /* Do not send Max aged LSA */
    if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "EXIT : O3RedLsuConstructAndSendBulkUpdt\n");

        return OSIX_SUCCESS;
    }

    /* Construct the packet and send to the standby node */
    i4SendUpdate = O3RedLsuConstructLsuUpdt (pLsaInfo, *ppRmMsg, pu2Len);

    if (i4SendUpdate == OSPFV3_TRUE)
    {
        /* This packet is completely filled. Send the packet to standby
         * node and set ppRmMsg to NULL. If this pointer is set to NULL,
         * the message will be created suring the next LSU construction
         */
        if (V3OspfEnqMsgToRm (*ppRmMsg, *pu2Len) == OSIX_FAILURE)
        {
            OSPFV3_EXT_TRC (OSPFV3_RM_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                            " LSU bulk update send to RM failed");
            O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
            *ppRmMsg = NULL;
            *pu2Len = OSPFV3_ZERO;
            return OSIX_FAILURE;
        }
        *ppRmMsg = NULL;
        *pu2Len = OSPFV3_ZERO;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuConstructAndSendBulkUpdt\n");

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuConstructLsuUpdt                                  */
/*                                                                          */
/* Description  : This function is called during the bulk update process    */
/*                or dynamic sync-up by the active node. This function      */
/*                constructs the bulk update packet based on the LSA info   */
/*                passed                                                    */
/*                                                                          */
/* Input        : pLsaInfo    -     pointer to LSA Info                     */
/*                                                                          */
/* Output       : pRmMsg      -     pointer to RM Message                   */
/*                pu2Len      -     Length of the message                   */
/*                                                                          */
/* Returns      : OSIX_TRUE / OSIX_FALSE                                    */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedLsuConstructLsuUpdt (tV3OsLsaInfo * pLsaInfo, tRmMsg * pRmMsg,
                          UINT2 *pu2Len)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       destRtrId;
    UINT4               u4ContextId = 0;
    UINT4               u4StorageId = 0;
    UINT2               u2Len = 0;
    UINT1               u1FloodScope = 0;
    UINT1               u1RxmtFlag = 0;
    UINT1               u1NetworkType = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuConstructLsuUpdt\r\n");

    /* Format of LSU bulk update message
     * Field              Content           size in bytes
     * Message type       Bulk update       1
     * Message Length     Length of the
     *                    packet            2
     * Sub message type   LSU sub type      1
     * LSA scope          link/area/gbl     1
     * Context Id         Context Id        4
     * Network Type       Network Type      1
     * Storage identifier IfIndex for link
     *                    area Id for area
     *                    unused for gbl    4
     * Overflow state     Overflow state    1
     * Transit area       area Id           4
     * Dest router id     router Id         4
     * Rxmt flag          TRUE / FALSE      1
     * Arrival time       LSA arrival time  4
     * Age                LSA age           2
     * Functional eq      u1FnEqvlFlag      1
     * Length             Length of LSA     2
     * LSU                LSA content       length in previous field
     * internal type      for self LSA      2
     * asso pri presence  for self LSA      1
     * asso pri           for self LSA
     *                    asso prim in
     *                    LSA Desc is added Not required for
     *                                      0x2001, 0x2002, 0x2007,
     *                                      0x0008, 0x2009,
     *                                      0x2003/0x2004 -
     *                                      size of (tV3OsSummaryParam)
     *                                      0x4005 -
     *                                      sizeof (prefix length) +
     *                                      prefix length
     */

    MEMSET (transitAreaId, 0, sizeof (tV3OsAreaId));
    MEMSET (destRtrId, 0, sizeof (tV3OsRouterId));

    /* Check whether the sizeof of of current information can be accomodated
     * in the packet
     */
    u2Len = (UINT2) (OSPFV3_FIXED_LSU_BULK_UPDT_LEN + pLsaInfo->u2LsaLen);

    if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo) &&
        (pLsaInfo->pLsaDesc != NULL))
    {
        u2Len += sizeof (pLsaInfo->pLsaDesc->u2InternalLsaType)
            + sizeof (UINT1);

        /* Get the length of the associative primitive */
        u2Len += O3RedLsuGetAssoPrimitiveLength (pLsaInfo);
    }

    if ((u2Len + *pu2Len) > (UINT2) (OSPFV3_RED_MTU_SIZE))
    {
        /* This LSA cannot be updated in this packet. Send the current
         * information to the standby node after updating the value field
         */
        RM_DATA_ASSIGN_2_BYTE (pRmMsg, OSPFV3_RED_MSG_VALUE_OFFSET, (*pu2Len));
        return OSIX_TRUE;
    }

    /* The LSA info can be added in the packet
     * Add the contents to the RM buffer and update the global
     * temporary buffer
     */
    u1FloodScope = V3LsuGetLsaFloodScope (pLsaInfo->lsaId.u2LsaType);
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len, u1FloodScope);

    u4ContextId = pLsaInfo->pV3OspfCxt->u4ContextId;
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, *pu2Len, u4ContextId);

    if (u1FloodScope == OSPFV3_LINK_FLOOD_SCOPE)
    {
        u4StorageId = pLsaInfo->pInterface->u4InterfaceId;
        u1NetworkType = pLsaInfo->pInterface->u1NetworkType;

        if (u1NetworkType == OSPFV3_IF_VIRTUAL)
        {
            MEMCPY (transitAreaId, pLsaInfo->pInterface->transitAreaId,
                    sizeof (transitAreaId));

            MEMCPY (destRtrId, pLsaInfo->pInterface->destRtrId,
                    sizeof (destRtrId));
        }
    }
    else if (u1FloodScope == OSPFV3_AREA_FLOOD_SCOPE)
    {
        u4StorageId = OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->pArea->areaId);
    }
    else
    {
        u4StorageId = 0;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len, u1NetworkType);
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, *pu2Len, u4StorageId);
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len,
                           pLsaInfo->pV3OspfCxt->bOverflowState);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, transitAreaId, *pu2Len,
                           sizeof (transitAreaId));
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, destRtrId, *pu2Len, sizeof (destRtrId));

    /* If the LSA retransmission count is greater than 0, set the byte to
     * indicate that the LSA needs to be added in the rx list
     */
    if (pLsaInfo->u4RxmtCount > OSPFV3_ZERO)
    {
        u1RxmtFlag = (UINT1) OSPFV3_TRUE;
    }
    else
    {
        u1RxmtFlag = (UINT1) OSPFV3_FALSE;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len, u1RxmtFlag);

    /* Add the arrival time and LSA age */
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, *pu2Len, pLsaInfo->u4LsaArrivalTime);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, *pu2Len, pLsaInfo->u2LsaAge);
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len, pLsaInfo->u1FnEqvlFlag);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, *pu2Len, pLsaInfo->u2LsaLen);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pLsaInfo->pLsa, *pu2Len, pLsaInfo->u2LsaLen);

    if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo) &&
        (pLsaInfo->pLsaDesc != NULL))
    {
        OSPFV3_RED_PUT_2_BYTE (pRmMsg, *pu2Len,
                               pLsaInfo->pLsaDesc->u2InternalLsaType);

        /* Add the associative primitive to the RM Msg */
        O3RedLsuAddAssoPrimitive (pRmMsg, pu2Len, pLsaInfo);
    }

    OSPFV3_EXT_TRC4 (OSPFV3_RM_TRC,
                     pLsaInfo->pV3OspfCxt->u4ContextId,
                     "Added LSU in packet : "
                     "Type : %x LSID : %x Adv : %x Length : %x\r\n",
                     pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
                     pLsaInfo->u2LsaLen);

    gV3OsRtr.ospfRedInfo.u4LsaSyncCount++;

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuConstructLsuUpdt\r\n");

    return OSIX_FALSE;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuProcessLsuBlkUpdt                                 */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update message. This function gets the LSA and adds it to */
/*                the database. Additionally, it adds the LSA to the list   */
/*                of retranmitting nbrs if the flag is set                  */
/*                                                                          */
/* Input        : pRmMsg      -   pointer to RM message                     */
/*                u2DataLen   -   Data Length                               */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedLsuProcessLsuBlkUpdt (tRmMsg * pRmMsg, UINT2 u2DataLen)
{
    tV3OsRmLsdbInfo     lsdbInfo;
    tV3OsLsHeader       lsHeader;
    UINT4               u4Error = 0;
    INT4                i4RetVal = OSIX_SUCCESS;
    UINT2               u2ReadLen = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedLsuProcessLsuBlkUpdt\r\n");

    /* Since the message type, length of the value field and sub type
     * is read, update the read length
     */
    u2ReadLen = sizeof (UINT1) + sizeof (UINT2) + sizeof (UINT1);

    while (u2ReadLen < u2DataLen)
    {
        MEMSET (&lsdbInfo, OSPFV3_ZERO, sizeof (tV3OsRmLsdbInfo));

        /* Get the next LSA info from the bulk update */
        i4RetVal = O3RedLsuGetLsuFromUpdt (pRmMsg, &lsdbInfo, &u2ReadLen);
        UNUSED_PARAM (i4RetVal);

        /* Process the LSDB information */
        if ((lsdbInfo.pu1Lsa == NULL) ||
            (O3RedLsuProcessLsu (&lsdbInfo) == OSIX_FAILURE))
        {
            if (lsdbInfo.pu1Lsa == NULL)
            {
                SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                              "Failed to allocate memory for LSU\n"));

                OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                                gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                                u4ContextId, "LSU allocation failed\r\n");

                u4Error = (UINT4) RM_MEMALLOC_FAIL;
            }
            else
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "LSU bulk update processing failed\r\n"));

                OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                                gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                                u4ContextId,
                                "LSU bulk update processing failed\r\n");

                /* Clear the summary param allocated for inter area
                 * and inter prefix LSA */

                if ((lsdbInfo.u2InternalLsaType ==
                     OSPFV3_INTER_AREA_PREFIX_LSA) ||
                    (lsdbInfo.u2InternalLsaType ==
                     OSPFV3_INTER_AREA_ROUTER_LSA))
                {
                    if (lsdbInfo.pAssoPrimitive != NULL)
                    {
                        OSPFV3_SUM_PARAM_FREE (lsdbInfo.pAssoPrimitive);
                        lsdbInfo.pAssoPrimitive = NULL;
                    }
                }

                u4Error = (UINT4) RM_PROCESS_FAIL;
                MEMSET (&lsHeader, OSPFV3_ZERO, sizeof (tV3OsLsHeader));
                V3UtilExtractLsHeaderFromLbuf (lsdbInfo.pu1Lsa, &lsHeader);
                OSPFV3_LSA_TYPE_FREE (lsHeader.u2LsaType, lsdbInfo.pu1Lsa);
                lsdbInfo.pu1Lsa = NULL;
            }

            O3RedRmSendEvent ((UINT4) RM_BULK_UPDT_ABORT, u4Error);

            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                            "O3RedLsuProcessLsuBlkUpdt aborted\r\n");
            break;
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedLsuProcessLsuBlkUpdt\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuGetLsuFromUpdt                                    */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update or dynamic update message. This function gets a    */
/*                single LSA info from the bulk update message              */
/*                                                                          */
/* Input        : pRmMsg      -   pointer to RM message                     */
/*                                                                          */
/* Output       : pLsdbInfo   -   Pointer to the LSDB info                  */
/*                pu2ReadLen  -   No of bytes read                          */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedLsuGetLsuFromUpdt (tRmMsg * pRmMsg, tV3OsRmLsdbInfo * pLsdbInfo,
                        UINT2 *pu2ReadLen)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsLsHeader       lsHeader;
    tIp6Addr            ip6Addr;
    UINT2               u2OffSet = OSPFV3_ZERO;
    UINT2               u2LsaType = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedLsuGetLsuFromUpdt\r\n");

    MEMSET (&lsHeader, OSPFV3_ZERO, sizeof (tV3OsLsHeader));
    MEMSET (&ip6Addr, OSPFV3_ZERO, sizeof (tIp6Addr));
    /* Format of LSU bulk update message
     * Field              Content           size in bytes
     * Message type       Bulk update       1
     * Message Length     Length of the
     *                    packet            2
     * Sub message type   LSU sub type      1
     * LSA scope          link/area/gbl     1
     * Context Id         Context Id        4
     * Network Type       Network Type      1
     * Storage identifier IfIndex for link
     *                    area Id for area
     *                    unused for gbl    4
     * Overflow state     Overflow state    1
     * Transit area       area Id           4
     * Dest router id     router Id         4
     * Rxmt flag          TRUE / FALSE      1
     * Arrival time       LSA arrival time  4
     * Age                LSA age           2
     * Functional eq      u1FnEqvlFlag      1
     * Length             Length of LSA     2
     * LSU                LSA content       length in previous field
     * internal type      for self LSA      2
     * LSA changed        for self LSA      1
     * asso pri presence  for self LSA      1
     * asso pri           for self LSA
     *                    asso prim in
     *                    LSA Desc is added Not required for
     *                                      0x2001, 0x2002, 0x2007,
     *                                      0x0008, 0x2009,
     *                                      0x2003/0x2004 -
     *                                      size of (tV3OsSummaryParam)
     *                                      0x4005 -
     *                                      sizeof (external route)
     */

    u2OffSet = *pu2ReadLen;
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u1FloodScope);
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u4ContextId);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u1NetworkType);
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u4StorageId);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u1OverflowState);
    OSPFV3_RED_GET_N_BYTE (pRmMsg, pLsdbInfo->transitAreaId, u2OffSet,
                           sizeof (pLsdbInfo->transitAreaId));
    OSPFV3_RED_GET_N_BYTE (pRmMsg, pLsdbInfo->destRtrId, u2OffSet,
                           sizeof (pLsdbInfo->destRtrId));
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u1RxFlag);
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u4LsaArrivalTime);
    OSPFV3_RED_GET_2_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u2LsaAge);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u1FnEqvlFlag);
    OSPFV3_RED_GET_2_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u2LsaLen);

    pV3OspfCxt = V3UtilOspfGetCxt (pLsdbInfo->u4ContextId);

    pLsdbInfo->pu1Lsa = NULL;
    RM_GET_DATA_N_BYTE (pRmMsg, &u2LsaType,
                        (u2OffSet + sizeof (UINT2)), sizeof (UINT2));
    u2LsaType = OSIX_NTOHS (u2LsaType);
    if (pV3OspfCxt != NULL)
    {
        OSPFV3_LSA_TYPE_ALLOC (u2LsaType, &(pLsdbInfo->pu1Lsa));
    }
    if (pLsdbInfo->pu1Lsa == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      " LSA allocation failed "
                      "for context : %d and Length : %d\r\n",
                      pLsdbInfo->u4ContextId, pLsdbInfo->u2LsaLen));

        OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                         gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                         u4ContextId,
                         " LSA allocation failed "
                         "for context : %d and Length : %d\r\n",
                         pLsdbInfo->u4ContextId, pLsdbInfo->u2LsaLen);
        gu4O3RedLsuGetLsuFromUpdtFail++;
        return OSIX_FAILURE;
    }

    MEMSET (pLsdbInfo->pu1Lsa, OSPFV3_ZERO, pLsdbInfo->u2LsaLen);
    OSPFV3_RED_GET_N_BYTE (pRmMsg, pLsdbInfo->pu1Lsa, u2OffSet,
                           pLsdbInfo->u2LsaLen);
    /* Get the associate primitive for self-originated LSA */
    V3UtilExtractLsHeaderFromLbuf (pLsdbInfo->pu1Lsa, &lsHeader);
    if (V3UtilRtrIdComp (lsHeader.advRtrId, pV3OspfCxt->rtrId) == OSPFV3_EQUAL)
    {
        OSPFV3_RED_GET_2_BYTE (pRmMsg, u2OffSet, pLsdbInfo->u2InternalLsaType);

        /* Update the summary parameter in LSDB info */
        if (O3RedLsuUpdateAssoProimitive (pV3OspfCxt, pRmMsg, &u2OffSet,
                                          pLsdbInfo) == OSIX_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "LSU associative primitive addition failed\r\n"));

            OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                            gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                            u4ContextId,
                            "LSU associative primitive addition failed\r\n");
            OSPFV3_LSA_TYPE_FREE (lsHeader.u2LsaType, pLsdbInfo->pu1Lsa);
            pLsdbInfo->pu1Lsa = NULL;
            gu4O3RedLsuGetLsuFromUpdtFail++;
            return OSIX_FAILURE;
        }
    }

    *pu2ReadLen = u2OffSet;
    gV3OsRtr.ospfRedInfo.u4LsaSyncCount++;

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedLsuGetLsuFromUpdt\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuProcessLsu                                        */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update message. This function process the LSA and adds    */
/*                it to the databse. If the Rxmt flag is set, then add the  */
/*                LSA to all the nbr's rxmt list                            */
/*                                                                          */
/* Input        : pLsdbInfo   -   Pointer to the LSDB info                  */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedLsuProcessLsu (tV3OsRmLsdbInfo * pLsdbInfo)
{
    tV3OsLsHeader       lsHeader;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsAreaId         areaId;
    UINT4               u4LsaId = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedLsuProcessLsu\r\n");

    /* This function is added with reference to RFC 2328 section 13
     * and V3LsuProcessLsa
     */
    MEMSET (&areaId, OSPFV3_ZERO, sizeof (tV3OsAreaId));
    MEMSET (&lsHeader, OSPFV3_ZERO, sizeof (tV3OsLsHeader));
    V3UtilExtractLsHeaderFromLbuf (pLsdbInfo->pu1Lsa, &lsHeader);

    /* Get the LSA flood scope and Storage Id from tV3OsRmLsdbInfo
     * Storage Id identifies the structure where the LSA is stored.
     * Link scope LSA    -    Interface Id
     * Area scope LSA    -    Area Id
     * AS scope LSA      -    unused
     */
    if (pLsdbInfo->u1FloodScope == OSPFV3_LINK_FLOOD_SCOPE)
    {
        if (pLsdbInfo->u1NetworkType != OSPFV3_IF_VIRTUAL)
        {
            pInterface = V3GetFindIf (pLsdbInfo->u4StorageId);
        }
        else
        {
            pV3OspfCxt = V3UtilOspfGetCxt (pLsdbInfo->u4ContextId);

            if (pV3OspfCxt != NULL)
            {
                pInterface = V3GetFindVirtIfInCxt
                    (pV3OspfCxt, &(pLsdbInfo->transitAreaId),
                     &(pLsdbInfo->destRtrId));
            }
        }

        if (pInterface == NULL)
        {
            OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                             gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                             u4ContextId,
                             "OSPFv3 Associated If %d Not Found\r\n",
                             pLsdbInfo->u4StorageId);
            gu4O3RedLsuProcessLsuFail++;
            return OSIX_FAILURE;
        }
        else
        {
            pArea = pInterface->pArea;
        }
    }
    else if (pLsdbInfo->u1FloodScope == OSPFV3_AREA_FLOOD_SCOPE)
    {
        OSPFV3_BUFFER_DWTOPDU (areaId, pLsdbInfo->u4StorageId);
        pV3OspfCxt = V3UtilOspfGetCxt (pLsdbInfo->u4ContextId);

        if (pV3OspfCxt != NULL)
        {
            pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);
        }

        if (pArea == NULL)
        {
            OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                             gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                             u4ContextId,
                             "OSPFv3 Associated area %x Not Found\r\n",
                             pLsdbInfo->u4StorageId);
            gu4O3RedLsuProcessLsuFail++;
            return OSIX_FAILURE;
        }
    }
    else
    {
        pV3OspfCxt = V3UtilOspfGetCxt (pLsdbInfo->u4ContextId);

        if (pV3OspfCxt != NULL)
        {
            pArea = pV3OspfCxt->pBackbone;
        }
        else
        {
            OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                             gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                             u4ContextId,
                             "OSPFv3 Associated context %d Not Found\r\n",
                             pLsdbInfo->u4ContextId);
            gu4O3RedLsuProcessLsuFail++;
            return OSIX_FAILURE;
        }
    }

    pV3OspfCxt = V3UtilOspfGetCxt (pLsdbInfo->u4ContextId);

    if (pV3OspfCxt == NULL)
    {
        gu4O3RedLsuProcessLsuFail++;
        return OSIX_FAILURE;
    }

    pLsaInfo = V3LsuSearchDatabase (lsHeader.u2LsaType,
                                    &(lsHeader.linkStateId),
                                    &(lsHeader.advRtrId), pInterface, pArea);

    /* Check the Age of LSA is MAX AGE */
    if (OSPFV3_IS_MAX_AGE (pLsdbInfo->u2LsaAge))
    {
        if (pLsaInfo != NULL)
        {
            /* If the LSA is to be added in the rxmt list, do not delete
             * the LSA from the DB. Add the LSA in the rxmt list
             */

            if ((pLsaInfo->pLsaDesc != NULL) &&
                (pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired != OSIX_TRUE))
            {
                V3TmrDeleteTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer));
                pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
            }

            pLsaInfo->u2LsaAge = pLsdbInfo->u2LsaAge;

            if ((pLsdbInfo->u1RxFlag == OSPFV3_TRUE) &&
                ((O3RedRmAddToRxmtLst (pLsaInfo)) == OSIX_TRUE))
            {
                OSPFV3_EXT_TRC3
                    (OSPFV3_RM_TRC,
                     pLsaInfo->pV3OspfCxt->u4ContextId,
                     "Added Max age LSA in rxmt list "
                     "to LSDB :%x %x %x\r\n", pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
            }
            else
            {
                V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);
            }
        }
        OSPFV3_LSA_TYPE_FREE (lsHeader.u2LsaType, pLsdbInfo->pu1Lsa);
        pLsdbInfo->pu1Lsa = NULL;

        /* Clear the summary param allocated for inter area
         * and inter prefix LSA */

        if ((pLsdbInfo->u2InternalLsaType ==
             OSPFV3_INTER_AREA_PREFIX_LSA) ||
            (pLsdbInfo->u2InternalLsaType == OSPFV3_INTER_AREA_ROUTER_LSA))
        {
            if (pLsdbInfo->pAssoPrimitive != NULL)
            {
                OSPFV3_SUM_PARAM_FREE (pLsdbInfo->pAssoPrimitive);
                pLsdbInfo->pAssoPrimitive = NULL;
            }
        }

        return OSIX_SUCCESS;
    }

    /* Skip steps 1, 2, 3 as the LSA would have been validated in the
     * active node. Skip step 4 for processing the Max age LSA as during
     * bulk update max aged LSA will not be sent by the active node.
     * Once the LSA has reached max age, it will be deleted from
     * the database. In Step 5, the validation check performed in sub-
     * section a,b are not performed as it is vallidated in the active
     * node and the received LSA will either be a new LSA or the LSA
     * to be over-written (LSA to be over-written case occurrs when
     * the same LSA has been received through dynamic sync-up during
     * LSU bulk update process). 5-c is performed to remove the LSA
     * from all the re-transmission list
     */

    if (pLsaInfo != NULL)
    {
        V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_TRUE);
        pLsaInfo = V3LsuSearchDatabase (lsHeader.u2LsaType,
                                        &(lsHeader.linkStateId),
                                        &(lsHeader.advRtrId),
                                        pInterface, pArea);
    }

    /* 5-d Add the new LSA / replace the existing LSA to the DB
     * Route calculation timers will not be started in standby node
     * during bulk update in progress
     */
    if (pLsaInfo == NULL)
    {
        /* LSA copy is new, add the LSA to the Databse */
        pLsaInfo = V3LsuAddToLsdb (pArea, lsHeader.u2LsaType,
                                   &(lsHeader.linkStateId),
                                   &(lsHeader.advRtrId),
                                   pInterface, pLsdbInfo->pu1Lsa);
    }
    if (pLsaInfo == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "LSA Addition failed \r\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, "LSA Addition failed \r\n");
        gu4O3RedLsuProcessLsuFail++;
        return OSIX_FAILURE;
    }

    else if (pLsaInfo != NULL)
    {
        /* If the LSA is self-originated, then construct the LSA
         * descriptor and add to the databse before installing the
         * LSA
         */
        if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
        {
            O3RedRmConstructLsaDesc (pLsaInfo, pLsdbInfo, pInterface, pArea);
        }

        if (!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) ||
            (pLsaInfo->pLsaDesc != NULL))
        {
            V3LsuInstallLsa (pLsdbInfo->pu1Lsa, pLsaInfo,
                             pLsdbInfo->u1FnEqvlFlag);

            pLsaInfo->u1FnEqvlFlag = pLsdbInfo->u1FnEqvlFlag;
            /* Update the LSA age from the structure */
            pLsaInfo->u2LsaAge = pLsdbInfo->u2LsaAge;

            if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
            {
                /* Updating the internal LSA type as per origination thread */
                if ((pLsdbInfo->u2InternalLsaType == OSPFV3_AS_EXT_LSA) ||
                    (pLsdbInfo->u2InternalLsaType == OSPFV3_COND_AS_EXT_LSA))
                {
                    pLsaInfo->pLsaDesc->pLsaInfo->u1TrnsltType5
                        = OSPFV3_REDISTRIBUTED_TYPE5;
                }
                else if ((pLsdbInfo->u2InternalLsaType
                          == OSPFV3_AS_TRNSLTD_EXT_LSA) ||
                         (pLsdbInfo->u2InternalLsaType
                          == OSPFV3_AS_TRNSLTD_RNG_LSA))
                {
                    pLsaInfo->pLsaDesc->pLsaInfo->u1TrnsltType5 = OSIX_TRUE;
                }

                /* In case of external LSA, pLsaInfo should be added as a back
                 * pointer to external route
                 */
                if (pLsdbInfo->u2InternalLsaType == OSPFV3_AS_EXT_LSA)
                {
                    if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
                    {
                        ((tV3OsExtRoute *) (VOID *)
                         (pLsaInfo->pLsaDesc->pAssoPrimitive))->pLsaInfo
 = pLsaInfo;
                    }
                }

                u4LsaId = OSPFV3_BUFFER_DWFROMPDU (lsHeader.linkStateId);

                O3RedUtlSetLsaId (u4LsaId, pLsdbInfo, pLsaInfo);

                /* Set the link state id in the associative primitive */
                O3RedUtlSetAssoLsaId (lsHeader.linkStateId, pLsdbInfo,
                                      pLsaInfo);
            }

        }
        else
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to allocate memory for LSA Desc\r\n"));

            OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                            gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                            u4ContextId, "pLsaDesc allocation failed\r\n");
            gu4O3RedLsuProcessLsuFail++;
            return OSIX_FAILURE;
        }
    }
    else
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "pLsaInfo is NULL\r\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, "pLsaInfo is NULL\r\n");
        gu4O3RedLsuProcessLsuFail++;
        return OSIX_FAILURE;
    }

    /* Skip 5-e LSA Ack need not be sent by the standby node
     * 5-f seld-originated LSA is handled above. Steps 6,7,8 involves
     * DDP exchange process and flooding of packet. So these are skipped
     * Add the LSU to the rxmt list of all nbrs if the rxmt flag is set
     */
    if (pLsdbInfo->u1RxFlag == OSPFV3_TRUE)
    {
        O3RedRmAddToRxmtLst (pLsaInfo);
    }

    if ((pLsdbInfo->u1OverflowState == OSIX_TRUE) &&
        (pV3OspfCxt->bOverflowState != OSIX_TRUE))
    {
        V3RtrEnterOverflowStateInCxt (pV3OspfCxt);
    }

    pV3OspfCxt->bOverflowState = pLsdbInfo->u1OverflowState;

    OSPFV3_EXT_TRC4 (OSPFV3_RM_TRC,
                     pLsaInfo->pV3OspfCxt->u4ContextId,
                     "LSU Added "
                     "to LSDB :%x %x %x %x\r\n", pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
                     pLsaInfo->lsaSeqNum);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedLsuProcessLsu\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuSendSelfOriginatedLsa                             */
/*                                                                          */
/* Description  : This function is called when during standby to active     */
/*                This functions scans the LSA decs in all the context      */
/*                If LSA changed flag is set to true, then send the LSA     */
/*                This occurs when LSA needs to be originated in active     */
/*                node but min LSA interval timer has not been expired.     */
/*                                                                          */
/* Input        : pV3OspfCxt     -   Context pointer                        */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedLsuSendSelfOriginatedLsa (tV3OspfCxt * pV3OspfCxt)
{
    tTMO_DLL_NODE      *pLstNode = NULL;
    tV3OsLsaDesc       *pLsaDesc = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tIp6Addr            ip6Addr;
    UINT1               u1PrefixLen = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuSendSelfOriginatedLsa\n");

    /* If the context is in overflow state, flush all the self originated
     * LSA
     */
    if (pV3OspfCxt->bOverflowState == OSIX_TRUE)
    {
        V3LsuFlushAllNonDefSelfOrgAseLsaInCxt (pV3OspfCxt);
    }

    if ((!OSPFV3_IS_IN_OVERFLOW_STATE (pV3OspfCxt)) &&
        (OSPFV3_IS_EXT_LSDB_SIZE_LIMITED (pV3OspfCxt)) &&
        (pV3OspfCxt->u4NonDefAsLsaCount >= (UINT4) pV3OspfCxt->i4ExtLsdbLimit))
    {
        V3RtrEnterOverflowStateInCxt (pV3OspfCxt);
    }

    TMO_DLL_Scan (&(pV3OspfCxt->lsaDescLst), pLstNode, tTMO_DLL_NODE *)
    {
        pLsaDesc = OSPFV3_GET_BASE_PTR (tV3OsLsaDesc, nextLsaDesc, pLstNode);

        pLsaDesc->u1LsaChanged = OSIX_TRUE;

        /* If the associative primitive is NULL for LSA having
         * dynamic associative primitive
         */
        if (pLsaDesc->pAssoPrimitive == NULL)
        {
            if ((pLsaDesc->u2InternalLsaType == OSPFV3_AS_EXT_LSA) ||
                (pLsaDesc->u2InternalLsaType == OSPFV3_NSSA_LSA) ||
                (pLsaDesc->u2InternalLsaType == OSPFV3_DEFAULT_NSSA_LSA) ||
                (pLsaDesc->u2InternalLsaType == OSPFV3_AS_TRNSLTD_EXT_LSA))
            {
                u1PrefixLen = *(pLsaDesc->pLsaInfo->pLsa +
                                OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET);

                MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
                OSPFV3_IP6_ADDR_PREFIX_COPY
                    (ip6Addr,
                     *(pLsaDesc->pLsaInfo->pLsa +
                       OSPFV3_AS_EXT_LSA_PREF_OFFSET), u1PrefixLen);

                switch (pLsaDesc->u2InternalLsaType)
                {
                    case OSPFV3_DEFAULT_NSSA_LSA:
                    {
                        if (OSPFV3_IS_AREA_BORDER_RTR
                            (pLsaDesc->pLsaInfo->pV3OspfCxt))
                        {
                            break;
                        }
                    }
                    case OSPFV3_AS_EXT_LSA:
                    case OSPFV3_NSSA_LSA:
                    {
                        pLsaDesc->pAssoPrimitive =
                            (UINT1 *) V3ExtrtFindRouteInCxt
                            (pV3OspfCxt, &ip6Addr, u1PrefixLen);

                        if (pLsaDesc->pAssoPrimitive != NULL)
                        {
                            ((tV3OsExtRoute *) (VOID *) pLsaDesc->
                             pAssoPrimitive)->pLsaInfo = pLsaDesc->pLsaInfo;

                            MEMCPY (((tV3OsExtRoute *) (VOID *)
                                     (pLsaDesc->pAssoPrimitive))->
                                    linkStateId,
                                    pLsaDesc->pLsaInfo->lsaId.linkStateId,
                                    sizeof (tV3OsLinkStateId));
                        }
                        break;
                    }
                    case OSPFV3_AS_TRNSLTD_EXT_LSA:
                    {
                        pLsaDesc->pAssoPrimitive = (UINT1 *)
                            V3RtcFindRtEntryInCxt
                            (pV3OspfCxt, (VOID *) &(ip6Addr),
                             u1PrefixLen, OSPFV3_DEST_NETWORK);

                        if (pLsaDesc->pAssoPrimitive != NULL)
                        {
                            MEMCPY (((tV3OsRtEntry *) (VOID *)
                                     (pLsaDesc->pAssoPrimitive))->
                                    linkStateId,
                                    pLsaDesc->pLsaInfo->lsaId.linkStateId,
                                    sizeof (tV3OsLinkStateId));
                        }
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
        }

        /* Update the LSA Id in the routing entry for inter area
         * prefix LSA
         */
        if (pLsaDesc->u2InternalLsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
        {
            u1PrefixLen = *(pLsaDesc->pLsaInfo->pLsa +
                            OSPFV3_INTER_AREA_PREF_LEN_OFFSET);

            MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
            OSPFV3_IP6_ADDR_PREFIX_COPY
                (ip6Addr,
                 *(pLsaDesc->pLsaInfo->pLsa +
                   OSPFV3_INTER_AREA_PREF_OFFSET), u1PrefixLen);

            pRtEntry = V3RtcFindRtEntryInCxt
                (pV3OspfCxt, (VOID *) &(ip6Addr),
                 u1PrefixLen, OSPFV3_DEST_NETWORK);

            if (pRtEntry != NULL)
            {
                MEMCPY (pRtEntry->linkStateId,
                        pLsaDesc->pLsaInfo->lsaId.linkStateId,
                        sizeof (tV3OsLinkStateId));
            }
        }

        /* Do not re-grenerate the LSA if the LSA is aged or if the
         * context is in overflow state and the LSA is as-external
         */
        if (OSPFV3_IS_MAX_AGE (pLsaDesc->pLsaInfo->u2LsaAge))
        {
            pLsaDesc->pLsaInfo->u1FloodFlag = OSIX_TRUE;
            V3AgdFlushOut (pLsaDesc->pLsaInfo);
            continue;
        }

        if (pLsaDesc->u1MinLsaIntervalExpired == OSIX_TRUE)
        {
            if (pLsaDesc->u2InternalLsaType == OSPFV3_INTRA_AREA_PREFIX_LSA)
            {
                V3ResetMinLsaForAllIAPrefixLsa (pLsaDesc->pLsaInfo->pArea);
            }

            pLsaDesc->u1LsaChanged = OSIX_TRUE;
            pLsaDesc->u1MinLsaIntervalExpired = OSIX_FALSE;
            V3TmrSetTimer (&(pLsaDesc->minLsaIntervalTimer),
                           OSPFV3_MIN_LSA_INTERVAL_TIMER,
                           (OSPFV3_NO_OF_TICKS_PER_SEC *
                            OSPFV3_MIN_LSA_INTERVAL_VALUE));
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuSendSelfOriginatedLsa\n");

}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuGetAssoPrimitiveLength                            */
/*                                                                          */
/* Description  : This function gets the length of the associative          */
/*                attached to the LSA info                                  */
/*                                                                          */
/* Input        : pLsaInfo - Self originated LSA Info                       */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : Length of the associative primitive                       */
/*                                                                          */
/****************************************************************************/

PRIVATE UINT2
O3RedLsuGetAssoPrimitiveLength (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT2               u2Len = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuGetAssoPrimitiveLength\r\n");

    /* In some cases, while deleting the LSA from the database, the
     * associative primitive will be set to to NULL. So, if the LSA
     * age is max age and assocociative primitive is NULL, return
     * immediately
     */

    if (pLsaInfo->pLsaDesc->pAssoPrimitive == NULL)
    {
        return u2Len;
    }

    switch (pLsaInfo->pLsaDesc->u2InternalLsaType)
    {
        case OSPFV3_NETWORK_LSA:
        {
            /* This contains the interface id. For network LSA, this is used
             * for associative primitive.
             */
            u2Len = sizeof (UINT4);
            break;
        }
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            u2Len = sizeof (tV3OsSummaryParam);
            break;
        }
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (OSPFV3_IS_AREA_BORDER_RTR (pLsaInfo->pV3OspfCxt))
            {
                break;
            }
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        {
            pExtRoute =
                (tV3OsExtRoute *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;
            u2Len =
                (UINT2) (sizeof (UINT1) +
                         OSPFV3_GET_PREFIX_LEN_IN_BYTE (pExtRoute->
                                                        u1PrefixLength));
            break;
        }
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_COND_NSSA_LSA:
        {
            /* Index : Network, prefix length and area id */
            u2Len = sizeof (tIp6Addr) + sizeof (UINT1) + sizeof (tV3OsAreaId);
            break;
        }
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        {
            /* Index : area id, lsdb type, prefix, prefix length */
            u2Len = sizeof (tV3OsAreaId) + +sizeof (tIp6Addr) + sizeof (UINT1);
            break;
        }
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        {
            /* Index : Destination address and prefix length */
            u2Len = sizeof (tIp6Addr) + sizeof (UINT1);
            break;
        }
        default:
        {
            break;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuGetAssoPrimitiveLength\r\n");

    return u2Len;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuAddAssoPrimitive                                  */
/*                                                                          */
/* Description  : This function adds the associative primitive to the       */
/*                RM message                                                */
/*                                                                          */
/* Input        : pRmMsg   - RM message                                     */
/*                pu2Len   - Message offset                                 */
/*                pLsaInfo - LSA info to be added                           */
/*                                                                          */
/* Output       : pu2Len   - Message offset                                 */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
O3RedLsuAddAssoPrimitive (tRmMsg * pRmMsg, UINT2 *pu2Len,
                          tV3OsLsaInfo * pLsaInfo)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsAsExtAddrRange *pAsAddrRange = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsAreaId         areaId;
    UINT2               u2Len = *pu2Len;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuAddAssoPrimitive\r\n");

    /* In some cases, while deleting the LSA from the database, the
     * associative primitive will be set to to NULL. So, if the LSA
     * age is max age and assocociative primitive is NULL, return
     * immediately
     */
    if (pLsaInfo->pLsaDesc->pAssoPrimitive == NULL)
    {
        OSPFV3_RED_PUT_1_BYTE (pRmMsg, *pu2Len, OSIX_FALSE);
        return OSIX_SUCCESS;
    }
    else
    {
        OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, OSIX_TRUE);
    }

    switch (pLsaInfo->pLsaDesc->u2InternalLsaType)
    {
        case OSPFV3_NETWORK_LSA:
        {
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &(((tV3OsInterface *) (VOID *)
                                      (pLsaInfo->pLsaDesc->pAssoPrimitive))->
                                     u4InterfaceId), u2Len, sizeof (UINT4));
            break;
        }
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   pLsaInfo->pLsaDesc->pAssoPrimitive,
                                   u2Len, sizeof (tV3OsSummaryParam));
            break;
        }
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (OSPFV3_IS_AREA_BORDER_RTR (pLsaInfo->pV3OspfCxt))
            {
                /* Area pointer is the associative primitive */
                break;
            }
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        {
            pExtRoute =
                (tV3OsExtRoute *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;

            OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, pExtRoute->u1PrefixLength);
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &pExtRoute->ip6Prefix,
                                   u2Len,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE
                                   (pExtRoute->u1PrefixLength));
            break;
        }
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_COND_NSSA_LSA:
        {
            /* Index : Network, prefix length and area id */
            pAsAddrRange = (tV3OsAsExtAddrRange *) (VOID *)
                pLsaInfo->pLsaDesc->pAssoPrimitive;

            OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, pAsAddrRange->u1PrefixLength);
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &pAsAddrRange->ip6Addr,
                                   u2Len,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE
                                   (pAsAddrRange->u1PrefixLength));
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &pAsAddrRange->areaId,
                                   u2Len, sizeof (pAsAddrRange->areaId));
            break;
        }
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        {
            /* Index : area id, lsdb type, prefix, prefix length */
            pAddrRange =
                (tV3OsAddrRange *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;

            OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, pAddrRange->u1PrefixLength);
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &pAddrRange->ip6Addr,
                                   u2Len,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE
                                   (pAddrRange->u1PrefixLength));

            if (pAddrRange->pArea != NULL)
            {
                OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                       &pAddrRange->pArea->areaId, u2Len,
                                       sizeof (pAddrRange->pArea->areaId));
            }
            else
            {
                /* Add invalid area Id */
                MEMSET (&areaId, 'F', sizeof (tV3OsAreaId));
                OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                       &areaId, u2Len,
                                       sizeof (pAddrRange->pArea->areaId));
            }

            break;
        }
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        {
            /* Index : Destination address and prefix length */
            pRtEntry =
                (tV3OsRtEntry *) (VOID *) pLsaInfo->pLsaDesc->pAssoPrimitive;

            OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, pRtEntry->u1PrefixLen);
            OSPFV3_RED_PUT_N_BYTE (pRmMsg,
                                   &pRtEntry->destRtPrefix,
                                   u2Len,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE
                                   (pRtEntry->u1PrefixLen));
            break;
        }
        default:
        {
            *pu2Len = u2Len;
            return OSIX_FAILURE;
        }
    }

    *pu2Len = u2Len;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuAddAssoPrimitive\r\n");

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedLsuUpdateAssoProimitive                              */
/*                                                                          */
/* Description  : This function adds the associative primitive to the       */
/*                LSDB info after reading the message from RM               */
/*                                                                          */
/* Input        : pV3OspfCxt - Context pointer                              */
/*                pRmMsg     - RM message                                   */
/*                pu2Len     - Message offset                               */
/*                pLsdbInfo  - LSA info to be added                         */
/*                                                                          */
/* Output       : pu2Len   - Message offset                                 */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
O3RedLsuUpdateAssoProimitive (tV3OspfCxt * pV3OspfCxt, tRmMsg * pRmMsg,
                              UINT2 *pu2Len, tV3OsRmLsdbInfo * pLsdbInfo)
{
    tIp6Addr            ip6Addr;
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4InterfaceId = 0;
    UINT2               u2LsdbType = 0;
    UINT2               u2OffSet = *pu2Len;
    UINT1               u1PrefixLen = 0;
    UINT1               u1IsAssoPrimPresent = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : O3RedLsuUpdateAssoProimitive\r\n");

    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1IsAssoPrimPresent);

    if (u1IsAssoPrimPresent == OSIX_FALSE)
    {
        /* Associative primitive is not present */
        *pu2Len = u2OffSet;
        return OSIX_SUCCESS;
    }

    MEMSET (&ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&areaId, 0, sizeof (tV3OsAreaId));

    switch (pLsdbInfo->u2InternalLsaType)
    {
        case OSPFV3_ROUTER_LSA:
        case OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA:
        {
            OSPFV3_BUFFER_DWTOPDU (areaId, pLsdbInfo->u4StorageId);
            pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);
            pLsdbInfo->pAssoPrimitive = (UINT1 *) pArea;
            break;
        }
        case OSPFV3_INTRA_AREA_PREFIX_LSA:
        {
            /* In intra area prefix LSA, assocoative primitive contains
             * pointer to LSA. This LSA will be re-allocated during changes
             * So, the aassociative primitive is maintained as NULL in
             * standby node. During standby to active transition, LSA will
             * be re-originated and in this case, LSA will be added in the
             * associative primitive
             */
            pLsdbInfo->pAssoPrimitive = NULL;
            break;
        }
        case OSPFV3_NETWORK_LSA:
        {
            OSPFV3_RED_GET_N_BYTE
                (pRmMsg, &u4InterfaceId, u2OffSet, sizeof (UINT4));

            pInterface = V3GetFindIf (u4InterfaceId);

            pLsdbInfo->pAssoPrimitive = (UINT1 *) pInterface;
            break;
        }
        case OSPFV3_LINK_LSA:
        case OSPFV3_GRACE_LSA:
        {
            pInterface = V3GetFindIf (pLsdbInfo->u4StorageId);
            pLsdbInfo->pAssoPrimitive = (UINT1 *) pInterface;
            break;
        }
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            OSPFV3_SUM_PARAM_ALLOC (&(pLsdbInfo->pAssoPrimitive));
            if (pLsdbInfo->pAssoPrimitive != NULL)
            {
                MEMSET (pLsdbInfo->pAssoPrimitive, 0,
                        sizeof (tV3OsSummaryParam));
                OSPFV3_RED_GET_N_BYTE
                    (pRmMsg, pLsdbInfo->pAssoPrimitive,
                     u2OffSet, sizeof (tV3OsSummaryParam));
            }
            else
            {
                u2OffSet += sizeof (tV3OsSummaryParam);
                *pu2Len = u2OffSet;
                return OSIX_FAILURE;
            }
            break;
        }
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (OSPFV3_IS_AREA_BORDER_RTR (pV3OspfCxt))
            {
                OSPFV3_BUFFER_DWTOPDU (areaId, pLsdbInfo->u4StorageId);
                pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);
                pLsdbInfo->pAssoPrimitive = (UINT1 *) pArea;
                break;
            }
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        {
            OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1PrefixLen);
            OSPFV3_RED_GET_N_BYTE (pRmMsg, &ip6Addr, u2OffSet,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen));

            pLsdbInfo->pAssoPrimitive =
                (UINT1 *) V3ExtrtFindRouteInCxt
                (pV3OspfCxt, &ip6Addr, u1PrefixLen);
            break;
        }
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_COND_NSSA_LSA:
        {
            /* Index : Network, prefix length and area id */
            OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1PrefixLen);
            OSPFV3_RED_GET_N_BYTE (pRmMsg, &ip6Addr, u2OffSet,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen));
            OSPFV3_RED_GET_N_BYTE (pRmMsg, &areaId, u2OffSet, sizeof (areaId));

            if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID)
                == OSPFV3_EQUAL)
            {
                pLsdbInfo->pAssoPrimitive = (UINT1 *)
                    V3GetFindAsExtRngInCxt (pV3OspfCxt, &ip6Addr,
                                            u1PrefixLen, areaId);
            }
            else
            {
                pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

                if ((pArea == NULL) || (pArea->u4AreaType != OSPFV3_NSSA_AREA))
                {
                    pLsdbInfo->pAssoPrimitive = NULL;
                    break;
                }

                pLsdbInfo->pAssoPrimitive = (UINT1 *)
                    V3RagGetNssaAddrRange (pArea, &ip6Addr, u1PrefixLen);

            }

            break;
        }
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        {
            /* Index : area id, lsdb type, prefix, prefix length */
            OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1PrefixLen);
            OSPFV3_RED_GET_N_BYTE (pRmMsg,
                                   &ip6Addr,
                                   u2OffSet,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen));
            OSPFV3_RED_GET_N_BYTE (pRmMsg, &areaId, u2OffSet, sizeof (areaId));

            pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId);

            if (pArea == NULL)
            {
                break;
            }

            if (pLsdbInfo->u2InternalLsaType
                == OSPFV3_COND_INTER_AREA_PREFIX_LSA)
            {
                u2LsdbType = OSPFV3_INTER_AREA_PREFIX_LSA;
            }
            else
            {
                u2LsdbType = OSPFV3_NSSA_LSA;
            }

            pLsdbInfo->pAssoPrimitive = (UINT1 *)
                V3GetFindAreaAddrRange (pArea, &ip6Addr, u1PrefixLen,
                                        u2LsdbType);
            break;
        }
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        {
            /* Index : Destination address and prefix length */
            OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1PrefixLen);
            OSPFV3_RED_GET_N_BYTE (pRmMsg, &ip6Addr,
                                   u2OffSet,
                                   OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen));

            pLsdbInfo->pAssoPrimitive = (UINT1 *)
                V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                       (VOID *) &(ip6Addr),
                                       u1PrefixLen, OSPFV3_DEST_NETWORK);

            break;
        }

        default:
        {
            break;
        }
    }

    *pu2Len = u2OffSet;
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : O3RedLsuUpdateAssoProimitive\n");
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redlsu.c                           */
/*-----------------------------------------------------------------------*/
