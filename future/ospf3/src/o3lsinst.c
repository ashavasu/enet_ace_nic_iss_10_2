/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3lsinst.c,v 1.21 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures for installing
 *             LSAs in the database
 *
 *******************************************************************/

#include "o3inc.h"

PRIVATE VOID        V3LsuAddLsaToLst
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa));

PRIVATE UINT1       V3LsuAddOrDelLsaInRBTree
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 u1AddDelFlag));

PRIVATE VOID        V3LsuAddPrefixLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable,
        UINT1 *pLsa, UINT2 u2LsaType));

PRIVATE VOID        V3LsuAddLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable,
        tV3OsRouterId * pRtrId));

PRIVATE VOID V3LsuDeleteLsaFromLst PROTO ((tV3OsLsaInfo * pLsaInfo));

PRIVATE VOID        V3LsuDeletePrefixLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable,
        UINT2 u2LsaType));

PRIVATE VOID        V3LsuDeleteLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable,
        tV3OsRouterId * pRtrId));

PRIVATE VOID V3LsuProcessDcBitResetLsa PROTO ((tV3OsLsaInfo * pLsaInfo));

PRIVATE VOID V3LsuCheckDcBitLsa PROTO ((tV3OsLsaInfo * pLsaInfo));

PRIVATE VOID V3LsuFlushDnaLsa PROTO ((tV3OsArea * pArea));

PRIVATE VOID V3LsuCheckAndFlushIndicationLsa PROTO ((tV3OsArea * pArea));

PRIVATE VOID V3LsuSetDnaFlagForLinkScopeLsa PROTO ((tV3OsInterface * pInterface,
                                                    tV3OsRouterId * pRtrId,
                                                    UINT1 u1Flag));

PRIVATE VOID        V3LsuSetDnaFlagForAreaScopeLsa
PROTO ((tV3OsArea * pArea, tV3OsRouterId * pRtrId, UINT1 u1Flag));

PRIVATE VOID        V3LsuSetDnaFlagForAsScopeLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pRtrId, UINT1 u1Flag));

PRIVATE VOID        V3LsuDeleteIntraLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable));

PRIVATE VOID        V3LsuAddIntraLsaInHashLst
PROTO ((tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable, UINT1 *pLsa));

PRIVATE VOID        V3LsuCheckHashIndex
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa));

PUBLIC INT4         V3RbWalkHandleLsuSplAgeAction
PROTO ((tRBElem * pe, eRBVisit visit, UINT4 u4level, void *pArg, void *pOut));

PRIVATE VOID V3LsuFlushIndicationLsaInArea PROTO ((tV3OsArea * pArea));

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddToLsdb                                             */
/*                                                                           */
/* Description  : This procedure allocates a new lsa_info structure sets the */
/*                lsaId fields and adds it to the RB tree and hash table.    */
/*                                                                           */
/* Input        : pArea                : pointer to AREA                     */
/*                u2LsaType            : type of LSA                         */
/*                pLinkStateId         : Link State Id                       */
/*                pAdvRtrId            : advertising router id               */
/*                pIface               : pointer to the INTERFACE            */
/*                pLsa                 : pointer to LSA                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA Info, if LSA successfully added             */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsLsaInfo *
V3LsuAddToLsdb (tV3OsArea * pArea, UINT2 u2LsaType,
                tV3OsLinkStateId * pLinkStateId, tV3OsRouterId * pAdvRtrId,
                tV3OsInterface * pIface, UINT1 *pLsa)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddToLsdb\n");

    OSPFV3_LSA_INFO_ALLOC (&(pLsaInfo));
    if (NULL == pLsaInfo)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory for LSA Info\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "LSA Info Alloc Failure\n");
        return NULL;
    }

    MEMSET (pLsaInfo, 0, (sizeof (tV3OsLsaInfo)));

    TMO_SLL_Init_Node (&(pLsaInfo->nextLsaInfo));
    pLsaInfo->lsaId.u2LsaType = u2LsaType;
    OSPFV3_LINK_STATE_ID_COPY (pLsaInfo->lsaId.linkStateId, pLinkStateId);
    OSPFV3_RTR_ID_COPY (pLsaInfo->lsaId.advRtrId, pAdvRtrId);
    pLsaInfo->pArea = pArea;
    if (pIface != NULL)
    {
        pLsaInfo->pInterface = pIface;
    }
    pLsaInfo->u1FloodFlag = OSIX_FALSE;
    pLsaInfo->u1dnaLsaSplAgeFlag = OSPFV3_START;
    pLsaInfo->u4RxmtCount = 0;
    pLsaInfo->pV3OspfCxt = pArea->pV3OspfCxt;

    if (V3LsuAddOrDelLsaInRBTree (pLsaInfo, OSPFV3_ADD) == OSIX_FAILURE)
    {
        OSPFV3_LSA_INFO_FREE (pLsaInfo);
        return NULL;
    }

    if (IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (u2LsaType))
    {
        V3LsuAddLsaToLst (pLsaInfo, pLsa);
    }

    OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                pArea->pV3OspfCxt->u4ContextId, "New LSA Info Created\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddToLsdb\n");
    return (pLsaInfo);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddLsaToLst                                           */
/*                                                                           */
/* Description  : This function adds the lsa to the List.                    */
/*                                                                           */
/* Input        : pLsaInfo             : pointer to Lsa Info.                */
/*                pLsa                 : pointer to LSA                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuAddLsaToLst (tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa)
{

    tTMO_HASH_TABLE    *pLsaHashTable = NULL;
    tV3OsRouterId       rtrId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddLsaToLst\n");

    switch (pLsaInfo->lsaId.u2LsaType)
    {
        case OSPFV3_ROUTER_LSA:
            pLsaHashTable = pLsaInfo->pArea->pRtrLsaHashTable;
            V3LsuAddLsaInHashLst (pLsaInfo, pLsaHashTable,
                                  &pLsaInfo->lsaId.advRtrId);
            break;

        case OSPFV3_INTER_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pInterPrefixLsaTable;
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_INTER_AREA_PREFIX_LSA);
            break;

        case OSPFV3_INTER_AREA_ROUTER_LSA:
            pLsaHashTable = pLsaInfo->pArea->pInterRouterLsaTable;
            OSPFV3_RTR_ID_COPY (&rtrId, (pLsa + OSPFV3_DEST_RTR_ID_OFFSET));
            V3LsuAddLsaInHashLst (pLsaInfo, pLsaHashTable, &rtrId);
            break;

        case OSPFV3_AS_EXT_LSA:
            pLsaHashTable = pLsaInfo->pV3OspfCxt->pExtLsaHashTable;
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_AS_EXT_LSA);
            break;

        case OSPFV3_NSSA_LSA:
            pLsaHashTable = pLsaInfo->pArea->pNssaLsaHashTable;
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_NSSA_LSA);
            break;

        case OSPFV3_INTRA_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pIntraPrefixLsaTable;
            V3LsuAddIntraLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa);
            break;

        case OSPFV3_LINK_LSA:
            TMO_SLL_Add (&(pLsaInfo->pInterface->linkLsaLst),
                         &(pLsaInfo->nextLsaInfo));

            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddLsaToLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddOrDelLsaInRBTree                                   */
/*                                                                           */
/* Description  : This function adds or deletes the lsa to a particular RB   */
/*                tree depend on the flodding scope of the lsa and flag value*/
/*                                                                           */
/* Input        : pLsaInfo             : pointer to Lsa Info.                */
/*              : u1AddDelFlag         : OSPFV3_ADD or OSPFV3_DELETE.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if added or deleted successfully              */
/*                OSIX_FAILURE otherwise.                                    */
/*****************************************************************************/
PRIVATE UINT1
V3LsuAddOrDelLsaInRBTree (tV3OsLsaInfo * pLsaInfo, UINT1 u1AddDelFlag)
{
    tRBTree             pRBRoot = NULL;
    UINT1               u1FloodScope = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddOrDelLsaInRBTree\n");

    u1FloodScope = V3LsuGetLsaFloodScope (pLsaInfo->lsaId.u2LsaType);

    switch (u1FloodScope)
    {
        case OSPFV3_LINK_FLOOD_SCOPE:
            pRBRoot = pLsaInfo->pInterface->pLinkScopeLsaRBRoot;
            break;

        case OSPFV3_AREA_FLOOD_SCOPE:
            pRBRoot = pLsaInfo->pArea->pAreaScopeLsaRBRoot;
            break;

        default:
            /* Default case occurs for AS scope LSA */
            pRBRoot = pLsaInfo->pV3OspfCxt->pAsScopeLsaRBRoot;
            break;
    }

    if (u1AddDelFlag == OSPFV3_ADD)
    {
        if (RBTreeAdd (pRBRoot, pLsaInfo) == RB_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to add LSA in RB Tree\n"));

            OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC |
                        OSPFV3_CRITICAL_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "RB Tree Add Failure for LSA\n");
            gu4V3LsuAddOrDelLsaInRBTreeFail++;
            return OSIX_FAILURE;
        }
        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "LSA Node Added successfully in RB Tree \n");
    }
    else
    {
        if (RBTreeRemove (pRBRoot, pLsaInfo) == RB_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Failed to delete LSA from RB Tree\n"));

            OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC |
                        OSPFV3_CRITICAL_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "RB Tree delete Failure for LSA\n");
            gu4V3LsuAddOrDelLsaInRBTreeFail++;
            return OSIX_FAILURE;
        }

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "LSA Node deleted successfully from RB Tree \n");
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddOrDelLsaInRBTree\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGetLsaFloodScope                                      */
/*                                                                           */
/* Description  : This function gets the lsa flodding scope.                 */
/*                                                                           */
/* Input          u2LsaType            : Type of the LSA                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : flood scope of the LSA.                                    */
/*****************************************************************************/
PUBLIC UINT1
V3LsuGetLsaFloodScope (UINT2 u2LsaType)
{
    UINT1               u1FloodScope = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuGetLsaFloodScope\n");

    if ((IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (u2LsaType)) ||
        (OSPFV3_IS_U_BIT_SET_LSA (u2LsaType)))
    {
        if (OSPFV3_IS_AS_FLOOD_SCOPE_LSA (u2LsaType))
        {
            u1FloodScope = OSPFV3_AS_FLOOD_SCOPE;
        }
        else if (OSPFV3_IS_AREA_FLOOD_SCOPE_LSA (u2LsaType))
        {
            u1FloodScope = OSPFV3_AREA_FLOOD_SCOPE;
        }
        else
        {
            u1FloodScope = OSPFV3_LINK_FLOOD_SCOPE;
        }
    }
    else
    {
        u1FloodScope = OSPFV3_LINK_FLOOD_SCOPE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuGetLsaFloodScope\n");

    return u1FloodScope;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddPrefixLsaInHashLst                                 */
/*                                                                           */
/* Description  : This function adds Inter Area Prefix LSA, AS External LSA  */
/*                and NSSA LSA to a particular Hash Node.                    */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                pLsa                 : pointer to pLsa.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuAddPrefixLsaInHashLst (tV3OsLsaInfo * pLsaInfo,
                            tTMO_HASH_TABLE * pLsaHashTable, UINT1 *pLsa,
                            UINT2 u2LsaType)
{

    UINT1               u1PrefixLen = 0;
    UINT1               u1DbPrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    UINT1              *pDbAddrPrefix = NULL;
    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    UINT1               u1DbNodeFound = OSIX_FALSE;
    tV3OsDbNode        *pOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddPrefixLsaInHashLst\n");

    u1PrefixLen = *((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pAddrPrefix = (pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    u4HashIndex = V3LsaAddrPrefixHashFunc (pAddrPrefix, u1PrefixLen, u2LsaType);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        u1DbPrefixLen =
            *((UINT1 *) (pDbLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
        pDbAddrPrefix = (pDbLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

        if ((u1PrefixLen == u1DbPrefixLen) &&
            (MEMCMP (pAddrPrefix, pDbAddrPrefix,
                     OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen))
             == OSPFV3_EQUAL))
        {
            u1DbNodeFound = OSIX_TRUE;
            break;
        }
    }                            /* End of scan of Hash Bucket */

    /* If Database node is not present, then Create a hash node */
    if (u1DbNodeFound == OSIX_FALSE)
    {
        OSPFV3_DB_NODE_ALLOC (&pOsDbNode);
        if (NULL == pOsDbNode)
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for Database node\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "DB Node Alloc Failure\n");
            return;
        }

        /* Initialise the Hash node */
        TMO_SLL_Init (&(pOsDbNode->lsaLst));

        /* Adding the Created Hash node to Hash Table */
        TMO_HASH_Add_Node (pLsaHashTable,
                           &(pOsDbNode->NextDbNode), u4HashIndex, NULL);
    }

    TMO_SLL_Add (&(pOsDbNode->lsaLst), &(pLsaInfo->nextLsaInfo));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddPrefixLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddLsaInHashLst                                       */
/*                                                                           */
/* Description  : This function adds ROUTER LSA, Intra area prefix lsa  and  */
/*                Inter Area Router LSA to a particular Hash Node.           */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                pRtrId               : pointer to Router ID.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuAddLsaInHashLst (tV3OsLsaInfo * pLsaInfo, tTMO_HASH_TABLE * pLsaHashTable,
                      tV3OsRouterId * pRtrId)
{

    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsLsaInfo       *pLstLsaInfo = NULL;
    UINT1               u1DbNodeFound = OSIX_FALSE;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsRouterId       dbRtrId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddLsaInHashLst\n");

    u4HashIndex = V3LsaRtrIdHashFunc (pRtrId);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (pDbLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA)
        {
            OSPFV3_RTR_ID_COPY (&dbRtrId,
                                (pDbLsaInfo->pLsa + OSPFV3_DEST_RTR_ID_OFFSET));
        }
        else
        {
            OSPFV3_RTR_ID_COPY (&dbRtrId, &pDbLsaInfo->lsaId.advRtrId);
        }

        if (V3UtilRtrIdComp (dbRtrId, *pRtrId) == OSPFV3_EQUAL)
        {
            u1DbNodeFound = OSIX_TRUE;
            break;
        }                        /* End of check if matching Hash node */
    }                            /* End of scan of Hash Bucket */

    /* If Database node is not present, then Create a hash node */
    if (u1DbNodeFound == OSIX_FALSE)
    {
        OSPFV3_DB_NODE_ALLOC (&pOsDbNode);
        if (NULL == pOsDbNode)
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for Database node\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "DB Node Alloc Failure\n");
            return;
        }

        /* Initialise the Hash node */
        TMO_SLL_Init (&(pOsDbNode->lsaLst));

        /* Adding the Created Hash node to Hash Table */
        TMO_HASH_Add_Node (pLsaHashTable,
                           &(pOsDbNode->NextDbNode), u4HashIndex, NULL);
    }

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_ROUTER_LSA)
    {
        pPrevNode = NULL;

        TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
        {
            pLstLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
            if (V3UtilLsaIdComp
                (pLstLsaInfo->lsaId.u2LsaType,
                 pLstLsaInfo->lsaId.linkStateId,
                 pLstLsaInfo->lsaId.advRtrId,
                 pLsaInfo->lsaId.u2LsaType,
                 pLsaInfo->lsaId.linkStateId,
                 pLsaInfo->lsaId.advRtrId) == OSPFV3_GREATER)
            {
                break;
            }
            pPrevNode = pLstNode;
        }

        TMO_SLL_Insert (&pOsDbNode->lsaLst, pPrevNode,
                        &(pLsaInfo->nextLsaInfo));
    }
    else
    {
        TMO_SLL_Add (&(pOsDbNode->lsaLst), &(pLsaInfo->nextLsaInfo));
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuUpdateChksumAndCount                                  */
/*                                                                           */
/* Description  : This function Update the ChkSumsum and Count.              */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                u1ChksumFlag         : tells whether plus or minus         */
/*                                     : the chksum value.                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3LsuUpdateChksumAndCount (tV3OsLsaInfo * pLsaInfo, UINT1 u1ChksumFlag)
{

    UINT1               u1FloodScope = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuUpdateChksumAndCount\n");

    u1FloodScope = V3LsuGetLsaFloodScope (pLsaInfo->lsaId.u2LsaType);

    switch (u1FloodScope)
    {
        case OSPFV3_LINK_FLOOD_SCOPE:
            pLsaInfo->pInterface->u4LinkScopeLsaChkSumsum =
                (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pInterface->u4LinkScopeLsaChkSumsum +
                pLsaInfo->u2LsaChksum : pLsaInfo->pInterface->
                u4LinkScopeLsaChkSumsum - pLsaInfo->u2LsaChksum;

            (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pInterface->u4LinkScopeLsaCount++ :
                pLsaInfo->pInterface->u4LinkScopeLsaCount--;

            break;

        case OSPFV3_AREA_FLOOD_SCOPE:
            pLsaInfo->pArea->u4AreaScopeLsaChksumSum =
                (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pArea->u4AreaScopeLsaChksumSum +
                pLsaInfo->u2LsaChksum : pLsaInfo->pArea->
                u4AreaScopeLsaChksumSum - pLsaInfo->u2LsaChksum;

            (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pArea->u4AreaScopeLsaCount++ :
                pLsaInfo->pArea->u4AreaScopeLsaCount--;

            break;

        default:
            /* Default case occurs for AS scope LSA */
            pLsaInfo->pV3OspfCxt->u4AsScopeLsaChksumSum =
                (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pV3OspfCxt->u4AsScopeLsaChksumSum
                + pLsaInfo->u2LsaChksum :
                pLsaInfo->pV3OspfCxt->u4AsScopeLsaChksumSum
                - pLsaInfo->u2LsaChksum;

            (u1ChksumFlag == OSPFV3_ADD) ?
                pLsaInfo->pV3OspfCxt->u4AsScopeLsaCount++ :
                pLsaInfo->pV3OspfCxt->u4AsScopeLsaCount--;

            if (OSPFV3_IS_NON_DEFAULT_AS_EXTERNAL_LSA (pLsaInfo))
            {
                (u1ChksumFlag == OSPFV3_ADD) ?
                    pLsaInfo->pV3OspfCxt->u4NonDefAsLsaCount++ :
                    pLsaInfo->pV3OspfCxt->u4NonDefAsLsaCount--;
            }
            if (pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
            {
                (u1ChksumFlag == OSPFV3_ADD) ?
                    pLsaInfo->pV3OspfCxt->u4AsExtLsaCount++ :
                    pLsaInfo->pV3OspfCxt->u4AsExtLsaCount--;
            }

            break;

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuUpdateChksumAndCount\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuInstallLsa                                            */
/*                                                                           */
/* Description  : This procedure installs the LSA in the specified area. If  */
/*                there is any change appropriate routing table entries are  */
/*                re-calculated.                                             */
/*                                                                           */
/* Input        : pLsa                : poinetr to LSA                       */
/*                pLsaInfo            : pointer to the advertisement         */
/*                pInterface          : pointer to Interface                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuInstallLsa (UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo, UINT1 u1FnEqvlFlag)
{
    UINT1              *pCurrPtr = NULL;
    UINT1               u1ChangeFlag = OSIX_FALSE;
    UINT1               u1TmrFlag = OSIX_TRUE;
    tIp6Addr            oldNextHop;
    tIp6Addr            newNextHop;
    UINT1               u1Flag = OSIX_FALSE;
    UINT4               u4LinkStateId = 0;
    tV3OsLsaSeqNum      lsaSeqNum1 = 0;
    tV3OsLsaSeqNum      lsaSeqNum2 = 0;
    UINT2               u2LsaAge1 = 0;
    UINT2               u2LsaAge2 = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuInstallLsa\n");

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pLsaInfo->pV3OspfCxt->u4ContextId,
                 "Install LSA Type %x Link_state_ID %x Adv_rtr_ID %x\n",
                 pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    pCurrPtr = pLsa;

    if (V3LsuCompLsaContents (pLsaInfo, pLsa) == OSPFV3_NOT_EQUAL)
    {
        u1ChangeFlag = OSIX_TRUE;
    }
    else
    {
        OSPFV3_GET_LSA_AGE (pLsaInfo, &(u2LsaAge1));
        OSPFV3_BUFFER_GET_2_BYTE (pLsa, 0, u2LsaAge2);

        if ((OSPFV3_IS_MAX_AGE (u2LsaAge1)) && !(OSPFV3_IS_MAX_AGE (u2LsaAge2)))
        {
            OSPFV3_BUFFER_GET_4_BYTE (pLsaInfo->pLsa,
                                      OSPFV3_LS_SEQ_NUM_OFFSET_IN_LS_HEADER,
                                      lsaSeqNum1);
            OSPFV3_BUFFER_GET_4_BYTE (pLsa,
                                      OSPFV3_LS_SEQ_NUM_OFFSET_IN_LS_HEADER,
                                      lsaSeqNum2);

            if (lsaSeqNum1 != lsaSeqNum2)
            {
                u1ChangeFlag = OSIX_TRUE;
            }
        }
    }

    if ((u1ChangeFlag == OSIX_TRUE) && (pLsaInfo->pLsa == NULL) &&
        (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTRA_AREA_PREFIX_LSA))
    {
        V3RtcIncUpdateIntraAreaPrefLsa (NULL, pLsa, pLsaInfo, pLsaInfo->pArea);
    }

    if (pLsaInfo->pLsa != NULL)
    {
        if (u1ChangeFlag == OSIX_TRUE)
        {
            V3LsuCheckHashIndex (pLsaInfo, pLsa);
        }

        if (((u1ChangeFlag == OSIX_TRUE) ||
             (OSPFV3_IS_MAX_AGE ((OSPFV3_BUFFER_WFROMPDU (pLsa))))) &&
            (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTRA_AREA_PREFIX_LSA))
        {
            V3RtcIncUpdateIntraAreaPrefLsa (pLsaInfo->pLsa, pLsa, pLsaInfo,
                                            pLsaInfo->pArea);
        }

        u1TmrFlag = OSIX_FALSE;
        /* Check DC bit of the LSA */
        if (u1ChangeFlag == OSIX_TRUE)
        {
            V3LsuCheckDcBitLsa (pLsaInfo);
        }
        if ((u1ChangeFlag == OSIX_TRUE) &&
            (pLsaInfo->lsaId.u2LsaType == OSPFV3_LINK_LSA) &&
            (MEMCMP (pLsaInfo->pLsa + OSPFV3_LINK_LSA_HDR_LEN,
                     pLsa + OSPFV3_LINK_LSA_HDR_LEN,
                     OSPFV3_IPV6_ADDR_LEN) != OSPFV3_EQUAL))
        {
            MEMCPY (&oldNextHop, pLsaInfo->pLsa + OSPFV3_LINK_LSA_HDR_LEN,
                    OSPFV3_IPV6_ADDR_LEN);
            MEMCPY (&newNextHop, pLsa + OSPFV3_LINK_LSA_HDR_LEN,
                    OSPFV3_IPV6_ADDR_LEN);
            u1Flag = OSIX_TRUE;
        }

        /* Update the Chksum  and Count */
        V3LsuUpdateChksumAndCount (pLsaInfo, OSPFV3_DELETE);

        OSPFV3_LSA_TYPE_FREE (pLsaInfo->lsaId.u2LsaType, pLsaInfo->pLsa);
    }

    if (u1ChangeFlag == OSIX_TRUE)
    {
        pLsaInfo->u1FnEqvlFlag = u1FnEqvlFlag;
    }
    pLsaInfo->u2LsaAge = OSPFV3_LGET2BYTE (pCurrPtr);
    pLsaInfo->lsaId.u2LsaType = OSPFV3_LGET2BYTE (pCurrPtr);
    OSPFV3_LGETSTR (pCurrPtr, pLsaInfo->lsaId.linkStateId,
                    sizeof (tV3OsLinkStateId));
    OSPFV3_LGETSTR (pCurrPtr, pLsaInfo->lsaId.advRtrId, sizeof (tV3OsRouterId));
    pLsaInfo->lsaSeqNum = OSPFV3_LGET4BYTE (pCurrPtr);
    pLsaInfo->u2LsaChksum = OSPFV3_LGET2BYTE (pCurrPtr);
    pLsaInfo->u2LsaLen = OSPFV3_LGET2BYTE (pCurrPtr);
    pLsaInfo->pLsa = pLsa;
    pLsaInfo->u1TrnsltType5 = OSIX_FALSE;

    /* Upadate the Chksum  and Count */
    V3LsuUpdateChksumAndCount (pLsaInfo, OSPFV3_ADD);

    OsixGetSysTime ((tOsixSysTime *) & (pLsaInfo->u4LsaArrivalTime));

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_ROUTER_LSA)
    {
        u4LinkStateId = OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        if ((pLsaInfo->u2LsaLen == OSPFV3_ZERO_LINK_RTR_LSA_SIZE)
            && (u4LinkStateId > 0))
        {
            pLsaInfo->u2LsaAge = OSPFV3_MAX_AGE;
            V3TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                               OSPFV3_LSA_NORMAL_AGING_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC * 1);
        }
    }

    /* if lsa age is equal to MAX_AGE then the aging timer is set to 1 second */
    if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
    {
        if (pLsaInfo->lsaSeqNum < OSPFV3_MAX_SEQ_NUM)
        {
            pLsaInfo->u1FloodFlag = OSIX_TRUE;
            if (u1TmrFlag == OSIX_FALSE)
            {
                V3TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                                   OSPFV3_LSA_NORMAL_AGING_TIMER,
                                   OSPFV3_NO_OF_TICKS_PER_SEC * 1);
            }
            else
            {
                V3TmrSetTimer (&(pLsaInfo->lsaAgingTimer),
                               OSPFV3_LSA_NORMAL_AGING_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC * 1);
            }
        }
        u1ChangeFlag = OSIX_TRUE;
    }
    else
    {
        if (u1TmrFlag == OSIX_FALSE)
        {
            V3TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                               OSPFV3_LSA_NORMAL_AGING_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC *
                               (OSPFV3_CHECK_AGE -
                                (pLsaInfo->u2LsaAge % OSPFV3_CHECK_AGE)));
        }
        else
        {
            V3TmrSetTimer (&(pLsaInfo->lsaAgingTimer),
                           OSPFV3_LSA_NORMAL_AGING_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC *
                           (OSPFV3_CHECK_AGE -
                            (pLsaInfo->u2LsaAge % OSPFV3_CHECK_AGE)));
        }
    }
    if (pLsaInfo->pLsaDesc != NULL)
    {
        /* check for MAX age if lsa is max age don't start the timer */
        pLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_FALSE;
        V3TmrSetTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                       OSPFV3_MIN_LSA_INTERVAL_TIMER,
                       (OSPFV3_NO_OF_TICKS_PER_SEC *
                        OSPFV3_MIN_LSA_INTERVAL_VALUE));
    }
    if ((u1ChangeFlag == OSIX_TRUE)
        && !(OSPFV3_IS_MAX_AGE ((OSPFV3_BUFFER_WFROMPDU (pLsa)))))
    {
        V3LsuProcessDcBitResetLsa (pLsaInfo);
    }

    if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_LINK_LSA) &&
        (OSPFV3_IS_DR (pLsaInfo->pInterface)))
    {
        V3GenIntraAreaPrefixLsa (pLsaInfo->pInterface->pArea);
    }

    if (u1ChangeFlag == OSIX_TRUE)
    {
        switch (pLsaInfo->lsaId.u2LsaType)
        {
            case OSPFV3_ROUTER_LSA:
            case OSPFV3_NETWORK_LSA:
                if (pLsaInfo->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                {
                    V3RtcSetRtTimerInCxt (pLsaInfo->pV3OspfCxt);
                }

                if ((V3UtilAreaIdComp (pLsaInfo->pArea->areaId,
                                       (OSPFV3_NULL_ID
                                        (pLsaInfo->pV3OspfCxt->u4ContextId))) ==
                     OSPFV3_EQUAL)
                    && (TMO_SLL_Count (&(pLsaInfo->pV3OspfCxt->virtIfLst)) !=
                        0))
                {
                    pLsaInfo->pV3OspfCxt->u1VirtRtCal = OSIX_TRUE;
                }
                break;

            case OSPFV3_INTER_AREA_PREFIX_LSA:
            case OSPFV3_INTER_AREA_ROUTER_LSA:
                if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                     pLsaInfo->pV3OspfCxt->rtrId)
                    != OSPFV3_EQUAL)
                {
                    V3RtcIncUpdtRtSmmryLinkLsa (pLsaInfo);
                }
                break;

            case OSPFV3_AS_EXT_LSA:
            case OSPFV3_NSSA_LSA:
                if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                     pLsaInfo->pV3OspfCxt->rtrId)
                    != OSPFV3_EQUAL)
                {
                    V3RtcIncUpdateExtRoute (pLsaInfo);
                }
                break;

            case OSPFV3_LINK_LSA:
                if ((u1Flag == OSIX_TRUE) &&
                    (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                      pLsaInfo->pV3OspfCxt->rtrId)
                     != OSPFV3_EQUAL))
                {
                    V3RtcIncUpdateLinkLsa (pLsaInfo, &oldNextHop, &newNextHop);
                }
                break;

            default:
                break;
        }
    }

    /* Send the LSA to the standby node */
    if (O3RedDynConstructAndSendLsa (pLsaInfo) == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "LSA sync-up failed "
                      "Lsa originated type: %x, link state id: %x, "
                      "adv rtr id: %x\n",
                      pLsaInfo->lsaId.u2LsaType,
                      OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                      OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId)));

        OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                         pLsaInfo->pV3OspfCxt->u4ContextId,
                         "LSA sync-up failed "
                         "Lsa originated type: %x, link state id: %x, "
                         "adv rtr id: %x\n",
                         pLsaInfo->lsaId.u2LsaType,
                         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                                  lsaId.linkStateId),
                         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuInstallLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuClearLsaInfo                                          */
/*                                                                           */
/* Description  : remove the LsaInfo from RB tree and hash list. free the    */
/*                LsaInfo memory.                                            */
/*                                                                           */
/* Input        : pLsaInfo          : pointer to the advertisement           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuClearLsaInfo (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsArea          *pArea = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;
    UINT4               u4ContextId = OSPFV3_ZERO;
    UINT1               u1PrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuClearLsaInfo\n");

    u4ContextId = pLsaInfo->pV3OspfCxt->u4ContextId;

    /* delete LsaInfo from RB tree */
    if (V3LsuAddOrDelLsaInRBTree (pLsaInfo, OSPFV3_DELETE) == OSIX_FAILURE)
    {
        V3TmrDeleteTimer (&pLsaInfo->lsaAgingTimer);
        return;
    }
    /* delete from lsa list */
    if (IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED (pLsaInfo->lsaId.u2LsaType))
    {
        V3LsuDeleteLsaFromLst (pLsaInfo);
    }
    if (pLsaInfo->pV3OspfCxt->u1RtrDisableInProgress == OSIX_FALSE)
    {
        if (OSPFV3_IS_OPTION_BIT_PRESENT_IN_LSA (pLsaInfo->lsaId.u2LsaType) &&
            (!OSPFV3_IS_DC_BIT_SET_LSA (pLsaInfo->pLsa)) &&
            (!OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) &&
            (pLsaInfo->pArea->u4DcBitResetLsaCount == 0))
        {
            TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->areasLst), pArea, tV3OsArea *)
            {
                if (pArea->u4DcBitResetLsaCount != 0)
                {
                    V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                                           OSPFV3_SIG_INDICATION,
                                           (UINT1 *) NULL);
                    break;
                }
            }
        }

        if (((pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) ||
             (pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA)) &&
            (!OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) &&
            (pLsaInfo->u1FnEqvlFlag == OSIX_TRUE))
        {
            u1PrefixLen =
                *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
            pAddrPrefix = (pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);
            pExtRoute = V3ExtrtFindRouteInCxt (pLsaInfo->pV3OspfCxt,
                                               (tIp6Addr *) (VOID *)
                                               pAddrPrefix, u1PrefixLen);

            if (pExtRoute != NULL)
            {
                V3RagAddRouteInAggAddrRangeInCxt (pLsaInfo->pV3OspfCxt,
                                                  pExtRoute);
            }
        }

        switch (pLsaInfo->lsaId.u2LsaType)
        {
            case OSPFV3_ROUTER_LSA:
            case OSPFV3_NETWORK_LSA:
                if (pLsaInfo->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                {
                    V3RtcSetRtTimerInCxt (pLsaInfo->pV3OspfCxt);
                }

                if ((V3UtilAreaIdComp (pLsaInfo->pArea->areaId,
                                       OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
                    && (TMO_SLL_Count (&(pLsaInfo->pV3OspfCxt->virtIfLst)) !=
                        0))
                {
                    pLsaInfo->pV3OspfCxt->u1VirtRtCal = OSIX_TRUE;
                }
                break;

            case OSPFV3_INTER_AREA_PREFIX_LSA:
            case OSPFV3_INTER_AREA_ROUTER_LSA:
                if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                     pLsaInfo->pV3OspfCxt->rtrId)
                    != OSPFV3_EQUAL)
                {
                    V3RtcIncUpdtRtSmmryLinkLsa (pLsaInfo);
                }
                break;

            case OSPFV3_AS_EXT_LSA:
            case OSPFV3_NSSA_LSA:
                if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                     pLsaInfo->pV3OspfCxt->rtrId)
                    != OSPFV3_EQUAL)
                {
                    V3RtcIncUpdateExtRoute (pLsaInfo);
                }
                break;

            case OSPFV3_INTRA_AREA_PREFIX_LSA:
                V3RtcIncUpdateIntraAreaPrefLsa (pLsaInfo->pLsa, NULL, pLsaInfo,
                                                pLsaInfo->pArea);
                break;

            default:
                break;
        }
    }

    V3TmrDeleteTimer (&pLsaInfo->lsaAgingTimer);
    OSPFV3_LSA_TYPE_FREE (pLsaInfo->lsaId.u2LsaType, pLsaInfo->pLsa);
    OSPFV3_LSA_INFO_FREE (pLsaInfo);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: V3LsuClearLsaInfo\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteLsaFromLst                                      */
/*                                                                           */
/* Description  : This function delete the lsa to a particular List.         */
/*                                                                           */
/* Input        : pLsaInfo             : pointer to Lsa Info.                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuDeleteLsaFromLst (tV3OsLsaInfo * pLsaInfo)
{

    tTMO_HASH_TABLE    *pLsaHashTable = NULL;
    tV3OsRouterId       rtrId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteLsaFromLst\n");

    switch (pLsaInfo->lsaId.u2LsaType)
    {
        case OSPFV3_ROUTER_LSA:
            pLsaHashTable = pLsaInfo->pArea->pRtrLsaHashTable;
            V3LsuDeleteLsaInHashLst (pLsaInfo, pLsaHashTable,
                                     &pLsaInfo->lsaId.advRtrId);
            break;

        case OSPFV3_INTER_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pInterPrefixLsaTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_INTER_AREA_PREFIX_LSA);
            break;

        case OSPFV3_INTER_AREA_ROUTER_LSA:
            pLsaHashTable = pLsaInfo->pArea->pInterRouterLsaTable;
            OSPFV3_RTR_ID_COPY (&rtrId,
                                (pLsaInfo->pLsa + OSPFV3_DEST_RTR_ID_OFFSET));
            V3LsuDeleteLsaInHashLst (pLsaInfo, pLsaHashTable, &rtrId);
            break;

        case OSPFV3_AS_EXT_LSA:
            pLsaHashTable = pLsaInfo->pV3OspfCxt->pExtLsaHashTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_AS_EXT_LSA);
            break;

        case OSPFV3_NSSA_LSA:
            pLsaHashTable = pLsaInfo->pArea->pNssaLsaHashTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_NSSA_LSA);
            break;

        case OSPFV3_INTRA_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pIntraPrefixLsaTable;
            V3LsuDeleteIntraLsaInHashLst (pLsaInfo, pLsaHashTable);
            break;

        case OSPFV3_LINK_LSA:
            TMO_SLL_Delete (&(pLsaInfo->pInterface->linkLsaLst),
                            &(pLsaInfo->nextLsaInfo));
            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteLsaFromLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeletePrefixLsaInHashLst                              */
/*                                                                           */
/* Description  : This function delete Inter Area Prefix LSA, AS External LSA*/
/*                and NSSA LSA to a particular Hash Node.                    */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuDeletePrefixLsaInHashLst (tV3OsLsaInfo * pLsaInfo,
                               tTMO_HASH_TABLE * pLsaHashTable, UINT2 u2LsaType)
{

    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTempOsDbNode = NULL;
    tV3OsLsaInfo       *pLstLsaInfo = NULL;
    UINT1               u1PrefixLen = 0;
    UINT1               u1DbPrefixLen = 0;
    UINT1              *pAddrPrefix = NULL;
    UINT1              *pDbAddrPrefix = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeletePrefixLsaInHashLst\n");

    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    pAddrPrefix = (pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    u4HashIndex = V3LsaAddrPrefixHashFunc (pAddrPrefix, u1PrefixLen, u2LsaType);

    OSPFV3_DYNM_HASH_BUCKET_SCAN
        (pLsaHashTable, u4HashIndex, pOsDbNode, pTempOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        u1DbPrefixLen =
            *((UINT1 *) (pDbLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
        pDbAddrPrefix = (pDbLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

        if ((u1PrefixLen == u1DbPrefixLen) &&
            (MEMCMP (pAddrPrefix, pDbAddrPrefix,
                     OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen))
             == OSPFV3_EQUAL))
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLstLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
                if (pLstLsaInfo == pLsaInfo)
                {
                    TMO_SLL_Delete (&(pOsDbNode->lsaLst),
                                    &(pLstLsaInfo->nextLsaInfo));
                    break;
                }
            }
            /* Check if Hash bucket is empty, if so, delete the bucket */
            if (TMO_SLL_Count (&(pOsDbNode->lsaLst)) == 0)
            {
                /* Delete the hash node and free its memory */
                TMO_HASH_Delete_Node (pLsaHashTable,
                                      &(pOsDbNode->NextDbNode), u4HashIndex);

                OSPFV3_DB_NODE_FREE (pOsDbNode);
            }
            break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeletePrefixLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteLsaInHashLst                                    */
/*                                                                           */
/* Description  : This function delete ROUTER LSA, Intra area prefix lsa and */
/*                Inter Area Router LSA to a particular Hash Node.           */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                pRtrId               : pointer to Router ID.               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuDeleteLsaInHashLst (tV3OsLsaInfo * pLsaInfo,
                         tTMO_HASH_TABLE * pLsaHashTable,
                         tV3OsRouterId * pRtrId)
{

    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTempOsDbNode = NULL;
    tV3OsLsaInfo       *pLstLsaInfo = NULL;
    tV3OsRouterId       dbRtrId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteLsaInHashLst\n");

    u4HashIndex = V3LsaRtrIdHashFunc (pRtrId);

    OSPFV3_DYNM_HASH_BUCKET_SCAN
        (pLsaHashTable, u4HashIndex, pOsDbNode, pTempOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (pDbLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA)
        {
            OSPFV3_RTR_ID_COPY (&dbRtrId,
                                (pDbLsaInfo->pLsa + OSPFV3_DEST_RTR_ID_OFFSET));
        }
        else
        {
            OSPFV3_RTR_ID_COPY (&dbRtrId, (&pDbLsaInfo->lsaId.advRtrId));
        }

        if (V3UtilRtrIdComp (dbRtrId, *pRtrId) == OSPFV3_EQUAL)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLstLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);

                if (pLstLsaInfo == pLsaInfo)
                {
                    TMO_SLL_Delete (&(pOsDbNode->lsaLst),
                                    &(pLstLsaInfo->nextLsaInfo));
                    break;
                }
            }

            /* Check if Hash bucket is empty, if so, delete the bucket */
            if (TMO_SLL_Count (&(pOsDbNode->lsaLst)) == 0)
            {
                /* Delete the hash node and free its memory */
                TMO_HASH_Delete_Node (pLsaHashTable,
                                      &(pOsDbNode->NextDbNode), u4HashIndex);
                OSPFV3_DB_NODE_FREE (pOsDbNode);
            }
            break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuClearLsaDesc                                          */
/*                                                                           */
/* Description  : Clears the LSA descriptor.                                 */
/*                                                                           */
/* Input        : pLsaDesc          : pointer to the LSA descriptor          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuClearLsaDesc (tV3OsLsaDesc * pLsaDesc)
{
    UINT2               u2InternalType = pLsaDesc->u2InternalLsaType;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuClearLsaDesc\n");
    TMO_DLL_Delete (&(pLsaDesc->pLsaInfo->pV3OspfCxt->lsaDescLst),
                    &(pLsaDesc->nextLsaDesc));
    V3TmrDeleteTimer (&pLsaDesc->minLsaIntervalTimer);
    if (pLsaDesc->pAssoPrimitive != NULL)
    {
        if ((u2InternalType == OSPFV3_INTER_AREA_PREFIX_LSA) ||
            (u2InternalType == OSPFV3_INTER_AREA_ROUTER_LSA))
        {
            OSPFV3_SUM_PARAM_FREE (pLsaDesc->pAssoPrimitive);
        }
    }
    OSPFV3_LSA_DESC_FREE (pLsaDesc);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuClearLsaDesc\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteLsaFromDatabase                                 */
/*                                                                           */
/* Description  : This procedure deletes the specified lsa from the database.*/
/*                                                                           */
/* Input        : pLsaInfo           : pointer to the advertisement          */
/*                u1ForcedRelFlag    : flag                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if the lsa is deleted from the database         */
/*                OSIX_FALSE, otherwise                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
V3LsuDeleteLsaFromDatabase (tV3OsLsaInfo * pLsaInfo, UINT1 u1ForcedRelFlag)
{
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pNssaLsaInfo = NULL;
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteLsaFromDatabase\n");

    OSPFV3_TRC3 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                 pLsaInfo->pV3OspfCxt->u4ContextId,
                 "Delete Lsa Type %x linkStateId %x advRtrId %x ",
                 pLsaInfo->lsaId.u2LsaType,
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
                 OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    u4ContextId = pLsaInfo->pV3OspfCxt->u4ContextId;

    if (u1ForcedRelFlag == OSIX_FALSE)
    {
        if (((pLsaInfo->u4RxmtCount != 0) &&
             (pLsaInfo->lsaSeqNum < OSPFV3_MAX_SEQ_NUM)) ||
            (pLsaInfo->pV3OspfCxt->u4XchgOrLoadNbrCount != 0))
        {
            if (OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
            {
                pLsaInfo->u1FloodFlag = OSIX_TRUE;
                V3TmrRestartTimer (&(pLsaInfo->lsaAgingTimer),
                                   OSPFV3_LSA_NORMAL_AGING_TIMER,
                                   OSPFV3_NO_OF_TICKS_PER_SEC * 10);

            }
            gu4V3LsuDeleteLsaFromDatabaseFail++;
            return OSIX_FALSE;
        }

        /* if seq_num_wrap_around flag is TRUE then originate new advt */
        if ((pLsaInfo->pLsaDesc != NULL) &&
            (pLsaInfo->pLsaDesc->u1SeqNumWrapAround == OSIX_TRUE))
        {
            pLsaInfo->pLsaDesc->u1SeqNumWrapAround = OSIX_FALSE;
            pLsaInfo->pLsaDesc->u1LsaChanged = OSIX_TRUE;
            V3TmrRestartTimer (&(pLsaInfo->pLsaDesc->minLsaIntervalTimer),
                               OSPFV3_MIN_LSA_INTERVAL_TIMER,
                               OSPFV3_NO_OF_TICKS_PER_SEC * 1);
            gu4V3LsuDeleteLsaFromDatabaseFail++;
            return OSIX_FALSE;
        }
    }

    else
    {
        V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_FALSE);
    }

    V3LsuUpdateChksumAndCount (pLsaInfo, OSPFV3_DELETE);

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA)
    {
        TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
            {
                pNssaLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_NSSA_LSA,
                                         &(pLsaInfo->lsaId.linkStateId),
                                         &(pLsaInfo->pV3OspfCxt->rtrId), NULL,
                                         pArea);

                if ((pNssaLsaInfo != NULL) &&
                    (!OSPFV3_IS_MAX_AGE (pNssaLsaInfo->u2LsaAge)))
                {
                    V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                                           OSPFV3_SIG_NEXT_INSTANCE,
                                           (UINT1 *) pNssaLsaInfo->pLsaDesc);
                }
            }
        }
    }

    if (OSPFV3_IS_OPTION_BIT_PRESENT_IN_LSA (pLsaInfo->lsaId.u2LsaType))
    {
        if (!OSPFV3_IS_DC_BIT_SET_LSA (pLsaInfo->pLsa))
        {
            if ((pLsaInfo->pArea->u4DcBitResetLsaCount != 0) ||
                (pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount != 0))
            {
                OSPFV3_DEC_DC_BIT_RESET_LSA_COUNT (pLsaInfo);
                if ((pLsaInfo->lsaId.u2LsaType
                     == OSPFV3_INTER_AREA_ROUTER_LSA) &&
                    (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)))
                {
                    pLsaInfo->pArea->u4IndicationLsaCount--;

                    if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))
                    {
                        pLsaInfo->pArea->bIndicationLsaPresence = OSIX_FALSE;
                    }

                }
                if ((!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))) &&
                    (pLsaInfo->pArea->u4DcBitResetLsaCount == 0))
                {
                    V3LsuCheckAndFlushIndicationLsa (pLsaInfo->pArea);
                }
            }
        }
    }

    /* Send the LSA to the standby node */
    pLsaInfo->u2LsaAge = OSPFV3_MAX_AGE;
    if (O3RedDynConstructAndSendLsa (pLsaInfo) == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "LSA sync-up failed. "
                      "Lsa originated type: %x, link state id: %x, "
                      "adv rtr id: %x\n",
                      pLsaInfo->lsaId.u2LsaType,
                      OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                               lsaId.linkStateId),
                      OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId)));

        OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                         pLsaInfo->pV3OspfCxt->u4ContextId,
                         "LSA sync-up failed. "
                         "Lsa originated type: %x, link state id: %x, "
                         "adv rtr id: %x\n",
                         pLsaInfo->lsaId.u2LsaType,
                         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->
                                                  lsaId.linkStateId),
                         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));
    }

    if ((pLsaInfo->pLsaDesc != NULL) &&
        (pLsaInfo->pLsaDesc->u2InternalLsaType == OSPFV3_AS_EXT_LSA))
    {
        if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
        {
            ((tV3OsExtRoute *) (VOID *) (pLsaInfo->pLsaDesc->pAssoPrimitive))->
                pLsaInfo = NULL;
        }

    }

    if (pLsaInfo->pLsaDesc != NULL)
    {
        V3LsuClearLsaDesc (pLsaInfo->pLsaDesc);
        pLsaInfo->pLsaDesc = NULL;
    }

    V3LsuClearLsaInfo (pLsaInfo);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId,
                "EXIT: V3LsuDeleteLsaFromDatabase\n");

    return OSIX_TRUE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSearchDatabase                                        */
/*                                                                           */
/* Description  : This procedure searches for the specified advertisement in */
/*                the RB Tree. If the advertisement scope is AS Scope then   */
/*                the advertisement is searched in global RB Tree. If the    */
/*                advertisement scope is Area Scope then the advertisement   */
/*                is searched in Area RB Tree else in Interface RB Tree. if  */
/*                the searched advertisement is not found in the database    */
/*                then this function returns NULL.                           */
/*                                                                           */
/* Input        : u2LsaType          : type of LSA                           */
/*                pLinkStateId       : pointer to advertisement              */
/*                pAdvRtrId          : router id                             */
/*                pPtr               : NULL , for AS Scope LSA.              */
/*                                     Pointer to Area for Area Scope LSA.   */
/*                                     Pointer to Interface for Link Scope   */
/*                                     LSA.                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to LSA info structure, if found                    */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsLsaInfo *
V3LsuSearchDatabase (UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId,
                     tV3OsRouterId * pAdvRtrId, tV3OsInterface * pInterface,
                     tV3OsArea * pArea)
{

    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    UINT1               u1FloodScope = 0;
    tRBTree             pRBRoot = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3LsuSearchDatabase\n");

    lsaInfo.lsaId.u2LsaType = u2LsaType;
    OSPFV3_LINK_STATE_ID_COPY (&lsaInfo.lsaId.linkStateId, pLinkStateId);
    OSPFV3_RTR_ID_COPY (&lsaInfo.lsaId.advRtrId, pAdvRtrId);

    u1FloodScope = V3LsuGetLsaFloodScope (u2LsaType);

    switch (u1FloodScope)
    {
        case OSPFV3_LINK_FLOOD_SCOPE:
            if (pInterface != NULL)
            {
                pRBRoot = pInterface->pLinkScopeLsaRBRoot;
            }
            break;

        case OSPFV3_AREA_FLOOD_SCOPE:
            if (pArea != NULL)
            {
                pRBRoot = pArea->pAreaScopeLsaRBRoot;
            }
            break;

        default:
            /* Default case occurs for AS scope LSA */
            if ((pArea != NULL) && (pArea->pV3OspfCxt != NULL))
            {
                pRBRoot = pArea->pV3OspfCxt->pAsScopeLsaRBRoot;
            }
            break;

    }

    if (pRBRoot != NULL)
    {
        pLsaInfo = (tV3OsLsaInfo *) RBTreeGet (pRBRoot, (tRBElem *) & lsaInfo);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3LsuSearchDatabase\n");
    return pLsaInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteLsaFromDatabase                                 */
/*                                                                           */
/* Description  : This function checks whether LSA is DC bit set or reset and*/
/*                if DC bit is reset then flush the DNA lsa and generate     */
/*                Indication LSA.                                            */
/*                                                                           */
/* Input        : pLsaInfo           : pointer to the advertisement          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuProcessDcBitResetLsa (tV3OsLsaInfo * pLsaInfo)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuProcessDcBitResetLsa\n");

    if ((OSPFV3_IS_OPTION_BIT_PRESENT_IN_LSA (pLsaInfo->lsaId.u2LsaType))
        && (!OSPFV3_IS_DC_BIT_SET_LSA (pLsaInfo->pLsa)))
    {

        OSPFV3_INC_DC_BIT_RESET_LSA_COUNT (pLsaInfo);

        /* check is it an indication lsa */
        if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) &&
            (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)))
        {
            pLsaInfo->pArea->u4IndicationLsaCount++;
        }

        if ((pLsaInfo->pArea->u4DcBitResetLsaCount == 1) ||
            (pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount == 1))
        {
            V3LsuFlushDnaLsa (pLsaInfo->pArea);
            if ((pLsaInfo->pArea->u4DcBitResetLsaCount == 1) &&
                !((pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) &&
                  (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa))))
            {
                V3LsuFlushIndicationLsaInArea (pLsaInfo->pArea);
            }
        }

        if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
        {
            /* check for the generation of indication lsas */
            if ((!OSPFV3_IS_NULL_IP_ADDR (pLsaInfo->pArea->areaId)) &&
                (!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))) &&
                (pLsaInfo->lsaId.u2LsaType != OSPFV3_INTER_AREA_ROUTER_LSA) &&
                (!OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)))
            {
                /* point 1 of RFC 1793 sec 2.5.1 */
                V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                                       OSPFV3_SIG_INDICATION,
                                       (UINT1 *) pLsaInfo->pArea);
            }
            else if ((OSPFV3_IS_NULL_IP_ADDR (pLsaInfo->pArea->areaId)) &&
                     (!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))))
            {
                /* point 2 of RFC 1793 sec 2.5.1 */
                V3SignalLsaRegenInCxt (pLsaInfo->pV3OspfCxt,
                                       OSPFV3_SIG_INDICATION,
                                       (UINT1 *) pLsaInfo->pArea);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuProcessDcBitResetLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCheckDcBitLsa                                         */
/*                                                                           */
/* Description  : This function checks whether LSA is DC bit set or reset and*/
/*                if DC bit is reset previously and now it is set then flush */
/*                Indication LSA.                                            */
/*                                                                           */
/* Input        : pLsaInfo           : pointer to the advertisement          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuCheckDcBitLsa (tV3OsLsaInfo * pLsaInfo)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCheckDcBitLsa\n");

    if (OSPFV3_IS_OPTION_BIT_PRESENT_IN_LSA (pLsaInfo->lsaId.u2LsaType))
    {
        if (!OSPFV3_IS_DC_BIT_SET_LSA (pLsaInfo->pLsa))
        {
            if ((pLsaInfo->pArea->u4DcBitResetLsaCount != 0) ||
                (pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount != 0))
            {
                OSPFV3_DEC_DC_BIT_RESET_LSA_COUNT (pLsaInfo);
                if ((pLsaInfo->lsaId.u2LsaType ==
                     OSPFV3_INTER_AREA_ROUTER_LSA) &&
                    (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)))
                {
                    pLsaInfo->pArea->u4IndicationLsaCount--;
                }

                if ((pLsaInfo->pArea->u4DcBitResetLsaCount == 0) && (!(OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo))))    /* Dc bit fix */
                {
                    V3LsuCheckAndFlushIndicationLsa (pLsaInfo->pArea);
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCheckDcBitLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteAllLsasInCxt                                    */
/*                                                                           */
/* Description  : this routine deletes all the lsas fron the database.       */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* Input        : pV3OspfCxt     -    Context pointer                        */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteAllLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{

    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4CurrentTime = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteAllLsas\n");
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {

        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            V3LsuDeleteLinkScopeLsa (pInterface);
            /* Check whether we need to relinquish the route calculation
             * to do other OSPF processings */
            if (gV3OsRtr.u4RTStaggeringStatus == OSPFV3_STAGGERING_ENABLED)
            {
                u4CurrentTime = OsixGetSysUpTime ();
                /* current time greater than next relinquish time calcuteled
                 * in previous relinuish */
                if (u4CurrentTime >= pV3OspfCxt->u4StaggeringDelta)
                {
                    OSPF3RtcRelinquishInCxt (pV3OspfCxt);
                    /* next relinquish time calc in OSPF3RtcRelinquish() */
                }
            }
        }
        V3LsuDeleteAreaScopeLsa (pArea);

    }

    V3LsuDeleteASScopeLsaInCxt (pV3OspfCxt);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteAllLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteAreaScopeLsa                                    */
/*                                                                           */
/* Description  : This procedure is called when an area is being deleted, or */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pArea         : pointer to the AREA that is being deleted  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteAreaScopeLsa (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteAreaScopeLsa\n");
    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot)) != NULL)
    {
        V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteAreaScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteLinkScopeLsa                                    */
/*                                                                           */
/* Description  : This procedure is called when an Interface is being        */
/*                deleted, or when ospf is disabled.                         */
/*                                                                           */
/* Input        : pInterface : pointer to the Interface that is being deleted*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteLinkScopeLsa (tV3OsInterface * pInterface)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteLinkScopeLsa\n");
    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot)) != NULL)
    {
        V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteLinkScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteASScopeLsaInCxt                                 */
/*                                                                           */
/* Description  : This procedure is called when ospf is disabled.            */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuDeleteASScopeLsaInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteASScopeLsa\n");
    while ((pLsaInfo = (tV3OsLsaInfo *)
            RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot)) != NULL)
    {
        V3LsuDeleteLsaFromDatabase (pLsaInfo, OSIX_TRUE);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteASScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushAllSelfOrgExtLsasInCxt                           */
/*                                                                           */
/* Description  : This routine flushes all the external LSAs fronm the       */
/*                database.                                                  */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuFlushAllSelfOrgExtLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushAllSelfOrgExtLsas\n");
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) &&
            (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                              OSPFV3_RTR_ID (pV3OspfCxt)) == OSPFV3_EQUAL))
        {
            V3AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushAllSelfOrgExtLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushAllSelfOrgLsasInCxt                              */
/*                                                                           */
/* Description  : this routine fluses all the self originated lsas fron      */
/*                the database.                                              */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* Output       : none                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuFlushAllSelfOrgLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{

    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushAllSelfOrgLsas\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {

        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            V3LsuFlushSelfOrgLinkScopeLsa (pInterface);
        }
        V3LsuFlushSelfOrgAreaScopeLsa (pArea);
    }

    V3LsuFlushAllSelfOrgExtLsasInCxt (pV3OspfCxt);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushAllSelfOrgLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushSelfOrgAreaScopeLsa                              */
/*                                                                           */
/* Description  : This procedure is called when an area is being deleted, or */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pArea         : pointer to the AREA that is being deleted  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuFlushSelfOrgAreaScopeLsa (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushSelfOrgAreaScopeLsa\n");
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                             OSPFV3_RTR_ID (pArea->pV3OspfCxt)) == OSPFV3_EQUAL)
        {
            V3AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushSelfOrgAreaScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushSelfOrgLinkScopeLsa                              */
/*                                                                           */
/* Description  : This procedure is called when an If is being deleted, or   */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pInterface : pointer to the Interface that is being deleted*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuFlushSelfOrgLinkScopeLsa (tV3OsInterface * pInterface)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushSelfOrgLinkScopeLsa\n");
    pLsaInfo =
        (tV3OsLsaInfo *) RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                             OSPFV3_RTR_ID (pInterface->pArea->pV3OspfCxt))
            == OSPFV3_EQUAL)
        {
            V3AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushSelfOrgLinkScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushDnaLsa                                           */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This flushes of all the dna lsas in the area.              */
/*                                                                           */
/* Input        : pArea     : area from which dna lsa's have to be flushed.  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuFlushDnaLsa (tV3OsArea * pArea)
{

    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushDnaLsa\n");

    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if (OSPFV3_IS_DNA_LSA (pLsaInfo))
        {
            V3AgdFlushOut (pLsaInfo);
        }
        pLsaInfo = pNextLsaInfo;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushDnaLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCheckAndFlushIndicationLsa                            */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This checks & flushes of all indication lsas in the area.  */
/*                                                                           */
/* Input        : pArea     : area from which indication lsa's have          */
/*                             to be flushed.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuCheckAndFlushIndicationLsa (tV3OsArea * pArea)
{
    tV3OsArea          *pTmpArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCheckAndFlushIndicationLsa\n");

    /* DC bit fix */
    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pTmpArea, tV3OsArea *)
    {
        if (pTmpArea->u4DcBitResetLsaCount != 0)
        {
            return;
        }
    }
    pTmpArea = NULL;

    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pTmpArea, tV3OsArea *)
    {
        if ((pTmpArea == pArea))
        {
            continue;
        }
        V3LsuFlushIndicationLsaInArea (pTmpArea);

    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCheckAndFlushIndicationLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuFlushIndicationLsaInArea                     */
/*                                                                           */
/* Description  : This function is a part of DEMAND_CICUIT_SUPPORT.          */
/*                This flushes all indication lsas in the area.              */
/*                                                                           */
/* Input        : pArea     : area from which indication lsa's have          */
/*                             to be flushed.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
V3LsuFlushIndicationLsaInArea (tV3OsArea * pArea)
{
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pTmpNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpOsDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushIndicationLsaInArea\n");

    if (!OSPFV3_IS_INDICATION_LSA_PRESENCE (pArea))
    {
        return;
    }

    TMO_HASH_Scan_Table (pArea->pInterRouterLsaTable, u4HashKey)
    {
        TMO_HASH_DYN_Scan_Bucket (pArea->pInterRouterLsaTable,
                                  u4HashKey, pOsDbNode,
                                  pTmpOsDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode,
                                  pTmpNode, tTMO_SLL_NODE *)
            {
                pLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
                if ((pLsaInfo->lsaId.u2LsaType ==
                     OSPFV3_INTER_AREA_ROUTER_LSA) &&
                    (OSPFV3_IS_INDICATION_LSA (pLsaInfo->pLsa)) &&
                    (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)))
                {
                    pArea->bIndicationLsaPresence = OSIX_FALSE;
                    if (!OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
                    {
                        V3AgdFlushOut (pLsaInfo);
                        return;
                    }
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushIndicationLsaInArea\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGenerateNonDefaultAsExtLsasInCxt                      */
/*                                                                           */
/* Description  : This function generates the Non default AS_external LSA's. */
/*                                                                           */
/* Input        : pV3OspfCxt     -    Context pointer                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuGenerateNonDefaultAsExtLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGenerateNonDefaultAsExtLsas\n");

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "TrieGetFirstNode Failure\n");
        return;
    }

    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        if (pExtRoute->u1PrefixLength != 0)
        {
            V3RagFindMatChngBkBoneRangeInCxt (pV3OspfCxt, pExtRoute);
        }
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGenerateNonDefaultAsExtLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     :  V3LsuFlushAllNonDefSelfOrgAseLsaInCxt                     */
/*                                                                           */
/* Description  : This function flushes all the Non default self originated  */
/*                AS_external LSA's.                                         */
/*                                                                           */
/* Input        : pV3OspfCxt     -    Context pointer                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuFlushAllNonDefSelfOrgAseLsaInCxt (tV3OspfCxt * pV3OspfCxt)
{

    tV3OsExtRoute      *pExtRoute = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuFlushAllNonDefSelfOrgAseLsa\n");

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pV3OspfCxt->u4ContextId,
                    "TrieGetFirstNode Failure\n");
        return;
    }

    do
    {
        pExtRoute = (tV3OsExtRoute *) pTempPtr;
        if ((pExtRoute->u1PrefixLength != 0) && (pExtRoute->pLsaInfo != NULL))
        {
            V3AgdFlushOut (pExtRoute->pLsaInfo);
        }
        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pExtRoute->u1PrefixLength;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuFlushAllNonDefSelfOrgAseLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSplAgingActionInCxt                                   */
/*                                                                           */
/* Description  : This function set or reset the All ths  LSA generated by   */
/*                a particular router.                                       */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                pRtrId     : pointer to router id                          */
/*                u1Flag     : Indicate whether to set or reset dna flag     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuSplAgingActionInCxt (tV3OspfCxt * pV3OspfCxt,
                          tV3OsRouterId * pRtrId, UINT1 u1Flag)
{

    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSplAgingAction\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            V3LsuSetDnaFlagForLinkScopeLsa (pInterface, pRtrId, u1Flag);
        }
        V3LsuSetDnaFlagForAreaScopeLsa (pArea, pRtrId, u1Flag);
    }

    V3LsuSetDnaFlagForAsScopeLsaInCxt (pV3OspfCxt, pRtrId, u1Flag);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSplAgingAction\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3RbWalkHandleLsuSplAgeAction                              */
/*                                                                           */
/* Description  : This function sets or resets the SplAgeFlag of the LSA     */
/*                generated by a particular router.                          */
/*                                                                           */
/* Input        :    pe       : Pointer to LsaInfo                           */
/*                   pArg     : pointer to router id                         */
/*                   pOut     : Indicate whether to set or reset dna flag    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
V3RbWalkHandleLsuSplAgeAction (tRBElem * pe, eRBVisit visit, UINT4 u4level,
                               void *pArg, void *pOut)
{
    tV3OsLsaInfo       *pLsaInfo = (tV3OsLsaInfo *) pe;
    tV3OsRouterId      *pRtrId = (tV3OsRouterId *) pArg;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3RbWalkHandleLsuSplAgeAction\n");

    UNUSED_PARAM (u4level);

    if ((visit == leaf) || (visit == postorder))
    {
        if (pLsaInfo != NULL)
        {
            if ((V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId, *pRtrId)
                 == OSPFV3_EQUAL) && (OSPFV3_IS_DNA_LSA (pLsaInfo)))
            {
                pLsaInfo->u1dnaLsaSplAgeFlag = *(UINT1 *) pOut;
            }
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3RbWalkHandleLsuSplAgeAction\n");
    return (RB_WALK_CONT);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSetDnaFlagForLinkScopeLsa                             */
/*                                                                           */
/* Description  : This function set or reset the Link Scope LSA generated by */
/*                a particular router.                                       */
/*                                                                           */
/* Input        : pInterface : pointer to the Interface                      */
/*                pRtrId     : pointer to router id.                         */
/*                u1Flag     : Indicate whether to set or reset dna flag     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuSetDnaFlagForLinkScopeLsa (tV3OsInterface * pInterface, tV3OsRouterId
                                * pRtrId, UINT1 u1Flag)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSetDnaFlagForLinkScopeLsa\n");

    RBTreeWalk (pInterface->pLinkScopeLsaRBRoot,
                V3RbWalkHandleLsuSplAgeAction, pRtrId, &u1Flag);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSetDnaFlagForLinkScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSetDnaFlagForAreaScopeLsa                             */
/*                                                                           */
/* Description  : This function set or reset the Area Scope LSA generated by */
/*                a particular router.                                       */
/*                                                                           */
/* Input        : pArea      : pointer to the area                           */
/*                pRtrId     : pointer to router id.                         */
/*                u1Flag     : Indicate whether to set or reset dna flag     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuSetDnaFlagForAreaScopeLsa (tV3OsArea * pArea, tV3OsRouterId
                                * pRtrId, UINT1 u1Flag)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSetDnaFlagForAreaScopeLsa\n");

    RBTreeWalk (pArea->pAreaScopeLsaRBRoot,
                V3RbWalkHandleLsuSplAgeAction, pRtrId, &u1Flag);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSetDnaFlagForAreaScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuSetDnaFlagForAsScopeLsaInCxt                          */
/*                                                                           */
/* Description  : This function set or reset the AS Scope LSA generated by   */
/*                a particular router.                                       */
/*                                                                           */
/* Input        : pV3OspfCxt : Context pointer                               */
/*                pRtrId     : pointer to router id                          */
/*                u1Flag     : Indicate whether to set or reset dna flag     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3LsuSetDnaFlagForAsScopeLsaInCxt (tV3OspfCxt * pV3OspfCxt,
                                   tV3OsRouterId * pRtrId, UINT1 u1Flag)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuSetDnaFlagForAsScopeLsa\n");

    RBTreeWalk (pV3OspfCxt->pAsScopeLsaRBRoot,
                V3RbWalkHandleLsuSplAgeAction, pRtrId, &u1Flag);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuSetDnaFlagForAsScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuAddIntraLsaInHashLst                                  */
/*                                                                           */
/* Description  : This function adds Intra Area Prefix LSA                   */
/*                to a particular Hash Node.                                 */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                pLsa                 : pointer to pLsa.                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuAddIntraLsaInHashLst (tV3OsLsaInfo * pLsaInfo,
                           tTMO_HASH_TABLE * pLsaHashTable, UINT1 *pLsa)
{

    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    UINT1               u1DbNodeFound = OSIX_FALSE;
    tV3OsDbNode        *pOsDbNode = NULL;
    UINT2               u2RefLsaType = 0;
    tV3OsRouterId       refrtrId;
    tV3OsLinkStateId    refLsId;
    UINT1              *pCurrPtr = NULL;
    UINT2               u2DbRefLsaType = 0;
    tV3OsRouterId       dbrefrtrId;
    tV3OsLinkStateId    dbrefLsId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuAddIntraLsaInHashLst\n");

    pCurrPtr = pLsa + OSPFV3_REF_LS_TYPE_OFFSET;
    u2RefLsaType = OSPFV3_LGET2BYTE (pCurrPtr);
    OSPFV3_LGETSTR (pCurrPtr, refLsId, sizeof (tV3OsLinkStateId));
    OSPFV3_LGETSTR (pCurrPtr, refrtrId, sizeof (tV3OsRouterId));

    u4HashIndex = V3LsaIntraAreaHashFunc (u2RefLsaType, &refLsId, &refrtrId);

    TMO_HASH_Scan_Bucket (pLsaHashTable, u4HashIndex, pOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        pCurrPtr = pDbLsaInfo->pLsa + OSPFV3_REF_LS_TYPE_OFFSET;
        u2DbRefLsaType = OSPFV3_LGET2BYTE (pCurrPtr);
        OSPFV3_LGETSTR (pCurrPtr, dbrefLsId, sizeof (tV3OsLinkStateId));
        OSPFV3_LGETSTR (pCurrPtr, dbrefrtrId, sizeof (tV3OsRouterId));

        if ((u2RefLsaType == u2DbRefLsaType) &&
            (V3UtilRtrIdComp (dbrefrtrId, refrtrId) == OSPFV3_EQUAL) &&
            (V3UtilLinkStateIdComp (refLsId, dbrefLsId) == OSPFV3_EQUAL))
        {
            u1DbNodeFound = OSIX_TRUE;
            break;
        }
    }                            /* End of scan of Hash Bucket */

    /* If Database node is not present, then Create a hash node */
    if (u1DbNodeFound == OSIX_FALSE)
    {
        OSPFV3_DB_NODE_ALLOC (&pOsDbNode);
        if (NULL == pOsDbNode)
        {
            SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                          "Unable to allocate memory for Database node\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "DB Node Alloc Failure\n");
            return;
        }

        /* Initialise the Hash node */
        TMO_SLL_Init (&(pOsDbNode->lsaLst));

        /* Adding the Created Hash node to Hash Table */
        TMO_HASH_Add_Node (pLsaHashTable,
                           &(pOsDbNode->NextDbNode), u4HashIndex, NULL);

    }

    TMO_SLL_Add (&(pOsDbNode->lsaLst), &(pLsaInfo->nextLsaInfo));

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuAddIntraLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuDeleteIntraLsaInHashLst                               */
/*                                                                           */
/* Description  : This function delete Intra Area Prefix LSA                 */
/*                to a particular Hash Node.                                 */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsaHashTable        : pointer to Hash Table.              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3LsuDeleteIntraLsaInHashLst (tV3OsLsaInfo * pLsaInfo,
                              tTMO_HASH_TABLE * pLsaHashTable)
{
    UINT4               u4HashIndex = 0;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTempOsDbNode = NULL;
    tV3OsLsaInfo       *pLstLsaInfo = NULL;
    UINT2               u2RefLsaType = 0;
    tV3OsRouterId       refrtrId;
    tV3OsLinkStateId    refLsId;
    UINT1              *pCurrPtr = NULL;
    UINT2               u2DbRefLsaType = 0;
    tV3OsRouterId       dbrefrtrId;
    tV3OsLinkStateId    dbrefLsId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuDeleteIntraLsaInHashLst\n");

    pCurrPtr = pLsaInfo->pLsa + OSPFV3_REF_LS_TYPE_OFFSET;
    u2RefLsaType = OSPFV3_LGET2BYTE (pCurrPtr);
    OSPFV3_LGETSTR (pCurrPtr, refLsId, sizeof (tV3OsLinkStateId));
    OSPFV3_LGETSTR (pCurrPtr, refrtrId, sizeof (tV3OsRouterId));

    u4HashIndex = V3LsaIntraAreaHashFunc (u2RefLsaType, &refLsId, &refrtrId);

    OSPFV3_DYNM_HASH_BUCKET_SCAN
        (pLsaHashTable, u4HashIndex, pOsDbNode, pTempOsDbNode, tV3OsDbNode *)
    {
        pDbLstNode = TMO_SLL_First (&pOsDbNode->lsaLst);
        pDbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        pCurrPtr = pDbLsaInfo->pLsa + OSPFV3_REF_LS_TYPE_OFFSET;
        u2DbRefLsaType = OSPFV3_LGET2BYTE (pCurrPtr);
        OSPFV3_LGETSTR (pCurrPtr, dbrefLsId, sizeof (tV3OsLinkStateId));
        OSPFV3_LGETSTR (pCurrPtr, dbrefrtrId, sizeof (tV3OsRouterId));

        if ((u2RefLsaType == u2DbRefLsaType) &&
            (V3UtilRtrIdComp (dbrefrtrId, refrtrId) == OSPFV3_EQUAL) &&
            (V3UtilLinkStateIdComp (refLsId, dbrefLsId) == OSPFV3_EQUAL))
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pLstLsaInfo =
                    OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
                if (pLstLsaInfo == pLsaInfo)
                {
                    TMO_SLL_Delete (&(pOsDbNode->lsaLst),
                                    &(pLstLsaInfo->nextLsaInfo));
                    break;
                }
            }
            /* Check if Hash bucket is empty, if so, delete the bucket */
            if (TMO_SLL_Count (&(pOsDbNode->lsaLst)) == 0)
            {
                /* Delete the hash node and free its memory */
                TMO_HASH_Delete_Node (pLsaHashTable,
                                      &(pOsDbNode->NextDbNode), u4HashIndex);

                OSPFV3_DB_NODE_FREE (pOsDbNode);
            }
            break;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuDeleteIntraLsaInHashLst\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuCheckHashIndex                         */
/*                                                                           */
/* Description  : This function checks the LSA Hash Table and makes necessary*/
/*                 changes in the hash table entries                         */
/*                                                                 */
/*                                                                           */
/* Input          pLsaInfo             : pointer to Lsa Info.                */
/*                pLsa                 : pointer to Lsa.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

PRIVATE VOID
V3LsuCheckHashIndex (tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa)
{

    tTMO_HASH_TABLE    *pLsaHashTable = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuCheckHashIndex\n");

    switch (pLsaInfo->lsaId.u2LsaType)
    {
        case OSPFV3_INTER_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pInterPrefixLsaTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_INTER_AREA_PREFIX_LSA);
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_INTER_AREA_PREFIX_LSA);
            break;

        case OSPFV3_AS_EXT_LSA:
            pLsaHashTable = pLsaInfo->pV3OspfCxt->pExtLsaHashTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_AS_EXT_LSA);
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_AS_EXT_LSA);
            break;

        case OSPFV3_NSSA_LSA:
            pLsaHashTable = pLsaInfo->pArea->pNssaLsaHashTable;
            V3LsuDeletePrefixLsaInHashLst (pLsaInfo, pLsaHashTable,
                                           OSPFV3_NSSA_LSA);
            V3LsuAddPrefixLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa,
                                        OSPFV3_NSSA_LSA);
            break;

        case OSPFV3_INTRA_AREA_PREFIX_LSA:
            pLsaHashTable = pLsaInfo->pArea->pIntraPrefixLsaTable;
            V3LsuDeleteIntraLsaInHashLst (pLsaInfo, pLsaHashTable);
            V3LsuAddIntraLsaInHashLst (pLsaInfo, pLsaHashTable, pLsa);
            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuCheckHashIndex\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGenAllSelfOrgExtLsasInCxt                             */
/*                                                                           */
/* Description  : This routine re generated  all the external LSAs from the  */
/*                database.                                                  */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuGenAllSelfOrgExtLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGenAllSelfOrgExtLsas\n");
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) &&
            (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                              OSPFV3_RTR_ID (pV3OspfCxt)) == OSPFV3_EQUAL))
        {
            V3SignalLsaRegenInCxt (pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pLsaInfo->pLsaDesc);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGenAllSelfOrgExtLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* function     : V3LsuGenAllSelfOrgLsasInCxt                                */
/*                                                                           */
/* description  : this routine fluses all the self originated lsas fron      */
/*                the database.                                              */
/*                called only once when the protocol is brought down.        */
/*                                                                           */
/* Input        : pV3OspfCxt    -    Context pointer                         */
/*                                                                           */
/* output       : none                                                       */
/*                                                                           */
/* returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuGenAllSelfOrgLsasInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGenAllSelfOrgLsas\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {

        TMO_SLL_Scan (&(pArea->ifsInArea), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pLstNode);
            V3LsuGenSelfOrgLinkScopeLsa (pInterface);
        }
        V3LsuGenSelfOrgAreaScopeLsa (pArea);
    }

    V3LsuGenAllSelfOrgExtLsasInCxt (pV3OspfCxt);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGenAllSelfOrgLsas\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGenSelfOrgAreaScopeLsa                                */
/*                                                                           */
/* Description  : This procedure is called when an area is being deleted, or */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pArea         : pointer to the AREA that is being deleted  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuGenSelfOrgAreaScopeLsa (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGenSelfOrgAreaScopeLsa\n");
    pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                             OSPFV3_RTR_ID (pArea->pV3OspfCxt)) == OSPFV3_EQUAL)
        {
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pLsaInfo->pLsaDesc);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGenSelfOrgAreaScopeLsa\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3LsuGenSelfOrgLinkScopeLsa                                */
/*                                                                           */
/* Description  : This procedure is called when an If is being deleted, or   */
/*                when ospf is disabled.                                     */
/*                                                                           */
/* Input        : pInterface : pointer to the Interface that is being deleted*/
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3LsuGenSelfOrgLinkScopeLsa (tV3OsInterface * pInterface)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLsaInfo       *pNextLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3LsuGenSelfOrgLinkScopeLsa\n");
    pLsaInfo =
        (tV3OsLsaInfo *) RBTreeGetFirst (pInterface->pLinkScopeLsaRBRoot);

    while (pLsaInfo != NULL)
    {
        pNextLsaInfo =
            (tV3OsLsaInfo *) RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                            (tRBElem *) pLsaInfo, NULL);

        if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                             OSPFV3_RTR_ID (pInterface->pArea->pV3OspfCxt))
            == OSPFV3_EQUAL)
        {
            V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                                   OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pLsaInfo->pLsaDesc);
        }
        pLsaInfo = pNextLsaInfo;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3LsuGenSelfOrgLinkScopeLsa\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3lsinst.c                      */
/*-----------------------------------------------------------------------*/
