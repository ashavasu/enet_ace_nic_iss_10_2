/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3host.c,v 1.7 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains procedures related to the host 
 *         operations 
 *
 *******************************************************************/
#include "o3inc.h"
/*****************************************************************************/
/*                                                                           */
/* Function        : V3HostAddInCxt                                          */
/*                                                                           */
/* Description     : This procedure creates the host entry, initializes      */
/*                   and adds the specified host route to the host RB tree.  */
/*                                                                           */
/* Input           : pV3OspfCxt    : Context pointer                         */
/*                   pHostAddr     : ptr to host IP address                  */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : pHost   Pointer to Host entry if created succesfully    */
/*                   NULL otherwise.                                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tV3OsHost   *
V3HostAddInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pHostAddr)
{
    tV3OsHost          *pHost = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3HostAddInCxt\n");
    OSPFV3_HOST_ALLOC (&(pHost));
    if (NULL == pHost)
    {
        OSPFV3_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                    pV3OspfCxt->u4ContextId, "Host Alloc Failed\n");
        return NULL;
    }

    OSPFV3_IP6_ADDR_COPY (pHost->hostIp6Addr, *pHostAddr);
    pHost->pArea = pV3OspfCxt->pBackbone;
    pHost->u4HostMetric = OSPFV3_DEF_HOST_METRIC;
    pHost->hostStatus = OSPFV3_INVALID;

    if (RBTreeAdd (pV3OspfCxt->pHostRBRoot, pHost) == RB_FAILURE)
    {
        OSPFV3_HOST_FREE (pHost);
        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pV3OspfCxt->u4ContextId, "RB Tree Add Failure for Host\n");
        return NULL;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3HostAddInCxt\n");
    return pHost;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3HostDelete                                            */
/*                                                                           */
/* Description     : deletes the specified host from the RB Tree             */
/*                   and frees the memory                                    */
/*                                                                           */
/* Input           : pHost         : ptr to host                             */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : VOID                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3HostDelete (tV3OsHost * pHost)
{
    UINT4               u4ContextId = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pHost->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3HostDeleteInCxt\n");

    u4ContextId = pHost->pArea->pV3OspfCxt->u4ContextId;

    if (RBTreeRemove (pHost->pArea->pV3OspfCxt->pHostRBRoot, pHost)
        == RB_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC, u4ContextId,
                    "RB Tree delete Failure for Host\n");
        return OSIX_FAILURE;
    }

    OSPFV3_HOST_FREE (pHost);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: V3HostDeleteInCxt\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3HostDeleteAllHostsInCxt                               */
/*                                                                           */
/* Description     : This utility scans and deletes all the hosts in the     */
/*                   context.                                                */
/*                                                                           */
/* Input           : pV3OspfCxt    : Context pointer                         */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3HostDeleteAllHostsInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsHost          *pV3OsHost = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3HostDeleteAllHostsInCxt\n");
    while ((pV3OsHost = RBTreeGetFirst (pV3OspfCxt->pHostRBRoot)) != NULL)
    {
        V3HostDelete (pV3OsHost);
        pV3OsHost = NULL;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3HostDeleteAllHostsInCxt\n");
}

/*------------------------------------------------------------------------*/
/*                        End of the file o3host.c                          */
/*------------------------------------------------------------------------*/
