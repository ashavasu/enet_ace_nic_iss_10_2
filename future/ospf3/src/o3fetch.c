/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3fetch.c,v 1.29 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to Get Ospfv3
 *         MIB Objects. 
 *
 *******************************************************************/
#include "o3inc.h"
#include "ospf3lw.h"
#include "fsos3lw.h"

PRIVATE INT1        V3OspfRtTblGetObjectInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, VOID *DestId, UINT1 u1PfxLen,
        tIp6Addr * pNextHop, UINT4 *pu4Val, UINT1 u1ObjType, UINT1 u1DestType));
PRIVATE INT1        V3GetNextLeastNextHopIp6Addr
PROTO ((tV3OsRtEntry * pRtEntry, tIp6Addr * pNextHopIp6Addr,
        tIp6Addr * pNextLeastNextHopIp6Addr));
PRIVATE VOID        V3OspfGetRTAttribute
PROTO ((tV3OsRtEntry * pRtEntry, tIp6Addr nextHopIp6Addr, UINT1 u1ObjType,
        UINT4 *pu4ObjVal));
PRIVATE INT1        V3GetLeastNextHopIp6Addr
PROTO ((tV3OsRtEntry * pRtEntry, tIp6Addr * pLeastNextHopIp6Addr));

/*****************************************************************************/
/* Function     : V3GetNextLeastasExtAddrRange                               */
/* Description  : The routine returns the next least of the                  */
/*                AsExternalAggregationTable                                 */
/* Input        : pAsExtAddrRng                                              */
/*                                                                           */
/* Output       : pNextLeastAsExtAddrRng                                     */
/* Returns      : OSIX_SUCCESS, if least next AsExternalAggregation is found */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/

PRIVATE INT4
V3GetNextLeastasExtAddrRange (tV3OsAsExtAddrRange * pAsExtAddrRng,
                              tV3OsAsExtAddrRange * pNextLeastAsExtAddrRng)
{
    tTMO_SLL_NODE      *pScanNode = NULL;
    tV3OsAsExtAddrRange *pNxtAsExtRng = NULL;
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1EntryFound = OSIX_FALSE;

    /* Get Next Least AreaID */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst), pScanNode, tTMO_SLL_NODE *)
    {
        gV3OsRtr.pExtAggrTableCache = pScanNode;
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr, pScanNode);
        if ((V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr),
                                &(pAsExtAddrRng->ip6Addr)) == OSPFV3_EQUAL) &&
            (pScanAsExtRng->u1PrefixLength == pAsExtAddrRng->u1PrefixLength) &&
            (V3UtilAreaIdComp (pScanAsExtRng->areaId,
                               pAsExtAddrRng->areaId) == OSPFV3_GREATER))
        {
            if (pNxtAsExtRng == NULL)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
                continue;
            }
            if (V3UtilAreaIdComp (pScanAsExtRng->areaId,
                                  pNxtAsExtRng->areaId) == OSPFV3_LESS)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
            }
        }
    }
    if (u1EntryFound == OSIX_TRUE)
    {
        if (pNxtAsExtRng != NULL)
        {
            MEMCPY (pNextLeastAsExtAddrRng, pNxtAsExtRng,
                    sizeof (tV3OsAsExtAddrRange));
            return OSIX_SUCCESS;
        }
    }

    /* Get Next Least Prefix Length */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst), pScanNode, tTMO_SLL_NODE *)
    {
        gV3OsRtr.pExtAggrTableCache = pScanNode;
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr, pScanNode);
        if ((V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr),
                                &(pAsExtAddrRng->ip6Addr)) == OSPFV3_EQUAL) &&
            (pScanAsExtRng->u1PrefixLength > pAsExtAddrRng->u1PrefixLength))
        {
            if (pNxtAsExtRng == NULL)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
                continue;
            }
            if (pScanAsExtRng->u1PrefixLength < pNxtAsExtRng->u1PrefixLength)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
            }
        }
    }
    if (u1EntryFound == OSIX_TRUE)
    {
        if (pNxtAsExtRng != NULL)
        {
            MEMCPY (pNextLeastAsExtAddrRng, pNxtAsExtRng,
                    sizeof (tV3OsAsExtAddrRange));
            return OSIX_SUCCESS;
        }
    }

    /* Get Next Least Prefix */
    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst), pScanNode, tTMO_SLL_NODE *)
    {
        gV3OsRtr.pExtAggrTableCache = pScanNode;
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr, pScanNode);
        if (V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr),
                               &(pAsExtAddrRng->ip6Addr)) == OSPFV3_GREATER)
        {
            if (pNxtAsExtRng == NULL)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
                continue;
            }
            if (V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr),
                                   &(pNxtAsExtRng->ip6Addr)) == OSPFV3_LESS)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
            }
        }
        else if ((pNxtAsExtRng != NULL) &&
                 (V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr),
                                     &(pNxtAsExtRng->ip6Addr)) == OSPFV3_EQUAL))
        {
            if (pScanAsExtRng->u1PrefixLength < pNxtAsExtRng->u1PrefixLength)
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
            }
            else if ((pScanAsExtRng->u1PrefixLength ==
                      pNxtAsExtRng->u1PrefixLength) &&
                     (V3UtilAreaIdComp (pScanAsExtRng->areaId,
                                        pNxtAsExtRng->areaId) == OSPFV3_LESS))
            {
                pNxtAsExtRng = pScanAsExtRng;
                u1EntryFound = OSIX_TRUE;
            }
        }
    }
    if (u1EntryFound == OSIX_TRUE)
    {
        if (pNxtAsExtRng != NULL)
        {
            MEMCPY (pNextLeastAsExtAddrRng, pNxtAsExtRng,
                    sizeof (tV3OsAsExtAddrRange));
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RouterId
 Input       :  The Indices

                The Object 
                retValOspfv3RouterId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RouterId (UINT4 *pu4RetValOspfv3RouterId)
{
    tV3OsRouterId       rtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_RTR_ID_COPY (rtrId, pV3OspfCxt->rtrId);
    *pu4RetValOspfv3RouterId = OSPFV3_BUFFER_DWFROMPDU (rtrId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AdminStat
 Input       :  The Indices

                The Object 
                retValOspfv3AdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AdminStat (INT4 *pi4RetValOspfv3AdminStat)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3AdminStat = (INT4) pV3OspfCxt->admnStat;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VersionNumber
 Input       :  The Indices

                The Object 
                retValOspfv3VersionNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VersionNumber (INT4 *pi4RetValOspfv3VersionNumber)
{
    *pi4RetValOspfv3VersionNumber = OSPFV3_VERSION_NO;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaBdrRtrStatus
 Input       :  The Indices

                The Object 
                retValOspfv3AreaBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaBdrRtrStatus (INT4 *pi4RetValOspfv3AreaBdrRtrStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3AreaBdrRtrStatus =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bAreaBdrRtr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3ASBdrRtrStatus
 Input       :  The Indices

                The Object 
                retValOspfv3ASBdrRtrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ASBdrRtrStatus (INT4 *pi4RetValOspfv3ASBdrRtrStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3ASBdrRtrStatus =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bAsBdrRtr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsScopeLsaCount
 Input       :  The Indices

                The Object 
                retValOspfv3AsScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsScopeLsaCount (UINT4 *pu4RetValOspfv3AsScopeLsaCount)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3AsScopeLsaCount = pV3OspfCxt->u4AsScopeLsaCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsScopeLsaCksumSum
 Input       :  The Indices

                The Object 
                retValOspfv3AsScopeLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsScopeLsaCksumSum (INT4 *pi4RetValOspfv3AsScopeLsaCksumSum)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3AsScopeLsaCksumSum =
        (INT4) pV3OspfCxt->u4AsScopeLsaChksumSum;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3OriginateNewLsas
 Input       :  The Indices

                The Object 
                retValOspfv3OriginateNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3OriginateNewLsas (UINT4 *pu4RetValOspfv3OriginateNewLsas)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3OriginateNewLsas = pV3OspfCxt->u4OriginateNewLsa;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RxNewLsas
 Input       :  The Indices

                The Object 
                retValOspfv3RxNewLsas
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RxNewLsas (UINT4 *pu4RetValOspfv3RxNewLsas)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3RxNewLsas = pV3OspfCxt->u4RcvNewLsa;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3ExtLsaCount
 Input       :  The Indices

                The Object 
                retValOspfv3ExtLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ExtLsaCount (UINT4 *pu4RetValOspfv3ExtLsaCount)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3ExtLsaCount = pV3OspfCxt->u4AsExtLsaCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3ExtAreaLsdbLimit
 Input       :  The Indices

                The Object 
                retValOspfv3ExtAreaLsdbLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ExtAreaLsdbLimit (INT4 *pi4RetValOspfv3ExtAreaLsdbLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3ExtAreaLsdbLimit = pV3OspfCxt->i4ExtLsdbLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3MulticastExtensions
 Input       :  The Indices

                The Object 
                retValOspfv3MulticastExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3MulticastExtensions (tSNMP_OCTET_STRING_TYPE *
                                 pRetValOspfv3MulticastExtensions)
{
    MEMSET (pRetValOspfv3MulticastExtensions->pu1_OctetList, 0,
            pRetValOspfv3MulticastExtensions->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3ExitOverflowInterval
 Input       :  The Indices

                The Object 
                retValOspfv3ExitOverflowInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ExitOverflowInterval (UINT4 *pu4RetValOspfv3ExitOverflowInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3ExitOverflowInterval = pV3OspfCxt->u4ExitOverflowInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3DemandExtensions
 Input       :  The Indices

                The Object 
                retValOspfv3DemandExtensions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3DemandExtensions (INT4 *pi4RetValOspfv3DemandExtensions)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3DemandExtensions =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bDemandExtension);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3TrafficEngineeringSupport
 Input       :  The Indices

                The Object 
                retValOspfv3TrafficEngineeringSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3TrafficEngineeringSupport (INT4
                                       *pi4RetValOspfv3TrafficEngineeringSupport)
{
    *pi4RetValOspfv3TrafficEngineeringSupport = OSPFV3_DISABLED;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3ReferenceBandwidth
 Input       :  The Indices

                The Object 
                retValOspfv3ReferenceBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ReferenceBandwidth (UINT4 *pu4RetValOspfv3ReferenceBandwidth)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValOspfv3ReferenceBandwidth = pV3OspfCxt->u4RefBw;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RestartSupport
 Input       :  The Indices

                The Object 
                retValOspfv3RestartSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RestartSupport (INT4 *pi4RetValOspfv3RestartSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3RestartSupport = (INT4) pV3OspfCxt->u1RestartSupport;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RestartInterval
 Input       :  The Indices

                The Object 
                retValOspfv3RestartInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RestartInterval (INT4 *pi4RetValOspfv3RestartInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3RestartInterval = (INT4) pV3OspfCxt->u4GracePeriod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RestartStatus
 Input       :  The Indices

                The Object 
                retValOspfv3RestartStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RestartStatus (INT4 *pi4RetValOspfv3RestartStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3RestartStatus = (INT4) pV3OspfCxt->u1RestartStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RestartAge
 Input       :  The Indices

                The Object 
                retValOspfv3RestartAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RestartAge (INT4 *pi4RetValOspfv3RestartAge)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TmrGetRemainingTime
        (gV3OsRtr.timerLstId,
         (&(pV3OspfCxt->graceTimer.timerNode)),
         (UINT4 *) pi4RetValOspfv3RestartAge) == TMR_FAILURE)
    {

        *pi4RetValOspfv3RestartAge = 0;
        return SNMP_SUCCESS;
    }
    *pi4RetValOspfv3RestartAge =
        (*pi4RetValOspfv3RestartAge / OSPFV3_NO_OF_TICKS_PER_SEC);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3RestartExitReason
 Input       :  The Indices

                The Object 
                retValOspfv3RestartExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3RestartExitReason (INT4 *pi4RetValOspfv3RestartExitReason)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValOspfv3RestartExitReason = pV3OspfCxt->u1RestartExitReason;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3AreaTable
 Input       :  The Indices
                Ospfv3AreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3AreaTable (UINT4 *pu4Ospfv3AreaId)
{
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pArea = (tV3OsArea *) TMO_SLL_First (&(pV3OspfCxt->areasLst));

    if (pArea == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4Ospfv3AreaId = OSPFV3_BUFFER_DWFROMPDU (pArea->areaId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3AreaTable
 Input       :  The Indices
                Ospfv3AreaId
                nextOspfv3AreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3AreaTable (UINT4 u4Ospfv3AreaId,
                                UINT4 *pu4NextOspfv3AreaId)
{
    tV3OsAreaId         currAreaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currAreaId, u4Ospfv3AreaId);

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (currAreaId, pArea->areaId) == OSPFV3_LESS)
        {
            *pu4NextOspfv3AreaId = OSPFV3_BUFFER_DWFROMPDU (pArea->areaId);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  V3GetFindAreaInCxt

 Description :  To find area corresponding to given areaId in context

 Input       :  The Indices
                pV3OspfCxt
                pAreaId
 
 Output      :  None
                
 Returns     : pointer to the area if successfull
               NULL  otherwise  
****************************************************************************/

PUBLIC tV3OsArea   *
V3GetFindAreaInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pAreaId)
{
    tV3OsArea          *pArea = NULL;

    if (NULL != gV3OsRtr.pAggrTableCache)
    {
        if (OSPFV3_EQUAL ==
            V3UtilAreaIdComp (*pAreaId, gV3OsRtr.pAggrTableCache->areaId))
        {
            return gV3OsRtr.pAggrTableCache;
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (V3UtilAreaIdComp (*pAreaId, pArea->areaId) == OSPFV3_EQUAL)
        {
            return pArea;
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetOspfv3ImportAsExtern
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3ImportAsExtern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3ImportAsExtern (UINT4 u4Ospfv3AreaId,
                            INT4 *pi4RetValOspfv3ImportAsExtern)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {

        *pi4RetValOspfv3ImportAsExtern = (INT4) (pArea->u4AreaType);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaSpfRuns
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3SpfRuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaSpfRuns (UINT4 u4Ospfv3AreaId, UINT4 *pu4RetValOspfv3SpfRuns)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3SpfRuns = pArea->u4SpfRuns;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaBdrRtrCount
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaBdrRtrCount (UINT4 u4Ospfv3AreaId,
                             UINT4 *pu4RetValOspfv3AreaBdrRtrCount)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3AreaBdrRtrCount = pArea->u4AreaBdrRtrCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsBdrRtrCount
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AsBdrRtrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaAsBdrRtrCount (UINT4 u4Ospfv3AreaId,
                               UINT4 *pu4RetValOspfv3AsBdrRtrCount)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3AsBdrRtrCount = pArea->u4AsBdrRtrCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaScopeLsaCount
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaScopeLsaCount (UINT4 u4Ospfv3AreaId,
                               UINT4 *pu4RetValOspfv3AreaScopeLsaCount)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3AreaScopeLsaCount = pArea->u4AreaScopeLsaCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaScopeLsaCksumSum
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaScopeLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaScopeLsaCksumSum (UINT4 u4Ospfv3AreaId,
                                  INT4 *pi4RetValOspfv3AreaScopeLsaCksumSum)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaScopeLsaCksumSum = (INT4) pArea->
            u4AreaScopeLsaChksumSum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaSummary
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaSummary
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaSummary (UINT4 u4Ospfv3AreaId, INT4 *pi4RetValOspfv3AreaSummary)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaSummary = (INT4) pArea->u1SummaryFunctionality;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaStatus
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaStatus (UINT4 u4Ospfv3AreaId, INT4 *pi4RetValOspfv3AreaStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaStatus = (INT4) pArea->areaStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3StubMetric
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3StubMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3StubMetric (UINT4 u4Ospfv3AreaId, INT4 *pi4RetValOspfv3StubMetric)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3StubMetric = (INT4) pArea->stubDefaultCost.u4Metric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaStubMetricType
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3StubMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaStubMetricType (UINT4 u4Ospfv3AreaId,
                                INT4 *pi4RetValOspfv3StubMetricType)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3StubMetricType =
            (INT4) pArea->stubDefaultCost.u4MetricType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaNssaTranslatorRole
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaNssaTranslatorRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaNssaTranslatorRole (UINT4 u4Ospfv3AreaId,
                                    INT4 *pi4RetValOspfv3AreaNssaTranslatorRole)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaNssaTranslatorRole =
            (INT4) pArea->u1NssaTrnsltrRole;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaNssaTranslatorState
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaNssaTranslatorState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaNssaTranslatorState (UINT4 u4Ospfv3AreaId,
                                     INT4
                                     *pi4RetValOspfv3AreaNssaTranslatorState)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaNssaTranslatorState =
            (INT4) pArea->u1NssaTrnsltrState;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaNssaTranslatorStabilityInterval
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaNssaTranslatorStabilityInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaNssaTranslatorStabilityInterval (UINT4 u4Ospfv3AreaId,
                                                 UINT4
                                                 *pu4RetValOspfv3AreaNssaTranslatorStabilityInterval)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3AreaNssaTranslatorStabilityInterval =
            pArea->u4NssaTrnsltrStbltyInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaNssaTranslatorEvents
 Input       :  The Indices
                Ospfv3AreaId

                The Object 
                retValOspfv3AreaNssaTranslatorEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaNssaTranslatorEvents (UINT4 u4Ospfv3AreaId,
                                      UINT4
                                      *pu4RetValOspfv3AreaNssaTranslatorEvents)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pu4RetValOspfv3AreaNssaTranslatorEvents = pArea->u4NssaTrnsltrEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3AsLsdbTable
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3AsLsdbTable (UINT4 *pu4Ospfv3AsLsdbType,
                                   UINT4 *pu4Ospfv3AsLsdbRouterId,
                                   UINT4 *pu4Ospfv3AsLsdbLsid)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pV3OspfCxt->pAsScopeLsaRBRoot == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLsaInfo =
         (tV3OsLsaInfo *) RBTreeGetFirst (pV3OspfCxt->pAsScopeLsaRBRoot)) !=
        NULL)
    {
        *pu4Ospfv3AsLsdbType = pLsaInfo->lsaId.u2LsaType;
        *pu4Ospfv3AsLsdbRouterId =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
        *pu4Ospfv3AsLsdbLsid =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3AsLsdbTable
 Input       :  The Indices
                Ospfv3AsLsdbType
                nextOspfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                nextOspfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid
                nextOspfv3AsLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3AsLsdbTable (UINT4 u4Ospfv3AsLsdbType,
                                  UINT4 *pu4NextOspfv3AsLsdbType,
                                  UINT4 u4Ospfv3AsLsdbRouterId,
                                  UINT4 *pu4NextOspfv3AsLsdbRouterId,
                                  UINT4 u4Ospfv3AsLsdbLsid,
                                  UINT4 *pu4NextOspfv3AsLsdbLsid)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsRouterId       advRtrId;
    tV3OsLinkStateId    linkStateId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    lsaInfo.lsaId.u2LsaType = (UINT2) u4Ospfv3AsLsdbType;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_LINK_STATE_ID_COPY (lsaInfo.lsaId.linkStateId, linkStateId);

    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);
    OSPFV3_RTR_ID_COPY (lsaInfo.lsaId.advRtrId, advRtrId);

    if ((pLsaInfo =
         (tV3OsLsaInfo *) RBTreeGetNext (pV3OspfCxt->pAsScopeLsaRBRoot,
                                         (tRBElem *) & lsaInfo, NULL)) != NULL)
    {
        *pu4NextOspfv3AsLsdbType = pLsaInfo->lsaId.u2LsaType;
        *pu4NextOspfv3AsLsdbRouterId =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
        *pu4NextOspfv3AsLsdbLsid =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3AsLsdbSequence
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid

                The Object 
                retValOspfv3AsLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsLsdbSequence (UINT4 u4Ospfv3AsLsdbType,
                            UINT4 u4Ospfv3AsLsdbRouterId,
                            UINT4 u4Ospfv3AsLsdbLsid,
                            INT4 *pi4RetValOspfv3AsLsdbSequence)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType,
                                         &(linkStateId),
                                         &(advRtrId), NULL,
                                         pV3OspfCxt->pBackbone)) != NULL)
    {
        *pi4RetValOspfv3AsLsdbSequence = pLsaInfo->lsaSeqNum;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsLsdbAge
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid

                The Object 
                retValOspfv3AsLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsLsdbAge (UINT4 u4Ospfv3AsLsdbType,
                       UINT4 u4Ospfv3AsLsdbRouterId, UINT4 u4Ospfv3AsLsdbLsid,
                       INT4 *pi4RetValOspfv3AsLsdbAge)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType,
                                         &(linkStateId),
                                         &(advRtrId), NULL,
                                         pV3OspfCxt->pBackbone)) != NULL)
    {

        *pi4RetValOspfv3AsLsdbAge = (INT4) pLsaInfo->u2LsaAge;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsLsdbChecksum
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid

                The Object 
                retValOspfv3AsLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsLsdbChecksum (UINT4 u4Ospfv3AsLsdbType,
                            UINT4 u4Ospfv3AsLsdbRouterId,
                            UINT4 u4Ospfv3AsLsdbLsid,
                            INT4 *pi4RetValOspfv3AsLsdbChecksum)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType,
                                         &(linkStateId),
                                         &(advRtrId), NULL,
                                         pV3OspfCxt->pBackbone)) != NULL)
    {
        *pi4RetValOspfv3AsLsdbChecksum = (INT4) pLsaInfo->u2LsaChksum;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsLsdbAdvertisement
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid

                The Object 
                retValOspfv3AsLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsLsdbAdvertisement (UINT4 u4Ospfv3AsLsdbType,
                                 UINT4 u4Ospfv3AsLsdbRouterId,
                                 UINT4 u4Ospfv3AsLsdbLsid,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValOspfv3AsLsdbAdvertisement)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType,
                                         &(linkStateId),
                                         &(advRtrId), NULL,
                                         pV3OspfCxt->pBackbone)) != NULL)
    {
        MEMCPY (pRetValOspfv3AsLsdbAdvertisement->pu1_OctetList,
                pLsaInfo->pLsa, pLsaInfo->u2LsaLen);

        /* This Parameter is used for making Octet String. */
        pRetValOspfv3AsLsdbAdvertisement->i4_Length = (INT4) pLsaInfo->u2LsaLen;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AsLsdbTypeKnown
 Input       :  The Indices
                Ospfv3AsLsdbType
                Ospfv3AsLsdbRouterId
                Ospfv3AsLsdbLsid

                The Object 
                retValOspfv3AsLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AsLsdbTypeKnown (UINT4 u4Ospfv3AsLsdbType,
                             UINT4 u4Ospfv3AsLsdbRouterId,
                             UINT4 u4Ospfv3AsLsdbLsid,
                             INT4 *pi4RetValOspfv3AsLsdbTypeKnown)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AsLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AsLsdbRouterId);

    if ((V3LsuSearchDatabase ((UINT2) u4Ospfv3AsLsdbType,
                              &(linkStateId), &(advRtrId), NULL,
                              pV3OspfCxt->pBackbone) != NULL)
        && (IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED ((UINT2) u4Ospfv3AsLsdbType)
            != 0))
    {
        *pi4RetValOspfv3AsLsdbTypeKnown =
            OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_SUCCESS);
        return SNMP_SUCCESS;
    }

    *pi4RetValOspfv3AsLsdbTypeKnown = OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_FAILURE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3AreaLsdbTable
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3AreaLsdbTable (UINT4 *pu4Ospfv3AreaLsdbAreaId,
                                     UINT4 *pu4Ospfv3AreaLsdbType,
                                     UINT4 *pu4Ospfv3AreaLsdbRouterId,
                                     UINT4 *pu4Ospfv3AreaLsdbLsid)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if ((pLsaInfo =
             (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot)) !=
            NULL)
        {
            *pu4Ospfv3AreaLsdbAreaId = OSPFV3_BUFFER_DWFROMPDU (pArea->areaId);
            *pu4Ospfv3AreaLsdbType = pLsaInfo->lsaId.u2LsaType;
            *pu4Ospfv3AreaLsdbRouterId =
                OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
            *pu4Ospfv3AreaLsdbLsid =
                OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3AreaLsdbTable
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                nextOspfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                nextOspfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                nextOspfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid
                nextOspfv3AreaLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3AreaLsdbTable (UINT4 u4Ospfv3AreaLsdbAreaId,
                                    UINT4 *pu4NextOspfv3AreaLsdbAreaId,
                                    UINT4 u4Ospfv3AreaLsdbType,
                                    UINT4 *pu4NextOspfv3AreaLsdbType,
                                    UINT4 u4Ospfv3AreaLsdbRouterId,
                                    UINT4 *pu4NextOspfv3AreaLsdbRouterId,
                                    UINT4 u4Ospfv3AreaLsdbLsid,
                                    UINT4 *pu4NextOspfv3AreaLsdbLsid)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsRouterId       advRtrId;
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsLinkStateId    linkStateId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1Flag = OSIX_FALSE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) == NULL)
    {
        return SNMP_FAILURE;
    }

    lsaInfo.lsaId.u2LsaType = (UINT2) u4Ospfv3AreaLsdbType;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_LINK_STATE_ID_COPY (lsaInfo.lsaId.linkStateId, linkStateId);

    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);
    OSPFV3_RTR_ID_COPY (lsaInfo.lsaId.advRtrId, advRtrId);

    if ((pLsaInfo =
         (tV3OsLsaInfo *) RBTreeGetNext (pArea->pAreaScopeLsaRBRoot,
                                         (tRBElem *) & lsaInfo, NULL)) != NULL)
    {
        u1Flag = OSIX_TRUE;
    }
    else
    {
        while ((pArea = (tV3OsArea *) TMO_SLL_Next (&(pV3OspfCxt->areasLst),
                                                    &(pArea->nextArea))) !=
               NULL)
        {
            if ((pLsaInfo =
                 (tV3OsLsaInfo *) RBTreeGetFirst (pArea->
                                                  pAreaScopeLsaRBRoot)) != NULL)
            {
                u1Flag = OSIX_TRUE;
                break;
            }
        }
    }

    if (u1Flag == OSIX_TRUE)
    {
        *pu4NextOspfv3AreaLsdbAreaId = OSPFV3_BUFFER_DWFROMPDU (pArea->areaId);
        *pu4NextOspfv3AreaLsdbType = pLsaInfo->lsaId.u2LsaType;
        *pu4NextOspfv3AreaLsdbRouterId =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
        *pu4NextOspfv3AreaLsdbLsid =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3AreaLsdbSequence
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid

                The Object 
                retValOspfv3AreaLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaLsdbSequence (UINT4 u4Ospfv3AreaLsdbAreaId,
                              UINT4 u4Ospfv3AreaLsdbType,
                              UINT4 u4Ospfv3AreaLsdbRouterId,
                              UINT4 u4Ospfv3AreaLsdbLsid,
                              INT4 *pi4RetValOspfv3AreaLsdbSequence)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType,
                                             &(linkStateId),
                                             &(advRtrId), NULL, pArea)) != NULL)
        {

            *pi4RetValOspfv3AreaLsdbSequence = pLsaInfo->lsaSeqNum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaLsdbAge
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid

                The Object 
                retValOspfv3AreaLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaLsdbAge (UINT4 u4Ospfv3AreaLsdbAreaId,
                         UINT4 u4Ospfv3AreaLsdbType,
                         UINT4 u4Ospfv3AreaLsdbRouterId,
                         UINT4 u4Ospfv3AreaLsdbLsid,
                         INT4 *pi4RetValOspfv3AreaLsdbAge)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType,
                                             &(linkStateId),
                                             &(advRtrId), NULL, pArea)) != NULL)
        {

            *pi4RetValOspfv3AreaLsdbAge = (INT4) pLsaInfo->u2LsaAge;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaLsdbChecksum
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid

                The Object 
                retValOspfv3AreaLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaLsdbChecksum (UINT4 u4Ospfv3AreaLsdbAreaId,
                              UINT4 u4Ospfv3AreaLsdbType,
                              UINT4 u4Ospfv3AreaLsdbRouterId,
                              UINT4 u4Ospfv3AreaLsdbLsid,
                              INT4 *pi4RetValOspfv3AreaLsdbChecksum)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType,
                                             &(linkStateId),
                                             &(advRtrId), NULL, pArea)) != NULL)
        {

            *pi4RetValOspfv3AreaLsdbChecksum = (INT4) pLsaInfo->u2LsaChksum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaLsdbAdvertisement
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid

                The Object 
                retValOspfv3AreaLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaLsdbAdvertisement (UINT4 u4Ospfv3AreaLsdbAreaId,
                                   UINT4 u4Ospfv3AreaLsdbType,
                                   UINT4 u4Ospfv3AreaLsdbRouterId,
                                   UINT4 u4Ospfv3AreaLsdbLsid,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValOspfv3AreaLsdbAdvertisement)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType,
                                             &(linkStateId),
                                             &(advRtrId), NULL, pArea)) != NULL)
        {
            MEMCPY (pRetValOspfv3AreaLsdbAdvertisement->pu1_OctetList,
                    pLsaInfo->pLsa, pLsaInfo->u2LsaLen);

            /* This Parameter is used for making Octet String. */
            pRetValOspfv3AreaLsdbAdvertisement->i4_Length =
                (INT4) pLsaInfo->u2LsaLen;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaLsdbTypeKnown
 Input       :  The Indices
                Ospfv3AreaLsdbAreaId
                Ospfv3AreaLsdbType
                Ospfv3AreaLsdbRouterId
                Ospfv3AreaLsdbLsid

                The Object 
                retValOspfv3AreaLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaLsdbTypeKnown (UINT4 u4Ospfv3AreaLsdbAreaId,
                               UINT4 u4Ospfv3AreaLsdbType,
                               UINT4 u4Ospfv3AreaLsdbRouterId,
                               UINT4 u4Ospfv3AreaLsdbLsid,
                               INT4 *pi4RetValOspfv3AreaLsdbTypeKnown)
{
    tV3OsAreaId         areaId;
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaLsdbAreaId);
    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3AreaLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3AreaLsdbRouterId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((V3LsuSearchDatabase ((UINT2) u4Ospfv3AreaLsdbType,
                                  &(linkStateId),
                                  &(advRtrId), NULL,
                                  pArea) != NULL) &&
            (IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED
             ((UINT2) u4Ospfv3AreaLsdbType) != 0))
        {
            *pi4RetValOspfv3AreaLsdbTypeKnown =
                OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_SUCCESS);
            return SNMP_SUCCESS;
        }
    }

    *pi4RetValOspfv3AreaLsdbTypeKnown =
        OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_FAILURE);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3LinkLsdbTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3LinkLsdbTable
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3LinkLsdbTable (INT4 *pi4Ospfv3LinkLsdbIfIndex,
                                     UINT4 *pu4Ospfv3LinkLsdbType,
                                     UINT4 *pu4Ospfv3LinkLsdbRouterId,
                                     UINT4 *pu4Ospfv3LinkLsdbLsid)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot))
        != NULL)
    {
        do
        {
            if ((pLsaInfo =
                 (tV3OsLsaInfo *) RBTreeGetFirst (pInterface->
                                                  pLinkScopeLsaRBRoot)) != NULL)
            {
                *pi4Ospfv3LinkLsdbIfIndex = (INT4) pInterface->u4InterfaceId;
                *pu4Ospfv3LinkLsdbType = pLsaInfo->lsaId.u2LsaType;
                *pu4Ospfv3LinkLsdbRouterId =
                    OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
                *pu4Ospfv3LinkLsdbLsid =
                    OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
                return SNMP_SUCCESS;
            }
        }
        while ((pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface,
                                                  NULL)) != NULL);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3LinkLsdbTable
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                nextOspfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                nextOspfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                nextOspfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid
                nextOspfv3LinkLsdbLsid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3LinkLsdbTable (INT4 i4Ospfv3LinkLsdbIfIndex,
                                    INT4 *pi4NextOspfv3LinkLsdbIfIndex,
                                    UINT4 u4Ospfv3LinkLsdbType,
                                    UINT4 *pu4NextOspfv3LinkLsdbType,
                                    UINT4 u4Ospfv3LinkLsdbRouterId,
                                    UINT4 *pu4NextOspfv3LinkLsdbRouterId,
                                    UINT4 u4Ospfv3LinkLsdbLsid,
                                    UINT4 *pu4NextOspfv3LinkLsdbLsid)
{
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsRouterId       advRtrId;
    UINT1               u1Flag = OSIX_FALSE;
    tV3OsInterface     *pInterface = NULL;
    tV3OsLinkStateId    linkStateId;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) == NULL)
    {
        return SNMP_FAILURE;
    }

    lsaInfo.lsaId.u2LsaType = (UINT2) u4Ospfv3LinkLsdbType;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_LINK_STATE_ID_COPY (lsaInfo.lsaId.linkStateId, linkStateId);

    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);
    OSPFV3_RTR_ID_COPY (lsaInfo.lsaId.advRtrId, advRtrId);

    if ((pLsaInfo =
         (tV3OsLsaInfo *) RBTreeGetNext (pInterface->pLinkScopeLsaRBRoot,
                                         (tRBElem *) & lsaInfo, NULL)) != NULL)
    {
        u1Flag = OSIX_TRUE;
    }
    else
    {
        while ((pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface,
                                                  NULL)) != NULL)
        {
            if ((pLsaInfo =
                 (tV3OsLsaInfo *) RBTreeGetFirst (pInterface->
                                                  pLinkScopeLsaRBRoot)) != NULL)
            {
                u1Flag = OSIX_TRUE;
                break;
            }
        }
    }

    if (u1Flag == OSIX_TRUE)
    {
        *pi4NextOspfv3LinkLsdbIfIndex = (INT4) pInterface->u4InterfaceId;
        *pu4NextOspfv3LinkLsdbType = pLsaInfo->lsaId.u2LsaType;
        *pu4NextOspfv3LinkLsdbRouterId =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId);
        *pu4NextOspfv3LinkLsdbLsid =
            OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3LinkLsdbSequence
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid

                The Object 
                retValOspfv3LinkLsdbSequence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3LinkLsdbSequence (INT4 i4Ospfv3LinkLsdbIfIndex,
                              UINT4 u4Ospfv3LinkLsdbType,
                              UINT4 u4Ospfv3LinkLsdbRouterId,
                              UINT4 u4Ospfv3LinkLsdbLsid,
                              INT4 *pi4RetValOspfv3LinkLsdbSequence)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType,
                                             &(linkStateId),
                                             &(advRtrId),
                                             pInterface, NULL)) != NULL)
        {

            *pi4RetValOspfv3LinkLsdbSequence = pLsaInfo->lsaSeqNum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3LinkLsdbAge
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid

                The Object 
                retValOspfv3LinkLsdbAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3LinkLsdbAge (INT4 i4Ospfv3LinkLsdbIfIndex,
                         UINT4 u4Ospfv3LinkLsdbType,
                         UINT4 u4Ospfv3LinkLsdbRouterId,
                         UINT4 u4Ospfv3LinkLsdbLsid,
                         INT4 *pi4RetValOspfv3LinkLsdbAge)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType,
                                             &(linkStateId),
                                             &(advRtrId),
                                             pInterface, NULL)) != NULL)
        {

            *pi4RetValOspfv3LinkLsdbAge = (INT4) pLsaInfo->u2LsaAge;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3LinkLsdbChecksum
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid

                The Object 
                retValOspfv3LinkLsdbChecksum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3LinkLsdbChecksum (INT4 i4Ospfv3LinkLsdbIfIndex,
                              UINT4 u4Ospfv3LinkLsdbType,
                              UINT4 u4Ospfv3LinkLsdbRouterId,
                              UINT4 u4Ospfv3LinkLsdbLsid,
                              INT4 *pi4RetValOspfv3LinkLsdbChecksum)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType,
                                             &(linkStateId),
                                             &(advRtrId),
                                             pInterface, NULL)) != NULL)
        {

            *pi4RetValOspfv3LinkLsdbChecksum = (INT4) pLsaInfo->u2LsaChksum;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3LinkLsdbAdvertisement
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid

                The Object 
                retValOspfv3LinkLsdbAdvertisement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3LinkLsdbAdvertisement (INT4 i4Ospfv3LinkLsdbIfIndex,
                                   UINT4 u4Ospfv3LinkLsdbType,
                                   UINT4 u4Ospfv3LinkLsdbRouterId,
                                   UINT4 u4Ospfv3LinkLsdbLsid,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValOspfv3LinkLsdbAdvertisement)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) != NULL)
    {
        if ((pLsaInfo = V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType,
                                             &(linkStateId),
                                             &(advRtrId),
                                             pInterface, NULL)) != NULL)
        {
            MEMCPY (pRetValOspfv3LinkLsdbAdvertisement->pu1_OctetList,
                    pLsaInfo->pLsa, pLsaInfo->u2LsaLen);

            /* This Parameter is used for making Octet String. */
            pRetValOspfv3LinkLsdbAdvertisement->i4_Length =
                (INT4) pLsaInfo->u2LsaLen;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3LinkLsdbTypeKnown
 Input       :  The Indices
                Ospfv3LinkLsdbIfIndex
                Ospfv3LinkLsdbType
                Ospfv3LinkLsdbRouterId
                Ospfv3LinkLsdbLsid

                The Object 
                retValOspfv3LinkLsdbTypeKnown
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3LinkLsdbTypeKnown (INT4 i4Ospfv3LinkLsdbIfIndex,
                               UINT4 u4Ospfv3LinkLsdbType,
                               UINT4 u4Ospfv3LinkLsdbRouterId,
                               UINT4 u4Ospfv3LinkLsdbLsid,
                               INT4 *pi4RetValOspfv3LinkLsdbTypeKnown)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsRouterId       advRtrId;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_BUFFER_DWTOPDU (linkStateId, u4Ospfv3LinkLsdbLsid);
    OSPFV3_BUFFER_DWTOPDU (advRtrId, u4Ospfv3LinkLsdbRouterId);

    pInterface = (tV3OsInterface *) NULL;
    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3LinkLsdbIfIndex)) != NULL)
    {
        if ((V3LsuSearchDatabase ((UINT2) u4Ospfv3LinkLsdbType,
                                  &(linkStateId),
                                  &(advRtrId),
                                  pInterface, NULL) != NULL) &&
            (IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED
             ((UINT2) u4Ospfv3LinkLsdbType) != 0))
        {
            *pi4RetValOspfv3LinkLsdbTypeKnown =
                OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_SUCCESS);
            return SNMP_SUCCESS;
        }
    }

    *pi4RetValOspfv3LinkLsdbTypeKnown =
        OSPFV3_MAP_TO_TRUTH_VALUE (OSIX_SUCCESS);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ospfv3HostTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3HostTable
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3HostTable (INT4 *pi4Ospfv3HostAddressType,
                                 tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress)
{
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pHost =
         (tV3OsHost *) RBTreeGetFirst (pV3OspfCxt->pHostRBRoot)) != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (*pOspfv3HostAddress->pu1_OctetList,
                              pHost->hostIp6Addr);
        pOspfv3HostAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        *pi4Ospfv3HostAddressType = OSPFV3_INET_ADDR_TYPE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3HostTable
 Input       :  The Indices
                Ospfv3HostAddressType
                nextOspfv3HostAddressType
                Ospfv3HostAddress
                nextOspfv3HostAddress
 Output      :  The Get Next function gets the Next Index for
 l
 l
 l
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3HostTable (INT4 i4Ospfv3HostAddressType,
                                INT4 *pi4NextOspfv3HostAddressType,
                                tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pNextOspfv3HostAddress)
{
    tV3OsHost           currHost;
    tV3OsHost          *pNextHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3HostAddressType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (currHost.hostIp6Addr,
                          *(pOspfv3HostAddress->pu1_OctetList));

    if ((pNextHost = (tV3OsHost *) RBTreeGetNext (pV3OspfCxt->pHostRBRoot,
                                                  (tRBElem *) & currHost,
                                                  NULL)) != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (*pNextOspfv3HostAddress->pu1_OctetList,
                              pNextHost->hostIp6Addr);
        pOspfv3HostAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        *pi4NextOspfv3HostAddressType = OSPFV3_INET_ADDR_TYPE;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  V3GetFindHostInCxt
 Input       :  The Indices
                pV3OspfCxt
                pHostIp6Addr
  
 Output      :  None 
 Returns     :  pointer to the host on success
                NULL  otherwise
****************************************************************************/
PUBLIC tV3OsHost   *
V3GetFindHostInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pHostIp6Addr)
{
    tV3OsHost          *pHost = NULL;
    tV3OsHost           host;

    MEMCPY (&(host.hostIp6Addr), pHostIp6Addr, OSPFV3_IPV6_ADDR_LEN);

    pHost = ((tV3OsHost *) RBTreeGet (pV3OspfCxt->pHostRBRoot,
                                      (tRBElem *) & host));

    return pHost;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3HostMetric
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                retValOspfv3HostMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3HostMetric (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        INT4 *pi4RetValOspfv3HostMetric)
{
    tIp6Addr            hostIp6Addr;
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr, *(pOspfv3HostAddress->pu1_OctetList));

    if ((pHost = V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr)) != NULL)
    {
        *pi4RetValOspfv3HostMetric = (INT4) pHost->u4HostMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3HostStatus
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                retValOspfv3HostStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3HostStatus (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        INT4 *pi4RetValOspfv3HostStatus)
{
    tIp6Addr            hostIp6Addr;
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr, *(pOspfv3HostAddress->pu1_OctetList));

    if ((pHost = V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr)) != NULL)
    {
        *pi4RetValOspfv3HostStatus = (INT4) pHost->hostStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3HostAreaID
 Input       :  The Indices
                Ospfv3HostAddressType
                Ospfv3HostAddress

                The Object 
                retValOspfv3HostAreaID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3HostAreaID (INT4 i4Ospfv3HostAddressType,
                        tSNMP_OCTET_STRING_TYPE * pOspfv3HostAddress,
                        UINT4 *pu4RetValOspfv3HostAreaID)
{
    tIp6Addr            hostIp6Addr;
    tV3OsHost          *pHost = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3HostAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (hostIp6Addr, *(pOspfv3HostAddress->pu1_OctetList));

    if ((pHost = V3GetFindHostInCxt (pV3OspfCxt, &hostIp6Addr)) != NULL)
    {
        *pu4RetValOspfv3HostAreaID =
            OSPFV3_BUFFER_DWFROMPDU (pHost->pArea->areaId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3IfTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3IfTable
 Input       :  The Indices
                Ospfv3IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3IfTable (INT4 *pi4Ospfv3IfIndex)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot))
        != NULL)
    {
        *pi4Ospfv3IfIndex = (INT4) pInterface->u4InterfaceId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3IfTable
 Input       :  The Indices
                Ospfv3IfIndex
                nextOspfv3IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3IfTable (INT4 i4Ospfv3IfIndex, INT4 *pi4NextOspfv3IfIndex)
{
    tV3OsInterface      currIf;
    tV3OsInterface     *pNextIf = NULL;

    currIf.u4InterfaceId = (UINT4) i4Ospfv3IfIndex;

    if ((pNextIf = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                     (tRBElem *) & currIf,
                                                     NULL)) != NULL)
    {
        *pi4NextOspfv3IfIndex = (INT4) pNextIf->u4InterfaceId;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  V3GetFindIf
 Input       :  The Indices
                 u4IfIndex
  
 Output      :  None 
 Returns     :  pointer to the interface on success
                NULL  otherwise
****************************************************************************/
PUBLIC tV3OsInterface *
V3GetFindIf (UINT4 u4IfIndex)
{
    tV3OsInterface      curInterface;
    tV3OsInterface     *pInterface = NULL;

    curInterface.u4InterfaceId = u4IfIndex;

    pInterface = (tV3OsInterface *)
        RBTreeGet (gV3OsRtr.pIfRBRoot, (tRBElem *) & curInterface);

    return pInterface;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3IfAreaId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfAreaId (INT4 i4Ospfv3IfIndex, UINT4 *pu4RetValOspfv3IfAreaId)
{
    tV3OsAreaId         areaId;
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        if (pInterface->pArea != NULL)
        {
            OSPFV3_AREA_ID_COPY (areaId, pInterface->pArea->areaId);
            *pu4RetValOspfv3IfAreaId = OSPFV3_BUFFER_DWFROMPDU (areaId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfType
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfType (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfType)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfType = (INT4) pInterface->u1NetworkType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfAdminStat
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfAdminStat
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfAdminStat (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfAdminStat)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfAdminStat = (INT4) pInterface->admnStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfRtrPriority
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfRtrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfRtrPriority (INT4 i4Ospfv3IfIndex,
                           INT4 *pi4RetValOspfv3IfRtrPriority)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfRtrPriority = (INT4) pInterface->u1RtrPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfTransitDelay
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfTransitDelay (INT4 i4Ospfv3IfIndex,
                            INT4 *pi4RetValOspfv3IfTransitDelay)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfTransitDelay = (INT4) pInterface->u2IfTransDelay;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfRetransInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfRetransInterval (INT4 i4Ospfv3IfIndex,
                               INT4 *pi4RetValOspfv3IfRetransInterval)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfRetransInterval = (INT4) pInterface->u2RxmtInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfHelloInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfHelloInterval (INT4 i4Ospfv3IfIndex,
                             INT4 *pi4RetValOspfv3IfHelloInterval)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfHelloInterval = (INT4) pInterface->u2HelloInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfRtrDeadInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfRtrDeadInterval (INT4 i4Ospfv3IfIndex,
                               INT4 *pi4RetValOspfv3IfRtrDeadInterval)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfRtrDeadInterval =
            (UINT4) pInterface->u2RtrDeadInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfPollInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfPollInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfPollInterval (INT4 i4Ospfv3IfIndex,
                            UINT4 *pu4RetValOspfv3IfPollInterval)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pu4RetValOspfv3IfPollInterval = pInterface->u4PollInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfState
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfState (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfState)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfState = (INT4) pInterface->u1IsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfDesignatedRouter
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfDesignatedRouter (INT4 i4Ospfv3IfIndex,
                                UINT4 *pu4RetValOspfv3IfDesignatedRouter)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsRouterId       desgRtr;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        OSPFV3_RTR_ID_COPY (desgRtr, OSPFV3_GET_DR (pInterface));
        *pu4RetValOspfv3IfDesignatedRouter = OSPFV3_BUFFER_DWFROMPDU (desgRtr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfBackupDesignatedRouter
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfBackupDesignatedRouter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfBackupDesignatedRouter (INT4 i4Ospfv3IfIndex,
                                      UINT4
                                      *pu4RetValOspfv3IfBackupDesignatedRouter)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsRouterId       backupDR;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        OSPFV3_RTR_ID_COPY (backupDR, OSPFV3_GET_BDR (pInterface));
        *pu4RetValOspfv3IfBackupDesignatedRouter =
            OSPFV3_BUFFER_DWFROMPDU (backupDR);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfEvents
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfEvents (INT4 i4Ospfv3IfIndex, UINT4 *pu4RetValOspfv3IfEvents)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pu4RetValOspfv3IfEvents = pInterface->u4IfEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfStatus
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfStatus (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfStatus)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfStatus = pInterface->ifStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfMulticastForwarding
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfMulticastForwarding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfMulticastForwarding (INT4 i4Ospfv3IfIndex,
                                   INT4 *pi4RetValOspfv3IfMulticastForwarding)
{
    UNUSED_PARAM (i4Ospfv3IfIndex);

    *pi4RetValOspfv3IfMulticastForwarding = 1;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfDemand
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfDemand
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfDemand (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfDemand)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfDemand =
            OSPFV3_MAP_TO_TRUTH_VALUE (pInterface->bDcEndpt);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfMetricValue
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfMetricValue (INT4 i4Ospfv3IfIndex,
                           INT4 *pi4RetValOspfv3IfMetricValue)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfMetricValue = (INT4) pInterface->u4IfMetric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfLinkScopeLsaCount
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfLinkScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfLinkScopeLsaCount (INT4 i4Ospfv3IfIndex,
                                 UINT4 *pu4RetValOspfv3IfLinkScopeLsaCount)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pu4RetValOspfv3IfLinkScopeLsaCount = pInterface->u4LinkScopeLsaCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfLinkLsaCksumSum
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfLinkLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfLinkLsaCksumSum (INT4 i4Ospfv3IfIndex,
                               INT4 *pi4RetValOspfv3IfLinkLsaCksumSum)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfLinkLsaCksumSum =
            (INT4) pInterface->u4LinkScopeLsaChkSumsum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfInstId
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfInstId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfInstId (INT4 i4Ospfv3IfIndex, INT4 *pi4RetValOspfv3IfInstId)
{
    tV3OsInterface     *pInterface = NULL;
    INT1                i1RetVal = SNMP_FAILURE;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfInstId = (INT4) pInterface->u2InstId;
        i1RetVal = SNMP_SUCCESS;
    }
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfDemandNbrProbe
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfDemandNbrProbe
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfDemandNbrProbe (INT4 i4Ospfv3IfIndex,
                              INT4 *pi4RetValOspfv3IfDemandNbrProbe)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pi4RetValOspfv3IfDemandNbrProbe =
            OSPFV3_MAP_TO_TRUTH_VALUE (pInterface->bDemandNbrProbe);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfDemandNbrProbeRetxLimit
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfDemandNbrProbeRetxLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfDemandNbrProbeRetxLimit (INT4 i4Ospfv3IfIndex,
                                       UINT4
                                       *pu4RetValOspfv3IfDemandNbrProbeRetxLimit)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pu4RetValOspfv3IfDemandNbrProbeRetxLimit =
            pInterface->u4NbrProbeRxmtLimit;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3IfDemandNbrProbeInterval
 Input       :  The Indices
                Ospfv3IfIndex

                The Object 
                retValOspfv3IfDemandNbrProbeInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3IfDemandNbrProbeInterval (INT4 i4Ospfv3IfIndex,
                                      UINT4
                                      *pu4RetValOspfv3IfDemandNbrProbeInterval)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4Ospfv3IfIndex)) != NULL)
    {
        *pu4RetValOspfv3IfDemandNbrProbeInterval =
            pInterface->u4NbrProbeInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3VirtIfTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3VirtIfTable (UINT4 *pu4Ospfv3VirtIfAreaId,
                                   UINT4 *pu4Ospfv3VirtIfNeighbor)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLstNode = TMO_SLL_First (&(pV3OspfCxt->virtIfLst))) != NULL)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        OSPFV3_AREA_ID_COPY (transitAreaId, pInterface->transitAreaId);
        OSPFV3_RTR_ID_COPY (nbrId, pInterface->destRtrId);

        *pu4Ospfv3VirtIfAreaId = OSPFV3_BUFFER_DWFROMPDU (transitAreaId);
        *pu4Ospfv3VirtIfNeighbor = OSPFV3_BUFFER_DWFROMPDU (nbrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3VirtIfTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                nextOspfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
                nextOspfv3VirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3VirtIfTable (UINT4 u4Ospfv3VirtIfAreaId,
                                  UINT4 *pu4NextOspfv3VirtIfAreaId,
                                  UINT4 u4Ospfv3VirtIfNeighbor,
                                  UINT4 *pu4NextOspfv3VirtIfNeighbor)
{
    tV3OsAreaId         currAreaId;
    tV3OsRouterId       currNbrId;
    tV3OsAreaId         nextAreaId;
    tV3OsRouterId       nextNbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (currNbrId, u4Ospfv3VirtIfNeighbor);
    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        switch (V3UtilAreaIdComp (currAreaId, pInterface->transitAreaId))
        {
            case OSPFV3_GREATER:
                /* try next entry */
                continue;

            case OSPFV3_EQUAL:
                /* first index is complete */
                switch (V3UtilRtrIdComp (currNbrId, pInterface->destRtrId))
                {
                    case OSPFV3_GREATER:
                        /* try next entry */
                        continue;
                    case OSPFV3_EQUAL:
                        /* second index is complete */
                        /* try next entry */
                        continue;
                        /* next found, fall through */
                    default:
                        /* Default case hits when the returned value is
                         * OSPFV3_LESS
                         */
                        break;
                }
                /* next found, fall through */

            default:
                /* default occurs for OSPFV3_LESS */
                OSPFV3_AREA_ID_COPY (nextAreaId, pInterface->transitAreaId);
                OSPFV3_RTR_ID_COPY (nextNbrId, pInterface->destRtrId);

                *pu4NextOspfv3VirtIfAreaId =
                    OSPFV3_BUFFER_DWFROMPDU (nextAreaId);
                *pu4NextOspfv3VirtIfNeighbor =
                    OSPFV3_BUFFER_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/****************************************************************************
 Function    :  V3GetFindVirtIfInCxt
 Input       :  The Indices
                pV3OspfCxt
                pTransitAreaId
                pNbrId
 
 Output      :  The Get function gets the virtual interface for
                the given transit area Id & Nbr Id values. 
 Returns     :  pointer to the interface on success
                NULL otherwise 
****************************************************************************/

PUBLIC tV3OsInterface *
V3GetFindVirtIfInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTransitAreaId,
                      tV3OsRouterId * pNbrId)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (pV3OspfCxt == NULL)
    {
        return NULL;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);

        if ((pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
            && (V3UtilVirtIfIndComp (*pTransitAreaId, *pNbrId,
                                     pInterface->transitAreaId,
                                     pInterface->destRtrId) == OSPFV3_EQUAL))
        {
            return pInterface;
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfIndex
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfIndex (UINT4 u4Ospfv3VirtIfAreaId,
                         UINT4 u4Ospfv3VirtIfNeighbor,
                         INT4 *pi4RetValOspfv3VirtIfIndex)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfIndex = pInterface->u4InterfaceId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfTransitDelay
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfTransitDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfTransitDelay (UINT4 u4Ospfv3VirtIfAreaId,
                                UINT4 u4Ospfv3VirtIfNeighbor,
                                INT4 *pi4RetValOspfv3VirtIfTransitDelay)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfTransitDelay = pInterface->u2IfTransDelay;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfRetransInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfRetransInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfRetransInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 *pi4RetValOspfv3VirtIfRetransInterval)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfRetransInterval =
            (INT4) pInterface->u2RxmtInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfHelloInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfHelloInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfHelloInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                 UINT4 u4Ospfv3VirtIfNeighbor,
                                 INT4 *pi4RetValOspfv3VirtIfHelloInterval)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfHelloInterval =
            (INT4) pInterface->u2HelloInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfRtrDeadInterval
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfRtrDeadInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfRtrDeadInterval (UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 *pi4RetValOspfv3VirtIfRtrDeadInterval)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfRtrDeadInterval =
            (INT4) pInterface->u2RtrDeadInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfState
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfState (UINT4 u4Ospfv3VirtIfAreaId,
                         UINT4 u4Ospfv3VirtIfNeighbor,
                         INT4 *pi4RetValOspfv3VirtIfState)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfState = (UINT4) pInterface->u1IsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfEvents
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfEvents (UINT4 u4Ospfv3VirtIfAreaId,
                          UINT4 u4Ospfv3VirtIfNeighbor,
                          UINT4 *pu4RetValOspfv3VirtIfEvents)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pu4RetValOspfv3VirtIfEvents = (UINT4) pInterface->u4IfEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfStatus
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfStatus (UINT4 u4Ospfv3VirtIfAreaId,
                          UINT4 u4Ospfv3VirtIfNeighbor,
                          INT4 *pi4RetValOspfv3VirtIfStatus)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfStatus = (UINT4) pInterface->ifStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfLinkScopeLsaCount
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfLinkScopeLsaCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfLinkScopeLsaCount (UINT4 u4Ospfv3VirtIfAreaId,
                                     UINT4 u4Ospfv3VirtIfNeighbor,
                                     UINT4
                                     *pu4RetValOspfv3VirtIfLinkScopeLsaCount)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pu4RetValOspfv3VirtIfLinkScopeLsaCount =
            pInterface->u4LinkScopeLsaCount;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtIfLinkLsaCksumSum
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object 
                retValOspfv3VirtIfLinkLsaCksumSum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtIfLinkLsaCksumSum (UINT4 u4Ospfv3VirtIfAreaId,
                                   UINT4 u4Ospfv3VirtIfNeighbor,
                                   INT4 *pi4RetValOspfv3VirtIfLinkLsaCksumSum)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                            &(nbrId))) != NULL)
    {
        *pi4RetValOspfv3VirtIfLinkLsaCksumSum =
            (INT4) pInterface->u4LinkScopeLsaChkSumsum;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3NbrTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3NbrTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3NbrTable (INT4 *pi4Ospfv3NbrIfIndex,
                                UINT4 *pu4Ospfv3NbrRtrId)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gV3OsRtr.pNbrTableCache = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if ((pNbr->u1ConfigStatus != OSPFV3_CONFIGURED_NBR) &&
            (pNbr->u1ConfigStatus != OSPFV3_DISCOVERED_SBYIF))
        {
            *pi4Ospfv3NbrIfIndex = (INT4) pNbr->pInterface->u4InterfaceId;
            *pu4Ospfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId);
            gV3OsRtr.pNbrTableCache = pLstNode;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3NbrTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                nextOspfv3NbrIfIndex
                Ospfv3NbrRtrId
                nextOspfv3NbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3NbrTable (INT4 i4Ospfv3NbrIfIndex,
                               INT4 *pi4NextOspfv3NbrIfIndex,
                               UINT4 u4Ospfv3NbrRtrId,
                               UINT4 *pu4NextOspfv3NbrRtrId)
{
    tV3OsRouterId       currNbrRtrId;
    tV3OsRouterId       nextNbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currNbrRtrId, u4Ospfv3NbrRtrId);

    if (gV3OsRtr.pNbrTableCache != NULL)
    {
        for (;;)
        {
            gV3OsRtr.pNbrTableCache =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&(pV3OspfCxt->sortNbrLst),
                                                gV3OsRtr.pNbrTableCache);

            if (gV3OsRtr.pNbrTableCache == NULL)
            {
                return SNMP_FAILURE;
            }

            pNbr =
                OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                     gV3OsRtr.pNbrTableCache);

            if ((pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR) ||
                (pNbr->u1ConfigStatus == OSPFV3_DISCOVERED_SBYIF))
            {
                continue;
            }

            if (pNbr->pInterface->u4InterfaceId == (UINT4) i4Ospfv3NbrIfIndex)
            {
                if (V3UtilRtrIdComp (pNbr->nbrRtrId, currNbrRtrId) ==
                    OSPFV3_GREATER)
                {
                    OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                    *pi4NextOspfv3NbrIfIndex =
                        (INT4) pNbr->pInterface->u4InterfaceId;
                    *pu4NextOspfv3NbrRtrId =
                        OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                    return SNMP_SUCCESS;
                }
            }
            else if (pNbr->pInterface->u4InterfaceId >
                     (UINT4) i4Ospfv3NbrIfIndex)
            {
                OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                *pi4NextOspfv3NbrIfIndex =
                    (INT4) pNbr->pInterface->u4InterfaceId;
                *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                return SNMP_SUCCESS;
            }
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        gV3OsRtr.pNbrTableCache = pLstNode;
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR)
        {
            continue;
        }

        if (pNbr->pInterface->u4InterfaceId == (UINT4) i4Ospfv3NbrIfIndex)
        {
            if (V3UtilRtrIdComp (pNbr->nbrRtrId, currNbrRtrId) ==
                OSPFV3_GREATER)
            {
                OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                *pi4NextOspfv3NbrIfIndex =
                    (INT4) pNbr->pInterface->u4InterfaceId;
                *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                return SNMP_SUCCESS;
            }
        }
        else if (pNbr->pInterface->u4InterfaceId > (UINT4) i4Ospfv3NbrIfIndex)
        {
            OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
            *pi4NextOspfv3NbrIfIndex = (INT4) pNbr->pInterface->u4InterfaceId;
            *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/******************************************************************************
Function : V3GetFindNbrInCxt

Input    : pV3OspfCxt
           pNbrRtrId
           u4NbrIfIndex

Output   : None

Returns  : Neighbour structure
******************************************************************************/
PUBLIC tV3OsNeighbor *
V3GetFindNbrInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pNbrRtrId,
                   UINT4 u4NbrIfIndex)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (NULL != gV3OsRtr.pNbrTableCache)
    {
        pNbr =
            OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                 gV3OsRtr.pNbrTableCache);

        if (V3UtilRtrIdComp (*pNbrRtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL)
        {
            if (u4NbrIfIndex == pNbr->pInterface->u4InterfaceId)
            {
                return pNbr;
            }
        }
        pNbr = NULL;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (V3UtilRtrIdComp (*pNbrRtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL)
        {
            if (u4NbrIfIndex == pNbr->pInterface->u4InterfaceId)
            {
                return pNbr;
            }
        }
    }

    return NULL;
}

/******************************************************************************
Function : V3GetFindNbrInCxtForBfd

Input    : pV3OspfCxt
           pNbrRtrId
           u4NbrIfIndex

Output   : None

Returns  : Neighbour structure
******************************************************************************/
PUBLIC tV3OsNeighbor *
V3GetFindNbrInCxtForBfd (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pNbrIpAddr)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if (NULL != gV3OsRtr.pNbrTableCache)
    {
        pNbr =
            OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                 gV3OsRtr.pNbrTableCache);

        if (V3UtilIp6AddrComp (pNbrIpAddr, &(pNbr->nbrIpv6Addr)) ==
            OSPFV3_EQUAL)
        {
            return pNbr;
        }
        pNbr = NULL;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (V3UtilIp6AddrComp (pNbrIpAddr, &(pNbr->nbrIpv6Addr)) ==
            OSPFV3_EQUAL)
        {
            return pNbr;
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3NbrAddressType
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrAddressType (INT4 i4Ospfv3NbrIfIndex,
                            UINT4 u4Ospfv3NbrRtrId,
                            INT4 *pi4RetValOspfv3NbrAddressType)
{

    tV3OsRouterId       nbrRtrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if (V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                           (UINT4) i4Ospfv3NbrIfIndex) != NULL)
    {
        *pi4RetValOspfv3NbrAddressType = OSPFV3_INET_ADDR_TYPE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrAddress
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrAddress (INT4 i4Ospfv3NbrIfIndex,
                        UINT4 u4Ospfv3NbrRtrId,
                        tSNMP_OCTET_STRING_TYPE * pRetValOspfv3NbrAddress)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (*pRetValOspfv3NbrAddress->pu1_OctetList,
                              pNbr->nbrIpv6Addr);
        pRetValOspfv3NbrAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrOptions
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrOptions (INT4 i4Ospfv3NbrIfIndex, UINT4 u4Ospfv3NbrRtrId,
                        INT4 *pi4RetValOspfv3NbrOptions)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        MEMCPY (pi4RetValOspfv3NbrOptions, pNbr->nbrOptions, OSPFV3_OPTION_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrPriority
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrPriority (INT4 i4Ospfv3NbrIfIndex, UINT4 u4Ospfv3NbrRtrId,
                         INT4 *pi4RetValOspfv3NbrPriority)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrPriority = (INT4) pNbr->u1NbrRtrPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrState
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrState (INT4 i4Ospfv3NbrIfIndex, UINT4 u4Ospfv3NbrRtrId,
                      INT4 *pi4RetValOspfv3NbrState)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrState = (INT4) pNbr->u1NsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrEvents
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrEvents (INT4 i4Ospfv3NbrIfIndex, UINT4 u4Ospfv3NbrRtrId,
                       UINT4 *pu4RetValOspfv3NbrEvents)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pu4RetValOspfv3NbrEvents = pNbr->u4NbrEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrLsRetransQLen
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrLsRetransQLen (INT4 i4Ospfv3NbrIfIndex, UINT4 u4Ospfv3NbrRtrId,
                              UINT4 *pu4RetValOspfv3NbrLsRetransQLen)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pu4RetValOspfv3NbrLsRetransQLen = TMO_SLL_Count (&(pNbr->lsaRxmtLst));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrHelloSuppressed
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrHelloSuppressed (INT4 i4Ospfv3NbrIfIndex,
                                UINT4 u4Ospfv3NbrRtrId,
                                INT4 *pi4RetValOspfv3NbrHelloSuppressed)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrHelloSuppressed =
            OSPFV3_MAP_TO_TRUTH_VALUE (pNbr->bHelloSuppression);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrIfId
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrIfId (INT4 i4Ospfv3NbrIfIndex,
                     UINT4 u4Ospfv3NbrRtrId, INT4 *pi4RetValOspfv3NbrIfId)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrIfId = (INT4) pNbr->u4NbrInterfaceId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrRestartHelperStatus
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrRestartHelperStatus (INT4 i4Ospfv3NbrIfIndex,
                                    UINT4 u4Ospfv3NbrRtrId,
                                    INT4 *pi4RetValOspfv3NbrRestartHelperStatus)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrRestartHelperStatus = (INT4) pNbr->u1NbrHelperStatus;
        return SNMP_SUCCESS;
    }
    *pi4RetValOspfv3NbrRestartHelperStatus = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrRestartHelperAge
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrRestartHelperAge (INT4 i4Ospfv3NbrIfIndex,
                                 UINT4 u4Ospfv3NbrRtrId,
                                 INT4 *pi4RetValOspfv3NbrRestartHelperAge)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
        {
            if (TmrGetRemainingTime
                (gV3OsRtr.timerLstId,
                 (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                 (UINT4 *) pi4RetValOspfv3NbrRestartHelperAge) == TMR_FAILURE)
            {
                *pi4RetValOspfv3NbrRestartHelperAge = 0;
            }
            else
            {
                *pi4RetValOspfv3NbrRestartHelperAge =
                    (*pi4RetValOspfv3NbrRestartHelperAge /
                     OSPFV3_NO_OF_TICKS_PER_SEC);
            }
        }
        else
        {
            *pi4RetValOspfv3NbrRestartHelperAge = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbrRestartHelperExitReason
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object 
                retValOspfv3NbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbrRestartHelperExitReason (INT4 i4Ospfv3NbrIfIndex,
                                        UINT4 u4Ospfv3NbrRtrId,
                                        INT4
                                        *pi4RetValOspfv3NbrRestartHelperExitReason)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbrRestartHelperExitReason =
            (INT4) pNbr->u1NbrHelperExitReason;
        return SNMP_SUCCESS;
    }

    *pi4RetValOspfv3NbrRestartHelperExitReason = 0;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3NbmaNbrTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3NbmaNbrTable
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3NbmaNbrTable (INT4 *pi4Ospfv3NbmaNbrIfIndex,
                                    INT4 *pi4Ospfv3NbmaNbrAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pOspfv3NbmaNbrAddress)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);
        if (pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR)
        {
            *pi4Ospfv3NbmaNbrAddressType = OSPFV3_INET_ADDR_TYPE;
            *pi4Ospfv3NbmaNbrIfIndex = (INT4) pNbr->pInterface->u4InterfaceId;
            OSPFV3_IP6_ADDR_COPY (*pOspfv3NbmaNbrAddress->pu1_OctetList,
                                  pNbr->nbrIpv6Addr);
            pOspfv3NbmaNbrAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3NbmaNbrTable
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                nextOspfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                nextOspfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress
                nextOspfv3NbmaNbrAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3NbmaNbrTable (INT4 i4Ospfv3NbmaNbrIfIndex,
                                   INT4 *pi4NextOspfv3NbmaNbrIfIndex,
                                   INT4 i4Ospfv3NbmaNbrAddressType,
                                   INT4 *pi4NextOspfv3NbmaNbrAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pOspfv3NbmaNbrAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextOspfv3NbmaNbrAddress)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3NbmaNbrAddressType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);
        if (pNbr->u1ConfigStatus != OSPFV3_CONFIGURED_NBR)
        {
            continue;
        }

        switch (V3UtilIp6AddrComp
                ((tIp6Addr *) (VOID *) pOspfv3NbmaNbrAddress->pu1_OctetList,
                 &(pNbr->nbrIpv6Addr)))
        {
            case OSPFV3_GREATER:
                continue;
            case OSPFV3_EQUAL:
                if ((UINT4) i4Ospfv3NbmaNbrIfIndex >=
                    pNbr->pInterface->u4InterfaceId)
                {
                    continue;
                }
                /* fall through */
            default:
                /* Default case hits when the returned value is
                 * OSPFV3_LESS
                 */
                OSPFV3_IP6_ADDR_COPY (*pNextOspfv3NbmaNbrAddress->pu1_OctetList,
                                      pNbr->nbrIpv6Addr);
                pNextOspfv3NbmaNbrAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
                *pi4NextOspfv3NbmaNbrIfIndex =
                    (INT4) pNbr->pInterface->u4InterfaceId;
                *pi4NextOspfv3NbmaNbrAddressType = OSPFV3_INET_ADDR_TYPE;
                return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Utility routine used by other get routines */
/******************************************************************************
Function : V3GetFindNbmaNbrInCxt

Input    : pV3OspfCxt
           pNbrRtrId
           u4NbrIfIndex

Output   : None

Returns  : Neighbour structure
******************************************************************************/
PUBLIC tV3OsNeighbor *
V3GetFindNbmaNbrInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pIp6Addr,
                       UINT4 u4NbrIfIndex)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);
        if (pNbr->u1ConfigStatus != OSPFV3_CONFIGURED_NBR)
        {
            continue;
        }

        if (V3UtilIp6AddrComp (pIp6Addr, &(pNbr->nbrIpv6Addr)) == OSPFV3_EQUAL)
        {
            if (u4NbrIfIndex == pNbr->pInterface->u4InterfaceId)
            {
                return pNbr;
            }
        }
    }

    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3NbmaNbrPriority
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                retValOspfv3NbmaNbrPriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbmaNbrPriority (INT4 i4Ospfv3NbmaNbrIfIndex,
                             INT4 i4Ospfv3NbmaNbrAddressType,
                             tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                             INT4 *pi4RetValOspfv3NbmaNbrPriority)
{
    tIp6Addr            nbrIp6Addr;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_IP6_ADDR_COPY (nbrIp6Addr, *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if ((pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrIp6Addr,
                                       (UINT4) i4Ospfv3NbmaNbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbmaNbrPriority =
            (INT4) pNbr->u1ConfiguredNbrRtrPriority;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbmaNbrRtrId
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                retValOspfv3NbmaNbrRtrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbmaNbrRtrId (INT4 i4Ospfv3NbmaNbrIfIndex,
                          INT4 i4Ospfv3NbmaNbrAddressType,
                          tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                          UINT4 *pu4RetValOspfv3NbmaNbrRtrId)
{
    tIp6Addr            nbrIp6Addr;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (nbrIp6Addr, *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if ((pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrIp6Addr,
                                       (UINT4) i4Ospfv3NbmaNbrIfIndex)) != NULL)
    {
        *pu4RetValOspfv3NbmaNbrRtrId = OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbmaNbrState
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                retValOspfv3NbmaNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbmaNbrState (INT4 i4Ospfv3NbmaNbrIfIndex,
                          INT4 i4Ospfv3NbmaNbrAddressType,
                          tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                          INT4 *pi4RetValOspfv3NbmaNbrState)
{
    tIp6Addr            nbrIp6Addr;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (nbrIp6Addr, *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if ((pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrIp6Addr,
                                       (UINT4) i4Ospfv3NbmaNbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbmaNbrState = (INT4) pNbr->u1NsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbmaNbrStorageType
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                retValOspfv3NbmaNbrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbmaNbrStorageType (INT4 i4Ospfv3NbmaNbrIfIndex,
                                INT4 i4Ospfv3NbmaNbrAddressType,
                                tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                                INT4 *pi4RetValOspfv3NbmaNbrStorageType)
{
    UNUSED_PARAM (i4Ospfv3NbmaNbrIfIndex);
    UNUSED_PARAM (i4Ospfv3NbmaNbrAddressType);
    UNUSED_PARAM (pOspfv3NbmaNbrAddress);

    *pi4RetValOspfv3NbmaNbrStorageType = OSPFV3_NON_VOLATILE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetOspfv3NbmaNbrStatus
 Input       :  The Indices
                Ospfv3NbmaNbrIfIndex
                Ospfv3NbmaNbrAddressType
                Ospfv3NbmaNbrAddress

                The Object 
                retValOspfv3NbmaNbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3NbmaNbrStatus (INT4 i4Ospfv3NbmaNbrIfIndex,
                           INT4 i4Ospfv3NbmaNbrAddressType,
                           tSNMP_OCTET_STRING_TYPE * pOspfv3NbmaNbrAddress,
                           INT4 *pi4RetValOspfv3NbmaNbrStatus)
{
    tIp6Addr            nbrIp6Addr;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3NbmaNbrAddressType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (nbrIp6Addr, *(pOspfv3NbmaNbrAddress->pu1_OctetList));

    if ((pNbr = V3GetFindNbmaNbrInCxt (pV3OspfCxt, &nbrIp6Addr,
                                       (UINT4) i4Ospfv3NbmaNbrIfIndex)) != NULL)
    {
        *pi4RetValOspfv3NbmaNbrStatus = pNbr->nbrStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3VirtNbrTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3VirtNbrTable
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3VirtNbrTable (UINT4 *pu4Ospfv3VirtNbrArea,
                                    UINT4 *pu4Ospfv3VirtNbrRtrId)
{
    return nmhGetFirstIndexOspfv3VirtIfTable (pu4Ospfv3VirtNbrArea,
                                              pu4Ospfv3VirtNbrRtrId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3VirtNbrTable
 Input       :  The Indices
                Ospfv3VirtNbrArea
                nextOspfv3VirtNbrArea
                Ospfv3VirtNbrRtrId
                nextOspfv3VirtNbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3VirtNbrTable (UINT4 u4Ospfv3VirtNbrArea,
                                   UINT4 *pu4NextOspfv3VirtNbrArea,
                                   UINT4 u4Ospfv3VirtNbrRtrId,
                                   UINT4 *pu4NextOspfv3VirtNbrRtrId)
{
    return nmhGetNextIndexOspfv3VirtIfTable (u4Ospfv3VirtNbrArea,
                                             pu4NextOspfv3VirtNbrArea,
                                             u4Ospfv3VirtNbrRtrId,
                                             pu4NextOspfv3VirtNbrRtrId);
}

/* Utility routine used by other get routines */
/******************************************************************************
Function : V3GetFindVirtNbrInCxt

Input    : pV3OspfCxt
           pTransitAreaId
           pNbrId

Output   : None

Returns  : Neighbour structure
******************************************************************************/
PUBLIC tV3OsNeighbor *
V3GetFindVirtNbrInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTransitAreaId,
                       tV3OsRouterId * pNbrId)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    if ((pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, pTransitAreaId,
                                            pNbrId)) != NULL)
    {
        if ((pLstNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
        {
            return (OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode));
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrIfIndex
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrIfIndex (UINT4 u4Ospfv3VirtNbrArea,
                            UINT4 u4Ospfv3VirtNbrRtrId,
                            INT4 *pi4RetValOspfv3VirtNbrIfIndex)
{

    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrIfIndex = pNbr->pInterface->u4InterfaceId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrAddressType
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrAddressType (UINT4 u4Ospfv3VirtNbrArea,
                                UINT4 u4Ospfv3VirtNbrRtrId,
                                INT4 *pi4RetValOspfv3VirtNbrAddressType)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if (V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId, &nbrId) != NULL)
    {
        *pi4RetValOspfv3VirtNbrAddressType = OSPFV3_INET_ADDR_TYPE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrAddress
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrAddress (UINT4 u4Ospfv3VirtNbrArea,
                            UINT4 u4Ospfv3VirtNbrRtrId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValOspfv3VirtNbrAddress)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (*pRetValOspfv3VirtNbrAddress->pu1_OctetList,
                              pNbr->nbrIpv6Addr);
        pRetValOspfv3VirtNbrAddress->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrOptions
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrOptions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrOptions (UINT4 u4Ospfv3VirtNbrArea,
                            UINT4 u4Ospfv3VirtNbrRtrId,
                            INT4 *pi4RetValOspfv3VirtNbrOptions)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        MEMCPY (pi4RetValOspfv3VirtNbrOptions, pNbr->nbrOptions,
                OSPFV3_OPTION_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrState
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrState (UINT4 u4Ospfv3VirtNbrArea,
                          UINT4 u4Ospfv3VirtNbrRtrId,
                          INT4 *pi4RetValOspfv3VirtNbrState)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrState = pNbr->u1NsmState + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrEvents
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrEvents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrEvents (UINT4 u4Ospfv3VirtNbrArea,
                           UINT4 u4Ospfv3VirtNbrRtrId,
                           UINT4 *pu4RetValOspfv3VirtNbrEvents)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pu4RetValOspfv3VirtNbrEvents = pNbr->u4NbrEvents;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrLsRetransQLen
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrLsRetransQLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrLsRetransQLen (UINT4 u4Ospfv3VirtNbrArea,
                                  UINT4 u4Ospfv3VirtNbrRtrId,
                                  UINT4 *pu4RetValOspfv3VirtNbrLsRetransQLen)
{

    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pu4RetValOspfv3VirtNbrLsRetransQLen =
            TMO_SLL_Count (&(pNbr->lsaRxmtLst));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrHelloSuppressed
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrHelloSuppressed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrHelloSuppressed (UINT4 u4Ospfv3VirtNbrArea,
                                    UINT4 u4Ospfv3VirtNbrRtrId,
                                    INT4 *pi4RetValOspfv3VirtNbrHelloSuppressed)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrHelloSuppressed =
            OSPFV3_MAP_TO_TRUTH_VALUE (pNbr->bHelloSuppression);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrIfId
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrIfId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrIfId (UINT4 u4Ospfv3VirtNbrArea,
                         UINT4 u4Ospfv3VirtNbrRtrId,
                         INT4 *pi4RetValOspfv3VirtNbrIfId)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrIfId = (INT4) pNbr->u4NbrInterfaceId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrRestartHelperStatus
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrRestartHelperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrRestartHelperStatus (UINT4 u4Ospfv3VirtNbrArea,
                                        UINT4 u4Ospfv3VirtNbrRtrId,
                                        INT4
                                        *pi4RetValOspfv3VirtNbrRestartHelperStatus)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrRestartHelperStatus =
            (INT4) pNbr->u1NbrHelperStatus;
        return SNMP_SUCCESS;
    }

    *pi4RetValOspfv3VirtNbrRestartHelperStatus = 0;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrRestartHelperAge
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrRestartHelperAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrRestartHelperAge (UINT4 u4Ospfv3VirtNbrArea,
                                     UINT4 u4Ospfv3VirtNbrRtrId,
                                     INT4
                                     *pi4RetValOspfv3VirtNbrRestartHelperAge)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
        {
            if (TmrGetRemainingTime
                (gV3OsRtr.timerLstId,
                 (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                 (UINT4 *) pi4RetValOspfv3VirtNbrRestartHelperAge) ==
                TMR_FAILURE)
            {
                *pi4RetValOspfv3VirtNbrRestartHelperAge = 0;
            }
            else
            {
                *pi4RetValOspfv3VirtNbrRestartHelperAge =
                    (*pi4RetValOspfv3VirtNbrRestartHelperAge /
                     OSPFV3_NO_OF_TICKS_PER_SEC);
            }
        }
        else
        {
            *pi4RetValOspfv3VirtNbrRestartHelperAge = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3VirtNbrRestartHelperExitReason
 Input       :  The Indices
                Ospfv3VirtNbrArea
                Ospfv3VirtNbrRtrId

                The Object 
                retValOspfv3VirtNbrRestartHelperExitReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3VirtNbrRestartHelperExitReason (UINT4 u4Ospfv3VirtNbrArea,
                                            UINT4 u4Ospfv3VirtNbrRtrId,
                                            INT4
                                            *pi4RetValOspfv3VirtNbrRestartHelperExitReason)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtNbrArea);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtNbrRtrId);

    if ((pNbr = V3GetFindVirtNbrInCxt (pV3OspfCxt, &transitAreaId,
                                       &nbrId)) != NULL)
    {
        *pi4RetValOspfv3VirtNbrRestartHelperExitReason =
            (INT4) pNbr->u1NbrHelperExitReason;
        return SNMP_SUCCESS;
    }

    *pi4RetValOspfv3VirtNbrRestartHelperExitReason = 0;
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : Ospfv3AreaAggregateTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexOspfv3AreaAggregateTable
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexOspfv3AreaAggregateTable (UINT4 *pu4Ospfv3AreaAggregateAreaID,
                                          INT4
                                          *pi4Ospfv3AreaAggregateAreaLsdbType,
                                          INT4
                                          *pi4Ospfv3AreaAggregatePrefixType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pOspfv3AreaAggregatePrefix,
                                          UINT4
                                          *pi4Ospfv3AreaAggregatePrefixLength)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4Ospfv3AreaAggregatePrefixType = OSPFV3_INET_ADDR_TYPE;

    gV3OsRtr.pAggrTableCache = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if ((pAddrRange = (tV3OsAddrRange *)
             TMO_SLL_Last (&(pArea->type3AggrLst))) != NULL)
        {
            *pu4Ospfv3AreaAggregateAreaID = OSPFV3_BUFFER_DWFROMPDU
                (pArea->areaId);
            *pi4Ospfv3AreaAggregateAreaLsdbType = OSPFV3_INTER_AREA_PREFIX_LSA;

            OSPFV3_IP6_ADDR_COPY (*pOspfv3AreaAggregatePrefix->pu1_OctetList,
                                  pAddrRange->ip6Addr);
            pOspfv3AreaAggregatePrefix->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            *pi4Ospfv3AreaAggregatePrefixLength = pAddrRange->u1PrefixLength;
            gV3OsRtr.pAggrTableCache = pArea;
            return SNMP_SUCCESS;
        }
        else if ((pAddrRange =
                  (tV3OsAddrRange *) TMO_SLL_Last (&(pArea->type7AggrLst))) !=
                 NULL)
        {
            *pu4Ospfv3AreaAggregateAreaID = OSPFV3_BUFFER_DWFROMPDU
                (pArea->areaId);
            *pi4Ospfv3AreaAggregateAreaLsdbType = OSPFV3_NSSA_LSA;
            OSPFV3_IP6_ADDR_COPY (*pOspfv3AreaAggregatePrefix->pu1_OctetList,
                                  pAddrRange->ip6Addr);
            pOspfv3AreaAggregatePrefix->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            *pi4Ospfv3AreaAggregatePrefixLength = pAddrRange->u1PrefixLength;
            gV3OsRtr.pAggrTableCache = pArea;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexOspfv3AreaAggregateTable
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                nextOspfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                nextOspfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                nextOspfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                nextOspfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength
                nextOspfv3AreaAggregatePrefixLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexOspfv3AreaAggregateTable (UINT4 u4Ospfv3AreaAggregateAreaID,
                                         UINT4
                                         *pu4NextOspfv3AreaAggregateAreaID,
                                         INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                         INT4
                                         *pi4NextOspfv3AreaAggregateAreaLsdbType,
                                         INT4 i4Ospfv3AreaAggregatePrefixType,
                                         INT4
                                         *pi4NextOspfv3AreaAggregatePrefixType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pOspfv3AreaAggregatePrefix,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextOspfv3AreaAggregatePrefix,
                                         UINT4
                                         i4Ospfv3AreaAggregatePrefixLength,
                                         UINT4
                                         *pi4NextOspfv3AreaAggregatePrefixLength)
{
    tV3OsAreaId         currAreaId;
    tV3OsArea          *pArea = NULL;
    UINT1               u1Found = OSIX_FALSE;
    UINT1               u1IsGreater = OSIX_FALSE;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsAddrRange     *pAddrRangeFound = NULL;
    tV3OsAddrRange     *pPrevAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextOspfv3AreaAggregatePrefixType = OSPFV3_INET_ADDR_TYPE;

    OSPFV3_BUFFER_DWTOPDU (currAreaId, u4Ospfv3AreaAggregateAreaID);

    if (gV3OsRtr.pAggrTableCache != NULL)
    {

        switch (V3UtilAreaIdComp (currAreaId, gV3OsRtr.pAggrTableCache->areaId))
        {
            case OSPFV3_GREATER:
                /* try next entry */
                gV3OsRtr.pAggrTableCache = NULL;
                u1Found = OSIX_FALSE;
                break;

            case OSPFV3_EQUAL:
                /* first index is complete */
                if (i4Ospfv3AreaAggregateAreaLsdbType <=
                    OSPFV3_INTER_AREA_PREFIX_LSA)
                {
                    TMO_SLL_Scan (&(gV3OsRtr.pAggrTableCache->type3AggrLst),
                                  pAddrRange, tV3OsAddrRange *)
                    {
                        switch (V3UtilIp6AddrComp
                                ((tIp6Addr *) (VOID *)
                                 pOspfv3AreaAggregatePrefix->pu1_OctetList,
                                 &(pAddrRange->ip6Addr)))
                        {
                            case OSPFV3_LESS:
                                /* save previous node pointer */
                                pPrevAddrRange = pAddrRange;
                                /* try next entry */
                                continue;
                            case OSPFV3_EQUAL:
                                /* second index is complete */
                                if (i4Ospfv3AreaAggregatePrefixLength <
                                    pAddrRange->u1PrefixLength)
                                {
                                    /* save previous node pointer */
                                    pPrevAddrRange = pAddrRange;
                                    continue;
                                }
                                /* next found, fall through */
                            case OSPFV3_GREATER:
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_INTER_AREA_PREFIX_LSA;
                                if (pPrevAddrRange != NULL)
                                {
                                    pAddrRangeFound = pPrevAddrRange;
                                    u1Found = OSIX_TRUE;
                                }
                                else
                                {
                                    u1Found = OSIX_FALSE;
                                }
                                u1IsGreater = OSIX_TRUE;
                                /* next found, fall through */
                            default:
                                break;
                        }
                        if (u1IsGreater == OSIX_TRUE)
                        {
                            break;
                        }
                    }            /*TMO_SLL_Scan */
                    if (u1Found == OSIX_FALSE)
                    {
                        if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                             (&(gV3OsRtr.pAggrTableCache->type7AggrLst))) !=
                            NULL)
                        {
                            *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                OSPFV3_NSSA_LSA;
                            u1Found = OSIX_TRUE;
                        }
                    }
                }                /* end if for OSPFV3_INTER_AREA_PREFIX_LSA */

                if ((i4Ospfv3AreaAggregateAreaLsdbType <= OSPFV3_NSSA_LSA)
                    && (u1Found != OSIX_TRUE))
                {
                    TMO_SLL_Scan (&(gV3OsRtr.pAggrTableCache->type7AggrLst),
                                  pAddrRange, tV3OsAddrRange *)
                    {
                        switch (V3UtilIp6AddrComp
                                ((tIp6Addr *) (VOID *)
                                 (pOspfv3AreaAggregatePrefix->pu1_OctetList),
                                 &(pAddrRange->ip6Addr)))
                        {
                            case OSPFV3_LESS:
                                /* save previous node pointer */
                                pPrevAddrRange = pAddrRange;
                                /* try next entry */
                                continue;
                            case OSPFV3_EQUAL:
                                /* second index is complete */
                                if (i4Ospfv3AreaAggregatePrefixLength <
                                    pAddrRange->u1PrefixLength)
                                {
                                    /* save previous node pointer */
                                    pPrevAddrRange = pAddrRange;
                                    continue;
                                }
                                /* next found, fall through */
                            case OSPFV3_GREATER:
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_NSSA_LSA;
                                if (pPrevAddrRange != NULL)
                                {
                                    pAddrRangeFound = pPrevAddrRange;
                                    u1Found = OSIX_TRUE;
                                }
                                else
                                {
                                    u1Found = OSIX_FALSE;
                                }
                                u1IsGreater = OSIX_TRUE;
                                /* next found, fall through */
                            default:
                                break;
                        }
                        if (u1IsGreater == OSIX_TRUE)
                        {
                            break;
                        }
                    }
                }                /* end if for OSPFV3_NSSA_LSA */

                break;

            case OSPFV3_LESS:
                if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                     (&(gV3OsRtr.pAggrTableCache->type3AggrLst))) != NULL)
                {
                    *pi4NextOspfv3AreaAggregateAreaLsdbType =
                        OSPFV3_INTER_AREA_PREFIX_LSA;
                    u1Found = OSIX_TRUE;
                }
                else if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                          (&(gV3OsRtr.pAggrTableCache->type7AggrLst))) != NULL)
                {
                    *pi4NextOspfv3AreaAggregateAreaLsdbType = OSPFV3_NSSA_LSA;
                    u1Found = OSIX_TRUE;
                }

            default:
                break;
        }                        /* switch-case */

        if (u1Found == OSIX_TRUE)
        {
            *pu4NextOspfv3AreaAggregateAreaID = OSPFV3_BUFFER_DWFROMPDU
                (gV3OsRtr.pAggrTableCache->areaId);
            OSPFV3_IP6_ADDR_COPY
                (*pNextOspfv3AreaAggregatePrefix->pu1_OctetList,
                 pAddrRangeFound->ip6Addr);
            *pi4NextOspfv3AreaAggregatePrefixLength =
                pAddrRangeFound->u1PrefixLength;
            return SNMP_SUCCESS;
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        gV3OsRtr.pAggrTableCache = pArea;
        switch (V3UtilAreaIdComp (currAreaId, pArea->areaId))
        {
            case OSPFV3_GREATER:
                continue;

            case OSPFV3_EQUAL:
                /* first index is complete */
                if (i4Ospfv3AreaAggregateAreaLsdbType <=
                    OSPFV3_INTER_AREA_PREFIX_LSA)
                {
                    TMO_SLL_Scan (&(pArea->type3AggrLst), pAddrRange,
                                  tV3OsAddrRange *)
                    {
                        switch (V3UtilIp6AddrComp
                                ((tIp6Addr *) (VOID *)
                                 (pOspfv3AreaAggregatePrefix->pu1_OctetList),
                                 &(pAddrRange->ip6Addr)))
                        {
                            case OSPFV3_LESS:
                                /* save previous node pointer */
                                pPrevAddrRange = pAddrRange;
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_INTER_AREA_PREFIX_LSA;
                                u1Found = OSIX_TRUE;
                                /* try next entry */
                                continue;
                            case OSPFV3_EQUAL:
                                /* second index is complete */
                                if (i4Ospfv3AreaAggregatePrefixLength <
                                    pAddrRange->u1PrefixLength)
                                {
                                    /* save previous node pointer */
                                    pPrevAddrRange = pAddrRange;
                                    *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                        OSPFV3_INTER_AREA_PREFIX_LSA;
                                    u1Found = OSIX_TRUE;
                                    continue;
                                }
                                /* next found, fall through */
                            case OSPFV3_GREATER:
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_INTER_AREA_PREFIX_LSA;
                                if (pPrevAddrRange != NULL)
                                {
                                    pAddrRangeFound = pPrevAddrRange;
                                    u1Found = OSIX_TRUE;
                                }
                                else
                                {
                                    u1Found = OSIX_FALSE;
                                }
                                u1IsGreater = OSIX_TRUE;
                                /* next found, fall through */
                            default:
                                break;
                        }
                        if (u1IsGreater == OSIX_TRUE)
                        {
                            break;
                        }
                    }            /*TMO_SLL_Scan */

                    if (u1Found == OSIX_TRUE)
                    {
                        pAddrRangeFound = pPrevAddrRange;
                    }

                    if (u1Found == OSIX_FALSE)
                    {
                        if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                             (&(pArea->type7AggrLst))) != NULL)
                        {
                            *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                OSPFV3_NSSA_LSA;
                            u1Found = OSIX_TRUE;
                        }
                    }
                }                /* end if for OSPFV3_INTER_AREA_PREFIX_LSA */

                if ((i4Ospfv3AreaAggregateAreaLsdbType <= OSPFV3_NSSA_LSA)
                    && (u1Found != OSIX_TRUE))
                {
                    TMO_SLL_Scan (&(pArea->type7AggrLst), pAddrRange,
                                  tV3OsAddrRange *)
                    {
                        switch (V3UtilIp6AddrComp
                                ((tIp6Addr *) (VOID *)
                                 pOspfv3AreaAggregatePrefix->pu1_OctetList,
                                 &(pAddrRange->ip6Addr)))
                        {
                            case OSPFV3_LESS:
                                /* save previous node pointer */
                                pPrevAddrRange = pAddrRange;
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_NSSA_LSA;
                                u1Found = OSIX_TRUE;
                                /* try next entry */
                                continue;
                            case OSPFV3_EQUAL:
                                /* second index is complete */
                                if (i4Ospfv3AreaAggregatePrefixLength <
                                    pAddrRange->u1PrefixLength)
                                {
                                    /* save previous node pointer */
                                    pPrevAddrRange = pAddrRange;
                                    *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                        OSPFV3_NSSA_LSA;
                                    u1Found = OSIX_TRUE;
                                    continue;
                                }
                                /* next found, fall through */
                            case OSPFV3_GREATER:
                                *pi4NextOspfv3AreaAggregateAreaLsdbType =
                                    OSPFV3_NSSA_LSA;
                                if (pPrevAddrRange != NULL)
                                {
                                    pAddrRangeFound = pPrevAddrRange;
                                    u1Found = OSIX_TRUE;
                                }
                                else
                                {
                                    u1Found = OSIX_FALSE;
                                }
                                u1IsGreater = OSIX_TRUE;
                                /* next found, fall through */
                            default:
                                break;
                        }
                        if (u1IsGreater == OSIX_TRUE)
                        {
                            break;
                        }
                    }
                }                /* end if for OSPFV3_NSSA_LSA */

                if (u1Found != OSIX_TRUE)
                {
                    continue;
                }
                /* next found, fall through */
                /* next found, fall through */

            case OSPFV3_LESS:
                if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                     (&(gV3OsRtr.pAggrTableCache->type3AggrLst))) != NULL)
                {
                    *pi4NextOspfv3AreaAggregateAreaLsdbType =
                        OSPFV3_INTER_AREA_PREFIX_LSA;
                    u1Found = OSIX_TRUE;
                }
                else if ((pAddrRangeFound = (tV3OsAddrRange *) TMO_SLL_Last
                          (&(gV3OsRtr.pAggrTableCache->type7AggrLst))) != NULL)
                {
                    *pi4NextOspfv3AreaAggregateAreaLsdbType = OSPFV3_NSSA_LSA;
                    u1Found = OSIX_TRUE;
                }
                break;

            default:
                break;
        }                        /* switch-case */

        if (u1Found == OSIX_TRUE && pAddrRangeFound != NULL)
        {
            *pu4NextOspfv3AreaAggregateAreaID = OSPFV3_BUFFER_DWFROMPDU
                (pArea->areaId);
            OSPFV3_IP6_ADDR_COPY
                (*pNextOspfv3AreaAggregatePrefix->pu1_OctetList,
                 pAddrRangeFound->ip6Addr);
            *pi4NextOspfv3AreaAggregatePrefixLength =
                pAddrRangeFound->u1PrefixLength;
            return SNMP_SUCCESS;
        }

    }                            /* TMO_SLL_Scan */
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  V3GetFindAreaAddrRange
 Input       :  The Indices
                pArea - Pointer to area structure
        pIp6Addr - Address range prefix
        u1PfxLen - Prefix length
        u1LsdbType - Lsa type
 
 Output      :  None
                
 Returns     : pointer to the tV3OsAddrRange if successfull
               NULL  otherwise  
****************************************************************************/
PUBLIC tV3OsAddrRange *
V3GetFindAreaAddrRange (tV3OsArea * pArea, tIp6Addr * pIp6Addr, UINT1 u1PfxLen,
                        UINT2 u2LsdbType)
{
    tV3OsAddrRange     *pAddrRange = NULL;

    if (u2LsdbType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        TMO_SLL_Scan (&(pArea->type3AggrLst), pAddrRange, tV3OsAddrRange *)
        {
            /* if entry exists return success */
            if ((pAddrRange->u1PrefixLength == u1PfxLen) &&
                (V3UtilIp6PrefixComp
                 (pIp6Addr, &(pAddrRange->ip6Addr), u1PfxLen)) == OSPFV3_EQUAL)
            {
                return pAddrRange;
            }
        }
    }
    else if (u2LsdbType == OSPFV3_NSSA_LSA)
    {
        TMO_SLL_Scan (&(pArea->type7AggrLst), pAddrRange, tV3OsAddrRange *)
        {
            /* if entry exists return success */
            if ((pAddrRange->u1PrefixLength == u1PfxLen) &&
                (V3UtilIp6PrefixComp
                 (pIp6Addr, &(pAddrRange->ip6Addr), u1PfxLen)) == OSPFV3_EQUAL)
            {
                return pAddrRange;
            }
        }
    }
    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetOspfv3AreaAggregateStatus
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                retValOspfv3AreaAggregateStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaAggregateStatus (UINT4 u4Ospfv3AreaAggregateAreaID,
                                 INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                 INT4 i4Ospfv3AreaAggregatePrefixType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pOspfv3AreaAggregatePrefix,
                                 UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                 INT4 *pi4RetValOspfv3AreaAggregateStatus)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pAddrRange = V3GetFindAreaAddrRange
             (pArea, (tIp6Addr *) (VOID *) pOspfv3AreaAggregatePrefix->
              pu1_OctetList,
              (UINT1) i4Ospfv3AreaAggregatePrefixLength,
              (UINT2) i4Ospfv3AreaAggregateAreaLsdbType)) != NULL)
        {
            *pi4RetValOspfv3AreaAggregateStatus = pAddrRange->areaAggStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaAggregateEffect
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                retValOspfv3AreaAggregateEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaAggregateEffect (UINT4 u4Ospfv3AreaAggregateAreaID,
                                 INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                 INT4 i4Ospfv3AreaAggregatePrefixType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pOspfv3AreaAggregatePrefix,
                                 UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                 INT4 *pi4RetValOspfv3AreaAggregateEffect)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pAddrRange = V3GetFindAreaAddrRange
             (pArea, (tIp6Addr *) (VOID *) pOspfv3AreaAggregatePrefix->
              pu1_OctetList,
              (UINT1) i4Ospfv3AreaAggregatePrefixLength,
              (UINT2) i4Ospfv3AreaAggregateAreaLsdbType)) != NULL)
        {
            *pi4RetValOspfv3AreaAggregateEffect =
                (INT4) pAddrRange->u1AggrEffect;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaAggregateRouteTag
 Input       :  The Indices
                Ospfv3AreaAggregateAreaID
                Ospfv3AreaAggregateAreaLsdbType
                Ospfv3AreaAggregatePrefixType
                Ospfv3AreaAggregatePrefix
                Ospfv3AreaAggregatePrefixLength

                The Object 
                retValOspfv3AreaAggregateRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaAggregateRouteTag (UINT4 u4Ospfv3AreaAggregateAreaID,
                                   INT4 i4Ospfv3AreaAggregateAreaLsdbType,
                                   INT4 i4Ospfv3AreaAggregatePrefixType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pOspfv3AreaAggregatePrefix,
                                   UINT4 i4Ospfv3AreaAggregatePrefixLength,
                                   INT4 *pi4RetValOspfv3AreaAggregateRouteTag)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4Ospfv3AreaAggregatePrefixType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaAggregateAreaID);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        if ((pAddrRange = V3GetFindAreaAddrRange
             (pArea, (tIp6Addr *) (VOID *) pOspfv3AreaAggregatePrefix->
              pu1_OctetList,
              (UINT1) i4Ospfv3AreaAggregatePrefixLength,
              (UINT2) i4Ospfv3AreaAggregateAreaLsdbType)) != NULL)
        {
            *pi4RetValOspfv3AreaAggregateRouteTag =
                (INT4) pAddrRange->u4AddrRangeRtTag;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3OverFlowState
 Input       :  The Indices

                The Object 
                retValFutOspfv3OverFlowState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3OverFlowState (INT4 *pi4RetValFutOspfv3OverFlowState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3OverFlowState =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bOverflowState);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3TraceLevel
 Input       :  The Indices

                The Object 
                retValFutOspfv3TraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3TraceLevel (INT4 *pi4RetValFutOspfv3TraceLevel)
{
#ifdef TRACE_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3TraceLevel = (INT4) pV3OspfCxt->u4TraceValue;
#else
    /* If Trace wanted switch is not defined then Trace Value
     * will be returned as 0, That means no trace level.*/
    *pi4RetValFutOspfv3TraceLevel = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3ABRType
 Input       :  The Indices

                The Object 
                retValFutOspfv3ABRType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ABRType (INT4 *pi4RetValFutOspfv3ABRType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3ABRType = (INT4) pV3OspfCxt->u1ABRType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3NssaAsbrDefRtTrans
 Input       :  The Indices

                The Object 
                retValFutOspfv3NssaAsbrDefRtTrans
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3NssaAsbrDefRtTrans (INT4 *pi4RetValFutOspfv3NssaAsbrDefRtTrans)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3NssaAsbrDefRtTrans =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bNssaAsbrDefRtTrans);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3DefaultPassiveInterface
 Input       :  The Indices

                The Object 
                retValFutOspfv3DefaultPassiveInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3DefaultPassiveInterface (INT4
                                        *pi4RetValFutOspfv3DefaultPassiveInterface)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3DefaultPassiveInterface =
        OSPFV3_MAP_TO_TRUTH_VALUE (pV3OspfCxt->bDefaultPassiveInterface);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3SpfDelay
 Input       :  The Indices

                The Object 
                retValFutOspfv3SpfDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3SpfDelay (INT4 *pi4RetValFutOspfv3SpfDelay)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3SpfDelay = pV3OspfCxt->u4SpfInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3SpfHoldTime
 Input       :  The Indices

                The Object 
                retValFutOspfv3SpfHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3SpfHoldTime (INT4 *pi4RetValFutOspfv3SpfHoldTime)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3SpfHoldTime = (INT4) pV3OspfCxt->u4SpfHoldTimeInterval;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RestartStrictLsaChecking
 Input       :  The Indices

                The Object
                retValFutOspfv3RestartStrictLsaChecking
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RestartStrictLsaChecking (INT4
                                         *pi4RetValFutOspfv3RestartStrictLsaChecking)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3RestartStrictLsaChecking =
        (INT4) pV3OspfCxt->u1StrictLsaCheck;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3HelperSupport
 Input       :  The Indices

                The Object
                retValFutOspfv3HelperSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3HelperSupport (tSNMP_OCTET_STRING_TYPE *
                              pRetValFutOspfv3HelperSupport)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pRetValFutOspfv3HelperSupport->pu1_OctetList,
            &pV3OspfCxt->u1HelperSupport, 1);
    pRetValFutOspfv3HelperSupport->i4_Length = sizeof (UINT1);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3HelperGraceTimeLimit
 Input       :  The Indices

                The Object
                retValFutOspfv3HelperGraceTimeLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3HelperGraceTimeLimit (INT4
                                     *pi4RetValFutOspfv3HelperGraceTimeLimit)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3HelperGraceTimeLimit =
        (INT4) pV3OspfCxt->u4HelperGrTimeLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RestartAckState
 Input       :  The Indices

                The Object
                retValFutOspfv3RestartAckState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RestartAckState (INT4 *pi4RetValFutOspfv3RestartAckState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3RestartAckState = (INT4) pV3OspfCxt->u4IsGraceAckReq;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3GraceLsaRetransmitCount
 Input       :  The Indices

                The Object
                retValFutOspfv3GraceLsaRetransmitCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3GraceLsaRetransmitCount (INT4
                                        *pi4RetValFutOspfv3GraceLsaRetransmitCount)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3GraceLsaRetransmitCount =
        (INT4) pV3OspfCxt->u1GrLsaMaxTxCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RestartReason
 Input       :  The Indices

                The Object
                retValFutOspfv3RestartReason
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RestartReason (INT4 *pi4RetValFutOspfv3RestartReason)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3RestartReason = (INT4) pV3OspfCxt->u1RestartReason;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3ExtTraceLevel
 Input       :  The Indices

                The Object
                retValFutOspfv3ExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ExtTraceLevel (INT4 *pi4RetValFutOspfv3ExtTraceLevel)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
#ifdef TRACE_WANTED
    *pi4RetValFutOspfv3ExtTraceLevel = (INT4) pV3OspfCxt->u4ExtTraceValue;
#else
    /* If Trace wanted switch is not defined then Trace Value
     * will be returned as 0, That means no trace level.*/
    *pi4RetValFutOspfv3ExtTraceLevel = 0;
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3SetTraps
 Input       :  The Indices

                The Object
                retValFutOspfv3SetTraps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3SetTraps (INT4 *pi4RetValFutOspfv3SetTraps)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3SetTraps = (INT4) pV3OspfCxt->u4TrapControl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3IfTable
 Input       :  The Indices
                FutOspfv3IfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3IfTable (INT4 *pi4FutOspfv3IfIndex)
{
    return (nmhGetFirstIndexOspfv3IfTable (pi4FutOspfv3IfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3IfTable
 Input       :  The Indices
                FutOspfv3IfIndex
                nextFutOspfv3IfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3IfTable (INT4 i4FutOspfv3IfIndex,
                                 INT4 *pi4NextFutOspfv3IfIndex)
{
    return (nmhGetNextIndexOspfv3IfTable (i4FutOspfv3IfIndex,
                                          pi4NextFutOspfv3IfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfOperState
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfOperState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfOperState (INT4 i4FutOspfv3IfIndex,
                            INT4 *pi4RetValFutOspfv3IfOperState)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pi4RetValFutOspfv3IfOperState = (INT4) pInterface->operStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfPassive
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfPassive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfPassive (INT4 i4FutOspfv3IfIndex,
                          INT4 *pi4RetValFutOspfv3IfPassive)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pi4RetValFutOspfv3IfPassive =
            OSPFV3_MAP_TO_TRUTH_VALUE (pInterface->bPassive);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfNbrCount
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfNbrCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfNbrCount (INT4 i4FutOspfv3IfIndex,
                           UINT4 *pu4RetValFutOspfv3IfNbrCount)
{
    tV3OsInterface     *pInterface = NULL;

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfNbrCount = TMO_SLL_Count (&(pInterface->nbrsInIf));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAdjCount
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfAdjCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAdjCount (INT4 i4FutOspfv3IfIndex,
                           UINT4 *pu4RetValFutOspfv3IfAdjCount)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT2              *pCount = NULL;

    pCount = (UINT2 *) pu4RetValFutOspfv3IfAdjCount;

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pCount = 0;
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);
            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                (*pCount)++;
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfHelloRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfHelloRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfHelloRcvd (INT4 i4FutOspfv3IfIndex,
                            UINT4 *pu4RetValFutOspfv3IfHelloRcvd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfHelloRcvd = pInterface->u4HelloRcvdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfHelloRcvd = pInterface->u4HelloRcvdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfHelloTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfHelloTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfHelloTxed (INT4 i4FutOspfv3IfIndex,
                            UINT4 *pu4RetValFutOspfv3IfHelloTxed)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfHelloTxed = pInterface->u4HelloTxedCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfHelloTxed = pInterface->u4HelloTxedCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfHelloDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfHelloDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfHelloDisd (INT4 i4FutOspfv3IfIndex,
                            UINT4 *pu4RetValFutOspfv3IfHelloDisd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfHelloDisd = pInterface->u4HelloDisdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfHelloDisd = pInterface->u4HelloDisdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfDdpRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfDdpRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfDdpRcvd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfDdpRcvd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfDdpRcvd = pInterface->u4DdpRcvdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfDdpRcvd = pInterface->u4DdpRcvdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfDdpTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfDdpTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfDdpTxed (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfDdpTxed)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfDdpTxed = pInterface->u4DdpTxedCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfDdpTxed = pInterface->u4DdpTxedCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfDdpDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfDdpDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfDdpDisd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfDdpDisd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfDdpDisd = pInterface->u4DdpDisdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfDdpDisd = pInterface->u4DdpDisdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLrqRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLrqRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLrqRcvd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLrqRcvd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLrqRcvd = pInterface->u4LsaReqRcvdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLrqRcvd = pInterface->u4LsaReqRcvdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLrqTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLrqTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLrqTxed (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLrqTxed)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLrqTxed = pInterface->u4LsaReqTxedCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLrqTxed = pInterface->u4LsaReqTxedCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLrqDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLrqDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLrqDisd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLrqDisd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLrqDisd = pInterface->u4LsaReqDisdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLrqDisd = pInterface->u4LsaReqDisdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLsuRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLsuRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLsuRcvd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLsuRcvd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLsuRcvd = pInterface->u4LsaUpdateRcvdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLsuRcvd = pInterface->u4LsaUpdateRcvdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLsuTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLsuTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLsuTxed (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLsuTxed)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLsuTxed = pInterface->u4LsaUpdateTxedCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLsuTxed = pInterface->u4LsaUpdateTxedCount;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLsuDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLsuDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLsuDisd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLsuDisd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLsuDisd = pInterface->u4LsaUpdateDisdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLsuDisd = pInterface->u4LsaUpdateDisdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLakRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLakRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLakRcvd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLakRcvd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLakRcvd = pInterface->u4LsaAckRcvdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLakRcvd = pInterface->u4LsaAckRcvdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLakTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLakTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLakTxed (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLakTxed)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLakTxed = pInterface->u4LsaAckTxedCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLakTxed = pInterface->u4LsaAckTxedCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLakDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLakDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLakDisd (INT4 i4FutOspfv3IfIndex,
                          UINT4 *pu4RetValFutOspfv3IfLakDisd)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex)) != NULL)
    {
        *pu4RetValFutOspfv3IfLakDisd = pInterface->u4LsaAckDisdCount;
        return SNMP_SUCCESS;
    }
    else
    {
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            if (pInterface->u4InterfaceId == (UINT4) i4FutOspfv3IfIndex)
            {
                *pu4RetValFutOspfv3IfLakDisd = pInterface->u4LsaAckDisdCount;
                return SNMP_SUCCESS;
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfLinkLSASuppression
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfLinkLSASuppression
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfLinkLSASuppression (INT4 i4FutOspfv3IfIndex,
                                     INT4
                                     *pi4RetValFutOspfv3IfLinkLSASuppression)
{
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pi4RetValFutOspfv3IfLinkLSASuppression =
            OSPFV3_MAP_TO_TRUTH_VALUE (pInterface->bLinkLsaSuppress);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfBfdState
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object
                retValFutOspfv3IfBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfBfdState (INT4 i4FutOspfv3IfIndex,
                           INT4 *pi4RetValFutOspfv3IfBfdState)
{
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pi4RetValFutOspfv3IfBfdState = pInterface->u1BfdIfStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfCryptoAuthType
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfCryptoAuthType (INT4 i4FutOspfv3IfIndex,
                                 INT4 *pi4RetValFutOspfv3IfCryptoAuthType)
{
    INT1                i1Return = SNMP_FAILURE;
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pi4RetValFutOspfv3IfCryptoAuthType =
            (INT4) pInterface->u2CryptoAuthType;
        return SNMP_SUCCESS;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfCryptoAuthMode
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object 
                retValFutOspfv3IfCryptoAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfCryptoAuthMode (INT4 i4FutOspfv3IfIndex,
                                 INT4 *pi4RetValFutOspfv3IfCryptoAuthMode)
{
    INT1                i1Return = SNMP_FAILURE;
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pi4RetValFutOspfv3IfCryptoAuthMode = (INT4) pInterface->u1AuthMode;
        return SNMP_SUCCESS;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthTxed
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object
                retValFutOspfv3IfAuthTxed
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthTxed (INT4 i4FutOspfv3IfIndex,
                           UINT4 *pu4RetValFutOspfv3IfAuthTxed)
{
    INT1                i1Return = SNMP_FAILURE;
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pu4RetValFutOspfv3IfAuthTxed =
            (UINT4) pInterface->u4AuthUpdateTxedCount;
        return SNMP_SUCCESS;
    }

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthRcvd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object
                retValFutOspfv3IfAuthRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthRcvd (INT4 i4FutOspfv3IfIndex,
                           UINT4 *pu4RetValFutOspfv3IfAuthRcvd)
{
    INT1                i1Return = SNMP_FAILURE;
    tV3OsInterface     *pInterface;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pu4RetValFutOspfv3IfAuthRcvd =
            (UINT4) pInterface->u4AuthUpdateRcvdCount;
        return SNMP_SUCCESS;
    }
    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthDisd
 Input       :  The Indices
                FutOspfv3IfIndex

                The Object
                retValFutOspfv3IfAuthDisd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthDisd (INT4 i4FutOspfv3IfIndex,
                           UINT4 *pu4RetValFutOspfv3IfAuthDisd)
{
    INT1                i1Return = SNMP_FAILURE;
    tV3OsInterface     *pInterface = NULL;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfIndex);
    if (pInterface != NULL)
    {
        *pu4RetValFutOspfv3IfAuthDisd =
            pInterface->u4NotFoundSACnt + pInterface->u4DigestFailCnt +
            pInterface->u4CrytoSeqMismtchCnt + pInterface->u4ATHdrLengthErrCnt +
            pInterface->u4ATAlgoMismtchCnt;
        return SNMP_SUCCESS;
    }
    return i1Return;
}

/* LOW LEVEL Routines for Table : FutOspfv3IfAuthTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3IfAuthTable
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfv3IfAuthTable (INT4 *pi4FutOspfv3IfAuthIfIndex,
                                      INT4 *pi4FutOspfv3IfAuthKeyId)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    while (pInterface != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            *pi4FutOspfv3IfAuthIfIndex = (INT4) pInterface->u4InterfaceId;
            *pi4FutOspfv3IfAuthKeyId = (INT4) pAuthkeyInfo->u2AuthkeyId;
            return SNMP_SUCCESS;
        }
        pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                       (tRBElem *) pInterface,
                                                       NULL);

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3IfAuthTable
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                nextFutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId
                nextFutOspfv3IfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3IfAuthTable (INT4 i4FutOspfv3IfAuthIfIndex,
                                     INT4 *pi4NextFutOspfv3IfAuthIfIndex,
                                     INT4 i4FutOspfv3IfAuthKeyId,
                                     INT4 *pi4NextFutOspfv3IfAuthKeyId)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT2               u2AuthkeyId = 0;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    u2AuthkeyId = (UINT2) i4FutOspfv3IfAuthKeyId;

    pInterface = V3GetFindIf ((UINT4) i4FutOspfv3IfAuthIfIndex);

    if (pInterface == NULL)
    {
        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    }

    if (pInterface != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            if ((i4FutOspfv3IfAuthKeyId != -1) &&
                (pAuthkeyInfo->u2AuthkeyId <= u2AuthkeyId))
            {
                continue;
            }
            else
            {
                *pi4NextFutOspfv3IfAuthKeyId = (INT4) pAuthkeyInfo->u2AuthkeyId;
                *pi4NextFutOspfv3IfAuthIfIndex =
                    (INT4) pInterface->u4InterfaceId;
                return SNMP_SUCCESS;
            }
        }
    }

    pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                   (tRBElem *) pInterface,
                                                   NULL);
    if (pInterface != NULL)
    {
        if ((pLstNode = TMO_SLL_First (&(pInterface->sortAuthkeyLst))) != NULL)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            *pi4NextFutOspfv3IfAuthKeyId = (INT4) pAuthkeyInfo->u2AuthkeyId;
            *pi4NextFutOspfv3IfAuthIfIndex = (INT4) pInterface->u4InterfaceId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKey
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKey (INT4 i4FutOspfv3IfAuthIfIndex,
                          INT4 i4FutOspfv3IfAuthKeyId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFutOspfv3IfAuthKey)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pAuthkeyInfo = V3GetFindIfAuthkeyInfoInCxt ((UINT4)
                                                     i4FutOspfv3IfAuthIfIndex,
                                                     (UINT2)
                                                     i4FutOspfv3IfAuthKeyId)) !=
        NULL)
    {
        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            V3AuthKeyCopy (pRetValFutOspfv3IfAuthKey->pu1_OctetList,
                           pAuthkeyInfo->au1AuthKey,
                           ((INT4) STRLEN ((CHR1 *) pAuthkeyInfo->au1AuthKey)));
            pRetValFutOspfv3IfAuthKey->i4_Length =
                (INT4) STRLEN ((CHR1 *) pAuthkeyInfo->au1AuthKey);
        }
        else
        {
            pRetValFutOspfv3IfAuthKey->i4_Length = OSPFV3_ZERO;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKeyStartAccept (INT4 i4FutOspfv3IfAuthIfIndex,
                                     INT4 i4FutOspfv3IfAuthKeyId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pRetValFutOspfv3IfAuthKeyStartAccept)
{
    tUtlTm              tm;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];
    INT4                i4KeyLen = 0;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStartAccept, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY ((UINT1 *) pRetValFutOspfv3IfAuthKeyStartAccept->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3IfAuthKeyStartAccept->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKeyStartGenerate (INT4 i4FutOspfv3IfAuthIfIndex,
                                       INT4 i4FutOspfv3IfAuthKeyId,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pRetValFutOspfv3IfAuthKeyStartGenerate)
{
    tUtlTm              tm;
    INT4                i4KeyLen = 0;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStartGenerate, &tm) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY ((UINT1 *) pRetValFutOspfv3IfAuthKeyStartGenerate->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3IfAuthKeyStartGenerate->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKeyStopGenerate (INT4 i4FutOspfv3IfAuthIfIndex,
                                      INT4 i4FutOspfv3IfAuthKeyId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFutOspfv3IfAuthKeyStopGenerate)
{
    tUtlTm              tm;
    INT4                i4KeyLen = 0;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStopGenerate, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY ((UINT1 *) pRetValFutOspfv3IfAuthKeyStopGenerate->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3IfAuthKeyStopGenerate->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKeyStopAccept (INT4 i4FutOspfv3IfAuthIfIndex,
                                    INT4 i4FutOspfv3IfAuthKeyId,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pRetValFutOspfv3IfAuthKeyStopAccept)
{
    tUtlTm              tm;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    INT4                i4KeyLen = 0;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStopAccept, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY ((UINT1 *) pRetValFutOspfv3IfAuthKeyStopAccept->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3IfAuthKeyStopAccept->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFutOspfv3IfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3IfAuthIfIndex
                FutOspfv3IfAuthKeyId

                The Object 
                retValFutOspfv3IfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3IfAuthKeyStatus (INT4 i4FutOspfv3IfAuthIfIndex,
                                INT4 i4FutOspfv3IfAuthKeyId,
                                INT4 *pi4RetValFutOspfv3IfAuthKeyStatus)
{
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pAuthkeyInfo =
        V3GetFindIfAuthkeyInfoInCxt ((UINT4) i4FutOspfv3IfAuthIfIndex,
                                     (UINT2) i4FutOspfv3IfAuthKeyId);
    if (pAuthkeyInfo != NULL)
    {
        *pi4RetValFutOspfv3IfAuthKeyStatus = pAuthkeyInfo->u1AuthkeyStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FutOspfv3VirtIfAuthTable. */
/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3VirtIfAuthTable
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3VirtIfAuthTable (UINT4 *pu4FutOspfv3VirtIfAuthAreaId,
                                          UINT4 *pu4FutOspfv3VirtIfAuthNeighbor,
                                          INT4 *pi4FutOspfv3VirtIfAuthKeyId)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL, *p_lst_node = NULL;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = V3_GET_IF_PTR_FROM_SORT_LST (pLstNode);
        if ((p_lst_node =
             TMO_SLL_First ((tTMO_SLL *) & (pInterface->sortAuthkeyLst)))
            == NULL)
            continue;
        OSPFV3_RTR_ID_COPY (transitAreaId, pInterface->transitAreaId);
        OSPFV3_RTR_ID_COPY (nbrId, pInterface->destRtrId);
        *pu4FutOspfv3VirtIfAuthAreaId = OSPFV3_BUFFER_DWFROMPDU (transitAreaId);
        *pu4FutOspfv3VirtIfAuthNeighbor = OSPFV3_BUFFER_DWFROMPDU (nbrId);

        pAuthkeyInfo = (tAuthkeyInfo *) p_lst_node;
        *pi4FutOspfv3VirtIfAuthKeyId = (INT4) pAuthkeyInfo->u2AuthkeyId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3VirtIfAuthTable
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                nextFutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                nextFutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId
                nextFutOspfv3VirtIfAuthKeyId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3VirtIfAuthTable (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                         UINT4
                                         *pu4NextFutOspfv3VirtIfAuthAreaId,
                                         UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                         UINT4
                                         *pu4NextFutOspfv3VirtIfAuthNeighbor,
                                         INT4 i4FutOspfv3VirtIfAuthKeyId,
                                         INT4 *pi4NextFutOspfv3VirtIfAuthKeyId)
{
    UINT2               u2AuthkeyId;
    tV3OsAreaId         currAreaId;
    tV3OsRouterId       currNbrId;
    tV3OsAreaId         nextAreaId;
    tV3OsRouterId       nextNbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL, *p_lst_node = NULL;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (currNbrId, u4FutOspfv3VirtIfAuthNeighbor);
    u2AuthkeyId = (UINT2) i4FutOspfv3VirtIfAuthKeyId;

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = V3_GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (V3UtilVirtIfIndComp (pInterface->transitAreaId,
                                     pInterface->destRtrId,
                                     currAreaId, currNbrId))
        {
            case OSPFV3_LESS:
            {
                continue;
            }
            case OSPFV3_GREATER:
            {
                if ((p_lst_node = TMO_SLL_First
                     (&(pInterface->sortAuthkeyLst))) == NULL)
                {
                    continue;
                }
                pAuthkeyInfo = (tAuthkeyInfo *) p_lst_node;
                *pi4NextFutOspfv3VirtIfAuthKeyId =
                    (INT4) pAuthkeyInfo->u2AuthkeyId;
                OSPFV3_RTR_ID_COPY (nextAreaId, pInterface->transitAreaId);
                OSPFV3_RTR_ID_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextFutOspfv3VirtIfAuthAreaId =
                    OSPFV3_BUFFER_DWFROMPDU (nextAreaId);
                *pu4NextFutOspfv3VirtIfAuthNeighbor =
                    OSPFV3_BUFFER_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;
            }
            case OSPFV3_EQUAL:
            {
                TMO_SLL_Scan (&(pInterface->sortAuthkeyLst),
                              p_lst_node, tTMO_SLL_NODE *)
                {
                    pAuthkeyInfo = (tAuthkeyInfo *) p_lst_node;
                    if ((i4FutOspfv3VirtIfAuthKeyId != -1) &&
                        (pAuthkeyInfo->u2AuthkeyId <= u2AuthkeyId))
                    {
                        continue;
                    }
                    /*else if (pAuthkeyInfo->u2AuthkeyId > u2AuthkeyId) */
                    else
                    {
                        *pi4NextFutOspfv3VirtIfAuthKeyId =
                            (INT4) pAuthkeyInfo->u2AuthkeyId;
                        OSPFV3_RTR_ID_COPY (nextAreaId,
                                            pInterface->transitAreaId);
                        OSPFV3_RTR_ID_COPY (nextNbrId, pInterface->destRtrId);
                        *pu4NextFutOspfv3VirtIfAuthAreaId =
                            OSPFV3_BUFFER_DWFROMPDU (nextAreaId);
                        *pu4NextFutOspfv3VirtIfAuthNeighbor =
                            OSPFV3_BUFFER_DWFROMPDU (nextNbrId);
                        return (SNMP_SUCCESS);
                    }
                }
            }
            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKey
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKey (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                              UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                              INT4 i4FutOspfv3VirtIfAuthKeyId,
                              tSNMP_OCTET_STRING_TYPE
                              * pRetValFutOspfv3VirtIfAuthKey)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    if ((pAuthkeyInfo =
         V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId, &(nbrId),
                                          (UINT2) i4FutOspfv3VirtIfAuthKeyId))
        != NULL)
    {
        if (MsrGetSaveStatus () == ISS_TRUE)
        {
            V3AuthKeyCopy (pRetValFutOspfv3VirtIfAuthKey->pu1_OctetList,
                           pAuthkeyInfo->au1AuthKey,
                           (INT4) STRLEN ((CHR1 *) pAuthkeyInfo->au1AuthKey));
            pRetValFutOspfv3VirtIfAuthKey->i4_Length =
                (INT4) STRLEN ((CHR1 *) pAuthkeyInfo->au1AuthKey);
        }
        else
        {
            pRetValFutOspfv3VirtIfAuthKey->i4_Length = 0;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKeyStartAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKeyStartAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKeyStartAccept (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                         UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                         INT4 i4FutOspfv3VirtIfAuthKeyId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pRetValFutOspfv3VirtIfAuthKeyStartAccept)
{
    tV3OsAreaId         transitAreaId;
    INT4                i4KeyLen = 0;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    tUtlTm              tm;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    if ((pAuthkeyInfo =
         V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                          &transitAreaId, &(nbrId),
                                          (UINT2) i4FutOspfv3VirtIfAuthKeyId))
        == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStartAccept, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY (pRetValFutOspfv3VirtIfAuthKeyStartAccept->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3VirtIfAuthKeyStartAccept->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKeyStartGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKeyStartGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKeyStartGenerate (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                           UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                           INT4 i4FutOspfv3VirtIfAuthKeyId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValFutOspfv3VirtIfAuthKeyStartGenerate)
{
    tV3OsAreaId         transitAreaId;
    INT4                i4KeyLen = 0;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    if ((pAuthkeyInfo =
         V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                          &transitAreaId, &(nbrId),
                                          (UINT2) i4FutOspfv3VirtIfAuthKeyId))
        == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStartGenerate, &tm) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY (pRetValFutOspfv3VirtIfAuthKeyStartGenerate->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3VirtIfAuthKeyStartGenerate->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKeyStopGenerate
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKeyStopGenerate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKeyStopGenerate (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                          UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                          INT4 i4FutOspfv3VirtIfAuthKeyId,
                                          tSNMP_OCTET_STRING_TYPE
                                          *
                                          pRetValFutOspfv3VirtIfAuthKeyStopGenerate)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    INT4                i4KeyLen = 0;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    if ((pAuthkeyInfo =
         V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                          &transitAreaId, &(nbrId),
                                          (UINT2) i4FutOspfv3VirtIfAuthKeyId))
        == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStopGenerate, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY (pRetValFutOspfv3VirtIfAuthKeyStopGenerate->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3VirtIfAuthKeyStopGenerate->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKeyStopAccept
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKeyStopAccept
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKeyStopAccept (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                        UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                        INT4 i4FutOspfv3VirtIfAuthKeyId,
                                        tSNMP_OCTET_STRING_TYPE
                                        *
                                        pRetValFutOspfv3VirtIfAuthKeyStopAccept)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    INT4                i4KeyLen = 0;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1KeyConstantTime[OSPFV3_DST_TIME_LEN + 1];
    tUtlTm              tm;

    MEMSET (&tm, OSPFV3_ZERO, sizeof (tUtlTm));
    MEMSET (au1KeyConstantTime, OSPFV3_ZERO, OSPFV3_DST_TIME_LEN + 1);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    if ((pAuthkeyInfo =
         V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt,
                                          &transitAreaId, &(nbrId),
                                          (UINT2) i4FutOspfv3VirtIfAuthKeyId))
        == NULL)
    {
        return SNMP_FAILURE;
    }

    if (V3ConvertKeyTime (pAuthkeyInfo->u4KeyStopAccept, &tm) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    SPRINTF ((CHR1 *) au1KeyConstantTime,
             "%4u-%.2u-%.2u,%.2u:%.2u:%.2u",
             tm.tm_year, tm.tm_mon + 1, tm.tm_mday - 1, tm.tm_hour, tm.tm_min,
             tm.tm_sec);

    i4KeyLen = (INT4) STRLEN (au1KeyConstantTime);
    MEMCPY (pRetValFutOspfv3VirtIfAuthKeyStopAccept->pu1_OctetList,
            au1KeyConstantTime, i4KeyLen);
    pRetValFutOspfv3VirtIfAuthKeyStopAccept->i4_Length = i4KeyLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfAuthKeyStatus
 Input       :  The Indices
                FutOspfv3VirtIfAuthAreaId
                FutOspfv3VirtIfAuthNeighbor
                FutOspfv3VirtIfAuthKeyId

                The Object 
                retValFutOspfv3VirtIfAuthKeyStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfAuthKeyStatus (UINT4 u4FutOspfv3VirtIfAuthAreaId,
                                    UINT4 u4FutOspfv3VirtIfAuthNeighbor,
                                    INT4 i4FutOspfv3VirtIfAuthKeyId,
                                    INT4 *pi4RetValFutOspfv3VirtIfAuthKeyStatus)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tAuthkeyInfo       *pAuthkeyInfo = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4FutOspfv3VirtIfAuthAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4FutOspfv3VirtIfAuthNeighbor);

    pAuthkeyInfo = V3GetFindVirtIfAuthkeyInfoInCxt (pOspfCxt, &transitAreaId,
                                                    &(nbrId),
                                                    (UINT2)
                                                    i4FutOspfv3VirtIfAuthKeyId);

    if (pAuthkeyInfo != NULL)
    {
        *pi4RetValFutOspfv3VirtIfAuthKeyStatus =
            (INT4) pAuthkeyInfo->u1AuthkeyStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhGetFirstIndexFutOspfv3VirtIfCryptoAuthTable
 *  Input       :  The Indices Ospfv3VirtIfAreaId Ospfv3VirtIfNeighbor
 *  Output      :  The Get First Routines gets the Lexicographicaly
 *                 First Entry from the Table.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFirstIndexFutOspfv3VirtIfCryptoAuthTable (UINT4 *pu4Ospfv3VirtIfAreaId,
                                                UINT4 *pu4Ospfv3VirtIfNeighbor)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pLstNode = TMO_SLL_First (&(pOspfCxt->virtIfLst))) != NULL)
    {
        pInterface = V3_GET_IF_PTR_FROM_SORT_LST (pLstNode);
        OSPFV3_RTR_ID_COPY (transitAreaId, pInterface->transitAreaId);
        OSPFV3_RTR_ID_COPY (nbrId, pInterface->destRtrId);
        *pu4Ospfv3VirtIfAreaId = OSPFV3_BUFFER_DWFROMPDU (transitAreaId);
        *pu4Ospfv3VirtIfNeighbor = OSPFV3_BUFFER_DWFROMPDU (nbrId);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3VirtIfCryptoAuthTable
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                nextOspfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor
                nextOspfv3VirtIfNeighbor
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3VirtIfCryptoAuthTable (UINT4 u4Ospfv3VirtIfAreaId,
                                               UINT4 *pu4NextOspfv3VirtIfAreaId,
                                               UINT4 u4Ospfv3VirtIfNeighbor,
                                               UINT4
                                               *pu4NextOspfv3VirtIfNeighbor)
{
    tV3OsAreaId         currAreaId;
    tV3OsRouterId       currNbrId;
    tV3OsAreaId         nextAreaId;
    tV3OsRouterId       nextNbrId;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (currNbrId, u4Ospfv3VirtIfNeighbor);

    TMO_SLL_Scan (&(pOspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = V3_GET_IF_PTR_FROM_SORT_LST (pLstNode);

        switch (V3UtilAreaIdComp (currAreaId, pInterface->transitAreaId))
        {
            case OSPFV3_GREATER:
                /* try next entry */
                continue;

            case OSPFV3_EQUAL:
                /* first index is complete */
                switch (V3UtilAreaIdComp (currNbrId, pInterface->destRtrId))
                {
                    case OSPFV3_GREATER:
                        /* try next entry */
                        continue;
                    case OSPFV3_EQUAL:
                        /* second index is complete */
                        /* try next entry */
                        continue;
                        /* next found, fall through */
                    case OSPFV3_LESS:
                        break;
                        /* next found, fall through */
                    default:
                        break;
                }
                /* next found, fall through */

            case OSPFV3_LESS:
                OSPFV3_RTR_ID_COPY (nextAreaId, pInterface->transitAreaId);
                OSPFV3_RTR_ID_COPY (nextNbrId, pInterface->destRtrId);
                *pu4NextOspfv3VirtIfAreaId =
                    OSPFV3_BUFFER_DWFROMPDU (nextAreaId);
                *pu4NextOspfv3VirtIfNeighbor =
                    OSPFV3_BUFFER_DWFROMPDU (nextNbrId);
                return SNMP_SUCCESS;

            default:
                break;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfCryptoAuthType
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                retValFutOspfv3VirtIfCryptoAuthType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfCryptoAuthType (UINT4 u4Ospfv3VirtIfAreaId,
                                     UINT4 u4Ospfv3VirtIfNeighbor,
                                     INT4
                                     *pi4RetValFutOspfv3VirtIfCryptoAuthType)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    UNUSED_PARAM (u4Ospfv3VirtIfAreaId);
    UNUSED_PARAM (u4Ospfv3VirtIfNeighbor);

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);
    if ((pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValFutOspfv3VirtIfCryptoAuthType =
            (INT4) pInterface->u2CryptoAuthType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3VirtIfCryptoAuthMode
 Input       :  The Indices
                Ospfv3VirtIfAreaId
                Ospfv3VirtIfNeighbor

                The Object
                retValFutOspfv3VirtIfCryptoAuthMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3VirtIfCryptoAuthMode (UINT4 u4Ospfv3VirtIfAreaId,
                                     UINT4 u4Ospfv3VirtIfNeighbor,
                                     INT4
                                     *pi4RetValFutOspfv3VirtIfCryptoAuthMode)
{
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       nbrId;
    tV3OsInterface     *pInterface;
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (transitAreaId, u4Ospfv3VirtIfAreaId);
    OSPFV3_BUFFER_DWTOPDU (nbrId, u4Ospfv3VirtIfNeighbor);
    if ((pInterface = V3GetFindVirtIfInCxt (pOspfCxt, &transitAreaId, &nbrId))
        != NULL)
    {
        *pi4RetValFutOspfv3VirtIfCryptoAuthMode = (INT4) pInterface->u1AuthMode;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3RoutingTable
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3RoutingTable (INT4 *pi4FutOspfv3RouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3RouteDest,
                                       UINT4 *pi4FutOspfv3RoutePfxLength,
                                       INT4 *pi4FutOspfv3RouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3RouteNextHop)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tInputParams        inParams;
    tIp6Addr            ip6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4FutOspfv3RouteDestType = OSPFV3_INET_ADDR_TYPE;
    *pi4FutOspfv3RouteNextHopType = OSPFV3_INET_ADDR_TYPE;

    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, (VOID *) &pRtEntry,
                          &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (*pFutOspfv3RouteDest->pu1_OctetList,
                          pRtEntry->destRtPrefix);
    pFutOspfv3RouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
    *pi4FutOspfv3RoutePfxLength = pRtEntry->u1PrefixLen;

    V3GetLeastNextHopIp6Addr (pRtEntry, &ip6Addr);
    OSPFV3_IP6_ADDR_COPY (*pFutOspfv3RouteNextHop->pu1_OctetList, ip6Addr);
    pFutOspfv3RouteNextHop->i4_Length = OSPFV3_IPV6_ADDR_LEN;

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3RoutingTable
 Input       :  The Indices
                FutOspfv3RouteDestType
                nextFutOspfv3RouteDestType
                FutOspfv3RouteDest
                nextFutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                nextFutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                nextFutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop
                nextFutOspfv3RouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3RoutingTable (INT4 i4FutOspfv3RouteDestType,
                                      INT4 *pi4NextFutOspfv3RouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3RouteDest,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFutOspfv3RouteDest,
                                      UINT4 i4FutOspfv3RoutePfxLength,
                                      UINT4 *pi4NextFutOspfv3RoutePfxLength,
                                      INT4 i4FutOspfv3RouteNextHopType,
                                      INT4 *pi4NextFutOspfv3RouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3RouteNextHop,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFutOspfv3RouteNextHop)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsRtEntry       *pNextRtEntry = NULL;
    VOID               *pTemp = NULL;
    tInputParams        inParams;
    tOutputParams       outParams;
    tIp6Addr            nextHopIp6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               au1Key[OSPFV3_CLI_KEY_SIZE];
    VOID               *pTempPtr = NULL;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType > OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFutOspfv3RouteDestType = OSPFV3_INET_ADDR_TYPE;
    *pi4NextFutOspfv3RouteNextHopType = OSPFV3_INET_ADDR_TYPE;

    inParams.pRoot = pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = (UINT1) i4FutOspfv3RoutePfxLength;

    OSPFV3_IP6_ADDR_COPY (au1Key, *(pFutOspfv3RouteDest->pu1_OctetList));
    au1Key[sizeof (tIp6Addr)] = (UINT1) i4FutOspfv3RoutePfxLength;
    inParams.Key.pKey = au1Key;

    if (TrieSearch (&inParams, &outParams, &pTemp) != TRIE_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pRtEntry = (tV3OsRtEntry *) (VOID *) outParams.pAppSpecInfo;

    if (V3GetNextLeastNextHopIp6Addr (pRtEntry, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RouteNextHop->pu1_OctetList,
                                      &nextHopIp6Addr) != SNMP_SUCCESS)
    {
        if (TrieGetNextNode (&inParams, NULL,
                             &pTempPtr,
                             (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
        {
            return SNMP_FAILURE;
        }
        pNextRtEntry = (tV3OsRtEntry *) pTempPtr;
        if (V3GetLeastNextHopIp6Addr (pNextRtEntry,
                                      &nextHopIp6Addr) == OSIX_SUCCESS)
        {
            OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RouteDest->pu1_OctetList,
                                  pNextRtEntry->destRtPrefix);
            pNextFutOspfv3RouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            *pi4NextFutOspfv3RoutePfxLength = pNextRtEntry->u1PrefixLen;
            OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RouteNextHop->pu1_OctetList,
                                  nextHopIp6Addr);
            pNextFutOspfv3RouteNextHop->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RouteDest->pu1_OctetList,
                              pRtEntry->destRtPrefix);
        pNextFutOspfv3RouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        *pi4NextFutOspfv3RoutePfxLength = pRtEntry->u1PrefixLen;
        OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RouteNextHop->pu1_OctetList,
                              nextHopIp6Addr);
        pNextFutOspfv3RouteNextHop->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3NeighborBfdTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3NeighborBfdTable (INT4 *pi4Ospfv3NbrIfIndex,
                                           UINT4 *pu4Ospfv3NbrRtrId)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gV3OsRtr.pNbrTableCache = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if ((pNbr->u1ConfigStatus != OSPFV3_CONFIGURED_NBR) &&
            (pNbr->u1ConfigStatus != OSPFV3_DISCOVERED_SBYIF))
        {
            *pi4Ospfv3NbrIfIndex = (INT4) pNbr->pInterface->u4InterfaceId;
            *pu4Ospfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId);
            gV3OsRtr.pNbrTableCache = pLstNode;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3NeighborBfdTable
 Input       :  The Indices
                Ospfv3NbrIfIndex
                nextOspfv3NbrIfIndex
                Ospfv3NbrRtrId
                nextOspfv3NbrRtrId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3NeighborBfdTable (INT4 i4Ospfv3NbrIfIndex,
                                          INT4 *pi4NextOspfv3NbrIfIndex,
                                          UINT4 u4Ospfv3NbrRtrId,
                                          UINT4 *pu4NextOspfv3NbrRtrId)
{

    tV3OsRouterId       currNbrRtrId;
    tV3OsRouterId       nextNbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (currNbrRtrId, u4Ospfv3NbrRtrId);

    if (gV3OsRtr.pNbrTableCache != NULL)
    {
        for (;;)
        {
            gV3OsRtr.pNbrTableCache =
                (tTMO_SLL_NODE *) TMO_SLL_Next (&(pV3OspfCxt->sortNbrLst),
                                                gV3OsRtr.pNbrTableCache);

            if (gV3OsRtr.pNbrTableCache == NULL)
            {
                return SNMP_FAILURE;
            }

            pNbr =
                OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr,
                                     gV3OsRtr.pNbrTableCache);

            if ((pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR) ||
                (pNbr->u1ConfigStatus == OSPFV3_DISCOVERED_SBYIF))
            {
                continue;
            }

            if (pNbr->pInterface->u4InterfaceId == (UINT4) i4Ospfv3NbrIfIndex)
            {
                if (V3UtilRtrIdComp (pNbr->nbrRtrId, currNbrRtrId) ==
                    OSPFV3_GREATER)
                {
                    OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                    *pi4NextOspfv3NbrIfIndex =
                        (INT4) pNbr->pInterface->u4InterfaceId;
                    *pu4NextOspfv3NbrRtrId =
                        OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                    return SNMP_SUCCESS;
                }
            }
            else if (pNbr->pInterface->u4InterfaceId >
                     (UINT4) i4Ospfv3NbrIfIndex)
            {
                OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                *pi4NextOspfv3NbrIfIndex =
                    (INT4) pNbr->pInterface->u4InterfaceId;
                *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                return SNMP_SUCCESS;
            }
        }
    }

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        gV3OsRtr.pNbrTableCache = pLstNode;
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (pNbr->u1ConfigStatus == OSPFV3_CONFIGURED_NBR)
        {
            continue;
        }

        if (pNbr->pInterface->u4InterfaceId == (UINT4) i4Ospfv3NbrIfIndex)
        {
            if (V3UtilRtrIdComp (pNbr->nbrRtrId, currNbrRtrId) ==
                OSPFV3_GREATER)
            {
                OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
                *pi4NextOspfv3NbrIfIndex =
                    (INT4) pNbr->pInterface->u4InterfaceId;
                *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
                return SNMP_SUCCESS;
            }
        }
        else if (pNbr->pInterface->u4InterfaceId > (UINT4) i4Ospfv3NbrIfIndex)
        {
            OSPFV3_RTR_ID_COPY (nextNbrRtrId, pNbr->nbrRtrId);
            *pi4NextOspfv3NbrIfIndex = (INT4) pNbr->pInterface->u4InterfaceId;
            *pu4NextOspfv3NbrRtrId = OSPFV3_BUFFER_DWFROMPDU (nextNbrRtrId);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3NbrBfdState
 Input       :  The Indices
                Ospfv3NbrIfIndex
                Ospfv3NbrRtrId

                The Object
                retValFutOspfv3NbrBfdState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3NbrBfdState (INT4 i4Ospfv3NbrIfIndex,
                            UINT4 u4Ospfv3NbrRtrId,
                            INT4 *pi4RetValFutOspfv3NbrBfdState)
{
    tV3OsRouterId       nbrRtrId;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (nbrRtrId, u4Ospfv3NbrRtrId);

    if ((pNbr = V3GetFindNbrInCxt (pV3OspfCxt, &(nbrRtrId),
                                   (UINT4) i4Ospfv3NbrIfIndex)) != NULL)
    {
        *pi4RetValFutOspfv3NbrBfdState = (INT4) pNbr->u1NbrBfdState;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouteType
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop

                The Object 
                retValFutOspfv3RouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouteType (INT4 i4FutOspfv3RouteDestType,
                          tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteDest,
                          UINT4 i4FutOspfv3RoutePfxLength,
                          INT4 i4FutOspfv3RouteNextHopType,
                          tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteNextHop,
                          INT4 *pi4RetValFutOspfv3RouteType)
{
    INT1                i1ReturnValue = SNMP_FAILURE;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue = V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *)
                                               pFutOspfv3RouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               i4FutOspfv3RoutePfxLength,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3RouteNextHop->
                                               pu1_OctetList,
                                               (UINT4 *)
                                               pi4RetValFutOspfv3RouteType,
                                               OSPFV3_ROUTE_RTTYPE,
                                               OSPFV3_DEST_NETWORK);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouteAreaId
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop

                The Object 
                retValFutOspfv3RouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouteAreaId (INT4 i4FutOspfv3RouteDestType,
                            tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteDest,
                            UINT4 i4FutOspfv3RoutePfxLength,
                            INT4 i4FutOspfv3RouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteNextHop,
                            UINT4 *pu4RetValFutOspfv3RouteAreaId)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue = V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *)
                                               pFutOspfv3RouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               i4FutOspfv3RoutePfxLength,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3RouteNextHop->
                                               pu1_OctetList,
                                               pu4RetValFutOspfv3RouteAreaId,
                                               OSPFV3_ROUTE_AREAID,
                                               OSPFV3_DEST_NETWORK);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouteCost
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop

                The Object 
                retValFutOspfv3RouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouteCost (INT4 i4FutOspfv3RouteDestType,
                          tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteDest,
                          UINT4 i4FutOspfv3RoutePfxLength,
                          INT4 i4FutOspfv3RouteNextHopType,
                          tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteNextHop,
                          INT4 *pi4RetValFutOspfv3RouteCost)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue = V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *)
                                               pFutOspfv3RouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               i4FutOspfv3RoutePfxLength,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3RouteNextHop->
                                               pu1_OctetList,
                                               (UINT4 *)
                                               pi4RetValFutOspfv3RouteCost,
                                               OSPFV3_ROUTE_COST,
                                               OSPFV3_DEST_NETWORK);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouteType2Cost
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop

                The Object 
                retValFutOspfv3RouteType2Cost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouteType2Cost (INT4 i4FutOspfv3RouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteDest,
                               UINT4 i4FutOspfv3RoutePfxLength,
                               INT4 i4FutOspfv3RouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3RouteNextHop,
                               INT4 *pi4RetValFutOspfv3RouteType2Cost)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue = V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *)
                                               pFutOspfv3RouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               i4FutOspfv3RoutePfxLength,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3RouteNextHop->
                                               pu1_OctetList,
                                               (UINT4 *)
                                               pi4RetValFutOspfv3RouteType2Cost,
                                               OSPFV3_ROUTE_TYPE2COST,
                                               OSPFV3_DEST_NETWORK);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouteInterfaceIndex
 Input       :  The Indices
                FutOspfv3RouteDestType
                FutOspfv3RouteDest
                FutOspfv3RoutePfxLength
                FutOspfv3RouteNextHopType
                FutOspfv3RouteNextHop

                The Object 
                retValFutOspfv3RouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouteInterfaceIndex (INT4 i4FutOspfv3RouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFutOspfv3RouteDest,
                                    UINT4 i4FutOspfv3RoutePfxLength,
                                    INT4 i4FutOspfv3RouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFutOspfv3RouteNextHop,
                                    INT4 *pi4RetValFutOspfv3RouteInterfaceIndex)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RouteDestType != OSPFV3_INET_ADDR_TYPE ||
        i4FutOspfv3RouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue = V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *)
                                               pFutOspfv3RouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               i4FutOspfv3RoutePfxLength,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3RouteNextHop->
                                               pu1_OctetList,
                                               (UINT4 *)
                                               pi4RetValFutOspfv3RouteInterfaceIndex,
                                               OSPFV3_ROUTE_IFACEINDEX,
                                               OSPFV3_DEST_NETWORK);

    return i1ReturnValue;
}

/* LOW LEVEL Routines for Table : FutOspfv3AsExternalAggregationTable. */

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfv3AsExternalAggregationTable (INT4
                                                     *pi4FutOspfv3AsExternalAggregationNetType,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pFutOspfv3AsExternalAggregationNet,
                                                     UINT4
                                                     *pi4FutOspfv3AsExternalAggregationPfxLength,
                                                     UINT4
                                                     *pu4FutOspfv3AsExternalAggregationAreaId)
{
    tV3OsAsExtAddrRange AsExtAddrRng;
    tV3OsAsExtAddrRange NxtAsExtAddrRng;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (&AsExtAddrRng, OSPFV3_INIT_VAL, sizeof (tV3OsAsExtAddrRange));
    MEMSET (&NxtAsExtAddrRng, OSPFV3_INIT_VAL, sizeof (tV3OsAsExtAddrRange));

    *pi4FutOspfv3AsExternalAggregationNetType = OSPFV3_INET_ADDR_TYPE;

    if (V3GetNextLeastasExtAddrRange (&AsExtAddrRng, &NxtAsExtAddrRng)
        == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (*pFutOspfv3AsExternalAggregationNet->
                          pu1_OctetList, NxtAsExtAddrRng.ip6Addr);
    pFutOspfv3AsExternalAggregationNet->i4_Length = OSPFV3_IPV6_ADDR_LEN;
    *pi4FutOspfv3AsExternalAggregationPfxLength =
        NxtAsExtAddrRng.u1PrefixLength;
    *pu4FutOspfv3AsExternalAggregationAreaId =
        OSPFV3_BUFFER_DWFROMPDU (NxtAsExtAddrRng.areaId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3AsExternalAggregationTable
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                nextFutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                nextFutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                nextFutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId
                nextFutOspfv3AsExternalAggregationAreaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3AsExternalAggregationTable (INT4
                                                    i4FutOspfv3AsExternalAggregationNetType,
                                                    INT4
                                                    *pi4NextFutOspfv3AsExternalAggregationNetType,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pFutOspfv3AsExternalAggregationNet,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pNextFutOspfv3AsExternalAggregationNet,
                                                    UINT4
                                                    i4FutOspfv3AsExternalAggregationPfxLength,
                                                    UINT4
                                                    *pi4NextFutOspfv3AsExternalAggregationPfxLength,
                                                    UINT4
                                                    u4FutOspfv3AsExternalAggregationAreaId,
                                                    UINT4
                                                    *pu4NextFutOspfv3AsExternalAggregationAreaId)
{
    tV3OsAsExtAddrRange AsExtRng;
    tV3OsAsExtAddrRange NxtAsExtRng;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    MEMSET (&AsExtRng, OSPFV3_INIT_VAL, sizeof (tV3OsAsExtAddrRange));
    MEMSET (&NxtAsExtRng, OSPFV3_INIT_VAL, sizeof (tV3OsAsExtAddrRange));

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    *pi4NextFutOspfv3AsExternalAggregationNetType = OSPFV3_INET_ADDR_TYPE;

    OSPFV3_IP6_ADDR_COPY (AsExtRng.ip6Addr,
                          *pFutOspfv3AsExternalAggregationNet->pu1_OctetList);
    AsExtRng.u1PrefixLength = (UINT1) i4FutOspfv3AsExternalAggregationPfxLength;
    OSPFV3_BUFFER_DWTOPDU (AsExtRng.areaId,
                           u4FutOspfv3AsExternalAggregationAreaId);
    if (V3GetNextLeastasExtAddrRange (&AsExtRng, &NxtAsExtRng) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3AsExternalAggregationNet->
                          pu1_OctetList, NxtAsExtRng.ip6Addr);
    pNextFutOspfv3AsExternalAggregationNet->i4_Length = OSPFV3_IPV6_ADDR_LEN;
    *pi4NextFutOspfv3AsExternalAggregationPfxLength =
        NxtAsExtRng.u1PrefixLength;
    *pu4NextFutOspfv3AsExternalAggregationAreaId =
        OSPFV3_BUFFER_DWFROMPDU (NxtAsExtRng.areaId);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFutOspfv3AsExternalAggregationEffect
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                retValFutOspfv3AsExternalAggregationEffect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3AsExternalAggregationEffect (INT4
                                            i4FutOspfv3AsExternalAggregationNetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFutOspfv3AsExternalAggregationNet,
                                            UINT4
                                            i4FutOspfv3AsExternalAggregationPfxLength,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationAreaId,
                                            INT4
                                            *pi4RetValFutOspfv3AsExternalAggregationEffect)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    pAsExtAddrRng =
        V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                pFutOspfv3AsExternalAggregationNet->
                                pu1_OctetList,
                                (UINT1)
                                i4FutOspfv3AsExternalAggregationPfxLength,
                                areaId);

    if (pAsExtAddrRng != NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    " AS External Aggregation not existing\n");
        *pi4RetValFutOspfv3AsExternalAggregationEffect =
            (INT4) pAsExtAddrRng->u1RangeEffect;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  V3GetFindAsExtRngInCxt
 Input       :   Utility routine used by other GET functions
                 pV3OspfCxt
                 pAsExtNet 
                 u1PfxLen
                 areaId 

 Output      :  None

 Returns     :  As ext Range OR NULL
****************************************************************************/
PUBLIC tV3OsAsExtAddrRange *
V3GetFindAsExtRngInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pAsExtNet,
                        UINT1 u1PfxLen, tV3OsAreaId areaId)
{
    tV3OsAsExtAddrRange *pScanAsExtRng = NULL;
    tTMO_SLL_NODE      *pScanAsExtNode = NULL;

    if (NULL != gV3OsRtr.pExtAggrTableCache)
    {
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr,
                                             gV3OsRtr.pExtAggrTableCache);

        if ((V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr), pAsExtNet) ==
             OSPFV3_EQUAL) && (pScanAsExtRng->u1PrefixLength == u1PfxLen)
            && (V3UtilAreaIdComp (pScanAsExtRng->areaId, areaId) ==
                OSPFV3_EQUAL))
        {
            return pScanAsExtRng;
        }
        pScanAsExtRng = NULL;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->asExtAddrRangeLst),
                  pScanAsExtNode, tTMO_SLL_NODE *)
    {
        pScanAsExtRng = OSPFV3_GET_BASE_PTR (tV3OsAsExtAddrRange,
                                             nextAsExtAddrRngInRtr,
                                             pScanAsExtNode);
        if ((V3UtilIp6AddrComp (&(pScanAsExtRng->ip6Addr), pAsExtNet) ==
             OSPFV3_EQUAL) && (pScanAsExtRng->u1PrefixLength == u1PfxLen)
            && (V3UtilAreaIdComp (pScanAsExtRng->areaId, areaId) ==
                OSPFV3_EQUAL))
        {
            return pScanAsExtRng;
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3AsExternalAggregationTranslation
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                retValFutOspfv3AsExternalAggregationTranslation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3AsExternalAggregationTranslation (INT4
                                                 i4FutOspfv3AsExternalAggregationNetType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFutOspfv3AsExternalAggregationNet,
                                                 UINT4
                                                 i4FutOspfv3AsExternalAggregationPfxLength,
                                                 UINT4
                                                 u4FutOspfv3AsExternalAggregationAreaId,
                                                 INT4
                                                 *pi4RetValFutOspfv3AsExternalAggregationTranslation)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    pAsExtAddrRng =
        V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                pFutOspfv3AsExternalAggregationNet->
                                pu1_OctetList,
                                (UINT1)
                                i4FutOspfv3AsExternalAggregationPfxLength,
                                areaId);

    if (pAsExtAddrRng != NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    " AS External Aggregation not existing\n");
        *pi4RetValFutOspfv3AsExternalAggregationTranslation =
            OSPFV3_MAP_TO_TRUTH_VALUE (pAsExtAddrRng->u1AggTranslation);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3AsExternalAggregationStatus
 Input       :  The Indices
                FutOspfv3AsExternalAggregationNetType
                FutOspfv3AsExternalAggregationNet
                FutOspfv3AsExternalAggregationPfxLength
                FutOspfv3AsExternalAggregationAreaId

                The Object
                retValFutOspfv3AsExternalAggregationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3AsExternalAggregationStatus (INT4
                                            i4FutOspfv3AsExternalAggregationNetType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pFutOspfv3AsExternalAggregationNet,
                                            UINT4
                                            i4FutOspfv3AsExternalAggregationPfxLength,
                                            UINT4
                                            u4FutOspfv3AsExternalAggregationAreaId,
                                            INT4
                                            *pi4RetValFutOspfv3AsExternalAggregationStatus)
{
    tV3OsAreaId         areaId;
    tV3OsAsExtAddrRange *pAsExtAddrRng = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3AsExternalAggregationNetType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3AsExternalAggregationAreaId);

    pAsExtAddrRng =
        V3GetFindAsExtRngInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                pFutOspfv3AsExternalAggregationNet->
                                pu1_OctetList,
                                (UINT1)
                                i4FutOspfv3AsExternalAggregationPfxLength,
                                areaId);

    if (pAsExtAddrRng != NULL)
    {
        *pi4RetValFutOspfv3AsExternalAggregationStatus =
            (INT4) pAsExtAddrRng->rangeStatus;
        return SNMP_SUCCESS;
    }

    OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                " AS External Aggregation not existing\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3BRRouteTable
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfv3BRRouteTable (UINT4 *pu4FutOspfv3BRRouteDest,
                                       INT4 *pi4FutOspfv3BRRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3BRRouteNextHop,
                                       INT4 *pi4FutOspfv3BRRouteDestType)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tIp6Addr            leastNextHopIp6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pRtEntry =
         (tV3OsRtEntry *)
         TMO_SLL_First (&(pV3OspfCxt->ospfV3RtTable.routesList))) != NULL)
    {
        *pu4FutOspfv3BRRouteDest = OSPFV3_BUFFER_DWFROMPDU
            (pRtEntry->destRtRtrId);
        *pi4FutOspfv3BRRouteDestType = pRtEntry->u1DestType;
        V3GetLeastNextHopIp6Addr (pRtEntry, &leastNextHopIp6Addr);
        *pi4FutOspfv3BRRouteNextHopType = OSPFV3_INET_ADDR_TYPE;
        OSPFV3_IP6_ADDR_COPY (*pFutOspfv3BRRouteNextHop->pu1_OctetList,
                              leastNextHopIp6Addr);
        pFutOspfv3BRRouteNextHop->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3BRRouteTable
 Input       :  The Indices
                FutOspfv3BRRouteDest
                nextFutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                nextFutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                nextFutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType
                nextFutOspfv3BRRouteDestType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3BRRouteTable (UINT4 u4FutOspfv3BRRouteDest,
                                      UINT4 *pu4NextFutOspfv3BRRouteDest,
                                      INT4 i4FutOspfv3BRRouteNextHopType,
                                      INT4 *pi4NextFutOspfv3BRRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3BRRouteNextHop,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextFutOspfv3BRRouteNextHop,
                                      INT4 i4FutOspfv3BRRouteDestType,
                                      INT4 *pi4NextFutOspfv3BRRouteDestType)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsRouterId       rtrId;
    tIp6Addr            leastNextHopIp6Addr;
    tIp6Addr            nextLeastNextHopIp6Addr;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT1               u1Found = OSIX_FALSE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteNextHopType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }
    OSPFV3_BUFFER_DWTOPDU (rtrId, u4FutOspfv3BRRouteDest);

    TMO_SLL_Scan (&(pV3OspfCxt->ospfV3RtTable.routesList), pRtEntry,
                  tV3OsRtEntry *)
    {
        switch (V3UtilRtrIdComp (pRtEntry->destRtRtrId, rtrId))
        {
            case OSPFV3_LESS:
            {
                continue;
            }
            case OSPFV3_EQUAL:

                if (pRtEntry->u1DestType == i4FutOspfv3BRRouteDestType)
                {
                    if (V3GetNextLeastNextHopIp6Addr (pRtEntry,
                                                      (tIp6Addr *) (VOID *)
                                                      pFutOspfv3BRRouteNextHop->
                                                      pu1_OctetList,
                                                      &nextLeastNextHopIp6Addr)
                        != SNMP_SUCCESS)
                    {
                        continue;
                    }
                    else
                    {
                        OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3BRRouteNextHop->
                                              pu1_OctetList,
                                              nextLeastNextHopIp6Addr);
                        *pi4NextFutOspfv3BRRouteNextHopType =
                            i4FutOspfv3BRRouteNextHopType;
                        pNextFutOspfv3BRRouteNextHop->i4_Length =
                            OSPFV3_IPV6_ADDR_LEN;
                        u1Found = OSIX_TRUE;
                    }
                    break;
                }
                else if (pRtEntry->u1DestType < i4FutOspfv3BRRouteDestType)
                {
                    continue;
                }
                /* fall through */
            default:
                V3GetLeastNextHopIp6Addr (pRtEntry, &leastNextHopIp6Addr);
                OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3BRRouteNextHop->
                                      pu1_OctetList, leastNextHopIp6Addr);

                *pi4NextFutOspfv3BRRouteNextHopType =
                    i4FutOspfv3BRRouteNextHopType;
                pNextFutOspfv3BRRouteNextHop->i4_Length = OSPFV3_IPV6_ADDR_LEN;
                u1Found = OSIX_TRUE;
                break;
        }

        if (u1Found == OSIX_TRUE)
        {
            *pu4NextFutOspfv3BRRouteDest = OSPFV3_BUFFER_DWFROMPDU
                (pRtEntry->destRtRtrId);

            *pi4NextFutOspfv3BRRouteNextHopType = OSPFV3_INET_ADDR_TYPE;
            *pi4NextFutOspfv3BRRouteDestType = pRtEntry->u1DestType;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/***************************************************************************/
/* Function    :  V3OspfRtTblGetObjectInCxt                                */
/*                                                                         */
/* Description :  Find the Route Entry in the Routing table for the        */
/*                given indices                                            */
/*                                                                         */
/* Input       :  pV3OspfCxt : Context pointer                             */
/*                u4Dest : Destination Route                               */
/*                u4Mask : Mask of the route entry.                        */
/*                i4Tos  : Tos Value                                       */
/*                u4NextHop : Next Hop Value                               */
/*                pu4Val    : Pointet to the variable                      */
/*                u1ObjType  : Object Identifier.                          */
/*                u1DestType : Object Identifier.                          */
/* Output      :  None                                                     */
/*                                                                         */
/* Returns     :  SNMP_SUCCESS or SNMP_FAILURE                             */
/***************************************************************************/
PRIVATE INT1
V3OspfRtTblGetObjectInCxt (tV3OspfCxt * pV3OspfCxt, VOID *DestId,
                           UINT1 u1PfxLen, tIp6Addr * pNextHop, UINT4 *pu4Val,
                           UINT1 u1ObjType, UINT1 u1DestType)
{
    tV3OsRtEntry       *pRtEntry = NULL;

    UNUSED_PARAM (u1PfxLen);

    if ((pRtEntry =
         V3RtcFindRtEntryInCxt (pV3OspfCxt, (VOID *) DestId, u1PfxLen,
                                u1DestType)) == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u1ObjType)
    {
        case OSPFV3_ROUTE_RTTYPE:
            V3OspfGetRTAttribute (pRtEntry, *pNextHop, OSPFV3_ROUTE_RTTYPE,
                                  pu4Val);
            break;

        case OSPFV3_ROUTE_AREAID:
            V3OspfGetRTAttribute (pRtEntry, *pNextHop, OSPFV3_ROUTE_AREAID,
                                  pu4Val);
            break;

        case OSPFV3_ROUTE_COST:
            V3OspfGetRTAttribute (pRtEntry, *pNextHop, OSPFV3_ROUTE_COST,
                                  pu4Val);
            break;

        case OSPFV3_ROUTE_TYPE2COST:
            V3OspfGetRTAttribute (pRtEntry,
                                  *pNextHop, OSPFV3_ROUTE_TYPE2COST, pu4Val);
            break;

        case OSPFV3_ROUTE_IFACEINDEX:
            V3OspfGetRTAttribute (pRtEntry,
                                  *pNextHop, OSPFV3_ROUTE_IFACEINDEX, pu4Val);
            break;

        default:
            return SNMP_FAILURE;
    }

    return (SNMP_SUCCESS);
}

/***************************************************************************/
/* Function    :  V3OspfGetRTAttribute                                     */
/*                                                                         */
/* Description :  Find the attribute value for the given attribute in the  */
/*                Routing table                                            */
/*                given indices                                            */
/*                                                                         */
/* Input       :  pRtEnrty : Route Entry                                   */
/*                u4NextHop : Next Hop Value                               */
/*                u1ObjType  : Object Identifier.                          */
/*                pu4ObjVal : Object Value                                 */
/* Output      :  None                                                     */
/*                                                                         */
/* Returns     :  None                                                     */
/***************************************************************************/
PRIVATE VOID
V3OspfGetRTAttribute (tV3OsRtEntry * pRtEntry,
                      tIp6Addr nextHopIp6Addr, UINT1 u1ObjType,
                      UINT4 *pu4ObjVal)
{
    tV3OsPath          *pPath = NULL;
    INT2                i2NextHopCount = 0;

    *pu4ObjVal = (UINT4) 0;

    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        i2NextHopCount = 0;
        while ((i2NextHopCount < (pPath->u1HopCount)) &&
               (i2NextHopCount < OSPFV3_MAX_NEXT_HOPS))
        {
            if (V3UtilIp6AddrComp (&(nextHopIp6Addr),
                                   &(pPath->aNextHops[i2NextHopCount].
                                     nbrIpv6Addr)) == OSPFV3_EQUAL)
            {
                switch (u1ObjType)
                {
                    case OSPFV3_ROUTE_RTTYPE:
                        *pu4ObjVal = (UINT4) (pPath->u1PathType);
                        break;

                    case OSPFV3_ROUTE_AREAID:
                        *pu4ObjVal = OSPFV3_BUFFER_DWFROMPDU (pPath->areaId);
                        break;

                    case OSPFV3_ROUTE_COST:
                        *pu4ObjVal = pPath->u4Cost;
                        break;

                    case OSPFV3_ROUTE_TYPE2COST:
                        *pu4ObjVal = pPath->u4Type2Cost;
                        break;

                    case OSPFV3_ROUTE_IFACEINDEX:
                        if (pPath->aNextHops[i2NextHopCount].pInterface != NULL)
                        {
                            *pu4ObjVal = pPath->aNextHops[i2NextHopCount].
                                pInterface->u4InterfaceId;
                        }
                        else
                        {
                            *pu4ObjVal = 0;
                        }
                        break;

                    default:
                        break;
                }
            }
            i2NextHopCount++;
        }
    }
}

/*****************************************************************************/
/* Function     : V3GetLeastNextHopIp6Addr                                   */
/* Description  : The routine returns the least of the next hop IP6 address  */
/*                contained in an array corresponding to the given path      */
/* Input        : pRtEntry                                                   */
/*                pLeastNextHopIp6Addr                                       */
/* Output       : None                                                       */
/* Returns      : OSIX_SUCCESS, if least next hop IP6 address is found        */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/
PRIVATE INT1
V3GetLeastNextHopIp6Addr (tV3OsRtEntry * pRtEntry,
                          tIp6Addr * pLeastNextHopIp6Addr)
{
    INT2                i2NextHopCount = 0;
    tV3OsPath          *pPath = NULL;
    tIp6Addr            tempIp6Addr;

    MEMSET (pLeastNextHopIp6Addr, 0xff, sizeof (tIp6Addr));

    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        i2NextHopCount = 0;
        while ((i2NextHopCount < pPath->u1HopCount) &&
               (i2NextHopCount < OSPFV3_MAX_NEXT_HOPS))
        {
            if (V3UtilIp6AddrComp
                (&(pPath->aNextHops[i2NextHopCount].nbrIpv6Addr),
                 &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
            {
                OSPFV3_IP6_ADDR_COPY (*pLeastNextHopIp6Addr, gV3OsNullIp6Addr);
                return OSIX_SUCCESS;
            }
            if (V3UtilIp6AddrComp
                (&(pPath->aNextHops[i2NextHopCount].nbrIpv6Addr),
                 pLeastNextHopIp6Addr) == OSPFV3_LESS)
            {
                OSPFV3_IP6_ADDR_COPY (*pLeastNextHopIp6Addr,
                                      pPath->aNextHops[i2NextHopCount].
                                      nbrIpv6Addr);
            }
            i2NextHopCount++;
        }
    }

    MEMSET (&tempIp6Addr, 0xff, sizeof (tIp6Addr));

    if (V3UtilIp6AddrComp (pLeastNextHopIp6Addr, &tempIp6Addr) != OSPFV3_EQUAL)
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

/*****************************************************************************/
/* Function     : V3GetNextLeastNextHopIp6Addr                               */
/* Description  : The routine returns the next least of the next hop         */
/*                IP address                                                 */
/*                contained in an array corresponding to the given path      */
/* Input        : pPath                                                      */
/*                pNextLeastNextHopIp6Addr                                    */
/* Output       : None                                                       */
/* Returns      : OSIX_SUCCESS, if least next hop IP6 address is found        */
/*                OSIX_FAILURE, otherwise                                    */
/*****************************************************************************/

PRIVATE INT1
V3GetNextLeastNextHopIp6Addr (tV3OsRtEntry * pRtEntry,
                              tIp6Addr * pNextHopIp6Addr,
                              tIp6Addr * pNextLeastNextHopIp6Addr)
{
    INT2                i2NextHopCount = 0;
    tIp6Addr            tempNextHopIp6Addr;
    tV3OsTruthValue     bNextHopFound = OSIX_FALSE;
    tV3OsPath          *pPath = NULL;

    MEMSET (&tempNextHopIp6Addr, 0xff, sizeof (tIp6Addr));

    TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
    {
        i2NextHopCount = 0;
        while ((i2NextHopCount < pPath->u1HopCount) &&
               (i2NextHopCount < OSPFV3_MAX_NEXT_HOPS))
        {
            if (((V3UtilIp6AddrComp
                  (&(pPath->aNextHops[i2NextHopCount].nbrIpv6Addr),
                   pNextHopIp6Addr) == OSPFV3_GREATER)
                 &&
                 (((V3UtilIp6AddrComp
                    (&(pPath->aNextHops[i2NextHopCount].nbrIpv6Addr),
                     &tempNextHopIp6Addr)) == OSPFV3_LESS))))
            {
                OSPFV3_IP6ADDR_CPY (&tempNextHopIp6Addr,
                                    &(pPath->aNextHops[i2NextHopCount].
                                      nbrIpv6Addr));
                bNextHopFound = OSIX_TRUE;
            }
            i2NextHopCount++;
        }
    }

    if (bNextHopFound == OSIX_TRUE)
    {
        OSPFV3_IP6ADDR_CPY (pNextLeastNextHopIp6Addr, &tempNextHopIp6Addr);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3BRRouteType
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType

                The Object 
                retValFutOspfv3BRRouteType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BRRouteType (UINT4 u4FutOspfv3BRRouteDest,
                            INT4 i4FutOspfv3BRRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pFutOspfv3BRRouteNextHop,
                            INT4 i4FutOspfv3BRRouteDestType,
                            INT4 *pi4RetValFutOspfv3BRRouteType)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteDestType == OSPFV3_DEST_NETWORK)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3BRRouteDest);
    i1ReturnValue =
        V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *) areaId, 0,
                                   (tIp6Addr *) (VOID *)
                                   pFutOspfv3BRRouteNextHop->
                                   pu1_OctetList,
                                   (UINT4 *) pi4RetValFutOspfv3BRRouteType,
                                   OSPFV3_ROUTE_RTTYPE,
                                   (UINT1) i4FutOspfv3BRRouteDestType);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3BRRouteAreaId
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType

                The Object 
                retValFutOspfv3BRRouteAreaId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BRRouteAreaId (UINT4 u4FutOspfv3BRRouteDest,
                              INT4 i4FutOspfv3BRRouteNextHopType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFutOspfv3BRRouteNextHop,
                              INT4 i4FutOspfv3BRRouteDestType,
                              UINT4 *pu4RetValFutOspfv3BRRouteAreaId)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteDestType == OSPFV3_DEST_NETWORK)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3BRRouteDest);
    i1ReturnValue =
        V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *) areaId, 0,
                                   (tIp6Addr *) (VOID *)
                                   pFutOspfv3BRRouteNextHop->
                                   pu1_OctetList,
                                   pu4RetValFutOspfv3BRRouteAreaId,
                                   OSPFV3_ROUTE_AREAID,
                                   (UINT1) i4FutOspfv3BRRouteDestType);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3BRRouteCost
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType

                The Object 
                retValFutOspfv3BRRouteCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BRRouteCost (UINT4 u4FutOspfv3BRRouteDest,
                            INT4 i4FutOspfv3BRRouteNextHopType,
                            tSNMP_OCTET_STRING_TYPE * pFutOspfv3BRRouteNextHop,
                            INT4 i4FutOspfv3BRRouteDestType,
                            INT4 *pi4RetValFutOspfv3BRRouteCost)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteDestType == OSPFV3_DEST_NETWORK)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3BRRouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3BRRouteDest);

    i1ReturnValue =
        V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *) areaId, 0,
                                   (tIp6Addr *) (VOID *)
                                   pFutOspfv3BRRouteNextHop->
                                   pu1_OctetList,
                                   (UINT4 *) pi4RetValFutOspfv3BRRouteCost,
                                   OSPFV3_ROUTE_COST,
                                   (UINT1) i4FutOspfv3BRRouteDestType);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3BRRouteInterfaceIndex
 Input       :  The Indices
                FutOspfv3BRRouteDest
                FutOspfv3BRRouteNextHopType
                FutOspfv3BRRouteNextHop
                FutOspfv3BRRouteDestType

                The Object 
                retValFutOspfv3BRRouteInterfaceIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BRRouteInterfaceIndex (UINT4 u4FutOspfv3BRRouteDest,
                                      INT4 i4FutOspfv3BRRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3BRRouteNextHop,
                                      INT4 i4FutOspfv3BRRouteDestType,
                                      INT4
                                      *pi4RetValFutOspfv3BRRouteInterfaceIndex)
{
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1ReturnValue = SNMP_FAILURE;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (i4FutOspfv3BRRouteDestType == OSPFV3_DEST_NETWORK)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4FutOspfv3BRRouteDest);

    if (i4FutOspfv3BRRouteNextHopType != OSPFV3_INET_ADDR_TYPE)
    {
        return i1ReturnValue;
    }

    i1ReturnValue =
        V3OspfRtTblGetObjectInCxt (pV3OspfCxt, (VOID *) areaId, 0,
                                   (tIp6Addr *) (VOID *)
                                   pFutOspfv3BRRouteNextHop->
                                   pu1_OctetList,
                                   (UINT4 *)
                                   pi4RetValFutOspfv3BRRouteInterfaceIndex,
                                   OSPFV3_ROUTE_IFACEINDEX,
                                   (UINT1) i4FutOspfv3BRRouteDestType);

    return i1ReturnValue;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFutOspfv3RedistRouteCfgTable (INT4
                                              *pi4FutOspfv3RedistRouteDestType,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFutOspfv3RedistRouteDest,
                                              UINT4
                                              *pi4FutOspfv3RedistRoutePfxLength)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    gV3OsRtr.pRedistCfgTableCache = NULL;

    pRrdConfRtInfo = (tV3OsRedistrConfigRouteInfo *)
        TMO_SLL_First (&(pV3OspfCxt->redistrRouteConfigLst));
    if (pRrdConfRtInfo != NULL)
    {
        OSPFV3_IP6_ADDR_COPY (*pFutOspfv3RedistRouteDest->pu1_OctetList,
                              pRrdConfRtInfo->rrdConfigPrefix);
        pFutOspfv3RedistRouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
        *pi4FutOspfv3RedistRoutePfxLength = pRrdConfRtInfo->u1PrefixLength;
        *pi4FutOspfv3RedistRouteDestType = OSPFV3_INET_ADDR_TYPE;
        gV3OsRtr.pRedistCfgTableCache = pRrdConfRtInfo;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3RedistRouteCfgTable
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                nextFutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                nextFutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength
                nextFutOspfv3RedistRoutePfxLength
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3RedistRouteCfgTable (INT4
                                             i4FutOspfv3RedistRouteDestType,
                                             INT4
                                             *pi4NextFutOspfv3RedistRouteDestType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pFutOspfv3RedistRouteDest,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFutOspfv3RedistRouteDest,
                                             UINT4
                                             i4FutOspfv3RedistRoutePfxLength,
                                             UINT4
                                             *pi4NextFutOspfv3RedistRoutePfxLength)
{
    tV3OsRedistrConfigRouteInfo *pCurrRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType > OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if (gV3OsRtr.pRedistCfgTableCache != NULL)
    {
        gV3OsRtr.pRedistCfgTableCache =
            (tV3OsRedistrConfigRouteInfo *)
            TMO_SLL_Next (&(pV3OspfCxt->redistrRouteConfigLst),
                          &(gV3OsRtr.pRedistCfgTableCache->nextRrdConfigRoute));

        if (gV3OsRtr.pRedistCfgTableCache != NULL)
        {
            OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RedistRouteDest->pu1_OctetList,
                                  gV3OsRtr.pRedistCfgTableCache->
                                  rrdConfigPrefix);
            pNextFutOspfv3RedistRouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            *pi4NextFutOspfv3RedistRoutePfxLength =
                gV3OsRtr.pRedistCfgTableCache->u1PrefixLength;
            *pi4NextFutOspfv3RedistRouteDestType = OSPFV3_INET_ADDR_TYPE;
            return SNMP_SUCCESS;
        }
    }

    gV3OsRtr.pRedistCfgTableCache = NULL;

    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pCurrRrdConfRtInfo,
                  tV3OsRedistrConfigRouteInfo *)
    {
        if (V3UtilIp6AddrComp (&(pCurrRrdConfRtInfo->rrdConfigPrefix),
                               (tIp6Addr *) (VOID *)
                               pFutOspfv3RedistRouteDest->pu1_OctetList)
            == OSPFV3_LESS)
        {
            continue;
        }

        if ((V3UtilIp6AddrComp (&(pCurrRrdConfRtInfo->rrdConfigPrefix),
                                (tIp6Addr *) (VOID *)
                                pFutOspfv3RedistRouteDest->pu1_OctetList)
             == OSPFV3_GREATER)
            || (pCurrRrdConfRtInfo->u1PrefixLength >
                i4FutOspfv3RedistRoutePfxLength))
        {
            OSPFV3_IP6_ADDR_COPY (*pNextFutOspfv3RedistRouteDest->pu1_OctetList,
                                  pCurrRrdConfRtInfo->rrdConfigPrefix);
            pNextFutOspfv3RedistRouteDest->i4_Length = OSPFV3_IPV6_ADDR_LEN;
            *pi4NextFutOspfv3RedistRoutePfxLength =
                pCurrRrdConfRtInfo->u1PrefixLength;
            *pi4NextFutOspfv3RedistRouteDestType = OSPFV3_INET_ADDR_TYPE;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function        : V3GetFindRrdConfRtInfoInCxt                             */
/*                                                                           */
/* Description     : This procedure finds the matching entry from            */
/*                   RedistrRouteConfigLst SLL                               */
/*                                                                           */
/* Input           : pV3OspfCxt         - Context pointer                    */
/*                   pRrdDestIP6Addr    - Dest IP6 Address                   */
/*                   u1RrdDestPfxLen    - Prefix Length                      */
/*                                                                           */
/* Output          : None                                                    */
/*                                                                           */
/* Returns         : Pointer to the matching entry if present                */
/*                   NULL, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsRedistrConfigRouteInfo *
V3GetFindRrdConfRtInfoInCxt (tV3OspfCxt * pV3OspfCxt,
                             tIp6Addr * pRrdDestIP6Addr, UINT1 u1RrdDestPfxLen)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;

    /* XXX: Some clarification needed !!! */
    if (NULL != gV3OsRtr.pRedistCfgTableCache)
    {
        if ((OSPFV3_EQUAL == V3UtilIp6AddrComp (pRrdDestIP6Addr,
                                                &(gV3OsRtr.
                                                  pRedistCfgTableCache->
                                                  rrdConfigPrefix)))
            && (u1RrdDestPfxLen ==
                gV3OsRtr.pRedistCfgTableCache->u1PrefixLength))
        {
            return (gV3OsRtr.pRedistCfgTableCache);
        }

    }

    TMO_SLL_Scan (&(pV3OspfCxt->redistrRouteConfigLst), pRrdConfRtInfo,
                  tV3OsRedistrConfigRouteInfo *)
    {
        if ((V3UtilIp6AddrComp (pRrdDestIP6Addr,
                                &(pRrdConfRtInfo->rrdConfigPrefix)) ==
             OSPFV3_EQUAL)
            && (u1RrdDestPfxLen == pRrdConfRtInfo->u1PrefixLength))
        {
            return pRrdConfRtInfo;
        }
    }

    return NULL;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3RedistRouteMetric
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                retValFutOspfv3RedistRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RedistRouteMetric (INT4 i4FutOspfv3RedistRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3RedistRouteDest,
                                  UINT4 i4FutOspfv3RedistRoutePfxLength,
                                  INT4 *pi4RetValFutOspfv3RedistRouteMetric)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RedistRouteDest->pu1_OctetList,
                                      (UINT1) i4FutOspfv3RedistRoutePfxLength))
        != NULL)
    {
        *pi4RetValFutOspfv3RedistRouteMetric =
            (INT4) pRrdConfRtInfo->u4RrdRouteMetricValue;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RedistRouteMetricType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                retValFutOspfv3RedistRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RedistRouteMetricType (INT4 i4FutOspfv3RedistRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3RedistRouteDest,
                                      UINT4 i4FutOspfv3RedistRoutePfxLength,
                                      INT4
                                      *pi4RetValFutOspfv3RedistRouteMetricType)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RedistRouteDest->pu1_OctetList,
                                      (UINT1) i4FutOspfv3RedistRoutePfxLength))
        != NULL)
    {
        *pi4RetValFutOspfv3RedistRouteMetricType =
            (INT4) pRrdConfRtInfo->u1RrdRouteMetricType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RedistRouteTagType
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                retValFutOspfv3RedistRouteTagType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RedistRouteTagType (INT4 i4FutOspfv3RedistRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3RedistRouteDest,
                                   UINT4 i4FutOspfv3RedistRoutePfxLength,
                                   INT4 *pi4RetValFutOspfv3RedistRouteTagType)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RedistRouteDest->pu1_OctetList,
                                      (UINT1) i4FutOspfv3RedistRoutePfxLength))
        != NULL)
    {
        *pi4RetValFutOspfv3RedistRouteTagType =
            (INT4) pRrdConfRtInfo->u1RedistrTagType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RedistRouteTag
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                retValFutOspfv3RedistRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RedistRouteTag (INT4 i4FutOspfv3RedistRouteDestType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3RedistRouteDest,
                               UINT4 i4FutOspfv3RedistRoutePfxLength,
                               INT4 *pi4RetValFutOspfv3RedistRouteTag)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RedistRouteDest->pu1_OctetList,
                                      (UINT1) i4FutOspfv3RedistRoutePfxLength))
        != NULL)
    {
        *pi4RetValFutOspfv3RedistRouteTag =
            (INT4) pRrdConfRtInfo->u4ManualTagValue;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RedistRouteStatus
 Input       :  The Indices
                FutOspfv3RedistRouteDestType
                FutOspfv3RedistRouteDest
                FutOspfv3RedistRoutePfxLength

                The Object 
                retValFutOspfv3RedistRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RedistRouteStatus (INT4 i4FutOspfv3RedistRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3RedistRouteDest,
                                  UINT4 i4FutOspfv3RedistRoutePfxLength,
                                  INT4 *pi4RetValFutOspfv3RedistRouteStatus)
{
    tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3RedistRouteDestType != OSPFV3_INET_ADDR_TYPE)
    {
        return SNMP_FAILURE;
    }

    if ((pRrdConfRtInfo =
         V3GetFindRrdConfRtInfoInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                      pFutOspfv3RedistRouteDest->pu1_OctetList,
                                      (UINT1) i4FutOspfv3RedistRoutePfxLength))
        != NULL)
    {
        *pi4RetValFutOspfv3RedistRouteStatus =
            (INT4) pRrdConfRtInfo->rrdConfigRecStatus;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3RRDRouteMapName
 Input       :  The Indices

                The Object
                retValFutOspfv3RRDRouteMapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RRDRouteMapName (tSNMP_OCTET_STRING_TYPE
                                * pRetValFutOspfv3RRDRouteMapEnable)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFutOspfv3RRDRouteMapEnable->i4_Length =
        (INT4) STRLEN ((UINT1 *) pV3OspfCxt->au1RMapName);

    MEMCPY ((UINT1 *) pRetValFutOspfv3RRDRouteMapEnable->pu1_OctetList,
            (UINT1 *) pV3OspfCxt->au1RMapName,
            pRetValFutOspfv3RRDRouteMapEnable->i4_Length);
#else

    pRetValFutOspfv3RRDRouteMapEnable->i4_Length = 0;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RRDStatus
 Input       :  The Indices

                The Object 
                retValFutOspfv3RRDStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RRDStatus (INT4 *pi4RetValFutOspfv3RRDStatus)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3RRDStatus = (INT4) pV3OspfCxt->redistrAdmnStatus;
    return SNMP_SUCCESS;
#else

    *pi4RetValFutOspfv3RRDStatus = OSPFV3_DISABLED;
    return SNMP_SUCCESS;
#endif /* RRD_WANTED */
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RRDSrcProtoMask
 Input       :  The Indices

                The Object 
                retValFutOspfv3RRDSrcProtoMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RRDSrcProtoMask (INT4 *pi4RetValFutOspfv3RRDSrcProtoMask)
{
#ifdef RRD_WANTED
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3RRDSrcProtoMask =
        (INT4) pV3OspfCxt->u4RrdSrcProtoBitMask;
    return SNMP_SUCCESS;
#else
    *pi4RetValFutOspfv3RRDSrcProtoMask = 0;
    return SNMP_SUCCESS;
#endif /* RRD_WANTED */
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3RRDMetricValue
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object 
                retValFutOspfv3RRDMetricValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RRDMetricValue (INT4 i4FutOspfv3RRDProtocolId,
                               INT4 *pi4RetValFutOspfv3RRDMetricValue)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;
    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3RRDMetricValue =
        pOspfCxt->au4MetricValue[i4FutOspfv3RRDProtocolId - 1];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RRDMetricType
 Input       :  The Indices
                FutOspfv3RRDProtocolId

                The Object 
                retValFutOspfv3RRDMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RRDMetricType (INT4 i4FutOspfv3RRDProtocolId,
                              INT4 *pi4RetValFutOspfv3RRDMetricType)
{
    tV3OspfCxt         *pOspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pOspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3RRDMetricType =
        pOspfCxt->au4MetricType[i4FutOspfv3RRDProtocolId - 1];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RTStaggeringInterval
 Input       :  The Indices

                The Object
                retValFutOspfv3RTStaggeringInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RTStaggeringInterval (INT4
                                     *pi4RetValFutOspfv3RTStaggeringInterval)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3RTStaggeringInterval =
        (INT4) (pV3OspfCxt->u4RTStaggeringInterval);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RTStaggeringStatus
 Input       :  The Indices

                The Object
                retValFutOspfv3RTStaggeringStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RTStaggeringStatus (INT4 *pi4RetValFutOspfv3RTStaggeringStatus)
{

    *pi4RetValFutOspfv3RTStaggeringStatus =
        (INT4) gV3OsRtr.u4RTStaggeringStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3HotStandbyAdminStatus
 Input       :  The Indices

                The Object 
                retValFutOspfv3HotStandbyAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3HotStandbyAdminStatus (INT4
                                      *pi4RetValFutOspfv3HotStandbyAdminStatus)
{
    *pi4RetValFutOspfv3HotStandbyAdminStatus
        = gV3OsRtr.ospfRedInfo.i4HsAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3HotStandbyState
 Input       :  The Indices

                The Object 
                retValFutOspfv3HotStandbyState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3HotStandbyState (INT4 *pi4RetValFutOspfv3HotStandbyState)
{
    if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_INIT)
    {
        *pi4RetValFutOspfv3HotStandbyState = OSPFV3_HA_STATE_INIT;
    }
    else if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_ACTIVE_STANDBY_DOWN)
    {
        *pi4RetValFutOspfv3HotStandbyState = OSPFV3_HA_STATE_ACTIVE_STANDBYDOWN;
    }
    else if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_ACTIVE_STANDBY_UP)
    {
        *pi4RetValFutOspfv3HotStandbyState = OSPFV3_HA_STATE_ACTIVE_STANDBYUP;
    }
    else if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
    {
        *pi4RetValFutOspfv3HotStandbyState = OSPFV3_HA_STATE_STANDBY;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3DynamicBulkUpdStatus
 Input       :  The Indices

                The Object 
                retValFutOspfv3DynamicBulkUpdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3DynamicBulkUpdStatus (INT4
                                     *pi4RetValFutOspfv3DynamicBulkUpdStatus)
{
    if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
        OSPFV3_BULK_UPDT_NOT_STARTED)
    {
        *pi4RetValFutOspfv3DynamicBulkUpdStatus =
            OSPFV3_RED_DYN_BULK_NOT_STARTED;
    }
    else if ((gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus >
              OSPFV3_BULK_UPDT_NOT_STARTED) &&
             (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus <
              OSPFV3_RED_DYN_BULK_COMPLETED))
    {
        *pi4RetValFutOspfv3DynamicBulkUpdStatus =
            OSPFV3_RED_DYN_BULK_IN_PROGRESS;
    }
    else if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
             OSPFV3_BULK_UPDT_COMPLETED)
    {
        *pi4RetValFutOspfv3DynamicBulkUpdStatus = OSPFV3_RED_DYN_BULK_COMPLETED;
    }
    else if (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
             OSPFV3_BULK_UPDT_ABORTED)
    {
        *pi4RetValFutOspfv3DynamicBulkUpdStatus = OSPFV3_RED_DYN_BULK_ABORTED;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3StandbyHelloSyncCount
 Input       :  The Indices

                The Object 
                retValFutOspfv3StandbyHelloSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3StandbyHelloSyncCount (UINT4
                                      *pu4RetValFutOspfv3StandbyHelloSyncCount)
{
    *pu4RetValFutOspfv3StandbyHelloSyncCount =
        gV3OsRtr.ospfRedInfo.u4HelloSynCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3StandbyLsaSyncCount
 Input       :  The Indices

                The Object 
                retValFutOspfv3StandbyLsaSyncCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3StandbyLsaSyncCount (UINT4
                                    *pu4RetValFutOspfv3StandbyLsaSyncCount)
{
    *pu4RetValFutOspfv3StandbyLsaSyncCount =
        gV3OsRtr.ospfRedInfo.u4LsaSyncCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3BfdStatus
 Input       :  The Indices

                The Object
                retValFutOspfv3BfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BfdStatus (INT4 *pi4RetValFutOspfv3BfdStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3BfdStatus = (INT4) pV3OspfCxt->u1BfdAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3BfdAllIfState
 Input       :  The Indices

                The Object
                retValFutOspfv3BfdAllIfState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3BfdAllIfState (INT4 *pi4RetValFutOspfv3BfdAllIfState)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3BfdAllIfState = pV3OspfCxt->u1BfdStatusInAllIf;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3RouterIdPermanence
 Input       :  The Indices

                The Object
                retValFutOspfv3RouterIdPermanence
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3RouterIdPermanence (INT4 *pi4RetValFutOspfv3RouterIdPermanence)
{

    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFutOspfv3RouterIdPermanence = pV3OspfCxt->u1RouterIdStatus;
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3DistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pFutOspfv3DistInOutRouteMapName,
                                                         INT4
                                                         i4FutOspfv3DistInOutRouteMapType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pFutOspfv3DistInOutRouteMapName == NULL
        || pFutOspfv3DistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistanceFilterRMap,
             pFutOspfv3DistInOutRouteMapName) != 0)
        {
            i1RetVal = SNMP_FAILURE;
        }
    }
    else if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistributeInFilterRMap,
             pFutOspfv3DistInOutRouteMapName) != 0)
        {
            i1RetVal = SNMP_FAILURE;
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFutOspfv3DistInOutRouteMapName,
                                                 INT4
                                                 *pi4FutOspfv3DistInOutRouteMapType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tFilteringRMap     *pMinFilter = NULL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pV3OspfCxt != NULL)
    {
        pMinFilter =
            V3OspfGetMinFilterRMap (pV3OspfCxt->pDistanceFilterRMap,
                                    FILTERING_TYPE_DISTANCE,
                                    pV3OspfCxt->pDistributeInFilterRMap,
                                    FILTERING_TYPE_DISTRIB_IN);

        if (pMinFilter == NULL)
        {
            i1RetVal = SNMP_FAILURE;
        }

        if (i1RetVal == SNMP_SUCCESS)
        {
            pFutOspfv3DistInOutRouteMapName->i4_Length =
                (INT4) STRLEN (pMinFilter->au1DistInOutFilterRMapName);

            MEMCPY (pFutOspfv3DistInOutRouteMapName->pu1_OctetList,
                    pMinFilter->au1DistInOutFilterRMapName,
                    pFutOspfv3DistInOutRouteMapName->i4_Length);

            if (pMinFilter == pV3OspfCxt->pDistanceFilterRMap)
            {
                *pi4FutOspfv3DistInOutRouteMapType = FILTERING_TYPE_DISTANCE;
            }
            else
            {
                *pi4FutOspfv3DistInOutRouteMapType = FILTERING_TYPE_DISTRIB_IN;
            }
        }
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3DistInOutRouteMapTable
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                nextFutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType
                nextFutOspfv3DistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                                pFutOspfv3DistInOutRouteMapName,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextFutOspfv3DistInOutRouteMapName,
                                                INT4
                                                i4FutOspfv3DistInOutRouteMapType,
                                                INT4
                                                *pi4NextFutOspfv3DistInOutRouteMapType)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    tFilteringRMap     *pMinFilter = NULL;
    tFilteringRMap     *pRetFilter = NULL;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if ((pFutOspfv3DistInOutRouteMapName == NULL)
        || (pFutOspfv3DistInOutRouteMapName->i4_Length <= 0)
        || (pV3OspfCxt->pDistanceFilterRMap == NULL))
    {
        return SNMP_FAILURE;
    }

    pMinFilter =
        V3OspfGetMinFilterRMap (pV3OspfCxt->pDistanceFilterRMap,
                                FILTERING_TYPE_DISTANCE,
                                pV3OspfCxt->pDistributeInFilterRMap,
                                FILTERING_TYPE_DISTRIB_IN);

    if (pMinFilter == NULL)
    {
        i1RetVal = SNMP_FAILURE;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (pMinFilter == pV3OspfCxt->pDistanceFilterRMap
            && i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
        {
            pRetFilter = pV3OspfCxt->pDistributeInFilterRMap;
        }
        else if (pMinFilter == pV3OspfCxt->pDistributeInFilterRMap
                 && i4FutOspfv3DistInOutRouteMapType ==
                 FILTERING_TYPE_DISTRIB_IN)
        {
            pRetFilter = pV3OspfCxt->pDistanceFilterRMap;
        }

        if ((pRetFilter == NULL)
            || V3OspfCmpFilterRMapName
            (pMinFilter, pFutOspfv3DistInOutRouteMapName) != 0)
        {
            i1RetVal = SNMP_FAILURE;
        }
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        pNextFutOspfv3DistInOutRouteMapName->i4_Length =
            (INT4) STRLEN (pRetFilter->au1DistInOutFilterRMapName);

        MEMCPY (pNextFutOspfv3DistInOutRouteMapName->pu1_OctetList,
                pRetFilter->au1DistInOutFilterRMapName,
                pNextFutOspfv3DistInOutRouteMapName->i4_Length);

        if (pRetFilter == pV3OspfCxt->pDistanceFilterRMap)
        {
            *pi4NextFutOspfv3DistInOutRouteMapType = FILTERING_TYPE_DISTANCE;
        }
        else
        {
            *pi4NextFutOspfv3DistInOutRouteMapType = FILTERING_TYPE_DISTRIB_IN;
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3DistInOutRouteMapValue
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                retValFutOspfv3DistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3DistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3DistInOutRouteMapName,
                                       INT4 i4FutOspfv3DistInOutRouteMapType,
                                       INT4
                                       *pi4RetValFutOspfv3DistInOutRouteMapValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((pFutOspfv3DistInOutRouteMapName == NULL)
        || (pFutOspfv3DistInOutRouteMapName->i4_Length <= 0))
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE
        &&
        (V3OspfCmpFilterRMapName (pV3OspfCxt->pDistanceFilterRMap,
                                  pFutOspfv3DistInOutRouteMapName) == 0))
    {
        *pi4RetValFutOspfv3DistInOutRouteMapValue =
            pV3OspfCxt->pDistanceFilterRMap->u1RMapDistance;
    }
    else
    {
        *pi4RetValFutOspfv3DistInOutRouteMapValue = 0;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3DistInOutRouteMapRowStatus
 Input       :  The Indices
                FutOspfv3DistInOutRouteMapName
                FutOspfv3DistInOutRouteMapType

                The Object
                retValFutOspfv3DistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3DistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                           pFutOspfv3DistInOutRouteMapName,
                                           INT4
                                           i4FutOspfv3DistInOutRouteMapType,
                                           INT4
                                           *pi4RetValFutOspfv3DistInOutRouteMapRowStatus)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    INT1                i1RetVal = SNMP_SUCCESS;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pFutOspfv3DistInOutRouteMapName == NULL
        || pFutOspfv3DistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }

    if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistanceFilterRMap,
             pFutOspfv3DistInOutRouteMapName) != 0)
        {
            i1RetVal = SNMP_FAILURE;
        }
    }
    else if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (V3OspfCmpFilterRMapName
            (pV3OspfCxt->pDistributeInFilterRMap,
             pFutOspfv3DistInOutRouteMapName) != 0)
        {
            i1RetVal = SNMP_FAILURE;
        }
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
        {
            *pi4RetValFutOspfv3DistInOutRouteMapRowStatus =
                pV3OspfCxt->pDistanceFilterRMap->u1RowStatus;
        }
        else if (i4FutOspfv3DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
        {
            *pi4RetValFutOspfv3DistInOutRouteMapRowStatus =
                pV3OspfCxt->pDistributeInFilterRMap->u1RowStatus;
        }
    }
    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetFutOspf3PreferenceValue
 Input       :  The Indices

                The Object 
                retValFutOspf3PreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspf3PreferenceValue (INT4 *pi4RetValFutOspf3PreferenceValue)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pV3OspfCxt->u1Distance == 0)
    {
        pV3OspfCxt->u1Distance = IP6_PREFERENCE_OSPF;
    }

    *pi4RetValFutOspf3PreferenceValue = pV3OspfCxt->u1Distance;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  V3GetFindIfAuthkeyInfoInCxt
 Input       :  The Indices
                pOspfCxt
                u4IfAuthIfIndex
                u4AuthKeyId
 Output      :  The Get  function gets interface authentication info for
                the given ip address , addressless & authkey_id  Values.
 Returns     :  pointer to the  authkey_info on success
                NULL  otherwise
****************************************************************************/
tAuthkeyInfo       *
V3GetFindIfAuthkeyInfoInCxt (UINT4 u4IfAuthIfIndex, UINT2 u2AuthKeyId)
{
    tTMO_SLL_NODE      *pLstNode;
    tAuthkeyInfo       *pAuthkeyInfo;
    tV3OsInterface     *pInterface;

    if ((pInterface = V3GetFindIf (u4IfAuthIfIndex)) != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u2AuthkeyId == u2AuthKeyId)
                return (pAuthkeyInfo);
            else if (pAuthkeyInfo->u2AuthkeyId > u2AuthKeyId)
                return (NULL);
        }
    }
    return NULL;
}

/****************************************************************************
 Function    :  V3GetFindVirtIfAuthkeyInfoInCxt
 Input       :  The Indices
                pOspfCxt
                pTransitAreaId
                pNbrId
                u2AuthkeyId
 Output      :  The Get  function gets virtual interface authentication info for
                the given transit area Id , Nbr Id & authkey_id  Values.
 Returns     :  pointer to the  authkey_info on success
                NULL  otherwise
****************************************************************************/
tAuthkeyInfo       *
V3GetFindVirtIfAuthkeyInfoInCxt (tV3OspfCxt * pOspfCxt,
                                 tV3OsAreaId * pTransitAreaId,
                                 tV3OsRouterId * pNbrId, UINT2 u2AuthkeyId)
{
    tV3OsInterface     *pInterface;
    tTMO_SLL_NODE      *pLstNode;
    tAuthkeyInfo       *pAuthkeyInfo;

    if ((pInterface = V3GetFindVirtIfInCxt (pOspfCxt, pTransitAreaId, pNbrId))
        != NULL)
    {
        TMO_SLL_Scan (&(pInterface->sortAuthkeyLst), pLstNode, tTMO_SLL_NODE *)
        {
            pAuthkeyInfo = (tAuthkeyInfo *) pLstNode;
            if (pAuthkeyInfo->u2AuthkeyId == u2AuthkeyId)
                return (pAuthkeyInfo);
            else if (pAuthkeyInfo->u2AuthkeyId > u2AuthkeyId)
                return (NULL);
        }
    }
    return NULL;

}

/****************************************************************************
 Function    :  nmhGetOspfv3AreaDfInfOriginate
 Input       :  The Indices
                Ospfv3AreaId

                The Object
                retValOspfv3AreaDfInfOriginate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetOspfv3AreaDfInfOriginate (UINT4 u4Ospfv3AreaId,
                                INT4 *pi4RetValOspfv3AreaDfInfOriginate)
{
    tV3OsAreaId         areaId;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_BUFFER_DWTOPDU (areaId, u4Ospfv3AreaId);

    if ((pArea = V3GetFindAreaInCxt (pV3OspfCxt, &areaId)) != NULL)
    {
        *pi4RetValOspfv3AreaDfInfOriginate = (INT4) pArea->u4DfInfOriginate;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3RRDMetricTable
 Input       :  The Indices
                FutOspfv3RRDProtocolId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3RRDMetricTable (INT4 *pi4FutOspfv3RRDProtocolId)
{
    *pi4FutOspfv3RRDProtocolId = OSPFV3_REDISTRUTE_BGP;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3RRDMetricTable
 Input       :  The Indices
                FutOspfv3RRDProtocolId
                nextFutOspfv3RRDProtocolId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3RRDMetricTable (INT4 i4FutOspfv3RRDProtocolId,
                                        INT4 *pi4NextFutOspfv3RRDProtocolId)
{
    if (i4FutOspfv3RRDProtocolId < OSPFV3_MAX_PROTO_REDISTRUTE_SIZE)
    {
        *pi4NextFutOspfv3RRDProtocolId = i4FutOspfv3RRDProtocolId + 1;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3ClearProcess
 Input       :  The Indices

                The Object
                retValFutOspfv3ClearProcess
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ClearProcess (INT4 *pi4RetValFutOspfv3ClearProcess)
{
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFutOspfv3ClearProcess = (INT4) gu4ClearFlag;
    return SNMP_SUCCESS;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3fetch.c                      */
/*-----------------------------------------------------------------------*/
