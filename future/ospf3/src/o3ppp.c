/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3ppp.c,v 1.12 2017/12/26 13:34:28 siva Exp $
 *
 * Description: This file contains procedures used for transmission
 *              and reception of protocol packets to/from IP.
 *******************************************************************/
#include "o3inc.h"
#include "fsos3lw.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : V3PppSendPkt                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 8.1                           */
/*                This function is called whenever any of the protocol       */
/*                packets is to be sent. The Hello, DDP, LRQ, LSU, LAK       */
/*                module call this function to send packets.                 */
/*                                                                           */
/* Input        : pPkt          -   The pointer to OSPFv3 packet that is     */
/*                                  to be transmitted.                       */
/*                u2PktLen      -   The length of the packet.                */
/*                pInterface    -   The interface on which the packet is     */
/*                                  to be sent.                              */
/*                pDestIp6Addr  -   The destination address to be used       */
/*                                  while transmission. This is either       */
/*                                  ALL_SPF_ROUTERS or ALL_D_ROUTERS or      */
/*                                  the IPv6 address of the neighbor.        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3PppSendPkt (UINT1 *pPkt, UINT2 u2PktLen,
              tV3OsInterface * pInterface, tIp6Addr * pDestIp6Addr)
{

    UINT1               u1Type;
    UINT1               u1HopLimit = 0;
    tIp6Addr            srcIp6Addr;
    tIp6Addr            dstIp6Addr;
    UINT2               u2CheckSum = 0;
    UINT4               u4OutIfIndex = 0;
    UINT1              *pOspfPkt = NULL;
    UINT2               u2ATLen = 0;
    UINT1               u2ATStat = OSPFV3_SUCCESS;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3PppSendPkt\n");

    if (pInterface->u1NetworkType >= OSPFV3_MAX_IF_TYPE)
    {
        return;
    }

    /* Calculate the OSPFv3 Pkt Pointer */
    pOspfPkt = pPkt + IPV6_HEADER_LEN;
    OSPFV3_TRC4 (OSPFV3_PPP_TRC | CONTROL_PLANE_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "Packet to be txed on interface If_Index: %d"
                 " Addr: %s  Network Type: %s"
                 " Len: %d\n",
                 pInterface->u4InterfaceId,
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 gau1Os3DbgIfType[pInterface->u1NetworkType], u2PktLen);

    if ((pInterface->operStatus == OSPFV3_DISABLED) ||
        (pInterface->admnStatus == OSPFV3_DISABLED) ||
        (pInterface->bPassive == OSIX_TRUE))
    {

        /* If the oper status or the admin status is disabled then the
           buffer is released */
        return;
    }

    OSPFV3_BUFFER_GET_1_BYTE (pOspfPkt, OSPFV3_TYPE_OFFSET, u1Type);

    /* Setting Source IPv6 Addr */
    OSPFV3_IP6_ADDR_COPY (srcIp6Addr, pInterface->ifIp6Addr);

    /* Setting Destination IPv6 Addr */
    if (pInterface->u1NetworkType == OSPFV3_IF_PTOP)
    {
        OSPFV3_IP6_ADDR_COPY (dstIp6Addr, gV3OsAllSpfRtrs);
    }
    else
    {
        OSPFV3_IP6_ADDR_COPY (dstIp6Addr, *pDestIp6Addr);
    }

    /* Checking NULL source and destination Addr */
    if ((V3UtilIp6AddrComp (&srcIp6Addr, &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
        || (V3UtilIp6AddrComp (&dstIp6Addr, &gV3OsNullIp6Addr) == OSPFV3_EQUAL))
    {
        return;
    }

    u2ATStat =
        V3AuthSendPkt (pOspfPkt, u2PktLen, pInterface, dstIp6Addr, &u2ATLen);
    if (u2ATStat != OSPFV3_SUCCESS)
    {
        return;
    }

    /* Calculate ospfv3 Checksum */
    if (u2ATLen == 0)
    {
        u2CheckSum =
            V3UtilIp6Chksum (pOspfPkt, u2PktLen, &srcIp6Addr, &dstIp6Addr);

        OSPFV3_BUFFER_ASSIGN_2_BYTE (pOspfPkt, OSPFV3_CHKSUM_OFFSET,
                                     u2CheckSum);
    }

    if ((u1Type >= OSPFV3_HELLO_PKT) && (u1Type <= OSPFV3_LSA_ACK_PKT))
    {
        OSPFV3_TRC1 (CONTROL_PLANE_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Txmitting %s\n", gau1Os3DbgPktType[u1Type]);
    }

    OSPFV3_OUT_PKT_DUMP
        (pInterface->pArea->pV3OspfCxt->u4ContextId, pOspfPkt, u2PktLen);
    /* Dump the Outgoing Packet */

    /* Calculate the output ifIndex */
    if (OSPFV3_IS_VIRTUAL_IFACE (pInterface))
    {
        u4OutIfIndex = pInterface->u4OutIfIndex;
        u1HopLimit = IP6_DEF_HOP_LIMIT;
    }
    else
    {
        u4OutIfIndex = pInterface->u4InterfaceId;
        u1HopLimit = 1;
    }

    if (u1Type == OSPFV3_LSA_ACK_PKT || u1Type == OSPFV3_LS_UPDATE_PKT)
    {
        gV3OsRtr.u4AckOrUpdatePktSentCt++;

        if (gV3OsRtr.u4AckOrUpdatePktSentCt >= MAX_OSPF3_MSG_PROCESSED)
        {
            V3OspfCpuRelinquishHello ();
            gV3OsRtr.u4AckOrUpdatePktSentCt = 0;
        }
    }

    if (V3OspfSendPkt
        (pInterface->pArea->pV3OspfCxt->u4ContextId, pPkt,
         (UINT4) (u2PktLen + u2ATLen), u4OutIfIndex, &srcIp6Addr, &dstIp6Addr,
         u1HopLimit) == OSIX_SUCCESS)
    {

        switch (u1Type)
        {
            case OSPFV3_HELLO_PKT:
                OSPFV3_COUNTER_OP (pInterface->u4HelloTxedCount, 1);
                break;

            case OSPFV3_DD_PKT:
                OSPFV3_COUNTER_OP (pInterface->u4DdpTxedCount, 1);
                break;

            case OSPFV3_LSA_REQ_PKT:
                OSPFV3_COUNTER_OP (pInterface->u4LsaReqTxedCount, 1);
                break;

            case OSPFV3_LSA_ACK_PKT:
                OSPFV3_COUNTER_OP (pInterface->u4LsaAckTxedCount, 1);
                break;

            case OSPFV3_LS_UPDATE_PKT:
                OSPFV3_COUNTER_OP (pInterface->u4LsaUpdateTxedCount, 1);
                break;

            default:
                break;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3PppSendPkt\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3PppRcvPkt                                                */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 8.2                           */
/*                The received packet is associated with an interface. Then  */
/*                the OSPFv3 header fields are verified, If the packet type  */
/*                is hello, it is immediately handed over to the HP          */
/*                Submodule, otherwise it is associated with a neighbor and  */
/*                then handed over to the appropriate module for further     */
/*                processing.                                                */
/*                                                                           */
/* Input        : pPkt           -   Pointer to the OSPFv3 packet received   */
/*                                   from IPv6.                              */
/*                u2Len          -   The length of the packet.               */
/*                u4IfIndex      -   The incoming interface Index            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3PppRcvPkt (UINT1 *pPkt, UINT2 u2Len, UINT4 u4IfIndex, tIp6Addr * pSrcIp6Addr,
             tIp6Addr * pDestIp6Addr)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tV3OspfLPQMsg      *pOspfLPQMsg = NULL;
    UINT1               u1Type = 0;
    UINT1               u1Version = 0;
    tV3OsRouterId       headerRtrId;
    tV3OsAreaId         areaId;
    UINT1               u1AcptPktFlag = OSIX_FALSE;
    tV3OsRouterId      *pNbrSearchField = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pAssoIface = NULL;
    UINT2               u2TmpChecksum = 0;
    UINT2               u2TmpLen = 0;
    UINT1               u1InstId = 0;
    tV3OsInterface     *pInterface = NULL;
    INT4                i4ATInfo = OSPFV3_AT_SUCCESS;
    INT4                i4TraceVal = OSPFV3_INIT_VAL;
    UINT4               u4PktCount = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3PppRcvPkt\n");

    OSPFV3_GBL_TRC2 (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                     "Pkt Rcvd IfIndx: %d Len: %d\n", u4IfIndex, u2Len);
    nmhGetFutOspfv3TraceLevel (&i4TraceVal);

    if ((pInterface = V3GetFindIf (u4IfIndex)) == NULL)
    {

        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_INTERFACE_TRC,
                        "OSPFv3 Pkt Disc Associated If Not Found\n");
        return;
    }

    /* If the interface is Passive interface 
     * No need to process any OSPF protocol packet */

    if (pInterface->bPassive == OSIX_TRUE)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                        "Pkt Disc: Dropping Packets received on"
                        " Passive Interface\n ");

        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
        return;

    }

    OSPFV3_BUFFER_GET_1_BYTE (pPkt, OSPFV3_TYPE_OFFSET, u1Type);

    /* check protocol admin bStatus */
    if (pInterface->pArea->pV3OspfCxt->admnStat != OSPFV3_ENABLED)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                        "Pkt Disc (Proto Not Enabled)\n");

        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
        return;
    }

    OSPFV3_INC_PKT_DUMP
        (pInterface->pArea->pV3OspfCxt->u4ContextId, pPkt, u2Len);
    /* Dump the incoming packet */

    /* check state of interface */
    if (pInterface->u1IsmState == OSPFV3_IFS_STANDBY)
    {
        if (u1Type != OSPFV3_HELLO_PKT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (Standby Inf - packet type is non-hello\n");
            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
            return;
        }
    }
    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pInterface) &&
        (pInterface->u1IsmState != OSPFV3_IFS_LOOP_BACK))
    {
        u1AcptPktFlag = OSIX_TRUE;
    }

    if (u1AcptPktFlag == OSIX_FALSE)
    {
        if ((pInterface->u1IsmState == OSPFV3_IFS_DOWN) ||
            (pInterface->u1IsmState == OSPFV3_IFS_LOOP_BACK))
        {

            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (If Not UP)\n");

            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);

            return;
        }
    }

    /* Validate version number field in OSPF header */
    OSPFV3_BUFFER_GET_1_BYTE (pPkt, OSPFV3_VERSION_NO_OFFSET, u1Version);
    OSPFV3_BUFFER_GET_STRING (pPkt, headerRtrId, OSPFV3_RTR_ID_OFFSET,
                              OSPFV3_RTR_ID_LEN);
    OSPFV3_BUFFER_GET_STRING (pPkt, areaId, OSPFV3_AREA_ID_OFFSET,
                              OSPFV3_AREA_ID_LEN);

    if (u1Version != OSPFV3_VERSION_NO)
    {

        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);

        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                        "Pkt Disc (Ver No. Mismatch)\n");

        return;
    }

    /* Rtr-Id of rcvd pkt is zero */
    if (OSPFV3_BUFFER_DWFROMPDU (headerRtrId) == 0)
    {
        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);

        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                        "Pkt Disc (Router ID zero)\n");

        return;
    }

    /* Rcvd Pkt router id is same with this router */
    if (V3UtilRtrIdComp (headerRtrId,
                         pInterface->pArea->pV3OspfCxt->rtrId) == OSPFV3_EQUAL)
    {
        if ((u1Type != OSPFV3_HELLO_PKT)
            && (pInterface->u1NetworkType != OSPFV3_IF_BROADCAST))
        {
            /* Hello packets with same router id will be processed only if the
             * network is Broadcasd */
            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
            if (((i4TraceVal & CONTROL_PLANE_TRC) != 0) ||
                ((i4TraceVal & OSPFV3_PPP_TRC) != 0) ||
                ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Pkt Disc (Router ID same)"));

            }

            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (Router ID same)\n");
            return;
        }
    }

    /* Instance Id Checking
     * The instance id present in OSPF header should be equal to instance
     * id configured in the interface (RFC 2740 Sec 3.2.2)
     */
    OSPFV3_BUFFER_GET_1_BYTE (pPkt, OSPFV3_INSTANCE_OFFSET, u1InstId);
    if (u1InstId != pInterface->u2InstId)
    {
        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);

        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                        "Pkt Disc (Instance Id Mismatch)\n");

        return;
    }

    /* Validate area id field in OSPF header */
    if (V3UtilAreaIdComp (areaId, pInterface->pArea->areaId) != OSPFV3_EQUAL)
    {
        if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
        {
            /* associate with virtual interface */
            if ((pAssoIface = V3PppAssociateWithVirtualIf (pInterface,
                                                           &headerRtrId)) ==
                NULL)
            {
                OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
                if (((i4TraceVal & CONTROL_PLANE_TRC) != 0) ||
                    ((i4TraceVal & OSPFV3_PPP_TRC) != 0) ||
                    ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
                {
                    SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Pkt Disc (Association With Virt If Fails)"));

                }

                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                                "Pkt Disc (Association With Virt If Fails)\n");

                return;
            }

            pInterface = pAssoIface;
        }
        else
        {
            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
            if (((i4TraceVal & CONTROL_PLANE_TRC) != 0) ||
                ((i4TraceVal & OSPFV3_PPP_TRC) != 0) ||
                ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "Pkt Disc (AreaID MisMatch)"));

            }
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (AreaID MisMatch)\n");
            return;
        }
    }

    /* Check destination address */
    if ((V3UtilIp6AddrComp (pDestIp6Addr, &gV3OsAllDRtrs) == OSPFV3_EQUAL) &&
        (pInterface->u1IsmState != OSPFV3_IFS_DR) &&
        (pInterface->u1IsmState != OSPFV3_IFS_BACKUP))
    {
        OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
        if (((i4TraceVal & CONTROL_PLANE_TRC) != 0) ||
            ((i4TraceVal & OSPFV3_PPP_TRC) != 0) ||
            ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Pkt Disc (Dest Addr MisMatch)"));

        }
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                        "Pkt Disc (Dest Addr MisMatch)\n");
        return;
    }

    pNbrSearchField = &headerRtrId;
    pNbr = V3HpSearchNbrLst (pNbrSearchField, OSPFV3_ZERO, pInterface);
    if (u1Type != OSPFV3_HELLO_PKT)
    {
        if (pNbr == NULL)
        {
            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);

            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (Could Not be Associated With Any Nbr)\n");

            return;
        }
    }

    i4ATInfo =
        V3AuthRcvPkt (pPkt, u2Len, pInterface, pNbr, pSrcIp6Addr, u1Type);
    switch (i4ATInfo)
    {
        case OSPFV3_AT_NO_SUP:
        case OSPFV3_AT_SKIP:
            /* Check Checksum */
            OSPFV3_BUFFER_EXTRACT_2_BYTE (pPkt, OSPFV3_CHKSUM_OFFSET,
                                          u2TmpChecksum);
            if (i4ATInfo == OSPFV3_AT_NO_SUP)
            {
                OSPFV3_BUFFER_EXTRACT_2_BYTE (pPkt, OSPFV3_PKT_LENGTH_OFFSET,
                                              u2TmpLen);

                u2Len = (UINT2) u2TmpLen;
            }
            if (V3UtilIp6Chksum (pPkt, u2Len, pSrcIp6Addr, pDestIp6Addr) !=
                u2TmpChecksum)
            {
                OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                                "Rcvd Bad Pkt - Checksum Failure \n");
                return;
            }
            else
            {
                OSPFV3_BUFFER_ASSIGN_2_BYTE (pPkt, OSPFV3_CHKSUM_OFFSET,
                                             u2TmpChecksum);
            }
            break;
        case OSPFV3_AT_DROP:
            OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc Authentication failure\n");
            return;
        default:
            u2Len = (UINT2) (u2Len - i4ATInfo);
            break;
    }

    /* if packet type is hello invoke HpRcvHello procedure */
    if (u1Type == OSPFV3_HELLO_PKT)
    {
        OSPFV3_COUNTER_OP (pInterface->u4HelloRcvdCount, 1);
        V3HpRcvHello (pPkt, u2Len, pInterface, pSrcIp6Addr);
    }
    else
    {

        if (V3OspfIsLowPriorityOspfPkt (u1Type) == OSPFV3_SUCCESS)
        {
            if (OSPFV3_LPQMSG_ALLOC (&(pOspfLPQMsg)) != NULL)
            {
                pOspfLPQMsg->u4OspfMsgType = OSPFV3_PKT_ARRIVAL_EVENT;
                pOspfLPQMsg->unOspfMsgType.ospfIpIfParam.u4IfIndex = u4IfIndex;
                pOspfLPQMsg->unOspfMsgType.ospfIpIfParam.u4PktLen =
                    (UINT4) u2Len;
                pOspfLPQMsg->u4OspfCxtId =
                    pInterface->pArea->pV3OspfCxt->u4ContextId;
                pBuf = CRU_BUF_Allocate_MsgBufChain (u2Len, 0);
                if (pBuf == NULL)
                {
                    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                    "CRU_BUF_Allocate_MsgBufChain Failed\n");
                    OSPFV3_LPQMSG_FREE (pOspfLPQMsg);
                    return;
                }
                CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) (VOID *) (pPkt),
                                           0, u2Len);
                pOspfLPQMsg->unOspfMsgType.ospfIpIfParam.pChainBuf = pBuf;
                pOspfLPQMsg->unOspfMsgType.ospfIpIfParam.pNbr = pNbr;

                if (OsixQueSend (gV3OsRtr.Ospf3LPQId,
                                 (UINT1 *) (VOID *) &pOspfLPQMsg,
                                 OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    OSPFV3_LPQMSG_FREE (pOspfLPQMsg);
                    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                    "OSPFV3_MSGQ_EVENT Send to queue Failed\n");
                    return;
                }
                OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4PktCount);
                /* Self MSGQ Event is posted for processing of LowPrioMsgs
                 * in Main ReceiveEvent loop */
                if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT)
                    != OSIX_SUCCESS)
                {
                    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                    "OSPFV3_MSGQ_EVENT Send Evt Failed\n");
                }
                /* To support from system.size, the macro OSPF_LOW_PRIORITY_QUEUE_SIZE
                 * is replaced with sizing variable
                 * FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits/2
                 */
                if (u4PktCount ==
                    (FsOSPFV3SizingParams[MAX_OSPFV3_LP_QUEUE_SIZE_SIZING_ID].
                     u4PreAllocatedUnits / 5))
                {
                    V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
                }

                return;

            }
        }

        switch (u1Type)
        {
            case OSPFV3_DD_PKT:
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received DDP\n");
                OSPFV3_COUNTER_OP (pNbr->pInterface->u4DdpRcvdCount, 1);
                V3DdpRcvDdp (pPkt, u2Len, pNbr);
                break;

            case OSPFV3_LSA_REQ_PKT:
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LSR\n");
                OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaReqRcvdCount, 1);
                V3LrqRcvLsaReq (pPkt, u2Len, pNbr);
                break;

            case OSPFV3_LS_UPDATE_PKT:
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LSU\n");
                OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaUpdateRcvdCount, 1);
                V3LsuRcvLsUpdate (pPkt, pNbr);
                break;

            case OSPFV3_LSA_ACK_PKT:
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LAK\n");
                OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaAckRcvdCount, 1);
                V3LakRcvLsAck (pPkt, u2Len, pNbr);
                break;

            default:
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Rcvd Junk\n");
                OSPFV3_INC_DISCARD_OSPF_PKT (pInterface->pArea->pV3OspfCxt);
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                                "Pkt Disc (Invalid Pkt Type)\n");
                return;
        }
    }

    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Pkt Processing Over\n");
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3PppRcvPkt\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3PppAssociateWithVirtualIf                                */
/*                                                                           */
/* Description  : This procedure associates the received packet with a       */
/*                virtual interface based on the received interface and the  */
/*                router ID of the source of the packet.                     */
/*                                                                           */
/* Input        : pInterface       : the receiving interface                 */
/*                pRtrId           : the source router id                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to associated interface, if successfull            */
/*                FAILURE, otherwise                                         */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsInterface *
V3PppAssociateWithVirtualIf (tV3OsInterface * pInterface,
                             tV3OsRouterId * pRtrId)
{

    tV3OsInterface     *pLstIf = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    INT4                i4TraceVal = OSPFV3_INIT_VAL;

    nmhGetFutOspfv3TraceLevel (&i4TraceVal);
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3PppAssociateWithVirtualIf\n");

    if ((pInterface->pArea->pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE) ||
        (pInterface->pArea->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR))
    {
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pLstIf =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

            if ((V3UtilRtrIdComp (pLstIf->destRtrId, *pRtrId) ==
                 OSPFV3_EQUAL) &&
                (V3UtilAreaIdComp (pInterface->pArea->areaId,
                                   pLstIf->transitAreaId) == OSPFV3_EQUAL))
            {
                return pLstIf;
            }
        }
    }

    OSPFV3_TRC (CONTROL_PLANE_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Cannot Associate With Virt Nbr\n");

    if (((i4TraceVal & CONTROL_PLANE_TRC) != 0) ||
        ((i4TraceVal & OSPFV3_PPP_TRC) != 0) ||
        ((i4TraceVal & OSPFV3_ADJACENCY_TRC) != 0))
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot Associate With Virt Nbr"));

    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3PppAssociateWithVirtualIf\n");
    return (NULL);
}

/*---------------------------------------------------------------------------*/
/*                         End of file o3ppp.c                               */
/*---------------------------------------------------------------------------*/
