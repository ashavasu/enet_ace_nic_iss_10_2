/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agsumm.c,v 1.8 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to aggregation of
 *         Type-3 address range. 
 *
 *******************************************************************/
#include "o3inc.h"

/************************************************************************/
/*                                                                      */
/* Function     : V3RagFlushLsaFallingInAddrRange                       */
/*                                                                      */
/* Description  : This Function flushes the indvidual Network Entry     */
 /*               if they falls in Configured Address range             */
/*                                                                      */
/* Input        : pArea            :  Pointer to the AREA.              */
/*                pInternalRng     :  Pointer to Internal Address Range */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagFlushLsaFallingInAddrRange (tV3OsArea * pArea,
                                 tV3OsAddrRange * pInternalRng)
{
    tV3OsArea          *pLstArea = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tInputParams        inParams;
    VOID               *pLeafNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    tIp6Addr           *pIp6Addr = NULL;
    UINT1               u1PrefixLength = 0;

    VOID               *pTempPtr = NULL;

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        return;
    }

    pIp6Addr = &(pInternalRng->ip6Addr);
    u1PrefixLength = pInternalRng->u1PrefixLength;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlushLsaFallingInAddrRange\n");

    inParams.pRoot = pArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        pPath = OSPFV3_GET_PATH (pRtEntry);
        if ((pPath != NULL) && (V3UtilAreaIdComp (pPath->areaId, pArea->areaId)
                                == OSPFV3_EQUAL)
            && (OSPFV3_GET_PATH_TYPE (pRtEntry) == OSPFV3_INTRA_AREA))
        {
            TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pLstArea, tV3OsArea *)
            {
                if ((V3UtilAreaIdComp (pArea->areaId,
                                       pLstArea->areaId) != OSPFV3_EQUAL) &&
                    (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                          pIp6Addr,
                                          u1PrefixLength) == OSPFV3_EQUAL))
                {
                    if ((pLstArea->u4AreaType == OSPFV3_NORMAL_AREA) ||
                        ((pLstArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
                         (pLstArea->u1SummaryFunctionality
                          == OSPFV3_SEND_AREA_SUMMARY)))
                    {
                        if ((OSPFV3_IS_AREA_TRANSIT_CAPABLE (pLstArea)) &&
                            (V3UtilAreaIdComp
                             (pArea->areaId,
                              OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL))
                        {
                            /* Backbone Network Entry is not summarized in transit
                               area */
                            continue;
                        }
                        V3RtcFlushOutSummaryLsa (pLstArea,
                                                 OSPFV3_INTER_AREA_PREFIX_LSA,
                                                 &(pRtEntry->linkStateId));
                    }
                }
            }
        }                        /* End TMO_SLL_Scan of pArea */
        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
        au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlushLsaFallingInAddrRange\n");
    return;
}

/************************************************************************/
/* Function     : V3RagTransAreaFlushLsa                                */
/*                                                                      */
/* Description  : This Function flushes the indvidual Network Entry     */
/*                if they falls in Configured Address range.This        */
/*                function is called when the Transit Capability of     */
/*                Area is Disabled (Virtual Link is down)               */
/*                                                                      */
/* Input        : pLstArea     :  pointer to the AREA.                  */
/*                                                                      */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/************************************************************************/
PUBLIC VOID
V3RagTransAreaFlushLsa (tV3OsArea * pLstArea)
{
    tIp6Addr           *pIp6Addr = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tInputParams        inParams;
    VOID               *pLeafNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];
    UINT1               u1PrefixLen = 0;

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLstArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagTransAreaFlushLsa\n");

    /* Scan the Routing table */
    inParams.pRoot = pLstArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pLstArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        if ((V3UtilAreaIdComp (OSPFV3_GET_PATH_AREA_ID (pRtEntry),
                               OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL) &&
            (OSPFV3_IS_INTRA_AREA_PATH (pRtEntry)))
        {
            TMO_SLL_Scan (&(pLstArea->pV3OspfCxt->pBackbone->type3AggrLst),
                          pInternalRng, tV3OsAddrRange *)
            {
                pIp6Addr = &(pInternalRng->ip6Addr);
                u1PrefixLen = pInternalRng->u1PrefixLength;

                if (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                         pIp6Addr, u1PrefixLen) == OSPFV3_EQUAL)

                {
                    V3RtcFlushOutSummaryLsa (pLstArea,
                                             OSPFV3_INTER_AREA_PREFIX_LSA,
                                             &(pRtEntry->linkStateId));
                }
            }
        }

        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
        au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = au1Key;

    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    TMO_SLL_Scan (&(pLstArea->pV3OspfCxt->pBackbone->type3AggrLst),
                  pInternalRng, tV3OsAddrRange *)
    {
        pIp6Addr = &(pInternalRng->ip6Addr);
        u1PrefixLen = pInternalRng->u1PrefixLength;

        if (pInternalRng->u1AggrEffect != OSPFV3_DO_NOT_ADVERTISE_MATCHING)
        {
            V3GenerateLsa (pLstArea,
                           OSPFV3_COND_INTER_AREA_PREFIX_LSA,
                           &(pInternalRng->linkStateId),
                           (UINT1 *) pInternalRng, OSIX_FALSE);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLstArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagTransAreaFlushLsa\n");
    return;
}

/************************************************************************/
/* Function     : V3RagFlushAggLsaInTransitArea                         */
/*                                                                      */
/* Description  : This Function is called when the attached area has    */
/*                become Transit Area. In that case all the Aggregated  */
/*                LSA is flushed out and individual LSAs are generated  */
/*                                                                      */
/* Input        : pTranistArea    :  Pointer to the Tranist AREA.       */
/*                                                                      */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/************************************************************************/
PUBLIC VOID
V3RagFlushAggLsaInTransitArea (tV3OsArea * pTranistArea)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tIp6Addr           *pIp6Addr = NULL;
    UINT1               u1PrefixLength = 0;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;
    tInputParams        inParams;
    VOID               *pLeafNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pTranistArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagFlushAggLsaInTransitArea\n");

    /* Flusing the backbone aggregated LSAs from the transit area */
    TMO_SLL_Scan (&(pTranistArea->pV3OspfCxt->pBackbone->type3AggrLst),
                  pInternalRng, tV3OsAddrRange *)
    {
        pIp6Addr = &(pInternalRng->ip6Addr);
        u1PrefixLength = pInternalRng->u1PrefixLength;

        if (pInternalRng->u1AggrEffect == OSPFV3_DO_NOT_ADVERTISE_MATCHING)
        {
            continue;
        }

        if ((pLsaInfo = V3LsuSearchDatabase
             (OSPFV3_INTER_AREA_PREFIX_LSA,
              &(pInternalRng->linkStateId),
              &(pTranistArea->pV3OspfCxt->rtrId), NULL, pTranistArea)) != NULL)
        {
            V3AgdFlushOut (pLsaInfo);
        }
    }

    /* Scan the Routing table */
    inParams.pRoot = pTranistArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pTranistArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        /* If the route entry is INTRA AREA path through backbone then only do the 
           following. */
        if ((V3UtilAreaIdComp (OSPFV3_GET_PATH_AREA_ID (pRtEntry),
                               OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL) &&
            (OSPFV3_IS_INTRA_AREA_PATH (pRtEntry)))
        {
            TMO_SLL_Scan (&(pTranistArea->pV3OspfCxt->pBackbone->type3AggrLst),
                          pInternalRng, tV3OsAddrRange *)
            {
                pIp6Addr = &(pInternalRng->ip6Addr);
                u1PrefixLength = pInternalRng->u1PrefixLength;

                if (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                         pIp6Addr,
                                         u1PrefixLength) == OSPFV3_EQUAL)
                {
                    V3GenerateSummary (pTranistArea, pRtEntry);
                }
            }
        }

        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
        au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pTranistArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagFlushAggLsaInTransitArea\n");
    return;
}

/************************************************************************/
/* Function     : V3RagSetAggFlag                                       */
/*                                                                      */
/* Description  : This Function Scan the Routing table and if any       */
/*                route entry falls in Address range then set the flag  */
/*                this flag will be used for generation of agg lsa      */
/*                                                                      */
/* Input        : pArea        :  pointer to the AREA.                  */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/************************************************************************/
PUBLIC VOID
V3RagSetAggFlag (tV3OsArea * pArea)
{
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;
    tV3OsPath          *pPath = NULL;
    tInputParams        inParams;
    VOID               *pLeafNode = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    VOID               *pTempPtr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagSetAggFlag\n");

    TMO_SLL_Scan (&(pArea->type3AggrLst), pInternalRng, tV3OsAddrRange *)
    {
        pInternalRng->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
        pInternalRng->u4RtCount = 0;
    }

    /* Scan Routing Table */
    inParams.pRoot = pArea->pV3OspfCxt->ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pArea->pV3OspfCxt->ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        pPath = OSPFV3_GET_PATH (pRtEntry);
        if ((pPath != NULL) && (pPath->u1PathType == OSPFV3_INTRA_AREA) &&
            ((V3UtilAreaIdComp (pPath->areaId, pArea->areaId) == OSPFV3_EQUAL)))
        {
            TMO_SLL_Scan (&(pArea->type3AggrLst), pInternalRng,
                          tV3OsAddrRange *)
            {
                if (pInternalRng->areaAggStatus != ACTIVE)
                {
                    continue;
                }

                if (V3UtilIp6PrefixComp (&(pRtEntry->destRtPrefix),
                                         &(pInternalRng->ip6Addr),
                                         pInternalRng->u1PrefixLength)
                    == OSPFV3_EQUAL)
                {
                    if ((pInternalRng->metric.u4Metric ==
                         OSPFV3_LS_INFINITY_24BIT)
                        || (pInternalRng->metric.u4Metric < pPath->u4Cost))
                    {
                        pInternalRng->metric.u4Metric = pPath->u4Cost;
                    }
                    pInternalRng->u4RtCount++;
                    break;
                }
            }
        }
        pLeafNode = inParams.pLeafNode;
        OSPFV3_IP6ADDR_CPY (au1Key, &(pRtEntry->destRtPrefix));
        au1Key[OSPFV3_TRIE_KEY_SIZE - 1] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr,
                            (VOID **) &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagSetAggFlag\n");
    return;
}

/************************************************************************/
/* Function     : V3RagGenerateAggLsa                                   */
/*                                                                      */
/* Description  : This Function Scan the Area List and generate Agg     */
/*               Lsa if Flag is set else flush the Agg Lsa if           */
/*                flag is not set and Agg lsa is present in             */
/*                the database                                          */
/*                                                                      */
/* Input        : pArea :  pointer to the AREA.                         */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : None                                                  */
/************************************************************************/
PUBLIC VOID
V3RagGenerateAggLsa (tV3OsArea * pArea)
{
    tV3OsAddrRange     *pInternalRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagGenerateAggLsa\n");

    TMO_SLL_Scan (&(pArea->type3AggrLst), pInternalRng, tV3OsAddrRange *)
    {
        if (pInternalRng->u4RtCount > 0)
        {
            if (pInternalRng->u1AggrEffect != OSPFV3_DO_NOT_ADVERTISE_MATCHING)
            {
                V3GenerateAggLsa (pArea, pInternalRng);
            }
        }
        else
        {
            V3RtcFlushOutAggrLsa (pArea, pInternalRng);
            pInternalRng->metric.u4Metric = OSPFV3_LS_INFINITY_24BIT;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagGenerateAggLsa \n");
}

/******************************************************************************/
/*                                                                            */
/* Function     : V3AreaFindInternalAddrRngInCxt                              */
/*                                                                            */
/* Description  : This procedure finds the specified IPv6 address with one of */
/*                the router's configured Internal Address Range for an       */
/*                area.                                                       */
/*                                                                            */
/*                                                                            */
/* Input        : pV3OspfCxt    : Contetx pointer                             */
/*                pIp6Addr      : IPv6  address                               */
/*              : pAreaId      :  Area Identifier                             */
/* Output       : None                                                        */
/*                                                                            */
/* Returns      : IPv6 address range, on success                              */
/*                NULL, otherwise                                             */
/*                                                                            */
/******************************************************************************/
PUBLIC tV3OsAddrRange *
V3AreaFindInternalAddrRngInCxt (tV3OspfCxt * pV3OspfCxt, tIp6Addr * pIp6Addr,
                                tV3OsAreaId * pAreaId)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:V3AreaFindInternalAddrRngInCxt\n");

    pArea = V3GetFindAreaInCxt (pV3OspfCxt, pAreaId);

    if (pArea == NULL)
    {
        return NULL;
    }

    TMO_SLL_Scan (&(pArea->type3AggrLst), pInternalRng, tV3OsAddrRange *)
    {
        if ((V3UtilIp6PrefixComp (pIp6Addr,
                                  &(pInternalRng->ip6Addr),
                                  pInternalRng->u1PrefixLength))
            == OSPFV3_EQUAL)
        {
            return pInternalRng;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaFindInternalAddrRngInCxt\n");

    return NULL;
}

/*****************************************************************************/
/* Function     : V3AreaFindActiveInternalAddrRngInCxt                       */
/*                                                                           */
/* Description  : This procedure finds the specified IPv6 Address with       */
/*                the router's configured Internal Address Ranges            */
/*                the IP addr falls into.                                    */
/*                                                                           */
/* Input        : pV3OspfCxt   : Context pointer                             */
/*                pAddr        : IP address                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : IP address range, on success                               */
/*                NULL, otherwise                                            */
/*****************************************************************************/
PUBLIC tV3OsAddrRange *
V3AreaFindActiveInternalAddrRngInCxt (tV3OspfCxt * pV3OspfCxt,
                                      tIp6Addr * pIp6Addr)
{
    tV3OsArea          *pArea = NULL;
    tV3OsAddrRange     *pInternalRng = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3AreaFindActiveInternalAddrRngInCxt\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        TMO_SLL_Scan (&(pArea->type3AggrLst), pInternalRng, tV3OsAddrRange *)
        {
            if ((V3UtilIp6PrefixComp (pIp6Addr,
                                      &(pInternalRng->ip6Addr),
                                      pInternalRng->u1PrefixLength)
                 == OSPFV3_EQUAL) && (pInternalRng->u4RtCount > 0))
            {
                return pInternalRng;
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3AreaFindActiveInternalAddrRngInCxt\n");

    return NULL;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agsumm.c                     */
/*-----------------------------------------------------------------------*/
