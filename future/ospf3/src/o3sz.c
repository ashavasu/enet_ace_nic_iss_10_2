/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: o3sz.c,v 1.4 2013/12/18 12:48:31 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _OSPFV3SZ_C
#include "o3inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);

INT4
Ospfv3SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPFV3_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsOSPFV3SizingParams[i4SizingId].u4StructSize,
                              FsOSPFV3SizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(OSPFV3MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Ospfv3SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Ospfv3SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsOSPFV3SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, OSPFV3MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Ospfv3SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < OSPFV3_MAX_SIZING_ID; i4SizingId++)
    {
        if (OSPFV3MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (OSPFV3MemPoolIds[i4SizingId]);
            OSPFV3MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
