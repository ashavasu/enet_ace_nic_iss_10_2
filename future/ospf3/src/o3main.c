/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3main.c,v 1.39 2018/02/07 09:32:29 siva Exp $
 *
 * Description: This file contains the Ospfv3 task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#ifndef _OS3MAIN_C
#define _OS3MAIN_C
#include "o3inc.h"
#include "o3glob.h"

#include "o3rtstg.h"

/* Proto types of the functions private to this file only */
#ifdef OSPF3_BCMXNP_HELLO_WANTED
tOsixQId            gO3HelloPktQId;
tOsixTaskId         gO3TaskId;
#endif
PRIVATE INT4 V3OspfMemInit PROTO ((VOID));
PRIVATE INT4 V3OspfTaskInit PROTO ((VOID));
PRIVATE VOID V3OspfDeInit PROTO ((VOID));
PRIVATE VOID V3OspfProcessRMapHdlr PROTO ((tOsixMsg *));
PRIVATE VOID V3OspfProcessVcmChgMsg PROTO ((tV3OspfVcmInfo * pV3OspfVcmInfo));
PRIVATE INT4 V3OspfMemInitForModuleStart PROTO ((VOID));
PRIVATE VOID V3OspfMemDeInitForModuleShut PROTO ((VOID));
PRIVATE VOID        V3OspfProcessLPOSPFPacket
PROTO ((UINT1 *pPkt, UINT2 u2Len, tV3OsNeighbor * pNbr));
PRIVATE VOID V3OspfSendMsgToLowPriorityQ PROTO ((tV3OspfQMsg * pOspfQMsg));

#ifdef OSPF3_BCMXNP_HELLO_WANTED
extern UINT4
 
 
 
 CfaGetVlanIfIndex (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                    tVlanIfaceVlanId * pVlanId);
extern UINT4
 
 
 
 CfaGetVlanIfIndexWithoutLock (UINT4 u4IfIndex, tCRU_BUF_CHAIN_HEADER * pBuf,
                               tVlanIfaceVlanId * pVlanId);
extern INT4         VlanGetMbsmUpdateOnCardInsertionStatus (VOID);
extern VOID         IpifGetIfId (UINT2 u2Port, tIP_INTERFACE * pIfId);
#endif

/******************************************************************************
 * DESCRIPTION : Gets the Ospf3 Task Id
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : Ospf3 Task Id
 * NOTES       :
 ******************************************************************************/
tOsixTaskId
Ospf3GetTaskId (VOID)
{
    tOsixTaskId         Ospf3TskId = 0;
    if (OsixTskIdSelf (&Ospf3TskId) == OSIX_FAILURE)
    {
        return -1;
    }
    return Ospf3TskId;
}

/****************************************************************************
*                                                                           *
* Function     : V3OSPFTaskMain                                             *
*                                                                           *
* Description  : Main function of OSPFV3.                                   *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
V3OSPFTaskMain (INT1 *pTaskId)
{
    UINT4               u4Event = 0;
#ifdef ROUTEMAP_WANTED
    UINT2               u2Index = 0;
#endif
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
            defined (LNXIP6_WANTED)
    tV3OsVrfSock       *pQueMsg = NULL;
    INT4                i4RawSockId = -1;
    INT4                i4Count = 0;
    UINT1               u1Registerflag = 0;
#endif
    UINT4               u450msTicks = 50;
    tV3OsInterface     *pInterface = NULL;

    UNUSED_PARAM (pTaskId);

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OSPFTaskMain\n");

    if (V3OspfTaskInit () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        " !!!!! TASK INIT FAILURE  !!!!! \n");
        V3OspfDeInit ();
        OSPF3_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    /* Indicate ISS module regarding module start */
    IssSetModuleSystemControl (OSPF3_MODULE_ID, MODULE_START);

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_OSPF3, V3OspfSendRouteMapUpdateMsg);
    for (; u2Index < MAX_OSPFV3_RMAP_MSGS; u2Index++)
    {
        gV3OsRmapMsg[u2Index] = NULL;
    }
#endif

    OSPF3_INIT_COMPLETE (OSIX_SUCCESS);
    OsixGetTaskId (0, OSPFV3_TASK_NAME, &gV3OsRtr.Ospf3TaskId);
#ifdef OSPF3_BCMXNP_HELLO_WANTED
    gO3TaskId = gV3OsRtr.Ospf3TaskId;
#endif
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterOspf3Mibs ();
#endif

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
    if (V3OspfCreateNLHelloSocket () == OSPFV3_FAILURE)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_CRITICAL_TRC,
                         " !!!!! TASK INIT FAILURE !!!!! \n",
                         OSPFV3_INVALID_CXT_ID);
        OSPF3_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
#endif

    while (1)
    {

        OsixEvtRecv (OSPF3_TASK_ID,
                     OSPFV3_TIMER_EVENT | OSPFV3_RTM_EVENT | OSPFV3_CLEAR_EVENT
                     | OSPFV3_PKT_ARRIVAL_EVENT | OSPFV3_MSGQ_EVENT |
                     OSPFV3_RMQ_EVENT | OSPFV3_HELLO_TIMER_EVENT |
                     OSPFV3_GO_ACTIVE_RM_NOTIFY_EVENT | TM50MS_TIMER_EXP_EVENT |
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
                     OSPFV3_NLH_PKT_ARRIVAL_EVENT |
#endif
#endif
#ifdef OSPF3_BCMXNP_HELLO_WANTED
                     OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT |
#endif
                     OSPFV3_RM_SUB_BULK_EVENT, OSIX_WAIT, (UINT4 *) &(u4Event));
        V3OspfLock ();

        OSPFV3_GBL_TRC1 (CONTROL_PLANE_TRC, "Rx Event %d\n", u4Event);
        if (u4Event & OSPFV3_GO_ACTIVE_RM_NOTIFY_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                            "OSPFV3_GO_ACTIVE_RM_NOTIFY_EVENT \n");
            V3OspfRmHandleGoActiveEventNotify ();
        }

#ifdef OSPF3_BCMXNP_HELLO_WANTED
        if (u4Event & OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT)
        {
            V3OspfProcessNpHelloPacketQMsg (OSIX_TRUE);
        }
#endif
        if (u4Event & TM50MS_TIMER_EXP_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "TM50MS_TIMER_EXP_EVENT\n");
            pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
            if (pInterface != NULL)
            {
                do
                {
                    if (pInterface->delLsAck.u2CurrentLen > 0)
                    {
                        OSPFV3_TRC1 (OSPFV3_LAK_TRC | OSPFV3_ADJACENCY_TRC,
                                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                                     "From periodic timer, ACK to be transmitted on interface Id :%d\n",
                                     pInterface->u4InterfaceId);

                        V3LakSendDelayedAck (pInterface);
                    }
                    if (pInterface->lsUpdate.u2CurrentLen > 0)
                    {
                        OSPFV3_TRC1 (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                                     "From periodic timer, LSU to be transmitted on interface Id :%d\n",
                                     pInterface->u4InterfaceId);
                        V3LsuSendLsu (NULL, pInterface);
                    }
                }
                while ((pInterface = (tV3OsInterface *)
                        RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                       (tRBElem *) pInterface, NULL)) != NULL);

            }
            V3TmrSetTimer (&(gV3OsRtr.g50msTimerNode),
                           OSPFV3_50MS_LSU_LAK_TIMER, u450msTicks);
        }
        if (u4Event & OSPFV3_MSGQ_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_PKT_ARRIVAL_EVENT\n");
            V3OspfProcessQMsg (OSPF3_Q_ID);
        }

        if (u4Event & OSPFV3_RMQ_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_RMQ_EVENT\n");
            V3OspfProcessQMsg (OSPF3_RM_Q_ID);
        }

        if (u4Event & OSPFV3_RTM_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_RTM_EVENT\n");
#ifdef RRD_WANTED
            V3OspfProcessRtmRts ();
#endif
        }
        if (u4Event & OSPFV3_RM_SUB_BULK_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_RM_SUB_BULK_EVENT\n");

            O3RedBlkSendSubBulkUpdate ();
        }
        if (u4Event & OSPFV3_TIMER_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_TIMER_EVENT\n");

            V3TmrHandleExpiry ();
        }
        if (u4Event & OSPFV3_HELLO_TIMER_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_HELLO_TIMER_EVENT\n");
            V3TmrHandleHelloExpiry ();
        }
        if (u4Event & OSPFV3_CLEAR_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_CLEAR_EVENT\n");
            V3OspfProcessClearMsg ();
        }

#ifdef RAWSOCK_WANTED
        if (u4Event & OSPFV3_PKT_ARRIVAL_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_PKT_ARRIVAL_EVENT\n");

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
            defined (LNXIP6_WANTED)

            while (OsixQueRecv (OSPF3_LNX_Q_ID, (tOsixMsg **) (&pQueMsg),
                                OSIX_DEF_MSG_LEN,
                                OSPFV3_INIT_VAL) == OSIX_SUCCESS)
            {
                i4RawSockId = pQueMsg->i4SockFd;
                MemReleaseMemBlock (OSPFV3_VRF_SOCK_FD, (UINT1 *) pQueMsg);
                pQueMsg = NULL;

                V3OspfGetPkt (i4RawSockId);

                if (i4RawSockId > OSPFV3_INIT_VAL)
                {
                    for (i4Count = OSPFV3_INIT_VAL;
                         i4Count < SYS_DEF_MAX_NUM_CONTEXTS; i4Count++)
                    {
                        if (gV3OsRtr.ai4RawSockId[i4Count] == i4RawSockId)
                        {
                            u1Registerflag = OSPFV3_ONE;
                            break;
                        }
                    }
                    if (u1Registerflag == OSPFV3_ONE)
                    {
                        if (SelAddFd (i4RawSockId, V3SendEvent2Ospf) !=
                            OSIX_SUCCESS)
                        {
                            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                            "Adding Socket Descriptor "
                                            "to Select utility Failed \r\n ");
                        }
                    }
                }
            }
#else
            V3OspfGetPkt (gV3OsRtr.i4RawSockId);
            SelAddFd (gV3OsRtr.i4RawSockId, V3SendEvent2Ospf);
#endif
        }
#endif

        if (u4Event & OSPFV3_TIMER_EVENT)
        {
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "OSPFV3_TIMER_EVENT\n");

            V3TmrHandleExpiry ();
        }

#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
        /* Process the Packets received on Socket */
        if (u4Event & OSPFV3_NLH_PKT_ARRIVAL_EVENT)
        {
            Ip6ifGetNLHelloPkt ();
            /* Add the Socket Descriptor to Select utility 
             * for Packet Reception */
            /* This check must be done because thro. 
             * CLI we might delete the socket and initialized to -1*/
            if (gV3OsRtr.i4NLHelloSockId != -1)
            {
                if (SelAddFd
                    (gV3OsRtr.i4NLHelloSockId,
                     V3OspfPacketOnNLHelloSocket) != OSIX_SUCCESS)
                {
                    OSPFV3_GBL_TRC1 (OSPFV3_CRITICAL_TRC,
                                     "Adding Hello Socket Descriptor to Select utility Failed \r\n",
                                     OSPFV3_INVALID_CXT_ID);
                }
            }
        }

#endif
#endif

        /* Any event posted to the ISM process queue is executed now */
        V3IsmProcessSchedQueue ();

        /* Mutual exclusion flag OFF */
        V3OspfUnLock ();
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OSPFTaskMain\n");

}

/****************************************************************************
*                                                                           *
* Function     : V3OspfTaskInit                                             *
*                                                                           *
* Description  : OSPFV3 initialization routine.                             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
V3OspfTaskInit (VOID)
{
    tOsixSemId          SemId = 0;
    tV3OsAreaId         bBoneAreaId;
    INT4                i4SysLogId = OSPFV3_ZERO;
    MEMSET (&gV3OsRtr, 0, sizeof (tV3OsRtr));

    gu1O3HelloSwitchOverFlag = 0;
#ifdef TRACE_WANTED
    OSPFV3_GBL_TRC_FLAG = OSPFV3_CRITICAL_TRC;
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfTaskInit\n");

    if (OsixSemCrt ((UINT1 *) "OVME", &(gV3OsRtr.MulExclSemId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                         "Semaphore Creation failure for %s \n",
                         OSPFV3_MUT_EXCL_SEM_NAME);
        return OSIX_FAILURE;
    }
    OsixSemGive (OSPF3_MUL_EXCL_SEM_ID);

    if (OsixSemCrt (OSPFV3_RTMRT_LIST_SEM, &(gV3OsRtr.RtmLstSemId))
        != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC |
                         OSPFV3_CONFIGURATION_TRC,
                         "Semaphore creation failed  for %d \n",
                         OSPFV3_RTMRT_LIST_SEM);
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
    OsixSemGive (OSPF3_RTM_LST_SEM_ID);

    /* Create buffer pools for data structures */
    if (V3OspfMemInit () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "Memory allocation failed for OSPFv3 MemInit\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) OSPFV3_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsOSPFV3SizingParams[MAX_OSPFV3_QUEUE_SIZE_SIZING_ID].
                    u4PreAllocatedUnits, &(gV3OsRtr.Ospf3QId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "OSPQ Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    if (OsixQueCrt
        ((UINT1 *) OSPFV3_LOW_PRIORITY_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
         FsOSPFV3SizingParams[MAX_OSPFV3_LP_QUEUE_SIZE_SIZING_ID].
         u4PreAllocatedUnits, &(gV3OsRtr.Ospf3LPQId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "O3LQ Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    if (OsixQueCrt ((UINT1 *) OSPFV3_RM_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsOSPFV3SizingParams[MAX_OSPFV3_RM_QUEUE_SIZE_SIZING_ID].
                    u4PreAllocatedUnits,
                    &(gV3OsRtr.Ospf3RmQId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "OSPQ Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    if (OsixQueCrt ((UINT1 *) OSPFV3_LNXVRF_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsOSPFV3SizingParams[MAX_OSPFV3_VRF_SOCK_SIZING_ID].
                    u4PreAllocatedUnits,
                    &(gV3OsRtr.Ospf3LnxQId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "OSPQ Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
#endif

#ifdef OSPF3_BCMXNP_HELLO_WANTED
    if (OsixQueCrt ((UINT1 *) OSPFV3_NP_HELLO_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    MAX_OSPFV3_NP_QUEUE_SIZE,
                    &(gV3OsRtr.Ospf3NPHelloQId)) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "O3HQ Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
    gO3HelloPktQId = gV3OsRtr.Ospf3NPHelloQId;
#endif

    gV3OsRtr.ospfRedInfo.u4PrevRmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.i4HsAdminStatus = OSPFV3_DISABLED;
    gV3OsRtr.u4AckOrUpdatePktSentCt = 0;

    if (V3OspfRegisterWithRM () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "RM Registration failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    /* Register with VCM module to handle the context deletion indication */
    if (V3OspfRegisterWithVcm () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "VCM registration failed\r\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    gV3OsRtr.pIfRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsInterface, RbNode)),
                              V3RBInterfaceCmpFunc);
#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "OSPFV3", SYSLOG_ALERT_LEVEL);
    if (i4SysLogId < 0)
    {
        /* Indicate the status of initialization to the main routine */
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
    OSPFV3_SYSLOG_ID = (UINT4) i4SysLogId;
#else
    UNUSED_PARAM (i4SysLogId);
#endif

    if (gV3OsRtr.pIfRBRoot == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Interface RB Tree Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    gV3OsRtr.pRtrLsaCheck =
        RBTreeCreateEmbedded (OSPFV3_OFFSET (tV3OsRtrLsaRtInfo, RbNode),
                              V3CompRtrLsaLink);
    if (gV3OsRtr.pRtrLsaCheck == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "RtrLsaRtInfo RB Tree Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    gV3OsRtr.pNwLsaCheck =
        RBTreeCreateEmbedded (OSPFV3_OFFSET (tV3OsNwLsaRtInfo, RbNode),
                              V3CompNwLsaLink);
    if (gV3OsRtr.pNwLsaCheck == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "NwLsaRtInfo RB Tree Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    gV3OsRtr.pGRLsIdInfo =
        RBTreeCreateEmbedded (OSPFV3_OFFSET (tV3OsGRLsIdInfo, RbNode),
                              V3CompGRLsIdInfo);
    if (gV3OsRtr.pGRLsIdInfo == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "LsaIdInfo RB Tree Creation Failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    /* Create the timer list */
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, OSPFV3_TIMER_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.timerLstId))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "Timer list creation failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    /* Create the Hello timer list */
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, OSPFV3_HELLO_TIMER_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.hellotimerLstId))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "hello Timer list creation failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, TM50MS_TIMER_EXP_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.g50msTimerLst))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "Timer list creation failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    /* Initialise the VRF SPF DLL */
    TMO_DLL_Init (&(gV3OsRtr.vrfSpfList));
    TMO_SLL_Init (&(gV3OsRtr.rtmRoutesLst));

    /* Initialize the schedule queue. The first paramter is the context
     * parameter which is set for initializing particular context schedule
     * queue. The second parameter is the interface pointer which is set
     * for initializing particular interface schedule queue. In task
     * initialization, since all the queue should be initialized, both
     * the parameters are passed as NULL
     */
    V3IsmInitSchedQueueInCxt (NULL, NULL);

    /* Initialise the timer related structures */
    V3TmrInitTimerDesc ();

    /* The default context is created during task creation. This context
     * can neither be created or destroyed by the administrator
     */
    if (V3RtrCreateCxt (OSPFV3_DEFAULT_CXT_ID) == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Default context creation failed\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }

    /* If the system is operating in SI mode, the default context pointer
     * is set in the global pointer
     */
    if (V3OspfGetSystemModeExt (OSPF3_PROTOCOL_ID) == OSPFV3_SI_MODE)
    {
        gV3OsRtr.pV3OspfCxt = gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID];
    }

    /* Relinquish related initialization */
    gV3OsRtr.u4RTStaggeringStatus = OSPFV3_STAGGERING_ENABLED;
    gV3OsRtr.u4RTStaggeredCtxId = OSPFV3_INVALID_CXT_ID;

    /* Intiailize the VRF interval to the default value */
    gV3OsRtr.u4VrfSpfInterval = OSPFV3_DEF_VRF_SPF_INTERVAL;
    MEMSET (&bBoneAreaId, 0, OSPFV3_AREA_ID_LEN);

    gV3OsRtr.bIgnoreExpiredTimers = OSIX_FALSE;
    if (V3AuthCryptoHighSeq () == OSPFV3_AT_DROP)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "File creation failed for Authentication Trailer"
                        "sequence number\n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return OSIX_FAILURE;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfTaskInit\n");
    KW_FALSEPOSITIVE_FIX (SemId);
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : V3OspfMemInit                                              *
*                                                                           *
* Description  : Allocates all the memory that is required for OSPFV3.      *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if memory allocation successful              *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
V3OspfMemInit (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMemInit\n");

    if (V3OspfMemInitForModuleStart () == OSIX_FAILURE)
    {
        V3OspfMemDeInitForModuleShut ();
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "Memory allocation failed for " "OSPFv3 MemInit\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, OSPFV3_SYSLOG_ID,
                      "Memory allocation failed for " "OSPFv3 MemInit"));
        return OSIX_FAILURE;
    }
    Ospfv3SizingMemCreateMemPools ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfMemInit\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfMemClear                                             */
/*                                                                           */
/* Description  : Clears all the free queues.                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3OspfMemClear (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMemClear\n");

    Ospfv3SizingMemDeleteMemPools ();
#ifdef CLI_WANTED
    CsrMemDeAllocation (OSPF3_MODULE_ID);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfMemClear\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfProcessQMsg                                          */
/*                                                                           */
/* Description  : Process the queue for Ospfv3 packets                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessQMsg (tOsixQId QID)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;
    tOsixMsg           *pOsixMsg = NULL;
#ifdef MBSM_WANTED
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    INT4                i4RetVal = 0;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif /* MBSM_WANTED */

#ifdef BFD_WANTED
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tIp6Addr            NbrAddr;
#endif
    UINT4               u4LowPriMessageCount = 0;
    INT4                i4MsgRetVal = 0;
    UINT1               u1HPMsgCnt = 0;
#ifdef BFD_WANTED

    MEMSET (&NbrAddr, 0, sizeof (NbrAddr));
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfProcessQMsg\n");

    OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4LowPriMessageCount);

    i4MsgRetVal =
        (INT4) OsixQueRecv (QID, (UINT1 *) &pOsixMsg, OSIX_DEF_MSG_LEN,
                            (UINT4) 0);

    if (QID == OSPF3_Q_ID)
    {
        while ((i4MsgRetVal == OSIX_SUCCESS) || (u4LowPriMessageCount != 0))

        {
            pOspfQMsg = (tV3OspfQMsg *) pOsixMsg;
            if (i4MsgRetVal == OSIX_SUCCESS)
            {
                if (V3OspfIsLowPriorityMessage (pOspfQMsg->u4OspfMsgType) ==
                    OSPFV3_SUCCESS)
                {
                    V3OspfSendMsgToLowPriorityQ (pOspfQMsg);
                }
                else
                {
                    /* To avoid indefinite waiting of low priority messages,
                     * process low priority messages at regular interval*/
                    u1HPMsgCnt++;
                    if (u1HPMsgCnt >= MAX_OSPF3_HP_MSGS)
                    {

                        if (gV3OsRtr.u4RTStaggeringStatus ==
                            OSPFV3_STAGGERING_ENABLED)
                        {
                            /* process the low priority messages only when
                             * RTC is not relinquished */
                            if (u4LowPriMessageCount != 0)
                            {
                                V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
                            }
#ifdef OSPF3_BCMXNP_HELLO_WANTED
                            OsixEvtRecv (OSPF3_TASK_ID,
                                         OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT,
                                         OSIX_NO_WAIT, (UINT4 *) &(u4Event));
                            if (u4Event & OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT)
                            {
                                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                                "OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT\n");
                                V3OspfProcessNpHelloPacketQMsg (OSIX_FALSE);
                            }
#endif
                        }
                        u1HPMsgCnt = 0;
                    }

                    switch (pOspfQMsg->u4OspfMsgType)
                    {
                        case OSPFV3_PKT_ARRIVAL_EVENT:

#ifndef RAWSOCK_WANTED
                            /* If Raw socket is defined, packets are handled through
                             * packet arrival event
                             */
                            V3OspfDeqPkt (&
                                          (pOspfQMsg->unOspfMsgType.
                                           ospfIpIfParam));
#endif
                            break;
                        case OSPFV3_IPV6_IF_STAT_CHG_EVENT:

                            V3OspfIfStatusChgHdlr (&(pOspfQMsg->unOspfMsgType.
                                                     ospfIpIfStatParam));
                            break;
                        case OSPFV3_IPV6_IF_ADDR_CHG_EVENT:

                            V3IfAddrChgHdlr (&
                                             (pOspfQMsg->unOspfMsgType.
                                              ospfIpIfAddrParam));
                            break;
                        case OSPFV3_ROUTEMAP_UPDATE_EVENT:

                            V3OspfProcessRMapHdlr (pOspfQMsg->unOspfMsgType.
                                                   pV3OspfRMapParam);
                            break;

#ifdef MBSM_WANTED
                        case MBSM_MSG_CARD_INSERT:
                            i4ProtoId =
                                pOspfQMsg->unOspfMsgType.MbsmCardUpdate.
                                mbsmProtoMsg.i4ProtoCookie;
                            pSlotInfo =
                                &(pOspfQMsg->unOspfMsgType.MbsmCardUpdate.
                                  mbsmProtoMsg.MbsmSlotInfo);
                            i4RetVal =
                                V3OspfMbsmUpdateCardInsertion (pSlotInfo);
                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                            MbsmSendAckFromProto (&MbsmProtoAckMsg);

                            break;
                        case MBSM_MSG_CARD_REMOVE:
                            i4ProtoId =
                                pOspfQMsg->unOspfMsgType.MbsmCardUpdate.
                                mbsmProtoMsg.i4ProtoCookie;
                            pSlotInfo =
                                &(pOspfQMsg->unOspfMsgType.MbsmCardUpdate.
                                  mbsmProtoMsg.MbsmSlotInfo);
                            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                            MbsmProtoAckMsg.i4SlotId =
                                MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                            MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                            MbsmSendAckFromProto (&MbsmProtoAckMsg);
                            break;
#endif
                        case OSPFV3_VCM_CHG_EVENT:
                            V3OspfProcessVcmChgMsg (&pOspfQMsg->unOspfMsgType.
                                                    ospfVcmInfo);
                            break;
#ifdef BFD_WANTED
                        case OSPFV3_BFD_NBR_DOWN_EVENT:
                            pV3OspfCxt =
                                V3UtilOspfGetCxt (pOspfQMsg->u4OspfCxtId);

                            MEMCPY (&NbrAddr,
                                    &(pOspfQMsg->Ospfv3BfdMsgInfo.NbrAddr),
                                    sizeof (tIpAddr));

                            if (pV3OspfCxt != NULL)
                            {
                                if (pV3OspfCxt->u1Ospfv3RestartState !=
                                    OSPFV3_GR_NONE)
                                {
                                    if (BfdApiCheckControlPlaneDependent
                                        (pV3OspfCxt->u4ContextId,
                                         &(pOspfQMsg->Ospfv3BfdMsgInfo)) ==
                                        OSIX_SUCCESS)
                                    {
                                        O3GrExitGracefulRestartInCxt
                                            (pV3OspfCxt,
                                             OSPFV3_RESTART_TERM_BFD_DOWN);
                                    }
                                }

                                if ((pNbr = V3GetFindNbrInCxtForBfd (pV3OspfCxt,
                                                                     &NbrAddr))
                                    != NULL)
                                {
                                    /* BFD detect the failure in the path, so restarting
                                     * router is going down, change the helper state
                                     * as not helping, before calling NbrUpdateState
                                     * with NBR_DOWN indication*/
                                    if (pNbr->u1NbrHelperStatus ==
                                        OSPFV3_GR_HELPING)
                                    {
                                        pNbr->u1NbrHelperStatus =
                                            OSPFV3_GR_NOT_HELPING;
                                    }
                                    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                                    if ((pNbr->pInterface->u1NetworkType ==
                                         OSPFV3_IF_BROADCAST)
                                        || (pNbr->pInterface->u1NetworkType ==
                                            OSPFV3_IF_PTOP))
                                    {
                                        V3NbrDelete (pNbr);
                                    }
                                }
                            }
                            break;
#endif
                        default:
                            OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                                            "Wrong Message Type\n");
                            break;
                    }
                }
                OSPFV3_QMSG_FREE (pOspfQMsg);
            }
            else if (u4LowPriMessageCount != 0)
            {
                V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
            }
            OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4LowPriMessageCount);
            i4MsgRetVal =
                (INT4) OsixQueRecv (OSPF3_Q_ID, (UINT1 *) &pOsixMsg,
                                    OSIX_DEF_MSG_LEN, (UINT4) 0);
        }
    }
    else if (QID == OSPF3_RM_Q_ID)
    {
        while (i4MsgRetVal == OSIX_SUCCESS)
        {
            pOspfQMsg = (tV3OspfQMsg *) pOsixMsg;

            switch (pOspfQMsg->u4OspfMsgType)
            {
                case OSPFV3_RM_MSG_EVENT:
                    O3RedRmHandleRmEvents (&
                                           (pOspfQMsg->unOspfMsgType.
                                            ospfv3RmCtrlMsg));
                    break;
                default:
                    OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                                    "Wrong Message Type\n");
                    break;
            }

            OSPFV3_RM_QMSG_FREE (pOspfQMsg);
            i4MsgRetVal =
                (INT4) OsixQueRecv (OSPF3_RM_Q_ID, (UINT1 *) &pOsixMsg,
                                    OSIX_DEF_MSG_LEN, (UINT4) 0);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfProcessQMsg\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfLock                                                 */
/*                                                                           */
/* Description  : Lock the Ospfv3 procedure                                  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfLock (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfLock\n");

    if (OsixSemTake (OSPF3_MUL_EXCL_SEM_ID) != OSIX_SUCCESS)
    {
        gu4V3OspfLockFail++;
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                         "Acquiring Semaphore failed for %s \n",
                         OSPFV3_MUT_EXCL_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Acquiring Semaphore failed for OVME"));

        return SNMP_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfLock\n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfUnLock                                               */
/*                                                                           */
/* Description  : UnLock the Ospfv3 procedure                                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfUnLock (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfUnLock\n");

    if (OsixSemGive (OSPF3_MUL_EXCL_SEM_ID) != OSIX_SUCCESS)

    {
        gu4V3OspfUnLockFail++;
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                         "Semaphore Release failed for %s \n",
                         OSPFV3_MUT_EXCL_SEM_NAME);
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Semaphore Release failed for OVME"));

        return SNMP_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfUnLock\n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfTimedLock                                            */
/*                                                                           */
/* Description  : Lock the Ospfv3 procedure on time basis                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3OspfTimedLock (VOID)
{
    INT4                i4RetVal = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "FUNC:V3OspfTimedLock\n");

    i4RetVal =
        (INT4) OsixSemTimedTake (OSPF3_MUL_EXCL_SEM_ID,
                                 OSIX_MAX_WAIT_FOR_SEM_LOCK);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfTimedLock\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfDeInit                                               */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3OspfDeInit (VOID)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfDeInit\n");

    if (gV3OsRtr.pIfRBRoot != NULL)
    {
        RBTreeDelete (gV3OsRtr.pIfRBRoot);
    }

    /* Delete the default context if created */
    if (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID] != NULL)
    {
        V3RtrDeleteCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]);
    }
    if (gV3OsRtr.pRtrLsaCheck != NULL)
    {
        RBTreeDelete (gV3OsRtr.pRtrLsaCheck);
    }

    if (gV3OsRtr.pNwLsaCheck != NULL)
    {
        RBTreeDelete (gV3OsRtr.pNwLsaCheck);
    }

    if (OSPF3_RTM_LST_SEM_ID != NULL)
    {
        OsixSemDel (OSPF3_RTM_LST_SEM_ID);
    }

    V3OspfMemClear ();

    V3OspfDeRegisterWithRM ();

    /* De-register from VCM module */
    V3OspfDeRegisterWithVcm ();

    OsixSemDel (OSPF3_MUL_EXCL_SEM_ID);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfDeInit\n");
}

/*****************************************************************************/
/* Function     : OspfSendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to OSPFv3   */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if Route Map update Msg Sent to OSPFv3        */
/*                successfully                                               */
/*                OSIX_FAILURE otherwise                                     */
/*****************************************************************************/
PUBLIC INT4
V3OspfSendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tOsixMsg           *pBuf = NULL;
    tV3OspfQMsg        *pV3OspfQMsg = NULL;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfSendRouteMapUpdateMsg\n");

    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    if (pu1RMapName == NULL)
    {
        return OSIX_FAILURE;
    }

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    STRNCPY (au1NameBuf, pu1RMapName, RMAP_MAX_NAME_LEN);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Memory allocation failed for "
                        "route map update message\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, OSPFV3_SYSLOG_ID,
                      "Memory allocation failed for "
                      "route map update message"));
        return OSIX_FAILURE;
    }

    /*** copy status to offset 0 ***/
    if ((i4OutCome = CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Status, 0,
                                                sizeof (u4Status))) !=
        CRU_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "CRU buffer copy failed for "
                        "route map update message\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, OSPFV3_SYSLOG_ID,
                      "CRU buffer copy failed for "
                      "route map update message"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, au1NameBuf, sizeof (u4Status),
                                    RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "CRU buffer copy failed for "
                        "route map update message\r\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, OSPFV3_SYSLOG_ID,
                      "CRU buffer copy failed for "
                      "route map update message"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }

    /*** post to OSPFv3 queue ***/

    OSPFV3_QMSG_ALLOC (&(pV3OspfQMsg));

    if (pV3OspfQMsg == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "QMSG Allocation failed for "
                        "route map update message\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "QMSG Allocation failed for "
                      "route map update message"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return OSIX_FAILURE;
    }
    pV3OspfQMsg->u4OspfMsgType = OSPFV3_ROUTEMAP_UPDATE_EVENT;
    pV3OspfQMsg->unOspfMsgType.pV3OspfRMapParam = pBuf;

    if (OsixQueSend (OSPF3_Q_ID,
                     (UINT1 *) &pV3OspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Enqueue to OSPFv3 queue failed for"
                        " message received from Route-map module\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enqueue to OSPFv3 queue failed for"
                      " message received from Route-map module"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        OSPFV3_QMSG_FREE (pV3OspfQMsg);
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Event send failed for OSPFV3_ROUTEMAP_UPDATE_EVENT\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Event send failed for OSPFV3_ROUTEMAP_UPDATE_EVENT"));
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfSendRouteMapUpdateMsg\n");

    return i4OutCome;
}

/*****************************************************************************/
/* Function     : OspfProcessRMapHandler                                     */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3OspfProcessRMapHdlr (tOsixMsg * pBuf)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;
    UINT4               u4ContextId = 0;
    UINT2               u2Index = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfProcessRMapHdlr\n");

    if (pBuf == NULL)
    {
        return;
    }

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4Status, 0, sizeof (u4Status)) != sizeof (u4Status))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN + 4));
    if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName, sizeof (u4Status),
                                   RMAP_MAX_NAME_LEN) != RMAP_MAX_NAME_LEN)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (gV3OsRtr.u4RTStaggeredCtxId != OSPFV3_INVALID_CXT_ID)
    {
        /* This means that route map progress is relinquished in some context.
         * So don't process the routemap message now. Defer the route
         * map processing by reposting the event to Queue, Since it was already
         * dequeued.
         */
        for (; u2Index < MAX_OSPFV3_RMAP_MSGS; u2Index++)
        {
            if (gV3OsRmapMsg[u2Index] == NULL)
            {
                gV3OsRmapMsg[u2Index] = pBuf;
                break;
            }
        }
        return;
    }

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }

    /* Scan through all the context and change the status */
    for (u4ContextId = 0; u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT;
         u4ContextId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4ContextId] == NULL)
        {
            /* Get the next context id */
            continue;
        }

        if (gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistributeInFilterRMap != NULL)
        {
            if (STRCMP
                (gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistributeInFilterRMap->
                 au1DistInOutFilterRMapName, au1RMapName) == 0)
            {
                gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistributeInFilterRMap->
                    u1Status = u1Status;
            }
        }

        if (gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistanceFilterRMap != NULL)
        {
            if (STRCMP (gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistanceFilterRMap->
                        au1DistInOutFilterRMapName, au1RMapName) == 0)
            {
                gV3OsRtr.apV3OspfCxt[u4ContextId]->pDistanceFilterRMap->
                    u1Status = u1Status;
            }
        }
        /* Apply Rmap Rules for dropped route enries */
        V3UtilOspfApplyRMapRule (gV3OsRtr.apV3OspfCxt[u4ContextId]);
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfProcessRMapHdlr\n");

}

/*****************************************************************************/
/* Function     : V3OspfProcessVcmChgMsg                                     */
/*                                                                           */
/* Description  : This function processes VCM change event. The evtn is      */
/*                fired in the following scenarios                           */
/*                                                                           */
/*                1. Context is deleted in VCM                               */
/*                2. Interface is unmapped from a context in VCM             */
/*                                                                           */
/* Input        : pV3OspfVcmInfo  - Pointer to VCM Q msg structure           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Nonde                                                      */
/*****************************************************************************/
PRIVATE VOID
V3OspfProcessVcmChgMsg (tV3OspfVcmInfo * pV3OspfVcmInfo)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;

    /* Get the context pointer from the context id present in the
     * structure
     */
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfProcessVcmChgMsg\n");

    if (V3UtilOspfIsValidCxtId ((INT4) pV3OspfVcmInfo->u4VcmCxtId) ==
        OSIX_FAILURE)
    {
        /* Context id is present in OSPF struture */
        pV3OspfCxt = gV3OsRtr.apV3OspfCxt[pV3OspfVcmInfo->u4VcmCxtId];
    }

    if (pV3OspfCxt != NULL)
    {
        switch (pV3OspfVcmInfo->u1BitMap)
        {
            case VCM_CXT_STATUS_CHG_REQ:
            {
                /* Context deletion indication received */
                OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                "Context deletion indication\n");
                V3RtrDeleteCxt (pV3OspfCxt);
                break;
            }
            default:
                break;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfProcessVcmChgMsg\n");
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : V3OspfShutdownProcess                               */
/*                                                                         */
/*     Description   : This routing triggers planned graceful shutdown     */
/*                     This is called from ISS module.                     */
/*                                                                         */
/*     Input(s)      : None                                                */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

VOID
V3OspfShutdownProcess (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfShutdownProcess\r\n");

    O3GrInitiateRestart ();

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfShutdownProcess\r\n");
}

/*****************************************************************************/
/* Function     : V3OspfModuleShutDown                                       */
/*                                                                           */
/* Description  : This function  shuts down the ospfv3 module.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PUBLIC VOID
V3OspfModuleShutDown ()
{
    UINT4               u4CxtId = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfModuleShutDown\n");

    /* Fetch the first context id, then scan from there and delete
     * all the existing contexts */
    for (u4CxtId = 0; u4CxtId < OSPFV3_MAX_CONTEXTS_LIMIT; u4CxtId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4CxtId] != NULL)
        {
            V3RtrDeleteCxt (gV3OsRtr.apV3OspfCxt[u4CxtId]);
        }
    }

    /* Delete the VRF SPF timer */
    V3TmrDeleteTimer (&(gV3OsRtr.vrfSpfTimer));
    V3TmrDeleteTimer (&(gV3OsRtr.g50msTimerNode));
    /* Delete the timer list */
    TmrDeleteTimerList (gV3OsRtr.timerLstId);
    TmrDeleteTimerList (gV3OsRtr.hellotimerLstId);
    TmrDeleteTimerList (gV3OsRtr.g50msTimerLst);
    /* Clear the OSPFV3 QUEUE */
    V3UtilDeleteQueueMsg ();

    /* Delete Mempools except OSPFV3_QMSG_QID */
    V3OspfMemDeInitForModuleShut ();

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapDeRegister (RMAP_APP_OSPF3);
#endif

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    UnRegisterOspf3Mibs ();
#endif

    /* Deregister with RM */
    V3OspfDeRegisterWithRM ();

    /* De-register from VCM module */
    V3OspfDeRegisterWithVcm ();

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfModuleShutDown\n");
    return;
}

/*****************************************************************************/
/* Function     : V3OspfModuleStart                                          */
/*                                                                           */
/* Description  : This function starts the ospfv3 module.                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*****************************************************************************/
PUBLIC INT4
V3OspfModuleStart (VOID)
{
    tV3OsAreaId         bBoneAreaId;

    MEMSET (&gV3OsRtr, OSPFV3_ZERO, sizeof (tV3OsRtr));
    gu1O3HelloSwitchOverFlag = 0;
#ifdef TRACE_WANTED
    OSPFV3_GBL_TRC_FLAG = OSPFV3_CRITICAL_TRC;
#endif

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfModuleStart\n");

    /* Create buffer pools for data structures */
    if (V3OspfMemInitForModuleStart () == OSIX_FAILURE)
    {
        V3OspfMemDeInitForModuleShut ();
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "Memory allocation failed for OSPFv3 MemInit\n");
        return OSIX_FAILURE;
    }

    gV3OsRtr.ospfRedInfo.u4PrevRmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_INIT;
    gV3OsRtr.ospfRedInfo.i4HsAdminStatus = OSPFV3_DISABLED;

    if (V3OspfRegisterWithRM () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OS_RESOURCE_TRC | OSPFV3_CRITICAL_TRC,
                        "RM Registration failed\n");
        return OSIX_FAILURE;
    }

    gV3OsRtr.pIfRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsInterface, RbNode)),
                              V3RBInterfaceCmpFunc);

    if (gV3OsRtr.pIfRBRoot == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Interface RB Tree Creation Failed\n");
        return OSIX_FAILURE;
    }

    /* Create the timer list */
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, OSPFV3_TIMER_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.timerLstId))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "Timer list creation failed\n");
        return OSIX_FAILURE;
    }
    /* Create the Hello timer list */
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, OSPFV3_HELLO_TIMER_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.hellotimerLstId))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "Hello Timer list creation failed\n");
        return OSIX_FAILURE;
    }
    if (TmrCreateTimerList (OSPFV3_TASK_NAME, TM50MS_TIMER_EXP_EVENT, NULL,
                            (tTimerListId *) & (gV3OsRtr.g50msTimerLst))
        != TMR_SUCCESS)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "Timer list creation failed\n");
        return OSIX_FAILURE;
    }
    /* Initialise the VRF SPF DLL */
    TMO_DLL_Init (&(gV3OsRtr.vrfSpfList));

    /* Initialize the schedule queue. The first paramter is the context
     * parameter which is set for initializing particular context schedule
     * queue. The second parameter is the interface pointer which is set
     * for initializing particular interface schedule queue. In task
     * initialization, since all the queue should be initialized, both
     * the parameters are passed as NULL
     */
    V3IsmInitSchedQueueInCxt (NULL, NULL);

    /* Initialise the timer related structures */
    V3TmrInitTimerDesc ();

    /* The default context is created during task creation. This context
     * can neither be created or destroyed by the administrator
     */
    if (V3RtrCreateCxt (OSPFV3_DEFAULT_CXT_ID) == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC,
                        "Default context creation failed\n");
        return OSIX_FAILURE;
    }

    /* If the system is operating in SI mode, the default context pointer
     * is set in the global pointer
     */
    if (V3OspfGetSystemModeExt (OSPF3_PROTOCOL_ID) == OSPFV3_SI_MODE)
    {
        gV3OsRtr.pV3OspfCxt = gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID];
    }

    /* Relinquish related initialization */
    gV3OsRtr.u4RTStaggeringStatus = OSPFV3_STAGGERING_ENABLED;
    gV3OsRtr.u4RTStaggeredCtxId = OSPFV3_INVALID_CXT_ID;

    /* Intiailize the VRF interval to the default value */
    gV3OsRtr.u4VrfSpfInterval = OSPFV3_DEF_VRF_SPF_INTERVAL;
    MEMSET (&bBoneAreaId, 0, OSPFV3_AREA_ID_LEN);

    gV3OsRtr.bIgnoreExpiredTimers = OSIX_FALSE;

#ifdef ROUTEMAP_WANTED
    /* register callback for route map updates */
    RMapRegister (RMAP_APP_OSPF3, V3OspfSendRouteMapUpdateMsg);
#endif

#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    RegisterOspf3Mibs ();
#endif

    /* Register with VCM module to handle the context deletion indication */
    if (V3OspfRegisterWithVcm () == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        "VCM registration failed\r\n");
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfModuleStart\n");
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function     : V3OspfMemInitForModuleStart                                */
/*                                                                           */
/* Description  : This function is used to initialize all the memory pools   */
/*                required to start the ospfv3 Module                        */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*****************************************************************************/

PRIVATE INT4
V3OspfMemInitForModuleStart (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMemInitForModuleStart\n");

#ifdef CLI_WANTED
    if (CsrMemAllocation (OSPF3_MODULE_ID) == CSR_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:V3OspfMemInitForModuleStart\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3OspfMemDeInitForModuleShut                               */
/*                                                                           */
/* Description  : This function is used to Deinitialize the memory pools     */
/*                which are allocated on failure during init                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NONE                                                       */
/*****************************************************************************/

PRIVATE VOID
V3OspfMemDeInitForModuleShut (VOID)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMemDeInitOnInitFail\n");

#ifdef CLI_WANTED
    CsrMemDeAllocation (OSPF3_MODULE_ID);
#endif
    Ospfv3SizingMemDeleteMemPools ();

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfMemDeInitOnInitFail\n");

}

/************************************************************************/
/*  Function Name   : V3OspfMemReleaseMemBlock                            */
/*  Description     : Stops any stray timer and releases a block        */
/*                    back to a specified Pool/Heap                     */
/*  Input(s)        :                                                   */
/*                  : PoolId   - Pool to which to be released.          */
/*  Output(s)       : pu1Block - Pointer to block being released.       */
/*  Returns         : MEM_SUCCESS/MEM_FAILURE                           */
/************************************************************************/
UINT4
V3OspfMemReleaseMemBlock (tMemPoolId PoolId, UINT1 *pu1Block)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3OspfMemReleaseMemBlock\n");

    if (pu1Block == NULL)
    {
        return MEM_FAILURE;
    }

    if (PoolId == OSPFV3_CXT_QID)
    {
        tV3OspfCxt         *pV3OspfCxt = (tV3OspfCxt *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&(pV3OspfCxt->runRtTimer));
        V3TmrDeleteTimer (&(pV3OspfCxt->exitOverflowTimer));
        V3TmrDeleteTimer (&(pV3OspfCxt->graceTimer));
    }

    if (PoolId == OSPFV3_LSA_INFO_QID)
    {
        tV3OsLsaInfo       *pLsaInfo = (tV3OsLsaInfo *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&pLsaInfo->lsaAgingTimer);
    }

    if (PoolId == OSPFV3_LSA_DESC_QID)
    {
        tV3OsLsaDesc       *pLsaDesc = (tV3OsLsaDesc *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&pLsaDesc->minLsaIntervalTimer);
    }

    if (PoolId == OSPFV3_NBR_QID)
    {
        tV3OsNeighbor      *pNbr = (tV3OsNeighbor *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&(pNbr->lsaReqDesc.lsaReqRxmtTimer));
        V3TmrDeleteTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer));
        V3TmrDeleteTimer (&(pNbr->dbSummary.ddTimer));
        V3TmrDeleteTimer (&(pNbr->inactivityTimer));
        V3TmrDeleteTimer (&(pNbr->helperGraceTimer));
    }

    if (PoolId == OSPFV3_IF_QID)
    {
        tV3OsInterface     *pInterface = (tV3OsInterface *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&pInterface->delLsAck.delAckTimer);

        V3TmrDeleteTimer (&(pInterface->helloTimer));
        V3TmrDeleteTimer (&(pInterface->waitTimer));
        V3TmrDeleteTimer (&(pInterface->pollTimer));
        V3TmrDeleteTimer (&(pInterface->MultIfActiveDetectTimer));
        V3TmrDeleteTimer (&(pInterface->nbrProbeTimer));
    }

    if (PoolId == OSPFV3_AREA_QID)
    {
        tV3OsArea          *pArea = (tV3OsArea *) (VOID *) pu1Block;
        V3TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3OspfMemReleaseMemBlock\n");

    return (MemReleaseMemBlock ((tMemPoolId) PoolId, (UINT1 *) pu1Block));
}

#ifdef OSPF3_BCMXNP_HELLO_WANTED
/*****************************************************************************/
/* Function     : V3OspfProcessNpHelloPacketQMsg                             */
/*                                                                           */
/* Description  : This function is used to process OSPFV3 Hello packet rcvd  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : NONE                                                       */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessNpHelloPacketQMsg (UINT1 u1Flag)
{
    tV3OspfQMsg        *pOspfQMsg = NULL;
    tCRU_INTERFACE      IfaceId;
    UINT4               i4MsgRetVal = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4Port = 0;
    UINT4               u4VlanIfIndex = CFA_INVALID_INDEX;
    UINT4               u4LowPriMessageCount = 0;
    tOsixMsg           *pOsixMsg = NULL;
    UINT2               VlanId = 0;
    UINT2               u2VlanTag = 0;
    UINT1               u1HPMsgCnt = 0;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;

    if (OSPFV3_QMSG_ALLOC (&(pOspfQMsg)) == NULL)
    {
        OSPFV3_GBL_TRC1 (OS_RESOURCE_TRC | OSPFV3_ISM_TRC, " Alloc Failure\n",
                         OSPFV3_INVALID_CXT_ID);
        return;
    }

    while ((i4MsgRetVal = OsixQueRecv (gV3OsRtr.Ospf3NPHelloQId,
                                       (UINT1 *) &pOsixMsg, OSIX_DEF_MSG_LEN,
                                       (INT4) 0)) == OSIX_SUCCESS)
    {
        u4Port = pOsixMsg->ModuleData.InterfaceId.u4IfIndex;
        CfaGetIfBridgedIfaceStatus (u4Port, &u1BridgedIfaceStatus);
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            /*  All packets will have a VLAN tag. Retrieve the VLAN from the Ethernet packet and get
             *  CFA IP interface index associated with the VLAN.
             */
            CRU_BUF_Copy_FromBufChain (pOsixMsg, (UINT1 *) &VlanId,
                                       VLAN_TAG_VLANID_OFFSET, VLAN_TAG_SIZE);
            u2VlanTag = (UINT2) OSIX_NTOHS (VlanId);
            /* Isolate the VLAN Id from the TCI field  */
            VlanId = (tVlanId) (u2VlanTag & VLAN_ID_MASK);
            u4VlanIfIndex = CfaGetVlanInterfaceIndex (VlanId);

            if (u4VlanIfIndex == CFA_INVALID_INDEX)
            {
                if (VlanGetMbsmUpdateOnCardInsertionStatus () == TRUE)
                {
                    u4VlanIfIndex = (UINT4) CfaGetVlanIfIndexWithoutLock
                        (u4Port, pOsixMsg, (tVlanIfaceVlanId *) & VlanId);
                }
                else
                {
                    u4VlanIfIndex = (UINT4) CfaGetVlanIfIndex
                        (u4Port, pOsixMsg, (tVlanIfaceVlanId *) & VlanId);
                }
            }
            u4IfIndex = CFA_IF_IPPORT (u4VlanIfIndex);
        }
        else
        {
            u4IfIndex = CFA_IF_IPPORT (u4Port);
        }

        if (CRU_BUF_Move_ValidOffset (pOsixMsg, (CFA_ENET_V2_HEADER_SIZE + 4))
            != CRU_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pOsixMsg, OSPFV3_NORMAL_RELEASE);
            pOsixMsg = NULL;
            OSPFV3_QMSG_FREE (pOspfQMsg);
            return;                /* buffer is released by GDD */
        }
        MEMSET (&IfaceId, 0, sizeof (tCRU_INTERFACE));
        IpifGetIfId (u4IfIndex, &IfaceId);

        MEMSET (pOspfQMsg, 0, sizeof (tV3OspfQMsg));

        /* This need to be verified for Router port and L3 VLAN interface */
        CRU_BUF_Set_InterfaceId (pOsixMsg, IfaceId);

        pOspfQMsg->u4OspfMsgType = OSPFV3_PKT_ARRIVAL_EVENT;
        if (u1BridgedIfaceStatus == CFA_ENABLED)
        {
            pOspfQMsg->unOspfMsgType.ospfIpIfParam.u4IfIndex = u4VlanIfIndex;
        }
        else
        {
            pOspfQMsg->unOspfMsgType.ospfIpIfParam.u4IfIndex = u4Port;
        }
        pOspfQMsg->unOspfMsgType.ospfIpIfParam.u4PktLen = (UINT4)
            ((pOsixMsg->pFirstValidDataDesc->u4_ValidByteCount) - IP6_HDR_LEN);

        pOspfQMsg->unOspfMsgType.ospfIpIfParam.pChainBuf =
            (tCRU_BUF_CHAIN_HEADER *) pOsixMsg;

        if (NetIpv4GetCxtId ((u4IfIndex), &u4ContextId) == NETIPV4_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pOsixMsg, OSPFV3_NORMAL_RELEASE);
            OSPFV3_QMSG_FREE (pOspfQMsg);
            OSPFV3_GBL_TRC1 (CONTROL_PLANE_TRC,
                             "Invalid Context\n", OSPFV3_INVALID_CXT_ID);
            return;
        }

        pOspfQMsg->u4OspfCxtId = u4ContextId;

        if (i4MsgRetVal == OSIX_SUCCESS)
        {
            OSPFV3_GBL_TRC2 (CONTROL_PLANE_TRC | OSPFV3_CONFIGURATION_TRC,
                             "Rx SNMP Msg %x\n", pOspfQMsg,
                             OSPFV3_INVALID_CXT_ID);

            u1HPMsgCnt++;
            if (u1HPMsgCnt >= MAX_OSPF3_HP_MSGS)
            {
                /* process the low priority messages only when
                 * RTC is not relinquished
                 */
                if (u1Flag == OSIX_TRUE)
                {
                    OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4LowPriMessageCount);

                    if (u4LowPriMessageCount != 0)
                    {
                        V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
                    }
                }
                u1HPMsgCnt = 0;
            }

#ifndef RAWSOCK_WANTED
            V3OspfDeqPkt (&(pOspfQMsg->unOspfMsgType.ospfIpIfParam));
#endif
        }
    }
    OSPFV3_QMSG_FREE (pOspfQMsg);
    return;
}
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfProcessLowPriQMsg                         */
/*                                                                           */
/* Description  : This function gets the packets from the low priority queue */
/*                and processes them. This function is called only when the  */
/*                OSPF queue is empty or the low priority queue is full.     */
/*                Only 5% of the packets present in the low priority queue   */
/*                are processed and only if the OSPF queue is empty, the     */
/*                next 5% of the packets are considered for processing.      */
/*                                                                           */
/* Input        : au1QName: the low priority queue name                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessLowPriQMsg (tOsixQId QId)
{
    tV3OspfLPQMsg      *pOspfLowPriQMsg = NULL;
    UINT4               u4LowPriMessageCount = 0;
    UINT4               u4IncrCtr = 0;
    tOsixMsg           *pOsixMsg = NULL;
    UINT1              *pPkt = NULL;
    UINT1               u1Flag = 0;
    UINT4               u4ContextId = 0;

    OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4LowPriMessageCount);
    if (gV3OsRtr.pV3OspfCxt != NULL)
    {
        u4ContextId = gV3OsRtr.pV3OspfCxt->u4ContextId;
        if (gV3OsRtr.apV3OspfCxt[u4ContextId]->u4RouteCalcCompleted !=
            OSPFV3_ROUTE_CALC_COMPLETED)
        {
            /* route calculation is suspended
             * Now processing of low prioirty message can trigger route calc
             * so return without processing packets from low prioirty Q */
            return;

        }
    }
    while (OsixQueRecv (QId, (UINT1 *) &pOsixMsg, OSIX_DEF_MSG_LEN, (UINT4) 0)
           == OSIX_SUCCESS)
    {
        pOspfLowPriQMsg = (tV3OspfLPQMsg *) pOsixMsg;
        u4IncrCtr++;

        switch (pOspfLowPriQMsg->u4OspfMsgType)
        {

            case OSPFV3_IPV6_IF_STAT_CHG_EVENT:
                V3OspfIfStatusChgHdlr (&(pOspfLowPriQMsg->unOspfMsgType.
                                         ospfIpIfStatParam));

                break;

            case OSPFV3_PKT_ARRIVAL_EVENT:
                pPkt =
                    CRU_BUF_Get_DataPtr_IfLinear (pOspfLowPriQMsg->
                                                  unOspfMsgType.ospfIpIfParam.
                                                  pChainBuf, 0,
                                                  pOspfLowPriQMsg->
                                                  unOspfMsgType.ospfIpIfParam.
                                                  u4PktLen);

                if (pPkt == NULL)
                {
                    if (pOspfLowPriQMsg->unOspfMsgType.ospfIpIfParam.u4PktLen <=
                        OSPFV3_MAX_MSG_SIZE)
                    {
                        pPkt =
                            V3UtilOsMsgAlloc (pOspfLowPriQMsg->unOspfMsgType.
                                              ospfIpIfParam.u4PktLen);

                        if (pPkt != NULL)
                        {
                            u1Flag = 1;
                            CRU_BUF_Copy_FromBufChain (pOspfLowPriQMsg->
                                                       unOspfMsgType.
                                                       ospfIpIfParam.pChainBuf,
                                                       pPkt, 0,
                                                       pOspfLowPriQMsg->
                                                       unOspfMsgType.
                                                       ospfIpIfParam.u4PktLen);
                        }

                    }
                }
                if (pPkt != NULL)
                {
                    V3OspfProcessLPOSPFPacket ((pPkt),
                                               (UINT2) pOspfLowPriQMsg->
                                               unOspfMsgType.ospfIpIfParam.
                                               u4PktLen,
                                               pOspfLowPriQMsg->unOspfMsgType.
                                               ospfIpIfParam.pNbr);
                }
                if (u1Flag == 1)
                {
                    V3UtilOsMsgFree (pPkt);
                }

                CRU_BUF_Release_MsgBufChain (pOspfLowPriQMsg->unOspfMsgType.
                                             ospfIpIfParam.pChainBuf, FALSE);
                break;

            case OSPFV3_VCM_CHG_EVENT:
                V3OspfProcessVcmChgMsg (&pOspfLowPriQMsg->unOspfMsgType.
                                        ospfVcmInfo);
                break;
            default:
                break;
        }
        OSPFV3_LPQMSG_FREE (pOspfLowPriQMsg);
        if (u4IncrCtr >= MAX_OSPF3_LP_MSGS)
        {
            break;
        }

    }
    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Send Evt Failed\n");
    }
    V3OspfCpuRelinquishHello ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfCpuRelinquishHello                                   */
/*                                                                           */
/* Description  : This function processes Hello timer expiry                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfCpuRelinquishHello ()
{
    V3TmrHandleHelloExpiry ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfSendMsgToLowPriorityQ                                */
/*                                                                           */
/* Description  : This function processes messages posted in low priority Q  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfSendMsgToLowPriorityQ (tV3OspfQMsg * pOspfQMsg)
{
    tV3OspfLPQMsg      *pOspfLowPriQMsg = NULL;
    UINT4               u4PktCount = 0;

    if (OSPFV3_LPQMSG_ALLOC (&pOspfLowPriQMsg) == NULL)
    {
        return;
    }

    MEMSET (pOspfLowPriQMsg, 0, sizeof (tV3OspfLPQMsg));

    if (pOspfQMsg->u4OspfMsgType == OSPFV3_IPV6_IF_STAT_CHG_EVENT)
    {
        pOspfLowPriQMsg->u4OspfMsgType = pOspfQMsg->u4OspfMsgType;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4Index
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Index;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mtu
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mtu;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfSpeed
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfSpeed;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfStat
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4IfStat;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4OperStatus
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4OperStatus;
        pOspfLowPriQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mask
            = pOspfQMsg->unOspfMsgType.ospfIpIfStatParam.u4Mask;

    }
    else if (pOspfQMsg->u4OspfMsgType == OSPFV3_VCM_CHG_EVENT)
    {
        pOspfLowPriQMsg->u4OspfMsgType = pOspfQMsg->u4OspfMsgType;
        pOspfLowPriQMsg->unOspfMsgType.ospfVcmInfo.u4IpIfIndex
            = pOspfQMsg->unOspfMsgType.ospfVcmInfo.u4IpIfIndex;
        pOspfLowPriQMsg->unOspfMsgType.ospfVcmInfo.u4VcmCxtId
            = pOspfQMsg->unOspfMsgType.ospfVcmInfo.u4VcmCxtId;
        pOspfLowPriQMsg->unOspfMsgType.ospfVcmInfo.u1BitMap
            = pOspfQMsg->unOspfMsgType.ospfVcmInfo.u1BitMap;
    }
    else
    {
        /* Invalid message type */
        OSPFV3_LPQMSG_FREE (pOspfLowPriQMsg);
        return;
    }

    pOspfLowPriQMsg->u4OspfCxtId = pOspfQMsg->u4OspfCxtId;

    if (OsixQueSend
        (gV3OsRtr.Ospf3LPQId, (UINT1 *) (&pOspfLowPriQMsg),
         OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        OSPFV3_LPQMSG_FREE (pOspfLowPriQMsg);
        return;
    }

    OsixQueNumMsg (gV3OsRtr.Ospf3LPQId, &u4PktCount);

    /* To support from system.size , the macro OSPF_LOW_PRIORITY_QUEUE_SIZE
     * is replaced with sizing variable
     * FsOSPFSizingParams[MAX_OSPF_QUE_MSGS_SIZING_ID].u4PreAllocatedUnits/2
     */
    if (u4PktCount ==
        (FsOSPFV3SizingParams[MAX_OSPFV3_LP_QUEUE_SIZE_SIZING_ID].
         u4PreAllocatedUnits / 2))
    {
        V3OspfProcessLowPriQMsg (gV3OsRtr.Ospf3LPQId);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfProcessLPOSPFPacket                                  */
/*                                                                           */
/* Description  : This function processes messages posted in low priority Q  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessLPOSPFPacket (UINT1 *pPkt, UINT2 u2Len, tV3OsNeighbor * pNbr)
{
    UINT1               u1Type = 0;

    OSPFV3_BUFFER_GET_1_BYTE (pPkt, OSPFV3_TYPE_OFFSET, u1Type);
    switch (u1Type)
    {
        case OSPFV3_DD_PKT:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received DDP From\n");
            OSPFV3_COUNTER_OP (pNbr->pInterface->u4DdpRcvdCount, 1);
            V3DdpRcvDdp (pPkt, u2Len, pNbr);
            break;

        case OSPFV3_LSA_REQ_PKT:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LSR\n");
            OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaReqRcvdCount, 1)
                V3LrqRcvLsaReq (pPkt, u2Len, pNbr);
            break;

        case OSPFV3_LS_UPDATE_PKT:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LSU\n");
            OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaUpdateRcvdCount, 1);
            V3LsuRcvLsUpdate (pPkt, pNbr);
            break;

        case OSPFV3_LSA_ACK_PKT:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Received LAK\n");
            OSPFV3_COUNTER_OP (pNbr->pInterface->u4LsaAckRcvdCount, 1);
            V3LakRcvLsAck (pPkt, u2Len, pNbr);
            break;

        default:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Rcvd Junk\n");
            OSPFV3_INC_DISCARD_OSPF_PKT (pNbr->pInterface->pArea->pV3OspfCxt);
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC | OSPFV3_PPP_TRC,
                            "Pkt Disc (Invalid Pkt Type)\n");
            return;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfRmHandleGoActiveNotify                               */
/*                                                                           */
/* Description  : This function handles hello timer processing for go active */
/*                event from RM                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
V3OspfRmHandleGoActiveNotify (VOID)
{
    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_GO_ACTIVE_RM_NOTIFY_EVENT) !=
        OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_CRITICAL_TRC, "Send Evt Failed\n",
                         OSPFV3_INVALID_CXT_ID);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3OspfProcessClearMsg                                      */
/*                                                                           */
/* Description  : Process clear Ospfv3 process                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3OspfProcessClearMsg (VOID)
{
    UINT4               u4ContextId = OSPFV3_DEFAULT_CXT_ID;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsRouterId       rtrId;

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        OSPFV3_GBL_TRC (OSPFV3_CRITICAL_TRC, "Invalid context \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID, "Invalid context"));
        return;
    }
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC : V3OspfProcessClearMsg \n");

    gu4ClearFlag = OSIX_TRUE;
    if (V3RtrDisableInCxt (pV3OspfCxt, OSPFV3_TRUE) == OSPFV3_FAILURE)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "failed to disable router \n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to disable router"));

        return;
    }
    if (V3OspfRmGetNodeState () != RM_STANDBY)
    {
        if (O3RedDynOspfClearToStandby (OSIX_TRUE, pV3OspfCxt->u4ContextId) ==
            OSPFV3_FAILURE)
        {
            OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Clear ipv6 ospf process sync failed \n");

            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Clear ipv6 ospf process sync failed"));
        }
        if (pV3OspfCxt->u1RestartExitReason != OSPFV3_RESTART_INPROGRESS)
        {
            if (pV3OspfCxt->u1RouterIdStatus == OSPFV3_ROUTERID_DYNAMIC)
            {
                MEMSET (&rtrId, 0, sizeof (tV3OsRouterId));
                if (V3UtilSelectOspfRouterId (pV3OspfCxt->u4ContextId,
                                              &rtrId) == OSPFV3_SUCCESS)
                {
                    OSPFV3_IPV4_ADDR_COPY (&(pV3OspfCxt->rtrId), &(rtrId));
                }
                if (O3RedDynSendRtrIdToStandby (rtrId, pV3OspfCxt->u4ContextId)
                    == OSPFV3_FAILURE)
                {
                    OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                                pV3OspfCxt->u4ContextId,
                                "Failed to sync RouterId to standby \n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                                  "Failed to sync RouterId to standby"));
                }

            }
        }
    }

    if (V3RtrEnableInCxt (pV3OspfCxt, OSPFV3_TRUE) == OSPFV3_FAILURE)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Enable Router failed \n");

        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Enable Router failed"));
        return;
    }
    gu4ClearFlag = OSIX_FALSE;
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3main.c                       */
/*-----------------------------------------------------------------------*/
#endif /*_OS3MAIN_C */
