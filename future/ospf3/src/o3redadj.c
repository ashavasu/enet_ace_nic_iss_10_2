/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3redadj.c,v 1.12 2017/12/26 13:34:28 siva Exp $
 *
 * Description: 
 ********************************************************************/
#include "o3inc.h"

/* Prototypes */

PRIVATE INT4        O3RedAdjGetNbrStateInfo
PROTO ((tV3OsNeighbor * pNbr, tV3OsRmNbrState * pNbrStateInfo));

PRIVATE INT4        O3RedAdjProcessNbrStateInfo
PROTO ((tRmMsg * pRmMsg, UINT4 *pu4Offset));

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjProcessBulkHello                                   */
/*                                                                           */
/* Description  : This  function is called for the bulk hello received from  */
/*                from the active node                                       */
/*                                                                           */
/* Input        : pRmMsg                                                     */
/*                                                                           */
/* Output       : pu4Offset  -  Offset read in the hello bulk update         */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjProcessBulkHello (tRmMsg * pMsg, UINT4 *pu4Offset)
{
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4IfIndex;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface     *pAssoIface = NULL;
    tIp6Addr            SrcIp6Addr;
    tV3OsAreaId         areaId;
    tV3OsRouterId       headerRtrId;
    UINT2               u2PktLen = OSPFV3_ZERO;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedAdjProcessBulkHello\r\n");

    MEMSET (&SrcIp6Addr, OSPFV3_ZERO, sizeof (tIp6Addr));

    OSPFV3_RED_GET_2_BYTE (pMsg, u4Offset, u2PktLen);

    /* Get the packet */
    MEMSET (gV3OsRtr.au1Pkt, 0, OSPFV3_MAX_HELLO_PKT_LEN);

    OSPFV3_RED_GET_N_BYTE (pMsg, gV3OsRtr.au1Pkt, u4Offset, u2PktLen);
    OSPFV3_RED_GET_4_BYTE (pMsg, u4Offset, u4IfIndex);
    OSPFV3_RED_GET_N_BYTE (pMsg, &SrcIp6Addr, u4Offset, OSPFV3_IPV6_ADDR_LEN);

    /* Find the Interface structure pointer corresponding to that
       IfIndex */
    if ((pInterface = V3GetFindIf (u4IfIndex)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "OSPFv3 Packet Discarded. Assocoated Interface not found\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "OSPFv3 Pkt Disc Associated If Not Found\r\n");
        gu4O3RedAdjProcessBulkHelloFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Processing hello on interface : %d\n",
                     pInterface->u4InterfaceId);

    /* If the area id in the hello packet is 0.0.0.0 and the interface
     * is not associated with the area 0.0.0.0, then the hello packet
     * belongs to the virtual interface
     */
    OSPFV3_BUFFER_GET_STRING (gV3OsRtr.au1Pkt, areaId, OSPFV3_AREA_ID_OFFSET,
                              OSPFV3_AREA_ID_LEN);

    if (V3UtilAreaIdComp (areaId, pInterface->pArea->areaId) != OSPFV3_EQUAL)
    {
        if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
        {
            /* Get the router id of the from the OSPF header */
            OSPFV3_BUFFER_GET_STRING (gV3OsRtr.au1Pkt, headerRtrId,
                                      OSPFV3_RTR_ID_OFFSET, OSPFV3_RTR_ID_LEN);

            /* associate with virtual interface */
            if ((pAssoIface = V3PppAssociateWithVirtualIf (pInterface,
                                                           &headerRtrId)) ==
                NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "OSPFv3 Packet Discarded. Associated"
                              "virtual interface not found\n"));

                OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "OSPFv3 Pkt Discarded, Virtual interface "
                                "not found\r\n");
                gu4O3RedAdjProcessBulkHelloFail++;
                return OSIX_FAILURE;
            }

            pInterface = pAssoIface;
        }
        else
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "OSPFv3 Packet Discarded due to "
                          "Area mismatch.\n"));

            OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                            pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "OSPFv3 Pkt Discarded due to area mismatch\r\n");
            gu4O3RedAdjProcessBulkHelloFail++;
            return OSIX_FAILURE;
        }
    }

    V3HpRcvHello (gV3OsRtr.au1Pkt, u2PktLen, pInterface, &SrcIp6Addr);
    gV3OsRtr.ospfRedInfo.u4HelloSynCount++;
    *pu4Offset = u4Offset;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjProcessBulkHello\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjGetNbrStateInfo                                    */
/*                                                                           */
/* Description  : This  function is called for getiing the Nbr state info    */
/*                of the given Neighbour                                     */
/*                                                                           */
/* Input        : pNbr - Pointer to Neighbour Structure                      */
/*                pNbrStateInfo - Pointer to tV3OsRmHello Structure          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
O3RedAdjGetNbrStateInfo (tV3OsNeighbor * pNbr, tV3OsRmNbrState * pNbrStateInfo)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTRY : O3RedAdjGetNbrStateInfo\r\n");

    if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
    {
        MEMCPY (pNbrStateInfo->nbrId, pNbr->nbrRtrId, sizeof (pNbr->nbrRtrId));
        pNbrStateInfo->u4IfIndex = pNbr->pInterface->u4InterfaceId;
        pNbrStateInfo->u1CurrState = pNbr->u1NsmState;
        MEMCPY (pNbrStateInfo->transitAreaId, pNbr->pInterface->transitAreaId,
                sizeof (pNbr->pInterface->transitAreaId));
        MEMCPY (pNbrStateInfo->destRtrId, pNbr->pInterface->destRtrId,
                sizeof (pNbr->pInterface->destRtrId));
        pNbrStateInfo->u1NetworkType = pNbr->pInterface->u1NetworkType;
        pNbrStateInfo->u4ContextId
            = pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId;

        OSPFV3_TRC (OSPFV3_FN_EXIT,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT : O3RedAdjGetNbrStateInfo\r\n");

        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjProcessNbrStateInfo                                */
/*                                                                           */
/* Description  : This  function is called for processing the Nbr state info */
/*                of the given Neighbour                                     */
/*                                                                           */
/* Input        : pRmMsg - Pointer to RM Bulk update packet                  */
/*                                                                           */
/* Output       : pu4Offset - Bytes read from the packet                     */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjProcessNbrStateInfo (tRmMsg * pRmMsg, UINT4 *pu4Offset)
{
    tV3OsRmNbrState     NbrStateInfo;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT4               u4Offset = *pu4Offset;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedAdjProcessNbrStateInfo\r\n");
    MEMSET (&NbrStateInfo, OSPFV3_ZERO, sizeof (tV3OsRmNbrState));

    /* Copy the context id */
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, NbrStateInfo.u4ContextId);

    /* Copy the nbr Id */
    OSPFV3_RED_GET_N_BYTE (pRmMsg, NbrStateInfo.nbrId,
                           u4Offset, sizeof (NbrStateInfo.nbrId));

    /* Copy the interface index */
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, NbrStateInfo.u4IfIndex);

    /* Copy the neigbor state */
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, NbrStateInfo.u1CurrState);

    /* Copy the network type */
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, NbrStateInfo.u1NetworkType);

    /* Copy the transit area id */
    OSPFV3_RED_GET_N_BYTE (pRmMsg, NbrStateInfo.transitAreaId,
                           u4Offset, sizeof (NbrStateInfo.transitAreaId));

    /* Copy the dest router id */
    OSPFV3_RED_GET_N_BYTE (pRmMsg, NbrStateInfo.destRtrId,
                           u4Offset, sizeof (NbrStateInfo.destRtrId));

    pV3OspfCxt = V3UtilOspfGetCxt (NbrStateInfo.u4ContextId);

    if (NbrStateInfo.u1NetworkType != OSPFV3_IF_VIRTUAL)
    {
        pInterface = V3GetFindIf (NbrStateInfo.u4IfIndex);
    }
    else
    {
        if (pV3OspfCxt != NULL)
        {
            pInterface = V3GetFindVirtIfInCxt
                (pV3OspfCxt, &NbrStateInfo.transitAreaId,
                 &NbrStateInfo.destRtrId);
        }
    }

    if (pInterface == NULL)
    {
        gu4O3RedAdjProcessNbrStateInfoFail++;
        return OSIX_FAILURE;
    }

    if ((pNbr = V3HpSearchNbrLst (&NbrStateInfo.nbrId, 0, pInterface)) == NULL)
    {
        gu4O3RedAdjProcessNbrStateInfoFail++;
        return OSIX_FAILURE;
    }

    V3NbrUpdateState (pNbr, (UINT1) OSPFV3_NBRS_FULL);

    *pu4Offset = u4Offset;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjProcessNbrStateInfo\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjAddLastHelloDetails                                */
/*                                                                           */
/* Description  : The function adds the hello details received from the Nbr  */
/*                If standby is up and static bulk update is over, it sends  */
/*                the detail across to standby                               */
/*                                                                           */
/* Input        : pNbr             : The neighbor from which hello           */
/*                                   is received                             */
/*                u4InterfaceId    : the interface at which the hello        */
/*                pIp6SrcAddr      : the IPv6 Src addr of the nbr            */
/*                pHelloPkt        : The hello packet received from Nbr      */
/*                u2PktLen         : The length of the hello Pkt             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedAdjAddLastHelloDetails (tV3OsNeighbor * pNbr, UINT4 u4InterfaceId,
                             tIp6Addr * pSrcIp6Addr, UINT1 *pHelloPkt,
                             UINT2 u2PktLen)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedAdjAddLastHelloDetails\r\n");

    pNbr->lastHelloInfo.u4IfIndex = u4InterfaceId;
    pNbr->lastHelloInfo.u2Len = u2PktLen;

    MEMSET (pNbr->lastHelloInfo.au1Pkt, 0, OSPFV3_MAX_HELLO_PKT_LEN);
    MEMCPY (pNbr->lastHelloInfo.au1Pkt, pHelloPkt, u2PktLen);

    OSPFV3_IP6_ADDR_COPY ((pNbr->lastHelloInfo.SrcIp6Addr), (*pSrcIp6Addr));

    O3RedDynSendHelloToStandby (pNbr);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3RedAdjAddLastHelloDetails\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjSendBulkNbrInfo                                    */
/*                                                                           */
/* Description  : This function constructs and Sends out the bulk update     */
/*                for hello and neigbhor state                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pi4SendSubBulkEvent - indicates whether SUBBULK UDATE EVENT*/
/*                                      needs to be sent out or not          */
/*                                      Will be set to OSIX_SUCCESS, when the*/
/*                                      SUBBULK UDATE EVENT needs to sent    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjSendBulkNbrInfo (INT4 *pi4SendSubBulkEvent)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface      curInterface;
    tRmMsg             *pBuf = NULL;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT4               u4PktLen = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedAdjSendBulkNbrInfo\r\n");

    MEMSET (&curInterface, 0, sizeof (tV3OsInterface));

    curInterface.u4InterfaceId = gV3OsRtr.ospfRedInfo.u4LastIfIndex;

    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_NBR;

    OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                     gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->u4ContextId,
                     "Last send interface id : %d\r\n",
                     gV3OsRtr.ospfRedInfo.u4LastIfIndex);

    *pi4SendSubBulkEvent = OSIX_FAILURE;
    /* Allocate the Buffer for bulk update */
    if (O3RedAdjAllocateBulkUpdatePkt (&pBuf, &u4Offset, &u4PktLen)
        == OSIX_FAILURE)
    {
        gu4O3RedAdjSendBulkNbrInfoFail++;
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjSendBulkNbrInfo\r\n");
        return OSIX_FAILURE;
    }

    while ((pInterface = RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                        (tRBElem *) & curInterface,
                                        NULL)) != NULL)
    {
        curInterface.u4InterfaceId = pInterface->u4InterfaceId;

        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Starting Interface : %d\r\n",
                         pInterface->u4InterfaceId);

        if ((pInterface->operStatus == OSPFV3_DISABLED) ||
            (pInterface->admnStatus == OSPFV3_DISABLED))
        {
            continue;
        }

        if (O3RedIfSendIntfInfo (pInterface, &pBuf,
                                 &u4Offset, &u4PktLen) == OSIX_FAILURE)
        {
            if (pBuf != NULL)
            {
                RM_FREE (pBuf);
            }

            gu4O3RedAdjSendBulkNbrInfoFail++;
            return OSIX_FAILURE;
        }

        if (O3RedAdjSendNbrInfo (pInterface, &pBuf,
                                 &u4Offset, &u4PktLen) == OSIX_FAILURE)
        {
            if (pBuf != NULL)
            {
                RM_FREE (pBuf);
            }
            gu4O3RedAdjSendBulkNbrInfoFail++;
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                            "EXIT : O3RedAdjSendBulkNbrInfo\r\n");
            return OSIX_FAILURE;
        }
        if (O3RedUtlBulkUpdtRelinquish () == OSIX_SUCCESS)
        {
            /* Reached the max interfaces count. 
             * Implemented on considering relinguish factor.
             * Last sent Interface details are stored and
             * OSPFV3_RM_SUB_BULK_EVENT is sent to OSPFV3 task */
            gV3OsRtr.ospfRedInfo.u4LastIfIndex = pInterface->u4InterfaceId;
            *pi4SendSubBulkEvent = OSIX_SUCCESS;
            break;
        }
    }                            /* End of while loop for interface */

    if (u4PktLen == OSPFV3_RED_MSG_HDR_LEN)
    {
        /* This packet contains only RM Header
         * No dynamic bulk update information is added in this buffer
         */
        if (pBuf != NULL)
        {
            RM_FREE (pBuf);
        }
    }
    else
    {
        /* Send out the constructed packet */
        if (O3RedUtlSendBulkUpdate (pBuf, u4PktLen) == OSIX_FAILURE)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                            "EXIT : O3RedAdjSendBulkNbrInfo\r\n");
            gu4O3RedAdjSendBulkNbrInfoFail++;
            return OSIX_FAILURE;
        }
    }
    if (*pi4SendSubBulkEvent == OSIX_SUCCESS)
    {
        /* Sub Bulk event needs to be sent out to relinquish the 
         * current bulk update thread.
         * Bulk Update process will continue, 
         * when the OSPFV3_RM_SUB_BULK_EVENT is received.
         * Send OSIX_FAILURE to suspend the Bulk Update Process.
         */
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjSendBulkNbrInfo\r\n");
        gu4O3RedAdjSendBulkNbrInfoFail++;
        return OSIX_FAILURE;
    }
    /* NBR Bulk update is completed, continue with the 
     * Virtual neighbor bulk update */
    O3RedUtlResetHsLastInfo ();
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_VIRT_NBR;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjSendBulkNbrInfo\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjSendNbrInfo                                        */
/*                                                                           */
/* Description  : This function constructs the nbr hello and nbr state       */
/*                info in the bulk update packet. When the bulk update       */
/*                packet size reaches MAX MTU size, then the packte will sent*/
/*                out immediately                                            */
/*                                                                           */
/* Input        : pInterface - pointer to interface                          */
/*              : ppBuf  - Rm Bulk update packet buffer                      */
/*              : pu4Offset - pointer to current offset value in the pBuf    */
/*              : pu4PktLen - pointer to current packet length               */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*              :  pu4PktLen - pointer to current packet length              */
/*              :  Output values pu4OffSet and pu4Pktlen are updated when    */
/*                 packet is constructed and not sent out                    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjSendNbrInfo (tV3OsInterface * pInterface, tRmMsg ** ppBuf,
                     UINT4 *pu4Offset, UINT4 *pu4PktLen)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tRmMsg             *pRmMsg = *ppBuf;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4PktLen = *pu4PktLen;
    UINT4               u4NbrHelloInfoLen = OSPFV3_ZERO;
    UINT4               u4NbrStateInfoLen = OSPFV3_ZERO;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER : O3RedAdjSendNbrInfo for interface If %d\n",
                 pInterface->u4InterfaceId);

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

        /* In case of NBMA or PTMP, neighbor state can be down/attempt.
         * In such case, the last stored hello packet can be empty.
         * If the last hello packet is empty, then neighbor state cannot
         * be FULL. Scan the next neighbor.
         */
        if (pNbr->lastHelloInfo.u2Len == 0)
        {
            continue;
        }

        /* Length of the Hello info = PDU Length +  
         * Size of UINT2 (for storing the PDU Length) +
         * Size of UINT4 (for storing the if index)+
         * OSPFV3_IPV6_ADDR_LEN for IPv6 Neighbor Source Address*/

        u4NbrHelloInfoLen =
            (pNbr->lastHelloInfo.u2Len + OSPFV3_FIXED_NBR_HELLO_SIZE);

        if ((u4PktLen + u4NbrHelloInfoLen) > (UINT4) OSPFV3_RED_MTU_SIZE)
        {
            /* Max Packet Length is reached, Send out the packet */
            if (O3RedUtlSendBulkUpdate (pRmMsg, u4PktLen) == OSIX_FAILURE)
            {
                *ppBuf = NULL;
                return OSIX_FAILURE;
            }

            /* Allocate the New Buffer for bulk update */
            if (O3RedAdjAllocateBulkUpdatePkt (&pRmMsg, &u4Offset, &u4PktLen)
                == OSIX_FAILURE)
            {
                *ppBuf = NULL;
                return OSIX_FAILURE;
            }

        }

        if (O3RedAdjConstructNbrHelloInfo (pNbr, pRmMsg,
                                           &u4Offset,
                                           &u4PktLen) == OSIX_FAILURE)
        {
            OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "EXIT: O3RedAdjSendNbrInfo for interface If %d\n",
                         pInterface->u4InterfaceId);
            return OSIX_FAILURE;
        }

        u4NbrStateInfoLen = OSPFV3_FIXED_NBR_INFO_SIZE;
        if ((u4PktLen + u4NbrStateInfoLen) > (UINT4) OSPFV3_RED_MTU_SIZE)
        {
            /* Max Packet Length is reached, Send out the packet */
            if (O3RedUtlSendBulkUpdate (pRmMsg, u4PktLen) == OSIX_FAILURE)
            {
                *ppBuf = NULL;
                return OSIX_FAILURE;
            }

            /* Allocate the New Buffer for bulk update */
            if (O3RedAdjAllocateBulkUpdatePkt (&pRmMsg, &u4Offset, &u4PktLen)
                == OSIX_FAILURE)
            {
                *ppBuf = NULL;
                return OSIX_FAILURE;
            }
        }

        if (O3RedAdjConstructNbrState (pNbr, pRmMsg,
                                       &u4Offset, &u4PktLen) == OSIX_FAILURE)
        {
            OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "EXIT: O3RedAdjSendNbrInfo for interface If %d\n",
                         pInterface->u4InterfaceId);
            return OSIX_FAILURE;
        }
    }

    *pu4PktLen = u4PktLen;
    *pu4Offset = u4Offset;
    *ppBuf = pRmMsg;

    OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "EXIT: O3RedAdjSendNbrInfo for interface If %d\n",
                 pInterface->u4InterfaceId);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjConstructNbrHelloInfo                              */
/*                                                                           */
/* Description  : This function constructs the nbr hello                     */
/*                info in the bulk update packet.                            */
/*                                                                           */
/* Input        : pNbr       - pointer to Neighbor                           */
/*              : pBuf       - Rm Bulk update packet buffer                  */
/*              : pu4Offset  - pointer to current offset value in the pBuf   */
/*              : pu4PktLen  - pointer to current packet length              */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*              :  pu4PktLen - pointer to current packet length              */
/*              :  Output values pu4OffSet and pu4Pktlen are updated when    */
/*                 packet is constructed and not sent out                    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjConstructNbrHelloInfo (tV3OsNeighbor * pNbr, tRmMsg * pBuf,
                               UINT4 *pu4Offset, UINT4 *pu4PktLen)
{
    UINT1               u1MsgType = OSPFV3_ZERO;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4PktLen = *pu4PktLen;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedAdjConstructNbrHelloInfo\r\n");

    /*              Structure of bulk hello                    *
     *___________________________________________________________
     * Field         Value                       size (in bytes)
     *___________________________________________________________
     * SubBulk type  OSPFV3_RED_BULK_HELLO         1
     * Pkt Info :    pdu len + pdu + if + src ip   varies

     Structure of Pkt Info
     *___________________________________________________________
     * Field         Value                       size (in bytes)
     *___________________________________________________________
     * PDU len       Length of Hello pdu          2
     * PDU           Actual Hello PDu             Length of pdu varies
     * If Index      If Index in which pdu rcvd   4
     * Src Ip        Source Ip address            OSPFV3_IPV6_ADDR_LEN
     ------------------------------------------------------------*/

    /* Set the sub type in the bulk update as OSPFV3_RED_BULK_HELLO */
    u1MsgType = OSPFV3_RED_BULK_HELLO;
    OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, u1MsgType);
    u4PktLen++;

    /* Copy the Hello packet length */
    OSPFV3_RED_PUT_2_BYTE (pBuf, u4Offset, pNbr->lastHelloInfo.u2Len);
    u4PktLen = u4PktLen + (sizeof (UINT2));

    /* Copy the Hello packet buffer */
    OSPFV3_RED_PUT_N_BYTE (pBuf, pNbr->lastHelloInfo.au1Pkt,
                           u4Offset, pNbr->lastHelloInfo.u2Len);
    u4PktLen = u4PktLen + pNbr->lastHelloInfo.u2Len;

    /* Copy the Interface Index */
    OSPFV3_RED_PUT_4_BYTE (pBuf, u4Offset, pNbr->lastHelloInfo.u4IfIndex);
    u4PktLen = u4PktLen + (sizeof (UINT4));

    /* Copy the Neighbor source IPv6 address */
    OSPFV3_RED_PUT_N_BYTE (pBuf, &pNbr->lastHelloInfo.SrcIp6Addr,
                           u4Offset, (sizeof (tIp6Addr)));
    u4PktLen = u4PktLen + (sizeof (tIp6Addr));

    OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC,
                     gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                     u4ContextId,
                     "Added hello from %x through interface %d\r\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     pNbr->pInterface->u4InterfaceId);
    gV3OsRtr.ospfRedInfo.u4HelloSynCount++;

    *pu4PktLen = u4PktLen;
    *pu4Offset = u4Offset;
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedAdjConstructNbrHelloInfo\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjConstructNbrState                                  */
/*                                                                           */
/* Description  : This function constructs the nbr state                     */
/*                info in the bulk update packet.                            */
/*                                                                           */
/* Input        : pNbr       - pointer to Neighbor                           */
/*              : pBuf       - Rm Bulk update packet buffer                  */
/*              : pu4Offset  - pointer to current offset value in the pBuf   */
/*              : pu4PktLen  - pointer to current packet length              */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*              :  pu4PktLen - pointer to current packet length              */
/*              :  Output values pu4OffSet and pu4Pktlen are updated when    */
/*                 packet is constructed and not sent out                    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjConstructNbrState (tV3OsNeighbor * pNbr, tRmMsg * pBuf,
                           UINT4 *pu4Offset, UINT4 *pu4PktLen)
{
    tV3OsRmNbrState     NbrStateInfo;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4PktLen = *pu4PktLen;
    UINT1               u1MsgType = OSPFV3_ZERO;
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedAdjConstructNbrState\r\n");

    MEMSET (&NbrStateInfo, OSPFV3_ZERO, sizeof (tV3OsRmNbrState));

    /*              Structure of bulk Nbr State                 *
     *___________________________________________________________
     * Field         Value                       size (in bytes)
     *___________________________________________________________
     * SubBulk type  OSPFV3_RED_BULK_NBR_STATE     1
     * Pkt Info :    context id + Nbr Id +
     *               Network type + 
     *               transit area id + dest router
     *               id + if index + state         22
     ------------------------------------------------------------
     ------------------------------------------------------------*/

    if (O3RedAdjGetNbrStateInfo (pNbr, &NbrStateInfo) == OSPFV3_SUCCESS)
    {
        /* Set the sub type in the bulk update as OSPFV3_RED_BULK_NBR_STATE */
        u1MsgType = OSPFV3_RED_BULK_NBR_STATE;
        OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, u1MsgType);
        u4PktLen++;

        /* Copy the context id */
        OSPFV3_RED_PUT_4_BYTE (pBuf, u4Offset, NbrStateInfo.u4ContextId);
        u4PktLen = u4PktLen + (sizeof (UINT4));

        /* Copy the nbr Id */
        OSPFV3_RED_PUT_N_BYTE (pBuf, NbrStateInfo.nbrId,
                               u4Offset, sizeof (NbrStateInfo.nbrId));
        u4PktLen = u4PktLen + (sizeof (NbrStateInfo.nbrId));

        /* Copy the interface index */
        OSPFV3_RED_PUT_4_BYTE (pBuf, u4Offset, NbrStateInfo.u4IfIndex);
        u4PktLen = u4PktLen + (sizeof (UINT4));

        /* Copy the neigbor state */
        OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, NbrStateInfo.u1CurrState);
        u4PktLen++;

        /* Copy the network type */
        OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, NbrStateInfo.u1NetworkType);
        u4PktLen++;

        /* Copy the transit area id */
        OSPFV3_RED_PUT_N_BYTE (pBuf, NbrStateInfo.transitAreaId,
                               u4Offset, sizeof (NbrStateInfo.transitAreaId));
        u4PktLen = u4PktLen + (sizeof (NbrStateInfo.transitAreaId));

        /* Copy the dest router id */
        OSPFV3_RED_PUT_N_BYTE (pBuf, NbrStateInfo.destRtrId,
                               u4Offset, sizeof (NbrStateInfo.destRtrId));
        u4PktLen = u4PktLen + (sizeof (NbrStateInfo.destRtrId));

        OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC,
                         pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Added nbr state : %x %d\r\n",
                         OSPFV3_BUFFER_DWFROMPDU (NbrStateInfo.nbrId),
                         NbrStateInfo.u1CurrState);
    }
    else
    {
        KW_FALSEPOSITIVE_FIX (pBuf);
    }
    *pu4PktLen = u4PktLen;
    *pu4Offset = u4Offset;
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedAdjConstructNbrState\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjAllocateBulkUpdatePkt                              */
/*                                                                           */
/* Description  : This function allocates a CRU buffer for RM packet         */
/*                and sets the type filed                                    */
/*                                                                           */
/* Input        : ppBuf      - pointer to Buffer to be allocated             */
/*                                                                           */
/* Output       : pBuf       - Rm Bulk update packet buffer                  */
/*              : pu4PktLen  - pointer to current packet length              */
/*              : pu4Offset  - pointer to current offset value in the pBuf   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjAllocateBulkUpdatePkt (tRmMsg ** ppBuf, UINT4 *pu4PktLen,
                               UINT4 *pu4Offset)
{
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT1               u1MsgType = OSPFV3_ZERO;
    tRmMsg             *pBuf = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedAdjAllocateBulkUpdatePkt\r\n");

    /* Allocate a New Buffer and Initialize the Buffer */
    if ((pBuf = RM_ALLOC_TX_BUF (OSPFV3_RED_MTU_SIZE)) == NULL)
    {

        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "O3RedAdjAllocateBulkUpdatePkt: Cannot "
                      "allocate buffer for sending Nbr Bulk Update"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.
                        apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "Cannot allocate buffer for sending Hello to "
                        "Standby");
        O3RedRmSendEvent ((UINT4) RM_BULK_UPDT_ABORT, (UINT4) RM_MEMALLOC_FAIL);
        pBuf = NULL;
        gu4O3RedAdjAllocateBulkUpdatePktFail++;
        return OSIX_FAILURE;
    }
    /* Initialize offset */
    u4Offset = OSPFV3_ZERO;
    u1MsgType = RM_BULK_UPDATE_MSG;
    OSPFV3_RED_PUT_1_BYTE (pBuf, u4Offset, u1MsgType);

    /* Increment offset to pass the total packet length which
     * will be filled later 
     */

    u4Offset += sizeof (UINT2);
    *pu4PktLen = OSPFV3_RED_MSG_HDR_LEN;
    *pu4Offset = u4Offset;
    *ppBuf = pBuf;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjAllocateBulkUpdatePkt\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjProcessNbrInfo                                     */
/*                                                                           */
/* Description  : This function process the neighbor state info              */
/*                                                                           */
/* Input        : pMsg       - pointer to RM buffer                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedAdjProcessNbrInfo (tRmMsg * pMsg)
{
    UINT4               u4OffSet = OSPFV3_ONE;
    UINT1               u1SubMsgType;
    UINT2               u2PktLen = OSPFV3_ZERO;
    INT4                i4RetVal = OSIX_SUCCESS;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedAdjProcessNbrInfo\r\n");
    /* Extract Total Message length */
    OSPFV3_RED_GET_2_BYTE (pMsg, u4OffSet, u2PktLen);

    while ((u4OffSet < u2PktLen) && (i4RetVal == OSIX_SUCCESS))
    {
        /* Read the SubMessageType */
        OSPFV3_RED_GET_1_BYTE (pMsg, u4OffSet, u1SubMsgType);

        if (u1SubMsgType == OSPFV3_RED_BULK_HELLO)
        {
            i4RetVal = O3RedAdjProcessBulkHello (pMsg, &u4OffSet);
        }
        else if (u1SubMsgType == OSPFV3_RED_BULK_NBR_STATE)
        {
            /* Process nbr state bulk update */
            i4RetVal = O3RedAdjProcessNbrStateInfo (pMsg, &u4OffSet);
        }
        else if (u1SubMsgType == OSPFV3_RED_BULK_INTF)
        {
            /* Process interface related information */
            i4RetVal = O3RedIfProcessIntfInfo (pMsg, &u4OffSet);
        }
        else
        {
            /* Error in getting sub message type */
            i4RetVal = OSIX_FAILURE;
        }
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "OSPFv3 adjacency related bulk update "
                      "processing failed\r\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "OSPFv3 adjacency related bulk update "
                        "processing failed\r\n");
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjProcessNbrInfo\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjSendBulkVirtNbrInfo                                */
/*                                                                           */
/* Description  : This function constructs and Sends out the bulk update     */
/*                for virtual hello and virtual neigbhor state               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : pi4SendSubBulkEvent - indicates whether SUBBULK UDATE EVENT*/
/*                                      needs to be sent out or not          */
/*                                      Will be set to OSIX_SUCCESS, when the*/
/*                                      SUBBULK UDATE EVENT needs to sent    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedAdjSendBulkVirtNbrInfo (INT4 *pi4SendSubBulkEvent)
{
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tRmMsg             *pBuf = NULL;
    tV3OsRouterId       destRouterId;
    tV3OsAreaId         transitAreaId;
    UINT4               u4ContextId = OSPFV3_ZERO;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT4               u4PktLen = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedAdjSendBulkVirtNbrInfo\r\n");

    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_VIRT_NBR;
    MEMCPY (transitAreaId, gV3OsRtr.ospfRedInfo.transitAreaId,
            sizeof (tV3OsAreaId));
    MEMCPY (destRouterId, gV3OsRtr.ospfRedInfo.destRouterId,
            sizeof (tV3OsRouterId));

    *pi4SendSubBulkEvent = OSIX_FAILURE;

    /* Allocate the Buffer for bulk update */
    if (O3RedAdjAllocateBulkUpdatePkt (&pBuf, &u4Offset, &u4PktLen)
        == OSIX_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3RedAdjSendBulkVirtNbrInfo\r\n");
        gu4O3RedAdjSendBulkVirtNbrInfoFail++;
        return OSIX_FAILURE;
    }

    for (u4ContextId = gV3OsRtr.ospfRedInfo.u4LastCxtId;
         (u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT); u4ContextId++)
    {
        if ((pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4ContextId]) == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);

            if ((pInterface->operStatus == OSPFV3_DISABLED) ||
                (pInterface->admnStatus == OSPFV3_DISABLED))
            {
                continue;
            }

            if (V3UtilVirtIfIndComp (transitAreaId, destRouterId,
                                     pInterface->transitAreaId,
                                     pInterface->destRtrId) != OSPFV3_LESS)
            {
                continue;
            }

            if (O3RedAdjSendVirtNbrInfo (pInterface, &pBuf,
                                         &u4Offset, &u4PktLen) == OSIX_FAILURE)
            {
                if (pBuf != NULL)
                {
                    RM_FREE (pBuf);
                }
                OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                                "EXIT : O3RedAdjSendBulkVirtNbrInfo\r\n");
                gu4O3RedAdjSendBulkVirtNbrInfoFail++;
                return OSIX_FAILURE;
            }

            if (O3RedUtlBulkUpdtRelinquish () == OSIX_SUCCESS)
            {
                /* Reached the max interfaces count. 
                 * Implemented on considering relinguish factor.
                 * Last sent Interface details are stored and
                 * OSPFV3_RM_SUB_BULK_EVENT is sent to OSPFV3 task */
                gV3OsRtr.ospfRedInfo.u4LastCxtId = u4ContextId;
                MEMCPY (gV3OsRtr.ospfRedInfo.transitAreaId, transitAreaId,
                        sizeof (tV3OsAreaId));
                MEMCPY (gV3OsRtr.ospfRedInfo.destRouterId, destRouterId,
                        sizeof (tV3OsRouterId));
                *pi4SendSubBulkEvent = OSIX_SUCCESS;
                break;
            }
        }                        /* End of Virt if Scan */

        if (*pi4SendSubBulkEvent == OSIX_SUCCESS)
        {
            /* discontinue the for loop sub bulk event is sent */
            break;
        }
    }                            /* End of For Loop */

    if (u4PktLen == OSPFV3_RED_MSG_HDR_LEN)
    {
        /* This packet contains only RM Header
         * No dynamic bulk update information is added in this buffer
         */
        if (pBuf != NULL)
        {
            RM_FREE (pBuf);
        }
    }
    else
    {
        /* Send out the constructed packet */
        if (O3RedUtlSendBulkUpdate (pBuf, u4PktLen) == OSIX_FAILURE)
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                            "EXIT : O3RedAdjSendBulkVirtNbrInfo\r\n");
            gu4O3RedAdjSendBulkVirtNbrInfoFail++;
            return OSIX_FAILURE;
        }
    }
    if (*pi4SendSubBulkEvent == OSIX_SUCCESS)
    {
        /* Sub Bulk event needs to be sent out to relinquish the 
         * current bulk update thread.
         * Bulk Update process will continue, 
         * when the OSPFV3_RM_SUB_BULK_EVENT is received.
         * Send OSIX_FAILURE to suspend the Bulk Update Process.
         */
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3RedAdjSendBulkVirtNbrInfo\r\n");
        gu4O3RedAdjSendBulkVirtNbrInfoFail++;
        return OSIX_FAILURE;
    }

    /* Virt Nbr Bulk update is completed, continue with the 
     * LSU bulk update */
    O3RedUtlResetHsLastInfo ();
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_LSU;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedAdjSendBulkVirtNbrInfo\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedAdjSendVirtNbrInfo                                    */
/*                                                                           */
/* Description  : This function constructs the virtual nbr hello and nbr     */
/*                state info in the bulk update packet. When the bulk update */
/*                packet size reaches MAX MTU size, then the packte will sent*/
/*                out immediately                                            */
/*                                                                           */
/* Input        : pInterface - pointer to virtual interface                  */
/*              : pBuf  - Rm Bulk update packet buffer                       */
/*              : pu4Offset - pointer to current offset value in the pBuf    */
/*              : pu4PktLen - pointer to current packet length               */
/*                                                                           */
/* Output       : pu4Offset - pointer to current offset value in the pBuf    */
/*              :  pu4PktLen - pointer to current packet length              */
/*              :  Output values pu4OffSet and pu4Pktlen are updated when    */
/*                 packet is constructed and not sent out                    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
O3RedAdjSendVirtNbrInfo (tV3OsInterface * pInterface, tRmMsg ** pBuf,
                         UINT4 *pu4Offset, UINT4 *pu4PktLen)
{
    tV3OsNeighbor      *pVirtNbr = NULL;
    tTMO_SLL_NODE      *pLastNbr = NULL;
    UINT4               u4Offset = *pu4Offset;
    UINT4               u4PktLen = *pu4PktLen;
    UINT4               u4NbrHelloInfoLen = OSPFV3_ZERO;
    UINT4               u4NbrStateInfoLen = OSPFV3_ZERO;

    OSPFV3_TRC1 (OSPFV3_FN_ENTRY,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER : O3RedAdjSendVirtNbrInfo for interface If %d\n",
                 pInterface->u4InterfaceId);

    /* Virtual Interface contains only one neighbor */
    pLastNbr = TMO_SLL_First (&pInterface->nbrsInIf);

    if (pLastNbr == NULL)
    {
        return OSIX_SUCCESS;
    }

    /* Length of the Hello info = PDU Length +  
     * Size of UINT2 (for storing the PDU Length) +
     * Size of UINT4 (for storing the if index)+
     * OSPFV3_IPV6_ADDR_LEN for IPv6 Neighbor Source Address*/

    pVirtNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLastNbr);
    u4NbrHelloInfoLen =
        (pVirtNbr->lastHelloInfo.u2Len + OSPFV3_FIXED_NBR_HELLO_SIZE);

    if ((u4PktLen + u4NbrHelloInfoLen) > (UINT4) OSPFV3_RED_MTU_SIZE)
    {
        /* Max Packet Length is reached, Send out the packet */
        if (O3RedUtlSendBulkUpdate (*pBuf, u4PktLen) == OSIX_FAILURE)
        {
            gu4O3RedAdjSendVirtNbrInfoFail++;
            return OSIX_FAILURE;
        }

        /* Allocate the New Buffer for bulk update */
        if (O3RedAdjAllocateBulkUpdatePkt (pBuf, &u4Offset, &u4PktLen)
            == OSIX_FAILURE)
        {
            gu4O3RedAdjSendVirtNbrInfoFail++;
            return OSIX_FAILURE;
        }

    }

    if (O3RedAdjConstructNbrHelloInfo (pVirtNbr, *pBuf,
                                       &u4Offset, &u4PktLen) == OSIX_FAILURE)
    {
        OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "EXIT: O3RedAdjSendVirtNbrInfo for interface If %d\n",
                     pInterface->u4InterfaceId);
        gu4O3RedAdjSendVirtNbrInfoFail++;
        return OSIX_FAILURE;
    }

    u4NbrStateInfoLen = OSPFV3_FIXED_NBR_INFO_SIZE;
    if ((u4PktLen + u4NbrStateInfoLen) > (UINT4) OSPFV3_RED_MTU_SIZE)
    {
        /* Max Packet Length is reached, Send out the packet */
        if (O3RedUtlSendBulkUpdate (*pBuf, u4PktLen) == OSIX_FAILURE)
        {
            gu4O3RedAdjSendVirtNbrInfoFail++;
            return OSIX_FAILURE;
        }

        /* Allocate the New Buffer for bulk update */
        if (O3RedAdjAllocateBulkUpdatePkt (pBuf, &u4Offset, &u4PktLen)
            == OSIX_FAILURE)
        {
            gu4O3RedAdjSendVirtNbrInfoFail++;
            return OSIX_FAILURE;
        }
    }

    if (O3RedAdjConstructNbrState (pVirtNbr, *pBuf,
                                   &u4Offset, &u4PktLen) == OSIX_FAILURE)
    {
        OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "EXIT: O3RedAdjSendVirtNbrInfo for interface If %d\n",
                     pInterface->u4InterfaceId);
        gu4O3RedAdjSendVirtNbrInfoFail++;
        return OSIX_FAILURE;
    }

    *pu4PktLen = u4PktLen;
    *pu4Offset = u4Offset;

    OSPFV3_TRC1 (OSPFV3_FN_EXIT,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "EXIT: O3RedAdjSendVirtNbrInfo for interface If %d\n",
                 pInterface->u4InterfaceId);
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redadj.c                           */
/*-----------------------------------------------------------------------*/
