/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3reddyn.c,v 1.12 2018/01/25 10:04:16 siva Exp $
 *
 * Description: 
 ********************************************************************/
#include "o3inc.h"

/* Prototypes */

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedDynConstructAndSendLsa                               */
/*                                                                          */
/* Description  : This function is called when the active node originates   */
/*                a new LSA                                                 */
/*                                                                          */
/* Input        : pLsaInfo    -     pointer to LSA Info                     */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedDynConstructAndSendLsa (tV3OsLsaInfo * pLsaInfo)
{
    tRmMsg             *pRmMsg = NULL;
    UINT2               u2Len = 0;
    UINT1               u1MsgType = (UINT1) OSPFV3_RED_LSU;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynConstructAndSendLsa\r\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus < OSPFV3_BULK_UPDT_LSU))
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3RedDynConstructAndSendLsa\r\n");
        return OSIX_SUCCESS;
    }

    /* If it is a self-originated LSA and LSA Desc is NULL, then do
     * not send the LSU. This occurs when an invalid self-originated
     * LSA is received from the neighbor
     */
    if ((OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) &&
        (pLsaInfo->pLsaDesc == NULL))
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3RedDynConstructAndSendLsa\r\n");
        return OSIX_SUCCESS;
    }

    pRmMsg = RM_ALLOC_TX_BUF ((UINT2) OSPFV3_RED_MTU_SIZE);

#ifdef L2RED_WANTED
    if (pRmMsg == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for LSU Update\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC | OS_RESOURCE_TRC,
                        pLsaInfo->pV3OspfCxt->u4ContextId,
                        "LSU update " "allocation failed");
        gu4O3RedDynConstructAndSendLsaFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u2Len, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u2Len, 0);

    O3RedLsuConstructLsuUpdt (pLsaInfo, pRmMsg, &u2Len);

    RM_DATA_ASSIGN_2_BYTE (pRmMsg, OSPFV3_RED_BULK_LEN_OFFSET, u2Len);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedDynConstructAndSendLsa\r\n");
    return (V3OspfEnqMsgToRm (pRmMsg, u2Len));
#else
    UNUSED_PARAM (u2Len);
    UNUSED_PARAM (u1MsgType);
    return OSIX_FAILURE;
#endif
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedDynProcessLsuUpdt                                    */
/*                                                                          */
/* Description  : This function is called during the dynamic update process */
/*                by the standby node. This function process the dynamic    */
/*                LSU update. If the neighbor is full then the LSU is sent  */
/*                to normal processing else the LSU is dumped               */
/*                                                                          */
/* Input        : pRmMsg      -     pointer to RM message                   */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedDynProcessLsuUpdt (tRmMsg * pRmMsg)
{
    tV3OsRmLsdbInfo     lsdbInfo;
    tV3OsLsHeader       lsHeader;
    UINT2               u2Offset = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynProcessLsuUpdt\r\n");

    MEMSET (&lsdbInfo, 0, sizeof (tV3OsRmLsdbInfo));
    MEMSET (&lsHeader, 0, sizeof (tV3OsLsHeader));

    /* Skip Message type  and Packet Length */
    u2Offset += sizeof (UINT1) + sizeof (UINT2);

    O3RedLsuGetLsuFromUpdt (pRmMsg, &lsdbInfo, &u2Offset);

    if (lsdbInfo.pu1Lsa != NULL)
    {
        if (O3RedLsuProcessLsu (&lsdbInfo) == OSIX_FAILURE)
        {
            V3UtilExtractLsHeaderFromLbuf (lsdbInfo.pu1Lsa, &lsHeader);

            OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                             gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                             u4ContextId,
                             "Dynamic process LSA failed "
                             "LSA id %x Lsa Type %x Adv rtr id :%x\r\n",
                             lsHeader.u2LsaType,
                             OSPFV3_BUFFER_DWFROMPDU (lsHeader.linkStateId),
                             OSPFV3_BUFFER_DWFROMPDU (lsHeader.advRtrId));

            /* Clear the summary param allocated for inter area
             * and inter prefix LSA */

            if ((lsdbInfo.u2InternalLsaType ==
                 OSPFV3_INTER_AREA_PREFIX_LSA) ||
                (lsdbInfo.u2InternalLsaType == OSPFV3_INTER_AREA_ROUTER_LSA))
            {
                if (lsdbInfo.pAssoPrimitive != NULL)
                {
                    OSPFV3_SUM_PARAM_FREE (lsdbInfo.pAssoPrimitive);
                    lsdbInfo.pAssoPrimitive = NULL;
                }
            }

            OSPFV3_LSA_TYPE_FREE (lsHeader.u2LsaType, lsdbInfo.pu1Lsa);
            lsdbInfo.pu1Lsa = NULL;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedDynProcessLsuUpdt\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynSendHelloToStandby                                 */
/*                                                                           */
/* Description  : This function will form the dynamic update of Hello packet */
/*                information and send it to standby                         */
/*                                                                           */
/* Input        : pNbr           - Pointer to Neighbor                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynSendHelloToStandby (tV3OsNeighbor * pNbr)
{
    /* Allocate CRU Buf and form the packet and send */
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT1               u1MsgType = OSPFV3_ZERO;
    UINT2               u2MsgLen = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynSendHelloToStandby \r\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
         OSPFV3_BULK_UPDT_NOT_STARTED))
    {
        return;
    }

    u1MsgType = OSPFV3_RED_HELLO;
    u2MsgLen = (UINT2) (pNbr->lastHelloInfo.u2Len + OSPFV3_TWO + OSPFV3_FOUR
                        + OSPFV3_IPV6_ADDR_LEN + OSPFV3_RED_MSG_HDR_LEN);

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate buffer for sending Hello to Standby"));
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "Cannot allocate buffer for sending Hello to Standby");

        pNbr->u1HelloSyncState = OSIX_FALSE;
        return;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, pNbr->lastHelloInfo.u2Len);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pNbr->lastHelloInfo.au1Pkt,
                           u4Offset, pNbr->lastHelloInfo.u2Len);
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, pNbr->lastHelloInfo.u4IfIndex);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, &pNbr->lastHelloInfo.SrcIp6Addr, u4Offset,
                           OSPFV3_IPV6_ADDR_LEN);
    if (V3OspfEnqMsgToRm (pRmMsg, u2MsgLen) == OSIX_FAILURE)
    {
        O3RedRmSendEvent (RM_BULK_UPDT_ABORT, RM_SENDTO_FAIL);
        pNbr->u1HelloSyncState = OSIX_FALSE;
    }
    else
    {
        /* Increament sync Hello count */
        gV3OsRtr.ospfRedInfo.u4HelloSynCount++;
        pNbr->u1HelloSyncState = OSIX_TRUE;
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, "Sent dynamic hello to standby node\r\n");
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynSendHelloToStandby \r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynProcessHello                                       */
/*                                                                           */
/* Description  : This function will process the dynamic update of Hello     */
/*                information received from Active                           */
/*                                                                           */
/* Input        : pBuf - The Hello information received from Active          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynProcessHello (tRmMsg * pBuf)
{
    tIp6Addr            SrcIp6Addr;
    tV3OsAreaId         areaId;
    tV3OsRouterId       headerRtrId;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT4               u4IfIndex;
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface     *pAssoIface = NULL;
    UINT2               u2PktLen = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynProcessHello \r\n");

    MEMSET (&SrcIp6Addr, OSPFV3_ZERO, sizeof (tIp6Addr));
    MEMSET (areaId, 0, sizeof (areaId));
    MEMSET (headerRtrId, 0, sizeof (tV3OsRouterId));

    /* Skip Message type */
    u4Offset += OSPFV3_ONE;
    /* Skip Packet Length */
    u4Offset += OSPFV3_TWO;

    MEMSET (&SrcIp6Addr, OSPFV3_ZERO, sizeof (tIp6Addr));
    OSPFV3_RED_GET_2_BYTE (pBuf, u4Offset, u2PktLen);

    /* Get the packet */
    MEMSET (gV3OsRtr.au1Pkt, 0, OSPFV3_MAX_HELLO_PKT_LEN);

    OSPFV3_RED_GET_N_BYTE (pBuf, gV3OsRtr.au1Pkt, u4Offset, u2PktLen);
    OSPFV3_RED_GET_4_BYTE (pBuf, u4Offset, u4IfIndex);
    OSPFV3_RED_GET_N_BYTE (pBuf, &SrcIp6Addr, u4Offset, OSPFV3_IPV6_ADDR_LEN);

    /* Find the Interface structure pointer corresponding to that
       IfIndex */
    if ((pInterface = V3GetFindIf (u4IfIndex)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "OSPFv3 Pkt Disc Associated If Not Found\n"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "OSPFv3 Pkt Disc Associated If Not Found\n");
        return;
    }

    /* If the area id in the hello packet is 0.0.0.0 and the interface
     * is not associated with the area 0.0.0.0, then the hello packet
     * belongs to the virtual interface
     */
    OSPFV3_BUFFER_GET_STRING (gV3OsRtr.au1Pkt, areaId, OSPFV3_AREA_ID_OFFSET,
                              OSPFV3_AREA_ID_LEN);

    if (V3UtilAreaIdComp (areaId, pInterface->pArea->areaId) != OSPFV3_EQUAL)
    {
        if (V3UtilAreaIdComp (areaId, OSPFV3_BACKBONE_AREAID) == OSPFV3_EQUAL)
        {
            /* Get the router id of the from the OSPF header */
            OSPFV3_BUFFER_GET_STRING (gV3OsRtr.au1Pkt, headerRtrId,
                                      OSPFV3_RTR_ID_OFFSET, OSPFV3_RTR_ID_LEN);

            /* associate with virtual interface */
            if ((pAssoIface = V3PppAssociateWithVirtualIf (pInterface,
                                                           &headerRtrId)) ==
                NULL)
            {
                SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                              "OSPFv3 Pkt Discarded, Virtual interface "
                              "not found\r\n"));

                OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                                pInterface->pArea->pV3OspfCxt->u4ContextId,
                                "OSPFv3 Pkt Discarded, Virtual interface "
                                "not found\r\n");
                return;
            }

            pInterface = pAssoIface;
        }
        else
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "OSPFv3 Pkt Discarded due to area mismatch\r\n"));

            OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                            pInterface->pArea->pV3OspfCxt->u4ContextId,
                            "OSPFv3 Pkt Discarded due to area mismatch\r\n");
            return;
        }
    }

    V3HpRcvHello (gV3OsRtr.au1Pkt, u2PktLen, pInterface, &SrcIp6Addr);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynProcessHello \r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynSendLsAckInfoToStandby                             */
/*                                                                           */
/* Description  : This function will form the dynamic update of Ls Ack       */
/*                when Ls Ack is received from all the Nbrs and send         */
/*                                                                           */
/* Input        : pLsAckInfo - The details of Lsa for which all acks rcvd    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynSendLsAckInfoToStandby (tV3OsRmLsAckDetails * pLsAckInfo)
{

    UINT1               u1MsgType = (UINT1) OSPFV3_RED_LS_ACK;
    UINT2               u2MsgLen = OSPFV3_ZERO;
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedDynSendLsAckInfoToStandby \r\n");
    /* Length of the packet = Type (1) + Length field (2)
     *   + size of structure tV3OsRmLsAckDetails - Reserved (2)
     */

    u2MsgLen = sizeof (tV3OsRmLsAckDetails) + OSPFV3_ONE;

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate buffer for sending Ack Details to Standby"));

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "Cannot allocate buffer for sending Hello to Standby");
        return;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pLsAckInfo, u4Offset,
                           sizeof (tV3OsRmLsAckDetails) - OSPFV3_TWO);
    V3OspfEnqMsgToRm (pRmMsg, u2MsgLen);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:O3RedDynSendLsAckInfoToStandby \r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynProcessNbrState                                    */
/*                                                                           */
/* Description  : This function will process the dynamic update of Nbr State */
/*                state change                                               */
/*                                                                           */
/* Input        : pRmMsg - The packet Received from RM                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynProcessNbrState (tRmMsg * pRmMsg)
{
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsRouterId       nbrId;
    tV3OsAreaId         transitAreaId;
    tV3OsRouterId       destRtrId;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4InterfaceId = 0;
    UINT1               u1NetworkType = 0;
    UINT1               u1NsmState = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynProcessNbrState\r\n");

    MEMSET (nbrId, OSPFV3_ZERO, OSPFV3_RTR_ID_LEN);

    /* Length of the packet = Type (1) + Length field (2)
     *                        + context id (4) + network type (1)
     *                        + Transit area id (4)
     *                        + Destination router id (4)
     *                        + interface index (4)
     *                        + nbr router id (4) + state (1)
     */

    /* Skip Message type and packet length */
    u4Offset += OSPFV3_ONE;
    u4Offset += OSPFV3_TWO;

    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, u4ContextId);

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

    if (pV3OspfCxt == NULL)
    {
        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                         gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                         u4ContextId, "Context : %d "
                         "not available\r\n", u4ContextId);
        return;
    }

    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, u1NetworkType);
    OSPFV3_RED_GET_N_BYTE (pRmMsg, transitAreaId, u4Offset,
                           sizeof (transitAreaId));
    OSPFV3_RED_GET_N_BYTE (pRmMsg, destRtrId, u4Offset, sizeof (destRtrId));
    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, u4InterfaceId);

    /* Get the interface */
    if (u1NetworkType != OSPFV3_IF_VIRTUAL)
    {
        pInterface = V3GetFindIf (u4InterfaceId);
    }
    else
    {
        pInterface = V3GetFindVirtIfInCxt (pV3OspfCxt, &transitAreaId,
                                           &destRtrId);
    }

    if (pInterface == NULL)
    {
        OSPFV3_EXT_TRC2 (OSPFV3_RM_TRC, pV3OspfCxt->u4ContextId,
                         "Interface not available "
                         "Network : %d Index : %d\r\n",
                         u4ContextId, u4InterfaceId);
        return;
    }

    OSPFV3_RED_GET_N_BYTE (pRmMsg, nbrId, u4Offset, sizeof (nbrId));
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, u1NsmState);

    if ((pNbr = V3HpSearchNbrLst (&nbrId, OSPFV3_ZERO, pInterface)) == NULL)
    {
        OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC, pV3OspfCxt->u4ContextId,
                         "Nbr %x not found in interface : %d Network : %d\r\n",
                         OSPFV3_BUFFER_DWFROMPDU (nbrId),
                         u4InterfaceId, u1NetworkType);
        return;
    }

    OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC, pV3OspfCxt->u4ContextId,
                     "Processing State : %d for Nbr : %x Network : %d\r\n",
                     u1NsmState, OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     u1NetworkType);

    switch (u1NsmState)
    {
        case OSPFV3_NBRS_FULL:

            V3NbrUpdateState (pNbr, OSPFV3_NBRS_FULL);

            if (pInterface->pArea->pV3OspfCxt->u1RtrNetworkLsaChanged
                != OSIX_TRUE)
            {
                V3RtcSetRtTimerInCxt (pInterface->pArea->pV3OspfCxt);
            }
            break;

        case OSPFV3_NBRS_DOWN:

            V3NsmDown (pNbr);
            break;

        default:
            V3NbrUpdateState (pNbr, u1NsmState);
            break;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynProcessNbrState\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynProcessLsAckInfo                                   */
/*                                                                           */
/* Description  : This function will process the dynamic update of Ls Ack    */
/*                and clear the LSA from all the Rx lists                    */
/*                                                                           */
/* Input        : pRmMsg - The packet Received from RM                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynProcessLsAckInfo (tRmMsg * pRmMsg)
{
    tV3OsRmLsAckDetails LsAckDetails;
    tV3OsLsaInfo        lsaInfo;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OsArea          *pArea = NULL;
    tRBTree             pRBRoot = NULL;
    tV3OsAreaId         areaId;
    tV3OspfCxt         *pCxt = NULL;
    UINT4               u4Offset = OSPFV3_ZERO;
    UINT1               u1DelFlag = OSPFV3_ZERO;
    UINT1               u1FloodScope = OSPFV3_ZERO;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynProcessLsAckInfo\n");

    MEMSET (&LsAckDetails, OSPFV3_ZERO, sizeof (tV3OsRmLsAckDetails));
    MEMSET (&lsaInfo, OSPFV3_ZERO, sizeof (tV3OsLsaInfo));
    /* Increment the offset to pass the type and length field */
    u4Offset += OSPFV3_THREE;

    OSPFV3_RED_GET_N_BYTE (pRmMsg, &LsAckDetails, u4Offset,
                           sizeof (tV3OsRmLsAckDetails) - OSPFV3_TWO);
    u1FloodScope = V3LsuGetLsaFloodScope (LsAckDetails.u2LsType);

    MEMSET (&lsaInfo, OSPFV3_ZERO, sizeof (tV3OsLsaInfo));

    lsaInfo.lsaId.u2LsaType = LsAckDetails.u2LsType;
    OSPFV3_LINK_STATE_ID_COPY (&lsaInfo.lsaId.linkStateId,
                               &LsAckDetails.u4LsId);
    OSPFV3_RTR_ID_COPY (&lsaInfo.lsaId.advRtrId, &LsAckDetails.u4AdvRtrId);

    /* Find the Context pointer from Context Id */
    if ((pCxt = V3UtilOspfGetCxt (LsAckDetails.u4CxtId)) == NULL)
    {
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, "OSPFv3 Associated Context Not Found\n");
        return;
    }

    OSPFV3_EXT_TRC3
        (OSPFV3_RM_TRC,
         LsAckDetails.u4CxtId,
         "LSA Ack: %x %x %x\r\n", lsaInfo.lsaId.u2LsaType,
         OSPFV3_BUFFER_DWFROMPDU (lsaInfo.lsaId.linkStateId),
         OSPFV3_BUFFER_DWFROMPDU (lsaInfo.lsaId.advRtrId));

    switch (u1FloodScope)
    {
        case OSPFV3_LINK_FLOOD_SCOPE:
            /* Find the Interface structure pointer corresponding to that
             * IfIndex */
            if ((pInterface = V3GetFindIf (LsAckDetails.u4StorageId)) == NULL)
            {
                OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                                gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                                u4ContextId,
                                "OSPFv3 Associated If Not Found\n");
                return;
            }
            pRBRoot = pInterface->pLinkScopeLsaRBRoot;
            break;

        case OSPFV3_AREA_FLOOD_SCOPE:
            /* Find the area structure pointer associated with the Lsa */
            MEMCPY (&areaId, &LsAckDetails.u4StorageId, OSPFV3_AREA_ID_LEN);
            if ((pArea = V3GetFindAreaInCxt (pCxt, &areaId)) == NULL)
            {
                OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                                gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                                u4ContextId,
                                "OSPFv3 Associated Area Not Found\n");
                return;
            }
            pRBRoot = pArea->pAreaScopeLsaRBRoot;
            break;

        case OSPFV3_AS_FLOOD_SCOPE:

            pRBRoot = pCxt->pAsScopeLsaRBRoot;
            break;

        default:
            break;

    }

    pLsaInfo = (tV3OsLsaInfo *) RBTreeGet (pRBRoot, (tRBElem *) & lsaInfo);

    if (pLsaInfo != NULL)
    {
        u1DelFlag = (UINT1) OSIX_TRUE;
        V3LsuDeleteFromAllRxmtLst (pLsaInfo, u1DelFlag);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "EXIT: O3RedDynProcessLsAckInfo\n");
    return;
}

/*****************************************************************************/
/* Function     : O3RedDynLsuUpdAckToStandby                                 */
/*                                                                           */
/* Description  : This routine send the LS ack update to standby When        */
/*                LSAck from All Nbrs are received                           */
/*                                                                           */
/* Input        : pLsaInfo      : Pointer to the Lsa Info                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Void                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynLsuUpdAckToStandby (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsRmLsAckDetails LsAckInfo;
    UINT1               u1FloodScope = OSPFV3_ZERO;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: O3RedDynUpdAckToStandby\n");

    OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                     "Sending LSU Ack syn-up for :%x %x %x\r\n",
                     pLsaInfo->lsaId.u2LsaType,
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.
                                              linkStateId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
         OSPFV3_BULK_UPDT_NOT_STARTED))
    {
        return;
    }

    MEMSET (&LsAckInfo, OSPFV3_ZERO, sizeof (tV3OsRmLsAckDetails));
    OSPFV3_LINK_STATE_ID_COPY (&LsAckInfo.u4LsId, &pLsaInfo->lsaId.linkStateId);

    u1FloodScope = V3LsuGetLsaFloodScope (pLsaInfo->lsaId.u2LsaType);
    LsAckInfo.u4CxtId = pLsaInfo->pV3OspfCxt->u4ContextId;

    if (u1FloodScope == OSPFV3_LINK_FLOOD_SCOPE)
    {
        LsAckInfo.u4StorageId = pLsaInfo->pInterface->u4InterfaceId;
    }
    else if (u1FloodScope == OSPFV3_AREA_FLOOD_SCOPE)
    {
        MEMCPY (&LsAckInfo.u4StorageId, &pLsaInfo->pArea->areaId,
                OSPFV3_AREA_ID_LEN);
    }
    else
    {
        LsAckInfo.u4StorageId = OSPFV3_ZERO;
    }

    OSPFV3_RTR_ID_COPY (&LsAckInfo.u4AdvRtrId, &pLsaInfo->lsaId.advRtrId);
    LsAckInfo.u2LsType = pLsaInfo->lsaId.u2LsaType;

    OSPFV3_EXT_TRC3
        (OSPFV3_RM_TRC,
         LsAckInfo.u4CxtId,
         "LSA Ack %x %x %x\r\n", pLsaInfo->lsaId.u2LsaType,
         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId),
         OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId));

    O3RedDynSendLsAckInfoToStandby (&LsAckInfo);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: O3RedDynUpdAckToStandby\n");
    return;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynSendNbrStateToStandby                              */
/*                                                                           */
/* Description  : This function is called when a Nbr state change from or    */
/*                change to FULL state or change to DOWWN state              */
/*                                                                           */
/* Input        : u4NbrId      : Router Id of the neighbor                   */
/*                u4IfIndex    : Interface Index                             */
/*                u1State      : The Current Neighbour state                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynSendNbrStateToStandby (tV3OsNeighbor * pNbr)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = OSPFV3_RED_NBR_STATE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedDynSendNbrStateToStandby\r\n");

    /* Length of the packet = Type (1) + Length field (2)
     *                        + context id (4) + network type (1)
     *                        + Transit area id (4)
     *                        + Destination router id (4)
     *                        + interface index (4)
     *                        + nbr router id (4) + state (1)
     */

    u2MsgLen = (UINT2) OSPFV3_DYNAMIC_NBR_SIZE;

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate buffer for sending Hello to Standby"));
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId,
                        "Cannot allocate buffer for sending Hello to Standby");
        return;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset,
                           pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId);
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, pNbr->pInterface->u1NetworkType);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pNbr->pInterface->transitAreaId,
                           u4Offset, sizeof (pNbr->pInterface->transitAreaId));
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pNbr->pInterface->destRtrId,
                           u4Offset, sizeof (pNbr->pInterface->destRtrId));
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, pNbr->pInterface->u4InterfaceId);
    OSPFV3_RED_PUT_N_BYTE (pRmMsg, pNbr->nbrRtrId,
                           u4Offset, sizeof (pNbr->nbrRtrId));
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, pNbr->u1NsmState);

    V3OspfEnqMsgToRm (pRmMsg, u2MsgLen);

    OSPFV3_EXT_TRC3 (OSPFV3_RM_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "Sending State : %d for Nbr : %x Network : %d\r\n",
                     pNbr->u1NsmState, pNbr->nbrRtrId,
                     pNbr->pInterface->u1NetworkType);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynSendNbrStateToStandby\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynSendIntfInfoToStandby                              */
/*                                                                           */
/* Description  : This function constructs the intf information              */
/*                                                                           */
/* Input        : pInterface   - pointer to interface                        */
/*                u1Flag       - Deletion indication                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedDynSendIntfInfoToStandby (tV3OsInterface * pInterface, UINT1 u1Flag)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2PktLen = (UINT2) OSPFV3_INTF_INFO_SIZE;
    UINT1               u1MsgType = (UINT1) OSPFV3_RED_INTF;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedDynSendIntfInfoToStandby\r\n");

    if ((OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE) ||
        (gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus ==
         OSPFV3_BULK_UPDT_NOT_STARTED))
    {
        return OSIX_SUCCESS;
    }

    /* Allocate a New Buffer and Initialize the Buffer */
    if ((pRmMsg = RM_ALLOC_TX_BUF (OSPFV3_RED_MTU_SIZE)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Interface sync-up failed "
                      "for interface index : %x\n", pInterface->u4InterfaceId));

        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId,
                         "Intf sync-up failed "
                         "for interface index : %x\n",
                         pInterface->u4InterfaceId);
        gu4O3RedDynSendIntfInfoToStandbyFail++;
        return OSIX_FAILURE;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);

    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2PktLen);

    O3RedIfConstructIntfInfo (pInterface, pRmMsg, &u4Offset, u1Flag);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedDynSendIntfInfoToStandby\r\n");

    return (V3OspfEnqMsgToRm (pRmMsg, (UINT2) u4Offset));
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynProcessIntfInfo                                    */
/*                                                                           */
/* Description  : This function processes the intf information               */
/*                                                                           */
/* Input        : pRmMsg     -    RM Message                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3RedDynProcessIntfInfo (tRmMsg * pRmMsg)
{
    UINT4               u4Offset = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedDynProcessIntfInfo\r\n");

    /* Skip the message type and packet length */
    u4Offset = sizeof (UINT1) + sizeof (UINT2);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedDynProcessIntfInfo\r\n");

    return (O3RedIfProcessIntfInfo (pRmMsg, &u4Offset));
}

/***************************************************************************
  Function Name   : O3RedDynSendRtrIdToStandby
  Description     : This function syncs OSPF Router ID with STBY whenever
                    router-id is elected dynamically
  Input(s)        : u4RtrId - Router ID
  Output(s)       : None
  Returns         : None
****************************************************************************/
INT4
O3RedDynSendRtrIdToStandby (tV3OsRouterId RtrId, UINT4 u4ContextId)
{

    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = OSPFV3_RED_RTR_ID;
    UINT4               u4NodeState = gV3OsRtr.ospfRedInfo.u4RmState;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: O3RedDynSendRtrIdToStandby \n");

    /* Send router ID information only if current node state is *
     * active and peer is up */
    if (u4NodeState != OSPFV3_RED_ACTIVE_STANDBY_UP)
    {
        return OSPFV3_SUCCESS;
    }

    /* Length of the packet = Type (1) + msg size (2)
     *                        + router id (4)
     + context id (4) */

    u2MsgLen = (UINT2) OSPFV3_ROUTER_ID_INFO;

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate buffer for sending Router ID to Standby"));
        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                         u4ContextId,
                         "Cannot allocate buffer for sending Router ID to Standby"
                         "for router-id :%x\n", RtrId);

        return OSPFV3_FAILURE;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, u4ContextId);

    OSPFV3_RED_PUT_N_BYTE (pRmMsg, RtrId, u4Offset, MAX_IPV4_ADDR_LEN);
    V3OspfEnqMsgToRm (pRmMsg, u2MsgLen);

    OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                     u4ContextId,
                     " sending Router ID to Standby"
                     " for router-id :%x\n", RtrId);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynSendRouterIdToStandby\r\n");
    return OSPFV3_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3RedDynProcessRouterIdInfo                                */
/*                                                                           */
/* Description  : This function will process the dynamic update of router id */
/*                change                                                     */
/*                                                                           */
/* Input        : pRmMsg - The packet Received from RM                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3RedDynProcessRouterIdInfo (tRmMsg * pRmMsg)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsRouterId       RtrId;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: O3RedDynProcessRouterIdInfo\r\n");

    MEMSET (RtrId, OSPFV3_ZERO, OSPFV3_RTR_ID_LEN);

    /* Length of the packet = Type (1) + Length field (2)
     *                        + context id (4) + router id (4)
     */

    /* Skip Message type and packet length */
    u4Offset += OSPFV3_ONE;
    u4Offset += OSPFV3_TWO;

    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, u4ContextId);

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

    if (pV3OspfCxt == NULL)
    {
        OSPFV3_EXT_TRC1 (OSPFV3_RM_TRC,
                         gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                         u4ContextId, "Context : %d "
                         "not available\r\n", u4ContextId);
        return;
    }
    OSPFV3_RED_GET_N_BYTE (pRmMsg, RtrId, u4Offset, sizeof (RtrId));

    if (V3UtilRtrIdComp (pV3OspfCxt->rtrId, RtrId) != OSPFV3_EQUAL)
    {
        MEMCPY (pV3OspfCxt->rtrId, RtrId, OSPFV3_RTR_ID_LEN);
    }
    pV3OspfCxt->u1RouterIdStatus = OSPFV3_ROUTERID_DYNAMIC;
    /* Get the interface                        */
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynProcessRouterIdInfo\r\n");
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function     : O3RedDynOspfClearToStandby                                 */
 /*                                                                           */
 /* Description  : This function will process the dynamic update of           */
 /*                OSPF clear process to standby                              */
 /*                                                                           */
 /* Input        : u1Flag - To reset the OSPF Process                         */
 /*                u4ContextId - ContextID                                    */
 /*                                                                           */
 /* Output       : None                                                       */
 /*                                                                           */
 /* Returns      : VOID                                                       */
 /*                                                                           */
 /*****************************************************************************/
INT4
O3RedDynOspfClearToStandby (UINT1 u1Flag, UINT4 u4contextId)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = OSPFV3_RED_CLEAR;
    UINT4               u4NodeState = gV3OsRtr.ospfRedInfo.u4RmState;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: O3RedDynSendRtrIdToStandby \n");

    /* Send OSPF clear process */
    if (u4NodeState != OSPFV3_RED_ACTIVE_STANDBY_UP)
    {
        return OSPFV3_SUCCESS;
    }
    /* Length of u1MsgType = u1MsgType (1) + u2MsgLen (2)
     *                         + u4contextId (4)
     *                         + u1Flag (1) */

    u2MsgLen = OSPFV3_CLEAR_INFO;

    if ((pRmMsg = RM_ALLOC_TX_BUF ((UINT4) u2MsgLen)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Cannot allocate buffer for ospf clear process to standby"));
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        u4contextId,
                        "Cannot allocate buffer for ospf clear process to standby");
        return OSPFV3_FAILURE;
    }

    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1MsgType);
    OSPFV3_RED_PUT_2_BYTE (pRmMsg, u4Offset, u2MsgLen);
    OSPFV3_RED_PUT_4_BYTE (pRmMsg, u4Offset, u4contextId);
    OSPFV3_RED_PUT_1_BYTE (pRmMsg, u4Offset, u1Flag);

    V3OspfEnqMsgToRm (pRmMsg, u2MsgLen);

    OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                    u4contextId, " Sync ospf clear process to Standby");
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynOspfClearToStandby\r\n");
    return OSPFV3_SUCCESS;

}

 /*****************************************************************************/
 /*                                                                           */
 /* Function     : O3RedDynOspfClearProcess                                   */
 /*                                                                           */
 /* Description  : This function will process the OSPF clear process          */
 /*                received from active                                       */
 /*                                                                           */
 /* Input        : pRmMsg                                                     */
 /*                                                                           */
 /* Output       : None                                                       */
 /*                                                                           */
 /* Returns      : VOID                                                       */
 /*                                                                           */
 /*****************************************************************************/
PUBLIC VOID
O3RedDynOspfClearProcess (tRmMsg * pRmMsg)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1Flag = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTRY: O3RedDynOspfClearProcess\r\n");

    /* Length of the packet = Type (1) + Length field (2)
     *                        + context id (4) + u1Flag (1)
     */

    /* Skip Message type and packet length */
    u4Offset += OSPFV3_ONE;
    u4Offset += OSPFV3_TWO;

    OSPFV3_RED_GET_4_BYTE (pRmMsg, u4Offset, u4ContextId);
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u4Offset, u1Flag);

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

    if (pV3OspfCxt == NULL)
    {
        OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                        u4ContextId,
                        "ContextId not available to reset OSPF process\r\n");
        return;
    }

    if (u1Flag == OSPFV3_TRUE)
    {
        gu4ClearFlag = OSIX_TRUE;

        V3RtrDisableInCxt (pV3OspfCxt, OSPFV3_TRUE);

        if (V3RtrEnableInCxt (pV3OspfCxt, OSPFV3_TRUE) == OSIX_FAILURE)
        {
            OSPFV3_EXT_TRC (OSPFV3_RM_TRC,
                            u4ContextId, "Failed to enable router id \r\n");
            return;
        }
        gu4ClearFlag = OSIX_FALSE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedDynOspfClearProcess\r\n");
    return;

}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3reddyn.c                           */
/*-----------------------------------------------------------------------*/
