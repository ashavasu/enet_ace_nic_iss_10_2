/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3agtrns.c,v 1.9 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains procedures related to aggregation of
 *         translated type7 LSAs. 
 *
 *******************************************************************/
#include "o3inc.h"

/*******************************************************************/
/*                                                                 */
/* Function     :  V3RagNssaUpdtRngCostType                        */
/*                                                                 */
/* Description    : This routine updates range cost                */
/*                                                                 */
/*                .                                                */
/*                                                                 */
/* Input          : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA  */
/*                  type7Rng - Type 7 range                        */
/*                                                                 */
/* Output       : None                                             */
/*                                                                 */
/* Returns      : VOID                                             */
/*                                                                 */
/*******************************************************************/

PRIVATE VOID
V3RagNssaUpdtRngCostType (tV3OsLsaInfo * pLsaInfo, tV3OsAddrRange * pAddrRng)
{

    tV3OsExtLsaLink     extLsaLink;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaUpdtRngCostType \n");

    if (V3RtcGetExtLsaLink (pLsaInfo->pLsa,
                            pLsaInfo->u2LsaLen, &extLsaLink) == OSIX_FAILURE)
    {
        return;
    }
    if ((pAddrRng->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
    {
        (pAddrRng->metric).u4Metric = (extLsaLink.metric).u4Metric;
        (pAddrRng->metric).u4MetricType = (extLsaLink.metric).u4MetricType;
    }

    if ((extLsaLink.metric).u4MetricType == (pAddrRng->metric).u4MetricType)
    {
        if (((pAddrRng->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT) ||
            ((extLsaLink.metric).u4Metric > (pAddrRng->metric).u4Metric))
        {
            (pAddrRng->metric).u4Metric = (extLsaLink.metric).u4Metric;
        }
    }
    else if ((extLsaLink.metric).u4MetricType == OSPFV3_TYPE_2_METRIC)
    {
        (pAddrRng->metric).u4Metric = (extLsaLink.metric).u4Metric;
        (pAddrRng->metric).u4MetricType = (extLsaLink.metric).u4MetricType;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaUpdtRngCostType\n");
    return;
}

/**********************************************************************/
/*                                                                    */
/* Function     : V3RagNssaUpdtRngForLsa                              */
/*                                                                    */
/* Description  :  Updates the cost/path type for range               */
/*                 by considering the LSA whose age is not equal to   */
/*                 MAX_AGE                                            */
/*                                                                    */
/*                                                                    */
/* Input        : pArea         - Pointer to the area                 */
/*                pTransAddrRng- Pointer Translated Address Range     */
/*                                                                    */
/*                                                                    */
/* Output       : None                                                */
/*                                                                    */
/* Returns      : VOID                                                */
/*                                                                    */
/**********************************************************************/

PRIVATE UINT1
V3RagNssaUpdtRngForLsa (tV3OsArea * pArea, tV3OsAddrRange * pTransAddrRng)
{
    tV3OsAddrRange     *pScanAddrRng = NULL;
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    UINT4               u4HashKey = 0;
    tIp6Addr            lsaIp6Addr;
    tIp6Addr            tmpLsaIp6Addr;
    UINT1               u1LsaPrefixLen = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER:V3RagNssaUpdtRngForLsa \n");

    (pTransAddrRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
    (pTransAddrRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;

    TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashKey)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable, u4HashKey,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType7LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);
                if (OSPFV3_IS_MAX_AGE (pType7LsaInfo->u2LsaAge) ||
                    (pType7LsaInfo->u1TrnsltType5 == OSPFV3_FLUSHED_LSA))
                {
                    continue;
                }

                TMO_SLL_Scan (&(pArea->type7AggrLst), pScanAddrRng,
                              tV3OsAddrRange *)
                {
                    if ((pScanAddrRng->areaAggStatus != ACTIVE) &&
                        (pScanAddrRng->areaAggStatus != NOT_IN_SERVICE))
                    {
                        continue;
                    }

                    MEMSET (&lsaIp6Addr, 0, sizeof (tIp6Addr));
                    MEMSET (&tmpLsaIp6Addr, 0, sizeof (tIp6Addr));

                    u1LsaPrefixLen =
                        *((UINT1 *) (pType7LsaInfo->pLsa
                                     + OSPFV3_PREFIX_LEN_OFFSET));

                    OSPFV3_IP6_ADDR_PREFIX_COPY (lsaIp6Addr,
                                                 *(pType7LsaInfo->pLsa
                                                   + OSPFV3_ADDR_PREFIX_OFFSET),
                                                 u1LsaPrefixLen);

                    V3UtilIp6PrefixCopy (&tmpLsaIp6Addr,
                                         &lsaIp6Addr,
                                         pScanAddrRng->u1PrefixLength);

                    if ((V3UtilIp6AddrComp (&tmpLsaIp6Addr,
                                            &(pScanAddrRng->ip6Addr)) ==
                         OSPFV3_EQUAL)
                        && (u1LsaPrefixLen >= pScanAddrRng->u1PrefixLength)
                        && (pScanAddrRng->areaAggStatus == ACTIVE)
                        && (pScanAddrRng == pTransAddrRng))
                    {

                        V3RagNssaUpdtRngCostType (pType7LsaInfo, pTransAddrRng);
                        return OSIX_TRUE;
                    }
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT:V3RagNssaUpdtRngForLsa \n");
    return OSIX_FALSE;
}

/****************************************************************/
/*                                                              */
/* Function     : V3RagNssaType7AddrRangeAgg                    */
/*                                                              */
/* Description  : This routine aaggregates Type 7 LSAs          */
/*                based on Type 7 address ranges                */
/*                                                              */
/*                                                              */
/* Input        : pArea - Pointer to the area                   */
/*                pLsaInfo - Pointer to tLsaInfo for            */
/*                           Type 7 LSA                         */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/

PRIVATE INT4
V3RagNssaType7AddrRangeAgg (tV3OsArea * pArea, tV3OsLsaInfo * pLsaInfo)
{

    tV3OsAddrRange     *pTransLtdRange = NULL;
    tV3OsLsaInfo       *pFlshLsaInfo = NULL;
    tIp6Addr            lsaIp6Addr;
    tIp6Addr            tmpLsaIp6Addr;
    UINT1               u1LsaPrefixLen = 0;
    UINT1               u1MtchRng = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER:RagNssaType7AddrRangeAgg\n");

    TMO_SLL_Scan (&(pArea->type7AggrLst), pTransLtdRange, tV3OsAddrRange *)
    {
        if ((pTransLtdRange->areaAggStatus != ACTIVE) &&
            (pTransLtdRange->areaAggStatus != NOT_IN_SERVICE))
        {
            continue;
        }

        MEMSET (&lsaIp6Addr, 0, sizeof (tIp6Addr));

        u1LsaPrefixLen =
            *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));

        OSPFV3_IP6_ADDR_PREFIX_COPY (lsaIp6Addr,
                                     *(pLsaInfo->pLsa +
                                       OSPFV3_ADDR_PREFIX_OFFSET),
                                     u1LsaPrefixLen);

        V3UtilIp6PrefixCopy (&tmpLsaIp6Addr,
                             &lsaIp6Addr, pTransLtdRange->u1PrefixLength);

        if ((V3UtilIp6AddrComp (&tmpLsaIp6Addr,
                                &(pTransLtdRange->ip6Addr)) == OSPFV3_EQUAL) &&
            (u1LsaPrefixLen >= pTransLtdRange->u1PrefixLength) &&
            (pTransLtdRange->areaAggStatus == ACTIVE))
        {
            u1MtchRng = OSIX_TRUE;
            break;
        }
    }

    /* If no matching range is found */
    if (u1MtchRng == OSIX_FALSE)
    {
        return OSIX_FALSE;
    }
    if ((pLsaInfo->u2LsaAge == OSPFV3_MAX_AGE) ||
        (pLsaInfo->u1TrnsltType5 == OSPFV3_FLUSHED_LSA))
    {
        if (V3RagNssaUpdtRngForLsa (pArea, pTransLtdRange) != OSIX_TRUE)
        {
            pFlshLsaInfo =
                V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                     &(pTransLtdRange->linkStateId),
                                     &(pArea->pV3OspfCxt->rtrId), NULL, pArea);

            if ((pFlshLsaInfo != NULL) &&
                (pFlshLsaInfo->pLsaDesc->u2InternalLsaType ==
                 OSPFV3_AS_TRNSLTD_RNG_LSA))
            {
                V3AgdFlushOut (pFlshLsaInfo);
            }
        }
    }
    else
    {
        /* Updates Metric/Type for range */
        V3RagNssaUpdtRngCostType (pLsaInfo, pTransLtdRange);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT:RagNssaType7AddrRangeAgg\n");
    return OSIX_TRUE;
}

/*******************************************************************/
/*                                                                 */
/* Function     : V3RagNssaChkSelfOrgType5                         */
/*                                                                 */
/* Description  : This routine checks that if there                */
/*                is  any self -originated Type 5 LSA              */
/*                having same link state id as the LSA to be       */
/*                translated                                       */
/*                                                                 */
/* Input        : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA    */
/*                                                                 */
/* Output       : None                                             */
/*                                                                 */
/* Returns      : OSIX_TRUE - Type 7 is not Trnslted               */
/*                OSIX_FALSE - Type 7 is Trnslted                  */
/*******************************************************************/

PRIVATE UINT1
V3RagNssaChkSelfOrgType5 (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsLsHeader       lsHeader;
    tV3OsLsaInfo       *pSelfLsdbLsaInfo = NULL;
    tV3OsExtLsaLink     lsdbExtLsaLink;
    tV3OsExtLsaLink     extLsaLink;
    tV3OsPath          *pExtPath = NULL;
    tV3OsPath          *pSelfPath = NULL;
    tV3OsRtEntry       *pFwdAddrEntry = NULL;
    tV3OsRtEntry       *pLsdbFwdAddrEntry = NULL;
    UINT4               u4FwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
    UINT4               u4lsdbFwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
    UINT1               u1DoNotTrnslt = OSIX_FALSE;
    tV3OsDbNode        *pOsDbNode = NULL;
    UINT1               u1PrefixLen;
    UINT1              *pAddrPrefix = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER:V3RagNssaChkSelfOrgType5\n");

    V3UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);

    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));

    pAddrPrefix = (pLsaInfo->pLsa + OSPFV3_ADDR_PREFIX_OFFSET);

    pOsDbNode = V3IsDbNodePresent (pAddrPrefix, u1PrefixLen,
                                   pLsaInfo->pV3OspfCxt->pExtLsaHashTable,
                                   OSPFV3_AS_EXT_LSA);

    /* If Database node is not present, then return  */
    if (pOsDbNode == NULL)
    {
        return u1DoNotTrnslt;
    }

    TMO_SLL_Scan (&pOsDbNode->lsaLst, pDbLstNode, tTMO_SLL_NODE *)
    {
        pSelfLsdbLsaInfo =
            OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);

        if (V3UtilRtrIdComp (OSPFV3_RTR_ID (pLsaInfo->pV3OspfCxt),
                             pSelfLsdbLsaInfo->lsaId.advRtrId) != OSPFV3_EQUAL)
        {
            continue;
        }

        if (pSelfLsdbLsaInfo != NULL)
        {
            if (pSelfLsdbLsaInfo->u1TrnsltType5 == OSPFV3_REDISTRIBUTED_TYPE5)
            {

                V3RtcGetExtLsaLink (pSelfLsdbLsaInfo->pLsa,
                                    pSelfLsdbLsaInfo->u2LsaLen,
                                    &lsdbExtLsaLink);
                V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                                    &extLsaLink);

                if (V3UtilIp6AddrComp (&(extLsaLink.fwdAddr),
                                       &gV3OsNullIp6Addr) != OSPFV3_EQUAL)
                {
                    pFwdAddrEntry = V3RtcRtLookupInCxt
                        (pLsaInfo->pV3OspfCxt, &(extLsaLink.fwdAddr));
                    if (pFwdAddrEntry != NULL)
                    {
                        pExtPath = OSPFV3_GET_PATH (pFwdAddrEntry);

                        if ((pExtPath != NULL) &&
                            (pExtPath->u1PathType == OSPFV3_TYPE_1_METRIC))
                        {
                            u4FwdAddrCost = pExtPath->u4Cost;
                        }
                        else if ((pExtPath != NULL) &&
                                 (pExtPath->u1PathType == OSPFV3_TYPE_2_METRIC))
                        {
                            u4FwdAddrCost = pExtPath->u4Type2Cost;
                        }
                        else
                        {
                            u4FwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
                        }
                    }
                    else
                    {
                        u4FwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
                    }
                }
                else
                {
                    u4FwdAddrCost = 0;
                }
                if (V3UtilIp6AddrComp (&(lsdbExtLsaLink.fwdAddr),
                                       &gV3OsNullIp6Addr) != OSPFV3_EQUAL)
                {
                    pLsdbFwdAddrEntry =
                        V3RtcRtLookupInCxt (pLsaInfo->pV3OspfCxt,
                                            &(lsdbExtLsaLink.fwdAddr));

                    if (pLsdbFwdAddrEntry != NULL)
                    {
                        pSelfPath = OSPFV3_GET_PATH (pLsdbFwdAddrEntry);

                        if ((pSelfPath != NULL) &&
                            (pSelfPath->u1PathType == OSPFV3_TYPE_1_METRIC))
                        {
                            u4lsdbFwdAddrCost = pSelfPath->u4Cost;
                        }
                        else if ((pSelfPath != NULL) &&
                                 (pSelfPath->u1PathType ==
                                  OSPFV3_TYPE_2_METRIC))
                        {
                            u4lsdbFwdAddrCost = pSelfPath->u4Type2Cost;
                        }
                        else
                        {
                            u4FwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
                        }
                    }
                    else
                    {
                        u4FwdAddrCost = OSPFV3_LS_INFINITY_24BIT;
                    }
                }
                else
                {
                    u4lsdbFwdAddrCost = 0;
                }

                if (((lsdbExtLsaLink.metric).u4MetricType
                     == OSPFV3_TYPE_1_METRIC) &&
                    ((extLsaLink.metric).u4MetricType == OSPFV3_TYPE_2_METRIC))
                {
                    u1DoNotTrnslt = OSIX_TRUE;
                }

                if (((lsdbExtLsaLink.metric).u4MetricType
                     == OSPFV3_TYPE_2_METRIC) &&
                    ((extLsaLink.metric).u4MetricType == OSPFV3_TYPE_2_METRIC))
                {
                    if ((lsdbExtLsaLink.metric).u4Metric
                        < (extLsaLink.metric).u4Metric)
                    {
                        u1DoNotTrnslt = OSIX_TRUE;
                    }
                    else if ((lsdbExtLsaLink.metric).u4Metric
                             == (extLsaLink.metric).u4Metric)
                    {
                        /* Compare distance to fwd address  and decide */
                        if (u4lsdbFwdAddrCost < u4FwdAddrCost)
                        {
                            u1DoNotTrnslt = OSIX_TRUE;
                        }
                    }
                }

                if (((lsdbExtLsaLink.metric).u4MetricType
                     == OSPFV3_TYPE_1_METRIC) &&
                    ((extLsaLink.metric).u4MetricType == OSPFV3_TYPE_1_METRIC))
                {
                    /* Compare (cost+dis to fwd addr) and decide */
                    if (((lsdbExtLsaLink.metric).u4Metric + u4lsdbFwdAddrCost) <
                        ((extLsaLink.metric).u4Metric + u4FwdAddrCost))
                    {
                        u1DoNotTrnslt = OSIX_TRUE;
                    }
                }
            }
            else if (pSelfLsdbLsaInfo->pLsaDesc != NULL)
            {
                pSelfLsdbLsaInfo->pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT:V3RagNssaChkSelfOrgType5\n");
    return u1DoNotTrnslt;
}

/********************************************************************/
/*                                                                  */
/* Function     : V3RagNssaTrnsltType7NotInRng                      */
/*                                                                  */
/* Description  : This routine translates Type 7                    */
/*                which does not fall in any range                  */
/*                                                                  */
/* Input        : pLsaInfo - Pointer to tLsaInfo for Type 7 LSA     */
/*                                                                  */
/* Output       : None                                              */
/*                                                                  */
/* Returns      : Void                                              */
/*                                                                  */
/********************************************************************/

PRIVATE VOID
V3RagNssaTrnsltType7NotInRng (tV3OsLsaInfo * pLsaInfo)
{

    UINT1               u1DoNotTrnslt = OSIX_FALSE;
    tV3OsRtEntry       *pDestRtEntry = NULL;
    tIp6Addr            destIp6Addr;
    UINT1               u1PrefixLen = 0;
    tV3OsPath          *pPath = NULL;

    /* Handle the case :
       Presence of functionally equivalent Type 5 LSA
       generated by some other router 
     */

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaTrnsltType7NotInRng\n");

    MEMSET (&destIp6Addr, 0, sizeof (tIp6Addr));
    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));

    OSPFV3_IP6_ADDR_PREFIX_COPY (destIp6Addr,
                                 *(pLsaInfo->pLsa +
                                   OSPFV3_ADDR_PREFIX_OFFSET), u1PrefixLen);

    pDestRtEntry =
        V3RtcFindRtEntryInCxt (pLsaInfo->pV3OspfCxt, (VOID *) &(destIp6Addr),
                               u1PrefixLen, OSPFV3_DEST_NETWORK);

    if (pDestRtEntry != NULL)
    {
        pPath = OSPFV3_GET_PATH (pDestRtEntry);

        if ((pPath != NULL) &&
            ((pPath->u1PathType == OSPFV3_INTRA_AREA) ||
             (pPath->u1PathType == OSPFV3_INTER_AREA)))
        {
            return;
        }
    }
    else
    {
        return;
    }

    /* Check if there is any self -originated Type 5 LSA
       having same link state id as the LSA to be 
       translated
     */

    u1DoNotTrnslt = V3RagNssaChkSelfOrgType5 (pLsaInfo);
    if (u1DoNotTrnslt == OSIX_TRUE)
    {
        OSPFV3_TRC (OSPFV3_NSSA_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "LSDB Type 5 LSA is same or preferred \n");
        return;
    }

    /* For AS external LSA, backbone area pointer is passed as the
     * first parameter. This pointer ir used to retrive the context
     * pointer
     */

    V3GenerateLsa (pLsaInfo->pV3OspfCxt->pBackbone, OSPFV3_AS_TRNSLTD_EXT_LSA,
                   &(pDestRtEntry->linkStateId),
                   (UINT1 *) pDestRtEntry, OSIX_FALSE);

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaTrnsltType7NotInRng\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagNssaType7LsaTranslation                          */
/*                                                                      */
/* Description  : This routine performs Type 7 to Type 5  translation   */
/*                                                                      */
/* Input        : pArea - Pointer to area                               */
/*                pLsaInfo - Pointer to tLsaInfo                        */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/

PUBLIC VOID
V3RagNssaType7LsaTranslation (tV3OsArea * pArea, tV3OsLsaInfo * pLsaInfo)
{
    tV3OsRtEntry       *pDestRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tIp6Addr            addrPrefix;
    tV3OsExtLsaLink     lsdbExtLsaLink;
    UINT1               u1GeneralOption = OSPFV3_INVALID;
    UINT1               u1PrefixLen = 0;

    /*
       u1TrnsltType5 can take three values     
       REDISTRIBUTED_TYPE5  - 3     
       For Type 5 implies route imported from RTM            
       OSIX_TRUE             - 1     
       For Type 5 it implies it is translated Type 5    
       OSPF_FALSE    - 2    
       For Type 5 it implies unapproved Trnsltd Type 5        
       At the end of translation any unapproved Type 5         
       needs to be FLUSHED    
     */

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaType7LsaTranslation\n");

    if ((!OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt)) ||
        (pArea->u1NssaTrnsltrState == OSPFV3_TRNSLTR_STATE_DISABLED) ||
        (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE) ||
        (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE))
    {
        return;
    }
    if (OSPFV3_IS_P_BIT_SET (pLsaInfo->pLsa) == 0)
    {

        OSPFV3_TRC2 (OSPFV3_NSSA_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                     "Pbit Clear, LSA FRM Rtr %x, for net %x not trnsltd\n",
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.advRtrId),
                     OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.linkStateId));
        return;
    }

    u1GeneralOption = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE));

    if (OSPFV3_IS_F_BIT_SET (u1GeneralOption) == 0)
    {
        OSPFV3_TRC (OSPFV3_NSSA_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "For Nssa Lsa F Bit is not set so no Translation\n");
        return;
    }

    V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen, &lsdbExtLsaLink);

    if (V3UtilIp6AddrComp (&(lsdbExtLsaLink.fwdAddr),
                           &gV3OsNullIp6Addr) == OSPFV3_EQUAL)
    {
        OSPFV3_TRC2 (OSPFV3_NSSA_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                     "Fwd Addr 0.0.0.0, LSA FRM Rtr %x,"
                     " for net %x not trnsltd\n",
                     pLsaInfo->lsaId.advRtrId, pLsaInfo->lsaId.linkStateId);
        return;
    }

    MEMSET (&addrPrefix, 0, sizeof (tIp6Addr));

    u1PrefixLen = *((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET));
    OSPFV3_IP6_ADDR_PREFIX_COPY (addrPrefix,
                                 *(pLsaInfo->pLsa +
                                   OSPFV3_ADDR_PREFIX_OFFSET), u1PrefixLen);

    pDestRtEntry =
        V3RtcFindRtEntryInCxt (pArea->pV3OspfCxt, (VOID *) &(addrPrefix),
                               u1PrefixLen, OSPFV3_DEST_NETWORK);

    if (pDestRtEntry != NULL)
    {
        pPath = OSPFV3_GET_PATH (pDestRtEntry);

        if ((pPath != NULL) &&
            ((pPath->u1PathType == OSPFV3_INTRA_AREA) ||
             (pPath->u1PathType == OSPFV3_INTER_AREA) ||
             (pPath->pLsaInfo != pLsaInfo)))
        {
            return;
        }
    }
    if (V3RagNssaType7AddrRangeAgg (pArea, pLsaInfo) == OSIX_TRUE)
    {
        OSPFV3_TRC2 (OSPFV3_NSSA_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                     "LSA FRM Rtr %x, for net %x either"
                     "aggregated or DoNotAdvertise\n",
                     pLsaInfo->lsaId.advRtrId, pLsaInfo->lsaId.linkStateId);
        return;
    }

    V3RagNssaTrnsltType7NotInRng (pLsaInfo);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaType7LsaTranslation\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagNssaAdvertiseType7Rngs                           */
/*                                                                      */
/* Description  : This routine adv Type 7 Ranges                        */
/*                                                                      */
/* Input        : pArea - Pointer to area                               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagNssaAdvertiseType7Rngs (tV3OsArea * pArea)
{
    tV3OsAddrRange     *pTransltdRange = NULL;
    tV3OsLsaInfo       *pSelfLsdbLsaInfo = NULL;
    UINT1               u1GenType5FrmRng = OSIX_FALSE;
    UINT1               u1ReOrgFrmRng = OSIX_FALSE;
    tV3OsExtLsaLink     lsdbExtLsaLink;
    tV3OsExtRoute      *pExtRoute = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaAdvertiseType7Rngs\n");

    if ((pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE) ||
        (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE))
    {
        /* LSA's cannot be originated during graceful restart */
        return;
    }

    TMO_SLL_Scan (&(pArea->type7AggrLst), pTransltdRange, tV3OsAddrRange *)
    {
        if ((pTransltdRange->areaAggStatus != ACTIVE) &&
            (pTransltdRange->areaAggStatus != NOT_IN_SERVICE))
        {
            continue;
        }

        if (pTransltdRange->u1AggrEffect == OSPFV3_DO_NOT_ADVERTISE_MATCHING)
        {
            continue;
        }

        if ((pTransltdRange->metric).u4Metric == OSPFV3_LS_INFINITY_24BIT)
        {
            continue;
        }

        V3RagNssaFlshType5TrnsltdInCxt (pArea->pV3OspfCxt, pTransltdRange);

        pSelfLsdbLsaInfo =
            V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                 &(pTransltdRange->linkStateId),
                                 &(pArea->pV3OspfCxt->rtrId), NULL, pArea);

        u1GenType5FrmRng = OSIX_FALSE;
        u1ReOrgFrmRng = OSIX_FALSE;

        if (pSelfLsdbLsaInfo != NULL)
        {
            pSelfLsdbLsaInfo->pLsaDesc->pAssoPrimitive =
                (UINT1 *) pTransltdRange;
            V3RtcGetExtLsaLink (pSelfLsdbLsaInfo->pLsa,
                                pSelfLsdbLsaInfo->u2LsaLen, &lsdbExtLsaLink);

            if (((lsdbExtLsaLink.metric).u4Metric !=
                 (pTransltdRange->metric).u4Metric) ||
                ((lsdbExtLsaLink.metric).u4MetricType !=
                 (pTransltdRange->metric).u4MetricType) ||
                (V3UtilIp6AddrComp (&(lsdbExtLsaLink.fwdAddr),
                                    &gV3OsNullIp6Addr) != OSPFV3_EQUAL) ||
                (lsdbExtLsaLink.u4ExtRouteTag !=
                 pTransltdRange->u4AddrRangeRtTag))
            {
                if (pSelfLsdbLsaInfo->pLsaDesc->u2InternalLsaType
                    == OSPFV3_AS_TRNSLTD_RNG_LSA)
                {
                    u1ReOrgFrmRng = OSIX_TRUE;
                    pExtRoute =
                        V3ExtrtFindRouteInCxt
                        (pArea->pV3OspfCxt, &(pTransltdRange->ip6Addr),
                         pTransltdRange->u1PrefixLength);

                    if (pExtRoute != NULL)
                    {
                        if ((pExtRoute->metric).u4MetricType !=
                            (pTransltdRange->metric).u4MetricType)
                        {
                            (pTransltdRange->metric).u4Metric =
                                (pExtRoute->metric).u4Metric;
                            (pTransltdRange->metric).u4MetricType =
                                (pExtRoute->metric).u4MetricType;
                        }
                    }
                }
                else
                {
                    u1GenType5FrmRng = OSIX_TRUE;
                }
                if ((lsdbExtLsaLink.metric).u4MetricType
                    == OSPFV3_TYPE_2_METRIC)
                {

                    if ((pTransltdRange->metric).u4MetricType ==
                        OSPFV3_TYPE_2_METRIC)
                    {
                        if ((lsdbExtLsaLink.metric).u4Metric >
                            (pTransltdRange->metric).u4Metric)
                        {
                            (pTransltdRange->metric).u4Metric =
                                (lsdbExtLsaLink.metric).u4Metric;
                        }
                    }
                    else
                    {
                        (pTransltdRange->metric).u4Metric =
                            (lsdbExtLsaLink.metric).u4Metric;
                    }
                    (pTransltdRange->metric).u4MetricType =
                        (lsdbExtLsaLink.metric).u4MetricType;
                }

                if (((pTransltdRange->metric).u4MetricType ==
                     OSPFV3_TYPE_1_METRIC) &&
                    ((lsdbExtLsaLink.metric).u4MetricType
                     == OSPFV3_TYPE_1_METRIC))
                {
                    if ((lsdbExtLsaLink.metric).u4Metric >
                        (pTransltdRange->metric).u4Metric)
                    {
                        (pTransltdRange->metric).u4Metric =
                            (lsdbExtLsaLink.metric).u4Metric;
                    }
                }
            }

            if ((u1ReOrgFrmRng == OSIX_FALSE) &&
                (u1GenType5FrmRng == OSIX_FALSE))
            {
                if (pSelfLsdbLsaInfo->u1TrnsltType5 == OSIX_FALSE)
                {
                    pSelfLsdbLsaInfo->u1TrnsltType5 = OSIX_TRUE;
                }
            }
        }
        else
        {
            u1GenType5FrmRng = OSIX_TRUE;
        }

        if (u1GenType5FrmRng == OSIX_TRUE)
        {
            /* For AS external LSA, backbone area pointer is passed as the
             * first parameter. This pointer ir used to retrive the context
             * pointer
             */

            V3GenerateLsa (pArea->pV3OspfCxt->pBackbone,
                           OSPFV3_AS_TRNSLTD_RNG_LSA,
                           &(pTransltdRange->linkStateId),
                           (UINT1 *) pTransltdRange, OSIX_FALSE);
        }
        else if (u1ReOrgFrmRng == OSIX_TRUE)
        {
            V3SignalLsaRegenInCxt (pArea->pV3OspfCxt, OSPFV3_SIG_NEXT_INSTANCE,
                                   (UINT1 *) pSelfLsdbLsaInfo->pLsaDesc);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaAdvertiseType7Rngs\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagNssaFlshType5TrnsltdInCxt                        */
/*                                                                      */
/* Description  : Flush translated Type 5 falling in the range to be    */
/*                advertised.                                           */
/*                                                                      */
/* Input        : pV3OspfCxt    - Context pointer                       */
/*                pAddrRng      - Address Range                         */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagNssaFlshType5TrnsltdInCxt (tV3OspfCxt * pV3OspfCxt,
                                tV3OsAddrRange * pAddrRng)
{
    tV3OsLsaInfo       *pType5LsaInfo = NULL;
    tV3OsAddrRange     *pLsaAddrRng = NULL;
    tIp6Addr            lsaIp6Addr;
    UINT1               u1LsaPrefixLen = 0;
    tIp6Addr            rngIp6Addr;
    tIp6Addr            tmpLsaIp6Addr;
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaFlshType5Trnsltd\n");

    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashKey)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pV3OspfCxt->pExtLsaHashTable, u4HashKey,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType5LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);

                if (V3UtilRtrIdComp ((pType5LsaInfo->lsaId).advRtrId,
                                     pV3OspfCxt->rtrId) != OSPFV3_EQUAL)
                {
                    continue;
                }

                if (pType5LsaInfo->u1TrnsltType5 != OSIX_TRUE)
                {
                    continue;
                }

                MEMSET (&lsaIp6Addr, 0, sizeof (tIp6Addr));
                MEMSET (&tmpLsaIp6Addr, 0, sizeof (tIp6Addr));

                u1LsaPrefixLen =
                    *((UINT1 *) (pType5LsaInfo->pLsa
                                 + OSPFV3_PREFIX_LEN_OFFSET));

                OSPFV3_IP6_ADDR_PREFIX_COPY (lsaIp6Addr,
                                             *(pType5LsaInfo->pLsa +
                                               OSPFV3_ADDR_PREFIX_OFFSET),
                                             u1LsaPrefixLen);

                V3UtilIp6PrefixCopy (&rngIp6Addr,
                                     &(pAddrRng->ip6Addr),
                                     pAddrRng->u1PrefixLength);

                if (V3UtilIp6AddrComp (&rngIp6Addr, &lsaIp6Addr) ==
                    OSPFV3_EQUAL)
                {
                    continue;
                }

                V3UtilIp6PrefixCopy (&tmpLsaIp6Addr,
                                     &lsaIp6Addr, pAddrRng->u1PrefixLength);

                if (V3UtilIp6AddrComp (&tmpLsaIp6Addr,
                                       &(pAddrRng->ip6Addr)) == OSPFV3_EQUAL)
                {
                    if (pType5LsaInfo->pLsaDesc->u2InternalLsaType
                        == OSPFV3_AS_TRNSLTD_RNG_LSA)
                    {
                        pLsaAddrRng = (tV3OsAddrRange *) (VOID *)
                            pType5LsaInfo->pLsaDesc->pAssoPrimitive;

                        if (pLsaAddrRng->u1PrefixLength >
                            pAddrRng->u1PrefixLength)
                        {
                            OSPFV3_TRC (OSPFV3_NSSA_TRC,
                                        pV3OspfCxt->u4ContextId,
                                        "LSA is frm More Spec Rng."
                                        " Shud not flsh \n");
                        }
                        else
                        {
                            V3AgdFlushOut (pType5LsaInfo);
                        }
                    }
                    else
                    {
                        V3AgdFlushOut (pType5LsaInfo);
                    }
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaFlshType5Trnsltd\n");
    return;
}

/****************************************************************/
/*                                                              */
/* Function     : V3RagNssaType7To5TranslatorInCxt              */
/*                                                              */
/* Description  : This routine scans through Type 7 LSA         */
/*                list in NSSA area and invokes                 */
/*                "NssaType7LsaTranslation"  for each LSA       */
/*                                                              */
/* Input        : pV3OspfCxt - Context pointer                  */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3RagNssaType7To5TranslatorInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;
    tV3OsLsaInfo       *pType7LsaInfo = NULL;
    tV3OsLsaInfo       *pType5LsaInfo = NULL;
    tV3OsAddrRange     *pScanAddrRng = NULL;
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaType7To5TranslatorInCxt\n");

    if (pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        /* LSA cannot be generated in GR mode */
        return;
    }

    if (pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
    {
        return;
    }

    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashKey)
    {
        TMO_HASH_Scan_Bucket (pV3OspfCxt->pExtLsaHashTable, u4HashKey,
                              pOsDbNode, tV3OsDbNode *)
        {
            TMO_SLL_Scan (&pOsDbNode->lsaLst, pLstNode, tTMO_SLL_NODE *)
            {
                pType5LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);

                if (pType5LsaInfo->u1TrnsltType5 == OSIX_TRUE)
                {
                    pType5LsaInfo->u1TrnsltType5 = OSIX_FALSE;
                }
            }
        }

    }

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        return;
    }

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if ((pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
            (pArea->u1NssaTrnsltrState == OSPFV3_TRNSLTR_STATE_ENAELCTD))
        {
            /* Initialize the Lsacount of the Ranges before starting the 
               Translation */

            TMO_SLL_Scan (&(pArea->type7AggrLst), pScanAddrRng,
                          tV3OsAddrRange *)
            {
                if ((pScanAddrRng->areaAggStatus != ACTIVE) &&
                    (pScanAddrRng->areaAggStatus != NOT_IN_SERVICE))
                {
                    continue;
                }
                (pScanAddrRng->metric).u4Metric = OSPFV3_LS_INFINITY_24BIT;
                (pScanAddrRng->metric).u4MetricType = OSPFV3_TYPE_1_METRIC;
            }
            TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashKey)
            {
                OSPFV3_DYNM_HASH_BUCKET_SCAN (pArea->pNssaLsaHashTable,
                                              u4HashKey, pOsDbNode,
                                              pTmpDbNode, tV3OsDbNode *)
                {
                    OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst,
                                          pLstNode, pTempNode, tTMO_SLL_NODE *)
                    {
                        pType7LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                             nextLsaInfo,
                                                             pLstNode);

                        V3RagNssaType7LsaTranslation (pArea, pType7LsaInfo);
                    }
                }
            }
            V3RagNssaAdvertiseType7Rngs (pArea);
        }
    }                            /* End of TMO_SLL_Scan of Area List */

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaType7To5TranslatorInCxt\n");
    return;
}

/************************************************************************/
/*                                                                      */
/* Function     : V3RagNssaProcAggRangeInCxt                            */
/*                                                                      */
/* Description  : This routine performs Type 7 to Type 5  translation   */
/*                                                                      */
/* Input        : pV3OspfCxt - Context pointer                          */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3RagNssaProcAggRangeInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsLsaInfo       *pType5LsaInfo = NULL;
    UINT4               u4HashKey = 0;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tV3OsDbNode        *pOsDbNode = NULL;
    tV3OsDbNode        *pTmpDbNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3RagNssaProcAggRangeInCxt\n");

    V3RagNssaType7To5TranslatorInCxt (pV3OspfCxt);

    TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashKey)
    {
        OSPFV3_DYNM_HASH_BUCKET_SCAN (pV3OspfCxt->pExtLsaHashTable, u4HashKey,
                                      pOsDbNode, pTmpDbNode, tV3OsDbNode *)
        {
            OSPFV3_DYNM_SLL_SCAN (&pOsDbNode->lsaLst, pLstNode, pTempNode,
                                  tTMO_SLL_NODE *)
            {
                pType5LsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo,
                                                     nextLsaInfo, pLstNode);
                if ((pType5LsaInfo->u1TrnsltType5 == OSIX_FALSE) &&
                    (V3UtilRtrIdComp ((pV3OspfCxt->rtrId),
                                      (pType5LsaInfo->lsaId.advRtrId))
                     == OSPFV3_EQUAL))
                {
                    /* While flushing check what all clean up is required */
                    V3AgdFlushOut (pType5LsaInfo);
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3RagNssaProcAggRange\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3agtrns.c                     */
/*-----------------------------------------------------------------------*/
