/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rintra.c,v 1.12 2017/12/26 13:34:29 siva Exp $
 *
 * Description: This file contains procedures related to the
 *             intra-area route calculations.
 *         
 *
 *******************************************************************/
#include "o3inc.h"
PRIVATE VOID        V3RtcProcessRtPrefix
PROTO ((tV3OsCandteNode * pSpfNode, tV3OsPrefixInfo * pPrefInfo,
        tV3OsArea * pArea, tV3OsLsaInfo * pLsaInfo, UINT1 u1Status));

PRIVATE tV3OsLsaInfo *V3RtcGetOptionsRtrLsa PROTO ((tV3OsCandteNode * pVertex));
PRIVATE VOID        V3RtcUpdateIntraPathNextHops
PROTO ((tV3OsCandteNode * pSpfNode, tV3OsPath * pPath));

/*****************************************************************************/
/* Function     : V3RtcCalculateIntraAreaRoute                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 Section 16.1.                         */
/*                            RFC-2740 Section 3.8.1                         */
/*                This procedure calculates the Intra-Area routes            */
/*                corresponding to the specified area.                       */
/*                                                                           */
/*                pArea   : AREA whose intra-area routes are to be           */
/*                          calculated                                       */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS if the Intra-Area route calculation is        */
/*                successful otherwise returns OSIX_FAILURE                  */
/*****************************************************************************/
PUBLIC INT4
V3RtcCalculateIntraAreaRoute (tV3OsArea * pArea)
{
    UINT4               u4BdrRtrCnt = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateIntraAreaRoute\n");

    OSPFV3_TRC1 (OSPFV3_RT_TRC | OSPFV3_RTMODULE_TRC,
                 pArea->pV3OspfCxt->u4ContextId,
                 "Intra-Area Route Calculation: Area %x\n",
                 OSPFV3_BUFFER_DWFROMPDU (pArea->areaId));

    /* Mark all the nodes of the SPF tree as OSPFV3_NOT_FOUND */
    V3RtcMarkAllSpfNodes (pArea->pSpf);

    pArea->bPreviousTransitCapability = pArea->bTransitCapability;
    pArea->bTransitCapability = OSIX_FALSE;
    pArea->u4AbrStatFlg = OSIX_FALSE;
    u4BdrRtrCnt = pArea->u4AreaBdrRtrCount;

    if (pArea->u4ActIntCount > 0)
    {
        /* Construct the SPF tree for this area */
        V3RtcBuildSpfTree (pArea);
    }

    /* Process the unreachable SPF tree nodes */
    V3RtcProcessSpfNodes (pArea);

    if (u4BdrRtrCnt != pArea->u4AreaBdrRtrCount)
    {
        pArea->u4AbrStatFlg = OSIX_TRUE;
    }

    /* The area is becoming transit area */
    if ((pArea->bPreviousTransitCapability == OSIX_FALSE) &&
        (pArea->bTransitCapability == OSIX_TRUE))
    {
        V3RagFlushAggLsaInTransitArea (pArea);
    }
    /* Area is loosing tranist capability */
    else if ((pArea->bPreviousTransitCapability == OSIX_TRUE) &&
             (pArea->bTransitCapability == OSIX_FALSE))
    {
        V3RagTransAreaFlushLsa (pArea);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3RtcCalculateIntraAreaRoute\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : V3RtcProcessIntraAreaPrefLsa                               */
/*                                                                           */
/* Description  : This function scans all the prefixs and calculate the      */
/*                routes corresponding to the prefixes.                      */
/*                                                                           */
/* Input        : pSpfNode: Pointer to the SPF node whose Intra-Area         */
/*                          prefix LSA is being processed                    */
/*                pLsaInfo: Pointer to Intra-Area prefix LSA.                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessIntraAreaPrefLsa (tV3OsCandteNode * pSpfNode,
                              tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa,
                              tV3OsArea * pArea, UINT1 u1Status)
{
    UINT2               u2NoPrefix = 0;
    UINT2               u2Counter = 0;
    UINT1              *pCurrPtr = NULL;
    tV3OsPrefixInfo     prefixInfo;
    UINT2               u2Age;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessIntraAreaPrefLsa\n");

    if ((pSpfNode->u1VertType == OSPFV3_VERT_ROUTER) &&
        (V3UtilRtrIdComp (pSpfNode->vertexId.vertexRtrId,
                          pArea->pV3OspfCxt->rtrId) == OSPFV3_EQUAL))
    {
        OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtcProcessIntraAreaPrefLsa\n");

        return;
    }

    u2Age = OSPFV3_BUFFER_WFROMPDU (pLsa);
    pCurrPtr = pLsa + OSPFV3_LS_HEADER_SIZE;
    u2NoPrefix = OSPFV3_LGET2BYTE (pCurrPtr);

    pCurrPtr += (OSPFV3_LS_TYPE_LEN + OSPFV3_LINKSTATE_ID_LEN
                 + OSPFV3_RTR_ID_LEN);

    if ((u1Status != OSPFV3_DELETED) && (OSPFV3_IS_MAX_AGE (u2Age)))
    {
        u1Status = OSPFV3_DELETED;
    }

    /* Processing all the prefixes */
    for (u2Counter = 0; u2Counter < u2NoPrefix; u2Counter++)
    {
        pCurrPtr = V3UtilExtractPrefixFromLsa (pCurrPtr, &prefixInfo);

        /* If LA bit is set in the prefix, then it is the virtual 
         * link information */
        if (OSPFV3_IS_LA_BIT_SET (prefixInfo.u1PrefixOpt))
        {
            V3RtcProcessLABitPrefix (pSpfNode, &prefixInfo, pArea, u1Status);
        }
        V3RtcProcessRtPrefix (pSpfNode, &prefixInfo, pArea, pLsaInfo, u1Status);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcessIntraAreaPrefLsa\n");

}

/*****************************************************************************/
/* Function     : V3RtcProcessLABitPrefix                                    */
/*                                                                           */
/* Description  : This function is to process the LA-bit set prefix          */
/*                                                                           */
/* Input        : pVertex   : Pointer to the SPF node whose Intra-Area       */
/*                            prefix is being processed                      */
/*                pPrefInfo : Pointer to Intra-Area prefix LSA.              */
/*                pLsaInfo  : Pointer to Intra-Area prefix LSA.              */
/*                u1Status  : Status of the prefix.                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcProcessLABitPrefix (tV3OsCandteNode * pVertex, tV3OsPrefixInfo * pPrefInfo,
                         tV3OsArea * pArea, UINT1 u1Status)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsRtEntry       *pAbrRtEntry = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1Found = OSIX_FALSE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessLABitPrefix\n");
    TMO_SLL_Scan (&(pArea->pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                          nextVirtIfNode, pLstNode);
        if ((V3UtilRtrIdComp (pVertex->vertexId.vertexRtrId,
                              pInterface->destRtrId) == OSPFV3_EQUAL) &&
            (V3UtilAreaIdComp (pArea->areaId,
                               pInterface->transitAreaId) == OSPFV3_EQUAL))
        {
            u1Found = OSIX_TRUE;
            break;
        }
    }

    if (u1Found == OSIX_TRUE)
    {
        u1Found = OSIX_FALSE;
        pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf));
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
        {
            if (pArea->pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
            {
                if ((pAbrRtEntry =
                     V3RtcFindRtEntryInCxt
                     (pArea->pV3OspfCxt, (VOID *) &pInterface->destRtrId, 0,
                      OSPFV3_DEST_AREA_BORDER)) != NULL)
                {
                    V3VifSetVifValues (pInterface, pAbrRtEntry);
                    u1Found = OSIX_TRUE;
                }
            }
            else
            {
                V3VifSetVLValues (pInterface, pVertex);
                u1Found = OSIX_TRUE;
            }
        }

        if (u1Status == OSPFV3_CREATED)
        {
            OSPFV3_IP6_ADDR_COPY (pNbr->nbrIpv6Addr, pPrefInfo->addrPrefix);

            if (u1Found == OSIX_TRUE)
            {
                pInterface->operStatus = OSPFV3_ENABLED;
                V3IfUp (pInterface);
            }
        }

        if (u1Status == OSPFV3_UPDATED)
        {
            OSPFV3_IP6_ADDR_COPY (pNbr->nbrIpv6Addr, pPrefInfo->addrPrefix);

            if ((u1Found == OSIX_TRUE) &&
                (pInterface->u1IsmState == OSPFV3_IFS_DOWN))
            {
                pInterface->operStatus = OSPFV3_ENABLED;
                if (V3UtilIp6AddrComp
                    (&pInterface->ifIp6Addr, &gV3OsNullIp6Addr) != OSPFV3_EQUAL)
                {
                    V3IfUp (pInterface);
                }
            }
        }

        if (u1Status == OSPFV3_DELETED)
        {
            MEMSET (&(pNbr->nbrIpv6Addr), 0, OSPFV3_IPV6_ADDR_LEN);
            if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
            {
                OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_DOWN);
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcessLABitPrefix\n");

}

/*****************************************************************************/
/* Function     : V3RtcProcessRtPrefix                                       */
/*                                                                           */
/* Description  : This function calculate route entry for the prefix         */
/*                advertised in the intra area prefix LSA                    */
/*                                                                           */
/* Input        : pSpfNode  : Pointer to the SPF node whose Intra-Area       */
/*                            prefix is being processed                      */
/*                pPrefInfo : Pointer to Intra-Area prefix LSA.              */
/*                pArea     : Pointer to the area                            */
/*                pLsaInfo  : Pointer to the LSA information which contain   */
/*                            the prefix information.                        */
/*                u1Status  : Indicates whether the prefix information is    */
/*                            created or modified or deleted                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcProcessRtPrefix (tV3OsCandteNode * pSpfNode, tV3OsPrefixInfo * pPrefInfo,
                      tV3OsArea * pArea, tV3OsLsaInfo * pLsaInfo,
                      UINT1 u1Status)
{
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pNewRtEntry = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsPath          *pOldPath = NULL;
    tV3OsPath          *pNewPath = NULL;
    tV3OsArea          *pTransitArea = NULL;
    tV3OsDbNode        *pSumDbHashNode = NULL;
    UINT1               u1HopIndex = 0;
    tV3OsLsaNode       *pLsaNode = NULL;
    UINT1               u1Flag = OSIX_FALSE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcessRtPrefix\n");

    pNewRtEntry = &newRtEntry;
    MEMSET (pNewRtEntry, 0, sizeof (tV3OsRtEntry));
    pNewRtEntry->u1DestType = OSPFV3_DEST_NETWORK;
    pNewRtEntry->u1PrefixLen = pPrefInfo->u1PrefixLength;
    TMO_SLL_Init (&(pNewRtEntry->pathLst));
    TMO_SLL_Init (&(pNewRtEntry->lsaLst));
    OSPFV3_IP6_ADDR_COPY (pNewRtEntry->destRtPrefix, pPrefInfo->addrPrefix);

    pOldRtEntry = V3RtcFindRtEntryInCxt
        (pArea->pV3OspfCxt,
         (VOID *) &(pNewRtEntry->destRtPrefix),
         pNewRtEntry->u1PrefixLen, OSPFV3_DEST_NETWORK);

    if (pOldRtEntry != NULL)
    {
        pOldPath = OSPFV3_GET_PATH (pOldRtEntry);
    }

    if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
    {
        if (pPrefInfo->u2Metric == OSPFV3_LS_INFINITY_16BIT)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Metric of the prefix is INFINITY. So "
                        "ignored during route calculation\n");
        }
        else if (OSPFV3_BIT (pPrefInfo->u1PrefixOpt, OSPFV3_NU_BIT_MASK))
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "NU bit is set. So "
                        "ignored during route calculation\n");
        }
        else
        {
            if (pOldPath != NULL)
            {
                /* Find whether the LSA is in the list already or not */
                TMO_SLL_Scan (&(pOldRtEntry->lsaLst), pLsaNode, tV3OsLsaNode *)
                {
                    if (pLsaNode->pLsaInfo == pLsaInfo)
                    {
                        u1Flag = OSIX_TRUE;
                        break;
                    }
                }

                /* If the node is not present, then add the LSA to the 
                   list */
                if (u1Flag == OSIX_FALSE)
                {
                    /* Add the LSA to the LSAs list present in the route */
                    OSPFV3_LSA_NODE_ALLOC (&(pLsaNode));
                    if (NULL == pLsaNode)
                    {
                        OSPFV3_TRC (OS_RESOURCE_TRC,
                                    pArea->pV3OspfCxt->u4ContextId,
                                    "LSA node information Creation fails\n");
                        return;
                    }

                    pLsaNode->pLsaInfo = pLsaInfo;
                    TMO_SLL_Add (&(pOldRtEntry->lsaLst),
                                 &(pLsaNode->nextLsaInfo));
                }

                if (pOldRtEntry->u1RtType == OSPFV3_DIRECT)
                {
                    OSPFV3_TRC (OSPFV3_RT_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "Directly reachable route is present\n");
                    return;
                }

                pNewPath = V3RtcCreatePath (&(pArea->areaId),
                                            OSPFV3_INTRA_AREA,
                                            (pSpfNode->u4Cost +
                                             pPrefInfo->u2Metric), 0);

                if (pNewPath == NULL)
                {
                    return;
                }

                if (pSpfNode->u1VirtReachable != OSIX_TRUE)
                {
                    /* Copy the next hop information from the vertex node */
                    for (u1HopIndex = 0;
                         ((u1HopIndex < pSpfNode->u1HopCount) &&
                          (u1HopIndex < OSPFV3_MAX_NEXT_HOPS)); u1HopIndex++)
                    {
                        /* Next hop is not found in Old route entry */
                        /* Add the next hop information derived from ABR route entry */
                        OSPFV3_IP6_ADDR_COPY (pNewPath->aNextHops[u1HopIndex].
                                              nbrIpv6Addr,
                                              pSpfNode->aNextHops[u1HopIndex].
                                              nbrIpv6Addr);
                        OSPFV3_RTR_ID_COPY (&pNewPath->aNextHops[u1HopIndex].
                                            advRtrId,
                                            &pSpfNode->vertexId.vertexRtrId);
                        pNewPath->aNextHops[u1HopIndex].pInterface =
                            pSpfNode->aNextHops[u1HopIndex].pInterface;
                    }
                    pNewPath->u1HopCount = pSpfNode->u1HopCount;

                    V3RtcCalculateIntraPrefRoute (pOldRtEntry,
                                                  pNewPath, pLsaInfo,
                                                  pArea, pPrefInfo);

                    V3RtcAddPath (pNewRtEntry, pNewPath);
                    KW_FALSEPOSITIVE_FIX (pNewPath);
                }
                else
                {
                    V3RtcAddPath (pNewRtEntry, pNewPath);
                    KW_FALSEPOSITIVE_FIX (pNewPath);
                    /* If some transit areas are present, then consider the 
                     * summary LSAs of the transit area */
                    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pTransitArea,
                                  tV3OsArea *)
                    {
                        if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pTransitArea))
                        {
                            /* Search in the transit area to find the LSA */
                            if ((pSumDbHashNode =
                                 V3RtcSearchLsaHashNode
                                 ((VOID *) &(pNewRtEntry->destRtPrefix),
                                  pNewRtEntry->u1PrefixLen,
                                  OSPFV3_INTER_AREA_PREFIX_LSA,
                                  pTransitArea)) == NULL)
                            {
                                continue;
                            }
                            V3RtcProcessTransitAreaDbNode (pSumDbHashNode,
                                                           pNewRtEntry);
                        }
                    }
                    pNewPath = OSPFV3_GET_PATH (pNewRtEntry);
                    if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
                    {
                        TMO_SLL_Delete (&(pNewRtEntry->pathLst),
                                        &(pNewPath->nextPath));
                        OSPFV3_PATH_FREE (pNewPath);
                    }
                }
            }
            else
            {
                /* If Old path is not present, then create the new path and 
                   update the information */
                pNewPath = V3RtcCreatePath (&(pArea->areaId),
                                            OSPFV3_INTRA_AREA,
                                            (pSpfNode->u4Cost +
                                             pPrefInfo->u2Metric), 0);

                if (pNewPath == NULL)
                {
                    return;
                }

                /* Add the LSA to the LSAs list present in the route */
                OSPFV3_LSA_NODE_ALLOC (&(pLsaNode));
                if (NULL == pLsaNode)
                {
                    OSPFV3_PATH_FREE (pNewPath);
                    OSPFV3_TRC (OS_RESOURCE_TRC,
                                pArea->pV3OspfCxt->u4ContextId,
                                "LSA node information Creation fails\n");
                    return;
                }

                pLsaNode->pLsaInfo = pLsaInfo;

                TMO_SLL_Add (&(newRtEntry.lsaLst), &(pLsaNode->nextLsaInfo));

                V3RtcAddPath (pNewRtEntry, pNewPath);

                if (pSpfNode->u1VirtReachable != OSIX_TRUE)
                {
                    /* Copy the next hop information from the vertex node */
                    for (u1HopIndex = 0;
                         ((u1HopIndex < pSpfNode->u1HopCount) &&
                          (u1HopIndex < OSPFV3_MAX_NEXT_HOPS)); u1HopIndex++)
                    {
                        /* Next hop is not found in Old route entry */
                        /* Add the next hop information derived from ABR route entry */
                        OSPFV3_IP6_ADDR_COPY (pNewPath->aNextHops[u1HopIndex].
                                              nbrIpv6Addr,
                                              pSpfNode->aNextHops[u1HopIndex].
                                              nbrIpv6Addr);
                        OSPFV3_RTR_ID_COPY (&pNewPath->aNextHops[u1HopIndex].
                                            advRtrId,
                                            &pSpfNode->vertexId.vertexRtrId);
                        pNewPath->aNextHops[u1HopIndex].pInterface =
                            pSpfNode->aNextHops[u1HopIndex].pInterface;
                    }
                    pNewPath->u1HopCount = pSpfNode->u1HopCount;
                }
                else
                {
                    /* If some transit areas are present, then consider the 
                     * summary LSAs of the transit area */
                    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pTransitArea,
                                  tV3OsArea *)
                    {
                        if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pTransitArea))
                        {
                            /* Search in the transit area to find the LSA */
                            if ((pSumDbHashNode =
                                 V3RtcSearchLsaHashNode ((VOID *)
                                                         &(pNewRtEntry->
                                                           destRtPrefix),
                                                         pNewRtEntry->
                                                         u1PrefixLen,
                                                         OSPFV3_INTER_AREA_PREFIX_LSA,
                                                         pTransitArea)) == NULL)
                            {
                                continue;
                            }
                            V3RtcProcessTransitAreaDbNode (pSumDbHashNode,
                                                           pNewRtEntry);
                        }
                    }
                    pNewPath = OSPFV3_GET_PATH (pNewRtEntry);
                    if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
                    {
                        TMO_SLL_Delete (&(pNewRtEntry->pathLst),
                                        &(pNewPath->nextPath));
                        OSPFV3_PATH_FREE (pNewPath);
                    }
                }
            }
        }
    }
    else
    {
        if (pOldRtEntry != NULL)
        {
            /* Find whether the LSA is in the list already or not */
            TMO_SLL_Scan (&(pOldRtEntry->lsaLst), pLsaNode, tV3OsLsaNode *)
            {
                if (pLsaNode->pLsaInfo == pLsaInfo)
                {
                    u1Flag = OSIX_TRUE;
                    break;
                }
            }

            if (u1Flag == OSIX_TRUE)
            {
                TMO_SLL_Delete (&(pOldRtEntry->lsaLst),
                                &(pLsaNode->nextLsaInfo));
                OSPFV3_LSA_NODE_FREE (pLsaNode);
            }

            if ((pOldPath != NULL) &&
                (pOldPath->u1PathType == OSPFV3_INTRA_AREA) &&
                (V3UtilAreaIdComp (pOldPath->areaId,
                                   pArea->areaId) != OSPFV3_EQUAL))
            {
                return;
            }

            if (pOldRtEntry->u1RtType == OSPFV3_DIRECT)
            {
                OSPFV3_TRC (OSPFV3_RT_TRC,
                            pArea->pV3OspfCxt->u4ContextId,
                            "Directly reachable route is present\n");
                return;
            }

            if (TMO_SLL_Count (&(pOldRtEntry->lsaLst)) != 0)
            {
                pNewPath =
                    V3RtcCreatePath (&(pArea->areaId), OSPFV3_INTRA_AREA,
                                     OSPFV3_LS_INFINITY_16BIT, 0);

                if (pNewPath == NULL)
                {
                    return;
                }

                V3RtcCalculateIntraPrefRoute (pOldRtEntry,
                                              pNewPath, pLsaInfo,
                                              pArea, pPrefInfo);

                if (pNewPath->u4Cost != OSPFV3_LS_INFINITY_16BIT)
                {
                    V3RtcAddPath (pNewRtEntry, pNewPath);
                }
                else
                {
                    OSPFV3_PATH_FREE (pNewPath);
                    pNewPath = NULL;
                }
            }
        }

        if (pNewPath == NULL)
        {
            /* To see any Inter area route is present for the prefix */
            V3RtcCalculateRouteInCxt (pArea->pV3OspfCxt, pNewRtEntry);
            pNewPath = OSPFV3_GET_PATH (pNewRtEntry);
            if ((pNewPath != NULL) && (pNewPath->u1HopCount == 0))
            {
                TMO_SLL_Delete (&(pNewRtEntry->pathLst), &(pNewPath->nextPath));
                OSPFV3_PATH_FREE (pNewPath);
            }
        }
    }

    V3RtcProcessRtEntryChangesInCxt (pArea->pV3OspfCxt, pNewRtEntry,
                                     pOldRtEntry);
    KW_FALSEPOSITIVE_FIX (pNewPath);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcessRtPrefix\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateAbrRoute                                     */
/*                                                                           */
/* Description  : This function calculates ABR route entry and process the   */
/*                route entry changes of the calculated ABR route entry      */
/*                                                                           */
/* Input        : pSpfNode  : Pointer to the SPF node whose Intra-Area       */
/*                            prefix is being processed                      */
/*                pPrefInfo : Pointer to Intra-Area prefix LSA.              */
/*                u1Status  : Indicates whether the prefix information is    */
/*                            created or modified or deleted                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateAbrRoute (tV3OsCandteNode * pSpfNode, tV3OsArea * pArea,
                        UINT1 u1Status)
{
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsRtEntry       *pNewRtEntry = NULL;
    tV3OsPath          *pPath = NULL;
    tV3OsPath          *pNewAbrPath = NULL;
    UINT1               u1HopIndex = 0;
    UINT1               u1Flag = OSIX_FALSE;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateAbrRoute\n");

    pOldRtEntry = V3RtcFindRtEntryInCxt
        (pArea->pV3OspfCxt,
         (VOID *) &(pSpfNode->vertexId.vertexRtrId),
         0, OSPFV3_DEST_AREA_BORDER);

    if (pOldRtEntry == NULL)
    {
        pNewRtEntry = V3RtcCreateRtEntry ();
        if (pNewRtEntry == NULL)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Failed to allocate memory for " "ABR route entry\n");
            return;
        }
        pNewRtEntry->u1DestType = OSPFV3_DEST_AREA_BORDER;
        OSPFV3_RTR_ID_COPY (&pNewRtEntry->destRtRtrId,
                            &pSpfNode->vertexId.vertexRtrId);
        u1Flag = OSIX_TRUE;
    }
    else
    {
        pNewRtEntry = pOldRtEntry;
    }

    if (u1Status == OSPFV3_CREATED)
    {
        pNewAbrPath = V3RtcCreatePath (&(pArea->areaId), OSPFV3_INTRA_AREA,
                                       pSpfNode->u4Cost, 0);

        if (pNewAbrPath == NULL)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Failed to create new path\n");
            if (u1Flag == OSIX_TRUE)
            {
                V3RtcFreeRtEntry (pNewRtEntry);
            }
            return;
        }

        if (u1Flag == OSIX_TRUE)
        {
            V3RtcAddRtEntryInCxt (pArea->pV3OspfCxt, pNewRtEntry);
        }
        pNewRtEntry->u1Flag = OSPFV3_FOUND;

        /* Copy the next hop information from the vertex node */
        for (u1HopIndex = 0; ((u1HopIndex < pSpfNode->u1HopCount) &&
                              (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1HopIndex++)
        {
            /* Next hop is not found in Old route entry */
            /* Add the next hop information derived from ABR route entry */
            OSPFV3_IP6_ADDR_COPY (pNewAbrPath->aNextHops[u1HopIndex].
                                  nbrIpv6Addr,
                                  pSpfNode->aNextHops[u1HopIndex].nbrIpv6Addr);
            OSPFV3_RTR_ID_COPY (&pNewAbrPath->aNextHops[u1HopIndex].advRtrId,
                                &pSpfNode->vertexId.vertexRtrId);
            pNewAbrPath->aNextHops[u1HopIndex].pInterface =
                pSpfNode->aNextHops[u1HopIndex].pInterface;
        }
        pNewAbrPath->u1HopCount = pSpfNode->u1HopCount;

        V3RtcAddPath (pNewRtEntry, pNewAbrPath);

        if (pArea->pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
        {
            V3RtcProcessVirtualLinkInCxt (pArea->pV3OspfCxt, pNewRtEntry,
                                          pNewAbrPath, OSPFV3_IFE_UP);
        }

        pNewAbrPath->pLsaInfo = V3RtcGetOptionsRtrLsa (pSpfNode);
        pArea->u4AreaBdrRtrCount++;
    }
    else if (u1Status == OSPFV3_UPDATED)
    {
        TMO_SLL_Scan (&(pNewRtEntry->pathLst), pPath, tV3OsPath *)
        {
            if (V3UtilAreaIdComp (pArea->areaId, pPath->areaId) == OSPFV3_EQUAL)
            {
                pNewAbrPath = pPath;
                break;
            }
        }

        if (pNewAbrPath == NULL)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "No path exists\n");
            KW_FALSEPOSITIVE_FIX (pNewRtEntry);
            return;
        }

        pNewRtEntry->u1Flag = OSPFV3_FOUND;

        pNewAbrPath->u4Cost = pSpfNode->u4Cost;

        /* Copy the next hop information from the vertex node */
        for (u1HopIndex = 0; ((u1HopIndex < pSpfNode->u1HopCount) &&
                              (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1HopIndex++)
        {
            /* Next hop is not found in Old route entry */
            /* Add the next hop information derived from ABR route entry */
            OSPFV3_IP6_ADDR_COPY (pNewAbrPath->aNextHops[u1HopIndex].
                                  nbrIpv6Addr,
                                  pSpfNode->aNextHops[u1HopIndex].nbrIpv6Addr);
            OSPFV3_RTR_ID_COPY (&pNewAbrPath->aNextHops[u1HopIndex].advRtrId,
                                &pSpfNode->vertexId.vertexRtrId);
            pNewAbrPath->aNextHops[u1HopIndex].pInterface =
                pSpfNode->aNextHops[u1HopIndex].pInterface;
        }
        pNewAbrPath->u1HopCount = pSpfNode->u1HopCount;
        pNewAbrPath->pLsaInfo = V3RtcGetOptionsRtrLsa (pSpfNode);

        if (pArea->pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
        {
            V3RtcProcessVirtualLinkInCxt
                (pArea->pV3OspfCxt, pNewRtEntry, pNewAbrPath,
                 OSPFV3_VIF_COST_CHANGED);
        }
    }
    else if (u1Status == OSPFV3_DELETED)
    {
        TMO_SLL_Scan (&(pNewRtEntry->pathLst), pPath, tV3OsPath *)
        {
            if (V3UtilAreaIdComp (pArea->areaId, pPath->areaId) == OSPFV3_EQUAL)
            {
                pNewAbrPath = pPath;
                break;
            }
        }

        if (pNewAbrPath != NULL)
        {
            if (pArea->pV3OspfCxt->u1ABRType == OSPFV3_STANDARD_ABR)
            {
                V3RtcProcessVirtualLinkInCxt (pArea->pV3OspfCxt, pNewRtEntry,
                                              pNewAbrPath, OSPFV3_IFE_DOWN);
            }

            V3LsuChkAndGenIndicationLsa (&pNewRtEntry->destRtRtrId, pArea);
            TMO_SLL_Delete (&(pNewRtEntry->pathLst), &pPath->nextPath);
            OSPFV3_PATH_FREE (pNewAbrPath);

            /* If no paths are present, then delete the ABR route entry */
            if (TMO_SLL_Count (&(pNewRtEntry->pathLst)) == 0)
            {
                V3RtcDeleteRtEntryInCxt (pArea->pV3OspfCxt, pNewRtEntry);
                OSPFV3_RT_ENTRY_FREE (pNewRtEntry);
            }
            pArea->u4AreaBdrRtrCount--;
        }
    }
    KW_FALSEPOSITIVE_FIX (pNewAbrPath);
    KW_FALSEPOSITIVE_FIX1 (pNewRtEntry);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateAbrRoute\n");

}

/*****************************************************************************/
/* Function     : V3RtcCalculateAsbrRoute                                    */
/*                                                                           */
/* Description  : This function calculates ASBR route entry and process the  */
/*                route entry changes of the calculated ASBR route entry     */
/*                                                                           */
/* Input        : pSpfNode  : Pointer to the SPF node whose ASBR route is    */
/*                            calculated.                                    */
/*                pArea     : Pointer to area.                               */
/*                u1Status  : Indicates whether the ASBR   information is    */
/*                            created or modified or deleted                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateAsbrRoute (tV3OsCandteNode * pSpfNode, tV3OsArea * pArea,
                         UINT1 u1Status)
{
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pNewRtEntry = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsPath          *pNewPath = NULL;
    tV3OsArea          *pTempArea = NULL;
    UINT1               u1HopIndex = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateAsbrRoute\n");

    pNewRtEntry = &newRtEntry;
    MEMSET (pNewRtEntry, 0, sizeof (tV3OsRtEntry));
    pNewRtEntry->u1DestType = OSPFV3_DEST_AS_BOUNDARY;
    pNewRtEntry->u1PrefixLen = 0;
    TMO_SLL_Init (&(pNewRtEntry->pathLst));

    OSPFV3_RTR_ID_COPY (&pNewRtEntry->destRtRtrId,
                        &pSpfNode->vertexId.vertexRtrId);

    pOldRtEntry = V3RtcFindRtEntryInCxt
        (pArea->pV3OspfCxt, (VOID *) &(pNewRtEntry->destRtPrefix),
         pNewRtEntry->u1PrefixLen, OSPFV3_DEST_AS_BOUNDARY);

    if (u1Status == OSPFV3_CREATED)
    {
        pArea->u4AsBdrRtrCount++;
    }

    if (u1Status == OSPFV3_DELETED)
    {
        pArea->u4AsBdrRtrCount--;
    }

    if ((u1Status == OSPFV3_CREATED) || (u1Status == OSPFV3_UPDATED))
    {
        pNewPath = V3RtcCreatePath (&(pArea->areaId), OSPFV3_INTRA_AREA,
                                    pSpfNode->u4Cost, 0);
        if (pNewPath == NULL)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Failed to create new path\n");
            return;
        }

        /* Copy the next hop information from the vertex node */
        for (u1HopIndex = 0; ((u1HopIndex < pSpfNode->u1HopCount) &&
                              (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1HopIndex++)
        {
            /* Next hop is not found in Old route entry */
            /* Add the next hop information derived from ABR route entry */
            OSPFV3_IP6_ADDR_COPY (pNewPath->aNextHops[u1HopIndex].
                                  nbrIpv6Addr,
                                  pSpfNode->aNextHops[u1HopIndex].nbrIpv6Addr);
            OSPFV3_RTR_ID_COPY (&pNewPath->aNextHops[u1HopIndex].advRtrId,
                                &pSpfNode->vertexId.vertexRtrId);
            pNewPath->aNextHops[u1HopIndex].pInterface =
                pSpfNode->aNextHops[u1HopIndex].pInterface;
        }
        pNewPath->u1HopCount = pSpfNode->u1HopCount;

        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            if (pOldRtEntry != NULL)
            {
                V3RtcDeletePath (pOldRtEntry, &pArea->areaId);
            }
            else
            {
                /* Create the route entry and add the path to the route */
                pOldRtEntry = V3RtcCreateRtEntry ();
                if (pOldRtEntry == NULL)
                {
                    OSPFV3_PATH_FREE (pNewPath);
                    return;
                }

                MEMCPY (pOldRtEntry, pNewRtEntry, sizeof (tV3OsRtEntry));
                TMO_SLL_Init (&(pOldRtEntry->pathLst));

                V3RtcAddRtEntryInCxt (pArea->pV3OspfCxt, pOldRtEntry);
            }
            pOldRtEntry->u1Flag = OSPFV3_FOUND;
            V3RtcAddPath (pOldRtEntry, pNewPath);
        }
        else
        {
            TMO_SLL_Insert (&(pNewRtEntry->pathLst), NULL,
                            &(pNewPath->nextPath));
        }
    }
    else
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            if (pOldRtEntry != NULL)
            {
                V3RtcDeletePath (pOldRtEntry, &pArea->areaId);
                /* If no paths are there in the route entry, then delete
                   the route entry */
                if (TMO_SLL_Count (&(pOldRtEntry->pathLst)) == 0)
                {
                    V3RtcDeleteRtEntryInCxt (pArea->pV3OspfCxt, pOldRtEntry);
                    V3RtcFreeRtEntry (pOldRtEntry);
                }
            }
        }
        else
        {
            V3RtcCalculateRouteInCxt (pArea->pV3OspfCxt, pNewRtEntry);
            if (pOldRtEntry != NULL)
            {
                /* If destination is AS boundary router flush out type 4 lsa */
                if (OSPFV3_IS_DEST_ASBR (pOldRtEntry))
                {
                    TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pTempArea,
                                  tV3OsArea *)
                    {

                        /* If the associated area is the same area then skip route */
                        if (V3UtilAreaIdComp (pTempArea->areaId, pArea->areaId)
                            == OSPFV3_EQUAL)
                        {
                            continue;
                        }

                        V3RtcFlushOutSummaryLsa (pTempArea,
                                                 OSPFV3_INTER_AREA_ROUTER_LSA,
                                                 &(pOldRtEntry->linkStateId));
                    }
                }
                V3RtcDeletePath (pOldRtEntry, &pArea->areaId);
                if (TMO_SLL_Count (&(pOldRtEntry->pathLst)) == 0)
                {
                    V3RtcDeleteRtEntryInCxt (pArea->pV3OspfCxt, pOldRtEntry);
                    V3RtcFreeRtEntry (pOldRtEntry);
                }
            }
        }
        OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                    "EXIT : V3RtcCalculateAsbrRoute\n");

        return;
    }

    if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
    {
        V3RtcProcessASBRRtEntryChangesInCxt (pArea->pV3OspfCxt, pNewRtEntry,
                                             pOldRtEntry);
    }
    KW_FALSEPOSITIVE_FIX (pNewPath);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateAsbrRoute\n");

}

/*****************************************************************************/
/* Function     : V3RtcGetOptionsRtrLsa                                      */
/*                                                                           */
/* Description  : This function extracts router LSA whose options are used.  */
/*                                                                           */
/* Input        : pVertex    : Pointer to Vertex node                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PRIVATE tV3OsLsaInfo *
V3RtcGetOptionsRtrLsa (tV3OsCandteNode * pVertex)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcGetOptionsRtrLsa\n");

    TMO_SLL_Scan (&(pVertex->pDbNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
    {
        pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
        if (!OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge))
        {
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcGetOptionsRtrLsa\n");
            return pLsaInfo;
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function     : V3RtcCalculateIntraPrefRoute                               */
/*                                                                           */
/* Description  : This function calculates the intra area route by           */
/*                all the all the intra area prefix LSAs which contain this  */
/*                prefix.                                                    */
/*                                                                           */
/* Input        : pOldRtEntry : Pointer to the Old route entry               */
/*                pNewPath    : Pointer to the New path structure.           */
/*                pLsaInfo    : Pointer to the LSA which is triggering this  */
/*                              route calculation.                           */
/*                pArea       : Pointer to the area.                         */
/*                pPrefInfo   : Pointer to the prefix information.           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcCalculateIntraPrefRoute (tV3OsRtEntry * pOldRtEntry, tV3OsPath * pNewPath,
                              tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea,
                              tV3OsPrefixInfo * pPrefInfo)
{
    tV3OsLsaNode       *pLsaNode = NULL;
    tV3OsLsaInfo       *pOtherLsaInfo = NULL;
    UINT1              *pCurrPtr = NULL;
    tV3OsPrefixInfo     prefixInfo;
    tV3OsLsaId          refLsId;
    tV3OsCandteNode     key;
    tV3OsCandteNode    *pSpfNode = NULL;
    UINT4               u4Cost = 0;
    UINT2               u2NoPrefix = 0;
    UINT2               u2Counter = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcCalculateIntraPrefRoute\n");

    /* Scan and process all other LSAs of the route entry */
    TMO_SLL_Scan (&(pOldRtEntry->lsaLst), pLsaNode, tV3OsLsaNode *)
    {
        if (pLsaNode->pLsaInfo == pLsaInfo)
        {
            continue;
        }

        if (pLsaNode->pLsaInfo->pArea != pArea)
        {
            continue;
        }

        pOtherLsaInfo = pLsaNode->pLsaInfo;

        /* Get the REF LS ID information */
        V3RtcGetRefLsIdInfo (pOtherLsaInfo->pLsa, &refLsId);

        /* Find the SPF node corresponding to the ref link state information */

        if (refLsId.u2LsaType == OSPFV3_REF_ROUTER_LSA)
        {
            key.u1VertType = OSPFV3_VERT_ROUTER;
        }
        else
        {
            key.u1VertType = OSPFV3_VERT_NETWORK;
        }

        key.vertexId.u4VertexIfId =
            OSPFV3_BUFFER_DWFROMPDU (refLsId.linkStateId);

        OSPFV3_RTR_ID_COPY (key.vertexId.vertexRtrId, refLsId.advRtrId);

        pSpfNode = RBTreeGet (pArea->pSpf, &key);

        if (pSpfNode != NULL)
        {
            pCurrPtr = pOtherLsaInfo->pLsa + OSPFV3_LS_HEADER_SIZE;
            u2NoPrefix = OSPFV3_LGET2BYTE (pCurrPtr);
            pCurrPtr = pCurrPtr + 10;

            for (u2Counter = 0; u2Counter < u2NoPrefix; u2Counter++)
            {
                pCurrPtr = V3UtilExtractPrefixFromLsa (pCurrPtr, &prefixInfo);

                /* Check for the same prefix */
                if ((pPrefInfo->u1PrefixLength == prefixInfo.u1PrefixLength) &&
                    (V3UtilIp6PrefixComp (&pPrefInfo->addrPrefix,
                                          &prefixInfo.addrPrefix,
                                          pPrefInfo->u1PrefixLength) ==
                     OSPFV3_EQUAL))
                {
                    u4Cost = pSpfNode->u4Cost + prefixInfo.u2Metric;

                    if (u4Cost < pNewPath->u4Cost)
                    {
                        pNewPath->u4Cost = u4Cost;
                        pNewPath->u1HopCount = 0;
                        V3RtcUpdateIntraPathNextHops (pSpfNode, pNewPath);
                    }
                    else if (u4Cost == pNewPath->u4Cost)
                    {
                        V3RtcUpdateIntraPathNextHops (pSpfNode, pNewPath);
                    }
                    break;
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcCalculateIntraPrefRoute\n");

}

/*****************************************************************************/
/* Function     : V3RtcUpdateIntraPathNextHops                               */
/*                                                                           */
/* Description  : This function checks for the duplication and updates the   */
/*                vertex node next hops from parent.                         */
/*                                                                           */
/* Input        : pVertex : Pointer to vertex node whose next hops are to    */
/*                          be updated.                                      */
/*                pParent : Pointer to the parent node.                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcUpdateIntraPathNextHops (tV3OsCandteNode * pSpfNode, tV3OsPath * pPath)
{
    UINT1               u1VertexHopCount = 0;
    UINT1               u1Index1 = 0;
    UINT1               u1Index2 = 0;
    UINT1               u1HopPresent = OSIX_FALSE;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RtcUpdateIntraPathNextHops\n");
    u1VertexHopCount = pPath->u1HopCount;

    if (pPath->u1HopCount >= OSPFV3_MAX_NEXT_HOPS)
    {
        return;
    }

    for (u1Index1 = 0; ((u1Index1 < pSpfNode->u1HopCount) &&
                        (u1Index1 < OSPFV3_MAX_NEXT_HOPS)); u1Index1++)
    {
        for (u1Index2 = 0; ((u1Index2 < u1VertexHopCount) &&
                            (u1Index2 < OSPFV3_MAX_NEXT_HOPS)); u1Index2++)
        {
            if ((pPath->aNextHops[u1Index1].pInterface ==
                 pSpfNode->aNextHops[u1Index2].pInterface) &&
                (V3UtilIp6AddrComp
                 (&pPath->aNextHops[u1Index1].nbrIpv6Addr,
                  &pSpfNode->aNextHops[u1Index2].nbrIpv6Addr) == OSPFV3_EQUAL))
            {
                u1HopPresent = OSIX_TRUE;
                break;
            }
        }

        if (u1HopPresent == OSIX_FALSE)
        {
            pPath->aNextHops[pPath->u1HopCount].pInterface =
                pSpfNode->aNextHops[u1Index1].pInterface;
            OSPFV3_IP6_ADDR_COPY (pPath->
                                  aNextHops[pPath->u1HopCount].
                                  nbrIpv6Addr,
                                  pSpfNode->aNextHops[u1Index1].nbrIpv6Addr);
            pPath->u1HopCount++;
            /* Check if the number of next hops in node reaches max limit */
            if (pPath->u1HopCount == OSPFV3_MAX_NEXT_HOPS)
            {
                return;
            }
        }
        else
        {
            u1HopPresent = OSIX_FALSE;
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RtcUpdateIntraPathNextHops\n");

}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rintra.c                       */
/*----------------------------------------------------------------------*/
