/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rtincr.c,v 1.11 2017/12/26 13:34:29 siva Exp $
 *
 * Description:This file contains procedures for creating and
 *             deleting after getting some incremental updates. 
 *
 *******************************************************************/
#include "o3inc.h"
PRIVATE VOID
    V3RtcProcOtherIntraPrefLsaOfSpfNode PROTO ((tV3OsCandteNode * pSpfNode,
                                                tV3OsLsaInfo * pLsaInfo,
                                                tV3OsArea * pArea));

/*****************************************************************************/
/* Function     : V3RtcIncUpdateLinkLsa                                      */
/*                                                                           */
/* Description  : This function updates next hops of the route entries       */
/*                whose next hop is the changed old hop, with the new next   */
/*                hop                                                        */
/*                This function is called when there is a change in          */
/*                link local next hop filed of the Link LSA                  */
/*                                                                           */
/* Input        : pLsaInfo     : Pointer to new route entry                  */
/*                pOldNextHop  : Pointer to new next hop                     */
/*                pNewNextHop  : Pointer to old next hop                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcIncUpdateLinkLsa (tV3OsLsaInfo * pLsaInfo,
                       tIp6Addr * pOldNextHop, tIp6Addr * pNewNextHop)
{
    tV3OsPath          *pPath = NULL;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsInterface     *pInterface = NULL;
    VOID               *pLeafNode = NULL;
    tInputParams        inParams;
    UINT1               au1Key[OSPFV3_IPV6_ADDR_LEN + 1];
    UINT1               u1HopIndex = 0;

    VOID               *pTempPtr = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcIncUpdateLinkLsa\n");

    pInterface = pLsaInfo->pInterface;

    /* Updating ABR and ASBR route entries */
    TMO_SLL_Scan (&(pLsaInfo->pInterface->pArea->pV3OspfCxt->
                    ospfV3RtTable.routesList), pRtEntry, tV3OsRtEntry *)
    {
        TMO_SLL_Scan (&(pRtEntry->pathLst), pPath, tV3OsPath *)
        {
            if ((pPath->u1PathType == OSPFV3_INTRA_AREA) &&
                (V3UtilAreaIdComp (pPath->areaId,
                                   pInterface->pArea->areaId) != OSPFV3_EQUAL))
            {
                continue;
            }
            for (u1HopIndex = 0; ((u1HopIndex < pPath->u1HopCount) &&
                                  (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
                 u1HopIndex++)
            {
                if ((pInterface == pPath->aNextHops[u1HopIndex].pInterface) &&
                    (V3UtilIp6AddrComp
                     (&(pPath->aNextHops[u1HopIndex].nbrIpv6Addr),
                      pOldNextHop) == OSPFV3_EQUAL))
                {
                    OSPFV3_IP6_ADDR_COPY
                        (pPath->aNextHops[u1HopIndex].nbrIpv6Addr,
                         *pNewNextHop);
                }
            }
        }
    }

    /* Updating the network route entries */
    inParams.pRoot = pLsaInfo->pInterface->pArea->pV3OspfCxt->
        ospfV3RtTable.pOspfV3Root;
    inParams.i1AppId = pLsaInfo->pInterface->pArea->pV3OspfCxt->
        ospfV3RtTable.i1OspfId;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "TrieGetFirstNode Failure\n");
        return;
    }

    do
    {
        pRtEntry = (tV3OsRtEntry *) pTempPtr;
        pPath = OSPFV3_GET_PATH (pRtEntry);

        if (pPath == NULL)
        {
            return;
        }

        if ((pPath->u1PathType == OSPFV3_INTRA_AREA) &&
            (V3UtilAreaIdComp (pPath->areaId,
                               pInterface->pArea->areaId) != OSPFV3_EQUAL))
        {
            pLeafNode = inParams.pLeafNode;
            MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
            au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
            inParams.Key.pKey = (UINT1 *) au1Key;
            continue;
        }

        for (u1HopIndex = 0; ((u1HopIndex < pPath->u1HopCount) &&
                              (u1HopIndex < OSPFV3_MAX_NEXT_HOPS));
             u1HopIndex++)
        {
            if ((pInterface == pPath->aNextHops[u1HopIndex].pInterface) &&
                (V3UtilIp6AddrComp (&(pPath->aNextHops[u1HopIndex].nbrIpv6Addr),
                                    pOldNextHop) == OSPFV3_EQUAL))
            {
                /* Delete the route entry with old next hop from IP 
                 * routing table */
                V3RtmLeakRouteInCxt (pLsaInfo->pInterface->pArea->pV3OspfCxt,
                                     pRtEntry, pPath,
                                     u1HopIndex, NETIPV6_DELETE_ROUTE);

                OSPFV3_IP6_ADDR_COPY
                    (pPath->aNextHops[u1HopIndex].nbrIpv6Addr, *pNewNextHop);

                /* Adding the route entry with new next hop to IP 
                 * routing table */
                V3RtmLeakRouteInCxt (pLsaInfo->pInterface->pArea->pV3OspfCxt,
                                     pRtEntry, pPath, u1HopIndex,
                                     NETIPV6_ADD_ROUTE);
            }
        }

        pLeafNode = inParams.pLeafNode;
        MEMCPY (&(au1Key), &pRtEntry->destRtPrefix, OSPFV3_IPV6_ADDR_LEN);
        au1Key[OSPFV3_IPV6_ADDR_LEN] = pRtEntry->u1PrefixLen;
        inParams.Key.pKey = (UINT1 *) au1Key;
    }
    while (TrieGetNextNode (&inParams, (VOID *) pLeafNode,
                            &pTempPtr, (VOID **)
                            &(inParams.pLeafNode)) != TRIE_FAILURE);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcIncUpdateLinkLsa\n");

}

/***************************************************************************/
/* Function     : V3RtcIncUpdtRtSmmryLinkLsa                               */
/*                                                                         */
/* Description  : Reference Section 16.5 RFC 2328.                         */
/*                Reference Section 3.8.5 RFC 2740.                        */
/*                Incremental updates - Summary LSAs.                      */
/*                                                                         */
/* Input        : pLsaInfo  : Pointer to LSA                               */
/*                                                                         */
/* Output       : None                                                     */
/*                                                                         */
/* Returns      : VOID                                                     */
/***************************************************************************/
PUBLIC VOID
V3RtcIncUpdtRtSmmryLinkLsa (tV3OsLsaInfo * pLsaInfo)
{
    UINT1               u1DestType = OSPFV3_INVALID;
    UINT1               u1PrefixLen = 0;
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsRtEntry       *pNewRtEntry = NULL;
    tV3OsPath          *pOldPath = NULL;
    tV3OsArea          *pTmpArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsInterface     *pVirtInterface = NULL;
    INT4                i4ChangeFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcIncUpdtRtSmmryLinkLsa\n");

    /* If the summary LSA is received in transit area, then */
    if (OSPFV3_IS_AREA_TRANSIT_CAPABLE (pLsaInfo->pArea))
    {
        TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->virtIfLst), pLstNode,
                      tTMO_SLL_NODE *)
        {
            pVirtInterface =
                OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode, pLstNode);

            if (V3UtilAreaIdComp (pVirtInterface->transitAreaId,
                                  pLsaInfo->pArea->areaId) == OSPFV3_EQUAL)
            {
                pLsaInfo->pV3OspfCxt->u1VirtRtCal = OSIX_TRUE;
                if (pLsaInfo->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                {
                    V3RtcSetRtTimerInCxt (pLsaInfo->pV3OspfCxt);
                }
                return;
            }
        }
    }

    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
    pNewRtEntry = &newRtEntry;

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        u1DestType = OSPFV3_DEST_NETWORK;
        u1PrefixLen = *(pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);

        MEMSET (&(pNewRtEntry->destRtPrefix), 0, sizeof (tIp6Addr));
        OSPFV3_IP6_ADDR_PREFIX_COPY (pNewRtEntry->destRtPrefix,
                                     *(pLsaInfo->pLsa +
                                       OSPFV3_INTER_AREA_PREF_OFFSET),
                                     u1PrefixLen);

        pOldRtEntry = V3RtcFindRtEntryInCxt
            (pLsaInfo->pV3OspfCxt,
             (VOID *) &(pNewRtEntry->destRtPrefix),
             u1PrefixLen, OSPFV3_DEST_NETWORK);
        if (pOldRtEntry != NULL)
        {
            pOldPath = OSPFV3_GET_PATH (pOldRtEntry);
            if ((pOldPath != NULL) &&
                (pOldPath->u1PathType == OSPFV3_INTRA_AREA))
            {
                return;
            }
        }
    }

    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA)
    {
        u1DestType = OSPFV3_DEST_AS_BOUNDARY;
        u1PrefixLen = 0;
        OSPFV3_RTR_ID_COPY (&(pNewRtEntry->destRtRtrId),
                            pLsaInfo->pLsa +
                            OSPFV3_INTER_AREA_RTR_LSA_RTR_ID_OFFSET);
        pOldRtEntry =
            V3RtcFindRtEntryInCxt
            (pLsaInfo->pV3OspfCxt,
             (VOID *) &(pNewRtEntry->destRtRtrId), 0, OSPFV3_DEST_AS_BOUNDARY);

        if (pOldRtEntry != NULL)
        {
            TMO_SLL_Scan (&(pOldRtEntry->pathLst), pOldPath, tV3OsPath *)
            {
                pTmpArea = V3GetFindAreaInCxt (pLsaInfo->pV3OspfCxt,
                                               &(pOldPath->areaId));

                if ((pTmpArea != NULL)
                    && (pTmpArea->u4AreaType != OSPFV3_NSSA_LSA)
                    && (pOldPath->u1PathType == OSPFV3_INTRA_AREA))
                {
                    /* Already intra area route is present through non NSSA 
                     * area.
                     * Since Intra areas routes are preferred over Inter area
                     * no need to calculate Inter Area route for this prefix */
                    return;
                }
            }
        }
    }

    pNewRtEntry->u1DestType = u1DestType;
    pNewRtEntry->u1PrefixLen = u1PrefixLen;
    TMO_SLL_Init (&(pNewRtEntry->pathLst));

    if (((V3UtilRtrIdComp (OSPFV3_BACKBONE_AREAID,
                           pLsaInfo->pArea->areaId) == OSPFV3_EQUAL) ||
         (pLsaInfo->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE))
        || ((pLsaInfo->pV3OspfCxt->bAreaBdrRtr != OSIX_TRUE)
            && (pLsaInfo->pV3OspfCxt->pBackbone->u4ActIntCount == 0)
            && (pLsaInfo->pV3OspfCxt->u1ABRType != OSPFV3_STANDARD_ABR)))
    {
        V3RtcCalculateRouteInCxt (pLsaInfo->pV3OspfCxt, pNewRtEntry);

        if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
        {
            i4ChangeFlag =
                V3RtcProcessRtEntryChangesInCxt
                (pLsaInfo->pV3OspfCxt, pNewRtEntry, pOldRtEntry);
        }
        else
        {
            i4ChangeFlag =
                V3RtcProcessASBRRtEntryChangesInCxt
                (pLsaInfo->pV3OspfCxt, pNewRtEntry, pOldRtEntry);
        }

        if (i4ChangeFlag == OSIX_FALSE)
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "There is no change in routing table, "
                        "so no need to proceed further\n");
            return;
        }
    }

    /* Calculate All Ext routes */
    if (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA)
    {
        V3RtcChkAndCalAllExtRoutesInCxt (pLsaInfo->pV3OspfCxt,
                                         OSPFV3_DEST_NETWORK, NULL);
    }
    else
    {
        V3RtcChkAndCalAllExtRoutesInCxt
            (pLsaInfo->pV3OspfCxt, OSPFV3_DEST_AS_BOUNDARY,
             &pLsaInfo->lsaId.advRtrId);
    }

    V3RtcCalculateASExtRoutesInCxt (pLsaInfo->pV3OspfCxt);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcIncUpdtRtSmmryLinkLsa \n");
}

/****************************************************************************/
/* Function       : V3RtcIncUpdateExtRoute                                  */
/*                                                                          */
/* Description    : This function updates the route to the destination      */
/*                  described by the AS external and NSSA LSAs.             */
/*                                                                          */
/* Input          : pLsaInfo - AS external or NSSA LSA to be considered     */
/*                                                                          */
/* Output         : None                                                    */
/*                                                                          */
/* Returns        : VOID                                                    */
/****************************************************************************/
PUBLIC VOID
V3RtcIncUpdateExtRoute (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsArea          *pArea = NULL;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsRtEntry        newRtEntry;
    tV3OsDbNode        *pDbHashNode = NULL;
    UINT1               u1Changed = OSIX_FALSE;
    tV3OsExtLsaLink     extLsaLink;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcIncUpdateExtRoute\n");

    if (V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                            &extLsaLink) != OSIX_SUCCESS)
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "This LSA might not considered in route " "calculation");
        return;
    }

    /* Find the Old route entry */
    pOldRtEntry = V3RtcFindRtEntryInCxt
        (pLsaInfo->pV3OspfCxt,
         (VOID *) &extLsaLink.extRtPrefix.addrPrefix,
         extLsaLink.extRtPrefix.u1PrefixLength, OSPFV3_DEST_NETWORK);
    if (pOldRtEntry != NULL)
    {
        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);
        if ((pOldRtPath != NULL) &&
            ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) ||
             (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)))
        {
            OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                        "Old route entry is Intra/Inter " "Area route\n");
            return;
        }
    }

    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
    /* Initializing the new routing information */
    newRtEntry.u1PrefixLen = extLsaLink.extRtPrefix.u1PrefixLength;

    MEMSET (&(newRtEntry.destRtPrefix), 0, sizeof (tIp6Addr));
    OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                 extLsaLink.extRtPrefix.addrPrefix,
                                 newRtEntry.u1PrefixLen);

    TMO_SLL_Init (&(newRtEntry.pathLst));

    newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
    newRtEntry.u1Flag = 0;

    /* Process all the AS external LSAs of the same prefix */
    /* Backbone area is passed to access the context information */
    if ((pDbHashNode =
         V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.destRtPrefix,
                                 newRtEntry.u1PrefixLen,
                                 OSPFV3_AS_EXT_LSA,
                                 pLsaInfo->pV3OspfCxt->pBackbone)) != NULL)
    {
        V3RtcProcessExtLsaDbNode (pDbHashNode, &newRtEntry);
    }

    /* Process all the NSSA LSAs of the same prefix */
    TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        /* If the area is not NSSA, consider the next area */
        if (pArea->u4AreaType != OSPFV3_NSSA_AREA)
        {
            continue;
        }

        if ((pDbHashNode =
             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.destRtPrefix,
                                     newRtEntry.u1PrefixLen,
                                     OSPFV3_NSSA_LSA, pArea)) != NULL)
        {
            V3RtcProcessNssaLsaDbNode (pDbHashNode, &newRtEntry);
        }
    }

    u1Changed = (UINT1) (V3RtcProcessRtEntryChangesInCxt
                         (pLsaInfo->pV3OspfCxt, &newRtEntry, pOldRtEntry));

    UNUSED_PARAM (u1Changed);

    if ((pLsaInfo->lsaId.u2LsaType == OSPFV3_NSSA_LSA) &&
        (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (pLsaInfo->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
    {
        V3RagNssaType7LsaTranslation (pLsaInfo->pArea, pLsaInfo);
        V3RagNssaAdvertiseType7Rngs (pLsaInfo->pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcIncUpdateExtRoute\n");

}

/*****************************************************************************/
/* Function     : V3RtcChkAndCalAllExtRoutesInCxt                            */
/*                                                                           */
/* Description  : This function checks and calculate the all external routes */
/*                                                                           */
/* Input        : pV3OspfCxt  : DContext pointer                             */
/*                u1DestType  : Destination Type                             */
/*                pAsbrRtrId  : ASBR router ID.                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
     
     
     
     
    V3RtcChkAndCalAllExtRoutesInCxt
    (tV3OspfCxt * pV3OspfCxt, UINT1 u1DestType, tV3OsRouterId * pAsbrRtrId)
{
    UINT4               u4HashIndex = 0;
    tV3OsLsaInfo       *pExtLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsDbNode        *pDbNssaNode = NULL;
    tV3OsRtEntry        newRtEntry;
    tV3OsRtEntry       *pOldRtEntry = NULL;
    tV3OsPath          *pOldRtPath = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsArea          *pNextArea = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcChkAndCalAllExtRoutesInCxt\n");

    if (u1DestType == OSPFV3_DEST_NETWORK)
    {
        V3RtcCalculateASExtRoutesInCxt (pV3OspfCxt);
    }
    else
    {
        TMO_HASH_Scan_Table (pV3OspfCxt->pExtLsaHashTable, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (pV3OspfCxt->pExtLsaHashTable,
                                  u4HashIndex, pDbHashNode, tV3OsDbNode *)
            {
                pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                pExtLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                   pLstNode);

                /* If the external route is generated by this ASBR, process
                 * that external LSAs only */
                if (V3UtilRtrIdComp (pExtLsaInfo->lsaId.advRtrId,
                                     *pAsbrRtrId) != OSPFV3_EQUAL)
                {
                    continue;
                }

                MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                /* Initializing the new routing information */
                MEMSET (&(newRtEntry.destRtPrefix), 0, OSPFV3_IPV6_ADDR_LEN);

                newRtEntry.u1PrefixLen =
                    *(pExtLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);

                OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                             *(pExtLsaInfo->pLsa +
                                               OSPFV3_INTER_AREA_PREF_OFFSET),
                                             newRtEntry.u1PrefixLen);

                TMO_SLL_Init (&(newRtEntry.pathLst));
                newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
                newRtEntry.u1Flag = 0;

                pOldRtEntry =
                    V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                           (VOID *) &(newRtEntry.destRtPrefix),
                                           newRtEntry.u1PrefixLen,
                                           OSPFV3_DEST_NETWORK);

                if (pOldRtEntry != NULL)
                {
                    pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                    if ((pOldRtPath != NULL) &&
                        ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) ||
                         (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)))
                    {
                        /* Already intra area or inter area route is present. */
                        continue;
                    }
                }

                V3RtcProcessExtLsaDbNode (pDbHashNode, &newRtEntry);
                /* Process the route entry changes */
                V3RtcProcessRtEntryChangesInCxt (pV3OspfCxt,
                                                 &newRtEntry, pOldRtEntry);
            }
        }

        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            TMO_HASH_Scan_Table (pArea->pNssaLsaHashTable, u4HashIndex)
            {
                TMO_HASH_Scan_Bucket (pArea->pNssaLsaHashTable,
                                      u4HashIndex, pDbHashNode, tV3OsDbNode *)
                {
                    if (pDbHashNode->u1Flag == OSPFV3_USED)
                    {
                        continue;
                    }

                    pLstNode = TMO_SLL_First (&(pDbHashNode->lsaLst));

                    pLsaInfo = OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo,
                                                    pLstNode);
                    /* If the external route is generated by this ASBR, process
                     * that external LSAs only */
                    if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId,
                                         *pAsbrRtrId) != OSPFV3_EQUAL)
                    {
                        continue;
                    }

                    MEMSET (&newRtEntry, 0, sizeof (tV3OsRtEntry));
                    /* Initializing the new routing information */
                    MEMSET (&(newRtEntry.destRtPrefix), 0,
                            OSPFV3_IPV6_ADDR_LEN);

                    newRtEntry.u1PrefixLen =
                        *(pLsaInfo->pLsa + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);

                    OSPFV3_IP6_ADDR_PREFIX_COPY (newRtEntry.destRtPrefix,
                                                 *(pLsaInfo->pLsa +
                                                   OSPFV3_INTER_AREA_PREF_OFFSET),
                                                 newRtEntry.u1PrefixLen);
                    TMO_SLL_Init (&(newRtEntry.pathLst));
                    newRtEntry.u1DestType = OSPFV3_DEST_NETWORK;
                    newRtEntry.u1Flag = 0;

                    pOldRtEntry =
                        V3RtcFindRtEntryInCxt (pV3OspfCxt,
                                               (VOID *)
                                               &(newRtEntry.destRtPrefix),
                                               newRtEntry.u1PrefixLen,
                                               OSPFV3_DEST_NETWORK);

                    if (pOldRtEntry != NULL)
                    {
                        pOldRtPath = OSPFV3_GET_PATH (pOldRtEntry);

                        if ((pOldRtPath != NULL) &&
                            ((pOldRtPath->u1PathType == OSPFV3_INTRA_AREA) ||
                             (pOldRtPath->u1PathType == OSPFV3_INTER_AREA)))
                        {
                            /* Already intra area or inter area route is 
                             * present. */
                            continue;
                        }
                    }

                    V3RtcProcessNssaLsaDbNode (pDbHashNode, &newRtEntry);

                    /* Process NSSA LSAs of other areas */
                    pNextArea = pArea;

                    while ((pNextArea = (tV3OsArea *)
                            TMO_SLL_Next (&(pV3OspfCxt->areasLst),
                                          &pNextArea->nextArea)) != NULL)
                    {
                        if (pNextArea->u4AreaType != OSPFV3_NSSA_AREA)
                        {
                            continue;
                        }
                        if ((pDbNssaNode =
                             V3RtcSearchLsaHashNode ((VOID *) &newRtEntry.
                                                     destRtPrefix,
                                                     newRtEntry.u1PrefixLen,
                                                     OSPFV3_NSSA_LSA,
                                                     pNextArea)) != NULL)
                        {
                            V3RtcProcessNssaLsaDbNode (pDbNssaNode,
                                                       &newRtEntry);
                        }
                    }

                    /* Processing the route entry changes */
                    V3RtcProcessRtEntryChangesInCxt
                        (pV3OspfCxt, &newRtEntry, pOldRtEntry);
                    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                        (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
                    {
                        V3RagNssaType7LsaTranslation (pArea, pLsaInfo);
                        V3RagNssaAdvertiseType7Rngs (pArea);
                    }
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcChkAndCalAllExtRoutesInCxt\n");

}

/*****************************************************************************/
/* Function     : V3RtcIncUpdateIntraAreaPrefLsa                             */
/*                                                                           */
/* Description  : This function is to perform the incremental route          */
/*                calculation for intra area prefix LSA.                     */
/*                                                                           */
/* Input        : pOldLsa     : Pointer to Old LSA.                          */
/*                pNewLsa     : Pointer to New LSA.                          */
/*                pArea       : Pointer to area.                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3RtcIncUpdateIntraAreaPrefLsa (UINT1 *pOldLsa, UINT1 *pNewLsa,
                                tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea)
{
    tV3OsCandteNode    *pOldSpfNode = NULL;
    tV3OsCandteNode    *pNewSpfNode = NULL;
    tV3OsLsaId          oldLsId;
    tV3OsLsaId          newLsId;
    tV3OsCandteNode     key;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcIncUpdateIntraAreaPrefLsa\n");

    if (pOldLsa != NULL)
    {
        V3RtcGetRefLsIdInfo (pOldLsa, &oldLsId);

        if (oldLsId.u2LsaType == OSPFV3_REF_ROUTER_LSA)
        {
            key.u1VertType = OSPFV3_VERT_ROUTER;
        }
        else
        {
            key.u1VertType = OSPFV3_VERT_NETWORK;
        }

        key.vertexId.u4VertexIfId =
            OSPFV3_BUFFER_DWFROMPDU (oldLsId.linkStateId);

        OSPFV3_RTR_ID_COPY (key.vertexId.vertexRtrId, oldLsId.advRtrId);

        pOldSpfNode = RBTreeGet (pArea->pSpf, &key);

        if (pOldSpfNode != NULL)
        {
            V3RtcProcessIntraAreaPrefLsa (pOldSpfNode, pLsaInfo, pOldLsa,
                                          pArea, OSPFV3_DELETED);
            V3RtcProcOtherIntraPrefLsaOfSpfNode (pOldSpfNode, pLsaInfo, pArea);
        }
    }

    if (pNewLsa != NULL)
    {
        V3RtcGetRefLsIdInfo (pNewLsa, &newLsId);

        if (newLsId.u2LsaType == OSPFV3_REF_ROUTER_LSA)
        {
            key.u1VertType = OSPFV3_VERT_ROUTER;
        }
        else
        {
            key.u1VertType = OSPFV3_VERT_NETWORK;
        }

        key.vertexId.u4VertexIfId =
            OSPFV3_BUFFER_DWFROMPDU (newLsId.linkStateId);

        OSPFV3_RTR_ID_COPY (key.vertexId.vertexRtrId, newLsId.advRtrId);

        pNewSpfNode = RBTreeGet (pArea->pSpf, &key);

        if (pNewSpfNode != NULL)
        {
            V3RtcProcessIntraAreaPrefLsa (pNewSpfNode, pLsaInfo, pNewLsa,
                                          pArea, OSPFV3_CREATED);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcIncUpdateIntraAreaPrefLsa\n");

}

/*****************************************************************************/
/* Function     : V3RtcProcOtherIntraPrefLsaOfSpfNode                        */
/*                                                                           */
/* Description  : This function process the intra area prefix LSA of the     */
/*                spf node.                                                  */
/*                                                                           */
/* Input        : pSpfNode      : Pointer to SPF node.                       */
/*                pArea         : Pointer to area.                           */
/*                u1Status      : Indicating the created/updated/deleted     */
/*                                state of the SPF node.                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
PRIVATE VOID
V3RtcProcOtherIntraPrefLsaOfSpfNode (tV3OsCandteNode * pSpfNode,
                                     tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea)
{
    UINT2               u2RefLsType = OSPFV3_INVALID;
    tV3OsDbNode        *pDbHashNode = NULL;
    tV3OsLsaInfo       *pDbLsaInfo = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3RtcProcOtherIntraPrefLsaOfSpfNode\n");

    if (pSpfNode->u1VertType == OSPFV3_VERT_ROUTER)
    {
        u2RefLsType = OSPFV3_REF_ROUTER_LSA;
    }
    else
    {
        u2RefLsType = OSPFV3_REF_NETWORK_LSA;
    }

    pDbHashNode =
        V3RtcSearchIntraAreaPrefixNode (&(pSpfNode->vertexId.
                                          vertexRtrId),
                                        pSpfNode->vertexId.u4VertexIfId,
                                        u2RefLsType, pArea);
    if (pDbHashNode != NULL)
    {
        TMO_SLL_Scan (&(pDbHashNode->lsaLst), pLstNode, tTMO_SLL_NODE *)
        {
            pDbLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pLstNode);
            if (pLsaInfo == pDbLsaInfo)
            {
                continue;
            }
            V3RtcProcessIntraAreaPrefLsa (pSpfNode, pDbLsaInfo,
                                          pDbLsaInfo->pLsa, pArea,
                                          OSPFV3_CREATED);
        }
    }
    else
    {
        OSPFV3_TRC (OSPFV3_RT_TRC, pLsaInfo->pV3OspfCxt->u4ContextId,
                    "No Intra area prefix LSA present for " "candidate node\n");
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3RtcProcOtherIntraPrefLsaOfSpfNode\n");
}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rtincr.c                       */
/*----------------------------------------------------------------------*/
