/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3grcfg.c,v 1.10 2017/12/26 13:34:26 siva Exp $
 *
 * Description: This file contains the data preservation for
 *              Graceful restart process.
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrCfgStoreRestartInfo                                    */
/*                                                                           */
/* Description  : This routine stores all GR specific configurations         */
/*                in non-volatile storage.                                   */
/*                                                                           */
/*                                                                           */
/*                  1. GR status                                             */
/*                  2. Restart reason                                        */
/*                  3. Last attempted GR exit reason                         */
/*                  4. Absolute time at which GR terminates                  */
/*                                                                           */
/*                  1. Link State ID                                         */
/*                  2. Link Type                                             */
/*                  3. Prefix Length                                         */
/*                  4. IPv6 Address Prefix                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrCfgStoreRestartInfo (VOID)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OsSummaryParam  *pSummaryParam = NULL;
    tUtlTm              utlTm;
    tIp6Addr            addrPrefix;
    tV3OsExtLsaLink     extLsaLink;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4SysTime = OSPFV3_INIT_VAL;    /* Time since 2000 */
    UINT4               u4RemGracePeriod = OSPFV3_INIT_VAL;
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    UINT4               u4PrevCxtId = OSPFV3_INVALID_CXT_ID;
    INT4                i1FileFd = OSPFV3_INIT_VAL;
    UINT1               au1V3OspfCxtName[OSPFV3_CXT_MAX_LEN];
    UINT1              *pAddrPrefix = addrPrefix.u1_addr;
    CHR1                au1Buf[MAX_COLUMN_LEN + OSPFV3_ONE];

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: GrStoreRestartInfo\r\n");

    MEMSET (&addrPrefix, OSPFV3_INIT_VAL, sizeof (tIp6Addr));
    MEMSET (&utlTm, OSPFV3_INIT_VAL, sizeof (tUtlTm));
    MEMSET (au1V3OspfCxtName, OSPFV3_INIT_VAL, OSPFV3_CXT_MAX_LEN);
    MEMSET (&extLsaLink, OSPFV3_INIT_VAL, sizeof (tV3OsExtLsaLink));

    i1FileFd = FileOpen (OSPFV3_GR_CONF, OSIX_FILE_CR | OSIX_FILE_WO |
                         OSIX_FILE_AP);

    if (i1FileFd < OSPFV3_ZERO)
    {
        /* File Creation failed */
        return;
    }

    /* Store the restart status and absolute time at which GR terminates */

    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_FAILURE)
    {
        /* No context available */
        FileClose (i1FileFd);
        return;
    }

    do
    {
        u4PrevCxtId = u4ContextId;
        pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
        if (pV3OspfCxt == NULL)
        {
            FileClose (i1FileFd);
            return;
        }

        MEMSET (au1V3OspfCxtName, OSPFV3_INIT_VAL, OSPFV3_CXT_MAX_LEN);
        if (V3OspfGetAliasName (u4ContextId, au1V3OspfCxtName) == OSIX_FAILURE)
        {
            /* Scan the next context */
            continue;
        }

        if (pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED)
        {
            if (TmrGetRemainingTime
                (gV3OsRtr.timerLstId,
                 (&(pV3OspfCxt->graceTimer.timerNode)),
                 &u4RemGracePeriod) != TMR_SUCCESS)
            {
                u4RemGracePeriod = OSPFV3_INIT_VAL;
            }
            else
            {
                u4RemGracePeriod =
                    u4RemGracePeriod / OSPFV3_NO_OF_TICKS_PER_SEC;
            }
        }
        else
        {
            u4RemGracePeriod = pV3OspfCxt->u4GracePeriod;
        }
        V3TmrDeleteTimer (&(pV3OspfCxt->graceTimer));

        SNPRINTF (au1Buf, MAX_COLUMN_LEN, "CONTEXT = %s\n", au1V3OspfCxtName);
        FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        if ((u4RemGracePeriod == OSPFV3_INIT_VAL) ||
            (pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_NONE))
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "RESTART_STATUS = none\n");
        }
        else if (pV3OspfCxt->u1RestartStatus == OSPFV3_RESTART_PLANNED)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "RESTART_STATUS = planned\n");
        }

        FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        if (pV3OspfCxt->u1RestartReason == OSPFV3_GR_SW_RESTART)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_REASON = restart\n");
        }

        else if (pV3OspfCxt->u1RestartReason == OSPFV3_GR_SW_UPGRADE)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_REASON = upgrade\n");
        }

        else if (pV3OspfCxt->u1RestartReason == OSPFV3_GR_SW_RED)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_REASON = redundancy\n");
        }

        FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_NONE)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_EXIT_REASON = none\n");
        }

        else if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_EXIT_REASON = inprogress\n");
        }

        else if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_COMPLETED)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_EXIT_REASON = completed\n");
        }

        else if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_TIMEDOUT)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_EXIT_REASON = timedout\n");
        }

        else if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_TOP_CHG)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_EXIT_REASON = topchg\n");
        }
        FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        /* Store the 1s timer and current system time */
        MEMSET (&utlTm, OSPFV3_INIT_VAL, sizeof (tUtlTm));
        UtlGetTime (&utlTm);
        /* Get the seconds since the base year (2000) */
        u4SysTime = V3UtilGetSecondsSinceBase (utlTm);

        SNPRINTF (au1Buf, MAX_COLUMN_LEN, "GR_END_TIME = %d\n",
                  (u4SysTime + u4RemGracePeriod));
        FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
        {
            pLsaInfo =
                (tV3OsLsaInfo *) RBTreeGetFirst (pArea->pAreaScopeLsaRBRoot);
            while (pLsaInfo != NULL)
            {
                if (((pLsaInfo->lsaId.u2LsaType ==
                      OSPFV3_INTER_AREA_PREFIX_LSA) ||
                     (pLsaInfo->lsaId.u2LsaType ==
                      OSPFV3_COND_INTER_AREA_PREFIX_LSA) ||
                     (pLsaInfo->lsaId.u2LsaType ==
                      OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA)) &&
                    (pLsaInfo->pLsaDesc != NULL) &&
                    (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL))
                {
                    pSummaryParam = (tV3OsSummaryParam *) (VOID *)
                        (pLsaInfo->pLsaDesc->pAssoPrimitive);

                    SNPRINTF (au1Buf, MAX_COLUMN_LEN, "LINK_STATE_ID = %d\n",
                              OSPFV3_BUFFER_DWFROMPDU
                              (pLsaInfo->lsaId.linkStateId));
                    FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                    SNPRINTF (au1Buf, MAX_COLUMN_LEN, "LSA_TYPE = %d\n",
                              pLsaInfo->lsaId.u2LsaType);
                    FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                    SNPRINTF (au1Buf, MAX_COLUMN_LEN, "PREFIX_LEN = %d\n",
                              pSummaryParam->prefixInfo.u1PrefixLength);
                    FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                    OSPFV3_ADD_PREFIX_IN_LSA (pAddrPrefix,
                                              pSummaryParam->prefixInfo.
                                              addrPrefix,
                                              pSummaryParam->prefixInfo.
                                              u1PrefixLength);
                    SNPRINTF (au1Buf, MAX_COLUMN_LEN,
                              "ADDRESS_PREFIX = %u.%u.%u.%u\n",
                              addrPrefix.u4_addr[OSPFV3_INIT_VAL],
                              addrPrefix.u4_addr[OSPFV3_ONE],
                              addrPrefix.u4_addr[OSPFV3_TWO],
                              addrPrefix.u4_addr[OSPFV3_THREE]);
                    FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                }
                pLsaInfo = (tV3OsLsaInfo *) RBTreeGetNext
                    (pArea->pAreaScopeLsaRBRoot, (tRBElem *) pLsaInfo, NULL);

            }
        }

        pLsaInfo = (tV3OsLsaInfo *) RBTreeGetFirst
            (pV3OspfCxt->pAsScopeLsaRBRoot);

        while (pLsaInfo != NULL)
        {
            if ((pLsaInfo->pLsaDesc != NULL) &&
                (V3RtcGetExtLsaLink (pLsaInfo->pLsa, pLsaInfo->u2LsaLen,
                                     &extLsaLink) == OSIX_SUCCESS))
            {
                SNPRINTF (au1Buf, MAX_COLUMN_LEN, "LINK_STATE_ID = %d\n",
                          OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->lsaId.
                                                   linkStateId));
                FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                SNPRINTF (au1Buf, MAX_COLUMN_LEN, "LSA_TYPE = %d\n",
                          pLsaInfo->lsaId.u2LsaType);
                FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                SNPRINTF (au1Buf, MAX_COLUMN_LEN, "PREFIX_LEN = %d\n",
                          extLsaLink.extRtPrefix.u1PrefixLength);
                FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                SNPRINTF (au1Buf, MAX_COLUMN_LEN,
                          "ADDRESS_PREFIX = %u.%u.%u.%u\n",
                          extLsaLink.extRtPrefix.addrPrefix.
                          u4_addr[OSPFV3_INIT_VAL],
                          extLsaLink.extRtPrefix.addrPrefix.u4_addr[OSPFV3_ONE],
                          extLsaLink.extRtPrefix.addrPrefix.u4_addr[OSPFV3_TWO],
                          extLsaLink.extRtPrefix.addrPrefix.
                          u4_addr[OSPFV3_THREE]);
                FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));
                MEMSET (&extLsaLink, OSPFV3_INIT_VAL, sizeof (tV3OsExtLsaLink));
            }
            pLsaInfo = (tV3OsLsaInfo *) RBTreeGetNext
                (pV3OspfCxt->pAsScopeLsaRBRoot, (tRBElem *) pLsaInfo, NULL);
        }
        /* Since router-id should not change after gracefull resart,
         * so we are saving router-id and routerid-status only when
         * gracefull restart mode is enabled and router-id selected
         * as dynamically.*/
        if (pV3OspfCxt->u1RouterIdStatus != OSPFV3_ROUTERID_PERMANENT)
        {
            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "ROUTERID = %d.%d.%d.%d\n",
                      pV3OspfCxt->rtrId[0], pV3OspfCxt->rtrId[1],
                      pV3OspfCxt->rtrId[2], pV3OspfCxt->rtrId[3]);
            FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

            SNPRINTF (au1Buf, MAX_COLUMN_LEN, "ROUTERID_STATUS     = %d\n",
                      pV3OspfCxt->u1RouterIdStatus);
            FileWrite (i1FileFd, au1Buf, STRLEN (au1Buf));

        }

    }
    while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4ContextId) != OSIX_FAILURE);
    FileClose (i1FileFd);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrCfgStoreRestartInfo\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrCfgRestoreRestartInfo                                  */
/*                                                                           */
/* Description  : This routine restores all router specific GR               */
/*                configurations from non-volatile storage.                  */
/*                                                                           */
/*                  1. GR status                                             */
/*                  2. Restart reason                                        */
/*                  3. Last attempted GR exit reason                         */
/*                  4. Absolute time at which GR terminates                  */
/*                                                                           */
/* Input        : pV3OspfCxt     -    pointer to OSPFv3 context              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrCfgRestoreRestartInfo (tV3OspfCxt * pV3OspfCxt)
{
    tUtlTm              utlTm;    /* Used to get the time */
    tV3OsGRLsIdInfo    *pGRLsIdInfo = NULL;
    UINT4               u4SysTime = OSPFV3_INIT_VAL;
    /* Current system time */
    UINT4               u4GraceEndTime = OSPFV3_INIT_VAL;
    /* Absolute time at which GR
     * exits */
    UINT4               u4ContextId = OSPFV3_INVALID_CXT_ID;
    INT4                i1FileFd = OSPFV3_INIT_VAL;
    INT1                ai1Buf[MAX_COLUMN_LEN + OSPFV3_ONE] = "\0";
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_STR_LEN + OSPFV3_ONE] = "\0";
    /* Stores line identification */
    UINT1               u1EqualStr = OSPFV3_INIT_VAL;
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_STR_LEN + OSPFV3_ONE] = "\0";
    /* Stores the value for each
     * identification */
    UINT4               u4IpAddr = OSPFV3_INIT_VAL;

    MEMSET (&utlTm, OSPFV3_INIT_VAL, sizeof (tUtlTm));
    MEMSET (ai1Buf, OSPFV3_INIT_VAL, MAX_COLUMN_LEN + OSPFV3_ONE);
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER:O3GrCfgRestoreRestartInfo\r\n");

    i1FileFd = FileOpen (OSPFV3_GR_CONF, OSIX_FILE_RO);

    if (i1FileFd < OSPFV3_ZERO)
    {
        /* File Creation failed */
        return;
    }

    while (FsUtlReadLine (i1FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, OSPFV3_INIT_VAL, MAX_STR_LEN + OSPFV3_ONE);
        MEMSET (au1ValueStr, OSPFV3_INIT_VAL, MAX_STR_LEN + OSPFV3_ONE);
        pGRLsIdInfo = NULL;

        if ((STRCMP (ai1Buf, "")) != OSPFV3_INIT_VAL)
        {
            SSCANF ((CHR1 *) ai1Buf, "%40s %c %40s", au1NameStr, &u1EqualStr,
                    au1ValueStr);
            au1ValueStr[MAX_STR_LEN] = '\0';

            if (STRCMP (au1NameStr, "CONTEXT") == OSPFV3_INIT_VAL)
            {
                u4ContextId = OSPFV3_INVALID_CXT_ID;
                V3OspfIsVcmSwitchExist (au1ValueStr, &u4ContextId);
            }

            if ((STRCMP (au1NameStr, "RESTART_STATUS") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                if (STRCMP (au1ValueStr, "planned") == OSPFV3_INIT_VAL)
                {
                    pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_PLANNED;
                    pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_INPROGRESS;
                    pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_RESTART;
                    pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_SHUTDOWN;
                }
                if (STRCMP (au1ValueStr, "none") == OSPFV3_INIT_VAL)
                {
                    pV3OspfCxt->u1RestartStatus = OSPFV3_RESTART_NONE;
                    pV3OspfCxt->u1Ospfv3RestartState = OSPFV3_GR_NONE;
                    pV3OspfCxt->u1PrevOspfv3RestartState = OSPFV3_GR_SHUTDOWN;
                }
            }
            if ((STRCMP (au1NameStr, "GR_REASON") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                if (STRCMP (au1ValueStr, "restart") == OSPFV3_INIT_VAL)
                {
                    pV3OspfCxt->u1RestartReason = OSPFV3_GR_SW_RESTART;
                }
                if (STRCMP (au1ValueStr, "upgrade") == OSPFV3_INIT_VAL)
                {
                    pV3OspfCxt->u1RestartReason = OSPFV3_GR_SW_UPGRADE;
                }
                if (STRCMP (au1ValueStr, "redundancy") == OSPFV3_INIT_VAL)
                {
                    pV3OspfCxt->u1RestartReason = OSPFV3_GR_SW_RED;
                }
            }
            if ((STRCMP (au1NameStr, "GR_EXIT_REASON") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                if (pV3OspfCxt->u1RestartExitReason !=
                    OSPFV3_RESTART_INPROGRESS)
                {
                    if (STRCMP (au1ValueStr, "none") == OSPFV3_INIT_VAL)
                    {
                        pV3OspfCxt->u1RestartExitReason = OSPFV3_RESTART_NONE;
                    }
                    if (STRCMP (au1ValueStr, "completed") == OSPFV3_INIT_VAL)
                    {
                        pV3OspfCxt->u1RestartExitReason =
                            OSPFV3_RESTART_COMPLETED;
                    }
                    if (STRCMP (au1ValueStr, "timedout") == OSPFV3_INIT_VAL)
                    {
                        pV3OspfCxt->u1RestartExitReason =
                            OSPFV3_RESTART_TIMEDOUT;
                    }
                    if (STRCMP (au1ValueStr, "topchg") == OSPFV3_INIT_VAL)
                    {
                        pV3OspfCxt->u1RestartExitReason =
                            OSPFV3_RESTART_TOP_CHG;
                    }
                }
            }
            if ((STRCMP (au1NameStr, "GR_END_TIME") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                u4GraceEndTime = ATOI (au1ValueStr);

                /* Store the 1s timer and current system time */
                MEMSET (&utlTm, OSPFV3_INIT_VAL, sizeof (tUtlTm));
                UtlGetTime (&utlTm);
                /* Get the seconds since the base year (2000) */
                u4SysTime = V3UtilGetSecondsSinceBase (utlTm);

                /* u4SysTime contains current system time
                 * u4GraceEndTime contains the graceful restart end
                 * time
                 * Start the grace timer with the remaining time */
                if (u4GraceEndTime <= u4SysTime)
                {
                    FileClose (i1FileFd);
                    O3GrExitGracefulRestartInCxt (pV3OspfCxt,
                                                  OSPFV3_RESTART_TIMEDOUT);
                    return;
                }
                else if ((u4GraceEndTime > u4SysTime) &&
                         (pV3OspfCxt->u1RestartExitReason ==
                          OSPFV3_RESTART_INPROGRESS))
                {
                    V3TmrSetTimer (&(pV3OspfCxt->graceTimer),
                                   OSPFV3_RESTART_GRACE_TIMER,
                                   OSPFV3_NO_OF_TICKS_PER_SEC *
                                   (u4GraceEndTime - u4SysTime));

                }
            }
            if ((STRCMP (au1NameStr, "LINK_STATE_ID") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                OSPFV3_LSAID_ALLOC (&(pGRLsIdInfo));
                if (pGRLsIdInfo == NULL)
                {
                    RBTreeDrain (gV3OsRtr.pGRLsIdInfo, V3UtilRBFreeLsaIds,
                                 OSPFV3_INIT_VAL);
                    FileClose (i1FileFd);
                    return;
                }

                MEMCPY (pGRLsIdInfo->linkStateId, au1ValueStr,
                        OSPFV3_LINKSTATE_ID_LEN);

                /* Fetch Link Type */
                if (FsUtlReadLine (i1FileFd, ai1Buf) != OSIX_SUCCESS)
                {
                    OSPFV3_LSAID_FREE (pGRLsIdInfo);
                    RBTreeDrain (gV3OsRtr.pGRLsIdInfo, V3UtilRBFreeLsaIds,
                                 OSPFV3_INIT_VAL);
                    FileClose (i1FileFd);
                    return;
                }
                SSCANF ((CHR1 *) ai1Buf, "%40s %c %40s", au1NameStr,
                        &u1EqualStr, au1ValueStr);
                au1ValueStr[MAX_STR_LEN] = '\0';
                pGRLsIdInfo->u2LsaType = (UINT2) ATOI (au1ValueStr);

                /* Fetch Prefix Length */
                if (FsUtlReadLine (i1FileFd, ai1Buf) != OSIX_SUCCESS)
                {
                    OSPFV3_LSAID_FREE (pGRLsIdInfo);
                    RBTreeDrain (gV3OsRtr.pGRLsIdInfo, V3UtilRBFreeLsaIds,
                                 OSPFV3_INIT_VAL);
                    FileClose (i1FileFd);
                    return;
                }
                SSCANF ((CHR1 *) ai1Buf, "%40s %c %40s", au1NameStr,
                        &u1EqualStr, au1ValueStr);
                au1ValueStr[MAX_STR_LEN] = '\0';
                pGRLsIdInfo->u1PrefixLength = (UINT1) ATOI (au1ValueStr);

                /* Fetch Prefix */
                if (FsUtlReadLine (i1FileFd, ai1Buf) != OSIX_SUCCESS)
                {
                    OSPFV3_LSAID_FREE (pGRLsIdInfo);
                    RBTreeDrain (gV3OsRtr.pGRLsIdInfo, V3UtilRBFreeLsaIds,
                                 OSPFV3_INIT_VAL);
                    FileClose (i1FileFd);
                    return;
                }
                SSCANF ((CHR1 *) ai1Buf, "%40s %c %u.%u.%u.%u",
                        au1NameStr, &u1EqualStr,
                        &pGRLsIdInfo->addrPrefix.u4_addr[OSPFV3_INIT_VAL],
                        &pGRLsIdInfo->addrPrefix.u4_addr[OSPFV3_ONE],
                        &pGRLsIdInfo->addrPrefix.u4_addr[OSPFV3_TWO],
                        &pGRLsIdInfo->addrPrefix.u4_addr[OSPFV3_THREE]);
                if (RBTreeAdd (gV3OsRtr.pGRLsIdInfo, (tRBElem *) pGRLsIdInfo)
                    == RB_FAILURE)
                {
                    OSPFV3_GBL_TRC (CONTROL_PLANE_TRC,
                                    "RBTreeAdd failed in O3GrCfgRestoreRestartInfo\r\n");
                }
            }

            if ((STRCMP (au1NameStr, "ROUTERID") == OSPFV3_INIT_VAL) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                MEMSET (pV3OspfCxt->rtrId, 0, OSPFV3_IPV4_ADDR_LEN);
                u4IpAddr = 0;
                u4IpAddr = OSIX_NTOHL (INET_ADDR (au1ValueStr));
                OSPFV3_BUFFER_DWTOPDU (pV3OspfCxt->rtrId, u4IpAddr);
            }
            if ((STRCMP (au1NameStr, "ROUTERID_STATUS") == 0) &&
                (u4ContextId == pV3OspfCxt->u4ContextId))
            {
                if (pV3OspfCxt->u1RestartExitReason ==
                    OSPFV3_RESTART_INPROGRESS)
                {
                    pV3OspfCxt->u1RouterIdStatus = ATOI (au1ValueStr);
                }

            }
        }
        MEMSET (ai1Buf, OSPFV3_INIT_VAL, MAX_COLUMN_LEN + OSPFV3_ONE);
    }

    FileClose (i1FileFd);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrCfgRestoreRestartInfo\r\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3grcfg.c                      */
/*-----------------------------------------------------------------------*/
