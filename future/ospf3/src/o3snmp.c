
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: o3snmp.c,v 1.13 2015/03/08 11:08:23 siva Exp $
 *
 * Description: This file contain routines for ospf traps to SNMP 
 *          Agent and parameter modification SNMP event routines
 *          
 *******************************************************************/

#include "o3inc.h"
#include "ospf3mib.h"
#include "snmputil.h"

/* Proto types of the functions private to this file only */

tSNMP_OID_TYPE     *O3SnmpMakeObjIdFromDotNew (INT1 *textStr);

INT4                O3SnmpParseSubIdNew (UINT1 **tempPtr);

UINT4               OSPFV3_TRAPS_OID[] = { 1, 3, 6, 1, 4, 1, 2076, 90, 9, 2 };

/*****************************************************************************/
/*                                                                           */
/* Function     : O3SnmpIfSendTrapInCxt                                      */
/*                                                                           */
/* Description  : Routine to send trap to SNMP Agent.                        */
/*                                                                           */
/* Input        : pV3OspfCxt  : Pointer to context                           */
/*                u1TrapId    : Trap identifier                              */
/*                pTrapInfo : Pointer to structure having the trap info.     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3SnmpIfSendTrapInCxt (tV3OspfCxt * pV3OspfCxt, UINT1 u1TrapId, VOID *pTrapInfo)
{
#if defined(FUTURE_SNMP_WANTED) ||defined (SNMP_3_WANTED) ||defined (SNMPV3_WANTED)
    tV3OsRstStatChgTrapInfo *pRstStatChgTrapInfo = NULL;
    tV3OsNbrRstStatChgTrapInfo *pNbrRstStatChgTrapInfo = NULL;
    tV3OsVifRstStatChgTrapInfo *pVifRstStatChgTrapInfo = NULL;
    tV3OsRedStateChgTrapInfo *pRedStateChgTrapInfo = NULL;
    tV3OsRedBulkUpdAbortTrapInfo *pRedBulkUpdAbortTrapInfo = NULL;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    UINT1               au1Buf[256];
    tSNMP_COUNTER64_TYPE u8CounterVal = { 0, 0 };
    tSNMP_OID_TYPE     *pSnmpTrapOid = NULL;

    OSPFV3_TRC1 (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                 "Trap to be generated: %d\n", u1TrapId);

    /* check for the sliding window way of trap limitations */
    if (!(pV3OspfCxt->u1TrapCount < OSPFV3_ALLOWABLE_TRAP_COUNT_IN_SWP))
    {
        return;
    }

    pEnterpriseOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pEnterpriseOid == NULL)
    {
        return;
    }
    MEMCPY (pEnterpriseOid->pu4_OidList, OSPFV3_TRAPS_OID,
            sizeof (OSPFV3_TRAPS_OID));
    pEnterpriseOid->u4_Length = sizeof (OSPFV3_TRAPS_OID) / sizeof (UINT4);
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u1TrapId;

    pSnmpTrapOid = alloc_oid (SNMP_V2_TRAP_OID_LEN);
    if (pSnmpTrapOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    MEMCPY (pSnmpTrapOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpTrapOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       u8CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpTrapOid);
        return;
    }

    pStartVb = pVbList;

    switch (u1TrapId)
    {
        case OSPFV3_RST_STATUS_CHANGE_TRAP:

            pRstStatChgTrapInfo = (tV3OsRstStatChgTrapInfo *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "ospfv3RouterId");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pRstStatChgTrapInfo->rtrId,
                                           OSPFV3_RTR_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3RestartStatus");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u1RestartStatus,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3RestartInterval");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u4GracePeriod,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3RestartExitReason");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pRstStatChgTrapInfo->u1RestartExitReason,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case OSPFV3_NBR_RST_STATUS_CHANGE_TRAP:

            pNbrRstStatChgTrapInfo = (tV3OsNbrRstStatChgTrapInfo *) pTrapInfo;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3RouterId");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pNbrRstStatChgTrapInfo->rtrId,
                                           OSPFV3_RTR_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3NbrRtrId");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pNbrRstStatChgTrapInfo->nbrRtrId,
                                           OSPFV3_RTR_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3NbrIfIndex");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u4NbrIfId, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3NbrRestartHelperStatus");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u1NbrHelperStatus, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3NbrRestartHelperAge");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->u4NbrHelperAge,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3NbrRestartHelperExitReason");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pNbrRstStatChgTrapInfo->
                 u1NbrHelperExitReason, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case OSPFV3_VIRT_NBR_RST_STATUS_CHANGE_TRAP:

            pVifRstStatChgTrapInfo = (tV3OsVifRstStatChgTrapInfo *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "ospfv3RouterId");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->rtrId,
                                           OSPFV3_RTR_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3VirtNbrArea");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->
                                           VifNbrAreaId,
                                           OSPFV3_AREA_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3VirtNbrRtrId");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            if ((pOstring =
                 SNMP_AGT_FormOctetString (pVifRstStatChgTrapInfo->VifNbrId,
                                           OSPFV3_RTR_ID_LEN)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3VirtNbrRestartHelperStatus");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->
                 u1VifNbrHelperStatus, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3VirtNbrRestartHelperAge");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->u4VifNbrHelperAge,
                 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                free_octetstring (pOstring);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "ospfv3VirtNbrRestartHelperExitReason");
            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind
                (pOid,
                 SNMP_DATA_TYPE_GAUGE32,
                 pVifRstStatChgTrapInfo->
                 u1VifNbrHelperExitReason, 0, NULL, NULL, u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_FreeOid (pEnterpriseOid);
                SNMP_FreeOid (pSnmpTrapOid);
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;
            break;

        case OSPFV3_RED_STATE_CHANGE_TRAP:

            pRedStateChgTrapInfo = (tV3OsRedStateChgTrapInfo *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "ospfv3RouterId");

            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOstring = SNMP_AGT_FormOctetString (pRedStateChgTrapInfo->rtrId,
                                                 OSPFV3_RTR_ID_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfv3HotStandbyState");

            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         pRedStateChgTrapInfo->
                                                         i4HotStandbyState,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            break;

        case OSPFV3_RED_BULK_UPD_ABORT_TRAP:
            pRedBulkUpdAbortTrapInfo =
                (tV3OsRedBulkUpdAbortTrapInfo *) pTrapInfo;
            SPRINTF ((CHR1 *) au1Buf, "ospfv3RouterId");

            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pOstring =
                SNMP_AGT_FormOctetString (pRedBulkUpdAbortTrapInfo->rtrId,
                                          OSPFV3_RTR_ID_LEN);
            if (pOstring == NULL)
            {
                SNMP_FreeOid (pOid);
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_OCTET_PRIM,
                                                         0L, 0, pOstring, NULL,
                                                         u8CounterVal);
            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfv3DynamicBulkUpdStatus");

            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         pRedBulkUpdAbortTrapInfo->
                                                         i4DynamicBulkUpdStatus,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

            SPRINTF ((CHR1 *) au1Buf, "futOspfv3TrapBulkUpdAbortReason");

            if ((pOid = O3SnmpMakeObjIdFromDotNew ((INT1 *) au1Buf)) == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList->pNextVarBind =
                (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                         SNMP_DATA_TYPE_INTEGER,
                                                         0,
                                                         pRedBulkUpdAbortTrapInfo->
                                                         u1BulkUpdateAbortReason,
                                                         NULL, NULL,
                                                         u8CounterVal);

            if (pVbList->pNextVarBind == NULL)
            {
                SNMP_free_snmp_vb_list (pStartVb);
                return;
            }

            pVbList = pVbList->pNextVarBind;

        default:

            break;
    }

    /* The following API sends the Trap info to the FutureSNMP Agent. */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    /* Replace this function with the Corresponding Function provided by 
     * SNMP Agent for Notifying Traps.
     */
#else

    UNUSED_PARAM (u1TrapId);
    UNUSED_PARAM (pTrapInfo);

#endif /* FUTURE_SNMP_WANTED */
    pV3OspfCxt->u1TrapCount++;

}

/******************************************************************************
* Function : O3SnmpMakeObjIdFromDotNew                                         *
* Input    : textStr
* Output   : NONE
* Returns  : oidPtr or NULL                                                   
*******************************************************************************/

INT1                tempBuf[256 + 1];
tSNMP_OID_TYPE     *
O3SnmpMakeObjIdFromDotNew (INT1 *textStr)
{
    tSNMP_OID_TYPE     *oidPtr = NULL;
    INT1               *tempPtr = NULL, *dotPtr = NULL;
    UINT2               i = 0;
    UINT2               dotCount = 0;
    UINT1              *pu1TmpPtr = NULL;
    UINT2               u2BufferLen = 0;
    MEMSET (tempBuf, 0, sizeof (tempBuf));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*textStr))
    {
        dotPtr = (INT1 *) STRCHR ((INT1 *) textStr, '.');

        /* if no dot, point to end of string */
        if (dotPtr == NULL)
            dotPtr = textStr + STRLEN ((INT1 *) textStr);
        tempPtr = textStr;

        for (i = 0; ((tempPtr < dotPtr) && (i < 256)); i++)
            tempBuf[i] = *tempPtr++;
        tempBuf[i] = '\0';

        for (i = 0; ((i < OSPFV3_OID_TBL_SIZE &&
                      orig_mib_oid_table[i].pName != NULL)); i++)
        {
            if ((STRCMP (orig_mib_oid_table[i].pName, (INT1 *) tempBuf) ==
                 0)
                && (STRLEN ((INT1 *) tempBuf) ==
                    STRLEN (orig_mib_oid_table[i].pName)))
            {
                STRNCPY ((INT1 *) tempBuf, orig_mib_oid_table[i].pNumber,
                         (sizeof (tempBuf) - 1));

                break;
            }
        }

        if ((i == OSPFV3_OID_TBL_SIZE) || (orig_mib_oid_table[i].pName == NULL))
        {
            return (NULL);
        }

        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = sizeof (tempBuf) - STRLEN (tempBuf);
        STRNCAT ((INT1 *) tempBuf, (INT1 *) dotPtr,
                 (u2BufferLen <
                  STRLEN (dotPtr) ? u2BufferLen : STRLEN (dotPtr)));

    }
    else
    {                            /* is not alpha, so just copy into tempBuf */
        STRNCPY ((INT1 *) tempBuf, (INT1 *) textStr, (sizeof (tempBuf) - 1));
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    dotCount = 0;
    for (i = 0; ((i < 256) && tempBuf[i] != '\0'); i++)
    {
        if (tempBuf[i] == '.')
            dotCount++;
    }
    if ((oidPtr = alloc_oid ((INT4) (dotCount + 1))) == NULL)
    {
        return (NULL);
    }

    /* now we convert number.number.... strings */
    pu1TmpPtr = (UINT1 *) tempBuf;
    for (i = 0; i < dotCount + 1; i++)
    {
        if ((oidPtr->pu4_OidList[i] =
             ((UINT4) (O3SnmpParseSubIdNew (&pu1TmpPtr)))) == (UINT4) -1)
        {
            free_oid (oidPtr);
            return (NULL);
        }
        if (*pu1TmpPtr == '.')
            pu1TmpPtr++;        /* to skip over dot */
        else if (*pu1TmpPtr != '\0')
        {
            free_oid (oidPtr);
            return (NULL);
        }
    }                            /* end of for loop */

    return (oidPtr);
}

/******************************************************************************
* Function : O3SnmpParseSubIdNew
* Input    : tempPtr
* Output   : None
* Returns  : value of tempPtr or -1
*******************************************************************************/

INT4
O3SnmpParseSubIdNew (UINT1 **tempPtr)
{
    INT4                value = 0;
    UINT1              *tmp = NULL;

    for (tmp = *tempPtr; (((*tmp >= '0') && (*tmp <= '9')) ||
                          ((*tmp >= 'a') && (*tmp <= 'f')) ||
                          ((*tmp >= 'A') && (*tmp <= 'F'))); tmp++)
    {
        value = (value * 10) + (*tmp & 0xf);
    }

    if (*tempPtr == tmp)
    {
        value = -1;
    }
    *tempPtr = tmp;
    return (value);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3SnmpSendStatusChgTrapInCxt                               */
/*                                                                           */
/* Description  : This routine send the trap regarding the status change     */
/*                in restarting router and helper                            */
/*                                                                           */
/*                1. Restart status change                                   */
/*                2. Helper status change                                    */
/*                3. Virtual Neighbor Helper status change                   */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                pNbr         - Pointer to neighbor                         */
/*                u4Trap       - Trap info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3SnmpSendStatusChgTrapInCxt (tV3OspfCxt * pV3OspfCxt,
                              tV3OsNeighbor * pNbr, UINT4 u4Trap)
{
    tV3OsRstStatChgTrapInfo rstStatChgTrapInfo;
    tV3OsNbrRstStatChgTrapInfo nbrRstStatChgTrapInfo;
    tV3OsVifRstStatChgTrapInfo vifRstStatChgTrapInfo;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC: ENTRY O3SnmpSendStatusChgTrap\r\n");

    if (u4Trap == OSPFV3_RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&rstStatChgTrapInfo, 0, sizeof (tV3OsRstStatChgTrapInfo));

        if (OSPFV3_IS_TRAP_ENABLED (pV3OspfCxt, OSPFV3_RST_STATUS_CHANGE_TRAP))
        {
            OSPFV3_RTR_ID_COPY (rstStatChgTrapInfo.rtrId, pV3OspfCxt->rtrId);
            rstStatChgTrapInfo.u1RestartStatus = pV3OspfCxt->u1RestartStatus;
            rstStatChgTrapInfo.u4GracePeriod = pV3OspfCxt->u4GracePeriod;
            rstStatChgTrapInfo.u1RestartExitReason
                = pV3OspfCxt->u1RestartExitReason;

            O3SnmpIfSendTrapInCxt (pV3OspfCxt, OSPFV3_RST_STATUS_CHANGE_TRAP,
                                   &rstStatChgTrapInfo);
        }
    }
    else if (u4Trap == OSPFV3_NBR_RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&nbrRstStatChgTrapInfo, 0, sizeof (tV3OsNbrRstStatChgTrapInfo));

        if (OSPFV3_IS_TRAP_ENABLED
            (pV3OspfCxt, OSPFV3_NBR_RST_STATUS_CHANGE_TRAP))
        {
            OSPFV3_RTR_ID_COPY (nbrRstStatChgTrapInfo.rtrId, pV3OspfCxt->rtrId);
            OSPFV3_RTR_ID_COPY (nbrRstStatChgTrapInfo.nbrRtrId, pNbr->nbrRtrId);
            nbrRstStatChgTrapInfo.u4NbrIfId = pNbr->u4NbrInterfaceId;
            nbrRstStatChgTrapInfo.u1NbrHelperStatus = pNbr->u1NbrHelperStatus;
            nbrRstStatChgTrapInfo.u1NbrHelperExitReason
                = pNbr->u1NbrHelperExitReason;
            if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                if (TmrGetRemainingTime
                    (gV3OsRtr.timerLstId,
                     (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                     &nbrRstStatChgTrapInfo.u4NbrHelperAge) == TMR_FAILURE)
                {
                    nbrRstStatChgTrapInfo.u4NbrHelperAge = 0;
                }
                else
                {
                    nbrRstStatChgTrapInfo.u4NbrHelperAge =
                        (nbrRstStatChgTrapInfo.u4NbrHelperAge
                         / OSPFV3_NO_OF_TICKS_PER_SEC);
                }
            }
        }

        O3SnmpIfSendTrapInCxt (pV3OspfCxt, OSPFV3_NBR_RST_STATUS_CHANGE_TRAP,
                               &nbrRstStatChgTrapInfo);
    }
    else if (u4Trap == OSPFV3_VIRT_NBR_RST_STATUS_CHANGE_TRAP)
    {
        MEMSET (&vifRstStatChgTrapInfo, 0, sizeof (tV3OsVifRstStatChgTrapInfo));

        if (OSPFV3_IS_TRAP_ENABLED
            (pV3OspfCxt, OSPFV3_VIRT_NBR_RST_STATUS_CHANGE_TRAP))
        {
            OSPFV3_RTR_ID_COPY (vifRstStatChgTrapInfo.rtrId, pV3OspfCxt->rtrId);
            OSPFV3_RTR_ID_COPY (vifRstStatChgTrapInfo.VifNbrId, pNbr->nbrRtrId);
            OSPFV3_AREA_ID_COPY (vifRstStatChgTrapInfo.VifNbrAreaId,
                                 pNbr->pInterface->pArea->areaId);
            vifRstStatChgTrapInfo.u1VifNbrHelperStatus
                = pNbr->u1NbrHelperStatus;
            vifRstStatChgTrapInfo.u1VifNbrHelperExitReason
                = pNbr->u1NbrHelperExitReason;
            if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                if (TmrGetRemainingTime
                    (gV3OsRtr.timerLstId,
                     (tTmrAppTimer *) (&(pNbr->helperGraceTimer.timerNode)),
                     &vifRstStatChgTrapInfo.u4VifNbrHelperAge) == TMR_FAILURE)
                {
                    vifRstStatChgTrapInfo.u4VifNbrHelperAge = 0;
                }
                else
                {
                    vifRstStatChgTrapInfo.u4VifNbrHelperAge =
                        (vifRstStatChgTrapInfo.u4VifNbrHelperAge
                         / OSPFV3_NO_OF_TICKS_PER_SEC);
                }
            }
        }

        O3SnmpIfSendTrapInCxt (pV3OspfCxt,
                               OSPFV3_VIRT_NBR_RST_STATUS_CHANGE_TRAP,
                               &vifRstStatChgTrapInfo);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "FUNC: EXIT O3SnmpSendStatusChgTrap\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3SnmpAuthSeqNumWrapTrapInCxt                              */
/*                                                                           */
/* Description  : This routine send the trap regarding the sequence number   */
/*                wrap                                                       */
/*                                                                           */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                u4Trap       - Trap info                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3SnmpAuthSeqNumWrapTrapInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4Trap)
{
    tV3OsAuthSeqNumWrapTrapInfo AuthSeqNumTrapInfo;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "FUNC: ENTRY O3SnmpAuthSeqNumWrapTrapInCxt\r\n");

    if (u4Trap == OSPFV3_AUTH_SEQNUM_WRAP_TRAP)
    {
        MEMSET (&AuthSeqNumTrapInfo, 0, sizeof (tV3OsAuthSeqNumWrapTrapInfo));

        if (OSPFV3_IS_TRAP_ENABLED (pV3OspfCxt, OSPFV3_AUTH_SEQNUM_WRAP_TRAP))
        {
            OSPFV3_RTR_ID_COPY (AuthSeqNumTrapInfo.rtrId, pV3OspfCxt->rtrId);

            O3SnmpIfSendTrapInCxt (pV3OspfCxt, OSPFV3_AUTH_SEQNUM_WRAP_TRAP,
                                   &AuthSeqNumTrapInfo);
        }
    }
}


/********************************************************************
                    End  of  file o3snmp.c
********************************************************************/
