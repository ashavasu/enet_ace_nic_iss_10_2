/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3helper.c,v 1.8 2017/12/26 13:34:27 siva Exp $
 *
 * Description:This file contains the helper router related
 *             modules during Graceful restart process.
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CheckLinkLsaForTopologyChg                               */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*               receiving the link LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Link LSA1                */
/*                pLsa2   - Pointer to the received Link LSA2                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PRIVATE INT4
V3CheckLinkLsaForTopologyChg (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tV3OsPrefixInfo     PrefixInfo1, PrefixInfo2;
    tV3OsLsHeader       lsHeader;
    UINT1              *pu1Lsa1 = NULL;
    UINT1              *pu1Lsa2 = NULL;
    UINT4               u4PrefixCount1 = OSPFV3_INIT_VAL;
    UINT4               u4PrefixCount2 = OSPFV3_INIT_VAL;

    MEMSET (&PrefixInfo1, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&PrefixInfo2, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3CheckLinkLsaForTopologyChg\r\n");

    pu1Lsa1 = pLsa1->pLsa + OSPFV3_LINK_LSA_HDR_LEN;

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Skip Options */
    pu1Lsa2 = pLsa2 + OSPFV3_FOUR_BYTES;

    if (MEMCMP (pu1Lsa1, pu1Lsa2, OSPFV3_IPV6_ADDR_LEN) != OSPFV3_EQUAL)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    pu1Lsa1 = pu1Lsa1 + OSPFV3_IPV6_ADDR_LEN;
    pu1Lsa2 = pu1Lsa2 + OSPFV3_IPV6_ADDR_LEN;

    u4PrefixCount1 = OSPFV3_LGET4BYTE (pu1Lsa1);
    u4PrefixCount2 = OSPFV3_LGET4BYTE (pu1Lsa2);

    if (u4PrefixCount1 != u4PrefixCount2)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }
    while (u4PrefixCount1--)
    {
        pu1Lsa1 = V3UtilExtractPrefixFromLsa (pu1Lsa1, &PrefixInfo1);
        pu1Lsa2 = V3UtilExtractPrefixFromLsa (pu1Lsa2, &PrefixInfo2);

        if (MEMCMP (&PrefixInfo1, &PrefixInfo2, sizeof (tV3OsPrefixInfo))
            != OSPFV3_INIT_VAL)
        {
            return OSPFV3_TOPOLOGY_CHANGE;
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3CheckLinkLsaAForTopologyChg\r\n");
    return OSPFV3_NO_TOPOLOGY_CHANGE;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CheckRtrLsaForTopologyChg                                */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the Router LSA. Compares the two given LSAs and  */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Router LSA1              */
/*                pLsa2   - Pointer to the received Router LSA2              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PRIVATE INT4
V3CheckRtrLsaForTopologyChg (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tV3OsRtrLsaRtInfo   RtrLsaLink;
    tV3OsRtrLsaRtInfo  *pRtrLsaLinkFromLSDB = NULL;
    tV3OsLsHeader       lsHeader;
    UINT1              *pu1CurrPtr = NULL, *pu1LsaPtr = NULL;

    MEMSET (&RtrLsaLink, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3CheckRtrLsaForTopologyChg\r\n");

    pu1LsaPtr = pLsa1->pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;
    pu1CurrPtr = pLsa2 + OSPFV3_RTR_LSA_FIXED_PORTION;

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Compare the V, E, B bits in the LSA */
    if (MEMCMP (pu1LsaPtr, pu1CurrPtr, OSPFV3_ONE) != OSPFV3_INIT_VAL)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Construct the Router LSA RBTree with the Router LSA 
     * derived from OSPF LSDB */
    if (V3UtilConstRtrLsaRBTree (pLsa1->pLsa) == OSIX_FAILURE)
    {
        RBTreeDrain (gV3OsRtr.pRtrLsaCheck, V3UtilRBFreeRouterLinks,
                     OSPFV3_INIT_VAL);
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    while ((pu1CurrPtr - pLsa2) < lsHeader.u2LsaLen)
    {
        MEMSET (&RtrLsaLink, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
        O3GrLsuGetLinksFromLsa (pu1CurrPtr, &RtrLsaLink);

        /*Increment the Pointer for taking next link */
        pu1CurrPtr = pu1CurrPtr + OSPFV3_RTR_LSA_LINK_LEN;

        pRtrLsaLinkFromLSDB = RBTreeGet (gV3OsRtr.pRtrLsaCheck, &RtrLsaLink);

        if (pRtrLsaLinkFromLSDB == NULL)
        {
            /* Drain the creates Router LSA RBTree */
            RBTreeDrain (gV3OsRtr.pRtrLsaCheck, V3UtilRBFreeRouterLinks,
                         OSPFV3_INIT_VAL);
            return OSPFV3_TOPOLOGY_CHANGE;
        }
        if (pRtrLsaLinkFromLSDB->u2LinkMetric != RtrLsaLink.u2LinkMetric)
        {
            RBTreeDrain (gV3OsRtr.pRtrLsaCheck, V3UtilRBFreeRouterLinks,
                         OSPFV3_INIT_VAL);
            return OSPFV3_TOPOLOGY_CHANGE;
        }
    }

    /* Drain the creates Router LSA RBTree */
    RBTreeDrain (gV3OsRtr.pRtrLsaCheck, V3UtilRBFreeRouterLinks,
                 OSPFV3_INIT_VAL);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3CheckRtrLsaForTopologyChg\r\n");
    return OSPFV3_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CheckNwLsaForTopologyChg                                 */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the Network LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Network LSA1             */
/*                pLsa2   - Pointer to the received Network LSA2             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PRIVATE INT4
V3CheckNwLsaForTopologyChg (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tV3OsNwLsaRtInfo    NwLsaLinkInfo;
    tV3OsLsHeader       lsHeader;
    tV3OsNwLsaRtInfo   *pNwLsaLinkFromLSDB = NULL;
    UINT1              *pu1CurrPtr = NULL;

    MEMSET (&NwLsaLinkInfo, OSPFV3_INIT_VAL, sizeof (tV3OsNwLsaRtInfo));
    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3CheckNwLsaForTopologyChg\r\n");

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Construct the Network  LSA RBTree with the Network LSA 
     * derived from OSPF LSDB, RBTree Key is Attached Router Id */
    if (V3UtilConstNwLsaRBTree (pLsa1->pLsa) == OSIX_FAILURE)
    {
        RBTreeDrain (gV3OsRtr.pNwLsaCheck, V3UtilRBFreeNwLinks,
                     OSPFV3_INIT_VAL);
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Skip the LSA header, options fields */
    pu1CurrPtr = pLsa2 + OSPFV3_NTWR_LSA_FIXED_PORTION;

    while ((pu1CurrPtr - pLsa2) < lsHeader.u2LsaLen)
    {
        MEMSET (&NwLsaLinkInfo, OSPFV3_INIT_VAL, sizeof (tV3OsNwLsaRtInfo));
        NwLsaLinkInfo.u4AttachedRtrId = (UINT4) OSPFV3_LGET4BYTE (pu1CurrPtr);

        pNwLsaLinkFromLSDB = RBTreeGet (gV3OsRtr.pNwLsaCheck, &NwLsaLinkInfo);

        if (pNwLsaLinkFromLSDB == NULL)
        {
            /* Drain the creates Network LSA RBTree */
            RBTreeDrain (gV3OsRtr.pNwLsaCheck, V3UtilRBFreeNwLinks,
                         OSPFV3_INIT_VAL);
            return OSPFV3_TOPOLOGY_CHANGE;
        }
    }

    /* Drain the creates Network LSA RBTree */
    RBTreeDrain (gV3OsRtr.pNwLsaCheck, V3UtilRBFreeNwLinks, OSPFV3_INIT_VAL);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3CheckNwLsaForTopologyChg\r\n");

    return OSPFV3_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CheckSumLsaForTopologyChg                                */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*                receiving the summary LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received Summary LSA1             */
/*                pLsa2   - Pointer to the received summary LSA2             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PRIVATE INT4
V3CheckSumLsaForTopologyChg (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tV3OsPrefixInfo     PrefixInfo1, PrefixInfo2;
    tV3OsLsHeader       lsHeader;

    MEMSET (&PrefixInfo1, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&PrefixInfo2, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3CheckSumLsaForTopologyChg\r\n");

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }
    PrefixInfo1.u1PrefixLength = *(pLsa1->pLsa +
                                   OSPFV3_INTER_AREA_PREF_LEN_OFFSET);
    OSPFV3_IP6_ADDR_PREFIX_COPY (PrefixInfo1.addrPrefix,
                                 *(pLsa1->pLsa + OSPFV3_INTER_AREA_PREF_OFFSET),
                                 PrefixInfo1.u1PrefixLength);

    PrefixInfo2.u1PrefixLength = *(pLsa2 + OSPFV3_INTER_AREA_PREF_LEN_OFFSET);
    OSPFV3_IP6_ADDR_PREFIX_COPY (PrefixInfo2.addrPrefix,
                                 *(pLsa2 + OSPFV3_INTER_AREA_PREF_OFFSET),
                                 PrefixInfo2.u1PrefixLength);
    if (MEMCMP (&PrefixInfo1, &PrefixInfo2,
                sizeof (tV3OsPrefixInfo)) != OSPFV3_INIT_VAL)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3CheckSumLSAForTopologyChg\r\n");
    return OSPFV3_NO_TOPOLOGY_CHANGE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CheckExtLsaForTopologyChg                                */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY CHANGE upon     */
/*               receiving the external LSA. Compares the two given LSAs and */
/*                returns TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE  */
/*                 else                                                      */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received External LSA1            */
/*                pLsa2   - Pointer to the received External LSA2            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PRIVATE INT4
V3CheckExtLsaForTopologyChg (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    tV3OsPrefixInfo     PrefixInfo1, PrefixInfo2;
    tV3OsLsHeader       lsHeader;
    UINT1              *pu1LsaPtr = NULL;

    MEMSET (&PrefixInfo1, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&PrefixInfo2, OSPFV3_INIT_VAL, sizeof (tV3OsPrefixInfo));
    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    if ((pLsa1 == NULL) || (pLsa2 == NULL) || (pLsa1->pLsa == NULL))
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3CheckExtLsaForTopologyChg\r\n");

    pu1LsaPtr = pLsa1->pLsa + OSPFV3_LS_HEADER_SIZE + OSPFV3_ONE;

    /* extract ls header info */
    V3UtilExtractLsHeaderFromLbuf (pLsa2, &lsHeader);

    /* Compare the the LSA Length */
    if (pLsa1->u2LsaLen != lsHeader.u2LsaLen)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    /* Compare  the Metric */
    if (MEMCMP (pu1LsaPtr, pLsa2 + OSPFV3_ONE, OSPFV3_THREE) != OSPFV3_INIT_VAL)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    PrefixInfo1.u1PrefixLength = *(pLsa1->pLsa +
                                   OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET);
    OSPFV3_IP6_ADDR_PREFIX_COPY (PrefixInfo1.addrPrefix,
                                 *(pLsa1->pLsa + OSPFV3_AS_EXT_LSA_PREF_OFFSET),
                                 PrefixInfo1.u1PrefixLength);

    PrefixInfo2.u1PrefixLength = *(pLsa2 + OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET);
    OSPFV3_IP6_ADDR_PREFIX_COPY (PrefixInfo2.addrPrefix,
                                 *(pLsa2 + OSPFV3_AS_EXT_LSA_PREF_OFFSET),
                                 PrefixInfo2.u1PrefixLength);

    if (MEMCMP (&PrefixInfo1, &PrefixInfo2, sizeof (tV3OsPrefixInfo))
        != OSPFV3_INIT_VAL)
    {
        return OSPFV3_TOPOLOGY_CHANGE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3CheckExtLsaAForTopologyChg\r\n");
    return OSPFV3_NO_TOPOLOGY_CHANGE;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrFindTopologyChangeLsa                                  */
/*                                                                           */
/* Description  : This function is used to find the TOPOLOGY_CHANGE upon     */
/*                receiving the LSA. Compares the two given LSAs and returns */
/*                TOPOLOGY_CHANGE is it differs, NO_TOPOLOGY_CHANGE else     */
/*                                                                           */
/* Input        : pLsa1   - Pointer to the received LSA1                     */
/*                pLsa2   - Pointer to the received LSA2                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TOPOLOGY_CHANGE, if topology change occured                */
/*                NO_TOPOLOGY_CHANGE, if no topology change occured          */
/*                OSIX_FAILURE, if LSA received with unknown LSA type        */
/*****************************************************************************/

PUBLIC INT4
O3GrFindTopologyChangeLsa (tV3OsLsaInfo * pLsa1, UINT1 *pLsa2)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrFindTopologyChangeLsa\r\n");

    switch (pLsa1->lsaId.u2LsaType)
    {
        case OSPFV3_LINK_LSA:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC,
                            "Checking Topology Change for Link LSA \r\n");
            return (V3CheckLinkLsaForTopologyChg (pLsa1, pLsa2));

        case OSPFV3_ROUTER_LSA:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC,
                            "Checking Topology Change for Router LSA \r\n");
            return (V3CheckRtrLsaForTopologyChg (pLsa1, pLsa2));

        case OSPFV3_NETWORK_LSA:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC,
                            "Checking Topology Change for Network LSA \r\n");
            return (V3CheckNwLsaForTopologyChg (pLsa1, pLsa2));

        case OSPFV3_INTER_AREA_PREFIX_LSA:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC,
                            "Checking Topology Change for Summary LSA \r\n");
            return (V3CheckSumLsaForTopologyChg (pLsa1, pLsa2));

        case OSPFV3_AS_EXT_LSA:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC,
                            "Checking Topology Change for External LSA \r\n");
            return (V3CheckExtLsaForTopologyChg (pLsa1, pLsa2));

        default:
            OSPFV3_GBL_TRC (OSPFV3_HELPER_TRC, "Unknown LSA Type\r\n");
            break;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "ENTER: O3GrFindTopologyChangeLsa\r\n");

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrHelperProcessGraceLsa                                  */
/*                                                                           */
/* Description  : Reference : RFC-3623 section 3.1                           */
/*                This procedure takes care of the processing of a grace     */
/*                LSA received.                                              */
/*                This function  will be called when the Helper receives     */
/*                Grace LSA from the Restarting router                       */
/*                                                                           */
/* Input        : pNbr    - Pointer to the neighbor from where the GraceLSA  */
/*                          is received                                      */
/*                pHeader - Pointer to the LSA header                        */
/*                pLsa    - Pointer to the received Grace LSA                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if successfully processed                    */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrHelperProcessGraceLsa (tV3OsNeighbor * pNbr,
                           tV3OsLsHeader * pHeader, UINT1 *pLsa)
{
    tV3OsRxmtNode      *pRxmtNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsInterface     *pInterface = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsNeighbor      *pNeighbor = NULL;
    UINT1              *pu1CurPtr = NULL;
    UINT4               u4GracePeriod = OSPFV3_INIT_VAL;
    UINT1               u1RestartReason = OSPFV3_INIT_VAL;

    if ((pNbr == NULL) || (pNbr->pInterface == NULL) ||
        (pNbr->pInterface->pArea == NULL) ||
        (pNbr->pInterface->pArea->pV3OspfCxt == NULL))
    {
        return OSIX_FAILURE;
    }

    pV3OspfCxt = pNbr->pInterface->pArea->pV3OspfCxt;

    pInterface = pNbr->pInterface;
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: O3GrHelperProcessGraceLsa\r\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, pHeader->advRtrId) ==
            OSPFV3_EQUAL)
        {
            break;
        }
    }
    if (pNbrNode == NULL)
    {
        return OSIX_FAILURE;
    }
    pu1CurPtr = pLsa;
    pu1CurPtr = pu1CurPtr + OSPFV3_GR_LSA_GRACE_PERIOD_OFFSET;
    /* pLsa starts from TLV for Grace period */
    u4GracePeriod = OSPFV3_LGET4BYTE (pu1CurPtr);
    pu1CurPtr = pu1CurPtr + sizeof (UINT4);    /* Move pointer to Restart Reason */
    u1RestartReason = OSPFV3_LGET1BYTE (pu1CurPtr);

    /* 1. Check if the helper is performing GR */
    if (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS)
    {
        OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "This router is a GR router,Cann't act as Helper\r\n");
        return OSIX_FAILURE;
    }
    /* 2. Check Whether Helper is having support for this restart */
    if (!((OSPFV3_ONE << u1RestartReason) & (pV3OspfCxt->u1HelperSupport)))
    {
        OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Cann't act as helper,Helper is not having \
                support for this restart \r\n");
        pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;
        return OSIX_FAILURE;
    }

    /* 3. Check if the neighbor from where grace LSA is received 
     * is fully adjacent, If not fully adjacent, exit the helper  */

    if (!((OSPFV3_IS_NBR_FULL (pNbr)) ||
          (V3NsmToBecomeAdj (pNbr) == OSIX_TRUE) ||
          ((pNbr->u1NsmState == OSPFV3_NBRS_2WAY) &&
           (pInterface->u1IsmState == OSPFV3_IFS_DR_OTHER))))
    {
        OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Cann't act as Helper,Neighbor Status is not full\r\n");
        pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;
        return OSIX_FAILURE;
    }

    /* 4. LSA Age should be less than the Grace Period in Grace LSA */
    if (pHeader->u2LsaAge > u4GracePeriod)
    {
        OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC,
                        pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "Cann't act as helper,Max aged Grace LSA received \r\n");
        pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;
        return OSIX_FAILURE;
    }

    /* If strict LSA check is enabled, check for topology change */
    if (pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED)
    {
        TMO_SLL_Scan (&(pNbr->lsaRxmtLst), pRxmtNode, tV3OsRxmtNode *)
        {
            /* If topology change is detected, exit the helper */
            if ((V3UtilRtrIdComp (pNbr->nbrRtrId,
                                  pRxmtNode->pLsaInfo->lsaId.advRtrId) ==
                 OSPFV3_EQUAL)
                && (pRxmtNode->pLsaInfo->u1LsaRefresh == OSIX_TRUE))
            {
                OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC,
                                pNbr->pInterface->pArea->pV3OspfCxt->
                                u4ContextId,
                                "Cann't act as helper,Topology Change detected \r\n");
                pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;
                return OSIX_FAILURE;
            }
        }
    }

    OSPFV3_EXT_TRC1 (OSPFV3_HELPER_TRC,
                     pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                     " Acting as Helper for the Neighbor %x \r\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

    /* This router can enter into the helper process */
    pNbr->u1NbrHelperStatus = OSPFV3_GR_HELPING;
    pNbr->u1NbrHelperExitReason = OSPFV3_HELPER_INPROGRESS;
    pV3OspfCxt->u1HelperStatus = OSPFV3_GR_HELPING;

    /* Update u1GRRouterPresent in pInterface, this flag is required 
       in helper mode to know atlease on GR router present in this 
       interface as a neighbor */
    pNbr->pInterface->u1GRRouterPresent = OSIX_TRUE;

    /* Start the Grace Timer for GR Neighbor */
    /* Helper will support upto the configured Grace time limit only */
    if ((pV3OspfCxt->u4HelperGrTimeLimit > OSPFV3_INIT_VAL) &&
        (u4GracePeriod > pV3OspfCxt->u4HelperGrTimeLimit))
    {
        u4GracePeriod = pV3OspfCxt->u4HelperGrTimeLimit;
    }
    V3TmrSetTimer (&(pNbr->helperGraceTimer), OSPFV3_HELPER_GRACE_TIMER,
                   (OSPFV3_NO_OF_TICKS_PER_SEC * u4GracePeriod));

    /* Send trap notification regarding the status change.
     * Nbr parameter is required for Neighbor status change
     * when the router is acting as helper */
    O3SnmpSendStatusChgTrapInCxt (pV3OspfCxt, pNbr,
                                  OSPFV3_NBR_RST_STATUS_CHANGE_TRAP);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrHelperProcessGraceLsa\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrExitHelper                                             */
/*                                                                           */
/* Description  : Reference : RFC-3623 section 3.2                           */
/*                This function will be called when the router exits from    */
/*                from helper mode. It dows the following                    */
/*                Stops the helper grace timer,  does DR/BDR election,       */
/*                If the exit reason is unsuccessful, makes the GR router    */
/*                down and generates N/W LSA if the router is elected as DR  */
/*                                                                           */
/* Input        : pNbr    - Pointer to the neighbor for which the helper     */
/*                          support to be exited                             */
/*                u1ExitReason - Helper exit reason                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if successfully exited                       */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrExitHelper (UINT1 u1ExitReason, tV3OsNeighbor * pNbr)
{
    tV3OsLinkStateId    linkStateId;
    tV3OsArea          *pArea = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pAreaNode = NULL;
    tV3OsNeighbor      *pNeighbor = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4RtrLsaCnt = OSPFV3_INIT_VAL;
    UINT4               u4ContextId = 0;

    if ((pNbr == NULL) || (pNbr->pInterface == NULL))
    {
        gu4O3GrExitHelperFail++;
        return OSIX_FAILURE;
    }
    u4ContextId = pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId;

    MEMSET (linkStateId, OSPFV3_INIT_VAL, sizeof (tV3OsLinkStateId));
    pV3OspfCxt = pNbr->pInterface->pArea->pV3OspfCxt;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId, "ENTER: O3GrExitHelper\r\n");

    /* Update the Exit reason properly, reason can be GR_UNSUCCESSFUL, 
       GR_SUCCESSFUL, INTERFACE_UPDATE */
    pNbr->u1NbrHelperExitReason = u1ExitReason;
    pNbr->u1NbrHelperStatus = OSPFV3_GR_NOT_HELPING;

    /* Update the u1GRRouterPresent, If none of the neighbors in this interface
     *        are GR router, reset the u1GRRouterPresent */
    TMO_SLL_Scan (&(pNbr->pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);
        if (pNeighbor->u1NbrHelperStatus == OSPFV3_GR_HELPING)
        {
            pNeighbor->pInterface->u1GRRouterPresent = OSIX_TRUE;
            break;
        }
        pNeighbor->pInterface->u1GRRouterPresent = OSIX_FALSE;
    }

    pNeighbor = NULL;
    /* Reset Helper Status */
    pV3OspfCxt->u1HelperStatus = OSPFV3_GR_NOT_HELPING;
    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);
        if (pNeighbor->u1NbrHelperStatus == OSPFV3_GR_HELPING)
        {
            pV3OspfCxt->u1HelperStatus = OSPFV3_GR_HELPING;
        }
    }

    V3TmrDeleteTimer (&(pNbr->helperGraceTimer));

    OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC, u4ContextId,
                    "Start the DR/BDR election for Helper exit\r\n");
    /* Start the DR/BDR election for the interface which was undergone GR */
    if ((pNbr->pInterface->u1NetworkType != OSPFV3_IF_PTOP) &&
        (pNbr->pInterface->u1NetworkType != OSPFV3_IF_PTOMP))
    {
        V3HpElectDesgRtr (pNbr->pInterface);
    }

    /* Successful Helper exit, Generate Router and N/w LSA */
    OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC, u4ContextId,
                    " Successful Helper exit,Sending Router and N/W LSAs \r\n");
    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pAreaNode);
        for (u4RtrLsaCnt = OSPFV3_INIT_VAL;
             u4RtrLsaCnt <= pArea->u4rtrLsaCounter; u4RtrLsaCnt++)
        {
            OSPFV3_BUFFER_DWTOPDU (linkStateId, u4RtrLsaCnt);
            V3GenerateLsa (pArea, OSPFV3_ROUTER_LSA,
                           &linkStateId, (UINT1 *) pArea, OSIX_FALSE);
        }
    }

    /* Generate the network LSA if the interface is DR */
    if (OSPFV3_IS_DR (pNbr->pInterface))
    {
        OSPFV3_BUFFER_DWTOPDU (linkStateId, pNbr->pInterface->u4InterfaceId);
        V3GenerateLsa (pNbr->pInterface->pArea, OSPFV3_NETWORK_LSA,
                       &linkStateId, (UINT1 *) pNbr->pInterface, OSIX_FALSE);
    }

    if (u1ExitReason != (UINT1) OSPFV3_HELPER_COMPLETED)
    {
        OSPFV3_EXT_TRC (OSPFV3_HELPER_TRC, u4ContextId,
                        "Unsuccessful Helper exit,Making the neighbor down \r\n");
        V3NsmDown (pNbr);
    }
    /* If the unsuccessful helper exit, Make the Restarting Neighbor down */

    /* Send trap notification regarding the status change.
     *  Nbr parameter is required for Neighbor status change
     *  when the router is acting as helper */
    O3SnmpSendStatusChgTrapInCxt (pV3OspfCxt, pNbr,
                                  OSPFV3_NBR_RST_STATUS_CHANGE_TRAP);

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: O3GrExitHelper\r\n");

    return OSIX_SUCCESS;
}
