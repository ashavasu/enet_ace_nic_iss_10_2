/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3sim.c,v 1.12 2018/02/07 09:32:29 siva Exp $
*
* Description: Test MIB Low Level Routines
*********************************************************************/
# include "o3inc.h"
# include "fso3telw.h"
# include "ospf3lw.h"
#include "fsmio3cli.h"
/* LOW LEVEL Routines for Table : FutOspfv3TestIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3TestIfTable
 Input       :  The Indices
                FutOspfv3TestIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3TestIfTable (INT4 i4FutOspfv3TestIfIndex)
{
    return (nmhValidateIndexInstanceOspfv3IfTable (i4FutOspfv3TestIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3TestIfTable
 Input       :  The Indices
                FutOspfv3TestIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3TestIfTable (INT4 *pi4FutOspfv3TestIfIndex)
{
    return (nmhGetFirstIndexOspfv3IfTable (pi4FutOspfv3TestIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3TestIfTable
 Input       :  The Indices
                FutOspfv3TestIfIndex
                nextFutOspfv3TestIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3TestIfTable (INT4 i4FutOspfv3TestIfIndex,
                                     INT4 *pi4NextFutOspfv3TestIfIndex)
{
    return (nmhGetNextIndexOspfv3IfTable (i4FutOspfv3TestIfIndex,
                                          pi4NextFutOspfv3TestIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3TestDemandTraffic
 Input       :  The Indices
                FutOspfv3TestIfIndex

                The Object 
                retValFutOspfv3TestDemandTraffic
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3TestDemandTraffic (INT4 i4FutOspfv3TestIfIndex,
                                  INT4 *pi4RetValFutOspfv3TestDemandTraffic)
{
    UNUSED_PARAM (i4FutOspfv3TestIfIndex);
    *pi4RetValFutOspfv3TestDemandTraffic = OSPFV3_DISABLED;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfv3TestDemandTraffic
 Input       :  The Indices
                FutOspfv3TestIfIndex

                The Object 
                setValFutOspfv3TestDemandTraffic
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3TestDemandTraffic (INT4 i4FutOspfv3TestIfIndex,
                                  INT4 i4SetValFutOspfv3TestDemandTraffic)
{
    if (i4SetValFutOspfv3TestDemandTraffic == OSPFV3_ENABLED)
    {
        V3DemandProbeOpen ((UINT4) i4FutOspfv3TestIfIndex);
    }
    else
    {
        V3DemandProbeClose ((UINT4) i4FutOspfv3TestIfIndex);
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3TestDemandTraffic
 Input       :  The Indices
                FutOspfv3TestIfIndex

                The Object 
                testValFutOspfv3TestDemandTraffic
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3TestDemandTraffic (UINT4 *pu4ErrorCode,
                                     INT4 i4FutOspfv3TestIfIndex,
                                     INT4 i4TestValFutOspfv3TestDemandTraffic)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FutOspfv3TestIfIndex);
    UNUSED_PARAM (i4TestValFutOspfv3TestDemandTraffic);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3TestIfTable
 Input       :  The Indices
                FutOspfv3TestIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3TestIfTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FutOspfv3ExtRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFutOspfv3ExtRouteTable
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFutOspfv3ExtRouteTable (INT4
                                                i4FutOspfv3ExtRouteDestType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFutOspfv3ExtRouteDest,
                                                UINT4
                                                u4FutOspfv3ExtRoutePfxLength,
                                                INT4
                                                i4FutOspfv3ExtRouteNextHopType,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pFutOspfv3ExtRouteNextHop)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                       (tIp6Addr *) (VOID *)
                                       pFutOspfv3ExtRouteDest->
                                       pu1_OctetList,
                                       (UINT1) u4FutOspfv3ExtRoutePfxLength);
    if (pExtRoute == NULL)
    {
        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    " nmhValidateIndexInstanceFutOspfv3ExtRouteTable:"
                    "Failure\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFutOspfv3ExtRouteTable
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFutOspfv3ExtRouteTable (INT4 *pi4FutOspfv3ExtRouteDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFutOspfv3ExtRouteDest,
                                        UINT4 *pu4FutOspfv3ExtRoutePfxLength,
                                        INT4 *pi4FutOspfv3ExtRouteNextHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFutOspfv3ExtRouteNextHop)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tInputParams        inParams;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    VOID               *pTempPtr = NULL;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;
    inParams.u1PrefixLen = 0;
    inParams.Key.pKey = NULL;

    if (TrieGetFirstNode (&inParams, &pTempPtr,
                          (VOID **) &(inParams.pLeafNode)) == TRIE_FAILURE)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "No External routes"
                    " are present in external routing table\n");
        return SNMP_FAILURE;
    }
    pExtRoute = (tV3OsExtRoute *) pTempPtr;

    MEMCPY (pFutOspfv3ExtRouteDest->pu1_OctetList, &pExtRoute->ip6Prefix,
            OSPFV3_IPV6_ADDR_LEN);
    *pu4FutOspfv3ExtRoutePfxLength = pExtRoute->u1PrefixLength;
    MEMCPY (pFutOspfv3ExtRouteNextHop->pu1_OctetList, &pExtRoute->fwdAddr,
            OSPFV3_IPV6_ADDR_LEN);
    *pi4FutOspfv3ExtRouteDestType = OSPFV3_INET_ADDR_TYPE;
    *pi4FutOspfv3ExtRouteNextHopType = OSPFV3_INET_ADDR_TYPE;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFutOspfv3ExtRouteTable
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                nextFutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                nextFutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                nextFutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                nextFutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop
                nextFutOspfv3ExtRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFutOspfv3ExtRouteTable (INT4 i4FutOspfv3ExtRouteDestType,
                                       INT4 *pi4NextFutOspfv3ExtRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3ExtRouteDest,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFutOspfv3ExtRouteDest,
                                       UINT4 u4FutOspfv3ExtRoutePfxLength,
                                       UINT4 *pu4NextFutOspfv3ExtRoutePfxLength,
                                       INT4 i4FutOspfv3ExtRouteNextHopType,
                                       INT4
                                       *pi4NextFutOspfv3ExtRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFutOspfv3ExtRouteNextHop,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextFutOspfv3ExtRouteNextHop)
{
    tV3OsExtRoute      *pTempExtRoute = NULL;
    tInputParams        inParams;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    VOID               *pTempPtr = NULL;
    UINT1               au1Key[OSPFV3_TRIE_KEY_SIZE];

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    inParams.pRoot = pV3OspfCxt->pExtRtRoot;
    inParams.i1AppId = OSPFV3_RT_ID;
    inParams.pLeafNode = NULL;

    MEMCPY (&(au1Key), pFutOspfv3ExtRouteDest,
            pFutOspfv3ExtRouteDest->i4_Length);
    au1Key[OSPFV3_IPV6_ADDR_LEN] = (UINT1) u4FutOspfv3ExtRoutePfxLength;
    inParams.u1PrefixLen = (UINT1) u4FutOspfv3ExtRoutePfxLength;
    inParams.Key.pKey = (UINT1 *) au1Key;

    if (TrieGetNextNode (&inParams, (VOID *) NULL,
                         &pTempPtr, (VOID **)
                         &(inParams.pLeafNode)) != TRIE_SUCCESS)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId, "Next External route"
                    " not present in external routing table\n");
        return SNMP_FAILURE;
    }
    pTempExtRoute = (tV3OsExtRoute *) pTempPtr;

    MEMCPY (pNextFutOspfv3ExtRouteDest->pu1_OctetList,
            &pTempExtRoute->ip6Prefix, OSPFV3_IPV6_ADDR_LEN);
    *pu4NextFutOspfv3ExtRoutePfxLength = pTempExtRoute->u1PrefixLength;
    MEMCPY (pNextFutOspfv3ExtRouteNextHop->pu1_OctetList,
            &pTempExtRoute->fwdAddr, OSPFV3_IPV6_ADDR_LEN);

    *pi4NextFutOspfv3ExtRouteDestType = OSPFV3_INET_ADDR_TYPE;
    *pi4NextFutOspfv3ExtRouteNextHopType = OSPFV3_INET_ADDR_TYPE;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFutOspfv3ExtRouteStatus
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object 
                retValFutOspfv3ExtRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ExtRouteStatus (INT4 i4FutOspfv3ExtRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3ExtRouteDest,
                               UINT4 u4FutOspfv3ExtRoutePfxLength,
                               INT4 i4FutOspfv3ExtRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3ExtRouteNextHop,
                               INT4 *pi4RetValFutOspfv3ExtRouteStatus)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                            pFutOspfv3ExtRouteDest->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3ExtRoutePfxLength)) !=
        NULL)
    {
        *pi4RetValFutOspfv3ExtRouteStatus = (INT4) pExtRoute->rowStatus;
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3ExtRouteMetric
 Input       :  The Indices                                                                                                                                                                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                retValFutOspfv3ExtRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ExtRouteMetric (INT4 i4FutOspfv3ExtRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3ExtRouteDest,
                               UINT4 u4FutOspfv3ExtRoutePfxLength,
                               INT4 i4FutOspfv3ExtRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3ExtRouteNextHop,
                               INT4 *pi4RetValFutOspfv3ExtRouteMetric)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                            pFutOspfv3ExtRouteDest->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3ExtRoutePfxLength)) !=
        NULL)
    {
        *pi4RetValFutOspfv3ExtRouteMetric = (INT4) pExtRoute->metric.u4Metric;
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFutOspfv3ExtRouteMetricType
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                retValFutOspfv3ExtRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFutOspfv3ExtRouteMetricType (INT4 i4FutOspfv3ExtRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3ExtRouteDest,
                                   UINT4 u4FutOspfv3ExtRoutePfxLength,
                                   INT4 i4FutOspfv3ExtRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3ExtRouteNextHop,
                                   INT4 *pi4RetValFutOspfv3ExtRouteMetricType)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt, (tIp6Addr *) (VOID *)
                                            pFutOspfv3ExtRouteDest->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3ExtRoutePfxLength)) !=
        NULL)
    {
        *pi4RetValFutOspfv3ExtRouteMetricType =
            (INT4) pExtRoute->metric.u4MetricType;
        return SNMP_SUCCESS;
    }
    CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFutOspfv3ExtRouteStatus
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object 
                setValFutOspfv3ExtRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3ExtRouteStatus (INT4 i4FutOspfv3ExtRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3ExtRouteDest,
                               UINT4 u4FutOspfv3ExtRoutePfxLength,
                               INT4 i4FutOspfv3ExtRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3ExtRouteNextHop,
                               INT4 i4SetValFutOspfv3ExtRouteStatus)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OsExtRoute       extRoute;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;
    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "FUNC:nmhSetFutOspfv3ExtRouteStatus ExtRouteDestIp6Addr: %s ExtRoutePfxLength: %d\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) pFutOspfv3ExtRouteDest->
                               pu1_OctetList), u4FutOspfv3ExtRoutePfxLength);

    switch ((UINT1) i4SetValFutOspfv3ExtRouteStatus)
    {
        case CREATE_AND_WAIT:
            MEMSET (&extRoute, 0, sizeof (tV3OsExtRoute));
            OSPFV3_IP6_ADDR_COPY (extRoute.ip6Prefix,
                                  *(pFutOspfv3ExtRouteDest->pu1_OctetList));
            extRoute.u1PrefixLength = (UINT1) u4FutOspfv3ExtRoutePfxLength;
            OSPFV3_IP6_ADDR_COPY (extRoute.fwdAddr,
                                  *(pFutOspfv3ExtRouteNextHop->pu1_OctetList));
            extRoute.u4FwdIfIndex = 0;
            extRoute.u2SrcProto = 3;
            extRoute.rowStatus = NOT_IN_SERVICE;
            extRoute.metric.u4Metric = 10;
            extRoute.metric.u4MetricType = OSPFV3_TYPE_2_METRIC;

            if ((pExtRoute = V3ExtrtAddInCxt (pV3OspfCxt, &extRoute)) == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetFutOspfv3ExtRouteStatus:"
                            "Route Addition to Import List Failed\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
                return SNMP_FAILURE;
            }

            break;

        case CREATE_AND_GO:
            MEMSET (&extRoute, 0, sizeof (tV3OsExtRoute));
            OSPFV3_IP6_ADDR_COPY (extRoute.ip6Prefix,
                                  *(pFutOspfv3ExtRouteDest->pu1_OctetList));
            extRoute.u1PrefixLength = (UINT1) u4FutOspfv3ExtRoutePfxLength;
            OSPFV3_IP6_ADDR_COPY (extRoute.fwdAddr,
                                  *(pFutOspfv3ExtRouteNextHop->pu1_OctetList));
            extRoute.u4FwdIfIndex = 0;
            extRoute.u2SrcProto = 3;
            extRoute.rowStatus = ACTIVE;
            extRoute.metric.u4Metric = 10;
            extRoute.metric.u4MetricType = OSPFV3_TYPE_2_METRIC;

            if ((pExtRoute = V3ExtrtAddInCxt (pV3OspfCxt, &extRoute)) == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetFutOspfv3ExtRouteStatus:"
                            "Route Addition to Import List Failed\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
                return SNMP_FAILURE;
            }

            V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
            break;
        case DESTROY:
            if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                                    (tIp6Addr *) (VOID *)
                                                    pFutOspfv3ExtRouteDest->
                                                    pu1_OctetList,
                                                    (UINT1)
                                                    u4FutOspfv3ExtRoutePfxLength))
                == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetFutOspfv3ExtRouteStatus:"
                            "Route Not Found\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }

            V3ExtrtDeleteInCxt (pV3OspfCxt, pExtRoute);
            break;

        case NOT_IN_SERVICE:
            if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                                    (tIp6Addr *) (VOID *)
                                                    pFutOspfv3ExtRouteDest->
                                                    pu1_OctetList,
                                                    (UINT1)
                                                    u4FutOspfv3ExtRoutePfxLength))
                == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetFutOspfv3ExtRouteStatus:"
                            "Route Not Found\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            pExtRoute->rowStatus = NOT_IN_SERVICE;
            break;
        case ACTIVE:

            if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                                    (tIp6Addr *) (VOID *)
                                                    pFutOspfv3ExtRouteDest->
                                                    pu1_OctetList,
                                                    (UINT1)
                                                    u4FutOspfv3ExtRoutePfxLength))
                == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "nmhSetFutOspfv3ExtRouteStatus:"
                            "Route Not found\n");
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            pExtRoute->rowStatus = ACTIVE;
            V3RagAddRouteInAggAddrRangeInCxt (pV3OspfCxt, pExtRoute);
            break;
        default:
            break;
            /* end switch */
    }
    RM_GET_SEQ_NUM (&u4SeqNum);
#ifdef SNMP_3_WANTED
    V3UtilMsrForfutOspfExtRouteTable
        ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3ExtRouteDestType,
         pFutOspfv3ExtRouteDest, u4FutOspfv3ExtRoutePfxLength,
         i4FutOspfv3ExtRouteNextHopType, pFutOspfv3ExtRouteNextHop,
         i4SetValFutOspfv3ExtRouteStatus,
         FsMIOspfv3ExtRouteStatus,
         (sizeof (FsMIOspfv3ExtRouteStatus) /
          sizeof (UINT4)), OSPFV3_TRUE, u4SeqNum, SNMP_SUCCESS);
#else
    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFutOspfv3ExtRouteMetric
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                setValFutOspfv3ExtRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3ExtRouteMetric (INT4 i4FutOspfv3ExtRouteDestType,
                               tSNMP_OCTET_STRING_TYPE * pFutOspfv3ExtRouteDest,
                               UINT4 u4FutOspfv3ExtRoutePfxLength,
                               INT4 i4FutOspfv3ExtRouteNextHopType,
                               tSNMP_OCTET_STRING_TYPE *
                               pFutOspfv3ExtRouteNextHop,
                               INT4 i4SetValFutOspfv3ExtRouteMetric)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "FUNC:nmhSetFutOspfv3ExtRouteMetric ExtRouteDestIp6Addr: %s ExtRoutePfxLength: %d\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) pFutOspfv3ExtRouteDest->
                               pu1_OctetList), u4FutOspfv3ExtRoutePfxLength);

    if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                            (tIp6Addr *) (VOID *)
                                            pFutOspfv3ExtRouteDest->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3ExtRoutePfxLength)) ==
        NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "nmhSetFutOspfv3ExtRouteMetric:" "Route Not Found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if (pExtRoute->metric.u4Metric == (UINT4) i4SetValFutOspfv3ExtRouteMetric)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "nmhSetFutOspfv3ExtRouteMetric:"
                    "No change in metric value\n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    pExtRoute->metric.u4Metric = (UINT4) i4SetValFutOspfv3ExtRouteMetric;
#ifdef SNMP_3_WANTED
    V3UtilMsrForfutOspfExtRouteTable
        ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3ExtRouteDestType,
         pFutOspfv3ExtRouteDest, u4FutOspfv3ExtRoutePfxLength,
         i4FutOspfv3ExtRouteNextHopType, pFutOspfv3ExtRouteNextHop,
         i4SetValFutOspfv3ExtRouteMetric,
         FsMIOspfv3ExtRouteMetric,
         (sizeof (FsMIOspfv3ExtRouteMetric) / sizeof (UINT4)),
         OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#else
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
#endif
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFutOspfv3ExtRouteMetricType
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                setValFutOspfv3ExtRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFutOspfv3ExtRouteMetricType (INT4 i4FutOspfv3ExtRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3ExtRouteDest,
                                   UINT4 u4FutOspfv3ExtRoutePfxLength,
                                   INT4 i4FutOspfv3ExtRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFutOspfv3ExtRouteNextHop,
                                   INT4 i4SetValFutOspfv3ExtRouteMetricType)
{

    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;
    UINT4               u4SeqNum = 0;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    OSPFV3_TRC2 (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                 "FUNC:nmhSetFutOspfv3ExtRouteMetricType ExtRouteDestIp6Addr: %s ExtRoutePfxLength: %d\n",
                 Ip6PrintAddr ((tIp6Addr *) (VOID *) pFutOspfv3ExtRouteDest->
                               pu1_OctetList), u4FutOspfv3ExtRoutePfxLength);

    if ((pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                            (tIp6Addr *) (VOID *)
                                            pFutOspfv3ExtRouteDest->
                                            pu1_OctetList,
                                            (UINT1)
                                            u4FutOspfv3ExtRoutePfxLength)) ==
        NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "nmhSetFutOspfv3ExtRouteMetricType:" "Route Not Found\n");
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }
    if (pExtRoute->metric.u4MetricType ==
        (UINT4) i4SetValFutOspfv3ExtRouteMetricType)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "nmhSetFutOspfv3ExtRouteMetricType:"
                    "No change in metric value\n");
        return SNMP_SUCCESS;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    pExtRoute->metric.u4MetricType =
        (UINT4) i4SetValFutOspfv3ExtRouteMetricType;
#ifdef SNMP_3_WANTED
    V3UtilMsrForfutOspfExtRouteTable
        ((INT4) pV3OspfCxt->u4ContextId, i4FutOspfv3ExtRouteDestType,
         pFutOspfv3ExtRouteDest, u4FutOspfv3ExtRoutePfxLength,
         i4FutOspfv3ExtRouteNextHopType, pFutOspfv3ExtRouteNextHop,
         i4SetValFutOspfv3ExtRouteMetricType,
         FsMIOspfv3ExtRouteMetricType,
         (sizeof (FsMIOspfv3ExtRouteMetricType) / sizeof (UINT4)),
         OSPFV3_FALSE, u4SeqNum, SNMP_SUCCESS);
#else
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ExtRouteStatus
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object 
                testValFutOspfv3ExtRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ExtRouteStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4FutOspfv3ExtRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3ExtRouteDest,
                                  UINT4 u4FutOspfv3ExtRoutePfxLength,
                                  INT4 i4FutOspfv3ExtRouteNextHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3ExtRouteNextHop,
                                  INT4 i4TestValFutOspfv3ExtRouteStatus)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4FutOspfv3ExtRouteDestType);
    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    UNUSED_PARAM (i4FutOspfv3ExtRouteNextHopType);

    if (i4TestValFutOspfv3ExtRouteStatus == DESTROY)
    {
        return SNMP_SUCCESS;
    }

    switch ((UINT1) i4TestValFutOspfv3ExtRouteStatus)
    {
        case CREATE_AND_GO:
        case CREATE_AND_WAIT:
            pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3ExtRouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               u4FutOspfv3ExtRoutePfxLength);
            if (pExtRoute != NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For FutOspfv3ExtRouteStatus Failed\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_ENTRY_EXISTS);
                return SNMP_FAILURE;
            }
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For FutOspfv3ExtRouteStatus Success\n");
            return SNMP_SUCCESS;
        case ACTIVE:
        case NOT_IN_SERVICE:
            pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                               (tIp6Addr *) (VOID *)
                                               pFutOspfv3ExtRouteDest->
                                               pu1_OctetList,
                                               (UINT1)
                                               u4FutOspfv3ExtRoutePfxLength);
            if (pExtRoute == NULL)
            {
                OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                            pV3OspfCxt->u4ContextId,
                            "Test For FutOspfv3ExtRouteStatus Failed\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
                return SNMP_FAILURE;
            }
            OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                        pV3OspfCxt->u4ContextId,
                        "Test For FutOspfv3ExtRouteStatus Success\n");
            return SNMP_SUCCESS;
        default:
            break;
            /* end switch */
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For FutOspfv3ExtRouteStatus Failed\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ExtRouteMetric
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                testValFutOspfv3ExtRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ExtRouteMetric (UINT4 *pu4ErrorCode,
                                  INT4 i4FutOspfv3ExtRouteDestType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3ExtRouteDest,
                                  UINT4 u4FutOspfv3ExtRoutePfxLength,
                                  INT4 i4FutOspfv3ExtRouteNextHopType,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFutOspfv3ExtRouteNextHop,
                                  INT4 i4TestValFutOspfv3ExtRouteMetric)
{

    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4FutOspfv3ExtRouteDestType != OSPFV3_INET_ADDR_TYPE) ||
        (i4FutOspfv3ExtRouteNextHopType != OSPFV3_INET_ADDR_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    if (OSPFV3_IS_VALID_BIG_METRIC (i4TestValFutOspfv3ExtRouteMetric) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                       (tIp6Addr *) (VOID *)
                                       pFutOspfv3ExtRouteDest->
                                       pu1_OctetList,
                                       (UINT1) u4FutOspfv3ExtRoutePfxLength);
    if (pExtRoute == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For FutOspfv3ExtRouteMetric Failed\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For FutOspfv3ExtRouteMetric Success\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FutOspfv3ExtRouteMetricType
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop

                The Object
                testValFutOspfv3ExtRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FutOspfv3ExtRouteMetricType (UINT4 *pu4ErrorCode,
                                      INT4 i4FutOspfv3ExtRouteDestType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3ExtRouteDest,
                                      UINT4 u4FutOspfv3ExtRoutePfxLength,
                                      INT4 i4FutOspfv3ExtRouteNextHopType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFutOspfv3ExtRouteNextHop,
                                      INT4 i4TestValFutOspfv3ExtRouteMetricType)
{
    tV3OsExtRoute      *pExtRoute = NULL;
    tV3OspfCxt         *pV3OspfCxt = gV3OsRtr.pV3OspfCxt;

    UNUSED_PARAM (pFutOspfv3ExtRouteNextHop);
    if (pV3OspfCxt == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4FutOspfv3ExtRouteDestType != OSPFV3_INET_ADDR_TYPE) ||
        (i4FutOspfv3ExtRouteNextHopType != OSPFV3_INET_ADDR_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
    if (OSPFV3_IS_VALID_EXT_METRIC_TYPE
        (i4TestValFutOspfv3ExtRouteMetricType) == 0)

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_VALUE);
        return SNMP_FAILURE;
    }
    pExtRoute = V3ExtrtFindRouteInCxt (pV3OspfCxt,
                                       (tIp6Addr *) (VOID *)
                                       pFutOspfv3ExtRouteDest->
                                       pu1_OctetList,
                                       (UINT1) u4FutOspfv3ExtRoutePfxLength);
    if (pExtRoute == NULL)
    {
        OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                    pV3OspfCxt->u4ContextId,
                    "Test For FutOspfv3ExtRouteMetricType Failed\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (OSPFV3_CLI_INV_ENTRY);
        return SNMP_FAILURE;
    }
    OSPFV3_TRC (MGMT_TRC | OSPFV3_CONFIGURATION_TRC,
                pV3OspfCxt->u4ContextId,
                "Test For FutOspfv3ExtRouteMetricType Success\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FutOspfv3ExtRouteTable
 Input       :  The Indices
                FutOspfv3ExtRouteDestType
                FutOspfv3ExtRouteDest
                FutOspfv3ExtRoutePfxLength
                FutOspfv3ExtRouteNextHopType
                FutOspfv3ExtRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FutOspfv3ExtRouteTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
