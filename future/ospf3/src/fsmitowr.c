/********************************************************************
 *     Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *   
 *      $Id: fsmitowr.c,v 1.2 2017/12/26 13:34:26 siva Exp $
 *     
 *          Description:
 *
 ********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmitolw.h"
# include  "fsmitowr.h"
# include  "fsmitodb.h"
# include  "o3inc.h"

INT4
GetNextIndexFsMIOspfv3TestIfTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3TestIfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3TestIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSMITO ()
{
    SNMPRegisterMibWithLock (&fsmitoOID, &fsmitoEntry, V3OspfLock, V3OspfUnLock,
                             SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsmitoOID, (const UINT1 *) "fsmitos3");
}

VOID
UnRegisterFSMITO ()
{
    SNMPUnRegisterMib (&fsmitoOID, &fsmitoEntry);
    SNMPDelSysorEntry (&fsmitoOID, (const UINT1 *) "fsmitos3");
}

INT4
FsMIOspfv3TestDemandTrafficGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3TestIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3TestDemandTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3TestIfContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3TestIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3TestIfContextId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3TestDemandTrafficSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3TestDemandTraffic
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3TestDemandTrafficTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3TestDemandTraffic (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3TestIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3TestIfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMIOspfv3ExtRouteTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMIOspfv3ExtRouteTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMIOspfv3ExtRouteTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMIOspfv3ExtRouteMetricGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3ExtRouteMetric
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3ExtRouteMetricTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3ExtRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3ExtRouteStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMIOspfv3ExtRouteTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMIOspfv3ExtRouteStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMIOspfv3ExtRouteMetricSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3ExtRouteMetric
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteMetricTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3ExtRouteMetricType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMIOspfv3ExtRouteStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteMetricTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3ExtRouteMetric (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[5].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteMetricTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3ExtRouteMetricType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[4].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[5].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMIOspfv3ExtRouteStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[4].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[5].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMIOspfv3ExtRouteTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMIOspfv3ExtRouteTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
