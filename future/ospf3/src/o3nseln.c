/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3nseln.c,v 1.9 2017/12/26 13:34:28 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             NSSA elction process.
 *
 *******************************************************************/
#include "o3inc.h"

/************************************************************************/
/*                                                                      */
/* Function     : V3AreaNssaFsmTrnsltrRlChng                            */
/*                                                                      */
/* Description  : This handles the event NSSA Trnsltr RL CHNG           */
/*                Depending on NSSA State it triggers NSSA Trnsltr Eln  */
/*                                                                      */
/* Input        : pArea - Pointer to area                               */
/*                                                                      */
/* Output       : None                                                  */
/*                                                                      */
/* Returns      : VOID                                                  */
/*                                                                      */
/************************************************************************/
PUBLIC VOID
V3AreaNssaTrnsltrRlChng (tV3OsArea * pArea)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaTrnsltrRlChng\n");

    if (pArea->pV3OspfCxt->u1Ospfv3RestartState != OSPFV3_GR_NONE)
    {
        if (pArea->u1NssaTrnsltrRole == OSPFV3_TRNSLTR_ROLE_ALWAYS)
        {
            pArea->u1NssaTrnsltrState = OSPFV3_TRNSLTR_STATE_ENAELCTD;
        }
        /* Translation election and LSA orignation will not be done during GR */
        return;
    }

    if (pArea->u1NssaTrnsltrRole == OSPFV3_TRNSLTR_ROLE_ALWAYS)
    {
        V3AreaNssaFsmUpdtTrnsltrState (pArea,
                                       pArea->u1NssaTrnsltrState,
                                       OSPFV3_TRNSLTR_STATE_ENAELCTD);
    }
    else
    {
        V3AreaNssaFsmFindTranslator (pArea);
    }

    if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
    {
        V3GenLowestLsIdRtrLsa (pArea);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaTrnsltrRlChng \n");
    return;
}

/******************************************************************/
/*                                                                */
/* Function     : V3AreaNssaFsmTrgrTrnsltrElnInCxt                */
/*                                                                */
/* Description  : This routine scans each NSSA area and triggers  */
/*                NSSA Trnsltr Eln for that area                  */
/*                                                                */
/* Input        : pV3OspfCxt   -  Context pointer                 */
/*                                                                */
/* Output       : None                                            */
/*                                                                */
/* Returns      : VOID                                            */
/*                                                                */
/******************************************************************/
PUBLIC VOID
V3AreaNssaFsmTrgrTrnsltrElnInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaFsmTrgrTrnsltrEln\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if ((pArea->u4AreaType == OSPFV3_NSSA_AREA)
            && (TMO_SLL_Count (&(pArea->ifsInArea)) != 0))
        {
            V3AreaNssaFsmFindTranslator (pArea);
        }
        else
        {
            continue;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaFsmTrgrTrnsltrEln\n");
}

/****************************************************************/
/*                                                              */
/* Function     : V3AreaNssaFsmFindTranslator                   */
/*                                                              */
/* Description  : This routine determines new translator        */
/*                                                              */
/* Input        : pArea - Pointer to area                       */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3AreaNssaFsmFindTranslator (tV3OsArea * pArea)
{

    UINT1               u1ActNbrs = 0;
    tV3OsRtEntry       *pRtEntry = NULL;
    tV3OsArea          *pScanArea = NULL;
    UINT1               u1NotTrnsltr = OSIX_FALSE;
    UINT1               u1PrevTrnsltrState = OSPFV3_TRNSLTR_STATE_DISABLED;
    tV3OsPath          *pDestRtPath = NULL;
    tV3OsRtEntry       *pAbrRtEntry = NULL;
    tV3OsRtEntry       *pTmpAbrRtEntry = NULL;
    UINT1              *pTmpLsa = NULL;

    UINT4               u4CurrentTime = 0;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaFsmFindTranslator\n");

    if ((pArea->u4AreaType != OSPFV3_NSSA_AREA) ||
        (!OSPFV3_IS_AREA_BORDER_RTR (pArea->pV3OspfCxt)))
    {
        OSPFV3_TRC (OSPFV3_NSSA_TRC, pArea->pV3OspfCxt->u4ContextId,
                    "Rtr not ABR or AREA not NSSA \n");
        return;
    }

    u1PrevTrnsltrState = pArea->u1NssaTrnsltrState;

    switch (pArea->u1NssaTrnsltrRole)
    {

        case OSPFV3_TRNSLTR_ROLE_ALWAYS:
            V3AreaNssaFsmUpdtTrnsltrState (pArea, pArea->u1NssaTrnsltrState,
                                           OSPFV3_TRNSLTR_STATE_ENAELCTD);

            if (u1PrevTrnsltrState == OSPFV3_TRNSLTR_STATE_DISABLED)
            {
                if (pArea->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                {
                    V3RtcSetRtTimerInCxt (pArea->pV3OspfCxt);
                }
            }
            OSPFV3_TRC (OSPFV3_NSSA_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Rtr Conf Trnsltr - ALWAYS \n");
            break;

        case OSPFV3_TRNSLTR_ROLE_CANDIDATE:
            if (pArea->u4AreaBdrRtrCount == u1ActNbrs)
            {
                V3AreaNssaFsmUpdtTrnsltrState (pArea,
                                               pArea->u1NssaTrnsltrState,
                                               OSPFV3_TRNSLTR_STATE_ENAELCTD);

                if (u1PrevTrnsltrState == OSPFV3_TRNSLTR_STATE_DISABLED)
                {
                    if (pArea->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                    {
                        V3RtcSetRtTimerInCxt (pArea->pV3OspfCxt);
                    }
                }

                OSPFV3_TRC (OSPFV3_NSSA_TRC, pArea->pV3OspfCxt->u4ContextId,
                            "No active ABR in area, so elected Trnsltr \n");
                break;
            }

            TMO_DYN_SLL_Scan (&(pArea->pV3OspfCxt->ospfV3RtTable.routesList),
                              pAbrRtEntry, pTmpAbrRtEntry, tV3OsRtEntry *)
            {
                if (pAbrRtEntry->u1DestType != OSPFV3_DEST_AREA_BORDER)
                {
                    continue;
                }
                pDestRtPath = V3RtcFindPath (pAbrRtEntry, &pArea->areaId);

                if (pDestRtPath == NULL)
                {
                    continue;
                }

                pTmpLsa = pDestRtPath->pLsaInfo->pLsa;
                if ((OSPFV3_IS_NTBIT_SET_RTR_LSA
                     (*(pTmpLsa + OSPFV3_LS_HEADER_SIZE)))
                    ||
                    (V3UtilRtrIdComp
                     ((pArea->pV3OspfCxt->rtrId),
                      (pDestRtPath->pLsaInfo->lsaId.advRtrId)) == OSPFV3_LESS))
                {
                    /* There exist an ABR with either "Nt" bit set OR of    
                     * higher Rtr Id Check if this ABR is reachable as    
                     * ASBR in other area
                     */
                    pRtEntry = V3RtcFindRtEntryInCxt
                        (pArea->pV3OspfCxt,
                         (VOID *) &(pDestRtPath->pLsaInfo->lsaId.
                                    advRtrId), 0, OSPFV3_DEST_AS_BOUNDARY);

                    if (pRtEntry != NULL)
                    {
                        TMO_SLL_Scan (&(pArea->pV3OspfCxt->areasLst), pScanArea,
                                      tV3OsArea *)
                        {
                            if (V3UtilRtrIdComp (pScanArea->areaId,
                                                 pArea->areaId) != OSPFV3_EQUAL)
                            {
                                pDestRtPath =
                                    V3RtcFindPath (pRtEntry,
                                                   &pScanArea->areaId);

                                if (pDestRtPath != NULL)
                                {
                                    u1NotTrnsltr = OSIX_TRUE;
                                }
                            }
                        }
                    }
                }
                if (OSPFV3_STAGGERING_ENABLED == gV3OsRtr.u4RTStaggeringStatus)
                {
                    u4CurrentTime = OsixGetSysUpTime ();
                    /* current time greater than relinquished interval */
                    if (u4CurrentTime >= pArea->pV3OspfCxt->u4StaggeringDelta)
                    {
                        OSPF3RtcRelinquishInCxt (pArea->pV3OspfCxt);
                    }

                }
            }                    /* Scan */

            if (u1NotTrnsltr == OSIX_FALSE)
            {
                V3AreaNssaFsmUpdtTrnsltrState (pArea,
                                               pArea->u1NssaTrnsltrState,
                                               OSPFV3_TRNSLTR_STATE_ENAELCTD);

                if (u1PrevTrnsltrState == OSPFV3_TRNSLTR_STATE_DISABLED)
                {
                    if (pArea->pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
                    {
                        V3RtcSetRtTimerInCxt (pArea->pV3OspfCxt);
                    }
                }
            }
            else if ((u1NotTrnsltr == OSIX_TRUE) &&
                     (u1PrevTrnsltrState == OSPFV3_TRNSLTR_STATE_ENAELCTD))
            {
                V3TmrSetTimer (&(pArea->nssaStbltyIntrvlTmr),
                               OSPFV3_NSSA_STBLTY_INTERVAL_TIMER,
                               (OSPFV3_NO_OF_TICKS_PER_SEC *
                                pArea->u4NssaTrnsltrStbltyInterval));

                pArea->nssaStbltyIntrvlTmr.i2Status = OSIX_TRUE;
            }
            break;

        default:
            OSPFV3_TRC (OSPFV3_NSSA_TRC, pArea->pV3OspfCxt->u4ContextId,
                        "Error value in u1TrnsltrRole\n");
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaFsmFindTranslator\n");
}

/****************************************************************/
/*                                                              */
/* Function     : V3AreaNssaABRStatLostInCxt                    */
/*                                                              */
/* Description  : This routine scans through NSSA areas and     */
/*                if the router is NSSA Trnsltr for area, it    */
/*                invokes NssaTrnsltrStatLost                   */
/*                                                              */
/* Input        : pV3OspfCxt  -   Context pointer               */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3AreaNssaABRStatLostInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsArea          *pArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaABRStatLost\n");

    TMO_SLL_Scan (&(pV3OspfCxt->areasLst), pArea, tV3OsArea *)
    {
        if (pArea->u4AreaType == OSPFV3_NSSA_AREA)
        {
            OSPFV3_TRC1 (OSPFV3_NSSA_TRC, pV3OspfCxt->u4ContextId,
                         "Lost ABR Stat but was Trnsltr in area %x\n",
                         pArea->areaId);

            V3AreaNssaFsmUpdtTrnsltrState (pArea,
                                           pArea->u1NssaTrnsltrState,
                                           OSPFV3_TRNSLTR_STATE_DISABLED);
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaABRStatLost\n");
}

/****************************************************************/
/*                                                              */
/* Function     : V3AreaNssaTrnsltrStatLost                     */
/*                                                              */
/* Description  : This routine flushes self originated          */
/*                aggregated Type 5 LSAs, generated from        */
/*                configured Type 7 Address Range               */
/*                                                              */
/* Input        : pArea - Pointer to the area                   */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3AreaNssaTrnsltrStatLost (tV3OsArea * pArea)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsAddrRange     *pAddrRange = NULL;
    tV3OsExtRoute      *pExtRoute = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaTrnsltrStatLost\n");

    /* Function is invoked when NSSA ABR looses 
       Translator Status either when it is deposed
       by other NSSA ABR or when it looses its 
       ABR status itself.
       It flushes self-originated Type 5 
       as a result of Type 7 aggregation.
       Further it re-originates imported routes if
       they were previously screened by Type 7 
       range translation.
     */

    TMO_SLL_Scan (&(pArea->type7AggrLst), pAddrRange, tV3OsAddrRange *)
    {
        if (pAddrRange->areaAggStatus == ACTIVE)
        {
            pLsaInfo =
                V3LsuSearchDatabase (OSPFV3_AS_EXT_LSA,
                                     &(pAddrRange->linkStateId),
                                     &(pArea->pV3OspfCxt->rtrId), NULL, pArea);

            if ((pLsaInfo != NULL) &&
                (pLsaInfo->pLsaDesc->u2InternalLsaType ==
                 OSPFV3_AS_TRNSLTD_RNG_LSA))
            {
                V3AgdFlushOut (pLsaInfo);
                pExtRoute = V3ExtrtFindRouteInCxt (pArea->pV3OspfCxt,
                                                   &pAddrRange->ip6Addr,
                                                   pAddrRange->u1PrefixLength);
                if (pExtRoute != NULL)
                {
                    V3RagAddRouteInAggAddrRangeInCxt (pArea->pV3OspfCxt,
                                                      pExtRoute);
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaTrnsltrStatLost\n");
}

/****************************************************************/
/*                                                              */
/* Function     : V3AreaNssaFsmUpdtTrnsltrState                 */
/*                                                              */
/* Description  : Updates Trnsltr state and takes               */
/*                necessary action                              */
/*                                                              */
/*                                                              */
/* Input        : pArea - Pointer to the area                   */
/*                u1PrvState - Previous State                   */
/*                u1NewState - New State                        */
/*                                                              */
/* Output       : None                                          */
/*                                                              */
/* Returns      : VOID                                          */
/*                                                              */
/****************************************************************/
PUBLIC VOID
V3AreaNssaFsmUpdtTrnsltrState (tV3OsArea * pArea,
                               UINT1 u1PrvState, UINT1 u1NewState)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3AreaNssaFsmUpdtTrnsltrState\n");
    pArea->u1NssaTrnsltrState = u1NewState;

    if ((u1NewState == OSPFV3_TRNSLTR_STATE_ENAELCTD) &&
        (u1PrvState == OSPFV3_TRNSLTR_STATE_ENAELCTD))
    {
        if (pArea->nssaStbltyIntrvlTmr.i2Status == OSIX_TRUE)
        {
            pArea->nssaStbltyIntrvlTmr.i2Status = OSIX_FALSE;
            V3TmrDeleteTimer (&pArea->nssaStbltyIntrvlTmr);
        }
    }
    else if ((u1PrvState != OSPFV3_TRNSLTR_STATE_DISABLED) &&
             (u1NewState == OSPFV3_TRNSLTR_STATE_DISABLED))
    {
        if (pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
        {
            V3AreaNssaTrnsltrStatLost (pArea);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3AreaNssaFsmUpdtTrnsltrState\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  v3nseln.c                      */
/*-----------------------------------------------------------------------*/
