/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * * $Id: o3nsm.c,v 1.12 2017/12/26 13:34:28 siva Exp $
 *
 * Description:This file contains procedures related to the 
 *             neighbor state machine.
 *
 *******************************************************************/

#include "o3inc.h"

#include "o3nsm.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmRunNsm                                                */
/*                                                                           */
/* Description  : It is called whenever a neighbor state change event occurs.*/
/*                The state event table is maintained as a two-dimensional   */
/*                array of integers. Using the event and the state of the    */
/*                neighbor as indices to this array, an integer is obtained. */
/*                This integer is used as an index to an array of function   */
/*                pointers to obtain the actual action function to be        */
/*                executed.                                                  */
/*                                                                           */
/* Input        : pNbr             : Pointer to the interface whose state    */
/*                                   is to be changed                        */
/*                u1NbrEvent       : The event that has occurred             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmRunNsm (tV3OsNeighbor * pNbr, UINT1 u1NbrEvent)
{
    UINT4               u4ContextId;

    u4ContextId = pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId, "ENTER : V3NsmRunNsm\n");

    if ((pNbr->u1NsmState >= OSPFV3_MAX_NBR_STATE) ||
        (u1NbrEvent >= OSPFV3_MAX_NBR_EVENT) ||
        (pNbr->pInterface->u1NetworkType >= OSPFV3_MAX_IF_TYPE))
    {
        return;
    }
    if (u1NbrEvent == OSPFV3_NBRE_HELLO_RCVD)
    {
        pNbr->u4LastHelloRcvd = OsixGetSysUpTime ();
    }

    OSPFV3_TRC4 (CONTROL_PLANE_TRC | OSPFV3_NSM_TRC,
                 u4ContextId,
                 "NSM Nbr %x N/W Type %s Current State: %s Evt: %s\n",
                 OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                 gau1Os3DbgIfType[pNbr->pInterface->u1NetworkType],
                 gau1Os3DbgNbrState[pNbr->u1NsmState],
                 gau1Os3DbgNbrEvent[u1NbrEvent]);

    /* SEM Function is executed now */
    (*gau1Os3NsmFunc[gau1Os3NsmTable[u1NbrEvent][pNbr->u1NsmState]]) (pNbr);

    if (pNbr->u1NsmState < OSPFV3_MAX_NBR_STATE)
    {
        OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_NSM_TRC,
                     u4ContextId, "NSM Nbr %x Next State: %s\n",
                     OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId),
                     gau1Os3DbgNbrState[pNbr->u1NsmState]);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: V3NsmRunNsm\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmInvalid                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                This procedure  is invoked when an invalid event occurs.   */
/*                This procedure generates an appropriate error message.     */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmInvalid (tV3OsNeighbor * pNbr)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmInvalid\n");

    OSPFV3_COUNTER_OP (pNbr->u4NbrEvents, 1);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmInvalid\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmLlDown                                                */
/*                                                                           */
/* Description  : Reference : RFC-1793 section 3.2.2                         */
/*                The link state request list, link state retransmission list*/
/*                and the database summary list are cleared and the          */
/*                inactivity timer is disabled.                              */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed.                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmLlDown (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmLlDown\n");

    OSPFV3_SET_NULL_RTR_ID (pNbr->desgRtr);
    OSPFV3_SET_NULL_RTR_ID (pNbr->backupDesgRtr);

    V3NsmResetVariables (pNbr);
    V3TmrDeleteTimer (&(pNbr->inactivityTimer));

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface) &&
        (pNbr->u1NsmState >= OSPFV3_NBRS_INIT) &&
        (pNbr->pInterface->u1IsmState != OSPFV3_IFS_DOWN))
    {
        OSPFV3_GENERATE_IF_EVENT (pNbr->pInterface, OSPFV3_IFE_DOWN);
    }

    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);

    if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pNbr->pInterface))
    {
        V3TmrRestartTimer (&(pNbr->pInterface->pollTimer), OSPFV3_POLL_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC *
                           (pNbr->pInterface->u4PollInterval));
    }

    OSPFV3_TRC (OSPFV3_NSM_TRC,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "Nbr Brought DN\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmLlDown\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmDown                                                  */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                The LS retransmission list, the LS request list, and the   */
/*                database summary list are cleared and the next state is    */
/*                set to down.                                               */
/*                                                                           */
/* Input        : pNbr           : Pointer to the neighbor whose state is    */
/*                                 to be changed                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmDown (tV3OsNeighbor * pNbr)
{
    UINT4               u4ContextId;

    u4ContextId = pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId;
    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId, "ENTER : V3NsmDown\n");

    OSPFV3_SET_NULL_RTR_ID (pNbr->desgRtr);
    OSPFV3_SET_NULL_RTR_ID (pNbr->backupDesgRtr);

    V3NsmResetVariables (pNbr);
    V3TmrDeleteTimer (&(pNbr->inactivityTimer));
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
    if (pNbr->u1ConfigStatus != OSPFV3_CONFIGURED_NBR)
    {
        pNbr->bIsBfdDisable = OSIX_TRUE;
        V3NbrDelete (pNbr);
    }

    OSPFV3_TRC (OSPFV3_NSM_TRC, u4ContextId, "Nbr Deleted\n");
    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT: V3NsmDown\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3Nsm1wayRcvd                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                The LS retransmission list, the LS request list and the    */
/*                database summary list are cleared and the next state       */
/*                is init.                                                   */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state      */
/*                                  is to be changed                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3Nsm1wayRcvd (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3Nsm1wayRcvd\n");

    OSPFV3_SET_NULL_RTR_ID (pNbr->desgRtr);
    OSPFV3_SET_NULL_RTR_ID (pNbr->backupDesgRtr);

    V3NsmResetVariables (pNbr);
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_INIT);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3Nsm1wayRcvd\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmRestartAdj                                            */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                The LS retransmission list, the LS request list and the    */
/*                database summary list are cleared. The sequence number     */
/*                for this neighbor is initialized. The process of sending   */
/*                empty database description packets with I, M, MS bits set  */
/*                is initiated.                                              */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state      */
/*                                  is to be changed.                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmRestartAdj (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmRestartAdj\n");

    V3NsmResetVariables (pNbr);
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_EXSTART);
    OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
    pNbr->dbSummary.bMaster = OSIX_TRUE;
    V3DdpSendDdp (pNbr);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmRestartAdj\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmCheckAdj                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                Determines whether the neighboring router is adjacent.     */
/*                If yes, no state change occurs and no action is taken.     */
/*                Otherwise next state is 2-way and LS retransmission list,  */
/*                LS request list and database summary list are cleared.     */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmCheckAdj (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmCheckAdj\n");
    if (V3NsmToBecomeAdj (pNbr) == OSIX_FALSE)
    {

        V3NbrUpdateState (pNbr, OSPFV3_NBRS_2WAY);
        V3NsmResetVariables (pNbr);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmCheckAdj\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmProcessAdj                                            */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                Determines whether the neighbor is adjacent. If yes, the   */
/*                next state is EXSTART. The sequence number for this        */
/*                neighbor is initialized and initiates the process to       */
/*                send empty DDP with I, M, MS bits set. Otherwise no state  */
/*                change occurs.                                             */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmProcessAdj (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmProcessAdj\n");

    if (V3NsmToBecomeAdj (pNbr) == OSIX_TRUE)
    {

        V3NbrUpdateState (pNbr, OSPFV3_NBRS_EXSTART);
        OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
        pNbr->dbSummary.bMaster = OSIX_TRUE;
        V3DdpSendDdp (pNbr);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmProcessAdj\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmExchgDone                                             */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                If the LS request list is empty, then the next state is    */
/*                full. Otherwise, the next state is loading and it          */
/*                initiates a process to send LS request packets.            */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmExchgDone (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmExchgDone\n");

    if (TMO_SLL_Count (&(pNbr->lsaReqDesc.lsaReqLst)) == 0)
    {
        V3NbrUpdateState (pNbr, OSPFV3_NBRS_FULL);
        if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
            OSPFV3_GR_RESTART)
        {
            O3GrChkAndExitGracefulRestart (pNbr->pInterface->pArea->pV3OspfCxt);
        }
    }
    else
    {

        V3NbrUpdateState (pNbr, OSPFV3_NBRS_LOADING);
        if (pNbr->lsaReqDesc.bLsaReqOutstanding == OSIX_FALSE)
        {
            V3LrqSendLsaReq (pNbr);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmExchgDone\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmNegDone                                               */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                It creates the database summary list. Next state is        */
/*                exchange. It initiates process to send DDP.                */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmNegDone (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmNegDone\n");

    if (V3DdpBuildSummary (pNbr) == OSIX_TRUE)
    {
        /* printf ("\n\n V3NsmNegDone \n"); */
        V3NbrUpdateState (pNbr, OSPFV3_NBRS_EXCHANGE);
    }
    else
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Unable to allocate memory to database summary list\n"));

        OSPFV3_TRC (OSPFV3_NSM_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "NegDone failed due to  dd_summary_list alloc failure\n");

    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmNegDone\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3Nsm2wayRcvd                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                It determines whether the neighbor is adjacent. If yes, it */
/*                transitions to state ExStart, sets sequence number value   */
/*                and initiates process to send empty DDP with I, M, MS bits */
/*                set. Otherwise, the new state is 2way.                     */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3Nsm2wayRcvd (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3Nsm2wayRcvd\n");

    if (V3NsmToBecomeAdj (pNbr) == OSIX_TRUE)
    {

        V3NbrUpdateState (pNbr, OSPFV3_NBRS_EXSTART);
        if (pNbr->dbSummary.seqNum == 0)
        {
            OsixGetSysTime ((tOsixSysTime *) & (pNbr->dbSummary.seqNum));
        }
        else
        {
            pNbr->dbSummary.seqNum++;
        }
        pNbr->dbSummary.bMaster = OSIX_TRUE;
        V3DdpSendDdp (pNbr);
    }
    else
    {
        V3NbrUpdateState (pNbr, OSPFV3_NBRS_2WAY);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3Nsm2wayRcvd\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmStart                                                 */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                It sends hello packet to the neighbor and starts           */
/*                inactivity timer. Next state is Attempt.                   */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmStart (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmStart\n");

    /* At the time of Restart, neighbors must be learned only through
     * the received hello packet */
    if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState !=
        OSPFV3_GR_RESTART)
    {
        V3HpSendHello (pNbr->pInterface, pNbr, OSPFV3_HELLO_TIMER);
        pNbr->u4LastHelloRcvd = OsixGetSysUpTime ();
        V3TmrSetTimer (&(pNbr->inactivityTimer), OSPFV3_INACTIVITY_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pNbr->pInterface->u2RtrDeadInterval));
        V3NbrUpdateState (pNbr, OSPFV3_NBRS_ATTEMPT);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmStart\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmStartInactTimer                                       */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                This procedure starts the inactivity timer for the         */
/*                neighbor. The next state is init.                          */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmStartInactTimer (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmStartInactTimer\n");

    V3TmrSetTimer (&(pNbr->inactivityTimer), OSPFV3_INACTIVITY_TIMER,
                   OSPFV3_NO_OF_TICKS_PER_SEC *
                   (pNbr->pInterface->u2RtrDeadInterval));
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_INIT);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmStartInactTimer\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmRestartInactTimer                                     */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                This procedure restarts the inactivity timer. There is no  */
/*                state change.                                              */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmRestartInactTimer (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmRestartInactTimer\n");

    V3TmrRestartTimer (&(pNbr->inactivityTimer), OSPFV3_INACTIVITY_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pNbr->pInterface->u2RtrDeadInterval));

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmRestartInactTimer\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmAttHelloRcvd                                          */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                It restarts the inactivity timer for this neighbor. The    */
/*                new state of this neighbor is Init.                        */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmAttHelloRcvd (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmAttHelloRcvd\n");

    V3TmrRestartTimer (&(pNbr->inactivityTimer), OSPFV3_INACTIVITY_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pNbr->pInterface->u2RtrDeadInterval));
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_INIT);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmAttHelloRcvd\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmLdngDone                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                It indicates the adjacency forming process is complete.    */
/*                The next state is set to Full.                             */
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmLdngDone (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmLdngDone\n");
    V3NbrUpdateState (pNbr, OSPFV3_NBRS_FULL);
    if (pNbr->pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
        OSPFV3_GR_RESTART)
    {
        O3GrChkAndExitGracefulRestart (pNbr->pInterface->pArea->pV3OspfCxt);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmLdngDone\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmNoAction                                              */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.3                          */
/*                This procedure does not perform any action or state change.*/
/*                                                                           */
/* Input        : pNbr            : Pointer to the neighbor whose state is   */
/*                                  to be changed                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmNoAction (tV3OsNeighbor * pNbr)
{
    UNUSED_PARAM (pNbr);        /* Added to remove warning */

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmNoAction\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmNoAction\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmToBecomeAdj                                           */
/*                                                                           */
/* Description  : Reference : RFC-2328 section 10.4                          */
/*                This procedure determines whether this adjacency should be */
/*                maintained with this neighbor.                             */
/*                                                                           */
/* Input        : pNbr            : neighbor                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE, if adjacency should be formed with this       */
/*                neighbor OSIX_FALSE, otherwise                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3NsmToBecomeAdj (tV3OsNeighbor * pNbr)
{

    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmToBecomeAdj\n");
    pInterface = pNbr->pInterface;
    if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
        (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL) ||
        (pInterface->u1NetworkType == OSPFV3_IF_PTOMP) ||
        (pInterface->u1IsmState == OSPFV3_IFS_DR) ||
        (pInterface->u1IsmState == OSPFV3_IFS_BACKUP) ||
        (V3UtilRtrIdComp (pNbr->nbrRtrId,
                          OSPFV3_GET_DR (pInterface)) == OSPFV3_EQUAL) ||
        (V3UtilRtrIdComp (pNbr->nbrRtrId,
                          OSPFV3_GET_BDR (pInterface)) == OSPFV3_EQUAL))
    {

        OSPFV3_TRC (OSPFV3_NSM_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Should be adjacent\n");
        OSPFV3_TRC (OSPFV3_FN_EXIT,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3NsmToBecomeAdj\n");
        return (OSIX_TRUE);
    }
    else
    {

        OSPFV3_TRC (OSPFV3_NSM_TRC,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Need not be adjacent\n");
        OSPFV3_TRC (OSPFV3_FN_EXIT,
                    pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3NsmToBecomeAdj\n");

        return OSIX_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3NsmResetVariables                                        */
/*                                                                           */
/* Description  : Resets all the variables associated with the neighbor.     */
/*                                                                           */
/* Input        : pNbr            : neighbor                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3NsmResetVariables (tV3OsNeighbor * pNbr)
{

    OSPFV3_TRC (OSPFV3_FN_ENTRY,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER : V3NsmResetVariables\n");

    V3LsuClearRxmtLst (pNbr);
    V3LrqClearLsaReqLst (pNbr);
    V3DdpClearSummary (pNbr);

    OSPFV3_TRC (OSPFV3_FN_EXIT,
                pNbr->pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3NsmResetVariables\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3nsm.c                         */
/*-----------------------------------------------------------------------*/
