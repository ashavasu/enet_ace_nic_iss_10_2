/********************************************************************
 *  * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *  *
 *  * $Id: o3redutl.c,v 1.7 2017/12/26 13:34:28 siva Exp $
 *  *
 *  *
 ********************************************************************/

#include "o3inc.h"

/* Prototypes */

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlResetHsVariables                                  */
/*                                                                          */
/* Description  : This  function is called to reset the LSA counter, hello  */
/*                counter and Dynamic Bulk update state                     */
/*                                                                          */
/* Input        : pHelloInfo  -   pointer to RM hello structure             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedUtlResetHsVariables ()
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedUtlResetHsVariables \r\n");
    gV3OsRtr.ospfRedInfo.u4HelloSynCount = 0;
    gV3OsRtr.ospfRedInfo.u4LsaSyncCount = 0;
    gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_NOT_STARTED;
    gV3OsRtr.ospfRedInfo.b1IsBulkReqRcvd = OSPFV3_FALSE;
    O3RedUtlResetHsLastInfo ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedUtlResetHsVariables \r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlResetHsLastInfo                                   */
/*                                                                          */
/* Description  : This  function is called to reset the LSA counter, hello  */
/*                counter and Dynamic Bulk update state                     */
/*                                                                          */
/* Input        : pHelloInfo  -   pointer to RM hello structure             */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
O3RedUtlResetHsLastInfo (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedUtlResetHsLastInfo \r\n");
    gV3OsRtr.ospfRedInfo.u4LastCxtId = OSPFV3_ZERO;
    /* Reset the Last Interface info */
    gV3OsRtr.ospfRedInfo.u4LastIfIndex = OSPFV3_ZERO;
    /* Reset the Last LSA info */
    MEMSET (&gV3OsRtr.ospfRedInfo.lastLsaInfo, OSPFV3_ZERO,
            sizeof (tV3OsBulkLsuInfo));
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedUtlResetHsLastInfo \r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlSetLsaId                                          */
/*                                                                          */
/* Description  : This  function is called to set the LSA Id counter when   */
/*                self-originated LSA is received from the active node      */
/*                                                                          */
/* Input        : u4LsaId         - Links state id                          */
/*                pLsdbInfo       - LSDB info                               */
/*                pLsaInfo        - LSA info                                */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
O3RedUtlSetLsaId (UINT4 u4LsaId, tV3OsRmLsdbInfo * pLsdbInfo,
                  tV3OsLsaInfo * pLsaInfo)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedUtlSetLsaId\r\n");

    switch (pLsdbInfo->u2InternalLsaType)
    {
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            if (u4LsaId > pLsaInfo->pV3OspfCxt->u4InterAreaLsaCounter)
            {
                pLsaInfo->pV3OspfCxt->u4InterAreaLsaCounter = u4LsaId;
            }
            break;
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        case OSPFV3_COND_NSSA_LSA:
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (u4LsaId > pLsaInfo->pV3OspfCxt->u4AsExtLsaCounter)
            {
                pLsaInfo->pV3OspfCxt->u4AsExtLsaCounter = u4LsaId;
            }
            break;
        }
        case OSPFV3_INTRA_AREA_PREFIX_LSA:
        {
            if (u4LsaId > pLsaInfo->pArea->u4IntraAreaLsaCounter)
            {
                pLsaInfo->pArea->u4IntraAreaLsaCounter = u4LsaId;
            }
            break;
        }
        default:
        {
            break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedUtlSetLsaId\r\n");
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlSetAssoLsaId                                      */
/*                                                                          */
/* Description  : This  function is called to set the LSA Id counter when   */
/*                self-originated LSA is received from the active node      */
/*                The lsa id needs to be set in the corresponding primitive */
/*                                                                          */
/* Input        : linkStateId     - Links state id                          */
/*                pLsdbInfo       - LSDB info                               */
/*                pLsaInfo        - LSA info                                */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/
PUBLIC VOID
O3RedUtlSetAssoLsaId (tV3OsLinkStateId linkStateId, tV3OsRmLsdbInfo * pLsdbInfo,
                      tV3OsLsaInfo * pLsaInfo)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedUtlSetAssoLsaId\r\n");

    if (pLsaInfo->pLsaDesc->pAssoPrimitive == NULL)
    {
        return;
    }

    switch (pLsdbInfo->u2InternalLsaType)
    {
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        {
            MEMCPY (((tV3OsRtEntry *) (VOID *) (pLsaInfo->pLsaDesc->
                                                pAssoPrimitive))->linkStateId,
                    linkStateId, sizeof (tV3OsLinkStateId));
            break;
        }
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        {
            MEMCPY (((tV3OsAddrRange *) (VOID *) (pLsaInfo->pLsaDesc->
                                                  pAssoPrimitive))->linkStateId,
                    linkStateId, sizeof (tV3OsLinkStateId));
            break;
        }
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (OSPFV3_IS_AREA_BORDER_RTR (pLsaInfo->pV3OspfCxt))
            {
                break;
            }
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        {
            MEMCPY (((tV3OsExtRoute *) (VOID *) (pLsaInfo->pLsaDesc->
                                                 pAssoPrimitive))->linkStateId,
                    linkStateId, sizeof (tV3OsLinkStateId));
            break;
        }
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_COND_NSSA_LSA:
        {
            MEMCPY (((tV3OsAsExtAddrRange *) (VOID *) (pLsaInfo->pLsaDesc->
                                                       pAssoPrimitive))->
                    linkStateId, linkStateId, sizeof (tV3OsLinkStateId));
            break;
        }
        default:
        {
            break;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: O3RedUtlSetAssoLsaId\r\n");
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlSendBulkUpdate                                    */
/*                                                                          */
/* Description  : This  function adds the length in the LEN field and sends */
/*                the packet to the standby node                            */
/*                                                                          */
/* Input        : pBuf      -  RM message                                   */
/*                u4PktLen  -  Packet length                                */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedUtlSendBulkUpdate (tRmMsg * pBuf, UINT4 u4PktLen)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedUtlSendBulkUpdate \r\n");

    /* Set the total payload length in the packet at the 
     * offset OSPFV3_RED_BULK_LEN_OFFSET
     */

    if (pBuf == NULL)
    {
        return OSIX_SUCCESS;
    }

    RM_DATA_ASSIGN_2_BYTE (pBuf, OSPFV3_RED_BULK_LEN_OFFSET, (UINT2) u4PktLen);

    if (V3OspfEnqMsgToRm (pBuf, (UINT2) u4PktLen) == OSPFV3_FAILURE)
    {

        OSPFV3_EXT_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        gV3OsRtr.
                        apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                        u4ContextId, "Cannot enqueue message to RM module");
        O3RedRmSendEvent ((UINT4) RM_BULK_UPDT_ABORT, (UINT4) RM_SENDTO_FAIL);
        pBuf = NULL;
        gu4O3RedUtlSendBulkUpdateFail++;
        return OSIX_FAILURE;
    }

    pBuf = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedUtlSendBulkUpdate \r\n");

    return OSIX_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedUtlBulkUpdtRelinquish                                */
/*                                                                          */
/* Description  : This  function gets the current system time and compares  */
/*                with the current bulk update start time. If the time      */
/*                interval is more than the maximum time taken, then        */
/*                relinquish needs to be done                               */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                               */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedUtlBulkUpdtRelinquish (VOID)
{
    UINT4               u4CurrentTime = 0;
    UINT4               u4DeltaTime = 0;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedUtlBulkUpdtRelinquish \r\n");

    /* Get the current time */
    u4CurrentTime = OsixGetSysUpTime ();

    /* If the difference between the current time and the current bulk
     * update start time is greater than or equal to delta, then send
     * success to relinquish the bulk update process
     */

    if (gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime > u4CurrentTime)
    {
        /* This is the special scenario where wrap around has occurred.
         * In this case, get the remaining time between max UINT4 value
         * and gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime. Add the result
         * with the current time. Compare the new value with delta
         */
        u4DeltaTime = (OSPFV3_MAX_TIME -
                       gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime) +
            u4CurrentTime;
    }
    else
    {
        u4DeltaTime = u4CurrentTime -
            gV3OsRtr.ospfRedInfo.u4CurrentBulkStartTime;
    }

    if (u4DeltaTime >= OSPFV3_RED_BULK_UPDT_REL_TIME)
    {
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3RedUtlBulkUpdtRelinquish \r\n");
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : V3GetShowCmdOutputAndCalcChkSum                    */
/*                                                                           */
/* Description        : This funcion handles the execution of show commands  */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then being sent to RM.       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pu2SwAudChkSum                                       */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSPFV3_SUCCESS/OSPFV3_FAILURE                        */
/*****************************************************************************/
INT4
V3GetShowCmdOutputAndCalcChkSum (UINT2 *pu2SwAudChkSum)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : V3GetShowCmdOutputAndCalcChkSum\r\n");

#if (defined CLI_WANTED && defined RM_WANTED)

    if (RmRetrieveNodeState () == RM_ACTIVE)
    {
        if (V3CliGetShowCmdOutputToFile ((UINT1 *) OSPFV3_AUDIT_FILE_ACTIVE) !=
            OSPFV3_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "GetShRunFile Failed\n");
            return OSPFV3_FAILURE;
        }
        if (V3CliCalcSwAudCheckSum
            ((UINT1 *) OSPFV3_AUDIT_FILE_ACTIVE,
             pu2SwAudChkSum) != OSPFV3_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "CalcSwAudChkSum Failed\n");
            return OSPFV3_FAILURE;
        }
    }
    else if (RmRetrieveNodeState () == RM_STANDBY)
    {
        if (V3CliGetShowCmdOutputToFile ((UINT1 *) OSPFV3_AUDIT_FILE_STDBY) !=
            OSPFV3_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "GetShRunFile Failed\n");
            return OSPFV3_FAILURE;
        }
        if (V3CliCalcSwAudCheckSum
            ((UINT1 *) OSPFV3_AUDIT_FILE_STDBY,
             pu2SwAudChkSum) != OSPFV3_SUCCESS)
        {
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "CalcSwAudChkSum Failed\n");
            return OSPFV3_FAILURE;
        }
    }
    else
    {
        OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                        "Node State is neither active nor standby\n");
        return OSPFV3_FAILURE;
    }
#else
    UNUSED_PARAM (pu2SwAudChkSum);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT : V3GetShowCmdOutputAndCalcChkSum\r\n");
    return OSPFV3_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : V3RmEnqChkSumMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSPFV3_SUCCESS/OSPFV3_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
V3RmEnqChkSumMsgToRm (UINT2 u2AppId, UINT2 u2ChkSum)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3RmEnqChkSumMsgToRm\r\n");

#ifdef RM_WANTED
    if ((RmEnqChkSumMsgToRmFromApp (u2AppId, u2ChkSum)) == RM_FAILURE)
    {
        return OSPFV3_FAILURE;
    }
#else
    UNUSED_PARAM (u2AppId);
    UNUSED_PARAM (u2ChkSum);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3RmEnqChkSumMsgToRm\r\n");
    return OSPFV3_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                  End of the file o3redutl.c                           */
/*-----------------------------------------------------------------------*/
