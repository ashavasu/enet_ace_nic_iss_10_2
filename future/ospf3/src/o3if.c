/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3if.c,v 1.31 2018/01/25 10:04:15 siva Exp $
 *
 * Description: This file contains procedures for creating and
 *              deleting interface structures and handling interface
 *              up/down/loopback/unloop indications from lower
 *              levels.
 *******************************************************************/
#include "o3inc.h"

PRIVATE VOID V3IfPrefixDelete PROTO ((tV3OsInterface * pInterface,
                                      tIp6Addr * ip6Addr, UINT1 u1PrefixLen));

/* Proto types of the functions private to this file only */

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfCreateInCxt                                            */
/*                                                                           */
/* Description  : It creates a new interface structure with the fields set   */
/*                to the value specified. This procedure also adds the new   */
/*                interface structure to the interface RB Tree               */
/*                (gV3OsRtr.pIfRBRoot), and areas interface list             */
/*                (pArea.ifsInArea).                                         */
/*                                                                           */
/* Input        : pV3OspfCxt         : Context pointer                       */
/*                ipv6LinkLocalAddr  : IPv6 Link-Local address               */
/*                u4IfIndex          : Interface Index                       */
/*                u4MtuSize          : Maximum size of packet that can be    */
/*                                     transmitted on the specified          */
/*                                     interface                             */
/*                u4IfSpeed          : Speed of the underlying network       */
/*                u1IfType           : Interface Type                        */
/*                u1IfOperStat       : Operational status of the interface   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Pointer to the created interface, if successfully created  */
/*                NULL, otherwise                                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC tV3OsInterface *
V3IfCreateInCxt (tV3OspfCxt * pV3OspfCxt, tNetIpv6IfInfo * pNetIp6IfInfo)
{

    tV3OsInterface     *pInterface = NULL;
    tNetIpv6AddrInfo    ip6AddrInfo;
    tNetIpv6AddrInfo    tmpIp6AddrInfo;
    INT4                i4Status = NETIPV6_SUCCESS;
    UINT4               u4RbTreeStatus = RB_SUCCESS;
    UINT4               u4AddrlessIf = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3IfCreate \n");

    /* Allocate buffer for interface data structure */
    OSPFV3_IF_ALLOC (&(pInterface));
    if (NULL == pInterface)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for Interface\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_ISM_TRC,
                    pV3OspfCxt->u4ContextId, "If Alloc Failure\n");
        return NULL;
    }

    V3IfSetDefaultValues (pInterface);

    /* Create RB Tree for this interface */
    pInterface->pLinkScopeLsaRBRoot =
        RBTreeCreateEmbedded ((OSPFV3_OFFSET (tV3OsLsaInfo,
                                              rbNode.RbLinkScopeNode)),
                              V3CompareLsa);
    if (pInterface->pLinkScopeLsaRBRoot == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Link-Scope LSA RB Tree Creation Failed\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                    "Link-Scope LSA RB Tree Creation Failed\n");
        OSPFV3_IF_FREE (pInterface);
        return NULL;
    }

    u4AddrlessIf = pNetIp6IfInfo->u4AddressLessIf;
    pInterface->u4AddrlessIf = u4AddrlessIf;

    /* Initialize variables */
    OSPFV3_IP6_ADDR_COPY (pInterface->ifIp6Addr, pNetIp6IfInfo->Ip6Addr);
    pInterface->pArea = pV3OspfCxt->pBackbone;

    /* Population of Site-Local/Global Addresses */
    if (u4AddrlessIf != 0)
    {
        /*in numnumbered case,associated interface address need to be copied to unnumbered-interface */
        i4Status = NetIpv6GetFirstIfAddr (u4AddrlessIf, &(ip6AddrInfo));
    }
    else
    {
        i4Status =
            NetIpv6GetFirstIfAddr (pNetIp6IfInfo->u4IfIndex, &(ip6AddrInfo));
    }

    while (i4Status == NETIPV6_SUCCESS)
    {
        if ((V3IfPrefixAdd (pInterface, &(ip6AddrInfo.Ip6Addr),
                            (UINT1) ip6AddrInfo.u4PrefixLength)) ==
            OSIX_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Interface Prefix Addition Failed\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                        "Interface Prefix Addition Failed\n");
            break;
        }

        MEMCPY (&(tmpIp6AddrInfo), &(ip6AddrInfo), sizeof (tNetIpv6AddrInfo));

        if (u4AddrlessIf != 0)
        {
            i4Status = (INT4) NetIpv6GetNextIfAddr (u4AddrlessIf,
                                                    &(tmpIp6AddrInfo),
                                                    &(ip6AddrInfo));
        }
        else
        {
            i4Status = (INT4) NetIpv6GetNextIfAddr (pNetIp6IfInfo->u4IfIndex,
                                                    &(tmpIp6AddrInfo),
                                                    &(ip6AddrInfo));
        }

    }

    /*
     * The Interface oper status which is to be got from lower layers,
     * is obtained as one of the parameter which is in turn obtained from IP. 
     */
    pInterface->operStatus = (UINT1) pNetIp6IfInfo->u4Oper;

    pInterface->u4MtuSize = pNetIp6IfInfo->u4Mtu;
    V3MapIfType (pNetIp6IfInfo->u4InterfaceType, &pInterface->u1NetworkType);

    pInterface->u4IfMetric =
        V3IfGetMetricFromSpeedInCxt (pV3OspfCxt, pNetIp6IfInfo->u4IfSpeed,
                                     pNetIp6IfInfo->u4IfHighSpeed);
    /* Assign Interface Id */
    pInterface->u4InterfaceId = pNetIp6IfInfo->u4IfIndex;
    pInterface->u4MultActiveIfId = pNetIp6IfInfo->u4IfIndex;

    /* Add this interface to RB TREE */
    u4RbTreeStatus = RBTreeAdd (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface);
    if (u4RbTreeStatus == RB_FAILURE)
    {
        SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                      "Interface RB Tree Addition Failed\n"));

        OSPFV3_TRC (OSPFV3_CRITICAL_TRC, pV3OspfCxt->u4ContextId,
                    "Interface RB Tree Addition Failed\n");
        RBTreeDelete (pInterface->pLinkScopeLsaRBRoot);
        OSPFV3_IF_FREE (pInterface);
        return NULL;
    }

    TMO_SLL_Add (&(pV3OspfCxt->pBackbone->ifsInArea),
                 &(pInterface->nextIfInArea));

    V3SetRtrLsaLsId ((UINT1 *) pInterface, 0, OSIX_FALSE);

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                 pV3OspfCxt->u4ContextId, "If %s.%d Created\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    if (pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
    {
        /* This invokes Type 7 & router LSA orgination/flushing
         * This should not be called when router is performing GR */
        /* The Router may become ABR if the ABR Type supported
         * by Router is IBM Type ABR */
        if (pV3OspfCxt->u1ABRType == OSPFV3_IBM_ABR)
        {
            V3RtrHandleABRStatChngInCxt (pV3OspfCxt);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT : V3IfCreate \n");

    return pInterface;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetDefaultValues                                       */
/*                                                                           */
/* Description  : This procedure initialises the default values for the      */
/*                various fields in the interface data structure. This       */
/*                procedure is called during creation of an interface.       */
/*                                                                           */
/* Input        : pInterface    :    interface to be initialized             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfSetDefaultValues (tV3OsInterface * pInterface)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3IfSetDefaultValues \n");

    MEMSET (pInterface, 0, sizeof (tV3OsInterface));

    pInterface->admnStatus = OSPFV3_ENABLED;
    pInterface->u1IsmState = OSPFV3_IFS_DOWN;

    pInterface->u2HelloInterval = OSPFV3_DEF_HELLO_INTERVAL;
    pInterface->u2RtrDeadInterval = OSPFV3_DEF_RTR_DEAD_INTERVAL;
    pInterface->u2IfTransDelay = OSPFV3_DEF_IF_TRANS_DELAY;
    pInterface->u1RtrPriority = OSPFV3_DEF_RTR_PRIORITY;
    pInterface->u2RxmtInterval = OSPFV3_DEF_RXMT_INTERVAL;
    pInterface->u4PollInterval = OSPFV3_DEF_POLL_INTERVAL;
    pInterface->u2MultIfActiveDetectInterval =
        (OSPFV3_DEF_RTR_DEAD_INTERVAL / OSPFV3_TWO);

    pInterface->u4NbrProbeRxmtLimit = OSPFV3_DEF_NBR_PROBE_RXMT_LIMIT;
    pInterface->u4NbrProbeInterval = OSPFV3_DEF_NBR_PROBE_INTERVAL;
    pInterface->u1SdbyActCount = OSPFV3_DEF_IFOVERLINK_COUNT;

    pInterface->ifStatus = OSPFV3_INVALID;

    pInterface->bDemandNbrProbe = OSIX_FALSE;
    pInterface->bDcEndpt = OSIX_FALSE;
    OSPFV3_OPT_CLEAR_ALL (pInterface->ifOptions);
    pInterface->bPassive = OSIX_FALSE;
    pInterface->u1ActiveDetectTmrFlag = OSPFV3_MULTIF_ACTIVE_TMR_OFF;
    pInterface->bLinkLsaSuppress = OSIX_FALSE;

    /*Bfd default value */
    pInterface->u1BfdIfStatus = OSPFV3_BFD_DISABLED;

    /* OSPFV3 AT initialisation */
    pInterface->u1AuthMode = OSPFV3_AUTH_MODE_NONE;
    pInterface->u2AuthType = OSPFV3_NO_AUTH;
    pInterface->u2CryptoAuthType = OSPFV3_DEF_AUTH_TYPE;
    pInterface->cryptoSeqno.u4LowCryptSeqNum = OSPFV3_ZERO;

    TMO_SLL_Init (&(pInterface->nbrsInIf));
    TMO_SLL_Init (&(pInterface->ip6AddrLst));
    TMO_SLL_Init (&(pInterface->ip6NetPrefLst));
    TMO_SLL_Init (&(pInterface->linkLsaLst));
    TMO_SLL_Init (&(pInterface->sortAuthkeyLst));

    TMO_SLL_Init_Node (&(pInterface->nextIfInArea));
    TMO_SLL_Init_Node (&(pInterface->nextVirtIfNode));

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3IfSetDefaultValues \n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfActivate                                               */
/*                                                                           */
/* Description  : Generates the Event V3IfUp                                 */
/*                                                                           */
/* Input        : pInterface        : the interface to be inactivated        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfActivate (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfActivate\n");

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d To be Activated\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    /* If Default Passive Interface is set to true, then make
     * this interface as passive by default */
    if (pInterface->pArea->pV3OspfCxt->bDefaultPassiveInterface == OSIX_TRUE)
    {
        pInterface->bPassive = OSIX_TRUE;
    }

    V3IfUp (pInterface);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfActivate\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDelete                                                 */
/*                                                                           */
/* Description  : Deletes the specified interface structure from the hash    */
/*                table and other lists. The associated timers are disabled. */
/*                Also all neighbors on this interface are deleted. The      */
/*                memory allocated for this interface structure is freed.    */
/*                                                                           */
/* Input        : pInterface        : the interface to be deleted            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfDelete (tV3OsInterface * pInterface)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsPrefixNode    *pTmpPrefNode = NULL;
    tV3OsArea          *pTransArea = NULL;
    tV3OsArea          *pArea = NULL;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4RbTreeStatus = RB_FAILURE;
    UINT1               u1PrevState = OSPFV3_IFS_DOWN;
    tV3OsInterface     *pVirtInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT1               u1VirtRoute = OSIX_FALSE;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    UINT4               u4ContextId = 0;

    if (pInterface->pArea == NULL)
    {
        return;
    }

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDelete\n");
    OSPFV3_TRC2 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d To be Deleted\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    u4ContextId = pInterface->pArea->pV3OspfCxt->u4ContextId;
    u1PrevState = pInterface->u1IsmState;

    V3ResetRtrLsaLsId ((UINT1 *) pInterface, 0, OSIX_FALSE);
    /* Delete all Link-Scope LSA and Self-Originated LSAs */
    V3LsuFlushSelfOrgLinkScopeLsa (pInterface);
    V3LsuDeleteLinkScopeLsa (pInterface);

    if ((pInterface->u4MultActiveIfId == pInterface->u4InterfaceId) &&
        (pInterface->u1SdbyActCount > OSPFV3_DEF_IFOVERLINK_COUNT))
    {                            /* if the interface is Active over the link */

        V3IsmActiveDown (pInterface, OSPFV3_DOWN_ON_LINK);

    }
    else
    {                            /* If the interface is standby over the link */

        V3IsmStandbyDown (pInterface, OSPFV3_DOWN_ON_LINK);
    }

    V3IfUpdateState (pInterface, OSPFV3_IFS_DOWN);

    V3IfCleanup (pInterface, OSPFV3_IF_CLEANUP_DELETE);
    /* Clean up all Standby/Active Interface Nbrs */
    V3IfCleanup (pInterface, OSPFV3_IF_SBY_CLEANUP_DELETE);
    pInterface->u1SdbyActCount = OSPFV3_DEF_IFOVERLINK_COUNT;

    pArea = pInterface->pArea;
    pV3OspfCxt = pInterface->pArea->pV3OspfCxt;
    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));

    if (pInterface->u1NetworkType == OSPFV3_IF_VIRTUAL)
    {
        TMO_SLL_Delete (&(pV3OspfCxt->virtIfLst),
                        &(pInterface->nextVirtIfNode));
        if (!OSPFV3_IS_FORWARD_ADDR_NULL (&pInterface->ifIp6Addr))
        {
            TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
            {
                pVirtInterface = OSPFV3_GET_BASE_PTR
                    (tV3OsInterface, nextVirtIfNode, pLstNode);
                if ((V3UtilIp6AddrComp (&pInterface->ifIp6Addr,
                                        &pVirtInterface->ifIp6Addr) ==
                     OSPFV3_EQUAL))
                {
                    u1VirtRoute = OSIX_TRUE;
                }

            }
            if (u1VirtRoute == OSIX_FALSE)
            {
                V3RtcCalculateLocalHostRouteInCxt
                    (pV3OspfCxt,
                     &(pInterface->ifIp6Addr),
                     &(pInterface->transitAreaId), 0, OSPFV3_DELETED);
            }
        }

        pTransArea = V3GetFindAreaInCxt (pInterface->pArea->pV3OspfCxt,
                                         &pInterface->transitAreaId);
        if (pTransArea != NULL)
        {
            V3GenIntraAreaPrefixLsa (pTransArea);
        }
    }
    else
    {
        u4RbTreeStatus =
            RBTreeRemove (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface);
        if (u4RbTreeStatus == RB_FAILURE)
        {
            SYS_LOG_MSG ((OSPFV3_INFO_LEVEL, OSPFV3_SYSLOG_ID,
                          "Interface RB Tree Remove Failed\n"));

            OSPFV3_TRC (OSPFV3_CRITICAL_TRC, u4ContextId,
                        "Interface RB Tree Remove Failed\n");
        }

        if (u1PrevState > OSPFV3_IFS_DOWN)
        {
            V3RtcCalculateLocalRoutes (pInterface, OSPFV3_DELETED);
        }
    }
    if (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
    {
        V3RtrHandleABRStatChngInCxt (pInterface->pArea->pV3OspfCxt);
    }
    /* Delete the Prefix List */
    OSPFV3_DYNM_SLL_SCAN (&pInterface->ip6AddrLst, pPrefixNode, pTmpPrefNode,
                          tV3OsPrefixNode *)
    {
        TMO_SLL_Delete (&(pInterface->ip6AddrLst),
                        &(pPrefixNode->nextPrefixNode));

        OSPFV3_PREFIX_FREE (pPrefixNode);
    }

    /* Initialise the schedule queue */
    V3IsmInitSchedQueueInCxt (NULL, pInterface);

    RBTreeDelete (pInterface->pLinkScopeLsaRBRoot);
    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC, u4ContextId,
                 "If %s.%d Deleted\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    OSPFV3_IF_FREE (pInterface);

    if (TMO_SLL_Count (&(pArea->ifsInArea)) == 0)
    {
        V3LsuDeleteAreaScopeLsa (pArea);
    }

    if ((pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_RESTART) &&
        (pV3OspfCxt->u1RestartExitReason == OSPFV3_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pV3OspfCxt, OSPFV3_RESTART_TOP_CHG);
    }

    /* If the interface is down, If this router is helper for any neighbor
     * Stop the helper process */
    if ((pV3OspfCxt->u1HelperSupport & OSPFV3_GRACE_HELPER_ALL) &&
        (pV3OspfCxt->u1HelperStatus == OSPFV3_GR_HELPING) &&
        (pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED))
    {
        TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
            }
        }
        TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst), pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                              pLstNode);
            if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
            {
                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                            nextNbrInIf, pNbrNode);
                if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
                {
                    O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                }
            }
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId, "EXIT : V3IfDelete\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDisable                                                */
/*                                                                           */
/* Description  : This function disables the specified interface structure   */
/*                from all the lists. The associated timers are disabled.    */
/*                Also dynamically discovered neighbors on this interface    */
/*                are deleted.                                               */
/*                                                                           */
/* Input        : pInterface : the interface to be disabled                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfDisable (tV3OsInterface * pInterface)
{
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDisable\n");

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC |
                 OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d To Be Disabled\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    /* If the interface is disabled, If this router is helper for any neighbor
     * Stop the helper process */
    if ((pInterface->pArea->pV3OspfCxt->
         u1HelperSupport & OSPFV3_GRACE_HELPER_ALL) &&
        (pInterface->pArea->pV3OspfCxt->u1HelperStatus == OSPFV3_GR_HELPING) &&
        (pInterface->pArea->pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED))
    {
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->sortNbrLst),
                      pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
            }
        }
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                              pLstNode);
            if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
            {
                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                            nextNbrInIf, pNbrNode);
                if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
                {
                    O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                }
            }
        }
    }

    OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_DOWN);

    if ((pInterface->u4MultActiveIfId == pInterface->u4InterfaceId) &&
        (pInterface->u1SdbyActCount > OSPFV3_DEF_IFOVERLINK_COUNT))
    {                            /* If the interface is active over the link */

        V3IsmActiveDown (pInterface, OSPFV3_DOWN_ON_LINK);

    }
    else
    {                            /*if the interface is standby over the link */

        V3IsmStandbyDown (pInterface, OSPFV3_DOWN_ON_LINK);
    }
    pInterface->u1SdbyActCount = OSPFV3_DEF_IFOVERLINK_COUNT;

    if (pInterface->u1NetworkType != OSPFV3_IF_VIRTUAL)
    {
        V3RtcCalculateLocalRoutes (pInterface, OSPFV3_DELETED);
    }

    V3IfCleanup (pInterface, OSPFV3_IF_CLEANUP_DISABLE);
    V3IfCleanup (pInterface, OSPFV3_IF_SBY_CLEANUP_DELETE);

    /* Check whether the router is still an ABR
     * after disabling this interface
     */
    if (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE)
    {
        V3RtrHandleABRStatChngInCxt (pInterface->pArea->pV3OspfCxt);
    }

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d Disabled\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfDisable\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfUp                                                     */
/*                                                                           */
/* Description  : This function is invoked when we receive indication from   */
/*                the lower levels that this interface is operational.       */
/*                                                                           */
/* Input        : pInterface        : interface which has come up            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfUp (tV3OsInterface * pInterface)
{
    UINT1               u1PrvAbrStat = OSIX_FALSE;
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsArea          *pTmpArea = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfUp\n");

    OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    if ((pInterface->operStatus == OSPFV3_ENABLED) &&
        (pInterface->admnStatus == OSPFV3_ENABLED) &&
        (pInterface->ifStatus == ACTIVE) &&
        (pInterface->pArea->pV3OspfCxt->admnStat == OSPFV3_ENABLED))
    {

        /* Add the backbone area to areas list if the new iface coming up is
         * associated with backbone.
         */

        /* Check whether the router has become ABR after adding this 
           interface. */
        if (pInterface->u1NetworkType == OSPFV3_IF_LOOPBACK)
        {
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_LOOP_IND);
        }
        else
        {
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UP);
        }
        u1PrvAbrStat = (UINT1) pInterface->pArea->pV3OspfCxt->bAreaBdrRtr;
        if (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
            OSPFV3_GR_NONE)
        {
            /* This invokes Type 7 & router LSA orgination/flushing
             * This should not be called when router is performing GR */
            V3RtrHandleABRStatChngInCxt (pInterface->pArea->pV3OspfCxt);
        }
        if ((!((OSPFV3_IS_STUB_AREA (pInterface->pArea)) ||
               (OSPFV3_IS_NSSA_AREA (pInterface->pArea)))) &&
            (pInterface->pArea->u4ActIntCount == 1) &&
            (pInterface->pArea->pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE))
        {
            TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->areasLst),
                          pTmpArea, tV3OsArea *)
            {
                if (pTmpArea == pInterface->pArea)
                {
                    continue;
                }
                if (pTmpArea->u4DcBitResetLsaCount != 0)
                {
                    pInterface->pArea->bIndicationLsaPresence = OSIX_TRUE;
                    V3GenerateLsa (pInterface->pArea, OSPFV3_INDICATION_LSA,
                                   &pInterface->pArea->pV3OspfCxt->rtrId, NULL,
                                   OSIX_FALSE);
                    break;
                }
            }
        }

        if ((u1PrvAbrStat == pInterface->pArea->pV3OspfCxt->bAreaBdrRtr) &&
            (pInterface->pArea->pV3OspfCxt->bAreaBdrRtr == OSIX_TRUE))
        {
            /* Implies New interface has come up
             * in Rtr whose ABR state has not 
             * changed
             */
            if (pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA)
            {
                V3AreaNssaFsmFindTranslator (pInterface->pArea);
            }
        }

        V3RtcCalculateLocalRoutes (pInterface, OSPFV3_CREATED);
        if (pInterface->u1NetworkType != OSPFV3_IF_VIRTUAL)
        {
            /* Link-LSA Generation */
            OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, pInterface->u4InterfaceId);

            if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
                (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
            {

                if (pInterface->bLinkLsaSuppress == OSIX_FALSE)
                {
                    V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                                   &tmpLinkStateId, (UINT1 *) pInterface,
                                   OSIX_FALSE);
                }
            }
            else
            {
                V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                               &tmpLinkStateId, (UINT1 *) pInterface,
                               OSIX_FALSE);

            }

            /* Intra-Area-Prefix-Lsa has to be generated */
            V3GenIntraAreaPrefixLsa (pInterface->pArea);
        }

        OSPFV3_TRC2 (CONTROL_PLANE_TRC | OSPFV3_ISM_TRC,
                     pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "If %s.%d UP\n", Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
    }
    else
    {
        OSPFV3_TRC2 (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                     "EXIT : V3IfUp :If %s.%d UP Failed\n",
                     Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                     pInterface->u4InterfaceId);
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfUp\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDown                                               */
/*                                                                           */
/* Description  : It is invoked when indication is received from the lower   */
/*                levels that this interface is not operational. Then the    */
/*                interface is disabled for the OSPFv3 process. Also if it   */
/*                was the forwarding address for some Type-7 routes, then    */
/*                the routes forwarding address is modified.                 */
/*                                                                           */
/* Input        : pInterface        : interface to be brought down           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfDown (tV3OsInterface * pInterface)
{
    tV3OsLeakRoutes    *pIfLeakRoutes = NULL;
    UINT4               u4ContextId =
        pInterface->pArea->pV3OspfCxt->u4ContextId;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDown\n");

    OSPFV3_TRC2 (OSPFV3_ISM_TRC,
                 pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d To Be Brought DN\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);

    OSPFV3_IFLEAK_STATUS_NODE_ALLOC ((&pIfLeakRoutes));
    if (pIfLeakRoutes != NULL)
    {
        pIfLeakRoutes->u4IfIndex = pInterface->u4InterfaceId;

        if (RBTreeAdd ((gV3OsRtr.apV3OspfCxt[u4ContextId])->IfLeakRouteRBTree,
                       pIfLeakRoutes) == RB_FAILURE)
        {
            OSPFV3_TRC1 (OSPFV3_ISM_TRC,
                         pInterface->pArea->pV3OspfCxt->u4ContextId, "Failure",
                         Ip6PrintAddr (&(pInterface->ifIp6Addr)));
        }

        Rtm6ApiHandleProtocolStatusInCxt (u4ContextId, OSPF6_ID,
                                          RTM6_INTERFACE_SHUTDOWN,
                                          pIfLeakRoutes->u4IfIndex, 0, 0, 0);
    }

    V3IfDisable (pInterface);

    /* Check if this interface was Fwd Address for  */
    /* any Type 7 LSA originated by the router      */
    /* re-orignate that LSA                         */
    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
    {
        V3ResetNssaFwdAddr (pInterface);
    }
    if (pIfLeakRoutes != NULL)
    {
        RBTreeRemove ((gV3OsRtr.apV3OspfCxt[u4ContextId])->IfLeakRouteRBTree,
                      (tRBElem *) pIfLeakRoutes);
        OSPFV3_IFLEAK_STATUS_NODE_FREE ((pIfLeakRoutes));
    }

    OSPFV3_TRC2 (OSPFV3_ISM_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "If %s.%d Brought DN\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId);
    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfDown\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfUpdateState                                            */
/*                                                                           */
/* Description  : This function is invoked if a state transition occurs      */
/*                during the action functions of the interface state machine.*/
/*                This function updates the state of the specified interface */
/*                and depending on the state it takes action                 */
/*                                                                           */
/* Input        : pInterface       : the interface whose state is updated    */
/*                u1State          : the new state of the interface          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfUpdateState (tV3OsInterface * pInterface, UINT1 u1State)
{
    UINT1               u1PrevState = OSPFV3_IFS_DOWN;
    tV3OsRouterId       DesgBackRtr;
    tV3OsLsaInfo       *pLsaInfo = NULL;
    tV3OsLinkStateId    tmpLinkStateId;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pStdNode = NULL;
    tV3OsInterface     *pStandbyInterface = NULL;
    tV3OsNeighbor      *pStandbyLst = NULL;

    OSPFV3_TRC3 (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                 "ENTER : V3IfUpdateState: If %s.%d"
                 " To Be Updated New State %s\n",
                 Ip6PrintAddr (&(pInterface->ifIp6Addr)),
                 pInterface->u4InterfaceId, gau1Os3DbgIfState[u1State]);

    if (pInterface->u1IsmState != u1State)
    {
        if ((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ||
            (pInterface->u1NetworkType == OSPFV3_IF_PTOP))
        {
            if ((u1State != OSPFV3_IFS_DOWN) &&
                (pInterface->u1IsmState == OSPFV3_IFS_DOWN))
            {
                V3OspfJoinMcastGroup (&(gV3OsAllSpfRtrs),
                                      pInterface->u4InterfaceId,
                                      pInterface->pArea->pV3OspfCxt->
                                      u4ContextId);
            }
        }

        /* 
         * If interface is in state Designated Router then the network lsa 
         * and intra-area-prefix lsa (if any) should be flushed out. 
         */
        if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
             OSPFV3_GR_NONE))
        {
            /* lsa can be flushed only if router is not in GR state */
            if (pInterface->u1IsmState == OSPFV3_IFS_DR)
            {
                OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId,
                                       pInterface->u4InterfaceId);
                if ((pLsaInfo =
                     V3LsuSearchDatabase (OSPFV3_NETWORK_LSA, &(tmpLinkStateId),
                                          &(pInterface->pArea->pV3OspfCxt->
                                            rtrId), NULL,
                                          pInterface->pArea)) != NULL)
                {
                    /* Network lsa has to be flushed 
                     * out of the routing domain */
                    V3AgdFlushOut (pLsaInfo);
                }

                /* Intra-Area-Prefix-Lsa has been flushed in V3IfDelete */
                if (u1State != OSPFV3_IFS_DOWN)
                {
                    /*Intra-Area-Prefix-Lsa has  to be generated */
                    V3GenIntraAreaPrefixLsa (pInterface->pArea);
                }
            }
        }

        u1PrevState = pInterface->u1IsmState;
        pInterface->u1IsmState = u1State;

        if (pInterface->u1SdbyActCount > OSPFV3_DEF_IFOVERLINK_COUNT)
        {
            /* Update the DR and BDR of the Standby Interfaces */
            if ((u1State != OSPFV3_IFS_STANDBY) &&
                (pInterface->u4MultActiveIfId == pInterface->u4InterfaceId))
            {
                /* If pInterface is the Active Interface over the Link */
                pNode = NULL;
                MEMSET (DesgBackRtr, 0, OSPFV3_RTR_ID_LEN);

                TMO_SLL_Scan (&(pInterface->pArea->ifsInArea), pNode,
                              tTMO_SLL_NODE *)
                {
                    pStandbyInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                                             nextIfInArea,
                                                             pNode);

                    if ((pStandbyInterface->u4MultActiveIfId ==
                         pInterface->u4InterfaceId) &&
                        (pStandbyInterface->u4InterfaceId !=
                         pInterface->u4InterfaceId))
                    {
                        if (u1State == OSPFV3_IFS_DOWN)
                        {        /* Update Standby interface's DR and 
                                 * BDR over the link */

                            pStandbyInterface->u4DRInterfaceId = 0;
                            pStandbyInterface->u4BDRInterfaceId = 0;
                            OSPFV3_RTR_ID_COPY (pStandbyInterface->desgRtr,
                                                DesgBackRtr);
                            OSPFV3_RTR_ID_COPY (pStandbyInterface->
                                                backupDesgRtr, DesgBackRtr);

                        }
                        else
                        {        /* Update Standby interface's DR and 
                                   BDR over the link */

                            pStandbyInterface->u4DRInterfaceId =
                                pInterface->u4DRInterfaceId;
                            pStandbyInterface->u4BDRInterfaceId =
                                pInterface->u4BDRInterfaceId;
                            OSPFV3_RTR_ID_COPY (pStandbyInterface->desgRtr,
                                                pInterface->desgRtr);
                            OSPFV3_RTR_ID_COPY (pStandbyInterface->
                                                backupDesgRtr,
                                                pInterface->backupDesgRtr);

                        }

                    }

                }                /* End of scan */

            }

            if ((u1State == OSPFV3_IFS_DOWN) &&
                (u1PrevState != OSPFV3_IFS_STANDBY))
            {                    /* Intimation about the active interface
                                   DOWN over the link to IPv6  */

                V3OspfSendMultIfOnLink (pInterface, IPV6_ACTIVE_DELETE, 0);
            }
            else if ((u1State == OSPFV3_IFS_DR) ||
                     (u1State == OSPFV3_IFS_BACKUP) ||
                     (u1State == OSPFV3_IFS_DR_OTHER))
            {                    /* Intimate about multiple interfaces over the link to IPv6 */

                V3OspfSendMultIfOnLink (pInterface, IPV6_ACTIVE_ADD, 0);

            }

        }

        if ((u1State == OSPFV3_IFS_DOWN) || (u1State == OSPFV3_IFS_DR)
            || (u1State == OSPFV3_IFS_BACKUP)
            || (u1State == OSPFV3_IFS_DR_OTHER)
            || (u1State == OSPFV3_IFS_STANDBY))
        {                        /* Update the Active Detect Tmr flag to be 
                                 * OFF when status got updated */
            if (pInterface->u1ActiveDetectTmrFlag !=
                OSPFV3_MULTIF_ACTIVE_TMR_ON)
            {

                pInterface->u1ActiveDetectTmrFlag =
                    OSPFV3_MULTIF_ACTIVE_TMR_OFF;
            }
        }

        /* Reset the Active Interface Id in the Standby Interface when active
         *  is Down */

        if (u1State == OSPFV3_IFS_WAITING)
        {
            pInterface->u4MultActiveIfId = pInterface->u4InterfaceId;
        }

        if (u1State == OSPFV3_IFS_STANDBY)
        {                        /* When a interface is elected as Standby over the 
                                   link, then update the Router LSA */

            pStdNode = NULL;
            TMO_SLL_Scan (&pInterface->nbrsInIf, pStdNode, tTMO_SLL_NODE *)
            {
                pStandbyLst = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                   nextNbrInIf, pStdNode);
                if (pStandbyLst->u1NbrIfStatus == OSPFV3_ACTIVE_ON_LINK)
                {
                    break;
                }
            }

            if (pStandbyLst != NULL)
            {

                if (V3IsTransitLink (pStandbyLst->pActiveInterface)
                    == OSIX_TRUE)
                {
                    V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                                   &(pStandbyLst->pActiveInterface->
                                     linkStateId), NULL, OSIX_FALSE);
                }
            }

        }

        if (u1PrevState == OSPFV3_IFS_DOWN)
        {
            pInterface->pArea->u4ActIntCount++;
            /* The Area is becoming actively attached area */
            if (pInterface->pArea->u4ActIntCount == 1)
            {
                pInterface->pArea->pV3OspfCxt->u4ActiveAreaCount++;
                pInterface->pArea->bNewlyAttached = OSIX_TRUE;

                if (!OSPFV3_IS_VIRTUAL_IFACE (pInterface))
                {
                    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                        (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                         OSPFV3_GR_NONE))
                    {
                        V3GenerateLsasForExtRoutesInCxt
                            (pInterface->pArea->pV3OspfCxt);
                    }

                    if (pInterface->pArea->u4AreaType != OSPFV3_NSSA_AREA)
                    {
                        pInterface->pArea->stubDefaultCost.u4MetricType =
                            OSPFV3_METRIC;
                    }
                    else if (pInterface->pArea->u1SummaryFunctionality ==
                             OSPFV3_NO_AREA_SUMMARY)
                    {
                        pInterface->pArea->stubDefaultCost.
                            u4MetricType = OSPFV3_METRIC;
                    }

                }
            }
        }
        else if (u1State == OSPFV3_IFS_DOWN)
        {
            pInterface->pArea->u4ActIntCount--;
            /* The Area is becoming configured area from
             * actively attached area */
            if (pInterface->pArea->u4ActIntCount == 0)
            {
                pInterface->pArea->pV3OspfCxt->u4ActiveAreaCount--;
                pInterface->pArea->bNewlyAttached = OSIX_FALSE;
            }
        }

        if (((pInterface->u1IsmState == OSPFV3_IFS_DR) ||
             (pInterface->u1IsmState == OSPFV3_IFS_BACKUP)) &&
            ((u1PrevState != OSPFV3_IFS_DR) &&
             (u1PrevState != OSPFV3_IFS_BACKUP)) &&
            (!OSPFV3_IS_VIRTUAL_IFACE (pInterface)))
        {
            V3OspfJoinMcastGroup (&gV3OsAllDRtrs, pInterface->u4InterfaceId,
                                  pInterface->pArea->pV3OspfCxt->u4ContextId);
        }

        if (((u1PrevState == OSPFV3_IFS_DR) ||
             (u1PrevState == OSPFV3_IFS_BACKUP)) &&
            ((pInterface->u1IsmState != OSPFV3_IFS_DR) &&
             (pInterface->u1IsmState != OSPFV3_IFS_BACKUP)) &&
            (!OSPFV3_IS_VIRTUAL_IFACE (pInterface)))
        {
            V3OspfLeaveMcastGroup (&gV3OsAllDRtrs, pInterface->u4InterfaceId,
                                   pInterface->pArea->pV3OspfCxt->u4ContextId);
        }

        OSPFV3_COUNTER_OP (pInterface->u4IfEvents, 1);
        if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
            (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
             OSPFV3_GR_NONE))
        {
            /* lsa can be regenerated only if router is not in GR state */
            if ((pInterface->pArea->pV3OspfCxt->admnStat == OSPFV3_ENABLED) &&
                ((u1PrevState == OSPFV3_IFS_DOWN)
                 || (u1State == OSPFV3_IFS_DOWN)
                 || (u1State == OSPFV3_IFS_LOOP_BACK)))
            {
                V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                                       OSPFV3_SIG_IF_STATE_CHANGE,
                                       (UINT1 *) pInterface);
            }
        }

        if ((pInterface->pArea->pV3OspfCxt->admnStat == OSPFV3_ENABLED) &&
            ((u1State == OSPFV3_IFS_DOWN)) && (pInterface->u4NbrFullCount > 0))
        {
            V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                           &pInterface->linkStateId, NULL, OSIX_FALSE);
        }
        /* Before interface cleanup send all the LSA's present */
        if (pInterface->delLsAck.u2CurrentLen > 0)
        {
            V3LakSendDelayedAck (pInterface);
        }
        if (pInterface->lsUpdate.u2CurrentLen > 0)
        {
            V3LsuSendLsu (NULL, pInterface);
        }

        if ((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ||
            (pInterface->u1NetworkType == OSPFV3_IF_PTOP))
        {
            if ((pInterface->u1IsmState == OSPFV3_IFS_DOWN) &&
                (u1PrevState != OSPFV3_IFS_DOWN))
            {
                V3OspfLeaveMcastGroup (&(gV3OsAllSpfRtrs),
                                       pInterface->u4InterfaceId,
                                       pInterface->pArea->pV3OspfCxt->
                                       u4ContextId);
            }
        }

        V3ExtrtLsaPolicyHandlerInCxt (pInterface->pArea->pV3OspfCxt);

        /* Send the information to the standby node */
        O3RedDynSendIntfInfoToStandby (pInterface, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfUpdateState\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfCleanup                                                */
/*                                                                           */
/* Description  : Cleans up the specified interface.                         */
/*                                                                           */
/* Input        : pInterface      :  interface to be cleaned                 */
/*                u1CleanupFlag   :  flag                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfCleanup (tV3OsInterface * pInterface, UINT1 u1CleanupFlag)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfCleanup\n");

    /* Reset Variables */
    V3IsmResetVariables (pInterface);
    pPrevNode = NULL;

    while ((pNbrNode = TMO_SLL_Next (&(pInterface->nbrsInIf), pPrevNode))
           != NULL)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        pNbr->bIsBfdDisable = OSIX_FALSE;

        if (u1CleanupFlag == OSPFV3_IF_CLEANUP_DOWN)
        {
            if (pNbr->u1NbrIfStatus == OSPFV3_NORMAL_NBR)
            {
                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_LL_DOWN);
            }
            pPrevNode = pNbrNode;
        }
        else if (u1CleanupFlag == OSPFV3_IF_CLEANUP_DISABLE)
        {
            if (pNbr->u1ConfigStatus == OSPFV3_DISCOVERED_NBR)
            {
                /* V3NbrUpdateState is called to take care of the Full Nbr 
                 * count in Area Structure. Router LSA generation is based on
                 * this Count only. */

                V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                V3NbrDelete (pNbr);
            }
            else if (pNbr->u1ConfigStatus == OSPFV3_DISCOVERED_SBYIF)
            {
                pNbr->pActiveInterface = NULL;
                pNbr->u1NbrIfStatus = OSPFV3_DOWN_ON_LINK;
                V3NbrDelete (pNbr);

            }
            else
            {
                OSPFV3_GENERATE_NBR_EVENT (pNbr, OSPFV3_NBRE_LL_DOWN);
                pPrevNode = pNbrNode;
            }
        }
        else if (u1CleanupFlag == OSPFV3_IF_CLEANUP_DELETE)
        {
            /* NbrUpdateState is called to take care of the Full Nbr 
             * count in Area Structure. Router LSA generation is based on
             * this Count only. */
            if (pNbr->u1NbrIfStatus == OSPFV3_NORMAL_NBR)
            {
                V3NbrUpdateState (pNbr, OSPFV3_NBRS_DOWN);
                V3NbrDelete (pNbr);
            }
            else
            {
                pPrevNode = pNbrNode;
            }
        }
        else
        {
            if (pNbr->u1NbrIfStatus != OSPFV3_NORMAL_NBR)
            {
                pNbr->pActiveInterface = NULL;
                pNbr->u1NbrIfStatus = OSPFV3_DOWN_ON_LINK;
                V3NbrDelete (pNbr);

            }
            else
            {
                pPrevNode = pNbrNode;

            }

        }
    }
    if (u1CleanupFlag != OSPFV3_IF_SBY_CLEANUP_DELETE)
    {
        /* Disable Interface Timers */
        V3IsmDisableIfTimers (pInterface);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfCleanup\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetAreaId                                              */
/*                                                                           */
/* Description  : This routine is invoked to associate the interface to      */
/*                a area.                                                    */
/*                                                                           */
/* Input        : pInterface   : pointer to the interface which has to       */
/*                               associated.                                 */
/*                pAreaId      : pointer to areaId to which the interface    */
/*                               has to associated.                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfSetAreaId (tV3OsInterface * pInterface, tV3OsAreaId * pAreaId)
{
    tV3OsArea          *pNewArea = NULL;
    UINT1               u1UpFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetAreaId\n");

    pNewArea = V3GetFindAreaInCxt (pInterface->pArea->pV3OspfCxt, pAreaId);

    if (pNewArea == NULL)
    {
        return;
    }

    V3ResetRtrLsaLsId ((UINT1 *) pInterface, 0, OSIX_TRUE);

    /* Bring interface down */
    if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
    {
        V3IfDown (pInterface);
        u1UpFlag = OSIX_TRUE;
    }
    else
    {
        u1UpFlag = OSIX_FALSE;
    }

    /* Remove interface from current area
     * add interface to new area
     */
    TMO_SLL_Delete (&(pInterface->pArea->ifsInArea),
                    &(pInterface->nextIfInArea));

    if (TMO_SLL_Count (&(pInterface->pArea->ifsInArea)) == 0)
    {
        V3LsuDeleteAreaScopeLsa (pInterface->pArea);
    }

    TMO_SLL_Add (&(pNewArea->ifsInArea), &(pInterface->nextIfInArea));
    pInterface->pArea = pNewArea;

    V3SetRtrLsaLsId ((UINT1 *) pInterface, 0, OSIX_TRUE);
    /* Bring interface up */
    if (u1UpFlag == OSIX_TRUE)
    {
        V3IfUp (pInterface);
    }
    else
    {
        /* In the case of IBM Type ABR, the interface in DOWN state in the 
         * backbone area will have effect on the ABR status. 
         * So if the interface area-id is changing from backbone to non 
         * backbone area or from non backbone to backbone area, in IBM Type 
         * ABR we need to call V3RtrHandleABRStatChng function. */
        if ((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
             OSPFV3_GR_NONE)
            && (pInterface->pArea->pV3OspfCxt->u1ABRType == OSPFV3_IBM_ABR))
        {
            V3RtrHandleABRStatChngInCxt (pInterface->pArea->pV3OspfCxt);
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetAreaId\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetAdmnStat                                            */
/*                                                                           */
/* Description  : This routine is used to enable/disable the interface.      */
/*                                                                           */
/* Input        : pInterface    : pointer to interface.                      */
/*                u1Status      : adminstatus value.                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfSetAdmnStat (tV3OsInterface * pInterface, UINT1 u1Status)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetAdmnStat\n");

    switch (u1Status)
    {
        case OSPFV3_ENABLED:
            if (pInterface->admnStatus != OSPFV3_ENABLED)
            {
                pInterface->admnStatus = OSPFV3_ENABLED;
                V3IfUp (pInterface);
            }
            break;

        case OSPFV3_DISABLED:
            if (pInterface->admnStatus == OSPFV3_ENABLED)
            {
                pInterface->admnStatus = OSPFV3_DISABLED;
                V3IfDown (pInterface);
            }
            break;

        default:
            break;
            /* end of switch */
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetAdmnStat\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetOperStat                                            */
/*                                                                           */
/* Description  : This routine is invoked to set the operational status of   */
/*                Interface.                                                 */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface.                  */
/*                u1Status      : operational status value.                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfSetOperStat (tV3OsInterface * pInterface, UINT1 u1Status)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetOperStat\n");

    switch (u1Status)
    {
        case OSPFV3_ENABLED:
            pInterface->operStatus = u1Status;
            V3IfUp (pInterface);
            break;

        case OSPFV3_DISABLED:
            pInterface->operStatus = u1Status;
            V3IfDown (pInterface);
            break;

        case OSPFV3_LOOPBACK_IND:
            pInterface->operStatus = u1Status;
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_LOOP_IND);
            break;

        case OSPFV3_UNLOOP_IND:
            pInterface->operStatus = u1Status;
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UNLOOP_IND);
            break;

        default:
            break;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetOperStat\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetType                                                */
/*                                                                           */
/* Description  : This routine is invoked to associate the Network type to   */
/*                an Interface.                                              */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface                   */
/*                u1Type        : Network type                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfSetType (tV3OsInterface * pInterface, UINT1 u1Type)
{
    UINT1               u1UpFlag = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetType\n");

    /* Bring the interface down */
    if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
    {
        V3IfDown (pInterface);
        u1UpFlag = OSIX_TRUE;
    }

    pInterface->u1NetworkType = u1Type;

    /* Bring interface up */
    if (u1UpFlag == OSIX_TRUE)
    {
        if ((u1Type != OSPFV3_IFS_PTOP) && (u1Type != OSPFV3_IF_PTOMP))
        {
            pInterface->bLinkLsaSuppress = OSIX_FALSE;
        }
        V3IfUp (pInterface);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetType\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetDemand                                              */
/*                                                                           */
/* Description  : This routine is invoked to associate the interface to      */
/*                demand circuit                                             */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface which has to      */
/*                                associated.                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
V3IfSetDemand (tV3OsInterface * pInterface, UINT1 u1DemandValue)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetDemand\n");

    /* Bring the interface down */
    if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
    {
        V3IfDown (pInterface);
    }

    pInterface->bDcEndpt = u1DemandValue;

    if (pInterface->bDcEndpt == OSIX_TRUE)
    {
        OSPFV3_OPT_SET (pInterface->ifOptions, OSPFV3_DC_BIT_MASK);
    }
    else
    {
        OSPFV3_OPT_CLEAR_ALL (pInterface->ifOptions);
    }

    if (pInterface->u1IsmState == OSPFV3_IFS_DOWN)
    {
        /* Bring interface up */
        V3IfUp (pInterface);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetDemand\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetPassive                                             */
/*                                                                           */
/* Description    This function makes an interface as Passive.               */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface which has to      */
/*                                be made passive                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
V3IfSetPassive (tV3OsInterface * pInterface)
{
    UINT2               u2HelloInterval = 0;
    INT4                i4RetStat = OSIX_SUCCESS;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetPassive\n");

    if (pInterface->bPassive == OSIX_TRUE)
    {
        V3TmrDeleteTimer (&(pInterface->helloTimer));
        if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
            (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface)))
        {
            V3TmrDeleteTimer (&(pInterface->pollTimer));
        }
    }
    else
    {
        u2HelloInterval = pInterface->u2HelloInterval;

        /* The jitter is applicable for demand circuit interface */
        if (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface))
        {
            u2HelloInterval = (UINT2) V3UtilJitter (u2HelloInterval,
                                                    OSPFV3_JITTER);
        }

        if ((pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
            (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface)))
        {
            if (V3TmrSetTimer (&(pInterface->pollTimer), OSPFV3_POLL_TIMER,
                               (OSPFV3_NO_OF_TICKS_PER_SEC *
                                (pInterface->u4PollInterval)))
                == (INT4) TMR_FAILURE)
            {
                i4RetStat = OSIX_FAILURE;
            }
        }

        if (V3TmrSetTimer (&(pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                           OSPFV3_NO_OF_TICKS_PER_SEC * (u2HelloInterval))
            == (INT4) TMR_FAILURE)
        {
            i4RetStat = OSIX_FAILURE;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfSetPassive\n");

    return i4RetStat;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfAllPassive                                             */
/*                                                                           */
/* Description    This function scan all the interface and                   */
/*                make the pInterface->bPassive as true or false             */
/*                depend upon the incoming value.                            */
/*                                                                           */
/* Input        : pOspfCxt      : pointer to the context which has to        */
/*                                be made passive                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfAllPassive (UINT1 u1Ospfv3DefaultPassiveInterface)
{
    UINT1               u1UpFlag = OSPFV3_FALSE;
    tV3OsInterface     *pInterface;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3IfAllPassive \n");
    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);

    if (pInterface != NULL)
    {
        do
        {
            /* Bring the interface down */
            if (pInterface->u1IsmState != OSPFV3_IFS_DOWN)
            {
                V3IfDown (pInterface);
                u1UpFlag = OSPFV3_TRUE;
            }

            pInterface->bPassive = u1Ospfv3DefaultPassiveInterface;

            /* Bring interface up */
            if (u1UpFlag == OSPFV3_TRUE)
            {
                V3IfUp (pInterface);
            }
        }
        while ((pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface,
                                                  NULL)) != NULL);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3IfAllPassive \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetLinkLSASuppression                                  */
/*                                                                           */
/* Description    This function flushes the link lsa from the                */
/*                Interface database  if the Link LSA suppression is enabled */
/*                over pInterface or Generated a Link Lsa if the suppression */
/*                is disabled                                                */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface to                */
/*                                which link lsa has to be suppressed        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfSetLinkLSASuppression (tV3OsInterface * pInterface)
{
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsLsaInfo       *pLsaInfo = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfSetLinkLSASuppression\n");

    OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId, pInterface->u4InterfaceId);

    if (pInterface->bLinkLsaSuppress == OSPFV3_TRUE)
    {

        if ((pLsaInfo =
             V3LsuSearchDatabase (OSPFV3_LINK_LSA, &(tmpLinkStateId),
                                  &(pInterface->pArea->pV3OspfCxt->
                                    rtrId), pInterface, NULL)) != NULL)
        {
            /* Link lsa has to be flushed out of the neighbors database */
            V3AgdFlushOut (pLsaInfo);
        }
    }

    else
    {
        /* Generate Link Lsa for the specified link */
        V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                       &tmpLinkStateId, (UINT1 *) pInterface, OSIX_FALSE);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT :V3IfSetLinkLSASuppression\n");

    return;
}

/*****************************************************************************/
/* Function     : V3IfParamChange                                            */
/*                                                                           */
/* Description  : This routine generates the router LSa and intra area       */
/*                prefix LSA corresponding to the change in the interface    */
/*                parameter.                                                 */
/*                                                                           */
/* Input        : pInterface  :  Pointer interface whose parameter has       */
/*                               changed                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*****************************************************************************/
PUBLIC VOID
V3IfParamChange (tV3OsInterface * pInterface, UINT1 u1UpdateFlag)
{
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfParamChange\n");
    if (pInterface->u1IsmState == OSPFV3_IFS_STANDBY)
    {
        if (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_ON)
        {
            TmrStopTimer (gV3OsRtr.timerLstId,
                          &((&(pInterface->MultIfActiveDetectTimer))->
                            timerNode));

        }

        pInterface->u1ActiveDetectTmrFlag = OSPFV3_MULTIF_ACTIVE_TMR_OFF;
        OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UP);
        OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);
        OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3IfParamChange\n");
        return;

    }
    if (u1UpdateFlag == OSPFV3_UPDATE_METRIC)
    {
        if ((pInterface->u1SdbyActCount > OSPFV3_DEF_IFOVERLINK_COUNT) ||
            (pInterface->u1ActiveDetectTmrFlag == OSPFV3_MULTIF_ACTIVE_TMR_ON))
        {
            if (pInterface->u1ActiveDetectTmrFlag ==
                OSPFV3_MULTIF_ACTIVE_TMR_ON)
            {
                TmrStopTimer (gV3OsRtr.timerLstId,
                              &((&(pInterface->MultIfActiveDetectTimer))->
                                timerNode));
            }

            pInterface->u1ActiveDetectTmrFlag = OSPFV3_MULTIF_ACTIVE_TMR_OFF;

            V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);
            V3IfUpdateState (pInterface, OSPFV3_IFE_DOWN);
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_IFE_UP);
            OSPFV3_GENERATE_IF_EVENT (pInterface, OSPFV3_MULTIF_LINK);
        }

        /* If the interface is down, If this router is helper for any neighbor
         * Stop the helper process */
        if ((pInterface->pArea->pV3OspfCxt->
             u1HelperSupport & OSPFV3_GRACE_HELPER_ALL) &&
            (pInterface->pArea->pV3OspfCxt->u1HelperStatus == OSPFV3_GR_HELPING)
            && (pInterface->pArea->pV3OspfCxt->u1StrictLsaCheck ==
                OSIX_ENABLED))
        {
            TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->sortNbrLst),
                          pNbrNode, tTMO_SLL_NODE *)
            {
                pNbr =
                    OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pNbrNode);
                if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
                {
                    O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                }
            }
            TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                          pLstNode, tTMO_SLL_NODE *)
            {
                pInterface =
                    OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                         pLstNode);
                if ((pNbrNode =
                     TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
                {
                    pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                                nextNbrInIf, pNbrNode);
                    if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
                    {
                        O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                    }
                }
            }
        }
    }
    if (((pInterface->u1NetworkType == OSPFV3_IF_BROADCAST) ||
         (pInterface->u1NetworkType == OSPFV3_IF_NBMA)) &&
        (pInterface->u4NbrFullCount > 0))
    {
        V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                       &pInterface->linkStateId, NULL, OSIX_FALSE);
    }
    else if ((pInterface->u1NetworkType == OSPFV3_IF_PTOP) ||
             (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
    {
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                V3GenerateLsa (pInterface->pArea, OSPFV3_ROUTER_LSA,
                               &pInterface->linkStateId, NULL, OSIX_FALSE);
                break;
            }
        }
    }

    V3GenIntraAreaPrefixLsa (pInterface->pArea);

    V3RtcCalculateLocalRoutes (pInterface, OSPFV3_UPDATED);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IfParamChange\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfPrefixAdd                                              */
/*                                                                           */
/* Description    This function adds an IPv6 Address prefix into an interface*/
/*                structure                                                  */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface on which the      */
/*                                prefix has to be added                     */
/*                pIp6Addr      : IPv6 Address                               */
/*                u1PrefixLen   : Prefix Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : INT1                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT1
V3IfPrefixAdd (tV3OsInterface * pInterface, tIp6Addr * pIp6Addr,
               UINT1 u1PrefixLen)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tV3OsArea          *pArea = NULL;
    UINT1               u1GenArea = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfPrefixAdd\n");
    OSPFV3_PREFIX_ALLOC (&(pPrefixNode));
    if (NULL == pPrefixNode)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for prefix\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "Prefix Alloc Failure\n");
        gu4V3IfPrefixAddFail++;
        return (OSIX_FAILURE);
    }
    else
    {
        OSPFV3_IP6_ADDR_COPY (pPrefixNode->prefixInfo.addrPrefix, *pIp6Addr);

        if (pInterface->u4AddrlessIf != 0)
        {
            pPrefixNode->prefixInfo.u1PrefixLength = OSPFV3_MAX_PREFIX_LEN;
        }
        else
        {
            pPrefixNode->prefixInfo.u1PrefixLength = (UINT1) u1PrefixLen;
        }
        pPrefixNode->prefixInfo.u1PrefixOpt = 0;
        TMO_SLL_Add (&(pInterface->ip6AddrLst), &(pPrefixNode->nextPrefixNode));
        if (pInterface->u1IsmState > OSPFV3_IFS_DOWN)
        {
            V3RtcCalculateLocalRoute (pInterface, pPrefixNode, OSPFV3_CREATED);
        }
    }

    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
        (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState == OSPFV3_GR_NONE))
    {
        /* If the router is performing GR, then it should not originate 
         * External or NSSA lsas */
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->areasLst), pArea,
                      tV3OsArea *)
        {
            if ((pArea->u4AreaType == OSPFV3_NORMAL_AREA) &&
                (TMO_SLL_Count (&(pArea->ifsInArea)) != 0) &&
                (u1GenArea == OSIX_FALSE))

            {
                V3GenExtLsaWithFwdAddr (pInterface, pIp6Addr, u1PrefixLen);
                u1GenArea = OSIX_TRUE;
            }
            else if ((pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                     (TMO_SLL_Count (&(pArea->ifsInArea)) != 0))
            {
                V3GenNssaLsaWithNonZeroFwdAddr (pArea, pIp6Addr, u1PrefixLen);
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IfPrefixAdd\n");
    return (OSIX_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfPrefixDelete                                           */
/*                                                                           */
/* Description    This function removed an IPv6 Address prefix into an       */
/*                interface structure                                        */
/*                                                                           */
/* Input        : pInterface    : pointer to the interface from which the    */
/*                                prefix has to be deleted                   */
/*                pIp6Addr      : IPv6 Address                               */
/*                u1PrefixLen   : Prefix Length                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
V3IfPrefixDelete (tV3OsInterface * pInterface, tIp6Addr * pIp6Addr,
                  UINT1 u1PrefixLen)
{
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tTMO_SLL_NODE      *pAddrInfoNode = NULL;
    tV3OsArea          *pArea = NULL;
    UINT1               u1GenArea = OSIX_FALSE;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfPrefixDelete\n");
    /* If the interface is down, If this router is helper for any neighbor
     * Stop the helper process */
    if ((pInterface->pArea->pV3OspfCxt->
         u1HelperSupport & OSPFV3_GRACE_HELPER_ALL) &&
        (pInterface->pArea->pV3OspfCxt->u1HelperStatus == OSPFV3_GR_HELPING) &&
        (pInterface->pArea->pV3OspfCxt->u1StrictLsaCheck == OSIX_ENABLED))
    {
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->sortNbrLst),
                      pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pNbrNode);
            if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
            {
                O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
            }
        }
        TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->virtIfLst),
                      pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                              pLstNode);
            if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf))) != NULL)
            {
                pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor,
                                            nextNbrInIf, pNbrNode);
                if (pNbr->u1NbrHelperStatus == OSPFV3_GR_HELPING)
                {
                    O3GrExitHelper (OSPFV3_HELPER_TOP_CHG, pNbr);
                }
            }
        }
    }

    TMO_SLL_Scan (&(pInterface->ip6AddrLst), pAddrInfoNode, tTMO_SLL_NODE *)
    {
        pPrefixNode = OSPFV3_GET_BASE_PTR (tV3OsPrefixNode, nextPrefixNode,
                                           pAddrInfoNode);

        if (pPrefixNode->prefixInfo.u1PrefixLength == u1PrefixLen)
        {
            if (V3UtilIp6PrefixComp (&pPrefixNode->prefixInfo.addrPrefix,
                                     pIp6Addr, u1PrefixLen) == OSPFV3_EQUAL)
            {
                TMO_SLL_Delete (&(pInterface->ip6AddrLst),
                                &(pPrefixNode->nextPrefixNode));

                if (pInterface->u1IsmState > OSPFV3_IFS_DOWN)
                {
                    V3RtcCalculateLocalRoute (pInterface, pPrefixNode,
                                              OSPFV3_DELETED);
                }

                if (TMO_SLL_Count (&(pInterface->pArea->pV3OspfCxt->virtIfLst))
                    != 0)
                {
                    if (V3RtcFindRtEntryInCxt
                        (pInterface->pArea->pV3OspfCxt,
                         (VOID *)
                         &(pPrefixNode->prefixInfo.
                           addrPrefix), OSPFV3_MAX_PREFIX_LEN,
                         OSPFV3_DEST_NETWORK) != NULL)
                    {
                        V3RtcCalculateLocalHostRouteInCxt
                            (pInterface->pArea->pV3OspfCxt,
                             &(pPrefixNode->prefixInfo.
                               addrPrefix), NULL, 0, OSPFV3_DELETED);
                    }
                }

                OSPFV3_PREFIX_FREE (pPrefixNode);
                break;
            }
        }
    }

    TMO_SLL_Scan (&(pInterface->pArea->pV3OspfCxt->areasLst), pArea,
                  tV3OsArea *)
    {
        if ((pArea->u4AreaType == OSPFV3_NORMAL_AREA) &&
            (TMO_SLL_Count (&(pArea->ifsInArea)) != 0)
            && (u1GenArea == OSIX_FALSE))

        {
            V3GenExtLsaWithFwdAddr (pInterface, pIp6Addr, u1PrefixLen);
            u1GenArea = OSIX_TRUE;
        }
        else if ((pArea->u4AreaType == OSPFV3_NSSA_AREA) &&
                 (TMO_SLL_Count (&(pArea->ifsInArea)) != 0))
        {

            V3GenNssaLsaWithZeroFwdAddr (pArea, pIp6Addr, u1PrefixLen);
        }
    }

    if ((pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
         OSPFV3_GR_RESTART)
        && (pInterface->pArea->pV3OspfCxt->u1RestartExitReason ==
            OSPFV3_RESTART_INPROGRESS))
    {
        /* Exit GR with exit reason as TOPOLOGY change */
        O3GrExitGracefulRestartInCxt (pInterface->pArea->pV3OspfCxt,
                                      OSPFV3_RESTART_TOP_CHG);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT: V3IfPrefixDelete\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDemandProbeOpen                                        */
/*                                                                           */
/* Description  : This function starts the demand probing for the specific   */
/*                interface                                                  */
/*                                                                           */
/* Input        : pInterface   : Pointer to the interface to be used to      */
/*                               start probing                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfDemandProbeOpen (tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDemandProbeOpen\n");

    if (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface))
    {
        /* for all the neighbor over demand circuit */
        TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

            if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
            {
                V3TmrSetTimer (&(pInterface->nbrProbeTimer),
                               OSPFV3_NBR_PROBE_INTERVAL_TIMER,
                               (OSPFV3_NO_OF_TICKS_PER_SEC *
                                (pInterface->u4NbrProbeInterval)));
                break;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfDemandProbeOpen\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDemandProbeClose                                       */
/*                                                                           */
/* Description  : This function stops the demand probing for the specific    */
/*                interface                                                  */
/*                                                                           */
/* Input        : pInterface   : Pointer to the interface to be used to      */
/*                               stop probing                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfDemandProbeClose (tV3OsInterface * pInterface)
{
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDemandProbeClose\n");

    if (OSPFV3_IS_DC_EXT_APPLICABLE_IF (pInterface))
    {
        V3TmrDeleteTimer (&(pInterface->nbrProbeTimer));
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "EXIT : V3IfDemandProbeClose\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfSetMtuSize                                             */
/*                                                                           */
/* Description  : This function is invoked when we receive indication either */
/*                from lower levels or the network management the change in  */
/*                the MTU Size value.                                        */
/*                                                                           */
/* Input        : u4IfIndex   -  Interface Index                             */
/*                u4MtuSize   -  Interface MTU size                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SUCCESS/FAILURE.                                           */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
V3IfSetMtuSize (UINT4 u4IfIndex, UINT4 u4MtuSize)
{
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IfSetMtuSize\n");

    if ((pInterface = V3GetFindIf (u4IfIndex)) != NULL)
    {
        pInterface->u4MtuSize = u4MtuSize;
        return OSIX_SUCCESS;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3IfAllPassive \n");
    return OSIX_FAILURE;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : V3IfAddrChgHdlr                                     */
/*                                                                         */
/*     Description   : Function for handling interface address changes.    */
/*                     Whenever IPv6 senses change in Iface address        */
/*                     configurations, it intimates OSPFv3 by calling this */
/*                     function.                                           */
/*                                                                         */
/*     Input(s)      : pAddressChange  -  Pointer to the tNetIpv6AddrChange*/
/*                                        structure, which contains all the*/
/*                                        interface address change         */
/*                                        information.                     */
/*                                                                         */
/*     Output(s)     : None.                                               */
/*                                                                         */
/*     Returns       : VOID.                                               */
/*                                                                         */
/***************************************************************************/

PUBLIC VOID
V3IfAddrChgHdlr (tNetIpv6AddrChange * pAddressChange)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsLinkStateId    tmpLinkStateId;
    tV3OsPrefixNode    *pPrefixNode = NULL;
    tTMO_SLL_NODE      *pAddrInfoNode = NULL;
    UINT1               u1IsPrefixTobeAdded = OSIX_TRUE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: V3IfAddrChgHdlr\n");

    if (IS_ADDR_MULTI (pAddressChange->Ipv6AddrInfo.Ip6Addr))
    {
        return;
    }

    if ((pInterface = V3GetFindIf (pAddressChange->u4Index)) == NULL)
    {
        return;
    }

    switch (pAddressChange->u4Mask)
    {

        case NETIPV6_ADDRESS_ADD:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Addr Add Notification\n");

            if (IS_ADDR_LLOCAL (pAddressChange->Ipv6AddrInfo.Ip6Addr))
            {
                if (V3UtilIp6AddrComp
                    (&pInterface->ifIp6Addr,
                     &pAddressChange->Ipv6AddrInfo.Ip6Addr) != OSPFV3_EQUAL)
                {
                    OSPFV3_IP6_ADDR_COPY (pInterface->ifIp6Addr,
                                          pAddressChange->Ipv6AddrInfo.Ip6Addr);
                    OSPFV3_BUFFER_DWTOPDU (tmpLinkStateId,
                                           pInterface->u4InterfaceId);
                    if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                        (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                         OSPFV3_GR_NONE))
                    {
                        V3GenerateLsa (pInterface->pArea, OSPFV3_LINK_LSA,
                                       &tmpLinkStateId, (UINT1 *) pInterface,
                                       OSIX_FALSE);
                    }
                }
            }
            else
            {
                TMO_SLL_Scan (&(pInterface->ip6AddrLst),
                              pAddrInfoNode, tTMO_SLL_NODE *)
                {
                    pPrefixNode = OSPFV3_GET_BASE_PTR (tV3OsPrefixNode,
                                                       nextPrefixNode,
                                                       pAddrInfoNode);
                    if ((MEMCMP
                         (&pPrefixNode->prefixInfo.addrPrefix,
                          &pAddressChange->Ipv6AddrInfo.Ip6Addr,
                          sizeof (tIp6Addr)) == OSPFV3_EQUAL)
                        && (pPrefixNode->prefixInfo.u1PrefixLength ==
                            (UINT1) pAddressChange->Ipv6AddrInfo.
                            u4PrefixLength))
                    {
                        /* Given Prefix is already present no need to add 
                         * the prefix
                         */
                        u1IsPrefixTobeAdded = OSIX_FALSE;
                        break;
                    }
                }
                if (u1IsPrefixTobeAdded == OSIX_TRUE)
                {

                    if (((V3IfPrefixAdd (pInterface,
                                         &(pAddressChange->Ipv6AddrInfo.
                                           Ip6Addr),
                                         (UINT1) pAddressChange->Ipv6AddrInfo.
                                         u4PrefixLength)) != OSIX_FAILURE)
                        && (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
                        && (pInterface->pArea->pV3OspfCxt->
                            u1Ospfv3RestartState == OSPFV3_GR_NONE))
                    {
                        V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                                               OSPFV3_SIG_IF_PREFIX_ADDED,
                                               (UINT1 *) pInterface);
                    }
                }
            }
            break;

        case NETIPV6_ADDRESS_DELETE:
            OSPFV3_GBL_TRC (CONTROL_PLANE_TRC, "Addr Delete Notification\n");

            V3IfPrefixDelete (pInterface,
                              &(pAddressChange->Ipv6AddrInfo.Ip6Addr),
                              (UINT1) pAddressChange->Ipv6AddrInfo.
                              u4PrefixLength);
            if ((OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE) &&
                (pInterface->pArea->pV3OspfCxt->u1Ospfv3RestartState ==
                 OSPFV3_GR_NONE))
            {
                V3SignalLsaRegenInCxt (pInterface->pArea->pV3OspfCxt,
                                       OSPFV3_SIG_IF_PREFIX_DELETED,
                                       (UINT1 *) pInterface);
            }
            break;

        default:
            break;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: V3IfAddrChgHdlr\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfFindIfAddrFromRouterId                                 */
/*                                                                           */
/* Description  : Function to find the interface address from given router id*/
/*                                                                           */
/* Input        : rtrId                                                      */
/*                pInterface                                                 */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Router id.                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tIp6Addr    *
V3IfFindIfAddrFromRouterId (tV3OsRouterId rtrId, tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pInterface->pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfFindIfAddrFromRouterId\n");

    if (OSPFV3_IS_NULL_RTR_ID (rtrId))
    {
        return NULL;
    }

    if (V3UtilRtrIdComp (rtrId, pInterface->pArea->pV3OspfCxt->rtrId)
        == OSPFV3_EQUAL)
    {

        OSPFV3_TRC (OSPFV3_FN_EXIT, pInterface->pArea->pV3OspfCxt->u4ContextId,
                    "EXIT: V3IfFindIfAddrFromRouterId\n");
        return &(pInterface->ifIp6Addr);

    }

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);
        if (V3UtilRtrIdComp (rtrId, pNbr->nbrRtrId) == OSPFV3_EQUAL)
        {

            OSPFV3_TRC (OSPFV3_FN_EXIT,
                        pInterface->pArea->pV3OspfCxt->u4ContextId,
                        "EXIT: V3IfFindIfAddrFromRouterId\n");
            return &(pNbr->nbrIpv6Addr);
        }
    }
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfGetMetricFromSpeedInCxt                                */
/*                                                                           */
/* Description  : Function to get metric from spped                          */
/*                                                                           */
/* Input        : pV3OspfCxt         : Context pointer                       */
/*                u4IfSpeed                                                  */
/*                u4IfHighSpeed                                              */
/*                                                                           */
/* Output       : Metric Value.                                              */
/*                                                                           */
/* Returns      : Metric Value.                                              */
/*                                                                           */
/*****************************************************************************/
UINT4
V3IfGetMetricFromSpeedInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4IfSpeed,
                             UINT4 u4IfHighSpeed)
{
    UINT4               u4IfMetric = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3IfGetMetricFromSpeedInCxt \n");

    if (u4IfSpeed == OSPFV3_MAX_IF_SPEED)
    {
        if (u4IfHighSpeed != 0)
        {
            /* u4IfHighSpeed is represented in megabits/second
             * and Reference bandwidth is represented in kilobits/second. 
             */
            u4IfMetric = (pV3OspfCxt->u4RefBw /
                          (u4IfHighSpeed * OSPFV3_ONE_KBITS));
        }

    }
    else
    {
        /* To avoid integer overflow the calculation is done in kbps 
         * units. It is assumed that interface speed is atleast 1 kbps.*/
        if (u4IfSpeed >= OSPFV3_ONE_KBITS)
        {
            u4IfMetric = (pV3OspfCxt->u4RefBw / (u4IfSpeed / OSPFV3_ONE_KBITS));
        }
    }

    if (u4IfMetric == 0)
    {
        OSPFV3_GBL_TRC (OSPFV3_CONFIGURATION_TRC,
                        "Iface Metric is zero after metric calculation from speed.\n"
                        "hence setting to one\n");
        u4IfMetric = 1;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3IfGetMetricFromSpeedInCxt\n");

    return u4IfMetric;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilIfGetMetricFromSpeedInCxt                            */
/*                                                                           */
/* Description  : This utility function is show running config. This is      */
/*                avoid printing the below message during show running       */
/*                "Iface Metric is zero after metric calculation from        */
/*                speed.\n"                                                  */
/*                                                                           */
/* Input        : pV3OspfCxt         : Context pointer                       */
/*                u4IfSpeed                                                  */
/*                u4IfHighSpeed                                              */
/*                                                                           */
/* Output       : Metric Value.                                              */
/*                                                                           */
/* Returns      : Metric Value.                                              */
/*                                                                           */
/*****************************************************************************/
UINT4
V3UtilIfGetMetricFromSpeedInCxt (tV3OspfCxt * pV3OspfCxt, UINT4 u4IfSpeed,
                                 UINT4 u4IfHighSpeed)
{
    UINT4               u4IfMetric = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3UtilIfGetMetricFromSpeedInCxt\n");
    if (u4IfSpeed == OSPFV3_MAX_IF_SPEED)
    {
        if (u4IfHighSpeed != 0)
        {
            /* u4IfHighSpeed is represented in megabits/second
             * and Reference bandwidth is represented in kilobits/second. 
             */
            u4IfMetric = (pV3OspfCxt->u4RefBw /
                          (u4IfHighSpeed * OSPFV3_ONE_KBITS));
        }

    }
    else
    {
        /* To avoid integer overflow the calculation is done in kbps 
         * units. It is assumed that interface speed is atleast 1 kbps.*/
        if (u4IfSpeed >= OSPFV3_ONE_KBITS)
        {
            u4IfMetric = (pV3OspfCxt->u4RefBw / (u4IfSpeed / OSPFV3_ONE_KBITS));
        }
    }

    if (u4IfMetric == 0)
    {
        u4IfMetric = 1;
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3UtilIfGetMetricFromSpeedInCxt\n");
    return u4IfMetric;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3IfDeleteAllInterfaceInCxt                                */
/*                                                                           */
/* Description  : This utility scans and deletes all the interfaces in the   */
/*                context                                                    */
/*                                                                           */
/* Input        : pV3OspfCxt         : Context pointer                       */
/*                u4IfSpeed                                                  */
/*                u4IfHighSpeed                                              */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
V3IfDeleteAllInterfaceInCxt (tV3OspfCxt * pV3OspfCxt)
{
    tV3OsInterface     *pInterface = NULL;
    tV3OsInterface      curInterface;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: V3IfDeleteAllInterfaceInCxt\n");
    MEMSET (&curInterface, 0, sizeof (tV3OsInterface));

    while ((pInterface = RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                        (tRBElem *) & curInterface,
                                        NULL)) != NULL)
    {
        MEMCPY (&curInterface, pInterface, sizeof (tV3OsInterface));

        if (pInterface->pArea->pV3OspfCxt->u4ContextId !=
            pV3OspfCxt->u4ContextId)
        {
            continue;
        }

        V3IfDelete (pInterface);
        pInterface = NULL;
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: V3IfDeleteAllInterfaceInCxt\n");
}

/******************************************************************************/
/*                                                                            */
/* Function     : V3IfIsMetricChange                                          */
/*                                                                            */
/* Description  : This utility scans the interface list  & compares           */
/*                the metric of nbr  and its corresponding standby/active if's*/
/*                metric.                                                     */
/*                                                                            */
/* Input        : pNbr         : Pointer the Nbr whose metric has be checked  */
/*                                                                            */
/* Output       : pu4IfMetric - Updates the metric if changes                 */
/*                                                                            */
/* Returns      :  OSIX_TRUE /OSIX_FALSE.                                     */
/*                                                                            */
/******************************************************************************/

PUBLIC INT4
V3IfIsMetricChange (tV3OsNeighbor * pNbr, UINT4 *pu4IfMetric)
{
    tV3OsInterface     *pStandbyInterface = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : V3IfIsMetricChange\n");

    pStandbyInterface = V3IfGetPtrtoInterface (pNbr->pInterface->pArea,
                                               pNbr->u4NbrInterfaceId);
    if (pStandbyInterface != NULL)
    {

        if (pStandbyInterface->u4IfMetric != pNbr->u4NbrIfMetric)
        {                        /* Metric has got changes */

            *pu4IfMetric = pStandbyInterface->u4IfMetric;
            OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : V3IfIsMetricChange\n");
            return OSIX_TRUE;

        }
        else
        {                        /* No changes */

            *pu4IfMetric = pStandbyInterface->u4IfMetric;
            return OSIX_FALSE;
        }
    }

    *pu4IfMetric = OSPFV3_ZERO;
    return OSIX_FALSE;

}

/******************************************************************************/
/*                                                                            */
/* Function     : V3IfGetPtrtoInterface                                       */
/*                                                                            */
/* Description  : This function scans the interface list of the Area          */
/*                 and gets back the pointer to the interface id given as i/p */
/*                                                                            */
/* Input        : pArea         : Pointer to the Area in whose interface list */
/*                                should be scanned                           */
/*                u4InterfaceId : Interface Index Id                          */
/*                                                                            */
/* Output       : None                                                        */
/*                                                                            */
/* Returns      : Pointer to the matched Interface else NULL                  */
/*                                                                            */
/******************************************************************************/

PUBLIC
    tV3OsInterface * V3IfGetPtrtoInterface (tV3OsArea * pArea,
                                            UINT4 u4InterfaceId)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tV3OsInterface     *pInterface = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: V3IfGetPtrtoInterface\n");
    TMO_SLL_Scan (&(pArea->ifsInArea), pNode, tTMO_SLL_NODE *)
    {
        pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea, pNode);
        if (pInterface->u4InterfaceId == u4InterfaceId)
        {
            OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                        "EXIT: V3IfGetPtrtoInterface\n");
            return pInterface;
        }
    }
    return NULL;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : IssHealthChkClearCtrOspf3                                  */
/*                                                                           */
/* Description    This function clears the ospf3 counters.                   */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
Ospf3HealthChkClearCtr ()
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4CxtId = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : Ospf3HealthChkClearCtr\n");
    for (u4CxtId = 0; u4CxtId < OSPFV3_MAX_CONTEXTS_LIMIT; u4CxtId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4CxtId] == NULL)
        {
            continue;
        }
        V3OspfLock ();
        pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4CxtId];
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            pInterface->u4HelloRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpRcvdCount = 0;
            pInterface->u4LsaReqRcvdCount = 0;
            pInterface->u4LsaUpdateRcvdCount = 0;
            pInterface->u4LsaAckRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4DdpTxedCount = 0;
            pInterface->u4LsaReqTxedCount = 0;
            pInterface->u4LsaUpdateTxedCount = 0;
            pInterface->u4LsaAckTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpDisdCount = 0;
            pInterface->u4LsaReqDisdCount = 0;
            pInterface->u4LsaUpdateDisdCount = 0;
            pInterface->u4LsaAckDisdCount = 0;
            pInterface->u4NotFoundSACnt = 0;
            pInterface->u4DigestFailCnt = 0;
            pInterface->u4CrytoSeqMismtchCnt = 0;
            pInterface->u4ATHdrLengthErrCnt = 0;
            pInterface->u4ATAlgoMismtchCnt = 0;
            pInterface->u4AuthUpdateTxedCount = 0;
            pInterface->u4AuthUpdateRcvdCount = 0;
        }

        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        if (pInterface == NULL)
        {
            V3OspfUnLock ();
            return;
        }
        do
        {

            pInterface->u4HelloRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpRcvdCount = 0;
            pInterface->u4LsaReqRcvdCount = 0;
            pInterface->u4LsaUpdateRcvdCount = 0;
            pInterface->u4LsaAckRcvdCount = 0;
            pInterface->u4HelloTxedCount = 0;
            pInterface->u4DdpTxedCount = 0;
            pInterface->u4LsaReqTxedCount = 0;
            pInterface->u4LsaUpdateTxedCount = 0;
            pInterface->u4LsaAckTxedCount = 0;
            pInterface->u4HelloDisdCount = 0;
            pInterface->u4DdpDisdCount = 0;
            pInterface->u4LsaReqDisdCount = 0;
            pInterface->u4LsaUpdateDisdCount = 0;
            pInterface->u4LsaAckDisdCount = 0;
            pInterface->u4NotFoundSACnt = 0;
            pInterface->u4DigestFailCnt = 0;
            pInterface->u4CrytoSeqMismtchCnt = 0;
            pInterface->u4ATHdrLengthErrCnt = 0;
            pInterface->u4ATAlgoMismtchCnt = 0;
            pInterface->u4AuthUpdateTxedCount = 0;
            pInterface->u4AuthUpdateRcvdCount = 0;
        }
        while ((pInterface = (tV3OsInterface *)
                RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                               (tRBElem *) pInterface, NULL)) != NULL);

        V3OspfUnLock ();

    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : Ospf3HealthChkClearCtr\n");
    return;

}

/*******************************************
 Function :  IfOspfv3BfdRegister

 Input    :  tV3OsNeighbor

 Output   :  None

 Returns  :  OSPF_SUCCESS/OSPF_FAILURE
*******************************************/
PUBLIC INT4
IfOspfv3BfdRegister (tV3OsNeighbor * pNbr)
{
#ifdef BFD_WANTED
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT1               au1EventMask[1] = { 0 };

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER :IfOspfv3BfdRegister \n");
    MEMSET (au1EventMask, 0, sizeof (au1EventMask));

    if (pNbr == NULL)
    {
        return OSIX_FAILURE;
    }

    pV3OspfCxt = pNbr->pInterface->pArea->pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return OSIX_FAILURE;
    }
    /* Checking for BFD OSPF status Globally and
     * BFD status on All interfaces or specific interface*/
    if ((pV3OspfCxt->u1BfdAdminStatus == OSPFV3_BFD_ENABLED) &&
        ((pV3OspfCxt->u1BfdStatusInAllIf == OSPFV3_BFD_ENABLED) ||
         (pNbr->pInterface->u1BfdIfStatus == OSPFV3_BFD_ENABLED)))

    {
        /* Filling Ospf info for Registering */
        gBfdInParams.u4ReqType = BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING;
        gBfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_OSPF3;
        gBfdInParams.u4ContextId = pV3OspfCxt->u4ContextId;

        MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
                &pNbr->nbrIpv6Addr, MAX_IPV6_ADDR_LEN);
        MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
                &pNbr->pInterface->ifIp6Addr, MAX_IPV6_ADDR_LEN);

        gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
            IPVX_ADDR_FMLY_IPV6;
        gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
            Ospfv3GetIfIndexFromPort (pNbr->pInterface->u4InterfaceId);
        gBfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

        /* The Desired detection timer is not configurable. hence making
         * this value as detection multipler as BFD 1Sec*/
        gBfdInParams.BfdClntInfoParams.u4DesiredDetectionTime =
            BFD_DEFAULT_TIME_OUT;

        /* Informing status down notification to OSPF Que.
         * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
         * Hence the bit + 1 used to set/check the bit */
        OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
        MEMCPY (&(gBfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

        gBfdInParams.BfdClntInfoParams.pBfdNotifyPathStatusChange =
            Ospfv3HandleNbrPathStatusChange;

        /* BFD API call for registering OSPF Nbr path monitroing */
        if (BfdApiHandleExtRequest (pV3OspfCxt->u4ContextId, &gBfdInParams,
                                    NULL) != OSIX_SUCCESS)
        {
            OSPFV3_TRC (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                        "Bfd Registration failed\n");
            return OSIX_FAILURE;
        }
        else
        {
            pNbr->u1NbrBfdState = OSPFV3_BFD_ENABLED;
        }
    }
#else
    UNUSED_PARAM (pNbr);
#endif
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: IfOspfv3BfdRegister \n");
    return OSIX_SUCCESS;
}

/*******************************************
 Function :  IfOspfv3BfdDeRegister

 Input    :  pNbr

 Output   :  None

 Returns  :  OSPF_SUCCESS/OSPF_FAILURE
*******************************************/
PUBLIC INT4
IfOspfv3BfdDeRegister (tV3OsNeighbor * pNbr, BOOL1 bIsDynDis)
{
#ifdef BFD_WANTED
    tV3OspfCxt         *pV3OspfCxt = NULL;

    if (pNbr == NULL)
    {
        return OSIX_FAILURE;
    }

    pV3OspfCxt = pNbr->pInterface->pArea->pV3OspfCxt;

    if (pV3OspfCxt == NULL)
    {
        return OSIX_FAILURE;
    }

    if (bIsDynDis)
    {
        gBfdInParams.u4ReqType = BFD_CLIENT_DISABLE_FROM_IP_PATH_MONITORING;
    }
    else
    {
        gBfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;
    }

    gBfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_OSPF3;
    gBfdInParams.u4ContextId = pV3OspfCxt->u4ContextId;
    gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
        Ospfv3GetIfIndexFromPort (pNbr->pInterface->u4InterfaceId);

    MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
            &pNbr->nbrIpv6Addr, MAX_IPV6_ADDR_LEN);
    MEMCPY (&gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
            &pNbr->pInterface->ifIp6Addr, MAX_IPV6_ADDR_LEN);
    gBfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
        IPVX_ADDR_FMLY_IPV6;
    gBfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;

    /* API call to Deregister Nbr path monitroing by BFD */
    if (BfdApiHandleExtRequest (pV3OspfCxt->u4ContextId, &gBfdInParams,
                                NULL) != OSIX_SUCCESS)
    {
        OSPFV3_TRC (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                    "Bfd DeRegistration failed\n");
        return OSIX_FAILURE;
    }
    else
    {
        pNbr->u1NbrBfdState = OSPFV3_BFD_DISABLED;
    }
#else
    UNUSED_PARAM (pNbr);
    UNUSED_PARAM (bIsDynDis);
#endif /* BFD_WANTED */

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: IfOspfv3BfdDeRegister\n");

    return OSIX_SUCCESS;
}

/********************************
 Function :  Ospv3fHandleNbrPathStatusChange

 Input    :  u4ContextId
             pNbrPathInfo

 Output   :  None

 Returns  :  OSIX_SUCCESS/OSIX_FAILURE
*******************************************/
#ifdef BFD_WANTED
INT1
Ospfv3HandleNbrPathStatusChange (UINT4 u4ContextId,
                                 tBfdClientNbrIpPathInfo * pNbrPathInfo)
{
    tV3OspfQMsg        *pOspfQMsg;
    tV3OspfCxt         *pV3OspfCxt = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, u4ContextId,
                "ENTER: Ospfv3HandleNbrPathStatusChange\n");

    pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);
    if (pV3OspfCxt == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pNbrPathInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    if (OSPFV3_QMSG_ALLOC (&(pOspfQMsg)) == NULL)
    {
        SYS_LOG_MSG ((OSPFV3_ALERT_LEVEL, OSPFV3_SYSLOG_ID,
                      "Failed to allocate memory for QMsg\n"));

        OSPFV3_TRC (OS_RESOURCE_TRC | OSPFV3_ISM_TRC,
                    pV3OspfCxt->u4ContextId, "QMsg Alloc Failure\n");
        return OSIX_FAILURE;
    }
    pOspfQMsg->u4OspfMsgType = OSPFV3_BFD_NBR_DOWN_EVENT;

    MEMCPY (&(pOspfQMsg->Ospfv3BfdMsgInfo), pNbrPathInfo,
            sizeof (tBfdClientNbrIpPathInfo));

    pOspfQMsg->u4OspfCxtId = pV3OspfCxt->u4ContextId;

    if (OsixQueSend (OSPF3_Q_ID,
                     (UINT1 *) &pOspfQMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        OSPFV3_QMSG_FREE (pOspfQMsg);
        OSPFV3_TRC (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                    "Enqueue To OSPF Failed\n");
        return OSIX_FAILURE;
    }
    if (OsixEvtSend (OSPF3_TASK_ID, OSPFV3_MSGQ_EVENT) != OSIX_SUCCESS)
    {
        OSPFV3_TRC (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                    "Send Evt Failed\n");
        return OSIX_FAILURE;
    }
    OSPFV3_TRC (CONTROL_PLANE_TRC, pV3OspfCxt->u4ContextId,
                "Pkt Enqueued To OSPF\n");

    OSPFV3_TRC (OSPFV3_FN_EXIT, u4ContextId,
                "EXIT: Ospfv3HandleNbrPathStatusChange\n");
    return OSIX_SUCCESS;

}
#endif

/*****************************************************************************/
/*                                                                           */
/* Description  : Function to Get corresponding Interface Index for a port   */
/*                                                                           */
/* Input        : u4Port                                                     */
/*                                                                           */
/* Output       : IfIndex                                                    */
/*                                                                           */
/* Returns      :                                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
Ospfv3GetIfIndexFromPort (UINT4 u4Port)
{
    UINT4               u4CfaIfIndex;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: Ospfv3GetIfIndexFromPort\n");
    /* Get the CfaInterface Index */
    if (NetIpv6GetCfaIfIndexFromPort (u4Port, &u4CfaIfIndex) == NETIPV4_FAILURE)
    {
        return OSIX_FAILURE;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: Ospfv3GetIfIndexFromPort\n");
    return u4CfaIfIndex;
}

/*PopulateOspf3Counters is test code.This function is invoked from ISS.
 *It populates the ospfv3 counters/statistics for verfying the ospfv3 clear counters*/

/*****************************************************************************/
/*                                                                           */
/* Function     : PopulateOspf3Counters                                      */
/*                                                                           */
/* Description    This function populates the ospf3 counters.                */
/*                                                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
PopulateOspf3Counters ()
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4CxtId = 0;
    UINT4               u4Value = 5;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: PopulateOspf3Counters\n");
    for (u4CxtId = 0; u4CxtId < OSPFV3_MAX_CONTEXTS_LIMIT; u4CxtId++)
    {
        if (gV3OsRtr.apV3OspfCxt[u4CxtId] == NULL)
        {
            continue;
        }
        V3OspfLock ();
        pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4CxtId];
        TMO_SLL_Scan (&pV3OspfCxt->virtIfLst, pLstNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface,
                                              nextVirtIfNode, pLstNode);
            pInterface->u4HelloRcvdCount = u4Value;
            pInterface->u4HelloTxedCount = u4Value;
            pInterface->u4HelloDisdCount = u4Value;
            pInterface->u4DdpRcvdCount = u4Value;
            pInterface->u4LsaReqRcvdCount = u4Value;
            pInterface->u4LsaUpdateRcvdCount = u4Value;
            pInterface->u4LsaAckRcvdCount = u4Value;
            pInterface->u4HelloTxedCount = u4Value;
            pInterface->u4DdpTxedCount = u4Value;
            pInterface->u4LsaReqTxedCount = u4Value;
            pInterface->u4LsaUpdateTxedCount = u4Value;
            pInterface->u4LsaAckTxedCount = u4Value;
            pInterface->u4HelloDisdCount = u4Value;
            pInterface->u4DdpDisdCount = u4Value;
            pInterface->u4LsaReqDisdCount = u4Value;
            pInterface->u4LsaUpdateDisdCount = u4Value;
            pInterface->u4LsaAckDisdCount = u4Value;

        }

        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        if (pInterface == NULL)
        {
            V3OspfUnLock ();
            return;
        }
        do
        {

            pInterface->u4HelloRcvdCount = u4Value;
            pInterface->u4HelloTxedCount = u4Value;
            pInterface->u4HelloDisdCount = u4Value;
            pInterface->u4DdpRcvdCount = u4Value;
            pInterface->u4LsaReqRcvdCount = u4Value;
            pInterface->u4LsaUpdateRcvdCount = u4Value;
            pInterface->u4LsaAckRcvdCount = u4Value;
            pInterface->u4HelloTxedCount = u4Value;
            pInterface->u4DdpTxedCount = u4Value;
            pInterface->u4LsaReqTxedCount = u4Value;
            pInterface->u4LsaUpdateTxedCount = u4Value;
            pInterface->u4LsaAckTxedCount = u4Value;
            pInterface->u4HelloDisdCount = u4Value;
            pInterface->u4DdpDisdCount = u4Value;
            pInterface->u4LsaReqDisdCount = u4Value;
            pInterface->u4LsaUpdateDisdCount = u4Value;
            pInterface->u4LsaAckDisdCount = u4Value;

        }
        while ((pInterface = (tV3OsInterface *)
                RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                               (tRBElem *) pInterface, NULL)) != NULL);

        V3OspfUnLock ();

    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: PopulateOspf3Counters\n");
    return;

}

/*-----------------------------------------------------------------------*/
/*                       End of the file o3if.c                          */
/*-----------------------------------------------------------------------*/
