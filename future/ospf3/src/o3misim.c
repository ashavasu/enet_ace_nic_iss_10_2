/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3misim.c,v 1.4 2017/12/26 13:34:28 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include "o3inc.h"
# include "fso3telw.h"
# include "fsmitolw.h"
# include "fsmisolw.h"

/* LOW LEVEL Routines for Table : FsMIOspfv3TestIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3TestIfTable
 Input       :  The Indices
                FsMIOspfv3TestIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3TestIfTable (INT4 i4FsMIOspfv3TestIfIndex)
{
    return (nmhValidateIndexInstanceFutOspfv3TestIfTable
            (i4FsMIOspfv3TestIfIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3TestIfTable
 Input       :  The Indices
                FsMIOspfv3TestIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFirstIndexFsMIOspfv3TestIfTable (INT4 *pi4FsMIOspfv3TestIfIndex)
{
    return (nmhGetFirstIndexFutOspfv3TestIfTable (pi4FsMIOspfv3TestIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3TestIfTable
 Input       :  The Indices
                FsMIOspfv3TestIfIndex
                nextFsMIOspfv3TestIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3TestIfTable (INT4 i4FsMIOspfv3TestIfIndex,
                                      INT4 *pi4NextFsMIOspfv3TestIfIndex)
{
    return (nmhGetNextIndexFutOspfv3TestIfTable (i4FsMIOspfv3TestIfIndex,
                                                 pi4NextFsMIOspfv3TestIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3TestDemandTraffic
 Input       :  The Indices
                FsMIOspfv3TestIfIndex

                The Object 
                retValFsMIOspfv3TestDemandTraffic
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3TestDemandTraffic (INT4 i4FsMIOspfv3TestIfIndex,
                                   INT4 *pi1RetValFsMIOspfv3TestDemandTraffic)
{
    return (nmhGetFutOspfv3TestDemandTraffic (i4FsMIOspfv3TestIfIndex,
                                              pi1RetValFsMIOspfv3TestDemandTraffic));
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3TestIfContextId
 Input       :  The Indices
                FsMIOspfv3TestIfIndex

                The Object 
                retValFsMIOspfv3TestIfContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3TestIfContextId (INT4 i4FsMIOspfv3TestIfIndex,
                                 INT4 *pi1RetValFsMIOspfv3TestIfContextId)
{
    return (nmhGetFsMIStdOspfv3IfContextId (i4FsMIOspfv3TestIfIndex,
                                            pi1RetValFsMIOspfv3TestIfContextId));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3TestDemandTraffic
 Input       :  The Indices
                FsMIOspfv3TestIfIndex

                The Object 
                setValFsMIOspfv3TestDemandTraffic
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3TestDemandTraffic (INT4 i4FsMIOspfv3TestIfIndex,
                                   INT4 i4SetValFsMIOspfv3TestDemandTraffic)
{
    return (nmhSetFutOspfv3TestDemandTraffic (i4FsMIOspfv3TestIfIndex,
                                              i4SetValFsMIOspfv3TestDemandTraffic));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3TestDemandTraffic
 Input       :  The Indices
                FsMIOspfv3TestIfIndex

                The Object 
                testValFsMIOspfv3TestDemandTraffic
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3TestDemandTraffic (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMIOspfv3TestIfIndex,
                                      INT4 i4TestValFsMIOspfv3TestDemandTraffic)
{
    return (nmhTestv2FutOspfv3TestDemandTraffic (pu4ErrorCode,
                                                 i4FsMIOspfv3TestIfIndex,
                                                 i4TestValFsMIOspfv3TestDemandTraffic));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3TestIfTable
 Input       :  The Indices
                FsMIOspfv3TestIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3TestIfTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMIOspfv3ExtRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIOspfv3ExtRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIOspfv3ExtRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                                 INT4
                                                 i4FsMIOspfv3ExtRouteDestType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIOspfv3ExtRouteDest,
                                                 UINT4
                                                 u4FsMIOspfv3ExtRoutePfxLength,
                                                 INT4
                                                 i4FsMIOspfv3ExtRouteNextHopType,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pFsMIOspfv3ExtRouteNextHop)
{
    INT1                i1RetVal = SNMP_FAILURE;

    if ((i4FsMIStdOspfv3ContextId < OSPFV3_DEFAULT_CXT_ID) ||
        ((UINT4) i4FsMIStdOspfv3ContextId >= OSPFV3_MAX_CONTEXTS_LIMIT))
    {
        return SNMP_FAILURE;
    }

    if (V3OspfVcmIsVcExist ((UINT4) i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    i1RetVal =
        nmhValidateIndexInstanceFutOspfv3ExtRouteTable
        (i4FsMIOspfv3ExtRouteDestType, pFsMIOspfv3ExtRouteDest,
         u4FsMIOspfv3ExtRoutePfxLength, i4FsMIOspfv3ExtRouteNextHopType,
         pFsMIOspfv3ExtRouteNextHop);

    V3UtilReSetContext ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIOspfv3ExtRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIOspfv3ExtRouteTable (INT4 *pi4FsMIStdOspfv3ContextId,
                                         INT4 *pi4FsMIOspfv3ExtRouteDestType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIOspfv3ExtRouteDest,
                                         UINT4 *pu4FsMIOspfv3ExtRoutePfxLength,
                                         INT4 *pi4FsMIOspfv3ExtRouteNextHopType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsMIOspfv3ExtRouteNextHop)
{
    UINT4               u4CxtId = 0;
    UINT4               u4PrevCxtId = 0;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfGetFirstCxtId (&u4CxtId) != OSIX_FAILURE)
    {
        i1RetVal = SNMP_SUCCESS;
    }

    if (i1RetVal == SNMP_SUCCESS)
    {
        do
        {
            *pi4FsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext ((UINT4) u4CxtId) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3ExtRouteTable
                (pi4FsMIOspfv3ExtRouteDestType, pFsMIOspfv3ExtRouteDest,
                 pu4FsMIOspfv3ExtRoutePfxLength,
                 pi4FsMIOspfv3ExtRouteNextHopType, pFsMIOspfv3ExtRouteNextHop);
            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                return SNMP_SUCCESS;
            }
            u4PrevCxtId = u4CxtId;
        }
        while (V3UtilOspfGetNextCxtId (u4PrevCxtId, &u4CxtId) != OSIX_FAILURE);
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIOspfv3ExtRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                nextFsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                nextFsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                nextFsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                nextFsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                nextFsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop
                nextFsMIOspfv3ExtRouteNextHop
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIOspfv3ExtRouteTable (INT4 i4FsMIStdOspfv3ContextId,
                                        INT4 *pi4NextFsMIStdOspfv3ContextId,
                                        INT4 i4FsMIOspfv3ExtRouteDestType,
                                        INT4 *pi4NextFsMIOspfv3ExtRouteDestType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3ExtRouteDest,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsMIOspfv3ExtRouteDest,
                                        UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                        UINT4
                                        *pu4NextFsMIOspfv3ExtRoutePfxLength,
                                        INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                        INT4
                                        *pi4NextFsMIOspfv3ExtRouteNextHopType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMIOspfv3ExtRouteNextHop,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsMIOspfv3ExtRouteNextHop)
{
    UINT4               u4CxtId;
    INT1                i1RetVal = SNMP_FAILURE;

    if (V3UtilOspfIsValidCxtId (i4FsMIStdOspfv3ContextId) == OSIX_FAILURE)
    {
        if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
        {
            return i1RetVal;
        }

        i1RetVal =
            nmhGetNextIndexFutOspfv3ExtRouteTable (i4FsMIOspfv3ExtRouteDestType,
                                                   pi4NextFsMIOspfv3ExtRouteDestType,
                                                   pFsMIOspfv3ExtRouteDest,
                                                   pNextFsMIOspfv3ExtRouteDest,
                                                   u4FsMIOspfv3ExtRoutePfxLength,
                                                   pu4NextFsMIOspfv3ExtRoutePfxLength,
                                                   i4FsMIOspfv3ExtRouteNextHopType,
                                                   pi4NextFsMIOspfv3ExtRouteNextHopType,
                                                   pFsMIOspfv3ExtRouteNextHop,
                                                   pNextFsMIOspfv3ExtRouteNextHop);

        V3UtilReSetContext ();
    }
    if (i1RetVal == SNMP_FAILURE)
    {
        while ((V3UtilOspfGetNextCxtId ((UINT4) i4FsMIStdOspfv3ContextId,
                                        &u4CxtId)) == OSIX_SUCCESS)
        {
            *pi4NextFsMIStdOspfv3ContextId = (INT4) u4CxtId;

            if (V3UtilSetContext (u4CxtId) == SNMP_FAILURE)
            {
                break;
            }

            i1RetVal =
                nmhGetFirstIndexFutOspfv3ExtRouteTable
                (pi4NextFsMIOspfv3ExtRouteDestType, pNextFsMIOspfv3ExtRouteDest,
                 pu4NextFsMIOspfv3ExtRoutePfxLength,
                 pi4NextFsMIOspfv3ExtRouteNextHopType,
                 pNextFsMIOspfv3ExtRouteNextHop);

            V3UtilReSetContext ();

            if (i1RetVal == SNMP_SUCCESS)
            {
                break;
            }

            i4FsMIStdOspfv3ContextId = (INT4) u4CxtId;
        }
    }
    else
    {
        *pi4NextFsMIStdOspfv3ContextId = i4FsMIStdOspfv3ContextId;
    }
    return i1RetVal;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ExtRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                retValFsMIOspfv3ExtRouteStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ExtRouteStatus (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3ExtRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteDest,
                                UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteNextHop,
                                INT4 *pi1RetValFsMIOspfv3ExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3ExtRouteStatus (i4FsMIOspfv3ExtRouteDestType,
                                       pFsMIOspfv3ExtRouteDest,
                                       u4FsMIOspfv3ExtRoutePfxLength,
                                       i4FsMIOspfv3ExtRouteNextHopType,
                                       pFsMIOspfv3ExtRouteNextHop,
                                       pi1RetValFsMIOspfv3ExtRouteStatus);
    V3UtilReSetContext ();

    return i1Return;
}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ExtRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                retValFsMIOspfv3ExtRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ExtRouteMetric (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3ExtRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteDest,
                                UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteNextHop,
                                INT4 *pi4RetValFsMIOspfv3ExtRouteMetric)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3ExtRouteMetric (i4FsMIOspfv3ExtRouteDestType,
                                       pFsMIOspfv3ExtRouteDest,
                                       u4FsMIOspfv3ExtRoutePfxLength,
                                       i4FsMIOspfv3ExtRouteNextHopType,
                                       pFsMIOspfv3ExtRouteNextHop,
                                       pi4RetValFsMIOspfv3ExtRouteMetric);
    V3UtilReSetContext ();

    return i1Return;

}

/****************************************************************************
 Function    :  nmhGetFsMIOspfv3ExtRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                retValFsMIOspfv3ExtRouteMetricType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIOspfv3ExtRouteMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4FsMIOspfv3ExtRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3ExtRouteDest,
                                    UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                    INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3ExtRouteNextHop,
                                    INT4 *pi4RetValFsMIOspfv3ExtRouteMetricType)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhGetFutOspfv3ExtRouteMetricType (i4FsMIOspfv3ExtRouteDestType,
                                           pFsMIOspfv3ExtRouteDest,
                                           u4FsMIOspfv3ExtRoutePfxLength,
                                           i4FsMIOspfv3ExtRouteNextHopType,
                                           pFsMIOspfv3ExtRouteNextHop,
                                           pi4RetValFsMIOspfv3ExtRouteMetricType);
    V3UtilReSetContext ();

    return i1Return;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ExtRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                setValFsMIOspfv3ExtRouteStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ExtRouteStatus (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3ExtRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteDest,
                                UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteNextHop,
                                INT4 i4SetValFsMIOspfv3ExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3ExtRouteStatus (i4FsMIOspfv3ExtRouteDestType,
                                       pFsMIOspfv3ExtRouteDest,
                                       u4FsMIOspfv3ExtRoutePfxLength,
                                       i4FsMIOspfv3ExtRouteNextHopType,
                                       pFsMIOspfv3ExtRouteNextHop,
                                       i4SetValFsMIOspfv3ExtRouteStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ExtRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                setValFsMIOspfv3ExtRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ExtRouteMetric (INT4 i4FsMIStdOspfv3ContextId,
                                INT4 i4FsMIOspfv3ExtRouteDestType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteDest,
                                UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsMIOspfv3ExtRouteNextHop,
                                INT4 i4SetValFsMIOspfv3ExtRouteMetric)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3ExtRouteMetric (i4FsMIOspfv3ExtRouteDestType,
                                       pFsMIOspfv3ExtRouteDest,
                                       u4FsMIOspfv3ExtRoutePfxLength,
                                       i4FsMIOspfv3ExtRouteNextHopType,
                                       pFsMIOspfv3ExtRouteNextHop,
                                       i4SetValFsMIOspfv3ExtRouteMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhSetFsMIOspfv3ExtRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                setValFsMIOspfv3ExtRouteMetricType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIOspfv3ExtRouteMetricType (INT4 i4FsMIStdOspfv3ContextId,
                                    INT4 i4FsMIOspfv3ExtRouteDestType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3ExtRouteDest,
                                    UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                    INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMIOspfv3ExtRouteNextHop,
                                    INT4 i4SetValFsMIOspfv3ExtRouteMetricType)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        return i1Return;
    }

    i1Return =
        nmhSetFutOspfv3ExtRouteMetricType (i4FsMIOspfv3ExtRouteDestType,
                                           pFsMIOspfv3ExtRouteDest,
                                           u4FsMIOspfv3ExtRoutePfxLength,
                                           i4FsMIOspfv3ExtRouteNextHopType,
                                           pFsMIOspfv3ExtRouteNextHop,
                                           i4SetValFsMIOspfv3ExtRouteMetricType);
    V3UtilReSetContext ();
    return i1Return;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ExtRouteStatus
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                testValFsMIOspfv3ExtRouteStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ExtRouteStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3ExtRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3ExtRouteDest,
                                   UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                   INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3ExtRouteNextHop,
                                   INT4 i4TestValFsMIOspfv3ExtRouteStatus)
{
    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3ExtRouteStatus (pu4ErrorCode,
                                          i4FsMIOspfv3ExtRouteDestType,
                                          pFsMIOspfv3ExtRouteDest,
                                          u4FsMIOspfv3ExtRoutePfxLength,
                                          i4FsMIOspfv3ExtRouteNextHopType,
                                          pFsMIOspfv3ExtRouteNextHop,
                                          i4TestValFsMIOspfv3ExtRouteStatus);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ExtRouteMetric
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                testValFsMIOspfv3ExtRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ExtRouteMetric (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMIStdOspfv3ContextId,
                                   INT4 i4FsMIOspfv3ExtRouteDestType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3ExtRouteDest,
                                   UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                   INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsMIOspfv3ExtRouteNextHop,
                                   INT4 i4TestValFsMIOspfv3ExtRouteMetric)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3ExtRouteMetric (pu4ErrorCode,
                                          i4FsMIOspfv3ExtRouteDestType,
                                          pFsMIOspfv3ExtRouteDest,
                                          u4FsMIOspfv3ExtRoutePfxLength,
                                          i4FsMIOspfv3ExtRouteNextHopType,
                                          pFsMIOspfv3ExtRouteNextHop,
                                          i4TestValFsMIOspfv3ExtRouteMetric);
    V3UtilReSetContext ();
    return i1Return;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIOspfv3ExtRouteMetricType
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop

                The Object 
                testValFsMIOspfv3ExtRouteMetricType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIOspfv3ExtRouteMetricType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMIStdOspfv3ContextId,
                                       INT4 i4FsMIOspfv3ExtRouteDestType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3ExtRouteDest,
                                       UINT4 u4FsMIOspfv3ExtRoutePfxLength,
                                       INT4 i4FsMIOspfv3ExtRouteNextHopType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMIOspfv3ExtRouteNextHop,
                                       INT4
                                       i4TestValFsMIOspfv3ExtRouteMetricType)
{

    INT1                i1Return = SNMP_FAILURE;

    if (V3UtilSetContext ((UINT4) i4FsMIStdOspfv3ContextId) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return i1Return;
    }

    i1Return =
        nmhTestv2FutOspfv3ExtRouteMetricType (pu4ErrorCode,
                                              i4FsMIOspfv3ExtRouteDestType,
                                              pFsMIOspfv3ExtRouteDest,
                                              u4FsMIOspfv3ExtRoutePfxLength,
                                              i4FsMIOspfv3ExtRouteNextHopType,
                                              pFsMIOspfv3ExtRouteNextHop,
                                              i4TestValFsMIOspfv3ExtRouteMetricType);
    V3UtilReSetContext ();
    return i1Return;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMIOspfv3ExtRouteTable
 Input       :  The Indices
                FsMIStdOspfv3ContextId
                FsMIOspfv3ExtRouteDestType
                FsMIOspfv3ExtRouteDest
                FsMIOspfv3ExtRoutePfxLength
                FsMIOspfv3ExtRouteNextHopType
                FsMIOspfv3ExtRouteNextHop
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMIOspfv3ExtRouteTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
