
/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3rbutil.c,v 1.7 2017/12/26 13:34:28 siva Exp $
 *
 * Description: This file contains RB Tree compare functions. 
 *                 
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/* Function     : V3RtcCompareSpfNode                                        */
/*                                                                           */
/* Description  : Compare function for the nodes present in SPF Tree         */
/*                                                                           */
/* Input        : pNode1        : Pointer to SPF Tree node1.                 */
/*                pNode2        : Pointer to SPF Tree node2.                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
V3RtcCompareSpfNode (tRBElem * pNode1, tRBElem * pNode2)
{
    tV3OsCandteNode    *pSpfNode1 = (tV3OsCandteNode *) pNode1;
    tV3OsCandteNode    *pSpfNode2 = (tV3OsCandteNode *) pNode2;

    if (pSpfNode1->u1VertType < pSpfNode2->u1VertType)
    {
        return OSPFV3_LESS;
    }
    else if (pSpfNode1->u1VertType > pSpfNode2->u1VertType)
    {
        return OSPFV3_GREATER;
    }

    switch (V3UtilRtrIdComp (pSpfNode1->vertexId.vertexRtrId,
                             pSpfNode2->vertexId.vertexRtrId))
    {
        case OSPFV3_LESS:
            return OSPFV3_LESS;

        case OSPFV3_GREATER:
            return OSPFV3_GREATER;

        default:
            return (pSpfNode1->vertexId.u4VertexIfId -
                    pSpfNode2->vertexId.u4VertexIfId);
    }
}

/*****************************************************************************/
/* Function     : V3CompareHost                                              */
/*                                                                           */
/* Description  : Compare function for the Hosts                 */
/*                                                                           */
/* Input        : e1        Pointer to Host node1                 */
/*                e2        Pointer to Host node2                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
V3CompareHost (tRBElem * e1, tRBElem * e2)
{
    tV3OsHost          *pFirstHost = e1;
    tV3OsHost          *pSecondHost = e2;

    return MEMCMP (&pFirstHost->hostIp6Addr, &pSecondHost->hostIp6Addr,
                   OSPFV3_IPV6_ADDR_LEN);
}

/*****************************************************************************/
/* Function     : V3CompareLsa                                               */
/*                                                                           */
/* Description  : Compare function for the LSAs                     */
/*                                                                           */
/* Input        : e1        Pointer to LsaInfo node1                 */
/*                e2        Pointer to LsaInfo node2                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
V3CompareLsa (tRBElem * e1, tRBElem * e2)
{
    tV3OsLsaInfo       *pFstLsaInfo = e1;
    tV3OsLsaInfo       *pScndLsaInfo = e2;

    if (pFstLsaInfo->lsaId.u2LsaType != pScndLsaInfo->lsaId.u2LsaType)
    {
        if (pFstLsaInfo->lsaId.u2LsaType > pScndLsaInfo->lsaId.u2LsaType)
        {
            return OSPFV3_GREATER;
        }
        return OSPFV3_LESS;
    }

    if (V3UtilRtrIdComp (pFstLsaInfo->lsaId.advRtrId,
                         pScndLsaInfo->lsaId.advRtrId) != OSPFV3_EQUAL)
    {
        return (V3UtilRtrIdComp (pFstLsaInfo->lsaId.advRtrId,
                                 pScndLsaInfo->lsaId.advRtrId));

    }

    if (V3UtilLinkStateIdComp (pFstLsaInfo->lsaId.linkStateId,
                               pScndLsaInfo->lsaId.linkStateId) != OSPFV3_EQUAL)
    {
        return (V3UtilLinkStateIdComp (pFstLsaInfo->lsaId.linkStateId,
                                       pScndLsaInfo->lsaId.linkStateId));

    }

    return OSPFV3_EQUAL;
}

/*****************************************************************************/
/* Function     : V3RBInterfaceCmpFunc                                       */
/*                                                                           */
/* Description  : Compare function for the Interfaces                 */
/*                                                                           */
/* Input        : e1        Pointer to Interface node1                 */
/*                e2        Pointer to Interface node2                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. Less than 0 if key of first element is less than second */
/*                element key.                                               */
/*                3. Greater than 0 if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
PUBLIC INT4
V3RBInterfaceCmpFunc (tRBElem * e1, tRBElem * e2)
{
    tV3OsInterface     *pIface1 = (tV3OsInterface *) e1;
    tV3OsInterface     *pIface2 = (tV3OsInterface *) e2;

    if (pIface1->u4InterfaceId > pIface2->u4InterfaceId)
    {
        return OSPFV3_GREATER;
    }
    else if (pIface1->u4InterfaceId == pIface2->u4InterfaceId)
    {
        return OSPFV3_EQUAL;
    }
    else
    {
        return OSPFV3_LESS;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CompRtrLsaLink                                             */
/*                                                                           */
/* Description  : Key comparision function for router LSA RBTree search      */
/*                Compare the Neighbor Router Id and Neighbor Interface Id   */
/*                of the Links                                               */
/*                in LSA                                                     */
/*                                                                           */
/* Input        : e1   - Pointer to the Router LSA1                          */
/*                e2   - Pointer to the Router LSA2                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. OSPFV3_GREATER if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/
INT4
V3CompRtrLsaLink (tRBElem * e1, tRBElem * e2)
{
    tV3OsRtrLsaRtInfo  *pRtrLsa1 = e1;
    tV3OsRtrLsaRtInfo  *pRtrLsa2 = e2;
    INT1                i1RetVal = OSPFV3_EQUAL;

    if (pRtrLsa1->u1LinkType > pRtrLsa2->u1LinkType)
    {
        return OSPFV3_GREATER;
    }
    else if (pRtrLsa1->u1LinkType < pRtrLsa2->u1LinkType)
    {
        return OSPFV3_LESS;
    }
    if (pRtrLsa1->u4NbrIfId > pRtrLsa2->u4NbrIfId)
    {
        return OSPFV3_GREATER;
    }
    else if (pRtrLsa1->u4NbrIfId < pRtrLsa2->u4NbrIfId)
    {
        return OSPFV3_LESS;
    }
    i1RetVal = V3UtilRtrIdComp (pRtrLsa1->nbrRtrId, pRtrLsa2->nbrRtrId);
    if (i1RetVal < OSPFV3_EQUAL)
    {
        return OSPFV3_LESS;
    }
    else if (i1RetVal > OSPFV3_EQUAL)
    {
        return OSPFV3_GREATER;
    }
    return OSPFV3_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CompNwLsaLink                                            */
/*                                                                           */
/* Description  : Key comparision function for network LSA RBTree search     */
/*                Compares the attached Router ID                            */
/*                                                                           */
/* Input        : e1   - Pointer to the Network LSA1                         */
/*                e2   - Pointer to the Network LSA2                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. OSPFV3_GREATER if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/

INT4
V3CompNwLsaLink (tRBElem * e1, tRBElem * e2)
{
    tV3OsNwLsaRtInfo   *pRtrLsa1 = e1;
    tV3OsNwLsaRtInfo   *pRtrLsa2 = e2;

    if (pRtrLsa1->u4AttachedRtrId > pRtrLsa2->u4AttachedRtrId)
    {
        return OSPFV3_GREATER;
    }

    else if (pRtrLsa1->u4AttachedRtrId < pRtrLsa2->u4AttachedRtrId)
    {
        return OSPFV3_LESS;
    }
    return OSPFV3_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3CompGRLsIdInfo                                           */
/*                                                                           */
/* Description  : Key comparision function for LsaIdInfo  RBTree search      */
/*                Compares the Prefix Information                            */
/*                                                                           */
/* Input        : e1   - Pointer to the GR LsId Info1                        */
/*                e2   - Pointer to the GR LsId Info2                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : 1. OSPFV3_EQUAL if keys of both the elements are same.     */
/*                2. RB_LESS key of first element is less than second        */
/*                element key.                                               */
/*                3. OSPFV3_GREATER if key of first element is greater than  */
/*                second element key                                         */
/*****************************************************************************/

INT4
V3CompGRLsIdInfo (tRBElem * e1, tRBElem * e2)
{
    tV3OsGRLsIdInfo    *pLsId1 = e1;
    tV3OsGRLsIdInfo    *pLsId2 = e2;
    INT4                i4RetVal = OSPFV3_EQUAL;

    if (pLsId1->u1PrefixLength > pLsId2->u1PrefixLength)
    {
        return OSPFV3_GREATER;
    }
    else if (pLsId1->u1PrefixLength < pLsId2->u1PrefixLength)
    {
        return OSPFV3_LESS;
    }
    i4RetVal = V3UtilIp6PrefixComp (&pLsId1->addrPrefix,
                                    &pLsId2->addrPrefix,
                                    pLsId1->u1PrefixLength);
    if (i4RetVal < OSPFV3_EQUAL)
    {
        return OSPFV3_LESS;
    }
    else if (i4RetVal > OSPFV3_EQUAL)
    {
        return OSPFV3_GREATER;
    }

    return OSPFV3_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilRBFreeRouterLinks                                    */
/*                                                                           */
/* Description  : Frees router lsa link nodes used for GR consistency check  */
/*                                                                           */
/* Input        : pRBElem- Pointer to the Router Lsa Link                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
V3UtilRBFreeRouterLinks (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        OSPFV3_RTR_LSA_LINK_FREE (pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilRBFreeNwLinks                                        */
/*                                                                           */
/* Description  : Frees network lsa link nodes used for GR consistency check */
/*                                                                           */
/* Input        : pRBElem- Pointer to the network Lsa Link                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
V3UtilRBFreeNwLinks (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        OSPFV3_NW_LSA_LINK_FREE ((UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : V3UtilRBFreeLsaIds                                         */
/*                                                                           */
/* Description  : Frees Lsaid nodes used for GR consistency check            */
/*                                                                           */
/* Input        : pRBElem- Pointer to the network Lsa Link                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
V3UtilRBFreeLsaIds (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        OSPFV3_LSAID_FREE ((UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * Function    : V3OsUpdIfRouteLeakStatus ()
 * Description : This routine is used to compare the Interface leak status
 * Input(s)    : pRRDRouteNode - Key1
 *               pRRDRouteIn   - key2
 * Output(s)   : None
 * Globals     : Not Referred or Modified
 * Returns     : -1/1 when key elements are less or greater
 *                0   when key elements are equal
 ******************************************************************************/
INT4
V3OsUpdIfRouteLeakStatus (tRBElem * pIfLeakRoute, tRBElem * pIfLeakRouteIn)
{

    /* Compare the Interface index */
    if ((((tV3OsLeakRoutes *) pIfLeakRoute)->u4IfIndex) >
        (((tV3OsLeakRoutes *) pIfLeakRouteIn)->u4IfIndex))
        return 1;
    else if ((((tV3OsLeakRoutes *) pIfLeakRoute)->u4IfIndex) <
             (((tV3OsLeakRoutes *) pIfLeakRouteIn)->u4IfIndex))
        return -1;

    return 0;
}

/*----------------------------------------------------------------------*/
/*                     End of the file o3rbutil.c                       */
/*----------------------------------------------------------------------*/
