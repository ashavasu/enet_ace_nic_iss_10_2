/****************************************************************************/
/* Copyright (C) 2009 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/* $Id: o3redrm.c,v 1.9 2018/02/02 09:48:18 siva Exp $                                                    */
/*                                                                          */
/* Description: This file contains the handling of events to be             */
/*              sent to and received from the RM module                     */
/*                                                                          */
/****************************************************************************/
#include "o3inc.h"
/* Prototypes */

PRIVATE VOID O3RedRmHandleGoActive PROTO ((VOID));

PRIVATE INT1        O3RedRmProcessStandbyToActive
PROTO ((tRBElem * pNode, eRBVisit visit, UINT4 u4Level, VOID *arg, VOID *out));

PRIVATE VOID O3RedRmHandleGoStandby PROTO ((VOID));

PRIVATE VOID O3RedRmConfigRestoreComplete PROTO ((VOID));

PRIVATE VOID O3RedRmHandleStandbyUp PROTO ((tV3OsRmCtrlMsg * pRmCtrlMsg));

PRIVATE VOID O3RedRmHandleStandbyDown PROTO ((tV3OsRmCtrlMsg * pRmCtrlMsg));

PRIVATE VOID O3RedRmProcessRmMsg PROTO ((tV3OsRmCtrlMsg * pRmCtrlMsg));

PRIVATE INT4        O3RedRmAddtoIntfRxmt
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsInterface * pInterface));

PRIVATE VOID O3RmHwAudit PROTO ((VOID));

PRIVATE VOID O3RmHandleDynSyncAudit PROTO ((VOID));
/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmHandleRmEvents                                     */
/*                                                                          */
/* Description  : This  function is called while processing the queue       */
/*                message. This function handles all the events from RM     */
/*                                                                          */
/* Input        : pRmCtrlMsg  -   pointer to RM message structure           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

VOID
O3RedRmHandleRmEvents (tV3OsRmCtrlMsg * pRmCtrlMsg)
{

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmHandleRmEvents \r\n");

    if (pRmCtrlMsg == NULL)
    {
        return;
    }

    switch (pRmCtrlMsg->u1Event)
    {
        case RM_MESSAGE:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received Message from RM\r\n");

            O3RedRmProcessRmMsg (pRmCtrlMsg);
            break;

        case GO_ACTIVE:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received GO_ACTIVE from RM\r\n");
            O3RedRmHandleGoActive ();
            break;

        case GO_STANDBY:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received GO_STANDBY from RM\r\n");

            O3RedRmHandleGoStandby ();
            break;

        case RM_STANDBY_UP:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received standby up\r\n");

            O3RedRmHandleStandbyUp (pRmCtrlMsg);
            break;

        case RM_STANDBY_DOWN:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received standby up\r\n");
            O3RedRmHandleStandbyDown (pRmCtrlMsg);
            break;

        case L2_INITIATE_BULK_UPDATES:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Bulk updates initiated\r\n");

            O3RedBlkSendBulkRequest ();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received standby up\r\n");

            O3RedRmConfigRestoreComplete ();
            break;

        case RM_INIT_HW_AUDIT:
            if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
            {
                OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received RM_INIT_HW_AUDIT\r\n");

                O3RmHwAudit ();
            }
            break;

        case RM_DYNAMIC_SYNCH_AUDIT:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                            "Received RM_DYNAMIC_SYNCH_AUDIT \r\n");

            O3RmHandleDynSyncAudit ();
            break;

        default:
            OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Received invalid event\r\n");

            break;
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT:O3RedRmHandleRmEvents \r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmHandleGoActive                                     */
/*                                                                          */
/* Description  : This function is called when go active event is received  */
/*                from the RM module. This function takes care of handling  */
/*                the timers and setting the counters                       */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmHandleGoActive (VOID)
{
    tV3OsRedStateChgTrapInfo RedStateChgTrapInfo;
    tV3OspfCxt         *pV3OspfCxt = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;
    UINT4               u4ContextId = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmHandleGoActive\r\n");

    if (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE)
    {
        return;
    }

    MEMSET (&RedStateChgTrapInfo, 0, sizeof (tV3OsRedStateChgTrapInfo));
    /* OSPF will be disabled by default and can be enabled by the user only
     * after receiving the GO_ACTIVE event. So, in case of idle to active
     * (i.e. task initialization), the protocol will be disabled. No need
     * to handle idle to active event.
     * In case of configuration restoration, GO_ACTIVE event is given in
     * sequence from the lowest layer protocol to higher layer and finally to
     * MSR. This ensures that restoration process (from MSR) is started only
     * after OSPF has handled the GO_ACTIVE event.
     * Due to the above reasons, idle to active is not handled
     */
    gV3OsRtr.ospfRedInfo.u4PrevRmState = gV3OsRtr.ospfRedInfo.u4RmState;

    /* Update the no. of peers */
    gV3OsRtr.ospfRedInfo.u4PeerCount = (UINT4) V3OspfRmGetStandbyNodeCount ();

    if (gV3OsRtr.ospfRedInfo.u4PeerCount > 0)
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_DOWN;
    }

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    while (pInterface != NULL)
    {

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

            if (pInterface->bPassive != OSPFV3_TRUE)
            {
                V3HpSendHello (pNbr->pInterface, pNbr, OSPFV3_HELLO_TIMER);
            }
        }
        pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                       (tRBElem *) pInterface,
                                                       NULL);
    }
    gu1O3HelloSwitchOverFlag = 0;

    /* Reset counters & Bulk upd states */
    O3RedUtlResetHsVariables ();

    if ((gV3OsRtr.ospfRedInfo.u4PrevRmState == OSPFV3_RED_STANDBY) &&
        (OSPFv3_IS_NODE_ACTIVE () == OSPFV3_TRUE))
    {
        /* The node has transitted from standby to active state. This occurs
         * due to force switchover or active has crashed
         */

        /* If the admin status of the protocol is not disabled, trigger the
         * protocol actions. This will be checked in the RB Walk call back fn
         * Scan through all the interfaces and start the hello timer
         */

        RBTreeWalk (gV3OsRtr.pIfRBRoot,
                    (tRBWalkFn) O3RedRmProcessStandbyToActive, NULL, NULL);

        /* Scan all the virtual interfaces in all the context. Send the hello
         * packet and check the neighbor start
         */
        for (u4ContextId = 0; u4ContextId < OSPFV3_MAX_CONTEXTS_LIMIT;
             u4ContextId++)
        {
            pV3OspfCxt = gV3OsRtr.apV3OspfCxt[u4ContextId];

            if ((pV3OspfCxt == NULL) ||
                (pV3OspfCxt->admnStat == OSPFV3_DISABLED))
            {
                continue;
            }

            O3RedIfProcessVirtIfOnActive (pV3OspfCxt);

            /* Scan all the self originated LSA. If the LSA changed flag is set,
             * then a LSA needs to be originated but the node became active before
             * the expiry of min LSA timer
             */
            O3RedLsuSendSelfOriginatedLsa (pV3OspfCxt);

            /* Calculate the route */
            if (pV3OspfCxt->u1RtrNetworkLsaChanged != OSIX_TRUE)
            {
                V3RtcSetRtTimerInCxt (pV3OspfCxt);
            }
        }

        OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                        "Sent HP and checked "
                        "nbr states through all interfaces\r\n");
        V3AuthCryptoHighSeq ();
        O3RedRmSendEvent (RM_STANDBY_TO_ACTIVE_EVT_PROCESSED, RM_NONE);
    }
    else
    {
        O3RedRmSendEvent (RM_IDLE_TO_ACTIVE_EVT_PROCESSED, RM_NONE);
    }

    /* Send a snmp trap about this state change */

    if (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID] != NULL)
    {
        OSPFV3_RTR_ID_COPY (&RedStateChgTrapInfo.rtrId,
                            &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                            rtrId);
        if (gV3OsRtr.ospfRedInfo.u4PeerCount > 0)
        {
            RedStateChgTrapInfo.i4HotStandbyState =
                OSPFV3_HA_STATE_ACTIVE_STANDBYUP;
        }
        else
        {
            RedStateChgTrapInfo.i4HotStandbyState =
                OSPFV3_HA_STATE_ACTIVE_STANDBYDOWN;
        }
        O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                               OSPFV3_RED_STATE_CHANGE_TRAP,
                               &RedStateChgTrapInfo);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmHandleGoActive\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmProcessStandbyToActive                             */
/*                                                                          */
/* Description  : This function is called during RB Tree walk when          */
/*                GO_ACTIVE is recevied by standby node. In this function,  */
/*                all the OSPFv3 interfaces are scanned.                    */
/*                                                                          */
/*                1. Hello Timer expiry is performed on all interfaces      */
/*                   and Poll Timer expiry on NBMA and PTMP interfaces      */
/*                                                                          */
/*                2. Scan all the nbrs in the interface. Start the dead     */
/*                   interval timer for each nbr. Additionally, if the      */
/*                   nbr is not in FULL state, re-start the adjacency       */
/*                                                                          */
/*                                                                          */
/* Input        : Node     -    Pointer to tV3OsInterface                   */
/*                visit    -    RBTree info                                 */
/*                u4Level  -    RBTree info (unused)                        */
/*                arg      -    RBTree info (unused)                        */
/*                out      -    RBTree info (unused)                        */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : No of nodes walked in RB Tree                             */
/*                                                                          */
/****************************************************************************/

PRIVATE INT1
O3RedRmProcessStandbyToActive (tRBElem * pNode, eRBVisit visit, UINT4 u4Level,
                               VOID *arg, VOID *out)
{
    tV3OsInterface     *pInterface = NULL;

    UNUSED_PARAM (u4Level);
    UNUSED_PARAM (out);
    UNUSED_PARAM (arg);

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedRmProcessStandbyToActive\r\n");
    if ((visit == leaf) || (visit == postorder))
    {
        if (pNode != NULL)
        {
            pInterface = (tV3OsInterface *) pNode;

            if ((pInterface->operStatus == OSPFV3_ENABLED) &&
                (pInterface->admnStatus == OSPFV3_ENABLED) &&
                (pInterface->u1IsmState != OSPFV3_IFS_DOWN))
            {
                if (pInterface->u1IsmState == OSPFV3_IFS_WAITING)
                {
                    OSPFV3_GBL_TRC1 (OSPFV3_RM_TRC,
                                     "Interface %d is in waiting state. Start "
                                     "the waiting timer\r\n",
                                     pInterface->u4InterfaceId);

                    V3TmrSetTimer (&(pInterface->waitTimer), OSPFV3_WAIT_TIMER,
                                   (OSPFV3_NO_OF_TICKS_PER_SEC *
                                    (pInterface->u2RtrDeadInterval)));
                }

                O3RedRmCheckNbrs ((tV3OsInterface *) pNode);

                /* Call the hello timer expiry to send the hello through
                 * the interface
                 */
                V3UtilSendHello (pInterface, OSPFV3_HELLO_TIMER);

                /* If the interface is NBMA or PTMP,
                 * call the poll timer expiry
                 */

                if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF (pInterface) ||
                    (pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
                    (pInterface->u1NetworkType == OSPFV3_IF_PTOMP))
                {
                    V3UtilSendHello (pInterface, OSPFV3_POLL_TIMER);
                }
            }
        }
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedRmProcessStandbyToActive\r\n");
    return (RB_WALK_CONT);
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmCheckNbrs                                          */
/*                                                                          */
/* Description  : This function is the called during standby to active      */
/*                event.                                                    */
/*                                                                          */
/*                Scan all the nbrs in the interface. Start the dead        */
/*                interval timer for each nbr. Additionally, if the         */
/*                nbr is not in FULL state, re-start the adjacency          */
/*                                                                          */
/* Input        : pInterface   -  pointer to OSPFv3 interface               */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedRmCheckNbrs (tV3OsInterface * pInterface)
{
    tV3OsNeighbor      *pNbr = NULL;    /* Pointer to neighbor */
    tTMO_SLL_NODE      *pNbrNode = NULL;    /* Pointer to TMO_SLL_Node in
                                             * nbr structure used in scan
                                             */
    UINT4               u4RxmtTmrInterval = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmCheckNbrs\r\n");

    /* Scan through all the neighbors in the interface. In case of standby to
     * active, the neighbors state can be in INIT/TWO-WAY/EXSTART/FULL state.
     * If the neighbor is in exstart state, start the DDP exchange process
     * Additionally, start the inactivity timer for all neighbors
     */

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pNbrNode);

        /* If the nbr is down / attempt, then the nbr is configured but packet
         * is not received from this nbr. So no need to start the inactivity
         * timer
         */
        if (pNbr->u1NsmState == OSPFV3_NBRS_DOWN)
        {
            continue;
        }

        if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
        {
            /* Do not start the inactivity timer for DC applicable 
             * PTOP/PTMP and virtual interfaces. St the nbr suppression
             * flag
             */
            if (OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF
                (pInterface))
            {
                pNbr->bHelloSuppression = OSIX_TRUE;
            }
        }

        /* Start the inactivity timer */
        V3TmrSetTimer (&(pNbr->inactivityTimer),
                       OSPFV3_INACTIVITY_TIMER,
                       OSPFV3_NO_OF_TICKS_PER_SEC *
                       (pNbr->pInterface->u2RtrDeadInterval));

        if (pNbr->u1NsmState <= OSPFV3_NBRS_EXSTART)
        {
            /* Since the DDP process is triggered from the beginning,
             * clear all rxmt list for that neighbor, if any synced from
             * the active node
             */
            OSPFV3_GBL_TRC1 (OSPFV3_RM_TRC,
                             "Nbr %x in in exstart "
                             "state. Restarting adjacency\r\n",
                             OSPFV3_BUFFER_DWFROMPDU (pNbr->nbrRtrId));

            V3NsmRestartAdj (pNbr);
        }
        else if (pNbr->u1NsmState == OSPFV3_NBRS_FULL)
        {
            /* Since the neighbor is in FULL state, send the LSA to the
             * neighbor present in the neighbor rxmt list
             */
            if (pNbr->lsaRxmtDesc.u4LsaRxmtCount > 0)
            {
                if (V3LsuRxmtLsu (pNbr) == OSIX_SUCCESS)
                {
                    u4RxmtTmrInterval = OSPFV3_NO_OF_TICKS_PER_SEC *
                        (pNbr->pInterface->u2RxmtInterval);

                    V3TmrSetTimer (&(pNbr->lsaRxmtDesc.lsaRxmtTimer),
                                   OSPFV3_LSA_RXMT_TIMER,
                                   ((pNbr->pInterface->u1NetworkType ==
                                     OSPFV3_IF_BROADCAST) ?
                                    V3UtilJitter (u4RxmtTmrInterval,
                                                  OSPFV3_JITTER) :
                                    u4RxmtTmrInterval));
                }
            }
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmCheckNbrs\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmHandleGoStandby                                    */
/*                                                                          */
/* Description  : This function is called when go standby event is received */
/*                from the RM module. This function takes care of handling  */
/*                the timers and setting the counters                       */
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmHandleGoStandby (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4PrevContextId = 0;
    UINT4               u4ContextId = 0;

    /* If the current node status is idle, then no need to process this event
     * The node will become standby after receiving RM_CONFIG_RESTORE_COMPLETE
     * after completing MSR restoration
     */

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmHandleGoStandby\r\n");

    if (OSPFv3_IS_NODE_ACTIVE () != OSPFV3_TRUE)
    {
        return;
    }

    gV3OsRtr.ospfRedInfo.u4PeerCount = 0;

    /* Reset counters & Bulk upd states */
    O3RedUtlResetHsVariables ();

    gV3OsRtr.ospfRedInfo.u4PrevRmState = gV3OsRtr.ospfRedInfo.u4RmState;
    gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_STANDBY;

    /* This needs to be modified to audit the DB between the active and
     * standby. If there is a DB mismatch, then disable and enable
     * all the context ti start a fresh dynamic bulk updates
     */
    if (V3UtilOspfGetFirstCxtId (&u4ContextId) == OSIX_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;
            pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

            if ((pV3OspfCxt == NULL) ||
                (pV3OspfCxt->admnStat == OSPFV3_DISABLED))
            {
                continue;
            }

            V3RtrProtocolDisableTmrs (pV3OspfCxt);

        }
        while (V3UtilOspfGetNextCxtId (u4PrevContextId, &u4ContextId)
               == OSIX_SUCCESS);
    }

    /* Clear all the ISM schedule queues */
    V3IsmInitSchedQueueInCxt (NULL, NULL);

    O3RedRmSendEvent (RM_STANDBY_EVT_PROCESSED, RM_NONE);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmHandleGoStandby\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmConfigRestoreComplete                              */
/*                                                                          */
/* Description  : This function is called when configuration restoration    */
/*                complete event is received. If the current state is       */
/*                idle and RM state is standby, then GO_STANDBY is processed*/
/*                                                                          */
/* Input        : None                                                      */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmConfigRestoreComplete (VOID)
{
    tV3OspfCxt         *pV3OspfCxt = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3RedRmConfigRestoreComplete\r\n");

    if ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_INIT) &&
        (V3OspfRmGetNodeState () == RM_STANDBY))
    {
        gV3OsRtr.ospfRedInfo.u4PrevRmState = gV3OsRtr.ospfRedInfo.u4RmState;
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_STANDBY;

        /* Reset counters & Bulk upd states */
        O3RedUtlResetHsVariables ();

        /* Enable OSPF if the admin is enabled */
        for ((i4RetVal = V3UtilOspfGetFirstCxtId (&u4ContextId));
             (i4RetVal == OSIX_SUCCESS);
             (i4RetVal = V3UtilOspfGetNextCxtId (u4ContextId, &u4ContextId)))
        {
            pV3OspfCxt = V3UtilOspfGetCxt (u4ContextId);

            if ((pV3OspfCxt == NULL) ||
                (pV3OspfCxt->u1IsOspfEnabled == OSPFV3_FALSE))
            {
                continue;
            }

            V3RtrSetProtocolStatusInCxt (pV3OspfCxt, OSPFV3_ENABLED);
            pV3OspfCxt->u1IsOspfEnabled = OSPFV3_FALSE;
        }

        O3RedRmSendEvent (RM_STANDBY_EVT_PROCESSED, RM_NONE);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmConfigRestoreComplete\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmHandleStandbyUp                                    */
/*                                                                          */
/* Description  : This function is called when OSPFv3 receives standby up   */
/*                indication. This routine updates the standby count        */
/*                and if bulk request is received before standby up         */
/*                indication, bulk update is sent to the standby node       */
/*                                                                          */
/* Input        : pRmCtrlMsg   -  Control message received from RM          */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmHandleStandbyUp (tV3OsRmCtrlMsg * pRmCtrlMsg)
{
    tV3OsRedStateChgTrapInfo RedStateChgTrapInfo;
    tRmNodeInfo        *pData = NULL;
    BOOL1               b1Value = OSPFV3_FALSE;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmHandleStandbyUp\r\n");

    if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
    {
        OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                        "Got the Standby Up event at standby \r\n");
        return;
    }

    MEMSET (&RedStateChgTrapInfo, 0, sizeof (tV3OsRedStateChgTrapInfo));

    pData = (tRmNodeInfo *) pRmCtrlMsg->pData;
    gV3OsRtr.ospfRedInfo.u4PeerCount = pData->u1NumStandby;

    if (gV3OsRtr.ospfRedInfo.u4PeerCount > 0)
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_DOWN;
    }
    O3RedUtlResetHsVariables ();

    /* Release the message */
    V3OspfRmReleaseMemoryForMsg ((UINT1 *) pData);
    b1Value = gV3OsRtr.ospfRedInfo.b1IsBulkReqRcvd;
    if (b1Value == OSPFV3_TRUE)
    {
        gV3OsRtr.ospfRedInfo.b1IsBulkReqRcvd = OSPFV3_FALSE;

        /* Bulk request msg is recieved before RM_PEER_UP event.
         * So we are sending bulk updates now.
         */

        O3RedBlkSendBulkUpdate ();
    }

    /* Send a snmp trap about this state change */

    if (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID] != NULL)
    {
        OSPFV3_RTR_ID_COPY (&RedStateChgTrapInfo.rtrId,
                            &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->
                            rtrId);
        RedStateChgTrapInfo.i4HotStandbyState =
            OSPFV3_HA_STATE_ACTIVE_STANDBYUP;
        O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                               OSPFV3_RED_STATE_CHANGE_TRAP,
                               &RedStateChgTrapInfo);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmHandleStandbyUp\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmHandleStandbyDown                                  */
/*                                                                          */
/* Description  : This function is called when OSPFv3 receives standby down */
/*                indication. This routine updates the standby count        */
/*                                                                          */
/* Input        : pRmCtrlMsg   -  Control message received from RM          */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmHandleStandbyDown (tV3OsRmCtrlMsg * pRmCtrlMsg)
{
    tV3OsRedStateChgTrapInfo RedStateChgTrapInfo;
    tRmNodeInfo        *pData = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmHandleStandbyDown\r\n");
    if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
    {

        OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                        "Got the Standby Down event at standby \r\n");
        V3OspfRmReleaseMemoryForMsg ((UINT1 *) pData);
        return;
    }

    MEMSET (&RedStateChgTrapInfo, 0, sizeof (tV3OsRedStateChgTrapInfo));

    pData = (tRmNodeInfo *) pRmCtrlMsg->pData;
    gV3OsRtr.ospfRedInfo.u4PeerCount = pData->u1NumStandby;
    if (gV3OsRtr.ospfRedInfo.u4PeerCount > 0)
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_UP;
    }
    else
    {
        gV3OsRtr.ospfRedInfo.u4RmState = OSPFV3_RED_ACTIVE_STANDBY_DOWN;
    }
    /* Reset counters & Bulk upd states */
    O3RedUtlResetHsVariables ();

    /* Release the message */
    V3OspfRmReleaseMemoryForMsg ((UINT1 *) pData);

    /* Send a snmp trap about this state change */

    OSPFV3_RTR_ID_COPY (&RedStateChgTrapInfo.rtrId,
                        &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->rtrId);
    RedStateChgTrapInfo.i4HotStandbyState = OSPFV3_HA_STATE_ACTIVE_STANDBYDOWN;
    O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                           OSPFV3_RED_STATE_CHANGE_TRAP, &RedStateChgTrapInfo);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmHandleStandbyDown\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmProcessRmMsg                                       */
/*                                                                          */
/* Description  : This function is called when a RM message is received by  */
/*                the module.                                               */
/*                                                                          */
/* Input        : pRmCtrlMsg   -  Control message received from RM          */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
O3RedRmProcessRmMsg (tV3OsRmCtrlMsg * pRmCtrlMsg)
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4SeqNum = 0;
    UINT2               u2DataLen = 0;
    UINT2               u2OffSet = 0;
    UINT1               u1MsgType = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmProcessRmMsg\r\n");

    pRmMsg = pRmCtrlMsg->pData;

    u2DataLen = pRmCtrlMsg->u2DataLen;

    /* Read the sequence number from the RM header. Acknowledgement
     * should be given for the sequence number after processing the packet
     */
    RM_PKT_GET_SEQNUM (pRmMsg, &u4SeqNum);

    if (pRmMsg == NULL)
    {
        return;
    }

    /* Strip off the RM header */
    RM_STRIP_OFF_RM_HDR (pRmMsg, u2DataLen);

    /* Get the message type */
    OSPFV3_RED_GET_1_BYTE (pRmMsg, u2OffSet, u1MsgType);

    switch (u1MsgType)
    {
        case RM_BULK_UPDT_REQ_MSG:

            if (OSPFV3_IS_STANDBY_UP () == OSPFV3_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_PEER_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */

                gV3OsRtr.ospfRedInfo.b1IsBulkReqRcvd = OSPFV3_TRUE;
                OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                                "Received RM_BULK_UPDT_REQ_MSG. "
                                "But peer node is not ready \r\n");
                break;
            }

            gV3OsRtr.ospfRedInfo.b1IsBulkReqRcvd = OSPFV3_FALSE;
            O3RedBlkSendBulkUpdate ();
            break;

        case RM_BULK_UPDATE_MSG:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedBlkProcessBulkUpdt (pRmMsg, u2DataLen);
            }
            break;

        case RM_BULK_UPDT_TAIL_MSG:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedBlkProcessBulkTail ();
            }
            break;

        case OSPFV3_RED_LSU:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessLsuUpdt (pRmMsg);
            }
            break;

        case OSPFV3_RED_HELLO:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessHello (pRmMsg);
            }
            break;

        case OSPFV3_RED_NBR_STATE:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessNbrState (pRmMsg);
            }
            break;

        case OSPFV3_RED_LS_ACK:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessLsAckInfo (pRmMsg);
            }
            break;

        case OSPFV3_RED_INTF:

            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessIntfInfo (pRmMsg);
            }
            break;
        case OSPFV3_RED_RTR_ID:
            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynProcessRouterIdInfo (pRmMsg);
            }
            break;
        case OSPFV3_RED_CLEAR:
            if (gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_STANDBY)
            {
                O3RedDynOspfClearProcess (pRmMsg);
            }
            break;

        default:
            break;
    }

    /* Free the message and send the acknowledge to RM */
    RM_FREE (pRmMsg);
    V3OspfSendProtoAckToRM (u4SeqNum);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmProcessRmMsg\r\n");
    return;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmConstructLsaDesc                                   */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update message either through bulk update or dynamic      */
/*                update. This is called when the received LSU is self      */
/*                originated. Construct the LSA Descriptor                  */
/*                                                                          */
/* Input        : pLsaInfo    -   Pointer to the LSA info                   */
/*                pLsdbInfo   -   Pointer to the LSDB info                  */
/*                pInterface  -   Pointer to interface                      */
/*                pArea       -   Pointer to Area                           */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : VOID                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedRmConstructLsaDesc (tV3OsLsaInfo * pLsaInfo, tV3OsRmLsdbInfo * pLsdbInfo,
                         tV3OsInterface * pInterface, tV3OsArea * pArea)
{
    tV3OsLsaDesc       *pLsaDesc = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmConstructLsaDesc\r\n");

    if (pLsaInfo->pLsaDesc == NULL)
    {
        OSPFV3_LSA_DESC_ALLOC (&(pLsaDesc));

        if (pLsaDesc == NULL)
        {
            return;
        }

        MEMSET (pLsaDesc, 0, sizeof (tV3OsLsaDesc));

        TMO_DLL_Init_Node (&(pLsaDesc->nextLsaDesc));

        pLsaDesc->pLsaInfo = pLsaInfo;
        pLsaInfo->pLsaDesc = pLsaDesc;
        pLsaDesc->u1MinLsaIntervalExpired = OSIX_TRUE;
        pLsaDesc->u1LsaChanged = OSIX_FALSE;
        pLsaDesc->u1SeqNumWrapAround = OSIX_FALSE;
        pLsaInfo->pLsaDesc->u2InternalLsaType = pLsdbInfo->u2InternalLsaType;

        TMO_DLL_Add (&(pLsaInfo->pV3OspfCxt->lsaDescLst),
                     &(pLsaDesc->nextLsaDesc));
    }

    /* If the internal type give as input and the internal type
     * present in the LSA descriptor are not same, then
     * free the allocated summary param present in LSA Desc
     * if the internal type is OSPFV3_INTER_AREA_PREFIX_LSA or
     * OSPFV3_INTER_AREA_ROUTER_LSA
     */
    if (pLsaInfo->pLsaDesc->u2InternalLsaType != pLsdbInfo->u2InternalLsaType)
    {
        if ((pLsaInfo->pLsaDesc->u2InternalLsaType
             == OSPFV3_INTER_AREA_PREFIX_LSA) ||
            (pLsaInfo->pLsaDesc->u2InternalLsaType
             == OSPFV3_INTER_AREA_ROUTER_LSA))
        {
            if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
            {
                OSPFV3_SUM_PARAM_FREE (pLsaInfo->pLsaDesc->pAssoPrimitive);
            }
        }

        pLsaInfo->pLsaDesc->pAssoPrimitive = NULL;
        pLsaInfo->pLsaDesc->u2InternalLsaType = pLsdbInfo->u2InternalLsaType;
    }

    switch (pLsaInfo->pLsaDesc->u2InternalLsaType)
    {
        case OSPFV3_INTER_AREA_PREFIX_LSA:
        case OSPFV3_INTER_AREA_ROUTER_LSA:
        {
            if (pLsaInfo->pLsaDesc->pAssoPrimitive != NULL)
            {
                MEMCPY (pLsaInfo->pLsaDesc->pAssoPrimitive,
                        pLsdbInfo->pAssoPrimitive, sizeof (tV3OsSummaryParam));

                OSPFV3_SUM_PARAM_FREE (pLsdbInfo->pAssoPrimitive);
                pLsdbInfo->pAssoPrimitive = NULL;
            }
            else
            {
                pLsaInfo->pLsaDesc->pAssoPrimitive = pLsdbInfo->pAssoPrimitive;
            }
            break;
        }
        case OSPFV3_AS_EXT_LSA:
        case OSPFV3_COND_AS_EXT_LSA:
        case OSPFV3_COND_NSSA_LSA:
        case OSPFV3_COND_INTER_AREA_PREFIX_LSA:
        case OSPFV3_AS_TRNSLTD_RNG_LSA:
        case OSPFV3_AS_TRNSLTD_EXT_LSA:
        case OSPFV3_NSSA_LSA:
        case OSPFV3_NETWORK_LSA:
        {
            pLsaInfo->pLsaDesc->pAssoPrimitive = pLsdbInfo->pAssoPrimitive;
            break;
        }
        case OSPFV3_ROUTER_LSA:
        case OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA:
        {
            pLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pArea;
            break;
        }
        case OSPFV3_LINK_LSA:
        case OSPFV3_GRACE_LSA:
        {
            pLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pInterface;
            break;
        }
        case OSPFV3_DEFAULT_NSSA_LSA:
        {
            if (OSPFV3_IS_AREA_BORDER_RTR (pLsaInfo->pV3OspfCxt))
            {
                pLsaInfo->pLsaDesc->pAssoPrimitive = (UINT1 *) pArea;
            }
            else
            {
                pLsaInfo->pLsaDesc->pAssoPrimitive = pLsdbInfo->pAssoPrimitive;
            }
        }
        default:
            break;
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RedRmConstructLsaDesc\r\n");
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmAddToRxmtLst                                       */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update or dynamic update message. This function adds the  */
/*                LSA to the rxmt list. If the pNbr is NULL, then this      */
/*                function is called from bulk update process               */
/*                                                                          */
/* Input        : pLsaInfo    -   Pointer to the LSA info                   */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_TRUE if added / OSIX_FALSE                           */
/*                                                                          */
/****************************************************************************/

PUBLIC INT4
O3RedRmAddToRxmtLst (tV3OsLsaInfo * pLsaInfo)
{
    tV3OsLsHeader       lsHeader;
    tV3OsArea          *pArea = NULL;
    tV3OsInterface     *pInterface = NULL;
    tTMO_SLL_NODE      *pAreaNode = NULL;
    tTMO_SLL_NODE      *pIntfNode = NULL;
    INT4                i4RetVal = OSIX_FALSE;
    UINT1               u1FloodScope = 0;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedRmAddToRxmtLst\r\n");

    u1FloodScope = V3LsuGetLsaFloodScope (pLsaInfo->lsaId.u2LsaType);

    TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->areasLst), pAreaNode, tTMO_SLL_NODE *)
    {
        pArea = OSPFV3_GET_BASE_PTR (tV3OsArea, nextArea, pAreaNode);

        /* If the LSA type is inter area router LSA,  then do not send
         * the LSA to stub or nssa area.
         * If the LSA type is inter area prefix LSA, then do not send
         * the LSA to stub or nssa area if the summary functionality
         * is disabled
         * If the LSA is AS flood scope, then do not add the LSA is
         * area other than normal area
         */

        if ((pArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
            (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA))
        {
            continue;
        }

        if ((pArea->u4AreaType != OSPFV3_NORMAL_AREA) &&
            (pLsaInfo->lsaId.u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA) &&
            (!OSPFV3_IS_SEND_AREA_SUMMARY (pArea)))
        {
            continue;
        }

        if ((u1FloodScope == OSPFV3_AS_FLOOD_SCOPE) &&
            (pArea->u4AreaType != OSPFV3_NORMAL_AREA))
        {
            continue;
        }

        if ((V3UtilAreaIdComp (pArea->areaId,
                               pLsaInfo->pArea->areaId)) != OSPFV3_EQUAL)
        {
            /* Do not add the link scope and area scope LSA in other areas
             * if it is not DNA LSA
             */
            if (u1FloodScope != OSPFV3_AS_FLOOD_SCOPE)
            {
                continue;
            }
        }

        /* Scan the nbrs in the area and add to the nbr rxmt list */
        TMO_SLL_Scan (&(pArea->ifsInArea), pIntfNode, tTMO_SLL_NODE *)
        {
            pInterface = OSPFV3_GET_BASE_PTR (tV3OsInterface, nextIfInArea,
                                              pIntfNode);

            if (O3RedRmAddtoIntfRxmt (pLsaInfo, pInterface) == OSIX_TRUE)
            {
                i4RetVal = OSIX_TRUE;
            }
        }
    }

    if (u1FloodScope != OSPFV3_AS_FLOOD_SCOPE)
    {
        MEMSET (&lsHeader, 0, sizeof (tV3OsLsHeader));
        V3UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);

        if (lsHeader.u2LsaAge & OSPFV3_DO_NOT_AGE)
        {
            TMO_SLL_Scan (&(pLsaInfo->pV3OspfCxt->virtIfLst), pIntfNode,
                          tTMO_SLL_NODE *)
            {
                pInterface =
                    OSPFV3_GET_BASE_PTR (tV3OsInterface, nextVirtIfNode,
                                         pIntfNode);

                if (O3RedRmAddtoIntfRxmt (pLsaInfo, pInterface) == OSIX_TRUE)
                {
                    i4RetVal = OSIX_TRUE;
                }
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedRmAddToRxmtLst\r\n");

    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmAddtoIntfRxmt                                      */
/*                                                                          */
/* Description  : This function is called when standby node gets the bulk   */
/*                update or dynamic update message. This function adds the  */
/*                LSA to the rxmt list of the neighbor                      */
/*                                                                          */
/* Input        : pLsaInfo    -   Pointer to the LSA info                   */
/*                pInterface  -   Pointer to the interface                  */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : OSIX_TRUE if added / OSIX_FALSE                           */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
O3RedRmAddtoIntfRxmt (tV3OsLsaInfo * pLsaInfo, tV3OsInterface * pInterface)
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    INT4                i4RetVal = OSIX_FALSE;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER : O3RedRmAddtoIntfRxmt\r\n");

    TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
    {
        pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

        if (pNbr->u1NsmState != OSPFV3_NBRS_FULL)
        {
            continue;
        }

        i4RetVal = OSIX_TRUE;
        V3LsuAddToRxmtLst (pNbr, pLsaInfo);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT : O3RedRmAddtoIntfRxmt\r\n");
    return i4RetVal;
}

/****************************************************************************/
/*                                                                          */
/* Function     : O3RedRmSendEvent                                          */
/*                                                                          */
/* Description  : This function calls the RM API to notify the update       */
/*                events                                                    */
/*                                                                          */
/* Input        : u4Event     -    Event to be sent                         */
/*                u4Error     -    Error to be sent                         */
/*                                                                          */
/* Output       : None                                                      */
/*                                                                          */
/* Returns      : None                                                      */
/*                                                                          */
/****************************************************************************/

PUBLIC VOID
O3RedRmSendEvent (UINT4 u4Event, UINT4 u4Error)
{
    tV3OsRedBulkUpdAbortTrapInfo BulkUpdAbortTrapInfo;
    tRmProtoEvt         ProtoEvt;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RedRmSendEvent\r\n");

    MEMSET (&BulkUpdAbortTrapInfo, 0, sizeof (tV3OsRedBulkUpdAbortTrapInfo));
    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    ProtoEvt.u4Event = u4Event;
    ProtoEvt.u4Error = u4Error;

    if (u4Event == RM_BULK_UPDT_ABORT)
    {
        gV3OsRtr.ospfRedInfo.u4DynBulkUpdatStatus = OSPFV3_BULK_UPDT_ABORTED;
    }

    if (u4Error != RM_NONE)
    {
        OSPFV3_GBL_TRC (OSPFV3_RM_TRC | OSPFV3_CRITICAL_TRC,
                        "Error occurred during sync-up\r\n");
    }

    if (V3OspfSendEventToRm (&ProtoEvt) == OSIX_SUCCESS)
    {
        OSPFV3_GBL_TRC1 (OSPFV3_RM_TRC, "Sent event : %d\r\n", u4Event);

        if (u4Event == RM_BULK_UPDT_ABORT)
        {
            OSPFV3_RTR_ID_COPY
                (&BulkUpdAbortTrapInfo.rtrId,
                 &gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID]->rtrId);
            BulkUpdAbortTrapInfo.i4DynamicBulkUpdStatus =
                OSPFV3_RED_DYN_BULK_ABORTED;
            BulkUpdAbortTrapInfo.u1BulkUpdateAbortReason = (UINT1) u4Error;
            O3SnmpIfSendTrapInCxt (gV3OsRtr.apV3OspfCxt[OSPFV3_DEFAULT_CXT_ID],
                                   OSPFV3_RED_BULK_UPD_ABORT_TRAP,
                                   &BulkUpdAbortTrapInfo);
        }
    }
    else
    {
        OSPFV3_GBL_TRC1 (OSPFV3_RM_TRC, "Event %d send failure\r\n", u4Event);
    }
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3RedRmSendEvent\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : O3RmHwAudit                                          */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. When Audit event is received during Standby to*/
/*                       active state change, interface list in ospfv3       */
/*                      is scanned and oper status is compared with ipv6     */
/*                      oper status, if there is a difference then interface */
/*                      status change is triggerd in ospfv3                  */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
O3RmHwAudit (VOID)
{
    tV3OsInterface     *pInterface;
    tNetIpv6IfInfo      NetIp6IfInfo;
    UINT1               u1IfOperStat = 0;
    tNetIpv6HliParams   Ip6HlParams;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RmHwAudit\r\n");

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);

    if (pInterface != NULL)
    {
        do
        {
            MEMSET (&Ip6HlParams, ZERO, sizeof (tNetIpv6HliParams));
            if (NetIpv6GetIfInfo (pInterface->u4InterfaceId, &NetIp6IfInfo) ==
                NETIPV6_FAILURE)
            {
                break;
            }
            u1IfOperStat = (NetIp6IfInfo.u4Oper == NETIPV6_IF_UP) ?
                OSPFV3_ENABLED : OSPFV3_DISABLED;

            if (pInterface->operStatus != u1IfOperStat)
            {
                /* call the routine to handle oper status change */
                Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Index =
                    pInterface->u4InterfaceId;
                Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4IfStat =
                    NetIp6IfInfo.u4Oper;
                Ip6HlParams.u4Command = NETIPV6_INTERFACE_PARAMETER_CHANGE;
                Ip6HlParams.unIpv6HlCmdType.IfStatChange.u4Mask =
                    NETIPV6_INTERFACE_STATUS_CHANGE;
                V3Ipv6OspfInterface (&Ip6HlParams);

            }
        }
        while ((pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface,
                                                  NULL)) != NULL);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RmHwAudit\r\n");
    return;

}

/*****************************************************************************/
/* Function Name      : O3RmHandleDynSyncAudit                               */
/*                                                                           */
/* Description        : This function Handles the Dynamic Sync-up Audit      */
/*                      event. This event is used to start the audit         */
/*                      between the active and standby units for the         */
/*                      dynamic sync-up happened                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
O3RmHandleDynSyncAudit (VOID)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER : O3RmHandleDynSyncAudit\r\n");
    /*On receiving this event, ospf3 should execute show cmd and calculate checksum */
    O3ExecuteCmdAndCalculateChkSum ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT : O3RmHandleDynSyncAudit\r\n");
}

/*****************************************************************************/
/* Function Name      : O3ExecuteCmdAndCalculateChkSum                       */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then sent to RM.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
O3ExecuteCmdAndCalculateChkSum (VOID)
{
    /*Execute CLI commands and calculate checksum */
    UINT2               u2AppId = RM_OSPFV3_APP_ID;
    UINT2               u2ChkSum = 0;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY,
                    "ENTER : O3ExecuteCmdAndCalculateChkSum\r\n");
    V3OspfUnLock ();
    if (V3GetShowCmdOutputAndCalcChkSum (&u2ChkSum) == OSPFV3_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_RM_TRC,
                        "Checksum of calculation failed for VCM\n");
        V3OspfLock ();
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3ExecuteCmdAndCalculateChkSum\r\n");
        return;
    }
#ifdef L2RED_WANTED
    if (V3RmEnqChkSumMsgToRm (u2AppId, u2ChkSum) == OSPFV3_FAILURE)
    {
        OSPFV3_GBL_TRC (OSPFV3_RM_TRC, "Sending checkum to RM failed\n");
        V3OspfLock ();
        OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                        "EXIT : O3ExecuteCmdAndCalculateChkSum\r\n");
        return;
    }
#else
    UNUSED_PARAM (u2AppId);
#endif
    V3OspfLock ();
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT,
                    "EXIT : O3ExecuteCmdAndCalculateChkSum\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : V3OspfRmHandleGoActiveEventNotify                    */
/*                                                                           */
/* Description        : This function Handles the execution of show commands */
/*                      and calculation of checksum with the output.         */
/*                      The calculated value is then sent to RM.             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
V3OspfRmHandleGoActiveEventNotify ()
{
    tTMO_SLL_NODE      *pLstNode = NULL;
    tV3OsNeighbor      *pNbr = NULL;
    tV3OsInterface     *pInterface = NULL;
    UINT4               u4HelloTmrInterval = 0;

    pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
    while (pInterface != NULL)
    {

        TMO_SLL_Scan (&(pInterface->nbrsInIf), pLstNode, tTMO_SLL_NODE *)
        {
            pNbr = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextNbrInIf, pLstNode);

            if (pInterface->bPassive != OSPFV3_TRUE)
            {
                V3HpSendHello (pNbr->pInterface, pNbr, OSPFV3_HELLO_TIMER);
                u4HelloTmrInterval =
                    OSPFV3_NO_OF_TICKS_PER_SEC * (pInterface->u2HelloInterval);
                TmrStopTimer (gV3OsRtr.hellotimerLstId,
                              &(pInterface->helloTimer.timerNode));

                gu1O3HelloSwitchOverFlag = 1;

                V3TmrSetTimer (&(pInterface->helloTimer), OSPFV3_HELLO_TIMER,
                               ((pInterface->u1NetworkType ==
                                 OSPFV3_IF_BROADCAST) ?
                                V3UtilJitter (OSPFV3_NO_OF_TICKS_PER_SEC *
                                              (pInterface->u2HelloInterval),
                                              OSPFV3_JITTER) :
                                u4HelloTmrInterval));
            }
        }
        pInterface = (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                       (tRBElem *) pInterface,
                                                       NULL);
    }
}

/*--------------------------------------------------------------------------*/
/*                  End of the file o3redrm.c                               */
/*--------------------------------------------------------------------------*/
