/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3grlsu.c,v 1.5 2017/12/26 13:34:27 siva Exp $
 *
 * Description: This file contains the LSU processing done at restarting router
 *              during Graceful restart process.
 *
 *******************************************************************/
#include "o3inc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuGetLsaDesc                                          */
/*                                                                           */
/* Description  : This routine constructs the Lsa desc for the self org LSA  */
/*                received from the nbr during graceful restart mode         */
/*                                                                           */
/* Input        : pLsHeader       - pointer to LS Header                     */
/*                pNbr           -  pointer to the neighbor                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pointer to Lsa Desc                                        */
/*                                                                           */
/*****************************************************************************/
PRIVATE tV3OsLsaDesc *
O3GrLsuGetLsaDesc (tV3OsLsHeader * pLsHeader, tV3OsNeighbor * pNbr)
{
    UINT1              *pTmpPtr = NULL;
    UINT2               u2LsaType = OSPFV3_INIT_VAL;
    tV3OsLsaDesc       *pLsaDesc = NULL;
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrLsuGetLsaDesc\r\n");

    u2LsaType = pLsHeader->u2LsaType;
    if (u2LsaType == OSPFV3_AS_EXT_LSA)
    {
        pTmpPtr = NULL;
    }
    else if ((u2LsaType == OSPFV3_LINK_LSA) || (u2LsaType == OSPFV3_GRACE_LSA))
    {
        pTmpPtr = (UINT1 *) pNbr->pInterface;
    }
    else
    {
        pTmpPtr = (UINT1 *) pNbr->pInterface->pArea;
    }

    pLsaDesc = V3GetLsaDescInCxt (pNbr->pInterface->pArea->pV3OspfCxt,
                                  u2LsaType, &(pLsHeader->linkStateId),
                                  pTmpPtr);
    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrLsuGetLsaDesc\r\n");
    return pLsaDesc;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuSearchRtrLsa                                        */
/*                                                                           */
/* Description  : This routine scans the router LSA and searches for the     */
/*                given node                                                 */
/*                                                                           */
/* Input        : pLsaInfo      - pointer to LSA info structure              */
/*                pRtrLsaRtInfo - pointer to a link                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrLsuSearchRtrLsa (tV3OsLsaInfo * pLsaInfo, tV3OsRtrLsaRtInfo * pRtrLsaRtInfo)
{
    tV3OsRtrLsaRtInfo   nbrRtrLsaRtInfo;
    tV3OsLsHeader       lsHeader;
    UINT1              *pu1NbrLsaPtr = NULL;

    MEMSET (&lsHeader, OSPFV3_INIT_VAL, sizeof (tV3OsLsHeader));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pLsaInfo->pV3OspfCxt->u4ContextId,
                "ENTER :O3GrLsuSearchRtrLsa\r\n");

    /* Skip the LSA header, options fields */
    pu1NbrLsaPtr = pLsaInfo->pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;
    V3UtilExtractLsHeaderFromLbuf (pLsaInfo->pLsa, &lsHeader);

    while ((pu1NbrLsaPtr - pLsaInfo->pLsa) < lsHeader.u2LsaLen)
    {
        MEMSET (&nbrRtrLsaRtInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
        O3GrLsuGetLinksFromLsa (pu1NbrLsaPtr, &nbrRtrLsaRtInfo);

        /*Increment the Pointer for taking next link */
        pu1NbrLsaPtr = pu1NbrLsaPtr + OSPFV3_RTR_LSA_LINK_LEN;

        /* In this case, link type = transit, link id = interface ip
         * address (since this interface is DR, link data = Nbr ip
         * address */
        if ((nbrRtrLsaRtInfo.u1LinkType
             == pRtrLsaRtInfo->u1LinkType) &&
            (nbrRtrLsaRtInfo.u4NbrIfId == pRtrLsaRtInfo->u4NbrIfId) &&
            (V3UtilRtrIdComp (nbrRtrLsaRtInfo.nbrRtrId,
                              pRtrLsaRtInfo->nbrRtrId) == OSPFV3_EQUAL))
        {
            return OSIX_TRUE;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pLsaInfo->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuSearchRtrLsa\r\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuCheckSelfOrigLsa                                    */
/*                                                                           */
/* Description  : This routine checks for LSA inconsistency if the           */
/*                received LSA is self-originated LSA                        */
/*                                                                           */
/* Input        : pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE     -     if LSA is inconsistent                 */
/*                OSIX_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrLsuCheckSelfOrigLsa (tV3OsArea * pArea,
                         tV3OsLsHeader * pLsHeader, UINT1 *pLsa)
{
    tV3OsRtrLsaRtInfo   rtrLsaRtInfo;    /* Contains the particular link
                                           information from received
                                           self-originated router LSA */
    tV3OsRtrLsaRtInfo   rtrLsaSearchRtrInfo;
    /* Contains the link info to be
       searched from Nbr router LSA */
    tV3OsInterface     *pInterface = NULL;
    /* pointer to interface */
    tV3OsNeighbor      *pNeighbor = NULL;    /* pointer to neighbor */
    tV3OsLsaInfo       *pLsaInfo = NULL;    /* pointer to Nbr router LSA */
    tTMO_SLL_NODE      *pLstNode = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tV3OsRouterId       attachedRtrId;    /* Nbr id from network LSA */
    tV3OspfCxt         *pV3OspfCxt = pArea->pV3OspfCxt;
    UINT1              *pu1CurrPtr = NULL;
    /* pointer to scan LSA */
    UINT2               u2LsaType = pLsHeader->u2LsaType;

    MEMSET (&rtrLsaRtInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
    MEMSET (&rtrLsaSearchRtrInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
    MEMSET (attachedRtrId, OSPFV3_INIT_VAL, sizeof (attachedRtrId));

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: O3GrLsuCheckSelfOrigLsa\r\n");

    if (u2LsaType == OSPFV3_ROUTER_LSA)
    {
        /* If self-originated router LSA is received, then router LSA
         * originated by each neighbor in that interface should have a back
         * link (Transit or p2p) */

        /* Skip the LSA header, options fields */
        pu1CurrPtr = pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;

        /* No more links are there in present LSA  */
        while ((pu1CurrPtr - pLsa) < pLsHeader->u2LsaLen)
        {
            MEMSET (&rtrLsaRtInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
            O3GrLsuGetLinksFromLsa (pu1CurrPtr, &rtrLsaRtInfo);

            /*Increment the Pointer for taking next link */
            pu1CurrPtr = pu1CurrPtr + OSPFV3_RTR_LSA_LINK_LEN;

            if (rtrLsaRtInfo.u1LinkType == OSPFV3_TRANSIT_LINK)
            {
                pInterface = (tV3OsInterface *) RBTreeGetFirst
                    (gV3OsRtr.pIfRBRoot);
                while (pInterface != NULL)
                {
                    if (pInterface->u4InterfaceId == rtrLsaRtInfo.u4InterfaceId)
                    {
                        break;
                    }
                    pInterface = (tV3OsInterface *) RBTreeGetNext
                        (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface, NULL);
                }

                if (pInterface == NULL)
                {
                    /* Interface not available. Inconsistent LSA */
                    OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                     pArea->pV3OspfCxt->u4ContextId,
                                     "Inconsistent LSA"
                                     " Interface 0x%x not found \r\n",
                                     rtrLsaRtInfo.u4InterfaceId);
                    return OSIX_TRUE;
                }

                TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode,
                              tTMO_SLL_NODE *)
                {
                    pNeighbor = OSPFV3_GET_BASE_PTR
                        (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                    /* Search the database for the router LSA of neighbor */
                    pLsaInfo =
                        V3LsuSearchDatabase
                        (OSPFV3_ROUTER_LSA, &(pNeighbor->linkStateId),
                         &(pNeighbor->nbrRtrId), NULL, pArea);
                    if (pLsaInfo == NULL)
                    {
                        continue;
                    }

                    /* A link with type = transit, link id = DR ip address and
                     * link data = nbr ip address should exist. Else LSA is
                     * inconsistent */
                    MEMSET (&rtrLsaSearchRtrInfo, OSPFV3_INIT_VAL,
                            sizeof (tV3OsRtrLsaRtInfo));
                    rtrLsaSearchRtrInfo.u4NbrIfId = pInterface->u4DRInterfaceId;
                    OSPFV3_RTR_ID_COPY (&(rtrLsaSearchRtrInfo.nbrRtrId),
                                        pNeighbor->pInterface->desgRtr);
                    rtrLsaSearchRtrInfo.u1LinkType = rtrLsaRtInfo.u1LinkType;

                    if (O3GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                        == OSIX_FALSE)
                    {
                        return OSIX_TRUE;
                    }
                }
            }
            else
            {
                /* p2p or vitual link */
                /* In this case, the neighbor should also have a p2p or virtual
                 * link to the restarting router, else LSA is inconsistent */
                if (rtrLsaRtInfo.u1LinkType == OSPFV3_PTOP_LINK)
                {
                    pInterface = (tV3OsInterface *) RBTreeGetFirst
                        (gV3OsRtr.pIfRBRoot);
                    while (pInterface != NULL)
                    {
                        if (pInterface->u4InterfaceId ==
                            rtrLsaRtInfo.u4InterfaceId)
                        {
                            break;
                        }
                        pInterface = (tV3OsInterface *) RBTreeGetNext
                            (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface, NULL);
                    }
                    if (pInterface == NULL)
                    {
                        /* Interface not available. Inconsistent LSA */
                        OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                         pArea->pV3OspfCxt->u4ContextId,
                                         "Inconsistent LSA Interface 0x%x not found \r\n",
                                         rtrLsaRtInfo.u4InterfaceId);
                        return OSIX_TRUE;
                    }
                }
                else
                {
                    TMO_SLL_Scan (&(pV3OspfCxt->virtIfLst),
                                  pLstNode, tTMO_SLL_NODE *)
                    {
                        pInterface = OSPFV3_GET_BASE_PTR
                            (tV3OsInterface, nextVirtIfNode, pLstNode);

                        if ((pInterface != NULL) &&
                            (pInterface->u4InterfaceId ==
                             rtrLsaRtInfo.u4InterfaceId))
                        {
                            break;
                        }
                    }

                    if (pLstNode == NULL)
                    {
                        /* Interface not available. Inconsistent LSA */
                        OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                         pArea->pV3OspfCxt->u4ContextId,
                                         "Inconsistent LSA Interface 0x%x not found \r\n",
                                         rtrLsaRtInfo.u4InterfaceId);
                        return OSIX_TRUE;
                    }
                }

                if ((pNbrNode = TMO_SLL_First (&(pInterface->nbrsInIf)))
                    == NULL)
                {
                    /* Neighbor not found */
                    return OSIX_FALSE;
                }

                pNeighbor = OSPFV3_GET_BASE_PTR
                    (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                /* Search the database for the router LSA of neighbor */
                pLsaInfo =
                    V3LsuSearchDatabase (OSPFV3_ROUTER_LSA,
                                         &(pNeighbor->linkStateId),
                                         &(pNeighbor->nbrRtrId), NULL, pArea);
                if (pLsaInfo == NULL)
                {
                    continue;
                }

                MEMSET (&rtrLsaSearchRtrInfo, OSPFV3_INIT_VAL,
                        sizeof (tV3OsRtrLsaRtInfo));
                rtrLsaSearchRtrInfo.u4NbrIfId = pInterface->u4InterfaceId;
                OSPFV3_RTR_ID_COPY (&(rtrLsaSearchRtrInfo.nbrRtrId),
                                    pV3OspfCxt->rtrId);
                rtrLsaSearchRtrInfo.u1LinkType = rtrLsaRtInfo.u1LinkType;

                if (O3GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                    == OSIX_FALSE)
                {
                    /* Inconsistent */
                    OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                     pArea->pV3OspfCxt->u4ContextId,
                                     "Inconsistent LSA "
                                     "Nbr 0x%x does not have p2p link to router\r\n",
                                     OSPFV3_BUFFER_DWFROMPDU (pNeighbor->
                                                              nbrRtrId));
                    return OSIX_TRUE;
                }
            }
        }
    }
    else if (u2LsaType == OSPFV3_NETWORK_LSA)
    {
        /* If self-originated network LSA is received, then router LSA
         * originated by each router-id in the LSA should have a transit link
         * to the same network */

        /* Skip the LSA header and Option field */
        pu1CurrPtr = pLsa + OSPFV3_NTWR_LSA_FIXED_PORTION;

        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        while (pInterface != NULL)
        {
            if ((V3UtilRtrIdComp (pInterface->desgRtr,
                                  pLsHeader->advRtrId) == OSPFV3_EQUAL) ||
                (V3UtilRtrIdComp (pInterface->backupDesgRtr,
                                  pLsHeader->advRtrId) == OSPFV3_EQUAL))
            {
                break;
            }
            pInterface = (tV3OsInterface *) RBTreeGetNext
                (gV3OsRtr.pIfRBRoot, (tRBElem *) pInterface, NULL);
        }

        if (pInterface == NULL)
        {
            /* Advertised network LSA does not belong to any of
             * the interface. Hence LSA is not inconsistent */
            OSPFV3_EXT_TRC (OSPFV3_RESTART_TRC, pArea->pV3OspfCxt->u4ContextId,
                            "Advertised n/w LSA does not belong to"
                            " any interface\r\n");
            return OSIX_FALSE;
        }

        while (pu1CurrPtr < (pLsa + pLsHeader->u2LsaLen))
        {
            MEMSET (&attachedRtrId, OSPFV3_INIT_VAL, OSPFV3_RTR_ID_LEN);
            OSPFV3_LGETSTR (pu1CurrPtr, attachedRtrId, OSPFV3_RTR_ID_LEN);

            /* Scan all the neighbors in this interface to match the neighbor
             * id and get the corresponding neighbor ip address */
            if (V3UtilRtrIdComp (pV3OspfCxt->rtrId, attachedRtrId)
                == OSPFV3_EQUAL)
            {
                continue;
            }

            TMO_SLL_Scan (&(pInterface->nbrsInIf), pNbrNode, tTMO_SLL_NODE *)
            {
                pNeighbor = OSPFV3_GET_BASE_PTR
                    (tV3OsNeighbor, nextNbrInIf, pNbrNode);

                if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, attachedRtrId)
                    == OSPFV3_EQUAL)
                {
                    break;
                }
            }
            if (pNbrNode == NULL)
            {
                /* Neighbor is not found. The helper must have gone down
                 * during restart process or hello packet is not yet received 
                 * from that neighbor. So skip this inconsistancy check */
                OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                 pArea->pV3OspfCxt->u4ContextId,
                                 "Attached neighbor id 0x%x is not found",
                                 OSPFV3_BUFFER_DWFROMPDU (attachedRtrId));
                return OSIX_FALSE;
            }
            /* Search the database for the router LSA of attached router-id */
            pLsaInfo =
                V3LsuSearchDatabase (OSPFV3_ROUTER_LSA,
                                     &(pNeighbor->linkStateId),
                                     &(attachedRtrId), NULL, pArea);

            if (pLsaInfo == NULL)
            {
                continue;
            }

            MEMSET (&rtrLsaSearchRtrInfo, OSPFV3_INIT_VAL,
                    sizeof (tV3OsRtrLsaRtInfo));
            rtrLsaSearchRtrInfo.u4NbrIfId = pInterface->u4DRInterfaceId;
            OSPFV3_RTR_ID_COPY (&(rtrLsaSearchRtrInfo.nbrRtrId),
                                pNeighbor->pInterface->desgRtr);
            rtrLsaSearchRtrInfo.u1LinkType = OSPFV3_TRANSIT_LINK;

            if (O3GrLsuSearchRtrLsa (pLsaInfo, &rtrLsaSearchRtrInfo)
                == OSIX_FALSE)
            {
                /* Inconsistent */
                OSPFV3_EXT_TRC2 (OSPFV3_RESTART_TRC,
                                 pArea->pV3OspfCxt->u4ContextId,
                                 "Inconsistent LSA "
                                 "Attached neighbor id 0x%x does not have a transit link"
                                 " for DR 0x%x\r\n",
                                 OSPFV3_BUFFER_DWFROMPDU (attachedRtrId),
                                 OSPFV3_BUFFER_DWFROMPDU (pNeighbor->
                                                          pInterface->desgRtr));
                return OSIX_TRUE;
            }
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuCheckSelfOrigLsa\r\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuIsNbrOrigLsaInCxt                                   */
/*                                                                           */
/* Description  : This routine checks whether the LSA is originated by       */
/*                one of the attached neighbors                              */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                pLsHeader    - LSA header                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE     -     if LSA is nbr-originated               */
/*                OSIX_FALSE    -     if LSA is not nbr-originated           */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
O3GrLsuIsNbrOrigLsaInCxt (tV3OspfCxt * pV3OspfCxt, tV3OsLsHeader * pLsHeader)
{
    tV3OsNeighbor      *pNeighbor = NULL;
    tTMO_SLL_NODE      *pLstNode = NULL;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                " ENTER: O3GrLsuIsNbrOrigLsaInCxt\r\n");

    TMO_SLL_Scan (&(pV3OspfCxt->sortNbrLst), pLstNode, tTMO_SLL_NODE *)
    {
        pNeighbor = OSPFV3_GET_BASE_PTR (tV3OsNeighbor, nextSortNbr, pLstNode);

        if (V3UtilRtrIdComp (pNeighbor->nbrRtrId, pLsHeader->advRtrId)
            == OSPFV3_EQUAL)
        {
            return OSIX_TRUE;
        }
    }
    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuIsNbrOrigLsaInCxt\r\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuDelLinkInfoFrmRtrTree                               */
/*                                                                           */
/* Description  : This routine deletes the link info from router LSA         */
/*                This is called only for p2p and virtual link               */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                nbrRtrLsaRtInfo - Router LSA link info                     */
/*                pLsHeader       - pointer to LS Header                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
O3GrLsuDelLinkInfoFrmRtrTree (tV3OspfCxt * pV3OspfCxt,
                              tV3OsRtrLsaRtInfo nbrRtrLsaRtInfo,
                              tV3OsLsHeader * pLsHeader)
{
    tV3OsRtrLsaRtInfo   delRtrLsaLinkInfo;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pV3OspfCxt->u4ContextId,
                "ENTER: O3GrLsuDelLinkInfoFrmRtrTree\r\n");

    MEMSET (&delRtrLsaLinkInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));

    /* If the link does not belong to the restarting router, return */
    if (V3UtilRtrIdComp (pV3OspfCxt->rtrId,
                         nbrRtrLsaRtInfo.nbrRtrId) != OSPFV3_EQUAL)
    {
        return;
    }

    delRtrLsaLinkInfo.u1LinkType = nbrRtrLsaRtInfo.u1LinkType;
    OSPFV3_RTR_ID_COPY (&delRtrLsaLinkInfo.nbrRtrId, pLsHeader->advRtrId);
    delRtrLsaLinkInfo.u4NbrIfId = nbrRtrLsaRtInfo.u4InterfaceId;
    delRtrLsaLinkInfo.u4InterfaceId = nbrRtrLsaRtInfo.u4NbrIfId;
    RBTreeRem (gV3OsRtr.pRtrLsaCheck, (tRBElem *) & delRtrLsaLinkInfo);

    OSPFV3_TRC (OSPFV3_FN_EXIT, pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuDelLinkInfoFrmRtrTree\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuSearchRtrLsaRBTree                                  */
/*                                                                           */
/* Description  : This routine scan the router LSA link info RBTree          */
/*                During nbr router LSA scan, all the p2p/virtual links      */
/*                should have been deleted from RB Tree. If there is         */
/*                any p2p/virtual link available to the advertising          */
/*                router, then LSA is inconcistent                           */
/*                                                                           */
/* Input        : pLsHeader    - pointer to LS Header                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE/OSIX_FALSE                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
O3GrLsuSearchRtrLsaRBTree (tV3OsLsHeader * pLsHeader)
{
    tV3OsRtrLsaRtInfo  *pRtrLsaCheck = NULL;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrLsuSearchRtrLsaRBTree\r\n");

    if (gV3OsRtr.pRtrLsaCheck != NULL)
    {
        pRtrLsaCheck = (tV3OsRtrLsaRtInfo *) RBTreeGetFirst
            (gV3OsRtr.pRtrLsaCheck);

        while (pRtrLsaCheck != NULL)
        {
            /* Check whether it is the re-transmission node for Grace LSA */
            if ((pRtrLsaCheck->u1LinkType != OSPFV3_TRANSIT_LINK) &&
                (V3UtilRtrIdComp (pRtrLsaCheck->nbrRtrId,
                                  pLsHeader->advRtrId) == OSPFV3_EQUAL))
            {
                return OSIX_TRUE;
            }
            pRtrLsaCheck = (tV3OsRtrLsaRtInfo *) RBTreeGetNext
                (gV3OsRtr.pRtrLsaCheck, (tRBElem *) pRtrLsaCheck, NULL);
        }
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrLsuSearchRtrLsaRBTree\r\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuCheckNbrLsa                                         */
/*                                                                           */
/* Description  : This routine checks for LSA inconsistency if the           */
/*                received LSA is nbr-originated LSA                         */
/*                                                                           */
/* Input        : pV3OspfCxt   - Pointer to context                          */
/*                pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE     -     if LSA is inconsistent                 */
/*                OSIX_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
O3GrLsuCheckNbrLsa (tV3OsArea * pArea, tV3OsLsHeader * pLsHeader, UINT1 *pLsa)
{
    tV3OsLsaInfo       *pLsaInfo = NULL;    /* pointer to LsaInfo */
    tV3OsInterface     *pInterface = NULL;    /* pointer to interface */
    tV3OsRtrLsaRtInfo   nbrRtrLsaRtInfo;    /* Structure to store
                                             * the LSA links */
    tV3OsRtrLsaRtInfo   selfRtrLsaRtInfo;    /* Used for RBTree search 
                                               for links from self 
                                               router LSA */
    tV3OsRouterId       attachedRtrId;
    tRBElem            *pRBElem = NULL;
    tV3OsDbNode        *pDbHashNode = NULL;
    tTMO_SLL_NODE      *pDbLstNode = NULL;
    tV3OspfCxt         *pV3OspfCxt = pArea->pV3OspfCxt;
    UINT4               u4HashIndex = OSPFV3_INIT_VAL;
    INT4                i4RetVal = OSIX_FALSE;
    UINT1              *pu1CurrPtr = NULL;    /* Current LSA pointer */
    UINT2               u2LsaType = pLsHeader->u2LsaType;
    UINT1               u1NetworkFlag = OSIX_FALSE;

    MEMSET (&nbrRtrLsaRtInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
    MEMSET (&selfRtrLsaRtInfo, OSPFV3_INIT_VAL, sizeof (tV3OsRtrLsaRtInfo));
    MEMSET (attachedRtrId, OSPFV3_INIT_VAL, sizeof (tV3OsRouterId));
    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER: O3GrLsuCheckNbrLsa\r\n");

    if (u2LsaType == OSPFV3_ROUTER_LSA)
    {
        /* Neighbor originated router LSA */
        /* Inconsistency occurs in the following scenarios
         * In both these cases, Nbr has declared the restarting router as down
         *
         * 1. In broadcast/nbma/p2mp network, restarting router has a link
         *    to transit network but neighbor has a link to stub network
         * 2. In other networks, neighbor has advertised only a stub link
         *    but restarting router has advertised a stub link and a p2p
         *    link.
         */

        u4HashIndex = V3LsaRtrIdHashFunc (&(pV3OspfCxt->rtrId));
        TMO_HASH_Scan_Bucket (pArea->pRtrLsaHashTable, u4HashIndex,
                              pDbHashNode, tV3OsDbNode *)
        {
            pDbLstNode = TMO_SLL_First (&pDbHashNode->lsaLst);
            pLsaInfo =
                OSPFV3_GET_BASE_PTR (tV3OsLsaInfo, nextLsaInfo, pDbLstNode);
            if (V3UtilRtrIdComp (pLsaInfo->lsaId.advRtrId, pV3OspfCxt->rtrId) ==
                OSPFV3_EQUAL)

            {
                break;
            }
        }
        if (pLsaInfo == NULL)
        {
            /* Restarting router has not received the self-originated LSA */
            return OSIX_FALSE;
        }
        /* Scan the LSA and store the links in pRtrLsaCheck RBTree */
        if (V3UtilConstRtrLsaRBTree (pLsaInfo->pLsa) == OSIX_FAILURE)
        {
            return OSIX_FALSE;
        }

        /* Scan the LSA received from the neighbor */
        /* Skip the LSA header, options fields */
        pu1CurrPtr = pLsa + OSPFV3_RTR_LSA_FIXED_PORTION;

        /* No more links are there in present LSA  */
        while ((pu1CurrPtr - pLsa) < pLsHeader->u2LsaLen)
        {
            MEMSET (&nbrRtrLsaRtInfo, OSPFV3_INIT_VAL,
                    sizeof (tV3OsRtrLsaRtInfo));
            O3GrLsuGetLinksFromLsa (pu1CurrPtr, &nbrRtrLsaRtInfo);

            /*Increment the Pointer for taking next link */
            pu1CurrPtr = pu1CurrPtr + OSPFV3_RTR_LSA_LINK_LEN;

            /* If link type is p2p or virtual, then delete the corresponding
             * link advertised by the restarting router from RBTree. After 
             * scanning the LSA, if any p2p or virtual link advertised by
             * restarting router to the advertised router-id is available,
             * then the LSA is inconsistent */
            if ((nbrRtrLsaRtInfo.u1LinkType == OSPFV3_PTOP_LINK) ||
                (nbrRtrLsaRtInfo.u1LinkType == OSPFV3_VIRTUAL_LINK))
            {
                O3GrLsuDelLinkInfoFrmRtrTree
                    (pV3OspfCxt, nbrRtrLsaRtInfo, pLsHeader);
                continue;
            }

            /* Scan all interfaces and check designated router matches */
            pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
            while (pInterface != NULL)
            {
                if (((pInterface->u1NetworkType == OSPFV3_IF_NBMA) ||
                     (pInterface->u1NetworkType == OSPFV3_IF_BROADCAST)) &&
                    (V3UtilRtrIdComp (pInterface->desgRtr,
                                      nbrRtrLsaRtInfo.nbrRtrId)
                     == OSPFV3_EQUAL) &&
                    (pInterface->u4DRInterfaceId == nbrRtrLsaRtInfo.u4NbrIfId))
                {
                    MEMSET (&selfRtrLsaRtInfo, OSPFV3_INIT_VAL,
                            sizeof (tV3OsRtrLsaRtInfo));
                    selfRtrLsaRtInfo.u1LinkType = OSPFV3_TRANSIT_LINK;
                    OSPFV3_RTR_ID_COPY (&selfRtrLsaRtInfo.nbrRtrId,
                                        pInterface->desgRtr);
                    selfRtrLsaRtInfo.u4NbrIfId = pInterface->u4DRInterfaceId;

                    pRBElem = RBTreeRem (gV3OsRtr.pRtrLsaCheck,
                                         (tRBElem *) & selfRtrLsaRtInfo);
                    if (pRBElem == NULL)
                    {
                        /* Neighbor has originated a stub link but restarting
                         * router originated a transit link. This LSA is
                         * inconsistent */
                        RBTreeDrain (gV3OsRtr.pRtrLsaCheck,
                                     V3UtilRBFreeRouterLinks, OSPFV3_INIT_VAL);
                        OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                                         pArea->pV3OspfCxt->u4ContextId,
                                         "Inconsistent LSA "
                                         "Restarting router has a transit link"
                                         " for DR 0x%x\r\n",
                                         OSPFV3_BUFFER_DWFROMPDU (pInterface->
                                                                  desgRtr));
                        return OSIX_TRUE;
                    }
                }
                pInterface =
                    (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                      (tRBElem *) pInterface,
                                                      NULL);
            }
        }
        /* Scan the RB Tree and check if any p2p or virtual link is there
         * to advertising router. If there is any link, then the LSA is
         * is inconsistent */
        i4RetVal = O3GrLsuSearchRtrLsaRBTree (pLsHeader);
        RBTreeDrain (gV3OsRtr.pRtrLsaCheck, V3UtilRBFreeRouterLinks,
                     OSPFV3_INIT_VAL);

        if (i4RetVal == OSIX_TRUE)
        {
            OSPFV3_EXT_TRC (OSPFV3_RESTART_TRC,
                            pArea->pV3OspfCxt->u4ContextId,
                            "Inconsistent LSA "
                            "p2p/virtual link is not advertised by neighbor");
            return OSIX_TRUE;
        }
    }
    else if (u2LsaType == OSPFV3_NETWORK_LSA)
    {
        /* If advertising router id is not DR or BDR of any of the interface,
         * then skip the LSA */
        pInterface = (tV3OsInterface *) RBTreeGetFirst (gV3OsRtr.pIfRBRoot);
        while (pInterface != NULL)
        {
            if ((V3UtilRtrIdComp (pInterface->desgRtr,
                                  pLsHeader->advRtrId) == OSPFV3_EQUAL) ||
                (V3UtilRtrIdComp (pInterface->backupDesgRtr,
                                  pLsHeader->advRtrId) == OSPFV3_EQUAL))
            {
                break;
            }
            pInterface =
                (tV3OsInterface *) RBTreeGetNext (gV3OsRtr.pIfRBRoot,
                                                  (tRBElem *) pInterface, NULL);
        }
        if (pInterface == NULL)
        {
            return OSIX_FALSE;
        }
        /* Neighbor has originated a network LSA. Network LSA needs to be
         * considered in the following scenario
         *
         * If there are more than 2 routers in a transit network and restarting
         * router is not DR, then by quitting helper mode, helper will not send
         * a stub link in router LSA. LSA inconsistency can be identified if
         * the restarting router-id is not seen in the received network LSA */
        /* Skip LSA header */
        pu1CurrPtr = pLsa + OSPFV3_NTWR_LSA_FIXED_PORTION;
        while (pu1CurrPtr < (pLsa + pLsHeader->u2LsaLen))
        {
            MEMSET (&attachedRtrId, OSPFV3_INIT_VAL, OSPFV3_RTR_ID_LEN);

            OSPFV3_LGETSTR (pu1CurrPtr, attachedRtrId, OSPFV3_RTR_ID_LEN);
            if (V3UtilRtrIdComp (attachedRtrId, pV3OspfCxt->rtrId)
                == OSPFV3_EQUAL)
            {
                u1NetworkFlag = OSIX_TRUE;
                break;
            }
        }
        if ((u1NetworkFlag == OSIX_FALSE) &&
            (pu1CurrPtr >= (pLsa + pLsHeader->u2LsaLen)))
        {
            /* Restarting router-id is not found in network LSA */
            OSPFV3_EXT_TRC1 (OSPFV3_RESTART_TRC,
                             pArea->pV3OspfCxt->u4ContextId,
                             "Inconsistent LSA "
                             "Restarting router id is not found in network"
                             " LSA originated by neighbor %x\r\n",
                             OSPFV3_BUFFER_DWFROMPDU (pLsHeader->advRtrId));
            return OSIX_TRUE;
        }
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuCheckNbrLsa\r\n");
    return OSIX_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuGetLinksFromLsa                                     */
/*                                                                           */
/* Description  : This routine scans the router LSA from current pointer     */
/*                and updates the pRtrLsaRtInfo with next link               */
/*                                                                           */
/* Input        : pu1CurrPtr    - Current LSA pointer                        */
/*                                                                           */
/* Output       : pRtrLsaRtInfo - pointer to a link                          */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
O3GrLsuGetLinksFromLsa (UINT1 *pu1CurrPtr, tV3OsRtrLsaRtInfo * pRtrLsaRtInfo)
{
    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrLsuGetLinksFromLsa\r\n");

    pRtrLsaRtInfo->u1LinkType = OSPFV3_LGET1BYTE (pu1CurrPtr);
    /* Skip the Reserved field */
    pu1CurrPtr++;
    pRtrLsaRtInfo->u2LinkMetric = OSPFV3_LGET2BYTE (pu1CurrPtr);
    pRtrLsaRtInfo->u4InterfaceId = OSPFV3_LGET4BYTE (pu1CurrPtr);
    pRtrLsaRtInfo->u4NbrIfId = OSPFV3_LGET4BYTE (pu1CurrPtr);
    OSPFV3_LGETSTR (pu1CurrPtr, pRtrLsaRtInfo->nbrRtrId, OSPFV3_RTR_ID_LEN);

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrLsuGetLinksFromLsa\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuProcessRcvdSelfOrgLsa                               */
/*                                                                           */
/* Description  : This routine process the self orginated LSA received       */
/*                from the nbr during graceful restart mode                  */
/*                                                                           */
/* Input        : pLsaInfo        - pointer to  Self Orginated Lsa           */
/*                pLsHeader       - pointer to LS Header                     */
/*                pLsa            - pointer to Lsa pkt buffer                */
/*                pNbr           -  pointer to the neighbor                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSPFV3_DISCARDED                            */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT1
O3GrLsuProcessRcvdSelfOrgLsa (tV3OsLsHeader * pLsHeader,
                              tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa,
                              tV3OsNeighbor * pNbr)
{
    UINT1               u1LsaChngdFlag = OSIX_TRUE;
    tV3OsLsaDesc       *pLsaDesc = NULL;
    UINT1               u1AckFlag = OSPFV3_UNUSED_ACK_FLAG;

    OSPFV3_GBL_TRC (OSPFV3_FN_ENTRY, "ENTER: O3GrLsuProcessRcvdSelfOrgLsa\r\n");
    if (pLsHeader->lsaSeqNum == OSPFV3_MAX_SEQ_NUM)
    {
        return OSPFV3_DISCARDED;
    }

    if (pLsaInfo != (tV3OsLsaInfo *) NULL)
    {
        /* Set the u1LsaChngdFlag appropriately */
        u1LsaChngdFlag = (UINT1) V3LsuCheckActualChange (pLsaInfo->pLsa, pLsa);
    }

    /* If the lsa is received for first time, Add the New lsa to the
     * Data base */
    if (pLsaInfo == NULL)
    {
        pLsaDesc = O3GrLsuGetLsaDesc (pLsHeader, pNbr);
        if (pLsaDesc == NULL)
        {
            return OSPFV3_DISCARDED;
        }
        if ((pLsaInfo = V3LsuAddToLsdb (pNbr->pInterface->pArea,
                                        pLsHeader->u2LsaType,
                                        &(pLsHeader->linkStateId),
                                        &(pLsHeader->advRtrId),
                                        pNbr->pInterface, pLsa)) == NULL)
        {
            V3LsuClearLsaDesc (pLsaDesc);
            return OSPFV3_DISCARDED;
        }

        pLsaInfo->pLsaDesc = pLsaDesc;
        pLsaDesc->pLsaInfo = pLsaInfo;
        /* Set the bit in LSA to indicate that the self-originated LSA
         * is in restarting router */
        OSPFV3_LSA_REFRESH_BIT_SET (pLsaInfo, OSPFV3_GR_LSA_BIT);

        /* Install the New Lsa in the Link state database */
        V3LsuInstallLsa (pLsa, pLsaInfo, OSIX_FALSE);

    }
    else
    {
        if (V3LsuCompLsaInstance (pLsaInfo, pLsHeader) == OSPFV3_EQUAL)
        {
            V3LsuProcessEqualInstLsa (pNbr, pLsaInfo, pLsa);
            return OSPFV3_DISCARDED;
        }

        /* remove the old instance of lsa from all rxmt lists */
        V3LsuDeleteFromAllRxmtLst (pLsaInfo, OSIX_TRUE);

        /* Update the Old Lsa_info in the Data base  with new one */
        V3LsuInstallLsa (pLsa, pLsaInfo, OSIX_FALSE);

        /*
         * The flood procedure returns FLOOD_BACK or NO_FLOOD_BACK based on
         * whether the lsa has been flooded out on the receiving interface
         * or not
         */

        u1AckFlag = V3LsuFloodOut (pLsaInfo, pNbr,
                                   pNbr->pInterface->pArea,
                                   u1LsaChngdFlag, pNbr->pInterface);

        V3LsuProcessAckFlag (pNbr, pLsa, u1AckFlag);

    }

    pLsaInfo->pV3OspfCxt->u4RcvNewLsa++;
    V3UtilSetLsaId (OSPFV3_BUFFER_DWFROMPDU (pLsHeader->linkStateId),
                    pLsHeader->u2LsaType, pLsaInfo);

    if ((!OSPFV3_IS_IN_OVERFLOW_STATE (pLsaInfo->pV3OspfCxt)) &&
        (OSPFV3_IS_EXT_LSDB_SIZE_LIMITED (pLsaInfo->pV3OspfCxt)) &&
        (pLsaInfo->pV3OspfCxt->u4NonDefAsLsaCount ==
         (UINT4) pLsaInfo->pV3OspfCxt->i4ExtLsdbLimit))
    {

        OSPFV3_TRC (OSPFV3_LSU_TRC | OSPFV3_ADJACENCY_TRC,
                    pLsaInfo->pV3OspfCxt->u4ContextId,
                    "Rtr Enters OvrFlw State\n");
        V3RtrEnterOverflowStateInCxt (pLsaInfo->pV3OspfCxt);
    }

    OSPFV3_GBL_TRC (OSPFV3_FN_EXIT, "EXIT: O3GrLsuProcessRcvdSelfOrgLsa\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : O3GrLsuCheckLsaConsistency                                 */
/*                                                                           */
/* Description  : This module is called when the restarting router receives  */
/*                a LSA from the neighor. The module checks whether the      */
/*                neighbor.s LSA is consistent with restarting router's      */
/*                pre-restart LSA. When inconsistency is detected, the       */
/*                router exits restart mode.                                 */
/*                                                                           */
/* Input        : pArea        - Pointer to Area                             */
/*                pLsHeader    - LSA header                                  */
/*                pLsa         - pointer to LSA                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_TRUE     -     if LSA is inconsistent                 */
/*                OSIX_FALSE    -     if LSA is not inconsistent             */
/*                                                                           */
/*****************************************************************************/

PUBLIC INT4
O3GrLsuCheckLsaConsistency (tV3OsArea * pArea, tV3OsLsHeader * pLsHeader,
                            UINT1 *pLsa)
{
    tV3OspfCxt         *pV3OspfCxt = pArea->pV3OspfCxt;
    INT4                i4LsaInconsistent = OSIX_FALSE;
    UINT2               u2LsaType = pLsHeader->u2LsaType;

    OSPFV3_TRC (OSPFV3_FN_ENTRY, pArea->pV3OspfCxt->u4ContextId,
                "ENTER : O3GrLsuCheckLsaConsistency\r\n");

    if ((u2LsaType != OSPFV3_ROUTER_LSA) && (u2LsaType != OSPFV3_NETWORK_LSA))
    {
        /* Only router and network LSA are considered for LSA
         * inconsistency check */
        return OSIX_FALSE;
    }

    /* Only self-originated and neighbor originated LSAs are considered */
    if (V3UtilRtrIdComp (pLsHeader->advRtrId, pArea->pV3OspfCxt->rtrId)
        == OSPFV3_EQUAL)
    {
        i4LsaInconsistent = O3GrLsuCheckSelfOrigLsa (pArea, pLsHeader, pLsa);
    }
    else if (O3GrLsuIsNbrOrigLsaInCxt (pV3OspfCxt, pLsHeader) == OSIX_TRUE)
    {
        i4LsaInconsistent = O3GrLsuCheckNbrLsa (pArea, pLsHeader, pLsa);
    }

    OSPFV3_TRC (OSPFV3_FN_EXIT, pArea->pV3OspfCxt->u4ContextId,
                "EXIT: O3GrLsuCheckLsaConsistency\r\n");
    return i4LsaInconsistent;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  o3grlsu.c                      */
/*-----------------------------------------------------------------------*/
