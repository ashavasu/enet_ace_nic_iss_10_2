/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmio3db.h,v 1.7 2017/12/26 13:34:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMIO3DB_H
#define _FSMIO3DB_H

UINT1 FsMIOspfv3TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfv3RoutingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIOspfv3AsExternalAggregationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfv3BRRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfv3RedistRouteCfgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMIOspfv3RRDRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3RRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfv3DistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3PreferenceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3NeighborBfdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIOspfv3IfAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3VirtIfAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIOspfv3VirtIfCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsmio3 [] ={1,3,6,1,4,1,29601,2,24};
tSNMP_OID_TYPE fsmio3OID = {9, fsmio3};


/* Generated OID's for tables */
UINT4 FsMIOspfv3Table [] ={1,3,6,1,4,1,29601,2,24,2};
tSNMP_OID_TYPE FsMIOspfv3TableOID = {10, FsMIOspfv3Table};


UINT4 FsMIOspfv3IfTable [] ={1,3,6,1,4,1,29601,2,24,3};
tSNMP_OID_TYPE FsMIOspfv3IfTableOID = {10, FsMIOspfv3IfTable};


UINT4 FsMIOspfv3RoutingTable [] ={1,3,6,1,4,1,29601,2,24,4};
tSNMP_OID_TYPE FsMIOspfv3RoutingTableOID = {10, FsMIOspfv3RoutingTable};


UINT4 FsMIOspfv3AsExternalAggregationTable [] ={1,3,6,1,4,1,29601,2,24,5};
tSNMP_OID_TYPE FsMIOspfv3AsExternalAggregationTableOID = {10, FsMIOspfv3AsExternalAggregationTable};


UINT4 FsMIOspfv3BRRouteTable [] ={1,3,6,1,4,1,29601,2,24,6};
tSNMP_OID_TYPE FsMIOspfv3BRRouteTableOID = {10, FsMIOspfv3BRRouteTable};


UINT4 FsMIOspfv3RedistRouteCfgTable [] ={1,3,6,1,4,1,29601,2,24,7};
tSNMP_OID_TYPE FsMIOspfv3RedistRouteCfgTableOID = {10, FsMIOspfv3RedistRouteCfgTable};


UINT4 FsMIOspfv3RRDRouteTable [] ={1,3,6,1,4,1,29601,2,24,8,1,1};
tSNMP_OID_TYPE FsMIOspfv3RRDRouteTableOID = {12, FsMIOspfv3RRDRouteTable};

UINT4 FsMIOspfv3RRDMetricTable [] ={1,3,6,1,4,1,29601,2,24,8,2};
tSNMP_OID_TYPE FsMIOspfv3RRDMetricTableOID = {11, FsMIOspfv3RRDMetricTable};


UINT4 FsMIOspfv3DistInOutRouteMapTable [] ={1,3,6,1,4,1,29601,2,24,9,1};
tSNMP_OID_TYPE FsMIOspfv3DistInOutRouteMapTableOID = {11, FsMIOspfv3DistInOutRouteMapTable};


UINT4 FsMIOspfv3PreferenceTable [] ={1,3,6,1,4,1,29601,2,24,10,1};
tSNMP_OID_TYPE FsMIOspfv3PreferenceTableOID = {11, FsMIOspfv3PreferenceTable};


UINT4 FsMIOspfv3NeighborBfdTable [] ={1,3,6,1,4,1,29601,2,24,11,1};
tSNMP_OID_TYPE FsMIOspfv3NeighborBfdTableOID = {11, FsMIOspfv3NeighborBfdTable};

UINT4 FsMIOspfv3IfAuthTable [] ={1,3,6,1,4,1,29601,2,24,12};
tSNMP_OID_TYPE FsMIOspfv3IfAuthTableOID = {10, FsMIOspfv3IfAuthTable};

UINT4 FsMIOspfv3VirtIfAuthTable [] ={1,3,6,1,4,1,29601,2,24,13};
tSNMP_OID_TYPE FsMIOspfv3VirtIfAuthTableOID = {10, FsMIOspfv3VirtIfAuthTable};

UINT4 FsMIOspfv3VirtIfCryptoAuthTable [] ={1,3,6,1,4,1,29601,2,24,14};
tSNMP_OID_TYPE FsMIOspfv3VirtIfCryptoAuthTableOID = {10, FsMIOspfv3VirtIfCryptoAuthTable};

UINT4 FsMIOspfv3GlobalTraceLevel [ ] ={1,3,6,1,4,1,29601,2,24,1,1};
UINT4 FsMIOspfv3VrfSpfInterval [ ] ={1,3,6,1,4,1,29601,2,24,1,2};
UINT4 FsMIOspfv3RTStaggeringStatus [ ] ={1,3,6,1,4,1,29601,2,24,1,3};
UINT4 FsMIOspfv3HotStandbyAdminStatus [ ] ={1,3,6,1,4,1,29601,2,24,1,4};
UINT4 FsMIOspfv3HotStandbyState [ ] ={1,3,6,1,4,1,29601,2,24,1,5};
UINT4 FsMIOspfv3DynamicBulkUpdStatus [ ] ={1,3,6,1,4,1,29601,2,24,1,6};
UINT4 FsMIOspfv3StandbyHelloSyncCount [ ] ={1,3,6,1,4,1,29601,2,24,1,7};
UINT4 FsMIOspfv3StandbyLsaSyncCount [ ] ={1,3,6,1,4,1,29601,2,24,1,8};
UINT4 FsMIOspfv3ClearProcess [ ] ={1,3,6,1,4,1,29601,2,24,1,9};
UINT4 FsMIOspfv3OverFlowState [ ] ={1,3,6,1,4,1,29601,2,24,2,1,1};
UINT4 FsMIOspfv3TraceLevel [ ] ={1,3,6,1,4,1,29601,2,24,2,1,2};
UINT4 FsMIOspfv3ABRType [ ] ={1,3,6,1,4,1,29601,2,24,2,1,3};
UINT4 FsMIOspfv3NssaAsbrDefRtTrans [ ] ={1,3,6,1,4,1,29601,2,24,2,1,4};
UINT4 FsMIOspfv3DefaultPassiveInterface [ ] ={1,3,6,1,4,1,29601,2,24,2,1,5};
UINT4 FsMIOspfv3SpfDelay [ ] ={1,3,6,1,4,1,29601,2,24,2,1,6};
UINT4 FsMIOspfv3SpfHoldTime [ ] ={1,3,6,1,4,1,29601,2,24,2,1,7};
UINT4 FsMIOspfv3RTStaggeringInterval [ ] ={1,3,6,1,4,1,29601,2,24,2,1,8};
UINT4 FsMIOspfv3RestartStrictLsaChecking [ ] ={1,3,6,1,4,1,29601,2,24,2,1,9};
UINT4 FsMIOspfv3HelperSupport [ ] ={1,3,6,1,4,1,29601,2,24,2,1,10};
UINT4 FsMIOspfv3HelperGraceTimeLimit [ ] ={1,3,6,1,4,1,29601,2,24,2,1,11};
UINT4 FsMIOspfv3RestartAckState [ ] ={1,3,6,1,4,1,29601,2,24,2,1,12};
UINT4 FsMIOspfv3GraceLsaRetransmitCount [ ] ={1,3,6,1,4,1,29601,2,24,2,1,13};
UINT4 FsMIOspfv3RestartReason [ ] ={1,3,6,1,4,1,29601,2,24,2,1,14};
UINT4 FsMIOspfv3ExtTraceLevel [ ] ={1,3,6,1,4,1,29601,2,24,2,1,15};
UINT4 FsMIOspfv3SetTraps [ ] ={1,3,6,1,4,1,29601,2,24,2,1,16};
UINT4 FsMIOspfv3BfdStatus [ ] ={1,3,6,1,4,1,29601,2,24,2,1,17};
UINT4 FsMIOspfv3BfdAllIfState [ ] ={1,3,6,1,4,1,29601,2,24,2,1,18};
UINT4 FsMIOspfv3RouterIdPermanence [ ] ={1,3,6,1,4,1,29601,2,24,2,1,19};
UINT4 FsMIOspfv3IfIndex [ ] ={1,3,6,1,4,1,29601,2,24,3,1,1};
UINT4 FsMIOspfv3IfOperState [ ] ={1,3,6,1,4,1,29601,2,24,3,1,2};
UINT4 FsMIOspfv3IfPassive [ ] ={1,3,6,1,4,1,29601,2,24,3,1,3};
UINT4 FsMIOspfv3IfNbrCount [ ] ={1,3,6,1,4,1,29601,2,24,3,1,4};
UINT4 FsMIOspfv3IfAdjCount [ ] ={1,3,6,1,4,1,29601,2,24,3,1,5};
UINT4 FsMIOspfv3IfHelloRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,6};
UINT4 FsMIOspfv3IfHelloTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,7};
UINT4 FsMIOspfv3IfHelloDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,8};
UINT4 FsMIOspfv3IfDdpRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,9};
UINT4 FsMIOspfv3IfDdpTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,10};
UINT4 FsMIOspfv3IfDdpDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,11};
UINT4 FsMIOspfv3IfLrqRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,12};
UINT4 FsMIOspfv3IfLrqTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,13};
UINT4 FsMIOspfv3IfLrqDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,14};
UINT4 FsMIOspfv3IfLsuRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,15};
UINT4 FsMIOspfv3IfLsuTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,16};
UINT4 FsMIOspfv3IfLsuDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,17};
UINT4 FsMIOspfv3IfLakRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,18};
UINT4 FsMIOspfv3IfLakTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,19};
UINT4 FsMIOspfv3IfLakDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,20};
UINT4 FsMIOspfv3IfContextId [ ] ={1,3,6,1,4,1,29601,2,24,3,1,21};
UINT4 FsMIOspfv3IfLinkLSASuppression [ ] ={1,3,6,1,4,1,29601,2,24,3,1,22};
UINT4 FsMIOspfv3IfBfdState [ ] ={1,3,6,1,4,1,29601,2,24,3,1,23};
UINT4 FsMIOspfv3IfCryptoAuthType [ ] ={1,3,6,1,4,1,29601,2,24,3,1,24};
UINT4 FsMIOspfv3IfCryptoAuthMode [ ] ={1,3,6,1,4,1,29601,2,24,3,1,25};
UINT4 FsMIOspfv3IfAuthTxed [ ] ={1,3,6,1,4,1,29601,2,24,3,1,26};
UINT4 FsMIOspfv3IfAuthRcvd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,27};
UINT4 FsMIOspfv3IfAuthDisd [ ] ={1,3,6,1,4,1,29601,2,24,3,1,28};
UINT4 FsMIOspfv3RouteDestType [ ] ={1,3,6,1,4,1,29601,2,24,4,1,1};
UINT4 FsMIOspfv3RouteDest [ ] ={1,3,6,1,4,1,29601,2,24,4,1,2};
UINT4 FsMIOspfv3RoutePfxLength [ ] ={1,3,6,1,4,1,29601,2,24,4,1,3};
UINT4 FsMIOspfv3RouteNextHopType [ ] ={1,3,6,1,4,1,29601,2,24,4,1,4};
UINT4 FsMIOspfv3RouteNextHop [ ] ={1,3,6,1,4,1,29601,2,24,4,1,5};
UINT4 FsMIOspfv3RouteType [ ] ={1,3,6,1,4,1,29601,2,24,4,1,6};
UINT4 FsMIOspfv3RouteAreaId [ ] ={1,3,6,1,4,1,29601,2,24,4,1,7};
UINT4 FsMIOspfv3RouteCost [ ] ={1,3,6,1,4,1,29601,2,24,4,1,8};
UINT4 FsMIOspfv3RouteType2Cost [ ] ={1,3,6,1,4,1,29601,2,24,4,1,9};
UINT4 FsMIOspfv3RouteInterfaceIndex [ ] ={1,3,6,1,4,1,29601,2,24,4,1,10};
UINT4 FsMIOspfv3AsExternalAggregationNetType [ ] ={1,3,6,1,4,1,29601,2,24,5,1,1};
UINT4 FsMIOspfv3AsExternalAggregationNet [ ] ={1,3,6,1,4,1,29601,2,24,5,1,2};
UINT4 FsMIOspfv3AsExternalAggregationPfxLength [ ] ={1,3,6,1,4,1,29601,2,24,5,1,3};
UINT4 FsMIOspfv3AsExternalAggregationAreaId [ ] ={1,3,6,1,4,1,29601,2,24,5,1,4};
UINT4 FsMIOspfv3AsExternalAggregationEffect [ ] ={1,3,6,1,4,1,29601,2,24,5,1,5};
UINT4 FsMIOspfv3AsExternalAggregationTranslation [ ] ={1,3,6,1,4,1,29601,2,24,5,1,6};
UINT4 FsMIOspfv3AsExternalAggregationStatus [ ] ={1,3,6,1,4,1,29601,2,24,5,1,7};
UINT4 FsMIOspfv3BRRouteDest [ ] ={1,3,6,1,4,1,29601,2,24,6,1,1};
UINT4 FsMIOspfv3BRRouteNextHopType [ ] ={1,3,6,1,4,1,29601,2,24,6,1,2};
UINT4 FsMIOspfv3BRRouteNextHop [ ] ={1,3,6,1,4,1,29601,2,24,6,1,3};
UINT4 FsMIOspfv3BRRouteDestType [ ] ={1,3,6,1,4,1,29601,2,24,6,1,4};
UINT4 FsMIOspfv3BRRouteType [ ] ={1,3,6,1,4,1,29601,2,24,6,1,5};
UINT4 FsMIOspfv3BRRouteAreaId [ ] ={1,3,6,1,4,1,29601,2,24,6,1,6};
UINT4 FsMIOspfv3BRRouteCost [ ] ={1,3,6,1,4,1,29601,2,24,6,1,7};
UINT4 FsMIOspfv3BRRouteInterfaceIndex [ ] ={1,3,6,1,4,1,29601,2,24,6,1,8};
UINT4 FsMIOspfv3RedistRouteDestType [ ] ={1,3,6,1,4,1,29601,2,24,7,1,1};
UINT4 FsMIOspfv3RedistRouteDest [ ] ={1,3,6,1,4,1,29601,2,24,7,1,2};
UINT4 FsMIOspfv3RedistRoutePfxLength [ ] ={1,3,6,1,4,1,29601,2,24,7,1,3};
UINT4 FsMIOspfv3RedistRouteMetric [ ] ={1,3,6,1,4,1,29601,2,24,7,1,4};
UINT4 FsMIOspfv3RedistRouteMetricType [ ] ={1,3,6,1,4,1,29601,2,24,7,1,5};
UINT4 FsMIOspfv3RedistRouteTagType [ ] ={1,3,6,1,4,1,29601,2,24,7,1,6};
UINT4 FsMIOspfv3RedistRouteTag [ ] ={1,3,6,1,4,1,29601,2,24,7,1,7};
UINT4 FsMIOspfv3RedistRouteStatus [ ] ={1,3,6,1,4,1,29601,2,24,7,1,8};
UINT4 FsMIOspfv3RRDStatus [ ] ={1,3,6,1,4,1,29601,2,24,8,1,1,1,1};
UINT4 FsMIOspfv3RRDSrcProtoMask [ ] ={1,3,6,1,4,1,29601,2,24,8,1,1,1,2};
UINT4 FsMIOspfv3RRDRouteMapName [ ] ={1,3,6,1,4,1,29601,2,24,8,1,1,1,3};
UINT4 FsMIOspfv3RRDProtocolId [ ] ={1,3,6,1,4,1,29601,2,24,8,2,1,1};
UINT4 FsMIOspfv3RRDMetricValue [ ] ={1,3,6,1,4,1,29601,2,24,8,2,1,2};
UINT4 FsMIOspfv3RRDMetricType [ ] ={1,3,6,1,4,1,29601,2,24,8,2,1,3};
UINT4 FsMIOspfv3DistInOutRouteMapName [ ] ={1,3,6,1,4,1,29601,2,24,9,1,1,1};
UINT4 FsMIOspfv3DistInOutRouteMapType [ ] ={1,3,6,1,4,1,29601,2,24,9,1,1,2};
UINT4 FsMIOspfv3DistInOutRouteMapValue [ ] ={1,3,6,1,4,1,29601,2,24,9,1,1,3};
UINT4 FsMIOspfv3DistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,29601,2,24,9,1,1,4};
UINT4 FsMIOspfv3PreferenceValue [ ] ={1,3,6,1,4,1,29601,2,24,10,1,1,1};
UINT4 FsMIOspfv3NbrBfdState [ ] ={1,3,6,1,4,1,29601,2,24,11,1,1,1};
UINT4 FsMIOspfv3IfAuthIfIndex [ ] ={1,3,6,1,4,1,29601,2,24,12,1,1};
UINT4 FsMIOspfv3IfAuthKeyId [ ] ={1,3,6,1,4,1,29601,2,24,12,1,2};
UINT4 FsMIOspfv3IfAuthKey [ ] ={1,3,6,1,4,1,29601,2,24,12,1,3};
UINT4 FsMIOspfv3IfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,29601,2,24,12,1,4};
UINT4 FsMIOspfv3IfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,29601,2,24,12,1,5};
UINT4 FsMIOspfv3IfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,29601,2,24,12,1,6};
UINT4 FsMIOspfv3IfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,29601,2,24,12,1,7};
UINT4 FsMIOspfv3IfAuthKeyStatus [ ] ={1,3,6,1,4,1,29601,2,24,12,1,8};
UINT4 FsMIOspfv3VirtIfAuthAreaId [ ] ={1,3,6,1,4,1,29601,2,24,13,1,1};
UINT4 FsMIOspfv3VirtIfAuthNeighbor [ ] ={1,3,6,1,4,1,29601,2,24,13,1,2};
UINT4 FsMIOspfv3VirtIfAuthKeyId [ ] ={1,3,6,1,4,1,29601,2,24,13,1,3};
UINT4 FsMIOspfv3VirtIfAuthKey [ ] ={1,3,6,1,4,1,29601,2,24,13,1,4};
UINT4 FsMIOspfv3VirtIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,29601,2,24,13,1,5};
UINT4 FsMIOspfv3VirtIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,29601,2,24,13,1,6};
UINT4 FsMIOspfv3VirtIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,29601,2,24,13,1,7};
UINT4 FsMIOspfv3VirtIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,29601,2,24,13,1,8};
UINT4 FsMIOspfv3VirtIfAuthKeyStatus [ ] ={1,3,6,1,4,1,29601,2,24,13,1,9};
UINT4 FsMIOspfv3VirtIfCryptoAuthType [ ] ={1,3,6,1,4,1,29601,2,24,14,1,1};
UINT4 FsMIOspfv3VirtIfCryptoAuthMode [ ] ={1,3,6,1,4,1,29601,2,24,14,1,2};
UINT4 FsMIOspfv3TrapNbrIfIndex [ ] ={1,3,6,1,4,1,29601,2,24,101,1,1};
UINT4 FsMIOspfv3TrapVirtNbrRtrId [ ] ={1,3,6,1,4,1,29601,2,24,101,1,2};
UINT4 FsMIOspfv3TrapNbrRtrId [ ] ={1,3,6,1,4,1,29601,2,24,101,1,3};
UINT4 FsMIOspfv3TrapVirtNbrArea [ ] ={1,3,6,1,4,1,29601,2,24,101,1,4};
UINT4 FsMIOspfv3TrapBulkUpdAbortReason [ ] ={1,3,6,1,4,1,29601,2,24,101,1,5};


tSNMP_OID_TYPE FsMIOspfv3GlobalTraceLevelOID = {11, FsMIOspfv3GlobalTraceLevel};


tSNMP_OID_TYPE FsMIOspfv3VrfSpfIntervalOID = {11, FsMIOspfv3VrfSpfInterval};


tSNMP_OID_TYPE FsMIOspfv3RTStaggeringStatusOID = {11, FsMIOspfv3RTStaggeringStatus};


tSNMP_OID_TYPE FsMIOspfv3HotStandbyAdminStatusOID = {11, FsMIOspfv3HotStandbyAdminStatus};


tSNMP_OID_TYPE FsMIOspfv3HotStandbyStateOID = {11, FsMIOspfv3HotStandbyState};


tSNMP_OID_TYPE FsMIOspfv3DynamicBulkUpdStatusOID = {11, FsMIOspfv3DynamicBulkUpdStatus};


tSNMP_OID_TYPE FsMIOspfv3StandbyHelloSyncCountOID = {11, FsMIOspfv3StandbyHelloSyncCount};


tSNMP_OID_TYPE FsMIOspfv3StandbyLsaSyncCountOID = {11, FsMIOspfv3StandbyLsaSyncCount};

tSNMP_OID_TYPE FsMIOspfv3ClearProcessOID = {11, FsMIOspfv3ClearProcess};




tMbDbEntry FsMIOspfv3GlobalTraceLevelMibEntry[]= {

{{11,FsMIOspfv3GlobalTraceLevel}, NULL, FsMIOspfv3GlobalTraceLevelGet, FsMIOspfv3GlobalTraceLevelSet, FsMIOspfv3GlobalTraceLevelTest, FsMIOspfv3GlobalTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3GlobalTraceLevelEntry = { 1, FsMIOspfv3GlobalTraceLevelMibEntry };

tMbDbEntry FsMIOspfv3VrfSpfIntervalMibEntry[]= {

{{11,FsMIOspfv3VrfSpfInterval}, NULL, FsMIOspfv3VrfSpfIntervalGet, FsMIOspfv3VrfSpfIntervalSet, FsMIOspfv3VrfSpfIntervalTest, FsMIOspfv3VrfSpfIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},
};
tMibData FsMIOspfv3VrfSpfIntervalEntry = { 1, FsMIOspfv3VrfSpfIntervalMibEntry };

tMbDbEntry FsMIOspfv3RTStaggeringStatusMibEntry[]= {

{{11,FsMIOspfv3RTStaggeringStatus}, NULL, FsMIOspfv3RTStaggeringStatusGet, FsMIOspfv3RTStaggeringStatusSet, FsMIOspfv3RTStaggeringStatusTest, FsMIOspfv3RTStaggeringStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsMIOspfv3RTStaggeringStatusEntry = { 1, FsMIOspfv3RTStaggeringStatusMibEntry };

tMbDbEntry FsMIOspfv3HotStandbyAdminStatusMibEntry[]= {

{{11,FsMIOspfv3HotStandbyAdminStatus}, NULL, FsMIOspfv3HotStandbyAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3HotStandbyAdminStatusEntry = { 1, FsMIOspfv3HotStandbyAdminStatusMibEntry };

tMbDbEntry FsMIOspfv3HotStandbyStateMibEntry[]= {

{{11,FsMIOspfv3HotStandbyState}, NULL, FsMIOspfv3HotStandbyStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3HotStandbyStateEntry = { 1, FsMIOspfv3HotStandbyStateMibEntry };

tMbDbEntry FsMIOspfv3DynamicBulkUpdStatusMibEntry[]= {

{{11,FsMIOspfv3DynamicBulkUpdStatus}, NULL, FsMIOspfv3DynamicBulkUpdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3DynamicBulkUpdStatusEntry = { 1, FsMIOspfv3DynamicBulkUpdStatusMibEntry };

tMbDbEntry FsMIOspfv3StandbyHelloSyncCountMibEntry[]= {

{{11,FsMIOspfv3StandbyHelloSyncCount}, NULL, FsMIOspfv3StandbyHelloSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3StandbyHelloSyncCountEntry = { 1, FsMIOspfv3StandbyHelloSyncCountMibEntry };

tMbDbEntry FsMIOspfv3StandbyLsaSyncCountMibEntry[]= {

{{11,FsMIOspfv3StandbyLsaSyncCount}, NULL, FsMIOspfv3StandbyLsaSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3StandbyLsaSyncCountEntry = { 1, FsMIOspfv3StandbyLsaSyncCountMibEntry };

tMbDbEntry FsMIOspfv3ClearProcessMibEntry[] = {

{{11,FsMIOspfv3ClearProcess}, NULL, FsMIOspfv3ClearProcessGet, FsMIOspfv3ClearProcessSet, FsMIOspfv3ClearProcessTest, FsMIOspfv3ClearProcessDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},};
tMibData FsMIOspfv3ClearProcessEntry = {1, FsMIOspfv3ClearProcessMibEntry };



tMbDbEntry FsMIOspfv3TableMibEntry[]= {

{{12,FsMIOspfv3OverFlowState}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3OverFlowStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3TraceLevel}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3TraceLevelGet, FsMIOspfv3TraceLevelSet, FsMIOspfv3TraceLevelTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2048"},

{{12,FsMIOspfv3ABRType}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3ABRTypeGet, FsMIOspfv3ABRTypeSet, FsMIOspfv3ABRTypeTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfv3NssaAsbrDefRtTrans}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3NssaAsbrDefRtTransGet, FsMIOspfv3NssaAsbrDefRtTransSet, FsMIOspfv3NssaAsbrDefRtTransTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3DefaultPassiveInterface}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3DefaultPassiveInterfaceGet, FsMIOspfv3DefaultPassiveInterfaceSet, FsMIOspfv3DefaultPassiveInterfaceTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3SpfDelay}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3SpfDelayGet, FsMIOspfv3SpfDelaySet, FsMIOspfv3SpfDelayTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "5"},

{{12,FsMIOspfv3SpfHoldTime}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3SpfHoldTimeGet, FsMIOspfv3SpfHoldTimeSet, FsMIOspfv3SpfHoldTimeTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "10"},

{{12,FsMIOspfv3RTStaggeringInterval}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3RTStaggeringIntervalGet, FsMIOspfv3RTStaggeringIntervalSet, FsMIOspfv3RTStaggeringIntervalTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "10000"},

{{12,FsMIOspfv3RestartStrictLsaChecking}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3RestartStrictLsaCheckingGet, FsMIOspfv3RestartStrictLsaCheckingSet, FsMIOspfv3RestartStrictLsaCheckingTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3HelperSupport}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3HelperSupportGet, FsMIOspfv3HelperSupportSet, FsMIOspfv3HelperSupportTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3HelperGraceTimeLimit}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3HelperGraceTimeLimitGet, FsMIOspfv3HelperGraceTimeLimitSet, FsMIOspfv3HelperGraceTimeLimitTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "0"},

{{12,FsMIOspfv3RestartAckState}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3RestartAckStateGet, FsMIOspfv3RestartAckStateSet, FsMIOspfv3RestartAckStateTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "1"},

{{12,FsMIOspfv3GraceLsaRetransmitCount}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3GraceLsaRetransmitCountGet, FsMIOspfv3GraceLsaRetransmitCountSet, FsMIOspfv3GraceLsaRetransmitCountTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3RestartReason}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3RestartReasonGet, FsMIOspfv3RestartReasonSet, FsMIOspfv3RestartReasonTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "0"},

{{12,FsMIOspfv3ExtTraceLevel}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3ExtTraceLevelGet, FsMIOspfv3ExtTraceLevelSet, FsMIOspfv3ExtTraceLevelTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3SetTraps}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3SetTrapsGet, FsMIOspfv3SetTrapsSet, FsMIOspfv3SetTrapsTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3BfdStatus}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3BfdStatusGet, FsMIOspfv3BfdStatusSet, FsMIOspfv3BfdStatusTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3BfdAllIfState}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3BfdAllIfStateGet, FsMIOspfv3BfdAllIfStateSet, FsMIOspfv3BfdAllIfStateTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3RouterIdPermanence}, GetNextIndexFsMIOspfv3Table, FsMIOspfv3RouterIdPermanenceGet, FsMIOspfv3RouterIdPermanenceSet, FsMIOspfv3RouterIdPermanenceTest, FsMIOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TableINDEX, 1, 0, 0, "1"},
};
tMibData FsMIOspfv3TableEntry = { 19, FsMIOspfv3TableMibEntry };

tMbDbEntry FsMIOspfv3IfTableMibEntry[]= {

{{12,FsMIOspfv3IfIndex}, GetNextIndexFsMIOspfv3IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfOperState}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfPassive}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfPassiveGet, FsMIOspfv3IfPassiveSet, FsMIOspfv3IfPassiveTest, FsMIOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3IfNbrCount}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfNbrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfAdjCount}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfAdjCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfHelloRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfHelloRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfHelloTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfHelloTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfHelloDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfHelloDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfDdpRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfDdpRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfDdpTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfDdpTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfDdpDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfDdpDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLrqRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLrqRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLrqTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLrqTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLrqDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLrqDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLsuRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLsuRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLsuTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLsuTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLsuDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLsuDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLakRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLakRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLakTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLakTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLakDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLakDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfContextId}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfLinkLSASuppression}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfLinkLSASuppressionGet, FsMIOspfv3IfLinkLSASuppressionSet, FsMIOspfv3IfLinkLSASuppressionTest, FsMIOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3IfBfdState}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfBfdStateGet, FsMIOspfv3IfBfdStateSet, FsMIOspfv3IfBfdStateTest, FsMIOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{12,FsMIOspfv3IfCryptoAuthType}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfCryptoAuthTypeGet, FsMIOspfv3IfCryptoAuthTypeSet, FsMIOspfv3IfCryptoAuthTypeTest, FsMIOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfTableINDEX, 1, 0, 0, "6"},

{{12,FsMIOspfv3IfCryptoAuthMode}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfCryptoAuthModeGet, FsMIOspfv3IfCryptoAuthModeSet, FsMIOspfv3IfCryptoAuthModeTest, FsMIOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfTableINDEX, 1, 0, 0, "3"},

{{12,FsMIOspfv3IfAuthTxed}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfAuthTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthRcvd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfAuthRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthDisd}, GetNextIndexFsMIOspfv3IfTable, FsMIOspfv3IfAuthDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIOspfv3IfTableINDEX, 1, 0, 0, NULL},
};

tMibData FsMIOspfv3IfTableEntry = { 28, FsMIOspfv3IfTableMibEntry };

tMbDbEntry FsMIOspfv3RoutingTableMibEntry[]= {

{{12,FsMIOspfv3RouteDestType}, GetNextIndexFsMIOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteDest}, GetNextIndexFsMIOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RoutePfxLength}, GetNextIndexFsMIOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteNextHopType}, GetNextIndexFsMIOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteNextHop}, GetNextIndexFsMIOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteType}, GetNextIndexFsMIOspfv3RoutingTable, FsMIOspfv3RouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteAreaId}, GetNextIndexFsMIOspfv3RoutingTable, FsMIOspfv3RouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteCost}, GetNextIndexFsMIOspfv3RoutingTable, FsMIOspfv3RouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteType2Cost}, GetNextIndexFsMIOspfv3RoutingTable, FsMIOspfv3RouteType2CostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},

{{12,FsMIOspfv3RouteInterfaceIndex}, GetNextIndexFsMIOspfv3RoutingTable, FsMIOspfv3RouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3RoutingTableINDEX, 6, 0, 0, NULL},
};
tMibData FsMIOspfv3RoutingTableEntry = { 10, FsMIOspfv3RoutingTableMibEntry };

tMbDbEntry FsMIOspfv3AsExternalAggregationTableMibEntry[]= {

{{12,FsMIOspfv3AsExternalAggregationNetType}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3AsExternalAggregationNet}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3AsExternalAggregationPfxLength}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3AsExternalAggregationAreaId}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3AsExternalAggregationEffect}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, FsMIOspfv3AsExternalAggregationEffectGet, FsMIOspfv3AsExternalAggregationEffectSet, FsMIOspfv3AsExternalAggregationEffectTest, FsMIOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, "1"},

{{12,FsMIOspfv3AsExternalAggregationTranslation}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, FsMIOspfv3AsExternalAggregationTranslationGet, FsMIOspfv3AsExternalAggregationTranslationSet, FsMIOspfv3AsExternalAggregationTranslationTest, FsMIOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 0, "1"},

{{12,FsMIOspfv3AsExternalAggregationStatus}, GetNextIndexFsMIOspfv3AsExternalAggregationTable, FsMIOspfv3AsExternalAggregationStatusGet, FsMIOspfv3AsExternalAggregationStatusSet, FsMIOspfv3AsExternalAggregationStatusTest, FsMIOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3AsExternalAggregationTableINDEX, 5, 0, 1, NULL},
};
tMibData FsMIOspfv3AsExternalAggregationTableEntry = { 7, FsMIOspfv3AsExternalAggregationTableMibEntry };

tMbDbEntry FsMIOspfv3BRRouteTableMibEntry[]= {

{{12,FsMIOspfv3BRRouteDest}, GetNextIndexFsMIOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteNextHopType}, GetNextIndexFsMIOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteNextHop}, GetNextIndexFsMIOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteDestType}, GetNextIndexFsMIOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteType}, GetNextIndexFsMIOspfv3BRRouteTable, FsMIOspfv3BRRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteAreaId}, GetNextIndexFsMIOspfv3BRRouteTable, FsMIOspfv3BRRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteCost}, GetNextIndexFsMIOspfv3BRRouteTable, FsMIOspfv3BRRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},

{{12,FsMIOspfv3BRRouteInterfaceIndex}, GetNextIndexFsMIOspfv3BRRouteTable, FsMIOspfv3BRRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3BRRouteTableINDEX, 5, 0, 0, NULL},
};
tMibData FsMIOspfv3BRRouteTableEntry = { 8, FsMIOspfv3BRRouteTableMibEntry };

tMbDbEntry FsMIOspfv3RedistRouteCfgTableMibEntry[]= {

{{12,FsMIOspfv3RedistRouteDestType}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3RedistRouteDest}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3RedistRoutePfxLength}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3RedistRouteMetric}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, FsMIOspfv3RedistRouteMetricGet, FsMIOspfv3RedistRouteMetricSet, FsMIOspfv3RedistRouteMetricTest, FsMIOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, "10"},

{{12,FsMIOspfv3RedistRouteMetricType}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, FsMIOspfv3RedistRouteMetricTypeGet, FsMIOspfv3RedistRouteMetricTypeSet, FsMIOspfv3RedistRouteMetricTypeTest, FsMIOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, "4"},

{{12,FsMIOspfv3RedistRouteTagType}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, FsMIOspfv3RedistRouteTagTypeGet, FsMIOspfv3RedistRouteTagTypeSet, FsMIOspfv3RedistRouteTagTypeTest, FsMIOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, "1"},

{{12,FsMIOspfv3RedistRouteTag}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, FsMIOspfv3RedistRouteTagGet, FsMIOspfv3RedistRouteTagSet, FsMIOspfv3RedistRouteTagTest, FsMIOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 0, "0"},

{{12,FsMIOspfv3RedistRouteStatus}, GetNextIndexFsMIOspfv3RedistRouteCfgTable, FsMIOspfv3RedistRouteStatusGet, FsMIOspfv3RedistRouteStatusSet, FsMIOspfv3RedistRouteStatusTest, FsMIOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3RedistRouteCfgTableINDEX, 4, 0, 1, NULL},
};
tMibData FsMIOspfv3RedistRouteCfgTableEntry = { 8, FsMIOspfv3RedistRouteCfgTableMibEntry };

tMbDbEntry FsMIOspfv3RRDRouteTableMibEntry[]= {

{{14,FsMIOspfv3RRDStatus}, GetNextIndexFsMIOspfv3RRDRouteTable, FsMIOspfv3RRDStatusGet, FsMIOspfv3RRDStatusSet, FsMIOspfv3RRDStatusTest, FsMIOspfv3RRDRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3RRDRouteTableINDEX, 1, 0, 0, NULL},

{{14,FsMIOspfv3RRDSrcProtoMask}, GetNextIndexFsMIOspfv3RRDRouteTable, FsMIOspfv3RRDSrcProtoMaskGet, FsMIOspfv3RRDSrcProtoMaskSet, FsMIOspfv3RRDSrcProtoMaskTest, FsMIOspfv3RRDRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3RRDRouteTableINDEX, 1, 0, 0, "0"},

{{14,FsMIOspfv3RRDRouteMapName}, GetNextIndexFsMIOspfv3RRDRouteTable, FsMIOspfv3RRDRouteMapNameGet, FsMIOspfv3RRDRouteMapNameSet, FsMIOspfv3RRDRouteMapNameTest, FsMIOspfv3RRDRouteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3RRDRouteTableINDEX, 1, 0, 0, NULL},
};
tMibData FsMIOspfv3RRDRouteTableEntry = { 3, FsMIOspfv3RRDRouteTableMibEntry };

tMbDbEntry FsMIOspfv3RRDMetricTableMibEntry[]={
{{13,FsMIOspfv3RRDProtocolId}, GetNextIndexFsMIOspfv3RRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3RRDMetricTableINDEX, 2, 0, 0, NULL},

{{13,FsMIOspfv3RRDMetricValue}, GetNextIndexFsMIOspfv3RRDMetricTable, FsMIOspfv3RRDMetricValueGet, FsMIOspfv3RRDMetricValueSet, FsMIOspfv3RRDMetricValueTest, FsMIOspfv3RRDMetricTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3RRDMetricTableINDEX, 2, 0, 0, NULL},

{{13,FsMIOspfv3RRDMetricType}, GetNextIndexFsMIOspfv3RRDMetricTable, FsMIOspfv3RRDMetricTypeGet, FsMIOspfv3RRDMetricTypeSet, FsMIOspfv3RRDMetricTypeTest, FsMIOspfv3RRDMetricTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3RRDMetricTableINDEX, 2, 0, 0, "2"},
};

tMibData FsMIOspfv3RRDMetricTableEntry = { 3, FsMIOspfv3RRDMetricTableMibEntry };

tMbDbEntry FsMIOspfv3DistInOutRouteMapTableMibEntry[]= {

{{13,FsMIOspfv3DistInOutRouteMapName}, GetNextIndexFsMIOspfv3DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIOspfv3DistInOutRouteMapType}, GetNextIndexFsMIOspfv3DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfv3DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIOspfv3DistInOutRouteMapValue}, GetNextIndexFsMIOspfv3DistInOutRouteMapTable, FsMIOspfv3DistInOutRouteMapValueGet, FsMIOspfv3DistInOutRouteMapValueSet, FsMIOspfv3DistInOutRouteMapValueTest, FsMIOspfv3DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3DistInOutRouteMapTableINDEX, 3, 0, 0, NULL},

{{13,FsMIOspfv3DistInOutRouteMapRowStatus}, GetNextIndexFsMIOspfv3DistInOutRouteMapTable, FsMIOspfv3DistInOutRouteMapRowStatusGet, FsMIOspfv3DistInOutRouteMapRowStatusSet, FsMIOspfv3DistInOutRouteMapRowStatusTest, FsMIOspfv3DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3DistInOutRouteMapTableINDEX, 3, 0, 1, NULL},
};
tMibData FsMIOspfv3DistInOutRouteMapTableEntry = { 4, FsMIOspfv3DistInOutRouteMapTableMibEntry };

tMbDbEntry FsMIOspfv3PreferenceTableMibEntry[]= {

{{13,FsMIOspfv3PreferenceValue}, GetNextIndexFsMIOspfv3PreferenceTable, FsMIOspfv3PreferenceValueGet, FsMIOspfv3PreferenceValueSet, FsMIOspfv3PreferenceValueTest, FsMIOspfv3PreferenceTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3PreferenceTableINDEX, 1, 0, 0, "0"},
};
tMibData FsMIOspfv3PreferenceTableEntry = { 1, FsMIOspfv3PreferenceTableMibEntry };

tMbDbEntry FsMIOspfv3NeighborBfdTableMibEntry[]= {
{{13,FsMIOspfv3NbrBfdState}, GetNextIndexFsMIOspfv3NeighborBfdTable, FsMIOspfv3NbrBfdStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIOspfv3NeighborBfdTableINDEX, 2, 0, 0, NULL},                              };
tMibData FsMIOspfv3NeighborBfdTableEntry = { 1, FsMIOspfv3NeighborBfdTableMibEntry };

tMbDbEntry FsMIOspfv3IfAuthTableMibEntry[]= {
{{12,FsMIOspfv3IfAuthIfIndex}, GetNextIndexFsMIOspfv3IfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyId}, GetNextIndexFsMIOspfv3IfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKey}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyGet, FsMIOspfv3IfAuthKeySet, FsMIOspfv3IfAuthKeyTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyStartAccept}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyStartAcceptGet, FsMIOspfv3IfAuthKeyStartAcceptSet, FsMIOspfv3IfAuthKeyStartAcceptTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyStartGenerate}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyStartGenerateGet, FsMIOspfv3IfAuthKeyStartGenerateSet, FsMIOspfv3IfAuthKeyStartGenerateTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyStopGenerate}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyStopGenerateGet, FsMIOspfv3IfAuthKeyStopGenerateSet, FsMIOspfv3IfAuthKeyStopGenerateTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyStopAccept}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyStopAcceptGet, FsMIOspfv3IfAuthKeyStopAcceptSet, FsMIOspfv3IfAuthKeyStopAcceptTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},

{{12,FsMIOspfv3IfAuthKeyStatus}, GetNextIndexFsMIOspfv3IfAuthTable, FsMIOspfv3IfAuthKeyStatusGet, FsMIOspfv3IfAuthKeyStatusSet, FsMIOspfv3IfAuthKeyStatusTest, FsMIOspfv3IfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3IfAuthTableINDEX, 3, 0, 0, NULL},
};

tMibData FsMIOspfv3IfAuthTableEntry = { 8, FsMIOspfv3IfAuthTableMibEntry };

tMbDbEntry FsMIOspfv3VirtIfAuthTableMibEntry[]= {
{{12,FsMIOspfv3VirtIfAuthAreaId}, GetNextIndexFsMIOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthNeighbor}, GetNextIndexFsMIOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyId}, GetNextIndexFsMIOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKey}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyGet, FsMIOspfv3VirtIfAuthKeySet, FsMIOspfv3VirtIfAuthKeyTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyStartAccept}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyStartAcceptGet, FsMIOspfv3VirtIfAuthKeyStartAcceptSet, FsMIOspfv3VirtIfAuthKeyStartAcceptTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyStartGenerate}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyStartGenerateGet, FsMIOspfv3VirtIfAuthKeyStartGenerateSet, FsMIOspfv3VirtIfAuthKeyStartGenerateTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyStopGenerate}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyStopGenerateGet, FsMIOspfv3VirtIfAuthKeyStopGenerateSet, FsMIOspfv3VirtIfAuthKeyStopGenerateTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyStopAccept}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyStopAcceptGet, FsMIOspfv3VirtIfAuthKeyStopAcceptSet, FsMIOspfv3VirtIfAuthKeyStopAcceptTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},

{{12,FsMIOspfv3VirtIfAuthKeyStatus}, GetNextIndexFsMIOspfv3VirtIfAuthTable, FsMIOspfv3VirtIfAuthKeyStatusGet, FsMIOspfv3VirtIfAuthKeyStatusSet, FsMIOspfv3VirtIfAuthKeyStatusTest, FsMIOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3VirtIfAuthTableINDEX, 4, 0, 0, NULL},
};

tMibData FsMIOspfv3VirtIfAuthTableEntry = { 9, FsMIOspfv3VirtIfAuthTableMibEntry };

tMbDbEntry FsMIOspfv3VirtIfCryptoAuthTableMibEntry[]= {
{{12,FsMIOspfv3VirtIfCryptoAuthType}, GetNextIndexFsMIOspfv3VirtIfCryptoAuthTable, FsMIOspfv3VirtIfCryptoAuthTypeGet, FsMIOspfv3VirtIfCryptoAuthTypeSet, FsMIOspfv3VirtIfCryptoAuthTypeTest, FsMIOspfv3VirtIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3VirtIfCryptoAuthTableINDEX, 3, 0, 0, "6"},

{{12,FsMIOspfv3VirtIfCryptoAuthMode}, GetNextIndexFsMIOspfv3VirtIfCryptoAuthTable, FsMIOspfv3VirtIfCryptoAuthModeGet, FsMIOspfv3VirtIfCryptoAuthModeSet, FsMIOspfv3VirtIfCryptoAuthModeTest, FsMIOspfv3VirtIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3VirtIfCryptoAuthTableINDEX, 3, 0, 0, "3"},
};

tMibData FsMIOspfv3VirtIfCryptoAuthTableEntry = { 2, FsMIOspfv3VirtIfCryptoAuthTableMibEntry };

tMbDbEntry FsMIOspfv3TrapNbrIfIndexMibEntry[]= {

{{12,FsMIOspfv3TrapNbrIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3TrapNbrIfIndexEntry = { 1, FsMIOspfv3TrapNbrIfIndexMibEntry };

tMbDbEntry FsMIOspfv3TrapVirtNbrRtrIdMibEntry[]= {

{{12,FsMIOspfv3TrapVirtNbrRtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3TrapVirtNbrRtrIdEntry = { 1, FsMIOspfv3TrapVirtNbrRtrIdMibEntry };

tMbDbEntry FsMIOspfv3TrapNbrRtrIdMibEntry[]= {

{{12,FsMIOspfv3TrapNbrRtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3TrapNbrRtrIdEntry = { 1, FsMIOspfv3TrapNbrRtrIdMibEntry };

tMbDbEntry FsMIOspfv3TrapVirtNbrAreaMibEntry[]= {

{{12,FsMIOspfv3TrapVirtNbrArea}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3TrapVirtNbrAreaEntry = { 1, FsMIOspfv3TrapVirtNbrAreaMibEntry };

tMbDbEntry FsMIOspfv3TrapBulkUpdAbortReasonMibEntry[]= {

{{12,FsMIOspfv3TrapBulkUpdAbortReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData FsMIOspfv3TrapBulkUpdAbortReasonEntry = { 1, FsMIOspfv3TrapBulkUpdAbortReasonMibEntry };

#endif /* _FSMIO3DB_H */

