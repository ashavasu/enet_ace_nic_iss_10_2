/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3tdfs.h,v 1.30 2017/12/26 13:34:25 siva Exp $
 *
 * Description:This file contains type definitions relating
 *             to protocol with MI support.
 *
 *******************************************************************/
#ifndef _O3TDFS_H
#define _O3TDFS_H
#include "rmap.h"
/****************************************************************************
 * BASIC TYPE DEFINITIONS *
 ****************************************************************************/

typedef UINT1           tV3OsRouterId[OSPFV3_RTR_ID_LEN];
typedef UINT1           tV3OsAreaId[OSPFV3_AREA_ID_LEN];
typedef UINT1           tV3OsOptions[OSPFV3_OPTION_LEN];

/****************************************************************************
 * LSA Related DEFINITIONS *
 ****************************************************************************/

typedef UINT1           tV3OsLinkStateId[OSPFV3_LINKSTATE_ID_LEN];
typedef INT4            tV3OsLsaSeqNum;

/****************************************************************************
 * MIB Related DEFINITIONS *
 ****************************************************************************/
typedef UINT1           tV3OsTruthValue;
                            /* Structure to hold a Boolean value
                             * '1' - If TRUE
                             * '2' - If FALSE
                             */
typedef UINT1           tV3OspfStatus;
                            /* Structure to hold the general status
                             * '1' - If enabled
                             * '2' - If disabled
                             */
typedef UINT1           tV3OsValidation;
                            /* Structure to hold a Boolean value indicating
                             * whether a route metric is VALID/INVALID */
typedef UINT1           tV3OsRowStatus;
                            /* Structure to hold the different row status
                             * value as defined in RFC 1443 */
/****************************************************************************
 * RT-Calculation Related DEFINITIONS *
 ****************************************************************************/
typedef tRBTree         tV3OsSpf;
                            /* Spanning Tree structure. This has been mapped
                             * to the TMO hash table in this implementation */
typedef tTMO_HASH_TABLE tV3OsCandteLst;
                            /* List of candidates during RT calculation. This
                             * has been mapped to the TMO hash table in this
                             * implementation */


/* Timer Related data structures */
typedef struct _V3OsTimerDesc {
    VOID                (*pTimerExpFunc)(VOID *);
                            /* Pointer pointing to the call back function
                             * to be called after the timer expires */ 
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte alignment */
} tV3OsTimerDesc;

typedef struct _V3OspfTimer {
    tTmrAppTimer        timerNode;
                            /* Timer node information */
    INT2                i2Status;
                            /* Status of the timer */
    UINT1               u1TimerId;
                            /* TimerId of the timer */
    UINT1               u1Rsvd;
                            /* Included for 4-byte alignment */
} tV3OspfTimer;


/****************************************************************************/
/* Metric related.                                                          */
/****************************************************************************/
/* Type definitions related to metrics */
typedef struct _V3OsMetric {
    UINT4               u4Metric;
                            /* Indicates the metric cost */
    UINT4               u4MetricType; 
                            /* Indicates the type of the metric */
} tV3OsMetric;


/****************************************************************************/
/* Buffer QID Related.                                                      */
/****************************************************************************/
typedef struct _V3OsBufQid {
    INT4                i4LsaQid;
                            /* Smart Buddy instance Id created for 
                             * storing LSA */
} tV3OsBufQid;


/****************************************************************************/
/* OSPF PACKETS                                                             */
/****************************************************************************/

/* Format of OSPFv3 packet header */
typedef struct _V3OsHeader {
    UINT1               u1Version;
                            /* OSPFv3 version number */
    UINT1               u1Type;
                            /* OSPFv3 packet type */
    UINT2               u2Len;
                            /* Total Length of packet in bytes */
    tV3OsRouterId       rtrId;
                            /* Router Id of the packet's source */
    tV3OsAreaId         areaId;
                            /* Area Id the packet belongs to */
    UINT2               u2Chksum;
                           /* The standard IPv6 16-bit one's complement 
                            * checksum, covering the entire OSPF packet 
                            * and prepended IPv6 pseudo-header */ 
    UINT1               u1InstId;
                           /* This field is zero always, since this 
                            * implementaion does not support multiple 
                            * instance. */
    UINT1               u1Rsvd;
                            /* Included for 4-byte alignment */
}  tV3OsHeader;

/* Format of hello packet */
typedef struct _V3HelloStaticPortion {

    UINT4               u4InterfaceId;
                            /* Interface ID this is MIB-II IfIndex.  */  
    tV3OsOptions        options;
                            /* optional capabilities supported by this 
                             * router */
    UINT1               u1RtrPriority;
                            /* this router's priority used in DR election */
    UINT2               u2HelloInterval;
                            /* The interval between this router's hello 
                             * packets */
    UINT2               u2RtrDeadInterval;
                            /* The number of seconds before declaring a
                             * silent rtr dead */
    tV3OsRouterId       desgRtr;
                            /* The designated router in view of the
                             * advertising router */
    tV3OsRouterId       backupDesgRtr;
                            /* The backup designated router in advertising
                             * router's view */
}  tV3HelloStaticPortion;

/* format of link state advertisement header */

typedef struct _V3OsLsHeader {
    UINT2               u2LsaAge;
                            /* the time in seconds since the link state advt
                             * was originated */
    UINT2               u2LsaType;
                            /* the type of the link state advt.  */
    tV3OsLinkStateId    linkStateId;
                            /* this field distinguish between multiple LSA's
                             * whose LS type is same and advertise by the 
                             * same router. */
    tV3OsRouterId       advRtrId;
                            /* the router ID of the instance that originated
                             * the advt.  */
    tV3OsLsaSeqNum      lsaSeqNum;
                            /* identifies the instance of the advertisement */
    UINT2               u2LsaChksum;
                            /* the checksum of the complete contents of the
                             * link state advt */
    UINT2               u2LsaLen;
                            /* length in bytes of the advt.
                             * including the 16 -byte header */
}  tV3OsLsHeader;


/****************************************************************************/
/* Area Address Range (Aggregate)                                           */
/****************************************************************************/

/* Address Range data structures of Area */
typedef struct _V3OsAddrRange {

    tTMO_SLL_NODE       nextAddrRange; 
                            /* Points to the next address range structure in the
                             * linked list */
    tV3OsMetric         metric;
                            /* Specifies the cost to be advertised during
                             * summary LSA construction.  */
    tV3OsLinkStateId    linkStateId;
                             /* This is used for LSA Generation */
    tIp6Addr            ip6Addr;
                            /* From RFC - IPv6 address */
    struct _V3OsArea   *pArea;
                            /* Back pointer to area in which the range is
                             * added
                             */
    UINT4               u4AddrRangeRtTag;
                            /* Route Tag information of this address range */
    UINT4               u4RtCount;
                            /* Counter indicating the number of route entries 
                               falling in the address range */
    UINT1               u1PrefixLength;
                            /* Prefix Length */
    tV3OsRowStatus          areaAggStatus;
                            /* From MIB - * SNMP row status */
    UINT1               u1AggrEffect;
                            /* From RFC -
                             * Flag which indicates whether this address range
                             * is to be summarized or not, in LSAs.
                             * Value - 1, The address range is summarized
                             * Value - 2, The address range is not summarized
                             */
    UINT1               u1Rsvd; 
                            /* Added for 4-byte alignment */
} tV3OsAddrRange;


/****************************************************************************/
/* AREA                                                                     */
/****************************************************************************/

/* Area data structure */
typedef struct _V3OsArea {

    tTMO_SLL_NODE       nextArea;
                            /* Points to the next area structure in the
                             * linked list */
    tV3OsAreaId         areaId;
                            /* A 32-bit number that identifies the area.
                             * `0.0.0.0' is reserved for the backbone area */
    tTMO_SLL            type3AggrLst;
                            /* A list of aggregtion structure.This list is 
                             * for type 3 lsa aggregation. This list
                             * is in sorted order based on ipv6 prefix address
                             * and prefix length.  */
    tTMO_SLL            type7AggrLst;
                            /* A list of aggregtion structure.This list is 
                             * for type 7 lsa aggregation. This list
                             * is in sorted order based on ipv6 prefix address
                             * and prefix length.  */
    tTMO_SLL            ifsInArea;
                            /* From RFC -
                             * A list of interface structures corresponding to
                             * the router's interfaces connecting to this area.
                             */
    tTMO_SLL            IARefRtrPrefixLst;
    tTMO_HASH_TABLE    *pRtrLsaHashTable;
                            /* From RFC -
                             * A Hash Table of router LSAs generated by
                             * routers in this area. The Index to the Hash
                             * Table is Advertising Router ID.  */
    tTMO_HASH_TABLE    *pIntraPrefixLsaTable;
                            /* From RFC -
                             * A Hash Table of Intra Area Prefix LSAs 
                             * generated by routers in this area. The 
                             * Index to the Hash Table is Advertising 
                             * Router ID.  */
    tTMO_HASH_TABLE    *pInterPrefixLsaTable;
                            /* From RFC -
                             * A Hash Table of  Inter Area Prefix  LSAs 
                             * generated by area border routers. They 
                             * describe routes to destinations that are 
                             * internal to the AS, But external to this 
                             * area.The Index to the hash table
                             * is Ipv6 Address Prefix and Prefix length. */
    tTMO_HASH_TABLE    *pNssaLsaHashTable;
                            /* From RFC -
                             * A Hash Table of  NSSA  LSAs generated
                             * The Index to the hash table
                             * is Ipv6 Address Prefix and Prefix length.  */
    tTMO_HASH_TABLE    *pInterRouterLsaTable;
                            /* From RFC -
                             * A Hash Table of Inter Area Router LSA 
                             * The Index to the hash table 
                             * ASBR Router Id. 
                             */
    tRBTree             pAreaScopeLsaRBRoot;
                            /* Pointer to RB tree Root. RB tree maintain all 
                             * area scope LSA.  */
    tTMO_SLL            asExtAddrRangeLst; 
                            /* A singly linked list of AS External Address 
                             * range. */     
    tTMO_SLL            type7AddrRangeLst;     
                            /* A singly linked list of Type 7 Address range. */
    tV3OsMetric         stubDefaultCost;
                            /* From RFC -
                             * If this area is configured as a stub area and
                             * this router is itself an area border router, then
                             * this field specifies the cost to be advertised in
                             * the default summary link advertisement. */
    tV3OspfTimer        nssaStbltyIntrvlTmr;
                            /* Impementaion  -
                             * NSSA stability timer. */    
    tV3OsSpf            pSpf;
                            /* Pointer to the SPF tree resulted out of the
                             * previous RT calculation */
    struct _V3OspfCxt  *pV3OspfCxt;
                            /* Back pointer to the context structure */
    UINT4               u4AreaType;    
                            /* Indicates whether area is Stub area, NSSA area
                             * or Normal area. */
    UINT4               u4AbrStatFlg;    
                            /* This flag is used in NSSA translator election 
                             * algorithm to determine changes in the ABRs 
                             * in an area. */
    UINT4               u4AreaBdrRtrCount;
                            /* From MIB -
                             * The number of area border routers reachable
                             * within this area. This is initially '0', and is
                             * calculated in each SPF pass. */
    UINT4               u4AsBdrRtrCount;
                            /* From MIB -
                             * The number of AS boundary routers reachable
                             * within this area. This is initially '0', and is
                             * calculated in each SPF pass */
    UINT4               u4AreaScopeLsaChksumSum;
                            /* From MIB -
                             * The sum of the checksums of the LSAs in this
                             * area's database */
    UINT4               u4AreaScopeLsaCount;
                            /* From MIB -
                             * Number of Area Scope LSA in this area. */
    UINT4               u4ActIntCount;
                            /* Active Interfaces count in the Area */
    UINT4               u4FullNbrCount;
                            /* Number of Neighbors in the Area Which
                             * are in FULL state */
    UINT4               u4FullVirtNbrCount;
                            /* Number of Virtual Neighbors in the
                             * Area which are in FULL state */
    UINT4               u4NssaTrnsltrEvents;    
                            /* From MIB -
                             * Indicates the number of times translator state 
                             * changed.  */    
    UINT4               u4DfInfOriginate;

    UINT4               u4DcBitResetLsaCount;
                            /* denotes the no. of DC bit reset lsas in the
                             * area's database including the indication lsa */
    /* Dc Bit Fix */
    UINT4               u4DcBitResetSelfOrgLsaCount;
                            /* denotes the no. of DC bit reset lsas in the
                             * area's database including the indication lsa 
                             * that are self originated */
    UINT4               u4IndicationLsaCount;
                            /* denotes the no. of indication lsas in the
                             * area's database */
    UINT4               u4SpfRuns;
                            /* From MIB -
                             * The number of times the intra-area routing table
                             * has been calculated using this area's database */
    UINT4               u4NssaTrnsltrStbltyInterval;                         
                            /* From MIB -
                             * NSSA stability interval.  */    
    UINT4               u4rtrLsaCounter;
                            /* Counter for assigning link state Id when 
                             * originating multiple Router LSAs.*/
    UINT4               u4rtrLsaLinkSize;
                            /* Value indicates the size of the recent 
                             * router LSA generated by the router, used in 
                             * deciding whether to generate another Router 
                             * LSA or not */
    UINT4               u4IntraAreaLsaCounter;
                            /* Counter for assigning link state Id when 
                             * originating multiple Intra-Area Prefix LSAs.*/
    tV3OsOptions        areaOptions;                     
                            /* Indicates the optional capabilities supported
                             * by the area.  */
    tV3OsRowStatus      areaStatus;
                            /* From MIB -
                             * This field indicates the row status */
    UINT1               u1NssaTrnsltrRole;
                            /* From MIB -
                             * Indicates an NSSA Border router's ability to 
                             * perform NSSA translation of type-7 LSAs into 
                             * type-5 LSAs. */     
    UINT1               u1NssaTrnsltrState;    
                            /* From MIB -
                             * NSSA translator State. */
    tV3OsTruthValue     bNewlyAttached; 
                            /* This flag will be set if the area is newly 
                             * created and will be reset after the route 
                             * calculation and summary LSAs generation. */
    tV3OsTruthValue     bTransitCapability;
                            /* From RFC -
                             * indicates whether any active virtual links,
                             * in this area
                             * OSPF_TRUE when the area supports a virtual link
                             * otherwise OSPF_FALSE.  */
    tV3OsTruthValue     bPreviousTransitCapability;
                            /* Stores the prvious trasit capability of the 
                             * area during route calculation, and is useful
                             * in NSSA module */
    tV3OsTruthValue     bIndicationLsaPresence;
                            /* indicates whether the router originated any
                             * indication lsa into this area or not */
    UINT1               u1SummaryFunctionality;
                            /* From MIB -
                             * Indicates whether the summarization will be done
                             * for the stub area or not.  */
    UINT1               u1Pad;
} tV3OsArea;

/****************************************************************************/
/* HOST                                                                     */
/****************************************************************************/

typedef struct _V3OsHost {

    tRBNodeEmbd         RbNode;
                            /* Embedded RB Tree node for host nodes */
    tIp6Addr            hostIp6Addr;
                            /* Indicates the Host IPv6 address.  */
    tV3OsArea          *pArea;
                            /* From MIB -
                             * Pointer to the area to which the host belongs.*/
    UINT4               u4HostMetric;
                            /* cost associated with the host */
    tV3OsRowStatus      hostStatus;
                            /* From MIB - * Host Row Status.  */ 
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte alignment */
} tV3OsHost;

/****************************************************************************/
/* INTERFACE  RELATED STRUCTURES                                            */
/****************************************************************************/

/* structure for sending LAK packets on the interface */
typedef struct _V3OsDelLsAck {

    UINT1              *pAckPkt;
                             /* Pointer to the delayed Ack packet */
    tV3OspfTimer        delAckTimer;
                             /* An interval timer whose expiry results in the
                              * transmission of delayed acknowledgement */
    UINT2               u2CurrentLen;
                             /* current length of the pkt */
    UINT2               u2Rsvd;
                             /* Included for 4-byte alignment */
} tV3OsDelLsAck;

/* structure for sending LSU packets on the interface */
typedef struct _V3OsLsUpdate {

    UINT1              *pLsuPkt;
                            /* Pointer to the LSU packet */
    UINT2               u2LsaCount;
                            /* current count of lsas in the lsu pkt */
    UINT2               u2CurrentLen;
                            /* current length of the pkt */
} tV3OsLsUpdate;

typedef struct _V3CryptoSeqInfo {
        UINT4               u4HighCryptSeqNum;
        UINT4               u4LowCryptSeqNum;
} tV3CryptoSeqInfo;


typedef enum _OspfV3SHAversion {
    OSPFV3_AR_SHA256_ALGO = 2,
    OSPFV3_AR_SHA384_ALGO = 3,
    OSPFV3_AR_SHA512_ALGO = 4
} tV3ShaVersion;

/* Authentication Trailer information data structure */

typedef struct _authkeyInfo {
    tTMO_SLL_NODE       nextSortKey;
                            /* next key for the interface sorted by keyid
                             */
    UINT4               u4KeyStartAccept;
                            /* The time that the router will start
                             * accepting pkt with this key
                             */
    UINT4               u4KeyStartGenerate;
                            /* The time that the router will start
                             * using this key for pkt generation
                             */
    UINT4               u4KeyStopGenerate;
                            /* The time that the router will stop
                             * using this key for pkt generation
                             */
    UINT4               u4KeyStopAccept;
                            /* The time that the router will stop
                             * accepting pkt with this key
                             */
    UINT2               u2AuthkeyId;
                            /* authentication key identifier
                             */

    UINT1               au1AuthKey[OSPFV3_MAX_AUTHKEY_LEN + 1];
                            /* The authentication key
                             */
    UINT1               u1KeyLen;
                            /* Length of the Cryptographic authentication
                               key
                            */
    UINT1               u1AuthkeyStatus;
                            /* indicates status of this entry valid or invalid
                             */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                             */
} tAuthkeyInfo;


/* Interface data structure */
typedef struct _V3OsInterface {

    tRBNodeEmbd         RbNode;
                            /* Embedded RB Tree node */
    tTMO_SLL_NODE       nextIfInArea;
                            /* Pointer to the next element in the list of
                             * interfaces belonging to this area
                             * (Maintained as a singly linked list) */
    tTMO_SLL_NODE       nextVirtIfNode;
                            /* pointer to the linked list of virtual 
                             * interface. */
    tIp6Addr            ifIp6Addr;
                            /* From RFC -
                             * IP6 address of the interface */
    tRBTree             pLinkScopeLsaRBRoot;
                            /* Pointer to RB tree Root. RB tree maintain all 
                             * Link scope LSA. */
    tTMO_SLL            nbrsInIf;
                            /* Pointer to linked list of neighbors associated
                             * with this interface */
    tTMO_SLL            ip6AddrLst;
                            /* List of IPv6 addresses configured on 
                             * this interface.  */ 
    tTMO_SLL            ip6NetPrefLst;
                            /* List of IPv6 prefixes on the transit link. 
                             * Used in Intra-Area Prefix LSA generation */ 
    tTMO_SLL            linkLsaLst;
                           /* This List Contains all Link Lsa */
    tTMO_SLL            sortAuthkeyLst;
                           /*List of authentication key information. Sorted by key ID */
    tV3OsRouterId       desgRtr;
                            /* The designated router
                             * selected for attatched network */
    tV3OsRouterId       backupDesgRtr;
                            /* the backup designated router
                             * selected for attatched network */
    tV3OsRouterId       destRtrId;
                            /* The router id at the other end point of the
                             * links. (Valid for virtual links alone).  */
    tV3OsAreaId         transitAreaId;
                            /* The Area ID of the area through which packets
                             * over this link are transmitted. (Valid for
                             * virtual links alone).  */
    tV3OsLinkStateId    linkStateId;
    tV3OsArea           *pArea;
                            /* Pointer to the area to which the interface is
                             * associated */
    tV3OsDelLsAck       delLsAck;
                            /* Implementation -
                             * The Acknowledgement packet used to send delayed
                             * acknowledgements */
    tV3OsLsUpdate       lsUpdate;
                            /* Implementation - The LSU pkt that is to be 
                             * sent on this interface */
    tV3OspfTimer        helloTimer;
                            /* an interval timer which fires
                             * every hello_interval seconds */
    tV3OspfTimer        waitTimer;
                            /* a single shot timer which causes
                             * interface to exit waiting state */
    tV3OspfTimer        pollTimer;
                            /* NBMA and DC ptop nbrs -
                             * timer whose firing causes hellos
                             * to be sent to inactive nbrs
                             * An interval timer which fires every poll
                             * interval seconds */
    tV3OspfTimer        MultIfActiveDetectTimer;
                           /* a single shot timer which causes
                            * interface to elect the Active and 
                            * Standby interfaces */
    tV3OspfTimer        nbrProbeTimer;
                            /* From MIB -
                             * Neighbor Probe Timer.  */
    UINT4               u4InterfaceId;
                            /* From MIB - Interface ID of the interface this 
                             * is MIB-II IfIndex.  */ 
    UINT4               u4DRInterfaceId;
                            /* Impementaion -
                             * Interface ID of the designated router's 
                             * interface.  */
    UINT4               u4BDRInterfaceId;
                            /* Impementaion -
                             * Interface ID of the backup designated router's
                             * interface.  */
    UINT4               u4PollInterval;
                            /* Poll interval value */
    UINT4               u4NbrProbeInterval;
                            /* From MIB -
                             * Neighbor Probe Interval value.  */ 
    UINT4               u4NbrProbeRxmtLimit;
                            /* From MIB
                             * This linit indicates The number of 
                             * consecutive LSA retransmission before 
                             * neighbor adjacency brought down.  */ 
    UINT4               u4MtuSize;
                            /* Implementation -
                             * The maximum size of a packet that
                             * can be txed on this interface */
    UINT4               u4NbrFullCount;
                            /* This will indicate the Number of 
                             * Neighbors in FULL state on this Interface */
    UINT4               u4IfMetric;
                            /* Metric associated with this interface */
    UINT4               u4MultActiveIfId;
                            /*Implementation -
                             * Interface ID of the Elected Active Interface
                             * over multiple interface to a link */

    /* The following fields are counters for statistics */
    UINT4               u4HelloRcvdCount;
                            /* hello packets received */
    UINT4               u4HelloTxedCount;
                            /* hello packets transmitted */
    UINT4               u4HelloDisdCount;
                            /* hello packets discarded */

    UINT4               u4DdpRcvdCount;
                            /* DDP packets received */
    UINT4               u4DdpTxedCount;
                            /* DDP packets transmitted */
    UINT4               u4DdpDisdCount;
                            /* DDP packets discarded */

    UINT4               u4LsaReqRcvdCount;
                            /* LSA request packets received */
    UINT4               u4LsaReqTxedCount;
                            /* LSA request packets transmitted */
    UINT4               u4LsaReqDisdCount;
                            /* LSA request packets discarded */

    UINT4               u4LsaUpdateRcvdCount;
                            /* LSA update packets received */
    UINT4               u4LsaUpdateTxedCount;
                            /* LSA update packets transmitted */
    UINT4               u4LsaUpdateDisdCount;
                            /* LSA update packets discarded */

    UINT4               u4LsaAckRcvdCount;
                            /* LSA ack packets received */
    UINT4               u4LsaAckTxedCount;
                            /* LSA ack packets transmitted */
    UINT4               u4LsaAckDisdCount;
                            /* LSA ack packets discarded */
    UINT4 u4NotFoundSACnt;
      /*AT packets discarded on this interface due to unavailability of Security Association ID*/
    UINT4 u4DigestFailCnt;
                           /*AT packets discarded on this interface due to digest failures*/
    UINT4 u4CrytoSeqMismtchCnt;
                           /*AT packets discarded on this interface due to crypto sequence number mismatch*/
    UINT4 u4ATHdrLengthErrCnt;
                           /*AT packets discarded on this interface due to header length errors*/
    UINT4 u4ATAlgoMismtchCnt;
                           /*AT packets discarded on this interface due Algorithm mismatch on this interface*/
    UINT4               u4AuthUpdateTxedCount;
                            /* Auth update packets transmitted */
    UINT4               u4AuthUpdateRcvdCount;
                            /* Auth update packets received */
    UINT4               u4AuthUpdateDisdCount;
                            /* Auth update packets discarded */
    UINT4               u4IfEvents;
                            /* From MIB -
                             * the no. of times this interface has changed
                             * state,or an error has occurred */
    UINT4               u4LinkScopeLsaChkSumsum;
                            /* From MIB - The sum of the checksums of 
                             * the Link Scope LSAs */
    UINT4               u4LinkScopeLsaCount;
                            /* From MIB -
                             * Total number of Link Scope LSA.  */
    UINT4               u4OutIfIndex;
                            /* This is applicable only for virtual
                             * interface */
    UINT4               u4AddrlessIf; /*Unnumbered interface*/

    UINT2               u2HelloInterval;
                            /* Hello interval timer value */
    UINT2               u2RtrDeadInterval;
                            /* Router dead interval timer value */
    UINT2               u2RxmtInterval;
                            /* Retransmit interval value */
    UINT2               u2IfTransDelay;
                            /* Time taken to transmit an lsa
                             * over this interface */
    UINT2               u2MultIfActiveDetectInterval;
                            /* Implementation -
                             * Detect Active timer vlaue*/
    UINT2               u2InstId;
                            /* Unique channel id to separate the OSPFv3
                             * routing domain. Default value is 0.
                             * This instance id is compared to the instance
                             * id present in the OSPFv3 header during
                             * packet reception
                             */
    tV3OsRowStatus      ifStatus;
                            /* From MIB - SNMP if table row status */
    tV3OspfStatus       admnStatus;
                            /* From MIB -
                             * Administrative status of the interface */
    
    tV3OspfStatus       operStatus;
                            /* Implementation - operational state of 
                             * interface */
    tV3OsTruthValue     bDcEndpt;
                            /* whether the interface connects to a demand
                             * circuit or not */
    tV3OsTruthValue     bDemandNbrProbe;
                            /* From MIB -
                             * Indicates whether neighbor probing is enabled 
                             * or not. */
    tV3OsTruthValue     bPassive;
                            /* Flag to indicate whether the interface 
                             * is passive or not. */
    tV3OsTruthValue     bVlReachability; 
                            /* Implementation -
                             * Indicates whether virtual end point is 
                             * reachable or not. */ 
    tV3OsTruthValue     bLinkLsaSuppress;
                           /* From RFC -
                            * Flag to indicate whether Origination of
                            * Link LSA is suppressed or not */
    UINT1               u1IsmState;
                            /* From RFC -
                             * functional level of interface */
    UINT1               u1ConfStatus;
                            /* Indicates whether the interface has been
                             * configured as a DC end point or it is a
                             * discovered DC end point */
    UINT1               u1NetworkType;
                            /* From RFC -
                             * Type of attached network */
    UINT1               u1RtrPriority;
                            /* Priority of the router (defines the eligibility
                             * to become DR or BDR) */
    UINT1               u1GRRouterPresent;
                            /* GR router present in this interface, 
                             * this flag is required in helper mode 
                             * to know atlease one GR router present 
                             * in this interface as a neighbor */
    UINT1               u1GrLsaAckRcvd;
                            /* Grace LSA Ack Received */
    UINT1               u1ActiveDetectTmrFlag;
                            /* Implementation-
                             * Flag to indicate the stages of Active
                             * Detect Timer*/
    UINT1               u1SdbyActCount;
                            /* Implementation-
                             * Indicates the number of Interfaces of a router
                             * over the link .
                             * Default value is 1.
                             * This value is valid only for Active Interfaces
                             * over the link */
    tV3OsOptions        ifOptions;
                            /* Indicates the optional capabilities supported
                              * by the Interface.  */
    UINT1               u1BfdIfStatus; /* Used to check whether specific interface has bfd enabled or not */
    UINT2               u2AuthType; /*Check if AT enabled.[None/ crypto]*/
    UINT2               u2CryptoAuthType;/*Has the algorithm to be used for AT*/
    tAuthkeyInfo       *pAuthKeyId;/*Pointer SA ID in use.*/
    tAuthkeyInfo       *pLastAuthkey;
                            /* The AuthInfo last used */
    tV3CryptoSeqInfo    cryptoSeqno;/*Cryptographic sequence number*/
    UINT1               u1AuthMode;/*Specifies the mode of authentication  [transition/full]*/
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte Alignment
                            */
}  tV3OsInterface;


/* Queue structure which contains ism_scheduled ism events to be processed */
typedef struct _V3OsIsmSchedNode {
    tV3OsInterface      *pInterface;
                            /* Pointer to OSPFv3 interface */
    UINT1               u1Event;
                            /* Scheduled event */
    UINT1               u1IsNodeValid;
                            /* This contains the following values
                             * OSPFV3_TRUE    - The node is valid
                             * OSPFV3_FALSE   - The node is not
                             *                  valid. This will be set during
                             *                  context deletion/disable
                             *                  Interface deletion/disable
                             */
    UINT1               au1Rsvd[2];
                            /* Included for 4-byte alignment */
} tV3OsIsmSchedNode;

typedef struct _V3OsIsmSchedQueue {
    tV3OsIsmSchedNode   aIfEvents[OSPFV3_MAX_ISM_SCHED_QUEUE_SIZE];
    UINT1               u1FreeEntry;
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte alignment */
} tV3OsIsmSchedQueue;


/****************************************************************************/
/* NEIGHBOR RELATED STRUCTURES                                              */
/****************************************************************************/

/* Structure for sending DDP packets to the neighbor */
typedef struct _V3OsDbSummaryDesc {
    UINT1                   *dbSummaryLst;
                                /* Complete list of LS headers in the database
                                 * to be sent in DDPs. This is maintained as an
                                 * array. A contiguous block of memory is
                                 * allocated while creating this list based on
                                 * the number of LSAs currently present in the
                                 * database.  */
    tV3OspfTimer            ddTimer;
                                /* ddp rxmt timer
                                 * The usage of the timer depends on the
                                 * neighbor state.
                                 *  _________________________________________
                                 * | Neighbor | Master/ | Usage of ddTimer   |
                                 * | State    | Slave   |                    |
                                 * |          | status  |                    |
                                 * |__________|_________|____________________|
                                 * | EXSTART  | BOTH    | retransmission of  |
                                 * |          |         | DDP init packets   |
                                 * |          |         |                    |
                                 * | EXCHANGE | MASTER  | retransmission of  |
                                 * |          |         | unacknowledged DDP |
                                 * | LOADING/ |         |                    |
                                 * | FULL     | SLAVE   | maintaining the    |
                                 * |          |         | last transmitted   |
                                 * |          |         | DDP for            |
                                 * |          |         | rtr_dead_interval  |
                                 * |          |         | seconds in order to|
                                 * |          |         | respond to         |
                                 * |          |         | duplicates received|
                                 * |          |         | from the master.   |
                                 * |__________|_________|____________________|
                                 */
    tV3OsLsaSeqNum          seqNum;
                                /* This sequence number is used to identify
                                 * whether a received DDP is a duplicate or the
                                 * next in sequence */
    UINT1                  *dbSummaryPkt;
                                /* The DD packet that was transmitted the last
                                 * time. This is used for retransmission.  */
    struct _V3OsLsaInfo    *lstTxedLsa; 
                                /* Pointer to the last LSA being transfered */
    tRBTree                 pLsaRBRoot;
                                /* Pointer to the RB tree from which the last 
                                 * LSA being transfered. */
    UINT2                   u2LastTxedSummaryLen;
                                /* Length of the last transmitted summary 
                                 * list */
    tV3OsTruthValue         bMaster;
                                /* Indicates the status of this router during
                                 * the database exchange process with the
                                 * neighbor. It is set to TRUE if this router is
                                 * the master during exchange process */
    UINT1                   u1Rsvd;
                                /* Included for 4-byte alignment */
} tV3OsDbSummaryDesc;


/* Structure for LSA rxmission to the neighbor */
typedef struct _V3OsLsaRxmDesc {
    UINT4               u4LsaRxmtCount;
                            /* no. of lsas to be rxmtted */
    tV3OspfTimer        lsaRxmtTimer;
                            /* Timer started for LSA retransmission timeout
                             * period */
} tV3OsLsaRxmtDesc;

/* Structures for LSA request to the neighbor */
typedef struct _V3OsLsaReqNode {
    tTMO_SLL_NODE       nextLsaReqNode;
                            /* The next node in the lsaReqLst */
    tV3OsLsHeader       lsHeader;
                            /* LS header of the lsa to be requested */
} tV3OsLsaReqNode;

typedef struct _V3OsLsaReqDesc {
    tTMO_SLL            lsaReqLst;
                            /* The singly linked list of LS headers. This is
                             * maintained as a singly linked list */
    tV3OspfTimer        lsaReqRxmtTimer;
                            /* Unsatisfied requests are retransmitted every
                             * rxmt_interval seconds. This timer is used for
                             * that purpose */
    tTMO_SLL_NODE      *startNextReqPkt;
                            /* The start of the next LS req packet */
    tV3OsTruthValue     bLsaReqOutstanding;
                            /* Indicates whether there is any LSA request that
                             * is yet to be acknowledged
                             * OSPF_TRUE when lrq pkt is sent and waiting
                             * for response. OSPF_FALSE otherwise */
    UINT1               au1Rsvd[3];
                             /* Included for 4-byte alignment */
} tV3OsLsaReqDesc;


typedef struct _V3OsLastRxDdp {
    INT4                i4Ddseqno;
                            /* Contains the DD sequence number */
    tV3OsOptions        rtrOptions;
                            /* Indicates the optional capabilities supported
                             * by the router */
    UINT1               u1Flags;
                            /* The flag holding the values of Init, More and
                             * MS bits */
}tV3OsLastRxDdp;


/****************************************************************************/
/* Strucuture to store last received Hello for HA support                   */
/****************************************************************************/


typedef struct _V3OsRmHelloPkt
{
    tIp6Addr            SrcIp6Addr;       /* IPv6 address of the nbr */
    UINT1               au1Pkt[OSPFV3_MAX_HELLO_PKT_LEN];
                                          /* The actual hello packet */
    UINT4               u4IfIndex;        /* The receiving interface */
    UINT2               u2Len;            /* Length of the packet    */
    UINT1               au1Reserved [2];
}tV3OsRmHello;


/* Neighbor data structure */
typedef struct _V3OsNeighbor {
    tTMO_SLL_NODE       nextNbrInIf;
                            /* The next element in the list of neighbors
                             * belonging to the same interface */
    tTMO_SLL_NODE       nextSortNbr;
                            /* The next element in the list of neighbors
                             * maintained for the entire router */
    tV3OspfTimer        inactivityTimer;
                            /* From RFC -
                             * timer which indicates that no hello has been
                             * received from the neighbor recently */
    tV3OspfTimer        helperGraceTimer;
                            /* Neighbor Grace Timer */
    tTMO_SLL            lsaRxmtLst;
                            /* LSA rxmt List */
    tV3OsLastRxDdp      lastRxDdp;
                            /* From RFC -
                             * Indicates the last received data base description
                             * packet used to determine duplicate packets */
    tV3OsLsaRxmtDesc    lsaRxmtDesc;
                            /* From RFC -
                             * list of lsas to be retransmitted
                             * to this neighbor */
    tV3OsLsaReqDesc     lsaReqDesc;
                            /* From RFC -
                             * List of LS headers to be sent in the LS request
                             * packets to the neighbor. This is maintained as a
                             * linked list */
    tV3OsDbSummaryDesc  dbSummary;
                            /* From RFC -
                             * Complete list of LSAs in the database to be sent
                             * in DDPs during the database exchange process.
                             * This is maintained as an array */
    tV3OsInterface     *pInterface;
                            /* Implementation -
                             * Pointer to the interface to which this neighbor
                             * belongs */
    tV3OsInterface     *pActiveInterface;
                           /* Implementation -
                            * Pointer to the interface which is 
                            * elected as active over the link.
                            * If the nbr is a normal OSPFv3 Nbr, then
                            * it points to NULL*/
    tV3OsRouterId       nbrRtrId;
                            /* From RFC -
                             * router id of the neighbor */
    tIp6Addr            nbrIpv6Addr;
                            /* From RFC -
                             * IP6 address of the neighboring router's 
                             * interface */
    tV3OsRouterId       desgRtr;
                            /* From RFC -
                             * Neighbor's idea of the designated router of the
                             * attached network */
    tV3OsRouterId       backupDesgRtr;
                            /* From RFC -
                             * Neighbor's idea of the backup designated router
                             * of the attached network.  */
    tV3OsOptions        nbrOptions;
                            /* From RFC -
                             * optional capabilities of the neighbor */
    UINT1               u1Pad;

    tV3OsRmHello        lastHelloInfo;
                            /* The details of the last hello received from the neighbor */
    tV3OsLinkStateId    linkStateId;
    UINT4               u4NbrInterfaceId;
                            /* Interface Id of the neighboring router's 
                             * interface. */ 
    UINT4               u4NbrEvents;
                            /* From MIB -
                             * the no. of times the neighbor relation has
                             * changed state or an error had occurred */
    UINT4               u4NbrProbeCount;
                            /* the number of times probing failed for 
                             * for this neighbor.  */
    UINT4               u4HelloSuppressPtoMp; 
                            /* Implementaion -
                             * Hello suppresion over point to multi point 
                             * link.  */ 
    UINT4               u4NbrIfMetric;
                            /* Implementation -
                             * Metric field is valid for standby interface 
                             * Nbr of the link.
                             * Invalid i.e. 0 for Normal Nbr*/
    UINT4               u4LastHelloRcvd;
    UINT1               u1NbrIfStatus;
                            /* Implementation -
                             * Status flag to indicate whether the 
                             * nbr is Normal Nbr
                             * or Standby interface Nbr of the link*/
    UINT1               u1NsmState;
                            /* From RFC -
                             * functional level of the neighbor conversation */
    INT2                i2NbrsTableIndex;
                            /* The index to be used in the array of Nbr
                             * pointers */
    UINT1               u1NbrRtrPriority;
                            /* From RFC -
                             * router priority of the neighboring router */
    UINT1               u1ConfigStatus;
                            /* From MIB -
                             * flag which indicates whether this neighbor
                             * was dynamically discovered or configured */
    UINT1               u1GraceLsaTxCount;
                            /* No. of grace LSA transmitted to this neighbor */
    UINT1               u1NbrHelperStatus;
                            /* Helper Status */
    UINT1               u1NbrHelperExitReason;
                            /* Exit reason for last occurred Helper operation */
    UINT1               u1HelloSyncState;
                            /* OSIX_TRUE  -  If the last sent hello packet
                             *               succeeded
                             * OSIX_FALSE -  If last sent hello packet
                             *               failed
                             */
    tV3OsRowStatus      nbrStatus;
                            /* From MIB -
                             * SNMP row status for nbr table */
    tV3OsTruthValue     bHelloSuppression;
                            /* Indicates whether hello had been suppressed for
                             * this nbr relation or not */
    tV3CryptoSeqInfo    aCryptoSeq [OSPFV3_MAX_PKT_TYPE];/*Stores AT crypto seq number per packet type*/
    UINT1               u1NbrBfdState;     /* To Check the Neighbor BFD status */
    BOOL1               bIsBfdDisable;     /* To check if BFD session to be disabled */
    tV3OsTruthValue     bIsABitSet;/*To check if AT is needed.*/
    UINT1               u1ConfiguredNbrRtrPriority;

} tV3OsNeighbor;


/****************************************************************************/
/* LSAs                                                                     */
/****************************************************************************/

typedef struct _V3OsLsaDesc {
    tTMO_DLL_NODE         nextLsaDesc;
                            /* Pointer to the next LSA Descriptor */
    struct _V3OsLsaInfo  *pLsaInfo;
                            /* Pointer to the lsa_info field of this LSA */
    tV3OspfTimer          minLsaIntervalTimer;
                            /* Only when this timer fires, newer instance of
                             * the advertisement can be originated */
    UINT1                *pAssoPrimitive;
                            /* Pointer to     - Type of LSA
                             * -----------------------------
                             * area           -     0x2001 
                             * interface      -     0x2002
                             * summary param  -     0x2003
                             * summary param  -     0x2004
                             * external route -     0x4005
                             * area           -     0x2007
                             * interface      -     0x0008
                             * area           -     0x2009
                             */
    UINT2                 u2InternalLsaType;
                            /* Used to differentiate between network summary
                             * LSA and default summary LSA
                             */
    UINT1                 u1MinLsaIntervalExpired;
                            /* Flag which indicates whether minLsaInterval
                             * Timer has fired or not. If this flag is set then
                             * newer instances can be immediately originated
                             */
    UINT1                 u1LsaChanged;
                            /* This flag is checked when minLsaIntervalTimer
                             * fires. If at that time this flag is set, then a
                             * newer instance of the advertisement is
                             * originated
                             */
    UINT1                 u1SeqNumWrapAround;
                            /* Indicates that the LSA has been flushed out due
                             * to sequence number wrap around. If this flag is
                             * set when the LSA is actually removed from
                             * database, a new instance is originated
                             */
    UINT1                 au1Rsvd[3];
                            /* Included for 4-byte alignment */
} tV3OsLsaDesc;

typedef struct _V3OsLsaId {
    tV3OsLinkStateId    linkStateId;
                            /* Identifies the portion of the internet being
                             * described by this advertisement */
    tV3OsRouterId       advRtrId;
                            /* Router ID of the router that originated the
                             * Advertisement */
    UINT2               u2LsaType;
                            /* Type of LSA */
    UINT2               u2Rsvd;
                            /* Included for 4-byte alignment */
} tV3OsLsaId;


typedef struct _V3OsLsaInfo {
    union
    {
        tRBNodeEmbd     RbAsScopeNode;
                            /* Embedded RB Tree node for AS scope LSA */
        tRBNodeEmbd     RbAreaScopeNode;
                            /* Embedded RB Tree node for Area scope LSA */
        tRBNodeEmbd     RbLinkScopeNode;
                            /* Embedded RB Tree node for Link scope LSA */
    } rbNode;
    struct _V3OspfCxt  *pV3OspfCxt;
                            /* Back pointer to the context associated
                             * with the LSA
                             */
    tTMO_SLL_NODE       nextLsaInfo;
                            /* The next LSA of Intra Area Router LS type.
                             * In each area  the LSAs of the Intra Area 
                             * Router LSA type are maintained in a
                             * singly linked list 
                             */
    tV3OsLsaDesc       *pLsaDesc;
                            /* Contains information regarding self-
                             * originated LSAs */
    tV3OsArea          *pArea;
                            /* Pointer to the area structure to whose
                             * database this LSA belongs */
    tV3OsLsaId          lsaId;
                            /* Contains the lsa_type, linkStateId and the
                             * advRtrId fields used to identify an LSA in the
                             * database */
    tV3OsLsaSeqNum      lsaSeqNum;
                            /* The sequence number of the advertisement.
                             * Whenever a router generates a new instance of
                             * an advertisement its sequence number is
                             * incremented. This field is useful in
                             * differentiating  between  various  instances
                             * of  an advertisement.  */
    tV3OspfTimer        lsaAgingTimer;
                            /* A single shot timer whose firing indicates the
                             * LSA has reached OSPFV3_MAX_AGE.
                             * In context of DEMAND_ CIRCUIT_SUPPORT
                             * it has a different Meaning */
    tV3OsInterface     *pInterface;
                            /* Pointer to the interface. This is used to 
                             * re originate Link LSA.  */
    UINT1              *pLsa;
                            /* The pointer to actual LSA
                             * the lsa is stored in a linear buffer with the
                             * integers stored in network byte order */
    UINT4               u4LsaArrivalTime;
                            /* The time of arrival of an advertisement. This
                             * field is used in aging the advertisement. This
                             * field is also used to discard advertisements
                             * received in less than min_lsa_interval seconds
                             * after installation */
    UINT4               u4RxmtCount;
                            /* The number of neighbors in whose retransmit
                             * list this LSA is currently present */
    UINT2               u2LsaAge;
                            /* The age in seconds of the advertisement. An
                             * advertisement is flushed out of the routing
                             * domain when its age reaches OSPFV3_MAX_AGE */
    UINT2               u2LsaChksum;
                            /* The checksum of the complete contents of the
                             * LSA except the age field 'u2LsaAge'. This
                             * allows age field to be updated without
                             * updating the checksum field */
    UINT2               u2LsaLen;
                            /* The length of the LSA */
    UINT1               u1FloodFlag;
                            /* Whenever a OSPFV3_MAX_AGE Lsa is received, 
                             * this flag is set to TRUE. This is checked 
                             * when Lsa's are flushed */
    UINT1               u1dnaLsaSplAgeFlag;
                            /* Flag to identify and flush the stale DNA LSAs
                             * whose origination is unreachable for 
                             * OSPFV3_MAX_AGE seconds */
    UINT1               u1TrnsltType5;
                            /* If set to OSIX_TRUE denotes that 
                             * Type 5 is a result of Type 7 translation.*/
    UINT1               u1FnEqvlFlag;
    UINT1               u1LsaRefresh;
                            /* Used to distinguish between modified LSA 
                             * and periodic refresh LSA */
    UINT1               u1Rsvd[1];
} tV3OsLsaInfo;

typedef struct _V3OsLeakRoutes {
    tRBNodeEmbd     RbNode;
    UINT4           u4IfIndex;
}tV3OsLeakRoutes;

typedef struct _V3OsPrefixInfo {
    tIp6Addr            addrPrefix;
                            /* IPv6 Address Prefix.  */ 
    UINT1               u1PrefixOpt;
                            /* Option associated with the LSA.  */
    UINT1               u1PrefixLength;
                            /* IPv6 Prefix Length.  */ 
    UINT2               u2Metric;
} tV3OsPrefixInfo;

/* Structure used to store the prefix information on the interface */
typedef struct _V3OsPrefixNode {
    tTMO_SLL_NODE       nextPrefixNode;
                            /* Next node in the SLL of PrefixNode 
                             * structures. */
    tV3OsPrefixInfo     prefixInfo;
                            /* PrefixInfo structure which contains 
                             * prefix information */
} tV3OsPrefixNode;


/* Structure which is passed as a parameter to the procedure constructing
 * summary lsa for an area.  */
typedef struct _V3OsSummaryParam {
    tV3OsPrefixInfo     prefixInfo;
                            /* Prefix Information. */
    UINT4               u4Metric;
                            /* Cost associated with the LSA.  */ 
    tV3OsOptions        options;
                            /* These options are used for Inter-Area Router 
                             * LSA */
    UINT1               au1Pad[1];
} tV3OsSummaryParam;


/****************************************************************************/
/* ROUTING TABLE                                                            */
/****************************************************************************/

/* Data structure to store the next hop information */
typedef struct _V3OsRouteNextHop {
    tIp6Addr            nbrIpv6Addr;
                            /* IPv6 address of the neighbor, if the type of
                             * next hop is a neighbor */
    tV3OsRouterId       advRtrId;
                            /* Advertising Router ID associated with the
                             * routes next hop */
    tV3OsInterface     *pInterface;
                            /* Pointer to interface structure */
} tV3OsRouteNextHop;


/* Data structure used to store the OSPFv3 path information */
typedef struct _V3OsPath {

    tTMO_SLL_NODE       nextPath;
                            /* The next path through different area  */
    tV3OsAreaId         areaId;
                            /* From RFC --
                             * The ID of the area whose Link State Database has
                             * led to this path */
    tV3OsLsaInfo        *pLsaInfo;
                            /* From RFC --
                             * Pointer to the LSA that directly references the
                             * destination.  */
    tV3OsRouteNextHop   aNextHops[OSPFV3_MAX_NEXT_HOPS];
                            /* Implementation -- Array of next hops */
    UINT4               u4Cost;
                            /* From RFC --
                             * The cost associated with this path. For paths
                             * other than type-2 external, this field indicates
                             * the entire path's cost. For type-2 external paths
                             * this indicates the cost of the path internal to
                             * AS */
    UINT4               u4Type2Cost;
                            /* From RFC --
                             * Defined for type-2 external paths alone. This
                             * field indicates the cost of the path's external
                             * portion */
    UINT1               u1PathType;
                            /* From RFC --
                             * The type of the path specified by this entry.
                             * This can be intra-area, inter-area, type-1
                             * external or type-2 external */
    UINT1               u1HopCount;
                            /* Implementation --
                             * The number of nextHops in this path */
    UINT2               u2Rsvd;
                            /* Included for 4-byte alignment */
} tV3OsPath;

/* Data structure to store the OSPFv3 route entry */
typedef struct _V3OsRtEntry {

    tTMO_SLL_NODE       nextRtEntryNode;
                            /* Points to the next Routing Table entry */
    union {
        tV3OsRouterId   destRtrId;
                            /* ABR/ASBR router Id for the route entries 
                             * describing ABR or ASBR */
        tIp6Addr        destPrefix;
                            /* From RFC -
                             * For destination type as network it is their IP6
                             * address, for others it is their router ID */
    } unDestId;
    tTMO_SLL            pathLst;
                            /* Implementation -
                             * List of paths. Each node in the list indicates
                             * the path through an area */
    tTMO_SLL            lsaLst;
                            /* List of the intra area prefix LSAs which 
                               are describing this prefix */
    tV3OsLinkStateId    linkStateId;
                            /* Indicates the link state Id of the summary 
                             * LSA being generated corresponding to this 
                             * route entry */
    tV3OsOptions        options;
                            /* From RFC -
                             * If the destination is a router this field
                             * specifies the optional capabilities of the
                             * router */
    UINT1               u1DestType;
                            /* From RFC -
                             * Destination type of the route entry. This can be
                             * network type or Area Border Router or As
                             * Boundary Router */
    UINT1               u1PrefixLen;
                            /* Prefix length of this IPv6 route entry */
    UINT1               u1Flag;
                            /* Which indicates whether the route is modified 
                             * or not */
    UINT1               u1RtType;
    UINT1               u1IsRmapAffected;
                            /* Mark whether the route entry is dropped by
                             * Routemap rules */
#define destRtRtrId  unDestId.destRtrId
#define destRtPrefix unDestId.destPrefix
} tV3OsRtEntry ;


/* Structure to hold OSPFv3 routing information */
typedef struct      _V3OspfRt {
    VOID               *pOspfV3Root;
                            /* Points to the root of the Trie Instance */
    tTMO_SLL            routesList;
                            /* A singly linked list of routing table entries */
    INT1                i1OspfId;
                            /* Application Identifier */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte alignment */
} tV3OspfRt;


/****************************************************************************/
/*                Route Redistribution Related type definitions             */
/****************************************************************************/

/*
 * Structure describing the configuration of Metric Cost and Route Type
 * information to be applied to the routes learnt from the RTM
 */
typedef struct _V3OsRedistrConfigRouteInfo {

    tTMO_SLL_NODE       nextRrdConfigRoute;
                            /* next node */
    tIp6Addr            rrdConfigPrefix;
                            /* Prefix value of the configuration record */
    UINT4               u4RrdRouteMetricValue;
                            /* Configured metric value */
    UINT4               u4ManualTagValue;
                            /* Configured Tag Value */
    tV3OsRowStatus      rrdConfigRecStatus;
                            /* This field indicates the row status */
    UINT1               u1PrefixLength;
                            /* Prefix length of the Config record 
                             * external route */
    UINT1               u1RrdRouteMetricType;
                            /* Route type. By defualt it is AS External 
                             * type2 */
    UINT1               u1RedistrTagType;
                            /* Tag Type by default it is Manual Tag type */
} tV3OsRedistrConfigRouteInfo;

typedef struct _V3OsFwdAddr {
    tTMO_SLL_NODE        nextFwdAddr;
    tIp6Addr             fwdAddr;
} tV3OsFwdAddr;

/****************************************************************************/
/* Strucuture for HA support                                                */
/****************************************************************************/

typedef struct _V3OsBulkNbrInfo
{
    tIp6Addr            nbrIpv6Addr;
                           /* Address of the Neighbor */
    UINT4               u4NbrRtrId;
                           /* Neighbor router id */
    INT4                i4NbrIfIndex;
                           /* The interface index through which
                            * the neighbor is reached
                            */
    UINT1               u1ConfigStatus;
                           /* Type of Neighbor
                            * OSPFV3_DISCOVERED_NBR  1
                            * OSPFV3_CONFIGURED_NBR  2
                            * OSPFV3_VIRTUAL_NBR     3
                            */
    UINT1               au1Reserved [3];
}tV3OsBulkNbrInfo;

typedef struct _V3OsBulkLsuInfo
{
    tV3OsLsaId          lsaId;
                           /* Link state Index for each LSA */
    tV3OsAreaId         areaId;
                           /* Area Id for Area scope LSA */
}tV3OsBulkLsuInfo;

typedef struct  __V3OsRedInfo  {
    tV3OsBulkLsuInfo    lastLsaInfo;
                           /* Information about the last updated lsa info
                            * to the standby node
                            */
    tV3OsAreaId         transitAreaId;
                           /* Transit area id of the last sent virtual link */
    tV3OsRouterId       destRouterId;
                           /* Last sent virtual neighbor id */
    INT4                i4HsAdminStatus;
                           /* Admin status to indicate whether the module is
                            * registered to RM or not
                            */
    UINT4               u4LastIfIndex;
                           /* Information about the last inteface for 
                            * which nbr hello and nbr state are synced
                            * to the standby node
                            */
    UINT4               u4LastCxtId;
                           /* Information about the Last context added */
    UINT4               u4HelloSynCount;
                           /* Statistics about the no of hellos synced */
    UINT4               u4LsaSyncCount;
                           /* Statistics about the no of LSU synced */
    UINT4               u4RmState;
                           /* Current RM state */
    UINT4               u4PrevRmState;
                           /* Previous RM state */
    UINT4               u4PeerCount;
                           /* No of standby nodes */
    UINT4               u4DynBulkUpdatStatus;
                           /* This variable tells which sub bulk update is
                            * in progress. This is used to call the
                            * corresponding function when sub bulk update
                            * event is received
                            * OSPFV3_BULK_UPDT_NOT_STARTED  -  Bulk update is not started
                            * OSPFV3_BULK_UPDT_INTF         -  Interface Bulk in progress
                            * OSPFV3_BULK_UPDT_HELLO        -  Hello Bulk update in progress 
                            * OSPFV3_BULK_UPDT_LSU          -  LSU Bulk update in progress
                            * OSPFV3_BULK_UPDT_NBR_STATE    -  Nbr State Bulk update in progress
                            * OSPFV3_BULK_UPDT_COMPLETED    - Bulk update is completed
                            */
    UINT4               u4CurrentBulkStartTime;
                           /* This contains the system up time. This will be
                            * updated during the starting of bulk update and
                            * during the start of sub bulk update process event
                            * After sending some of the bulk updates, the
                            * system up time will be checked with this variable
                            * If the difference is greater than delta, then
                            * bulk update process will be relinquished.
                            */

    BOOL1            b1IsBulkReqRcvd; /* This flag will be set to
                                    * OSPFV3_TRUE if bulk request
                                    * is received before getting
                                    * the RM_STANDBY_UP event.
                                   */
    UINT1            u1Rsvd[3];

} tV3OsRedInfo;

typedef struct _V3OsRmNbrState
{
    /* If this structure is modified, then OSPFV3_FIXED_NBR_INFO_SIZE macro
     * should be modified
     */
    tV3OsAreaId         transitAreaId;
                           /* Transit area id for virtual links */
    tV3OsRouterId       destRtrId;
                           /* Virtual nbr router id */
    tV3OsRouterId       nbrId;
                           /* Nbr router id */
    UINT4               u4IfIndex;
                           /* The interface index through which
                            * the neighbor is reached
                            */
    UINT4               u4ContextId;
                           /* Context id used for virtual links */
    UINT1               u1CurrState;
                           /* Current Neighbor state machine */
    UINT1               u1NetworkType;
                           /* Underlying network type */
    UINT1               au1Reserved[2];
}tV3OsRmNbrState;

typedef struct _V3OsRmLsdbInfo
{
    tV3OsRouterId       nbrRtrId;
                           /* Neighbor router-id from whom dynamic LSU
                            * is received
                            */
    tV3OsAreaId         transitAreaId;
                           /* Transit area Id for virtual links */
    tV3OsRouterId       destRtrId;
                           /* Destination router id for virtual links */
    UINT1              *pu1Lsa;
    UINT1              *pAssoPrimitive;
                           /* Asso primitive used for LSA Desc */
    UINT4               u4LsaArrivalTime;
                           /* The LSU arrival time */
    UINT4               u4ContextId;
                           /* The context to which the LSU belongs */
    UINT4               u4StorageId;
                           /* Indicates where LSA is to be added
                            * Link scope    -   Interface Id
                            * Area scope    -   Area Id
                            * AS scope      -   unused
                            */
    UINT2               u2LsaAge;
                           /* The current LSA age to be sent during bulk
                            * updates. In case of dynamic updates, no need
                            * to send the LSA age as LSU will be sent as soon
                            * as it is received
                            */ 
    UINT2               u2LsaLen;
                           /* PDU length */
    UINT2               u2InternalLsaType;
                           /* Internal LSA type */
    UINT1               u1RxFlag;
                           /* Flag to indicate whether LSA is present
                            * in any rxmt list
                            */
    UINT1               u1FloodScope;
                           /* Flood scope of LSA - Link / Area / AS */
    UINT1               u1FnEqvlFlag;
                           /* Functional equivalent for AS and NSSA lsa */
    UINT1               u1NetworkType;
                           /* Network type */
    UINT1               u1OverflowState;
                           /* Overflow state */
    UINT1               au1Rsvd [1];
                           /* 4-byte alignment */
} tV3OsRmLsdbInfo;

typedef struct _V3OsRmLsAckDetails
{
    UINT4               u4LsId;
                           /* Link State Id */
    UINT4               u4StorageId;
                           /* Indicates where LSA is to be added
                            * Link scope    -   Interface Id
                            * Area scope    -   Area Id
                            * AS scope      -   unused
                            */
    UINT4               u4AdvRtrId;
                           /* Advertising router id */
    UINT4               u4CxtId;
                           /* Context Id */
    UINT2               u2LsType;
                           /* LSA Type */
    UINT1               au1Reserved [2];
}tV3OsRmLsAckDetails;


/****************************************************************************/
/* RTM ROUTES STRUCTURE                                                     */
/****************************************************************************/

typedef struct _V3OspfRtmNode
{
    tTMO_SLL_NODE       nextRtmNode;
                           /* Pointer to the next RTM information */
    tRtm6RegnAckMsg     RegnAckMsg;
                           /* Information regarding the registration with
                            * RTM6
                            */
    tNetIpv6RtInfo      RtInfo;
                           /* RTM route info */
    UINT1               u1MessageType;
                           /* RTM message type */
    UINT1               au1Rsvd [3];
                           /* 4 byte alignment */
}tV3OspfRtmNode;

/****************************************************************************/
/* CONTEXT STRUCTRE                                                         */
/****************************************************************************/

typedef struct _V3OspfCxt {
    tV3OsRouterId       rtrId;
                            /* From RFC -
                             * A 32-bit number that uniquely identifies the
                             * context */
    tV3OsRouterId       ConfRtrId;
    VOID               *pExtRtRoot;
                            /* Trie Root for storing external routes */
    tV3OsArea          *pBackbone;
                            /* Pointer to the backbone area structure */
    tV3OsCandteLst     *pCandteLst;
                            /* The hash table structure that is used to
                             * maintain the candidate list structure during
                             * the routing table calculation.  */
    tFilteringRMap     *pDistributeInFilterRMap;
                             /* Config for Distribute in filtering feature */
    tFilteringRMap     *pDistanceFilterRMap;
                             /* Config for Distance filtering feature */
    tTMO_HASH_TABLE    *pExtLsaHashTable;
                            /* From RFC -
                             * A Hash Table of AS External LSAs generated
                             * by ASBR routers. The Index to the hash table
                             * is IP6 Address Prefix and Prefix length.  */
    tRBTree             pAsScopeLsaRBRoot;
                            /* Pointer to RB tree Root. RB tree maintain all 
                             * AS scope LSA.  */
    tRBTree             pHostRBRoot;
                            /* Implementation -
                             * Root of the RB tree storing hosts directly 
                             * attached to this instance */
    tRBTree             IfLeakRouteRBTree; /*RBTree to store Interface Leak Route Status*/    
    tTMO_DLL            lsaDescLst;
                            /* List of LSA Descriptors containing the self
                             * originated LSA
                             */
    tTMO_SLL            areasLst;
                            /* From RFC -
                             * Contains the list of attached area structures,
                             * which is maintained as a singly linked list.
                             * This list is maintained in the increasing order
                             * of area id. This list contains the backbone
                             * structure also if the router is attached to the
                             * backbone */
    tTMO_SLL            fwdAddrLst;
                            /* It is a list of Forwarding Address for which
                             * AS-Extrenal LSAs or Type 7 NSSA LSAs are 
                             * originated */
    tTMO_SLL            asExtAddrRangeLst; 
                            /* Implementation -
                             * List of all AS external address range structures.
                             */   
    tTMO_SLL            virtIfLst;
                            /* Implementation -
                             * List of all configured virtual interfaces, which
                             * is maintained in the order of Area ID and
                             * Neighbor ID.  */
    tTMO_SLL            sortNbrLst;
                            /* List storing the neighbors in sorted order */
    tTMO_SLL            redistrRouteConfigLst;
                            /* The SLL that specifies the OSPF metric &
                             * metric type for the redistributed routes */
    tTMO_DLL_NODE       nextVrfSpfNode;
                            /* Pointer to the next context node waiting for
                             * route calculation
                             */
    tV3OspfRt           ospfV3RtTable;
                            /* Pointer to the OSPF routing table of 
                             * this router */
    tV3OspfTimer        runRtTimer;
                            /* Whenever this timer fires the flags
                             * 'rtr_network_lsa_changed' and
                             * 'as_ext_lsa_change' are checked. If any of
                             * these flags are set, the routing table has to
                             * be recalculated */
    tV3OspfTimer        exitOverflowTimer;
                            /* The timer interval for which the router stays
                             * in the overflow state. */
    tV3OspfTimer        graceTimer;
                            /* Grace Timer */
    UINT4               u4ContextId;
                            /* The unique identifer for this instance */
    UINT4               u4XchgOrLoadNbrCount;
                            /* Implementation -
                             * The number of neighboring routers in this area,
                             * which are in state exchange or loading. This is
                             * used while flushing MAX_AGE advertisements from
                             * the database. Such advertisements can be flushed
                             * only when this count is '0' */
    UINT4               u4OriginateNewLsa;
                            /* From MIB -
                             * The number of LSAs originated by this instance */
    UINT4               u4RcvNewLsa;
                            /* From MIB -
                             * The number of LSAs received that have been
                             * determined to be newer originations */
    UINT4               u4NssaAreaCount; 
                            /* Number of NSSA areas associated with the router.
                             */
    UINT4               u4AsScopeLsaChksumSum;
                            /* From MIB -
                             * Sum of the checksums of the external LSAs */
    UINT4               u4AsExtLsaCount;
                            /* From MIB - 
                             * Total number of AS external LSA. */
    UINT4               u4AsScopeLsaCount;
                            /* From MIB - 
                             * Total number of AS Scope LSA. */
    UINT4               u4TraceValue;
                            /* Value of the trace level */
    UINT4               u4ExtTraceValue;
                            /* Extended trace level */
#ifdef TRACE_WANTED
#endif
    UINT4               u4ExitOverflowInterval;
                            /* The timer interval for which the
                             * exitOverflowTimer is run. */
    UINT4               u4RefBw;
                            /* From MIB -
                             * Reference bandwidth in kilobits/second for
                             * calculating default interface metrics */
    UINT4               u4SpfInterval;
                            /* Implementation - SPF interval */ 
    UINT4               u4SpfHoldTimeInterval;
                            /* Interval between successive SPF route 
                             * calculations */
    UINT4               u4ActiveAreaCount;
                            /* Varibale to indicate number of configured
                             * Areas.  */
    UINT4               u4IfCount;
    UINT4               u4AsExtLsaCounter;
                            /* Counter used to set the link state Id of 
                             * the AS external LSAs */
    UINT4               u4InterAreaLsaCounter;
                            /* Counter used to set the link state Id of 
                             * the Inter-Area Prefix and Inter-Area 
                             * Router LSAs */
    UINT4               u4NonDefAsLsaCount;
                            /* Indicates the number of non default External
                             * LSAs in the database */
    UINT4               u4LastCalTime;
                            /* Last route calculation time */
    UINT4               u4StaggeringDelta;
                            /* This will denote the time when current route
                             * calculation should be relinquished */
    UINT4               u4RTStaggeringInterval;
                            /* Relinquish Interval, equal HelloInterval by
                             * defaul(10 sec.) */
    UINT4               u4RouteCalcCompleted;
                            /* Current calc state: in_process/finished
                             */
    UINT4               u4OspfPktsDisd;
    UINT4               u4TrapControl;
                           /* Bit map used to selectively enable the required
                            * TRAPS. When a particular bit is set, then the
                            * corresponding Trap is enabled. */
    UINT4               u4GracePeriod;
                            /* Maximum restart interval */
    UINT4               u4IsGraceAckReq;
                            /* For Grace Restart Ack is required from
                             * helper or not
                             */
    UINT4               u4HelperGrTimeLimit;
                            /* Grace Time limit supported by helper */
    UINT4               au4MetricValue[OSPFV3_MAX_PROTO_REDISTRUTE_SIZE];
    UINT4               au4MetricType[OSPFV3_MAX_PROTO_REDISTRUTE_SIZE];

    INT4                i4CliHandle;
    INT4                i4ExtLsdbLimit;
                            /* Indicates the max no. of ASE lsas that can
                             * reside in the database */
    UINT4               u4IfDownIndex;
#ifdef RRD_WANTED
    UINT4               u4RrdSrcProtoBitMask;
                            /* Bit Mask of source protocols from which
                             * redistribution is enabled. Used when  sending
                             * redistribution enable/disable message to RTM
                             */
    tV3OspfStatus       redistrAdmnStatus;
                            /* Indicates whether route distribution in OSPF
                             * is enabled or not. */
    UINT1               au1Rsvd1 [3];
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
                           /* Indicates the Route Map Name using which
                            * Route Redistribution will occur */
#endif
    tV3OsTruthValue     bOverflowState;
                            /* Indicates whether the router is in overflow
                             * state or not.  */
    tV3OsTruthValue     bDefaultAseLsaPresenc;
                            /* Indicates whether the router has default ASE
                             * LSA or not.  */
    tV3OsTruthValue     bDemandExtension;
                            /* Indicates whether the router supports Demand
                             * Circuit extensions or not. OSPF_TRUE when
                             * support is provided, otherwise OSPF_FALSE.
                             * Default value is OSPF_TRUE.  */

    tV3OsTruthValue     bDefaultPassiveInterface; 
                            /* When set to true all OSPF interfaces created
                             * after this setting will be passive interfaces.
                             * If set to false OSPF interfaces created after
                             * this setting will not be passive. */
    tV3OsTruthValue     bAreaBdrRtr;
                            /* From MIB -
                             * If this field is TRUE then this router is an area
                             * border router. This field is FALSE otherwise
                             */
    tV3OsTruthValue     bAsBdrRtr;
                            /* From MIB -
                             * If this field is TRUE then this router is an AS
                             * boundary router. This field is FALSE otherwise
                             */
    tV3OsTruthValue     bGenType5Flag;
                            /* Variable indicates whether Type5 aggregated 
                             * or non-aggregated LSAs are generated or 
                             * flushed out. */
    tV3OsTruthValue     bNssaAsbrDefRtTrans;
                            /* Flag to indicate whether the router is 
                             * route translator by default or not */
    tV3OsTruthValue     bVrfSpfTmrStatus;
                            /* TRUE  - If the context is added in the DLL
                             *         for route calculation
                             * FALSE - If the context is not added in the DLL
                             *         for route calculation
                             */
    tV3OspfStatus       admnStat;
                            /* From MIB -
                             * The administrative status of the instance.
                             * If this is set to ENABLED then the router takes
                             * part in OSPF routing, otherwise this field
                             * is set to DISABLED
                             */
    UINT1               au1Rsvd2 [3];
                           /* 4-byte alignment */
    tV3OsOptions        rtrOptions;                             
                            /* Indicates the optional capabilities supported
                             * by the router. */
    tV3OsRowStatus      contextStatus;
                            /* MIB Row status used for context creation,
                             * modification and deletion
                             */
    UINT1               u1ABRType;
                            /* Type of the ABR supported by Router.
                             * 1. Standard ABR.
                             * 2, Cisco Type ABR.
                             * 3. IBM Type ABR. */

    UINT1               u1RtrNetworkLsaChanged;
                            /* Whenever a router LSA or network LSA in the
                             * database changes, this flag is set to indicate
                             * that the routing table has to be recalculated
                             */
    UINT1               u1AsbSummaryLsaChanged;
                            /* Whenever an ASBR LSA in the database changes,
                             * this flag is set to indicate that all the
                             * external routes in the routing table has to be
                             * recalculated */
    UINT1               u1RtrDisableInProgress;
    UINT1               u1VirtRtCal;
    UINT1               u1Distance; /* Distance value for OSPFv3 routes */
    UINT1               u1GrLsaMaxTxCount;
                            /* Grace LSA Maximum transmit count */
    UINT1               u1RestartReason;
                            /* Reason for Graceful restart */
    UINT1               u1RestartSupport;
                            /* Graceful restart support for this router */
    UINT1               u1RestartStatus;
                            /* Current status of Graceful restart */
    UINT1               u1HelperSupport;
                            /* Helper Support for this router */
    UINT1               u1RestartExitReason;
                            /* Exit reason for last graceful restart process */
    UINT1               u1StrictLsaCheck;
                            /* Strict LSA check option */
    UINT1               u1Ospfv3RestartState;
                            /* Indicates whether restart process is started
                             * or not
                             * Flag takes the following values
                             * OSPFV3_GR_SHUTDOWN(1) - Ospf is undergoing
                             *                         planned GR shutdown
                             * OSPFV3_GR_RESTART(2) - Ospf is GR -restart phase
                             * OSPFV3_GR_NONE(0) - Ospf is not in GR state
                             */
    UINT1               u1PrevOspfv3RestartState;
                            /* Indicates the previous state of variable
                             * u1Ospfv3RestartState
                             */
    UINT1               u1TrapCount;
                            /* Number of traps generated so far in the current
                             * sliding window period. */
    UINT1               u1IsOspfEnabled;
                            /* Used to check whether OSPF is enabled or not
                             * through MSR restoration during standby boot-up
                             */
    UINT1               u1GraceLsaTxCount;
                            /* Number of grace LSA sent.  */
    UINT1               u1HelperStatus;
                            /* Acting as Helper or not */

    UINT1               u1BfdAdminStatus; /*To check BFD status globally */
    UINT1               u1BfdStatusInAllIf; /*To check BFD status in all interfaces */
    UINT1               u1RouterIdStatus; /* To check if router-id is configured or dynamic */
    UINT1               u1RouteLeakStatus;
    UINT1               u1IfRouteLeakStatus;
    UINT1               au1Pad[3];

} tV3OspfCxt;

/****************************************************************************/
/* ROUTER                                                                   */
/****************************************************************************/

typedef struct _V3OsRtr {
    tRBTree             pIfRBRoot;
                            /* Root of the RB tree used to store OSPFv3 
                             * interfaces
                             */
    tRBTree             pRtrLsaCheck;
                            /* Temporarily maintains router LSA link
                             * information
                             */
    tRBTree             pNwLsaCheck;
                            /* Temporarily maintains network LSA
                             * link information
                             */
    tRBTree             pGRLsIdInfo;
                            /* Temporarily maintains Link State 
                             * Id information */
    tTMO_DLL            vrfSpfList;
                            /* The DLL of context waiting for route
                             * calculation.
                             */
    tTMO_SLL            rtmRoutesLst;
                            /* From RFC -
                             * All routes to destinations external to the AS are
                             * maintained in AS boundary routers as a singly
                             * linked list */
    tV3OsIsmSchedQueue  ismSchedQueue;
                            /* Implementation -
                             * The queue that contains the events for which the
                             * ISM is scheduled */
    tV3OsBufQid         bufQid;
                            /* Holds the mem pool Ids. */
    tTimerListId        timerLstId;
                            /* Used to store the Id of the timers list 
                             * being used by OSPFv3 */
    tTimerListId        hellotimerLstId;
                            /* Used to store the Id of the timers list 
                             * being used by OSPFv3 */
    tV3OspfTimer        vrfSpfTimer;
                            /* The timer interval to delay the route
                             * calculation in the instance
                             */
                                 
    tTimerListId       g50msTimerLst;
    tV3OspfTimer        g50msTimerNode;
    tV3OspfCxt         *apV3OspfCxt [SYS_DEF_MAX_NUM_CONTEXTS];
                            /* Pointer to array of OSPFv3 instances */
    tV3OspfCxt         *pV3OspfCxt;
                            /* The current context being modified.
                             * This is used for management purpose
                             * when configuring through MI routines
                             */
    tV3OsTimerDesc      aTimerDesc[OSPFV3_MAX_TIMERS];
                            /* Array holding the description of the timers */
    tV3OsArea          *pAggrTableCache;
                            /* last item accessed by AggregationTable
                             * GET_NEXT */
    tV3OsRedistrConfigRouteInfo *pRedistCfgTableCache;
                            /* last item accessed by
                             * RedistRouteCfgTable GET_NEXT */
    tV3OsRedInfo        ospfRedInfo;
                            /* Redundancy related Information */
    UINT1               au1Pkt[OSPFV3_MAX_HELLO_PKT_LEN];
                            /* Hello packet used as temporarary buffer while
                             * processing the hello packet in standby node
                             */
    tTMO_SLL_NODE      *pNbrTableCache;
                            /* last item accessed by NbrTable GET_NEXT */
    tTMO_SLL_NODE      *pExtAggrTableCache;
                            /* last item accessed by
                             * ExternalAggregationTable GET_NEXT */
    tOsixSemId          RtmLstSemId;
    UINT4               u4VrfSpfInterval;
                            /* The delay to be introduced between the
                             * route calculation in successive context
                             */

    UINT4               u4GblTrcValue;
                            /* Value of the trace level */
#ifdef TRACE_WANTED
#endif
    UINT4               u4RTStaggeringStatus;
                            /* Provision to enable/disable OSPFv3
                             * CPU-Relinquish functionality at run-time with
                             * default value as enabled.
                             */
    UINT4               u4RTStaggeredCtxId;
                            /* The current context that is in relinquished
                             * state. If this points to invalid context id,
                             * then none of the context is in relinquished
                             * state
                             */
    UINT4               u4ActiveCxtCount;
                            /* Number of active context present in the router
                             * This is used for creation and deletion of raw
                             * socket
                             */
    UINT4               u4LastRtCalcTime;
                            /* This maintains the last route calculation
                             * completion time among all the context. This
                             * is used to provide a delay between route
                             * calculation of successive context
                             */
#ifdef RAWSOCK_WANTED
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
    INT4                ai4RawSockId[SYS_DEF_MAX_NUM_CONTEXTS];
#else
    INT4                i4RawSockId;
#endif
                            /* Raw socket Descriptor */
#endif

#ifdef SYSLOG_WANTED
    UINT4                u4SysLogId;
                            /* Registration Id for OSPFV3 in SysLog */
#endif
                              
    tV3OsTruthValue     bIgnoreExpiredTimers;
                            /* When set to true, expired timers will be
                             * processed, but no call back function will 
                             * be called in TmrHandleExpiry
                             */
    UINT1               au1Rsvd [3];
                            /* 4-byte alignment */
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
    INT4                i4NLHelloSockId;
#endif
#endif
    tOsixSemId          MulExclSemId;
    tOsixTaskId         Ospf3TaskId;
    tOsixQId            Ospf3QId;
    tOsixQId            Ospf3RmQId;
    tOsixQId            Ospf3LnxQId;
    tOsixQId            Ospf3LPQId;
    tOsixQId            Ospf3NPHelloQId;
    UINT4               u4AckOrUpdatePktSentCt;

} tV3OsRtr;


/****************************************************************************/
/* ROUTING TABLE CALCULATION                                                */
/****************************************************************************/

typedef struct _V3OsCandteNextHop {
    tV3OsInterface     *pInterface;
                            /* Pointer to the interface */
    tIp6Addr            nbrIpv6Addr;
                            /* Neighbor IP6 address */
} tV3OsCandteNextHop;


/* Vertex ID structure -- Ref: RFC 2740 -- Section 3.8.1 */
typedef struct _V3OsVertexId {
    tV3OsRouterId       vertexRtrId;
                            /* For 
                             * Vertex Router  -- Router ID of the OSPFv3 Router
                             * Vertex Network -- Router ID of the DR router
                             */
    UINT4               u4VertexIfId;
                            /* For 
                             * Vertex Router  -- Not used
                             * Vertex Network -- Interface ID of the DR
                             */
} tV3OsVertexId;


/* ------------------------------------------------------- */
/* OSPFV3 Router/Intra-Area prefix LSA Hash node structure */
/* ------------------------------------------------------- */
typedef struct _V3OsDbNode {
    tTMO_HASH_NODE      NextDbNode;   
                            /* Points to next DB hash node   */
    tTMO_SLL            lsaLst;  
                            /* List holding all Router or Intra-
                             * Area prefix LSAs generated by
                             * the router */
    UINT1               u1Flag;
    UINT1               au1Rsvd[3];
} tV3OsDbNode;


typedef struct _V3OsCandteNode {
    tRBNodeEmbd         RbNode;
                            /* Embedded RB Tree node for SPF nodes */
    tTMO_HASH_NODE      candteHashNode;
                            /* Node associated with the candidate */
    tV3OsVertexId       vertexId;
                            /* Vertex id associated with the candidate */
    tV3OsCandteNextHop  aNextHops[OSPFV3_MAX_NEXT_HOPS];
                            /* Array of next hops associated with the 
                             * candidate */
    union {
        tV3OsDbNode    *pV3OsDbNode;
                            /* Pointer to Hash table node of Router LSA 
                             * hash table in case of vertex router */
        tV3OsLsaInfo   *pLsaInfo;
                            /* Pointer to network LSA in case of 
                             * vertex network */  
    } unNodeInfo;
    UINT4               u4Cost;
                            /* Cost derived from the Router LSA */
    UINT1               u1VertType;
                            /* Vertex Type  - Value
                             * --------------------
                             * Router       -  1
                             * Network      -  0 */
    UINT1               u1HopCount;
                            /* The number of next hops in this path */
    UINT1               u1Flag;
                            /* Indicates whether the node is 
                             * newlt created, modified or not found.
                             * This field is used in intra-area route 
                             * calculations.
                             */
    UINT1               u1Options;
                            /* Indicating the optional capabilities 
                             * of the router corresponding to the router */
    UINT1               u1VirtReachable;
                            /* Indicates whether the candidate node is reachable 
                             * through virtual link or not */
    UINT1               au1Rsvd[3];
                           /* Added for 4-byte alignment */
#define pDbNode  unNodeInfo.pV3OsDbNode
#define pOsLsaInfo unNodeInfo.pLsaInfo
} tV3OsCandteNode;


/* structure used to store links obtained from Router or Network LSA
 * during calculation of intra area routes */
typedef struct _V3OsLinkNode {
    UINT4               u4InterfaceId;
                            /* Interface ID assigned to the interface 
                             * being described */
    UINT4               u4NbrIfId;
                            /* Neighbor interface Id if link is PTOP
                             * DR interface Id if MULTIACCESS */
    tV3OsRouterId       nbrRtrId;
                            /* Neighbor Routers router Id if interface is PTOP
                             * DR router id if the interface is MULTIACCESS */
    UINT2               u2LinkMetric;
                            /* Indicates the cost of using this outbound
                             * router link for traffic of the specified */
    UINT1               u1LinkType;
                            /* Indicates the type of link by which the router
                             * is connected to another router/network */
    UINT1               u1Rsvd;
                            /* Included for 4-byte alignment */
} tV3OsLinkNode;

/* structure used to store links obtained from AS external LSA
 * during calculation of ASE route */
typedef struct _V3OsExtLsaLink {
    tIp6Addr            fwdAddr;
                            /* Indicates the address to which the data
                             * destined for the advertised destination will
                             * be forwarded */
    tV3OsMetric         metric;
                            /* Contains metric and metric type of the 
                             * external LSA */
    tV3OsPrefixInfo     extRtPrefix;
                            /* Contains the prefix information described 
                             * by AS external LSA */
    UINT4               u4ExtRouteTag;
                            /* This 32-bit field has the value attached to
                             * each external route */
    UINT1               u1FieldBits;
                            /* Store the contents of the Field bits */
    UINT1               au1Rsvd[3];
                            /* Added for 4-byte alignment */
} tV3OsExtLsaLink;

/****************************************************************************/
/* EXTERNAL ROUTES configured                                               */
/****************************************************************************/

typedef struct _V3OsAsExtAddrRange  {
    tTMO_SLL_NODE       nextAsExtAddrRange;
                            /* Points to the next external route in the
                             * singly linked list. */
    tTMO_SLL_NODE       nextAsExtAddrRngInRtr;
                            /* Pointer to next address range in the 
                             * global Router Data Structure. */
    tTMO_SLL            extRtLst;
                            /* List of External Routes subsumed to 
                             * this address range. */
    tV3OsLinkStateId    linkStateId;
                            /* Indicates the link state Id of the LSA
                             * being generated for this address range */
    tV3OsAreaId         areaId;
                            /* Area Identifier */
    tV3OsMetric         metric;
                            /* Metric value of the address range */
    tIp6Addr            ip6Addr;
                            /* Contains the IPv6 address. This IPv6 
                             * address and the prefix length together, 
                             * specify a range of addresses. */
    UINT1               u1PrefixLength;
                            /* This prefix length and IPv6 address 
                             * together specify a range of address. */
    UINT1               u1AggTranslation;
                            /* This is required for NSSA Translation. */
    UINT1               u1RangeEffect;
                            /* This variable for Aggregation Effect. 
                             * It performs aggregation according to 
                             * the values advertise, doNotAdvertise,
                             * allowAll or denyAll */
    UINT1               u1AttrMask;
                            /* Used for managing the RowStatus 
                             * manipulation */
    tV3OsRowStatus      rangeStatus;
                            /* Indicates the row status of the address 
                             * range */
    UINT1               au1Rsvd[3];
                            /* Included for 4-byte alignment */
} tV3OsAsExtAddrRange;


/* Structure to hold the external route information */
typedef struct _V3OsExtRoute  {

    tTMO_SLL_NODE       nextExtRouteInRange;
                            /* Pointer to the next External Routes 
                             * falling in this address range.*/
    tV3OsAsExtAddrRange *pAsExtAddrRange;
                            /* Pointer to the address range to which 
                             * this external route belongs */
    tIp6Addr            ip6Prefix;
                            /* From RFC -
                             * Indicates the IP6 address of the network. */
    tV3OsMetric         metric;
                            /* Indicates the metric information of 
                             * External route */
    tIp6Addr            fwdAddr;
                            /* Specifies address of the router to which the
                             * packet is to be sent */
    UINT4               u4FwdIfIndex;
                            /* Indicates the index of the interface to reach the
                             * destination.  */
    UINT4               u4ExtTag;
                            /* The following two fields will have valid only if
                             * this rtr is to forward the pkts
                             * This 32-bit field is attached to each external
                             * route.  */
    tV3OsLinkStateId    linkStateId;
                            /* Indicates the link state Id of the  
                             * External LSA generated for this external 
                             * route */
    tV3OsLsaInfo       *pLsaInfo;
                            /* Pointer to AS external LSA. */
    UINT2               u2SrcProto;
                            /* Source of the route. can be
                             * local (2), * static (3), * RIP (8), * BGP (14) */
    UINT1               u1PrefixLength;
                            /* Indicates the prefix length of the network.  */
   tV3OsRowStatus       rowStatus;
    UINT1               u1Level; /* ISIS Level-1 or ISIS Level-2*/
    UINT1               au1Pad[3];
} tV3OsExtRoute;

typedef struct _V3OspfLPIp6Pkt {
    UINT4               u4IfIndex;
                            /* Interface index of the interface on which 
                             * this packet is received */
    UINT4               u4PktLen; 
                            /* Length of the received packet */
    tCRU_BUF_CHAIN_HEADER  *pChainBuf; 
                            /* Pointer to CRU buffer if the received 
                             * packet is in the form of CRU buffer */
    UINT1              *pLinearBuf;
                            /* Pointer to linear buffer if the received 
                             * packet is in the form of linear buffer */
    tV3OsNeighbor      *pNbr;
} tV3OspfLPIp6Pkt;

typedef struct _V3OspfIp6Pkt {
    UINT4               u4IfIndex;
                            /* Interface index of the interface on which 
                             * this packet is received */
    UINT4               u4PktLen; 
                            /* Length of the received packet */
    tCRU_BUF_CHAIN_HEADER  *pChainBuf; 
                            /* Pointer to CRU buffer if the received 
                             * packet is in the form of CRU buffer */
    UINT1              *pLinearBuf;
                            /* Pointer to linear buffer if the received 
                             * packet is in the form of linear buffer */
} tV3OspfIp6Pkt;

typedef struct _V3OsRxmtNode {
    tTMO_SLL_NODE       nextLsaInfo;

    tV3OsLsaInfo       *pLsaInfo;

} tV3OsRxmtNode;

typedef struct _V3OsLsaNode {
    tTMO_SLL_NODE       nextLsaInfo;

    tV3OsLsaInfo       *pLsaInfo;

} tV3OsLsaNode;

typedef struct _V3OsRmCtrlMsg {
    tRmMsg             *pData;     /* RM message pointer */
    UINT2               u2DataLen; /* Length of RM message */
    UINT1               u1Event;   /* RM event */
    UINT1               au1Pad[1]; /* Reserved for 4-byte align */
} tV3OsRmCtrlMsg;

/* Structure to handle the message from VCM module */
typedef struct _V3OspfVcmInfo {
    UINT4               u4IpIfIndex;
    UINT4               u4VcmCxtId;
    UINT1               u1BitMap;
    UINT1               au1Rsvd[3];
} tV3OspfVcmInfo;

typedef struct _V3OspfLPQMsg {
    UINT4               u4OspfMsgType;
    union
    {
        tV3OspfLPIp6Pkt       ospfIpIfParam; 
                            /* OSPFv3 packet information */
        tNetIpv6IfStatChange   ospfIpIfStatParam; 
                            /* IPv6 interface information */
        tV3OspfVcmInfo  ospfVcmInfo;
                            /* VCM context/interface deletion
                             * indication
                             */
    } unOspfMsgType;
    UINT4                 u4OspfCxtId;
} tV3OspfLPQMsg;

typedef struct _V3OspfQMsg {
    UINT4               u4OspfMsgType;
    union
    {
        tV3OsRmCtrlMsg  ospfv3RmCtrlMsg;
                            /* Structure to store RM message */
        tV3OspfIp6Pkt   ospfIpIfParam; 
                            /* OSPFv3 packet information */
        tNetIpv6AddrChange   ospfIpIfAddrParam; 
                            /* IPv6 address information */
        tNetIpv6IfStatChange   ospfIpIfStatParam; 
                            /* IPv6 interface information */
        tOsixMsg       *pV3OspfRMapParam;
                            /* Route map status notification */
        tV3OspfVcmInfo  ospfVcmInfo;
                            /* VCM context/interface deletion
                             * indication
                             */
#ifdef MBSM_WANTED
        struct MbsmIndication {
            tMbsmProtoMsg mbsmProtoMsg;
        }MbsmCardUpdate;
#endif

#ifdef BFD_WANTED
        tBfdClientNbrIpPathInfo BfdClientNbrIpPathInfo;
#endif

    } unOspfMsgType;
    UINT4                 u4OspfCxtId;
#ifdef BFD_WANTED
#define Ospfv3BfdMsgInfo unOspfMsgType.BfdClientNbrIpPathInfo
#endif

} tV3OspfQMsg;

/* The following structure holds the router LSA related route information */

typedef struct _V3OsRtrLsaRtInfo {
    tRBNodeEmbd         RbNode;
    tV3OsRouterId       nbrRtrId;
    UINT4               u4InterfaceId;
    UINT4               u4NbrIfId;
    UINT2               u2LinkMetric;
    UINT1               u1LinkType;
    UINT1               u1Rsvd;
} tV3OsRtrLsaRtInfo;

/* The following structure holds the network LSA related route information. */

typedef struct _V3OsNwLsaRtInfo {
    tRBNodeEmbd         RbNode;
    UINT4               u4AttachedRtrId;
} tV3OsNwLsaRtInfo;

/* The following structure holds the external LSA related route information. */

typedef struct _V3OsExtLsaRtInfo {
    tIp6Addr            ip6Prefix;
    UINT1               u1PrefixLength;
    UINT1               au1Rsvd[3];
} tV3OsExtLsaRtInfo;

typedef struct _V3OsGRLsIdInfo {
    tRBNodeEmbd         RbNode;
    tIp6Addr            addrPrefix;
                /* IPv6 Address Prefix.  */
    tV3OsLinkStateId    linkStateId;
    UINT2               u2LsaType;
                /* Type of LSA */
    UINT1               u1PrefixLength;
                /* IPv6 Prefix Length.  */
    UINT1       u1Rsvd;
} tV3OsGRLsIdInfo;

typedef struct _V3OsExtLsa {
        UINT1         au1OsExtLsa[MAX_OSPFV3_EXT_LSA_SIZE];
} tV3OsExtLsa;

typedef struct _V3OsIntLsa {
        UINT1         au1OsLsa[MAX_OSPFV3_INT_LSA_SIZE];
} tV3OsIntLsa;

typedef struct _V3OsSockFdParam {
    INT4                    i4SockFd;
} tV3OsVrfSock;

#endif /* _O3TDFS_H */
