/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: o3extn.h,v 1.11 2017/12/26 13:34:24 siva Exp $
 *
 * Description:This file contains all global variables relating to
 *             protocol and the implementation of it.
 *
 *******************************************************************/
#ifndef _O3EXTN_H
#define _O3EXTN_H

PUBLIC tIp6Addr  gV3OsAllSpfRtrs;
PUBLIC tIp6Addr  gV3OsAllDRtrs;
PUBLIC tIp6Addr  gV3OsNullIp6Addr;
PUBLIC tV3OsRouterId gV3OsNullIpAddr;
PUBLIC tIp6Addr  gV3OsDefaultDest;                  
PUBLIC tV3OsRtr gV3OsRtr;
PUBLIC UINT4 gu1O3HelloSwitchOverFlag;
PUBLIC struct TrieAppFns   V3RtcTrieLibFuncs;

/* Global variables which are used for debugging & Trace log */
PUBLIC UINT1 gau1Os3DbgIfType[OSPFV3_MAX_IF_TYPE][20];
PUBLIC UINT1 gau1Os3DbgIfState[OSPFV3_MAX_IF_STATE][20];
PUBLIC UINT1 gau1Os3DbgIfEvent[OSPFV3_MAX_IF_EVENT][30];
PUBLIC UINT1 gau1Os3DbgNbrState[OSPFV3_MAX_NBR_STATE][20];
PUBLIC UINT1 gau1Os3DbgNbrEvent[OSPFV3_MAX_NBR_EVENT][30];
PUBLIC UINT1 gau1Os3DbgNbrType[OSPFV3_MAX_NBR_TYPE][20];
PUBLIC UINT1 gau1Os3DbgTimerId[OSPFV3_MAX_TIMERS][30];
PUBLIC UINT1 gau1Os3DbgLsaType[10][30];
PUBLIC UINT1 gau1Os3DbgPktType[6][30];
PUBLIC UINT1 gau1Os3DbgAbrType[OSPFV3_MAX_AREA_TYPE][20];

/* GR Global Varibales */
PUBLIC UINT4 gu4Os3CsrFlag;
PUBLIC UINT4 gu4Os3RestoreFlag;

/*Clear Global Flag*/
PUBLIC UINT4 gu4ClearFlag;

/* RMap global variable */
PUBLIC tOsixMsg *gV3OsRmapMsg[MAX_OSPFV3_RMAP_MSGS];

/* BFD  global variable */
#ifdef BFD_WANTED
PUBLIC tBfdReqParams     gBfdInParams;
#endif

/*AT global variable*/
PUBLIC UINT4 gu4Os3ATHigSeqNum;


#endif  /* _O3EXTN_H */
