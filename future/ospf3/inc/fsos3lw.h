/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsos3lw.h,v 1.10 2017/12/26 13:34:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3OverFlowState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3TraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3ABRType ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3NssaAsbrDefRtTrans ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3DefaultPassiveInterface ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3SpfDelay ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3SpfHoldTime ARG_LIST((INT4 *));



INT1
nmhGetFutOspfv3RTStaggeringInterval ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RTStaggeringStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RestartStrictLsaChecking ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3HelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3HelperGraceTimeLimit ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RestartAckState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3GraceLsaRetransmitCount ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RestartReason ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3ExtTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3SetTraps ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3HotStandbyAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3HotStandbyState ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3DynamicBulkUpdStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3StandbyHelloSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFutOspfv3StandbyLsaSyncCount ARG_LIST((UINT4 *));

INT1 
nmhGetFutOspfv3BfdStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3BfdAllIfState ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFutOspfv3RouterIdPermanence ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3ClearProcess ARG_LIST((INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3TraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3ABRType ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3NssaAsbrDefRtTrans ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3DefaultPassiveInterface ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3SpfDelay ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3SpfHoldTime ARG_LIST((INT4 ));


INT1
nmhSetFutOspfv3RTStaggeringInterval ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RTStaggeringStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RestartStrictLsaChecking ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3HelperSupport ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3HelperGraceTimeLimit ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RestartAckState ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3GraceLsaRetransmitCount ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RestartReason ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3ExtTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3SetTraps ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3BfdStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3BfdAllIfState ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RouterIdPermanence ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3ClearProcess ARG_LIST((INT4 ));




/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3TraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3ABRType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3NssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3DefaultPassiveInterface ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3SpfDelay ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3SpfHoldTime ARG_LIST((UINT4 *  ,INT4 ));



INT1
nmhTestv2FutOspfv3RTStaggeringInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RTStaggeringStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3HelperSupport ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3HelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RestartAckState ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3GraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RestartReason ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3ExtTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3SetTraps ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3BfdStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3BfdAllIfState ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RouterIdPermanence ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3ClearProcess ARG_LIST((UINT4 *  ,INT4 ));



/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3TraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3ABRType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3NssaAsbrDefRtTrans ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3DefaultPassiveInterface ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3SpfDelay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3SpfHoldTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhDepv2FutOspfv3RTStaggeringInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RTStaggeringStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RestartStrictLsaChecking ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3HelperSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3HelperGraceTimeLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RestartAckState ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3GraceLsaRetransmitCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RestartReason ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3ExtTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3SetTraps ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3BfdStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3BfdAllIfState ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RouterIdPermanence ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3ClearProcess ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));



/* Proto Validate Index Instance for FutOspfv3IfTable. */
INT1
nmhValidateIndexInstanceFutOspfv3IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3IfTable  */

INT1
nmhGetFirstIndexFutOspfv3IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3IfOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfPassive ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfNbrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfAdjCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfHelloRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfHelloTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfHelloDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfDdpRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfDdpTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfDdpDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLrqRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLrqTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLrqDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLsuRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLsuTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLsuDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLakRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLakTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfLakDisd ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFutOspfv3IfLinkLSASuppression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfBfdState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfCryptoAuthType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfCryptoAuthMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3IfAuthTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfAuthRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3IfAuthDisd ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3IfPassive ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutOspfv3IfLinkLSASuppression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutOspfv3IfBfdState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutOspfv3IfCryptoAuthType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutOspfv3IfCryptoAuthMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3IfPassive ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FutOspfv3IfLinkLSASuppression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FutOspfv3IfBfdState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3IfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3IfCryptoAuthMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3RoutingTable. */
INT1
nmhValidateIndexInstanceFutOspfv3RoutingTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3RoutingTable  */

INT1
nmhGetFirstIndexFutOspfv3RoutingTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3RoutingTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3RouteType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFutOspfv3RouteAreaId ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFutOspfv3RouteCost ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFutOspfv3RouteType2Cost ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFutOspfv3RouteInterfaceIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FutOspfv3AsExternalAggregationTable. */
INT1
nmhValidateIndexInstanceFutOspfv3AsExternalAggregationTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3AsExternalAggregationTable  */

INT1
nmhGetFirstIndexFutOspfv3AsExternalAggregationTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3AsExternalAggregationTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3AsExternalAggregationEffect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3AsExternalAggregationTranslation ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3AsExternalAggregationStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3AsExternalAggregationEffect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3AsExternalAggregationTranslation ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3AsExternalAggregationStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3AsExternalAggregationEffect ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3AsExternalAggregationTranslation ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3AsExternalAggregationStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3AsExternalAggregationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3BRRouteTable. */
INT1
nmhValidateIndexInstanceFutOspfv3BRRouteTable ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3BRRouteTable  */

INT1
nmhGetFirstIndexFutOspfv3BRRouteTable ARG_LIST((UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3BRRouteTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3BRRouteType ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFutOspfv3BRRouteAreaId ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFutOspfv3BRRouteCost ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFutOspfv3BRRouteInterfaceIndex ARG_LIST((UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfv3RedistRouteCfgTable. */
INT1
nmhValidateIndexInstanceFutOspfv3RedistRouteCfgTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3RedistRouteCfgTable  */

INT1
nmhGetFirstIndexFutOspfv3RedistRouteCfgTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3RedistRouteCfgTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3RedistRouteMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3RedistRouteMetricType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3RedistRouteTagType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3RedistRouteTag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3RedistRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3RedistRouteMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3RedistRouteMetricType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3RedistRouteTagType ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3RedistRouteTag ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3RedistRouteStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3RedistRouteMetric ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3RedistRouteMetricType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3RedistRouteTagType ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3RedistRouteTag ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3RedistRouteStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3RedistRouteCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3RRDStatus ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RRDSrcProtoMask ARG_LIST((INT4 *));

INT1
nmhGetFutOspfv3RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3RRDStatus ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RRDSrcProtoMask ARG_LIST((INT4 ));

INT1
nmhSetFutOspfv3RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3RRDStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RRDSrcProtoMask ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FutOspfv3RRDRouteMapName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3RRDStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RRDSrcProtoMask ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FutOspfv3RRDRouteMapName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3RRDMetricTable. */
INT1
nmhValidateIndexInstanceFutOspfv3RRDMetricTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3RRDMetricTable  */

INT1
nmhGetFirstIndexFutOspfv3RRDMetricTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3RRDMetricTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3RRDMetricValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFutOspfv3RRDMetricType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3RRDMetricValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFutOspfv3RRDMetricType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3RRDMetricValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3RRDMetricType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3RRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3DistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFutOspfv3DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3DistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFutOspfv3DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFutOspfv3DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFutOspfv3DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3DistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFutOspf3PreferenceValue ARG_LIST((INT4 *));
INT1
nmhSetFutOspf3PreferenceValue ARG_LIST((INT4 ));
INT1
nmhTestv2FutOspf3PreferenceValue ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhDepv2FutOspf3PreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3NeighborBfdTable. */
INT1
nmhValidateIndexInstanceFutOspfv3NeighborBfdTable ARG_LIST((INT4  , UINT4 ));                                      
/* Proto Type for Low Level GET FIRST fn for FutOspfv3NeighborBfdTable  */

INT1
nmhGetFirstIndexFutOspfv3NeighborBfdTable ARG_LIST((INT4 * , UINT4 *));                                            
/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3NeighborBfdTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */                                                           
INT1
nmhGetFutOspfv3NbrBfdState ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FutOspfv3IfAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfv3IfAuthTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3IfAuthTable  */

INT1
nmhGetFirstIndexFutOspfv3IfAuthTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3IfAuthTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3IfAuthKey ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3IfAuthKeyStartAccept ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3IfAuthKeyStartGenerate ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3IfAuthKeyStopGenerate ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3IfAuthKeyStopAccept ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3IfAuthKeyStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3IfAuthKey ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3IfAuthKeyStartAccept ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3IfAuthKeyStartGenerate ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3IfAuthKeyStopGenerate ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3IfAuthKeyStopAccept ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3IfAuthKeyStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3IfAuthKey ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3IfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3IfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3IfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3IfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3IfAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3IfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3VirtIfAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfv3VirtIfAuthTable ARG_LIST((UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3VirtIfAuthTable  */

INT1
nmhGetFirstIndexFutOspfv3VirtIfAuthTable ARG_LIST((UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3VirtIfAuthTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3VirtIfAuthKey ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3VirtIfAuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3VirtIfAuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFutOspfv3VirtIfAuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3VirtIfAuthKey ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3VirtIfAuthKeyStartAccept ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3VirtIfAuthKeyStopAccept ARG_LIST((UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFutOspfv3VirtIfAuthKeyStatus ARG_LIST((UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3VirtIfAuthKey ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FutOspfv3VirtIfAuthKeyStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3VirtIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FutOspfv3VirtIfCryptoAuthTable. */
INT1
nmhValidateIndexInstanceFutOspfv3VirtIfCryptoAuthTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FutOspfv3VirtIfCryptoAuthTable  */

INT1
nmhGetFirstIndexFutOspfv3VirtIfCryptoAuthTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFutOspfv3VirtIfCryptoAuthTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFutOspfv3VirtIfCryptoAuthType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFutOspfv3VirtIfCryptoAuthMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFutOspfv3VirtIfCryptoAuthType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFutOspfv3VirtIfCryptoAuthMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FutOspfv3VirtIfCryptoAuthType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FutOspfv3VirtIfCryptoAuthMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FutOspfv3VirtIfCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
