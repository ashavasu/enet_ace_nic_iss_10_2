/************************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved                               *
* Description:This file contains compile time switch options for       *
*             OSPFv3 protocol.                                         *
************************************************************************/
/**********************************************************************
 * 
 * RAWSOCK_WANTED             -     TO ENABLE RAW SOCKET SUPPORT. IF NOT
 *                                   ENABLED, A FUNCTIONAL INTERFACE WITH
 *                                   THE IPV6 STACK IS ASSUMED.
 **********************************************************************/

#ifndef __O3CFG_H
#define __O3CFG_H

#ifdef BSDCOMP_SLI_WANTED
#define RAWSOCK_WANTED
#endif
/* For windows compilation it is requied to define to macroes */
/* #define IP6_WANTED
  #define WINDOWS_DEBUG */


#endif/* !__O3CFG_H */
