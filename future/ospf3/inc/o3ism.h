/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: o3ism.h,v 1.3 2011/03/18 10:47:25 siva Exp $
 *
 * Description:This file contains constants, typdefinitions
 *             and static varible declaration specific to osism.c
 *
 *******************************************************************/
#ifndef _O3ISM_H
#define _O3ISM_H


/* ISM Action (IA) routine numbers */
#define  OS3_IA0                  0
#define  OS3_IA1                  1
#define  OS3_IA2                  2
#define  OS3_IA3                  3
#define  OS3_IA4                  4
#define  OS3_IA5                  5
#define  OS3_IA6                  6
#define  OSPFV3_MAX_ISM_FUNC  7

/* the interface state machine transition table */

static UINT1  gau1Os3IsmTable[OSPFV3_MAX_IF_EVENT][OSPFV3_MAX_IF_STATE] = {

/*______________________________________________________________________________
       DOWN     LOOPBACK  WAITING    P-TO-P      DR     BACKUP   DROTHER   STANDBY  
________________________________________________________________________________
INTERFACE UP */
  {   OS3_IA1,  OS3_IA0,  OS3_IA0,  OS3_IA0,  OS3_IA0,  OS3_IA0, OS3_IA0,  OS3_IA1},
/*______________________________________________________________________________
WAIT TIMER */
  {   OS3_IA0,  OS3_IA0,  OS3_IA2,  OS3_IA0,  OS3_IA0,  OS3_IA0, OS3_IA0,  OS3_IA0},
/*______________________________________________________________________________
BACKUP SEEN */
  {   OS3_IA0,  OS3_IA0,  OS3_IA2,  OS3_IA0,  OS3_IA0,  OS3_IA0, OS3_IA0,  OS3_IA0},
/*______________________________________________________________________________
NBR CHANGE */
  {   OS3_IA0,  OS3_IA0,  OS3_IA0,  OS3_IA0,  OS3_IA2,  OS3_IA2, OS3_IA2,  OS3_IA0},
/*______________________________________________________________________________
LOOP IND */ 
  {   OS3_IA5,  OS3_IA5,  OS3_IA5,  OS3_IA5,  OS3_IA5,  OS3_IA5, OS3_IA5,  OS3_IA0},
/*______________________________________________________________________________
UNLOOP IND */
  {   OS3_IA0,  OS3_IA4,  OS3_IA0,  OS3_IA0,  OS3_IA0,  OS3_IA0, OS3_IA0,  OS3_IA0},
/*______________________________________________________________________________
INTERFACE DOWN*/
  {   OS3_IA3,  OS3_IA3,  OS3_IA3,  OS3_IA3,  OS3_IA3,  OS3_IA3, OS3_IA3,  OS3_IA3},
/*______________________________________________________________________________
MULTIPLE INTERFACES TO LINK */
  {   OS3_IA0,  OS3_IA0,  OS3_IA6,  OS3_IA0,  OS3_IA6,  OS3_IA6, OS3_IA6,  OS3_IA6},
/*__________________________________________________________________________________
*/
};

/* type of the functions which perform the action in ISM */

typedef  void (*tV3OsIsmFunc)(tV3OsInterface *);

/* array of pointers to functions performing actions during ISM transitions */

tV3OsIsmFunc gau1Os3IsmFunc[OSPFV3_MAX_ISM_FUNC] = {
               /* OS3_IA0 */          V3IsmInvalid,
               /* OS3_IA1 */          V3IsmIfUp,
               /* OS3_IA2 */          V3IsmElectDr,
               /* OS3_IA3 */          V3IsmIfReset,
               /* OS3_IA4 */          V3IsmIfDown,
               /* OS3_IA5 */          V3IsmIfLoop,
               /* OS3_IA6 */          V3IsmElectActive,
               };
               
#endif /* _O3ISM_H */
