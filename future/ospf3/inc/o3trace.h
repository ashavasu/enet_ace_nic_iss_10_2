 /********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3trace.h,v 1.9 2017/12/26 13:34:25 siva Exp $
 *
 * Description:This file contains procedures and definitions
 *             used for debugging.
 *
 *******************************************************************/


#ifndef _O3DEBUG_H
#define _O3DEBUG_H

#define  OSPFV3_SENT           1
#define  OSPFV3_RCVD           2

#ifdef TRACE_WANTED

#define  OSPFV3_TRC_FLAG(CxtId)        V3UtilGetTraceFlagInCxt(CxtId,OSPFV3_CXT_TRACE_TYPE)
#define  OSPFV3_GBL_TRC_FLAG           gV3OsRtr.u4GblTrcValue
#define  OSPFV3_EXT_TRC_FLAG(CxtId)    V3UtilGetTraceFlagInCxt(CxtId,OSPFV3_CXT_EXT_TRACE_TYPE)
#define  OSPFV3_NAME                   (const char *)"OSPFV3"
#define  CXT_TRC(Fmt,CxtId)            "Context : %d : " Fmt, CxtId

#define OSPFV3_PKT_DUMP(pBuf, Length, fmt) \
            MOD_PKT_DUMP(OSPFV3_TRC_FLAG, DUMP_TRC, OSPFV3_NAME, pBuf, Length, fmt)

#define OSPFV3_TRC(Value, CxtId, Fmt)       UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                      Value,        \
                                                      OSPFV3_NAME,        \
                                                      CXT_TRC(Fmt,CxtId))

#define OSPFV3_TRC1(Value, CxtId, Fmt, Arg) UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                      Value,        \
                                                      OSPFV3_NAME,        \
                                                      CXT_TRC(Fmt,CxtId),     \
                                                      Arg)

#define OSPFV3_TRC2(Value, CxtId, Fmt, Arg1, Arg2)                            \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),    \
                                                       Arg1, Arg2)

#define OSPFV3_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)                  \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2, Arg3)

#define OSPFV3_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)            \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2,   \
                                                       Arg3, Arg4)

#define OSPFV3_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)      \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2,        \
                                                       Arg3, Arg4,        \
                                                       Arg5)

#define OSPFV3_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,               \
                                                       OSPFV3_NAME,         \
                                                       CXT_TRC(Fmt,CxtId),  \
                                                       Arg1, Arg2,          \
                                                       Arg3, Arg4,          \
                                                       Arg5, Arg6)

#define OSPFV3_TRC7(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
                                             UtlTrcLog(OSPFV3_TRC_FLAG(CxtId), \
                                                       Value,               \
                                                       OSPFV3_NAME,         \
                                                       CXT_TRC(Fmt,CxtId),  \
                                                       Arg1, Arg2,          \
                                                       Arg3, Arg4,          \
                                                       Arg5, Arg6,          \
                                                       Arg7)

#define OSPFV3_GBL_TRC(Value, Fmt)       UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                   Value,        \
                                                   OSPFV3_NAME,        \
                                                   Fmt)

#define OSPFV3_GBL_TRC1(Value, Fmt, Arg) UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                   Value,        \
                                                   OSPFV3_NAME,        \
                                                   Fmt,          \
                                                   Arg)

#define OSPFV3_GBL_TRC2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2)

#define OSPFV3_GBL_TRC3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3)

#define OSPFV3_GBL_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4)

#define OSPFV3_GBL_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define OSPFV3_GBL_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)

#define OSPFV3_GBL_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
                                      UtlTrcLog(OSPFV3_GBL_TRC_FLAG, \
                                                Value,        \
                                                OSPFV3_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6, Arg7)

#define OSPFV3_EXT_TRC(Value, CxtId, Fmt)       UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                      Value,        \
                                                      OSPFV3_NAME,        \
                                                      CXT_TRC(Fmt,CxtId))

#define OSPFV3_EXT_TRC1(Value, CxtId, Fmt, Arg) UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                      Value,        \
                                                      OSPFV3_NAME,        \
                                                      CXT_TRC(Fmt,CxtId),     \
                                                      Arg)

#define OSPFV3_EXT_TRC2(Value, CxtId, Fmt, Arg1, Arg2)                            \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),    \
                                                       Arg1, Arg2)

#define OSPFV3_EXT_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)                  \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2, Arg3)

#define OSPFV3_EXT_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)            \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2,   \
                                                       Arg3, Arg4)

#define OSPFV3_EXT_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)      \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,        \
                                                       OSPFV3_NAME,        \
                                                       CXT_TRC(Fmt,CxtId),\
                                                       Arg1, Arg2,        \
                                                       Arg3, Arg4,        \
                                                       Arg5)

#define OSPFV3_EXT_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,               \
                                                       OSPFV3_NAME,         \
                                                       CXT_TRC(Fmt,CxtId),  \
                                                       Arg1, Arg2,          \
                                                       Arg3, Arg4,          \
                                                       Arg5, Arg6)

#define OSPFV3_EXT_TRC7(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
                                             UtlTrcLog(OSPFV3_EXT_TRC_FLAG(CxtId), \
                                                       Value,               \
                                                       OSPFV3_NAME,         \
                                                       CXT_TRC(Fmt,CxtId),  \
                                                       Arg1, Arg2,          \
                                                       Arg3, Arg4,          \
                                                       Arg5, Arg6,          \
                                                       Arg7)


#else /* TRACE_WANTED */

#define  OSPFV3_TRC_FLAG
#define  OSPFV3_EXT_TRC_FLAG
#define  OSPFV3_GBL_TRC_FLAG
#define  OSPFV3_NAME
#define  OSPFV3_PKT_DUMP(pBuf, Length, fmt)
#define  OSPFV3_TRC(Value, CxtId, Fmt)
#define  OSPFV3_TRC1(Value, CxtId, Fmt, Arg)
#define  OSPFV3_TRC2(Value, CxtId, Fmt, Arg1, Arg2)
#define  OSPFV3_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)
#define  OSPFV3_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  OSPFV3_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  OSPFV3_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  OSPFV3_TRC7(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)
#define  OSPFV3_GBL_TRC(Value, Fmt)
#define  OSPFV3_GBL_TRC1(Value, Fmt, Arg)
#define  OSPFV3_GBL_TRC2(Value, Fmt, Arg1, Arg2)
#define  OSPFV3_GBL_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define  OSPFV3_GBL_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  OSPFV3_GBL_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  OSPFV3_GBL_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  OSPFV3_GBL_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)
#define  OSPFV3_EXT_TRC(Value, CxtId, Fmt)
#define  OSPFV3_EXT_TRC1(Value, CxtId, Fmt, Arg)
#define  OSPFV3_EXT_TRC2(Value, CxtId, Fmt, Arg1, Arg2)
#define  OSPFV3_EXT_TRC3(Value, CxtId, Fmt, Arg1, Arg2, Arg3)
#define  OSPFV3_EXT_TRC4(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  OSPFV3_EXT_TRC5(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  OSPFV3_EXT_TRC6(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  OSPFV3_EXT_TRC7(Value, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#endif /* TRACE_WANTED */

/* OSPFV3 Specific Trace Categories */
#define  OSPFV3_HP_TRC     0x00010000
#define  OSPFV3_DDP_TRC    0x00020000
#define  OSPFV3_LRQ_TRC    0x00040000
#define  OSPFV3_LSU_TRC    0x00080000
#define  OSPFV3_LAK_TRC    0x00100000
#define  OSPFV3_ISM_TRC    0x00200000
#define  OSPFV3_NSM_TRC    0x00400000
#define  OSPFV3_RT_TRC     0x00800000

#define  OSPFV3_NSSA_TRC   0x04000000
#define  OSPFV3_RAG_TRC    0x08000000

/* OSPFV3 MODULE Specific Trace Categories */
#define  OSPFV3_CONFIGURATION_TRC  0x10000000 
#define  OSPFV3_ADJACENCY_TRC      0x20000000 
#define  OSPFV3_LSDB_TRC           0x40000000 
#define  OSPFV3_PPP_TRC            0x80000000 
#define  OSPFV3_RTMODULE_TRC       0x01000000 
#define  OSPFV3_INTERFACE_TRC      0x02000000 
#define  OSPFV3_AUTH_TRC           0x00001000
/* OSPF FUNCTION Specific Trace Categories */
#define  OSPFV3_FN_ENTRY  0x00001000 
#define  OSPFV3_FN_EXIT   0x00002000 

/* OSPF MEMORY RESOURCE Specific Trace Categories */
#define  OSPFV3_MEM_SUCC  0x00004000 
#define  OSPFV3_MEM_FAIL  0x00008000 

/*-- New Packet Dumping Specific definations --*/

/* OSPF Packet Dumping Trace Categories */
#define  OSPFV3_DUMP_HGH_TRC  0x00000100  /* Packet HighLevel dump */
#define  OSPFV3_DUMP_LOW_TRC  0x00000200  /* Packet LowLevel dump */
#define  OSPFV3_DUMP_HEX_TRC  0x00000400  /* Packet Hex dump */
#define  OSPFV3_CRITICAL_TRC  0x00000800  /* Critical Trace */

/* OSPF Graceful restart Trace Categories */
#define  OSPFV3_RESTART_TRC   0x00000001  /* Restarting module */
#define  OSPFV3_HELPER_TRC    0x00000002  /* Helper module */
#define  OSPFV3_RM_TRC        0x00000004

#ifdef TRACE_WANTED
/* Incoming Packet Dumping */
#define OSPFV3_INC_PKT_DUMP(CxtId, pBuf, u2BufLen)   \
        V3DbgOspfDumpPktData (OSPFV3_TRC_FLAG(CxtId), \
                              OSPFV3_DIR_IN, pBuf, u2BufLen)

/* Outgoing Packet Dumping */
#define OSPFV3_OUT_PKT_DUMP(CxtId, pBuf, u2BufLen)   \
        V3DbgOspfDumpPktData (OSPFV3_TRC_FLAG(CxtId), \
                              OSPFV3_DIR_OUT, pBuf, u2BufLen)

#else /* Packet Dump is not included */
#define OSPFV3_INC_PKT_DUMP(CxtId, pBuf, u2BufLen)
#define OSPFV3_OUT_PKT_DUMP(CxtId, pBuf, u2BufLen) 
#endif

#define OSPFV3_HIGH_DUMP      0x01    /* High level dumping (One liner output)*/
#define OSPFV3_LOW_DUMP       0x02    /* Low level dumping (Detailed output) */
#define OSPFV3_HEX_DUMP       0x03    /* Hex level dumping (Hex bytes) */

#define OSPFV3_DIR_IN         0x01    /* Incoming pakcet */
#define OSPFV3_DIR_OUT        0x02    /* Outgoing pakcet */

/* the maximum size for packet decodes is 40K bytes of string */
#define OSPFV3_MAX_LOG_STR_LEN     256
#define OSPFV3_MAX_DUMP_BUF   (2 * 1024U)
#define OSPFV3_BIT(x, n)      (x & n)
#define OSPFV3BUF(BufPtr)     BufPtr+STRLEN(BufPtr)
#define OSPFV3_TRC_PRINTF(pBuf)   UtlTrcPrint(pBuf)
#define OSPFV3_NO_OF_BYTE_PER_LINE  16

#endif /* _O3DEBUG_H */
