/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: o3tmr.h,v 1.6 2017/12/26 13:34:25 siva Exp $
*
* Description: This file contains definitions relating to timers.
*********************************************************************/
#ifndef _O3TMR_H
#define _O3TMR_H


/* constants for timer types */
#define  OSPFV3_HELLO_TIMER                  1
#define  OSPFV3_POLL_TIMER                   2
#define  OSPFV3_WAIT_TIMER                   3
#define  OSPFV3_INACTIVITY_TIMER             4
#define  OSPFV3_DD_INIT_RXMT_TIMER           5
#define  OSPFV3_DD_RXMT_TIMER                6
#define  OSPFV3_DD_LAST_PKT_LIFE_TIME_TIMER  7
#define  OSPFV3_LSA_REQ_RXMT_TIMER           8
#define  OSPFV3_LSA_NORMAL_AGING_TIMER       9
#define  OSPFV3_MIN_LSA_INTERVAL_TIMER       10
#define  OSPFV3_LSA_RXMT_TIMER               11
#define  OSPFV3_DEL_ACK_TIMER                12
#define  OSPFV3_RUN_RT_TIMER                 13
#define  OSPFV3_EXIT_OVERFLOW_TIMER          14
#define  OSPFV3_NSSA_STBLTY_INTERVAL_TIMER   15
#define  OSPFV3_NBR_PROBE_INTERVAL_TIMER     16
#define  OSPFV3_VRF_SPF_TIMER                17
#define  OSPFV3_RESTART_GRACE_TIMER          18
#define  OSPFV3_HELPER_GRACE_TIMER           19
#define  OSPFV3_MULTIF_ACTIVE_DETECT_TIMER   20
#define  OSPFV3_50MS_LSU_LAK_TIMER           21

#define  OSPFV3_MAX_TIMERS                   22
#define  OSPFV3_INVALID_TIMER                OSPFV3_MAX_TIMERS



/* This is to indicate the starting & stopping of dna_spl_aging timer */
#define  OSPFV3_START  1
#define  OSPFV3_STOP   0

#define     OSPFV3_NO_OF_TICKS_PER_SEC  SYS_NUM_OF_TIME_UNITS_IN_A_SEC

/* constant specifying the percentage of jitter */
#define  OSPFV3_JITTER       2

#define  OSPFV3_TIMER_EVENT  0x00000100
#define  OSPFV3_HELLO_TIMER_EVENT  0x00020000

#define  OSPFV3_DEF_SPF_INT  5
#define  OSPFV3_DEF_HOLD_TIME_INT 10


PUBLIC VOID V3TmrMultIfActiveDetectTimer PROTO ((VOID *pArg));

#ifdef RAWSOCK_WANTED
/* Sets the socket receive in non blocking mode */
#define OSPF_RAWSOCK_NON_BLOCK        16
#endif

#endif
