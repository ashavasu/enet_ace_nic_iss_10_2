/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fso3tedb.h,v 1.5 2017/12/26 13:34:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSO3TEDB_H
#define _FSO3TEDB_H

UINT1 FutOspfv3TestIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfv3ExtRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};

UINT4 fso3te [] ={1,3,6,1,4,1,2076,301};
tSNMP_OID_TYPE fso3teOID = {8, fso3te};


UINT4 FutOspfv3TestIfIndex [ ] ={1,3,6,1,4,1,2076,301,1,1,1};
UINT4 FutOspfv3TestDemandTraffic [ ] ={1,3,6,1,4,1,2076,301,1,1,2};
UINT4 FutOspfv3ExtRouteDestType [ ] ={1,3,6,1,4,1,2076,301,2,1,1};
UINT4 FutOspfv3ExtRouteDest [ ] ={1,3,6,1,4,1,2076,301,2,1,2};
UINT4 FutOspfv3ExtRoutePfxLength [ ] ={1,3,6,1,4,1,2076,301,2,1,3};
UINT4 FutOspfv3ExtRouteNextHopType [ ] ={1,3,6,1,4,1,2076,301,2,1,4};
UINT4 FutOspfv3ExtRouteNextHop [ ] ={1,3,6,1,4,1,2076,301,2,1,5};
UINT4 FutOspfv3ExtRouteMetric [ ] ={1,3,6,1,4,1,2076,301,2,1,6};
UINT4 FutOspfv3ExtRouteMetricType [ ] ={1,3,6,1,4,1,2076,301,2,1,7};
UINT4 FutOspfv3ExtRouteStatus [ ] ={1,3,6,1,4,1,2076,301,2,1,8};




tMbDbEntry fso3teMibEntry[]= {

{{11,FutOspfv3TestIfIndex}, GetNextIndexFutOspfv3TestIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3TestIfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3TestDemandTraffic}, GetNextIndexFutOspfv3TestIfTable, FutOspfv3TestDemandTrafficGet, FutOspfv3TestDemandTrafficSet, FutOspfv3TestDemandTrafficTest, FutOspfv3TestIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3TestIfTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfv3ExtRouteDestType}, GetNextIndexFutOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3ExtRouteDest}, GetNextIndexFutOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3ExtRoutePfxLength}, GetNextIndexFutOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3ExtRouteNextHopType}, GetNextIndexFutOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3ExtRouteNextHop}, GetNextIndexFutOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3ExtRouteMetric}, GetNextIndexFutOspfv3ExtRouteTable, FutOspfv3ExtRouteMetricGet, FutOspfv3ExtRouteMetricSet, FutOspfv3ExtRouteMetricTest, FutOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, "10"},

{{11,FutOspfv3ExtRouteMetricType}, GetNextIndexFutOspfv3ExtRouteTable, FutOspfv3ExtRouteMetricTypeGet, FutOspfv3ExtRouteMetricTypeSet, FutOspfv3ExtRouteMetricTypeTest, FutOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3ExtRouteTableINDEX, 5, 0, 0, "3"},

{{11,FutOspfv3ExtRouteStatus}, GetNextIndexFutOspfv3ExtRouteTable, FutOspfv3ExtRouteStatusGet, FutOspfv3ExtRouteStatusSet, FutOspfv3ExtRouteStatusTest, FutOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3ExtRouteTableINDEX, 5, 0, 1, NULL},
};
tMibData fso3teEntry = { 10, fso3teMibEntry };

#endif /* _FSO3TEDB_H */

