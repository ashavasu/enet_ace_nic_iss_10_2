/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3snmp.h,v 1.5 2015/03/08 11:12:07 siva Exp $
 *
 * Description:This file contains macros, constants and
 *             type definitions relating to network management.
 *
 *******************************************************************/

#ifndef _O3SNMPIF_H
#define _O3SNMPIF_H

/* Size of SNMPv2 trap id */
#define SNMP_V2_TRAP_OID_LEN            11

/* Traps IDs */
#define OSPFV3_RST_STATUS_CHANGE_TRAP          1
#define OSPFV3_NBR_RST_STATUS_CHANGE_TRAP      2
#define OSPFV3_VIRT_NBR_RST_STATUS_CHANGE_TRAP 3
#define OSPFV3_RED_STATE_CHANGE_TRAP           4 
#define OSPFV3_RED_BULK_UPD_ABORT_TRAP         5
#define OSPFV3_ALLOWABLE_TRAP_COUNT_IN_SWP     7

/* Traps for RFC - 7611 */
#define OSPFV3_AUTH_SEQNUM_WRAP_TRAP       6

/*
 * Following are the structures which are used for passing various informations
 * regarding the traps that are generated.
 */

typedef struct _V3OsRstStatChgTrapInfo {
    tV3OsRouterId       rtrId;
    UINT4               u4GracePeriod;
    UINT1               u1RestartStatus;
    UINT1               u1RestartExitReason;
    UINT1               au1Pad[2];
}tV3OsRstStatChgTrapInfo;

typedef struct _V3OsNbrRstStatChgTrapInfo {
    tV3OsRouterId       rtrId;
    tV3OsRouterId       nbrRtrId;
    UINT4               u4NbrIfId;
    UINT4               u4NbrHelperAge;
    UINT1               u1NbrHelperStatus;
    UINT1               u1NbrHelperExitReason;
    UINT1               au1Pad[2];
}tV3OsNbrRstStatChgTrapInfo;

typedef struct _V3OsVifRstStatChgTrapInfo {
    tV3OsRouterId       rtrId;
    tV3OsRouterId       VifNbrId;
    tV3OsAreaId         VifNbrAreaId;
    UINT4               u4VifNbrHelperAge;
    UINT1               u1VifNbrHelperStatus;
    UINT1               u1VifNbrHelperExitReason;
    UINT1               au1Pad[2];
}tV3OsVifRstStatChgTrapInfo;

typedef struct _V3OsRedStateChgTrap {
    tV3OsRouterId       rtrId;
    INT4                i4HotStandbyState;
}tV3OsRedStateChgTrapInfo;

typedef struct _V3OsRedBulkUpdAbortTrap {
    tV3OsRouterId       rtrId;
    INT4                i4DynamicBulkUpdStatus; 
    UINT1               u1BulkUpdateAbortReason;
    UINT1               au1Reserved [3];
}tV3OsRedBulkUpdAbortTrapInfo;

typedef struct _V3OsAuthSeqNumWrapTrap {
    tV3OsRouterId       rtrId;
    UINT1               au1Reserved [3];
}tV3OsAuthSeqNumWrapTrapInfo;

#define OSPFV3_TRAPS_OID_LEN  9

#define OSPFV3_OID_TBL_SIZE sizeof(orig_mib_oid_table)/sizeof(struct MIB_OID)
#endif
