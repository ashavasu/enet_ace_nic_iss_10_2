/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsmio3lw.h,v 1.5 2017/12/26 13:34:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3GlobalTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3VrfSpfInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3RTStaggeringStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3HotStandbyAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3HotStandbyState ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3DynamicBulkUpdStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMIOspfv3StandbyHelloSyncCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMIOspfv3StandbyLsaSyncCount ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIOspfv3ClearProcess ARG_LIST((INT4 *));




/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3GlobalTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMIOspfv3VrfSpfInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMIOspfv3RTStaggeringStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMIOspfv3ClearProcess ARG_LIST((INT4 ));




/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3GlobalTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3VrfSpfInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RTStaggeringStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3ClearProcess ARG_LIST((UINT4 *  ,INT4 ));



/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3GlobalTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIOspfv3VrfSpfInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIOspfv3RTStaggeringStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMIOspfv3ClearProcess ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));




/* Proto Validate Index Instance for FsMIOspfv3Table. */
INT1
nmhValidateIndexInstanceFsMIOspfv3Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3Table  */

INT1
nmhGetFirstIndexFsMIOspfv3Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3OverFlowState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3TraceLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3ABRType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3NssaAsbrDefRtTrans ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3DefaultPassiveInterface ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3SpfDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3SpfHoldTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RTStaggeringInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RestartStrictLsaChecking ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3HelperSupport ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3HelperGraceTimeLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RestartAckState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3GraceLsaRetransmitCount ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RestartReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3ExtTraceLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3SetTraps ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3BfdStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3BfdAllIfState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RouterIdPermanence ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3TraceLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3ABRType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3NssaAsbrDefRtTrans ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3DefaultPassiveInterface ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3SpfDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3SpfHoldTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RTStaggeringInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RestartStrictLsaChecking ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3HelperSupport ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3HelperGraceTimeLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RestartAckState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3GraceLsaRetransmitCount ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RestartReason ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3ExtTraceLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3SetTraps ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3BfdStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3BfdAllIfState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RouterIdPermanence ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3TraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3ABRType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3NssaAsbrDefRtTrans ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3DefaultPassiveInterface ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3SpfDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3SpfHoldTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RTStaggeringInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RestartStrictLsaChecking ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3HelperSupport ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3HelperGraceTimeLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RestartAckState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3GraceLsaRetransmitCount ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RestartReason ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3ExtTraceLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3SetTraps ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3BfdStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3BfdAllIfState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RouterIdPermanence ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3Table ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3IfTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3IfTable  */

INT1
nmhGetFirstIndexFsMIOspfv3IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3IfOperState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfPassive ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfNbrCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfAdjCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfHelloRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfHelloTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfHelloDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfDdpRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfDdpTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfDdpDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLrqRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLrqTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLrqDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLsuRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLsuTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLsuDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLakRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLakTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfLakDisd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfContextId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIOspfv3IfLinkLSASuppression ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfBfdState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfCryptoAuthType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfCryptoAuthMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3IfAuthTxed ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfAuthRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3IfAuthDisd ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3IfPassive ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3IfLinkLSASuppression ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3IfBfdState ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3IfCryptoAuthType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3IfCryptoAuthMode ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3IfPassive ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsMIOspfv3IfLinkLSASuppression ARG_LIST((UINT4 *  ,INT4  ,INT4 ));
INT1
nmhTestv2FsMIOspfv3IfBfdState ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3IfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3IfCryptoAuthMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3RoutingTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3RoutingTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3RoutingTable  */

INT1
nmhGetFirstIndexFsMIOspfv3RoutingTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3RoutingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3RouteType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIOspfv3RouteAreaId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIOspfv3RouteCost ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIOspfv3RouteType2Cost ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIOspfv3RouteInterfaceIndex ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsMIOspfv3AsExternalAggregationTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3AsExternalAggregationTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3AsExternalAggregationTable  */

INT1
nmhGetFirstIndexFsMIOspfv3AsExternalAggregationTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3AsExternalAggregationTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3AsExternalAggregationEffect ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3AsExternalAggregationTranslation ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3AsExternalAggregationStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3AsExternalAggregationEffect ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3AsExternalAggregationTranslation ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3AsExternalAggregationStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3AsExternalAggregationEffect ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3AsExternalAggregationTranslation ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3AsExternalAggregationStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3AsExternalAggregationTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3BRRouteTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3BRRouteTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3BRRouteTable  */

INT1
nmhGetFirstIndexFsMIOspfv3BRRouteTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3BRRouteTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3BRRouteType ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3BRRouteAreaId ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetFsMIOspfv3BRRouteCost ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3BRRouteInterfaceIndex ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfv3RedistRouteCfgTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3RedistRouteCfgTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3RedistRouteCfgTable  */

INT1
nmhGetFirstIndexFsMIOspfv3RedistRouteCfgTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3RedistRouteCfgTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3RedistRouteMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RedistRouteMetricType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RedistRouteTagType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RedistRouteTag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RedistRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3RedistRouteMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RedistRouteMetricType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RedistRouteTagType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RedistRouteTag ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RedistRouteStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3RedistRouteMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RedistRouteMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RedistRouteTagType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RedistRouteTag ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RedistRouteStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3RedistRouteCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3RRDRouteTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3RRDRouteTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3RRDRouteTable  */

INT1
nmhGetFirstIndexFsMIOspfv3RRDRouteTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3RRDRouteTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3RRDStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RRDSrcProtoMask ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RRDRouteMapName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3RRDStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RRDSrcProtoMask ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RRDRouteMapName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3RRDStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RRDSrcProtoMask ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RRDRouteMapName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3RRDRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3RRDMetricTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3RRDMetricTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3RRDMetricTable  */

INT1
nmhGetFirstIndexFsMIOspfv3RRDMetricTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3RRDMetricTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3RRDMetricValue ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3RRDMetricType ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3RRDMetricValue ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3RRDMetricType ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3RRDMetricValue ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3RRDMetricType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3RRDMetricTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3DistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3DistInOutRouteMapTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3DistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsMIOspfv3DistInOutRouteMapTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3DistInOutRouteMapTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3DistInOutRouteMapValue ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3DistInOutRouteMapValue ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3DistInOutRouteMapValue ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3PreferenceTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3PreferenceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3PreferenceTable  */

INT1
nmhGetFirstIndexFsMIOspfv3PreferenceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3PreferenceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3PreferenceValue ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3PreferenceValue ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3PreferenceValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3PreferenceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto Validate Index Instance for FsMIOspfv3NeighborBfdTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3NeighborBfdTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3NeighborBfdTable  */

INT1
nmhGetFirstIndexFsMIOspfv3NeighborBfdTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3NeighborBfdTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3NbrBfdState ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIOspfv3IfAuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3IfAuthTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3IfAuthTable  */

INT1
nmhGetFirstIndexFsMIOspfv3IfAuthTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3IfAuthTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3IfAuthKey ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3IfAuthKeyStartAccept ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3IfAuthKeyStartGenerate ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3IfAuthKeyStopGenerate ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3IfAuthKeyStopAccept ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3IfAuthKeyStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3IfAuthKey ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3IfAuthKeyStartAccept ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3IfAuthKeyStartGenerate ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3IfAuthKeyStopGenerate ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3IfAuthKeyStopAccept ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3IfAuthKeyStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3IfAuthKey ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3IfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3IfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3IfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3IfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3IfAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3IfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3VirtIfAuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3VirtIfAuthTable ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3VirtIfAuthTable  */

INT1
nmhGetFirstIndexFsMIOspfv3VirtIfAuthTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3VirtIfAuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3VirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIOspfv3VirtIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3VirtIfAuthKey ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStartAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStopAccept ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIOspfv3VirtIfAuthKeyStatus ARG_LIST((INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKey ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStartAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStartGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStopGenerate ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStopAccept ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIOspfv3VirtIfAuthKeyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3VirtIfAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIOspfv3VirtIfCryptoAuthTable. */
INT1
nmhValidateIndexInstanceFsMIOspfv3VirtIfCryptoAuthTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIOspfv3VirtIfCryptoAuthTable  */

INT1
nmhGetFirstIndexFsMIOspfv3VirtIfCryptoAuthTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIOspfv3VirtIfCryptoAuthTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIOspfv3VirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIOspfv3VirtIfCryptoAuthMode ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIOspfv3VirtIfCryptoAuthType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIOspfv3VirtIfCryptoAuthMode ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIOspfv3VirtIfCryptoAuthType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIOspfv3VirtIfCryptoAuthMode ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIOspfv3VirtIfCryptoAuthTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
