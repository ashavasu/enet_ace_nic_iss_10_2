/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsmitodb.h,v 1.2 2017/12/26 13:34:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMITODB_H
#define _FSMITODB_H

UINT1 FsMIOspfv3TestIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIOspfv3ExtRouteTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};

UINT4 fsmito [] ={1,3,6,1,4,1,2076,2,24,100};
tSNMP_OID_TYPE fsmitoOID = {10, fsmito};


UINT4 FsMIOspfv3TestIfIndex [ ] ={1,3,6,1,4,1,2076,2,24,100,1,1,1};
UINT4 FsMIOspfv3TestDemandTraffic [ ] ={1,3,6,1,4,1,2076,2,24,100,1,1,2};
UINT4 FsMIOspfv3TestIfContextId [ ] ={1,3,6,1,4,1,2076,2,24,100,1,1,3};
UINT4 FsMIOspfv3ExtRouteDestType [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,1};
UINT4 FsMIOspfv3ExtRouteDest [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,2};
UINT4 FsMIOspfv3ExtRoutePfxLength [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,3};
UINT4 FsMIOspfv3ExtRouteNextHopType [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,4};
UINT4 FsMIOspfv3ExtRouteNextHop [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,5};
UINT4 FsMIOspfv3ExtRouteMetric [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,6};
UINT4 FsMIOspfv3ExtRouteMetricType [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,7};
UINT4 FsMIOspfv3ExtRouteStatus [ ] ={1,3,6,1,4,1,2076,2,24,100,2,1,8};




tMbDbEntry fsmitoMibEntry[]= {

{{13,FsMIOspfv3TestIfIndex}, GetNextIndexFsMIOspfv3TestIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3TestIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIOspfv3TestDemandTraffic}, GetNextIndexFsMIOspfv3TestIfTable, FsMIOspfv3TestDemandTrafficGet, FsMIOspfv3TestDemandTrafficSet, FsMIOspfv3TestDemandTrafficTest, FsMIOspfv3TestIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3TestIfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIOspfv3TestIfContextId}, GetNextIndexFsMIOspfv3TestIfTable, FsMIOspfv3TestIfContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIOspfv3TestIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIOspfv3ExtRouteDestType}, GetNextIndexFsMIOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsMIOspfv3ExtRouteDest}, GetNextIndexFsMIOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsMIOspfv3ExtRoutePfxLength}, GetNextIndexFsMIOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsMIOspfv3ExtRouteNextHopType}, GetNextIndexFsMIOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsMIOspfv3ExtRouteNextHop}, GetNextIndexFsMIOspfv3ExtRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, NULL},

{{13,FsMIOspfv3ExtRouteMetric}, GetNextIndexFsMIOspfv3ExtRouteTable, FsMIOspfv3ExtRouteMetricGet, FsMIOspfv3ExtRouteMetricSet, FsMIOspfv3ExtRouteMetricTest, FsMIOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, "10"},

{{13,FsMIOspfv3ExtRouteMetricType}, GetNextIndexFsMIOspfv3ExtRouteTable, FsMIOspfv3ExtRouteMetricTypeGet, FsMIOspfv3ExtRouteMetricTypeSet, FsMIOspfv3ExtRouteMetricTypeTest, FsMIOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 0, "3"},

{{13,FsMIOspfv3ExtRouteStatus}, GetNextIndexFsMIOspfv3ExtRouteTable, FsMIOspfv3ExtRouteStatusGet, FsMIOspfv3ExtRouteStatusSet, FsMIOspfv3ExtRouteStatusTest, FsMIOspfv3ExtRouteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIOspfv3ExtRouteTableINDEX, 6, 0, 1, NULL},
};
tMibData fsmitoEntry = { 11, fsmitoMibEntry };

#endif /* _FSMITODB_H */

