/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3rtstg.h,v 1.4 2016/07/07 14:19:18 siva Exp $
*
* Description:This file contains constants prototypes for all the
*           functions related to route calculation relinquish.
*
*******************************************************************/

#ifndef __O3RTSTG_H

#define __O3RTSTG_H

#define OSPFV3_INVALID_STAGGERING_DELTA 0xFFFFFFFF

/* Function prototypes */
PUBLIC VOID    OSPF3RtcRelinquishInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID    OSPF3RtRelinquishProcessEvents PROTO ((VOID));

#endif /*__O3RTSTG_H*/



