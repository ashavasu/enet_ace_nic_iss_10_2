/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsos3db.h,v 1.11 2017/12/26 13:34:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSOS3DB_H
#define _FSOS3DB_H

UINT1 FutOspfv3IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfv3RoutingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FutOspfv3AsExternalAggregationTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfv3BRRouteTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfv3RedistRouteCfgTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FutOspfv3RRDMetricTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FutOspfv3DistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfv3NeighborBfdTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FutOspfv3IfAuthTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfv3VirtIfAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FutOspfv3VirtIfCryptoAuthTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fsos3 [] ={1,3,6,1,4,1,2076,90};
tSNMP_OID_TYPE fsos3OID = {8, fsos3};


UINT4 FutOspfv3OverFlowState [ ] ={1,3,6,1,4,1,2076,90,1,1};
UINT4 FutOspfv3TraceLevel [ ] ={1,3,6,1,4,1,2076,90,1,2};
UINT4 FutOspfv3ABRType [ ] ={1,3,6,1,4,1,2076,90,1,3};
UINT4 FutOspfv3NssaAsbrDefRtTrans [ ] ={1,3,6,1,4,1,2076,90,1,4};
UINT4 FutOspfv3DefaultPassiveInterface [ ] ={1,3,6,1,4,1,2076,90,1,5};
UINT4 FutOspfv3SpfDelay [ ] ={1,3,6,1,4,1,2076,90,1,6};
UINT4 FutOspfv3SpfHoldTime [ ] ={1,3,6,1,4,1,2076,90,1,7};
UINT4 FutOspfv3RTStaggeringInterval [ ] ={1,3,6,1,4,1,2076,90,1,8};
UINT4 FutOspfv3RTStaggeringStatus [ ] ={1,3,6,1,4,1,2076,90,1,9};
UINT4 FutOspfv3RestartStrictLsaChecking [ ] ={1,3,6,1,4,1,2076,90,1,10};
UINT4 FutOspfv3HelperSupport [ ] ={1,3,6,1,4,1,2076,90,1,11};
UINT4 FutOspfv3HelperGraceTimeLimit [ ] ={1,3,6,1,4,1,2076,90,1,12};
UINT4 FutOspfv3RestartAckState [ ] ={1,3,6,1,4,1,2076,90,1,13};
UINT4 FutOspfv3GraceLsaRetransmitCount [ ] ={1,3,6,1,4,1,2076,90,1,14};
UINT4 FutOspfv3RestartReason [ ] ={1,3,6,1,4,1,2076,90,1,15};
UINT4 FutOspfv3ExtTraceLevel [ ] ={1,3,6,1,4,1,2076,90,1,16};
UINT4 FutOspfv3SetTraps [ ] ={1,3,6,1,4,1,2076,90,1,17};
UINT4 FutOspfv3HotStandbyAdminStatus [ ] ={1,3,6,1,4,1,2076,90,1,18};
UINT4 FutOspfv3HotStandbyState [ ] ={1,3,6,1,4,1,2076,90,1,19};
UINT4 FutOspfv3DynamicBulkUpdStatus [ ] ={1,3,6,1,4,1,2076,90,1,20};
UINT4 FutOspfv3StandbyHelloSyncCount [ ] ={1,3,6,1,4,1,2076,90,1,21};
UINT4 FutOspfv3StandbyLsaSyncCount [ ] ={1,3,6,1,4,1,2076,90,1,22};
UINT4 FutOspfv3BfdStatus [ ] ={1,3,6,1,4,1,2076,90,1,23};
UINT4 FutOspfv3BfdAllIfState [ ] ={1,3,6,1,4,1,2076,90,1,24};
UINT4 FutOspfv3RouterIdPermanence [ ] ={1,3,6,1,4,1,2076,90,1,25};
UINT4 FutOspfv3ClearProcess [ ] ={1,3,6,1,4,1,2076,90,1,26};
UINT4 FutOspfv3IfIndex [ ] ={1,3,6,1,4,1,2076,90,2,1,1};
UINT4 FutOspfv3IfOperState [ ] ={1,3,6,1,4,1,2076,90,2,1,2};
UINT4 FutOspfv3IfPassive [ ] ={1,3,6,1,4,1,2076,90,2,1,3};
UINT4 FutOspfv3IfNbrCount [ ] ={1,3,6,1,4,1,2076,90,2,1,4};
UINT4 FutOspfv3IfAdjCount [ ] ={1,3,6,1,4,1,2076,90,2,1,5};
UINT4 FutOspfv3IfHelloRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,6};
UINT4 FutOspfv3IfHelloTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,7};
UINT4 FutOspfv3IfHelloDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,8};
UINT4 FutOspfv3IfDdpRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,9};
UINT4 FutOspfv3IfDdpTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,10};
UINT4 FutOspfv3IfDdpDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,11};
UINT4 FutOspfv3IfLrqRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,12};
UINT4 FutOspfv3IfLrqTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,13};
UINT4 FutOspfv3IfLrqDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,14};
UINT4 FutOspfv3IfLsuRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,15};
UINT4 FutOspfv3IfLsuTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,16};
UINT4 FutOspfv3IfLsuDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,17};
UINT4 FutOspfv3IfLakRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,18};
UINT4 FutOspfv3IfLakTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,19};
UINT4 FutOspfv3IfLakDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,20};
UINT4 FutOspfv3IfLinkLSASuppression [ ] ={1,3,6,1,4,1,2076,90,2,1,21};
UINT4 FutOspfv3IfBfdState [ ] ={1,3,6,1,4,1,2076,90,2,1,22};
UINT4 FutOspfv3IfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,90,2,1,23};
UINT4 FutOspfv3IfCryptoAuthMode [ ] ={1,3,6,1,4,1,2076,90,2,1,24};
UINT4 FutOspfv3IfAuthTxed [ ] ={1,3,6,1,4,1,2076,90,2,1,25};
UINT4 FutOspfv3IfAuthRcvd [ ] ={1,3,6,1,4,1,2076,90,2,1,26};
UINT4 FutOspfv3IfAuthDisd [ ] ={1,3,6,1,4,1,2076,90,2,1,27};
UINT4 FutOspfv3RouteDestType [ ] ={1,3,6,1,4,1,2076,90,3,1,1};
UINT4 FutOspfv3RouteDest [ ] ={1,3,6,1,4,1,2076,90,3,1,2};
UINT4 FutOspfv3RoutePfxLength [ ] ={1,3,6,1,4,1,2076,90,3,1,3};
UINT4 FutOspfv3RouteNextHopType [ ] ={1,3,6,1,4,1,2076,90,3,1,4};
UINT4 FutOspfv3RouteNextHop [ ] ={1,3,6,1,4,1,2076,90,3,1,5};
UINT4 FutOspfv3RouteType [ ] ={1,3,6,1,4,1,2076,90,3,1,6};
UINT4 FutOspfv3RouteAreaId [ ] ={1,3,6,1,4,1,2076,90,3,1,7};
UINT4 FutOspfv3RouteCost [ ] ={1,3,6,1,4,1,2076,90,3,1,8};
UINT4 FutOspfv3RouteType2Cost [ ] ={1,3,6,1,4,1,2076,90,3,1,9};
UINT4 FutOspfv3RouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,90,3,1,10};
UINT4 FutOspfv3AsExternalAggregationNetType [ ] ={1,3,6,1,4,1,2076,90,4,1,1};
UINT4 FutOspfv3AsExternalAggregationNet [ ] ={1,3,6,1,4,1,2076,90,4,1,2};
UINT4 FutOspfv3AsExternalAggregationPfxLength [ ] ={1,3,6,1,4,1,2076,90,4,1,3};
UINT4 FutOspfv3AsExternalAggregationAreaId [ ] ={1,3,6,1,4,1,2076,90,4,1,4};
UINT4 FutOspfv3AsExternalAggregationEffect [ ] ={1,3,6,1,4,1,2076,90,4,1,5};
UINT4 FutOspfv3AsExternalAggregationTranslation [ ] ={1,3,6,1,4,1,2076,90,4,1,6};
UINT4 FutOspfv3AsExternalAggregationStatus [ ] ={1,3,6,1,4,1,2076,90,4,1,7};
UINT4 FutOspfv3BRRouteDest [ ] ={1,3,6,1,4,1,2076,90,5,1,1};
UINT4 FutOspfv3BRRouteNextHopType [ ] ={1,3,6,1,4,1,2076,90,5,1,2};
UINT4 FutOspfv3BRRouteNextHop [ ] ={1,3,6,1,4,1,2076,90,5,1,3};
UINT4 FutOspfv3BRRouteDestType [ ] ={1,3,6,1,4,1,2076,90,5,1,4};
UINT4 FutOspfv3BRRouteType [ ] ={1,3,6,1,4,1,2076,90,5,1,5};
UINT4 FutOspfv3BRRouteAreaId [ ] ={1,3,6,1,4,1,2076,90,5,1,6};
UINT4 FutOspfv3BRRouteCost [ ] ={1,3,6,1,4,1,2076,90,5,1,7};
UINT4 FutOspfv3BRRouteInterfaceIndex [ ] ={1,3,6,1,4,1,2076,90,5,1,8};
UINT4 FutOspfv3RedistRouteDestType [ ] ={1,3,6,1,4,1,2076,90,6,1,1};
UINT4 FutOspfv3RedistRouteDest [ ] ={1,3,6,1,4,1,2076,90,6,1,2};
UINT4 FutOspfv3RedistRoutePfxLength [ ] ={1,3,6,1,4,1,2076,90,6,1,3};
UINT4 FutOspfv3RedistRouteMetric [ ] ={1,3,6,1,4,1,2076,90,6,1,4};
UINT4 FutOspfv3RedistRouteMetricType [ ] ={1,3,6,1,4,1,2076,90,6,1,5};
UINT4 FutOspfv3RedistRouteTagType [ ] ={1,3,6,1,4,1,2076,90,6,1,6};
UINT4 FutOspfv3RedistRouteTag [ ] ={1,3,6,1,4,1,2076,90,6,1,7};
UINT4 FutOspfv3RedistRouteStatus [ ] ={1,3,6,1,4,1,2076,90,6,1,8};
UINT4 FutOspfv3RRDStatus [ ] ={1,3,6,1,4,1,2076,90,7,1,1};
UINT4 FutOspfv3RRDSrcProtoMask [ ] ={1,3,6,1,4,1,2076,90,7,1,2};
UINT4 FutOspfv3RRDRouteMapName [ ] ={1,3,6,1,4,1,2076,90,7,1,3};
UINT4 FutOspfv3RRDProtocolId [ ] ={1,3,6,1,4,1,2076,90,7,1,4,1,1};
UINT4 FutOspfv3RRDMetricValue [ ] ={1,3,6,1,4,1,2076,90,7,1,4,1,2};
UINT4 FutOspfv3RRDMetricType [ ] ={1,3,6,1,4,1,2076,90,7,1,4,1,3};
UINT4 FutOspfv3DistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,90,8,1,1,1};
UINT4 FutOspfv3DistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,90,8,1,1,2};
UINT4 FutOspfv3DistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,90,8,1,1,3};
UINT4 FutOspfv3DistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,90,8,1,1,4};
UINT4 FutOspf3PreferenceValue [ ] ={1,3,6,1,4,1,2076,90,9,1};
UINT4 FutOspfv3NbrBfdState [ ] ={1,3,6,1,4,1,2076,90,11,1,1,1};
UINT4 FutOspfv3IfAuthIfIndex [ ] ={1,3,6,1,4,1,2076,90,12,1,1};
UINT4 FutOspfv3IfAuthKeyId [ ] ={1,3,6,1,4,1,2076,90,12,1,2};
UINT4 FutOspfv3IfAuthKey [ ] ={1,3,6,1,4,1,2076,90,12,1,3};
UINT4 FutOspfv3IfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,90,12,1,4};
UINT4 FutOspfv3IfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,90,12,1,5};
UINT4 FutOspfv3IfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,90,12,1,6};
UINT4 FutOspfv3IfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,90,12,1,7};
UINT4 FutOspfv3IfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,90,12,1,8};
UINT4 FutOspfv3VirtIfAuthAreaId [ ] ={1,3,6,1,4,1,2076,90,13,1,1};
UINT4 FutOspfv3VirtIfAuthNeighbor [ ] ={1,3,6,1,4,1,2076,90,13,1,2};
UINT4 FutOspfv3VirtIfAuthKeyId [ ] ={1,3,6,1,4,1,2076,90,13,1,3};
UINT4 FutOspfv3VirtIfAuthKey [ ] ={1,3,6,1,4,1,2076,90,13,1,4};
UINT4 FutOspfv3VirtIfAuthKeyStartAccept [ ] ={1,3,6,1,4,1,2076,90,13,1,5};
UINT4 FutOspfv3VirtIfAuthKeyStartGenerate [ ] ={1,3,6,1,4,1,2076,90,13,1,6};
UINT4 FutOspfv3VirtIfAuthKeyStopGenerate [ ] ={1,3,6,1,4,1,2076,90,13,1,7};
UINT4 FutOspfv3VirtIfAuthKeyStopAccept [ ] ={1,3,6,1,4,1,2076,90,13,1,8};
UINT4 FutOspfv3VirtIfAuthKeyStatus [ ] ={1,3,6,1,4,1,2076,90,13,1,9};
UINT4 FutOspfv3VirtIfCryptoAuthType [ ] ={1,3,6,1,4,1,2076,90,14,1,1};
UINT4 FutOspfv3VirtIfCryptoAuthMode [ ] ={1,3,6,1,4,1,2076,90,14,1,2};
UINT4 FutOspfv3TrapNbrIfIndex [ ] ={1,3,6,1,4,1,2076,90,101,1,1};
UINT4 FutOspfv3TrapVirtNbrRtrId [ ] ={1,3,6,1,4,1,2076,90,101,1,2};
UINT4 FutOspfv3TrapNbrRtrId [ ] ={1,3,6,1,4,1,2076,90,101,1,3};
UINT4 FutOspfv3TrapVirtNbrArea [ ] ={1,3,6,1,4,1,2076,90,101,1,4};
UINT4 FutOspfv3TrapBulkUpdAbortReason [ ] ={1,3,6,1,4,1,2076,90,101,1,5};




tMbDbEntry fsos3MibEntry[]= {

{{10,FutOspfv3OverFlowState}, NULL, FutOspfv3OverFlowStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3TraceLevel}, NULL, FutOspfv3TraceLevelGet, FutOspfv3TraceLevelSet, FutOspfv3TraceLevelTest, FutOspfv3TraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2048"},

{{10,FutOspfv3ABRType}, NULL, FutOspfv3ABRTypeGet, FutOspfv3ABRTypeSet, FutOspfv3ABRTypeTest, FutOspfv3ABRTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfv3NssaAsbrDefRtTrans}, NULL, FutOspfv3NssaAsbrDefRtTransGet, FutOspfv3NssaAsbrDefRtTransSet, FutOspfv3NssaAsbrDefRtTransTest, FutOspfv3NssaAsbrDefRtTransDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3DefaultPassiveInterface}, NULL, FutOspfv3DefaultPassiveInterfaceGet, FutOspfv3DefaultPassiveInterfaceSet, FutOspfv3DefaultPassiveInterfaceTest, FutOspfv3DefaultPassiveInterfaceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3SpfDelay}, NULL, FutOspfv3SpfDelayGet, FutOspfv3SpfDelaySet, FutOspfv3SpfDelayTest, FutOspfv3SpfDelayDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{10,FutOspfv3SpfHoldTime}, NULL, FutOspfv3SpfHoldTimeGet, FutOspfv3SpfHoldTimeSet, FutOspfv3SpfHoldTimeTest, FutOspfv3SpfHoldTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{10,FutOspfv3RTStaggeringInterval}, NULL, FutOspfv3RTStaggeringIntervalGet, FutOspfv3RTStaggeringIntervalSet, FutOspfv3RTStaggeringIntervalTest, FutOspfv3RTStaggeringIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10000"},

{{10,FutOspfv3RTStaggeringStatus}, NULL, FutOspfv3RTStaggeringStatusGet, FutOspfv3RTStaggeringStatusSet, FutOspfv3RTStaggeringStatusTest, FutOspfv3RTStaggeringStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfv3RestartStrictLsaChecking}, NULL, FutOspfv3RestartStrictLsaCheckingGet, FutOspfv3RestartStrictLsaCheckingSet, FutOspfv3RestartStrictLsaCheckingTest, FutOspfv3RestartStrictLsaCheckingDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3HelperSupport}, NULL, FutOspfv3HelperSupportGet, FutOspfv3HelperSupportSet, FutOspfv3HelperSupportTest, FutOspfv3HelperSupportDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3HelperGraceTimeLimit}, NULL, FutOspfv3HelperGraceTimeLimitGet, FutOspfv3HelperGraceTimeLimitSet, FutOspfv3HelperGraceTimeLimitTest, FutOspfv3HelperGraceTimeLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FutOspfv3RestartAckState}, NULL, FutOspfv3RestartAckStateGet, FutOspfv3RestartAckStateSet, FutOspfv3RestartAckStateTest, FutOspfv3RestartAckStateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfv3GraceLsaRetransmitCount}, NULL, FutOspfv3GraceLsaRetransmitCountGet, FutOspfv3GraceLsaRetransmitCountSet, FutOspfv3GraceLsaRetransmitCountTest, FutOspfv3GraceLsaRetransmitCountDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3RestartReason}, NULL, FutOspfv3RestartReasonGet, FutOspfv3RestartReasonSet, FutOspfv3RestartReasonTest, FutOspfv3RestartReasonDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FutOspfv3ExtTraceLevel}, NULL, FutOspfv3ExtTraceLevelGet, FutOspfv3ExtTraceLevelSet, FutOspfv3ExtTraceLevelTest, FutOspfv3ExtTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3SetTraps}, NULL, FutOspfv3SetTrapsGet, FutOspfv3SetTrapsSet, FutOspfv3SetTrapsTest, FutOspfv3SetTrapsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3HotStandbyAdminStatus}, NULL, FutOspfv3HotStandbyAdminStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3HotStandbyState}, NULL, FutOspfv3HotStandbyStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3DynamicBulkUpdStatus}, NULL, FutOspfv3DynamicBulkUpdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3StandbyHelloSyncCount}, NULL, FutOspfv3StandbyHelloSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3StandbyLsaSyncCount}, NULL, FutOspfv3StandbyLsaSyncCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,FutOspfv3BfdStatus}, NULL, FutOspfv3BfdStatusGet, FutOspfv3BfdStatusSet, FutOspfv3BfdStatusTest, FutOspfv3BfdStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3BfdAllIfState}, NULL, FutOspfv3BfdAllIfStateGet, FutOspfv3BfdAllIfStateSet, FutOspfv3BfdAllIfStateTest, FutOspfv3BfdAllIfStateDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FutOspfv3RouterIdPermanence}, NULL, FutOspfv3RouterIdPermanenceGet, FutOspfv3RouterIdPermanenceSet, FutOspfv3RouterIdPermanenceTest, FutOspfv3RouterIdPermanenceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,FutOspfv3ClearProcess}, NULL, FutOspfv3ClearProcessGet, FutOspfv3ClearProcessSet, FutOspfv3ClearProcessTest, FutOspfv3ClearProcessDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3IfIndex}, GetNextIndexFutOspfv3IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfOperState}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfOperStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfPassive}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfPassiveGet, FutOspfv3IfPassiveSet, FutOspfv3IfPassiveTest, FutOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfv3IfNbrCount}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfNbrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfAdjCount}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfAdjCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfHelloRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfHelloRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfHelloTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfHelloTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfHelloDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfHelloDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfDdpRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfDdpRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfDdpTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfDdpTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfDdpDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfDdpDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLrqRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLrqRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLrqTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLrqTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLrqDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLrqDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLsuRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLsuRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLsuTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLsuTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLsuDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLsuDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLakRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLakRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLakTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLakTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLakDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLakDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfLinkLSASuppression}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfLinkLSASuppressionGet, FutOspfv3IfLinkLSASuppressionSet, FutOspfv3IfLinkLSASuppressionTest, FutOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfv3IfBfdState}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfBfdStateGet, FutOspfv3IfBfdStateSet, FutOspfv3IfBfdStateTest, FutOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{11,FutOspfv3IfCryptoAuthType}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfCryptoAuthTypeGet, FutOspfv3IfCryptoAuthTypeSet, FutOspfv3IfCryptoAuthTypeTest, FutOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfTableINDEX, 1, 0, 0, "6"},

{{11,FutOspfv3IfCryptoAuthMode}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfCryptoAuthModeGet, FutOspfv3IfCryptoAuthModeSet, FutOspfv3IfCryptoAuthModeTest, FutOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfTableINDEX, 1, 0, 0, "3"},

{{11,FutOspfv3IfAuthTxed}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfAuthTxedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfAuthRcvd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfAuthRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3IfAuthDisd}, GetNextIndexFutOspfv3IfTable, FutOspfv3IfAuthDisdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FutOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{11,FutOspfv3RouteDestType}, GetNextIndexFutOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteDest}, GetNextIndexFutOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RoutePfxLength}, GetNextIndexFutOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteNextHopType}, GetNextIndexFutOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteNextHop}, GetNextIndexFutOspfv3RoutingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteType}, GetNextIndexFutOspfv3RoutingTable, FutOspfv3RouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteAreaId}, GetNextIndexFutOspfv3RoutingTable, FutOspfv3RouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteCost}, GetNextIndexFutOspfv3RoutingTable, FutOspfv3RouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteType2Cost}, GetNextIndexFutOspfv3RoutingTable, FutOspfv3RouteType2CostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3RouteInterfaceIndex}, GetNextIndexFutOspfv3RoutingTable, FutOspfv3RouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfv3RoutingTableINDEX, 5, 0, 0, NULL},

{{11,FutOspfv3AsExternalAggregationNetType}, GetNextIndexFutOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3AsExternalAggregationNet}, GetNextIndexFutOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3AsExternalAggregationPfxLength}, GetNextIndexFutOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3AsExternalAggregationAreaId}, GetNextIndexFutOspfv3AsExternalAggregationTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3AsExternalAggregationEffect}, GetNextIndexFutOspfv3AsExternalAggregationTable, FutOspfv3AsExternalAggregationEffectGet, FutOspfv3AsExternalAggregationEffectSet, FutOspfv3AsExternalAggregationEffectTest, FutOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, "1"},

{{11,FutOspfv3AsExternalAggregationTranslation}, GetNextIndexFutOspfv3AsExternalAggregationTable, FutOspfv3AsExternalAggregationTranslationGet, FutOspfv3AsExternalAggregationTranslationSet, FutOspfv3AsExternalAggregationTranslationTest, FutOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 0, "1"},

{{11,FutOspfv3AsExternalAggregationStatus}, GetNextIndexFutOspfv3AsExternalAggregationTable, FutOspfv3AsExternalAggregationStatusGet, FutOspfv3AsExternalAggregationStatusSet, FutOspfv3AsExternalAggregationStatusTest, FutOspfv3AsExternalAggregationTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3AsExternalAggregationTableINDEX, 4, 0, 1, NULL},

{{11,FutOspfv3BRRouteDest}, GetNextIndexFutOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteNextHopType}, GetNextIndexFutOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteNextHop}, GetNextIndexFutOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteDestType}, GetNextIndexFutOspfv3BRRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteType}, GetNextIndexFutOspfv3BRRouteTable, FutOspfv3BRRouteTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteAreaId}, GetNextIndexFutOspfv3BRRouteTable, FutOspfv3BRRouteAreaIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteCost}, GetNextIndexFutOspfv3BRRouteTable, FutOspfv3BRRouteCostGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3BRRouteInterfaceIndex}, GetNextIndexFutOspfv3BRRouteTable, FutOspfv3BRRouteInterfaceIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfv3BRRouteTableINDEX, 4, 0, 0, NULL},

{{11,FutOspfv3RedistRouteDestType}, GetNextIndexFutOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3RedistRouteDest}, GetNextIndexFutOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3RedistRoutePfxLength}, GetNextIndexFutOspfv3RedistRouteCfgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3RedistRouteMetric}, GetNextIndexFutOspfv3RedistRouteCfgTable, FutOspfv3RedistRouteMetricGet, FutOspfv3RedistRouteMetricSet, FutOspfv3RedistRouteMetricTest, FutOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, "10"},

{{11,FutOspfv3RedistRouteMetricType}, GetNextIndexFutOspfv3RedistRouteCfgTable, FutOspfv3RedistRouteMetricTypeGet, FutOspfv3RedistRouteMetricTypeSet, FutOspfv3RedistRouteMetricTypeTest, FutOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, "4"},

{{11,FutOspfv3RedistRouteTagType}, GetNextIndexFutOspfv3RedistRouteCfgTable, FutOspfv3RedistRouteTagTypeGet, FutOspfv3RedistRouteTagTypeSet, FutOspfv3RedistRouteTagTypeTest, FutOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, "1"},

{{11,FutOspfv3RedistRouteTag}, GetNextIndexFutOspfv3RedistRouteCfgTable, FutOspfv3RedistRouteTagGet, FutOspfv3RedistRouteTagSet, FutOspfv3RedistRouteTagTest, FutOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 0, "0"},

{{11,FutOspfv3RedistRouteStatus}, GetNextIndexFutOspfv3RedistRouteCfgTable, FutOspfv3RedistRouteStatusGet, FutOspfv3RedistRouteStatusSet, FutOspfv3RedistRouteStatusTest, FutOspfv3RedistRouteCfgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3RedistRouteCfgTableINDEX, 3, 0, 1, NULL},

{{11,FutOspfv3RRDStatus}, NULL, FutOspfv3RRDStatusGet, FutOspfv3RRDStatusSet, FutOspfv3RRDStatusTest, FutOspfv3RRDStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3RRDSrcProtoMask}, NULL, FutOspfv3RRDSrcProtoMaskGet, FutOspfv3RRDSrcProtoMaskSet, FutOspfv3RRDSrcProtoMaskTest, FutOspfv3RRDSrcProtoMaskDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FutOspfv3RRDRouteMapName}, NULL, FutOspfv3RRDRouteMapNameGet, FutOspfv3RRDRouteMapNameSet, FutOspfv3RRDRouteMapNameTest, FutOspfv3RRDRouteMapNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FutOspfv3RRDProtocolId}, GetNextIndexFutOspfv3RRDMetricTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3RRDMetricTableINDEX, 1, 0, 0, NULL},

{{13,FutOspfv3RRDMetricValue}, GetNextIndexFutOspfv3RRDMetricTable, FutOspfv3RRDMetricValueGet, FutOspfv3RRDMetricValueSet, FutOspfv3RRDMetricValueTest, FutOspfv3RRDMetricTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfv3RRDMetricTableINDEX, 1, 0, 0, NULL},

{{13,FutOspfv3RRDMetricType}, GetNextIndexFutOspfv3RRDMetricTable, FutOspfv3RRDMetricTypeGet, FutOspfv3RRDMetricTypeSet, FutOspfv3RRDMetricTypeTest, FutOspfv3RRDMetricTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3RRDMetricTableINDEX, 1, 0, 0, "2"},

{{12,FutOspfv3DistInOutRouteMapName}, GetNextIndexFutOspfv3DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FutOspfv3DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfv3DistInOutRouteMapType}, GetNextIndexFutOspfv3DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfv3DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfv3DistInOutRouteMapValue}, GetNextIndexFutOspfv3DistInOutRouteMapTable, FutOspfv3DistInOutRouteMapValueGet, FutOspfv3DistInOutRouteMapValueSet, FutOspfv3DistInOutRouteMapValueTest, FutOspfv3DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FutOspfv3DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FutOspfv3DistInOutRouteMapRowStatus}, GetNextIndexFutOspfv3DistInOutRouteMapTable, FutOspfv3DistInOutRouteMapRowStatusGet, FutOspfv3DistInOutRouteMapRowStatusSet, FutOspfv3DistInOutRouteMapRowStatusTest, FutOspfv3DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3DistInOutRouteMapTableINDEX, 2, 0, 1, NULL},

{{10,FutOspf3PreferenceValue}, NULL, FutOspf3PreferenceValueGet, FutOspf3PreferenceValueSet, FutOspf3PreferenceValueTest, FutOspf3PreferenceValueDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{12,FutOspfv3NbrBfdState}, GetNextIndexFutOspfv3NeighborBfdTable, FutOspfv3NbrBfdStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FutOspfv3NeighborBfdTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfv3IfAuthIfIndex}, GetNextIndexFutOspfv3IfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FutOspfv3IfAuthTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfv3IfAuthKeyId}, GetNextIndexFutOspfv3IfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfv3IfAuthTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfv3IfAuthKey}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyGet, FutOspfv3IfAuthKeySet, FutOspfv3IfAuthKeyTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 0, NULL},

{{11,FutOspfv3IfAuthKeyStartAccept}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyStartAcceptGet, FutOspfv3IfAuthKeyStartAcceptSet, FutOspfv3IfAuthKeyStartAcceptTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 0, "0000000000000000"},

{{11,FutOspfv3IfAuthKeyStartGenerate}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyStartGenerateGet, FutOspfv3IfAuthKeyStartGenerateSet, FutOspfv3IfAuthKeyStartGenerateTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 0, "0000000000000000"},

{{11,FutOspfv3IfAuthKeyStopGenerate}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyStopGenerateGet, FutOspfv3IfAuthKeyStopGenerateSet, FutOspfv3IfAuthKeyStopGenerateTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 0, "0000000000000000"},

{{11,FutOspfv3IfAuthKeyStopAccept}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyStopAcceptGet, FutOspfv3IfAuthKeyStopAcceptSet, FutOspfv3IfAuthKeyStopAcceptTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 0, "0000000000000000"},

{{11,FutOspfv3IfAuthKeyStatus}, GetNextIndexFutOspfv3IfAuthTable, FutOspfv3IfAuthKeyStatusGet, FutOspfv3IfAuthKeyStatusSet, FutOspfv3IfAuthKeyStatusTest, FutOspfv3IfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3IfAuthTableINDEX, 2, 0, 1, NULL},

{{11,FutOspfv3VirtIfAuthAreaId}, GetNextIndexFutOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3VirtIfAuthNeighbor}, GetNextIndexFutOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3VirtIfAuthKeyId}, GetNextIndexFutOspfv3VirtIfAuthTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3VirtIfAuthKey}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyGet, FutOspfv3VirtIfAuthKeySet, FutOspfv3VirtIfAuthKeyTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, NULL},

{{11,FutOspfv3VirtIfAuthKeyStartAccept}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyStartAcceptGet, FutOspfv3VirtIfAuthKeyStartAcceptSet, FutOspfv3VirtIfAuthKeyStartAcceptTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, "0000000000000000"},

{{11,FutOspfv3VirtIfAuthKeyStartGenerate}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyStartGenerateGet, FutOspfv3VirtIfAuthKeyStartGenerateSet, FutOspfv3VirtIfAuthKeyStartGenerateTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, "0000000000000000"},

{{11,FutOspfv3VirtIfAuthKeyStopGenerate}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyStopGenerateGet, FutOspfv3VirtIfAuthKeyStopGenerateSet, FutOspfv3VirtIfAuthKeyStopGenerateTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, "0000000000000000"},

{{11,FutOspfv3VirtIfAuthKeyStopAccept}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyStopAcceptGet, FutOspfv3VirtIfAuthKeyStopAcceptSet, FutOspfv3VirtIfAuthKeyStopAcceptTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 0, "0000000000000000"},

{{11,FutOspfv3VirtIfAuthKeyStatus}, GetNextIndexFutOspfv3VirtIfAuthTable, FutOspfv3VirtIfAuthKeyStatusGet, FutOspfv3VirtIfAuthKeyStatusSet, FutOspfv3VirtIfAuthKeyStatusTest, FutOspfv3VirtIfAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3VirtIfAuthTableINDEX, 3, 0, 1, NULL},

{{11,FutOspfv3VirtIfCryptoAuthType}, GetNextIndexFutOspfv3VirtIfCryptoAuthTable, FutOspfv3VirtIfCryptoAuthTypeGet, FutOspfv3VirtIfCryptoAuthTypeSet, FutOspfv3VirtIfCryptoAuthTypeTest, FutOspfv3VirtIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3VirtIfCryptoAuthTableINDEX, 2, 0, 0, "6"},

{{11,FutOspfv3VirtIfCryptoAuthMode}, GetNextIndexFutOspfv3VirtIfCryptoAuthTable, FutOspfv3VirtIfCryptoAuthModeGet, FutOspfv3VirtIfCryptoAuthModeSet, FutOspfv3VirtIfCryptoAuthModeTest, FutOspfv3VirtIfCryptoAuthTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FutOspfv3VirtIfCryptoAuthTableINDEX, 2, 0, 0, "3"},

{{11,FutOspfv3TrapNbrIfIndex}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3TrapVirtNbrRtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3TrapNbrRtrId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3TrapVirtNbrArea}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FutOspfv3TrapBulkUpdAbortReason}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fsos3Entry = { 122, fsos3MibEntry };

#endif /* _FSOS3DB_H */

