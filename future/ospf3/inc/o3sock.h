#include "fssocket.h"
#ifdef RAWSOCK_WANTED 
/* #include <netinet/in.h>
#include <errno.h>
 #include <net/if_dl.h> 
#include <sys/socket.h> */ 
#include <sys/uio.h>

#ifndef BSDCOMP_SLI_WANTED
#define CMSG_ALIGN(len) (((len) + sizeof (size_t) - 1) \
		             & (size_t) ~(sizeof (size_t) - 1))
	
#define CMSG_SPACE(len) (CMSG_ALIGN (len) \
		             + CMSG_ALIGN (sizeof (struct cmsghdr)))

#define CMSG_LEN(len)   (CMSG_ALIGN (sizeof (struct cmsghdr)) + (len))

#define CMSG_NXTHDR(mhdr, cmsg) __cmsg_nxthdr (mhdr, cmsg)

extern struct cmsghdr *__cmsg_nxthdr (struct msghdr *__mhdr,
			                      struct cmsghdr *__cmsg) __THROW;

#define	 MSG_TRUNC  0x20
#define	 MSG_CTRUNC 0x08


#endif

union controlUnion
{
    struct cmsghdr      cMsgHdr;
    char                control [CMSG_SPACE (sizeof (struct in6_addr)) +
                                 CMSG_SPACE (sizeof (tIn6Pktinfo))];
};
#endif
