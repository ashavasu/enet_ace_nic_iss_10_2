/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ospf3db.h,v 1.4 2017/12/26 13:34:25 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _OSPF3DB_H
#define _OSPF3DB_H

UINT1 Ospfv3AreaTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3AsLsdbTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3AreaLsdbTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3LinkLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3HostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Ospfv3IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 Ospfv3VirtIfTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3NbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3NbmaNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Ospfv3VirtNbrTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 Ospfv3AreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 ospf3 [] ={1,3,6,1,3,102};
tSNMP_OID_TYPE ospf3OID = {6, ospf3};


UINT4 Ospfv3RouterId [ ] ={1,3,6,1,3,102,1,1,1};
UINT4 Ospfv3AdminStat [ ] ={1,3,6,1,3,102,1,1,2};
UINT4 Ospfv3VersionNumber [ ] ={1,3,6,1,3,102,1,1,3};
UINT4 Ospfv3AreaBdrRtrStatus [ ] ={1,3,6,1,3,102,1,1,4};
UINT4 Ospfv3ASBdrRtrStatus [ ] ={1,3,6,1,3,102,1,1,5};
UINT4 Ospfv3AsScopeLsaCount [ ] ={1,3,6,1,3,102,1,1,6};
UINT4 Ospfv3AsScopeLsaCksumSum [ ] ={1,3,6,1,3,102,1,1,7};
UINT4 Ospfv3OriginateNewLsas [ ] ={1,3,6,1,3,102,1,1,8};
UINT4 Ospfv3RxNewLsas [ ] ={1,3,6,1,3,102,1,1,9};
UINT4 Ospfv3ExtLsaCount [ ] ={1,3,6,1,3,102,1,1,10};
UINT4 Ospfv3ExtAreaLsdbLimit [ ] ={1,3,6,1,3,102,1,1,11};
UINT4 Ospfv3MulticastExtensions [ ] ={1,3,6,1,3,102,1,1,12};
UINT4 Ospfv3ExitOverflowInterval [ ] ={1,3,6,1,3,102,1,1,13};
UINT4 Ospfv3DemandExtensions [ ] ={1,3,6,1,3,102,1,1,14};
UINT4 Ospfv3TrafficEngineeringSupport [ ] ={1,3,6,1,3,102,1,1,15};
UINT4 Ospfv3ReferenceBandwidth [ ] ={1,3,6,1,3,102,1,1,16};
UINT4 Ospfv3RestartSupport [ ] ={1,3,6,1,3,102,1,1,17};
UINT4 Ospfv3RestartInterval [ ] ={1,3,6,1,3,102,1,1,18};
UINT4 Ospfv3RestartStatus [ ] ={1,3,6,1,3,102,1,1,19};
UINT4 Ospfv3RestartAge [ ] ={1,3,6,1,3,102,1,1,20};
UINT4 Ospfv3RestartExitReason [ ] ={1,3,6,1,3,102,1,1,21};
UINT4 Ospfv3AreaId [ ] ={1,3,6,1,3,102,1,2,1,1};
UINT4 Ospfv3ImportAsExtern [ ] ={1,3,6,1,3,102,1,2,1,2};
UINT4 Ospfv3AreaSpfRuns [ ] ={1,3,6,1,3,102,1,2,1,3};
UINT4 Ospfv3AreaBdrRtrCount [ ] ={1,3,6,1,3,102,1,2,1,4};
UINT4 Ospfv3AreaAsBdrRtrCount [ ] ={1,3,6,1,3,102,1,2,1,5};
UINT4 Ospfv3AreaScopeLsaCount [ ] ={1,3,6,1,3,102,1,2,1,6};
UINT4 Ospfv3AreaScopeLsaCksumSum [ ] ={1,3,6,1,3,102,1,2,1,7};
UINT4 Ospfv3AreaSummary [ ] ={1,3,6,1,3,102,1,2,1,8};
UINT4 Ospfv3AreaStatus [ ] ={1,3,6,1,3,102,1,2,1,9};
UINT4 Ospfv3StubMetric [ ] ={1,3,6,1,3,102,1,2,1,10};
UINT4 Ospfv3AreaNssaTranslatorRole [ ] ={1,3,6,1,3,102,1,2,1,11};
UINT4 Ospfv3AreaNssaTranslatorState [ ] ={1,3,6,1,3,102,1,2,1,12};
UINT4 Ospfv3AreaNssaTranslatorStabilityInterval [ ] ={1,3,6,1,3,102,1,2,1,13};
UINT4 Ospfv3AreaNssaTranslatorEvents [ ] ={1,3,6,1,3,102,1,2,1,14};
UINT4 Ospfv3AreaStubMetricType [ ] ={1,3,6,1,3,102,1,2,1,15};
UINT4 Ospfv3AreaDfInfOriginate [ ] ={1,3,6,1,3,102,1,2,1,16};
UINT4 Ospfv3AsLsdbType [ ] ={1,3,6,1,3,102,1,3,1,1};
UINT4 Ospfv3AsLsdbRouterId [ ] ={1,3,6,1,3,102,1,3,1,2};
UINT4 Ospfv3AsLsdbLsid [ ] ={1,3,6,1,3,102,1,3,1,3};
UINT4 Ospfv3AsLsdbSequence [ ] ={1,3,6,1,3,102,1,3,1,4};
UINT4 Ospfv3AsLsdbAge [ ] ={1,3,6,1,3,102,1,3,1,5};
UINT4 Ospfv3AsLsdbChecksum [ ] ={1,3,6,1,3,102,1,3,1,6};
UINT4 Ospfv3AsLsdbAdvertisement [ ] ={1,3,6,1,3,102,1,3,1,7};
UINT4 Ospfv3AsLsdbTypeKnown [ ] ={1,3,6,1,3,102,1,3,1,8};
UINT4 Ospfv3AreaLsdbAreaId [ ] ={1,3,6,1,3,102,1,4,1,1};
UINT4 Ospfv3AreaLsdbType [ ] ={1,3,6,1,3,102,1,4,1,2};
UINT4 Ospfv3AreaLsdbRouterId [ ] ={1,3,6,1,3,102,1,4,1,3};
UINT4 Ospfv3AreaLsdbLsid [ ] ={1,3,6,1,3,102,1,4,1,4};
UINT4 Ospfv3AreaLsdbSequence [ ] ={1,3,6,1,3,102,1,4,1,5};
UINT4 Ospfv3AreaLsdbAge [ ] ={1,3,6,1,3,102,1,4,1,6};
UINT4 Ospfv3AreaLsdbChecksum [ ] ={1,3,6,1,3,102,1,4,1,7};
UINT4 Ospfv3AreaLsdbAdvertisement [ ] ={1,3,6,1,3,102,1,4,1,8};
UINT4 Ospfv3AreaLsdbTypeKnown [ ] ={1,3,6,1,3,102,1,4,1,9};
UINT4 Ospfv3LinkLsdbIfIndex [ ] ={1,3,6,1,3,102,1,5,1,1};
UINT4 Ospfv3LinkLsdbType [ ] ={1,3,6,1,3,102,1,5,1,2};
UINT4 Ospfv3LinkLsdbRouterId [ ] ={1,3,6,1,3,102,1,5,1,3};
UINT4 Ospfv3LinkLsdbLsid [ ] ={1,3,6,1,3,102,1,5,1,4};
UINT4 Ospfv3LinkLsdbSequence [ ] ={1,3,6,1,3,102,1,5,1,5};
UINT4 Ospfv3LinkLsdbAge [ ] ={1,3,6,1,3,102,1,5,1,6};
UINT4 Ospfv3LinkLsdbChecksum [ ] ={1,3,6,1,3,102,1,5,1,7};
UINT4 Ospfv3LinkLsdbAdvertisement [ ] ={1,3,6,1,3,102,1,5,1,8};
UINT4 Ospfv3LinkLsdbTypeKnown [ ] ={1,3,6,1,3,102,1,5,1,9};
UINT4 Ospfv3HostAddressType [ ] ={1,3,6,1,3,102,1,6,1,1};
UINT4 Ospfv3HostAddress [ ] ={1,3,6,1,3,102,1,6,1,2};
UINT4 Ospfv3HostMetric [ ] ={1,3,6,1,3,102,1,6,1,3};
UINT4 Ospfv3HostStatus [ ] ={1,3,6,1,3,102,1,6,1,4};
UINT4 Ospfv3HostAreaID [ ] ={1,3,6,1,3,102,1,6,1,5};
UINT4 Ospfv3IfIndex [ ] ={1,3,6,1,3,102,1,7,1,1};
UINT4 Ospfv3IfAreaId [ ] ={1,3,6,1,3,102,1,7,1,2};
UINT4 Ospfv3IfType [ ] ={1,3,6,1,3,102,1,7,1,3};
UINT4 Ospfv3IfAdminStat [ ] ={1,3,6,1,3,102,1,7,1,4};
UINT4 Ospfv3IfRtrPriority [ ] ={1,3,6,1,3,102,1,7,1,5};
UINT4 Ospfv3IfTransitDelay [ ] ={1,3,6,1,3,102,1,7,1,6};
UINT4 Ospfv3IfRetransInterval [ ] ={1,3,6,1,3,102,1,7,1,7};
UINT4 Ospfv3IfHelloInterval [ ] ={1,3,6,1,3,102,1,7,1,8};
UINT4 Ospfv3IfRtrDeadInterval [ ] ={1,3,6,1,3,102,1,7,1,9};
UINT4 Ospfv3IfPollInterval [ ] ={1,3,6,1,3,102,1,7,1,10};
UINT4 Ospfv3IfState [ ] ={1,3,6,1,3,102,1,7,1,11};
UINT4 Ospfv3IfDesignatedRouter [ ] ={1,3,6,1,3,102,1,7,1,12};
UINT4 Ospfv3IfBackupDesignatedRouter [ ] ={1,3,6,1,3,102,1,7,1,14};
UINT4 Ospfv3IfEvents [ ] ={1,3,6,1,3,102,1,7,1,15};
UINT4 Ospfv3IfStatus [ ] ={1,3,6,1,3,102,1,7,1,17};
UINT4 Ospfv3IfMulticastForwarding [ ] ={1,3,6,1,3,102,1,7,1,18};
UINT4 Ospfv3IfDemand [ ] ={1,3,6,1,3,102,1,7,1,19};
UINT4 Ospfv3IfMetricValue [ ] ={1,3,6,1,3,102,1,7,1,20};
UINT4 Ospfv3IfLinkScopeLsaCount [ ] ={1,3,6,1,3,102,1,7,1,21};
UINT4 Ospfv3IfLinkLsaCksumSum [ ] ={1,3,6,1,3,102,1,7,1,22};
UINT4 Ospfv3IfInstId [ ] ={1,3,6,1,3,102,1,7,1,23};
UINT4 Ospfv3IfDemandNbrProbe [ ] ={1,3,6,1,3,102,1,7,1,24};
UINT4 Ospfv3IfDemandNbrProbeRetxLimit [ ] ={1,3,6,1,3,102,1,7,1,25};
UINT4 Ospfv3IfDemandNbrProbeInterval [ ] ={1,3,6,1,3,102,1,7,1,26};
UINT4 Ospfv3VirtIfAreaId [ ] ={1,3,6,1,3,102,1,8,1,1};
UINT4 Ospfv3VirtIfNeighbor [ ] ={1,3,6,1,3,102,1,8,1,2};
UINT4 Ospfv3VirtIfIndex [ ] ={1,3,6,1,3,102,1,8,1,3};
UINT4 Ospfv3VirtIfTransitDelay [ ] ={1,3,6,1,3,102,1,8,1,4};
UINT4 Ospfv3VirtIfRetransInterval [ ] ={1,3,6,1,3,102,1,8,1,5};
UINT4 Ospfv3VirtIfHelloInterval [ ] ={1,3,6,1,3,102,1,8,1,6};
UINT4 Ospfv3VirtIfRtrDeadInterval [ ] ={1,3,6,1,3,102,1,8,1,7};
UINT4 Ospfv3VirtIfState [ ] ={1,3,6,1,3,102,1,8,1,8};
UINT4 Ospfv3VirtIfEvents [ ] ={1,3,6,1,3,102,1,8,1,9};
UINT4 Ospfv3VirtIfStatus [ ] ={1,3,6,1,3,102,1,8,1,10};
UINT4 Ospfv3VirtIfLinkScopeLsaCount [ ] ={1,3,6,1,3,102,1,8,1,11};
UINT4 Ospfv3VirtIfLinkLsaCksumSum [ ] ={1,3,6,1,3,102,1,8,1,12};
UINT4 Ospfv3NbrIfIndex [ ] ={1,3,6,1,3,102,1,9,1,1};
UINT4 Ospfv3NbrRtrId [ ] ={1,3,6,1,3,102,1,9,1,2};
UINT4 Ospfv3NbrAddressType [ ] ={1,3,6,1,3,102,1,9,1,3};
UINT4 Ospfv3NbrAddress [ ] ={1,3,6,1,3,102,1,9,1,4};
UINT4 Ospfv3NbrOptions [ ] ={1,3,6,1,3,102,1,9,1,5};
UINT4 Ospfv3NbrPriority [ ] ={1,3,6,1,3,102,1,9,1,6};
UINT4 Ospfv3NbrState [ ] ={1,3,6,1,3,102,1,9,1,7};
UINT4 Ospfv3NbrEvents [ ] ={1,3,6,1,3,102,1,9,1,8};
UINT4 Ospfv3NbrLsRetransQLen [ ] ={1,3,6,1,3,102,1,9,1,9};
UINT4 Ospfv3NbrHelloSuppressed [ ] ={1,3,6,1,3,102,1,9,1,10};
UINT4 Ospfv3NbrIfId [ ] ={1,3,6,1,3,102,1,9,1,11};
UINT4 Ospfv3NbrRestartHelperStatus [ ] ={1,3,6,1,3,102,1,9,1,12};
UINT4 Ospfv3NbrRestartHelperAge [ ] ={1,3,6,1,3,102,1,9,1,13};
UINT4 Ospfv3NbrRestartHelperExitReason [ ] ={1,3,6,1,3,102,1,9,1,14};
UINT4 Ospfv3NbmaNbrIfIndex [ ] ={1,3,6,1,3,102,1,10,1,1};
UINT4 Ospfv3NbmaNbrAddressType [ ] ={1,3,6,1,3,102,1,10,1,2};
UINT4 Ospfv3NbmaNbrAddress [ ] ={1,3,6,1,3,102,1,10,1,3};
UINT4 Ospfv3NbmaNbrPriority [ ] ={1,3,6,1,3,102,1,10,1,4};
UINT4 Ospfv3NbmaNbrRtrId [ ] ={1,3,6,1,3,102,1,10,1,5};
UINT4 Ospfv3NbmaNbrState [ ] ={1,3,6,1,3,102,1,10,1,6};
UINT4 Ospfv3NbmaNbrStorageType [ ] ={1,3,6,1,3,102,1,10,1,7};
UINT4 Ospfv3NbmaNbrStatus [ ] ={1,3,6,1,3,102,1,10,1,8};
UINT4 Ospfv3VirtNbrArea [ ] ={1,3,6,1,3,102,1,11,1,1};
UINT4 Ospfv3VirtNbrRtrId [ ] ={1,3,6,1,3,102,1,11,1,2};
UINT4 Ospfv3VirtNbrIfIndex [ ] ={1,3,6,1,3,102,1,11,1,3};
UINT4 Ospfv3VirtNbrAddressType [ ] ={1,3,6,1,3,102,1,11,1,4};
UINT4 Ospfv3VirtNbrAddress [ ] ={1,3,6,1,3,102,1,11,1,5};
UINT4 Ospfv3VirtNbrOptions [ ] ={1,3,6,1,3,102,1,11,1,6};
UINT4 Ospfv3VirtNbrState [ ] ={1,3,6,1,3,102,1,11,1,7};
UINT4 Ospfv3VirtNbrEvents [ ] ={1,3,6,1,3,102,1,11,1,8};
UINT4 Ospfv3VirtNbrLsRetransQLen [ ] ={1,3,6,1,3,102,1,11,1,9};
UINT4 Ospfv3VirtNbrHelloSuppressed [ ] ={1,3,6,1,3,102,1,11,1,10};
UINT4 Ospfv3VirtNbrIfId [ ] ={1,3,6,1,3,102,1,11,1,11};
UINT4 Ospfv3VirtNbrRestartHelperStatus [ ] ={1,3,6,1,3,102,1,11,1,12};
UINT4 Ospfv3VirtNbrRestartHelperAge [ ] ={1,3,6,1,3,102,1,11,1,13};
UINT4 Ospfv3VirtNbrRestartHelperExitReason [ ] ={1,3,6,1,3,102,1,11,1,14};
UINT4 Ospfv3AreaAggregateAreaID [ ] ={1,3,6,1,3,102,1,12,1,1};
UINT4 Ospfv3AreaAggregateAreaLsdbType [ ] ={1,3,6,1,3,102,1,12,1,2};
UINT4 Ospfv3AreaAggregatePrefixType [ ] ={1,3,6,1,3,102,1,12,1,4};
UINT4 Ospfv3AreaAggregatePrefix [ ] ={1,3,6,1,3,102,1,12,1,5};
UINT4 Ospfv3AreaAggregatePrefixLength [ ] ={1,3,6,1,3,102,1,12,1,6};
UINT4 Ospfv3AreaAggregateStatus [ ] ={1,3,6,1,3,102,1,12,1,7};
UINT4 Ospfv3AreaAggregateEffect [ ] ={1,3,6,1,3,102,1,12,1,8};
UINT4 Ospfv3AreaAggregateRouteTag [ ] ={1,3,6,1,3,102,1,12,1,9};


tMbDbEntry ospf3MibEntry[]= {

{{9,Ospfv3RouterId}, NULL, Ospfv3RouterIdGet, Ospfv3RouterIdSet, Ospfv3RouterIdTest, Ospfv3RouterIdDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3AdminStat}, NULL, Ospfv3AdminStatGet, Ospfv3AdminStatSet, Ospfv3AdminStatTest, Ospfv3AdminStatDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3VersionNumber}, NULL, Ospfv3VersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3AreaBdrRtrStatus}, NULL, Ospfv3AreaBdrRtrStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3ASBdrRtrStatus}, NULL, Ospfv3ASBdrRtrStatusGet, Ospfv3ASBdrRtrStatusSet, Ospfv3ASBdrRtrStatusTest, Ospfv3ASBdrRtrStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3AsScopeLsaCount}, NULL, Ospfv3AsScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3AsScopeLsaCksumSum}, NULL, Ospfv3AsScopeLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3OriginateNewLsas}, NULL, Ospfv3OriginateNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RxNewLsas}, NULL, Ospfv3RxNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3ExtLsaCount}, NULL, Ospfv3ExtLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3ExtAreaLsdbLimit}, NULL, Ospfv3ExtAreaLsdbLimitGet, Ospfv3ExtAreaLsdbLimitSet, Ospfv3ExtAreaLsdbLimitTest, Ospfv3ExtAreaLsdbLimitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3MulticastExtensions}, NULL, Ospfv3MulticastExtensionsGet, Ospfv3MulticastExtensionsSet, Ospfv3MulticastExtensionsTest, Ospfv3MulticastExtensionsDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3ExitOverflowInterval}, NULL, Ospfv3ExitOverflowIntervalGet, Ospfv3ExitOverflowIntervalSet, Ospfv3ExitOverflowIntervalTest, Ospfv3ExitOverflowIntervalDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3DemandExtensions}, NULL, Ospfv3DemandExtensionsGet, Ospfv3DemandExtensionsSet, Ospfv3DemandExtensionsTest, Ospfv3DemandExtensionsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3TrafficEngineeringSupport}, NULL, Ospfv3TrafficEngineeringSupportGet, Ospfv3TrafficEngineeringSupportSet, Ospfv3TrafficEngineeringSupportTest, Ospfv3TrafficEngineeringSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3ReferenceBandwidth}, NULL, Ospfv3ReferenceBandwidthGet, Ospfv3ReferenceBandwidthSet, Ospfv3ReferenceBandwidthTest, Ospfv3ReferenceBandwidthDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RestartSupport}, NULL, Ospfv3RestartSupportGet, Ospfv3RestartSupportSet, Ospfv3RestartSupportTest, Ospfv3RestartSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RestartInterval}, NULL, Ospfv3RestartIntervalGet, Ospfv3RestartIntervalSet, Ospfv3RestartIntervalTest, Ospfv3RestartIntervalDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RestartStatus}, NULL, Ospfv3RestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RestartAge}, NULL, Ospfv3RestartAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{9,Ospfv3RestartExitReason}, NULL, Ospfv3RestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{10,Ospfv3AreaId}, GetNextIndexOspfv3AreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3ImportAsExtern}, GetNextIndexOspfv3AreaTable, Ospfv3ImportAsExternGet, Ospfv3ImportAsExternSet, Ospfv3ImportAsExternTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "1"},

{{10,Ospfv3AreaSpfRuns}, GetNextIndexOspfv3AreaTable, Ospfv3AreaSpfRunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaBdrRtrCount}, GetNextIndexOspfv3AreaTable, Ospfv3AreaBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaAsBdrRtrCount}, GetNextIndexOspfv3AreaTable, Ospfv3AreaAsBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaScopeLsaCount}, GetNextIndexOspfv3AreaTable, Ospfv3AreaScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaScopeLsaCksumSum}, GetNextIndexOspfv3AreaTable, Ospfv3AreaScopeLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaSummary}, GetNextIndexOspfv3AreaTable, Ospfv3AreaSummaryGet, Ospfv3AreaSummarySet, Ospfv3AreaSummaryTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "2"},

{{10,Ospfv3AreaStatus}, GetNextIndexOspfv3AreaTable, Ospfv3AreaStatusGet, Ospfv3AreaStatusSet, Ospfv3AreaStatusTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 1, NULL},

{{10,Ospfv3StubMetric}, GetNextIndexOspfv3AreaTable, Ospfv3StubMetricGet, Ospfv3StubMetricSet, Ospfv3StubMetricTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaNssaTranslatorRole}, GetNextIndexOspfv3AreaTable, Ospfv3AreaNssaTranslatorRoleGet, Ospfv3AreaNssaTranslatorRoleSet, Ospfv3AreaNssaTranslatorRoleTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "2"},

{{10,Ospfv3AreaNssaTranslatorState}, GetNextIndexOspfv3AreaTable, Ospfv3AreaNssaTranslatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaNssaTranslatorStabilityInterval}, GetNextIndexOspfv3AreaTable, Ospfv3AreaNssaTranslatorStabilityIntervalGet, Ospfv3AreaNssaTranslatorStabilityIntervalSet, Ospfv3AreaNssaTranslatorStabilityIntervalTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "40"},

{{10,Ospfv3AreaNssaTranslatorEvents}, GetNextIndexOspfv3AreaTable, Ospfv3AreaNssaTranslatorEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3AreaTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3AreaStubMetricType}, GetNextIndexOspfv3AreaTable, Ospfv3AreaStubMetricTypeGet, Ospfv3AreaStubMetricTypeSet, Ospfv3AreaStubMetricTypeTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "1"},

{{10,Ospfv3AreaDfInfOriginate}, GetNextIndexOspfv3AreaTable, Ospfv3AreaDfInfOriginateGet, Ospfv3AreaDfInfOriginateSet, Ospfv3AreaDfInfOriginateTest, Ospfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaTableINDEX, 1, 0, 0, "2"},

{{10,Ospfv3AsLsdbType}, GetNextIndexOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbRouterId}, GetNextIndexOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbLsid}, GetNextIndexOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbSequence}, GetNextIndexOspfv3AsLsdbTable, Ospfv3AsLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbAge}, GetNextIndexOspfv3AsLsdbTable, Ospfv3AsLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbChecksum}, GetNextIndexOspfv3AsLsdbTable, Ospfv3AsLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbAdvertisement}, GetNextIndexOspfv3AsLsdbTable, Ospfv3AsLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AsLsdbTypeKnown}, GetNextIndexOspfv3AsLsdbTable, Ospfv3AsLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3AsLsdbTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3AreaLsdbAreaId}, GetNextIndexOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbType}, GetNextIndexOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbRouterId}, GetNextIndexOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbLsid}, GetNextIndexOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbSequence}, GetNextIndexOspfv3AreaLsdbTable, Ospfv3AreaLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbAge}, GetNextIndexOspfv3AreaLsdbTable, Ospfv3AreaLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbChecksum}, GetNextIndexOspfv3AreaLsdbTable, Ospfv3AreaLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbAdvertisement}, GetNextIndexOspfv3AreaLsdbTable, Ospfv3AreaLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3AreaLsdbTypeKnown}, GetNextIndexOspfv3AreaLsdbTable, Ospfv3AreaLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3AreaLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbIfIndex}, GetNextIndexOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbType}, GetNextIndexOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbRouterId}, GetNextIndexOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbLsid}, GetNextIndexOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbSequence}, GetNextIndexOspfv3LinkLsdbTable, Ospfv3LinkLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbAge}, GetNextIndexOspfv3LinkLsdbTable, Ospfv3LinkLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbChecksum}, GetNextIndexOspfv3LinkLsdbTable, Ospfv3LinkLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbAdvertisement}, GetNextIndexOspfv3LinkLsdbTable, Ospfv3LinkLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3LinkLsdbTypeKnown}, GetNextIndexOspfv3LinkLsdbTable, Ospfv3LinkLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{10,Ospfv3HostAddressType}, GetNextIndexOspfv3HostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3HostTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3HostAddress}, GetNextIndexOspfv3HostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ospfv3HostTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3HostMetric}, GetNextIndexOspfv3HostTable, Ospfv3HostMetricGet, Ospfv3HostMetricSet, Ospfv3HostMetricTest, Ospfv3HostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3HostTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3HostStatus}, GetNextIndexOspfv3HostTable, Ospfv3HostStatusGet, Ospfv3HostStatusSet, Ospfv3HostStatusTest, Ospfv3HostTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3HostTableINDEX, 2, 0, 1, NULL},

{{10,Ospfv3HostAreaID}, GetNextIndexOspfv3HostTable, Ospfv3HostAreaIDGet, Ospfv3HostAreaIDSet, Ospfv3HostAreaIDTest, Ospfv3HostTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Ospfv3HostTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3IfIndex}, GetNextIndexOspfv3IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfAreaId}, GetNextIndexOspfv3IfTable, Ospfv3IfAreaIdGet, Ospfv3IfAreaIdSet, Ospfv3IfAreaIdTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "0"},

{{10,Ospfv3IfType}, GetNextIndexOspfv3IfTable, Ospfv3IfTypeGet, Ospfv3IfTypeSet, Ospfv3IfTypeTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfAdminStat}, GetNextIndexOspfv3IfTable, Ospfv3IfAdminStatGet, Ospfv3IfAdminStatSet, Ospfv3IfAdminStatTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfRtrPriority}, GetNextIndexOspfv3IfTable, Ospfv3IfRtrPriorityGet, Ospfv3IfRtrPrioritySet, Ospfv3IfRtrPriorityTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "1"},

{{10,Ospfv3IfTransitDelay}, GetNextIndexOspfv3IfTable, Ospfv3IfTransitDelayGet, Ospfv3IfTransitDelaySet, Ospfv3IfTransitDelayTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "1"},

{{10,Ospfv3IfRetransInterval}, GetNextIndexOspfv3IfTable, Ospfv3IfRetransIntervalGet, Ospfv3IfRetransIntervalSet, Ospfv3IfRetransIntervalTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "5"},

{{10,Ospfv3IfHelloInterval}, GetNextIndexOspfv3IfTable, Ospfv3IfHelloIntervalGet, Ospfv3IfHelloIntervalSet, Ospfv3IfHelloIntervalTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "10"},

{{10,Ospfv3IfRtrDeadInterval}, GetNextIndexOspfv3IfTable, Ospfv3IfRtrDeadIntervalGet, Ospfv3IfRtrDeadIntervalSet, Ospfv3IfRtrDeadIntervalTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "40"},

{{10,Ospfv3IfPollInterval}, GetNextIndexOspfv3IfTable, Ospfv3IfPollIntervalGet, Ospfv3IfPollIntervalSet, Ospfv3IfPollIntervalTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "120"},

{{10,Ospfv3IfState}, GetNextIndexOspfv3IfTable, Ospfv3IfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfDesignatedRouter}, GetNextIndexOspfv3IfTable, Ospfv3IfDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfBackupDesignatedRouter}, GetNextIndexOspfv3IfTable, Ospfv3IfBackupDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfEvents}, GetNextIndexOspfv3IfTable, Ospfv3IfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfStatus}, GetNextIndexOspfv3IfTable, Ospfv3IfStatusGet, Ospfv3IfStatusSet, Ospfv3IfStatusTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 1, NULL},

{{10,Ospfv3IfMulticastForwarding}, GetNextIndexOspfv3IfTable, Ospfv3IfMulticastForwardingGet, Ospfv3IfMulticastForwardingSet, Ospfv3IfMulticastForwardingTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "1"},

{{10,Ospfv3IfDemand}, GetNextIndexOspfv3IfTable, Ospfv3IfDemandGet, Ospfv3IfDemandSet, Ospfv3IfDemandTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "2"},

{{10,Ospfv3IfMetricValue}, GetNextIndexOspfv3IfTable, Ospfv3IfMetricValueGet, Ospfv3IfMetricValueSet, Ospfv3IfMetricValueTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfLinkScopeLsaCount}, GetNextIndexOspfv3IfTable, Ospfv3IfLinkScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfLinkLsaCksumSum}, GetNextIndexOspfv3IfTable, Ospfv3IfLinkLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3IfTableINDEX, 1, 0, 0, NULL},

{{10,Ospfv3IfInstId}, GetNextIndexOspfv3IfTable, Ospfv3IfInstIdGet, Ospfv3IfInstIdSet, Ospfv3IfInstIdTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "0"},

{{10,Ospfv3IfDemandNbrProbe}, GetNextIndexOspfv3IfTable, Ospfv3IfDemandNbrProbeGet, Ospfv3IfDemandNbrProbeSet, Ospfv3IfDemandNbrProbeTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "2"},

{{10,Ospfv3IfDemandNbrProbeRetxLimit}, GetNextIndexOspfv3IfTable, Ospfv3IfDemandNbrProbeRetxLimitGet, Ospfv3IfDemandNbrProbeRetxLimitSet, Ospfv3IfDemandNbrProbeRetxLimitTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "10"},

{{10,Ospfv3IfDemandNbrProbeInterval}, GetNextIndexOspfv3IfTable, Ospfv3IfDemandNbrProbeIntervalGet, Ospfv3IfDemandNbrProbeIntervalSet, Ospfv3IfDemandNbrProbeIntervalTest, Ospfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ospfv3IfTableINDEX, 1, 0, 0, "120"},

{{10,Ospfv3VirtIfAreaId}, GetNextIndexOspfv3VirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfNeighbor}, GetNextIndexOspfv3VirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfIndex}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfIndexGet, Ospfv3VirtIfIndexSet, Ospfv3VirtIfIndexTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfTransitDelay}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfTransitDelayGet, Ospfv3VirtIfTransitDelaySet, Ospfv3VirtIfTransitDelayTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 0, "1"},

{{10,Ospfv3VirtIfRetransInterval}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfRetransIntervalGet, Ospfv3VirtIfRetransIntervalSet, Ospfv3VirtIfRetransIntervalTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 0, "5"},

{{10,Ospfv3VirtIfHelloInterval}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfHelloIntervalGet, Ospfv3VirtIfHelloIntervalSet, Ospfv3VirtIfHelloIntervalTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 0, "10"},

{{10,Ospfv3VirtIfRtrDeadInterval}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfRtrDeadIntervalGet, Ospfv3VirtIfRtrDeadIntervalSet, Ospfv3VirtIfRtrDeadIntervalTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 0, "60"},

{{10,Ospfv3VirtIfState}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfEvents}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfStatus}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfStatusGet, Ospfv3VirtIfStatusSet, Ospfv3VirtIfStatusTest, Ospfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3VirtIfTableINDEX, 2, 0, 1, NULL},

{{10,Ospfv3VirtIfLinkScopeLsaCount}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfLinkScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtIfLinkLsaCksumSum}, GetNextIndexOspfv3VirtIfTable, Ospfv3VirtIfLinkLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3VirtIfTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrIfIndex}, GetNextIndexOspfv3NbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrRtrId}, GetNextIndexOspfv3NbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrAddressType}, GetNextIndexOspfv3NbrTable, Ospfv3NbrAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrAddress}, GetNextIndexOspfv3NbrTable, Ospfv3NbrAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrOptions}, GetNextIndexOspfv3NbrTable, Ospfv3NbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrPriority}, GetNextIndexOspfv3NbrTable, Ospfv3NbrPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrState}, GetNextIndexOspfv3NbrTable, Ospfv3NbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrEvents}, GetNextIndexOspfv3NbrTable, Ospfv3NbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrLsRetransQLen}, GetNextIndexOspfv3NbrTable, Ospfv3NbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrHelloSuppressed}, GetNextIndexOspfv3NbrTable, Ospfv3NbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrIfId}, GetNextIndexOspfv3NbrTable, Ospfv3NbrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrRestartHelperStatus}, GetNextIndexOspfv3NbrTable, Ospfv3NbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrRestartHelperAge}, GetNextIndexOspfv3NbrTable, Ospfv3NbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbrRestartHelperExitReason}, GetNextIndexOspfv3NbrTable, Ospfv3NbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3NbmaNbrIfIndex}, GetNextIndexOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3NbmaNbrAddressType}, GetNextIndexOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3NbmaNbrAddress}, GetNextIndexOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3NbmaNbrPriority}, GetNextIndexOspfv3NbmaNbrTable, Ospfv3NbmaNbrPriorityGet, Ospfv3NbmaNbrPrioritySet, Ospfv3NbmaNbrPriorityTest, Ospfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, "1"},

{{10,Ospfv3NbmaNbrRtrId}, GetNextIndexOspfv3NbmaNbrTable, Ospfv3NbmaNbrRtrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3NbmaNbrState}, GetNextIndexOspfv3NbmaNbrTable, Ospfv3NbmaNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{10,Ospfv3NbmaNbrStorageType}, GetNextIndexOspfv3NbmaNbrTable, Ospfv3NbmaNbrStorageTypeGet, Ospfv3NbmaNbrStorageTypeSet, Ospfv3NbmaNbrStorageTypeTest, Ospfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3NbmaNbrTableINDEX, 3, 0, 0, "3"},

{{10,Ospfv3NbmaNbrStatus}, GetNextIndexOspfv3NbmaNbrTable, Ospfv3NbmaNbrStatusGet, Ospfv3NbmaNbrStatusSet, Ospfv3NbmaNbrStatusTest, Ospfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3NbmaNbrTableINDEX, 3, 0, 1, NULL},

{{10,Ospfv3VirtNbrArea}, GetNextIndexOspfv3VirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrRtrId}, GetNextIndexOspfv3VirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrIfIndex}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrAddressType}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrAddress}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrOptions}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrState}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrEvents}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrLsRetransQLen}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrHelloSuppressed}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrIfId}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrRestartHelperStatus}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrRestartHelperAge}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3VirtNbrRestartHelperExitReason}, GetNextIndexOspfv3VirtNbrTable, Ospfv3VirtNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ospfv3VirtNbrTableINDEX, 2, 0, 0, NULL},

{{10,Ospfv3AreaAggregateAreaID}, GetNextIndexOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, NULL},

{{10,Ospfv3AreaAggregateAreaLsdbType}, GetNextIndexOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, NULL},

{{10,Ospfv3AreaAggregatePrefixType}, GetNextIndexOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, NULL},

{{10,Ospfv3AreaAggregatePrefix}, GetNextIndexOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, NULL},

{{10,Ospfv3AreaAggregatePrefixLength}, GetNextIndexOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, NULL},

{{10,Ospfv3AreaAggregateStatus}, GetNextIndexOspfv3AreaAggregateTable, Ospfv3AreaAggregateStatusGet, Ospfv3AreaAggregateStatusSet, Ospfv3AreaAggregateStatusTest, Ospfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaAggregateTableINDEX, 5, 0, 1, NULL},

{{10,Ospfv3AreaAggregateEffect}, GetNextIndexOspfv3AreaAggregateTable, Ospfv3AreaAggregateEffectGet, Ospfv3AreaAggregateEffectSet, Ospfv3AreaAggregateEffectTest, Ospfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, "1"},

{{10,Ospfv3AreaAggregateRouteTag}, GetNextIndexOspfv3AreaAggregateTable, Ospfv3AreaAggregateRouteTagGet, Ospfv3AreaAggregateRouteTagSet, Ospfv3AreaAggregateRouteTagTest, Ospfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Ospfv3AreaAggregateTableINDEX, 5, 0, 0, "0"},
};
tMibData ospf3Entry = { 147, ospf3MibEntry };
#endif /* _OSPF3DB_H */

