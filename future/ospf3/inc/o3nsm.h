/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: o3nsm.h,v 1.2 2007/02/01 15:01:10 iss Exp $
 *
 * Description:This file contains constants, macros, typdefinitions
 *             and static varible declaration specific to osnsm.c
 *
 *******************************************************************/

#ifndef _O3NSM_H
#define _O3NSM_H


/* NSM Action (NA) routine numbers */
#define  OS3_NA0           0  
#define  OS3_NA1           1  
#define  OS3_NA2           2  
#define  OS3_NA3           3  
#define  OS3_NA4           4  
#define  OS3_NA5           5  
#define  OS3_NA6           6  
#define  OS3_NA7           7  
#define  OS3_NA8           8  
#define  OS3_NA9           9  
#define  OS3_NA10          10 
#define  OS3_NA11          11 
#define  OS3_NA12          12 
#define  OS3_NA13          13 
#define  OS3_NA14          14 
#define  OS3_NA15          15 
#define  OSPFV3_MAX_NSM_FUNC  16 

/* the neighbor state machine transition table */

static UINT1  gau1Os3NsmTable[OSPFV3_MAX_NBR_EVENT][OSPFV3_MAX_NBR_STATE]  = { 
/*______________________________________________________________________________

    DOWN      ATTEMPT    INIT       2WAY    EXSTRT    EXCHNG   LOADING    FULL
________________________________________________________________________________
HELLO RCVD */ 
 { OS3_NA10, OS3_NA12, OS3_NA11, OS3_NA11, OS3_NA11, OS3_NA11, OS3_NA11, OS3_NA11 },
/*______________________________________________________________________________
START */
 { OS3_NA9,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0 },
/*______________________________________________________________________________
2WAY RCVD */
 { OS3_NA0,  OS3_NA0,  OS3_NA8,  OS3_NA14, OS3_NA14, OS3_NA14, OS3_NA14, OS3_NA14 },
/*______________________________________________________________________________
NEG DONE */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA7,  OS3_NA0,  OS3_NA0,  OS3_NA0 },
/*______________________________________________________________________________
EXCHANGE DONE */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA6,  OS3_NA0,  OS3_NA0 },
/*______________________________________________________________________________
BAD LSREQ */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA3,  OS3_NA3,  OS3_NA3 },
/*______________________________________________________________________________
LDNG DONE */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA13, OS3_NA0 },
/*______________________________________________________________________________
ADJ OK?  */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA5,  OS3_NA4,  OS3_NA4,  OS3_NA4,  OS3_NA4 },
/*______________________________________________________________________________
SEQ NUM MISMATCH */
 { OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA0,  OS3_NA3,  OS3_NA3,  OS3_NA3 },
/*______________________________________________________________________________
1-WAY */
 { OS3_NA0,  OS3_NA0,  OS3_NA14, OS3_NA2,  OS3_NA2,  OS3_NA2,  OS3_NA2,  OS3_NA2 },
/*______________________________________________________________________________
KILL NBR */
 { OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1 },
/*______________________________________________________________________________
INACT TIMER */
 { OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1,  OS3_NA1 },
/*______________________________________________________________________________
LLDOWN */
 { OS3_NA15, OS3_NA15, OS3_NA15, OS3_NA15, OS3_NA15, OS3_NA15, OS3_NA15, OS3_NA15 }
/*______________________________________________________________________________
*/
                            } ;

/* type of the functions which perform the action in NBRSM */
typedef  void (*tV3OsNsmFunc)(tV3OsNeighbor *);

/* array of pointers to functions performing actions during NBRSM transitions */

tV3OsNsmFunc gau1Os3NsmFunc[OSPFV3_MAX_NSM_FUNC] = {
                         /* OS3_NA0 */           V3NsmInvalid,
                         /* OS3_NA1 */           V3NsmDown,
                         /* OS3_NA2 */           V3Nsm1wayRcvd,
                         /* OS3_NA3 */           V3NsmRestartAdj,
                         /* OS3_NA4 */           V3NsmCheckAdj,
                         /* OS3_NA5 */           V3NsmProcessAdj,
                         /* OS3_NA6 */           V3NsmExchgDone,
                         /* OS3_NA7 */           V3NsmNegDone,
                         /* OS3_NA8 */           V3Nsm2wayRcvd,
                         /* OS3_NA9 */           V3NsmStart,
                         /* OS3_NA10*/           V3NsmStartInactTimer,
                         /* OS3_NA11*/           V3NsmRestartInactTimer,
                         /* OS3_NA12*/           V3NsmAttHelloRcvd,    
                         /* OS3_NA13*/           V3NsmLdngDone,
                         /* OS3_NA14*/           V3NsmNoAction,
                         /* OS3_NA15*/           V3NsmLlDown
                                               };

#endif /* _O3NSM_H */
