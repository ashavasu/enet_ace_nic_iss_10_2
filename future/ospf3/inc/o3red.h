/********************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *   $Id: o3red.h,v 1.6 2018/02/01 11:02:57 siva Exp $
 *
 * Description:This file contains constants, macros
 *             and static varible declaration specific to OSPFv3 Rm
 *
 *
 ***********************************************************************/


#ifndef _O3RED_H
#define _O3RED_H

#define OSPFV3_RED_MTU_SIZE               1500 - RM_HDR_LENGTH
#define OSPFV3_RED_MSG_HDR_LEN            3
#define OSPFV3_RED_BULK_LEN_OFFSET        1
#define OSPFV3_RED_BULK_NO_OF_PKT_OFFSET  4

/* RM MESSAGE TYPES */
/* The first three messages corresponds to the RM message types
 * 1 - Bulk request
 * 2 - Bulk updates
 * 3 - Bulk tail
 * So, the dynamic message type starts from 4
 */


#define OSPFV3_RED_HELLO      4
#define OSPFV3_RED_LSU        5
#define OSPFV3_RED_NBR_STATE  6
#define OSPFV3_RED_LS_ACK     7
#define OSPFV3_RED_INTF       8
#define OSPFV3_RED_RTR_ID     9
#define OSPFV3_RED_CLEAR      10
  

enum {

 OSPFV3_RED_INIT = 1,           /* The OSPFv3 instance is up */

 OSPFV3_RED_ACTIVE_STANDBY_UP, /* The current OSPFv3 instance is up and
           functioning as active node and its peer
           is functioning as the standby */

 OSPFV3_RED_ACTIVE_STANDBY_DOWN, /* The current OSPFv3 instance is up
      and functioning as active node but
      its peer is down */

 OSPFV3_RED_STANDBY              /* The node is in standby state */
};


/* SUB BULK MESSAGE TYPE */
#define OSPFV3_RED_BULK_HELLO             1
#define OSPFV3_RED_BULK_INTF              2
#define OSPFV3_RED_BULK_NBR_STATE         3
#define OSPFV3_RED_BULK_LSU               4
#define OSPFV3_RED_BULK_DB_TIMER          5
#define OSPFV3_RED_BULK_STAB_TIMER        6
#define OSPFV3_RED_BULK_ROUTER_ID         7

/* Maximum time to relinquish the current bulk update process (1 sec) */
#define OSPFV3_RED_BULK_UPDT_REL_TIME     1

/* Maximum time (Max UINT4 value) */
#define OSPFV3_MAX_TIME                   0xFFFFFFFF

/* Definitions for LSU bulk update and dynamic updates */
/* Maximum LSA to be sent at a stretch during sub bulk update event */
#define OSPFV3_MAX_LSA_PER_BULK_EVENT     15
/* Assuming maximum size of AS scope LSA to be 100 bytes. One bulk update
 * can have a maximum of 10 10 12 bulk updates. This count is chosen by
 * assuming to send 5 bulk updates at a stretch
 */
#define OSPFV3_MAX_AS_LSA_PER_BULK_UPDT   50


/* Fixed size of Nbr Hello Info =
 * Size of UINT2 (for storing the PDU Length) +
 * Size of UINT4 (for storing the if index)+
 * tIp6Addr for IPv6 Neighbor Source Address
 * */
#define OSPFV3_FIXED_NBR_HELLO_SIZE      sizeof (UINT2) \
                                          + sizeof (UINT4)\
                                          + sizeof (tIp6Addr)

/* Size of interface info in the sync-up message
 * context id (4) + interface id (4) + MTU (4) + speed (4) +
 * Deletion status (1) + oper status (1) +
 * interface state (1) + DR router id (4) + DR interface id (4) +
 * BDR router id (4) + BDR interface id (4)
 */
#define OSPFV3_INTF_INFO_SIZE            sizeof (UINT4) + sizeof (UINT4) + \
                                         sizeof (UINT4) + sizeof (UINT4) + \
                                         sizeof (UINT1) + sizeof (UINT1) + \
                                         sizeof (UINT1) + \
                                         sizeof (UINT4) + sizeof (UINT4) + \
                                         sizeof (UINT4) + sizeof (UINT4)
                                         
/* Fixed size of Nbr Hello Info =
 * Size of UINT4 (Neighbor Id) +
 * Size of UINT4 (for if index)+
 * Size of UINT1 (for neighbor state)
 */
/* Sizeof the nbr info structure - 2 bytes used for reseverd */
#define OSPFV3_FIXED_NBR_INFO_SIZE      sizeof (tV3OsRmNbrState) - 2

/* Fixed size of bulk update packet
 * LSA scope + Context id + network type + Storage id + overflow state +
 * transit area id + destination router id + Rxmt flag +
 * Arrival time + Age + functional equaivalent flag + LSA length
 */
#define OSPFV3_FIXED_LSU_BULK_UPDT_LEN    sizeof (UINT1) + sizeof (UINT4) \
                                          + sizeof (UINT1) + sizeof (UINT4) \
                                          + sizeof (UINT1) + sizeof (UINT4) \
                                          + sizeof (UINT4) + sizeof (UINT1) \
                                          + sizeof (UINT4) + sizeof (UINT2) \
                                          + sizeof (UINT1) + sizeof (UINT2)

/* Fixed size of dynamic LSU
 * LSA Type +Nbr If index + Nbr router id + Rxmt flag + LSU Arrival time + LSU length
 */
#define OSPFV3_FIXED_LSU_DYNM_UPDT_LEN    sizeof (UINT1) + sizeof (UINT4) + \
                                          sizeof (UINT4) + sizeof (UINT1) + \
                                          sizeof (UINT4) + sizeof (UINT2)

/* Size for nbr dynamic size
 * Type (1) + Length field (2) + context id (4) + network type (1)
 * + Transit area id (4) + Destination router id (4) + interface index (4)
 * + nbr router id (4) + state (1)
 */
#define OSPFV3_DYNAMIC_NBR_SIZE           sizeof (UINT1) + sizeof (UINT2) + \
                                          sizeof (UINT4) + sizeof (UINT1) + \
                                          sizeof (UINT4) + sizeof (UINT4) + \
                                          sizeof (UINT4) + sizeof (UINT4) + \
                                          sizeof (UINT1)

/* Size of DB timer info
 * size of message type + size of context id + size of remaining time
 */
#define OSPFV3_DB_TIMER_INFO              sizeof (UINT1) + sizeof (UINT4) + \
                                          sizeof (UINT4)


/* size of message type + size of message len + router id + size of context Id */
      
#define OSPFV3_ROUTER_ID_INFO              sizeof (UINT1) + sizeof (UINT2) + \
                                          sizeof (UINT4) + sizeof (UINT4)

#define OSPFV3_CLEAR_INFO                 sizeof (UINT1) + sizeof (UINT2) + \
                                          sizeof (UINT1) + sizeof (UINT4)


/* Size of NSSA stability timer info
 * size of message type + size of context id + size of area id 
 * size of remaining time
 */
#define OSPFV3_STAB_TIMER_INFO            sizeof (UINT1) + sizeof (UINT4) + \
                                          sizeof (UINT4) + sizeof (UINT4)

#define OSPFV3_RED_MSG_VALUE_OFFSET       1


/* Byte length in RM packets */
#define OSPFV3_BULK_REQ_LEN               1
#define OSPFV3_BULK_TAIL_LEN              1


/* Bulk update status */
#define OSPFV3_BULK_UPDT_NOT_STARTED      1
#define OSPFV3_BULK_UPDT_NBR              2
#define OSPFV3_BULK_UPDT_VIRT_NBR         3
#define OSPFV3_BULK_UPDT_LSU              4
#define OSPFV3_BULK_UPDT_ROUTERID         5
#define OSPFV3_BULK_UPDT_COMPLETED        6
#define OSPFV3_BULK_UPDT_ABORTED          7
/* RM PACKET CONSTRUCTION MACROS */
#define OSPFV3_RED_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define OSPFV3_RED_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define OSPFV3_RED_PUT_4_BYTE(pMsg, u4Offset, u4Value) \
{\
    RM_DATA_ASSIGN_4_BYTE (pMsg, u4Offset, u4Value); \
    u4Offset += 4; \
}

#define OSPFV3_RED_PUT_N_BYTE(pdest, psrc, u4Offset, u4Size) \
{\
    RM_COPY_TO_OFFSET(pdest, psrc, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define OSPFV3_RED_GET_1_BYTE(pMsg, u4Offset, u1MesgType) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1MesgType); \
    u4Offset += 1; \
}

#define OSPFV3_RED_GET_2_BYTE(pMsg, u4Offset, u2MesgType) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2MesgType); \
    u4Offset += 2; \
}

#define OSPFV3_RED_GET_4_BYTE(pMsg, u4Offset, u4MesgType) \
{\
    RM_GET_DATA_4_BYTE (pMsg, u4Offset, u4MesgType); \
    u4Offset += 4; \
}

#define OSPFV3_RED_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}

#define OSPFV3_IS_STANDBY_UP() \
 ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_ACTIVE_STANDBY_UP) \
  ? OSPFV3_TRUE : OSPFV3_FALSE)


#define OSPFv3_IS_NODE_ACTIVE() \
 ((gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_ACTIVE_STANDBY_UP || \
  gV3OsRtr.ospfRedInfo.u4RmState == OSPFV3_RED_ACTIVE_STANDBY_DOWN) \
  ? OSPFV3_TRUE : OSPFV3_FALSE)


/* Prototypes */
/*********************** o3redrm.c ****************************************/
PUBLIC VOID O3RedRmHandleRmEvents PROTO ((tV3OsRmCtrlMsg * pRmCtrlMsg));

PUBLIC VOID O3RedRmConstructLsaDesc
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsRmLsdbInfo * pLsdbInfo,
        tV3OsInterface * pInterface, tV3OsArea * pArea));

PUBLIC INT4 O3RedRmAddToRxmtLst PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC VOID O3RedRmSendEvent PROTO ((UINT4 u4Event, UINT4 u4Error));

PUBLIC VOID O3RedRmCheckNbrs PROTO ((tV3OsInterface * pInterface));


PUBLIC VOID O3ExecuteCmdAndCalculateChkSum PROTO ((VOID));

PUBLIC VOID V3OspfRmHandleGoActiveEventNotify PROTO ((VOID));
/*********************** o3redblk.c ***************************************/
PUBLIC VOID O3RedBlkSendBulkUpdate PROTO ((VOID));

PUBLIC VOID O3RedBlkSendSubBulkUpdate PROTO ((VOID));

PUBLIC VOID O3RedBlkSendBulkRequest PROTO ((VOID));

PUBLIC VOID O3RedBlkProcessBulkUpdt
PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen));

PUBLIC VOID O3RedBlkProcessBulkTail PROTO ((VOID));

PUBLIC INT4
O3RedProcessRouterIdInfo PROTO((tRmMsg * pRmMsg));

PUBLIC INT4
O3RedSendRouterIdInfo PROTO ((INT4 *pi4SendSubBulkEvent));


/*********************** o3reddyn.c ***************************************/
PUBLIC VOID O3RedDynProcessLsuUpdt PROTO ((tRmMsg * pRmMsg));

PUBLIC VOID O3RedDynSendHelloToStandby
PROTO ((tV3OsNeighbor * pNbr));

PUBLIC VOID O3RedDynProcessHello PROTO ((tRmMsg *pBuf));

PUBLIC VOID O3RedDynSendLsAckInfoToStandby
PROTO ((tV3OsRmLsAckDetails * pLsAckInfo));

PUBLIC VOID O3RedDynProcessNbrState PROTO ((tRmMsg * pRmMsg));

PUBLIC VOID O3RedDynSendNbrStateToStandby
PROTO ((tV3OsNeighbor * pNbr));

PUBLIC VOID
O3RedDynLsuUpdAckToStandby PROTO ((tV3OsLsaInfo *  pLsaInfo));

PUBLIC VOID
O3RedDynProcessLsAckInfo PROTO ((tRmMsg * pRmMsg));

PUBLIC INT4
O3RedDynConstructAndSendLsa PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC INT4
O3RedDynSendIntfInfoToStandby PROTO ((tV3OsInterface * pInterface,
                                      UINT1 u1Flag));

PUBLIC INT4 O3RedDynProcessIntfInfo PROTO ((tRmMsg * pRmMsg));


PUBLIC INT4
O3RedDynSendRtrIdToStandby PROTO ((tV3OsRouterId RtrId, UINT4 u4ContextId));

PUBLIC VOID
O3RedDynProcessRouterIdInfo PROTO ((tRmMsg * pRmMsg));

PUBLIC INT4
O3RedDynOspfClearToStandby PROTO ((UINT1 u1Flag, UINT4 u4ContextId));

PUBLIC VOID
O3RedDynOspfClearProcess PROTO ((tRmMsg * pRmMsg));

/*********************** o3redadj.c ***************************************/
PUBLIC INT4 O3RedAdjSendBulkNbrInfo PROTO ((INT4 *pi4SendSubBulkEvent));

PUBLIC INT4
O3RedAdjProcessBulkHello PROTO((tRmMsg * pMsg, UINT4 *pu4Offset));

PUBLIC VOID O3RedAdjAddLastHelloDetails
PROTO ((tV3OsNeighbor *pNbr,UINT4 u4InterfaceId,
        tIp6Addr *pSrcIp6Addr, UINT1 *pHelloPkt,UINT2 u2PktLen));

PUBLIC INT4
O3RedAdjSendNbrInfo PROTO((tV3OsInterface *pInterface, tRmMsg ** ppBuf,
                     UINT4 *pu4Offset, UINT4 *pu4PktLen));
PUBLIC INT4
O3RedAdjConstructNbrHelloInfo PROTO ((tV3OsNeighbor *pNbr, tRmMsg *pBuf, 
                               UINT4 *pu4Offset, UINT4 *pu4PktLen));
PUBLIC INT4
O3RedAdjConstructNbrState PROTO((tV3OsNeighbor *pNbr, tRmMsg *pBuf, 
                                 UINT4 *pu4Offset, UINT4 *pu4PktLen));

PUBLIC INT4
O3RedAdjAllocateBulkUpdatePkt  
PROTO ((tRmMsg **ppBuf, UINT4 *pu4PktLen, UINT4 *pu4Offset));

PUBLIC VOID O3RedAdjProcessNbrInfo PROTO ((tRmMsg * pMsg));

PUBLIC INT4 O3RedAdjSendBulkVirtNbrInfo PROTO ((INT4 *pi4SendSubBulkEvent));

PUBLIC INT4
O3RedAdjSendVirtNbrInfo PROTO ((tV3OsInterface *pInterface, tRmMsg **pBuf,
                                UINT4 *pu4Offset, UINT4 *pu4PktLen));
/*********************** o3redlsu.c ***************************************/
PUBLIC INT4 O3RedLsuSendLsu PROTO ((INT4 *pi4SendSubBulkEvent));

PUBLIC VOID O3RedLsuProcessLsuBlkUpdt
PROTO ((tRmMsg * pRmMsg, UINT2 u2DataLen));

PUBLIC INT4 O3RedLsuConstructLsuUpdt
PROTO ((tV3OsLsaInfo * pLsaInfo, tRmMsg * pRmMsg, UINT2 *pu2Len));

PUBLIC INT4 O3RedLsuGetLsuFromUpdt
PROTO ((tRmMsg * pRmMsg, tV3OsRmLsdbInfo * pLsdbInfo, UINT2 *pu2ReadLen));

PUBLIC INT4 O3RedLsuProcessLsu PROTO ((tV3OsRmLsdbInfo * pLsdbInfo));

PUBLIC VOID O3RedLsuSendSelfOriginatedLsa PROTO ((tV3OspfCxt * pV3OspfCxt));

/*********************** o3redutl.c ***************************************/

PUBLIC VOID O3RedUtlResetHsVariables PROTO ((VOID));

PUBLIC INT4
V3GetShowCmdOutputAndCalcChkSum PROTO ((UINT2 *pu2SwAudChkSum));

PUBLIC VOID O3RedUtlResetHsLastInfo PROTO ((VOID));

PUBLIC VOID O3RedUtlSetLsaId
PROTO ((UINT4 u4LsaId, tV3OsRmLsdbInfo * pLsdbInfo,
        tV3OsLsaInfo * pLsaInfo));

PUBLIC INT4
O3RedUtlSendBulkUpdate PROTO ((tRmMsg *pBuf, UINT4 u4PktLen));

PUBLIC INT4 O3RedUtlBulkUpdtRelinquish PROTO ((VOID));

PUBLIC VOID O3RedUtlSetAssoLsaId
PROTO ((tV3OsLinkStateId linkStateId, tV3OsRmLsdbInfo * pLsdbInfo,
        tV3OsLsaInfo * pLsaInfo));
INT4
V3RmEnqChkSumMsgToRm PROTO ((UINT2 u2AppId, UINT2 u2ChkSum));

/*********************** o3redif.c ***************************************/
PUBLIC INT4
O3RedIfSendIntfInfo PROTO ((tV3OsInterface * pInterface, tRmMsg ** ppBuf,
                            UINT4 * pu4Offset, UINT4 * pu4PktLen));

PUBLIC INT4
O3RedIfConstructIntfInfo PROTO ((tV3OsInterface * pInterface, tRmMsg * pBuf,
                                 UINT4 * pu4Offset, UINT1 u1Flag));

PUBLIC INT4
O3RedIfProcessIntfInfo PROTO ((tRmMsg * pBuf, UINT4 * pu4Offset));

PUBLIC VOID
O3RedIfProcessVirtIfOnActive PROTO ((tV3OspfCxt * pV3OspfCxt));

/*********************** o3redtmr.c ***************************************/

PUBLIC INT4
O3RedTmrSendTimerInfo PROTO ((INT4 *pi4SendSubBulkEvent));

PUBLIC INT4
O3RedTmrProcessTimerInfo PROTO ((tRmMsg * pRmMsg));

#endif
