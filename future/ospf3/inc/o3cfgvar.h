/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *   $Id: o3cfgvar.h,v 1.8 2017/12/26 13:34:24 siva Exp $
 *
 *   Description:
 *
 **********************************************************************/

#ifndef _O3CFGVAR_H
#define _O3CFGVAR_H 

/****************************************************************************/
 /* Configurtion constants; porting sensitive                                */
 /****************************************************************************/
 
#define  OSPFV3_MAX_EXT_LSAS       MAX_OSPFV3_EXT_ROUTES 
#define  OSPFV3_MAX_ROUTES         MAX_OSPFV3_INTERNAL_ROUTES + OSPFV3_MAX_LEARNT_EXT_ROUTES 





 

#define  OSPFV3_MAX_NEXT_HOPS      16                                           


/* 
 * no. of hash buckets = 
 * total no. of objects stored / no. of objects per hash bucket;
 * no. of objects per hash bucket is chosen to be 16
 * it is subject to change
 */
#define  OSPFV3_BUCKET_SIZE  16 
 
#define  OSPFV3_EXT_LSA_HASH_TABLE_SIZE  ((OSPFV3_MAX_EXT_LSAS / OSPFV3_BUCKET_SIZE) + 1)    
#define  OSPFV3_CANDTE_HASH_TABLE_SIZE   ((MAX_OSPFV3_SPF_NODES / OSPFV3_BUCKET_SIZE) + 1)
#define  OSPFV3_LSA_HASH_TABLE_SIZE      ((OSPFV3_MAX_LSAS_PER_AREA / OSPFV3_BUCKET_SIZE) + 1)

#define  OSPFV3_MAX_RTR_LSA_INFO      ((OSPFV3_ROUTER_LSA_SIZE - (OSPFV3_LS_HEADER_SIZE + OSPFV3_OPTION_LEN + 1)) / \
                                       (sizeof (tV3OsRtrLsaRtInfo) - sizeof (tRBNodeEmbd) - sizeof (UINT1)))
#define  OSPFV3_MAX_NW_LSA_INFO      ((OSPFV3_MAX_LSA_SIZE - (OSPFV3_LS_HEADER_SIZE + OSPFV3_OPTION_LEN + 1)) / \
                                      (sizeof (tV3OsNwLsaRtInfo) - sizeof (tRBNodeEmbd)))
#define  OSPFV3_MAX_LSAID_INFO       ((OSPFV3_MAX_LSAS_PER_AREA * MAX_OSPFV3_AREAS + OSPFV3_MAX_EXT_LSAS) / \
                                      ((sizeof (tV3OsGRLsIdInfo) - sizeof (tRBNodeEmbd) - sizeof (UINT1))))
#define  OSPFV3_MAX_GRACE_LSA_SENT   180 /* Maximum number of grace LSA can be sent out */

#endif
