/********************************************************************
 *      Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *       $Id: fso3tewr.h,v 1.3 2017/12/26 13:34:24 siva Exp $
 *     
 *       Description:
 *
 **************************************************************************/



#ifndef _FSO3TEWR_H
#define _FSO3TEWR_H
INT4 GetNextIndexFutOspfv3TestIfTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSO3TE(VOID);

VOID UnRegisterFSO3TE(VOID);
INT4 FutOspfv3TestDemandTrafficGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3TestDemandTrafficSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3TestDemandTrafficTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfv3TestIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFutOspfv3ExtRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FutOspfv3ExtRouteMetricGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteMetricSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FutOspfv3ExtRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSO3TEWR_H */
