/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3defn.h,v 1.27 2018/01/16 10:46:01 siva Exp $
 *
 * Description:This file contains all constants relating to
 *             protocol and the implementation of it.
 *
 *******************************************************************/
#ifndef _O3DEFN_H
#define _O3DEFN_H

/****************************************************************************/
/* Architectural Constants                                                  */
/****************************************************************************/
#define  OSPFV3_LSA_REFRESH_INTERVAL    (OSPFV3_NO_OF_TICKS_PER_SEC * 30* 60)
#define  OSPFV3_MIN_LSA_ARRIVAL         (OSPFV3_NO_OF_TICKS_PER_SEC * 1)
#define  OSPFV3_MIN_LSA_INTERVAL_VALUE  5
#define  OSPFV3_MAX_AGE                 (60 * 60)
#define  OSPFV3_CHECK_AGE               (5 * 60)
#define  OSPFV3_MAX_AGE_DIFF            (15 * 60)

/****************************************************************************/
/* Multiple Interfaces over a link */
/****************************************************************************/ 
#define  OSPFV3_MAX_IF_OVER_LINK        8 /* Max no.of sby interfaces over
                                           a link */
#define  OSPFV3_DEF_IFOVERLINK_COUNT    1 /* Default no.of interfaces over
                                           a link */

/****************************************************************************/
/* General  Constants                                                  */
/****************************************************************************/

#define  OSPFV3_INIT_VAL                       0
#define  OSPFV3_ONE                            1
#define  OSPFV3_TWO                            2
#define  OSPFV3_THREE                          3
#define  OSPFV3_FOUR                           4
#define  OSPFV3_VERSION_NO                     3
#define  OSPFV3_INSTANCE_ID                    0
#define  OSPFV3_HEADER_SIZE                    16

/* OSPFV3 packet types */
#define  OSPFV3_HELLO_PKT                      1
#define  OSPFV3_DD_PKT                         2
#define  OSPFV3_LSA_REQ_PKT                    3
#define  OSPFV3_LS_UPDATE_PKT                  4
#define  OSPFV3_LSA_ACK_PKT                    5
#define  OSPFV3_MAX_PKT_TYPE                   6

/* Different Types of ABR Types supported */
#define  OSPFV3_STANDARD_ABR            1
#define  OSPFV3_CISCO_ABR               2
#define  OSPFV3_IBM_ABR                 3
#define  OSPFV3_DEF_ABR_TYPE OSPFV3_STANDARD_ABR

/****************************************************************************/
/* Link State Database related constants                                   */
/****************************************************************************/

#define  OSPFV3_LS_HEADER_SIZE                         20
#define  OSPFV3_LS_TYPE_LEN                            2
#define  OSPFV3_AGE_SIZE_IN_LS_HEADER                  2

/* Offset for Link state Header fields */ 
#define  OSPFV3_AGE_OFFSET_IN_LS_HEADER              0
#define  OSPFV3_LS_SEQ_NUM_OFFSET_IN_LS_HEADER       12
#define  OSPFV3_CHKSUM_OFFSET_IN_LS_HEADER           16
#define  OSPFV3_LENGTH_OFFSET_IN_LS_HEADER           18

/* LSA comparision */
#define  OSPFV3_LSA_EQUAL                      0
#define  OSPFV3_LSA1                           1
#define  OSPFV3_LSA2                           2

#define OSPFV3_UPDATE_LSAS                     0
#define OSPFV3_UPDATE_METRIC                   1
/* LSA types */
#define  OSPFV3_ROUTER_LSA                    0x2001
#define  OSPFV3_NETWORK_LSA                   0x2002
#define  OSPFV3_INTER_AREA_PREFIX_LSA         0x2003
#define  OSPFV3_INTER_AREA_ROUTER_LSA         0x2004
#define  OSPFV3_AS_EXT_LSA                    0x4005
#define  OSPFV3_NSSA_LSA               0x2007
#define  OSPFV3_LINK_LSA                      0x0008
#define  OSPFV3_STANDBY_LINK_LSA              0x0009
#define  OSPFV3_INTRA_AREA_PREFIX_LSA         0x2009
#define  OSPFV3_COND_INTER_AREA_PREFIX_LSA    0x2010
#define  OSPFV3_DEFAULT_INTER_AREA_PREFIX_LSA 0x2011
#define  OSPFV3_DEFAULT_NSSA_LSA              0x2012
#define  OSPFV3_COND_NSSA_LSA        0x2013
#define  OSPFV3_INDICATION_LSA                0x2014
#define  OSPFV3_AS_TRNSLTD_EXT_LSA       0x4006
#define  OSPFV3_AS_TRNSLTD_RNG_LSA            0x4007
#define  OSPFV3_COND_AS_EXT_LSA               0x4008
#define  OSPFV3_GRACE_LSA                     0x000b

/* Scope of the LSAs */
#define  OSPFV3_LINK_FLOOD_SCOPE   1
#define  OSPFV3_AREA_FLOOD_SCOPE   2
#define  OSPFV3_AS_FLOOD_SCOPE     3

/* Constants used to indicate the type of event to lsa origination procedure */
#define  OSPFV3_SIG_LS_REFRESH                 0
#define  OSPFV3_SIG_IF_STATE_CHANGE            1
#define  OSPFV3_SIG_DR_CHANGE                  2
#define  OSPFV3_SIG_NBR_STATE_CHANGE           3
#define  OSPFV3_SIG_SUMMARY_GEN                5
#define  OSPFV3_SIG_HOST_ATTACHED              8
#define  OSPFV3_SIG_HOST_DETACHED              9
#define  OSPFV3_SIG_DEFAULT_SUMMARY            10
#define  OSPFV3_SIG_VIRT_LINK_COST_CHANGE      11
#define  OSPFV3_SIG_NEXT_INSTANCE              12
#define  OSPFV3_SIG_INDICATION                 14
#define  OSPFV3_SIG_IF_PREFIX_ADDED            15
#define  OSPFV3_SIG_IF_PREFIX_DELETED          16
#define  OSPFV3_SIG_IF_PRIORITY_CHANGE         17
#define  OSPFV3_SIG_GRACE_LSA_GEN              18

/* Sequence Number */
#define  OSPFV3_MIN_SEQ_NUM                    0x80000001
#define  OSPFV3_MAX_SEQ_NUM                    0x7fffffff

/* link types to be used in router lsa */
#define  OSPFV3_PTOP_LINK                      1
#define  OSPFV3_TRANSIT_LINK                   2
#define  OSPFV3_VIRTUAL_LINK                   4

#define  OSPFV3_LSU_FIXED_PORTION_SIZE         4
#define  OSPFV3_LSA_REQ_SIZE                   12

/* constants used to identify the type of ack to be sent */
#define  OSPFV3_UNUSED_ACK_FLAG                0
#define  OSPFV3_IMPLIED_ACK                    1
#define  OSPFV3_NO_IMPLIED_ACK                 2
#define  OSPFV3_FLOOD_BACK                     3
#define  OSPFV3_NO_FLOOD_BACK                  4

/* For LSA comaprision */
#define  OSPFV3_RCVD_LSA                       1
#define  OSPFV3_DB_LSA                         2

#define  OSPFV3_REF_LS_TYPE_OFFSET             22
#define  OSPFV3_DEST_RTR_ID_OFFSET             28
#define  OSPFV3_PREFIX_LEN_OFFSET              24
#define  OSPFV3_ADDR_PREFIX_OFFSET             28
#define  OSPFV3_SUMMARY_LSA_METRIC_OFFSET      25
#define  OSPFV3_LS_PREFIX_NUM_OFFSET           40
#define  OSPFV3_LS_PREFIX_NUM_SIZE             4
#define  OSPFV3_LS_PREFIX_LEN_SIZE             1
#define  OSPFV3_LS_LEN_OPT_RES_SIZE            4

#define  OSPFV3_ZERO_LINK_RTR_LSA_SIZE         24

#define  OSPFV3_REF_LS_HDR_SIZE                12
#define  OSPFV3_MAX_NUM_PREFIX      ((OSPFV3_MAX_LSA_SIZE - \
                                     (OSPFV3_LS_HEADER_SIZE + \
                                      OSPFV3_INTRA_AREA_PFX_LSA_HDR_LEN))\
                                      / (OSPFV3_IPV6_ADDR_LEN + 4)) 

#define  OSPFV3_MAX_PREFIX_LEN                 128

/****************************************************************************/
/* ALL BIT MASK                                                             */
/****************************************************************************/

/* masks used  for flags in DDP */
#define  OSPFV3_I_BIT_MASK            4    /* 00000100 */
#define  OSPFV3_M_BIT_MASK            2    /* 00000010 */
#define  OSPFV3_MS_BIT_MASK           1    /* 00000001 */

/* constants used for setting the VEB bits in router LSAs */
#define  OSPFV3_ABR_BIT_MASK          1    /* 00000001 */
#define  OSPFV3_ASBR_BIT_MASK         2    /* 00000010 */
#define  OSPFV3_V_BIT_MASK            4    /* 00000100 */
#define  OSPFV3_NT_BIT_MASK        16   /* 00010000 */

/* Constant used for Prefix Option */
#define OSPFV3_NU_BIT_MASK            1   /* 00000001 */
#define OSPFV3_NU_BIT_CLEAR_MASK      254 /* 11111110 */
#define OSPFV3_LA_BIT_MASK            2   /* 00000010 */
#define OSPFV3_P_BIT_MASK             8   /* 00001000 */

/* constants used for setting options */
#define  OSPFV3_V6_BIT_MASK           1    /* 00000001 */
#define  OSPFV3_E_BIT_MASK            2    /* 00000010 */
#define  OSPFV3_MC_BIT_MASK           4    /* 00000100 */
#define  OSPFV3_N_BIT_MASK            8    /* 00001000 */
#define  OSPFV3_R_BIT_MASK       16   /* 00010000 */
#define  OSPFV3_DC_BIT_MASK           32   /* 00100000 */
#define  OSPFV3_AT_BIT_MASK           4   /* 00000100 */
#define  OSPFV3_L_BIT_MASK            2   /* 00000010 */

/* Constant used fir E,F,T bit for AS external and NSSA LSA */
#define OSPFV3_EXT_T_BIT_MASK         1    /* 00000001 */
#define OSPFV3_EXT_F_BIT_MASK         2    /* 00000010 */
#define OSPFV3_EXT_E_BIT_MASK         4    /* 00000100 */

/****************************************************************************/
/* Interface related constants                                              */
/****************************************************************************/

/* interface types */
#define  OSPFV3_IF_BROADCAST          1
#define  OSPFV3_IF_NBMA               2
#define  OSPFV3_IF_PTOP               3
#define  OSPFV3_IF_VIRTUAL            4
#define  OSPFV3_IF_PTOMP              5
#define  OSPFV3_IF_LOOPBACK           6
#define  OSPFV3_MAX_IF_TYPE           7

/* interface states */
#define  OSPFV3_IFS_DOWN              0
#define  OSPFV3_IFS_LOOP_BACK         1
#define  OSPFV3_IFS_WAITING           2
#define  OSPFV3_IFS_PTOP              3
#define  OSPFV3_IFS_DR                4
#define  OSPFV3_IFS_BACKUP            5
#define  OSPFV3_IFS_DR_OTHER          6
#define  OSPFV3_IFS_STANDBY           7
#define  OSPFV3_MAX_IF_STATE          8

/* interface events */
#define  OSPFV3_IFE_UP                0
#define  OSPFV3_IFE_WAIT_TIMER        1
#define  OSPFV3_IFE_BACKUP_SEEN       2
#define  OSPFV3_IFE_NBR_CHANGE        3
#define  OSPFV3_IFE_LOOP_IND          4
#define  OSPFV3_IFE_UNLOOP_IND        5
#define  OSPFV3_IFE_DOWN              6
#define  OSPFV3_MULTIF_LINK           7
#define  OSPFV3_MAX_IF_EVENT          8

/* interface cleanup operations */
#define  OSPFV3_IF_CLEANUP_DOWN       0
#define  OSPFV3_IF_CLEANUP_DISABLE    1
#define  OSPFV3_IF_CLEANUP_DELETE     2
#define  OSPFV3_IF_SBY_CLEANUP_DELETE 3

/* For Multiple Interfaces to link's operation */
#define  OSPFV3_MULTIF_ACTIVE_TMR_OFF  0 /* Active Detect timer is OFF */
#define  OSPFV3_MULTIF_ACTIVE_TMR_ON   1 /* Active Detect timer is ON  */
#define  OSPFV3_MULTIF_ACTIVE_TMR_STOP 2 /* Active Detect timer expired */

/* constants used for interface state change indications */
#define  OSPFV3_OPERUP             1
#define  OSPFV3_OPERDOWN           2
#define  OSPFV3_LOOPBACK_IND       3
#define  OSPFV3_UNLOOP_IND         4

/* For Demand Circuit */
#define  OSPFV3_DISCOVERED_ENDPOINT            1

/* For Virtual Interface cost change */
#define  OSPFV3_VIF_COST_CHANGED               8

/* constants used for defailt values for interface parameter */
#define  OSPFV3_DEF_STAG_INTERVAL              10000  /*milliseconds*/
#define  OSPFV3_DEF_HELLO_INTERVAL             10
#define  OSPFV3_DEF_VIRT_IF_RTR_DEAD_INTERVAL  60
#define  OSPFV3_DEF_RTR_DEAD_INTERVAL          40
#define  OSPFV3_DEF_IF_TRANS_DELAY             1
#define  OSPFV3_DEF_RTR_PRIORITY               1
#define  OSPFV3_DEF_RXMT_INTERVAL              5
#define  OSPFV3_DEF_POLL_INTERVAL              120
#define  OSPFV3_DEF_REF_BW               100000

/****************************************************************************/
/* Neighbor related constants                                               */
/****************************************************************************/

/*  neighbor states */
#define  OSPFV3_NBRS_DOWN                      0
#define  OSPFV3_NBRS_ATTEMPT                   1
#define  OSPFV3_NBRS_INIT                      2
#define  OSPFV3_NBRS_2WAY                      3
#define  OSPFV3_NBRS_EXSTART                   4
#define  OSPFV3_NBRS_EXCHANGE                  5
#define  OSPFV3_NBRS_LOADING                   6
#define  OSPFV3_NBRS_FULL                      7
#define  OSPFV3_MAX_NBR_STATE                  8

/* neighbor events */         
#define  OSPFV3_NBRE_HELLO_RCVD                0
#define  OSPFV3_NBRE_START                     1
#define  OSPFV3_NBRE_2WAY_RCVD                 2
#define  OSPFV3_NBRE_NEG_DONE                  3
#define  OSPFV3_NBRE_EXCHANGE_DONE             4
#define  OSPFV3_NBRE_BAD_LS_REQ                5
#define  OSPFV3_NBRE_LOADING_DONE              6
#define  OSPFV3_NBRE_ADJ_OK                    7
#define  OSPFV3_NBRE_SEQ_NUM_MISMATCH          8
#define  OSPFV3_NBRE_1WAY_RCVD                 9
#define  OSPFV3_NBRE_KILL_NBR                  10
#define  OSPFV3_NBRE_INACTIVITY_TIMER          11
#define  OSPFV3_NBRE_LL_DOWN                   12
#define  OSPFV3_MAX_NBR_EVENT                  13

/* configuration status of the neighbor */
#define  OSPFV3_DISCOVERED_NBR                 1    /* dynamic */
#define  OSPFV3_CONFIGURED_NBR                 2    /* permanent */
#define  OSPFV3_VIRTUAL_NBR                    3    /* Virtual Nbr used in HA */
#define  OSPFV3_DISCOVERED_SBYIF               4    /* Standby Interface's 
                                                     * of the link */

/* standby/active flags of an interface */
#define OSPFV3_DOWN_ON_LINK              0 /* Interface Down on link */
#define OSPFV3_ACTIVE_ON_LINK            1 /* Interface is Active over link */
#define OSPFV3_STANDBY_ON_LINK           2 /* Interface is standby over link */

/* Flag to indicate the type of Neighbor */
#define OSPFV3_NORMAL_NBR                3 /* Normal OSPFv3 Nbr */
#define OSPFV3_MAX_NBR_TYPE              4 /* Max no.of OSPFv3 nbr types */

/* For Nbr Probing */
#define  OSPFV3_DEF_NBR_PROBE_RXMT_LIMIT       10
#define  OSPFV3_DEF_NBR_PROBE_INTERVAL         120


/****************************************************************************/
/* Routing Table related constants                                          */
/****************************************************************************/
                                    
/* Types of destinations in routing table. */
#define  OSPFV3_DEST_NETWORK                   1
#define  OSPFV3_DEST_AREA_BORDER               2
#define  OSPFV3_DEST_AS_BOUNDARY               3

/* Types of paths in routing table */
#define  OSPFV3_INTRA_AREA                     1
#define  OSPFV3_INTER_AREA                     2
#define  OSPFV3_TYPE_1_EXT                     3
#define  OSPFV3_TYPE_2_EXT                     4
#define  OSPFV3_INVALID_ROUTE                  5

/* constants for vertex types in candidate list */
#define  OSPFV3_VERT_NETWORK          0
#define  OSPFV3_VERT_ROUTER           1

#define OSPFV3_NOT_FOUND_MASK         4
#define OSPFV3_FOUND                  1
#define OSPFV3_NOT_FOUND              4

#define OSPFV3_DIRECT                 1
#define OSPFV3_INDIRECT               2

#define OSPFV3_USED                   1
#define OSPFV3_NOT_USED               2

#define OSPFV3_CREATED                1
#define OSPFV3_UPDATED                2
#define OSPFV3_DELETED                3

#define OSPFV3_REF_ROUTER_LSA         OSPFV3_ROUTER_LSA
#define OSPFV3_REF_NETWORK_LSA        OSPFV3_NETWORK_LSA

#define OSPFV3_NO_CHANGE              1

#define OSPFV3_CAND_NODE_KEY_LEN      9

#define OSPFV3_NEW_PATH               1
#define OSPFV3_REPLACE_PATH           2
#define OSPFV3_ADD_HOPS               3

/****************************************************************************/
/* Area related constants */
/****************************************************************************/

#define  OSPFV3_NO_AREA_SUMMARY                1
#define  OSPFV3_SEND_AREA_SUMMARY              2

#define  OSPFV3_SUMMARY_LINK                   3
#define  OSPFV3_NSSA_EXTERNAL_LINK             7

#define  OSPFV3_ADVERTISE_MATCHING             1
#define  OSPFV3_DO_NOT_ADVERTISE_MATCHING      2

/* Constants specifying different OSPF area types */
#define OSPFV3_NORMAL_AREA          1
#define OSPFV3_STUB_AREA                2
#define OSPFV3_NSSA_AREA                3
#define OSPFV3_MAX_AREA_TYPE            4

#define OSPFV3_DEFAULT_SUMM_COST                1
#define OSPFV3_DEF_HOST_METRIC                  10
#define OSPFV3_MAX_AUTHKEYS_PER_IF         5
/* row creation/deletion values as defined in MIB */
#define  OSPFV3_VALID              1
#define  OSPFV3_INVALID            2

#define  OSPFV3_AUTH_MODE_TRANS    2
#define  OSPFV3_AUTH_MODE_FULL     1
#define  OSPFV3_AUTH_MODE_NONE     3
#define  OSPFV3_AUTH_MODE_DEF      OSPFV3_AUTH_MODE_FULL

/* authentication types as per MIB */
#define  OSPFV3_NO_AUTH       2
#define  OSPFV3_CRYPT_AUTH    1

/* Constants are used in manipulating the number of days in a year */
enum {
OSPFV3_MAX_MONTH_DAYS = 31,
OSPFV3_MIN_MONTH_DAYS = 30,
OSPFV3_MAX_LEAP_FEB_DAYS = 29,
OSPFV3_MIN_LEAP_FEB_DAYS = 28,
OSPFV3_MAX_MAR_DAYS = 62,
OSPFV3_MAX_APR_DAYS = 92,
OSPFV3_MAX_MAY_DAYS = 123,
OSPFV3_MAX_JUN_DAYS = 153,
OSPFV3_MAX_JUL_DAYS = 184,
OSPFV3_MAX_AUG_DAYS = 215,
OSPFV3_MAX_SEP_DAYS = 245,
OSPFV3_MAX_OCT_DAYS = 276,
OSPFV3_MAX_NOV_DAYS = 306,
OSPFV3_MAX_MIN = 59,
OSPFV3_MAX_HOUR = 23
};

/* Time Buffer related macros */
#define OSPFV3_TMP_NUM_STR    7
#define OSPFV3_INVALID_HOUR   8
#define OSPFV3_INVALID_MIN    7
#define OSPFV3_INVALID_SEC    6
#define OSPFV3_INVALID_DATE   5
#define OSPFV3_INVALID_MONTH  4
#define OSPFV3_INVALID_YEAR   3
#define OSPFV3_ONE            1
#define OSPFV3_BASE_YEAR       2000


/****************************************************************************/
/* NSSA Area related constants */
/****************************************************************************/

/* NSSA Translator States */
#define OSPFV3_TRNSLTR_STATE_ENAELCTD   2
#define OSPFV3_TRNSLTR_STATE_DISABLED   3

/* NSSA Translator Role */
#define OSPFV3_TRNSLTR_ROLE_ALWAYS   1
#define OSPFV3_TRNSLTR_ROLE_CANDIDATE   2

#define OSPFV3_NSSA_TRNSLTR_STBLTY_INTRVL              40
#define OSPFV3_NSSA_INVALID_STBLTY_INTRVL              -1

/* Default Infomation Originate */
#define OSPFV3_DEFAULT_INFO_ORIGINATE      1
#define OSPFV3_NO_DEFAULT_INFO_ORIGINATE   2

/* Constants used for route calculation staggering */
#define OSPFV3_STAGGERING_ENABLED  1
#define OSPFV3_STAGGERING_DISABLED 2

#define OSPFV3_MIN_STAGGERING_INTERVAL 1000 /* milliseconds */
#define OSPFV3_MAX_STAGGERING_INTERVAL 2147483647 /* milliseconds */

 /* constans used to indicate route calculation current status */
#define OSPFV3_ROUTE_CALC_COMPLETED    1
#define OSPFV3_ROUTE_CALC_SUSPENDED    0
#define OSPFV3_ROUTE_CALC_IN_PROGRESS  2

/****************************************************************************/
/* Aggregation                                                             */
/****************************************************************************/

/* Masks for route aggregation*/
#define OSPFV3_RAG_TRANS_MASK            2  /* 00000010 */
#define OSPFV3_RAG_ADV_MASK              4  /* 00000100 */
#define OSPFV3_RAG_DONOTADV_MASK         8  /* 00001000 */
#define OSPFV3_RAG_ALLOW_MASK            16 /* 00010000 */
#define OSPFV3_RAG_DENY_MASK             32 /* 00100000 */

#define OSPFV3_RAG_TRANS_RESET          253
#define OSPFV3_RAG_EFFECT_FND            60

#define OSPFV3_RAG_ADVERTISE              1
#define OSPFV3_RAG_DO_NOT_ADVERTISE       2
#define OSPFV3_RAG_ALLOW_ALL              3
#define OSPFV3_RAG_DENY_ALL               4
#define OSPFV3_RAG_NO_CHNG                0

/****************************************************************************/
/* Miscellaneous                                                           */
/****************************************************************************/

#define  OSPFV3_IP6_ADDR_ACCESS_IN_SHORT_INT    8
#define  OSPFV3_MAX_PERCENTAGE                  100
#define  OSPFV3_INVALID_TIMER_TYPE              -1

#define  OSPFV3_IF_METRIC_LEN       2
#define  OSPFV3_RTR_ID_LEN          4
#define  OSPFV3_AREA_ID_LEN         4
#define  OSPFV3_LINKSTATE_ID_LEN    4
#define  OSPFV3_OPTION_LEN          3
#define  OSPFV3_BIG_METRIC_SIZE     3

#define OSPFV3_IFACE_MTU(p_iface)   (p_iface->u4MtuSize)

#define OSPFV3_ADD                  1
#define OSPFV3_DELETE               2

/* constants used in setting flags during DR election */
#define  OSPFV3_ALL_RTRS                 0
#define  OSPFV3_ONLY_BDRS                1

#define  OSPFV3_REDISTRIBUTED_TYPE5      3

/* row creation/deletion values as defined in MIB */
#define  OSPFV3_VALID              1
#define  OSPFV3_INVALID            2
 
#define OSPFV3_METRIC              1
#define OSPFV3_TYPE1EXT_METRIC     2
#define OSPFV3_TYPE2EXT_METRIC     3

#define  OSPFV3_LS_INFINITY_16BIT           0xffff   /* used in router LSA  */
#define  OSPFV3_LS_INFINITY_24BIT           0xffffff /* used in  ext LSA
            Inter Intra Extrenal LSA*/
#define  OSPFV3_DO_NOT_AGE                   0x8000
#define  OSPFV3_FLUSHED_LSA                  4

#define  OSPFV3_HELLO_FIXED_PORTION_SIZE       20
#define  OSPFV3_DDP_FIXED_PORTION_SIZE         12

#define  OSPFV3_VIRTUAL_IF_RXMT_INTERVAL       5

#define  OSPFV3_INELIGIBLE_RTR_PRIORITY        0
                                    
#define  OSPFV3_IPV6_ADDR_LEN                  16
#define  OSPFV3_MAX_ADDR_LEN         32

#define  OSPFV3_IPV4_ADDR_LEN        4
/* Maximum size of Hello packet
 * OSPF header size + Hello packet fixed portion +
 * maximum no of nbrs supported * sizeof (rtrid)
 */
#define  OSPFV3_MAX_HELLO_PKT_LEN         OSPFV3_HEADER_SIZE + \
                                          OSPFV3_HELLO_FIXED_PORTION_SIZE + \
                                          (OSPFV3_RTR_ID_LEN * \
                                           MAX_OSPFV3_NBRS)
                                          
                                               

/* For Database overflow */
#define  OSPFV3_NO_LIMIT                       -1
#define  OSPFV3_DONT_LEAVE_OVERFLOW_STATE       0
#define  OSPFV3_OVERFLOW_APPROACHING_LIMIT     ((9 * gV3OsRtr.i4ExtLsdbLimit) / 10)
                                                 /* ninety percent of the limit */

/* flags used in packet transmission */
#define  OSPFV3_NO_RELEASE                     0
#define  OSPFV3_RELEASE                        1

/* flags used when received packets are freed,after processing*/
#define  OSPFV3_NORMAL_RELEASE                 0

/* constants used in comparison functions */
#define  OSPFV3_EQUAL                     0
#define  OSPFV3_GREATER                   1
#define  OSPFV3_LESS                     -1
#define  OSPFV3_NOT_EQUAL                 3

#define  OSPFV3_TYPE_1_METRIC      OSPFV3_TYPE_1_EXT
#define  OSPFV3_TYPE_2_METRIC      OSPFV3_TYPE_2_EXT

/* Values used in Trie */
#define  OSPFV3_RT_TYPE                   6
#define  OSPFV3_EXT_RT_TYPE               9

#define OSPFV3_RT_TYPE_IN_CXT(pV3OspfCxt)\
                         (OSPFV3_RT_TYPE | (pV3OspfCxt->u4ContextId << 16))

#define OSPFV3_EXT_RT_TYPE_IN_CXT(pV3OspfCxt)\
                         (OSPFV3_EXT_RT_TYPE | (pV3OspfCxt->u4ContextId << 16))

/* value returned by lsa_process_lsa() */
/* process lsa returns OSIX_SUCCESS (0) / OSIX_FAILURE (1)
 * so, this macro is kept as 2
 */
#define  OSPFV3_DISCARDED                      2

/* values used in HpRcvHello() */
#define  OSPFV3_ISM_SCHEDULED                  1
#define  OSPFV3_ISM_NOT_SCHEDULED              0

#ifdef RAWSOCK_WANTED
#define  OSPFV3_DEF_SELECT_INTERVAL        10
#endif

#define OSPFV3_MUT_EXCL_SEM_NAME (const UINT1 *) "OVME"
#define OSPFV3_SEM_CREATE_INIT_CNT            1

/* constants and macros related to OSPFv3-RTM interaction */
#define  OSPFV3_MANUAL_TAG                     1
#define  OSPFV3_AUTOMATIC_TAG                  2
#define  OSPFV3_MANUAL_TAG_MASK                0x7fffffff

#define  OSPFV3_AS_EXT_DEF_METRIC              10

#define  OSPFV3_MIN_METRIC_VALUE               0
#define  OSPFV3_MAX_METRIC_VALUE               16777214

#ifdef RRD_WANTED
#define  OSPFV3_RRD_SRC_PROTO_BIT_MASK \
                 ( RTM6_DIRECT_MASK | RTM6_STATIC_MASK | \
     RTM6_RIP_MASK | RTM6_BGP_MASK | RTM6_ISISL1L2_MASK)

#define  OSPFV3_IS_VALID_RRD_SRC_PROTO_MASK_VALUE(x) \
                          ((x == 0) || !((x) & \
       ~(OSPFV3_RRD_SRC_PROTO_BIT_MASK)))
#endif /* RRD_WANTED */

#define OSPFV3_ROUTE_RTTYPE     1
#define OSPFV3_ROUTE_AREAID     2
#define OSPFV3_ROUTE_COST       3
#define OSPFV3_ROUTE_TYPE2COST  4
#define OSPFV3_ROUTE_IFACEINDEX 5

#define OSPFV3_IS_E_BIT_SET(u1Options)\
                         (u1Options & OSPFV3_ASBR_BIT_MASK)

#define OSPFV3_IS_B_BIT_SET(u1Options)\
                         (u1Options & OSPFV3_ABR_BIT_MASK)

#define OSPFV3_IS_E_BIT_EXT_LSA_SET(u1Options)\
  (u1Options & OSPFV3_EXT_E_BIT_MASK)

#define OSPFV3_IS_LA_BIT_SET(u1Options)\
                    (u1Options & OSPFV3_LA_BIT_MASK)

#define OSPFV3_RTR_LSA_LINK_LEN  16

#define OSPFV3_ONE_BYTE  1
#define OSPFV3_TWO_BYTES 2
#define OSPFV3_FOUR_BYTES 4
#define OSPFV3_BYTE_SIZE        8
#define OSPFV3_FOUR_BYTE_SIZE 32


#define OSPFV3_ADDR_PREFIX_LSA_HASH_KEY_SIZE \
                         (sizeof (tIp6Addr) + sizeof (UINT1)) 

#define OSPFV3_INTRA_AREA_PREFIX_LSA_HASH_KEY_SIZE \
        (sizeof(UINT2) + sizeof(tV3OsLinkStateId) + sizeof(tV3OsRouterId))

#define OSFPV3_RTR_LSA_NO_OF_LINKS(pLsaInfo)\
   ((pLsaInfo->u2LsaLen - OSPFV3_LS_HEADER_SIZE -\
   - 4 ) / \
   OSPFV3_RTR_LSA_LINK_LEN)

#define OSFPV3_LINK_LSA_NO_OF_PFXS(pLsaInfo) \
   OSPFV3_BUFFER_DWFROMPDU (pLsaInfo->pLsa + \
   + OSPFV3_LS_PREFIX_NUM_OFFSET)

#define OSPFV3_OPTIONS_OFFSET_IN_LS_HEADER 23

#define OSPFV3_TRIE_KEY_SIZE 17

#define OSPFV3_OPT_BYTE_ONE         0
#define OSPFV3_OPT_BYTE_TWO         1
#define OSPFV3_OPT_BYTE_THREE       2

#define OSPFV3_ROUTER_LSA_HDR_LEN              4
#define OSPFV3_NETWORK_LSA_HDR_LEN             4
#define OSPFV3_INTER_AREA_PFX_LSA_HDR_LEN      8
#define OSPFV3_INTER_AREA_RTR_LSA_HDR_LEN      12
#define OSPFV3_AS_EXTERN_LSA_HDR_LEN           8
#define OSPFV3_NSSA_LSA_HDR_LEN            8
#define OSPFV3_LINK_LSA_HDR_LEN                24
#define OSPFV3_INTRA_AREA_PFX_LSA_HDR_LEN          12

#define  OSPFV3_PKT_HEADER_LEN     16

/* Ospfv3 Packet Offset Constants */
#define  OSPFV3_VERSION_NO_OFFSET  0
#define  OSPFV3_TYPE_OFFSET        1
#define  OSPFV3_PKT_LENGTH_OFFSET  2
#define  OSPFV3_RTR_ID_OFFSET      4
#define  OSPFV3_AREA_ID_OFFSET     8
#define  OSPFV3_CHKSUM_OFFSET      12
#define  OSPFV3_INSTANCE_OFFSET    14
#define  OSPFV3_RSVD_OFFSET        15

/* Hello Packet Offset Constants */
#define  OSPFV3_HP_INTERFACE_ID_OFFSET       16
#define  OSPFV3_HP_RTR_PRI_OFFSET            20 
#define  OSPFV3_HP_OPTIONS_OFFSET            21 
#define  OSPFV3_HP_HELLO_INTERVAL_OFFSET     24 
#define  OSPFV3_HP_RTR_DEAD_INTERVAL_OFFSET  26 
#define  OSPFV3_HP_DSG_RTR_OFFSET            28 
#define  OSPFV3_HP_BK_DSG_RTR_OFFSET         32 
#define  OSPFV3_HP_NBR_ID_OFFSET             36 

/* DDP Packet Offset Constants */
#define  OSPFV3_DDP_OPTIONS_OFFSET           17 
#define  OSPFV3_DDP_IFACE_MTU_OFFSET         20 
#define  OSPFV3_DDP_RSVD_OFFSET              22   
#define  OSPFV3_DDP_MASK_OFFSET              23
#define  OSPFV3_DDP_SEQ_NUM_OFFSET           24
#define  OSPFV3_DDP_SUMMARY_LST_OFFSET       OSPFV3_PKT_HEADER_LEN + \
                               OSPFV3_DDP_FIXED_PORTION_SIZE
/* LRQ Packet Offset Constants */
#define  OSPFV3_LRQ_TYPE_OFFSET            18
#define  OSPFV3_LRQ_STATE_ID_OFFSET        20
#define  OSPFV3_LRQ_ADV_RTRID_OFFSET       24

/* LSU Packet Offset Constants */
#define OSPFV3_LSU_LSA_OFFSET              OSPFV3_PKT_HEADER_LEN + 4
                                                               
#define OSPFV3_RTR_LSA_FIXED_PORTION       OSPFV3_LS_HEADER_SIZE + 4
#define OSPFV3_NTWR_LSA_FIXED_PORTION      OSPFV3_LS_HEADER_SIZE + 4

#define OSPFV3_INTER_AREA_PREF_LEN_OFFSET  OSPFV3_LS_HEADER_SIZE + 4
#define OSPFV3_INTER_AREA_PREF_OFFSET      OSPFV3_LS_HEADER_SIZE + 8

#define OSPFV3_INTER_AREA_RTR_LSA_RTR_ID_OFFSET OSPFV3_LS_HEADER_SIZE + 8

#define OSPFV3_AS_EXT_LSA_PREF_LEN_OFFSET  OSPFV3_LS_HEADER_SIZE + 4
#define OSPFV3_AS_EXT_LSA_PREF_OFFSET      OSPFV3_LS_HEADER_SIZE + 8

#define OSPFV3_INET_ADDR_TYPE  2

#define OSPFV3_SECS_IN_HOUR 3600      /* No of seconds in a hour */
#define OSPFV3_SECS_IN_MIN  60        /* No of seconds in a minute */
#define OSPFV3_MSEC_IN_SEC  1000      /* No of milli seconds in a second */

#define OSPFV3_ONE_KBITS    1000
#define OSPFV3_ONE_MBITS    1000000

#define OSPFV3_MAX_IF_SPEED 0xffffffff 

#define OSPFV3_MAX_RTR_LINKS 50
#define OSPFV3_NONE 1
#define OSPFV3_NON_VOLATILE 3

#define OSPFV3_MAX_MOD_COUNTER_VALUE 4102

#define OSPFV3_RTR_LSA_METRIC_OFFSET    2
#define OSPFV3_RTR_LSA_IFID_OFFSET      2   
#define OSPFV3_RTR_LSA_NBR_RTRID_OFFSET 12         
#define OSPFV3_INTER_AREA_PFX_LSA_PFXLEN_OFFSET 4
#define OSPFV3_EXT_LSA_REF_LSTYPE_OFFSET 2
    
/* Default VRF SPF interfaval to delay the successive route calculation */
#define OSPFV3_DEF_VRF_SPF_INTERVAL      10
/* Macro to get the lower byte of LSA */
#define OSPFV3_GET_LOWER_LSA_TYPE_BYTE(lsaType) lsaType & 0xff

#define OSPFV3_GR_LSA_GRACE_PERIOD_OFFSET      OSPFV3_LS_HEADER_SIZE + 4 /* grace period */

/* Macros used by restarting router during graceful restart */

/* OSPFV3_RESTART_NONE is used to indicate that no restart process has
 * been attempted */
#define OSPFV3_RESTART_INPROGRESS 2 /* The router is undergoing
                                   * graceful restart */
#define OSPFV3_RESTART_COMPLETED  3 /* Last restart attempt is successful */
#define OSPFV3_RESTART_TIMEDOUT   4 /* Last restart process has been timed
                                   * out */
#define OSPFV3_RESTART_TOP_CHG    5 /* Last restart process has been exited
                                   * abnormally due to topology change */

#define OSPFV3_RESTART_TERM_BFD_DOWN  OSPFV3_RESTART_TOP_CHG
                                   /* OSPFV3 Graceful restart is aborted due to BFD down message
                                    * hence terminating graceful restart by sending
                                    * topology change notification.
                                    */


/* Current graceful restart operation */
#define OSPFV3_RESTART_UNPLANNED  3 /* Unplanned restart is in progress */

/* Last Graceful restart operation status */
/* OSPF restart state */
#define OSPFV3_GR_NONE              0 /* Normal OSPF operation */
#define OSPFV3_GR_SHUTDOWN          1 /* OSPF graceful shutdown */
#define OSPFV3_GR_RESTART           2 /* OSPF graceful restart */

/* Grace interval */
#define OSPFV3_GR_MIN_INTERVAL      1
#define OSPFV3_GR_MAX_INTERVAL      1800
#define OSPFV3_GR_DEF_INTERVAL      120

/* Size of TLV in Grace LSA */
#define OSPFV3_GR_TLV_SIZE          8

/* TLV value used in Grace LSA */
/* Grace period TLV */
#define OSPFV3_GR_PERIOD_TLV_TYPE        (UINT2)1
#define OSPFV3_GR_PERIOD_TLV_LENGTH      (UINT2)4

/* Restart reason TLV */
#define OSPFV3_GR_REASON_TLV_TYPE        (UINT2)2
#define OSPFV3_GR_REASON_TLV_LENGTH      (UINT2)1

/* GR related configuration file */
#define OSPFV3_GR_CONF              (const UINT1 *)"ospf3gr.conf"

/* Helper related macros */
#define OSPFV3_GR_NOT_HELPING       1
#define OSPFV3_GR_HELPING           2

/* Helper Exit Reason */
#define OSPFV3_HELPER_NONE                     1
#define OSPFV3_HELPER_INPROGRESS               2
#define OSPFV3_HELPER_COMPLETED                3
#define OSPFV3_HELPER_GRACE_TIMEDOUT           4
#define OSPFV3_HELPER_TOP_CHG                  5

#define OSPFV3_NO_TOPOLOGY_CHANGE  1
#define OSPFV3_TOPOLOGY_CHANGE     0

/* Bit position in LSA refresh flag */
#define OSPFV3_GR_LSA_BIT          1

/*Default grace LSA can be sent out*/
#define OSPFV3_DEF_GRACE_LSA_SENT  2

/* Syslog id for OSPFV3 */
#define OSPFV3_SYSLOG_ID gV3OsRtr.u4SysLogId

/* Macros for constants */
#define OSPFV3_ZERO                       0

/* Macros for defining the trace type */
#define OSPFV3_CXT_TRACE_TYPE             1
#define OSPFV3_CXT_EXT_TRACE_TYPE         2

#define MAX_COLUMN_LEN 256
#define MAX_STR_LEN    40

#define  OSPFV3_MAX_CONTEXTS_LIMIT  FsOSPFV3SizingParams[MAX_OSPFV3_CONTEXTS_SIZING_ID].u4PreAllocatedUnits 
#define OSPFV3_ALL_PKT_TYPE 0
#define OSPFV3_CRYPTOSEQ_MAX_NUM 0xFFFFFFFF
#define OSPFV3_CRYPTOSEQ_MIN_NUM 0x0

/* Constants used in AT*/
#define OSPFV3_IS_AT_BIT_SET(u1Options)\
                    (u1Options & OSPFV3_AT_BIT_MASK)
#define OSPFV3_IS_L_BIT_SET(u1Options)\
                    (u1Options & OSPFV3_L_BIT_MASK)
#define OSPFV3_REVD_AUTH_TYPE    0
#define OSPFV3_HMAC_AUTH_TYPE    1

/*AT Packet Offset Constant */
#define  OSPFV3_AT_TYP_OFFSET           0
#define  OSPFV3_AT_LEN_OFFSET         2
#define  OSPFV3_AT_RSVD_OFFSET              4
#define  OSPFV3_AT_SA_ID_OFFSET              6
#define  OSPFV3_AT_HIGSEQ_NUM_OFFSET           8
#define  OSPFV3_AT_LOWSEQ_NUM_OFFSET           12
#define  OSPFV3_AT_FIXED_PORTION_SIZE   16 

/* Crypto Authentication types */
#define OSPFV3_AUTH_SHA1  1
#define OSPFV3_AUTH_SHA2_256 3
#define OSPFV3_AUTH_SHA2_384 4
#define OSPFV3_AUTH_SHA2_512 5
#define OSPFV3_AUTH_NO_TYPE  6
#define OSPFV3_DEF_AUTH_TYPE OSPFV3_AUTH_NO_TYPE

#define OSPFV3_MAX_CRYPTOTYPE_VALUE 6
#define OSPFV3_MIN_CRYPTOTYPE_VALUE 1

#define OSPFV3_LLS_LEN_OFFSET            2
#define OSPFV3_MAX_AUTHKEY_LEN           16
#define OSPFV3_MAX_CRYPTO_KEY_LEN           35
#define OSPFV3_APAD_VALUE 0x878fe1f3

/* RM audit macros */
#define OSPFV3_DYN_MAX_CMDS 9
#define OSPFV3_AUDIT_FILE_ACTIVE        "/tmp/ospfv3_output_file_active"
#define OSPFV3_AUDIT_FILE_STDBY         "/tmp/ospfv3_output_file_stdby"
#define OSPFV3_CLI_EOF                  2
#define OSPFV3_CLI_NO_EOF               1
#define OSPFV3_CLI_RDONLY               OSIX_FILE_RO
#define OSPFV3_CLI_WRONLY               OSIX_FILE_WO
#define OSPFV3_CLI_MAX_GROUPS_LINE_LEN  200

#define  OSPFV3_REDISTRUTE_BGP            1
#define  OSPFV3_REDISTRUTE_RIP             2
#define  OSPFV3_REDISTRUTE_CONNECTED       3
#define  OSPFV3_REDISTRUTE_STATIC          4
#define  OSPFV3_REDISTRUTE_ISIS            5
#define  OSPFV3_MAX_PROTO_REDISTRUTE_SIZE  5

#endif

#ifdef _OS3MAIN_C
UINT4 gu4V3OspfLockFail                      = 0; 
UINT4 gu4V3UtilOspfIsValidCxtIdFail          = 0;
UINT4 gu4V3RtmTxRedistributeMsgInCxtFail     = 0; 
UINT4 gu4V3AuthSendPktFail                   = 0; 
UINT4 gu4V3OspfSendPktFail                   = 0; 
UINT4 gu4V3SendPktOnUnNumberedIntfFail       = 0; 
UINT4 gu4O3GrExitHelperFail                  = 0; 
UINT4 gu4V3OspfJoinMcastGroupFail            = 0; 
UINT4 gu4V3LsuDeleteLsaFromDatabaseFail      = 0; 
UINT4 gu4V3SignalLsaRegenInCxtFail           = 0; 
UINT4 gu4V3RtcProcessRtEntryChangesInCxtFail = 0;
UINT4 gu4V3RtcGetExtLsaLinkFail              = 0;
UINT4 gu4V3RtcCheckAndGenAggrLsaInCxtFail    = 0;
UINT4 gu4O3RedDynConstructAndSendLsaFail     = 0;
UINT4 gu4V3OspfEnqMsgToRmFail                = 0;
UINT4 gu4V3AuthProcPktFail                   = 0;
UINT4 gu4V3LsuAddToLsuFail                   = 0;
UINT4 gu4O3RedDynSendIntfInfoToStandbyFail   = 0;
UINT4 gu4V3RagChkLsaFallInPrvRngFail         = 0; 
UINT4 gu4V3UtilIsMatchedPrefHashNodeFail     = 0;
UINT4 gu4V3RtcIsLsaValidForRtCalcFail        = 0;
UINT4 gu4V3ApplyInFilterInCxtFail            = 0;
UINT4 gu4V3AuthGetATposFail                  = 0;
UINT4 gu4V3WriteCryptoHighSeqFail            = 0;
UINT4 gu4V3AuthRmCryptSeqMsgToRmFail      = 0;
UINT4 gu4V3LsuAddOrDelLsaInRBTreeFail        = 0; 
UINT4 gu4V3IfPrefixAddFail                   = 0;
UINT4 gu4V3OspfSendEventToRmFail             = 0;
UINT4 gu4O3RedIfSendIntfInfoFail             = 0;
UINT4 gu4O3RedUtlSendBulkUpdateFail          = 0; 
UINT4 gu4O3RedAdjAllocateBulkUpdatePktFail   = 0;
UINT4 gu4O3RedAdjSendBulkNbrInfoFail         = 0;
UINT4 gu4O3RedTmrAddStabilityInfoFail        = 0;
UINT4 gu4O3RedTmrSendTimerInfoFail           = 0;
UINT4 gu4V3OspfUnLockFail                    = 0;
UINT4 gu4V3HpRcvHelloFail        = 0;
UINT4 gu4O3RedAdjSendBulkVirtNbrInfoFail     = 0;
UINT4 gu4O3RedAdjSendVirtNbrInfoFail         = 0;
UINT4 gu4O3RedAdjProcessBulkHelloFail        = 0;
UINT4 gu4O3RedAdjProcessNbrStateInfoFail     = 0; 
UINT4 gu4O3RedIfProcessIntfInfoFail          = 0;
UINT4 gu4O3RedLsuGetLsuFromUpdtFail          = 0;
UINT4 gu4O3RedLsuProcessLsuFail              = 0;
UINT4 gu4V3RagGenExtLsaInCxtFail             = 0; 
UINT4 gu4V3IsEligibleToGenAseLsaInCxtFail    = 0;
UINT4 gu4V3IsFunctionalityEqvLsaFail         = 0;
#else
extern UINT4 gu4V3OspfLockFail;                     
extern UINT4 gu4V3UtilOspfIsValidCxtIdFail;         
extern UINT4 gu4V3RtmTxRedistributeMsgInCxtFail;    
extern UINT4 gu4V3AuthSendPktFail;                  
extern UINT4 gu4V3OspfSendPktFail;                  
extern UINT4 gu4V3SendPktOnUnNumberedIntfFail;      
extern UINT4 gu4O3GrExitHelperFail;                 
extern UINT4 gu4V3OspfJoinMcastGroupFail;           
extern UINT4 gu4V3LsuDeleteLsaFromDatabaseFail;     
extern UINT4 gu4V3SignalLsaRegenInCxtFail;          
extern UINT4 gu4V3RtcProcessRtEntryChangesInCxtFail;
extern UINT4 gu4V3RtcGetExtLsaLinkFail;             
extern UINT4 gu4V3RtcCheckAndGenAggrLsaInCxtFail;   
extern UINT4 gu4O3RedDynConstructAndSendLsaFail;    
extern UINT4 gu4V3OspfEnqMsgToRmFail;               
extern UINT4 gu4V3AuthProcPktFail;                  
extern UINT4 gu4V3LsuAddToLsuFail;                  
extern UINT4 gu4O3RedDynSendIntfInfoToStandbyFail;  
extern UINT4 gu4V3RagChkLsaFallInPrvRngFail;        
extern UINT4 gu4V3UtilIsMatchedPrefHashNodeFail;    
extern UINT4 gu4V3RtcIsLsaValidForRtCalcFail;       
extern UINT4 gu4V3ApplyInFilterInCxtFail;           
extern UINT4 gu4V3AuthGetATposFail;                 
extern UINT4 gu4V3WriteCryptoHighSeqFail;           
extern UINT4 gu4V3AuthRmCryptSeqMsgToRmFail;        
extern UINT4 gu4V3LsuAddOrDelLsaInRBTreeFail;       
extern UINT4 gu4V3IfPrefixAddFail;                  
extern UINT4 gu4V3OspfSendEventToRmFail;            
extern UINT4 gu4O3RedIfSendIntfInfoFail;            
extern UINT4 gu4O3RedUtlSendBulkUpdateFail;         
extern UINT4 gu4O3RedAdjAllocateBulkUpdatePktFail;  
extern UINT4 gu4O3RedAdjSendBulkNbrInfoFail;        
extern UINT4 gu4O3RedTmrAddStabilityInfoFail;       
extern UINT4 gu4O3RedTmrSendTimerInfoFail;          
extern UINT4 gu4V3OspfUnLockFail;               
extern UINT4 gu4V3HpRcvHelloFail;     
extern UINT4 gu4O3RedAdjSendBulkVirtNbrInfoFail;
extern UINT4 gu4O3RedAdjSendVirtNbrInfoFail;
extern UINT4 gu4O3RedAdjProcessBulkHelloFail;
extern UINT4 gu4O3RedAdjProcessNbrStateInfoFail;
extern UINT4 gu4O3RedIfProcessIntfInfoFail;
extern UINT4 gu4O3RedLsuGetLsuFromUpdtFail;
extern UINT4 gu4O3RedLsuProcessLsuFail;
extern UINT4 gu4V3RagGenExtLsaInCxtFail;
extern UINT4 gu4V3IsEligibleToGenAseLsaInCxtFail;
extern UINT4 gu4V3IsFunctionalityEqvLsaFail;
#endif /* _OS3MAIN_C */
