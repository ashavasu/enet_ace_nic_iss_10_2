/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ospf3lw.h,v 1.4 2017/12/26 13:34:25 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3RouterId ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3AdminStat ARG_LIST((INT4 *));

INT1
nmhGetOspfv3VersionNumber ARG_LIST((INT4 *));

INT1
nmhGetOspfv3AreaBdrRtrStatus ARG_LIST((INT4 *));

INT1
nmhGetOspfv3ASBdrRtrStatus ARG_LIST((INT4 *));

INT1
nmhGetOspfv3AsScopeLsaCount ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3AsScopeLsaCksumSum ARG_LIST((INT4 *));

INT1
nmhGetOspfv3OriginateNewLsas ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3RxNewLsas ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3ExtLsaCount ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3ExtAreaLsdbLimit ARG_LIST((INT4 *));

INT1
nmhGetOspfv3MulticastExtensions ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3ExitOverflowInterval ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3DemandExtensions ARG_LIST((INT4 *));

INT1
nmhGetOspfv3TrafficEngineeringSupport ARG_LIST((INT4 *));

INT1
nmhGetOspfv3ReferenceBandwidth ARG_LIST((UINT4 *));

INT1
nmhGetOspfv3RestartSupport ARG_LIST((INT4 *));

INT1
nmhGetOspfv3RestartInterval ARG_LIST((INT4 *));

INT1
nmhGetOspfv3RestartStatus ARG_LIST((INT4 *));

INT1
nmhGetOspfv3RestartAge ARG_LIST((INT4 *));

INT1
nmhGetOspfv3RestartExitReason ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3RouterId ARG_LIST((UINT4 ));

INT1
nmhSetOspfv3AdminStat ARG_LIST((INT4 ));

INT1
nmhSetOspfv3ASBdrRtrStatus ARG_LIST((INT4 ));

INT1
nmhSetOspfv3ExtAreaLsdbLimit ARG_LIST((INT4 ));

INT1
nmhSetOspfv3MulticastExtensions ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetOspfv3ExitOverflowInterval ARG_LIST((UINT4 ));

INT1
nmhSetOspfv3DemandExtensions ARG_LIST((INT4 ));

INT1
nmhSetOspfv3TrafficEngineeringSupport ARG_LIST((INT4 ));

INT1
nmhSetOspfv3ReferenceBandwidth ARG_LIST((UINT4 ));

INT1
nmhSetOspfv3RestartSupport ARG_LIST((INT4 ));

INT1
nmhSetOspfv3RestartInterval ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3RouterId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Ospfv3AdminStat ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3ASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3ExtAreaLsdbLimit ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3MulticastExtensions ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ospfv3ExitOverflowInterval ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Ospfv3DemandExtensions ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3TrafficEngineeringSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3ReferenceBandwidth ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2Ospfv3RestartSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Ospfv3RestartInterval ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3RouterId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3AdminStat ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3ASBdrRtrStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3ExtAreaLsdbLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3MulticastExtensions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3ExitOverflowInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3DemandExtensions ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3TrafficEngineeringSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3ReferenceBandwidth ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3RestartSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Ospfv3RestartInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3AreaTable. */
INT1
nmhValidateIndexInstanceOspfv3AreaTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3AreaTable  */

INT1
nmhGetFirstIndexOspfv3AreaTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3AreaTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3ImportAsExtern ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaSpfRuns ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaBdrRtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaAsBdrRtrCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaScopeLsaCount ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaScopeLsaCksumSum ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaSummary ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3StubMetric ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaNssaTranslatorRole ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaNssaTranslatorState ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaNssaTranslatorStabilityInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaNssaTranslatorEvents ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetOspfv3AreaStubMetricType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetOspfv3AreaDfInfOriginate ARG_LIST((UINT4 ,INT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3ImportAsExtern ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaSummary ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3StubMetric ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaNssaTranslatorRole ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaNssaTranslatorStabilityInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetOspfv3AreaStubMetricType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaDfInfOriginate ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3ImportAsExtern ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaSummary ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3StubMetric ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaNssaTranslatorRole ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaNssaTranslatorStabilityInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Ospfv3AreaStubMetricType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaDfInfOriginate ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3AreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3AsLsdbTable. */
INT1
nmhValidateIndexInstanceOspfv3AsLsdbTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3AsLsdbTable  */

INT1
nmhGetFirstIndexOspfv3AsLsdbTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3AsLsdbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3AsLsdbSequence ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AsLsdbAge ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AsLsdbChecksum ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AsLsdbAdvertisement ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3AsLsdbTypeKnown ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Ospfv3AreaLsdbTable. */
INT1
nmhValidateIndexInstanceOspfv3AreaLsdbTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3AreaLsdbTable  */

INT1
nmhGetFirstIndexOspfv3AreaLsdbTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3AreaLsdbTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3AreaLsdbSequence ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaLsdbAge ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaLsdbChecksum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaLsdbAdvertisement ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3AreaLsdbTypeKnown ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Ospfv3LinkLsdbTable. */
INT1
nmhValidateIndexInstanceOspfv3LinkLsdbTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3LinkLsdbTable  */

INT1
nmhGetFirstIndexOspfv3LinkLsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3LinkLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3LinkLsdbSequence ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3LinkLsdbAge ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3LinkLsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3LinkLsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3LinkLsdbTypeKnown ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Ospfv3HostTable. */
INT1
nmhValidateIndexInstanceOspfv3HostTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ospfv3HostTable  */

INT1
nmhGetFirstIndexOspfv3HostTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3HostTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3HostMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetOspfv3HostStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetOspfv3HostAreaID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3HostMetric ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetOspfv3HostStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetOspfv3HostAreaID ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3HostMetric ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Ospfv3HostStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Ospfv3HostAreaID ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3HostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3IfTable. */
INT1
nmhValidateIndexInstanceOspfv3IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3IfTable  */

INT1
nmhGetFirstIndexOspfv3IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3IfAreaId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfAdminStat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfRtrPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfTransitDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfRetransInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfHelloInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfRtrDeadInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfPollInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfDesignatedRouter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfBackupDesignatedRouter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfEvents ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfMulticastForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfDemand ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfMetricValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfLinkScopeLsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfLinkLsaCksumSum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfInstId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfDemandNbrProbe ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3IfAreaId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetOspfv3IfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfAdminStat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfRtrPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfTransitDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfRetransInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfHelloInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfRtrDeadInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfPollInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetOspfv3IfStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfMulticastForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfDemand ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfMetricValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfInstId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfDemandNbrProbe ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3IfAreaId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ospfv3IfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfAdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfRtrPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfTransitDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfRetransInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfHelloInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfPollInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ospfv3IfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfMulticastForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfDemand ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfMetricValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfInstId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfDemandNbrProbe ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Ospfv3IfDemandNbrProbeRetxLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2Ospfv3IfDemandNbrProbeInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3VirtIfTable. */
INT1
nmhValidateIndexInstanceOspfv3VirtIfTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3VirtIfTable  */

INT1
nmhGetFirstIndexOspfv3VirtIfTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3VirtIfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3VirtIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfTransitDelay ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfRetransInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfHelloInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfEvents ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3VirtIfStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtIfLinkScopeLsaCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3VirtIfLinkLsaCksumSum ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3VirtIfIndex ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfv3VirtIfTransitDelay ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfv3VirtIfRetransInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfv3VirtIfHelloInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfv3VirtIfRtrDeadInterval ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetOspfv3VirtIfStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3VirtIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3VirtIfTransitDelay ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3VirtIfRetransInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3VirtIfHelloInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3VirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3VirtIfStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3VirtIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3NbrTable. */
INT1
nmhValidateIndexInstanceOspfv3NbrTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3NbrTable  */

INT1
nmhGetFirstIndexOspfv3NbrTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3NbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3NbrAddressType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3NbrOptions ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrPriority ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrEvents ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3NbrLsRetransQLen ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3NbrHelloSuppressed ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrIfId ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrRestartHelperStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrRestartHelperAge ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3NbrRestartHelperExitReason ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Ospfv3NbmaNbrTable. */
INT1
nmhValidateIndexInstanceOspfv3NbmaNbrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for Ospfv3NbmaNbrTable  */

INT1
nmhGetFirstIndexOspfv3NbmaNbrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3NbmaNbrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3NbmaNbrPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetOspfv3NbmaNbrRtrId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetOspfv3NbmaNbrState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetOspfv3NbmaNbrStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetOspfv3NbmaNbrStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3NbmaNbrPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetOspfv3NbmaNbrStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetOspfv3NbmaNbrStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3NbmaNbrPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Ospfv3NbmaNbrStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2Ospfv3NbmaNbrStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3NbmaNbrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ospfv3VirtNbrTable. */
INT1
nmhValidateIndexInstanceOspfv3VirtNbrTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3VirtNbrTable  */

INT1
nmhGetFirstIndexOspfv3VirtNbrTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3VirtNbrTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3VirtNbrIfIndex ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrAddressType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrAddress ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetOspfv3VirtNbrOptions ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrEvents ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3VirtNbrLsRetransQLen ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetOspfv3VirtNbrHelloSuppressed ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrIfId ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrRestartHelperStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrRestartHelperAge ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetOspfv3VirtNbrRestartHelperExitReason ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for Ospfv3AreaAggregateTable. */
INT1
nmhValidateIndexInstanceOspfv3AreaAggregateTable ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ospfv3AreaAggregateTable  */

INT1
nmhGetFirstIndexOspfv3AreaAggregateTable ARG_LIST((UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexOspfv3AreaAggregateTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetOspfv3AreaAggregateStatus ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaAggregateEffect ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetOspfv3AreaAggregateRouteTag ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetOspfv3AreaAggregateStatus ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaAggregateEffect ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetOspfv3AreaAggregateRouteTag ARG_LIST((UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ospfv3AreaAggregateStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaAggregateEffect ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2Ospfv3AreaAggregateRouteTag ARG_LIST((UINT4 *  ,UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ospfv3AreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
