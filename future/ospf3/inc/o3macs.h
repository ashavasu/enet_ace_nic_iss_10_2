/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3macs.h,v 1.22 2018/02/01 11:01:23 siva Exp $
 *
 * Description:This file contains macros relating to protocol.
 *
 *******************************************************************/
#ifndef _O3MACS_H
#define _O3MACS_H

/****************************************************************************/
/* mathematical                                                             */
/****************************************************************************/

#define  OSPFV3_MIN(a,b)  ((a < b) ? a : b )
#define  OSPFV3_MAX(a,b)  ((a > b) ? a : b )
#define  OSPFV3_DIFF(a,b)  ((a < b) ? (b-a) : (a-b))
#define  OSPFV3_ROUND_OFF(x,y)  x = (UINT2) (x - (x % y))
                                   /* converts x to a multiple of y */

#define  OSPFV3_OFFSET(x,y)   FSAP_OFFSETOF(x,y)
#define  OSPFV3_COUNTER_OP(var, value)    (var) += (value);

/* MACRO used for setting Option bits */
#define  OSPFV3_OPT_SET(x,opt)   ((x)[2] |=  (opt))
#define  OSPFV3_OPT_ISSET(x,opt) ((x)[2] &   (opt))
#define  OSPFV3_OPT_CLEAR(x,opt) ((x)[2] &= ~(opt))
#define  OSPFV3_OPT_CLEAR_ALL(x) ((x)[0] = (x)[1] = (x)[2] = 0)

/****************************************************************************/
/* macros for operation on statiscal counters and gauges                    */
/****************************************************************************/

#define OSPFV3_INC_DISCARD_OSPF_PKT(pV3OspfCxt) \
        OSPFV3_COUNTER_OP(pV3OspfCxt->u4OspfPktsDisd, 1)
#define OSPFV3_INC_DISCARD_HELLO_CNT(p_iface) \
        OSPFV3_COUNTER_OP(p_iface->u4HelloDisdCount, 1); 
#define OSPFV3_INC_DISCARD_DDP_CNT(p_iface) \
        OSPFV3_COUNTER_OP(p_iface->u4DdpDisdCount, 1);
#define OSPFV3_INC_DISCARD_LRQ_CNT(p_iface) \
        OSPFV3_COUNTER_OP(p_iface->u4LsaReqDisdCount, 1);
#define OSPFV3_INC_DISCARD_LSU_CNT(p_iface) \
        OSPFV3_COUNTER_OP(p_iface->u4LsaUpdateDisdCount, 1);
#define OSPFV3_INC_DISCARD_LAK_CNT(p_iface) \
        OSPFV3_COUNTER_OP(p_iface->u4LsaAckDisdCount, 1);

/****************************************************************************/
/* state machines                                                           */
/****************************************************************************/
 
#define  OSPFV3_GENERATE_IF_EVENT(pInterface, event)  V3IsmRunIsm(pInterface, event); 
#define  OSPFV3_GENERATE_NBR_EVENT(pNbr, event)       V3NsmRunNsm(pNbr, event);       

/****************************************************************************/
/* Macro to delete a node while scanning the list                           */
/****************************************************************************/

#define OSPFV3_DYNM_SLL_SCAN(pList, pNode, pTempNode, TypeCast) \
        for(((pNode) = (TypeCast)(TMO_SLL_First((pList)))),   \
            ((pTempNode) = ((pNode == NULL) ? NULL : \
                           ((TypeCast)    \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))); \
            (pNode) != NULL;  \
            (pNode) = (pTempNode),  \
            ((pTempNode) = ((pNode == NULL) ? NULL :  \
                           ((TypeCast)  \
                            (TMO_SLL_Next((pList),  \
                                          ((tTMO_SLL_NODE *)(VOID *)(pNode))))))))

#define OSPFV3_DYNM_HASH_BUCKET_SCAN(pHashTab, u4HashIndex, \
                     pNodePtr, pTempNode, TypeCast) \
                     OSPFV3_DYNM_SLL_SCAN(&(pHashTab)->HashList[(u4HashIndex)] ,\
                                           pNodePtr, pTempNode, TypeCast)

/****************************************************************************/
/* macros for accessing context variables                                   */
/****************************************************************************/

#define  OSPFV3_RTR_ID(pCxt)       pCxt->rtrId

/****************************************************************************/
/* macros  for Demand Circuit.                                             */
/****************************************************************************/

#define OSPFV3_IS_SUPPORTING_DC_EXTENSIONS(pCxt) (pCxt->bDemandExtension == OSIX_TRUE)

#define OSPFV3_IS_DC_EXT_APPLICABLE_IF(interface) \
                    (interface->bDcEndpt == OSIX_TRUE)

#define OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_IF(interface) \
        (interface->bDcEndpt == OSIX_TRUE &&  \
         interface->u1NetworkType == OSPFV3_IF_PTOP)

#define OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_IF(interface) \
       (interface->bDcEndpt == OSIX_TRUE && \
        (interface->u1NetworkType == OSPFV3_IF_PTOP ||  \
         interface->u1NetworkType == OSPFV3_IF_VIRTUAL))

#define OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_VIRT_OR_PTOMP_IF(interface) \
       (interface->bDcEndpt == OSIX_TRUE && \
        (interface->u1NetworkType == OSPFV3_IF_PTOP ||  \
         interface->u1NetworkType == OSPFV3_IF_VIRTUAL || \
        interface->u1NetworkType == OSPFV3_IF_PTOMP))

#define OSPFV3_IS_DC_EXT_APPLICABLE_PTOP_OR_PTOMP_IF(interface) \
       (interface->bDcEndpt == OSIX_TRUE && \
        (interface->u1NetworkType == OSPFV3_IF_PTOP ||  \
        interface->u1NetworkType == OSPFV3_IF_PTOMP))

#define OSPFV3_IS_DISCOVERED_DC_ENDPOINT_IFACE(interface) \
       (interface->u1ConfStatus == OSPFV3_DISCOVERED_ENDPOINT) 

#define OSPFV3_IS_HELLO_SUPPRESSED_NBR(nbr) \
       (nbr->bHelloSuppression == OSIX_TRUE)

/****************************************************************************/
/* Macros used for Link State Database module                              */
/****************************************************************************/

#define OSPFV3_ADD_RTR_LSA_LINK(currPtr,u1LinkType,u2Metric,u4IfId,u4NbrIfId,nbrRtrId) \
                      OSPFV3_LADD1BYTE(currPtr, u1LinkType); \
                      OSPFV3_LADD1BYTE(currPtr, 0); \
                      OSPFV3_LADD2BYTE(currPtr, u2Metric); \
                      OSPFV3_LADD4BYTE(currPtr, u4IfId); \
                      OSPFV3_LADD4BYTE(currPtr, u4NbrIfId); \
                      OSPFV3_LADDSTR(currPtr, nbrRtrId, OSPFV3_RTR_ID_LEN);

#define OSPFV3_ADD_ADDR_PREFIX(currPtr,u4Metric,u1PrefixLen,ip6Addr) \
                      OSPFV3_LADD3BYTE(currPtr, u4Metric); \
                      OSPFV3_LADD1BYTE(currPtr, u1PrefixLen); \
                      OSPFV3_LADD3BYTE(currPtr, 0); \
        OSPFV3_ADD_PREFIX_IN_LSA(currPtr, ip6Addr, u1PrefixLen)\
        currPtr = currPtr + OSPFV3_GET_PREFIX_LEN_IN_BYTE(u1PrefixLen);

#define OSPFV3_ADD_LINK_LSA_PREFIX(currPtr,u1PrefixLen,u1PrefOption,ip6Addr) \
                      OSPFV3_LADD1BYTE(currPtr, u1PrefixLen); \
                      OSPFV3_LADD1BYTE(currPtr, u1PrefOption); \
                      OSPFV3_LADD2BYTE(currPtr, 0); \
        OSPFV3_ADD_PREFIX_IN_LSA(currPtr, ip6Addr, u1PrefixLen)\
        currPtr = currPtr + OSPFV3_GET_PREFIX_LEN_IN_BYTE(u1PrefixLen);

#define OSPFV3_ADD_NSSA_ADDR_PREFIX(currPtr,u4Metric,u1PrefixLen,u1PreOpt, ip6Addr) \
                      OSPFV3_LADD3BYTE(currPtr, u4Metric); \
                      OSPFV3_LADD1BYTE(currPtr, u1PrefixLen); \
                      OSPFV3_LADD1BYTE(currPtr, u1PreOpt); \
                      OSPFV3_LADD2BYTE(currPtr, 0); \
        OSPFV3_ADD_PREFIX_IN_LSA(currPtr, ip6Addr, u1PrefixLen)\
        currPtr = currPtr + OSPFV3_GET_PREFIX_LEN_IN_BYTE(u1PrefixLen);

#define OSPFV3_ADD_INTRA_AREA_PREFIX(currPtr, prefixInfo) \
                      OSPFV3_LADD1BYTE(currPtr, prefixInfo.u1PrefixLength); \
                      OSPFV3_LADD1BYTE(currPtr, prefixInfo.u1PrefixOpt); \
                      OSPFV3_LADD2BYTE(currPtr, prefixInfo.u2Metric); \
        OSPFV3_ADD_PREFIX_IN_LSA(currPtr, prefixInfo.addrPrefix, prefixInfo.u1PrefixLength)\
        currPtr = currPtr + OSPFV3_GET_PREFIX_LEN_IN_BYTE(prefixInfo.u1PrefixLength);

#define IS_OSPFV3_LSA_FUNCTION_CODE_RECOGNISED(u2LsaType) \
                      ((u2LsaType == OSPFV3_ROUTER_LSA ) ||\
                       (u2LsaType == OSPFV3_NETWORK_LSA) ||\
                       (u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA) ||\
                       (u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) ||\
                       (u2LsaType == OSPFV3_AS_EXT_LSA) ||\
                       (u2LsaType == OSPFV3_LINK_LSA) ||\
                       (u2LsaType == OSPFV3_NSSA_LSA) ||\
                       (u2LsaType == OSPFV3_INTRA_AREA_PREFIX_LSA) ||\
                       (u2LsaType == OSPFV3_GRACE_LSA))

#define IS_OSPFV3_ADDR_PREFIX_LSA(u2LsaType) \
                       ((u2LsaType == OSPFV3_INTER_AREA_PREFIX_LSA) ||\
                       (u2LsaType == OSPFV3_AS_EXT_LSA) ||\
                       (u2LsaType == OSPFV3_NSSA_LSA))


#define OSPFV3_IS_U_BIT_SET_LSA(u2LsaType) (u2LsaType & 0x8000)

#define OSPFV3_IS_RESERVED_FLOOD_SCOPE_LSA(u2LsaType) \
                      ((u2LsaType & 0x6000) == 0x6000)
        
#define OSPFV3_IS_AS_FLOOD_SCOPE_LSA(u2LsaType)  (u2LsaType & 0x4000)

#define OSPFV3_IS_AREA_FLOOD_SCOPE_LSA(u2LsaType) (u2LsaType & 0x2000)

#define OSPFV3_IS_OPTION_BIT_PRESENT_IN_LSA(u2LsaType)\
                      ((u2LsaType == OSPFV3_ROUTER_LSA ) ||\
                       (u2LsaType == OSPFV3_NETWORK_LSA) ||\
                       (u2LsaType == OSPFV3_INTER_AREA_ROUTER_LSA) ||\
                       (u2LsaType == OSPFV3_LINK_LSA))

#define OSPFV3_ROUTER_LSA_SIZE  OSPFV3_MAX_LSA_SIZE
#define OSPFV3_INTRA_AREA_PREFIX_LSA_SIZE  OSPFV3_MAX_LSA_SIZE
#define OSPFV3_NETWORK_LSA_SIZE(pInterface) (OSPFV3_LS_HEADER_SIZE + \
                                 ((pInterface->u4NbrFullCount +\
                                   1) * OSPFV3_RTR_ID_LEN) + \
                          OSPFV3_OPTION_LEN + 1)

#define OSPFV3_INTER_AREA_PREFIX_LSA_SIZE    (OSPFV3_LS_HEADER_SIZE + 24)

#define OSPFV3_INTER_AREA_ROUTER_LSA_SIZE    (OSPFV3_LS_HEADER_SIZE + 12)
#define OSPFV3_GRACE_LSA_SIZE (OSPFV3_LS_HEADER_SIZE + \
                               (2 * OSPFV3_GR_TLV_SIZE))

#define OSPFV3_AS_EXT_LSA_SIZE        (OSPFV3_LS_HEADER_SIZE +  48)

#define OSPFV3_NSSA_LSA_SIZE  (OSPFV3_LS_HEADER_SIZE + 48)

#define OSPFV3_LINK_LSA_SIZE(pInterface)    (OSPFV3_LS_HEADER_SIZE + \
                                     20 + \
                                 ((TMO_SLL_Count (&pInterface->ip6AddrLst)) *\
                            20) + OSPFV3_OPTION_LEN + 1)

/****************************************************************************/
/* macros  for Interface                                                  */
/****************************************************************************/

#define OSPFV3_IS_VIRTUAL_IFACE(p_iface)       \
                                (p_iface->u1NetworkType == OSPFV3_IF_VIRTUAL)

#define OSPFV3_IS_PTOP_IFACE(p_iface)          \
                                (p_iface->u1NetworkType == OSPFV3_IF_PTOP)
#define OSPFV3_IS_PTOMP_IFACE(p_iface)          \
                                (p_iface->u1NetworkType == OSPFV3_IF_PTOMP)

#define OSPFV3_IS_NBMA_IFACE(pInterface) \
                          (pInterface->u1NetworkType == OSPFV3_IF_NBMA)


/****************************************************************************/
/* macros  for Routing Module                                              */
/****************************************************************************/

#define  OSPFV3_IS_DEST_NETWORK(p_rte)  \
         ((p_rte != NULL) && (p_rte->u1DestType == OSPFV3_DEST_NETWORK))
#define  OSPFV3_IS_DEST_ABR(p_rte)      \
         ((p_rte != NULL) && (p_rte->u1DestType == OSPFV3_DEST_AREA_BORDER))
#define  OSPFV3_IS_DEST_ASBR(p_rte)     \
         ((p_rte != NULL) && (p_rte->u1DestType == OSPFV3_DEST_AS_BOUNDARY))

#define OSPFV3_IS_INTRA_AREA_PATH(p_rte)       \
         (OSPFV3_GET_PATH_TYPE(p_rte) == OSPFV3_INTRA_AREA)

#define OSPFV3_IS_INTER_AREA_PATH(p_rte)       \
         (OSPFV3_GET_PATH_TYPE(p_rte) == OSPFV3_INTER_AREA)

#define OSPFV3_IS_TYPE_1_EXT_PATH(p_rte)       \
         (OSPFV3_GET_PATH_TYPE(p_rte) == OSPFV3_TYPE_1_EXT)

#define OSPFV3_IS_TYPE_2_EXT_PATH(p_rte)       \
         (OSPFV3_GET_PATH_TYPE(p_rte) == OSPFV3_TYPE_2_EXT)


/****************************************************************************/
/* macros  for conditions                                                   */
/****************************************************************************/

#define OSPFV3_GET_IF_AREA_ID(x)  (((x)->u1NetworkType == OSPFV3_IF_VIRTUAL) ? \
                        (x)->pArea->pV3OspfCxt->pBackbone->areaId : (x)->pArea->areaId )

#define  OSPFV3_IS_AREA_BORDER_RTR(pCxt) (pCxt->bAreaBdrRtr == OSIX_TRUE)

#define  OSPFV3_IS_AS_BOUNDARY_RTR(pCxt) (pCxt->bAsBdrRtr == OSIX_TRUE)

#define  OSPFV3_IS_RTR_ENABLED(pCxt)     (pCxt->admnStat == OSPFV3_ENABLED)

#define  OSPFV3_IS_STUB_AREA(pArea)      (pArea->u4AreaType == OSPFV3_STUB_AREA)

#define  OSPFV3_IS_NORMAL_AREA(pArea)  (pArea->u4AreaType == OSPFV3_NORMAL_AREA)

#define  OSPFV3_IS_NSSA_AREA(pArea)  (pArea->u4AreaType == OSPFV3_NSSA_AREA) 

#define OSPFV3_IS_SELF_ORIGINATED_LSA(pLsaInfo) \
                        (V3UtilRtrIdComp(pLsaInfo->lsaId.advRtrId,\
                         pLsaInfo->pV3OspfCxt->rtrId) == OSPFV3_EQUAL)
 
#define OSPFV3_IS_AREA_TRANSIT_CAPABLE(pArea) \
                                (pArea->bTransitCapability == OSIX_TRUE)

#define OSPFV3_IS_DR(p_iface)           (p_iface->u1IsmState == OSPFV3_IFS_DR)
#define OSPFV3_IS_BDR(p_iface)          (p_iface->u1IsmState == OSPFV3_IFS_BACKUP)
#define OSPFV3_GET_DR(x)           ((x)->desgRtr)
#define OSPFV3_GET_BDR(x)          ((x)->backupDesgRtr)

#define  OSPFV3_IS_NBR_FULL(pNbr)      (pNbr->u1NsmState == OSPFV3_NBRS_FULL)        

#define OSPFV3_IS_MAX_AGE(age) ((age >= OSPFV3_MAX_AGE && age < OSPFV3_DO_NOT_AGE) || \
                     (age >= OSPFV3_MAX_AGE + OSPFV3_DO_NOT_AGE))

#define OSPFV3_IS_SEND_AREA_SUMMARY(pArea)    \
                         (pArea->u1SummaryFunctionality == OSPFV3_SEND_AREA_SUMMARY)

#define OSPFV3_IS_IN_OVERFLOW_STATE(pCxt)     (pCxt->bOverflowState == OSIX_TRUE) 

#define OSPFV3_IS_EXT_LSDB_SIZE_LIMITED(pCxt)  (pCxt->i4ExtLsdbLimit != OSPFV3_NO_LIMIT) 

#define OSPFV3_IS_RTR_CFGD_TO_LEAVE_OVFL_STATE(pCxt) \
             (pCxt->u4ExitOverflowInterval != OSPFV3_DONT_LEAVE_OVERFLOW_STATE)


#define OSPFV3_IS_CONFIGURED_BACKBONE(pCxt) \
 (TMO_SLL_Count (&(pCxt->pBackbone->ifsInArea)) > 0)

#define OSPFV3_CAN_RTR_LEAVE_OVERFLOW_STATE    \
 (INT4) gV3OsRtr.u4AsExtLsaCount > gV3OsRtr.i4ExtLsdbLimit
 

#define OSPFV3_IS_ACTIVELY_ATTACHED_AREA(pArea) \
  (pArea->u4ActIntCount > 0)
 

 
#define  OSPFV3_RTR_ID_COPY(dst,src)   MEMCPY(dst, src, OSPFV3_RTR_ID_LEN)
#define  OSPFV3_LINK_STATE_ID_COPY(dst,src)\
         MEMCPY(dst, src, OSPFV3_LINKSTATE_ID_LEN)
#define  OSPFV3_AREA_ID_COPY(dst,src)   MEMCPY(dst, src, OSPFV3_AREA_ID_LEN)

#define OSPFV3_IS_AREA_BORDER_RTR_LSA(pLsa)  (*((UINT1 *)(pLsa + OSPFV3_LS_HEADER_SIZE)) \
                                         & OSPFV3_ABR_MASK)
#define OSPFV3_IS_AS_BOUNDARY_RTR_LSA(pLsa)  (*((UINT1 *)(pLsa + OSPFV3_LS_HEADER_SIZE)) \
                                         & OSPFV3_ASBR_MASK)

#define OSPFV3_IS_DNA_LSA(lsa_info) \
                ((lsa_info->u2LsaAge) & OSPFV3_DO_NOT_AGE)

#define OSPFV3_GET_LSA_OPTIONS(pLsa)      *((UINT1 *)((pLsa) + \
                                             OSPFV3_OPTIONS_OFFSET_IN_LS_HEADER))

#define OSPFV3_IS_DC_BIT_SET_LSA(pLsa) \
                ((OSPFV3_GET_LSA_OPTIONS(pLsa)) & OSPFV3_DC_BIT_MASK)

#define OSPFV3_IS_DC_EXT_APPLICABLE_IF(interface) \
                (interface->bDcEndpt == OSIX_TRUE)

#define OSPFV3_IS_DC_EXT_APPLICABLE_PTOMP_IF(interface) \
           (interface->bDcEndpt == OSIX_TRUE &&  \
            interface->u1NetworkType == OSPFV3_IF_PTOMP)

#define IS_VALID_AREA_ID(u4Addr)      (u4Addr < 0xe0000000)
/*  Getting the base pointers */
#define OSPFV3_GET_BASE_PTR(type, memberName, pMember) \
       (type *) ((FS_ULONG)(pMember) - ((FS_ULONG) &((type *)0)->memberName))

#define OSPFV3_IS_V_BIT_SET(u1Options) \
                         (u1Options & OSPFV3_V_BIT_MASK)



/* This macro gets the path type of the rt entry */
#define OSPFV3_GET_PATH_TYPE(pRtEntry) \
   ((TMO_SLL_Count(&(pRtEntry->pathLst)) == 0)? (OSPFV3_INVALID_ROUTE): \
    (((tV3OsPath *)TMO_SLL_First(&(pRtEntry->pathLst)))->u1PathType))

/* This macro gets the area id of the Route entry  */
#define OSPFV3_GET_PATH_AREA_ID(pRtEntry) \
   ((tV3OsPath *)TMO_SLL_First(&(pRtEntry->pathLst)))->areaId

#define OSPFV3_GET_PATH(pRtEntry) \
   (tV3OsPath *)TMO_SLL_First( &(pRtEntry->pathLst))

/* Macros for NSSA */
#define OSPFV3_IS_TRNST_AREA(pArea)  (pArea->u4AreaType == OSPFV3_NORMAL_AREA)
#define OSPFV3_IS_TRNSLTR_ALWAYS(pArea) \
  (pArea->u1NssaTrnsltrRole == OSPFV3_TRNSLTR_ROLE_ALWAYS)

#define OSPFV3_IS_NSSA_ABR(pCxt) ((OSPFV3_IS_AREA_BORDER_RTR(pCxt)) \
                                 && (pCxt->u4NssaAreaCount > 0 ))

#define OSPFV3_IS_INDICATION_LSA_PRESENCE(area) \
                    (area->bIndicationLsaPresence == OSIX_TRUE)

#define OSPFV3_IS_DNA_LSA_PROCESS_CAPABLE(area) \
          ((area->u4DcBitResetLsaCount) + (area->u4DcBitResetSelfOrgLsaCount) == 0)

#define OSPFV3_E_BIT_OPTION_MISMATCH(pInterface,options) \
        ((pInterface->pArea->u4AreaType == OSPFV3_NORMAL_AREA) \
        && !(OSPFV3_OPT_ISSET(options, OSPFV3_E_BIT_MASK))) \
        ||(((pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) \
        ||(pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA)) \
        &&(OSPFV3_OPT_ISSET(options, OSPFV3_E_BIT_MASK)))

/* To check "N" bit option matching for NSSA */
#define OSPFV3_N_BIT_OPTION_MISMATCH(pInterface,options) \
  (((pInterface->pArea->u4AreaType == OSPFV3_NSSA_AREA) \
  && !(OSPFV3_OPT_ISSET(options, OSPFV3_N_BIT_MASK))) \
  || (((pInterface->pArea->u4AreaType == OSPFV3_NORMAL_AREA) \
           || (pInterface->pArea->u4AreaType == OSPFV3_STUB_AREA)) \
           && (OSPFV3_OPT_ISSET(options, OSPFV3_N_BIT_MASK))))

#define OSPFV3_NBR_LOSE_OR_GAIN_DR_OR_BDR(pNbr,dr_bdr_rtr) \
          ((V3UtilRtrIdComp(pNbr->nbrRtrId, pNbr->backupDesgRtr) != OSPFV3_EQUAL) \
            &&(V3UtilRtrIdComp(pNbr->nbrRtrId, dr_bdr_rtr) == OSPFV3_EQUAL)) \
          ||((V3UtilRtrIdComp(pNbr->nbrRtrId, pNbr->backupDesgRtr) == OSPFV3_EQUAL) \
             &&(V3UtilRtrIdComp(pNbr->nbrRtrId, dr_bdr_rtr) != OSPFV3_EQUAL))

#define  OSPFV3_LADD1BYTE(addr, val)     *((UINT1 *)addr) = val; addr++;             
#define  OSPFV3_LADD2BYTE(addr, val)     OSPFV3_BUFFER_WTOPDU(addr, val); addr += 2;  
#define  OSPFV3_LADD3BYTE(addr, val)     OSPFV3_THREE_BYTE_TO_PDU(addr, (val)); addr += 3;  
#define  OSPFV3_LADD4BYTE(addr, val)     OSPFV3_BUFFER_DWTOPDU(addr, val); addr += 4; 
#define  OSPFV3_LADDSTR(addr, src, len)  MEMCPY(addr, src, len); addr += len;    

/* macros extracting info from linear buffer */
#define OSPFV3_LGETSTR(addr, dest, len) \
                               MEMCPY((UINT1 *)dest, (UINT1 *)addr, len);\
                               addr += len;

/* the following macros have certain constraints
 * 1. used only in the rhs of assignment operator
 * 2. addr should be a single variable(not an expression)
 * 3. the macro itself should not be a part of any expression
 */

#define  OSPFV3_LGET1BYTE(addr)  *((UINT1 *)addr); addr += 1;             
#define  OSPFV3_LGET2BYTE(addr)  OSPFV3_BUFFER_WFROMPDU(addr); addr += 2;  
#define  OSPFV3_LGET3BYTE(addr)  OSPFV3_THREE_BYTE_FROM_PDU(addr); addr += 3;    
#define  OSPFV3_LGET4BYTE(addr)  OSPFV3_BUFFER_DWFROMPDU(addr); addr += 4; 

#define OSPFV3_THREE_BYTE_TO_PDU(addr, val) { \
                 *((UINT1 *)(addr)   ) =(UINT1) ((val >> 16)&0xff); \
                 *((UINT1 *)(addr) +1) =(UINT1) ((val >> 8)&0xff); \
                 *((UINT1 *)(addr) +2) =(UINT1) (val & 0xff);}

#define OSPFV3_THREE_BYTE_FROM_PDU(addr) \
       ( ((*((UINT1 *)(addr)   ) << 16) & 0x00ff0000) |\
         ((*((UINT1 *)(addr) +1) << 8 ) & 0x0000ff00) |\
         ((*((UINT1 *)(addr) +2)      ) & 0x000000ff) )

#define OSPFV3_IS_INDICATION_LSA(pLsa) \
          ((!(OSPFV3_IS_DC_BIT_SET_LSA(pLsa))) && \
          (OSPFV3_THREE_BYTE_FROM_PDU(pLsa + OSPFV3_SUMMARY_LSA_METRIC_OFFSET) \
              == OSPFV3_LS_INFINITY_24BIT))


#define OSPFV3_GET_LSA_AGE(pLsaInfo,pu2_lsa_age) { \
  if ((OSPFV3_IS_MAX_AGE (pLsaInfo->u2LsaAge)) && (!OSPFV3_IS_DNA_LSA (pLsaInfo)))\
      *(pu2_lsa_age) = pLsaInfo->u2LsaAge;\
  else {\
  UINT4 u4RemainingTime = 0; \
  TmrGetRemainingTime ( gV3OsRtr.timerLstId, (tTmrAppTimer *)&(pLsaInfo->lsaAgingTimer.timerNode) ,  (UINT4 *)&(u4RemainingTime) );\
  *(pu2_lsa_age) = (UINT2) ((OSPFV3_IS_DNA_LSA(pLsaInfo)) ? (pLsaInfo->u2LsaAge) : (pLsaInfo->u2LsaAge + ((OSPFV3_CHECK_AGE - (pLsaInfo->u2LsaAge % OSPFV3_CHECK_AGE)) - (UINT2)(u4RemainingTime/OSPFV3_NO_OF_TICKS_PER_SEC))));}\
}

#define OSPFV3_IS_ACTIVE_BACKBONE_CONNECTION(pCxt) \
                              (pCxt->pBackbone->u4FullNbrCount > 0)

#define OSPFV3_LSU_FIND_RECENT_HDR(p_hdr1,p_hdr2) \
        V3LsuFindRecentLsa ((p_hdr1)->lsaSeqNum, (p_hdr1)->u2LsaAge, \
                            (p_hdr1)->u2LsaChksum, (p_hdr2)->lsaSeqNum, \
                            (p_hdr2)->u2LsaAge, (p_hdr2)->u2LsaChksum)

#define OSPFV3_GET_NON_DEF_ASE_LSA_COUNT(pCxt)    (INT4)pCxt->u4NonDefAsLsaCount

#define   OSPFV3_SET_NULL_RTR_ID(rtrId) OSPFV3_BUFFER_DWTOPDU((rtrId), 0)
#define   OSPFV3_IS_NULL_RTR_ID(rtrId) (OSPFV3_BUFFER_DWFROMPDU((rtrId)) == 0)

#define  OSPFV3_BACKBONE_AREAID gV3OsNullIpAddr 

#define  OSPFV3_NULL_RTRID  OSPFV3_BACKBONE_AREAID

#define OSPFV3_NULL_LSID OSPFV3_NULL_RTRID
#define OSPFV3_NULL_ID(u4ContextId) \
    (gV3OsRtr.apV3OspfCxt[u4ContextId]->pBackbone->areaId)

#define V3UtilLinkStateIdComp(addr1, addr2)\
             V3UtilRtrIdComp(addr1, addr2) 

#define OSPFV3_IS_FORWARD_ADDR_NULL(pAddr) \
                     (MEMCMP (pAddr, &gV3OsNullIp6Addr, OSPFV3_IPV6_ADDR_LEN) \
              == OSPFV3_EQUAL) 

#define OSPFV3_IS_NULL_IP_ADDR(addr)   (OSPFV3_BUFFER_DWFROMPDU((addr)) == 0)

#define OSPFV3_IS_NON_DEFAULT_AS_EXTERNAL_LSA(pLsaInfo) \
       ((pLsaInfo->lsaId.u2LsaType == OSPFV3_AS_EXT_LSA) && \
        ((*((UINT1 *) (pLsaInfo->pLsa + OSPFV3_PREFIX_LEN_OFFSET))) != 0))


#define OSPFV3_IS_NON_DEFAULT_ASE_LSA(pLsa, lsHeader)\
                          ((lsHeader.u2LsaType == OSPFV3_AS_EXT_LSA) && \
        ((*((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET))) != 0))

#define  OSPFV3_IS_P_BIT_SET(pLsa) \
   ((*((UINT1 *) (pLsa + OSPFV3_PREFIX_LEN_OFFSET + 1))) & OSPFV3_P_BIT_MASK)

#define OSPFV3_IS_NU_OR_LA_BIT_SET(u1Option) \
         ((u1Option & OSPFV3_LA_BIT_MASK) || (u1Option & OSPFV3_NU_BIT_MASK)) 


#define  OSPFV3_IS_F_BIT_SET(u1GeneralOptions) \
                    (u1GeneralOptions & OSPFV3_EXT_F_BIT_MASK)


#define  OSPFV3_IS_NTBIT_SET_RTR_LSA(u1Options) \
                    (u1Options & OSPFV3_NT_BIT_MASK)


#define OSPFV3_GET_PREFIX_LEN_IN_BYTE(u1PrefixLen) \
                       (((u1PrefixLen + 31) / 32 ) * 4)

#define OSPFV3_IP6_ADDR_COPY(dst, src) MEMCPY (&dst, &src, \
                          OSPFV3_IPV6_ADDR_LEN)

#define OSPFV3_IP6_ADDR_PREFIX_COPY(dst, src, u1PrefixLen) MEMCPY (&dst, &src, \
                          OSPFV3_GET_PREFIX_LEN_IN_BYTE(u1PrefixLen))

#define  OSPFV3_IPV4_ADDR_COPY(dst,src)   MEMCPY(dst, src, OSPFV3_IPV4_ADDR_LEN)

/* Macros for validation of various data types. */
 
#define OSPFV3_IS_VALID_TRUTH_VALUE(x)      ((x == OSPFV3_ENABLED) || \
 (x == OSPFV3_DISABLED))
  

#define OSPFV3_IS_VALID_EXT_LSDB_LIMIT(x)       (x >= -1)
  
#define OSPFV3_IS_VALID_ROWSTATUS_VALUE(x) \
          ((x == ACTIVE) || (x == NOT_READY) || (x == NOT_IN_SERVICE))
   
#define OSPFV3_IS_VALID_METRIC(x)       ((x > 0) && (x <= 0xffff))

#define OSPFV3_IS_VALID_SPF_OR_HOLD_DELAY(x)       ((x >= 0) && (x <= 0xffff))

#define OSPFV3_IS_VALID_BIG_METRIC(x)   ((x >= 0) && (x <= 0xffffff))
#define OSPFV3_IS_VALID_PRIORITY(x)     ((x >= 0) && (x <= 0xff))
#define OSPFV3_IS_VALID_INSTANCE_ID(x)  ((x >= 0) && (x <= 0xff))
#define OSPFV3_IS_VALID_POS_INTEGER(x)  ((x >=0) && (x <= 0x7fffffff))
#define OSPFV3_IS_VALID_HELLO_RANGE(x)  ((x >= 1) && (x <= 0xffff))
#define OSPFV3_IS_VALID_MIN_LSA_TIME(x)  ((x > 0) && (x <= 0xffff))
#define OSPFV3_IS_VALID_AGE(x)          ((x >= 1) && (x <= 1800)) 
#define OSPFV3_IS_VALID_AGG_EFFECT(x) ((x == 1) || (x == 2))
#define OSPFV3_IS_VALID_DEAD_INTERVAL(x) ((x > 0) && (x <= 0xffff))
   
#define OSPFV3_IS_VALID_IF_INDEX(x) (x > 0)
#define OSPFV3_IS_VALID_POLL_INTERVAL(x) ((x > 0) && (x <= 0xffffffff))
   
#define OSPFV3_IS_VALID_IF_TYPE_VALUE(x)        ( (x > 0) && (x < 6) && (x != 4))

   
#define OSPFV3_IS_VALID_ABR_TYPE(x)    ((x == OSPFV3_STANDARD_ABR) || \
  (x == OSPFV3_CISCO_ABR)    || \
  (x == OSPFV3_IBM_ABR))
 

#define OSPFV3_IS_VALID_EXT_METRIC_TYPE(x)   ((x == OSPFV3_TYPE_1_METRIC) || \
  (x == OSPFV3_TYPE_2_METRIC))
 
#define OSPFV3_IS_VALID_RRD_TAG_TYPE_VALUE(x) \
( ((x) == OSPFV3_MANUAL_TAG) || (x == OSPFV3_AUTOMATIC_TAG) )
 

#define OSPFV3_IS_VALID_RRD_STATUS_VALUE(x) \
 ( ((x) == OSPFV3_ENABLED) || (x == OSPFV3_DISABLED) )
 
 
#define OSPFV3_IS_INTERNAL_RTR_IN_STUB_AREA(x) \
 ((TMO_SLL_Count(&(pV3OspfCxt->areasLst)) == 1) && \
  ((((tV3OsArea*)TMO_SLL_First(&pV3OspfCxt->areasLst))->u4AreaType)  \
   == OSPFV3_STUB_AREA ))

#define OSPFV3_IS_VALID_ROUTER_ID(x)  (V3UtilRtrIdComp (x, OSPFV3_NULL_RTRID) \
  != OSPFV3_EQUAL)
 
#define OSPFV3_ADD_PREFIX_IN_LSA(currPtr, ip6Addr, u1PrefixLen) \
{\
 UINT1 u1Bits = 0; \
        UINT1 u1Bytes = 0; \
        u1Bytes = (UINT1) (u1PrefixLen /8); \
 u1Bits = (UINT1) (u1PrefixLen % 8); \
 MEMCPY (currPtr, &ip6Addr, u1Bytes);\
 MEMSET (currPtr + u1Bytes, 0 , \
  OSPFV3_GET_PREFIX_LEN_IN_BYTE (u1PrefixLen) - u1Bytes); \
    if (u1Bytes < OSPFV3_IPV6_ADDR_LEN) \
       *(currPtr + u1Bytes) = \
        (UINT1) (ip6Addr.u1_addr[u1Bytes] & (0xff << (8 - u1Bits)));\
}
 
/* Invalid context Id */
#define OSPFV3_INVALID_CXT_ID (OSPFV3_MAX_CONTEXTS_LIMIT + 1)

/* Invalid Cli context Id */
#define OSPFV3_CLI_INVALID_CXT_ID    0xFFFFFFFF

/* Macros for return values */
#define OSPFV3_SUCCESS         OSIX_SUCCESS
#define OSPFV3_FAILURE         OSIX_FAILURE

/* Macros for true or false */
#define OSPFV3_TRUE            OSIX_TRUE
#define OSPFV3_FALSE           OSIX_FALSE
#define OSPF3_MAX_INPUT_SIZE   5

/* Macros to set/reset the bit in LsaRefesh flag in LsaInfo structure */
/* pLsaInfo is the LSA pointer and BitPosition is the exact bit to be set */
#define OSPFV3_LSA_REFRESH_BIT_SET(pLsaInfo,BitPosition) \
        pLsaInfo->u1LsaRefresh = (pLsaInfo->u1LsaRefresh | (0x01 << (BitPosition-1)))

#define OSPFV3_LSA_REFRESH_BIT_RESET(pLsaInfo,BitPosition) \
        pLsaInfo->u1LsaRefresh &= (~(0x01 << (BitPosition-1)))

#define OSPFV3_IS_LSA_REFRESH_BIT_SET(pLsaInfo,BitPosition) \
        (pLsaInfo->u1LsaRefresh & (0x01 << (BitPosition-1)))

#define OSPFV3_IS_TRAP_ENABLED(pV3OspfCxt, TrapId) (pV3OspfCxt->u4TrapControl & \
                                         (1 << (TrapId - 1)))

/* Macros for SYSLOG ALERT LEVEL */
#define OSPFV3_DEBUG_LEVEL    SYSLOG_DEBUG_LEVEL    /* Debug messages */

#define OSPFV3_INFO_LEVEL     SYSLOG_INFO_LEVEL     /* Used for logging 
                                                     * informational
                                                     * messages
                                                     */

#define OSPFV3_NOTICE_LEVEL   SYSLOG_NOTICE_LEVEL   /* Used for logging 
                                                     * message that
                                                     * require attention 
                                                     * but are not errors
                                                     */

#define OSPFV3_WARN_LEVEL     SYSLOG_WARN_LEVEL     /* Used for logging 
                                                     * warning messages 
                                                     */

#define OSPFV3_ERROR_LEVEL    SYSLOG_ERROR_LEVEL    /* Used for error 
                                                     * messages
                                                     */

#define OSPFV3_CRITICAL_LEVEL SYSLOG_CRITICAL_LEVEL /* Used for logging 
                                                     * critical errors 
                                                     */

#define OSPFV3_ALERT_LEVEL    SYSLOG_ALERT_LEVEL    /* Used for logging 
                                                     * messages that
                                                     * require immediate 
                                                     * attention.
                                                     */

#define OSPFV3_EMERG_LEVEL    SYSLOG_EMERG_LEVEL    /* Used for logging 
                                                     * messages that requires
                                                     * Emergency action
                                                     */


/* Dc Bit Fix */
#define OSPFV3_INC_DC_BIT_RESET_LSA_COUNT(pLsaInfo) \
    ((OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) ? \
          (pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount++) : \
          (pLsaInfo->pArea->u4DcBitResetLsaCount++))

#define OSPFV3_DEC_DC_BIT_RESET_LSA_COUNT(pLsaInfo) \
    if (OSPFV3_IS_SELF_ORIGINATED_LSA (pLsaInfo)) { \
        if (pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount != 0) \
            pLsaInfo->pArea->u4DcBitResetSelfOrgLsaCount--; } \
    else {\
        if (pLsaInfo->pArea->u4DcBitResetLsaCount != 0) \
          pLsaInfo->pArea->u4DcBitResetLsaCount--; } 
/****************************************************************************/
/* getting the appropriate pointers from the SLLs                           */
/****************************************************************************/                                                

#define V3_GET_IF_PTR_FROM_SORT_LST(x) ((tV3OsInterface *)(VOID *)(((UINT1 *)(x)) \
                                   - OSPFV3_OFFSET(tV3OsInterface, nextVirtIfNode)))


/*Macros for checking AT status*/
#define OSPFV3_AT_SUCCESS   1
#define OSPFV3_AT_SKIP      -2
#define OSPFV3_AT_FAIL      0
#define OSPFV3_AT_NBR_UPD   2
#define OSPFV3_AT_DROP      -1
#define OSPFV3_AT_NO_SUP      -3

#define OSPFV3_AT_ENABLED      1
#define OSPFV3_AT_DISABLED     2

#define  OSPFV3_SHA1_DIGEST_LEN    20
#define  OSPFV3_SHA2_256_DIGEST_LEN 32
#define  OSPFV3_SHA2_384_DIGEST_LEN 48
#define  OSPFV3_SHA2_512_DIGEST_LEN 64
/* Octets for SHA Types*/
#define OSPFV3_OCTET_SHA1  1
#define OSPFV3_OCTET_SHA2_256 4
#define OSPFV3_OCTET_SHA2_384 8
#define OSPFV3_OCTET_SHA2_512 12

#define  OSPF3_MUL_EXCL_SEM_ID    (gV3OsRtr.MulExclSemId)
#define  OSPF3_TASK_ID            (gV3OsRtr.Ospf3TaskId)
#define  OSPF3_Q_ID               (gV3OsRtr.Ospf3QId)
#define  OSPF3_RM_Q_ID            (gV3OsRtr.Ospf3RmQId)
#define  OSPF3_LNX_Q_ID           (gV3OsRtr.Ospf3LnxQId)
#define  OSPF3_RTM_LST_SEM_ID     (gV3OsRtr.RtmLstSemId)
#define  OSPF3_LOW_PRIO_Q_ID      (gV3OsRtr.Ospf3LPQId)
#ifdef   OSPF3_BCMXNP_HELLO_WANTED
#define  OSPF3_NP_HELLO_Q_ID      (gV3OsRtr.Ospf3NPHelloQId)
#endif
#define  OSPFV3_MAX_AUTHKEY_VALUE  65535
#define  OSPFV3_MIN_AUTHKEY_VALUE  0
#define  DEF_KEY_START_ACCEPT    0
#define  DEF_KEY_START_GENERATE  0             

#define  DEF_KEY_STOP_GENERATE   (UINT4)-1
#define  DEF_KEY_STOP_ACCEPT     (UINT4)-1
#define  OSPFV3_MAX_BUFFER_SIZE    2000
#endif /* _O3MACS_H */
