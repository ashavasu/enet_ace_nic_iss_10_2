/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3port.h,v 1.17 2017/12/26 13:34:25 siva Exp $
 *
 * Description:Contains the global definitions
 *             and prototypes to be used by the
 *             external modules.
 *
 *******************************************************************/
#ifndef  _O3PORT_H

#define  _O3PORT_H

#define OSPFV3_MAP_TO_TRUTH_VALUE(x) \
 ((x == OSIX_TRUE) ? OSPFV3_ENABLED : OSPFV3_DISABLED)
#define OSPFV3_MAP_FROM_TRUTH_VALUE(x) \
 ((x == OSPFV3_ENABLED) ? OSIX_TRUE : OSIX_FALSE)


/*** MACROs For Host To Network Byte Order Conversion ***
********************************************************/
#define OSPFV3_BUFFER_WTOPDU(pu1PduAddr,u2Value) \
               V3UtilWordToPdu(pu1PduAddr, u2Value)          

#define OSPFV3_BUFFER_DWTOPDU(pu1PduAddr,u4Value) \
               V3UtilDwordToPdu(pu1PduAddr, u4Value)          

/*******************************************************
*** MACROs For Network To Host Byte Order Conversion ***
********************************************************/
#define OSPFV3_BUFFER_WFROMPDU(pu1PduAddr) \
        OSIX_NTOHS(*((UINT2 *)(VOID *)(pu1PduAddr)))

#define OSPFV3_BUFFER_DWFROMPDU(pu1PduAddr) \
               V3UtilDwordFromPdu(pu1PduAddr)          

#define OSPFV3_CRU_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   CRU_BUF_Copy_FromBufChain(pMsg, ((UINT1 *)(VOID *) &u2Value), u4Offset, 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}


#define OSPFV3_BUFFER_GET_1_BYTE(pMsg, u4Offset, u1Value) \
   MEMCPY((UINT1 *)&u1Value, (pMsg + u4Offset), 1)

#define OSPFV3_BUFFER_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   MEMCPY((UINT1 *)&u2Value, (pMsg + u4Offset), 2);\
   u2Value = OSIX_NTOHS(u2Value);\
}

#define OSPFV3_BUFFER_GET_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   MEMCPY((UINT1 *)&u4Value, (pMsg + u4Offset), 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define OSPFV3_BUFFER_GET_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   MEMCPY(pu1_StringMemArea, pBufChain + u4Offset, u4_StringLength);\
}

#define OSPFV3_BUFFER_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  u1LinearBuf = (UINT1) u1Value;\
   MEMCPY((pMsg + u4Offset), ((UINT1 *) &u1LinearBuf), 1);\
}

#define OSPFV3_BUFFER_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   MEMCPY((pMsg + u4Offset), ((UINT1 *) &u2LinearBuf), 2);\
}

#define OSPFV3_BUFFER_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   MEMCPY((pMsg + u4Offset), ((UINT1 *) &u4LinearBuf), 4);\
}

#define OSPFV3_BUFFER_ASSIGN_STRING(pBufChain,pu1_StringMemArea,u4Offset,u4_StringLength) \
{\
   MEMCPY((pBufChain + u4Offset), pu1_StringMemArea, u4_StringLength);\
}

#define OSPFV3_BUFFER_EXTRACT_1_BYTE(pBufChain,u4Offset,u1Value)  \
{\
   MEMCPY(&u1Value, (pBufChain + u4Offset), 1);\
}

#define OSPFV3_BUFFER_EXTRACT_2_BYTE(pBufChain,u4Offset,u2Value)  \
{\
   MEMCPY(((UINT1 *) &u2Value), (pBufChain + u4Offset), 2);\
   u2Value = OSIX_NTOHS (u2Value);\
}

#define OSPFV3_BUFFER_EXTRACT_4_BYTE(pBufChain,u4Offset,u4Value)  \
{\
   MEMCPY(((UINT1 *) &u4Value), (pBufChain + u4Offset), 4);\
   u4Value = OSIX_NTOHL (u4Value);\
}

#define OSPFV3_BUFFER_EXTRACT_STRING(pBufChain,pu1StringMemArea,u4Offset,u4StringLength) \
{\
   MEMCPY(pu1StringMemArea, (pBufChain + u4Offset), u4StringLength);\
} 

#define OSPFV3_BUFFER_FILL_CHAR_IN_CHAIN(pChainHdr,u1Char,u4Offset,u4RepCount) \
{ \
   UINT1 u1LoopCountVar = 0; \
   UINT1 u1CharValue = u1Char; \
   UINT4 u4OffsetCopy = u4Offset; \
   for(u1LoopCountVar = 0; u1LoopCountVar < u4RepCount; u1LoopCountVar++){ \
      MEMCPY((pChainHdr + u4OffsetCopy),\
      (UINT1 *)&(u1CharValue), 1); \
      u4OffsetCopy++; \
   } \
}

#define OSPFV3_BUFFER_APPEND_STRING(pBufChain,pu1StringMemArea,u4Offset,u4StringLength) \
        MEMCPY((pBufChain + u4Offset), pu1StringMemArea, u4StringLength); 

#define OSPFV3_CRU_BUF_ALLOCATE_FREE_OBJ(PoolId,ppu1Block) \
        (*(ppu1Block) = (VOID *) MemAllocMemBlk ((tMemPoolId) PoolId))

#define OSPFV3_CRU_BUF_RELEASE_FREE_OBJ(PoolId,pu1Block) \
      V3OspfMemReleaseMemBlock ((tMemPoolId)PoolId,(UINT1 *)pu1Block)

#define  IP6_HDR_LEN                       sizeof(tIp6Hdr)
#define  OSPFV3_MEM_POOL_BLOCK_SIZE    64  
#define  OSPFV3_MEM_POOL_NO_OF_BLOCKS  512 
#define  OSPFV3_IF_NAME_LEN            64

/* Macros for RAW sockets */
#define OSPFV3_SEND_TO           sendto 
#define OSPFV3_SET_SOCK_OPT      setsockopt 
#define OSPFV3_RECV_FROM         recvfrom 
#define OSPFV3_CLOSE             close 
#define OSPFV3_SOCKET            socket 
#define OSPFV3_SELECT            select 
#define OSPFV3_FCNTL             fcntl 
        
/********* Definitions for FSAP2 - Task Priorities *********/
#define  OSPFV3_ROUTING_TASK_PRIORITY      100 

#define  OSPFV3_QUEUE_NAME                "OS3Q"
#define  OSPFV3_RM_QUEUE_NAME             "OS3R"
#define  OSPFV3_LOW_PRIORITY_QUEUE_NAME   "O3LQ"
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED) && \
    defined (LNXIP6_WANTED)
#define  OSPFV3_LNXVRF_QUEUE_NAME         "OSLV"
#endif
#ifdef OSPF3_BCMXNP_HELLO_WANTED
#define  OSPFV3_NP_HELLO_QUEUE_NAME         "O3HQ"
#endif


/********* Definitions for FSAP2 - Event Bit Positions *****/
#define  TMO1_TIMER_01_EXP_EVENT     (0x00001000) 
#define  TMO1_TASK_QUE_00_ENQ_EVENT  (0x00002000) 
#define  TMO1_TASK_QUE_05_ENQ_EVENT  (0x00004000) 
#define  TMO1_TASK_QUE_15_ENQ_EVENT  (0x00008000) 

#define OSPFV3_MSGQ_IF_EVENT      TMO1_TASK_QUE_15_ENQ_EVENT 
#define OSPFV3_IPV6_IF_MSG        1

/* The protocol field in IP header should be set to 89 */
#define  OSPFV3_PROTO  89 

#define OSPFV3_TASK_NAME    (const UINT1 *) "OSV3"       /* OSPFv3 Task Name */
#define OSPFV3_RT_SEM_NAME  "V3RT"
#define OSPFV3_RTMRT_LIST_SEM (UINT1 *) "V3RTMSEM"

#define OSPFV3_ALLOC_FAILURE -1   

#define OSPFV3_DEF_REF_BW       100000 
#define OSPFV3_IPV6_VERSION     6
#define OSPFV3_IP6ADDR_CPY(dst, src) MEMCPY(dst, src, sizeof(tIp6Addr))

#define OSPFV3_MAX_MTU_SIZE     1280
#define MAX_OSPF3_HP_MSGS  25
#define MAX_OSPF3_MSG_PROCESSED 10
#define MAX_OSPF3_LP_MSGS  40

#define OSPFV3_PKT_ARRIVAL_EVENT          0x00000001  
#define OSPFV3_RTM_EVENT               0x00000010

/* Timer event uses 0x100 */
#define OSPFV3_MSGQ_EVENT                 0x00001000
#define OSPFV3_RM_SUB_BULK_EVENT          0x00002000
#define  OSPFV3_NLH_PKT_ARRIVAL_EVENT     0x00004000
#ifdef OSPF3_BCMXNP_HELLO_WANTED
#define  OSPFV3_NP_HELLO_PKT_ARRIVAL_EVENT 0x00008000
#endif

#define  OSPFV3_GO_ACTIVE_RM_NOTIFY_EVENT  0x00040000
#define  OSPFV3_RMQ_EVENT                  0x00080000
#define  OSPFV3_CLEAR_EVENT                0x00100000
#define  TM50MS_TIMER_EXP_EVENT            0x00200000

/* Macros used to differentiate the messages in the message queues */
#define OSPFV3_IPV6_IF_ADDR_CHG_EVENT   4 
#define OSPFV3_IPV6_IF_STAT_CHG_EVENT   8 
#define OSPFV3_ROUTEMAP_UPDATE_EVENT      16
#define OSPFV3_VCM_CHG_EVENT              32
#define OSPFV3_RM_MSG_EVENT               64

/* BFD Event for neighbor down */
#define OSPFV3_BFD_NBR_DOWN_EVENT 128

#define OSPFV3_VAL_SET           1
#define OSPFV3_VAL_NO           2

#define OSPFV3_ALIGN_DATA(a,b) ((a % b) == 0) ? (a/b) :\
        ((a/b) + 1)


#define   OSPFV3_RT_ID      0x06 

#define OSPFV3_MUT_EXCL_SEM_NAME (const UINT1 *) "OVME"
#define OSPFV3_SEM_CREATE_INIT_CNT            1

#ifdef RAWSOCK_WANTED
#define  OSPFV3_DEF_SELECT_INTERVAL        10
#endif

#define OSPF3_INIT_COMPLETE(u4Status) lrInitComplete(u4Status)

#ifdef RM_WANTED
#define  OSPF3_IS_NP_PROGRAMMING_ALLOWED() L2RED_IS_NP_PROGRAMMING_ALLOWED()
#else
#define  OSPF3_IS_NP_PROGRAMMING_ALLOWED() OSIX_TRUE
#endif

#endif  /*  _O3PORT_H  */

