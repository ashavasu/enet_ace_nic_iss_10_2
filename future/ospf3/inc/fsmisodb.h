/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsmisodb.h,v 1.2 2017/12/26 13:34:24 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMISODB_H
#define _FSMISODB_H

UINT1 FsMIStdOspfv3TableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMIStdOspfv3AreaTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3AsLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3AreaLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3LinkLsdbTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3HostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIStdOspfv3IfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMIStdOspfv3VirtIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3NbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3NbmaNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsMIStdOspfv3VirtNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMIStdOspfv3AreaAggregateTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_IP_ADDR_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmiso [] ={1,3,6,1,4,1,29601,2,23};
tSNMP_OID_TYPE fsmisoOID = {9, fsmiso};


UINT4 FsMIStdOspfv3ContextId [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,1};
UINT4 FsMIStdOspfv3RouterId [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,2};
UINT4 FsMIStdOspfv3AdminStat [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,3};
UINT4 FsMIStdOspfv3VersionNumber [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,4};
UINT4 FsMIStdOspfv3AreaBdrRtrStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,5};
UINT4 FsMIStdOspfv3ASBdrRtrStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,6};
UINT4 FsMIStdOspfv3AsScopeLsaCount [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,7};
UINT4 FsMIStdOspfv3AsScopeLsaCksumSum [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,8};
UINT4 FsMIStdOspfv3OriginateNewLsas [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,9};
UINT4 FsMIStdOspfv3RxNewLsas [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,10};
UINT4 FsMIStdOspfv3ExtLsaCount [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,11};
UINT4 FsMIStdOspfv3ExtAreaLsdbLimit [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,12};
UINT4 FsMIStdOspfv3MulticastExtensions [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,13};
UINT4 FsMIStdOspfv3ExitOverflowInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,14};
UINT4 FsMIStdOspfv3DemandExtensions [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,15};
UINT4 FsMIStdOspfv3TrafficEngineeringSupport [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,16};
UINT4 FsMIStdOspfv3ReferenceBandwidth [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,17};
UINT4 FsMIStdOspfv3RestartSupport [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,18};
UINT4 FsMIStdOspfv3RestartInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,19};
UINT4 FsMIStdOspfv3RestartStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,20};
UINT4 FsMIStdOspfv3RestartAge [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,21};
UINT4 FsMIStdOspfv3RestartExitReason [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,22};
UINT4 FsMIStdOspfv3Status [ ] ={1,3,6,1,4,1,29601,2,23,1,1,1,23};
UINT4 FsMIStdOspfv3AreaId [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,1};
UINT4 FsMIStdOspfv3ImportAsExtern [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,2};
UINT4 FsMIStdOspfv3AreaSpfRuns [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,3};
UINT4 FsMIStdOspfv3AreaBdrRtrCount [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,4};
UINT4 FsMIStdOspfv3AreaAsBdrRtrCount [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,5};
UINT4 FsMIStdOspfv3AreaScopeLsaCount [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,6};
UINT4 FsMIStdOspfv3AreaScopeLsaCksumSum [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,7};
UINT4 FsMIStdOspfv3AreaSummary [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,8};
UINT4 FsMIStdOspfv3StubMetric [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,9};
UINT4 FsMIStdOspfv3AreaNssaTranslatorRole [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,10};
UINT4 FsMIStdOspfv3AreaNssaTranslatorState [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,11};
UINT4 FsMIStdOspfv3AreaNssaTranslatorStabilityInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,12};
UINT4 FsMIStdOspfv3AreaNssaTranslatorEvents [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,13};
UINT4 FsMIStdOspfv3AreaStubMetricType [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,14};
UINT4 FsMIStdOspfv3AreaStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,15};
UINT4 FsMIStdOspfv3AreaDfInfOriginate [ ] ={1,3,6,1,4,1,29601,2,23,1,2,1,16};
UINT4 FsMIStdOspfv3AsLsdbType [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,1};
UINT4 FsMIStdOspfv3AsLsdbRouterId [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,2};
UINT4 FsMIStdOspfv3AsLsdbLsid [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,3};
UINT4 FsMIStdOspfv3AsLsdbSequence [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,4};
UINT4 FsMIStdOspfv3AsLsdbAge [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,5};
UINT4 FsMIStdOspfv3AsLsdbChecksum [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,6};
UINT4 FsMIStdOspfv3AsLsdbAdvertisement [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,7};
UINT4 FsMIStdOspfv3AsLsdbTypeKnown [ ] ={1,3,6,1,4,1,29601,2,23,1,3,1,8};
UINT4 FsMIStdOspfv3AreaLsdbAreaId [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,1};
UINT4 FsMIStdOspfv3AreaLsdbType [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,2};
UINT4 FsMIStdOspfv3AreaLsdbRouterId [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,3};
UINT4 FsMIStdOspfv3AreaLsdbLsid [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,4};
UINT4 FsMIStdOspfv3AreaLsdbSequence [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,5};
UINT4 FsMIStdOspfv3AreaLsdbAge [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,6};
UINT4 FsMIStdOspfv3AreaLsdbChecksum [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,7};
UINT4 FsMIStdOspfv3AreaLsdbAdvertisement [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,8};
UINT4 FsMIStdOspfv3AreaLsdbTypeKnown [ ] ={1,3,6,1,4,1,29601,2,23,1,4,1,9};
UINT4 FsMIStdOspfv3LinkLsdbIfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,1};
UINT4 FsMIStdOspfv3LinkLsdbType [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,2};
UINT4 FsMIStdOspfv3LinkLsdbRouterId [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,3};
UINT4 FsMIStdOspfv3LinkLsdbLsid [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,4};
UINT4 FsMIStdOspfv3LinkLsdbSequence [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,5};
UINT4 FsMIStdOspfv3LinkLsdbAge [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,6};
UINT4 FsMIStdOspfv3LinkLsdbChecksum [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,7};
UINT4 FsMIStdOspfv3LinkLsdbAdvertisement [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,8};
UINT4 FsMIStdOspfv3LinkLsdbTypeKnown [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,9};
UINT4 FsMIStdOspfv3LinkLsdbContextId [ ] ={1,3,6,1,4,1,29601,2,23,1,5,1,10};
UINT4 FsMIStdOspfv3HostAddressType [ ] ={1,3,6,1,4,1,29601,2,23,1,6,1,1};
UINT4 FsMIStdOspfv3HostAddress [ ] ={1,3,6,1,4,1,29601,2,23,1,6,1,2};
UINT4 FsMIStdOspfv3HostMetric [ ] ={1,3,6,1,4,1,29601,2,23,1,6,1,3};
UINT4 FsMIStdOspfv3HostAreaID [ ] ={1,3,6,1,4,1,29601,2,23,1,6,1,4};
UINT4 FsMIStdOspfv3HostStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,6,1,5};
UINT4 FsMIStdOspfv3IfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,1};
UINT4 FsMIStdOspfv3IfAreaId [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,2};
UINT4 FsMIStdOspfv3IfType [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,3};
UINT4 FsMIStdOspfv3IfAdminStat [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,4};
UINT4 FsMIStdOspfv3IfRtrPriority [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,5};
UINT4 FsMIStdOspfv3IfTransitDelay [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,6};
UINT4 FsMIStdOspfv3IfRetransInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,7};
UINT4 FsMIStdOspfv3IfHelloInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,8};
UINT4 FsMIStdOspfv3IfRtrDeadInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,9};
UINT4 FsMIStdOspfv3IfPollInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,10};
UINT4 FsMIStdOspfv3IfState [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,11};
UINT4 FsMIStdOspfv3IfDesignatedRouter [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,12};
UINT4 FsMIStdOspfv3IfBackupDesignatedRouter [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,13};
UINT4 FsMIStdOspfv3IfEvents [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,14};
UINT4 FsMIStdOspfv3IfMulticastForwarding [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,15};
UINT4 FsMIStdOspfv3IfDemand [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,16};
UINT4 FsMIStdOspfv3IfMetricValue [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,17};
UINT4 FsMIStdOspfv3IfLinkScopeLsaCount [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,18};
UINT4 FsMIStdOspfv3IfLinkLsaCksumSum [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,19};
UINT4 FsMIStdOspfv3IfInstId [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,20};
UINT4 FsMIStdOspfv3IfDemandNbrProbe [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,21};
UINT4 FsMIStdOspfv3IfDemandNbrProbeRetxLimit [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,22};
UINT4 FsMIStdOspfv3IfDemandNbrProbeInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,23};
UINT4 FsMIStdOspfv3IfContextId [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,24};
UINT4 FsMIStdOspfv3IfStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,7,1,25};
UINT4 FsMIStdOspfv3VirtIfAreaId [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,1};
UINT4 FsMIStdOspfv3VirtIfNeighbor [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,2};
UINT4 FsMIStdOspfv3VirtIfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,3};
UINT4 FsMIStdOspfv3VirtIfTransitDelay [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,4};
UINT4 FsMIStdOspfv3VirtIfRetransInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,5};
UINT4 FsMIStdOspfv3VirtIfHelloInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,6};
UINT4 FsMIStdOspfv3VirtIfRtrDeadInterval [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,7};
UINT4 FsMIStdOspfv3VirtIfState [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,8};
UINT4 FsMIStdOspfv3VirtIfEvents [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,9};
UINT4 FsMIStdOspfv3VirtIfLinkScopeLsaCount [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,10};
UINT4 FsMIStdOspfv3VirtIfLinkLsaCksumSum [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,11};
UINT4 FsMIStdOspfv3VirtIfStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,8,1,12};
UINT4 FsMIStdOspfv3NbrIfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,1};
UINT4 FsMIStdOspfv3NbrRtrId [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,2};
UINT4 FsMIStdOspfv3NbrAddressType [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,3};
UINT4 FsMIStdOspfv3NbrAddress [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,4};
UINT4 FsMIStdOspfv3NbrOptions [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,5};
UINT4 FsMIStdOspfv3NbrPriority [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,6};
UINT4 FsMIStdOspfv3NbrState [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,7};
UINT4 FsMIStdOspfv3NbrEvents [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,8};
UINT4 FsMIStdOspfv3NbrLsRetransQLen [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,9};
UINT4 FsMIStdOspfv3NbrHelloSuppressed [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,10};
UINT4 FsMIStdOspfv3NbrIfId [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,11};
UINT4 FsMIStdOspfv3NbrRestartHelperStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,12};
UINT4 FsMIStdOspfv3NbrRestartHelperAge [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,13};
UINT4 FsMIStdOspfv3NbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,14};
UINT4 FsMIStdOspfv3NbrContextId [ ] ={1,3,6,1,4,1,29601,2,23,1,9,1,15};
UINT4 FsMIStdOspfv3NbmaNbrIfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,1};
UINT4 FsMIStdOspfv3NbmaNbrAddressType [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,2};
UINT4 FsMIStdOspfv3NbmaNbrAddress [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,3};
UINT4 FsMIStdOspfv3NbmaNbrPriority [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,4};
UINT4 FsMIStdOspfv3NbmaNbrRtrId [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,5};
UINT4 FsMIStdOspfv3NbmaNbrState [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,6};
UINT4 FsMIStdOspfv3NbmaNbrStorageType [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,7};
UINT4 FsMIStdOspfv3NbmaNbrContextId [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,8};
UINT4 FsMIStdOspfv3NbmaNbrStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,10,1,9};
UINT4 FsMIStdOspfv3VirtNbrArea [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,1};
UINT4 FsMIStdOspfv3VirtNbrRtrId [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,2};
UINT4 FsMIStdOspfv3VirtNbrIfIndex [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,3};
UINT4 FsMIStdOspfv3VirtNbrAddressType [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,4};
UINT4 FsMIStdOspfv3VirtNbrAddress [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,5};
UINT4 FsMIStdOspfv3VirtNbrOptions [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,6};
UINT4 FsMIStdOspfv3VirtNbrState [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,7};
UINT4 FsMIStdOspfv3VirtNbrEvents [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,8};
UINT4 FsMIStdOspfv3VirtNbrLsRetransQLen [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,9};
UINT4 FsMIStdOspfv3VirtNbrHelloSuppressed [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,10};
UINT4 FsMIStdOspfv3VirtNbrIfId [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,11};
UINT4 FsMIStdOspfv3VirtNbrRestartHelperStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,12};
UINT4 FsMIStdOspfv3VirtNbrRestartHelperAge [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,13};
UINT4 FsMIStdOspfv3VirtNbrRestartHelperExitReason [ ] ={1,3,6,1,4,1,29601,2,23,1,11,1,14};
UINT4 FsMIStdOspfv3AreaAggregateAreaID [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,1};
UINT4 FsMIStdOspfv3AreaAggregateAreaLsdbType [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,2};
UINT4 FsMIStdOspfv3AreaAggregatePrefixType [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,4};
UINT4 FsMIStdOspfv3AreaAggregatePrefix [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,5};
UINT4 FsMIStdOspfv3AreaAggregatePrefixLength [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,6};
UINT4 FsMIStdOspfv3AreaAggregateEffect [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,7};
UINT4 FsMIStdOspfv3AreaAggregateRouteTag [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,8};
UINT4 FsMIStdOspfv3AreaAggregateStatus [ ] ={1,3,6,1,4,1,29601,2,23,1,12,1,9};


tMbDbEntry fsmisoMibEntry[]= {

{{13,FsMIStdOspfv3ContextId}, GetNextIndexFsMIStdOspfv3Table, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RouterId}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RouterIdGet, FsMIStdOspfv3RouterIdSet, FsMIStdOspfv3RouterIdTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3AdminStat}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3AdminStatGet, FsMIStdOspfv3AdminStatSet, FsMIStdOspfv3AdminStatTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3VersionNumber}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3VersionNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaBdrRtrStatus}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3AreaBdrRtrStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3ASBdrRtrStatus}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3ASBdrRtrStatusGet, FsMIStdOspfv3ASBdrRtrStatusSet, FsMIStdOspfv3ASBdrRtrStatusTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3AsScopeLsaCount}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3AsScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3AsScopeLsaCksumSum}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3AsScopeLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3OriginateNewLsas}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3OriginateNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RxNewLsas}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RxNewLsasGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3ExtLsaCount}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3ExtLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3ExtAreaLsdbLimit}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3ExtAreaLsdbLimitGet, FsMIStdOspfv3ExtAreaLsdbLimitSet, FsMIStdOspfv3ExtAreaLsdbLimitTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3MulticastExtensions}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3MulticastExtensionsGet, FsMIStdOspfv3MulticastExtensionsSet, FsMIStdOspfv3MulticastExtensionsTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3ExitOverflowInterval}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3ExitOverflowIntervalGet, FsMIStdOspfv3ExitOverflowIntervalSet, FsMIStdOspfv3ExitOverflowIntervalTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3DemandExtensions}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3DemandExtensionsGet, FsMIStdOspfv3DemandExtensionsSet, FsMIStdOspfv3DemandExtensionsTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3TrafficEngineeringSupport}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3TrafficEngineeringSupportGet, FsMIStdOspfv3TrafficEngineeringSupportSet, FsMIStdOspfv3TrafficEngineeringSupportTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3ReferenceBandwidth}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3ReferenceBandwidthGet, FsMIStdOspfv3ReferenceBandwidthSet, FsMIStdOspfv3ReferenceBandwidthTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RestartSupport}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RestartSupportGet, FsMIStdOspfv3RestartSupportSet, FsMIStdOspfv3RestartSupportTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RestartInterval}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RestartIntervalGet, FsMIStdOspfv3RestartIntervalSet, FsMIStdOspfv3RestartIntervalTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RestartStatus}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RestartStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RestartAge}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RestartAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3RestartExitReason}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3RestartExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3TableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3Status}, GetNextIndexFsMIStdOspfv3Table, FsMIStdOspfv3StatusGet, FsMIStdOspfv3StatusSet, FsMIStdOspfv3StatusTest, FsMIStdOspfv3TableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3TableINDEX, 1, 0, 1, NULL},

{{13,FsMIStdOspfv3AreaId}, GetNextIndexFsMIStdOspfv3AreaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3ImportAsExtern}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3ImportAsExternGet, FsMIStdOspfv3ImportAsExternSet, FsMIStdOspfv3ImportAsExternTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "1"},

{{13,FsMIStdOspfv3AreaSpfRuns}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaSpfRunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaBdrRtrCount}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAsBdrRtrCount}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaAsBdrRtrCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaScopeLsaCount}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaScopeLsaCksumSum}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaScopeLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaSummary}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaSummaryGet, FsMIStdOspfv3AreaSummarySet, FsMIStdOspfv3AreaSummaryTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdOspfv3StubMetric}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3StubMetricGet, FsMIStdOspfv3StubMetricSet, FsMIStdOspfv3StubMetricTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaNssaTranslatorRole}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaNssaTranslatorRoleGet, FsMIStdOspfv3AreaNssaTranslatorRoleSet, FsMIStdOspfv3AreaNssaTranslatorRoleTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdOspfv3AreaNssaTranslatorState}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaNssaTranslatorStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaNssaTranslatorStabilityInterval}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaNssaTranslatorStabilityIntervalGet, FsMIStdOspfv3AreaNssaTranslatorStabilityIntervalSet, FsMIStdOspfv3AreaNssaTranslatorStabilityIntervalTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "40"},

{{13,FsMIStdOspfv3AreaNssaTranslatorEvents}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaNssaTranslatorEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaStubMetricType}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaStubMetricTypeGet, FsMIStdOspfv3AreaStubMetricTypeSet, FsMIStdOspfv3AreaStubMetricTypeTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "1"},

{{13,FsMIStdOspfv3AreaStatus}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaStatusGet, FsMIStdOspfv3AreaStatusSet, FsMIStdOspfv3AreaStatusTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 1, NULL},

{{13,FsMIStdOspfv3AreaDfInfOriginate}, GetNextIndexFsMIStdOspfv3AreaTable, FsMIStdOspfv3AreaDfInfOriginateGet, FsMIStdOspfv3AreaDfInfOriginateSet, FsMIStdOspfv3AreaDfInfOriginateTest, FsMIStdOspfv3AreaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaTableINDEX, 2, 0, 0, "2"},

{{13,FsMIStdOspfv3AsLsdbType}, GetNextIndexFsMIStdOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbRouterId}, GetNextIndexFsMIStdOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbLsid}, GetNextIndexFsMIStdOspfv3AsLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbSequence}, GetNextIndexFsMIStdOspfv3AsLsdbTable, FsMIStdOspfv3AsLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbAge}, GetNextIndexFsMIStdOspfv3AsLsdbTable, FsMIStdOspfv3AsLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbChecksum}, GetNextIndexFsMIStdOspfv3AsLsdbTable, FsMIStdOspfv3AsLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbAdvertisement}, GetNextIndexFsMIStdOspfv3AsLsdbTable, FsMIStdOspfv3AsLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AsLsdbTypeKnown}, GetNextIndexFsMIStdOspfv3AsLsdbTable, FsMIStdOspfv3AsLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3AsLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbAreaId}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbType}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbRouterId}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbLsid}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbSequence}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, FsMIStdOspfv3AreaLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbAge}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, FsMIStdOspfv3AreaLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbChecksum}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, FsMIStdOspfv3AreaLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbAdvertisement}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, FsMIStdOspfv3AreaLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaLsdbTypeKnown}, GetNextIndexFsMIStdOspfv3AreaLsdbTable, FsMIStdOspfv3AreaLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3AreaLsdbTableINDEX, 5, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbIfIndex}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbType}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbRouterId}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbLsid}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbSequence}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbSequenceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbAge}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbChecksum}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbChecksumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbAdvertisement}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbAdvertisementGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbTypeKnown}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbTypeKnownGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3LinkLsdbContextId}, GetNextIndexFsMIStdOspfv3LinkLsdbTable, FsMIStdOspfv3LinkLsdbContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3LinkLsdbTableINDEX, 4, 0, 0, NULL},

{{13,FsMIStdOspfv3HostAddressType}, GetNextIndexFsMIStdOspfv3HostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3HostTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3HostAddress}, GetNextIndexFsMIStdOspfv3HostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdOspfv3HostTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3HostMetric}, GetNextIndexFsMIStdOspfv3HostTable, FsMIStdOspfv3HostMetricGet, FsMIStdOspfv3HostMetricSet, FsMIStdOspfv3HostMetricTest, FsMIStdOspfv3HostTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3HostTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3HostAreaID}, GetNextIndexFsMIStdOspfv3HostTable, FsMIStdOspfv3HostAreaIDGet, FsMIStdOspfv3HostAreaIDSet, FsMIStdOspfv3HostAreaIDTest, FsMIStdOspfv3HostTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdOspfv3HostTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3HostStatus}, GetNextIndexFsMIStdOspfv3HostTable, FsMIStdOspfv3HostStatusGet, FsMIStdOspfv3HostStatusSet, FsMIStdOspfv3HostStatusTest, FsMIStdOspfv3HostTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3HostTableINDEX, 3, 0, 1, NULL},

{{13,FsMIStdOspfv3IfIndex}, GetNextIndexFsMIStdOspfv3IfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfAreaId}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfAreaIdGet, FsMIStdOspfv3IfAreaIdSet, FsMIStdOspfv3IfAreaIdTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "0"},

{{13,FsMIStdOspfv3IfType}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfTypeGet, FsMIStdOspfv3IfTypeSet, FsMIStdOspfv3IfTypeTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfAdminStat}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfAdminStatGet, FsMIStdOspfv3IfAdminStatSet, FsMIStdOspfv3IfAdminStatTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfRtrPriority}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfRtrPriorityGet, FsMIStdOspfv3IfRtrPrioritySet, FsMIStdOspfv3IfRtrPriorityTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIStdOspfv3IfTransitDelay}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfTransitDelayGet, FsMIStdOspfv3IfTransitDelaySet, FsMIStdOspfv3IfTransitDelayTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIStdOspfv3IfRetransInterval}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfRetransIntervalGet, FsMIStdOspfv3IfRetransIntervalSet, FsMIStdOspfv3IfRetransIntervalTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "5"},

{{13,FsMIStdOspfv3IfHelloInterval}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfHelloIntervalGet, FsMIStdOspfv3IfHelloIntervalSet, FsMIStdOspfv3IfHelloIntervalTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "10"},

{{13,FsMIStdOspfv3IfRtrDeadInterval}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfRtrDeadIntervalGet, FsMIStdOspfv3IfRtrDeadIntervalSet, FsMIStdOspfv3IfRtrDeadIntervalTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "40"},

{{13,FsMIStdOspfv3IfPollInterval}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfPollIntervalGet, FsMIStdOspfv3IfPollIntervalSet, FsMIStdOspfv3IfPollIntervalTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "120"},

{{13,FsMIStdOspfv3IfState}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfDesignatedRouter}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfBackupDesignatedRouter}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfBackupDesignatedRouterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfEvents}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfMulticastForwarding}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfMulticastForwardingGet, FsMIStdOspfv3IfMulticastForwardingSet, FsMIStdOspfv3IfMulticastForwardingTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "1"},

{{13,FsMIStdOspfv3IfDemand}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfDemandGet, FsMIStdOspfv3IfDemandSet, FsMIStdOspfv3IfDemandTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIStdOspfv3IfMetricValue}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfMetricValueGet, FsMIStdOspfv3IfMetricValueSet, FsMIStdOspfv3IfMetricValueTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfLinkScopeLsaCount}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfLinkScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfLinkLsaCksumSum}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfLinkLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfInstId}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfInstIdGet, FsMIStdOspfv3IfInstIdSet, FsMIStdOspfv3IfInstIdTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "0"},

{{13,FsMIStdOspfv3IfDemandNbrProbe}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfDemandNbrProbeGet, FsMIStdOspfv3IfDemandNbrProbeSet, FsMIStdOspfv3IfDemandNbrProbeTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "2"},

{{13,FsMIStdOspfv3IfDemandNbrProbeRetxLimit}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfDemandNbrProbeRetxLimitGet, FsMIStdOspfv3IfDemandNbrProbeRetxLimitSet, FsMIStdOspfv3IfDemandNbrProbeRetxLimitTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "10"},

{{13,FsMIStdOspfv3IfDemandNbrProbeInterval}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfDemandNbrProbeIntervalGet, FsMIStdOspfv3IfDemandNbrProbeIntervalSet, FsMIStdOspfv3IfDemandNbrProbeIntervalTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, "120"},

{{13,FsMIStdOspfv3IfContextId}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3IfTableINDEX, 1, 0, 0, NULL},

{{13,FsMIStdOspfv3IfStatus}, GetNextIndexFsMIStdOspfv3IfTable, FsMIStdOspfv3IfStatusGet, FsMIStdOspfv3IfStatusSet, FsMIStdOspfv3IfStatusTest, FsMIStdOspfv3IfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3IfTableINDEX, 1, 0, 1, NULL},

{{13,FsMIStdOspfv3VirtIfAreaId}, GetNextIndexFsMIStdOspfv3VirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfNeighbor}, GetNextIndexFsMIStdOspfv3VirtIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfIndex}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfIndexGet, FsMIStdOspfv3VirtIfIndexSet, FsMIStdOspfv3VirtIfIndexTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfTransitDelay}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfTransitDelayGet, FsMIStdOspfv3VirtIfTransitDelaySet, FsMIStdOspfv3VirtIfTransitDelayTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, "1"},

{{13,FsMIStdOspfv3VirtIfRetransInterval}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfRetransIntervalGet, FsMIStdOspfv3VirtIfRetransIntervalSet, FsMIStdOspfv3VirtIfRetransIntervalTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, "5"},

{{13,FsMIStdOspfv3VirtIfHelloInterval}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfHelloIntervalGet, FsMIStdOspfv3VirtIfHelloIntervalSet, FsMIStdOspfv3VirtIfHelloIntervalTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, "10"},

{{13,FsMIStdOspfv3VirtIfRtrDeadInterval}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfRtrDeadIntervalGet, FsMIStdOspfv3VirtIfRtrDeadIntervalSet, FsMIStdOspfv3VirtIfRtrDeadIntervalTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, "60"},

{{13,FsMIStdOspfv3VirtIfState}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfEvents}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfLinkScopeLsaCount}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfLinkScopeLsaCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfLinkLsaCksumSum}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfLinkLsaCksumSumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtIfStatus}, GetNextIndexFsMIStdOspfv3VirtIfTable, FsMIStdOspfv3VirtIfStatusGet, FsMIStdOspfv3VirtIfStatusSet, FsMIStdOspfv3VirtIfStatusTest, FsMIStdOspfv3VirtIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3VirtIfTableINDEX, 3, 0, 1, NULL},

{{13,FsMIStdOspfv3NbrIfIndex}, GetNextIndexFsMIStdOspfv3NbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrRtrId}, GetNextIndexFsMIStdOspfv3NbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrAddressType}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrAddress}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrOptions}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrPriority}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrPriorityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrState}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrEvents}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrLsRetransQLen}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrHelloSuppressed}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrIfId}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrRestartHelperStatus}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrRestartHelperAge}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrRestartHelperExitReason}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbrContextId}, GetNextIndexFsMIStdOspfv3NbrTable, FsMIStdOspfv3NbrContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3NbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrIfIndex}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrAddressType}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrAddress}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrPriority}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrPriorityGet, FsMIStdOspfv3NbmaNbrPrioritySet, FsMIStdOspfv3NbmaNbrPriorityTest, FsMIStdOspfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, "1"},

{{13,FsMIStdOspfv3NbmaNbrRtrId}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrRtrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READONLY, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrState}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrStorageType}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrStorageTypeGet, FsMIStdOspfv3NbmaNbrStorageTypeSet, FsMIStdOspfv3NbmaNbrStorageTypeTest, FsMIStdOspfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, "3"},

{{13,FsMIStdOspfv3NbmaNbrContextId}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrContextIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3NbmaNbrStatus}, GetNextIndexFsMIStdOspfv3NbmaNbrTable, FsMIStdOspfv3NbmaNbrStatusGet, FsMIStdOspfv3NbmaNbrStatusSet, FsMIStdOspfv3NbmaNbrStatusTest, FsMIStdOspfv3NbmaNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3NbmaNbrTableINDEX, 3, 0, 1, NULL},

{{13,FsMIStdOspfv3VirtNbrArea}, GetNextIndexFsMIStdOspfv3VirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrRtrId}, GetNextIndexFsMIStdOspfv3VirtNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrIfIndex}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrAddressType}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrAddressTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrAddress}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrOptions}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrOptionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrState}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrEvents}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrEventsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrLsRetransQLen}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrLsRetransQLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrHelloSuppressed}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrHelloSuppressedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrIfId}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrIfIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrRestartHelperStatus}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrRestartHelperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrRestartHelperAge}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrRestartHelperAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3VirtNbrRestartHelperExitReason}, GetNextIndexFsMIStdOspfv3VirtNbrTable, FsMIStdOspfv3VirtNbrRestartHelperExitReasonGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMIStdOspfv3VirtNbrTableINDEX, 3, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregateAreaID}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregateAreaLsdbType}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregatePrefixType}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregatePrefix}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregatePrefixLength}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, NULL},

{{13,FsMIStdOspfv3AreaAggregateEffect}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, FsMIStdOspfv3AreaAggregateEffectGet, FsMIStdOspfv3AreaAggregateEffectSet, FsMIStdOspfv3AreaAggregateEffectTest, FsMIStdOspfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, "0"},

{{13,FsMIStdOspfv3AreaAggregateRouteTag}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, FsMIStdOspfv3AreaAggregateRouteTagGet, FsMIStdOspfv3AreaAggregateRouteTagSet, FsMIStdOspfv3AreaAggregateRouteTagTest, FsMIStdOspfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 0, "0"},

{{13,FsMIStdOspfv3AreaAggregateStatus}, GetNextIndexFsMIStdOspfv3AreaAggregateTable, FsMIStdOspfv3AreaAggregateStatusGet, FsMIStdOspfv3AreaAggregateStatusSet, FsMIStdOspfv3AreaAggregateStatusTest, FsMIStdOspfv3AreaAggregateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMIStdOspfv3AreaAggregateTableINDEX, 6, 0, 1, NULL},
};
tMibData fsmisoEntry = { 154, fsmisoMibEntry };
#endif /* _FSMISODB_H */

