/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: o3prot.h,v 1.34 2018/01/25 10:03:45 siva Exp $
*
* Description:This file contains constants prototypes for all the
*           functions.
*
*******************************************************************/
#ifndef _O3PROT_H
#define _O3PROT_H

/******************************* o3util.c **********************************/
PUBLIC VOID V3UtilConstructHdr PROTO((tV3OsInterface *pInterface,
                                      UINT1 *pBuf, UINT1 u1Type, UINT2 u2Len));
PUBLIC INT1 V3UtilVirtIfIndComp
PROTO((tV3OsAreaId areaId1, tV3OsRouterId nbrId1,
       tV3OsAreaId areaId2, tV3OsRouterId nbrId2));
PUBLIC UINT2 V3UtilIp6Chksum PROTO((UINT1 *pBuf, UINT4 u4Len, tIp6Addr *srcAddr,
                                    tIp6Addr *dstAddr));
UINT4 V3UtilJitter PROTO((UINT4 u4Value, UINT4 u4JitterPcent));
PUBLIC INT1 V3UtilAreaIdComp PROTO((tV3OsAreaId area1, tV3OsAreaId area2));
PUBLIC INT1 V3UtilRtrIdComp PROTO((tV3OsRouterId area1, tV3OsRouterId area2));
PUBLIC VOID
V3UtilExtractLsHeaderFromPkt PROTO ((UINT1 *pPkt, tV3OsLsHeader * pLsHeader,
                              UINT2 u2OffsetLsaCount, UINT1 u1PktType));
PUBLIC UINT4 V3UtilGetTraceFlagInCxt PROTO ((UINT4 u4ContextId, UINT1 u1TraceType));
PUBLIC INT4 V3UtilSetContext PROTO ((UINT4 u4ContextId));
PUBLIC VOID V3UtilReSetContext PROTO ((VOID));
PUBLIC UINT4 V3UtilGetSecondsSinceBase PROTO ((tUtlTm utlTm));
PUBLIC VOID
V3UtilSendHello PROTO ((tV3OsInterface * pInterface, UINT1 u1Flag));
PUBLIC VOID
V3UtilOspfApplyRMapRule PROTO ((tV3OspfCxt *pV3OspfCxt));
PUBLIC VOID V3UtilDeleteQueueMsg PROTO ((VOID));
PUBLIC UINT4 V3UtilDwordFromPdu (UINT1 *);
PUBLIC VOID V3UtilDwordToPdu(UINT1 *,UINT4 u4Value);
PUBLIC VOID V3UtilWordToPdu(UINT1 *,UINT2 u2Value);
PUBLIC INT4 V3GetUnnumberedInterface (UINT4 u4IfIndex, tNetIpv6IfInfo * pNetIpv6IfInfo);
#ifdef OSPF3_BCMXNP_HELLO_WANTED
PUBLIC INT4 V3NPSendHelloPktInCxt (UINT1 *pPkt, UINT2 u2PktLen,
              tV3OsInterface * pInterface, tIp6Addr * pDestIp6Addr);
#endif

/******************************* o3ppp.c **********************************/
PUBLIC VOID V3PppSendPkt  PROTO((UINT1 * pPkt, UINT2 u2PktLen,
                                 tV3OsInterface * pInterface,
                                 tIp6Addr * pDestIp6Addr));
PUBLIC VOID V3PppRcvPkt   PROTO((UINT1 * pPkt, UINT2 u2Len,
                                 UINT4 u4IfIndex, tIp6Addr *pSrcAddr,
                                 tIp6Addr *pDestAddr));
PUBLIC tV3OsInterface * V3PppAssociateWithVirtualIf
PROTO ((tV3OsInterface * pInterface, tV3OsRouterId * pRtrId));

/******************************* o3hp.c ***********************************/
PUBLIC VOID V3HpSendHello PROTO((tV3OsInterface * pInterface,
                                 tV3OsNeighbor * pNbr, UINT1 u1TimerId));
PUBLIC tV3OsNeighbor  * V3HpSearchNbrLst PROTO((tV3OsRouterId * pRtrId,
                                 UINT4 u4InterfaceId,
                                 tV3OsInterface * pInterface));
PUBLIC VOID V3HpElectDesgRtr PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3HpElectActiveOnLink  PROTO ((tV3OsInterface *pInterface));

PUBLIC INT4 V3HpRcvHello PROTO((UINT1 * pHelloPkt, UINT2 u2PktLen,
                                tV3OsInterface * pInterface,
                                tIp6Addr * pSrcIp6Addr));
PUBLIC  
VOID V3HpGenerateStandbyLinkLsa PROTO ((tV3OsInterface * pInterface));

/******************************* o3ddp.c **********************************/
PUBLIC VOID V3DdpRcvDdp    PROTO((UINT1 * pDdPkt, UINT2 u2Len,
                           tV3OsNeighbor * pNbr));
PUBLIC UINT1 V3DdpBuildSummary PROTO((tV3OsNeighbor * pNbr));
PUBLIC VOID V3DdpClearSummary PROTO((VOID *pArg));
PUBLIC VOID V3DdpRxmtDdp PROTO((tV3OsNeighbor * pNbr));
PUBLIC VOID V3DdpSendDdp PROTO((tV3OsNeighbor * pNbr));

/******************************* o3if.c ***********************************/
PUBLIC tV3OsInterface *
V3IfCreateInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt,
                        tNetIpv6IfInfo *pNetIp6IfInfo));
PUBLIC VOID V3IfSetDefaultValues PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfActivate PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfDelete PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfDisable PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfUp PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfDown PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfUpdateState PROTO((tV3OsInterface * pInterface, UINT1 u1State));
PUBLIC VOID V3IfCleanup PROTO((tV3OsInterface * pInterface,
                               UINT1 u1CleanupFlag));
PUBLIC VOID V3IfSetAreaId PROTO((tV3OsInterface * pInterface,
                                tV3OsAreaId * pAreaId));
PUBLIC VOID V3IfSetAdmnStat PROTO((tV3OsInterface * pInterface,
                                  UINT1 u1Status));
PUBLIC VOID V3IfSetOperStat PROTO((tV3OsInterface * pInterface,
                            UINT1 u1Status));
PUBLIC INT1 V3IfSetMtuSize PROTO ((UINT4 u4IfIndex, UINT4 u4MtuSize));

PUBLIC INT1 V3IfPrefixAdd PROTO ((tV3OsInterface * pInterface,                                                                                                                                 tIp6Addr * ip6Addr, UINT1 u1PrefixLen));
PUBLIC VOID V3IfSetType PROTO((tV3OsInterface * pInterface, UINT1 u1Type));
PUBLIC VOID V3IfSetDemand PROTO((tV3OsInterface * pInterface,
                                UINT1 u1DemandValue));
PUBLIC INT4 V3IfSetPassive PROTO((tV3OsInterface * pInterface));

PUBLIC VOID
V3IfAllPassive PROTO((UINT1 u1Ospfv3DefaultPassiveInterface));


PUBLIC VOID 
V3IfSetLinkLSASuppression PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfParamChange PROTO((tV3OsInterface * pInterface,
                                   UINT1 u1UpdateFlag));
PUBLIC VOID V3IfDemandProbeOpen PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfDemandProbeClose PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IfAddrChgHdlr PROTO((tNetIpv6AddrChange *pAddressChange));
PUBLIC INT1 V3MapIfType PROTO ((UINT4 u4NetIp6IfType, UINT1 *pu1IfType));
PUBLIC UINT4 V3IfGetMetricFromSpeedInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT4 u4IfSpeed, UINT4 u4IfHighSpeed));
PUBLIC UINT4 V3UtilIfGetMetricFromSpeedInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT4 u4IfSpeed, UINT4 u4IfHighSpeed));
PUBLIC VOID V3IfDeleteAllInterfaceInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt));
PUBLIC INT4 V3IfIsMetricChange PROTO ((tV3OsNeighbor *pNbr,
                                       UINT4 *pu4IfMetric));
PUBLIC 
tV3OsInterface * V3IfGetPtrtoInterface PROTO ((tV3OsArea *pArea, UINT4 u4InterfaceId));


#ifdef BFD_WANTED
PUBLIC INT1 Ospfv3HandleNbrPathStatusChange PROTO ((UINT4 u4ContextId,
                                          tBfdClientNbrIpPathInfo * pNbrPathInfo));
#endif
PUBLIC INT4 IfOspfv3BfdRegister PROTO ((tV3OsNeighbor * pNbr));
PUBLIC INT4 IfOspfv3BfdDeRegister PROTO ((tV3OsNeighbor * pNbr, BOOL1 bIsDynDis));
PUBLIC UINT4 Ospfv3GetIfIndexFromPort PROTO ((UINT4 u4Port));


/******************************* o3vif.c **********************************/
PUBLIC tV3OsInterface  *
V3VifCreateInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt, tV3OsAreaId * pTransitAreaId,
                         tV3OsRouterId * pNbrId, tV3OsRowStatus rowStatus));
PUBLIC VOID V3VifActivate PROTO((tV3OsInterface * pInterface, tV3OsArea *pTransitArea));
PUBLIC VOID V3VifSetVifValues PROTO((tV3OsInterface * pVirtIf,
                                 tV3OsRtEntry * pRtEntry));
PUBLIC VOID V3VifSetVLValues PROTO((tV3OsInterface * pVirtIf,
                                 tV3OsCandteNode * pSpfNode));
PUBLIC VOID
V3VIfDeleteAllInterfaceInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt));

/******************************* o3nbr.c **********************************/
PUBLIC tV3OsNeighbor  *V3NbrCreate PROTO((tIp6Addr * pNbrIp6Addr,
                                    tV3OsRouterId * pNbrRtrId,
                                    tV3OsInterface * pInterface,
                                    UINT1 u1ConfigStatus));
PUBLIC VOID V3NbrActivate PROTO((tV3OsNeighbor * pNbr));
PUBLIC VOID V3NbrInactivate PROTO((tV3OsNeighbor * pNbr));
PUBLIC VOID V3NbrDelete PROTO((tV3OsNeighbor * pNbr));
PUBLIC VOID V3NbrUpdateState PROTO((tV3OsNeighbor * pNbr, UINT1 u1State));
PUBLIC INT4 V3NbrProcessPriorityChange PROTO((tV3OsNeighbor * pNbr,
                                    UINT1 u1NewPriority));
PUBLIC INT1 V3OspfNbrLlDown PROTO((tIp6Addr *ip6LinkAddr, UINT4 u4IfIndex));
PUBLIC VOID V3NbrGenerateRtrNetworkIntraLsas PROTO ((tV3OsNeighbor * pNbr));
PUBLIC  VOID V3NbrAddToSortNbrLst PROTO ((tV3OsNeighbor * pNbr));


/******************************* o3ism.c **********************************/
PUBLIC VOID V3IsmRunIsm PROTO((tV3OsInterface * pInterface, UINT1 u1IfEvent));
PUBLIC VOID V3IsmInvalid        PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmIfUp           PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmElectDr        PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmIfReset        PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmIfDown         PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmIfLoop         PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmElectActive    PROTO((tV3OsInterface *pInterface));
PUBLIC VOID V3IsmResetVariables PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IsmDisableIfTimers PROTO((tV3OsInterface * pInterface));
PUBLIC VOID V3IsmInitSchedQueueInCxt
PROTO ((tV3OspfCxt * pOspfv3Cxt, tV3OsInterface * pInterface));
PUBLIC VOID V3IsmProcessSchedQueue PROTO((VOID));
PUBLIC VOID V3IsmSchedule PROTO((tV3OsInterface * pInterface, UINT1 u1Event));
PUBLIC VOID V3IsmActiveDown     PROTO((tV3OsInterface *pInterface, 
                                       UINT4 u4DownIfId));
PUBLIC VOID V3IsmStandbyDown    PROTO((tV3OsInterface *pInterface, 
                                       UINT4 u4DownIfId));

/******************************* o3nsm.c **********************************/
PUBLIC VOID V3NsmRunNsm         PROTO((tV3OsNeighbor *pNbr , UINT1 nbr_event ));
PUBLIC VOID V3NsmInvalid        PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmLlDown         PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmDown              PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3Nsm1wayRcvd          PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmRestartAdj        PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmCheckAdj          PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmProcessAdj        PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmExchgDone         PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmNegDone           PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3Nsm2wayRcvd          PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmStart             PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmStartInactTimer   PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmRestartInactTimer PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmAttHelloRcvd      PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmLdngDone          PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmNoAction          PROTO((tV3OsNeighbor *pNbr));
PUBLIC VOID V3NsmResetVariables    PROTO((tV3OsNeighbor* pNbr));
PUBLIC INT1 V3NsmToBecomeAdj     PROTO ((tV3OsNeighbor * pNbr));

/********************************* o3fscli.c *************************************************/
PUBLIC INT1
V3OspfGetInterfaceNameFromIndex PROTO ((UINT4 u4IfIndex, UINT1 *pu1IfName));
PUBLIC tIp6Addr *
V3IfFindIfAddrFromRouterId PROTO ((tV3OsRouterId rtrId, tV3OsInterface *pInterface));
/******************************* ospf3cli.c *************************************************/
PUBLIC INT1        V3OspfCliConfigIfLinkLSASuppression
PROTO ((tCliHandle cliHandle, INT4 i4Passive, UINT4 u4IfIndex));
INT1
V3CliReadLineFromFile PROTO ((INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen,
                           INT2 *pi2ReadLen));
INT4
V3CliCalcSwAudCheckSum PROTO ((UINT1 *pu1FileName, UINT2 *pu2ChkSum));

INT4
V3CliGetShowCmdOutputToFile PROTO ((UINT1 *pu1FileName));
    
/********************************* o3main.c *************************************************/
PUBLIC tOsixTaskId Ospf3GetTaskId PROTO ((VOID)) ;
PUBLIC VOID V3OSPFTaskMain PROTO ((INT1 *pTaskId));
PUBLIC UINT4 V3OspfMemReleaseMemBlock (tMemPoolId PoolId, UINT1 *pu1Block);
PUBLIC VOID V3OspfProcessQMsg PROTO ((tOsixQId QID));
PUBLIC INT4 V3OspfSendRouteMapUpdateMsg PROTO ((UINT1*, UINT4));
PUBLIC VOID V3OspfDeleteRtTable PROTO ((VOID *pRoot));
PUBLIC VOID V3OspfMemClear PROTO ((VOID));
PUBLIC VOID V3OspfModuleShutDown PROTO ((VOID));
PUBLIC INT4 V3OspfModuleStart PROTO ((VOID));
PUBLIC VOID V3OspfProcessClearMsg PROTO ((VOID));
/********************************* o3port.c *************************************************/
PUBLIC VOID V3Ipv6OspfInterface PROTO ((tNetIpv6HliParams * pIp6HliParams));
PUBLIC INT4 V3OspfSendPkt PROTO ((UINT4 u4ContextId, UINT1 *pu1DataPkt, UINT4 u4Length,
               UINT4 u4IfIndex, tIp6Addr * pSrcIp6Addr, tIp6Addr * pDstIp6Addr,UINT1 u1HopLimit));
PUBLIC INT1 V3OspfJoinMcastGroup PROTO ((tIp6Addr * pMcastGroupAddr, UINT4 u4IfIndex, 
                                         UINT4 u4ContextId));
PUBLIC INT1 V3OspfLeaveMcastGroup PROTO ((tIp6Addr * pMcastGroupAddr, UINT4 u4IfIndex,
                                          UINT4 u4ContextId));
PUBLIC VOID V3OspfSendMultIfOnLink PROTO ((tV3OsInterface *pInterface, 
                                           UINT2 u1Operation,UINT4 u4DownIfId));
#ifdef RAWSOCK_WANTED
PUBLIC VOID V3OspfGetPkt PROTO ((INT4 i4RawSockId));
#endif

PUBLIC VOID V3DemandProbeOpen 
PROTO ((UINT4 u4IfIndex));

PUBLIC VOID V3DemandProbeClose 
PROTO ((UINT4 u4IfIndex));

PUBLIC VOID V3OSPFSetIfOperStat PROTO ((UINT4 u4IfIndex, UINT1 u1Status));
PUBLIC VOID V3OspfIfStatusChgHdlr PROTO((tNetIpv6IfStatChange *pIfStatusChange));

PUBLIC VOID V3OspfRcvPkt  PROTO((tCRU_BUF_CHAIN_HEADER *pControlPkt,
                                 UINT1 *pDataPkt, UINT4 u4Length,
                                 UINT4 u4IfIndex));
PUBLIC VOID V3OspfDeqPkt  PROTO((tV3OspfIp6Pkt *pV3OspfIp6Pkt));

PUBLIC VOID V3ProcessIp6Header PROTO ((UINT1 *pPkt, UINT4 *u4Ip6HdrLen,
                       tIp6Addr *pSrcIp6Addr, tIp6Addr *pDestIp6Addr));
PUBLIC UINT1 *V3UtilOsMsgAlloc PROTO((UINT4 u4Size));
PUBLIC VOID V3UtilOsMsgFree PROTO((UINT1 *msg));
PUBLIC VOID V3OspfIfAddrChg PROTO((tNetIpv6AddrChange *pAddrChange));
PUBLIC VOID V3OspfIfStatChg PROTO((tNetIpv6IfStatChange *pIfStatusChange));
PUBLIC INT4 V3OspfGetAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));
PUBLIC INT4 V3OspfVcmIsVcExist PROTO ((UINT4 u4OspfCxtId));
PUBLIC INT4 V3OspfIsVcmSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));
PUBLIC INT4 V3OspfGetSystemMode PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4 V3OspfGetSystemModeExt PROTO ((UINT2 u2ProtocolId));
PUBLIC INT4 V3OspfRegisterWithVcm PROTO ((VOID));
PUBLIC VOID V3OspfDeRegisterWithVcm PROTO ((VOID));
PUBLIC VOID V3OspfVcmCallback
PROTO ((UINT4 u4IpIfIndex, UINT4 u4ContextId, UINT1 u1BitMap));
PUBLIC INT4 V3OspfGetCxtIdFromCfaIndex
PROTO ((UINT4 u4IfIndex, UINT4 *pu4CxtId));
PUBLIC tFilteringRMap * V3OspfGetMinFilterRMap
PROTO ((tFilteringRMap * pFilterRMap1, INT1 i1Type1,
        tFilteringRMap * pFilterRMap2, INT1 i1Type2));
PUBLIC INT4 V3OspfCmpFilterRMapName
PROTO ((tFilteringRMap * pFilterRMap, tSNMP_OCTET_STRING_TYPE * pRMapName));

PUBLIC INT4 V3OspfRegisterWithRM PROTO ((VOID));
PUBLIC INT4 V3OspfDeRegisterWithRM PROTO ((VOID));
PUBLIC INT4 V3OspfEnqMsgToRm PROTO ((tRmMsg * pRmMsg, UINT2 u2Len));
PUBLIC INT4 V3OspfRmGetNodeState PROTO ((VOID));
PUBLIC INT4 V3OspfSendEventToRm PROTO ((tRmProtoEvt * pEvt));
PUBLIC VOID V3OspfRmSetBulkUpdateStatus PROTO ((VOID));
PUBLIC UINT4 V3OspfRmReleaseMemoryForMsg PROTO ((UINT1 * pu1RmMsg));
PUBLIC INT4 V3OspfRcvPktFromRM
PROTO ((UINT1 u1Event, tRmMsg * pRmMsg, UINT2 u2DataLen));
PUBLIC UINT1 V3OspfRmGetStandbyNodeCount PROTO ((VOID));
PUBLIC VOID V3OspfSendProtoAckToRM PROTO ((UINT4 u4SeqNum));
#ifdef RAWSOCK_HELLO_PKT_WANTED
#ifdef LNXIP6_WANTED
PUBLIC INT4 V3OspfCreateNLHelloSocket          PROTO((VOID));
PUBLIC VOID Ip6ifGetNLHelloPkt                PROTO((VOID));
PUBLIC INT4 V3OspfSetNLHelloSockOptions        PROTO((VOID));
PUBLIC VOID V3OspfCloseNLHelloSocket           PROTO((VOID));
PUBLIC VOID V3OspfPacketOnNLHelloSocket PROTO ((INT4 i4SockFd));
#endif
#endif


/********************************* o3tmr.c *************************************************/
PUBLIC VOID V3TmrInitTimerDesc PROTO ((VOID));
PUBLIC INT4 V3TmrSetTimer PROTO ((tV3OspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks));
PUBLIC VOID V3TmrRestartTimer PROTO ((tV3OspfTimer * pTimer, UINT1 u1TimerId, INT4 i4NrTicks));
PUBLIC VOID V3TmrDeleteTimer PROTO ((tV3OspfTimer * pTimer));
PUBLIC VOID V3TmrHandleExpiry PROTO ((VOID));
PUBLIC VOID V3TmrHandleHelloExpiry PROTO ((VOID));
/********************************* o3rtr.c *************************************************/
PUBLIC VOID V3RtrSetAsbrStatusInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                            UINT1 u1AsBdrRtrStatus));
PUBLIC VOID V3RtrEnterOverflowStateInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID V3RtrSetExtLsdbLimitInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC INT1 V3RtrSetRouterIdInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                          tV3OsRouterId rtrId));
PUBLIC VOID V3RtrSetExitOverflowIntervalInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID V3RtrHandleABRStatChngInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID V3RtrSetABRTypeInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                         UINT4 u4ABRType));
PUBLIC INT1 V3RtrSetProtocolStatusInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 u1AdmnStatus));


PUBLIC INT4
V3UtilSelectOspfRouterId PROTO ((UINT4 u4OspfCxtId,  tV3OsRouterId * prtrId));

PUBLIC VOID
V3RtrProtocolDisableTmrs PROTO ((tV3OspfCxt * pV3OspfCxt));



#ifdef RAWSOCK_WANTED
PUBLIC INT1 V3OspfCreateSocket PROTO ((UINT4 u4ContextId));
PUBLIC VOID V3OspfCloseSocket PROTO ((UINT4 u4ContextId));
PUBLIC VOID V3SendEvent2Ospf PROTO ((INT4 i4RawSockId));
#endif
PUBLIC INT1 V3RtrEnableInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,UINT1 u1ClearFlag));
PUBLIC INT1 V3RtrDisableInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,UINT1 u1ClearFlag));
PUBLIC INT4 V3RtrCreateCxt PROTO ((UINT4 u4ContextId));
PUBLIC INT4 V3RtrDeleteCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC INT1 V3RtrCheckABRStatInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/********************************* o3fetch.c *************************************************/
PUBLIC tV3OsArea *V3GetFindAreaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pAreaId));
PUBLIC tV3OsInterface     *V3GetFindIf PROTO ((UINT4 u4IfIndex));
PUBLIC tV3OsHost  *V3GetFindHostInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pHostIp6Addr));
PUBLIC tV3OsInterface *V3GetFindVirtIfInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTransitAreaId,
        tV3OsRouterId * pNbrId));
PUBLIC tV3OsNeighbor *V3GetFindNbrInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pNbrRtrId,
        UINT4 u4NbrIfIndex));

PUBLIC tV3OsNeighbor * V3GetFindNbrInCxtForBfd 
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pNbrIpAddr));

PUBLIC tV3OsNeighbor  *V3GetFindNbmaNbrInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pIp6Addr, UINT4 u4NbrIfIndex));
PUBLIC tV3OsNeighbor *V3GetFindVirtNbrInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pTransitAreaId,
        tV3OsRouterId * pNbrId));
PUBLIC tV3OsAsExtAddrRange *
V3GetFindAsExtRngInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pAsExtNet,
                               UINT1 u1PfxLen, tV3OsAreaId areaId));
PUBLIC tV3OsAddrRange *
V3GetFindAreaAddrRange PROTO ((tV3OsArea * pArea, tIp6Addr * pIp6Addr, UINT1 u1PfxLen,
                        UINT2 u2LsdbType));
PUBLIC tV3OsRedistrConfigRouteInfo 
*V3GetFindRrdConfRtInfoInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                     tIp6Addr * pRrdDestIP6Addr,
                                     UINT1 u1RrdDestPfxLen));
/************************** o3area.c  ***************************************/
PUBLIC tV3OsArea*  V3AreaCreateInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId * pAreaId));
PUBLIC VOID V3AreaParamChange PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaDelete PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaSendSummaryStatusChange PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaNssaSetAggrEffect
PROTO((tV3OsArea * pArea, tV3OsAddrRange* pNssaTrnsAddrRange,
       UINT1 u1AggrEffect));
PUBLIC VOID 
V3AreaNormalSetAggrEffect PROTO((tV3OsArea* pArea,
                   tV3OsAddrRange* pInternalAddrRange,
                   UINT1 u1AggrEffect));
PUBLIC VOID 
V3AreaChangeAreaType PROTO((tV3OsArea * pArea, INT4 i4AreaType));
PUBLIC VOID 
V3AreaSetAddrRangeTag PROTO ((tV3OsArea * pArea,
                              tV3OsAddrRange* pNssaTrnsAddrRange));
PUBLIC VOID
V3AreaDeleteAllAreasInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt));

/**************************o3rngint.c ***************************************/
PUBLIC VOID V3AreaDeleteTrnsAddrRng PROTO((tV3OsArea* pArea,
                                tV3OsAddrRange* pNssaTrnsAddrRange));

PUBLIC VOID V3AreaDeleteInternalAddrRng PROTO((tV3OsArea * pArea,
                              tV3OsAddrRange* pInternalRng));

PUBLIC tV3OsAddrRange *V3AreaCreateAddrRange PROTO((tV3OsArea * pArea,
                                                tIp6Addr * pIp6Addr,
                                        UINT1  u1PrefixLength,
                                    UINT2 u2LsdbType,
                                    tV3OsRowStatus bStatus));
PUBLIC VOID 
V3AreaActiveInternalAddrRng PROTO((tV3OsArea * pArea, 
                           tV3OsAddrRange* pInternalRng));
PUBLIC VOID V3AreaActiveTransAddrRngInCxt
PROTO((tV3OspfCxt * pV3OspfCxt));

PUBLIC UINT1 V3AreaAddRangeInRtLstInCxt
PROTO((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange));

PUBLIC INT1 V3AreaInsertRngInAreaLst PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                                       tV3OsArea * pArea));

/**************************o3rngext.c ***************************************/
PUBLIC VOID V3AreaExtAddrRangeAttrChngInCxt
PROTO((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange));
PUBLIC tV3OsAsExtAddrRange *
V3AreaAsExtAddDefaultValuesInCxt
PROTO((tV3OspfCxt * pV3OspfCxt, tIp6Addr* ip6Addr,
       UINT1 u1PrefixLength, UINT4 u4AreaId));
PUBLIC VOID
V3AreaAddExtAddrRangeInCxt
PROTO((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange));
PUBLIC VOID
V3AreaDelExtAddrRangeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange,
        UINT1 u1Flag));
PUBLIC VOID
V3AreaDeleteAllExtAggInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/*****************************o3nseln.c**************************************/
PUBLIC VOID V3AreaNssaTrnsltrRlChng PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaNssaFsmTrgrTrnsltrElnInCxt PROTO((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID V3AreaNssaFsmFindTranslator PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaNssaABRStatLostInCxt PROTO((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID V3AreaNssaTrnsltrStatLost PROTO((tV3OsArea * pArea));
PUBLIC VOID V3AreaNssaFsmUpdtTrnsltrState PROTO((tV3OsArea * pArea,
                                         UINT1 u1PrvState,
                            UINT1 u1NewState));

/*****************************o3agutil.c**************************************/
PUBLIC INT1
V3RagCompAddrRng PROTO((tV3OsAsExtAddrRange * pAsExtRange1,
                        tV3OsAsExtAddrRange * pAsExtRange2));
PUBLIC UINT1
V3RagIsExtRtFallInRange PROTO((tV3OsExtRoute*    pExtRoute,
                               tV3OsAsExtAddrRange* pAsExtRange));

PUBLIC tV3OsAsExtAddrRange *
V3RagFindMatChngBkBoneRangeInCxt PROTO((tV3OspfCxt * pV3OspfCxt,
                                        tV3OsExtRoute * pExtRoute));

PUBLIC INT1
V3RagChkLsaFallInRng PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                            tV3OsLsaInfo * pLsaInfo));
PUBLIC UINT1
V3RagCompRngAndLsa PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                          tV3OsLsaInfo * pLsaInfo));
PUBLIC UINT1
V3RagChkLsaFallInPrvRng PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                               tV3OsLsaInfo * pLsaInfo));
PUBLIC UINT1
V3RagUpdatCostTypeForRange PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                                  tV3OsExtRoute * pExtRoute));
PUBLIC VOID
V3RagUpdtRtLstBtwRangesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange,
        tV3OsAsExtAddrRange * pSpecLessRange, UINT1 u1Flag));
PUBLIC VOID
V3RagUpdtRtLstFromExtRtTableInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange));

PUBLIC VOID
V3RagOriginateNonAggLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute, tV3OsArea * pArea));

PUBLIC VOID
V3RagAddRouteInAggAddrRangeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute));

PUBLIC VOID
V3RagDelRouteFromAggAddrRangeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute));

tV3OsAsExtAddrRange *
V3RagGetNssaAddrRange
PROTO ((tV3OsArea * pArea, tIp6Addr * pIp6Addr, UINT1 u1PrefixLen));
/*****************************o3agsumm.c**************************************/
PUBLIC VOID
V3RagFlushLsaFallingInAddrRange PROTO((tV3OsArea * pArea,
                              tV3OsAddrRange* pInternalRng));
PUBLIC VOID V3RagSetAggFlag PROTO((tV3OsArea * pArea));
PUBLIC VOID V3RagGenerateAggLsa PROTO((tV3OsArea * pArea));
PUBLIC tV3OsAddrRange  * V3AreaFindInternalAddrRngInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr* pIp6Addr,
        tV3OsAreaId * pAreaId));
PUBLIC tV3OsAddrRange  * V3AreaFindActiveInternalAddrRngInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr*  pIp6Addr));

/***************************** o3agext.c**********************************/
PUBLIC VOID
V3RagUpdtBboneRngFrmExtLstInCxt PROTO((tV3OspfCxt * pV3OspfCxt,
                                       tV3OsAsExtAddrRange * pAsExtRange));
PUBLIC VOID
V3RagFlushBkBoneRangeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange* pAsExtRange,
        tV3OsExtRoute* pExtRoute));
PUBLIC UINT1
V3RagGenExtLsaInCxt PROTO((tV3OspfCxt * pV3OspfCxt,
                           UINT2 u2Type, UINT1 *pPtr));
PUBLIC VOID
V3RagInsertRouteInBkboneAggRngInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange,
        tV3OsExtRoute * pExtRoute));
PUBLIC VOID
V3RagProcessBkBoneRangeInCxt PROTO((tV3OspfCxt * pV3OspfCxt,
                                    tV3OsAsExtAddrRange * pAsExtRange));

/***************************** o3agnssa.c**********************************/
PUBLIC VOID
V3RagScanRtTableForAddrRangesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pMoreSpecRange,
        tV3OsAsExtAddrRange * pLessSpecRange));

PUBLIC UINT1
V3RagChkExtRtFallInPrvRngInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAsExtAddrRange * pAsExtRange,
        tV3OsExtRoute * pExtRoute));
PUBLIC tV3OsAsExtAddrRange *
V3RagFindAggAddrRangeInNssaForRt PROTO((tV3OsExtRoute * pExtRoute,
                               tV3OsArea * pArea));
PUBLIC VOID
V3RagFlushNssaRange PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                   tV3OsExtRoute * pExtRoute,
                   tV3OsArea * pArea));
PUBLIC VOID
V3RagProcessNssaRange PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                     tV3OsArea * pArea));
PUBLIC VOID
V3RagProcBboneRngForNssa PROTO((tV3OsAsExtAddrRange * pAsExtRange,
                        tV3OsArea * pArea));

/***************************** o3agtrns.c**********************************/
PUBLIC VOID
V3RagNssaType7LsaTranslation 
           PROTO((tV3OsArea * pArea, tV3OsLsaInfo * pLsaInfo));
PUBLIC VOID
V3RagNssaAdvertiseType7Rngs PROTO((tV3OsArea * pArea));
PUBLIC VOID
V3RagNssaFlshType5TrnsltdInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAddrRange * pAddrRng));
PUBLIC VOID
V3RagNssaType7To5TranslatorInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID
V3RagNssaProcAggRangeInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/***************************** o3util.c**********************************/
PUBLIC INT1
V3UtilIp6AddrComp PROTO((tIp6Addr* pAddr1, tIp6Addr* pAddr2));
PUBLIC INT4
V3UtilIp6PrefixComp PROTO((tIp6Addr* pAddr1,
                    tIp6Addr*  pAddr2,
                        UINT1 u1PrefixLen));
PUBLIC VOID
V3UtilIp6PrefixCopy PROTO((tIp6Addr*  pDest,
                    tIp6Addr*  pSrc,
                       UINT1 u1PrefixLen));
PUBLIC VOID V3UtilGetCxtName PROTO ((UINT4 u4ContextId, UINT1 * pu1Alias));
PUBLIC INT4 V3UtilOspfGetFirstCxtId PROTO ((UINT4 * pu4ContextId));
PUBLIC INT4 V3UtilOspfGetNextCxtId
PROTO ((UINT4 u4ContextId, UINT4 * pu4NextContextId));
PUBLIC INT4 V3UtilOspfIsValidCxtId PROTO ((INT4 i4ContextId));
PUBLIC tV3OspfCxt * V3UtilOspfGetCxt PROTO ((UINT4 u4ContextId));

/**************************** Inc MSR *************************************/
PUBLIC VOID
V3UtilMsrForScalarObj 
PROTO ((INT4 i4SetIntVal, UINT4 *pu4ObjectId,
        UINT4 u4OidLen, UINT1 u1IsRowStatus));

PUBLIC VOID V3UtilMsrForOspfTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, INT4 i4SetIntVal,
        tSNMP_OCTET_STRING_TYPE * pSetVal, UINT4 * pu4ObjectId, UINT4 u4OidLen,
        CHR1 c1Type, UINT1 IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForStdOspfAreaTable 
PROTO ((INT4 i4FsMIStdOspfv3ContextId, UINT4 u4FsMIStdOspfv3AreaId, 
        INT4 i4SetVal, UINT4 *pu4ObjectId, UINT4 u4OidLen,
        CHR1 c1Type, UINT1 IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForStdOspfHostTable 
PROTO ((INT4 i4FsMIStdOspfv3ContextId, INT4 i4FsMIStdOspfv3HostAddressType, 
        tSNMP_OCTET_STRING_TYPE * pFsMIStdOspfv3HostAddress, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, CHR1 c1Type, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForOspfIfTable 
PROTO ((INT4 i4FsMIStdOspfv3IfIndex, INT4 i4SetVal, UINT4 *pu4ObjectId, 
        CHR1 c1Type, UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForStdOspfVirtIfTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, UINT4 u4FsMIStdOspfv3VirtIfAreaId, 
        UINT4 u4FsMIStdOspfv3VirtIfNeighbor, INT4 i4SetVal, UINT4 *pu4ObjectId, 
        UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForStdOspfNbmaNbrTable 
PROTO ((INT4 i4FsMIStdOspfv3NbmaNbrIfIndex, INT4 i4FsMIStdOspfv3NbmaNbrAddressType, 
        tSNMP_OCTET_STRING_TYPE * pFsMIStdOspfv3NbmaNbrAddress, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForStdOspfAreaAggregateTable 
PROTO ((INT4 i4FsMIStdOspfv3ContextId, UINT4 u4FsMIStdOspfv3AreaAggregateAreaID, 
        INT4 i4FsMIStdOspfv3AreaAggregateAreaLsdbType, 
        INT4 i4FsMIStdOspfv3AreaAggregatePrefixType, 
        tSNMP_OCTET_STRING_TYPE * pFsMIStdOspfv3AreaAggregatePrefix, 
        UINT4 u4FsMIStdOspfv3AreaAggregatePrefixLength, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForAsExternalAggregationEffect 
PROTO ((INT4 i4FsMIStdOspfv3ContextId,  
        INT4 i4FsMIOspfv3AsExternalAggregationNetType, 
        tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3AsExternalAggregationNet, 
        UINT4 u4FsMIOspfv3AsExternalAggregationPfxLength, 
        UINT4 u4FsMIOspfv3AsExternalAggregationAreaId, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForOspfRedistRouteCfgTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, INT4 i4FsMIOspfv3RedistRouteDestType, 
        tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3RedistRouteDest, 
        UINT4 u4FsMIOspfv3RedistRoutePfxLength, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT1 IsRowStatus,
        UINT4 u4SeqNum, INT1 i1ConfStatus));


PUBLIC VOID
V3UtilMsrForfutOspfExtRouteTable PROTO((INT4 i4FsMIStdOspfv3ContextId,
                                     INT4 i4FsMIOspfv3ExtRouteDestType ,
         tSNMP_OCTET_STRING_TYPE *pFsMIOspfv3ExtRouteDest ,
         UINT4 u4FsMIOspfv3ExtRoutePfxLength ,
         INT4 i4FsMIOspfv3ExtRouteNextHopType ,
         tSNMP_OCTET_STRING_TYPE *pFsMIOspfv3ExtRouteNextHop ,
                                     INT4 i4SetVal, UINT4 *pu4ObjectId,
                                     UINT4 u4OidLen, UINT1 u1IsRowStatus,
                                     UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForDistInOutRouteMapTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, 
        tSNMP_OCTET_STRING_TYPE * pFsMIOspfv3DistInOutRouteMapName, 
        INT4 i4FsMIOspfv3DistInOutRouteMapType, INT4 i4SetVal, 
        UINT4 *pu4ObjectId, UINT4 u4OidLen, UINT1 IsRowStatus));

PUBLIC VOID V3UtilMsrForOspfVirtIfCryptoAuthTable
PROTO((INT4 i4FsMIStdOspfv3ContextId, UINT4 u4FsMIStdOspfv3AreaId,
         UINT4 u4FsMIStdOspfv3NbrId, INT4 i4SetVal,
         UINT4 *pu4ObjectId, UINT4 u4OidLen,
         UINT1 u1IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus));

PUBLIC VOID V3UtilMsrForOspfIfAuthTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, INT4 i4FsMIStdOspfv3IfIndex,INT4 i4KeyId,
                         tSNMP_OCTET_STRING_TYPE *pSetVal, INT4 i4SetVal,
                         UINT4 *pu4ObjectId, CHR1 c1Type, UINT4 u4OidLen,
                         UINT1 u1IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus));
PUBLIC VOID V3UtilMsrForOspfVirtIfAuthTable
PROTO ((INT4 i4FsMIStdOspfv3ContextId, UINT4 u4FsMIStdOspfv3AreaId,
                         UINT4 u4FsMIStdOspfv3NbrId, INT4 i4KeyId,
                         tSNMP_OCTET_STRING_TYPE *pSetVal, INT4 i4SetVal,
                         UINT4 *pu4ObjectId, CHR1 c1Type, UINT4 u4OidLen,
                         UINT1 u1IsRowStatus, UINT4 u4SeqNum, INT1 i1ConfStatus));




/***************************** o3lsflod.c**********************************/
PUBLIC VOID V3LsuAddToRxmtLst
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo));

PUBLIC tV3OsRxmtNode * V3LsuSearchLsaInRxmtLst
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo));

PUBLIC VOID V3LsuDeleteFromRxmtLst 
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
        UINT1 u1DelFlag));

PUBLIC VOID V3LsuDeleteFromAllRxmtLst 
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 u1DelFlag));

PUBLIC VOID V3LsuClearRxmtLst 
PROTO ((tV3OsNeighbor * pNbr));

PUBLIC UINT1 V3LsuRxmtLsu 
PROTO ((tV3OsNeighbor * pNbr));

PUBLIC UINT1 V3LsuFloodOut 
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsNeighbor * pRcvNbr,
        tV3OsArea * pFloodArea, UINT1 u1LsaChngdFlag,
        tV3OsInterface * pIface));

PUBLIC UINT1 V3LsuAddToLsu 
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo,
        tV3OsInterface * pInterface));

PUBLIC VOID V3LsuSendAllLsuInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuSendLsu
PROTO ((tV3OsNeighbor * pNbr, tV3OsInterface * pInterface));

PUBLIC VOID V3LsuClearUpdate 
PROTO ((tV3OsLsUpdate * pUpdate));

PUBLIC UINT2 V3LsuGetLsaAge 
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsInterface * pInterface));

PUBLIC VOID V3LsuProcessAckFlag 
PROTO ((tV3OsNeighbor *pNbr, UINT1 *pLsa, UINT1 u1AckFlag));

/*****************************o3lsproc.c**********************************/

PUBLIC UINT1 V3LsuCompLsaInstance 
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsLsHeader * pRcvdLsHeader));

PUBLIC UINT1 V3LsuFindRecentLsa 
PROTO ((tV3OsLsaSeqNum seqNum1, UINT2 u2Age1, UINT2 u2Chksum1,
           tV3OsLsaSeqNum seqNum2, UINT2 u2Age2, UINT2 u2Chksum2));

PUBLIC UINT1 V3LsuCompLsaContents 
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 *pRcvLsa));

PUBLIC INT4 V3LsuCheckActualChange 
PROTO ((UINT1 *pDbLsa, UINT1 *pRcvdLsa));

PUBLIC VOID V3LsuRcvLsUpdate 
PROTO ((UINT1 *pLsUpdate, tV3OsNeighbor * pNbr));

PUBLIC VOID V3LsuProcessRcvdSelfOrgLsa 
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsLsaSeqNum i4LsaSeqNum));

PUBLIC VOID        V3LsuProcessEqualInstLsa
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa));

/* o3lsinst.c */

PUBLIC VOID        V3LsuUpdateChksumAndCount
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 u1ChksumFlag));

PUBLIC VOID V3LsuClearLsaInfo PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC INT1        V3LsuProcessLsa
PROTO ((UINT1 *pLsa, UINT2 u2Len, tV3OsNeighbor * pNbr));

/*****************************o3lsinst.c**********************************/

PUBLIC tV3OsLsaInfo * V3LsuAddToLsdb 
PROTO ((tV3OsArea * pArea, UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId,
          tV3OsRouterId * pAdvRtrId, tV3OsInterface * pIface, UINT1 *pLsa));

PUBLIC UINT1 V3LsuGetLsaFloodScope 
PROTO ((UINT2 u2LsaType));

PUBLIC VOID V3LsuInstallLsa 
PROTO ((UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo, UINT1 u1FnEqvlFlag));

PUBLIC VOID V3LsuClearLsaDesc 
PROTO ((tV3OsLsaDesc * pLsaDesc));

PUBLIC UINT1 V3LsuDeleteLsaFromDatabase
PROTO ((tV3OsLsaInfo * pLsaInfo, UINT1 u1ForcedRelFlag));

PUBLIC tV3OsLsaInfo * V3LsuSearchDatabase 
PROTO ((UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId,
        tV3OsRouterId * pAdvRtrId, tV3OsInterface * pInterface, 
    tV3OsArea * pArea));

PUBLIC VOID V3LsuDeleteAllLsasInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuDeleteAreaScopeLsa 
PROTO ((tV3OsArea * pArea));

PUBLIC VOID V3LsuDeleteLinkScopeLsa
PROTO ((tV3OsInterface * pInterface));

PUBLIC VOID V3LsuDeleteASScopeLsaInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuFlushAllSelfOrgExtLsasInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuFlushAllSelfOrgLsasInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuFlushSelfOrgAreaScopeLsa 
PROTO ((tV3OsArea * pArea));

PUBLIC VOID V3LsuFlushSelfOrgLinkScopeLsa 
PROTO ((tV3OsInterface * pInterface));

PUBLIC VOID V3LsuGenerateNonDefaultAsExtLsasInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuFlushAllNonDefSelfOrgAseLsaInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuSplAgingActionInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId  *pRtrId, UINT1 u1Flag));

PUBLIC VOID V3LsuGenAllSelfOrgExtLsasInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuGenAllSelfOrgLsasInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3LsuGenSelfOrgAreaScopeLsa 
PROTO ((tV3OsArea * pArea));

PUBLIC VOID V3LsuGenSelfOrgLinkScopeLsa 
PROTO ((tV3OsInterface * pInterface));

PUBLIC INT1 V3IfDemandRBTAction 
PROTO ((tRBElem * pNode, eRBVisit visit, UINT4 u4Level,
        VOID *arg, VOID *out));

/*****************************o3lrq.c**********************************/

PUBLIC VOID V3LrqRcvLsaReq 
PROTO ((UINT1 *pLsaReqPkt, UINT2 u2Len, tV3OsNeighbor * pNbr));

PUBLIC VOID V3LrqSendLsaReq 
PROTO ((tV3OsNeighbor * pNbr));

PUBLIC VOID V3LrqRxmtLsaReq 
PROTO ((VOID *pArg));

PUBLIC UINT1 V3LrqAddToLsaReqLst 
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsHeader * pLsHeader));

PUBLIC VOID V3LrqDeleteFromLsaReqLst 
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsaReqNode * pLsaReqNode));

PUBLIC tV3OsLsaReqNode * V3LrqSearchLsaReqLst 
PROTO ((tV3OsNeighbor * pNbr, tV3OsLsHeader * pLsHeader));

PUBLIC VOID V3LrqClearLsaReqLst PROTO ((tV3OsNeighbor * pNbr));

/*****************************o3util.c**********************************/

PUBLIC INT1 V3UtilLsaIdComp 
PROTO ((UINT2 u2LsaType1, tV3OsLinkStateId linkStateId1, tV3OsRouterId advRtrId1,
        UINT2 u2LsaType2, tV3OsLinkStateId linkStateId2, tV3OsRouterId advRtrId2));

PUBLIC INT1 V3UtilVerifyLsaFletChksum 
PROTO ((UINT1 *pLsa, UINT2 u2Len));

PUBLIC VOID V3UtilComputeLsaFletChksum 
PROTO ((UINT1 *pLsa, UINT2 u2Len));
    
PUBLIC VOID
V3UtilExtractLsHeaderFromLbuf PROTO ((UINT1 *pLsa, tV3OsLsHeader * pLsHeader));


PUBLIC UINT4 V3UtilHashGetValue
PROTO ((UINT4 u4HashSize, UINT1 *pKey, UINT1 u1KeyLen));

PUBLIC INT1 V3UtilFindIsTransitArea 
PROTO ((tV3OsArea * pArea));

PUBLIC INT4 V3UtilConstRtrLsaRBTree PROTO ((UINT1 *pLsa));
PUBLIC INT4 V3UtilConstNwLsaRBTree PROTO ((UINT1 *pLsa));

PUBLIC VOID V3UtilSetLsaId
PROTO ((UINT4 u4LsaId, UINT2 u2InternalLsaType, tV3OsLsaInfo * pLsaInfo));

/*****************************o3lak.c**********************************/

PUBLIC VOID V3LakRcvLsAck 
PROTO ((UINT1 *pLsAckPkt, UINT2 u2PktLen, tV3OsNeighbor *pNbr));

PUBLIC VOID V3LakSendDirect
PROTO ((tV3OsNeighbor * pNbr, UINT1 *pLsa));

PUBLIC VOID V3LakSendDelayedAck PROTO ((VOID *pArg)); 

PUBLIC VOID V3LakAddToDelayedAck 
PROTO ((tV3OsInterface * pInterface, UINT1 *pLsa));

PUBLIC VOID V3LakClearDelayedAck PROTO ((tV3OsInterface * pInterface));

/*****************************o3lsgen.c**********************************/
PUBLIC  tV3OsLsaDesc *V3GetLsaDescInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId, UINT1 *pPtr));

PUBLIC VOID V3GenerateLsa 
PROTO ((tV3OsArea * pArea, UINT2 u2InternalType,
        tV3OsLinkStateId * pLsId, UINT1 *pPtr, UINT1 u1RefreshFlag));

PUBLIC INT4 V3SignalLsaRegenInCxt
PROTO ((tV3OspfCxt * pv3OspfCxt, UINT1 u1Type, UINT1 *pPtr));

PUBLIC VOID V3GenerateLsasForExtRoutesInCxt
PROTO ((tV3OspfCxt * pv3OspfCxt));

PUBLIC VOID V3GenerateDefSumToStubNssaArea PROTO((tV3OsArea * pArea));
PUBLIC VOID V3GenerateSummary 
PROTO ((tV3OsArea * pArea, tV3OsRtEntry * pRtEntry));

PUBLIC VOID V3GenerateSummaryToArea PROTO ((tV3OsArea * pArea));

PUBLIC VOID V3GenerateNssaDfLsa 
PROTO ((tV3OsArea * pArea, UINT2 u2InternalType, tV3OsLinkStateId * pLsId));

PUBLIC VOID V3GenLowestLsIdRtrLsa 
PROTO ((tV3OsArea  *pArea));

PUBLIC INT4 V3CondenseSummaryInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));
PUBLIC VOID V3ChkAndGenerateSumToNewAreasInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3ResetMinLsaForAllIAPrefixLsa 
PROTO ((tV3OsArea * pArea));

/*****************************o3lscons.c**********************************/

PUBLIC VOID V3ResetNssaFwdAddr 
PROTO ((tV3OsInterface * pInterface));
    
PUBLIC UINT1  * V3ConstructIntraAreaPrefixLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr,
        tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));

PUBLIC UINT1  * V3ConstructNetworkLsa 
PROTO ((tV3OsInterface * pInterface, tV3OsLsaSeqNum i4SeqNum));

PUBLIC UINT1  * V3ConstructLinkLsa 
PROTO ((tV3OsInterface * pInterface, 
        tV3OsLsaSeqNum i4SeqNum, UINT1 *pu1LinkSup));

PUBLIC UINT1  * V3ConstructInterAreaPrefixLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT2 u2Type, UINT1 *pStruct,
        tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));

PUBLIC UINT1  * V3ConstructInterAreaRouterLsa 
PROTO ((tV3OsArea * pArea, UINT2 u2Type, UINT1 *pStruct,
           tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));

PUBLIC UINT1  * V3ConstructRtrLsa
PROTO ((tV3OsArea * pArea, tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));

PUBLIC UINT1 * V3ConstructGraceLsa 
PROTO ((tV3OsInterface * pInterface, tV3OsLsaSeqNum i4SeqNum));


PUBLIC VOID V3AddFwdAddrToLstInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr fwdAddr));

PUBLIC VOID
V3GenExtLsaWithFwdAddr PROTO((tV3OsInterface * pInterface,
                          tIp6Addr *pIp6Addr,
                          UINT1      u1PrefixLen));


PUBLIC VOID
V3GenNssaLsaWithZeroFwdAddr PROTO((tV3OsArea* pArea,
                       tIp6Addr* pIp6Addr,
                       UINT1     u1PrefixLen));

PUBLIC VOID
V3GenNssaLsaWithNonZeroFwdAddr PROTO((tV3OsArea* pArea,
                          tIp6Addr* pIp6Addr,
                           UINT1     u1PrefixLen));


PUBLIC UINT1  * V3ConstructAsExtLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr, UINT2 u2InternalType,
        tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));

PUBLIC UINT1  * V3ConstructNssaLsa 
PROTO ((UINT1 *pPtr, UINT2 u2InternalType, tV3OsArea * pArea,
        tV3OsLsaSeqNum i4SeqNum, tV3OsLinkStateId * pLsId));


PUBLIC INT4 V3BuildSummaryParam 
PROTO ((tV3OsArea * pArea, tV3OsRtEntry * pRtEntry,
        tV3OsSummaryParam * pSummaryParam));

PUBLIC UINT1 * V3RefreshLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsLsaDesc * pLsaDesc, INT4 i4SeqNum,
        UINT2 u2LsaType, tV3OsLinkStateId * pLsId));

PUBLIC UINT1 V3IsTransitLink PROTO ((tV3OsInterface * pInterface));


PUBLIC VOID V3BuildDefaultSummaryParam 
PROTO ((tV3OsArea * pArea, tV3OsSummaryParam * pSummaryParam));



PUBLIC INT4 V3IsEligibleToGenAseLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute));

PUBLIC INT4 V3IsEligibleToGenNssaLsa 
PROTO ((tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea));

PUBLIC UINT1 V3HandleFuncEqvNssaLsa 
PROTO ((UINT1 *pLsa, tV3OsNeighbor * pNbr));

PUBLIC UINT1 V3HandleFuncEqvAsExtLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pLsa));

PUBLIC tV3OsDbNode * V3IsDbNodePresent 
PROTO ((UINT1 * pAddrPrefix, UINT1 u1PrefixLen, 
    tTMO_HASH_TABLE * pLsaHashTable, UINT2  u2LsaType));

PUBLIC INT4 V3ModifyNssaAsbrDefRtTransInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));


PUBLIC VOID V3SetLsIdInterAreaPrefixLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr, UINT2 u2Type));

PUBLIC VOID V3SetLsIdAsExtLsaInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 *pPtr, UINT2 u2Type));

PUBLIC VOID V3SetRtrLsaLsId 
PROTO ((UINT1 * pPtr, UINT1 u1Type, UINT1 u1ChngType));

PUBLIC VOID V3ResetRtrLsaLsId 
PROTO ((UINT1 * pPtr, UINT1 u1Type, UINT1 u1ChngType));

/*****************************o3host.c**********************************/

PUBLIC tV3OsHost * V3HostAddInCxt
PROTO ((tV3OspfCxt * pOspfv3Cxt, tIp6Addr * pHostAddr));

PUBLIC INT4 V3HostDelete PROTO ((tV3OsHost * pHost));

PUBLIC VOID V3HostDeleteAllHostsInCxt PROTO ((tV3OspfCxt * pOspfv3Cxt));

/*****************************o3agd.c**********************************/

PUBLIC VOID V3AgdFlushOut PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC INT4 V3AgdCheckChksum PROTO ((tV3OsLsaInfo * pLsaInfo));

/*****************************o3intra.c**********************************/

PUBLIC VOID V3GenIntraAreaPrefixLsa PROTO ((tV3OsArea * pArea));

/*****************************o3rintra.c**********************************/

PUBLIC VOID 
     V3RtcCalculateAbrRoute
PROTO ((tV3OsCandteNode * pSpfNode, tV3OsArea * pArea, UINT1 u1Status));

PUBLIC VOID 
     V3RtcCalculateAsbrRoute
PROTO ((tV3OsCandteNode * pSpfNode, tV3OsArea * pArea, UINT1 u1Status));


PUBLIC INT4
V3RtcCalculateIntraAreaRoute PROTO ((tV3OsArea * pArea));

PUBLIC VOID
V3RtcProcessLABitPrefix PROTO ((tV3OsCandteNode * pVertex, 
                    tV3OsPrefixInfo * pPrefInfo,
                       tV3OsArea * pArea, UINT1 u1Status));

PUBLIC VOID
V3RtcProcessIntraAreaPrefLsa PROTO ((tV3OsCandteNode * pSpfNode,
                             tV3OsLsaInfo * pLsaInfo, UINT1 * pLsa, 
                             tV3OsArea * pArea, UINT1 u1Status));

PUBLIC INT4
V3RtcIsChangeInSpfNodes PROTO ((tV3OsCandteNode * pOldSpfNode,
                       tV3OsCandteNode * pNewSpfNode));

PUBLIC INT4
V3RtcCalculateNextHop PROTO ((tV3OsInterface * pInterface, 
                              tV3OsCandteNode * pVertex,
                              tV3OsLinkNode * pLinkNode, tIp6Addr * pNextHop,
                              UINT1 u1CountFlag));

/*****************************o3spf.c**********************************/

PUBLIC VOID V3RtcBuildSpfTree PROTO ((tV3OsArea * pArea));
PUBLIC VOID V3RtcMarkAllSpfNodes PROTO ((tV3OsSpf pSpf));

PUBLIC VOID V3RtcProcessSpfNodes PROTO ((tV3OsArea * pArea));
PUBLIC VOID
V3RtcProcIntraPrefLsaOfSpfNode PROTO ((tV3OsCandteNode * pSpfNode, 
                        tV3OsArea * pArea, UINT1 u1Status));

PUBLIC tV3OsCandteNode *V3RtcSearchSpfTree
PROTO ((tV3OsSpf pSpfTree, tV3OsRouterId * pVertexRtrId, UINT4 u4VertexIfId,
        UINT1 u1VertType));

/*****************************o3rinter.c**********************************/

PUBLIC VOID V3RtcCalAllInterAreaRtsInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcCalInterAreaRtsAtABRInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcCalInterAreaRtsAtNonABRInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcCalculateInterAreaRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry));

/*****************************o3rtc.c**********************************/

PUBLIC INT1
V3RtcInitRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 u1ApplnId, UINT4 u4Type));

PUBLIC INT4 
V3RtcSetVirtNextHopInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsAreaId *pTranAreaId, 
        tV3OsRouterId *pDestRtrId, tIp6Addr *pVirtNextHop));
PUBLIC VOID
V3RtcProcessVirtualLinkInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pAbrRtEntry,
        tV3OsPath * pAbrPath, UINT1 u1Event));
PUBLIC VOID
V3RtcProcNonStandABRVirtualLink PROTO ((tV3OsCandteNode * pSpfNode,
                                 tV3OsArea * pArea, UINT1 u1Event));
PUBLIC VOID 
V3RtcCalculateLocalRoutes PROTO ((tV3OsInterface * pInterface, 
                      UINT1 u1Status));

PUBLIC VOID
V3RtcCalculateLocalRoute PROTO ((tV3OsInterface * pInterface, 
                  tV3OsPrefixNode *pPrefixNode, UINT1 u1Status));

PUBLIC VOID
V3RtcCalculateLocalHostRouteInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pHostAddr, tV3OsAreaId *pAreaId,
        UINT4 u4HostMetric, UINT1 u1Status));

PUBLIC VOID
V3RtcClearAndDeleteRtFromIpInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcCalculateRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC tV3OsRtEntry *
V3RtcFindRtEntryInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, VOID *pDest, UINT1 u1PrefixLen,
        UINT1 u1DestType));

PUBLIC VOID
V3RtcDeleteRtEntryInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));

PUBLIC VOID
V3RtcFreeRtEntry PROTO ((tV3OsRtEntry * pRtEntry));

PUBLIC UINT1       *
V3RtcSearchDatabase PROTO ((UINT2 u2LsaType, tV3OsRouterId * pVertexRtrId,
                            UINT4 u4VertexIfId, tV3OsArea * pArea));

PUBLIC tV3OsDbNode *
V3RtcSearchIntraAreaPrefixNode PROTO ((tV3OsRouterId * pVertexRtrId,
                                       UINT4 u4VertexIfId, UINT2 u2RefLsType,
                                       tV3OsArea * pArea));

PUBLIC tV3OsPath  *
V3RtcCreatePath PROTO ((tV3OsAreaId * pAreaId,
                        UINT1 u1PathType, UINT4 u4Cost, UINT4 u4Type2Cost));

PUBLIC VOID V3RtcAddPath 
PROTO ((tV3OsRtEntry * pRtEntry, tV3OsPath * pPath));

PUBLIC INT4 V3RtcProcessRtEntryChangesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry,
        tV3OsRtEntry * pOldRtEntry));

PUBLIC INT4
V3RtcProcessASBRRtEntryChangesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry,
        tV3OsRtEntry * pOldRtEntry));

PUBLIC INT1 V3RtcIsLsaValidForRtCalc 
PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC INT4 V3RtcGetExtLsaLink
PROTO ((UINT1 *pLsa, UINT2 u2LsaLen, tV3OsExtLsaLink * pExtLsaLink));

PUBLIC tV3OsRtEntry *
V3RtcRtLookupInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                           tIp6Addr * pDestIp6Addr));

PUBLIC tV3OsPath   * V3RtcFindAbrPathInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pAbrRtrId,
        tV3OsAreaId * pAreaId));

PUBLIC tV3OsPath   * V3RtcFindAsbrPathInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRouterId * pAsbrRtrId,
        tV3OsArea * pArea));
PUBLIC tV3OsPath   * V3RtcFindPreferredAsbrPathInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt,
 tV3OsRouterId * pAsbrRtrId));

PUBLIC tV3OsPath   * V3RtcFindPath 
PROTO ((tV3OsRtEntry * pRtEntry, tV3OsAreaId * pAreaId));

PUBLIC VOID
V3RtcDeletePath PROTO ((tV3OsRtEntry * pRtEntry, tV3OsAreaId * pAreaId));

PUBLIC VOID V3RtcProcessInterAreaDbNode 
PROTO ((tV3OsDbNode * pDbHashNode, tV3OsRtEntry * pDestRtEntry));

PUBLIC VOID V3RtcProcessTransitAreaDbNode
PROTO ((tV3OsDbNode * pDbHashNode, tV3OsRtEntry * pDestRtEntry));

PUBLIC VOID V3RtcProcessExtLsaDbNode 
PROTO ((tV3OsDbNode * pDbHashNode, tV3OsRtEntry * pDestRtEntry));

PUBLIC VOID V3RtcProcessNssaLsaDbNode 
PROTO ((tV3OsDbNode * pDbHashNode, tV3OsRtEntry * pDestRtEntry));

PUBLIC UINT1
V3RtcProcessNextHopChangesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry,
        tV3OsPath * pNewRtPath, tV3OsRtEntry * pOldRtEntry,
        tV3OsPath * pOldRtPath));

PUBLIC VOID
V3RtcFlushOutSummaryFromAllAreasInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT2 u2LsaType, tV3OsRtEntry * pRtEntry));

PUBLIC VOID
V3RtcFlushOutSummaryLsa PROTO ((tV3OsArea * pArea,
                         UINT2 u2LsaType, tV3OsLinkStateId * pLinkStateId));

PUBLIC VOID V3RtcClearSpfTree 
PROTO ((tV3OsSpf pSpfTree));

PUBLIC VOID V3RtcUpdateNextHops 
PROTO ((tV3OsPath * pDestRtPath,
                     tV3OsPath * pRtrPath, tV3OsLsaInfo * pLsaInfo));

PUBLIC VOID V3RtcCalculateRouteInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry));

PUBLIC VOID V3RtcGetRtTag 
PROTO ((tV3OsPath * pPath, UINT4 *pu4RtTag));

PUBLIC VOID V3RtcMarkAllRoutesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcProcessUnreachableRouteInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));

PUBLIC VOID V3RtcProcessUnreachableRoutesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID 
V3RtcSetRtTimerInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID
V3RtcCalculateAllLocalHostRouteInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

/*****************************o3rtincr.c**********************************/

PUBLIC VOID V3RtcChkAndCalAllExtRoutesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 u1DestType, tV3OsRouterId * pAsbrRtrId));

PUBLIC VOID V3RtcIncUpdateLinkLsa
PROTO ((tV3OsLsaInfo * pLsaInfo,
                       tIp6Addr * pOldNextHop, tIp6Addr * pNewNextHop));

PUBLIC VOID V3RtcIncUpdtRtSmmryLinkLsa PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC VOID V3RtcIncUpdateExtRoute PROTO ((tV3OsLsaInfo * pLsaInfo));

PUBLIC VOID
V3RtcIncUpdateIntraAreaPrefLsa PROTO ((UINT1 * pOldLsa, UINT1 * pNewLsa, 
                                      tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea));

PUBLIC VOID
V3RtcGetRefLsIdInfo PROTO ((UINT1 *pLsa, tV3OsLsaId * pLsaId));

/*****************************o3rtext.c**********************************/

PUBLIC VOID V3RtcCalculateASExtRoutesInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID V3RtcCalculateExtRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pNewRtEntry,
        tV3OsDbNode * pDbExtLsaNode));

/*****************************o3rbutil.c**********************************/

PUBLIC INT4
V3RtcCompareSpfNode PROTO ((tRBElem * pNode1, tRBElem * pNode2));

PUBLIC INT4 V3CompareHost 
PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC INT4 V3CompareLsa 
PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC UINT4 V3LsaRtrIdHashFunc 
PROTO ((tV3OsRouterId * pAdvRtrId));

PUBLIC INT4 V3RBInterfaceCmpFunc PROTO((tRBElem *e1, tRBElem *e2));

PUBLIC UINT4 V3LsaAddrPrefixHashFunc 
PROTO ((UINT1 * pAddrPrefix, UINT1 u1PrefixLength, UINT2 u2LsaType));

PUBLIC UINT4 V3LsaIntraAreaHashFunc
PROTO ((UINT2 u2RefLsaType, tV3OsLinkStateId * pRefLinkStateId, 
            tV3OsRouterId * pRefAdvRtrId));

PUBLIC INT4 V3CompRtrLsaLink
PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC INT4 V3CompNwLsaLink
PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC INT4 V3CompGRLsIdInfo 
PROTO ((tRBElem * e1, tRBElem * e2));

PUBLIC INT4 V3UtilRBFreeRouterLinks
PROTO ((tRBElem *pRBElem, UINT4 u4Arg));

PUBLIC INT4 V3UtilRBFreeNwLinks
PROTO ((tRBElem *pRBElem, UINT4 u4Arg));

PUBLIC INT4 V3OsUpdIfRouteLeakStatus 
PROTO ((tRBElem * pIfLeakRoute, tRBElem * pIfLeakRouteIn));

PUBLIC INT4 V3UtilRBFreeLsaIds
PROTO ((tRBElem *pRBElem, UINT4 u4Arg));

PUBLIC tV3OsDbNode *
V3RtcSearchLsaHashNode PROTO ((VOID *pPtr, UINT1 u1PrefixLen,
                        UINT2 u2LsaType, tV3OsArea * pArea));

PUBLIC tV3OsRtEntry * V3RtcCreateRtEntry PROTO ((VOID));

PUBLIC VOID V3RtcAddRtEntryInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));

PUBLIC UINT1 *
V3UtilExtractPrefixFromLsa PROTO ((UINT1 * pCurrPtr, 
                      tV3OsPrefixInfo *pPrefInfo));

PUBLIC INT1 
V3UtilIsMatchedPrefHashNode PROTO ((tV3OsDbNode * pDbHashNode, 
                        tIp6Addr * pRtPrefix, UINT1 u1PrefixLen));

/*****************************o3extrt.c**********************************/

PUBLIC VOID
V3ExtrtLsaPolicyHandlerInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID
V3ExtrtParamChangeInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute * pExtRoute));

PUBLIC VOID
V3ExtrtDeleteInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                           tV3OsExtRoute * pExtRoute));

PUBLIC VOID
V3ExtrtDeleteAllInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC tV3OsExtRoute * V3ExtrtAddInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsExtRoute *pRtmInfo));

PUBLIC INT1
V3ExtRtInitRtInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 u1ApplnId,
                           UINT4 u4Type));

PUBLIC tV3OsExtRoute * 
V3ExtrtFindRouteInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr *pPrefix, UINT1 u1PrefixLength));

/*****************************o3rtm.c**********************************/
PUBLIC INT4
V3RtmGrNotifInd PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID 
V3RtmRrdConfRtInfoDeleteAllInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));

PUBLIC VOID
V3RtmUpdateNewRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));

PUBLIC VOID
V3RtmDeleteRtInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry));

PUBLIC VOID
V3RtmLeakRouteInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRtEntry * pRtEntry, tV3OsPath * pPath,
        UINT1 u1HopIndex, UINT1 u1CmdType));

PUBLIC VOID 
V3OspfProcessRtmRts PROTO ((VOID));

PUBLIC VOID
V3RtmSetRRDConfigRecordInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo));

PUBLIC VOID 
V3RtmDelRRDConfigRecordInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsRedistrConfigRouteInfo *pRrdConfRtInfo));

PUBLIC tV3OsRedistrConfigRouteInfo *
V3RrdConfRtInfoCreateInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, tIp6Addr * pRrdPrefix, UINT1 u1PrefixLen));

#ifdef RRD_WANTED 
PUBLIC INT4 
V3RtmSendToOspf PROTO ((tRtm6RespInfo * pRespInfo, tRtm6MsgHdr * pRtm6Header));

PUBLIC INT4
V3RtmTxRedistributeMsgInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT4 u4SrcProtoBitMask,
        UINT1 u1Rrd6MsgType, UINT1 *pu1RMapName));

PUBLIC INT4
V3RtmRRDSrcProtoDisableInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt, UINT4 u4SrcProtoBitMask));
#endif

PUBLIC INT4 V3RtcTrieAddRtEntry PROTO ((VOID *pInputParams,
                                         VOID *pOutputParams,
                                         tV3OsRtEntry ** ppAppSpecInfo,
                                         tV3OsRtEntry * pNewAppSpecInfo));
PUBLIC INT4
V3RtcTrieDeleteRtEntry PROTO ((VOID *pInputParams, VOID **ppAppSpecInfo,
                        VOID *pOutputParams, VOID *pNextHop, tKey Key));
PUBLIC UINT1      *V3RtcTrieDelAllRtEntry PROTO ((tInputParams * pInputParams,
                                                   VOID *dummy,
                                                   tOutputParams *
                                                   pOutputParams));
PUBLIC INT4 V3RtcTrieDelRoute
PROTO ((VOID *pInpRtInfo, VOID **ppAppPtr, tKey key));
PUBLIC INT4 V3RtcTrieSearchRtEntry
PROTO ((tInputParams * pInputParams, tOutputParams * pOutputParams,
        VOID *pAppSpecInfo));
PUBLIC INT4 V3RtcTrieLookUpEntry
PROTO ((tInputParams * pInputParams, tOutputParams * pOutputParams,
        VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key));
PUBLIC INT4
V3RtcTrieFindBestMatch PROTO ((UINT2 u2KeySize, tInputParams * pInputParams,
                   VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key));
PUBLIC VOID V3RtcTrieDelete PROTO ((VOID *pInput));

PUBLIC VOID V3RtcMarkAllPrefixAndAsbrLsasInCxt
PROTO ((tV3OspfCxt * pV3OspfCxt));


PUBLIC  VOID 
V3LsuChkAndGenIndicationLsa PROTO ((tV3OsRouterId  *pRtrId, tV3OsArea *pArea));


PUBLIC INT4 
V3GenerateAggLsa PROTO ((tV3OsArea * pArea, tV3OsAddrRange * pAddrRng));
PUBLIC VOID 
V3RtcFlushOutAggrLsa PROTO ((tV3OsArea * pArea, tV3OsAddrRange * pAddrRng));
PUBLIC VOID
V3RagFlushAggLsaInTransitArea PROTO ((tV3OsArea * pTransitArea));
PUBLIC VOID
V3RagTransAreaFlushLsa PROTO ((tV3OsArea * pLstArea));

PUBLIC UINT1 V3ChkFnEqLsaChngOrNot 
PROTO ((UINT1 *pLsa, tV3OsLsaInfo * pLsaInfo));

PUBLIC UINT1
V3FilterRouteSourceInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
                                 tNetIpv6RtInfo * pRtInfo));
#ifdef TRACE_WANTED 
/*****************************o3lsinst.c**********************************/
/* o3dbg.c  */

PUBLIC VOID
V3DbgOspfDumpPktData PROTO ((UINT4 u4Trace, UINT1 u1Direction,
                      UINT1 *pBuf, UINT2 u2BufLen));
#endif /* TRACE_WANTED */

PUBLIC VOID
V3RtcCalculateIntraPrefRoute PROTO ((tV3OsRtEntry * pOldRtEntry,
                              tV3OsPath * pNewPath,
                              tV3OsLsaInfo * pLsaInfo, tV3OsArea * pArea,
                              tV3OsPrefixInfo * pPrefInfo));

/******************************* o3gr.c **********************************/
PUBLIC VOID O3GrDeleteLsaFromDatabase PROTO ((tV3OsLsaInfo * pLsaInfo));
PUBLIC INT4 O3GrDeleteFromRxmtLst PROTO ((tV3OsNeighbor * pNbr, 
   tV3OsLsaInfo * pGrLsaInfo));
PUBLIC INT1 O3GrDisableInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC INT4 O3GrCheckRxmtLstInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC INT4 O3GrExitGracefulRestartInCxt 
PROTO ((tV3OspfCxt * pV3OspfCxt,UINT1 u1ExitReason));
PUBLIC VOID O3GrChkAndExitGracefulRestart
PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID O3GrCheckAndShutdownProcess PROTO((VOID));
PUBLIC VOID O3GrShutdownProcess PROTO ((VOID));
PUBLIC INT4 O3GrInitiateRestart PROTO ((VOID));
PUBLIC VOID O3GrSetLsIdInterAreaPrefixLsa PROTO ((tV3OspfCxt * pV3OspfCxt,
            UINT1 *pPtr, UINT2 u2Type));
PUBLIC VOID O3GrSetLsIdAsExtLsaInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, 
   UINT1 *pPtr, UINT2 u2Type));

/******************************* o3grcfg.c **********************************/
PUBLIC VOID O3GrCfgStoreRestartInfo PROTO ((VOID));
PUBLIC VOID O3GrCfgRestoreRestartInfo PROTO ((tV3OspfCxt * pV3OspfCxt));

/******************************* o3grapi.c **********************************/
PUBLIC VOID O3GrApiLsaDeleteAllInCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID O3GrApiRtDeleteAllInCxt PROTO ((tV3OspfRt * pV3OspfRt));
PUBLIC VOID O3GrApiIfDisable PROTO ((tV3OsInterface * pInterface));
PUBLIC VOID O3GrApiDeleteCxt PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID O3GrApiFlushUnwantedSelfLsa PROTO ((tV3OspfCxt * pV3OspfCxt));
PUBLIC VOID O3GrApiTaskDeInit PROTO ((VOID));
PUBLIC INT1 O3GrApiIfDisableRBTAction PROTO ((tRBElem * pNode, eRBVisit visit,
   UINT4 u4Level, VOID *arg, VOID *out));
PUBLIC VOID O3GrApiConstAndSendGraceLsa PROTO ((tV3OsInterface *pInterface));
/******************************* o3grlsu.c **********************************/
PUBLIC INT4 O3GrLsuSearchRtrLsa PROTO ((tV3OsLsaInfo * pLsaInfo,
          tV3OsRtrLsaRtInfo * pRtrLsaRtInfo));
PUBLIC INT4 O3GrLsuCheckSelfOrigLsa PROTO ((tV3OsArea * pArea,
          tV3OsLsHeader * pLsHeader, UINT1 *pLsa));
PUBLIC INT4 O3GrLsuIsNbrOrigLsaInCxt PROTO ((tV3OspfCxt * pV3OspfCxt,
          tV3OsLsHeader * pLsHeader));
PUBLIC VOID O3GrLsuDelLinkInfoFrmRtrTree PROTO ((tV3OspfCxt * pV3OspfCxt,
          tV3OsRtrLsaRtInfo nbrRtrLsaRtInfo, tV3OsLsHeader * pLsHeader));
PUBLIC INT4 O3GrLsuSearchRtrLsaRBTree PROTO ((tV3OsLsHeader * pLsHeader));
PUBLIC INT4 O3GrLsuCheckNbrLsa PROTO ((tV3OsArea * pArea, 
   tV3OsLsHeader * pLsHeader, UINT1 *pLsa));
PUBLIC INT1 O3GrLsuProcessRcvdSelfOrgLsa PROTO ((tV3OsLsHeader * pLsHeader,
   tV3OsLsaInfo * pLsaInfo, UINT1 *pLsa, tV3OsNeighbor * pNbr));
PUBLIC INT4 O3GrLsuCheckLsaConsistency PROTO ((tV3OsArea * pArea,
   tV3OsLsHeader * pLsHeader, UINT1 *pLsa));
PUBLIC VOID O3GrLsuGetLinksFromLsa PROTO ((UINT1 *pu1CurrPtr, 
   tV3OsRtrLsaRtInfo * pRtrLsaRtInfo));

/******************************* o3helper.c **********************************/
PUBLIC INT4 O3GrExitHelper PROTO ((UINT1 u1ExitReason, tV3OsNeighbor * pNbr));
PUBLIC INT4 O3GrFindTopologyChangeLsa PROTO ((tV3OsLsaInfo * pLsa1, UINT1 *pLsa2));
PUBLIC INT4 O3GrHelperProcessGraceLsa PROTO ((tV3OsNeighbor * pNbr, 
   tV3OsLsHeader * pHeader, UINT1 *pLsa));

/******************************* o3snmp.c **********************************/
PUBLIC VOID
O3SnmpIfSendTrapInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, UINT1 u1TrapId, VOID *pTrapInfo));
PUBLIC VOID
O3SnmpSendStatusChgTrapInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, tV3OsNeighbor * pNbr, UINT4 u4Trap));
/******************************* o3auth.c **********************************/
PUBLIC tAuthkeyInfo* V3GetFindVirtIfAuthkeyInfoInCxt PROTO ((tV3OspfCxt * pOspfCxt, tV3OsAreaId * pTransitAreaId, tV3OsRouterId * pNbrId, UINT2 u2AuthkeyId));
PUBLIC tAuthkeyInfo* V3GetFindIfAuthkeyInfoInCxt PROTO (( UINT4 u4IfAuthIfIndex, UINT2 u2AuthKeyId));
PUBLIC VOID V3AuthKeyCopy PROTO ((UINT1 *pu1Dst, UINT1 *pu1Src, INT4 i4Len));
PUBLIC tAuthkeyInfo * V3GetAuthkeyTouse PROTO ((tV3OsInterface * pInterface));
PUBLIC INT4 V3OspfConvertTimeForSnmp PROTO ((UINT1 *pu1TimeStr, tUtlTm * tm));
PUBLIC INT4 V3OspfConvertTime PROTO ((UINT1 *pu1TimeStr, tUtlTm * tm));
PUBLIC VOID
O3SnmpAuthSeqNumWrapTrapInCxt PROTO ((tV3OspfCxt * pV3OspfCxt, UINT4 u4Trap));
PUBLIC VOID V3OspfPrintKeyTime PROTO ((UINT4 u4Secs, UINT1 *pOspfv3KeyTime));
PUBLIC tAuthkeyInfo * V3AuthKeyInfoCreate PROTO((VOID));
PUBLIC VOID V3SortInsertAuthKeyInfo PROTO
((tTMO_SLL * pauthkeyLst, tAuthkeyInfo * pAuthkeyInfo));
PUBLIC INT1 V3ConvertKeyTime PROTO (( UINT4 u4Secs , tUtlTm *tm));
PUBLIC UINT1
V3AuthSendPkt PROTO ((UINT1 *pPkt, UINT2 u2PktLen, tV3OsInterface * pInterface, tIp6Addr dstIp6Addr, UINT2 *pu2ATLen));
PUBLIC INT4 V3AuthRcvPkt  PROTO ((UINT1 *pPkt, UINT2 u2Len, tV3OsInterface * pInterface, tV3OsNeighbor *pNbr, tIp6Addr * pSrcIp6Addr,UINT1 u1Typ));
PUBLIC UINT1 V3WriteCryptoHighSeq  PROTO ((tV3OsInterface * pInterface));
PUBLIC INT4
V3AuthRmCryptSeqMsgToRm PROTO ((VOID));
PUBLIC INT4 V3AuthCryptoHighSeq PROTO ((VOID));
PUBLIC VOID V3ApplyDistance(tV3OspfCxt * pV3OspfCxt);
#endif /* __O3PORT_H */
