/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: o3inc.h,v 1.18 2015/03/11 10:55:47 siva Exp $
 *
 * Description:This file contains the list of include files
 *
 *******************************************************************/
#ifndef _O3INC_H
#define _O3INC_H

#include "lr.h"
#include "cfa.h"
#include "ipv6.h"
#include "fssnmp.h"
#include "ospf3.h"
#include "o3port.h"
#include "ip6util.h"
#include "redblack.h"
#include "trie.h"
#include "cli.h"
#include "iss.h"
#include "csr.h"
#include "fssyslog.h"
#include "bfd.h"

#include "rmap.h"
#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ip6np.h"
#include "ospf3npwr.h"
#endif

#include "rtm6.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "vcm.h"
#include "rmgr.h"

/* OSPFv3 include files */
#include "o3cfg.h"
#include "o3defn.h"
#include "o3cfgvar.h"
#include "o3tmr.h"
#include "o3bufif.h"
#include "o3macs.h"
#include "o3tdfs.h"
#include "o3extn.h"
#include "ospf3cli.h"
#include "o3prot.h"
#include "o3trace.h"

#include "o3rtstg.h"
#include "o3snmp.h"
#include "o3sock.h"
#include "o3red.h"
#include "o3sz.h"
#if defined (VRF_WANTED) && defined (LINUX_310_WANTED)
#include "lnxip.h"
#endif
#include "arHmac_api.h"
#include "msr.h"
#include "snmputil.h"

#endif /* _O3INC_H */
