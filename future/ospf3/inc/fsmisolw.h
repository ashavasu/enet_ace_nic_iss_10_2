/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* $Id: fsmisolw.h,v 1.2 2017/12/26 13:34:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMIStdOspfv3Table. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3Table ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3Table  */

INT1
nmhGetFirstIndexFsMIStdOspfv3Table ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3Table ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3RouterId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AdminStat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VersionNumber ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaBdrRtrStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3ASBdrRtrStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AsScopeLsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AsScopeLsaCksumSum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3OriginateNewLsas ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3RxNewLsas ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3ExtLsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3MulticastExtensions ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3ExitOverflowInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3DemandExtensions ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3TrafficEngineeringSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3ReferenceBandwidth ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3RestartSupport ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3RestartInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3RestartStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3RestartAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3RestartExitReason ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3Status ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3RouterId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3AdminStat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3ASBdrRtrStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3MulticastExtensions ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMIStdOspfv3ExitOverflowInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3DemandExtensions ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3TrafficEngineeringSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3ReferenceBandwidth ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3RestartSupport ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3RestartInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3Status ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3RouterId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3AdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3ASBdrRtrStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3ExtAreaLsdbLimit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3MulticastExtensions ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMIStdOspfv3ExitOverflowInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3DemandExtensions ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3TrafficEngineeringSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3ReferenceBandwidth ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3RestartSupport ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3RestartInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3Status ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3Table ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3AreaTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3AreaTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3AreaTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3ImportAsExtern ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaSpfRuns ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaBdrRtrCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaAsBdrRtrCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaScopeLsaCount ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaScopeLsaCksumSum ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaSummary ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3StubMetric ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorRole ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaNssaTranslatorEvents ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3AreaStubMetricType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhGetFsMIStdOspfv3AreaDfInfOriginate ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3ImportAsExtern ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaSummary ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3StubMetric ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaNssaTranslatorRole ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaNssaTranslatorStabilityInterval ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3AreaStubMetricType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaDfInfOriginate ARG_LIST((INT4  , UINT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3ImportAsExtern ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaSummary ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3StubMetric ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaNssaTranslatorRole ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaNssaTranslatorStabilityInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaStubMetricType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaDfInfOriginate ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3AreaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3AsLsdbTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3AsLsdbTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3AsLsdbTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3AsLsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3AsLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3AsLsdbSequence ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AsLsdbAge ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AsLsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AsLsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3AsLsdbTypeKnown ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfv3AreaLsdbTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaLsdbTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3AreaLsdbTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaLsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3AreaLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3AreaLsdbSequence ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaLsdbAge ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaLsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaLsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3AreaLsdbTypeKnown ARG_LIST((INT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfv3LinkLsdbTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3LinkLsdbTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3LinkLsdbTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3LinkLsdbTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3LinkLsdbTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3LinkLsdbSequence ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3LinkLsdbAge ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3LinkLsdbChecksum ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3LinkLsdbAdvertisement ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3LinkLsdbTypeKnown ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3LinkLsdbContextId ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfv3HostTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3HostTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3HostTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3HostTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3HostTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3HostMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdOspfv3HostAreaID ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdOspfv3HostStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3HostMetric ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdOspfv3HostAreaID ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3HostStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3HostMetric ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3HostAreaID ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3HostStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3HostTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3IfTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3IfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3IfTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3IfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3IfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3IfAreaId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfAdminStat ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfRtrPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfTransitDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfRetransInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfHelloInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfRtrDeadInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfPollInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfState ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfDesignatedRouter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfBackupDesignatedRouter ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfEvents ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfMulticastForwarding ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfDemand ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfMetricValue ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfLinkScopeLsaCount ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfLinkLsaCksumSum ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfInstId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbe ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3IfContextId ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3IfStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3IfAreaId ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3IfType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfAdminStat ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfRtrPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfTransitDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfRetransInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfHelloInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfRtrDeadInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfPollInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3IfMulticastForwarding ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfDemand ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfMetricValue ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfInstId ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbe ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3IfDemandNbrProbeInterval ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMIStdOspfv3IfStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3IfAreaId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfAdminStat ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfRtrPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfTransitDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfRetransInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfHelloInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfPollInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfMulticastForwarding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfDemand ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfMetricValue ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfInstId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbe ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbeRetxLimit ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfDemandNbrProbeInterval ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMIStdOspfv3IfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3IfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3VirtIfTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3VirtIfTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3VirtIfTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3VirtIfTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3VirtIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3VirtIfIndex ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfState ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfEvents ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfLinkScopeLsaCount ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfLinkLsaCksumSum ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3VirtIfIndex ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3VirtIfTransitDelay ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3VirtIfRetransInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3VirtIfHelloInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3VirtIfRtrDeadInterval ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3VirtIfStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3VirtIfIndex ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3VirtIfTransitDelay ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3VirtIfRetransInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3VirtIfHelloInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3VirtIfRtrDeadInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3VirtIfStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3VirtIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3NbrTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3NbrTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3NbrTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3NbrTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3NbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3NbrAddressType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrAddress ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3NbrOptions ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrPriority ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrEvents ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3NbrLsRetransQLen ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3NbrHelloSuppressed ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrIfId ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrRestartHelperStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrRestartHelperAge ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrRestartHelperExitReason ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbrContextId ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfv3NbmaNbrTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3NbmaNbrTable ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3NbmaNbrTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3NbmaNbrTable ARG_LIST((INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3NbmaNbrTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3NbmaNbrPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbmaNbrRtrId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetFsMIStdOspfv3NbmaNbrState ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbmaNbrStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbmaNbrContextId ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsMIStdOspfv3NbmaNbrStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3NbmaNbrPriority ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdOspfv3NbmaNbrStorageType ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMIStdOspfv3NbmaNbrStatus ARG_LIST((INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3NbmaNbrPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3NbmaNbrStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3NbmaNbrStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3NbmaNbrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMIStdOspfv3VirtNbrTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3VirtNbrTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3VirtNbrTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3VirtNbrTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3VirtNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3VirtNbrIfIndex ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrAddressType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrAddress ARG_LIST((INT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIStdOspfv3VirtNbrOptions ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrState ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrEvents ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrLsRetransQLen ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrHelloSuppressed ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrIfId ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperAge ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3VirtNbrRestartHelperExitReason ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsMIStdOspfv3AreaAggregateTable. */
INT1
nmhValidateIndexInstanceFsMIStdOspfv3AreaAggregateTable ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMIStdOspfv3AreaAggregateTable  */

INT1
nmhGetFirstIndexFsMIStdOspfv3AreaAggregateTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMIStdOspfv3AreaAggregateTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMIStdOspfv3AreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaAggregateRouteTag ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetFsMIStdOspfv3AreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMIStdOspfv3AreaAggregateEffect ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaAggregateRouteTag ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetFsMIStdOspfv3AreaAggregateStatus ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMIStdOspfv3AreaAggregateEffect ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaAggregateRouteTag ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMIStdOspfv3AreaAggregateStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMIStdOspfv3AreaAggregateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
