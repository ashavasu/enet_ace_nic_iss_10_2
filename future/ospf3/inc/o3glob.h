/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: o3glob.h,v 1.10 2017/12/26 13:34:25 siva Exp $
*
* Description: This file contains all global variable declarations
*********************************************************************/
#ifndef _O3GLOB_H
#define _O3GLOB_H

/* AllSPFRouters -- FF02::5 */
tIp6Addr gV3OsAllSpfRtrs   = {{{0xff,0x02,0x00,0x00,0x00,0x00,0x00,0x00,
                       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x05}}};
/* AllDRRouters  -- FF02::6 */
tIp6Addr gV3OsAllDRtrs     = {{{0xff,0x02,0x00,0x00,0x00,0x00,0x00,0x00,
                       0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x06}}};
tIp6Addr gV3OsNullIp6Addr  = {{{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}}};
tV3OsRouterId gV3OsNullIpAddr  = {0,0,0,0};
tIp6Addr gV3OsDefaultDest  = {{{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}}};
                    
tIp6Addr gV3OsHostRouteMask = {{{0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,
                       0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff}}};
tV3OsRtr gV3OsRtr;
UINT4 gu4ClearFlag;
UINT4 gu1O3HelloSwitchOverFlag;
UINT4 gu4Os3CsrFlag;
UINT4 gu4Os3RestoreFlag;
UINT4 gu4Os3ATHigSeqNum;
/* RMap global variable */
tOsixMsg *gV3OsRmapMsg[MAX_OSPFV3_RMAP_MSGS];

struct TrieAppFns   V3RtcTrieLibFuncs = {
    (INT4 (*)(tInputParams * , VOID *,
              VOID **, VOID *)) V3RtcTrieAddRtEntry,

    (INT4 (*)(tInputParams * , VOID **,
              VOID *, VOID *pNxtHop,
              tKey Key)) V3RtcTrieDeleteRtEntry,

    (INT4 (*)(tInputParams * , VOID *,
              VOID *)) V3RtcTrieSearchRtEntry,

    (INT4 (*)(tInputParams * , VOID *,
              VOID *, UINT2 ,
              tKey key)) V3RtcTrieLookUpEntry,

    (VOID *(*)(tInputParams *, VOID (*)(VOID *), VOID *)) NULL,

    (VOID (*)(VOID *)) NULL,

    (INT4 (*)(VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*)(VOID *, VOID *, tKey Key)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID **, VOID *, UINT4)) NULL,

    (VOID *(*)(tInputParams * pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) V3RtcTrieDelAllRtEntry,

    (VOID (*)(VOID *)) V3RtcTrieDelete,

    (INT4 (*)(VOID *, VOID **, tKey key)) V3RtcTrieDelRoute,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*) (UINT2 , tInputParams * , VOID *, VOID *, tKey ))
         V3RtcTrieFindBestMatch,

    (INT4 (*)(tInputParams *, VOID *, VOID *, tKey)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID *, UINT2, tKey)) NULL
};

/* Global variables which are used for debugging & Trace log */
/* The Various Traps that are Generated */
UINT1  gau1Os3DbgIfType[7][20] = 
                          { "",
                            "BROADCAST",
                            "NBMA",
                            "PTOP",
                            "",
                   "PTOM",
                            ""};

/* Interface states */
UINT1  gau1Os3DbgIfState[OSPFV3_MAX_IF_STATE][20] =
         { "DOWN",
           "LOOP_BACK",
           "WAITING",
           "PTOP",
           "DR",
           "BACKUP",
           "DR_OTHER", 
           "SBY_LINK"};

/* Interface events */
UINT1 gau1Os3DbgIfEvent[OSPFV3_MAX_IF_EVENT][30] =
       { "IFE_UP",
         "IFE_WAIT_TIMER",
         "IFE_BACKUP_SEEN",
         "IFE_NBR_CHANGE",
         "IFE_LOOP_IND",
         "IFE_UNLOOP_IND",
         "IFE_DOWN" };

/*  Neighbor states */
UINT1 gau1Os3DbgNbrState[OSPFV3_MAX_NBR_STATE][20] =
                { "DOWN",
                  "ATTEMPT",
                  "INIT",
                  "2WAY",
                  "EXSTART",
                  "EXCHANGE",
                  "LOADING",
                  "FULL" };
/* Neighbor type */
UINT1 gau1Os3DbgNbrType[OSPFV3_MAX_NBR_TYPE][20] =
                { "DOWN",
                  "ACTIVE_LINK",
                  "STANDBY_LINK",
                  "NORMAL" };


/* Events causing neighbor state transitions */
UINT1 gau1Os3DbgNbrEvent[OSPFV3_MAX_NBR_EVENT][30] =
       { "NBRE_HELLO_RCVD",
         "NBRE_START",
         "NBRE_2WAY_RCVD",
         "NBRE_NEG_DONE",
         "NBRE_EXCHANGE_DONE",
         "NBRE_BAD_LS_REQ",
         "NBRE_LOADING_DONE",
         "NBRE_ADJ_OK",
         "NBRE_SEQ_NUM_MISMATCH",
         "NBRE_1WAY_RCVD",
         "NBRE_KILL_NBR",
         "NBRE_INACTIVITY_TIMER",
         "NBRE_LL_DOWN" };

/* Types of timers used */
UINT1 gau1Os3DbgTimerId[OSPFV3_MAX_TIMERS][30] = 
    { "HELLO_TIMER",
      "POLL_TIMER",
      "WAIT_TIMER",
      "INACTIVITY_TIMER",
      "DD_INIT_RXMT_TIMER",
      "DD_RXMT_TIMER",
      "DD_LAST_PKT_LIFE_TIME_TIMER",
      "LSA_REQ_RXMT_TIMER",
      "LSA_NORMAL_AGING_TIMER",
      "MIN_LSA_INTERVAL_TIMER",
      "LSA_RXMT_TIMER",
      "DEL_ACK_TIMER",
      "SPF_TIMER",
      "EXIT_OVERFLOW_TIMER",
      "NSSA_STBLTY_INTERVAL_TIMER",
      "NBR_PROBE_INT_TIMER" };

UINT1 gau1Os3DbgLsaType[10][30] = 
        { "",
      "ROUTER_LSA",
      "NETWORK_LSA",
      "INTER_AREA_PREFIX_LSA",
      "INTER_AREA_ROUTER_LSA",
      "AS_EXT_LSA",
      "",
      "NSSA_LSA",
      "LINK_LSA",
      "INTRA_AREA_PREFIX_LSA"
        };

UINT1 gau1Os3DbgPktType[6][30] = 
       { "",
         "HELLO_PKT",
         "DD_PKT",
         "LSA_REQ_PKT",
         "LS_UPDATE_PKT",
         "LSA_ACK_PKT"  };

UINT1 gau1Os3DbgAbrType[OSPFV3_MAX_AREA_TYPE][20] = 
       { "",
         "Standard ABR",
         "Cisco ABR",
         "IBM ABR"  };

#endif  /* _O3GLOB_H */
