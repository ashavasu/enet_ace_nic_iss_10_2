/********************************************************************
 *       Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *       $Id: fsmitowr.h,v 1.2 2017/12/26 13:34:24 siva Exp $
 *   
 *       Description:
 *     
 *************************************************************************/



#ifndef _FSMITOWR_H
#define _FSMITOWR_H
INT4 GetNextIndexFsMIOspfv3TestIfTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSMITO(VOID);

VOID UnRegisterFSMITO(VOID);
INT4 FsMIOspfv3TestDemandTrafficGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3TestIfContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3TestDemandTrafficSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3TestDemandTrafficTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3TestIfTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMIOspfv3ExtRouteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMIOspfv3ExtRouteMetricGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteMetricTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteMetricSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteMetricTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteMetricTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteMetricTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMIOspfv3ExtRouteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _FSMITOWR_H */
