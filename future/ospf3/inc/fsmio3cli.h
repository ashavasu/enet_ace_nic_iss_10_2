/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: fsmio3cli.h,v 1.5 2017/12/26 13:34:24 siva Exp $
 *
 * Description:extern declaration corresponding to 
 *             OID variable present in protocol db.h
 *
 *******************************************************************/
/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMIOspfv3GlobalTraceLevel[11];
extern UINT4 FsMIOspfv3VrfSpfInterval[11];
extern UINT4 FsMIOspfv3OverFlowState[12]; 
extern UINT4 FsMIOspfv3TraceLevel[12];
extern UINT4 FsMIOspfv3ABRType[12];
extern UINT4 FsMIOspfv3NssaAsbrDefRtTrans[12]; 
extern UINT4 FsMIOspfv3DefaultPassiveInterface[12];
extern UINT4 FsMIOspfv3SpfDelay[12];
extern UINT4 FsMIOspfv3SpfHoldTime[12];
extern UINT4 FsMIOspfv3RTStaggeringInterval[12]; 
extern UINT4 FsMIOspfv3RTStaggeringStatus[11];
extern UINT4 FsMIOspfv3ClearProcess[11];
extern UINT4 FsMIOspfv3IfBfdState[12];
extern UINT4 FsMIOspfv3BfdStatus[12];
extern UINT4 FsMIOspfv3BfdAllIfState[12];
extern UINT4 FsMIOspfv3RouterIdPermanence[12];
extern UINT4 FsMIOspfv3RestartStrictLsaChecking[12];
extern UINT4 FsMIOspfv3HelperSupport[12];
extern UINT4 FsMIOspfv3HelperGraceTimeLimit[12]; 
extern UINT4 FsMIOspfv3RestartAckState[12];
extern UINT4 FsMIOspfv3GraceLsaRetransmitCount[12];
extern UINT4 FsMIOspfv3RestartReason[12];
extern UINT4 FsMIOspfv3ExtTraceLevel[12];
extern UINT4 FsMIOspfv3SetTraps[12];
extern UINT4 FsMIOspfv3IfIndex[12];
extern UINT4 FsMIOspfv3IfOperState[12];
extern UINT4 FsMIOspfv3IfPassive[12];
extern UINT4 FsMIOspfv3IfNbrCount[12];
extern UINT4 FsMIOspfv3IfAdjCount[12];
extern UINT4 FsMIOspfv3IfHelloRcvd[12];
extern UINT4 FsMIOspfv3IfHelloTxed[12];
extern UINT4 FsMIOspfv3IfHelloDisd[12];
extern UINT4 FsMIOspfv3IfDdpRcvd[12];
extern UINT4 FsMIOspfv3IfDdpTxed[12];
extern UINT4 FsMIOspfv3IfDdpDisd[12];
extern UINT4 FsMIOspfv3IfLrqRcvd[12];
extern UINT4 FsMIOspfv3IfLrqTxed[12];
extern UINT4 FsMIOspfv3IfLrqDisd[12];
extern UINT4 FsMIOspfv3IfLsuRcvd[12];
extern UINT4 FsMIOspfv3IfLsuTxed[12];
extern UINT4 FsMIOspfv3IfLsuDisd[12];
extern UINT4 FsMIOspfv3IfLakRcvd[12];
extern UINT4 FsMIOspfv3IfLakTxed[12];
extern UINT4 FsMIOspfv3IfLakDisd[12];
extern UINT4 FsMIOspfv3IfContextId[12];
extern UINT4 FsMIOspfv3IfLinkLSASuppression[12]; 
extern UINT4 FsMIOspfv3RouteDestType[12];
extern UINT4 FsMIOspfv3RouteDest[12];
extern UINT4 FsMIOspfv3RoutePfxLength[12];
extern UINT4 FsMIOspfv3RouteNextHopType[12];
extern UINT4 FsMIOspfv3RouteNextHop[12];
extern UINT4 FsMIOspfv3RouteType[12];
extern UINT4 FsMIOspfv3RouteAreaId[12];
extern UINT4 FsMIOspfv3RouteCost[12];
extern UINT4 FsMIOspfv3RouteType2Cost[12];
extern UINT4 FsMIOspfv3RouteInterfaceIndex[12];
extern UINT4 FsMIOspfv3AsExternalAggregationNetType[12];
extern UINT4 FsMIOspfv3AsExternalAggregationNet[12];
extern UINT4 FsMIOspfv3AsExternalAggregationPfxLength[12];
extern UINT4 FsMIOspfv3AsExternalAggregationAreaId[12];
extern UINT4 FsMIOspfv3AsExternalAggregationEffect[12];
extern UINT4 FsMIOspfv3AsExternalAggregationTranslation[12];
extern UINT4 FsMIOspfv3AsExternalAggregationStatus [12];
extern UINT4 FsMIOspfv3BRRouteDest[12];
extern UINT4 FsMIOspfv3BRRouteNextHopType[12];
extern UINT4 FsMIOspfv3BRRouteNextHop[12];
extern UINT4 FsMIOspfv3BRRouteDestType[12];
extern UINT4 FsMIOspfv3BRRouteType[12];
extern UINT4 FsMIOspfv3BRRouteAreaId[12];
extern UINT4 FsMIOspfv3BRRouteCost[12];
extern UINT4 FsMIOspfv3BRRouteInterfaceIndex[12];
extern UINT4 FsMIOspfv3RedistRouteDestType[12];
extern UINT4 FsMIOspfv3RedistRouteDest[12];
extern UINT4 FsMIOspfv3RedistRoutePfxLength[12];
extern UINT4 FsMIOspfv3RedistRouteMetric[12];
extern UINT4 FsMIOspfv3RedistRouteMetricType[12];
extern UINT4 FsMIOspfv3RedistRouteTagType[12];
extern UINT4 FsMIOspfv3RedistRouteTag[12];
extern UINT4 FsMIOspfv3RedistRouteStatus[12];
extern UINT4 FsMIOspfv3RRDStatus[14];
extern UINT4 FsMIOspfv3RRDSrcProtoMask[14];
extern UINT4 FsMIOspfv3RRDRouteMapName[14];
extern UINT4 FsMIOspfv3DistInOutRouteMapName[13];
extern UINT4 FsMIOspfv3DistInOutRouteMapType[13];
extern UINT4 FsMIOspfv3DistInOutRouteMapValue[13];
extern UINT4 FsMIOspfv3DistInOutRouteMapRowStatus[13];
extern UINT4 FsMIOspfv3PreferenceValue[13 ];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIOspfv3IfAuthIfIndex[12];
extern UINT4 FsMIOspfv3IfAuthKeyId[12];
extern UINT4 FsMIOspfv3IfAuthKey[12];
extern UINT4 FsMIOspfv3IfAuthKeyStartAccept[12];
extern UINT4 FsMIOspfv3IfAuthKeyStartGenerate[12];
extern UINT4 FsMIOspfv3IfAuthKeyStopGenerate[12];
extern UINT4 FsMIOspfv3IfAuthKeyStopAccept[12];
extern UINT4 FsMIOspfv3IfAuthKeyStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIOspfv3VirtIfAuthAreaId[12];
extern UINT4 FsMIOspfv3VirtIfAuthNeighbor[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyId[12];
extern UINT4 FsMIOspfv3VirtIfAuthKey[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyStartAccept[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyStartGenerate[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyStopGenerate[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyStopAccept[12];
extern UINT4 FsMIOspfv3VirtIfAuthKeyStatus[12];

/* extern declaration corresponding to OID variable present in protocol db.h */
extern UINT4 FsMIOspfv3VirtIfCryptoAuthType[12];
extern UINT4 FsMIOspfv3VirtIfCryptoAuthMode[12];
extern UINT4 FsMIOspfv3IfCryptoAuthType[12];
extern UINT4 FsMIOspfv3IfCryptoAuthMode[12];




extern UINT4 FsMIOspfv3ExtRouteDestType[13];
extern UINT4 FsMIOspfv3ExtRouteDest[13];
extern UINT4 FsMIOspfv3ExtRoutePfxLength[13];
extern UINT4 FsMIOspfv3ExtRouteNextHopType[13];
extern UINT4 FsMIOspfv3ExtRouteNextHop[13];
extern UINT4 FsMIOspfv3ExtRouteMetric[13];
extern UINT4 FsMIOspfv3ExtRouteMetricType[13];
extern UINT4 FsMIOspfv3ExtRouteStatus[13];  


extern UINT4 FsMIOspfv3RRDProtocolId[13];
extern UINT4 FsMIOspfv3RRDMetricValue[13];
extern UINT4 FsMIOspfv3RRDMetricType[13];

