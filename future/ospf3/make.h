##############################################################
# Copyright (C) 2009 Aricent Inc . All Rights Reserved
#
# $$
#
# Description: Specifies the options and modules to be
#              including for building the FutureOSPFv3 product.
#
##############################################################

GLOBAL_OPNS = ${GENERAL_COMPILATION_SWITCHES}\
              ${SYSTEM_COMPILATION_SWITCHES}

############################################################################
#                         Directories                                      #
############################################################################

#Fill the base directory
OSPF3_BASE_DIR = ${BASE_DIR}/ospf3
OSPF3_SRCD = ${OSPF3_BASE_DIR}/src
OSPF3_INCD = ${OSPF3_BASE_DIR}/inc
OSPF3_OBJD = ${OSPF3_BASE_DIR}/obj

OSPF3_LIB_INCD  = ${BASE_DIR}/util/ip6

DEPENDENCIES_FOR_OSPF3 = ${BASE_DIR}/fsap2/cmn/redblack.h \
                         ${BASE_DIR}/inc/trie.h \
                         ${OSPF3_LIB_INCD}/ip6util.h \
                         ${OSPF3_BASE_DIR}/make.h \
                         ${OSPF3_BASE_DIR}/Makefile \
                         ${COMMON_DEPENDENCIES}

ifeq (${CLI}, YES)
OSPF3_CLI_INCLUDE_FILES =    \
    $(CLI_INCL_DIR)/ospf3cli.h

endif

IP6_BASE_DIR = ${BASE_DIR}/ip6
RTM6_BASE_DIR = ${IP6_BASE_DIR}/rtm6
IP6_RTMINCD = ${RTM6_BASE_DIR}/inc

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

OSPF3_GLOBAL_INCLUDES  = -I${OSPF3_INCD} -I${IP6_RTMINCD} -I$(OSPF3_LIB_INCD)

GLOBAL_INCLUDES  = ${OSPF3_GLOBAL_INCLUDES} \
                   ${COMMON_INCLUDE_DIRS}

#############################################################################


##########################################################################
# Each line below indicates whether that module is to be included in the #
# OSPF3 build. A YES indicates inclusion.                                #
##########################################################################
TESTMIB        = NO
