/* $Id: snpport.c,v 1.54 2017/12/11 10:02:25 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpport.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : snooping porting module                        */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains porting routines            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006            Initial Creation                       */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

#ifdef LLDP_WANTED
#include "lldp.h"
#endif

/*****************************************************************************/
/* Function Name      : SnoopMiVlanDelDynamicMcastInfoForVlan                */
/*                                                                           */
/* Description        : This function calls the VLAN Module to delete        */
/*                      dynamically learnt multicast information for the     */
/*                      given VLAN and insatnce.                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId  - Vlan Identifier                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopMiVlanDelDynamicMcastInfoForVlan (UINT4 u4Instance, tSnoopTag VlanId)
{
    VlanMiDelDynamicMcastInfoForVlan (u4Instance, SNOOP_OUTER_VLAN (VlanId));
}

/*****************************************************************************/
/* Function Name      : SnoopMiVlanSnoopGetTxPortList                          */
/*                                                                           */
/* Description        : This function calls the VLAN Module to get           */
/*                      tagged and untagged portlist for the given VLAN ID   */
/*                                                                           */
/* Input(s)           : DestMacAddr  - Destination MAC Address               */
/*                      u2Port       - Port Number                           */
/*                      VlanId       - VLAN Identifier                       */
/*                      u1Forward       - VLAN_FORWARD_SPECIFIC/             */
/*                                        VLAN_FORWARD_ALL                   */
/*                      PortBitmap   - Port list                             */
/*                                                                           */
/* Output(s)          : TaggedPortBitmap - Tagged port list                  */
/*                      UntaggedPortBitmap - Untagged port list              */
/*                                                                           */
/* Return Value(s)    : VLAN_FORWARD/ VLAN_NO_FORWARD                        */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopMiVlanSnoopGetTxPortList (UINT4 u4ContextId, tMacAddr DestMacAddr,
                               UINT4 u4Port, tSnoopTag VlanId,
                               UINT1 u1Forward, tSnoopIfPortBmp PortBitmap,
                               tSnoopIfPortBmp TaggedPortBitmap,
                               tSnoopIfPortBmp UntaggedPortBitmap)
{
    tPortList          *pPortList = NULL;
    tPortList          *pTaggedPortList = NULL;
    tPortList          *pUntaggedPortList = NULL;
    INT4                i4RetVal = 0;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ALL_FAILURE_TRC, SNOOP_OS_RES_DBG,
                   "Error in Allocating memory for bitlist\n");
        return VLAN_NO_FORWARD;
    }

    pTaggedPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTaggedPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ALL_FAILURE_TRC, SNOOP_OS_RES_DBG,
                   "Error in Allocating memory for bitlist\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        return VLAN_NO_FORWARD;
    }

    pUntaggedPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pUntaggedPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ALL_FAILURE_TRC, SNOOP_OS_RES_DBG,
                   "Error in Allocating memory for bitlist\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        FsUtilReleaseBitList ((UINT1 *) pTaggedPortList);
        return VLAN_NO_FORWARD;
    }

    SNOOP_MEM_SET (pPortList, 0, sizeof (tPortList));
    SNOOP_MEM_SET (pTaggedPortList, 0, sizeof (tPortList));
    SNOOP_MEM_SET (pUntaggedPortList, 0, sizeof (tPortList));

    /* In metro package, the size of tPortList and tSnoopIfPortList
     * varies. tPortList (1308 bytes) and tSnoopIfPortList (128 bytes). 
     * Hence, for interfacing with the vlan module a local variable
     * is used to obtain the entire portlist and only the 1st 128 bytes
     * is used by snooping module as it is unaware of the
     * VIP configurations */

    SNOOP_MEM_CPY (pPortList, PortBitmap, sizeof (tSnoopIfPortBmp));

    i4RetVal = VlanMiSnoopGetTxPortList (u4ContextId, DestMacAddr, u4Port,
                                         SNOOP_OUTER_VLAN (VlanId),
                                         u1Forward, *pPortList,
                                         *pTaggedPortList, *pUntaggedPortList);

    SNOOP_MEM_CPY (PortBitmap, pPortList, sizeof (tSnoopIfPortBmp));
    SNOOP_MEM_CPY (TaggedPortBitmap, pTaggedPortList, sizeof (tSnoopIfPortBmp));
    SNOOP_MEM_CPY (UntaggedPortBitmap, pUntaggedPortList,
                   sizeof (tSnoopIfPortBmp));

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    FsUtilReleaseBitList ((UINT1 *) pTaggedPortList);
    FsUtilReleaseBitList ((UINT1 *) pUntaggedPortList);
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : SnoopMiVlanUpdateDynamicMcastInfo                    */
/*                                                                           */
/* Description        : This function calls the VLAN Module to update        */
/*                      VLAN multicast table for agiven instance             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance ID for which the updation is   */
/*                                   done                                    */
/*                      MacGroupAddr - Mcast MAC address                     */
/*                      VlanId       - Vlan Identifier                       */
/*                      u4Port       - Port Number                           */
/*                      u1Action     - VLAN_ADD / VLAN_DELETE                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopMiVlanUpdateDynamicMcastInfo (UINT4 u4Instance, tMacAddr MacGroupAddr,
                                   tSnoopTag VlanId, UINT4 u4Port,
                                   UINT1 u1Action)
{
    UINT4               u4PhyPort = 0;

    if (u4Port != SNOOP_INVALID_PORT)
    {
        u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u4Port);
    }

    if (VlanMiUpdateDynamicMcastInfo (u4Instance, MacGroupAddr,
                                      SNOOP_OUTER_VLAN (VlanId), u4PhyPort,
                                      u1Action) != VLAN_SUCCESS)
    {
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMiVlanDeleteAllDynamicMcastInfo                 */
/*                                                                           */
/* Description        : This function calls the VLAN Module to delete        */
/*                      all dynamic multicast entries.                       */
/*                                                                           */
/* Input(s)           : u4Port   - SNOOP_INVALID_PORT                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopMiVlanDeleteAllDynamicMcastInfo (UINT4 u4Instance, UINT4 u4Port,
                                      UINT1 u1AddressType)
{
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        VlanMiDeleteAllDynamicMcastInfo (u4Instance, u4Port,
                                         IPVX_ADDR_FMLY_IPV6);
    }
    else if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        VlanMiDeleteAllDynamicMcastInfo (u4Instance, u4Port,
                                         IPVX_ADDR_FMLY_IPV4);
    }
}

/*****************************************************************************/
/* Function Name      : SnoopGetSysMacAddress                                */
/*                                                                           */
/* Description        : This function calls the CFA module to get the        */
/*                      system MAC address.                                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                                                                           */
/* Output(s)          : pSwitchMac - pointer to the switch MAC address       */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopGetSysMacAddress (UINT4 u4Instance, tMacAddr pSwitchMac)
{
    IssGetContextMacAddress (u4Instance, pSwitchMac);
}

/*****************************************************************************/
/* Function Name      : SnoopGetFwdModeFromNvRam                             */
/*                                                                           */
/* Description        : This function calls the ISS module to get the        */
/*                      multicast forwarding mode from the nvram.            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Mcast Forwarding Mode                                */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopGetFwdModeFromNvRam (VOID)
{
    return (IssGetSnoopFwdModeFromNvRam ());
}

/*****************************************************************************/
/* Function Name      : SnoopSetFwdModeToNvRam                               */
/*                                                                           */
/* Description        : This function calls the ISS module to set the        */
/*                      multicast forwarding mode to nvram.                  */
/*                                                                           */
/* Input(s)           : u4McastForwardingMode -  Mcast Forwarding Mode       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopSetFwdModeToNvRam (UINT4 u4McastForwardingMode)
{
    IssSetSnoopFwdModeToNvRam (u4McastForwardingMode);
}

/*****************************************************************************/
/* Function Name      : SnoopMiGetVlanEgressPorts                            */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the      */
/*                      VLAN egress porlist for the given vlan, instance     */
/*                      L2Iwf returns local port list for the given VLAN     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : EgressPortBitmap - egress port list (physical port)  */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopMiGetVlanEgressPorts (UINT4 u4Instance, tSnoopTag VlanId,
                           tSnoopIfPortBmp EgressPortBitmap)
{
    tPortList          *pEgressPortList = NULL;

    pEgressPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pEgressPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4Instance %d\r\n", u4Instance);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pEgressPortList, 0, sizeof (tPortList));

    /* In metro package, the size of tPortList and tSnoopIfPortList
     * varies. tPortList (1308 bytes) and tSnoopIfPortList (128 bytes). 
     * Hence, for interfacing with the l2iwf module a local variable
     * is used to obtain the entire portlist and only the 1st 128 bytes
     * is used by snooping module as it is unaware of the
     * VIP configurations */

    if (L2IwfMiGetVlanEgressPorts (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                   *pEgressPortList) == L2IWF_SUCCESS)
    {
        SNOOP_MEM_CPY (EgressPortBitmap, pEgressPortList,
                       sizeof (tSnoopIfPortList));
        FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
        return SNOOP_SUCCESS;
    }

    FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopMiGetVlanLocalEgressPorts                       */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to get the      */
/*                      VLAN egress porlist for the given vlan, instance     */
/*                      L2Iwf returns local port list for the given VLAN     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : EgressPortBitmap - egress port list, local port list */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopMiGetVlanLocalEgressPorts (UINT4 u4Instance, tSnoopTag VlanId,
                                tSnoopPortBmp EgressPortBitmap)
{
    if (L2IwfMiGetVlanLocalEgressPorts (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                        EgressPortBitmap) == L2IWF_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanTagFrame                                    */
/*                                                                           */
/* Description        : This function calls the VLAN Module to send the      */
/*                      tag frame.                                           */
/*                                                                           */
/* Input(s)           : pDupBuf - Buffer to be tagged                        */
/*                      VlanId  - Vlan Identifier                            */
/*                      u1Priority - SNOOP_VLAN_HIGHEST_PRIORITY             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopVlanTagFrame (tCRU_BUF_CHAIN_HEADER * pDupBuf, tVlanId VlanId,
                   UINT1 u1Priority)
{
    VlanTagFrame (pDupBuf, VlanId, u1Priority);

    return;
}

#ifdef SRCMAC_CTP_WANTED
/*****************************************************************************/
/* Function Name      : SnoopHandleOutgoingPkt                             */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      snooping packet.                                     */
/*                                                                           */
/* Input(s)           : pBuf             - pointer to the outgoing packet    */
/*                      PortBitmap       - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopHandleOutgoingPkt (UINT4 u4Instance, tSnoopTag VlanId,
                        tCRU_BUF_CHAIN_HEADER * pBuf,
                        tSnoopIfPortBmp PortBitmap, INT4 u4PktSize,
                        UINT2 u2Protocol, UINT1 u1Encap)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tMacAddr            SrcMacAddr;
    UINT4               u4PhyPort = 0;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (SrcMacAddr, 0, sizeof (tMacAddr));

    /* The PortBitmap obtained contains the physical portlist
     * hence, the port list is scanned for the max physical and lagg
     * interfaces only */
    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS; u4Port++)
    {
        SNOOP_IS_PORT_PRESENT (u4Port, PortBitmap, bResult);

        u4PhyPort = u4Port;

        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        /*Check for the membership of the given Port with given VlanId */
        if (SnoopMiIsVlanMemberPort (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                     u4PhyPort) == SNOOP_FALSE)
        {
            continue;
        }

        /*Call the API for geting SrcMac Address */
        SnoopGetSrcMacAddress ((UINT2) u4PhyPort, VlanId, SrcMacAddr);

        pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

        if (pDupBuf != NULL)
        {
            /*Fill the Src Mac in Packet */
            CRU_BUF_Copy_OverBufChain (pDupBuf, SrcMacAddr,
                                       SNOOP_ETH_ADDR_SIZE,
                                       SNOOP_ETH_ADDR_SIZE);

            /*For sending pkt on given Port */
            L2IwfHandleOutgoingPktOnPort (pDupBuf, u4PhyPort,
                                          u4PktSize, u2Protocol, u1Encap);
        }
    }

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
}

/*****************************************************************************/
/* Function Name      : SnoopHandlePortOutgoingPkt                           */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      snooping packet on a particular port.                */
/*                                                                           */
/* Input(s)           : pBuf             - pointer to the outgoing packet    */
/*                      u4IfIndex        - Interface Index                   */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopHandlePortOutgoingPkt (UINT4 u4Instance, tSnoopTag VlanId,
                            tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                            UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1Encap)
{
    tMacAddr            SrcMacAddr;

    SNOOP_MEM_SET (SrcMacAddr, 0, sizeof (tMacAddr));

    /*Check for the membership of the given Port with given VlanId */
    if (SnoopMiIsVlanMemberPort (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                 u4IfIndex) == SNOOP_FALSE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    /*Call the API for geting SrcMac Address */
    SnoopGetSrcMacAddress ((UINT2) u4IfIndex, VlanId, SrcMacAddr);

    /*Fill the Src Mac in Packet */
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr,
                               SNOOP_ETH_ADDR_SIZE, SNOOP_ETH_ADDR_SIZE);

    /*For sending pkt on given Port */
    L2IwfHandleOutgoingPktOnPort (pBuf, u4IfIndex,
                                  u4PktSize, u2Protocol, u1Encap);
}

#endif

/*****************************************************************************/
/* Function Name      : SnoopMiIsVlanActive                                  */
/*                                                                           */
/* Description        : This routine calls L2Iwf to check whether the given  */
/*                      Vlan is active in a given instance                   */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId - VLAN Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE/ SNOOP_FALSE                              */
/*****************************************************************************/
BOOL1
SnoopMiIsVlanActive (UINT4 u4Instance, tSnoopTag VlanId)
{
    if (L2IwfMiIsVlanActive (u4Instance,
                             SNOOP_OUTER_VLAN (VlanId)) == OSIX_TRUE)
    {
        return SNOOP_TRUE;
    }

    return SNOOP_FALSE;
}

/*****************************************************************************/
/* Function Name      : SnoopMiIsVlanMemberPort                              */
/*                                                                           */
/* Description        : This routine calls L2Iwf to check whether the given  */
/*                      port is a member of the given Vlan.                  */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4PortIndex   - Port Number                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE/SNOOP_FALSE                               */
/*****************************************************************************/
BOOL1
SnoopMiIsVlanMemberPort (UINT4 u4Instance, tSnoopTag VlanId, UINT4 u4PortIndex)
{
    if (L2IwfMiIsVlanMemberPort (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                 u4PortIndex) == OSIX_TRUE)
    {
        return SNOOP_TRUE;
    }

    return SNOOP_FALSE;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanApiForwardOnPorts                           */
/*                                                                           */
/* Description        : This function calls the VLAN Module to forward the   */
/*                      packet on specific portlist                          */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be forwarded.                       */
/*                       pVlanFwdInfo- Forwarding information needed by      */
/*                                        VLAN to transmit the packet.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVlanApiForwardOnPorts (tCRU_BUF_CHAIN_DESC * pBuf,
                            tVlanFwdInfo * pVlanFwdInfo)
{

    return VlanApiForwardOnPorts (pBuf, pVlanFwdInfo);
}

#ifdef SRCMAC_CTP_WANTED

/*****************************************************************************/
/* Function Name      : SnoopGetSrcMacAddress                                */
/*                                                                           */
/* Description        : This function is called to get the source MAC        */
/*                      address of the outgoing packat.                      */
/*                                                                           */
/* Input(s)           : u2Port       - Port Number                           */
/*                      VlanId      - Vlan Identifier of the packet.         */
/*                                                                           */
/* Output(s)          : SrcMacAddr     -  Src MAC address                    */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopGetSrcMacAddress (UINT2 u2Port, tSnoopTag VlanId, tMacAddr SrcMacAddr)
{
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (SrcMacAddr);
}

#endif
#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : SnoopCfaCliGetIfList                                 */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface list from the given string.                */
/*                                                                           */
/* Input(s)           : pi1IfName        :Port Number                        */
/*                    : pi1IfListStr     : Pointer to the string             */
/*                    : pu1IfListArray   :Pointer to the string in which the */
/*                      bits for the port list will be set                   */
/*                    : u4IfListArrayLen :Array Length                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCfaCliGetIfList (INT1 *pi1IfName, INT1 *pi1IfListStr,
                      UINT1 *pu1IfListArray, UINT4 u4IfListArrayLen)
{
    return (CfaCliGetIfList (pi1IfName, pi1IfListStr, pu1IfListArray,
                             u4IfListArrayLen));
}
#endif
/*****************************************************************************/
/* Function Name      : SnoopCfaCliGetIfName                                 */
/*                                                                           */
/* Description        : This function calls the CFA Module to get the        */
/*                      interface name from the given interface index.       */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pi1IfName - Interface name of the given IfIndex.     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
}

/*****************************************************************************/
/* Function Name      : SnoopCfaGetIfInfo                                    */
/*                                                                           */
/* Description        : This function returns the interface related params   */
/*                       assigned to this interface                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface index                          */
/*                                                                           */
/* Output(s)          : pIfInfo   - Interface Information                    */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : SnoopGmrpIsGmrpEnabledInContext                      */
/*                                                                           */
/* Description        : This function calls the GARP Module to get the       */
/*                      status of the GMRP (ENABLED/ DISABLED).              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopGmrpIsGmrpEnabledInContext (UINT4 u4ContextId)
{
    return (GmrpIsGmrpEnabledInContext (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : SnoopMmrpIsMmrpEnabledInContext                      */
/*                                                                           */
/* Description        : This function calls the MRP Module to get the        */
/*                      status of the MMRP (ENABLED/ DISABLED).              */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopMmrpIsMmrpEnabledInContext (UINT4 u4ContextId)
{
    return (MrpApiIsMmrpEnabled (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : SnoopVcmGetAliasName                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************/
/* Function Name      : SnoopVcmGetContextInfoFromIfIndex                    */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Context-Id and the Localport number.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier.                    */
/*                                                                           */
/* Output(s)          : pu4ContextId - Context Identifier.                   */
/*                      pu2LocalPortId - Local port number.                  */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmGetContextInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                   UINT2 *pu2LocalPortId)
{
    return (VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                          pu2LocalPortId));
}

/*****************************************************************************/
/* Function Name      : SnoopVcmIsSwitchExist                                */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************/
/* Function Name      : SnoopVcmGetSystemMode                                */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : SnoopVcmGetSystemModeExt                                */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : SnoopL2IwfGetNextValidPortForContext                 */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the next */
/*                      Valid port for the context.                          */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                      u2LocalPortId - Local Port Identifier.               */
/*                                                                           */
/* Output(s)          : pu2NextLocalPort - Next local port identifier.       */
/*                      pu4NextIfIndex - Next Interface Index.               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                      UINT2 *pu2NextLocalPort,
                                      UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_SNOOP;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/*****************************************************************************/
/* Function Name      : SnoopL2IwfIsIgmpTunnelEnabledOnAnyPort               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      Status of IGMP tunneling.                            */
/*                                                                           */
/* Input(s)           : u2ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopL2IwfIsIgmpTunnelEnabledOnAnyPort (UINT4 u4ContextId)
{
    return (L2IwfIsIgmpTunnelEnabledOnAnyPort (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : SnoopL2IwfIsPortInPortChannel                        */
/*                                                                           */
/* Description        : This function calls the L2IWF module to check        */
/*                      whether the port is in port-channel.                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopL2IwfIsPortInPortChannel (UINT4 u4IfIndex)
{
    return (L2IwfIsPortInPortChannel (u4IfIndex));
}

/*****************************************************************************/
/* Function Name      : SnoopConfigSwitchIp                                  */
/*                                                                           */
/* Description        : This function will be called to initialize the       */
/*                      switch IP that will be used as the Src IP for        */
/*                      all IGS packets in proxy mode.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopConfigSwitchIp (VOID)
{
    gu4SwitchIp = IssGetIpAddrFromNvRam ();
}

/*****************************************************************************/
/* Function Name      : SnoopTacSearchGrpAddressInProfile                    */
/*                                                                           */
/* Description        : This function calls the TAC module to check          */
/*                      whether the particular group address is configured   */
/*                      in the profile                                       */
/*                                                                           */
/* Input(s)           : u4ProfileId   - Profile Identifier                   */
/*                      u4GrpAddr     - Group address to be searched in the  */
/*                      given profile                                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE/SNOOP_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacSearchGrpAddressInProfile (UINT4 u4ProfileId, UINT4 u4GrpAddr)
{
    if (TacSearchGrpAddressInProfile (u4ProfileId, u4GrpAddr) == TACM_TRUE)
    {
        return SNOOP_TRUE;
    }
    return SNOOP_FALSE;
}

/*****************************************************************************/
/* Function Name      : SnoopTacApiGetPrfStats                               */
/*                                                                           */
/* Description        : This function calls the TAC module to get the        */
/*                      statistics about the particular profile              */
/*                                                                           */
/* Input(s)           : u4Instance  - Context Identifier                     */
/*                      u4ProfileId - Profile Identifier                     */
/*                      u1AddressType - Address Type                         */
/*                      pTacmPrfStats - Profile Status                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacApiGetPrfStats (UINT4 u4Instance, UINT4 u4ProfileId,
                        UINT1 u1AddressType, tTacmPrfStats * pTacmPrfStats)
{
    if (TacApiGetPrfStats (u4Instance, u4ProfileId, u1AddressType,
                           pTacmPrfStats) == TACM_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopTacApiUpdatePortRefCount                        */
/*                                                                           */
/* Description        : This function updates the port reference count for   */
/*                      the particular profile. A profile can be deleted     */
/*                      only if the port reference count is 0                */
/*                                                                           */
/* Input(s)           : u4ProfileId      - Profile ID                        */
/*                      u1AddressType    - Internet address type             */
/*                      u1UpdtFlag       - TACM_INTF_MAP/TACM_INTF_UNMAP     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : TACM_SUCCESS/TACM_FAILURE                            */
/*****************************************************************************/
INT4
SnoopTacApiUpdatePortRefCount (UINT4 u4ProfileId, UINT1 u1AddressType,
                               UINT1 u1UpdtFlag)
{
    if (TacApiUpdatePortRefCount (u4ProfileId, u1AddressType,
                                  u1UpdtFlag) == TACM_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopTacApiUpdateVlanRefCount                        */
/*                                                                           */
/* Description        : This function calls the TAC module to update the     */
/*                      reference count for this profile, if the particular  */
/*                      profile is mapped or unmapped to a VLAN.             */
/*                                                                           */
/* Input(s)           : u4Instance  - Context Identifier                     */
/*                      u4ProfileId - Profile Identifier                     */
/*                      u1AddressType - Address Type                         */
/*                      u1UpdtFlag    - Updation flag - Map or Unmap         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacApiUpdateVlanRefCount (UINT4 u4Instance, UINT4 u4ProfileId,
                               UINT1 u1AddressType, UINT1 u1UpdtFlag)
{
    if (TacApiUpdateVlanRefCount (u4Instance, u4ProfileId, u1AddressType,
                                  u1UpdtFlag) == TACM_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopTacApiGetPrfAction                              */
/*                                                                           */
/* Description        : This function calls the TAC module to get the        */
/*                      profile action for this profile.                     */
/*                                                                           */
/* Input(s)           : u4ProfileId - Profile Identifier                     */
/*                      u1AddressType - Address Type                         */
/*                                                                           */
/* Output(s)          : pu1ProfileAction - Profile action                    */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacApiGetPrfAction (UINT4 u4ProfileId, UINT1 u1AddressType,
                         UINT1 *pu1ProfileAction)
{
    if (TacApiGetPrfAction (u4ProfileId, u1AddressType, pu1ProfileAction)
        == TACM_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopTacSearchGrpSrcRecordInProfile                  */
/*                                                                           */
/* Description        : This function calls the TAC module to get the        */
/*                      match for Group/Source address passed in the         */
/*                      pGrpRecords. Status Flag of each node will be        */
/*                      updated by TAC, if there is any match                */
/*                                                                           */
/* Input(s)           : u2ProfileId   - Profile Identifier                   */
/*                      pGrpRecords   - Pointer to SLL of Group & Source     */
/*                      address, received in the incoming packet.            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacSearchGrpSrcRecordInProfile (UINT4 u4ProfileId,
                                     tGrpRecords * pGrpRecords)
{
    if (TacSearchGrpSrcRecordInProfile (u4ProfileId, pGrpRecords) ==
        TACM_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopTacApiFilterChannel                         */
/*                                                                           */
/*    Description         : This function fills the group, source, version   */
/*                          and type of packet in the TACM structure and     */
/*                          calls the TAC API. The function returns the      */
/*                          action decided by the TAC module                 */
/*                                                                           */
/*    Input(s)            : pGroupAddr    - pointer to IPvXAddr having the   */
/*                                          greoup address                   */
/*                          u4ProfileId   - Profile Identifier               */
/*                          u2NumSrc      - Number of sources                */
/*                          u1Version     - Packet version                   */
/*                          u1PktType     - Packet type                      */
/*                          u1RecordType  - Type for V3 packets              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables                                                       */
/*    Referred           : gpSSMSrcInfoBuffer                                */
/*                                                                           */
/*    Global Variables                                                       */
/*    Modified           : gpSSMSrcInfoBuffer                                */
/*                                                                           */
/*    Returns            : TACM_PERMIT or TACM_DENY                          */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopTacApiFilterChannel (tIPvXAddr * pGroupAddr, UINT4 u4ProfileId,
                          UINT2 u2NumSrc, UINT1 u1Version,
                          UINT1 u1PktType, UINT1 u1RecordType)
{
    tTacFilterPkt       TacFilterPkt;

    MEMSET (&TacFilterPkt, 0, sizeof (tTacFilterPkt));

    IPVX_ADDR_COPY (&TacFilterPkt.GroupAddr, pGroupAddr);
    TacFilterPkt.u4ProfileId = u4ProfileId;
    TacFilterPkt.u4NumSrc = u2NumSrc;
    TacFilterPkt.u1Version = u1Version;
    TacFilterPkt.u1PktType = u1PktType;

    switch (u1RecordType)
    {
        case SNOOP_IS_INCLUDE:
        case SNOOP_TO_INCLUDE:
        {
            TacFilterPkt.u1Mode = TACM_IGMP_INCLUDE;
            break;
        }
        case SNOOP_IS_EXCLUDE:
        case SNOOP_TO_EXCLUDE:
        {
            TacFilterPkt.u1Mode = TACM_IGMP_EXCLUDE;
            break;
        }
        case SNOOP_ALLOW:
        {
            TacFilterPkt.u1Mode = TACM_IGMP_ALLOW;
            break;
        }
        case SNOOP_BLOCK:
        {
            TacFilterPkt.u1Mode = TACM_IGMP_BLOCK;
            break;
        }
        default:
        {
            /* The default case occurs for V1/V2 reports */
            break;
        }
    }

    if ((u1Version == (UINT1) TACM_IGMP_VERSION3) && (u2NumSrc != 0))
    {
        return TacApiFilterChannel (&TacFilterPkt, gpSSMSrcInfoBuffer);
    }

    return TacApiFilterChannel (&TacFilterPkt, NULL);
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : SnoopRmEnqMsgToRmFromAppl                            */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                           UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : SnoopRmGetNodeState                                  */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : SnoopRmGetStandbyNodeCount                           */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
SnoopRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : SnoopRmGetStaticConfigStatus                         */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
SnoopRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : SnoopRmHandleProtocolEvent                        */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                       RM_BULK_UPDT_ABORT /                */
/*                                       RM_STANDBY_TO_ACTIVE_EVT_PROCESSED/ */
/*                                       RM_IDLE_TO_ACTIVE_EVT_PROCESSED /   */
/*                                       RM_STANDBY_EVT_PROCESSED)           */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
SnoopRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : SnoopRmRegisterProtocols                             */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : SnoopRmReleaseMemoryForMsg                           */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : SnoopRmSendEventToRmTask                             */
/*                                                                           */
/* Description        : This function calls the RM module to post event to   */
/*                      RM task.                                             */
/*                                                                           */
/* Input(s)           : u4Event - Event type.                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopRmSendEventToRmTask (UINT4 u4Event)
{
    tRmProtoEvt         ProtoEvt;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
    ProtoEvt.u4SendEvent = u4Event;

    return RmApiHandleProtocolEvent (&ProtoEvt);
}

/*****************************************************************************/
/* Function Name      : SnoopRmSetBulkUpdatesStatus                          */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}

/*****************************************************************************/
/* Function Name      : SnoopVlanRedStartMcastAudit                          */
/*                                                                           */
/* Description        : This function calls the VLAN Module to indicate that */
/*                      the node becomes active.                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopVlanRedStartMcastAudit (VOID)
{
    VlanRedStartMcastAudit ();
}

#ifdef LLDP_WANTED
/*****************************************************************************/
/* Function Name      : SnoopLldpRedRcvPktFromRm                             */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the LLDP*/
/*                      queue                                                */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if the event is sent successfully then return        */
/*                      OSIX_SUCCESS otherwise return OSIX_FAILURE           */
/*****************************************************************************/
INT4
SnoopLldpRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    return (LldpRedRcvPktFromRm (u1Event, pData, u2DataLen));
}
#endif
#endif
/*****************************************************************************/
/* Function Name      : SnoopVcmSispGetPhysicalPortOfSispPort                */
/*                                                                           */
/* Description        : This function will provide the physical interface    */
/*                      index over which the given logical port runs. This   */
/*                      will be invoked to obtain the corresponding          */
/*                      physical interface index over which a logical port   */
/*                      runs.                                                */
/*                                                                           */
/* Input(s)           : u4IfIndex - Logical interface index.                 */
/*                                                                           */
/* Output(s)          : pu4PhyIndex - Physical interface index over which    */
/*                                    this logical runs.                     */
/*                                                                           */
/* Return Value(s)    : VCM_SUCCESS/VCM_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVcmSispGetPhysicalPortOfSispPort (UINT4 u4IfIndex, UINT4 *pu4PhyIndex)
{
    return (VcmSispGetPhysicalPortOfSispPort (u4IfIndex, pu4PhyIndex));
}

/*****************************************************************************/
/* Function Name      : SnoopVlanCheckWildCardEntry                          */
/*                                                                           */
/* Description        : This function calls the VLAN Module to check for the */
/*                      wildcard entry for the given mac                     */
/*                                                                           */
/* Input(s)           : WildCardMacAddr - Mac address of wild card entry     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE/SNOOP_FALSE                               */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVlanCheckWildCardEntry (tMacAddr WildCardMacAddr)
{
    if (VlanCheckWildCardEntry (WildCardMacAddr) == VLAN_FALSE)
    {
        return SNOOP_TRUE;
    }

    return SNOOP_FALSE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnpPortRegisterWithNetip                         */
/*                                                                           */
/*    Description         : This function is called to register with Netip   */
/*                            for ip address change indication                 */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
SnpPortRegisterWithNetip (VOID)
{
    tNetIpRegInfo       NetRegInfo;

    /* Register with IP */
    MEMSET ((UINT1 *) &NetRegInfo, 0, sizeof (tNetIpRegInfo));
    NetRegInfo.u2InfoMask = NETIPV4_IFCHG_REQ;
    NetRegInfo.u1ProtoId = SNOOP_ID;
    NetRegInfo.pIfStChng = (VOID *) SnpApiIndicateIpAddrChange;
    NetRegInfo.pRtChng = NULL;
    NetRegInfo.pProtoPktRecv = NULL;
    if (NetIpv4RegisterHigherLayerProtocol (&NetRegInfo) == NETIPV4_FAILURE)
    {
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnpPortDeRegisterWithNetip                       */
/*                                                                           */
/*    Description         : This function is called to deregister with Netip */
/*                            for ip address change indication                 */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*                                                                           */
/*****************************************************************************/
INT4
SnpPortDeRegisterWithNetip (VOID)
{
    /* De-Register with IP */
    if (NetIpv4DeRegisterHigherLayerProtocol (SNOOP_ID) == NETIPV4_FAILURE)
    {
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnpPortGetVlanIpInfo                                 */
/*                                                                           */
/* Description        : This function is used to get the IP address of       */
/*                      the vlan interface.                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - L3 Interface IfIndex                     */
/*                                                                           */
/* Output(s)          : pu4VlanIp - IP address of the given L3 interface     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnpPortGetVlanIpInfo (UINT4 u4IfIndex, UINT4 *pu4VlanIp)
{
    tIpConfigInfo       IpIntfInfo;

    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIntfInfo) == CFA_SUCCESS)
    {
        *pu4VlanIp = IpIntfInfo.u4Addr;
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopGetIfOperStatus                                 */
/*                                                                           */
/* Description        : This function calls the CFA module to get the        */
/*                      oper status of an interface                          */
/*                                                                           */
/* Input(s)           : u4Port - Port number                                 */
/*                                                                           */
/* Output(s)          : pu1OperStatus - pointer to the oper status           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopGetIfOperStatus (UINT4 u4Port, UINT1 *pu1OperStatus)
{
    tCfaIfInfo          IfInfo;

    if (CfaGetIfInfo (u4Port, &IfInfo) == CFA_FAILURE)
    {
        return SNOOP_FAILURE;
    }

    *pu1OperStatus = IfInfo.u1IfOperStatus;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopGetIgsStatus                            */
/*                                                                           */
/*    Description         : This function validates the given VLAN ID        */
/*                          and check IP based IGS and Sparse Mode is        */
/*                          enabled in a vlan                                */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS/OSIX_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGetIgsStatus (tVlanId VlanId)
{

    INT4                i4RetVal = OSIX_FAILURE;

    SNOOP_LOCK ();

    i4RetVal = SnoopUtilGetIgsStatus (VlanId);

    SNOOP_UNLOCK ();

    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : SnoopGetForwardingDataBase                           */
/*                                                                           */
/* Description        : This function will return (S,G) Entry and PortList   */
/*                      for the incoming VLAN ID                             */
/*                      Note. Source IP and Group IP should be filled        */
/*                      with current value to get the next valid value.      */
/*                      if given with 0's then first S,G will be returned    */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      pSrcAddr - Source Address (initial value)            */
/*                      pGrpAddr - Group Address  (initial value)            */
/*                      PortBitmap - Portlist to be updated.                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : NONE                                                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCCESS/OSIX_FAILURE                           */
/******************************************************************************/
INT4
SnoopGetForwardingDataBase (tVlanId VlanId, tIPvXAddr * pSrcAddr,
                            tIPvXAddr * pGrpAddr)
{

    INT4                i4RetVal = OSIX_FAILURE;

    SNOOP_LOCK ();

    i4RetVal = SnoopUtilGetForwardingDataBase (VlanId, pSrcAddr, pGrpAddr);

    SNOOP_UNLOCK ();

    return i4RetVal;

}

#ifdef ICCH_WANTED
/*****************************************************************************/
/* Function Name      : SnoopIcchRegisterProtocols                        */
/*                                                                           */
/* Description        : This function calls the ICCH module to register.     */
/*                                                                           */
/* Input(s)           : tIcchRegParams - Reg. params to be provided by       */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopIcchRegisterProtocols (tIcchRegParams * pIcchReg)
{
    return (IcchRegisterProtocols (pIcchReg));
}

/*****************************************************************************/
/* Function Name      : SnoopIcchEnqMsgToIcchFromAppl                        */
/*                                                                           */
/* Description        : This function calls the ICCH Module to enqueue the   */
/*                      message from applications to ICCH task.              */
/*                      ICCH_MESSAGE, ICCH_BULK_REQ_MESSAGE,                 */
/*                      ICCH_BULK_TAIL_MESSAGE types are enqueued to ICCH    */
/*                                                                           */
/* Input(s)           : pIcchMsg - msg from appl.                            */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : ICCH_FAILURE/ICCH_SUCCESS                            */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopIcchEnqMsgToIcchFromAppl (tIcchMsg * pIcchMsg, UINT2 u2DataLen,
                               UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
    return (IcchEnqMsgToIcchFromAppl
            (pIcchMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}
#endif

/*****************************************************************************/
/* Function Name      : SnoopLaGetMCLAGSystemStatus                          */
/*                                                                           */
/* Description        : This function is used to get the MCLAG system status */
/*                      whether MCLAG is enabled/disbaled in the system      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gu1MCLAGSystemStatus                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_DISABLED - If MCLAG is disabled in the system     */
/*                      LA_ENABLED  - If MCLAG is enabled in the system      */
/*                                                                           */
/*****************************************************************************/
UINT1
SnoopLaGetMCLAGSystemStatus (VOID)
{
#ifdef LA_WANTED
    return (LaGetMCLAGSystemStatus ());
#else
    return LA_DISABLED;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopIcchIsIcclInterface                             */
/*                                                                           */
/* Description        : This Function checks whether the given interface is  */
/*                      ICCL Interface                                       */
/*                                                                           */
/* Input(s)           : u4IfIndex  -  If Index                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchSessionInfo                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE or OSIX_FAILURE                            */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
SnoopIcchIsIcclInterface (UINT4 u4PhyPort)
{
#ifdef ICCH_WANTED
    return (IcchIsIcclInterface (u4PhyPort));
#else
    UNUSED_PARAM (u4PhyPort);
    return OSIX_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopLaIsMclagInterface                              */
/*                                                                           */
/* Description        : This function is used to check whether the interface */
/*                      is MC-LAG interface or not.                          */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopLaIsMclagInterface (UINT4 u4IfIndex, UINT1 *pu1IsMclagEnabled)
{
#ifdef LA_WANTED
    UINT1               u1IsMclag = 0;
    LaApiIsMclagInterface (u4IfIndex, &u1IsMclag);
    if (u1IsMclag == OSIX_FALSE)
    {
        *pu1IsMclagEnabled = OSIX_FALSE;
    }
    else
    {
        *pu1IsMclagEnabled = OSIX_TRUE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1IsMclagEnabled);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchGetIcclIfIndex                              */
/*                                                                           */
/* Description        : This function is used to fetch the ICCL interface    */
/*                      index.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : *pu4IfIndex - ICCL interface index.                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopIcchGetIcclIfIndex (UINT4 *pu4IfIndex)
{
#ifdef ICCH_WANTED
    IcchGetIcclIfIndex (pu4IfIndex);
#else
    UNUSED_PARAM (pu4IfIndex);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchIsIcclVlan                                  */
/*                                                                           */
/* Description        : This function checks whether the VLAN ID is ICCL     */
/*                      VLAN                                                 */
/*                                                                           */
/* Input(s)           : u2VlanId - VLAN Identifier                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gIcchSessionInfo[index].VlanId                       */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
SnoopIcchIsIcclVlan (UINT2 u2VlanId)
{
#ifdef ICCH_WANTED
    return IcchIsIcclVlan (u2VlanId);
#else
    UNUSED_PARAM (u2VlanId);
    return OSIX_FAILURE;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopLaGetAggIndexFromAdminKey                       */
/*                                                                           */
/* Description        : This function is used to get the port-channel index  */
/*                      from the given admin key.                            */
/*                                                                           */
/* Input(s)           : u2AdminKey - admin key of the port-channel           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu4IfIndex - pointer to fill the port-channel index  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopLaGetAggIndexFromAdminKey (UINT2 u2AdminKey, UINT4 *pu4IfIndex)
{
#ifdef LA_WANTED
    return (LaGetAggIndexFromAdminKey (u2AdminKey, pu4IfIndex));
#else
    UNUSED_PARAM (u2AdminKey);
    UNUSED_PARAM (pu4IfIndex);
    return LA_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopLaUpdateActorPortChannelAdminKey                */
/*                                                                           */
/* Description        : This function is used to get the admin key from      */
/*                      the given port-channel index.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - If index of the port-channel             */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pu2AdminKey - pointer to fill the admin key          */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : LA_TRUE/LA_FALSE                                     */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopLaUpdateActorPortChannelAdminKey (UINT4 u4IfIndex, UINT2 *pu2AdminKey)
{
#ifdef LA_WANTED
    return (LaUpdateActorPortChannelAdminKey (u4IfIndex, pu2AdminKey));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2AdminKey);
    return LA_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopCfaGetInterfaceBrgPortType                      */
/*                                                                           */
/* Description        : This function is used to get the interface bridge    */
/*                      port type for the given Interface index.             */
/*                                                                           */
/* Input(s)           : u4IfIndex - If index                                 */
/*                                                                           */
/* Output(s)          : pi4IfBrgPortType - Bridge Port Type.                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCfaGetInterfaceBrgPortType (UINT4 u4IfIndex, INT4 *pi4IfBrgPortType)
{
    return (CfaGetInterfaceBrgPortType (u4IfIndex, pi4IfBrgPortType));
}

/**** UNKNOWN_MULTICAST_CHANGE - START ****/
/*****************************************************************************/
/* Function Name      : SnoopMiGetEgressPorts                                */
/*                                                                           */
/* Description        : This function will return EgressPorts of the         */
/*                      given VLAN.                                          */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      PortBitmap - Portlist to be updated.                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : NONE                                                     */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCCESS/OSIX_FAILURE                           */
/******************************************************************************/

INT4
SnoopMiGetEgressPorts (tVlanId VlanId, tPortList pPortBitmap)
{
    if (VlanGetEgressPorts (VlanId, (tLocalPortList *) pPortBitmap) !=
        VLAN_SUCCESS)
    {
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;

}

/**** UNKNOWN_MULTICAST_CHANGE - END ****/
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopVlanApiEvbGetSbpPortsOnUap                  */
/*                                                                           */
/*    Description         : This function retrieves the SBP ports present on */
/*                          the UAP.                                         */
/*                                                                           */
/*    Input(s)            : u4UapIfIndex - UAP If Index.                     */
/*                                                                           */
/*    Output(s)           : pSbpArray    - SBP ports.                        */
/*                                                                           */
/*    Returns             : VLAN_SUCCESS/VLAN_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopVlanApiEvbGetSbpPortsOnUap (UINT4 u4UapIfIndex,
                                 tVlanEvbSbpArray * pSbpArray)
{
    return (VlanApiEvbGetSbpPortsOnUap (u4UapIfIndex, pSbpArray));
}
