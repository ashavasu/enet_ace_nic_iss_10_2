/* $Id: snptmr.c,v 1.87 2018/01/22 09:41:25 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snptmr.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping Timer                                 */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains timer routines              */
/*                            for the snooping module                        */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopTmrStartTimer                                   */
/*                                                                           */
/* Description        : This function starts the timer for the specified     */
/*                      App Timer (timer node)                               */
/*                                                                           */
/* Input(s)           : pSnoopTmrNode - Timer Node                           */
/*                      u4Duration - duration for which timer is started     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrStartTimer (tSnoopTmrNode * pSnoopTmrNode, UINT4 u4Duration)
{
    tSnoopAppTimer     *pAppTmr = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = pSnoopTmrNode->u4Instance;
    gu4SnoopDbgInstId = pSnoopTmrNode->u4Instance;

    /* Protocol timers should not be started in the standby node */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE))
    {
        /* Since the timer is not started, reset the timer type since this would 
         * be set before calling this function */
        pSnoopTmrNode->u1TimerType = SNOOP_INVALID_TIMER_TYPE;
        return SNOOP_SUCCESS;
    }

    SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                        SNOOP_DBG_TMR,
                        SNOOP_TMR_DBG, "Starting timer for "
                        "TimerType : %s and duration is %d\r\n",
                        gau1TmrType[pSnoopTmrNode->u1TimerType], u4Duration);

    /* Check whether timer type and APPtimer data field has been set */
    if (pSnoopTmrNode->u1TimerType == SNOOP_INVALID_TIMER_TYPE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                       SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_TMR_NAME, "Invalid Timer type in start timer\r\n");
        SNOOP_TRC (SNOOP_DBG_FLAG, SNOOP_CONTROL_TRC, SNOOP_CONTROL_TRC,
                   "Invalid Timer type in start timer\r\n");
        return SNOOP_FAILURE;
    }

    pAppTmr = &pSnoopTmrNode->AppTmr;

    u4Duration = u4Duration * SYS_TIME_TICKS_IN_A_SEC;

    if (TmrStartTimer (SNOOP_TMR_LIST_ID, pAppTmr, u4Duration) != TMR_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                            SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                            SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                            "Unable to start the SNOOP Timer of type %s \r\n",
                            gau1TmrType[pSnoopTmrNode->u1TimerType]);
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrStopTimer                                    */
/*                                                                           */
/* Description        : This function stops the specified timer              */
/*                                                                           */
/* Input(s)           : pTmrNode - pointer to timer node                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrStopTimer (tSnoopTmrNode * pSnoopTmrNode)
{
    if (pSnoopTmrNode == NULL)
    {
        return SNOOP_FAILURE;
    }

    /* This is done for trace messages */
    gu4SnoopTrcInstId = pSnoopTmrNode->u4Instance;
    gu4SnoopDbgInstId = pSnoopTmrNode->u4Instance;

    /* Protocol timers should not be used in the standby node */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) &&
        (SNOOP_NODE_STATUS () != SNOOP_ACTIVE_TO_STANDBY_IN_PROGRESS) &&
        (SNOOP_NODE_STATUS () != SNOOP_SHUT_START_IN_PROGRESS))
    {
        pSnoopTmrNode->u1TimerType = SNOOP_INVALID_TIMER_TYPE;
        return SNOOP_SUCCESS;
    }

    if (pSnoopTmrNode->u1TimerType == SNOOP_INVALID_TIMER_TYPE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                       SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                       "Invalid Timer type to stop the timer\r\n");
        return SNOOP_FAILURE;

    }

    if (TmrStopTimer (SNOOP_TMR_LIST_ID, &pSnoopTmrNode->AppTmr) != TMR_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                       SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                       "Invalid Timer ID. Timer not present\r\n");
    }
    pSnoopTmrNode->u1TimerType = SNOOP_INVALID_TIMER_TYPE;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrGetRemainingTime                             */
/*                                                                           */
/* Description        : This function gets remaining time for expiry of the  */
/*                      given timer reference node, used here to mainly      */
/*                      identify the presence of a timer type                */
/*                                                                           */
/* Input(s)           : pSnoopTmrNode - Timer Node                           */
/*                      u4Duration - duration for which timer is started     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrGetRemainingTime (tSnoopTmrNode * pSnoopTmrNode)
{
    tSnoopAppTimer     *pAppTmr = NULL;
    UINT4               u4RemainingTime = 0;
    if (pSnoopTmrNode == NULL)
    {
        return SNOOP_FAILURE;
    }

    /* Check whether timer type and APPtimer data field has been set */
    if (pSnoopTmrNode->u1TimerType == 0)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                   "Invalid Timer type in get remaining time.\r\n");
        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC, SNOOP_CONTROL_TRC,
                   "Invalid Timer type in get remaining time.\r\n");
        return SNOOP_FAILURE;
    }

    pAppTmr = &pSnoopTmrNode->AppTmr;

    if (TmrGetRemainingTime (SNOOP_TMR_LIST_ID,
                             pAppTmr, &u4RemainingTime) != TMR_SUCCESS)
    {
        /* Timer not present - Return Failure to start a new timer */
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pSnoopTmrNode->u4Instance),
                       SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                       "Invalid Timer ID. Timer not present.\r\n");
        return SNOOP_FAILURE;
    }

    /* Timer present already - Return Success */
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrExpiryHandler                                */
/*                                                                           */
/* Description        : This function is called from STAP timer task when    */
/*                      any SNOOP timer expires. The expiry is handled from  */
/*                      here                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :                                                      */
/*****************************************************************************/
VOID
SnoopTmrExpiryHandler (VOID)
{
    tSnoopTmrNode      *pExpiredTmr = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tTimerListId        TmrListId;
    UINT4               u4Offset = 0;
    INT2                i2TmrCnt = 0;

    /* Take a protocol lock here */

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return;
    }
    TmrListId = gSnoopTimerListId;

    pExpiredTmr = NULL;

    while ((pExpiredTmr = (tSnoopTmrNode *)
            TmrGetNextExpiredTimer (TmrListId)) != NULL)
    {
        /* This is done for trace messages */
        gu4SnoopTrcInstId = pExpiredTmr->u4Instance;
        gu4SnoopDbgInstId = pExpiredTmr->u4Instance;

#ifdef L2RED_WANTED
        /* Protocol timers should not be started in the standby node */
        if ((RmGetNodeState () != RM_ACTIVE))
        {
            /* Since the timer is not started, reset the timer type since this would 
             * be set before calling this function */
            pExpiredTmr->u1TimerType = SNOOP_INVALID_TIMER_TYPE;
            continue;
        }
#endif

        switch (pExpiredTmr->u1TimerType)
        {
#ifdef L2RED_WANTED
            case SNOOP_RED_SWITCHOVER_QUERY_TIMER:
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_QRY_DBG,
                           SNOOP_QRY_DBG, "Switchover Query Timer"
                           "Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC, "Switchover Query Timer Timer "
                           "Expired\r\n");
                pExpiredTmr->u1TimerType = 0;

                SnoopRedSwitchOverQueryTmrExpiry ();
                break;

#endif /* L2RED_WANTED */
                /* Process Report Forward timer expiry */
            case SNOOP_REPORT_FWD_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG, "Report Forward Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Report Forward Timer Expired\r\n");
                pExpiredTmr->u1TimerType = 0;
                pExpiredTmr->u4Instance = 0;
                u4Offset = SNOOP_OFFSET (tSnoopGroupEntry, ReportFwdTimer);

                pSnoopGroupEntry =
                    (tSnoopGroupEntry *) (VOID *)
                    ((UINT1 *) pExpiredTmr - u4Offset);
                pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
                break;

            case SNOOP_QUERY_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_QRY_DBG, "Query Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC, "Query Timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopVlanEntry, QueryTimer);

                if (SnoopTmrQueryTimerExpiry (pExpiredTmr->u4Instance,
                                              ((UINT1 *) pExpiredTmr -
                                               u4Offset)) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_QRY_DBG,
                               SNOOP_QRY_DBG,
                               "Query timer expiry processing failed\r\n");
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "Query timer expiry processing failed\r\n");
                }
                break;
            case SNOOP_ROUTER_PORT_PURGE_V1_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "V1 Router port purge Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "V1 Router port purge Timer Expired\r\n");

                u4Offset =
                    SNOOP_OFFSET (tSnoopRtrPortEntry, V1RtrPortPurgeTimer);

                if (SnoopTmrV1RtrPortPurgeTimerExpiry (pExpiredTmr->u4Instance,
                                                       ((UINT1 *) pExpiredTmr -
                                                        u4Offset))
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                               SNOOP_TMR_DBG,
                               "V1 Router port purge timer expiry "
                               "processing failed\r\n");
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "V1 Router port purge timer expiry "
                               "processing failed\r\n");
                }
                break;

            case SNOOP_ROUTER_PORT_PURGE_V2_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "V2 Router port purge Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "V2 Router port purge Timer Expired\r\n");

                u4Offset =
                    SNOOP_OFFSET (tSnoopRtrPortEntry, V2RtrPortPurgeTimer);

                if (SnoopTmrV2RtrPortPurgeTimerExpiry (pExpiredTmr->u4Instance,
                                                       ((UINT1 *) pExpiredTmr -
                                                        u4Offset))
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                               SNOOP_TMR_DBG,
                               "V2 Router port purge timer expiry "
                               "processing failed\r\n");
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "V2 Router port purge timer expiry "
                               "processing failed\r\n");
                }
                break;

            case SNOOP_ROUTER_PORT_PURGE_V3_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "V3 Router port purge Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "V3 Router port purge Timer Expired\r\n");

                u4Offset =
                    SNOOP_OFFSET (tSnoopRtrPortEntry, V3RtrPortPurgeTimer);

                if (SnoopTmrV3RtrPortPurgeTimerExpiry (pExpiredTmr->u4Instance,
                                                       ((UINT1 *) pExpiredTmr -
                                                        u4Offset))
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_TRC_CONTROL |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_TMR_NAME,
                                   "V3 Router port purge timer expiry "
                                   "processing failed\r\n");
                }
                break;

            case SNOOP_ASM_PORT_PURGE_TIMER:

                /* Process ASM port purge timer for the port which is in 
                 * ASMPortList record. */
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG, "ASM Port purge Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "ASM Port purge Timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopPortEntry, PurgeOrGrpQueryTimer);

                if (SnoopTmrASMPortPurgeTimerExpiry (pExpiredTmr->u4Instance,
                                                     ((UINT1 *) pExpiredTmr -
                                                      u4Offset)) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_TRC_CONTROL |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_TMR_NAME,
                                   "ASM Port purge timer expiry "
                                   "processing failed\r\n");
                }

                break;
            case SNOOP_GRP_QUERY_TIMER:

                /* This timer is started on reception of a leave message */
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_QRY_DBG,
                           "Leave/Done or Group query timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Leave/Done or Group query timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopPortEntry, PurgeOrGrpQueryTimer);

                if (SnoopTmrGrpQueryTimerExpiry (pExpiredTmr->u4Instance,
                                                 ((UINT1 *) pExpiredTmr -
                                                  u4Offset)) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                                   "Leave/Done or Group query "
                                   "processing failed\r\n");
                }
                break;
            case SNOOP_IP_FWD_ENTRY_TIMER:
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "IP forwarding entry timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "IP forwarding entry timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopIpGrpFwdEntry, EntryTimer);

                if (SnoopTmrIpFwdEntryTimerExpiry (pExpiredTmr->u4Instance,
                                                   ((UINT1 *) pExpiredTmr -
                                                    u4Offset)) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                                   "IP Forwarding entry timer "
                                   "processing failed\r\n");
                }
                break;

            case SNOOP_HOST_PRESENT_TIMER:
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "SNOOP Host present timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "SNOOP Host present timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopPortEntry, HostPresentTimer);

                if (SnoopTmrHostPresentTimerExpiry (pExpiredTmr->u4Instance,
                                                    ((UINT1 *) pExpiredTmr -
                                                     u4Offset))
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_TRC_CONTROL |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_TMR_NAME,
                                   "SNOOP Host present timer "
                                   "processing failed\r\n");
                }
                break;

            case SNOOP_SRC_LIST_CLEANUP_TIMER:
                if (SnoopUtilCleanUpSrcList (pExpiredTmr) == SNOOP_FAILURE)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_TRC_CONTROL |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_TMR_NAME,
                                   "SNOOP Source Cleanup timer expiry "
                                   "processing failed\r\n");
                }

                break;
            case SNOOP_OTHER_QUERIER_PRESENT_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "Other Querier Present Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Other Querier Present Timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopVlanEntry, OtherQPresentTimer);

                if (SnoopTmrOtherQuerierPresentTimerExpiry
                    (pExpiredTmr->u4Instance,
                     ((UINT1 *) pExpiredTmr - u4Offset)) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_QRY_NAME,
                                   "Other Querier Present timer expiry processing failed\r\n");
                }
                break;
            case SNOOP_IN_GROUP_SOURCE_TIMER:

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "Group Specific Source Information Timer Expired\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Group Specific Source Information Timer Expired\r\n");

                u4Offset = SNOOP_OFFSET (tSnoopGroupEntry, InGrpSrcTimer);

                if (SnoopTmrGroupSourceInfoTimerExpiry (pExpiredTmr->u4Instance,
                                                        ((UINT1 *) pExpiredTmr -
                                                         u4Offset)) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                   (pExpiredTmr->u4Instance),
                                   SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                                   "Group specific source information timer expiry processing failed\r\n");
                }
                break;

            default:
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG, "Invalid Timer type\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC, "Invalid Timer type\r\n");
        }

        i2TmrCnt++;
        if (i2TmrCnt >= SNOOP_MAX_TIMER_COUNTS)
        {
            if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_TMR_EXPIRY_EVENT)
                != OSIX_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pExpiredTmr->u4Instance),
                               SNOOP_TRC_CONTROL | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_TMR_NAME,
                               "Send Timer Expiry Evt Failed\r\n");
            }
            else
            {
                SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                SNOOP_CONTROL_TRC,
                                "Relinquishing after Processing %d Timers and "
                                "Posting Timer Expiry Event Succeeds\r\n",
                                i2TmrCnt);
            }
            break;
        }
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
               SNOOP_EXIT_DBG, "Exit :SnoopTmrExpiryHandler\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrQueryTimerExpiry                             */
/*                                                                           */
/* Description        : This function processes the query timer expiry.      */
/*                                                                           */
/* Input(s)           : pArg - Vlan Entry whocse query timer has expired     */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrQueryTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = pArg;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
    {
        if (SnoopVlanSendGeneralQuery (u4InstId, SNOOP_INVALID_PORT,
                                       pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_QRY | SNOOP_TRC_CONTROL |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_QRY_NAME,
                           "Unable to send General Query message on query "
                           "timer expiry\n");
            return SNOOP_FAILURE;
        }
        /* Update the First Query response
         * flag for the existing group records */
        SnoopUtilUpdateFirstQueryRespStatus (u4InstId);

        if (pSnoopVlanEntry->u1StartUpQCount <
            pSnoopVlanEntry->u1MaxStartUpQCount)
        {
            pSnoopVlanEntry->u1StartUpQCount++;
        }

        /* Restart the Query Timer */
        pSnoopVlanEntry->QueryTimer.u4Instance = u4InstId;
        pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;

        if (pSnoopVlanEntry->u1StartUpQCount >=
            pSnoopVlanEntry->u1MaxStartUpQCount)
        {
            /* 
             * If the Startup Query count is exhausted
             * we need to reset the interval to regular
             * Query Timer Interval
             * to the IGMP_QUERY_TIMER_ID
             */
            /* Restart the Query Timer */
            SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer,
                                pSnoopVlanEntry->u2QueryInterval);

            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                            SNOOP_TMR_DBG,
                            "Query timer restarted for %d seconds\r\n",
                            pSnoopVlanEntry->u2QueryInterval);
            SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                            SNOOP_CONTROL_TRC,
                            "Query timer restarted for %d seconds\r\n",
                            pSnoopVlanEntry->u2QueryInterval);
        }
        else
        {
            /* Restart the Query Timer */
            SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer,
                                pSnoopVlanEntry->u2StartUpQInterval);

            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                            SNOOP_TMR_DBG,
                            "Query timer restarted for %d seconds\r\n",
                            pSnoopVlanEntry->u2StartUpQInterval);
            SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                            SNOOP_CONTROL_TRC,
                            "Query timer restarted for %d seconds\r\n",
                            pSnoopVlanEntry->u2StartUpQInterval);
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrOtherQuerierPresentTimerExpiry               */
/*                                                                           */
/* Description        : This function processes the Other querier present    */
/*             timer expiry.                           */
/*                                                                           */
/* Input(s)           : pArg - Vlan Entry whose other querier present timer  */
/*                   has expired                        */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrOtherQuerierPresentTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = pArg;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;
    pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;
    pSnoopVlanEntry->u1StartUpQCount = 0;;
    if (SnoopVlanSendGeneralQuery (u4InstId, SNOOP_INVALID_PORT,
                                   pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_QRY_NAME,
                       "Unable to send General Query message on query "
                       "timer expiry\n");
        return SNOOP_FAILURE;
    }
    pSnoopVlanEntry->u1StartUpQCount++;
#ifdef L2RED_WANTED
    SnoopRedSyncStartUpQryCount (u4InstId, pSnoopVlanEntry);
#endif
    pSnoopVlanEntry->QueryTimer.u4Instance = u4InstId;
    pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;
    SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer,
                        pSnoopVlanEntry->u2StartUpQInterval);

    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                    "Query timer restarted for %d seconds\r\n",
                    pSnoopVlanEntry->u2StartUpQInterval);
    SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC, SNOOP_CONTROL_TRC,
                    "Query timer restarted for %d seconds\r\n",
                    pSnoopVlanEntry->u2StartUpQInterval);

    SnoopRedActiveSendQuerierSync (u4InstId, pSnoopVlanEntry->VlanId,
                                   pSnoopVlanEntry->u1Querier,
                                   pSnoopVlanEntry->u1AddressType,
                                   pSnoopVlanEntry->u4ElectedQuerier);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrGroupSourceInfoTimerExpiry                   */
/*                                                                           */
/* Description        : This function processes the group specific source    */
/*             information timer expiry.                            */
/*                                                                           */
/* Input(s)           : pArg - Group Entry whose group specific source       */
/*                   information has expired                  */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrGroupSourceInfoTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = pArg;
#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Port = 0;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        /* Delete the group node and free memblock */
        if (SnoopGrpDeleteGroupEntry (u4InstId, pSnoopGroupEntry->VlanId,
                                      pSnoopGroupEntry->GroupIpAddr) !=
            SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Deletion of Group record failed\r\n");
            return SNOOP_FAILURE;
        }

#ifdef NPAPI_WANTED

        u4GrpAddr = SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->GroupIpAddr.au1Addr);
        SNOOP_GET_MAC_FROM_IPV4 (u4GrpAddr, GroupMacAddr);
        if (VlanMiUpdateDynamicMcastInfo (u4InstId, GroupMacAddr,
                                          SNOOP_OUTER_VLAN (pSnoopGroupEntry->
                                                            VlanId), u4Port,
                                          VLAN_CLEAR_UNKNOWN_MCAST) !=
            VLAN_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DATA_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_PKT_NAME,
                           "Failed to remove Group entry in hardware\n");
            return SNOOP_FAILURE;
        }
#endif
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrV1RtrPortPurgeTimerExpiry                    */
/*                                                                           */
/* Description        : This function processes the V1 router port purge timer  */
/*                      expiry                                               */
/*                                                                           */
/* Input(s)           : pArg - pointer to the router port entry              */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrV1RtrPortPurgeTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopRtrPortEntry *pRtrPortEntry = pArg;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    INT4                i4RetVal = SNOOP_SUCCESS;

    pSnoopVlanEntry = (tSnoopVlanEntry *) pRtrPortEntry->pVlanEntry;
    pRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType = SNOOP_INVALID_TIMER_TYPE;

    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (pRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION2);

            /*   V2 router port purge timer is running so return */
            return i4RetVal;
        }
        else if (pRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType
                 != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION3);
            /*   V3 router port purge timer is running so return */
            return i4RetVal;
        }
    }
    /* Purge the router port */
    i4RetVal = SnoopHandleRtrPortPurge (u4InstId, pRtrPortEntry);
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : SnoopTmrV2RtrPortPurgeTimerExpiry                    */
/*                                                                           */
/* Description        : This function processes the V2 router port purge timer  */
/*                      expiry                                               */
/*                                                                           */
/* Input(s)           : pArg - pointer to the router port entry              */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrV2RtrPortPurgeTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopRtrPortEntry *pRtrPortEntry = pArg;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    INT4                i4RetVal = SNOOP_SUCCESS;

    pSnoopVlanEntry = (tSnoopVlanEntry *) pRtrPortEntry->pVlanEntry;
    pRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType = SNOOP_INVALID_TIMER_TYPE;
    if (pRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType
        != SNOOP_INVALID_TIMER_TYPE)
    {
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION1);
            /*   V1 router port purge timer is running so return */
            return i4RetVal;
        }
    }
    else if (pRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType
             != SNOOP_INVALID_TIMER_TYPE)
    {
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION3);
        }
        else if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry, SNOOP_MLD_VERSION2);
        }
        /*   V3 router port purge timer is running so return */
        return i4RetVal;
    }

    /* Purge the router port */
    i4RetVal = SnoopHandleRtrPortPurge (u4InstId, pRtrPortEntry);
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : SnoopTmrV3RtrPortPurgeTimerExpiry                    */
/*                                                                           */
/* Description        : This function processes the V3 router port purge timer  */
/*                      expiry                                               */
/*                                                                           */
/* Input(s)           : pArg - pointer to the router port entry              */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrV3RtrPortPurgeTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    tSnoopRtrPortEntry *pRtrPortEntry = pArg;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    INT4                i4RetVal = SNOOP_SUCCESS;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    pSnoopVlanEntry = (tSnoopVlanEntry *) pRtrPortEntry->pVlanEntry;

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    pRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType = SNOOP_INVALID_TIMER_TYPE;
    if (pRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType
        != SNOOP_INVALID_TIMER_TYPE)
    {
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION1);
            /*   V1 router port purge timer is running so return */
            return i4RetVal;
        }
    }
    else if (pRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType
             != SNOOP_INVALID_TIMER_TYPE)
    {
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           SNOOP_IGS_IGMP_VERSION2);
        }
        else if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry, SNOOP_MLD_VERSION1);
        }
        /*   V2 router port purge timer is running so return */
        return i4RetVal;
    }

    /* Purge the router port */
    i4RetVal = SnoopHandleRtrPortPurge (u4InstId, pRtrPortEntry);
    return i4RetVal;

}

/*****************************************************************************/
/* Function Name      : SnoopHandleRtrPortPurge                              */
/*                                                                           */
/* Description        : This function purges the dynamically learned         */
/*                      router ports                                         */
/*                                                                           */
/* Input(s)           : pArg - pointer to the router port entry              */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopHandleRtrPortPurge (UINT4 u4InstId, tSnoopRtrPortEntry * pRtrPortEntry)
{
    tSnoopIfPortList   *pSnpPortList = NULL;
    UINT1              *pBitmapVlan = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tMacAddr            NullMacAddr;
    UINT4               u4Port = 0;
    UINT1               u1QTransitionStatus = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    u4Port = pRtrPortEntry->u4Port;
    pSnoopVlanEntry = (tSnoopVlanEntry *) pRtrPortEntry->pVlanEntry;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    /* Check if the  port is present in the static router port bitmap,
     * if present dont do anything */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap,
                           bResult);

    if (bResult == OSIX_TRUE)
    {
        /* Check if the port is also learnt from the general query then delete the
         * from the QuerierRtrPortBitmap of the VLAN entry */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->QuerierRtrPortBitmap);
        }
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);
        /* Rtr port purging is not done as this is a static router port */

        if (((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4) &&
             (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer !=
              SNOOP_IGS_IGMP_VERSION3)) ||
            ((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6) &&
             (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer
              != SNOOP_MLD_VERSION2)))
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                           u1ConfigOperVer);
        }
        else
        {
            SnoopUtilUpdateRtrPortOperVer (pRtrPortEntry,
                                           pRtrPortEntry->u1ConfigOperVer);
        }

        /* Operating Version of the router port has changed */
        SnoopRedActiveSendRtrPortVerSync (u4InstId, pRtrPortEntry->u4Port,
                                          pRtrPortEntry->u1OperVer,
                                          pSnoopVlanEntry);

        /* Check if the QuerierRtrPortBitmap for this VLAN is not NULL. If not
         * null, then do return */
        if (SNOOP_MEM_CMP (pSnoopVlanEntry->QuerierRtrPortBitmap,
                           gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
        {
            pRtrPortEntry->u1IsDynamic = SNOOP_FALSE;
            /* Send a Router Port Sync Message to Standby Node. */
            SnoopRedActiveSendRtrPortListSync (u4InstId, pSnoopVlanEntry);

            return SNOOP_SUCCESS;
        }

        /* This is done for trace messages */
        gu4SnoopTrcInstId = u4InstId;
        gu4SnoopDbgInstId = u4InstId;

        /* Reset the Dynamic flag, since it was originally learnt as dynamic,
         *          * later it has been configured as static by user,
         *                   * so dynamic flag has to be made reset when port purge timer expires*/
        pRtrPortEntry->u1IsDynamic = SNOOP_FALSE;
        /* Send a Router Port Sync Message to Standby Node. */
        SnoopRedActiveSendRtrPortListSync (u4InstId, pSnoopVlanEntry);

        return SNOOP_SUCCESS;
    }

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /*Release the router port node and update the VLAN router port bitmap */
    SNOOP_SLL_DELETE (&pSnoopVlanEntry->RtrPortList, pRtrPortEntry);
    SnoopRedSyncBulkUpdNextEntry (u4InstId, pRtrPortEntry,
                                  SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE);

    /* Remove the router port entry from the global RBTree */
    RBTreeRem (gRtrPortTblRBRoot, pRtrPortEntry);

    SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4InstId, pRtrPortEntry);

    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);

    /* Check if the port is learnt from the general query then delete the
     * from the QuerierRtrPortBitmap of the VLAN entry */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                           bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                  pSnoopVlanEntry->QuerierRtrPortBitmap);
    }
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V3RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V3RtrPortBitmap);
    }

    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V2RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V2RtrPortBitmap);
    }

    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V1RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V1RtrPortBitmap);
    }

    /* Update the multicast forwarding table by removing the port */
    if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        if (SNOOP_SYSTEM_SPARSE_MODE (u4InstId) == SNOOP_ENABLE)
        {
            pSnpPortList =
                (tSnoopIfPortList *)
                FsUtilAllocBitList (sizeof (tSnoopIfPortList));

            if (pSnpPortList == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "SnoopHandleRtrPortPurge: "
                               "Error in allocating memory for pSnpPortList\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortList));

            pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pBitmapVlan == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "SnoopHandleRtrPortPurge: "
                               "Error in allocating memory for pBitmapVlan\r\n");
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));
            SNOOP_MEM_CPY (pBitmapVlan, &pSnoopVlanEntry->RtrPortBitmap,
                           SNOOP_PORT_LIST_SIZE);

            if (SnoopGetPhyPortBmp (u4InstId, pBitmapVlan, *pSnpPortList)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Unable to get the physical port bitmap from the virtual"
                               " port bitmap\r\n");
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                return SNOOP_FAILURE;
            }

            SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                           (pSnoopVlanEntry->VlanId),
                                           (tPortList *) (*pSnpPortList));
            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
            UtilPlstReleaseLocalPortList (pBitmapVlan);
        }

        /* Delete the port from MAC based multicast forwarding table */
        if (SnoopFwdUpdateRtrPortToMacFwdTable (u4InstId, u4Port,
                                                pSnoopVlanEntry->VlanId,
                                                pSnoopVlanEntry->u1AddressType,
                                                NullMacAddr, SNOOP_DEL_PORT)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME,
                           "Deletion of port from MAC forwarding entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* Forwarding Mode is IP based so update the IP forwarding table */
        if (SnoopFwdUpdateRtrPortToIpFwdTable (u4InstId, u4Port,
                                               pSnoopVlanEntry->VlanId,
                                               pSnoopVlanEntry->u1AddressType,
                                               SNOOP_DEL_PORT) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME,
                           "Deletion of port from IP forwarding entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* Send a Sync Message containing Updated Router Port list for the
     * VLAN.
     */
    SnoopRedActiveSendRtrPortListSync (u4InstId, pSnoopVlanEntry);
    /* Check if the QuerierRtrPortBitmapy for this VLAN is not  NULL 
     * then do return */
    if (SNOOP_MEM_CMP (pSnoopVlanEntry->QuerierRtrPortBitmap,
                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
    {
        return SNOOP_SUCCESS;
    }
    if (SNOOP_PROXY_STATUS (u4InstId, pSnoopVlanEntry->u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        /* If the switch is configured as a Querier/ Non Querier, 
         *            then copy the configured Querier information to the Vlan table */
        if ((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier) &&
            (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier !=
             pSnoopVlanEntry->u1Querier))
        {
            u1QTransitionStatus = SNOOP_TRUE;
            pSnoopVlanEntry->u1Querier =
                pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier;
        }

        if (u1QTransitionStatus == SNOOP_TRUE)
        {
            if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
            {
                SnoopHandleTransitionToQuerier (u4InstId, pSnoopVlanEntry);
            }
            else
            {
                SnoopHandleTransitionToNonQuerier (u4InstId, pSnoopVlanEntry);
            }
        }
        else
        {
            if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
            {
                /* Restart the Query Timer */
                pSnoopVlanEntry->QueryTimer.u4Instance = u4InstId;
                pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;

                SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer,
                                    pSnoopVlanEntry->u2StartUpQInterval);
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                                SNOOP_TMR_DBG,
                                "Query timer restarted for %d seconds\r\n",
                                pSnoopVlanEntry->u2StartUpQInterval);
                SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                SNOOP_CONTROL_TRC,
                                "Query timer restarted for %d seconds\r\n",
                                pSnoopVlanEntry->u2StartUpQInterval);
            }
        }

    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleSSMPortPurge                              */
/*                                                                           */
/* Description        : This function processes the SSM Port Purging         */
/*                                                                           */
/* Input(s)           : u4InstId  - Instance Identifier                      */
/*                      pSnoopSSMPortEntry - SSM port entry to be deleted    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopHandleSSMPortPurge (UINT4 u4InstId, tSnoopPortEntry * pSnoopSSMPortEntry)
{
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntryNode = NULL;
    tIPvXAddr           NullAddr;
    tSnoopTag           VlanId;
    UINT4               u4Port = 0;
    UINT4               u4PhyPort = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1AddressType = 0;
    UINT1               u1IsLastGroupEntry = SNOOP_FALSE;
    UINT1               u1Value = 0;
    UINT1               u1PortEntryFound = SNOOP_FALSE;
    UINT1               u1Snooping = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET ((UINT1 *) &NullAddr, 0, sizeof (tIPvXAddr));
    u4Port = pSnoopSSMPortEntry->u4Port;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    pSnoopGroupEntry = (tSnoopGroupEntry *) pSnoopSSMPortEntry->pGroupEntry;
    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4InstId, (u1AddressType - 1))
        == SNOOP_FALSE)
    {
        u1Snooping = SNOOP_TRUE;
    }

    /* Delete all the Host information for SSM port entry */
    if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
    {
        while ((pSnoopHostEntry = (tSnoopHostEntry *)
                SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList)) != NULL)
        {
            /* Remove the host from the Host Information RBTree */
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4InstId)->HostEntry,
                          (tRBElem *) pSnoopHostEntry);
            SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList, pSnoopHostEntry);

            SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4InstId, pSnoopHostEntry);
        }
    }

    /*Release the port node from the SSMPortList */
    if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType
        != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
    }

    /*  Decrement the threshold current Count by the number
     * of bits set in the Include source bitmap */
    u4PhyPort = SNOOP_GET_IFINDEX (u4InstId, u4Port);

    /* Get the port entry; If port entry is absent, no port-based action will
     * be taken for the packet. */
    if (SnoopGetPortCfgEntry (u4InstId, u4PhyPort, VlanId, u1AddressType,
                              &pSnoopPortCfgEntry) == SNOOP_SUCCESS)
    {
        u1PortEntryFound = SNOOP_TRUE;
        pSnoopExtPortCfgEntryNode = pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
        if (pSnoopExtPortCfgEntryNode != NULL)
        {
            /* Store the ports, Old include bitmap */
            if ((pSnoopExtPortCfgEntryNode->u1SnoopPortMaxLimitType
                 == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
                (SNOOP_INSTANCE_INFO (u4InstId)->
                 SnoopInfo[u1AddressType - 1].u1FilterStatus != SNOOP_DISABLE))
            {
                for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE;
                     u2ByteIndex++)
                {
                    u1Value =
                        pSnoopSSMPortEntry->pSourceBmp->InclSrcBmp[u2ByteIndex];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                          && (u1Value != 0)); u2BitIndex++)
                    {
                        if ((u1Value & SNOOP_BIT8) != 0)
                        {
                            SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntryNode);
                        }
                        u1Value = (UINT1) (u1Value << 1);
                    }
                }
            }
        }
    }

    SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList, pSnoopSSMPortEntry);

    SNOOP_SSM_PORT_FREE_MEMBLK (u4InstId, pSnoopSSMPortEntry);

    /* Before updating the consolidated port bitmap and forwarding
     * table verify once that there are no ASM receivers on the port 
     */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->ASMPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        /* ASM host is present on the port. So dont update the consolidated 
         * port bitmap. */
        SnoopGrpConsolidateSrcInfo (u4InstId, 0, 0, gNullSrcBmp,
                                    gNullSrcBmp, pSnoopGroupEntry, SNOOP_TRUE);
        return SNOOP_SUCCESS;
    }

    if (pSnoopExtPortCfgEntryNode != NULL)
    {

        /* decrease the current threshold by 1 */
        if ((pSnoopExtPortCfgEntryNode->u1SnoopPortMaxLimitType
             == SNOOP_PORT_LMT_TYPE_GROUPS) &&
            (SNOOP_INSTANCE_INFO (u4InstId)->
             SnoopInfo[u1AddressType - 1].u1FilterStatus != SNOOP_DISABLE))
        {
            if (u1PortEntryFound == SNOOP_TRUE)
                /* If the limit type set is for groups, decrement
                 * the current limit count */
                SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntryNode);
        }
    }

    /* Update the Group entry port bitmap */
    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

    if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry, NullAddr,
                                  u4Port, gNullPortBitMap,
                                  SNOOP_DEL_PORT) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_FWD_NAME,
                       "Updation of IP forwarding table on reception "
                       "of leave message failed\r\n");
    }

    pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;
    if (pSnoopConsGroupEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME, "No consolidated group"
                       "entry present for the group\r\n");
        return SNOOP_FAILURE;
    }

    /* The consolidated report / leave messages will be sent to router ports 
     * for proxy/proxy-reporting mode in the following scenarios:
     *   1 When there are other ports present in the group entry,
     *      -  We have to send reports only if a filter mode change or
     *         source list change occurs in the consolidated group entry. 
     *   2 This is the last port in the group entry but there are other 
     *     group entries belonging to the consolidated entry
     *      -  We have to send reports only if a filter mode change or
     *         source list change occurs in the consolidated group entry. 
     *   3 If this the last group in the consolidated group entry
     *      -  A leave message needs to be send 
     */
    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                       SNOOP_PORT_LIST_SIZE) != 0)
    {
        SnoopGrpConsolidateSrcInfo (u4InstId, 0, 0, gNullSrcBmp,
                                    gNullSrcBmp, pSnoopGroupEntry, SNOOP_FALSE);
        SnoopGrpCheckAndSendConsReport (u4InstId, pSnoopConsGroupEntry);
        return SNOOP_SUCCESS;
    }

    /* Do not send Leave message to the Router ports if there is
     * any receiver for the group */
    if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 1)
    {
        u1IsLastGroupEntry = SNOOP_TRUE;
    }

    if (u1IsLastGroupEntry == SNOOP_TRUE)
    {
        SnoopGrpConsolidateSrcInfo (u4InstId, 0, 0, gNullSrcBmp,
                                    gNullSrcBmp, pSnoopGroupEntry, SNOOP_TRUE);
        /* Form and Send Leave message */
        if (u1Snooping != SNOOP_TRUE)
        {
#ifdef IGS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                if (IgsEncodeAndSendLeaveMsg (u4InstId, VlanId,
                                              pSnoopConsGroupEntry)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4InstId),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                        "Unable to send Leave"
                                        "message for the Group %s\r\n",
                                        SnoopPrintIPvxAddress
                                        (pSnoopGroupEntry->GroupIpAddr));
                    return SNOOP_FAILURE;
                }
            }
#endif
#ifdef MLDS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                if (MldsEncodeAndSendDoneMsg (u4InstId, VlanId,
                                              pSnoopConsGroupEntry)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4InstId),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                    "Unable to send Done"
                                    "message for the Group %s\r\n",
                                    SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                           GroupIpAddr));
                    return SNOOP_FAILURE;
                }
            }
#endif
        }
    }

    if (u1IsLastGroupEntry == SNOOP_TRUE)
    {
        SnoopUtilDeleteIpFwdEntriesForOuterVlan (u4InstId, pSnoopGroupEntry);
    }

    /* Delete the group node and free memblock */
    if (SnoopGrpDeleteGroupEntry (u4InstId, VlanId,
                                  pSnoopGroupEntry->GroupIpAddr)
        != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME, "Deletion of Group record failed\r\n");
        return SNOOP_FAILURE;
    }

    if (u1IsLastGroupEntry == SNOOP_FALSE)
    {
        SnoopGrpCheckAndSendConsReport (u4InstId, pSnoopConsGroupEntry);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrASMPortPurgeTimerExpiry                      */
/*                                                                           */
/* Description        : This function processes the ASMPortList port purge   */
/*                      timer expiry.                                        */
/*                                                                           */
/* Input(s)           : pArg - pointer to ASM Port Entry                     */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrASMPortPurgeTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    UINT1              *pTempPortBitMap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = pArg;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntryNode = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tIPvXAddr           NullAddr;
    tIPvXAddr           GrpAddr;
    tSnoopSrcBmp        SnoopExclBmp;
    tSnoopTag           VlanId;
    INT4                i4Result = SNOOP_SUCCESS;
    UINT4               u4PhyPort = 0;
    UINT4               u4Port = 0;
    UINT4               u4LearntPort = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    BOOL1               bRes = OSIX_FALSE;
    UINT1               u1PortFlag = 0;
    UINT1               u1SSMReceiver = SNOOP_FALSE;
    UINT1               u1AddressType = 0;
    UINT1               u1IsLastGroupEntry = SNOOP_FALSE;
    UINT1               u1Snooping = SNOOP_FALSE;

#ifdef ICCH_WANTED
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    UINT1              *pPortBitmap = NULL;
    UINT4               u4PhyMcLagPort = 0;
    UINT4               u4GrpAddr = 0;
    UINT1               u1McLagInterface = OSIX_FALSE;
    UINT2               u2AdminKey = 0;
    BOOL1               bIcclInterface = OSIX_FALSE;
#endif

#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

#ifdef ICCH_WANTED
    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
#endif

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET ((UINT1 *) &NullAddr, 0, sizeof (tIPvXAddr));

    u4Port = pSnoopASMPortEntry->u4Port;
#ifdef ICCH_WANTED
    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        u4PhyMcLagPort = SNOOP_GET_IFINDEX (u4InstId, u4Port);
        SnoopLaIsMclagInterface (u4PhyMcLagPort, &u1McLagInterface);
        bIcclInterface = SnoopIcchIsIcclInterface (u4PhyMcLagPort);
    }
#endif
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;
    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    pSnoopGroupEntry = (tSnoopGroupEntry *) pSnoopASMPortEntry->pGroupEntry;
    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;
    IPVX_ADDR_COPY (&GrpAddr, &(pSnoopGroupEntry->GroupIpAddr));

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SnoopVlanGetVlanEntry (u4InstId, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME, "Unable to get Vlan record "
                       "for the given instance and the VlanId \r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4InstId, u1AddressType - 1)
        == SNOOP_FALSE)
    {
        u1Snooping = SNOOP_TRUE;
    }

#ifdef ICCH_WANTED
    if ((u1McLagInterface == OSIX_TRUE)
        && (pSnoopGroupEntry->u1McLagLeaveReceived == SNOOP_FALSE))
    {
        pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_TRUE;
        SnoopLaUpdateActorPortChannelAdminKey (u4PhyMcLagPort, &u2AdminKey);

        SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, pSnoopGroupEntry->VlanId,
                       sizeof (tSnoopTag));
        SnoopIcchSyncLeaveMessage (u4InstId, GrpAddr, &SnoopIcchInfo,
                                   u2AdminKey);
    }

    if (bIcclInterface == OSIX_TRUE)
    {
#ifdef IGS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            u4GrpAddr = SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr);
            i4Result = IgsEncodeQuery (u4InstId, u4GrpAddr, SNOOP_GRP_QUERY,
                                       pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                       u1ConfigOperVer, &pOutBuf,
                                       pSnoopVlanEntry);
        }
#endif
#ifdef MLDS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            i4Result =
                MldsEncodeQuery (u4InstId, GrpAddr.au1Addr, SNOOP_GRP_QUERY,
                                 SNOOP_MLD_VERSION2, &pOutBuf, pSnoopVlanEntry);
        }
#endif

        if (i4Result != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_PKT_NAME,
                           "Group Specific Query construct and send - failed\r\n");
            CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
            return SNOOP_FAILURE;
        }

        pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pPortBitmap == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Error in allocating memory for pPortBitmap\r\n");
            CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

        SNOOP_ADD_TO_PORT_LIST (u4Port, pPortBitmap);
        SnoopVlanForwardPacket (u4InstId, VlanId,
                                pSnoopVlanEntry->u1AddressType,
                                SNOOP_INVALID_PORT, VLAN_FORWARD_SPECIFIC,
                                pOutBuf, pPortBitmap, SNOOP_GRP_QUERY_SENT);
        UtilPlstReleaseLocalPortList (pPortBitmap);

    }
#endif

    /*Stop the Host present timer */
    if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType
        != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP, SNOOP_GRP_DBG,
               "ASM port expired deleting all the hosts attached\r\n");

    /* Delete all the hosts attached to the port */
    if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
    {
        while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList)) != NULL)
        {
            pRBElem = (tRBElem *) pSnoopASMHostEntry;
            /* Remove the host from the Host Information RBTree */
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4InstId)->HostEntry, pRBElem);
            SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                              pSnoopASMHostEntry);

            SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4InstId, pSnoopASMHostEntry);
        }
    }

    /* Release the port node from the ASMPortList */
    SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList, pSnoopASMPortEntry);
    SNOOP_ASM_PORT_FREE_MEMBLK (u4InstId, pSnoopASMPortEntry);

    /* Update the Group entry for ASM port bitmap */
    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->ASMPortBitmap);

    /* Before updating the consolidated port bitmap and forwarding
     * table verify there is any SSM receivers present for the port 
     */
    if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_IP)
    {
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopSSMPortEntry, tSnoopPortEntry *)
        {
            if (pSnoopSSMPortEntry->u4Port == u4Port)
            {
                u1SSMReceiver = SNOOP_TRUE;
                break;
            }
        }
    }
    if (u1SSMReceiver == SNOOP_FALSE)
    {
        /* Get the port configuration. If the port configuration says to drop
         * reports, drop the report*/
        u4PhyPort = SNOOP_GET_IFINDEX (u4InstId, u4Port);

        /* Get the port entry; If port entry is absent, no port-based action
         * will be taken for the packet.*/
        if (SnoopGetPortCfgEntry (u4InstId, u4PhyPort, pSnoopGroupEntry->VlanId,
                                  pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                  &pSnoopPortCfgEntry) == SNOOP_SUCCESS)
        {
            pSnoopExtPortCfgEntryNode =
                pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
            if (pSnoopExtPortCfgEntryNode != NULL)
            {
                if ((pSnoopExtPortCfgEntryNode->u1SnoopPortMaxLimitType
                     == SNOOP_PORT_LMT_TYPE_GROUPS) &&
                    (SNOOP_INSTANCE_INFO (u4InstId)->
                     SnoopInfo[u1AddressType - 1].
                     u1FilterStatus != SNOOP_DISABLE))
                {
                    /* If the limit type set is for groups, decrement
                     * the current limit count */
                    SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntryNode);
                }
            }
        }

        /* Update the Group entry port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

#ifdef L2RED_WANTED
        /* Update the Group entry V1 port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->V1PortBitmap);
#endif

        if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            SnoopRedActiveSendGrpInfoSync (u4InstId, pSnoopGroupEntry);

            if (SnoopFwdUpdateMacFwdTable (u4InstId, pSnoopGroupEntry,
                                           u4Port, SNOOP_DEL_PORT, 0)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                           "on ASM port purge timer expiry failed\r\n");
            }
        }
        else
        {
            if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry,
                                          NullAddr, u4Port, gNullPortBitMap,
                                          SNOOP_DEL_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of IP forwarding table "
                           "on reception of leave messaga failed\r\n");
            }
        }
    }

    /* Do not send Leave message to the Router ports if there is
     * a receiver present for the group */
    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                       SNOOP_PORT_LIST_SIZE) != 0)
    {
        if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_IP)
        {
            /* Check if any ASM receivers are there if present nothing needs 
             * to be done */
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap, gNullPortBitMap,
                               SNOOP_PORT_LIST_SIZE) == 0)
            {
                /* If the ASM port bitmap has become NULL set the exclude source
                 * bitmap for the group to default value and consolidate the 
                 * group source bitmaps */
                SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                               SNOOP_SRC_LIST_SIZE);

                SnoopGrpConsolidateSrcInfo (u4InstId, 0, 0, gNullSrcBmp,
                                            gNullSrcBmp, pSnoopGroupEntry,
                                            SNOOP_TRUE);

                /* Check for any filter mode change has occurred so just update
                 * router with the filter mode change record */
                if (SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                                   SNOOP_SRC_LIST_SIZE) != 0)
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                }
                else
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
                }

                /* After SnoopGrpConsolidateSrcInfo if the u1ASMHostPresent of 
                 * consolidated group is SNOOP_TRUE then there is some more 
                 * ASM host is present for this consolidated gtroup. So 
                 * nothing needs to be done. */
                if ((pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent
                     != SNOOP_FALSE) || (u1Snooping == SNOOP_TRUE))
                {
                    return SNOOP_SUCCESS;
                }
                /* Send a filter mode change record */
#ifdef IGS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (IgsEncodeQueryResponse
                        (u4InstId, pSnoopVlanEntry,
                         pSnoopGroupEntry->pConsGroupEntry, 0) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_PKT_NAME,
                                       "Unable to send response to query\r\n");
                        return SNOOP_FAILURE;
                    }
                }
#endif
#ifdef MLDS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    if (MldsEncodeQueryResponse
                        (u4InstId, pSnoopVlanEntry,
                         pSnoopGroupEntry->pConsGroupEntry, 0) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_PKT_NAME,
                                       "Unable to send response to query\r\n");
                        return SNOOP_FAILURE;
                    }
                }
#endif
            }
            return SNOOP_SUCCESS;
        }
        else if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            if (((SNOOP_INSTANCE_INFO (u4InstId)->
                  SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                  u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS)) &&
                ((SNOOP_INSTANCE_INFO (u4InstId)->
                  SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                  u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS)))
            {
                pTempPortBitMap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pTempPortBitMap == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "SnoopTmrASMPortPurgeTimerExpiry: "
                                   "Error in allocating memory for pTempPortBitMap\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

                pSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pSnoopRtrPortBmp == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "SnoopTmrASMPortPurgeTimerExpiry: "
                                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                    UtilPlstReleaseLocalPortList (pTempPortBitMap);
                    return SNOOP_FAILURE;
                }
                MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                SNOOP_MEM_CPY (pTempPortBitMap, pSnoopGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SNOOP_DEL_FROM_PORT_LIST (u4Port, pTempPortBitMap);
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4InstId,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);

                for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE;
                     u2BytIndex++)
                {
                    if (u2BytIndex >= SNOOP_PORT_LIST_SIZE)
                    {
                        continue;
                    }

                    u1PortFlag = pTempPortBitMap[u2BytIndex];
                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {

                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4LearntPort =
                                (UINT4) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                   pSnoopRtrPortBmp, bRes);
                            if (bRes == OSIX_TRUE)
                            {
                                u4RtrPortCount++;
                            }
                            else
                            {
                                u4LrtPortCount++;
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
                UtilPlstReleaseLocalPortList (pTempPortBitMap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

                if ((u4LrtPortCount > 0) || (u4RtrPortCount > 1))
                {
                    return SNOOP_SUCCESS;
                }
            }
            else
            {
                return SNOOP_SUCCESS;
            }
        }
    }

    /* If this is the only group present in the consolidated group
     * entry then send a Leave message to the router ports */
    pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;

    /* No Need to check for NULL, Since the consolidated group entry 
     * will present when group entry is there */
    if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 1)
    {
        u1IsLastGroupEntry = SNOOP_TRUE;
    }

    if (u1IsLastGroupEntry == SNOOP_TRUE && u1Snooping == SNOOP_FALSE)
    {
        /* Form and Send Leave message if there is no receivers present in the
         * group */
#ifdef IGS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (IgsEncodeAndSendLeaveMsg (u4InstId, VlanId,
                                          pSnoopConsGroupEntry)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4InstId),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                    "Unable to send Leave message for the Group %s"
                                    "\r\n",
                                    SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                           GroupIpAddr));
                return SNOOP_FAILURE;
            }
        }
#endif
#ifdef MLDS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (MldsEncodeAndSendDoneMsg (u4InstId, VlanId,
                                          pSnoopConsGroupEntry)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4InstId),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                    "Unable to send Leave message for the Group %s"
                                    "\r\n",
                                    SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                           GroupIpAddr));
                return SNOOP_FAILURE;
            }
        }
#endif
    }

    if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType == SNOOP_INVALID_TIMER_TYPE)
    {
        if ((SNOOP_SYSTEM_SPARSE_MODE (u4InstId) == SNOOP_ENABLE) &&
            (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_MAC))
        {
            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u4InstId, VlanId,
                                          pSnoopGroupEntry->GroupIpAddr)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Deletion of Group record failed\r\n");
                return SNOOP_FAILURE;
            }
        }
    }
    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        /*If the portlist is NULL,remove the entry from forwarding
         *table also if control plane driven mode is enabled*/
        if (SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4InstId) == SNOOP_ENABLE)
        {
            if ((SnoopFwdGetIpForwardingEntry
                 (u4InstId, pSnoopGroupEntry->VlanId,
                  pSnoopGroupEntry->GroupIpAddr, NullAddr,
                  &pSnoopIpGrpFwdEntry) == SNOOP_SUCCESS))
            {
                if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry,
                                              NullAddr, u4Port, gNullPortBitMap,
                                              SNOOP_DELETE_FWD_ENTRY) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4InstId),
                                        SNOOP_DBG_ALL_FAILURE,
                                        SNOOP_FWD_TRC,
                                        "Deletion of forwarding table entry "
                                        "failed for group %s Vlan %d\r\n",
                                        SnoopPrintIPvxAddress
                                        (pSnoopGroupEntry->GroupIpAddr),
                                        SNOOP_OUTER_VLAN (VlanId));
                }
            }
        }
        if (SnoopGrpDeleteGroupEntry (u4InstId, VlanId,
                                      pSnoopGroupEntry->GroupIpAddr)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Deletion of Group record failed\r\n");
            return SNOOP_FAILURE;
        }
    }

    if (u1IsLastGroupEntry == SNOOP_FALSE)
    {
        if (SnoopGrpGetConsGroupEntry (u4InstId, VlanId, GrpAddr,
                                       &pSnoopConsGroupEntry) == SNOOP_FAILURE)
        {
            return SNOOP_SUCCESS;
        }

        SnoopGrpConsolidateGroupInfo (u4InstId, pSnoopConsGroupEntry);

        if ((SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_IP) &&
            (u1Snooping == SNOOP_FALSE))
        {
            /* If there is some ASM host present then there is no 
             * change in the consolidated bitmap. */
            if (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_FALSE)
            {
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    i4Result = IgsEncodeQueryResponse
                        (u4InstId, pSnoopVlanEntry, pSnoopConsGroupEntry, 0);
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Result = MldsEncodeQueryResponse
                        (u4InstId, pSnoopVlanEntry, pSnoopConsGroupEntry, 0);
                }
#endif
                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC, "Unable to send the updation"
                               " of consolidated bitmap \r\n");
                }
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrGrpQueryTimerExpiry                          */
/*                                                                           */
/* Description        : This function processes the Group Specific Query     */
/*                      timer expiry.                                        */
/*                                                                           */
/* Input(s)           : pArg - pointer to ASM Port Entry                     */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrGrpQueryTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    UINT1              *pPortBitMap = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = pArg;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopTag           VlanId;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1GrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1AddressType = 0;
    UINT4               u4TimerValue = 0;

    SNOOP_MEM_SET (au1GrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    pSnoopGroupEntry = (tSnoopGroupEntry *) pSnoopASMPortEntry->pGroupEntry;
    SNOOP_MEM_CPY (au1GrpIpAddr, pSnoopGroupEntry->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SnoopVlanGetVlanEntry (u4InstId, VlanId,
                               u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME,
                       "Unable to get Vlan record for the given instance "
                       "and the VlanId \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u4TimerValue = pSnoopVlanEntry->u4RobustnessValue;
    }
    else if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        u4TimerValue = SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddressType -
                                                                 1].
            u1RetryCount;
    }

    /* Check if the OperVersion of the VLAN is version v1, then do not
     * send the leave message to the router ports and start the port
     * purge interval for the port on which the Group Query timer expired */
    if (pSnoopASMPortEntry->u1RetryCount < u4TimerValue)
    {
        /* Retry count has not reached the maximum configured value
         * so just send a group specific query again on to the port
         * on which leave was received and restart the group query 
         * timer for the group query interval and increment the 
         * ports retry count */

#ifdef IGS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (IgsEncodeQuery (u4InstId, SNOOP_PTR_FETCH_4 (au1GrpIpAddr),
                                SNOOP_GRP_QUERY,
                                pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                u1ConfigOperVer, &pBuf,
                                pSnoopVlanEntry) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_DBG_PKT | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_PKT_NAME,
                               "Group Specific Query construct and send - failed\r\n");
                return SNOOP_FAILURE;
            }
        }
#endif
#ifdef MLDS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (MldsEncodeQuery (u4InstId, au1GrpIpAddr,
                                 SNOOP_GRP_QUERY,
                                 SNOOP_MLD_VERSION2, &pBuf, pSnoopVlanEntry)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_DBG_PKT | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_PKT_NAME,
                               "Group Specific Query construct and send - failed\r\n");
                return SNOOP_FAILURE;
            }
        }
#endif
        pPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pPortBitMap == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "SnoopTmrGrpQueryTimerExpiry: "
                           "Error in allocating memory for pPortBitMap\r\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return SNOOP_FAILURE;
        }
        MEMSET (pPortBitMap, 0, sizeof (tSnoopPortBmp));

        SNOOP_ADD_TO_PORT_LIST (pSnoopASMPortEntry->u4Port, pPortBitMap);

        if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
        {
            SnoopVlanTagFrame (pBuf, SNOOP_INNER_VLAN (VlanId),
                               SNOOP_VLAN_HIGHEST_PRIORITY);
        }

        if ((SNOOP_INSTANCE_ENH_MODE (u4InstId) == SNOOP_ENABLE))
        {
            SnoopCfaGetIfInfo (pSnoopASMPortEntry->u4Port, &CfaIfInfo);

            if (CfaIfInfo.u1BrgPortType == CFA_CUSTOMER_EDGE_PORT)
            {
                /* Outgoing port is CEP. The tag will be added
                   in SnoopMiVlanSnoopFwdPacketOnPortList */
                SnoopVlanUntagFrame (pBuf);
            }
        }

        SnoopVlanForwardPacket (u4InstId, VlanId, u1AddressType,
                                SNOOP_INVALID_PORT, VLAN_FORWARD_SPECIFIC,
                                pBuf, pPortBitMap, SNOOP_GRP_QUERY_SENT);
        UtilPlstReleaseLocalPortList (pPortBitMap);

        /* restart the Group Query timer */
        pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u4Instance = u4InstId;
        pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType =
            SNOOP_GRP_QUERY_TIMER;

        SnoopTmrStartTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer,
                            SNOOP_INSTANCE_INFO (u4InstId)->
                            SnoopInfo[u1AddressType - 1].u4GrpQueryInt);
        pSnoopASMPortEntry->u1RetryCount++;
        return SNOOP_SUCCESS;
    }
    else
    {
        /* Retry count has reached the maximum value for the port so the
           given the receiver enough time to register for reception. So 
           process this is similar to port purge timer expiry */

        if (SnoopTmrASMPortPurgeTimerExpiry (u4InstId, pSnoopASMPortEntry)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_TMR | SNOOP_TRC_CONTROL |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_TMR_NAME,
                           "Processing Leave Timer expiry - failed\r\n");
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrIpFwdEntryTimerExpiry                        */
/*                                                                           */
/* Description        : This function processes the IP forward entry         */
/*                      timer expiry.                                        */
/*                                                                           */
/* Input(s)           : pArg - pointer to ASM Port Entry                     */
/*                    : u4InstId - Instance Identifier                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopTmrIpFwdEntryTimerExpiry (UINT4 u4InstId, VOID *pArg)
{
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = pArg;
    tSnoopIpGrpFwdEntry *pIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pTempIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tIPvXAddr           GrpIpAddr;
    tIPvXAddr           SrcIpAddr;
#ifdef NPAPI_WANTED
    UINT4               u4HitBitStatus = FALSE;
    UINT1               u1AddressType = 0;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopConsolidatedGroupEntry SnoopConsGroupEntry;
#ifdef IGS_WANTED
    tSnoopIfPortList   *pSnpPortList = NULL;
    tPortList          *pPortList = NULL;
    tPortList          *pUntagPortList = NULL;
    tIgsHwIpFwdInfo    *pIgsHwIpFwdInfo = NULL;
#endif
#ifdef PIM_WANTED
    UINT4               u4VlanIfIndex = 0;
#endif
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);

    if (pSnoopIpGrpFwdEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME,
                       "Input validation failed at SnoopTmrIpFwdEntryTimerExpiry \r\n");
        return SNOOP_FAILURE;
    }
#ifdef NPAPI_WANTED
    u1AddressType = pSnoopIpGrpFwdEntry->u1AddressType;
    SNOOP_MEM_SET (&SnoopConsGroupEntry, 0,
                   sizeof (tSnoopConsolidatedGroupEntry));
#endif

    if (SnoopGrpGetGroupEntry (u4InstId, pSnoopIpGrpFwdEntry->VlanId,
                               pSnoopIpGrpFwdEntry->GrpIpAddr,
                               &pSnoopGroupEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                   "Unable to get group record for the given instance "
                   "and the VlanId \r\n");
    }
#ifdef NPAPI_WANTED
#ifdef IGS_WANTED
    if (SNOOP_HW_IP_FW_INFO_POOL_ID == 0)
    {
        return SNOOP_FAILURE;
    }
    SNOOP_HW_IP_ALLOC_MEMBLK (pIgsHwIpFwdInfo);
    if (pIgsHwIpFwdInfo == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pIgsHwIpFwdInfo, 0, sizeof (tIgsHwIpFwdInfo));

    pIgsHwIpFwdInfo->OuterVlanId =
        SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
    pIgsHwIpFwdInfo->InnerVlanId =
        SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
    pIgsHwIpFwdInfo->u4GrpAddr =
        SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr);
    pIgsHwIpFwdInfo->u4SrcAddr =
        SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr);

    if (pSnoopIpGrpFwdEntry->u1AddPortStatus == HW_ADD_PORT_FAIL)
    {
        pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;

        pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                           "Error in Allocating memory for bitlist \r\n");
            SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
            return SNOOP_FAILURE;
        }

        pUntagPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pUntagPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                           "Error in Allocating memory for bitlist \r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
            return SNOOP_FAILURE;
        }

        pSnpPortList =
            (tSnoopIfPortList *) FsUtilAllocBitList (sizeof (tSnoopIfPortList));

        if (pSnpPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                           "Error in Allocating memory for bitlist \r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
            SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
            return SNOOP_FAILURE;
        }

        SNOOP_MEM_SET (pPortList, 0, BRG_PORT_LIST_SIZE);
        SNOOP_MEM_SET (pUntagPortList, 0, BRG_PORT_LIST_SIZE);
        SNOOP_MEM_SET (pSnpPortList, 0, SNOOP_IF_PORT_LIST_SIZE);

        if (SnoopGetIfPortBmp (u4InstId, pSnoopIpGrpFwdEntry->PortBitmap,
                               *pSnpPortList) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Unable to get the physical port bitmap from the virtual"
                           " port bitmap\r\n");
            FsUtilReleaseBitList ((UINT1 *) pPortList);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
            SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
            return SNOOP_FAILURE;
        }

        SNOOP_MEM_CPY (pPortList, pSnpPortList, sizeof (tSnoopIfPortList));

#ifdef PIM_WANTED
        u4VlanIfIndex = CfaGetVlanInterfaceIndex (pIgsHwIpFwdInfo->OuterVlanId);
        /*Check if PIM is enabled using VlanID */
        if (SPimPortCheckDROnInterface (u4VlanIfIndex) != OSIX_SUCCESS)
        {
            if (FsMiNpUpdateIpmcFwdEntries
                (u4InstId, SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                 SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                 SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                 *pPortList, *pUntagPortList, SNOOP_HW_ADD_PORT) != FNP_SUCCESS)
            {
                SNOOP_DBG_ARG3 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_FWD_TRC,
                                "V4 Multicast forward entry port additon in hardware for"
                                " Vlan %d Source %x, Group %x failed\r\n",
                                SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                                SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->
                                                   SrcIpAddr.au1Addr),
                                SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->
                                                   GrpIpAddr.au1Addr));

                SnoopHandleAddPortFailure (pSnoopIpGrpFwdEntry->VlanId,
                                           pSnoopIpGrpFwdEntry->GrpIpAddr,
                                           pSnoopIpGrpFwdEntry->SrcIpAddr,
                                           u4InstId);

            }
        }
#endif

        SNOOP_MEM_CPY (pIgsHwIpFwdInfo->PortList,
                       pPortList, sizeof (tPortList));
        SNOOP_MEM_CPY (pIgsHwIpFwdInfo->UntagPortList,
                       pUntagPortList, sizeof (tPortList));

        if (IgsFsMiIgsHwUpdateIpmcEntry (u4InstId, pIgsHwIpFwdInfo,
                                         SNOOP_HW_ADD_PORT) != FNP_SUCCESS)

        {
            SNOOP_DBG_ARG4 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_FWD_TRC,
                            "V4 Multicast forward entry port additon in hardware for"
                            " OuterVlan %d Source %x, Group %x InnerVlan %s "
                            "failed\r\n",
                            SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                            SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.
                                               au1Addr),
                            SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.
                                               au1Addr),
                            SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId));

            SnoopHandleAddPortFailure (pSnoopIpGrpFwdEntry->VlanId,
                                       pSnoopIpGrpFwdEntry->GrpIpAddr,
                                       pSnoopIpGrpFwdEntry->SrcIpAddr,
                                       u4InstId);

        }

        pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4InstId;
        pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType = SNOOP_IP_FWD_ENTRY_TIMER;

        /* If Add Port failed for hardware then no need to restart the timer
         * as Hardware port addition failure routines handle fail case.
         */
        if (pSnoopIpGrpFwdEntry->u1AddPortStatus != HW_ADD_PORT_FAIL)
        {
            if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                                    SNOOP_IP_FWD_ENTRY_TIMER_VAL) !=
                SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Unable to restart IP forwarding entry timer \r\n");
                FsUtilReleaseBitList ((UINT1 *) pPortList);
                FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
                return SNOOP_FAILURE;

            }
        }

        FsUtilReleaseBitList ((UINT1 *) pPortList);
        FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
        FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
        SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
        return SNOOP_SUCCESS;
    }

    else
    {
#endif
#endif
#ifdef NPAPI_WANTED
#ifdef IGS_WANTED                /* MLDS_CHANGES */
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (IgsFsMiNpGetFwdEntryHitBitStatus
                (u4InstId, SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                 SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                 SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr)) ==
                FNP_SUCCESS)
            {
                u4HitBitStatus = TRUE;
            }

            pIgsHwIpFwdInfo->OuterVlanId =
                SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
            pIgsHwIpFwdInfo->InnerVlanId =
                SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
            pIgsHwIpFwdInfo->u4GrpAddr =
                SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr);
            pIgsHwIpFwdInfo->u4SrcAddr =
                SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr);
        }
        SNOOP_HW_IP_FREE_MEMBLK (pIgsHwIpFwdInfo);
#endif

#ifdef MLDS_WANTED                /* MLDS_CHANGES */
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (MldsFsMiNpGetIp6FwdEntryHitBitStatus
                (u4InstId, SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                 pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr,
                 pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr) == FNP_SUCCESS)
            {
                u4HitBitStatus = TRUE;
            }
        }
#endif

        /* If the entry is not used in the hardware delete the (S,G) entry */
        if (u4HitBitStatus == FALSE)
        {
#endif
            if (pSnoopGroupEntry != NULL)
            {
                SNOOP_MEM_CPY ((UINT1 *) &GrpIpAddr,
                               (UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                               sizeof (tIPvXAddr));
                SNOOP_MEM_CPY ((UINT1 *) &SrcIpAddr,
                               (UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                               sizeof (tIPvXAddr));

                if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry,
                                              pSnoopIpGrpFwdEntry->SrcIpAddr,
                                              0,
                                              pSnoopIpGrpFwdEntry->PortBitmap,
                                              SNOOP_DELETE_FWD_ENTRY) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                   "Unable to delete the IP forwarding entry in "
                                   "hardware\r\n");
                    return SNOOP_FAILURE;
                }
                else
                {
#ifdef L2RED_WANTED
                    SnoopActiveRedGrpDeleteSync (u4InstId,
                                                 pSnoopIpGrpFwdEntry->SrcIpAddr,
                                                 pSnoopGroupEntry);
#endif
                }
                if (SNOOP_MEM_CMP
                    (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) == 0)
                {
                    if (SnoopGrpDeleteGroupEntry
                        (u4InstId, pSnoopGroupEntry->VlanId,
                         pSnoopGroupEntry->GroupIpAddr) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                       "Deletion of Group record failed\r\n");
                        return SNOOP_FAILURE;

                    }
                }
                /* Get the entry created for router-port entry (Inner-VLAN 0) */
                SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

                SNOOP_OUTER_VLAN (IpGrpFwdEntry.VlanId) =
                    SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
                SNOOP_INNER_VLAN (IpGrpFwdEntry.VlanId) = SNOOP_INVALID_VLAN_ID;
                IpGrpFwdEntry.u1AddressType = GrpIpAddr.u1Afi;
                IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                                (UINT1 *) &(GrpIpAddr));
                IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                                (UINT1 *) &(SrcIpAddr));

                pRBElem = RBTreeGet
                    (SNOOP_INSTANCE_INFO (u4InstId)->IpMcastFwdEntry,
                     (tRBElem *) & IpGrpFwdEntry);

                /* If the entry is not present do nothing. 
                 * Otherwise we have to get the next IP FWD entry to check 
                 * any other entry present for this Outer-VLAN, Group, Source. 
                 * If it is present there is no need to delete the entry */
                if (pRBElem != NULL)
                {
                    pIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
                    if (SnoopVlanGetVlanEntry (u4InstId,
                                               IpGrpFwdEntry.VlanId,
                                               GrpIpAddr.u1Afi,
                                               &pSnoopVlanEntry)
                        != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG_ARG1
                            (SNOOP_DBG_FLAG_INST (u4InstId),
                             SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                             SNOOP_VLAN_NAME, "VLAN entry not found in Snoop"
                             "Vlan Entry Table, for VLAN %d\r\n",
                             SNOOP_OUTER_VLAN (IpGrpFwdEntry.VlanId));
                        return SNOOP_FAILURE;
                    }
                    pSnoopRtrPortBmp =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pSnoopRtrPortBmp == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                       "SnoopTmrIpFwdEntryTimerExpiry: "
                                       "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                        return SNOOP_FAILURE;
                    }
                    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                    SnoopVlanGetRtrPortFromPVlanMappingInfo
                        (u4InstId, SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId),
                         pSnoopVlanEntry->u1AddressType, 0, pSnoopRtrPortBmp,
                         pSnoopVlanEntry);

                    if (SNOOP_MEM_CMP (pIpGrpFwdEntry->PortBitmap,
                                       pSnoopRtrPortBmp,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        /* Some ports are already learnt for this group */
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_SUCCESS;
                    }
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    pRBNextElem = RBTreeGetNext
                        (SNOOP_INSTANCE_INFO (u4InstId)->IpMcastFwdEntry,
                         pRBElem, NULL);

                    pTempIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBNextElem;

                    if ((pTempIpGrpFwdEntry != NULL) &&
                        (SNOOP_MEM_CMP ((UINT1 *) &SrcIpAddr,
                                        (UINT1 *) &(pTempIpGrpFwdEntry->
                                                    SrcIpAddr),
                                        sizeof (tIPvXAddr)) == 0) &&
                        (SNOOP_MEM_CMP ((UINT1 *) &GrpIpAddr,
                                        (UINT1 *) &(pTempIpGrpFwdEntry->
                                                    GrpIpAddr),
                                        sizeof (tIPvXAddr)) == 0) &&
                        (SNOOP_OUTER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
                         SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)) &&
                        (SNOOP_INNER_VLAN (pTempIpGrpFwdEntry->VlanId) !=
                         SNOOP_INVALID_VLAN_ID))
                    {
                        /* There is some more entry present for this 
                         * Outer-VLAN so no need to delete the entry */
                        return SNOOP_SUCCESS;
                    }

                    if (SnoopDeleteIpFwdTable (u4InstId, pIpGrpFwdEntry)
                        != SNOOP_SUCCESS)
                    {
                        return SNOOP_FAILURE;
                    }
                }
            }
            else
            {
                if (SnoopDeleteIpFwdTable (u4InstId, pSnoopIpGrpFwdEntry)
                    != SNOOP_SUCCESS)
                {
                    return SNOOP_FAILURE;
                }

            }
#ifdef NPAPI_WANTED
        }
        else
        {
            if ((pSnoopIpGrpFwdEntry != NULL) && (pSnoopGroupEntry != NULL))
            {
                /* If the forwarding entry is used in the hardware restart the 
                 * timer for the group entry */
                pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4InstId;
                pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType =
                    SNOOP_IP_FWD_ENTRY_TIMER;

                if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                                        SNOOP_IP_FWD_ENTRY_TIMER_VAL) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "Unable to restart IP forwarding entry timer \r\n");
                    return SNOOP_FAILURE;

                }
            }
            else if ((pSnoopIpGrpFwdEntry != NULL)
                     && (pSnoopGroupEntry == NULL))
            {
                SNOOP_OUTER_VLAN (SnoopConsGroupEntry.VlanId) =
                    SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
                IPVX_ADDR_COPY (&SnoopConsGroupEntry.GroupIpAddr,
                                &pSnoopIpGrpFwdEntry->GrpIpAddr);

                pRBElem =
                    RBTreeGet (SNOOP_INSTANCE_INFO (u4InstId)->ConsGroupEntry,
                               (tRBElem *) & SnoopConsGroupEntry);

                pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;

                if (pSnoopConsGroupEntry != NULL)
                {
                    if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList)
                        != 0)
                    {

                        pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4InstId;
                        pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType =
                            SNOOP_IP_FWD_ENTRY_TIMER;

                        if (SnoopTmrStartTimer
                            (&pSnoopIpGrpFwdEntry->EntryTimer,
                             SNOOP_IP_FWD_ENTRY_TIMER_VAL) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                       SNOOP_OS_RES_DBG,
                                       "Unable to restart IP forwarding entry timer \r\n");
                            return SNOOP_FAILURE;

                        }

                        return SNOOP_SUCCESS;
                    }
                }

                if (SnoopDeleteIpFwdTable (u4InstId, pSnoopIpGrpFwdEntry)
                    != SNOOP_SUCCESS)
                {
                    return SNOOP_FAILURE;
                }
            }

        }

#ifdef IGS_WANTED
    }
#endif
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopTmrHostPresentTimerExpiry                       */
/*                                                                           */
/* Description        : This function processes the Host present             */
/*                      timer expiry.                                        */
/*                                                                           */
/* Input(s)           : pArg  - Pointer to port entry                        */
/*                    : u4InstId  - Instance Identifier                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopTmrHostPresentTimerExpiry (UINT4 u4Instance, VOID *pArg)
{
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopPortEntry    *pSnoopPortEntry = pArg;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopHostEntry    *pSnoopNextHostEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntryNode = NULL;
    tSnoopSrcBmp        TempInclSrcBmp;
    tSnoopSrcBmp        TempXorInclBmp;
    tSnoopTag           VlanId;
    UINT4               u4MaxLength = 0;
    UINT4               u4PhyPort = 0;
    UINT4               u4Port = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NumSrcs = 0;
    UINT2               u2IpAddrLen = 0;
    UINT2               u2Byte = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2HostPurgeInterval = 0;
    UINT1               u1AuxDataLen = 0;
    UINT1               u1Value = 0;
    UINT1               u1Consolidate = SNOOP_FALSE;
    UINT1               u1RecordType = 0;
    UINT1               u1Forward = 0;
    UINT1               u1AddressType = 0;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1ChannelTypeFound = SNOOP_FALSE;
    INT4                i4RetVal = SNOOP_SUCCESS;
#ifdef ICCH_WANTED
    tCRU_BUF_CHAIN_HEADER *pIcclBuf = NULL;
    UINT1              *pPortBitmap = NULL;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    INT4                i4Result = SNOOP_SUCCESS;
    UINT4               u4MclagPhyPort = 0;
    UINT4               u4GrpAddr = 0;
    UINT1               u1McLagInterface = OSIX_FALSE;
    UINT2               u2AdminKey = 0;
    BOOL1               bIcclInterface = OSIX_FALSE;
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_CPY (TempInclSrcBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    pSnoopGroupEntry = (tSnoopGroupEntry *) pSnoopPortEntry->pGroupEntry;
    u4Port = pSnoopPortEntry->u4Port;
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME,
                       "Unable to get Vlan record for the given instance "
                       "and the VlanId \r\n");
        return SNOOP_FAILURE;
    }

#ifdef ICCH_WANTED
    /* Do not delete the host entries if port purge timer is still
       running for this group and MCLAG is enabled for port-channel
       This is done for the following purpose,
       When purge timer is expired in any node this will be synced
       to neighbor node and group specific query triggered.
       At this time Host present timer expiry can delete the entries.
       The deletion now avoided so that query is transmitted
       to sync with the leave processing mechanism in MCLAG ports.
     */

    u4MclagPhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPortEntry->u4Port);

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        SnoopLaIsMclagInterface (u4MclagPhyPort, &u1McLagInterface);
    }

    if ((pSnoopPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
         SNOOP_INVALID_TIMER_TYPE) && (u1McLagInterface == OSIX_TRUE))
    {
        return SNOOP_SUCCESS;
    }
#endif

    /* Reset the u1V1HostPresent flag. This will be set again if any one
     * v1 host is present on this port */
    /* For the SSM port entries, setting the u1V1HostPresent to SNOOP_FALSE
     * does not have any impact. And it will always be SNOOP_FALSE as the
     * the SSM Port entry cannot have any v1 host registered on the SSM port */
    pSnoopPortEntry->u1V1HostPresent = SNOOP_FALSE;

    /* Delete all the Host information which are in second half. 
     * Move the hosts present in first half to second half.*/
    pSnoopHostEntry =
        (tSnoopHostEntry *) SNOOP_SLL_FIRST (&pSnoopPortEntry->HostList);

    while (pSnoopHostEntry != NULL)
    {
        pSnoopNextHostEntry =
            (tSnoopHostEntry *) SNOOP_SLL_NEXT (&pSnoopPortEntry->HostList,
                                                &pSnoopHostEntry->
                                                NextHostEntry);

        if (pSnoopHostEntry->u1HostTimerStatus == SNOOP_FIRST_HALF)
        {
            pSnoopHostEntry->u1HostTimerStatus = SNOOP_SECOND_HALF;
            if (pSnoopHostEntry->SnoopHostType == SNOOP_ASM_V1HOST)
            {
                pSnoopPortEntry->u1V1HostPresent = SNOOP_TRUE;
            }
        }
        else
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_MGMT,
                            SNOOP_MGMT_DBG, "SSM host %s is deleted\r\n",
                            SnoopPrintIPvxAddress (pSnoopHostEntry->
                                                   HostIpAddr));
            /* Remove the host from the Host Information RBTree */
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                          (tRBElem *) pSnoopHostEntry);
            SNOOP_SLL_DELETE (&pSnoopPortEntry->HostList, pSnoopHostEntry);

            if (pSnoopHostEntry->SnoopHostType == SNOOP_SSM_HOST)
            {
                SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostEntry);
            }
            else
            {
                SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostEntry);
            }
            u1Consolidate = SNOOP_TRUE;
        }
        pSnoopHostEntry = pSnoopNextHostEntry;
    }

    /* If all the hosts registered on this port are removed,
     * call the port purge handler */
    if (SNOOP_SLL_COUNT (&pSnoopPortEntry->HostList) == 0)
    {
        if (pSnoopPortEntry->pSourceBmp != NULL)
        {

#ifdef ICCH_WANTED
            SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));

            if ((u1McLagInterface == OSIX_TRUE) &&
                (pSnoopGroupEntry->u1McLagLeaveReceived == SNOOP_FALSE))
            {
                pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_TRUE;
                SnoopLaUpdateActorPortChannelAdminKey (u4MclagPhyPort,
                                                       &u2AdminKey);

                SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, pSnoopGroupEntry->VlanId,
                               sizeof (tSnoopTag));
                SnoopIcchSyncLeaveMessage (u4Instance,
                                           pSnoopGroupEntry->GroupIpAddr,
                                           &SnoopIcchInfo, u2AdminKey);
            }

            bIcclInterface = SnoopIcchIsIcclInterface (u4MclagPhyPort);

            if (bIcclInterface == OSIX_TRUE)
            {
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    u4GrpAddr =
                        SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->GroupIpAddr.
                                           au1Addr);

                    i4Result =
                        IgsEncodeQuery (u4Instance, u4GrpAddr, SNOOP_GRP_QUERY,
                                        pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                        u1ConfigOperVer, &pIcclBuf,
                                        pSnoopVlanEntry);
                }
#endif
#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Result =
                        MldsEncodeQuery (u4Instance,
                                         pSnoopGroupEntry->GroupIpAddr.au1Addr,
                                         SNOOP_GRP_QUERY, SNOOP_MLD_VERSION2,
                                         &pIcclBuf, pSnoopVlanEntry);
                }
#endif
                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_PKT_NAME,
                                   "Group Specific Query construct and send - failed\r\n");
                    return SNOOP_FAILURE;
                }

                pPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pPortBitmap == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Error in allocating memory for pPortBitmap\r\n");
                    CRU_BUF_Release_MsgBufChain (pIcclBuf, FALSE);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

                SNOOP_ADD_TO_PORT_LIST (pSnoopPortEntry->u4Port, pPortBitmap);
                SnoopVlanForwardPacket (u4Instance, VlanId,
                                        pSnoopVlanEntry->u1AddressType,
                                        SNOOP_INVALID_PORT,
                                        VLAN_FORWARD_SPECIFIC, pIcclBuf,
                                        pPortBitmap, SNOOP_GRP_QUERY_SENT);
                UtilPlstReleaseLocalPortList (pPortBitmap);

            }
#endif
            /* The Port Entry is a SSM Port. Call SSM port purge handler */
            SnoopHandleSSMPortPurge (u4Instance, pSnoopPortEntry);
        }
        else
        {
            if (pSnoopPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                i4RetVal =
                    SnoopTmrStopTimer (&pSnoopPortEntry->PurgeOrGrpQueryTimer);
                UNUSED_PARAM (i4RetVal);
            }
            /* The Port Entry is a ASM Port. Call ASM port purge handler */
            i4RetVal =
                SnoopTmrASMPortPurgeTimerExpiry (u4Instance, pSnoopPortEntry);
            UNUSED_PARAM (i4RetVal);
        }
        return SNOOP_SUCCESS;
    }

    /* We have atleast one host present on the port. Let us re-start
     * the host present timer 
     */

    u2HostPurgeInterval = (UINT2) (pSnoopVlanEntry->u2PortPurgeInt /
                                   SNOOP_DEF_RETRY_COUNT);
    if ((pSnoopVlanEntry->u2PortPurgeInt % 2) != 0)
    {
        /*if the portPurge interval is an odd value , as host present timer is
         * running in two halves , there may be a miss for a sec.
         * So during starting the second half of the timer , incrementing one */
        u2HostPurgeInterval = u2HostPurgeInterval + 1;
    }
    pSnoopPortEntry->HostPresentTimer.u4Instance = u4Instance;
    pSnoopPortEntry->HostPresentTimer.u1TimerType = SNOOP_HOST_PRESENT_TIMER;
    SnoopTmrStartTimer (&pSnoopPortEntry->HostPresentTimer,
                        u2HostPurgeInterval);

    /* For the ASM port entry, we have atleast one host present on the port.
     * As, it does not result in any filter mode change, group consolidation
     * is not required */
    if (pSnoopPortEntry->pSourceBmp == NULL)
    {
        return SNOOP_SUCCESS;
    }

    /* For the SSM port entries Consolidate the group record
     * and send SSM report upstream 
     * if necessary */

    if (u1Consolidate == SNOOP_TRUE)
    {
        u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u4Port);

        /* Get the port entry; If port entry is absent, no port-based action will
         * be taken for the packet. */
        if (SnoopGetPortCfgEntry (u4Instance, u4PhyPort, VlanId,
                                  pSnoopVlanEntry->u1AddressType,
                                  &pSnoopPortCfgEntry) == SNOOP_SUCCESS)
        {
            pSnoopExtPortCfgEntryNode =
                pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
            /* u1PortEntryFound = SNOOP_TRUE; */
            /* store the ports, Old include bitmap */
            if (pSnoopExtPortCfgEntryNode != NULL)
            {
                if ((pSnoopExtPortCfgEntryNode->u1SnoopPortMaxLimitType
                     == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
                    (SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[u1AddressType - 1].u1FilterStatus
                     != SNOOP_DISABLE))
                {
                    u1ChannelTypeFound = SNOOP_TRUE;
                    SNOOP_MEM_CPY (TempInclSrcBmp,
                                   pSnoopPortEntry->pSourceBmp->InclSrcBmp,
                                   SNOOP_SRC_LIST_SIZE);
                }
            }
        }
        SnoopGrpConsolidateSrcInfo
            (u4Instance, SNOOP_HOST_TIMER_EXPIRY,
             pSnoopGroupEntry->pConsGroupEntry->u1FilterMode,
             pSnoopGroupEntry->pConsGroupEntry->InclSrcBmp,
             pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
             pSnoopGroupEntry, SNOOP_TRUE);

        /* After consolidation, check if there is any change in the source
         * bitmap */
        if (u1ChannelTypeFound == SNOOP_TRUE)
        {

            MEMCPY (TempXorInclBmp, TempInclSrcBmp, SNOOP_SRC_LIST_SIZE);
            SNOOP_XOR_SRC_BMP (TempXorInclBmp,
                               pSnoopPortEntry->pSourceBmp->InclSrcBmp);
            if (SNOOP_MEM_CMP (TempXorInclBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE)
                != 0)
            {
                for (u2Byte = 0; u2Byte < SNOOP_SRC_LIST_SIZE; u2Byte++)
                {
                    u1Value = TempXorInclBmp[u2Byte];
                    for (u2BitIndex = 0;
                         ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                          && (u1Value != 0)); u2BitIndex++)
                    {
                        if ((u1Value & SNOOP_BIT8) != 0)
                        {
                            SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntryNode);
                        }
                        u1Value = (UINT1) (u1Value << 1);
                    }
                }
            }
        }

        if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED
            (u4Instance, (u1AddressType - 1)) == SNOOP_FALSE)
        {
            return SNOOP_SUCCESS;
        }

        if (gu4SendRecType == 0)
        {
            return SNOOP_SUCCESS;
        }

        /* Proxy is enabled so generate a report based upon 
         * the operating Version for the VLAN and send it */
        SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
        /* if v3 Router port list is not empty then send a V3 Report */
        pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pSnoopRtrPortBmp == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "SnoopTmrHostPresentTimerExpiry: "
                           "Error in allocating memory for pSnoopRtrPortBmp\r\n");
            return SNOOP_FAILURE;
        }
        MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                 SNOOP_OUTER_VLAN (VlanId),
                                                 u1AddressType,
                                                 SNOOP_IGS_IGMP_VERSION3,
                                                 pSnoopRtrPortBmp,
                                                 pSnoopVlanEntry);
        SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->StaticRtrPortBitmap,
                                pSnoopRtrPortBmp);
        if (SNOOP_MEM_CMP
            (pSnoopRtrPortBmp, gNullPortBitMap, sizeof (tSnoopPortBmp)) != 0)
        {
            /* if the forwarding is done based on IP then constrcut the 
               SSM Reports for all the Group records with list 
               of sources */
            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
            SNOOP_MEM_CPY
                (au1GrpAddr,
                 pSnoopGroupEntry->pConsGroupEntry->GroupIpAddr.au1Addr,
                 IPVX_MAX_INET_ADDR_LEN);

            SNOOP_INET_HTONL (au1GrpAddr);

            u2IpAddrLen = (pSnoopVlanEntry->u1AddressType ==
                           SNOOP_ADDR_TYPE_IPV4) ?
                SNOOP_IP_ADDR_SIZE : IPVX_MAX_INET_ADDR_LEN;

            /* Get the record type and number of sources from 
             * gu4SendRecType which is updated for any new registration 
             * or source list change for a group record */

            u1RecordType = (UINT1) (gu4SendRecType & 0x0000ffff);
            u2NumSrcs = SNOOP_HTONS (gu4SendRecType >> 16);
            u1AuxDataLen = 0;
            u2GrpRec++;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u1RecordType, SNOOP_OFFSET_ONE);
            u4MaxLength += SNOOP_OFFSET_ONE;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u1AuxDataLen, SNOOP_OFFSET_ONE);
            u4MaxLength += SNOOP_OFFSET_ONE;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u2NumSrcs, SNOOP_SRC_CNT_OFFSET);
            u4MaxLength += SNOOP_SRC_CNT_OFFSET;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           au1GrpAddr, u2IpAddrLen);
            u4MaxLength += u2IpAddrLen;

            u2NumSrcs = (UINT2) (gu4SendRecType >> 16);

            if (u2NumSrcs != 0)
            {
                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               gpSSMSrcInfoBuffer, u2IpAddrLen * u2NumSrcs);
                u4MaxLength += (u2NumSrcs * u2IpAddrLen);
            }

#ifdef IGS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                if (IgsEncodeAggV3Report (u4Instance, u2GrpRec, u4MaxLength,
                                          &pOutBuf) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_PKT | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_PKT_NAME,
                                   "Unable to generate and send Igs "
                                   "V3 report on to router ports\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;

                }
            }
#endif
#ifdef MLDS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                if (MldsEncodeAggV2Report (u4Instance, u2GrpRec, u4MaxLength,
                                           &pOutBuf) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_PKT | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_PKT_NAME,
                                   "Unable to generate and send Mlds "
                                   "V2 report on to router ports\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;

                }
            }
#endif

            u1Forward =
                ((SNOOP_INSTANCE_INFO (u4Instance)->
                  SnoopInfo[u1AddressType - 1].u1ReportFwdAll ==
                  SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                 VLAN_FORWARD_SPECIFIC);
            SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                                    u1AddressType, SNOOP_INVALID_PORT,
                                    u1Forward, pOutBuf,
                                    pSnoopRtrPortBmp, SNOOP_SSMREPORT_SENT);
            gu4SendRecType = 0;
        }
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
    }
    UNUSED_PARAM (i4RetVal);
    return SNOOP_SUCCESS;
}
