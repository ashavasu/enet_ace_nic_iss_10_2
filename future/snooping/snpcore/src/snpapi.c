/* SOURCE FILE HEADER :
 *
 * $Id: snpapi.c,v 1.41.2.1 2018/03/28 13:59:45 siva Exp $
 * */
/*****************************************************************************/
/*    FILE  NAMEn           : snpapi.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Main Module                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains the exported APIs of        */
/*                            snooping(MLD/IGMP) module                      */
/*---------------------------------------------------------------------------*/
#include "snpinc.h"
#include "snpextn.h"

extern UINT4        FsSnoopInstanceGlobalMcastFwdMode[12];

/*****************************************************************************/
/* Function Name      : SnpLock                                              */
/*                                                                           */
/* Description        : This function is used to take the Snoop mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP,SnpMain                                        */
/*****************************************************************************/
INT4
SnpLock (VOID)
{
    if (SNOOP_TAKE_SEM (gSnoopSemId) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE, SNOOP_MEM_NAME,
                       "Failed to Take SNOOP Mutual Exclusion "
                       "Semaphore \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnpUnLock                                            */
/*                                                                           */
/* Description        : This function is used to give the Snoop mutual       */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNMP_SUCCESS or SNMP_FAILURE                         */
/*                                                                           */
/* Called By          : Protocol Main and SNMP/CLI                           */
/*                                                                           */
/* Calling Function   :  SNMP, SnpMain                                       */
/*****************************************************************************/
INT4
SnpUnLock (VOID)
{
    SNOOP_GIVE_SEM (gSnoopSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMiGetForwardingType                             */
/*                                                                           */
/* Description        : This function returns the type of Forwarding type    */
/*                      configured (IP/MAC based Multicast Fwd) for a given  */
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : u1McastFwdMode                                       */
/*****************************************************************************/
INT1
SnoopMiGetForwardingType (UINT4 u4Instance)
{
    UINT1               u1McastFwdMode = 0;

    if (gu1SnoopInit == SNOOP_TRUE)
    {
        SNOOP_LOCK ();
        if (SNOOP_INSTANCE_INFO (u4Instance) != NULL)
        {
            u1McastFwdMode = SNOOP_MCAST_FWD_MODE (u4Instance);
        }
        SNOOP_UNLOCK ();
    }
    return u1McastFwdMode;
}

/*****************************************************************************/
/* Function Name      : SnoopMainEnqueueMcastFrame                           */
/*                                                                           */
/* Description        : This function sends an event to Snoop Task to process*/
/*                      MultiCast frame and this will be called from VLAN    */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      u2IfIndex - interface index                          */
/*                      VlanId   - Vlan Id of the packet. Will be zero if    */
/*                                 called from Bridge module                 */
/*                      u1AddressType - Address Type (IPV4/IPV6)             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopMainEnqueueMcastFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                            tSnoopVlanId VlanId, UINT1 u1AddressType)
{
    tSnoopQMsg         *pSnoopQMsg = NULL;
    UINT4               u4Event = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;
    UINT1               u1PktType = 0;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopEnqueueMulticastFrame\r\n");
    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME, "SNOOP is shutdown\r\n");

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME, "SNOOP is shutdown\r\n");
        return SNOOP_FAILURE;
    }

    if (SnoopVcmGetContextInfoFromIfIndex ((UINT4) u2IfIndex,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, packet dropped\r\n");

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");

        }

        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "packet enqueue message \r\n");
        return SNOOP_FAILURE;
    }

    /* ND packets need not be snooped */
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1PktType,
                                   SNOOP_ICMP_TYPE_OFFSET, 1);
    }

    if (SNOOP_IS_ND6_PACKET (u1PktType))
    {

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    if ((pSnoopQMsg = (tSnoopQMsg *) SNOOP_QPKT_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_MEM_NAME, "Memory allocation failed for packet "
                       "processing queue message\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "processing queue message\n");

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopQMsg, 0, sizeof (tSnoopQMsg));

    pSnoopQMsg->pInQMsg = pBuf;
    pSnoopQMsg->u4Instance = u4InstanceId;
    pSnoopQMsg->u2LocalPort = u2InstPort;
    pSnoopQMsg->VlanInfo.OuterVlanTag.u2VlanId = VlanId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_PKT_QUEUE_ID, (UINT1 *) &pSnoopQMsg,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pSnoopQMsg) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Queue message release failed in "
                           "multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    u4Event = (u1AddressType == SNOOP_ADDR_TYPE_IPV4) ?
        SNOOP_IGS_PKT_ENQ_EVENT : SNOOP_MLDS_PKT_ENQ_EVENT;

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, u4Event);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopEnqueueMulticastFrame                           */
/*                                                                           */
/* Description        : This function sends an event to Snoop Task to process*/
/*                      MultiCast frame.                                      */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      u2IfIndex - interface index                          */
/*                      VlanId   - Vlan Id of the packet. Will be zero if    */
/*                                 called from Bridge module                 */
/*                      u2TagLen - Tag Length                                */
/*                      u1AddressType - Address Type (IPV4/IPV6)             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopEnqueueMulticastFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                            tSnoopVlanInfo VlanInfo, UINT2 u2TagLen,
                            UINT1 u1AddressType)
{
    tSnoopQMsg         *pSnoopQMsg = NULL;
    UINT4               u4Event = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;
    UINT1               u1PktType = 0;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME, "SNOOP is shutdown\r\n");
        return SNOOP_FAILURE;
    }

    if (SnoopVcmGetContextInfoFromIfIndex ((UINT4) u2IfIndex,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "packet enqueue message \r\n");
        return SNOOP_FAILURE;
    }

    /* ND packets need not be snooped */
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {

        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1PktType,
                                   SNOOP_ICMP_TYPE_OFFSET, 1);
    }

    if (SNOOP_IS_ND6_PACKET (u1PktType))
    {

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    if ((pSnoopQMsg = (tSnoopQMsg *) SNOOP_QPKT_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "Memory allocation failed for packet "
                       "processing queue message\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "processing queue message\n");

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "CRU buffer release failed in multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopQMsg, 0, sizeof (tSnoopQMsg));

    pSnoopQMsg->pInQMsg = pBuf;
    pSnoopQMsg->u4Instance = u4InstanceId;
    pSnoopQMsg->u2LocalPort = u2InstPort;
    SNOOP_MEM_CPY (&(pSnoopQMsg->VlanInfo), &VlanInfo, sizeof (tSnoopVlanInfo));
    pSnoopQMsg->u2TagLen = u2TagLen;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_PKT_QUEUE_ID, (UINT1 *) &pSnoopQMsg,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "CRU buffer release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFFER, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_RELEASE_FAIL],
                                   "Queue Id %d\r\n", SNOOP_PKT_QUEUE_ID);
        }

        if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pSnoopQMsg) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "Queue message release failed in "
                           "multicast packet processing\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_TRC,
                                   SnoopSysErrString[SYS_LOG_MEM_RELEASE_FAIL],
                                   "multicast packet processing\n");
        }

        return SNOOP_FAILURE;
    }

    u4Event = (u1AddressType == SNOOP_ADDR_TYPE_IPV4) ?
        SNOOP_IGS_PKT_ENQ_EVENT : SNOOP_MLDS_PKT_ENQ_EVENT;

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, u4Event);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeletePort                                      */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the deletion of a Port.                  */
/*                                                                           */
/* Input(s)           : u4Port - The Port number of the port to be deleted   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopDeletePort (UINT4 u4Port)
{
    tSnoopMsgNode      *pNode = NULL;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                       "Failed to post message - delete port  \n");
        return SNOOP_FAILURE;
    }

    if (u4Port > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }

    if (SnoopVcmGetContextInfoFromIfIndex (u4Port,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, Failed to post message - "
                       "delete port \r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "port delete message \r\n");
        return SNOOP_FAILURE;
    }

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_DELETE_PORT_MSG;
    pNode->u4Instance = u4InstanceId;
    pNode->u2LocalPort = u2InstPort;
    pNode->uInMsg.u4Port = u4Port;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return SNOOP_SUCCESS;
}

#ifdef IGS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopIsIgmpSnoopingEnabled                           */
/*                                                                           */
/* Description        : This function checks if IGMP snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopIsIgmpSnoopingEnabled (UINT4 u4Instance)
{
    if (gu1SnoopInit == SNOOP_TRUE)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance) != NULL)
        {
            if (SNOOP_SNOOPING_STATUS
                (u4Instance, (SNOOP_ADDR_TYPE_IPV4 - 1)) == SNOOP_ENABLE)
            {
                return SNOOP_ENABLED;
            }
        }
    }
    return SNOOP_DISABLED;
}
#endif

#ifdef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopIsMldSnoopingEnabled                             */
/*                                                                           */
/* Description        : This function checks if MLD snooping feature is     */
/*                      enabled in the system or not.                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopIsMldSnoopingEnabled (UINT4 u4Instance)
{
    if (gu1SnoopInit == SNOOP_TRUE)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance) != NULL)
        {
            if (SNOOP_SNOOPING_STATUS
                (u4Instance, (SNOOP_ADDR_TYPE_IPV6 - 1)) == SNOOP_ENABLE)
            {
                return SNOOP_ENABLED;
            }
        }
    }
    return SNOOP_DISABLED;
}
#endif

/***************************************************************/
/*  Function Name   : SnoopIsSnoopStartedInContext             */
/*  Description     : Returns the SNOOP Module Status          */
/*                    Application                              */
/*  Input(s)        : Context                                  */
/*  Output(s)       :                                          */
/*  Returns         : SNOOP_SHUTDOWN /SNOOP_START              */
/***************************************************************/
INT4
SnoopIsSnoopStartedInContext (UINT4 u4ContextId)
{
    return (SNOOP_SYSTEM_STATUS (u4ContextId));
}

/*****************************************************************************/
/* Function Name      : SnoopPortOperIndication                              */
/*                                                                           */
/* Description        : This API is called from the CFA Module to indicate   */
/*                      the port status indication                           */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                              */
/*                      u1Status - Link Status (Up or Down)                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status)
{
    tL2IwfContextInfo   L2IwfContextInfo;

    /* Get the Context Information before calling SnoopWrPortOperIndication ()
     * for backward compatibility */
    if (VcmGetContextInfoFromIfIndex (u4IfIndex, &L2IwfContextInfo.u4ContextId,
                                      &L2IwfContextInfo.u2LocalPortId) ==
        VCM_FAILURE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Failed to get the context information\r\n");
        return SNOOP_FAILURE;
    }

    return (SnoopWrPortOperIndication (u4IfIndex, u1Status, &L2IwfContextInfo));
}

/*****************************************************************************/
/* Function Name      : SnoopWrPortOperIndication                            */
/*                                                                           */
/* Description        : This Wrapper Function is called from the CFA Module  */
/*                      to indicate the port status indication               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port number                              */
/*                      u1Status - Link Status (Up or Down)                  */
/*                      pL2IwfContextInfo - Context Information of the port  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopWrPortOperIndication (UINT4 u4IfIndex, UINT1 u1Status,
                           tL2IwfContextInfo * pL2IwfContextInfo)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_TRC (SNOOP_GBL_TRC_FLAG, SNOOP_ALL_FAILURE_TRC,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                       "Failed to post message - port oper status \n");
        return SNOOP_FAILURE;
    }

    if (u4IfIndex > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }

    if (SNOOP_SYSTEM_STATUS (pL2IwfContextInfo->u4ContextId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pL2IwfContextInfo->u4ContextId),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "port oper status message \r\n");
        return SNOOP_FAILURE;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (pL2IwfContextInfo->u4ContextId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message queue \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for index %d\r\n", u4IfIndex);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    if (u1Status == CFA_IF_DOWN)
    {
        pNode->u2MsgType = SNOOP_DISABLE_PORT_MSG;
    }
    if (u1Status == CFA_IF_UP)
    {
        pNode->u2MsgType = SNOOP_ENABLE_PORT_MSG;
    }
    pNode->uInMsg.u4Port = u4IfIndex;
    pNode->u4Instance = pL2IwfContextInfo->u4ContextId;
    pNode->u2LocalPort = pL2IwfContextInfo->u2LocalPortId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed for port oper"
                           " status indication\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for port oper status indication - queue id %d\r\n",
                                   SNOOP_MSG_QUEUE_ID);
        }
        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUpdateHwProperties                             */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module        */
/*                      to indicate the port-channel status indication       */
/*                                                                           */
/* Input(s)           : u2AggId - AggId                                      */
/*                      u4IfIndex - Port Index                               */
/*                      u1Status  - Add/Remove from Port-channel             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : L2Iwf Module                                         */
/*****************************************************************************/
INT4
SnoopUpdateHwProperties (UINT2 u2AggId, UINT4 u4IfIndex, UINT1 u1Status)
{

    tSnoopMsgNode      *pNode = NULL;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_TRC (SNOOP_GBL_TRC_FLAG, SNOOP_ALL_FAILURE_TRC,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                       "Failed to post message - port oper status \n");
        return SNOOP_FAILURE;
    }

    if (SnoopVcmGetContextInfoFromIfIndex ((UINT4) u2AggId,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, Failed to post message - "
                       "port oper status \r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_ALL_FAILURE_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "port oper status message \r\n");
        return SNOOP_FAILURE;
    }

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message queue \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "aggid is %d\r\n", u2AggId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = u1Status;
    pNode->u2AggId = u2AggId;
    pNode->uInMsg.u4Port = u4IfIndex;
    pNode->u4Instance = u4InstanceId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed for port oper"
                           " status indication\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }
        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMiDelMcastFwdEntryForVlan                       */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the deletion of a VLAN for a given       */
/*                      Instance.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4Instance - Instance for which the operation        */
/*                                   should be done                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
SnoopMiDelMcastFwdEntryForVlan (UINT4 u4Instance, tSnoopVlanId VlanId)
{
    tSnoopMsgNode      *pNode = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    INT4                i4CheckVlan = SNOOP_FALSE;
    tSnoopTag           SnoopVlanId;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised\n");
        return SNOOP_FAILURE;
    }
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "vlan delete message \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_OUTER_VLAN (SnoopVlanId) = VlanId;
    SNOOP_INNER_VLAN (SnoopVlanId) = SNOOP_INVALID_VLAN_ID;
    if ((SnoopVlanGetVlanEntry (u4Instance,
                                SnoopVlanId,
                                SNOOP_ADDR_TYPE_IPV4,
                                &pSnoopVlanEntry)) == SNOOP_SUCCESS)
    {
        i4CheckVlan = SNOOP_TRUE;
    }
    else if ((SnoopVlanGetVlanEntry (u4Instance,
                                     SnoopVlanId,
                                     SNOOP_ADDR_TYPE_IPV6,
                                     &pSnoopVlanEntry)) == SNOOP_SUCCESS)
    {
        i4CheckVlan = SNOOP_TRUE;
    }
    if (i4CheckVlan == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_ALL_FAILURE,
                            SNOOP_GBL_NAME,
                            "Failed to fetch VLAN entry for vlan %d\r\n",
                            SnoopVlanId);
        return SNOOP_FAILURE;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "vlan id %d\r\n", VlanId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_VLAN_DELETE_EVENT;
    SNOOP_OUTER_VLAN (pNode->uInMsg.VlanId) = VlanId;
    pNode->u4Instance = u4Instance;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed in Vlan deletion event\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id%d\r\n", SNOOP_MSG_QUEUE_ID);
        }
        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMiUpdatePortList                                */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to indicate the port list updation for a given       */
/*                      instance.                                            */
/*                                                                           */
/* Input(s)           : u4Instance - Instance for which the operation        */
/*                                   should be done                          */
/*                      VlanId  - Vlan Identifier                            */
/*                    : McastAddress - Multicast mac address, will be NULL   */
/*                                     for vlan updation.                    */
/*                    : AddPortBitmap - Ports to be added to snoop Fwd Table */
/*                    : DelPortBitmap - Ports to be deleted from snoop Fwd   */
/*                                      Table                                */
/*                      u1PortType - To indicate VLAN/Mcast Ports updation   */
/*                                   VLAN_UPDT_VLAN_PORTS /                  */
/*                                   VLAN_UPDT_MCAST_PORTS                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopMiUpdatePortList (UINT4 u4Instance, tSnoopVlanId VlanId,
                       tMacAddr McastAddr, tSnoopIfPortBmp AddPortBitmap,
                       tSnoopIfPortBmp DelPortBitmap, UINT1 u1PortType)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised\n");
        return SNOOP_FAILURE;
    }
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "vlan update message \r\n");
        return SNOOP_FAILURE;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message queue \r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_UPDATE_VLAN_PORTS;
    pNode->u4Instance = u4Instance;

    SNOOP_OUTER_VLAN (pNode->uInMsg.VlanPortListMac.VlanId) = VlanId;

    /* Copy Mcast Addr only for Multicast updations. 
     * Will be NULL when Vlan Member Port Update comes. */
    if (VLAN_UPDT_MCAST_PORTS == u1PortType)
    {
        SNOOP_MEM_CPY (pNode->uInMsg.VlanPortListMac.McastAddr, McastAddr,
                       sizeof (tMacAddr));
    }

    SNOOP_MEM_CPY (pNode->uInMsg.VlanPortListMac.AddPortList, AddPortBitmap,
                   sizeof (tSnoopIfPortBmp));

    SNOOP_MEM_CPY (pNode->uInMsg.VlanPortListMac.DelPortList, DelPortBitmap,
                   sizeof (tSnoopIfPortBmp));

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed for forbidden"
                           " port indication\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }
        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIndicateTopoChange                              */
/*                                                                           */
/* Description        : This function is called from the STP Module          */
/*                      to indicate the spanning tree topology change by     */
/*                      sending an Event to the Snoop Task .                 */
/*                                                                           */
/* Input(s)           : u4IfIndex - The Port responsible for the topology    */
/*                                  change                                   */
/*                      VlanList-List of Vlans                               */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*****************************************************************************/
INT4
SnoopIndicateTopoChange (UINT4 u4IfIndex, tSnoopVlanList VlanList)
{
    tSnoopMsgNode      *pNode = NULL;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_TRC (SNOOP_GBL_TRC_FLAG, SNOOP_ALL_FAILURE_TRC,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                       "Failed to post message - Topology Change  \n");
        return SNOOP_FAILURE;
    }

    if (u4IfIndex > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }

    if (SnoopVcmGetContextInfoFromIfIndex (u4IfIndex,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, Failed to post message - "
                       "Topology Change \r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "topology change message \r\n");
        return SNOOP_FAILURE;
    }

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for interface index %d\r\n", u4IfIndex);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_TOPO_CHANGE;
    pNode->u4Instance = u4InstanceId;
    pNode->u2LocalPort = u2InstPort;
    pNode->uInMsg.StpTcInfo.u4Port = u4IfIndex;
    MEMCPY (pNode->uInMsg.StpTcInfo.VlanList, VlanList,
            sizeof (tSnoopVlanList));

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message release failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCreateContext                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context created, this*/
/*                          function post a message to SNOOP module for      */
/*                          context creation                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SNOOP_SUCCESS if message posting is successfll    */
/*                         else SNOOP_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCreateContext (UINT4 u4ContextId)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "context creation \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_CREATE_CONTEXT;
    pNode->u4Instance = u4ContextId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopDeleteContext                              */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context deleted, this*/
/*                          function post a message to SNOOP module for      */
/*                          context deletion                                 */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SNOOP_SUCCESS if message posting is successfll    */
/*                         else SNOOP_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopDeleteContext (UINT4 u4ContextId)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "context deletion \r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_DELETE_CONTEXT;
    pNode->u4Instance = u4ContextId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMapPort                                         */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the mapping a port to acontext           */
/*                                                                           */
/* Input(s)           : u4Context - instance Id,                             */
/*                      u4IfIndex - Interface index,                         */
/*                      u2Port  - Local port number corresponding to physical*/
/*                                port                                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopMapPort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2Port)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }
    if (u4IfIndex > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }
    if (SNOOP_SYSTEM_STATUS (u4ContextId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "map port message \r\n");
        return SNOOP_FAILURE;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "map port \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u2Port);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_MAP_PORT;
    pNode->u4Instance = u4ContextId;
    pNode->u2LocalPort = u2Port;
    pNode->uInMsg.u4Port = u4IfIndex;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed map port\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUnmapPort                                       */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u4IfIndex - The Physical Port number be unmapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopUnmapPort (UINT4 u4IfIndex)
{
    tSnoopMsgNode      *pNode = NULL;
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }
    if (u4IfIndex > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }
    if (SnoopVcmGetContextInfoFromIfIndex (u4IfIndex, &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, packet dropped\r\n");
        return SNOOP_FAILURE;
    }
    if (SNOOP_SYSTEM_STATUS (u4InstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "unmap port message \r\n");
        return SNOOP_FAILURE;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "port un-mapping \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for ifindex %d\r\n", u4IfIndex);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_UNMAP_PORT;
    pNode->u4Instance = u4InstanceId;
    pNode->u2LocalPort = u2InstPort;
    pNode->uInMsg.u4Port = u4IfIndex;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port unmap\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    L2MI_SYNC_TAKE_SEM ();
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUpdateContextName                           */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever the context name is    */
/*                          modified, this  function post a message to SNOOP */
/*                          module for update the context name.              */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SNOOP_SUCCESS if message posting is successfll    */
/*                         else SNOOP_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopUpdateContextName (UINT4 u4ContextId)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "update context name \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_UPDATE_CONTEXT_NAME;
    pNode->u4Instance = u4ContextId;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed for context name "
                           "updation.\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUpdateVlanStatus                                */
/*                                                                           */
/* Description        : This function is called from the VLAN Module         */
/*                      to post a message to IGS in order to do the foll:    */
/*                      ->flush the informations learned via bridge when     */
/*                        Vlan is Disabled in the system                     */
/*                      ->flush the informations learned via Vlan   when     */
/*                        Vlan is enabled in the system                      */
/*                                                                           */
/* Input(s)           : b1Status - VLAN_ENABLED/VLAN_DISABLED                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : VLAN Module                                          */
/*****************************************************************************/
INT4
SnoopUpdateVlanStatus (BOOL1 b1Status)
{
    UNUSED_PARAM (b1Status);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopCreatePort                                      */
/*                                                                           */
/* Description        : This function is called from the L2IWF               */
/*                      to indicate the creation of a port                   */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id mapped to the port        */
/*                      u4IfIndex     - Physical index of the port           */
/*                      u2LocalPortId - Local port id in VCM for the port    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopCreatePort (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 u2LocalPortId)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return SNOOP_FAILURE;
    }

    if (u4IfIndex > SNOOP_MAX_PORTS)
    {
        return SNOOP_SUCCESS;
    }
    if (SNOOP_SYSTEM_STATUS (u4ContextId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "create port message \r\n");
        return SNOOP_FAILURE;
    }

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "u2LocalPortId is %d\r\n", u2LocalPortId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_CREATE_PORT_MSG;
    pNode->u4Instance = u4ContextId;
    pNode->u2LocalPort = u2LocalPortId;
    pNode->uInMsg.u4Port = u4IfIndex;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);

    L2_SYNC_TAKE_SEM ();

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessMultiCastFrame                             */
/*                                                                           */
/* Description        : This function processes the incoming multicast frame */
/*                      This function will be called from Bridge module if   */
/*                      VLAN is not enabled else from VLAN module            */
/*                                                                           */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      pbrgIf   - contains the extracted Src & Dst  MAC     */
/*                                 addresses                                 */
/*                      VlanId   - Vlan Id of the packet. Will be zero if    */
/*                                 called from Bridge module                 */
/*                                                                           */
/* Output(s)          : pu2Result- IGS_FORWARD_ALL, IGS_FORWARD_SPECIFIC     */
/*                                 IGS_RELEASE_BUFFER                        */
/*                      PortList- Port List through which the packet has to  */
/*                                 be forwarded. (Valid only when result is  */
/*                                 IGS_FORWARD_SPECIFIC)                     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                            */
/*****************************************************************************/
INT4
SnoopProcessMultiCastFrame (tCRU_BUF_CHAIN_HEADER * pBuf,
                            tBrgIf * pBrgIf, tSnoopVlanId VlanId,
                            UINT2 *pu2Result, tSnoopPortList PortList)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pBrgIf);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (pu2Result);
    UNUSED_PARAM (PortList);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessMcastDataPkt                               */
/*                                                                           */
/* Description        : This function processes the incoming multicast       */
/*                      frame other than IGMP control packets                */
/*                                                                           */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      pMsgAttr - contains the extracted Src & Dst          */
/*                                 MAC addresses                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                            */
/*****************************************************************************/
INT4
SnoopProcessMcastDataPkt (tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopMsgAtt * pMsgAttr)
{
    UNUSED_PARAM (pMsgAttr);
    UNUSED_PARAM (pBuf);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetMcIgsFwdPorts                        */
/*                                                                           */
/*    Description         : This function returns Portlist, UntagPortlist    */
/*                          for a given Multicast group entry.               */
/*                          The port list will be retrieved from             */
/*                          1. IGS  - If Igs forwarding type is IPMC         */
/*                          1. VLAN - If Igs forwarding type is L2MC         */
/*                                                                           */
/*    Input(s)            : VlanId  - Vlan Id                                */
/*                          u4GrpAddr - Group address                        */
/*                          u4SrcAddr - Group Source address                 */
/*                                                                           */
/*    Output(s)           : PortList - Port list for this MC group to return */
/*                          UntagPortList - Untag port list                  */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopUtilGetMcIgsFwdPorts (tSnoopVlanId VlanId, UINT4 u4GrpAddr,
                           UINT4 u4SrcAddr, tPortList PortList,
                           tPortList UntagPortList)
{
    tSnoopTag           SnoopVlanId;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tSnoopTag));

    SNOOP_OUTER_VLAN (SnoopVlanId) = VlanId;

    return SnoopUtilMiGetMcIgsFwdPorts (SNOOP_DEFAULT_INSTANCE, SnoopVlanId,
                                        u4GrpAddr, u4SrcAddr, PortList,
                                        UntagPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetMcMldsFwdPorts                       */
/*                                                                           */
/*    Description         : This function returns Portlist, UntagPortlist    */
/*                          for a given Multicast group entry.               */
/*                          The port list will be retrieved from             */
/*                          1. MLDS - If Mlds forwarding type is IPMC        */
/*                          1. VLAN - If Mlds forwarding type is L2MC        */
/*                                                                           */
/*    Input(s)            : VlanId  - Vlan Id                                */
/*                          u4GrpAddr - Group address                        */
/*                          u4SrcAddr - Group Source address                 */
/*                                                                           */
/*    Output(s)           : PortList - Port list for this MC group to return */
/*                          UntagPortList - Untag port list                  */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopUtilGetMcMldsFwdPorts (tSnoopVlanId VlanId,
                            UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                            tPortList PortList, tPortList UntagPortList)
{
    tSnoopTag           SnoopVlanId;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tSnoopTag));

    SNOOP_OUTER_VLAN (SnoopVlanId) = VlanId;

    return SnoopUtilGetMcastMldsIpFwdPorts (SNOOP_DEFAULT_INSTANCE, SnoopVlanId,
                                            pu1GrpAddr, pu1SrcAddr, PortList,
                                            UntagPortList);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopIsRtrPortConfigured                         */
/*                                                                           */
/*    Description         : This function checks if static router ports are  */
/*                          configured in IGS.                               */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance Id                         */
/*                          u1AddrType- Address type ( IPV4 or IPV6)         */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS - If router port is configured.    */
/*                          SNOOP_FAILURE - If router port is not configured */
/*****************************************************************************/

INT4
SnoopIsRtrPortConfigured (UINT4 u4Instance, UINT1 u1AddrType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;

    SNOOP_LOCK ();
    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        SNOOP_UNLOCK ();
        return SNOOP_FAILURE;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_UNLOCK ();
        return SNOOP_FAILURE;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        pRBElem = pRBElemNext;

        if (pSnoopVlanEntry->u1AddressType != u1AddrType)
        {
            continue;
        }

        /* If the static router ports are configured in any of the VLAN in the 
         * context, return SUCCESS, else FAILURE.
         */
        if (((SNOOP_MEM_CMP (pSnoopVlanEntry->StaticRtrPortBitmap,
                             gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0))
            || (SNOOP_MEM_CMP (pSnoopVlanEntry->pSnoopVlanCfgEntry->
                               CfgStaticRtrPortBmp, gNullPortBitMap,
                               SNOOP_PORT_LIST_SIZE) != 0))
        {
            SNOOP_UNLOCK ();
            return SNOOP_SUCCESS;

        }
    }
    SNOOP_UNLOCK ();
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopMainDisableSnooping                             */
/*                                                                           */
/* Description        : This function will be called when manager disables   */
/*                      IGMP/MLD snooping module in the system and releases  */
/*                      all the memory blocks alloted for table entries to   */
/*                      the respective memory pools                          */
/*                                                                           */
/* Input(s)           : u1AddressType - specifies IGS/MLDS                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMainDisableSnooping (UINT4 u4Instance, UINT1 u1AddressType)
{
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_ADDRESS_TYPE (u1AddressType);

    if (SNOOP_SNOOPING_STATUS (u4Instance, u1AddressType - 1) == SNOOP_DISABLE)
    {
        SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_INIT_NAME,
                            "Already disabled for this instance %d and the address "
                            "type %d\r\n", u4Instance, u1AddressType);
        return;
    }

    SNOOP_SNOOPING_STATUS (u4Instance, u1AddressType - 1) = SNOOP_DISABLE;
    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                        SNOOP_DBG_INIT,
                        SNOOP_INIT_NAME,
                        "IGMP Snooping disabled for instance %u\r\n",
                        u4Instance);

#ifdef L2RED_WANTED
    /* Stop IGS Switchover timer if it is running */
    if (SnoopTmrGetRemainingTime (&SNOOP_RED_SWITCHOVER_TMR_NODE ())
        == SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RED | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_RED_NAME,
                       "SNOOP Switchover timer is running. "
                       "Stopped forcefully...\n");
        SnoopTmrStopTimer (&SNOOP_RED_SWITCHOVER_TMR_NODE ());

        SNOOP_RED_GENERAL_QUERY_INPROGRESS = SNOOP_FALSE;
        SNOOP_RED_SWITCHOVER_TMR_NODE ().u1TimerType = SNOOP_INVALID_TIMER_TYPE;
    }

    /* Stop the Src List Clear timer if it is running */
    if (SnoopTmrGetRemainingTime
        (&SNOOP_INSTANCE_INFO (u4Instance)->SrcListCleanUpTmr) == SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RED,
                       SNOOP_RED_NAME,
                       "SNOOP Src List Cleanup timer is running. "
                       "Stopped forcefully...\n");
        SnoopTmrStopTimer (&SNOOP_INSTANCE_INFO (u4Instance)->
                           SrcListCleanUpTmr);
    }
#endif /* L2RED_WANTED */

    /* delete all dynamically learnt forwarding information the multicast 
     * table entries 
     */
    SnoopFwdDeleteMcastFwdEntries (u4Instance, u1AddressType);

    /* Delete all dynamically learnt Group membership information */
    SnoopGrpDeleteGroupEntries (u4Instance, u1AddressType);

    /* Delete all dynamic VLAN and Router port and statistics information */
    SnoopVlanDeleteVlanEntries (u4Instance, u1AddressType);
    /* Disable Sparse Mode when disabling Snooping Globally */
    if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
    {
        if (SnoopUtilHandleSparseMode (u4Instance, SNOOP_DISABLE) !=
            SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GBL_NAME,
                           "Unable to change the Sparse Mode\r\n");
        }
    }

    /* Nullify the Global Source Information Buffer */
    if (gpSSMSrcInfoBuffer != NULL)
    {
        SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);
    }

#ifdef NPAPI_WANTED
#ifdef IGS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
        {
            if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
            {
                if (IgsFsMiIgsHwSparseMode
                    (u4Instance, SNOOP_DISABLE) == FNP_FAILURE)
                {
                    SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_MGMT,
                                   SNOOP_MGMT_DBG,
                                   "H/W updation for sparse mode is failed \r\n");
                }
            }
            if (SnoopHwDisableIgmpSnooping (u4Instance) == SNOOP_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_INIT_NAME,
                               "Unable to disable IGMP Snooping in hardware\r\n");
            }
        }
    }
#endif

#ifdef MLDS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
        {
            if (SnoopHwDisableMldSnooping (u4Instance) == SNOOP_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_INIT_NAME,
                               "Unable to disable MLD Snooping in hardware\r\n");
            }
        }
    }
#endif

#endif

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopModuleShutdown                                  */
/*                                                                           */
/* Description        : This is an API to shutdown the SNOOPING module       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopModuleShutdown (VOID)
{
    SNOOP_LOCK ();
    if (SnoopMainModuleShutdown () == SNOOP_FAILURE)
    {
        SNOOP_UNLOCK ();
        return SNOOP_FAILURE;
    }
    SNOOP_UNLOCK ();
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopStartModule                                     */
/*                                                                           */
/* Description        : This is an API to start the SNOOPING module          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopStartModule (VOID)
{
    SNOOP_LOCK ();
    if (SnoopMainStartModule () == SNOOP_FAILURE)
    {
        SNOOP_UNLOCK ();
        return SNOOP_FAILURE;
    }
    SNOOP_UNLOCK ();
    return SNOOP_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      :     IgsNpCallBack                                    */
/*                                                                           */
/* Description        :  This function is called by HW module to update      */
/*                       the software with the return value of the           */
/*                       HW call                                             */
/*                                                                           */
/*                                                                           */
/* Input(s)           :  u4NpCallId - The callId denoting the HW call        */
/*                       lv - The union returned by the NP sim module        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  none                                                */
/*                                                                           */
/*****************************************************************************/

VOID
IgsNpCallBack (UINT4 u4NpCallId, unAsyncNpapi * lv)
{
    tSnoopMsgNode      *pNode = NULL;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        return;
    }
    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for message posting -"
                       "context deletion \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4NpCallId %d \r\n", u4NpCallId);
        return;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_NP_CALLBACK;

    pNode->uInMsg.IpmcEntry.u4npCallId = u4NpCallId;

    if (u4NpCallId == AS_FS_MI_NP_UPDATE_IPMC_FWD_ENTRIES)
    {
        pNode->uInMsg.IpmcEntry.u4Instance =
            lv->FsMiNpUpdateIpmcFwdEntries.u4ContextId;
        pNode->uInMsg.IpmcEntry.u4GrpAddr =
            lv->FsMiNpUpdateIpmcFwdEntries.u4GrpAddr;
        pNode->uInMsg.IpmcEntry.u4SrcAddr =
            lv->FsMiNpUpdateIpmcFwdEntries.u4SrcAddr;
        MEMCPY (pNode->uInMsg.IpmcEntry.PortList,
                lv->FsMiNpUpdateIpmcFwdEntries.PortList, sizeof (tPortList));
        MEMCPY (pNode->uInMsg.IpmcEntry.UntagPortList,
                lv->FsMiNpUpdateIpmcFwdEntries.UntagPortList,
                sizeof (tPortList));
        pNode->uInMsg.IpmcEntry.u1EventType =
            lv->FsMiNpUpdateIpmcFwdEntries.u1EventType;
        pNode->uInMsg.IpmcEntry.rval = lv->FsMiNpUpdateIpmcFwdEntries.rval;
        pNode->uInMsg.IpmcEntry.OuterVlanId =
            lv->FsMiNpUpdateIpmcFwdEntries.VlanId;
        pNode->uInMsg.IpmcEntry.InnerVlanId = 0;
    }
    else if (u4NpCallId == AS_FS_MI_IGS_HW_UPDATE_IPMC_ENTRY)
    {
        pNode->uInMsg.IpmcEntry.u4Instance =
            lv->FsMiIgsHwUpdateIpmcEntry.u4Instance;
        pNode->uInMsg.IpmcEntry.u4GrpAddr =
            lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.u4GrpAddr;
        pNode->uInMsg.IpmcEntry.u4SrcAddr =
            lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.u4SrcAddr;

        MEMCPY (pNode->uInMsg.IpmcEntry.PortList,
                lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.PortList,
                sizeof (tPortList));
        MEMCPY (pNode->uInMsg.IpmcEntry.UntagPortList,
                lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.UntagPortList,
                sizeof (tPortList));

        pNode->uInMsg.IpmcEntry.u1EventType =
            lv->FsMiIgsHwUpdateIpmcEntry.u1Action;
        pNode->uInMsg.IpmcEntry.rval = lv->FsMiIgsHwUpdateIpmcEntry.rval;

        pNode->uInMsg.IpmcEntry.OuterVlanId =
            lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.OuterVlanId;
        pNode->uInMsg.IpmcEntry.InnerVlanId =
            lv->FsMiIgsHwUpdateIpmcEntry.IgsHwIpFwdInfo.InnerVlanId;
    }

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed port delete\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }

        return;
    }

    if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Sending SNOOP_MSG_EVENT to SNOOP Task failed\r\n");
        return;
    }

    return;
}
#endif
/*****************************************************************************/
/* Function Name      : SnoopCreateVlanIndication                            */
/*                                                                           */
/* Description        : This function is called from the L2IWF Module        */
/*                      to indicate the creation of a VLAN for a given       */
/*                      Instance.                                            */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4Instance - Instance for which the operation        */
/*                                   should be done                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*****************************************************************************/
INT4
SnoopCreateVlanIndication (UINT4 u4Instance, tSnoopVlanId VlanId)
{
    tSnoopMsgNode      *pNode = NULL;
    tSnoopTag           SnoopVlanId;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tSnoopTag));

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised\n");
        return SNOOP_FAILURE;
    }
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post Snoop "
                       "vlan delete message \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_OUTER_VLAN (SnoopVlanId) = VlanId;
    SNOOP_INNER_VLAN (SnoopVlanId) = SNOOP_INVALID_VLAN_ID;

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_VLAN_CREATE_EVENT;
    SNOOP_OUTER_VLAN (pNode->uInMsg.VlanId) = VlanId;
    pNode->u4Instance = u4Instance;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Queue message realease failed in Vlan deletion event\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_PKT_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_QUEUE_RELEASE_FAIL],
                                   "for queue id %d\r\n", SNOOP_MSG_QUEUE_ID);
        }
        return SNOOP_FAILURE;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeleteFwdAndGroupTableDynamicInfo               */
/*                                                                           */
/* Description        : This function deletes the multicast                  */
/*                      entries for a MAC address which is configured        */
/*                      as a Wild card                                       */
/*                                                                           */
/* Input(s)           : u4ContextId      - context Id                        */
/*                      MacAddr          - Mac address                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopDeleteFwdAndGroupTableDynamicInfo (UINT4 u4ContextId, tMacAddr MacAddr)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopFilter        SnoopDelEntry;
    UINT4               u4Instance = 0;
    UINT4               u4Context = 0;

    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));

    u4Context = SNOOP_DEFAULT_INSTANCE;

    if (u4ContextId != SNOOP_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        u4Context = u4ContextId;
    }

    u4Instance = SNOOP_GET_INSTANCE_ID (u4Context);

    /* If the forwarding mode is MAC based forwarding mode */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry != NULL)
        {
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                MacMcastFwdEntry);

            while (pRBElem != NULL)
            {
                pRBNextElem =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   MacMcastFwdEntry, pRBElem, NULL);

                pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

                if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->MacGroupAddr,
                                   MacAddr, sizeof (tMacAddr)) != 0)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }

                /* Delete the list of pointers to the group entry */
                while ((pSnoopGroupEntry = (tSnoopGroupEntry *)
                        SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->GroupEntryList))
                       != NULL)
                {
                    if ((SnoopGrpDeleteGroupEntry (u4Instance,
                                                   pSnoopMacGrpFwdEntry->VlanId,
                                                   pSnoopGroupEntry->
                                                   GroupIpAddr)) !=
                        SNOOP_SUCCESS)
                    {
                        continue;
                    }
                }

                SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                              pSnoopMacGrpFwdEntry,
                                              SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);

                SNOOP_MEM_SET (pSnoopMacGrpFwdEntry->PortBitmap, 0,
                               SNOOP_PORT_LIST_SIZE);

                /* Indicate the aging out of MAC  forwarding entry */
                SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                               pSnoopMacGrpFwdEntry->VlanId,
                               sizeof (tSnoopTag));
                SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                               pSnoopMacGrpFwdEntry->MacGroupAddr,
                               SNOOP_MAC_ADDR_LEN);
                SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if ((pSnoopInstInfo != NULL) && (pSnoopMacGrpFwdEntry != NULL))
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                              MacMcastFwdEntry,
                              (tRBElem *) pSnoopMacGrpFwdEntry);

                SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                pSnoopMacGrpFwdEntry);

                pRBElem = pRBNextElem;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopVlanSetForwardUnregPorts                    */
/*                                                                           */
/*    Description         : This function is called to update the Router Port*/
/*                          list of IGS into Vlan Forwarding Database if IGS */
/*                          IGS is enabled.                                  */
/*                          If MLDS is enabled it just returns SNOOP_SUCCESS */
/*    Input(s)            : u4VlanIndex - Vlan Identifier                    */
/*                          pPortList - Port List                            */
/*                                                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS                                     */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopVlanSetForwardUnregPorts (UINT4 u4VlanIndex, tPortList * pPortList)
{
#ifdef IGS_WANTED
    VlanSetForwardUnregPortsFromIgs (u4VlanIndex, pPortList);
#endif
#ifdef MLDS_WANTED
    UNUSED_PARAM (u4VlanIndex);
    UNUSED_PARAM (pPortList);
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnpApiIndicateIpAddrChange                           */
/*                                                                           */
/* Description        : This function is called from the CFA Module          */
/*                      to indicate the IP Address change of the          */
/*                      Vlan interface to sending an Event to the Snoop Task.*/
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
SnpApiIndicateIpAddrChange (tNetIpv4IfInfo * pIpIfInfo, UINT1 u1Bitmap)
{
    tSnoopMsgNode      *pNode = NULL;

    if (u1Bitmap & OPER_STATE)
    {
        if (gu1SnoopInit == SNOOP_FALSE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                           "Failed to post message - IP Address Change  \n");
            return SNOOP_FAILURE;
        }

        if (SNOOP_SYSTEM_STATUS (gu1SnoopInstanceId) == SNOOP_SHUTDOWN)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (gu1SnoopInstanceId),
                           SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GBL_NAME,
                           "Snooping module is shutdown. Cannot Post Snoop "
                           "IP Address change message \r\n");
            return SNOOP_FAILURE;
        }

        if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (gu1SnoopInstanceId),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Memory allocation failed for queue message\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "fro u1Bitmap %d\r\n", u1Bitmap);
            return SNOOP_FAILURE;
        }

        SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

        pNode->u2MsgType = SNOOP_IPADDR_CHANGE;
        pNode->u4Instance = gu1SnoopInstanceId;
        pNode->u4Addr = pIpIfInfo->u4Addr;
        pNode->u2IntfVlan = pIpIfInfo->u2VlanId;
        pNode->u4OperStatus = pIpIfInfo->u4Oper;

        if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                                 SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)

        {
            if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (gu1SnoopInstanceId),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_MEM_NAME,
                               "Queue message release failed IP Address change\n");
            }

            return SNOOP_FAILURE;
        }

        SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopNotifyCardRemovalEvent                          */
/*                                                                           */
/* Description        : This function is called from the LA  Module          */
/*                      after processing the card removal event              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopNotifyCardRemovalEvent (VOID)
{
#ifdef L2RED_WANTED
    gu1SnoopLaCardRemovalProcessed = SNOOP_TRUE;
    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_RED_GO_ACTIVE_EVENT);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopNotifyMcastMode                                    */
/*                                                                           */
/* Description        : This function is called from the RM  Module          */
/*                      to Pre Sync up the SNOOP MODE in Nvram               */
/*                                                                           */
/* Input(s)           : u4SnoopFwdMode - IP or MAC  Mode                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopNotifyMcastMode (UINT4 u4SnoopFwdMode)
{

    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    MEMSET (&SnmpNotifyInfo, SNOOP_ZERO, sizeof (tSnmpNotifyInfo));

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsSnoopInstanceGlobalMcastFwdMode,
                          u4SeqNum, FALSE, SnpLock, SnpUnLock, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", u4SnoopFwdMode));

}

/*****************************************************************************/
/* Function Name      : SnoopInformRestorationComplete                       */
/*                                                                           */
/* Description        : This function is called from MSR module to indicate  */
/*                      the restoration process (MSR(                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopInformRestorationComplete (VOID)
{
    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised"
                       "Failed to post MSR complete notification message  \n");
        return;
    }

    if (SNOOP_SYSTEM_STATUS (gu1SnoopInstanceId) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (gu1SnoopInstanceId),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Snooping module is shutdown. Cannot Post MSR "
                       "completion notification message \r\n");
        return;
    }

    SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSR_COMPLETE_EVENT);
    return;
}
