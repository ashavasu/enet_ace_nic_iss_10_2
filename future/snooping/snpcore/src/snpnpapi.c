/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpnpapi.c,v 1.31 2017/08/11 13:52:37 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : snpnpapi.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        :  Snooping                                      */
/*    MODULE NAME           :  snooping Processing                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains functions that interface    */
/*                            with hardware                                  */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

#ifdef IGS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopHwEnableIgmpSnooping                            */
/*                                                                           */
/* Description        : This function enables IGMP Snooping functionality    */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT1
SnoopHwEnableIgmpSnooping (UINT4 u4Instance)
{
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        /* Enable MAC based Forwarding at Hardware */
        if (IgsFsMiIgsHwEnableIgmpSnooping (u4Instance) == FNP_FAILURE)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_NP_NAME,
                           "Unable to enable IGS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_IGS_SNOOP_NP_ENABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP,
                            SNOOP_NP_NAME,
                            "Successfully enabled IGS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    else
    {
        /* Enable IP based Forwarding at Hardware */
        if (IgsFsMiIgsHwEnableIpIgmpSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_NP_NAME,
                           "Unable to enable IGS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_IGS_SNOOP_NP_ENABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP,
                            SNOOP_NP_NAME,
                            "Successfully enabled IGS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    return SNOOP_SUCCESS;
}
#endif

#ifdef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopHwEnableMldSnooping                             */
/*                                                                           */
/* Description        : This function enables MLD Snooping functionality     */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT1
SnoopHwEnableMldSnooping (UINT4 u4Instance)
{
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        /* Enable MAC based Forwarding at Hardware */
        if (MldsFsMiMldsHwEnableMldSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_DBG (SNOOP_MLDS_TRC_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                       "Unable to enable MLDS in Hardware for instance \r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_NP_NAME,
                            "Successfully enabled MLDS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    else
    {
        /* Enable IP based Forwarding at Hardware */
        if (MldsFsMiMldsHwEnableIpMldSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_DBG (SNOOP_MLDS_TRC_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                       "Unable to enable MLDS in Hardware for instance \r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_NP_NAME,
                            "Successfully enabled MLDS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    return SNOOP_SUCCESS;
}
#endif

#ifdef IGS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopHwDisableIgmpSnooping                           */
/*                                                                           */
/* Description        : This function disables IGMP Snooping functionality   */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT1
SnoopHwDisableIgmpSnooping (UINT4 u4Instance)
{
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        /* Disable MAC based Forwarding at Hardware */
        if (IgsFsMiIgsHwDisableIgmpSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE, SNOOP_NP_NAME,
                           "Unable to disable IGS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_IGS_SNOOP_NP_DISABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP,
                            SNOOP_NP_NAME,
                            "Successfully disabled IGS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    else
    {
        /* Disable IP based Forwarding at Hardware */
        if (IgsFsMiIgsHwDisableIpIgmpSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_NP_NAME,
                           "Unable to disable IGS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_IGS_SNOOP_NP_DISABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP,
                            SNOOP_NP_NAME,
                            "Successfully disabled IGS in Hardware for instance %d \r\n",
                            u4Instance);
    }

    return SNOOP_SUCCESS;
}
#endif

#ifdef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : SnoopHwDisableMldSnooping                           */
/*                                                                           */
/* Description        : This function disables MLD Snooping functionality   */
/*                      at the hardware when desired                         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT1
SnoopHwDisableMldSnooping (UINT4 u4Instance)
{
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        /* Disable MAC based Forwarding at Hardware */
        if (MldsFsMiMldsHwDisableMldSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_DBG (SNOOP_MLDS_TRC_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                       "Unable to disable MLDS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_MLD_SNOOP_NP_DISABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_NP_NAME,
                            "Successfully disabled MLDS in Hardware for instance %d \r\n",
                            u4Instance);
    }
    else
    {
        /* Disable IP based Forwarding at Hardware */
        if (MldsFsMiMldsHwDisableIpMldSnooping (u4Instance) == FNP_FAILURE)
        {
            SNOOP_DBG (SNOOP_MLDS_TRC_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                       "Unable to disable MLDS in Hardware for instance \r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_NP, SNOOP_NP_DBG,
                                   SnoopSysErrString
                                   [SYS_LOG_MLD_SNOOP_NP_DISABLE_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_NP_NAME,
                            "Successfully disabled MLDS in Hardware for instance %d \r\n",
                            u4Instance);
    }

    return SNOOP_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : SnoopNpUpdateMcastFwdEntry                             */
/*                                                                           */
/* Description        : This function updates the hardware multicast         */
/*                      forwarding entries                                   */
/*                      (Creation , Deletion and Port Bitmap Updation)       */
/*                                                                           */
/* Input(s)           : u4Instance   -  Instance number                      */
/*                      VlanId       -  Vlan Identifier                      */
/*                      SrcAddr    - Source IP Address                     */
/*                      GrpAddr    - Group IP Address                      */
/*                      u4PortBitmap - Port Bitmap                           */
/*                      u1EventType  - (Create/Delete/add port/remove port)  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                            */
/*****************************************************************************/
INT4
SnoopNpUpdateMcastFwdEntry (UINT4 u4Instance, tSnoopTag VlanId,
                            tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                            tSnoopPortBmp PortBitmap, UINT1 u1EventType)
{
    tIgsHwIpFwdInfo     IgsHwIpFwdInfo;
    tPortList           PortList;
    tPortList           UntagPortList;
    tPortList           TempPortList;
    tPortList           TempUntagPortList;
    tSnoopIfPortList    SnpPortList;
    tSnoopIfPortList    SnpUntagPortList;
    INT4                i4RetVal = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&IgsHwIpFwdInfo, 0, sizeof (tIgsHwIpFwdInfo));
    SNOOP_MEM_SET (&PortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&UntagPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&TempPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&TempUntagPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&SnpPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&SnpUntagPortList, 0, SNOOP_IF_PORT_LIST_SIZE);

    if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_FALSE)
    {
        return (SNOOP_SUCCESS);
    }

    /* If event is LA port update, then its special case of
     * giving member port updation of portchannel for h/w sync up.
     * Since member port is not visible
     * to IGS, this function will fail. So dont check with IGS
     * database
     */

    if ((u1EventType != SNOOP_ADD_LA_PORT) &&
        (u1EventType != SNOOP_DEL_LA_PORT))
    {

        /*SNOOPING will be maintaining the virtual port bitmap corresponding to 
         * the physical port bitmap obtained from the multiple instance manager
         */

        if (SnoopGetPhyPortBmp (u4Instance, PortBitmap, SnpPortList)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Unable to get the physical port bitmap from the virtual"
                           " port bitmap for index \r\n");
            return SNOOP_FAILURE;
        }
    }

    IgsHwIpFwdInfo.OuterVlanId = SNOOP_OUTER_VLAN (VlanId);
    IgsHwIpFwdInfo.InnerVlanId = SNOOP_INNER_VLAN (VlanId);
    IgsHwIpFwdInfo.u4GrpAddr = SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr);
    IgsHwIpFwdInfo.u4SrcAddr = SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr);

    /* In metro package, the size of tPortList and tSnoopIfPortList
     * varies. tPortList (1308 bytes) and tSnoopIfPortList (128 bytes). 
     * Hence, for interfacing with the npapi a local variable
     * is used to obtain the providing the portlist. The portlist is
     * filled only the 1st 128 bytes as the snooping module 
     * is unaware of the VIP configurations */

    if ((u1EventType == SNOOP_ADD_LA_PORT)
        || (u1EventType == SNOOP_DEL_LA_PORT))
    {
        SNOOP_MEM_CPY (PortList, PortBitmap, sizeof (tSnoopPortBmp));
    }
    else
    {
        SNOOP_MEM_CPY (PortList, SnpPortList, sizeof (tSnoopIfPortList));
    }
    SNOOP_MEM_CPY (UntagPortList, SnpUntagPortList, sizeof (tSnoopIfPortList));

    switch (u1EventType)
    {
        case SNOOP_CREATE_FWD_ENTRY:

#ifdef IGS_WANTED

            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {

                /* Get the actual port list for this Group and 
                 * pass it to NP for programming */

                i4RetVal =
                    SnoopUtilGetMcastIgsIpFwdPorts (u4Instance, VlanId,
                                                    SNOOP_PTR_FETCH_4 (GrpAddr.
                                                                       au1Addr),
                                                    SNOOP_PTR_FETCH_4 (SrcAddr.
                                                                       au1Addr),
                                                    PortList, UntagPortList);

                /* For Event type create entry, the return value from 
                 * IgsGetMcastIpFwdPorts should not be considered, as 
                 * it may return failure from VlanIgsGetTxPortList because
                 * of NULL portlist in IGS IpFwd entry at creation time.
                 * The portlist will be updated thru ADD_PORT event from IGS.
                 */

                if (i4RetVal == SNOOP_FAILURE)
                {
                    SNOOP_GBL_DBG_ARG4 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                        " V4 Multicast forward entry port retrieval from Vlan for"
                                        " OuterVlan %d Source %d, Group %d "
                                        "InnerVlan %d failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                                        SNOOP_INNER_VLAN (VlanId));
                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_CPY (TempPortList, PortList, sizeof (tPortList));
                SNOOP_MEM_CPY (TempUntagPortList, UntagPortList,
                               sizeof (tPortList));

                /* If enhanced mode is disabled, program IPMC table
                   in H/w using outer vlan 
                 */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
                {
                    if (IgsFsMiNpUpdateIpmcFwdEntries
                        (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr), PortList,
                         UntagPortList, SNOOP_HW_CREATE_ENTRY) != FNP_SUCCESS)
                    {
                        SNOOP_DBG_ARG3 (SNOOP_IGS_DBG_FLAG,
                                        SNOOP_CONTROL_PATH_TRC, SNOOP_FWD_TRC,
                                        "V4 Multicast forward entry creation in hardware for"
                                        " Vlan %d Source %x, Group %x failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
                        SnoopHandleGroupCreateFailure (VlanId, GrpAddr, SrcAddr,
                                                       u4Instance);
                        return SNOOP_FAILURE;
                    }
                }

                SNOOP_MEM_CPY (PortList, TempPortList, sizeof (tPortList));
                SNOOP_MEM_CPY (UntagPortList, TempUntagPortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.PortList, PortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList,
                               sizeof (tPortList));

                if (IgsFsMiIgsHwUpdateIpmcEntry
                    (u4Instance, &IgsHwIpFwdInfo,
                     SNOOP_HW_CREATE_ENTRY) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG4
                        (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                         SNOOP_FWD_TRC,
                         "Multicast forward entry creation in hardware for"
                         " OuterVlan %d Source %x, Group %x InnerVlan %d "
                         "failed\r\n", SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_INNER_VLAN (VlanId));

                    SnoopHandleGroupCreateFailure (VlanId, GrpAddr, SrcAddr,
                                                   u4Instance);

                    return SNOOP_SUCCESS;

                }
            }
#endif

#ifdef MLDS_WANTED

            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {

                i4RetVal =
                    SnoopUtilGetMcastMldsIpFwdPorts (u4Instance, VlanId,
                                                     GrpAddr.au1Addr,
                                                     SrcAddr.au1Addr, PortList,
                                                     UntagPortList);

                if (i4RetVal == SNOOP_FAILURE)
                {
                    SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                        " V6 Multicast forward entry port retrieval from Vlan for"
                                        " Vlan %d Source %s, Group %s failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SnoopPrintIPvxAddress (SrcAddr),
                                        SnoopPrintIPvxAddress (GrpAddr));
                    return SNOOP_FAILURE;
                }

                if (MldsFsMiNpUpdateIp6mcFwdEntries
                    (u4Instance, SNOOP_OUTER_VLAN (VlanId), GrpAddr.au1Addr,
                     SrcAddr.au1Addr, PortList, UntagPortList,
                     SNOOP_HW_CREATE_ENTRY) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG3 (SNOOP_MLDS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_FWD_TRC,
                                    "V6 Multicast forward entry creation in hardware for"
                                    " Vlan %d Source %s, Group %s failed\r\n",
                                    SNOOP_OUTER_VLAN (VlanId),
                                    SnoopPrintIPvxAddress (SrcAddr),
                                    SnoopPrintIPvxAddress (GrpAddr));
                    return SNOOP_FAILURE;
                }
            }
#endif

            gu2MaxHwEntryCount++;
            break;

        case SNOOP_DELETE_FWD_ENTRY:

#ifdef IGS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_MEM_CPY (TempPortList, PortList, sizeof (tPortList));
                SNOOP_MEM_CPY (TempUntagPortList, UntagPortList,
                               sizeof (tPortList));

                /* If enhanced mode is disabled, program IPMC table
                   in H/w using outer vlan 
                 */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
                {
                    if (IgsFsMiNpUpdateIpmcFwdEntries
                        (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr), PortList,
                         UntagPortList, SNOOP_HW_DELETE_ENTRY) != FNP_SUCCESS)
                    {
                        SNOOP_DBG_ARG3 (SNOOP_IGS_DBG_FLAG,
                                        SNOOP_CONTROL_PATH_TRC, SNOOP_FWD_TRC,
                                        "V4 Multicast forward entry deletion in hardware"
                                        " for Vlan %d Source %x, Group %x failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
                        SnoopHandleGroupDeleteFailure (VlanId, GrpAddr, SrcAddr,
                                                       u4Instance);
                        return SNOOP_SUCCESS;
                    }

                }

                SNOOP_MEM_CPY (PortList, TempPortList, sizeof (tPortList));
                SNOOP_MEM_CPY (UntagPortList, TempUntagPortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.PortList, PortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList,
                               sizeof (tPortList));

                if (IgsFsMiIgsHwUpdateIpmcEntry
                    (u4Instance, &IgsHwIpFwdInfo,
                     SNOOP_HW_DELETE_ENTRY) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG4
                        (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                         SNOOP_FWD_TRC,
                         "V4 Multicast forward entry deletion in hardware"
                         " for OuterVlan %d Source %x, Group %x "
                         "InnerVlan %d failed\r\n",
                         SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_INNER_VLAN (VlanId));
                    SnoopHandleGroupDeleteFailure (VlanId, GrpAddr, SrcAddr,
                                                   u4Instance);
                    return SNOOP_SUCCESS;
                }
            }
#endif

#ifdef MLDS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                if (MldsFsMiNpUpdateIp6mcFwdEntries
                    (u4Instance, SNOOP_OUTER_VLAN (VlanId), GrpAddr.au1Addr,
                     SrcAddr.au1Addr, PortList, UntagPortList,
                     SNOOP_HW_DELETE_ENTRY) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG3 (SNOOP_MLDS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_FWD_TRC,
                                    "V6 Multicast forward entry deletion in hardware"
                                    " for Vlan %d Source %s, Group %s failed\r\n",
                                    SNOOP_OUTER_VLAN (VlanId),
                                    SnoopPrintIPvxAddress (SrcAddr),
                                    SnoopPrintIPvxAddress (GrpAddr));
                    return SNOOP_FAILURE;
                }
            }
#endif

            gu2MaxHwEntryCount--;
            break;

        case SNOOP_ADD_PORT:
        case SNOOP_ADD_LA_PORT:

            /* Get the actual port list for this Group and 
             * pass it to NP for programming */

#ifdef IGS_WANTED

            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {

                i4RetVal = SnoopUtilGetMcastIgsIpFwdPorts
                    (u4Instance, VlanId, SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                     SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr), PortList,
                     UntagPortList);

                if (i4RetVal == SNOOP_FAILURE)
                {
                    SNOOP_GBL_DBG_ARG4 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                        "V4 Multicast forward entry port retrieval from Vlan for"
                                        " OuterVlan %d Source %x, Group %x "
                                        "InnerVlan %d failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                                        SNOOP_INNER_VLAN (VlanId));

                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_CPY (TempPortList, PortList, sizeof (tPortList));
                SNOOP_MEM_CPY (TempUntagPortList, UntagPortList,
                               sizeof (tPortList));
                /* If enhanced mode is disabled, program IPMC table
                   in H/w using outer vlan 
                 */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
                {
                    if (IgsFsMiNpUpdateIpmcFwdEntries
                        (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr), PortList,
                         UntagPortList, SNOOP_HW_ADD_PORT) != FNP_SUCCESS)
                    {
                        SNOOP_DBG_ARG3 (SNOOP_IGS_DBG_FLAG,
                                        SNOOP_CONTROL_PATH_TRC, SNOOP_FWD_TRC,
                                        "V4 Multicast forward entry port additon in hardware for"
                                        " Vlan %d Source %x, Group %x failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
                        SnoopHandleAddPortFailure (VlanId, GrpAddr, SrcAddr,
                                                   u4Instance);
                        return SNOOP_SUCCESS;
                    }

                }

                SNOOP_MEM_CPY (PortList, TempPortList, sizeof (tPortList));
                SNOOP_MEM_CPY (UntagPortList, TempUntagPortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.PortList, PortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList,
                               sizeof (tPortList));

                if (IgsFsMiIgsHwUpdateIpmcEntry
                    (u4Instance, &IgsHwIpFwdInfo,
                     SNOOP_HW_ADD_PORT) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG4
                        (SNOOP_IGS_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                         SNOOP_FWD_TRC,
                         "V4 Multicast forward entry port additon in hardware "
                         " for OuterVlan %d Source %x, Group %x "
                         "InnerVlan %d failed\r\n",
                         SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_INNER_VLAN (VlanId));
                    SnoopHandleAddPortFailure (VlanId, GrpAddr, SrcAddr,
                                               u4Instance);
                    return SNOOP_SUCCESS;
                }
            }
#endif

#ifdef MLDS_WANTED

            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {

                i4RetVal = SnoopUtilGetMcastMldsIpFwdPorts
                    (u4Instance, VlanId, GrpAddr.au1Addr, SrcAddr.au1Addr,
                     PortList, UntagPortList);

                if (i4RetVal == SNOOP_FAILURE)
                {
                    SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                        "V6 Multicast forward entry port retrieval from Vlan for"
                                        " Vlan %d Source %s, Group %s failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SnoopPrintIPvxAddress (SrcAddr),
                                        SnoopPrintIPvxAddress (GrpAddr));

                    return SNOOP_FAILURE;
                }

                if (MldsFsMiNpUpdateIp6mcFwdEntries
                    (u4Instance, SNOOP_OUTER_VLAN (VlanId), GrpAddr.au1Addr,
                     SrcAddr.au1Addr, PortList, UntagPortList,
                     SNOOP_HW_ADD_PORT) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG3 (SNOOP_MLDS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_FWD_TRC,
                                    "V6 Multicast forward entry port additon in hardware for"
                                    " Vlan %d Source %s, Group %s failed\r\n",
                                    SNOOP_OUTER_VLAN (VlanId),
                                    SnoopPrintIPvxAddress (SrcAddr),
                                    SnoopPrintIPvxAddress (GrpAddr));
                    return SNOOP_FAILURE;
                }
            }
#endif

            break;

        case SNOOP_DEL_PORT:
        case SNOOP_DEL_LA_PORT:

#ifdef IGS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_MEM_CPY (TempPortList, PortList, sizeof (tPortList));
                SNOOP_MEM_CPY (TempUntagPortList, UntagPortList,
                               sizeof (tPortList));

                /* If enhanced mode is disabled, program IPMC table
                   in H/w using outer vlan
                 */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
                {

                    if (IgsFsMiNpUpdateIpmcFwdEntries
                        (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr), PortList,
                         UntagPortList, SNOOP_HW_DEL_PORT) != FNP_SUCCESS)
                    {
                        SNOOP_DBG_ARG3 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_FWD_TRC,
                                        "V4 Multicast forward entry port deletion in hardware for"
                                        " Vlan %d Source %x, Group %x failed\r\n",
                                        SNOOP_OUTER_VLAN (VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
                        SnoopHandleDeletePortFailure (VlanId, GrpAddr, SrcAddr,
                                                      u4Instance);
                        return SNOOP_SUCCESS;
                    }
                }

                SNOOP_MEM_CPY (PortList, TempPortList, sizeof (tPortList));
                SNOOP_MEM_CPY (UntagPortList, TempUntagPortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.PortList, PortList,
                               sizeof (tPortList));
                SNOOP_MEM_CPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList,
                               sizeof (tPortList));

                if (IgsFsMiIgsHwUpdateIpmcEntry
                    (u4Instance, &IgsHwIpFwdInfo,
                     SNOOP_HW_DEL_PORT) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG4
                        (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                         SNOOP_FWD_TRC,
                         "V4 Multicast forward entry port deletion in hardware "
                         " for OuterVlan %d Source %x, Group %x "
                         "InnerVlan %d failed\r\n",
                         SNOOP_OUTER_VLAN (VlanId),
                         SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                         SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                         SNOOP_INNER_VLAN (VlanId));
                    SnoopHandleDeletePortFailure (VlanId, GrpAddr, SrcAddr,
                                                  u4Instance);
                    return SNOOP_SUCCESS;

                }
            }
#endif

#ifdef MLDS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SnoopUtilGetMcastMldsIpFwdPorts
                    (u4Instance, VlanId, GrpAddr.au1Addr, SrcAddr.au1Addr,
                     PortList, UntagPortList);

                if (MldsFsMiNpUpdateIp6mcFwdEntries
                    (u4Instance, SNOOP_OUTER_VLAN (VlanId), GrpAddr.au1Addr,
                     SrcAddr.au1Addr, PortList, UntagPortList,
                     SNOOP_HW_DEL_PORT) != FNP_SUCCESS)
                {
                    SNOOP_DBG_ARG3 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_FWD_TRC,
                                    "V6 Multicast forward entry port deletion in hardware for"
                                    " Vlan %d Source %s, Group %s failed\r\n",
                                    SNOOP_OUTER_VLAN (VlanId),
                                    SnoopPrintIPvxAddress (SrcAddr),
                                    SnoopPrintIPvxAddress (GrpAddr));

                    return SNOOP_FAILURE;
                }
            }
#endif

            break;

        default:
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME,
                           "Unknown event type for Hardware update\r\n");
            return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : SnoopNpGetMulticastEgressPorts                   */
/*                                                                           */
/*    Description         : This function returns the port bitmap for a      */
/*                          multicast group..                                */
/*                                                                           */
/*    Input(s)            : VlanId  - VlanId                                 */
/*                          GrpAddr   - Mcast Group address                  */
/*                          SrcAddr   - Mcast Source address                 */
/*                          tSnoopPortBmp - PortBitmap                       */
/*                                                                           */
/*    Output(s)           : Updated PortList                                 */
/*                                                                           */
/*    Return Value(s)     : SNOOP_SUCCESS / SNOOP_FAILURE                    */
/*****************************************************************************/
INT4
SnoopNpGetMulticastEgressPorts (UINT4 u4Instance, tSnoopTag VlanId,
                                tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                                tSnoopIfPortBmp PortBitmap)
{
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortBmp       FwdPortBitmap;
    UINT1               u1AddrType = 0;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    u1AddrType = GrpAddr.u1Afi;
    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               u1AddrType, &pSnoopVlanEntry) == SNOOP_FAILURE)
    {
        return SNOOP_FAILURE;
    }

    if (SnoopFwdGetIpForwardingEntry (u4Instance, VlanId, GrpAddr,
                                      SrcAddr, &pSnoopIpGrpFwdEntry) !=
        SNOOP_FAILURE)
    {
        SnoopGetPhyPortBmp (u4Instance, pSnoopIpGrpFwdEntry->PortBitmap,
                            PortBitmap);
    }
    else
    {
        if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                                   GrpAddr, &pSnoopGroupEntry) == SNOOP_FAILURE)
        {
            return SNOOP_FAILURE;
        }
        else
        {
            /* Check if any V2 receivers are present */
            if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
            {
                u1ASMHostPresent = SNOOP_TRUE;
            }

            /* Check if source is present */
            SNOOP_IS_SOURCE_PRESENT (u4Instance, SrcAddr, bResult, u2SrcIndex);

            if (u2SrcIndex != 0xffff)
            {
                u2SrcIndex++;
            }

            /* Get the list of ports on which receivers are connected for 
             * this group record */
            SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                      u1ASMHostPresent, FwdPortBitmap);

            /* Add the router port bitmap if present to the newly created 
             * multicast forwarding entry */
            if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) ==
                SNOOP_INVALID_VLAN_ID)
            {
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                        FwdPortBitmap);
            }

            SnoopGetPhyPortBmp (u4Instance, FwdPortBitmap, PortBitmap);
        }
    }
    UNUSED_PARAM (bResult);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopNpIndicateMRP                               */
/*                                                                           */
/*    Description         : This function indicated MRP (PIM/ DVMRP/         */
/*                          IGMP PROXY regarding port bitmap change          */
/*                                                                           */
/*    Input(s)            : GrpIpAddr - Group IP address                     */
/*                        : InVlanId - VLAN ID                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_TRUE/SNOOP_FALSE                           */
/*****************************************************************************/
INT4
SnoopNpIndicateMRP (tIPvXAddr GrpIpAddr, tSnoopTag InVlanId)
{
#ifndef IGS_OPTIMIZE
    tSnoopMacAddr       GrpMacAddr;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1MRPPresent = SNOOP_FALSE;
    UINT4               u4VlanIfIndex = 0;
    UINT1               u1AddrType = SNOOP_ADDR_TYPE_IPV4;
#ifdef PIM_WANTED
    UINT4               VlanId = 0;
#endif
#ifndef PIM_WANTED
    UNUSED_PARAM (InVlanId);
    UNUSED_PARAM (u4VlanIfIndex);
#endif
    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /* Indicate PIM about port bitmap change if PIM 
     * is enabled */
    SNOOP_MEM_CPY (au1GroupAddr, GrpIpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);
#ifdef IGS_WANTED
    if (GrpIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        u1AddrType = SNOOP_ADDR_TYPE_IPV4;
        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4 (au1GroupAddr), GrpMacAddr);
    }
#endif
#ifdef MLDS_WANTED
    if (GrpIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        u1AddrType = SNOOP_ADDR_TYPE_IPV6;
        SNOOP_GET_MAC_FROM_IPV6 (au1GroupAddr, GrpMacAddr);
    }
#endif

#ifdef PIM_WANTED
    VlanId = (UINT4) SNOOP_OUTER_VLAN (InVlanId);
    u4VlanIfIndex = CfaGetVlanInterfaceIndex (SNOOP_OUTER_VLAN (InVlanId));
    if (CFA_INVALID_INDEX == u4VlanIfIndex && u4VlanIfIndex != 0)
    {
        SNOOP_DBG_ARG1 (SNOOP_IGS_DBG_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                        "Invalid interface index %u\r\n", u4VlanIfIndex);
        return u1MRPPresent;
    }
    if ((!PimIsPimEnabled ()) && (SPimPortCheckOnInterface (u4VlanIfIndex)
                                  == OSIX_SUCCESS))
        /*    if (!PimIsPimEnabled ()) */
    {
        u1MRPPresent = SNOOP_TRUE;
        PimVlanPortBmpChgNotify (GrpMacAddr, SNOOP_OUTER_VLAN (InVlanId),
                                 u1AddrType);
    }
    UNUSED_PARAM (VlanId);
#endif

    /* Indicate DVMRP about port bitmap change if DVMRP 
     * is enabled */
#ifdef DVMRP_WANTED
    if (DvmrpIsDvmrpEnabled () == SNOOP_TRUE)
    {
        u1MRPPresent = SNOOP_TRUE;
        DvmrpVlanPortBmpChgNotify (GrpMacAddr, SNOOP_OUTER_VLAN (InVlanId));
    }
#endif

    /* Indicate IGMP Proxy if IGMP Proxy is enabled  */
#ifdef IGMPPRXY_WANTED
    if (IgpPortIsIgmpPrxyEnabled () == SNOOP_TRUE)
    {
        u1MRPPresent = SNOOP_TRUE;
        IgpPortBmpChgNotify (GrpMacAddr, SNOOP_OUTER_VLAN (InVlanId));
    }
#endif

    return u1MRPPresent;
#else
    UNUSED_PARAM (GrpIpAddr);
    UNUSED_PARAM (InVlanId);
    return SNOOP_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name      : SnoopHwPortRateLimit                                 */
/*                                                                           */
/* Description        : This function configures rate limit for a port on    */
/*                      downstream interface at the hardware when desired    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                    : u4Port     - Port Number                             */
/*                    : u1AddrType -  Address Type                           */
/*                    : VlanId     -  Vlan Id                                */
/*                    : u4Rate     -  No of packets per second               */
/*                    : u1Action   -  Type of action to be                   */
/*                             performed.(Add/Remove Rate-limit for port     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopHwPortRateLimit (UINT4 u4Instance, UINT4 u4Port, UINT1 u1AddrType,
                      tSnoopTag VlanId, UINT4 u4Rate, UINT1 u1Action)
{
#ifdef IGS_WANTED
    tIgsHwRateLmt       IgsHwRateLmt;
    BOOL1               bResult = SNOOP_FALSE;
    UINT4               u4PhyPort = 0;

    MEMSET (&IgsHwRateLmt, 0, sizeof (tIgsHwRateLmt));
    /*Check the whether port is logical interface */
    SNOOP_IS_SISP_PORT (u4Port, bResult);

    if (bResult == SNOOP_TRUE)
    {
        /*Get the corrosponding physical index for logical interface */
        u4PhyPort = SNOOP_GET_PHY_PORT (u4Port);
    }
    else
    {
        u4PhyPort = u4Port;
    }

    IgsHwRateLmt.u4Port = u4PhyPort;
    IgsHwRateLmt.InnerVlanId = SNOOP_INNER_VLAN (VlanId);
    IgsHwRateLmt.u4Rate = u4Rate;
    if (u1AddrType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (IgsFsMiIgsHwPortRateLimit (u4Instance, &IgsHwRateLmt, u1Action)
            == FNP_FAILURE)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_NP | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_NP_NAME,
                                "Unable to configure rate-limit in Hardware for"
                                "instance %u\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
#else
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1AddrType);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Rate);
    UNUSED_PARAM (u1Action);
    return SNOOP_SUCCESS;
#endif
}

/*****************************************************************************
 *                                                                          
 *   Function Name       : SnoopClearHwIPMcastTable 
 *                                                                             
 *   Description         : This function calls the NPAPI to clear the 
 *                         hardware multicast table entries for a particular
 *                         context. 
 *                                                                          
 *   Input(s)            : None
 *                                                                          
 *   Output(s)           : None                                             
 *                                                                          
 *   Returns             : None    
 *****************************************************************************/
VOID
SnoopNpClearHwIPMcastTable (UINT4 u4ContextId)
{
#ifdef IGS_WANTED
    IgsFsMiIgsHwClearIpmcFwdEntries (u4ContextId);
#else
    UNUSED_PARAM (u4ContextId);
#endif
    return;
}
