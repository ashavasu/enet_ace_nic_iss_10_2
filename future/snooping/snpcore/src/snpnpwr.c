/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpnpwr.c,v 1.6 2010/05/13 11:53:58 prabuc Exp $
 *
 * Description: All prototypes for Multiple Instance Network Processor 
 *              API functions done here
 *******************************************************************/

#include "lr.h"
#include "cfa.h"
#include "bridge.h"
#include "fsvlan.h"
#include "snp.h"
#include "snpinc.h"
#include "npapi.h"

#ifdef IGS_WANTED
/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnableIgmpSnooping                          */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware for a given instance. This creates and      */
/*                      installs filter needed for snooping feature          */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwEnableIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);

    return FsIgsHwEnableIgmpSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDisableIpIgmpSnooping                       */
/*                                                                           */
/* Description        : This function disables IGMP snooping feature in the  */
/*                      hardware for given instance. This removes & destroys */
/*                      filter needed for snooping feature                   */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwDisableIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwDisableIgmpSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiNpUpdateIpmcFwdEntries                           */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardware for a given instance based on the    */
/*                      EventType.                                           */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpUpdateIpmcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                            UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                            tPortList PortList, tPortList UntagPortList,
                            UINT1 u1EventType)
{
    UNUSED_PARAM (u4Instance);
    return FsNpUpdateIpmcFwdEntries (VlanId, u4GrpAddr, u4SrcAddr, PortList,
                                     UntagPortList, u1EventType);
}

/*****************************************************************************/
/*    Function Name       : FsMiIgsHwClearIpmcFwdEntries                     */
/*                                                                           */
/*    Description         : This function calls the NPAPI to clear the       */
/*                          hardware multicast table entries  for a          */
/*                          particular context.                              */
/*                                                                           */
/*    Input(s)            : u4Instance - Insatnce Id                         */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Returns             : NONE                                             */
/*****************************************************************************/
VOID
FsMiIgsHwClearIpmcFwdEntries (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    FsIgsHwClearIpmcFwdEntries ();
    return;
}

/*****************************************************************************/
/* Function Name      : FsMiNpGetFwdEntryHitBitStatus                        */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry for a given instance is used for sending data  */
/*                      traffic                                              */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - vlan id                                     */
/*                      u4GrpAddr - Group Address                            */
/*                      u4SrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpGetFwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                               UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    UNUSED_PARAM (u4Instance);
    return FsNpGetFwdEntryHitBitStatus (VlanId, u4GrpAddr, u4SrcAddr);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwSparseMode                                  */
/*                                                                           */
/* Description        : This function enables/disables IGMP sparse mode in   */
/*                      the hardware.                                        */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1SparseStatus - Sparse Mode Status                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwSparseMode (UINT4 u4ContextId, UINT1 u1SparseStatus)
{
    UNUSED_PARAM (u4ContextId);
    return FsIgsHwEnhancedMode (u1SparseStatus);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnhancedMode                                */
/*                                                                           */
/* Description        : This function enables IGMP Enhanced mode in the      */
/*                      hardware.                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      u1EnhStatus - Enhanced Mode Status                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwEnhancedMode (UINT4 u4ContextId, UINT1 u1EnhStatus)
{
    UNUSED_PARAM (u4ContextId);
    return FsIgsHwEnhancedMode (u1EnhStatus);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwPortRateLimit                               */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be NULL. When enhanced mode is       */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or NULL.When rate limit is          */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : u4Instance - Identifier for the Virtual Switch       */
/*                                                                           */
/*                    : Struct tIgsHwRateLmt - It contains                   */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                                                                           */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                        UINT1 u1Action)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwPortRateLimit (pIgsHwRateLmt, u1Action);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwAddRtrPort                                  */
/*                                                                           */
/* Description        : This function adds router Port to router Portlsit    */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be added                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwAddRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwAddRtrPort (VlanId, u2Port);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDelRtrPort                                      */
/*                                                                           */
/* Description        : This function deletes Port from router Portlsit      */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                      VlanId - Vlan ID to which the MAC address should be  */
/*                               associated. This will be zero if no VLAN    */
/*                      u2Port - Port number to be deleted                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwDelRtrPort (UINT4 u4Instance, tSnoopVlanId VlanId, UINT2 u2Port)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwDelRtrPort (VlanId, u2Port);
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwEnableIpIgmpSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  IGMP is enabled for */
/*                      a given instance                                     */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwEnableIpIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwEnableIpIgmpSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwDisableIpIgmpSnooping                       */
/*                                                                           */
/* Description        : This function is called when IP IGMP is disabled for */
/*                      a given instance                                    */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsHwDisableIpIgmpSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsHwDisableIpIgmpSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwUpdateIpmcEntry                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardware for a given instance based on the    */
/*                      EventType.                                           */
/*                                                                           */
/* Input(s)           : u4Instance      - Insatnce Id                        */
/*                      pIgsHwIpFwdInfo - IP Fwd information                 */
/*                      u1Action        - SNOOP_HW_CREATE_ENTRY /            */
/*                                        SNOOP_HW_DELETE_ENTRY /            */
/*                                        SNOOP_HW_ADD_PORT /                */
/*                                        SNOOP_HW_DEL_PORT /                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwUpdateIpmcEntry (UINT4 u4Instance, tIgsHwIpFwdInfo * pIgsHwIpFwdInfo,
                          UINT1 u1Action)
{
    UNUSED_PARAM (u4Instance);
    return (FsIgsHwUpdateIpmcEntry (pIgsHwIpFwdInfo, u1Action));
}

/*****************************************************************************/
/* Function Name      : FsMiIgsHwGetIpFwdEntryHitBitStatus                   */
/*                                                                           */
/* Description        : This function is used to check whether the forwarding*/
/*                      entry for a given instance is used for sending data  */
/*                      traffic                                              */
/*                                                                           */
/* Input(s)           : u4Instance      - Insatnce Id                        */
/*                      pIgsHwIpFwdInfo - IP FWD Information                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsHwGetIpFwdEntryHitBitStatus (UINT4 u4Instance,
                                    tIgsHwIpFwdInfo * pIgsHwIpFwdInfo)
{
    UNUSED_PARAM (u4Instance);
    return (FsIgsHwGetIpFwdEntryHitBitStatus (pIgsHwIpFwdInfo));
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwEnableIgmpSnooping                      */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsMbsmHwEnableIgmpSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsIgsMbsmHwEnableIgmpSnooping (pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwEnableIpIgmpSnooping                    */
/*                                                                           */
/* Description        : This function enables IGMP snooping feature in the   */
/*                      hardware. Filters are added for receiving IGMP       */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiIgsMbsmHwEnableIpIgmpSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsIgsMbsmHwEnableIpIgmpSnooping (pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiIgsMbsmHwPortRateLimit                           */
/*                                                                           */
/* Description        : This function configures IGMP Rate Limit for a       */
/*                      downstream interface in hardware. In default mode,   */
/*                      the downstream interface corresponds to only port and*/
/*                      inner-vlan will be NULL. When enhanced mode is       */
/*                      enabled, downstream interface corresponds to port and*/
/*                      inner-vlan.In this mode inner-vlan may be within the */
/*                      valid vlan range or NULL.When rate limit is          */
/*                      configured for an interface, this API will be invoked*/
/*                      with action as .ADD. and u4Rate as the required rate.*/
/*                      When rate limit is configured as zero (rate limit all*/
/*                      packets on this interface) this API will be invoked  */
/*                      with action as .ADD. and u4Rate as zero. When rate   */
/*                      limit is configured to default value ( no rate       */
/*                      limiting to be applied on this interface) this API   */
/*                      will be invoked with action as .REMOVE. and the      */
/*                      u4Rate can be ignored.                               */
/*                                                                           */
/* Input(s)           : u4Instance - Identifier for the Virtual Switch       */
/*                                                                           */
/*                    : Struct tIgsHwRateLmt - It contains                   */
/*                      UINT4 u4Port            :  Port Number               */
/*                      tVlanId InnerVlanId     :  Inner-Vlan                */
/*                      UINT4 u4Rate            :  No of packets per second  */
/*                                                                           */
/*                    : UINT1 u1Action - Type of action to be performed.     */
/*                                        ( Add/Remove Rate-limit for port ) */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiIgsMbsmHwPortRateLimit (UINT4 u4Instance, tIgsHwRateLmt * pIgsHwRateLmt,
                            UINT1 u1Action, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4Instance);
    return FsIgsMbsmHwPortRateLimit (pIgsHwRateLmt, u1Action, pSlotInfo);
}
#endif /* MBSM_WANTED */

#endif /* IGS_WANTED */

#ifdef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : FsMiMldsHwEnableMldSnooping                            */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. This creates and installs filter needed    */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiMldsHwEnableMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsMldsHwEnableMldSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwDisableMldSnooping                           */
/*                                                                           */
/* Description        : This function disables MLD snooping feature in the  */
/*                      hardware. This removes & destroys filter needed      */
/*                      for snooping feature                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwDisableMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsMldsHwDisableMldSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiNpUpdateIp6mcFwdEntries                             */
/*                                                                           */
/* Description        : This function Updates IP Multicast forwarding entries*/
/*                      in the hardwarei based on the EventType.             */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpUpdateIp6mcFwdEntries (UINT4 u4Instance, tSnoopVlanId VlanId,
                             UINT1 *pGrpAddr, UINT1 *pSrcAddr,
                             tPortList PortList, tPortList UntagPortList,
                             UINT1 u1EventType)
{
    UNUSED_PARAM (u4Instance);
    return FsNpUpdateIp6mcFwdEntries (VlanId, pGrpAddr, pSrcAddr, PortList,
                                      UntagPortList, u1EventType);

}

/*****************************************************************************/
/* Function Name      : FsMiNpGetIp6FwdEntryHitBitStatus                          */
/*                                                                           */
/* Description        : This function gets how many times this forwarding    */
/*                      entry is used for sending data traffic               */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId - vlan id                                     */
/*                      pGrpAddr - Group Address                            */
/*                      pSrcAddr - Source Address                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiNpGetIp6FwdEntryHitBitStatus (UINT4 u4Instance, tSnoopVlanId VlanId,
                                  UINT1 *pGrpAddr, UINT1 *pSrcAddr)
{
    UNUSED_PARAM (u4Instance);
    return FsNpGetIp6FwdEntryHitBitStatus (VlanId, pGrpAddr, pSrcAddr);

}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwEnableIpMldSnooping                          */
/*                                                                           */
/* Description        : This function is called when IP  MLD is enabled      */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwEnableIpMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsMldsHwEnableIpMldSnooping ();
}

/*****************************************************************************/
/* Function Name      : FsMiMldsHwDisableIpMldSnooping                         */
/*                                                                           */
/* Description        : This function is called when IP MLD is disabled.     */
/*                                                                           */
/* Input(s)           : u4Instance - Insatnce Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsHwDisableIpMldSnooping (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);
    return FsMldsHwDisableIpMldSnooping ();
}

#ifdef MBSM_WANTED
/*****************************************************************************/
/* Function Name      : FsMiMldsMbsmHwEnableMldSnooping                      */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      L2MC table is initialized for building MAC based     */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/

INT4
FsMiMldsMbsmHwEnableMldSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsMldsMbsmHwEnableMldSnooping (pSlotInfo));
}

/*****************************************************************************/
/* Function Name      : FsMiMldsMbsmHwEnableIpMldSnooping                    */
/*                                                                           */
/* Description        : This function enables MLD snooping feature in the    */
/*                      hardware. Filters are added for receiving MLD        */
/*                      IPMC table is initialized for building IP based      */
/*                      Multicast data forwarding                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FNP_SUCCESS / FNP_FAILURE                            */
/*****************************************************************************/
INT4
FsMiMldsMbsmHwEnableIpMldSnooping (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (u4ContextId);
    return (FsMldsMbsmHwEnableIpMldSnooping (pSlotInfo));
}
#endif /* MBSM_WANTED */

#endif /* MLDS_WANTED */
