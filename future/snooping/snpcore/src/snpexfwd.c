/*****************************************************************************/
/*    FILE  NAME            : snpexfwd.c                                     */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : snooping extended forwarding module            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains forwarding routines         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    March 2010            Initial Creation                         */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopMiVlanSnoopFwdPacketOnPortList                  */
/*                                                                           */
/* Description        : This function calls the VLAN Module to get           */
/*                      forward the packet on a given portlist & given VLAN  */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be forwarded.                       */
/*                     DestMacAddr  - Destination MAC Address                */
/*                      u2Port       - Packet received on this Port Number   */
/*                      VlanId       - VLAN Identifier                       */
/*                      u1Forward       - VLAN_FORWARD_SPECIFIC/             */
/*                                        VLAN_FORWARD_ALL                   */
/*                      PortBitmap   - Port list                             */
/*                                                                           */
/* Output(s)          : TaggedPortBitmap - Tagged port list                  */
/*                      UntaggedPortBitmap - Untagged port list              */
/*                                                                           */
/* Return Value(s)    : VLAN_FORWARD/ VLAN_NO_FORWARD                        */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopMiVlanSnoopFwdPacketOnPortList (tCRU_BUF_CHAIN_DESC * pBuf,
                                     UINT4 u4ContextId, tMacAddr DestMacAddr,
                                     UINT2 u2Port, tSnoopTag VlanId,
                                     UINT1 u1IsQuery, UINT1 u1Forward,
                                     tSnoopPortBmp PortBitmap)
{

    tVlanFwdInfo        FwdInfo;
    INT4                i4RetVal = 0;

    SNOOP_MEM_SET (&FwdInfo, 0, sizeof (tVlanFwdInfo));

    FwdInfo.u4ContextId = u4ContextId;
    SNOOP_MEM_CPY (FwdInfo.DestAddr, DestMacAddr, sizeof (MAC_ADDR_LEN));
    FwdInfo.u4InPort = u2Port;
    FwdInfo.u1Action = VLAN_FORWARD_SPECIFIC;
    FwdInfo.u1SendOnAllCVlans = u1IsQuery;

    if (u1IsQuery == OSIX_TRUE)
    {
        FwdInfo.u1Action = u1Forward;
    }

    if (u1Forward == VLAN_FORWARD_ALL)
    {
        SnoopMiGetVlanLocalEgressPorts (u4ContextId, VlanId, PortBitmap);
    }

#ifdef SRCMAC_CTP_WANTED
    if (u2Port == SNOOP_INVALID_PORT)
    {
        FwdInfo.u1OverrideSrcMac = OSIX_TRUE;
    }
#else
    FwdInfo.u1OverrideSrcMac = OSIX_FALSE;
#endif

    FwdInfo.VlanTag.OuterVlanTag.u2VlanId = SNOOP_OUTER_VLAN (VlanId);
    FwdInfo.VlanTag.OuterVlanTag.u1TagType = VLAN_UNTAGGED;
    FwdInfo.VlanTag.OuterVlanTag.u1Priority = VLAN_HIGHEST_PRIORITY;
    FwdInfo.VlanTag.OuterVlanTag.u1DropEligible = 0;

    if (SNOOP_INNER_VLAN (VlanId) != VLAN_NULL_VLAN_ID)
    {
        FwdInfo.VlanTag.InnerVlanTag.u2VlanId = SNOOP_INNER_VLAN (VlanId);
        FwdInfo.VlanTag.InnerVlanTag.u1TagType = VLAN_TAGGED;
        FwdInfo.VlanTag.InnerVlanTag.u1Priority = 0;
        FwdInfo.VlanTag.InnerVlanTag.u1DropEligible = 0;
    }
    else
    {
        FwdInfo.VlanTag.InnerVlanTag.u1TagType = VLAN_UNTAGGED;
        FwdInfo.VlanTag.InnerVlanTag.u1Priority = 0;
        FwdInfo.VlanTag.InnerVlanTag.u1DropEligible = 0;
    }

    /* tSnoopPortBmp and tLocalPortList are same in size */
    FwdInfo.pPortList = gSnoopLocalPortList;
    FwdInfo.u1IsPortListLocal = OSIX_TRUE;
    FwdInfo.u1FrameType = VLAN_MCAST_FRAME;

    SNOOP_MEM_CPY (FwdInfo.pPortList, PortBitmap, sizeof (tSnoopPortBmp));

    i4RetVal = SnoopVlanApiForwardOnPorts (pBuf, &FwdInfo);

    SNOOP_MEM_SET (gSnoopLocalPortList, 0, sizeof (tSnoopPortBmp));

    return i4RetVal;
}
