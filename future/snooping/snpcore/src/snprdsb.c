/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *               */
/* $Id: snprdsb.c,v 1.10 2017/08/08 13:19:08 siva Exp $         */
/* Licensee Aricent Inc., 2005                              */

#include "snpinc.h"
#include "snpred.h"
/*****************************************************************************/
/*    FILE  NAME            : snpredummy.c                                   */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) redundancy Module          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains redundancy functions        */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnoopRedHandleRmEvents                           */
/*                                                                          */
/*    Description        : This is function handles the received RM events  */
/*                         by invoking the approriate routines to handle    */
/*                         the same.                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
INT4
SnoopRedHandleRmEvents (tSnoopMsgNode * pMsgNode)
{
    UNUSED_PARAM (pMsgNode);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRegisterWithRM                                  */
/*                                                                           */
/* Description        : Registers Snoop with Redundancy Manager RM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If registration is success then SNOOP_SUCCESS        */
/*                      Otherwise SNOOP_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedRegisterWithRM (VOID)
{
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedDeRegisterWithRM                             */
/*                                                                           */
/* Description        : Deregisters Snoop with Redundancy Manager RM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If deregistration is success then SNOOP_SUCCESS        */
/*                      Otherwise SNOOP_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedDeRegisterWithRM (VOID)
{
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeActiveFromIdle                       */
/*                                                                           */
/* Description        : This function will be invoked when the node becomes  */
/*                      Active from the Idle state.  This function will      */
/*                      enable/disable the module based on the current       */
/*                      protocol administrative status                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeActiveFromIdle (VOID)
{
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeStandbyFromIdle                      */
/*                                                                           */
/* Description        : This function will be invoked when the node becomes  */
/*                      standby from the idle state.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeStandbyFromIdle (VOID)
{
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendRtrPortListSync                    */
/*                                                                           */
/* Description        : This function sends the dynamic sync of router port  */
/*                      list for the VLAN from Snoop protocol in Active      */
/*                      node to Snoop protocol in Standby node through RM.   */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Number                      */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                      containing router information to be  */
/*                                      synced up.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendRtrPortListSync (UINT4 u4InstId,
                                   tSnoopVlanEntry * pSnoopVlanEntry)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (pSnoopVlanEntry);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendGrpInfoSync                        */
/*                                                                           */
/* Description        : This function sends the multicast group membership   */
/*                      update information from Snoop in Active node to      */
/*                      Snoop in Standby node through RM.                    */
/*                                                                           */
/* Input(s)           : u4InstId     - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Address          */
/*                      FwdPortList  - Learnt Forwarding Port List           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendGrpInfoSync (UINT4 u4InstId,
                               tSnoopGroupEntry * pSnoopGroupEntry)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (pSnoopGroupEntry);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendQuerierSync                        */
/*                                                                           */
/* Description        : This function sends the dynamic sync of change in    */
/*                      querier information for a VLAN in Snoop protocol in  */
/*                      Active node to Snoop in Standby node through RM      */
/*                                                                           */
/* Input(s)           : u4InstId    - Instance Id                            */
/*                      VlanId      - Vlan Identifier                        */
/*                      u1Querier   - SNOOP_QUERIER / SNOOP_NON_QUERIER      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendQuerierSync (UINT4 u4InstId, tSnoopTag VlanId,
                               UINT1 u1Querier, UINT1 u1AddressType,
                               UINT4 u4QuerierIP)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1Querier);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (u4QuerierIP);

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendVerSync                            */
/*                                                                           */
/* Description        : This function sends the dynamic sync of change in    */
/*                      operating version of IGMP for a VLAN in SNOOP proto  */
/*                      in Active node to SNOOP in Standby node through RM   */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Id                          */
/*                      VlanId        - Vlan Identifier                      */
/*                      u1OperVersion - SNOOP_IGS_IGMP_VERSION1 /            */
/*                                      SNOOP_IGS_IGMP_VERSION2 /            */
/*                                      SNOOP_IGS_IGMP_VERSION3              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendVerSync (UINT4 u4InstId, tSnoopTag VlanId,
                           UINT1 u1OperVersion, UINT1 u1AddressType,
                           UINT4 u4Port)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1OperVersion);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (u4Port);

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendForwardModeSync                    */
/*                                                                           */
/* Description        : This function sends the updates regarding the change */
/*                      in Multicast fowarding mode of Snoop in Active node  */
/*                      to Snoop in Standby node through RM.                 */
/*                                                                           */
/* Input(s)           : u1McastFwdMode  - Snoop_MCAST_FWD_MODE_MAC/          */
/*                                        Snoop_MCAST_FWD_MODE_IP            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendForwardModeSync (UINT4 u4InstId, UINT1 u1McastFwdMode)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (u1McastFwdMode);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendDelVlanSync                        */
/*                                                                           */
/* Description        : This function sends the trigger to standby node,     */
/*                      wnen the VLAN is deleted in active node.             */
/*                                                                           */
/* Input(s)           : u4InstId - Instance Id                               */
/*                      VlanId   - Vlan Id                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendDelVlanSync (UINT4 u4InstId, tSnoopTag VlanId,
                               UINT1 u1AddressType)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1AddressType);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedProcessSyncUpdate                            */
/*                                                                           */
/* Description        : This function applies the Snoop dynamic/bulk update  */
/*                      message immediately in the standby node Snoop module,*/
/*                      when it is received from active node.                */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to RM message containing Snoop dynamic*/
/*                      data.                                                */
/*                      u2Length - Length of Snoop dynamic sync update       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedProcessSyncUpdate (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedSyncBulkUpdNextEntry                         */
/*                                                                           */
/* Description        : This function is used to update the BulkUpdNextEntry */
/*                      when the entry is removed.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopRedSyncBulkUpdNextEntry (UINT4 u4Instance, tRBElem * pSnoopElem,
                              tSnoopBulkUpdSyncTypes SnoopBulkUpdSyncType)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (pSnoopElem);
    UNUSED_PARAM (SnoopBulkUpdSyncType);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendRtrPortVerSync                     */
/*                                                                           */
/* Description        : This function sends the dynamic sync of router ports */
/*                      for the VLAN from SNOOP protocol in Active           */
/*                      node to SNOOP protocol in Standby node through RM.   */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Number                      */
/*                      u4Port        - Rtr port                             */
/*                      u1version     - Rtr port version                     */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                      containing router information to be  */
/*                                      synced up.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendRtrPortVerSync (UINT4 u4InstId, UINT4 u4Port, UINT1 u1Version,
                                  tSnoopVlanEntry * pSnoopVlanEntry)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Version);
    UNUSED_PARAM (pSnoopVlanEntry);

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendV3RtrPortIntrSync                  */
/*                                                                           */
/* Description        : This function sends the dynamic sync of V3 router    */
/*                      port purge interval for a port from SNOOP protocol   */
/*                      in Active node to Standby node through RM.           */
/*                                                                           */
/* Input(s)           : u4InstId        - Instance Number                    */
/*                      u4Port          - Port Number                        */
/*                      u4Interval      - V3 Router Port Purge Interval      */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                        containing router port interval    */
/*                                        information to be synced up.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendV3RtrPortIntrSync (UINT4 u4InstId, UINT4 u4Port,
                                     UINT4 u4Interval,
                                     tSnoopVlanEntry * pSnoopVlanEntry)
{
    UNUSED_PARAM (u4InstId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4Interval);
    UNUSED_PARAM (pSnoopVlanEntry);

    return (SNOOP_SUCCESS);
}
