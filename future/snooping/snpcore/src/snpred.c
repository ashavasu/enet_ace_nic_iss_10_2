/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpred.c,v 1.92 2018/02/19 10:03:49 siva Exp $
 *
 * Description: This file contains function definition for
 *              Snooping redunancy implementation. 
 *******************************************************************/

/*****************************************************************************/
/*    FILE  NAME            : snpred.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping Redundancy                            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains function definition for     */
/*                            for Snooping redunancy implementation.         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/

#include "snpinc.h"
#include "snpred.h"

/****************************************************************************/
/* Function Name      : SnoopRedHandleRmEvents                              */
/*                                                                          */
/* Description        : This is function handles the received RM events     */
/*                      by invoking the approriate routines to handle       */
/*                      the same.                                           */
/*                                                                          */
/* Input(s)           : None                                                */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : SNOOP_SUCCESS/SNOOP_FAILURE.                        */
/****************************************************************************/
INT4
SnoopRedHandleRmEvents (tSnoopMsgNode * pMsgNode)
{
    tRmNodeInfo        *pData = NULL;
    tSnoopNodeState     SnpPrevNodeState = SNOOP_IDLE_NODE;
    tRmProtoEvt         ProtoEvt;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (pMsgNode == NULL)
    {
        return SNOOP_FAILURE;
    }

    switch (pMsgNode->uInMsg.RmData.u4Events)
    {
        case GO_ACTIVE:

            if (SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE)
            {
                break;
            }
            SnpPrevNodeState = SNOOP_NODE_STATUS ();

            SnoopRedMakeNodeActive ();

            if (SnpPrevNodeState == SNOOP_IDLE_NODE)
            {
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else
            {
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            SnoopRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:
            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                break;
            }
            SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;
            if (SNOOP_NODE_STATUS () == SNOOP_IDLE_NODE)
            {
                /* Ignore this event. Node will become standby once
                 * static configurations are restored. */
                break;
            }

            /* Node must be in ACTIVE state  */
            SnoopRedMakeNodeStandbyFromActive ();
            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
            SNOOP_RED_BULK_SYNC_INPROGRESS = SNOOP_FALSE;
            gu1SnoopRedAuditInProgress = SNOOP_FALSE;
            break;

        case RM_INIT_HW_AUDIT:
            if (SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE)
            {
                if (SNOOP_RED_IS_HW_AUDIT_REQ () == SNOOP_TRUE)
                {
                    SNOOP_RED_IS_HW_AUDIT_REQ () = SNOOP_FALSE;
                    SnoopRedAudit ();
                }
            }
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            if (SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE)
            {
                /* Ignore this event. */
                break;
            }

            /* Node must be in IDLE/STANDBY state */
            SnoopRedProcessConfigRestoreEvent ();
            ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pMsgNode->uInMsg.RmData.pRmMsg;
            SNOOP_NUM_STANDBY_NODES () = pData->u1NumStandby;
            SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
            if (SNOOP_RED_BULK_REQ_RECD () == SNOOP_TRUE)
            {
                SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;

                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () = 0;
                SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
                SNOOP_RED_BULK_UPD_VLAN_ENTRY () = NULL;
                SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () = NULL;
                SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = NULL;

                SnoopRedHandleBulkUpdEvent ();
            }
            break;

        case RM_STANDBY_DOWN:
            pData = (tRmNodeInfo *) pMsgNode->uInMsg.RmData.pRmMsg;
            SNOOP_NUM_STANDBY_NODES () = pData->u1NumStandby;
            SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:
            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsgNode->uInMsg.RmData.pRmMsg, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsgNode->uInMsg.RmData.pRmMsg,
                                 pMsgNode->uInMsg.RmData.u2DataLen);

            ProtoAck.u4AppId = RM_SNOOP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            SnoopRedProcessSyncUpdate ((tRmMsg *)
                                       pMsgNode->uInMsg.RmData.pRmMsg,
                                       pMsgNode->uInMsg.RmData.u2DataLen);

            /* Processing of message is over, 
             * hence free the RM message. */
            RM_FREE ((tRmMsg *) pMsgNode->uInMsg.RmData.pRmMsg);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case L2_INITIATE_BULK_UPDATES:
            /* Send a Bulk Request for Active to send its so far learnt
             * dynamic sync information.
             */
            SnoopRedStandbySendBulkReq ();
            break;

        case L2_COMPLETE_SYNC_UP:
            SnoopRedTrigHigherLayer (L2_COMPLETE_SYNC_UP);
            break;

        default:
            break;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRegisterWithRM                                  */
/*                                                                           */
/* Description        : Registers Snoop with Redundancy Manager RM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If registration is success then SNOOP_SUCCESS        */
/*                      Otherwise SNOOP_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    SNOOP_MEM_SET (&RmRegParams, 0, sizeof (tRmRegParams));

    /* Register SNOOP with RM, by providing the identifier used by RM to 
     * identify the SNOOP application and 
     * the Callback function SnoopRedRcvPktFromRm() to be invoked by
     * RM to post events to SNOOP module.
     */
    RmRegParams.u4EntId = RM_SNOOP_APP_ID;
    RmRegParams.pFnRcvPkt = SnoopRedRcvPktFromRm;

    if (SnoopRmRegisterProtocols (&RmRegParams) != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "SNOOP Registration with RM Failed \r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedDeRegisterWithRM                             */
/*                                                                           */
/* Description        : Deregisters Snoop with Redundancy Manager RM.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : If deregistration is success then SNOOP_SUCCESS        */
/*                      Otherwise SNOOP_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedDeRegisterWithRM (VOID)
{
    if (RmDeRegisterProtocols (RM_SNOOP_APP_ID) != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "SNOOP Registration with RM Failed \r\n");
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedRcvPktFromRm                                 */
/*                                                                           */
/* Description        : For received events from RM this function generates  */
/*                      the corresponding events and sends the event to SNOOP*/
/*                      task. For other events this function enqueues the    */
/*                      given data to the queue and sends the corresponding  */
/*                      event to SNOOP task.                                 */
/*                                                                           */
/* Input(s)           : u1Event   - Event given by RM module                 */
/*                      pData     - Msg to be enqueue                        */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event is sent then return     */
/*                      SNOOP_SUCCESS otherwise return SNOOP_FAILURE         */
/*****************************************************************************/
INT4
SnoopRedRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tSnoopMsgNode      *pNode = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message is absent and hence no need to process and no need
         * to send events to SNOOP task. */

        return RM_FAILURE;
    }

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while receiving packet from RM\r\n");
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        return RM_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_RM_DATA;
    pNode->uInMsg.RmData.u4Events = u1Event;
    pNode->uInMsg.RmData.pRmMsg = pData;
    pNode->uInMsg.RmData.u2DataLen = u2DataLen;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    }
    else
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Posting message to SNOOP message queue failed\r\n");

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_RED_NAME, "Queue message release failure \n");

            return RM_FAILURE;
        }
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeActive                               */
/*                                                                           */
/* Description        : The function will be invoked when the node is to be  */
/*                      to transit to Active.                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (SNOOP_NODE_STATUS () == SNOOP_IDLE_NODE)
    {
        /* Invoke the function to transit the node From Idle state 
         * to Active state
         */
        SnoopRedMakeNodeActiveFromIdle ();
    }
    else if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
    {
        if ((SnoopRmGetStandbyNodeCount () == 0) &&
            (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE) &&
            (gu1SnoopLaCardRemovalProcessed == SNOOP_FALSE))
        {
            return SNOOP_SUCCESS;
        }

        SnoopRedMakeNodeActiveFromStandby ();
    }

    if (SNOOP_RED_BULK_REQ_RECD () == SNOOP_TRUE)
    {
        SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;

        SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () = 0;
        SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
        SNOOP_RED_BULK_UPD_VLAN_ENTRY () = NULL;
        SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () = NULL;
        SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;
        SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = NULL;

        SnoopRedHandleBulkUpdEvent ();
    }

    SnoopRmHandleProtocolEvent (&ProtoEvt);
    SNOOP_RED_BULK_SYNC_INPROGRESS = SNOOP_FALSE;
    gu1SnoopRedAuditInProgress = SNOOP_FALSE;
    gu1SnoopLaCardRemovalProcessed = SNOOP_FALSE;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeActiveFromIdle                       */
/*                                                                           */
/* Description        : This function will be invoked when the node becomes  */
/*                      Active from the Idle state.  This function will      */
/*                      enable/disable the module based on the current       */
/*                      protocol administrative status                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeActiveFromIdle (VOID)
{
    UINT4               u4Instance = 0;
    UINT1               u1AddressType = 0;

    /* Make the SNOOP node as Active. */
    SNOOP_NODE_STATUS () = SNOOP_ACTIVE_NODE;
    SNOOP_NUM_STANDBY_NODES () = SnoopRmGetStandbyNodeCount ();
    /* If the SNOOP Protocol is shut, then do nothing,
     * else Enable/Disable the SNOOP protocol based on the Protocol admin
     * status configured.
     */
    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            /* Check if the SNOOP protocol has be to ENABLED/DISABLED as per the
             * Administration configuration.
             */
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                if (SNOOP_PROTOCOL_ADMIN_STATUS
                    (u4Instance, (u1AddressType - 1)) != SNOOP_ENABLE)
                {
                    /* Do nothing since, SNOOP Protocol is disabled */
                    continue;
                }
                /* Enable the SNOOP protocol since Admin configuration is to enable
                 * the SNOOP protocol.
                 */
                SnoopMainEnableSnooping (u4Instance, u1AddressType);
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeActiveFromStandby                    */
/*                                                                           */
/* Description        : This function will be invoked when the node becomes  */
/*                      ACTIVE from the STANDBY state.  This function will   */
/*                      start protocol timers using the synced up control    */
/*                      plane to perform protocol operations and trigger     */
/*                      the audit module                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeActiveFromStandby (VOID)
{
    UINT4               u4Instance = 0;
    UINT1               u1AddressType = 0;

    SNOOP_NUM_STANDBY_NODES () = SnoopRmGetStandbyNodeCount ();

    /* If SNOOP Protocol is shut, then 
     *    Move the node to Active state and 
     *    Return
     * else
     *   If SNOOP protocol is disabled then
     *      Move the node to Active state and Return.
     *   Else if SNOOP protocol is enabled then
     *      Apply all dynamically synced information.
     *      Enable the SNOOP Protocol.
     *      Move the node to Active state
     */

    SNOOP_NODE_STATUS () = SNOOP_ACTIVE_NODE;

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddressType - 1))
                    != SNOOP_ENABLE)
                {
                    /* Since the SNOOP Protocol is disabled as per sync up,
                     * just transit the node to Active state.*/
                    continue;
                }

                /* Trigger switchover module to start protocol operations */
                SnoopRedSwitchOver (u4Instance, u1AddressType);
                if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {
                    SNOOP_RED_IS_HW_AUDIT_REQ () = SNOOP_TRUE;
                }

            }
        }
    }

    /* Start the General Query reply timer only if 
     * SNOOP_RED_GENERAL_QUERY_INPROGRESS flag is set. Flag will be set only if 
     * forwarding mode is IP for atleast one context
     * This Flag can be set even if there is no snoop enabled vlan present so that
     * IPMC Audit will be started envery time, and hardware entries will be cleaned up.
     * This is done to avoid stale entries present in the Hw */

    if (SNOOP_RED_GENERAL_QUERY_INPROGRESS == SNOOP_TRUE)
    {
        /* Start the Switchover Query Timer */
        SNOOP_RED_SWITCHOVER_TMR_NODE ().u1TimerType
            = SNOOP_RED_SWITCHOVER_QUERY_TIMER;
        SnoopTmrStartTimer (&SNOOP_RED_SWITCHOVER_TMR_NODE (),
                            SNOOP_RED_SW_OVER_QUERY_TMR_INTERVAL);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedSwitchOver                                   */
/*                                                                           */
/* Description        : This function will be invoked when the node is to be */
/*                      to transit from active to standby state.It reads the */
/*                      synced up control plane to start protocol timers to  */
/*                      perform protocol operations.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedSwitchOver (UINT4 u4Instance, UINT1 u1AddressType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortNode = NULL;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* For IP forwarding mode, send out general query on snoop enabled
     * vlan to build the database. */

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        SnoopRedSendGeneralQuery (u4Instance, u1AddressType);
        return SNOOP_SUCCESS;
    }

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
               "In SNOOPING Switchover module - L2RED \r\n");

    /* Switchover module should start the following protocol timers for 
     * MAC forwarding mode :
     *  Query timer.
     *  Router port purge timer.
     *  PurgeOrGrpQueryTimer.
     */

    if ((SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL) &&
        (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "No VLAN entries & Group entries found "
                   "in this instance \r\n");
        return SNOOP_SUCCESS;
    }

    /* Starting Query Timer & Router port purge timer in all VLAN's.
     */

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        pRBElem = pRBElemNext;

        if (pSnoopVlanEntry->u1AddressType != u1AddressType)
        {
            continue;
        }

        /* If the switch is configured as querier, start the
         * query timer in this VLAN.
         */
        if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
        {
            pSnoopVlanEntry->QueryTimer.u4Instance = u4Instance;
            pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;

            /* Start the query timer with short interval. */
            SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer,
                                SNOOP_STARTUP_QRY_INTERVAL);
        }
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        {
            if (((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier) ==
                 SNOOP_QUERIER)
                && (pSnoopVlanEntry->u1Querier == SNOOP_NON_QUERIER))
            {
                pSnoopVlanEntry->OtherQPresentTimer.u4Instance = u4Instance;
                pSnoopVlanEntry->OtherQPresentTimer.u1TimerType =
                    SNOOP_OTHER_QUERIER_PRESENT_TIMER;

                /* Start Other Querier present interval timer */
                SnoopTmrStartTimer (&pSnoopVlanEntry->OtherQPresentTimer,
                                    pSnoopVlanEntry->u2OtherQPresentInt);
            }
        }
        /* Router port purge timer is started on all learnt
         * router ports in this VLAN.
         */
        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pSnoopRtrPortEntry, tSnoopRtrPortEntry *)
        {
            SnoopUtilStartRtrPortPurgeTmr (u4Instance, pSnoopRtrPortEntry,
                                           pSnoopRtrPortEntry->u1OperVer,
                                           pSnoopRtrPortEntry->
                                           u4V3RtrPortPurgeInt);
        }
    }

    /* Starting Port Purge Timer for all ports in forwarding port list
     * of group entries.
     */

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        pRBElem = pRBElemNext;

        if (pSnoopGroupEntry->GroupIpAddr.u1Afi != u1AddressType)
        {
            continue;
        }

        pSnoopVlanEntry = NULL;
        if (SnoopVlanGetVlanEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                   pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                   &pSnoopVlanEntry) == SNOOP_FAILURE)
        {
            continue;
        }

        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortNode, tSnoopPortEntry *)
        {
            /* Start the port purge timer for the Port on which V1/V2 
               report was received */

            pSnoopASMPortNode->PurgeOrGrpQueryTimer.u4Instance = u4Instance;
            pSnoopASMPortNode->PurgeOrGrpQueryTimer.u1TimerType =
                SNOOP_ASM_PORT_PURGE_TIMER;

            SnoopTmrStartTimer (&(pSnoopASMPortNode->PurgeOrGrpQueryTimer),
                                pSnoopVlanEntry->u2PortPurgeInt);
        }

        /* Setting u1ReportFwdFlag = SNOOP_REPORT_FORWARD, will forward the
         * forcoming report for this group and start the report forward
         * timer.
         */
        pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedProcessConfigRestoreEvent                    */
/*                                                                           */
/* Description        : The function will be invoked to process the event    */
/*                      RM_CONFIG_RESTORE_COMPLETE                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedProcessConfigRestoreEvent (VOID)
{
    if ((SNOOP_NODE_STATUS () == SNOOP_IDLE_NODE) &&
        (SNOOP_RM_GET_NODE_STATUS () == RM_STANDBY))
    {
        /* Invoke the function to transit the node From Idle state to 
         * Standby state 
         */
        SnoopRedMakeNodeStandbyFromIdle ();
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeStandbyFromIdle                      */
/*                                                                           */
/* Description        : This function will be invoked when the node becomes  */
/*                      standby from the idle state.                         */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeStandbyFromIdle (VOID)
{
    UINT4               u4Instance = 0;
    UINT1               u1AddressType = 0;

    /* Make the SNOOP node as Standby */
    SNOOP_NODE_STATUS () = SNOOP_STANDBY_NODE;
    SNOOP_INIT_NUM_STANDBY_NODES ();

    /* If the SNOOP Protocol is shut, then do nothing,
     * else Enable/Disable the SNOOP protocol based on the Protocol admin
     * status configured.
     */
    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            /* Check if the SNOOP protocol has be to ENABLED/DISABLED as per the
             * Administration configuration.
             */
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                if (SNOOP_PROTOCOL_ADMIN_STATUS
                    (u4Instance, (u1AddressType - 1)) != SNOOP_ENABLE)
                {
                    /* Do nothing since, SNOOP Protocol is disabled */
                    continue;
                }
                /* Enable the SNOOP protocol since Admin configuration is to enable
                 * the SNOOP protocol.
                 */
                SnoopMainEnableSnooping (u4Instance, u1AddressType);

                /* Bulk updates should be triggered to get dynamic updates from
                 * active node. Since bulk updates have to be done sequentially from
                 * lower layer to higher layer, Snooping module will get
                 * L2_INITIATE_BULK_UPDATES from lower layer module(VLAN).
                 */
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedMakeNodeStandbyFromActive                    */
/*                                                                           */
/* Description        : This function transit the node from Active to        */
/*                      Standby.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedMakeNodeStandbyFromActive (VOID)
{
    UINT4               u4Instance = 0;
    UINT1               u1AddressType = 0;
    tSnoopRedMcastCacheNode SnoopRedCacheNode;
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;

    SNOOP_MEM_SET (&SnoopRedCacheNode, 0, sizeof (tSnoopRedMcastCacheNode));

    SNOOP_INIT_NUM_STANDBY_NODES ();

    /* In this transition, Protocol timers started in the active node must be
     * stopped, but NPAPI's should not be invoked. Dynamic protocol changes
     * should be flushed.
     * This state is introduced to stop the protocol timers.
     */
    SNOOP_NODE_STATUS () = SNOOP_ACTIVE_TO_STANDBY_IN_PROGRESS;

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            /* Disable module to flush the dynamic protocol changes */
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                SnoopMainDisableSnooping (u4Instance, u1AddressType);
            }
            SnoopRedCacheNode.u4ContextId = u4Instance;
            pRBElem =
                RBTreeGetNext (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                               &SnoopRedCacheNode, NULL);

            while (pRBElem != NULL)
            {
                pSnoopRedCacheNode = (tSnoopRedMcastCacheNode *) pRBElem;
                pRBNextElem =
                    RBTreeGetNext (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                                   pRBElem, NULL);
                RBTreeRem (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                           (tRBElem *) pSnoopRedCacheNode);
                SNOOP_RED_GRP_FREE_MEMBLK (u4ContextId, pSnoopRedCacheNode);
                pRBElem = pRBNextElem;
            }
        }
    }

    SNOOP_NODE_STATUS () = SNOOP_STANDBY_NODE;

    /* Check if the SNOOP protocol has be to ENABLED/DISABLED as per the
     * Administration configuration.
     */

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                if (SNOOP_PROTOCOL_ADMIN_STATUS
                    (u4Instance, (u1AddressType - 1)) != SNOOP_ENABLE)
                {
                    /* Do nothing since, SNOOP Protocol is disabled */
                    continue;
                }
                /* Enable the SNOOP protocol since Admin configuration is to enable
                 * the SNOOP protocol.
                 */
                SnoopMainEnableSnooping (u4Instance, u1AddressType);

                /* Bulk updates should be triggered to get dynamic updates from
                 * active node. Since bulk updates have to be done sequentially from
                 * lower layer to higher layer, Snooping module will get
                 * L2_INITIATE_BULK_UPDATES from lower layer module(VLAN).
                 */
            }
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveProcessBulkReq                         */
/*                                                                           */
/* Description        : This function sends all dynamically learnt SNOOP     */
/*                      information as a Bulk Update message on knowing that */
/*                      a new standby node has come up.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveProcessBulkReq (VOID *pvPeerId)
{
    INT4                i4VlanSyncRetVal = 0;
    INT4                i4GrpSyncRetVal = 0;
    INT4                i4RtrPortSyncRetVal = 0;
    UINT4               u4Instance = 0;

    UNUSED_PARAM (pvPeerId);

    /* Bulk Request is received indicating that there boots a Standby node 
     * in the network.  Now the information to be synced up should be 
     * send as a Bulk Update Message.
     */

    /* Send all dynamically learnt router port on VLAN basis,
     * changed querier configuration and changed version 
     * configuration as a Bulk update message to the Standby Node.
     */
    if (gu1SnoopInit == SNOOP_FALSE)
    {
        /* SNOOPING completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_SNOOP_APP_ID);
        SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        SnoopRedSendBulkUpdTailMsg ();

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_RED | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised\n");
        return SNOOP_SUCCESS;
    }
    for (u4Instance = SNOOP_RED_BULK_UPD_NEXT_CONTEXTID ();
         u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SNOOPING_STATUS_IN_CONTEXT
            (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_DISABLE)
        {
            SNOOP_RED_BULK_UPD_NEXT_CONTEXTID ()++;
            continue;
        }

        if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry != NULL)
        {
            if (SNOOP_RED_BULK_UPD_VLAN_ENTRY () == NULL &&
                SNOOP_RED_BULK_UPD_FLAG == SNOOP_FALSE)
            {
                SNOOP_RED_BULK_UPD_VLAN_ENTRY () =
                    RBTreeGetFirst (SNOOP_INSTANCE_INFO
                                    (u4Instance)->VlanEntry);
            }
        }

        if (gRtrPortTblRBRoot != NULL)
        {
            if (SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () == NULL &&
                SNOOP_RED_RTR_PORT_BULK_UPD_FLAG == SNOOP_FALSE)
            {
                SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () =
                    RBTreeGetFirst (gRtrPortTblRBRoot);
            }
        }

        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry != NULL)
        {
            if (SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () == NULL &&
                SNOOP_RED_BULK_UPD_FLAG == SNOOP_FALSE)
            {
                SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY ()
                    = RBTreeGetFirst (SNOOP_INSTANCE_INFO
                                      (u4Instance)->MacMcastFwdEntry);
            }
        }

        if (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry != NULL)
        {
            if (SNOOP_RED_BULK_UPD_IP_FWD_ENTRY () == NULL &&
                SNOOP_RED_BULK_UPD_FLAG == SNOOP_FALSE)
            {
                SNOOP_RED_BULK_UPD_IP_FWD_ENTRY ()
                    = RBTreeGetFirst (SNOOP_INSTANCE_INFO
                                      (u4Instance)->IpMcastFwdEntry);
            }
        }

        i4VlanSyncRetVal = SnoopRedVlanSnoopSyncBulkReq (u4Instance);
        if (i4VlanSyncRetVal == SNOOP_FAILURE)
        {
            /* SNOOPING completes it's sync up during standby boot-up and this
             * needs to be informed to RMGR */
            RmSetBulkUpdatesStatus (RM_SNOOP_APP_ID);
            SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;

            /* Send the tail msg to indicate the completion of Bulk
             * update process.*/
            SnoopRedSendBulkUpdTailMsg ();
            return SNOOP_SUCCESS;
        }
        if (SNOOP_RED_RTR_PORT_BULK_UPD_FLAG == SNOOP_FALSE)
        {
            i4RtrPortSyncRetVal = SnoopRedRtrPortSnoopSyncBulkReq ();
        }

        if (i4RtrPortSyncRetVal == SNOOP_FAILURE)
        {
            /* SNOOPING completes it's sync up during standby boot-up and this
             * needs to be informed to RMGR */
            RmSetBulkUpdatesStatus (RM_SNOOP_APP_ID);
            SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;

            /* Send the tail msg to indicate the completion of Bulk
             * update process.*/
            SnoopRedSendBulkUpdTailMsg ();
            return SNOOP_SUCCESS;
        }

        /* Send all learnt multicast group membership information as a
         * a Bulk update message to the Standby Node.
         */
        i4GrpSyncRetVal = SnoopRedGrpInfoSyncBulkReq (u4Instance);

        /* If msg send to RM fails or lack of memory in system, we should
         * not send SNOOP_RED_BULK_UPD_EVENT
         */
        if (i4GrpSyncRetVal == SNOOP_FAILURE)
        {
            /* SNOOPING completes it's sync up during standby boot-up and this
             * needs to be informed to RMGR */
            RmSetBulkUpdatesStatus (RM_SNOOP_APP_ID);
            SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;

            /* Send the tail msg to indicate the completion of Bulk
             * update process.*/
            SnoopRedSendBulkUpdTailMsg ();
            return SNOOP_SUCCESS;
        }
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            /* Check whether more VLAN entires and IP forward table entries are present, for bulk sync */

            if ((SNOOP_RED_BULK_UPD_VLAN_ENTRY () == NULL) &&
                (SNOOP_RED_BULK_UPD_IP_FWD_ENTRY () == NULL) &&
                (SNOOP_RED_BULK_UPD_FLAG == SNOOP_TRUE))
            {
                SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
                SNOOP_RED_BULK_UPD_NEXT_CONTEXTID ()++;
            }
            else
            {
                break;
            }

        }

        if (SNOOP_RED_BULK_UPD_VLAN_ENTRY () == NULL &&
            SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () == NULL &&
            SNOOP_RED_BULK_UPD_FLAG == SNOOP_TRUE)
        {
            SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
            SNOOP_RED_BULK_UPD_NEXT_CONTEXTID ()++;
        }
        else
        {
            break;
        }
    }

    if (SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () < SNOOP_MAX_INSTANCES)
    {
        SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_RED_BULK_UPD_EVENT);
    }
    else
    {
        /* SNOOPING completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        RmSetBulkUpdatesStatus (RM_SNOOP_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;
        SnoopRedSendBulkUpdTailMsg ();
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedRtrPortSnoopSyncBulkReq                      */
/*                                                                           */
/* Description        : This function sends dynamically learnt router port   */
/*                      information to the standby node.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedRtrPortSnoopSyncBulkReq (VOID)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRmMsg             *pMsg = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    UINT4               u4InstId = 0;
    UINT4               u4BulkUpdRtrPortCnt = 0;
    UINT2               u2BufSize = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1PktType = 0;
    BOOL1               bQuerierRtrPort = SNOOP_FALSE;
    BOOL1               bDynRtrPort = SNOOP_FALSE;

    pRBElem = SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY ();
    if (pRBElem != NULL)
    {
        pSnoopRtrPortEntry = (tSnoopRtrPortEntry *) pRBElem;
        u4InstId = SNOOP_GLOBAL_INSTANCE (pSnoopRtrPortEntry->u4PhyPortIndex);
        /* This is done for trace messages */
        gu4SnoopTrcInstId = u4InstId;
        gu4SnoopDbgInstId = u4InstId;
    }
    else
    {
        SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_TRUE;
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "SNOOP Node going to form Rtr Port Bulk Update \r\n");

    u2BufSize = SNOOP_BULK_SPLIT_MSG_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation failure for forming bulk update of \
                 Snoop Red Rtr Port Sync Data\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for forming bulk update of snoop Red Rtr Port Sync Data\r\n");
        return SNOOP_FAILURE;
    }

    u4Offset = SNOOP_RM_OFFSET;

    /* Number of Vlans per sub bulk update */
    u4BulkUpdRtrPortCnt = SNOOP_RED_NO_OF_RTR_PORTS_PER_BULK_UPDATE;
    while ((pRBElem != NULL) && (u4BulkUpdRtrPortCnt > 0))
    {
        pSnoopRtrPortEntry = (tSnoopRtrPortEntry *) pRBElem;

        /* Fill the Rtr Port Snoop bulk update buffer with Router Port  
         * information . */
        pSnoopVlanEntry = pSnoopRtrPortEntry->pVlanEntry;
        SNOOP_IS_PORT_PRESENT (pSnoopRtrPortEntry->u4Port,
                               pSnoopVlanEntry->DynRtrPortBitmap, bDynRtrPort);

        if (bDynRtrPort == OSIX_TRUE)
        {
            SNOOP_IS_PORT_PRESENT (pSnoopRtrPortEntry->u4Port,
                                   pSnoopVlanEntry->QuerierRtrPortBitmap,
                                   bQuerierRtrPort);

            u1PktType =
                (bQuerierRtrPort ==
                 SNOOP_TRUE) ? SNOOP_QUERIER_RTR_PORT :
                SNOOP_NON_QUERIER_RTR_PORT;

            /* Fill the type of Msg Type as IGS_RED_RTR_PORT */
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                u1MsgType = IGS_RED_RTR_PORT;
            }

            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

            /* Fill the Length of the router port sync message for VLAN. */
            u2MsgLen =
                (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                         SNOOP_PORT_FIELD_SIZE + 3);

            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

            /* Fill the Instance Id */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

            /* Fill the Outer-VLAN Identifier */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Inner-VLAN Identifier */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Address Type */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNOOP_ADDR_TYPE_IPV4);

            /* Fill the Port Number */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, pSnoopRtrPortEntry->u4Port);

            /* Fill the Version */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset,
                                 pSnoopRtrPortEntry->u1OperVer);

            /* Fill the PacketType */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PktType);

            if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_RTR_PORT_MSG_LEN)
            {
                /* The current buffer is full, hence transmit the 
                 * information contained in the current buffer and create 
                 * another buffer to fill the sync message.*/
                if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                               RM_SNOOP_APP_ID,
                                               RM_SNOOP_APP_ID) != RM_SUCCESS)
                {
                    RM_FREE (pMsg);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Enqueuing Vlan Snoop Sync data fails \r\n");
                    return SNOOP_FAILURE;
                }

                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_RED_TRC,
                               "Memory Pool creation failure for Snoop Red "
                               "Router port Sync Data\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "Router port Sync Data\r\n");
                    return SNOOP_FAILURE;
                }

                /* Reset the Buffer offset */
                u4Offset = SNOOP_RM_OFFSET;
            }
        }

        if (pSnoopRtrPortEntry->u1OperVer == SNOOP_IGS_IGMP_VERSION3)
        {
            /* Fill the type of Msg Type as IGS_RED_RTR_PORT_INTR */
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                u1MsgType = IGS_RED_RTR_PORT_INTR;
            }

            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

            /* Fill the Length of the router port sync message for a port. */
            u2MsgLen =
                (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                         SNOOP_PORT_FIELD_SIZE + 5);
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

            /* Fill the Instance Id */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

            /* Fill the Outer-VLAN Identifier */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Inner-VLAN Identifier */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Address Type */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNOOP_ADDR_TYPE_IPV4);

            /* Fill the Port Number */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, pSnoopRtrPortEntry->u4Port);

            /* Fill the V3 Router port Interval */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset,
                                 pSnoopRtrPortEntry->u4V3RtrPortPurgeInt);

            if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_RTR_PORT_MSG_LEN)
            {
                /* The current buffer is full, hence transmit the 
                 * information contained in the current buffer and create 
                 * another buffer to fill the sync message.*/
                /* pMsg is freed only in failure case. RM will free the buf
                 * in success case. */
                if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                               RM_SNOOP_APP_ID,
                                               RM_SNOOP_APP_ID) != RM_SUCCESS)
                {
                    RM_FREE (pMsg);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Enqueuing Snoop RtrPort Sync data fails \r\n");
                    return SNOOP_FAILURE;
                }

                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Memory Pool creation failure for Snoop Red "
                                   "Router port Sync Data\r\n");
                    return SNOOP_FAILURE;
                }

                /* Reset the Buffer offset */
                u4Offset = SNOOP_RM_OFFSET;
            }
        }

        pRBElem = RBTreeGetNext (gRtrPortTblRBRoot, pRBElem, NULL);

        u4BulkUpdRtrPortCnt--;

        SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () = pRBElem;
    }

    if (SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () == NULL)
    {
        SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_TRUE;
    }

    if (u4Offset != 0)
    {
        /* Call the API provided by RM to send the 
         * bulk update message to RM for standby */
        if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                       RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
            != RM_SUCCESS)
        {
            RM_FREE (pMsg);

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Enqueuing Vlan Snoop Sync data failes \r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* Free the Allocated Buffer */
        RM_FREE (pMsg);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedVlanSnoopSyncBulkReq                         */
/*                                                                           */
/* Description        : This function sends dynamically learnt router port   */
/*                      on VLAN basis, changed querier configuration and     */
/*                      changed version configuration as a Bulk update       */
/*                      message to the standby node.                         */
/*                                                                           */
/* Input(s)           : u4Instance - Virtual ContextId                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedVlanSnoopSyncBulkReq (UINT4 u4Instance)
{
    UINT1              *pDynamicRtrPortBitMap = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2BufSize = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLength = 0;
    UINT4               u4BulkUpdVlanCnt = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RED, SNOOP_RED_TRC,
               "SNOOP Node going to form Vlan Snoop Bulk Update \r\n");

    pDynamicRtrPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDynamicRtrPortBitMap == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pDynamicRtrPortBitMap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pDynamicRtrPortBitMap, 0, sizeof (tSnoopPortBmp));

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "No Vlan Entry found for this instance. Hence no Bulk "
                   "Updates\r\n");
        UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
        return SNOOP_SUCCESS;
    }

    u2BufSize = SNOOP_BULK_SPLIT_MSG_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation failure for forming bulk update of \
                 Snoop Red Group Sync Data\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);

        return SNOOP_FAILURE;
    }

    u4Offset = SNOOP_RM_OFFSET;

    /* Number of Vlans per sub bulk update */
    u4BulkUpdVlanCnt = SNOOP_RED_NO_OF_VLANS_PER_BULK_UPDATE;

    pRBElem = SNOOP_RED_BULK_UPD_VLAN_ENTRY ();

    /* For each VLAN present in the Vlan Entry, use the dynamically learnt
     * router ports, changed querier configuration and changed IGMP version
     * information to form bulk updates.
     */
    while (pRBElem != NULL && u4BulkUpdVlanCnt > 0)
    {
        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        /* Fill the Vlan Snoop bulk update buffer with Router Port list 
         * information for this VLAN. */
        if (SNOOP_SLL_COUNT (&pSnoopVlanEntry->RtrPortList) != 0)
        {
            SNOOP_MEM_SET (pDynamicRtrPortBitMap, 0, sizeof (tSnoopPortBmp));

            MEMCPY (pDynamicRtrPortBitMap, pSnoopVlanEntry->DynRtrPortBitmap,
                    sizeof (tSnoopPortBmp));
            /* If there exist any space in the buffer to hold this
             * vlan router port list.
             */
            if ((UINT2) (u2BufSize - u4Offset) <
                SNOOP_RED_RTR_PORT_LIST_MSG_LEN)
            {
                /* The current buffer is full, hence transmit the 
                 * information contained in the current buffer and create 
                 * another buffer to fill the sync message.*/
                if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                               RM_SNOOP_APP_ID,
                                               RM_SNOOP_APP_ID) != RM_SUCCESS)
                {
                    RM_FREE (pMsg);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Enqueuing Vlan Snoop Sync data fails \r\n");
                    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                    return SNOOP_FAILURE;
                }

                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Memory Pool creation failure for Snoop Red "
                                   "Vlan Snoop Sync Data\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "Vlan Snoop Sync Data\r\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    SnoopRmHandleProtocolEvent (&ProtoEvt);
                    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                    return SNOOP_FAILURE;
                }

                /* Reset the Buffer offset */
                u4Offset = SNOOP_RM_OFFSET;
            }

            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                /* Fill the Message Type as IGS_RED_RTR_PORT_LIST */
                u1MsgType = IGS_RED_RTR_PORT_LIST;
            }

            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

            /* Fill the Length of sync = InstId Size + VlanID size + 
             * Router Port list size + Querier Router Port list size */
            u2MsgLength = (UINT2) (SNOOP_INST_ID_FIELD_SIZE +
                                   SNOOP_VLANID_FIELD_SIZE +
                                   SNOOP_PORT_LIST_SIZE + SNOOP_PORT_LIST_SIZE);

            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLength);

            /* Fill the Instance Id */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

            /* Fill the Outer-Vlan ID */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Inner-Vlan ID */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Dynamic Router port list */
            SNOOP_RM_PUT_N_BYTE (pMsg, pDynamicRtrPortBitMap, &u4Offset,
                                 SNOOP_PORT_LIST_SIZE);

            /* Fill the Querier Router Port List */
            SNOOP_RM_PUT_N_BYTE (pMsg, pSnoopVlanEntry->QuerierRtrPortBitmap,
                                 &u4Offset, SNOOP_PORT_LIST_SIZE);
        }

        /* Check if the querier configuration is changed for this VLAN,
         * if so then include the configuration change information in the 
         * bulk update message.
         */

        if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_QUER_MSG_LEN)
        {
            /* Call the API provided by RM to send the 
             * bulk update message to RM for standby */
            if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                           RM_SNOOP_APP_ID,
                                           RM_SNOOP_APP_ID) != RM_SUCCESS)
            {
                RM_FREE (pMsg);

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Memory Pool creation failure Snoop Red Vlan "
                               "Snoop Sync Data\r\n");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "Snoop Sync Data\r\n");
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                SnoopRmHandleProtocolEvent (&ProtoEvt);
                UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                return SNOOP_FAILURE;
            }

            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Memory Pool creation failure "
                               "for Snoop Red Vlan Snoop Sync Data\r\n");
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                SnoopRmHandleProtocolEvent (&ProtoEvt);
                UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                return SNOOP_FAILURE;
            }

            /* Reset the Buffer offset */
            u4Offset = SNOOP_RM_OFFSET;
        }

        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            /* Fill the type of Event as IGS_RED_QUERIER_CONFIG */
            u1MsgType = IGS_RED_QUERIER_CONFIG;
        }

        SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

        /* Fill the Instance Id */
        SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

        /* Fill the Outer-VLAN Identifier */
        SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                             SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

        /* Fill the Inner-VLAN Identifier */
        SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                             SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

        /* Fill QUERIER / NON_QUERIER information */
        SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, pSnoopVlanEntry->u1Querier);
        SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset,
                             pSnoopVlanEntry->u4ElectedQuerier);

        {
            if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_STARTUP_QRY_MSG_LEN)
            {
                /* Call the API provided by RM to send the 
                 * bulk update message to RM for standby */
                if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                               RM_SNOOP_APP_ID,
                                               RM_SNOOP_APP_ID) != RM_SUCCESS)
                {
                    RM_FREE (pMsg);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Memory Pool creation failure Snoop Red Vlan startup query count"
                                   "Snoop Sync Data\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "Snoop startup query count Sync Data\r\n");
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    SnoopRmHandleProtocolEvent (&ProtoEvt);
                    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                    return SNOOP_FAILURE;
                }

                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Memory Pool creation failure "
                                   "for Snoop Red Vlan startup query count Sync Data\r\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    SnoopRmHandleProtocolEvent (&ProtoEvt);
                    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
                    return SNOOP_FAILURE;
                }

                /* Reset the Buffer offset */
                u4Offset = SNOOP_RM_OFFSET;
            }

            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                /* Fill the type of Event as IGS_RED_STARTUP_QRY_COUNT_SYNC */
                u1MsgType = IGS_RED_STARTUP_QRY_COUNT_SYNC;
            }

            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

            /* Fill the Instance Id */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

            /* Fill the Outer-Vlan Identifier */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            /* Fill the Inner-Vlan Identifier */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset,
                                 pSnoopVlanEntry->u1StartUpQCount);
        }

        pRBElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        u4BulkUpdVlanCnt--;

        SNOOP_RED_BULK_UPD_VLAN_ENTRY () = pRBElem;
    }
    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);
    if (SNOOP_RED_BULK_UPD_VLAN_ENTRY () == NULL)
    {
        SNOOP_RED_BULK_UPD_FLAG = SNOOP_TRUE;
    }

    if (u4Offset != 0)
    {
        /* Call the API provided by RM to send the 
         * bulk update message to RM for standby */
        if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                       RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
            != RM_SUCCESS)
        {
            RM_FREE (pMsg);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                       "Enqueuing Vlan Snoop Sync data failes \r\n");
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* Free the Allocated Buffer */
        RM_FREE (pMsg);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedGrpInfoSyncBulkReq                           */
/*                                                                           */
/*                                                                           */
/* Input(s)           : pvPeerId - Id of Standby Node                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedGrpInfoSyncBulkReq (UINT4 u4Instance)
{
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tRBElem            *pRBElem = NULL;
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2BufSize = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLength = 0;
    UINT4               u4BulkUpdFwdEntryCount = 0;

    MEMSET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Mcast Group Membership Info Sync Bulk Req \r\n");

    u2BufSize = SNOOP_BULK_SPLIT_MSG_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation for Mcast Group Membership "
                       "Info failed \r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        return SNOOP_FAILURE;
    }

    u4Offset = SNOOP_RM_OFFSET;

    /* Number of MAC fwd entries per sub bulk update */
    u4BulkUpdFwdEntryCount = SNOOP_RED_NO_OF_MAC_FWD_ENTRIES_PER_SUB_UPDATE;

    pRBElem = SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY ();

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry == NULL)
        {
            RM_FREE (pMsg);

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "No Multicast Entry present hence no Bulk updates \r\n");
            return SNOOP_FAILURE;
        }
        pRBElem = SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY ();

        while (pRBElem != NULL && u4BulkUpdFwdEntryCount > 0)
        {
            pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

            SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                            pTempSnoopGroupEntry, tSnoopGroupEntry *)
            {
                if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_GRP_INFO_MSG_LEN)
                {
                    /* Call the API provided by RM to send the 
                     * bulk update message to RM for standby */
                    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u4Offset,
                                                   RM_SNOOP_APP_ID,
                                                   RM_SNOOP_APP_ID) !=
                        RM_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_RED_TRC,
                                   "Enqueuing Grp Info Sync data failes \r\n");
                        RM_FREE (pMsg);
                        ProtoEvt.u4Error = RM_SENDTO_FAIL;
                        SnoopRmHandleProtocolEvent (&ProtoEvt);
                        return SNOOP_FAILURE;
                    }

                    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_RED_TRC,
                                   "Memory Pool creation failure for Snoop Red "
                                   "Vlan Snoop Sync Data\r\n");
                        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                        SnoopRmHandleProtocolEvent (&ProtoEvt);
                        return SNOOP_FAILURE;
                    }

                    /* Reset the Buffer offset */
                    u4Offset = SNOOP_RM_OFFSET;
                }

                /* Fill the Message Type as IGS_RED_GRP_INFO_MSG */
                SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, IGS_RED_GRP_INFO_MSG);

                /* Fill the Length of VlanID + Group IP + Port list size */
                u2MsgLength = (UINT2) (SNOOP_INST_ID_FIELD_SIZE +
                                       SNOOP_VLANID_FIELD_SIZE +
                                       (sizeof (tIPvXAddr)) +
                                       SNOOP_PORT_LIST_SIZE +
                                       SNOOP_PORT_LIST_SIZE);

                SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLength);

                /* Fill the Instance Id */
                SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

                /* Fill the Outer-Vlan ID */
                SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (pTempSnoopGroupEntry->
                                                       VlanId));

                /* Fill the Inner-Vlan ID */
                SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (pTempSnoopGroupEntry->
                                                       VlanId));

                /* Fill the Group IP Address */
                SNOOP_RM_PUT_N_BYTE (pMsg, &(pTempSnoopGroupEntry->GroupIpAddr),
                                     &u4Offset, sizeof (tIPvXAddr));

                /* Fill the Member port list */
                SNOOP_RM_PUT_N_BYTE (pMsg, pTempSnoopGroupEntry->PortBitmap,
                                     &u4Offset, SNOOP_PORT_LIST_SIZE);

                /* Fill the V1 forwarding port list */
                SNOOP_RM_PUT_N_BYTE (pMsg, pTempSnoopGroupEntry->V1PortBitmap,
                                     &u4Offset, SNOOP_PORT_LIST_SIZE);
            }

            pRBElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               MacMcastFwdEntry, pRBElem, NULL);

            u4BulkUpdFwdEntryCount--;

            SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = pRBElem;
        }

        if (SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () == NULL)
        {
            SNOOP_RED_BULK_UPD_FLAG = SNOOP_TRUE;
        }
    }

    else if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        pRBElem = SNOOP_RED_BULK_UPD_IP_FWD_ENTRY ();

        while ((pRBElem != NULL) && (u4BulkUpdFwdEntryCount > 0))
        {
            IpGrpFwdEntry = *(tSnoopIpGrpFwdEntry *) pRBElem;
            pRBElem =
                RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           (tRBElem *) & IpGrpFwdEntry);

            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if ((UINT2) (u2BufSize - u4Offset) < SNOOP_RED_IP_INFO_MSG_LEN)
            {
                /* Call the API provided by RM to send the
                 * bulk update message to RM for standby */
                if (SnoopRmEnqMsgToRmFromAppl (pMsg, (UINT2) u4Offset,
                                               RM_SNOOP_APP_ID,
                                               RM_SNOOP_APP_ID) != RM_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_RED_TRC,
                               "Enqueuing Grp Info Sync data failes \r\n");
                    RM_FREE (pMsg);
                    ProtoEvt.u4Error = RM_SENDTO_FAIL;
                    SnoopRmHandleProtocolEvent (&ProtoEvt);
                    return SNOOP_FAILURE;
                }

                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_RED_TRC,
                               "Memory Pool creation failure for Snoop Red "
                               "Vlan Snoop Sync Data\r\n");
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    SnoopRmHandleProtocolEvent (&ProtoEvt);
                    return SNOOP_FAILURE;
                }

                /* Reset the Buffer offset */
                u4Offset = SNOOP_RM_OFFSET;
            }

            /* Fill the Message Type as IGS_RED_GRP_INFO_MSG */
            SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, IGS_RED_IP_INFO_MSG);

            /* Fill the Length of VlanID + Group IP + Port list size */
            u2MsgLength = (UINT2) (SNOOP_INST_ID_FIELD_SIZE +
                                   SNOOP_VLANID_FIELD_SIZE +
                                   (sizeof (tIPvXAddr)) + (sizeof (tIPvXAddr)) +
                                   SNOOP_PORT_LIST_SIZE + SNOOP_PORT_LIST_SIZE);

            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLength);

            /* Fill the Instance Id */
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);
            /* Fill the Outer-Vlan ID */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->
                                                   VlanId));

            /* Fill the Inner-Vlan ID */
            SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                                 SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->
                                                   VlanId));
            SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset,
                                 pSnoopIpGrpFwdEntry->u4SnoopHwId);
            /* Fill the Source IP Address */
            SNOOP_RM_PUT_N_BYTE (pMsg, &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                                 &u4Offset, sizeof (tIPvXAddr));

            /* Fill the Group IP Address */
            SNOOP_RM_PUT_N_BYTE (pMsg, &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                                 &u4Offset, sizeof (tIPvXAddr));

            /* Fill the Member port list */
            SNOOP_RM_PUT_N_BYTE (pMsg, pSnoopIpGrpFwdEntry->PortBitmap,
                                 &u4Offset, SNOOP_PORT_LIST_SIZE);

            pRBElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               IpMcastFwdEntry, pRBElem, NULL);

            u4BulkUpdFwdEntryCount--;

            SNOOP_RED_BULK_UPD_IP_FWD_ENTRY () = pRBElem;
        }

        if (SNOOP_RED_BULK_UPD_IP_FWD_ENTRY () == NULL)
        {
            SNOOP_RED_BULK_UPD_FLAG = SNOOP_TRUE;
        }

    }

    if (u4Offset != 0)
    {
        /* Call the API provided by RM to send the 
         * bulk update message to RM for standby */
        if (SnoopRmEnqMsgToRmFromAppl (pMsg, (UINT2) u4Offset,
                                       RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
            != RM_SUCCESS)
        {
            RM_FREE (pMsg);

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Enqueuing Grp Info Sync data fails \r\n");
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* Free the Allocated Buffer */
        RM_FREE (pMsg);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendRtrPortVerSync                     */
/*                                                                           */
/* Description        : This function sends the dynamic sync of router ports */
/*                      for the VLAN from SNOOP protocol in Active           */
/*                      node to SNOOP protocol in Standby node through RM.   */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Number                      */
/*                      u4Port        - Port Number                          */
/*                      u1Version     - IGMP version                         */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                      containing router information to be  */
/*                                      synced up.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendRtrPortVerSync (UINT4 u4InstId, UINT4 u4Port, UINT1 u1Version,
                                  tSnoopVlanEntry * pSnoopVlanEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT2               u2DataLen = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1PktType = 0;
    BOOL1               bQuerierRtrPort = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Router Ports Sync Data\r\n");

    /* Form a dynamic sync up message of router port list for VLAN
     * in the following format :-

     *      <-----1 Byte-----><---2---> <--1--><--2--><---1--><--4--><--1-->
     ************************************************************************
     *     *                   * Length *      *      * Addr  * Port * Vers *
     * RM  *IGS_RED_RTR_PORT   * 1+2+1+2* Inst * Vlan * Type  * Num  * ion  *
     * Hdr *                   * 1+4+1+1*  Id  *  Id  *       *      *      *
     *     *                   * Bytes  *      *      *       *      *      *
     ************************************************************************
     <--1-->
     ********
     *Port  *
     *Type  *
     ********   

     * The RM Hdr shall be included by RM.
     */

    u2DataLen = (UINT2) SNOOP_RED_RTR_PORT_MSG_LEN;

    /* Allocate memory for data to be sent to standby through RM.
     * This memory will be freed by RM after Txmitting
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Memory Pool creation failure for Router"
                       " Port Sync Data\r\n");

        return (SNOOP_FAILURE);
    }

    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                           bQuerierRtrPort);

    u1PktType = (bQuerierRtrPort == SNOOP_TRUE) ? SNOOP_QUERIER_RTR_PORT :
        SNOOP_NON_QUERIER_RTR_PORT;

    /* Fill the type of Msg Type as IGS_RED_RTR_PORT */
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u1MsgType = IGS_RED_RTR_PORT;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Length of the router port sync message for VLAN. */
    u2MsgLen = (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                        SNOOP_PORT_FIELD_SIZE + 3);
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Address Type */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNOOP_ADDR_TYPE_IPV4);

    /* Fill the Port Number */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Port);

    /* Fill the Version */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1Version);

    /* Fill the PacketType */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1PktType);

    /* Call the API provided by RM to send the formed sync message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Router Port Sync data fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendV3RtrPortIntrSync                  */
/*                                                                           */
/* Description        : This function sends the dynamic sync of V3 router    */
/*                      port purge interval for a port from SNOOP protocol   */
/*                      in Active node to Standby node through RM.           */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Number                      */
/*                      u4Port        - Port Number                          */
/*                      u4Interval    - V3 Router Port Purge Interval        */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                        containing router port interval    */
/*                                        information to be synced up.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendV3RtrPortIntrSync (UINT4 u4InstId, UINT4 u4Port,
                                     UINT4 u4Interval,
                                     tSnoopVlanEntry * pSnoopVlanEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT2               u2DataLen = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Router Ports purge Interval Sync Data\r\n");

    /* Form a dynamic sync up message of router port interval for port
     * in the following format :-

     *    <--------1 Byte--------><---2---> <--1--><--2--><---1--><--4--><---4--->
     *****************************************************************************
     *    *                      * Length *      *      * Addr  * Port * purge  *
     * RM *IGS_RED_RTR_PORT_INTR * 1+2+1+2* Inst * Vlan * Type  * Num  *interval*
     * Hdr*                      * 1+4+1+4*  Id  *  Id  *       *      *        *
     *    *                      * Bytes  *      *      *       *      *        *
     ******************************************************************************
     * The RM Hdr shall be included by RM.
     */

    u2DataLen = (UINT2) SNOOP_RED_RTR_INTR_MSG_LEN;

    /* Allocate memory for data to be sent to standby through RM.
     * This memory will be freed by RM after Txmitting
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Memory Pool creation failure for Router"
                       " Port Sync Data\r\n");
        return (SNOOP_FAILURE);
    }

    /* Fill the type of Msg Type as IGS_RED_RTR_PORT_INTR */
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u1MsgType = IGS_RED_RTR_PORT_INTR;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Length of the router port sync message for a port. */
    u2MsgLen = (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                        SNOOP_PORT_FIELD_SIZE + 5);
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Address Type */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, SNOOP_ADDR_TYPE_IPV4);

    /* Fill the Port Number */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Port);

    /* Fill the V3 Router port Interval */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Interval);

    /* Call the API provided by RM to send the formed sync message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Router Port interval Sync data fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }
    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendRtrPortListSync                    */
/*                                                                           */
/* Description        : This function sends the dynamic sync of router port  */
/*                      list for the VLAN from SNOOP protocol in Active      */
/*                      node to SNOOP protocol in Standby node through RM.   */
/*                                                                           */
/* Input(s)           : u4InstId      - Instance Number                      */
/*                      pSnoopVlanEntry - Pointer to Vlan Entry structure    */
/*                                      containing router information to be  */
/*                                      synced up.                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendRtrPortListSync (UINT4 u4InstId,
                                   tSnoopVlanEntry * pSnoopVlanEntry)
{
    UINT1              *pDynamicRtrPortBitMap = NULL;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT2               u2DataLen = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Router Ports Sync Data\r\n");

    /* Form a dynamic sync up message of router port list for VLAN
     * in the following format :-

     *    <-----1 Byte-----><---2---><--1--><--2--><---N---><----N--------> 
     **********************************************************************
     *     *                * Length *      *      * Rtr   *              *
     * RM  *IGS_RED_RTR_PORT* 1+2+N+N* Inst * Vlan * Port  * Querier Rtr  *
     * Hdr *_LIST           * Bytes  *  Id  *  Id  * List  * Port List    *
     **********************************************************************

     * The RM Hdr shall be included by RM.
     */

    u2DataLen = (UINT2) (SNOOP_RED_RTR_PORT_LIST_MSG_LEN);

    /* Allocate memory for data to be sent to standby through RM.
     * This memory will be freed by RM after Txmitting
     */
    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Memory Pool creation failure for Router Port Sync Data\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for Router Port Sync Data for u4InstId %d\r\n",
                               u4InstId);
        return (SNOOP_FAILURE);
    }

    /* Fill the type of Msg Type as IGS_RED_RTR_PORT_LIST */
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u1MsgType = IGS_RED_RTR_PORT_LIST;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Length of the router port sync message for VLAN. */
    u2MsgLen = (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                        SNOOP_PORT_LIST_SIZE + SNOOP_PORT_LIST_SIZE);
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopVlanEntry->VlanId));

    pDynamicRtrPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDynamicRtrPortBitMap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pDynamicRtrPortBitMap\r\n");
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pDynamicRtrPortBitMap, 0, sizeof (tSnoopPortBmp));

    /* Fill the Dynamically learnt Router Port List */
    MEMCPY (pDynamicRtrPortBitMap, pSnoopVlanEntry->DynRtrPortBitmap,
            sizeof (tSnoopPortBmp));

    /* Fill the Dynamically learnt router port list */
    SNOOP_RM_PUT_N_BYTE (pMsg, pDynamicRtrPortBitMap, &u4Offset,
                         SNOOP_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pDynamicRtrPortBitMap);

    /* Fill the Querier learnt Router Port List */
    SNOOP_RM_PUT_N_BYTE (pMsg, pSnoopVlanEntry->QuerierRtrPortBitmap,
                         &u4Offset, SNOOP_PORT_LIST_SIZE);

    /* Call the API provided by RM to send the formed sync message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Router Port Sync data fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendGrpInfoSync                        */
/*                                                                           */
/* Description        : This function sends the multicast group membership   */
/*                      update information from SNOOP in Active node to      */
/*                      SNOOP in Standby node through RM.                    */
/*                                                                           */
/* Input(s)           : u4InstId     - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Address          */
/*                      FwdPortList  - Learnt Forwarding Port List           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendGrpInfoSync (UINT4 u4InstId,
                               tSnoopGroupEntry * pSnoopGroupEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT2               u2DataLen = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Multicast Group Membership information Sync Up \r\n");

    /* Allocate memory for dynamic sync of Multicast group membership
     * information sync to the standby through RM.
     * This memory will be freed by RM after Txmitting.
     */
    u2DataLen = (UINT2) SNOOP_RED_GRP_INFO_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation failure for forming dynamic sync of \
                 Multicast Group Membership Info fails\r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a dynamic sync up message for updation of Multicast group 
     * membership information with the following format :-

     *        <---1 Bytes---><---2---><--1--><-2-><-----20----><--N--><--N-->  
     ************************************************************************
     *        *             *Length  *      *     * Multicast * Fwd  * V1   *
     * RM Hdr * IGS_RED_GRP *1+2+4+N * Inst *Vlan * Group IP  * Port * Port *
     *        * _INFO_MSG   *Bytes   *  Id  * Id  * Addr      * List * List *
     ************************************************************************

     * The RM Hdr shall be included by RM.
     */

    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_GRP_INFO_MSG */
        u1MsgType = IGS_RED_GRP_INFO_MSG;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Group Membership information length */
    u2MsgLen = (UINT2) (SNOOP_INST_ID_FIELD_SIZE + SNOOP_VLANID_FIELD_SIZE +
                        sizeof (tIPvXAddr) + SNOOP_PORT_LIST_SIZE +
                        SNOOP_PORT_LIST_SIZE);
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId));

    /* Fill the Multicast Group IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, &(pSnoopGroupEntry->GroupIpAddr),
                         &u4Offset, sizeof (tIPvXAddr));

    /* Fill the updated forwarding port list for this Multicast Group
     * IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, pSnoopGroupEntry->PortBitmap, &u4Offset,
                         SNOOP_PORT_LIST_SIZE);

    /* Fill the updated V1 forwarding port list for this Multicast Group
     * IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, pSnoopGroupEntry->V1PortBitmap, &u4Offset,
                         SNOOP_PORT_LIST_SIZE);

    /* Call the API provided by RM to send the formed sync up 
     * message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Group Membership Info fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopActiveRedGrpSync                                */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4InstId     - Instance Id                           */
/*             SnoopGrpIpAddr - Multicast Group IP Address           */
/*                PortBitmap - Forward port bit map             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopActiveRedGrpSync (UINT4 u4Instance,
                       tIPvXAddr SrcIpAddr, tSnoopGroupEntry * pSnoopGroupEntry,
                       tSnoopPortBmp PortBitmap)
{
    tSnoopIpGrpFwdEntry *pIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2DataLen = 0;
    UINT1               u1MsgType = 0;

    MEMSET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((RmGetNodeState () != RM_ACTIVE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        return (SNOOP_FAILURE);
    }

    if (SnoopFwdGetIpForwardingEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                      pSnoopGroupEntry->GroupIpAddr, SrcIpAddr,
                                      &pIpGrpFwdEntry) == SNOOP_FAILURE)
    {
        return SNOOP_FAILURE;
    }
    SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                        "Multicast Forwarding information Sync Up for Vlan : %d and Group : %s\r\n",
                        SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                        SnoopPrintIPvxAddress (pSnoopGroupEntry->GroupIpAddr));

    /* Allocate memory for dynamic sync of Multicast forwarding
     * information sync to the standby through RM.
     * This memory will be freed by RM after Txmitting.
     */
    u2DataLen = (UINT2) SNOOP_RED_IP_INFO_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation failure for forming dynamic sync of \
                 Multicast Forwarding Info fails\r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a dynamic sync up message for updation of Multicast forwarding table 
     * information with the following format :-

     *<---1---><-----2----><---1---><---2--><-20-><----N----><----N---><--N--><--N-->
     ********************************************************************************
     *        *             *Length  *      *     * Group IP * Source * Fwd  * V1   *
     * RM Hdr * IGS_RED_IP  *1+2+4+N * Inst *Vlan * Addr     * Addr   * Port * Port *
     *        * _INFO_MSG   *Bytes   *  Id  * Id  *          *        * List * List *
     ********************************************************************************

     * The RM Hdr shall be included by RM.
     */

    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_IP_INFO_MSG */
        u1MsgType = IGS_RED_IP_INFO_MSG;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    u2MsgLen = SNOOP_RED_IP_INFO_MSG_LEN;
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId));

    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, pIpGrpFwdEntry->u4SnoopHwId);
    /* Fill the Multicast Source IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, &(SrcIpAddr), &u4Offset, sizeof (tIPvXAddr));

    /* Fill the Multicast Group IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, &(pSnoopGroupEntry->GroupIpAddr),
                         &u4Offset, sizeof (tIPvXAddr));

    /* Fill the updated forwarding port list */
    SNOOP_RM_PUT_N_BYTE (pMsg, PortBitmap, &u4Offset, SNOOP_PORT_LIST_SIZE);

    /* Call the API provided by RM to send the formed sync up
     * message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Frwd Info fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendQuerierSync                        */
/*                                                                           */
/* Description        : This function sends the dynamic sync of change in    */
/*                      querier information for a VLAN in SNOOP protocol in  */
/*                      Active node to SNOOP in Standby node through RM      */
/*                                                                           */
/* Input(s)           : u4InstId    - Instance Id                            */
/*                      VlanId      - Vlan Identifier                        */
/*                      u1Querier   - SNOOP_QUERIER / SNOOP_NON_QUERIER      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendQuerierSync (UINT4 u4InstId, tSnoopTag VlanId,
                               UINT1 u1Querier, UINT1 u1AddressType,
                               UINT4 u4QuerierAddress)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT2               u2DataLen = 0;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "IGMP Querier Info Sync Fails \r\n");

    /* Allocate memory for dynamic sync of change in querier information
     * for a VLAN, to standby through RM.
     */
    u2DataLen = (UINT2) SNOOP_RED_QUER_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Buffer creation failure for forming Querier information \
                 sync fails \r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a dynamic sync up message for change in querier information
     * with the following format :-

     *        <-------- 1 Byte---------><--1--><--2--><--------1--------> 
     **********************************************************************
     *        *                        *      *       * SNOOP_QUERIER/    *
     * RM Hdr * IGS_RED_QUERIER_CONFIG * Inst * Vlan  * SNOOP_NON_QUERIER *
     *        *                        *  Id  *  Id   *                 *
     **********************************************************************

     * The RM Hdr shall be included by RM.
     */

    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_QUERIER_CONFIG */
        u1MsgType = IGS_RED_QUERIER_CONFIG;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_OUTER_VLAN (VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_INNER_VLAN (VlanId));

    /* Fill QUERIER / NON_QUERIER information */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1Querier);

    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4QuerierAddress);

    /* Call the API provided by RM to send the formed sync up message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Enqueuing Querier Snoop Sync data fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendForwardModeSync                    */
/*                                                                           */
/* Description        : This function sends the updates regarding the change */
/*                      in Multicast fowarding mode of SNOOP in Active node  */
/*                      to SNOOP in Standby node through RM.                 */
/*                                                                           */
/* Input(s)           : u1McastFwdMode  - SNOOP_MCAST_FWD_MODE_MAC/          */
/*                                        SNOOP_MCAST_FWD_MODE_IP            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendForwardModeSync (UINT4 u4InstId, UINT1 u1McastFwdMode)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Mcast Fwd Mode Sync \r\n");

    /* Allocate memory for dynamic sync of change in querier information
     * for a VLAN, to standby through RM.
     */
    u2DataLen = SNOOP_RED_FRWD_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Buffer creation failure for forming Mcast Forward Mode "
                   "Sync \r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a dynamic sync up message for change in Multicast forwarding
     * mode with the following format :-

     *        <-----------1 Byte-------------><--1--><----------1------------> 
     *************************************************************************
     *        *                              *      *                         *
     * RM Hdr * SNOOP_RED_MCAST_FORWARD_MODE * Inst *SNOOP_MCAST_FWD_MODE_MAC * 
     *        *                              *  Id  *SNOOP_MCAST_FWD_MODE_IP  *
     *************************************************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the type of Event as SNOOP_RED_MCAST_FORWARD_MODE */
    u1MsgType = SNOOP_RED_MCAST_FORWARD_MODE;
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Multicast fowarding mode */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1McastFwdMode);

    /* Call the API provided by RM to send the formed sync up message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Enqueuing Multicast forwarding mode fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendDelVlanSync                        */
/*                                                                           */
/* Description        : This function sends the trigger to standby node,     */
/*                      when the VLAN is deleted in active node.             */
/*                                                                           */
/* Input(s)           : u4InstId - Instance Id                               */
/*                      VlanId   - Vlan Id                                   */
/*                      u1AddressType - specifies SNOOP/MLDS                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedActiveSendDelVlanSync (UINT4 u4InstId, tSnoopTag VlanId,
                               UINT1 u1AddressType)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                    "Snoop Protocol Disable for Instance %d \r\n", u4InstId);

    /* Allocate memory for the trigger to be send to standby through RM.
     * This memory will be freed by RM after Txmitting
     */

    u2DataLen = (UINT2) SNOOP_RED_DEL_VLAN_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Buffer creation failure for Snoop Protocol Disable "
                   "Sync fails\r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a trigger to flush all the previously synced messages.

     *        <--------1 Byte-----------><--1--><---2--->
     ***************************************************** 
     *        *                         *      *         *
     * RM Hdr * IGS_RED_DEL_VLAN_SYNC * Inst * Vlan Id   * 
     *        *                         *  Id  *         *  
     *****************************************************

     * The RM Hdr shall be included by RM.
     */
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_DEL_VLAN_SYNC */
        u1MsgType = IGS_RED_DEL_VLAN_SYNC;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-Vlan Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_OUTER_VLAN (VlanId));

    /* Fill the Inner-Vlan Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_INNER_VLAN (VlanId));

    /* Call the API provided by RM to send the data to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        == RM_FAILURE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Snoop Disable Sync fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedActiveSendAddVlanSync                        */
/*                                                                           */
/* Description        : This function sends the trigger to standby node,     */
/*                      when the VLAN is added in active node.               */
/*                                                                           */
/* Input(s)           : u4InstId - Instance Id                               */
/*                      VlanId   - Vlan Id                                   */
/*                      u1AddressType - specifies SNOOP/MLDS                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopRedActiveSendAddVlanSync (UINT4 u4InstId, tSnoopTag VlanId,
                               UINT1 u1AddressType)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                    "Snoop Protocol Add Vlan for Instance %d \r\n", u4InstId);

    /* Allocate memory for the trigger to be send to standby through RM.
     * This memory will be freed by RM after Txmitting
     */

    u2DataLen = (UINT2) SNOOP_RED_ADD_VLAN_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Buffer creation failure for Snoop Add Vlan "
                   "Sync fails\r\n");
        return (SNOOP_FAILURE);
    }
    /* Form a trigger to flush all the previously synced messages.

     *        <--------1 Byte-----------><--1--><---2--->
     *****************************************************
     *        *                         *      *         *
     * RM Hdr * IGS_RED_ADD_VLAN_SYNC * Inst * Vlan Id   *
     *        *                         *  Id  *         *
     *****************************************************
     * The RM Hdr shall be included by RM.
     */
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_ADD_VLAN_SYNC */
        u1MsgType = IGS_RED_ADD_VLAN_SYNC;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-Vlan Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_OUTER_VLAN (VlanId));

    /* Fill the Inner-Vlan Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, SNOOP_INNER_VLAN (VlanId));

    /* Call the API provided by RM to send the data to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        == RM_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Enqueuing Snoop Add Vlan Sync fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedStandbySendBulkReq                           */
/*                                                                           */
/* Description        : This function sends a bulk request message to        */
/*                      SNOOP active node.                                   */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedStandbySendBulkReq (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Check if there exist any standby, only then 
     * sync up message needs be to send.
     */
    if (SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE)
    {
        /* Node is not in Standby mode, hence no need to send a
         * bulk request message. */
        return SNOOP_SUCCESS;
    }

    /* Allocate memory for the bulk request message */
    u2DataLen = SNOOP_RED_BULK_REQ_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Buffer creation failure for Snoop Bulk Request Message fails\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        return (SNOOP_FAILURE);
    }

    /* Form a bulk request message. 

     *        <--------1 Byte--------->
     ********************************** 
     *        *                       *
     * RM Hdr * IGS_RED_BULK_REQ_MSG  * 
     *        *                       *  
     **********************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the type of Event as IGS_RED_BULK_REQ_MSG_LEN */
    u1MsgType = IGS_RED_BULK_REQ_MSG;
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Call the API provided by RM to send the data to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        == RM_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Enqueuing Snoop Disable Sync fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRedProcessSyncUpdate                            */
/*                                                                           */
/* Description        : This function applies the Snoop dynamic/bulk update  */
/*                      message immediately in the standby node Snoop module,*/
/*                      when it is received from active node.                */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to RM message containing SNOOP dynamic*/
/*                      data.                                                */
/*                      u2Length - Length of SNOOP dynamic sync update       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedProcessSyncUpdate (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT1              *pFwdPortBmp = NULL;
    UINT1              *pV1FwdPortBmp = NULL;
    UINT1              *pRtrPortBmp = NULL;
    UINT1              *pQuerierRtrPortBmp = NULL;
    tIPvXAddr           McastGrpIpAddr;
    tIPvXAddr           McastSrcIpAddr;
    tRmProtoEvt         ProtoEvt;
    tSnoopTag           VlanId;
    INT4                i4RetVal = SNOOP_SUCCESS;
    UINT4               u4Instance = 0;
    UINT4               u4Port = 0;
    UINT4               u4Interval = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT4               u4SnoopHwId = 0;
    UINT4               u4QuerierAddress = 0;
    UINT2               u2PortListLen = SNOOP_PORT_LIST_SIZE;
    UINT2               u2PortListLenInMsg = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1Querier = 0;
    UINT1               u1McastFwdMode = 0;
    UINT1               u1AddressType = 0;
    UINT1               u1Version = 0;
    UINT1               u1RtrPortType = 0;
    UINT1               u1StartUpQryCount = 0;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    SNOOP_MEM_SET (&VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&McastGrpIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&McastSrcIpAddr, 0, sizeof (tIPvXAddr));

    while (u4Offset < u2Length)
    {
        /* Extract the Msg Type */
        SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1MsgType);

        if (u1MsgType == IGS_RED_BULK_REQ_MSG)
        {
            if (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE)
            {
                /* This is a special case, where bulk request msg from
                 * standby is coming before RM_STANDBY_UP. So no need to
                 * process the bulk request now. Bulk updates will be send
                 * on RM_STANDBY_UP event.
                 */
                SNOOP_RED_BULK_REQ_RECD () = SNOOP_TRUE;
                return SNOOP_SUCCESS;
            }

            SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;
            /* On recieving IGS_RED_BULK_REQ_MSG, we should restart the
             * bulk updation process.
             */
            if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry != NULL)
            {
                SNOOP_RED_BULK_UPD_VLAN_ENTRY () =
                    RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                    VlanEntry);
            }

            if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry != NULL)
            {
                SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () =
                    RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                    MacMcastFwdEntry);
            }
            SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () = 0;
            SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
            SNOOP_RED_BULK_UPD_VLAN_ENTRY () = NULL;
            SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () = NULL;
            SNOOP_RED_RTR_PORT_BULK_UPD_FLAG = SNOOP_FALSE;
            SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = NULL;

            SnoopRedHandleBulkUpdEvent ();

            return SNOOP_SUCCESS;
        }

        /* Rest of the sync up messages should be processed only
         * by Standby node.
         * Before processing the sync up messages, static configurations
         * must be restored.
         */
        if ((SNOOP_NODE_STATUS () != SNOOP_STANDBY_NODE) &&
            (SNOOP_RM_GET_STATIC_CONFIG_STATUS () != RM_STATIC_CONFIG_RESTORED))
        {
            return SNOOP_SUCCESS;
        }

        /* Based on the type of message in the buffer, decode the
         * message content and store it in the sync data store.
         */
        switch (u1MsgType)
        {

            case SNOOP_RED_MCAST_FORWARD_MODE:

                /* Get the Instance Id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Get the Multicast forwarding mode information */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1McastFwdMode);

                SnoopRedUpdateMcastFwdModeSync (u4Instance, u1McastFwdMode);

                break;

            case IGS_RED_DEL_VLAN_SYNC:
                /* Get the Instance Id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);

                /* Get the Outer-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Get the Inner-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                SnoopRedHandleDelVlanSync (u4Instance, VlanId, u1MsgType);
                break;

            case IGS_RED_ADD_VLAN_SYNC:
                /* Get the Instance Id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Get the Outer-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Get the Inner-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                SnoopRedHandleAddVlanSync (u4Instance, VlanId);
                break;

            case IGS_RED_RTR_PORT:

                /* Extract the len of the message content */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);

                /* Extract the instance id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Extract the Outer-VLAN ID of the router list */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Extract the Inner-VLAN ID of the router list */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                /* Extract the AddressType */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1AddressType);

                /* Extract the router port */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Port);

                /* Extract the router port version */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1Version);

                /* Extract the router port type */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1RtrPortType);

                SnoopRedUpdateRtrPortVerSync (u4Instance, VlanId, u4Port,
                                              u1AddressType, u1Version,
                                              u1RtrPortType);
                break;
            case IGS_RED_STARTUP_QRY_COUNT_SYNC:

                /* Extract the instance id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Extract the Outer-VLAN ID of the router port */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));
                /* Extract the startup query count type */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1StartUpQryCount);

                SnoopRedUpdateStartUpQryCount (u4Instance, VlanId,
                                               u1StartUpQryCount);
                break;

            case IGS_RED_RTR_PORT_INTR:

                /* Extract the len of the message content */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);

                /* Extract the instance id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Extract the Outer-VLAN ID of the router port */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Extract the Inner-VLAN ID of the router port */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                /* Extract the AddressType */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1AddressType);

                /* Extract the router port */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Port);

                /* Extract the router port interval */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Interval);

                SnoopRedUpdateRtrPortIntervalSync (u4Instance, VlanId, u4Port,
                                                   u1AddressType, u4Interval);
                break;
            case IGS_RED_RTR_PORT_LIST:

                /* Extract the len of the message content */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);

                /* Extract the instance id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Extract the Outer-VLAN ID of the router list */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Extract the Inner-VLAN ID of the router list */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                /* Compute the size of port list bitmap used in the
                 * message.
                 */
                u2PortListLenInMsg = (UINT2) ((u2MsgLen -
                                               (SNOOP_INST_ID_FIELD_SIZE +
                                                SNOOP_VLANID_FIELD_SIZE)) / 2);

                /* Extract the Router list and Querier Router port list
                 * from the message */
                pRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pRtrPortBmp == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Error in allocating memory for pRtrPortBmp\r\n");
                    return SNOOP_FAILURE;
                }
                MEMSET (pRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                pQuerierRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pQuerierRtrPortBmp == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Error in allocating memory for"
                                   "pQuerierRtrPortBmp\r\n");
                    UtilPlstReleaseLocalPortList (pRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                MEMSET (pQuerierRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                if (u2PortListLenInMsg < u2PortListLen)
                {
                    /* Received port list size is lesser than what
                     * we have */
                    /* doubt - why offset is not decremented */
                    /* Extract the Router list */
                    SNOOP_RM_GET_N_BYTE (pMsg, pRtrPortBmp, &u4Offset,
                                         u2PortListLenInMsg);

                    /* Extract the Querier Router port list */
                    SNOOP_RM_GET_N_BYTE (pMsg, pQuerierRtrPortBmp, &u4Offset,
                                         u2PortListLenInMsg);
                }
                else
                {
                    /* Received port list size is either same or
                     * greater than what we use */

                    /* Extract the Router list */
                    SNOOP_RM_GET_N_BYTE (pMsg, pRtrPortBmp, &u4Offset,
                                         u2PortListLen);

                    /* Move the offset to skip the remaining bytes in 
                     * the current message in router list field. */
                    u4Offset += (u2PortListLenInMsg - u2PortListLen);

                    /* Extract the Querier Router list */
                    SNOOP_RM_GET_N_BYTE (pMsg, pQuerierRtrPortBmp, &u4Offset,
                                         u2PortListLen);

                    /* Move the offset to skip the remaining bytes in 
                     * the current message in Querier router list field. */
                    u4Offset += (u2PortListLenInMsg - u2PortListLen);
                }

                SnoopRedUpdateRtrPortListSync (u4Instance, VlanId,
                                               pRtrPortBmp, pQuerierRtrPortBmp,
                                               u1MsgType);
                UtilPlstReleaseLocalPortList (pQuerierRtrPortBmp);
                UtilPlstReleaseLocalPortList (pRtrPortBmp);
                break;

            case IGS_RED_GRP_INFO_MSG:

                /* Extract the Multicast group information present in the
                 * message */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));
                SNOOP_RM_GET_N_BYTE (pMsg, &McastGrpIpAddr, &u4Offset,
                                     sizeof (tIPvXAddr));

                u2PortListLenInMsg = (UINT2) ((u2MsgLen -
                                               (SNOOP_INST_ID_FIELD_SIZE +
                                                SNOOP_VLANID_FIELD_SIZE +
                                                sizeof (tIPvXAddr))) / 2);
                pFwdPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pFwdPortBmp == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Error in allocating memory for pFwdPortBmp\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pFwdPortBmp, 0, sizeof (tSnoopPortBmp));

                if (u2PortListLenInMsg < u2PortListLen)
                {
                    /* Received port list size is lesser than what
                     * we have *//* doubt - why offset not incremented */
                    SNOOP_RM_GET_N_BYTE (pMsg, pFwdPortBmp, &u4Offset,
                                         u2PortListLenInMsg);
                }
                else
                {
                    /* Received port list size is either same or
                     * greater than what we use */
                    SNOOP_RM_GET_N_BYTE (pMsg, pFwdPortBmp, &u4Offset,
                                         u2PortListLen);
                    /* Move the offset to skip the remaining bytes in 
                     * the current  message. */
                    u4Offset += (u2PortListLenInMsg - u2PortListLen);
                }
                pV1FwdPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pV1FwdPortBmp == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Error in allocating memory for pV1FwdPortBmp\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBmp);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pV1FwdPortBmp, 0, sizeof (tSnoopPortBmp));

                if (u2PortListLenInMsg < u2PortListLen)
                {
                    /* Received port list size is lesser than what
                     * we have *//* doubt - why offset not incremented */
                    SNOOP_RM_GET_N_BYTE (pMsg, pV1FwdPortBmp, &u4Offset,
                                         u2PortListLenInMsg);
                }
                else
                {
                    /* Received port list size is either same or
                     * greater than what we use */
                    SNOOP_RM_GET_N_BYTE (pMsg, pV1FwdPortBmp, &u4Offset,
                                         u2PortListLen);
                    /* Move the offset to skip the remaining bytes in 
                     * the current  message. */
                    u4Offset += (u2PortListLenInMsg - u2PortListLen);
                }

                i4RetVal = SnoopRedUpdateMemPortSync (u4Instance, VlanId,
                                                      McastGrpIpAddr,
                                                      pFwdPortBmp,
                                                      pV1FwdPortBmp, u1MsgType);
                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                UtilPlstReleaseLocalPortList (pV1FwdPortBmp);

                break;

            case IGS_RED_IP_INFO_MSG:

                /* Extract the Multicast forwarding information present in the
                 * message */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Get the Outer-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4SnoopHwId);
                SNOOP_RM_GET_N_BYTE (pMsg, &McastSrcIpAddr, &u4Offset,
                                     sizeof (tIPvXAddr));

                SNOOP_RM_GET_N_BYTE (pMsg, &McastGrpIpAddr, &u4Offset,
                                     sizeof (tIPvXAddr));

                pFwdPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pFwdPortBmp == NULL)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Error in allocating memory for pFwdPortBmp\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pFwdPortBmp, 0, sizeof (tSnoopPortBmp));

                SNOOP_RM_GET_N_BYTE (pMsg, pFwdPortBmp, &u4Offset,
                                     SNOOP_PORT_LIST_SIZE);
                if (SnoopSyncIpFwdTable (u4Instance, VlanId,
                                         McastGrpIpAddr, McastSrcIpAddr,
                                         pFwdPortBmp, u4SnoopHwId)
                    != SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Unable to sync up mcast IP "
                                   "forwarding table entry\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBmp);
                    return SNOOP_FAILURE;
                }

                UtilPlstReleaseLocalPortList (pFwdPortBmp);

                break;

            case IGS_RED_QUERIER_CONFIG:

                /* Get the Instance Id */
                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4Instance);
                SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

                /* Get the Outer-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_OUTER_VLAN (VlanId));

                /* Get the Inner-Vlan Identifier */
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset,
                                     SNOOP_INNER_VLAN (VlanId));

                /* Get the Changed Querier information */
                SNOOP_RM_GET_1_BYTE (pMsg, &u4Offset, u1Querier);

                SNOOP_RM_GET_4_BYTE (pMsg, &u4Offset, u4QuerierAddress);

                i4RetVal = SnoopRedUpdateQuerierSync (u4Instance, VlanId,
                                                      u1Querier, u1MsgType,
                                                      u4QuerierAddress);

                break;

            case IGS_RED_BULK_UPD_TAIL_MSG:

                ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
                ProtoEvt.u4Error = RM_NONE;
                SnoopRmHandleProtocolEvent (&ProtoEvt);
                SNOOP_RED_BULK_SYNC_INPROGRESS = SNOOP_FALSE;
                break;

            case SNOOP_RED_SYNC_MCAST_CACHE_INFO:
                SNOOP_RM_GET_2_BYTE (pMsg, &u4Offset, u2MsgLen);
                SnoopRedProcessMcastCacheInfo (pMsg, u4Instance, &u4Offset);
                break;

            default:
                break;
        }
        if (i4RetVal == SNOOP_FAILURE)
        {
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateRtrPortVerSync                         */
/*                                                                           */
/* Description        : This function stores the router port information     */
/*                      present in the sync message received from SNOOP      */
/*                      protocol in active node.                             */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      AddrType     - Address Type                          */
/*                      u1Version    - Version                               */
/*                      u1RtrPorType - QUERIER / NON-QUERIER RTR PORT        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateRtrPortVerSync (UINT4 u4Instance, tSnoopTag VlanId,
                              UINT4 u4Port, UINT1 u1AddressType,
                              UINT1 u1Version, UINT1 u1RtrPortType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tMacAddr            NullMacAddr;
    UINT1               u1FoundFlag = SNOOP_FALSE;

    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying Router port sync up message \r\n");

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Snooping module disabled.Discarding the sync "
                       "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                      u1AddressType, &pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_RED_TRC, "Unable to create VLAN "
                            "entry for VLAN id %d\n",
                            SNOOP_OUTER_VLAN (VlanId));
            return SNOOP_FAILURE;
        }
    }

    SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                    pRtrPortNode, tSnoopRtrPortEntry *)
    {
        if (pRtrPortNode->u4Port == u4Port)
        {
            u1FoundFlag = SNOOP_TRUE;
            break;
        }
    }

    if ((u1FoundFlag == SNOOP_TRUE) && (pRtrPortNode->u1OperVer < u1Version))
    {
        if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION1)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V1RtrPortBitmap);

        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION2)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V2RtrPortBitmap);
        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION3)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V3RtrPortBitmap);
        }

        /* update the current operating version 
         * of the router port with the new version */
        pRtrPortNode->u1OperVer = u1Version;

        if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION2)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V2RtrPortBitmap);
        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION3)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V3RtrPortBitmap);
        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION1)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V1RtrPortBitmap);
        }

        return SNOOP_SUCCESS;
    }
    /*Check whether it is dynamic mrouter port */
    if ((pRtrPortNode != NULL) && (pRtrPortNode->u1IsDynamic == OSIX_TRUE))
    {
        if (SnoopVlanUpdateLearntRtrPort (u4Instance, pSnoopVlanEntry,
                                          u4Port, u1RtrPortType, u1Version,
                                          SNOOP_ZERO) != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Router port updation failed\r\n");
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateRtrPortIntervalSync                    */
/*                                                                           */
/* Description        : This function stores the router port information     */
/*                      present in the sync message received from SNOOP      */
/*                      protocol in active node.                             */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      AddrType     - Address Type                          */
/*                      u4Interval   - V3 Rtr port purge interval            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateRtrPortIntervalSync (UINT4 u4Instance, tSnoopTag VlanId,
                                   UINT4 u4Port, UINT1 u1AddressType,
                                   UINT4 u4Interval)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;

    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying Router port sync up message \r\n");

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                      u1AddressType, &pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                "Unable to create VLAN "
                                "entry for VLAN id %d\n",
                                SNOOP_OUTER_VLAN (VlanId));
            return SNOOP_FAILURE;
        }
    }

    SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                    pRtrPortNode, tSnoopRtrPortEntry *)
    {
        if (pRtrPortNode->u4Port == u4Port)
        {
            /* update the current operating version 
             * of the router port with the new version */
            pRtrPortNode->u4V3RtrPortPurgeInt = u4Interval;
            break;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateRtrPortListSync                        */
/*                                                                           */
/* Description        : This function stores the router port information     */
/*                      present in the sync message received from SNOOP      */
/*                      protocol in active node.                             */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance Id                            */
/*                      VlanId      - Vlan Identifier                        */
/*                      RtrPortList - Learnt Router Port list                */
/*                      QuerierRtrPortBmp - Querier Learnt Router Port list  */
/*                      u1MsgType - specifies the Msg recvd from IGS/MLDS    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateRtrPortListSync (UINT4 u4Instance, tSnoopTag VlanId,
                               tSnoopPortBmp RtrPortList,
                               tSnoopPortBmp QuerierRtrPortList,
                               UINT1 u1MsgType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pAddRtrPortList = NULL;
    UINT1              *pDelRtrPortList = NULL;
    tMacAddr            NullMacAddr;
    UINT4               u4Index = 0;
    UINT1               u1AddressType = 0;
    UINT1               u1Version = 0;
    BOOL1               bAdded = OSIX_FALSE;
    BOOL1               bDeleted = OSIX_FALSE;
    BOOL1               bQuerierRtrPort = OSIX_FALSE;
    BOOL1               bIsStaticRtrPort = OSIX_FALSE;

    UNUSED_PARAM (u1MsgType);
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying Router port sync up message \r\n");

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                      u1AddressType, &pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                "Unable to create VLAN "
                                "entry for VLAN id %d\n",
                                SNOOP_OUTER_VLAN (VlanId));
            return SNOOP_FAILURE;
        }
    }
    pAddRtrPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pAddRtrPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pAddRtrPortList\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pAddRtrPortList, 0, sizeof (tSnoopPortBmp));

    pDelRtrPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDelRtrPortList == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pDelRtrPortList \r\n");
        UtilPlstReleaseLocalPortList (pAddRtrPortList);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pDelRtrPortList, 0, sizeof (tSnoopPortBmp));

    /* Extract the newly added & deleted router ports */
    for (u4Index = 0; u4Index < SNOOP_PORT_LIST_SIZE; u4Index++)
    {
        pAddRtrPortList[u4Index]
            = (UINT1) (~(pSnoopVlanEntry->DynRtrPortBitmap[u4Index])
                       & (RtrPortList[u4Index]));

        pDelRtrPortList[u4Index]
            = (UINT1) (~(RtrPortList[u4Index]) &
                       (pSnoopVlanEntry->DynRtrPortBitmap[u4Index]));
    }

    /* Apply the Router Port */
    for (u4Index = 1; u4Index <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Index++)
    {
        SNOOP_IS_PORT_PRESENT (u4Index, pAddRtrPortList, bAdded);
        SNOOP_IS_PORT_PRESENT (u4Index, pDelRtrPortList, bDeleted);

        if (bAdded == OSIX_TRUE)
        {
            SNOOP_IS_PORT_PRESENT (u4Index, QuerierRtrPortList,
                                   bQuerierRtrPort);

            if (bQuerierRtrPort == OSIX_TRUE)
            {
                u1Version = (pSnoopVlanEntry->u1AddressType ==
                             SNOOP_ADDR_TYPE_IPV4) ?
                    SNOOP_IGS_IGMP_VERSION3 : SNOOP_MLD_VERSION2;

                /* Port learnt through IGMP query */
                if (SnoopVlanUpdateLearntRtrPort (u4Instance,
                                                  pSnoopVlanEntry,
                                                  u4Index,
                                                  SNOOP_IGMP_QUERY,
                                                  u1Version,
                                                  SNOOP_ZERO) != SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Router port updation \
                               failed\r\n");
                    UtilPlstReleaseLocalPortList (pAddRtrPortList);
                    UtilPlstReleaseLocalPortList (pDelRtrPortList);
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                u1Version = (pSnoopVlanEntry->u1AddressType ==
                             SNOOP_ADDR_TYPE_IPV4) ? pSnoopVlanEntry->
                    pSnoopVlanCfgEntry->u1ConfigOperVer : SNOOP_MLD_VERSION2;

                /* Port learnt through router control message */
                if (SnoopVlanUpdateLearntRtrPort (u4Instance,
                                                  pSnoopVlanEntry,
                                                  u4Index,
                                                  SNOOP_ROUTER_MSG,
                                                  u1Version,
                                                  SNOOP_ZERO) != SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Router port updation \
                               failed\r\n");
                    UtilPlstReleaseLocalPortList (pAddRtrPortList);
                    UtilPlstReleaseLocalPortList (pDelRtrPortList);
                    return SNOOP_FAILURE;
                }
            }
        }

        if (bDeleted == OSIX_TRUE)
        {
            SNOOP_IS_PORT_PRESENT (u4Index,
                                   pSnoopVlanEntry->StaticRtrPortBitmap,
                                   bIsStaticRtrPort);
            if (bIsStaticRtrPort == SNOOP_FALSE)
            {
                /* Remove the port from the router port information */
                if (SnoopVlanDeleteRtrPortEntry (u4Instance, pSnoopVlanEntry,
                                                 u4Index, NullMacAddr,
                                                 SNOOP_DEL_PORT) !=
                    SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Delete router port entry " "failed\r\n");
                    UtilPlstReleaseLocalPortList (pAddRtrPortList);
                    UtilPlstReleaseLocalPortList (pDelRtrPortList);
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                if (SnoopVlanDeleteRtrPortEntry (u4Instance, pSnoopVlanEntry,
                                                 u4Index, NullMacAddr,
                                                 SNOOP_DISABLE_PORT) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                   "Delete router port entry " "failed\r\n");
                    UtilPlstReleaseLocalPortList (pAddRtrPortList);
                    UtilPlstReleaseLocalPortList (pDelRtrPortList);
                    return SNOOP_FAILURE;

                }
            }
        }
    }

    UtilPlstReleaseLocalPortList (pAddRtrPortList);
    UtilPlstReleaseLocalPortList (pDelRtrPortList);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopSyncIpFwdTable                                  */
/*                                                                           */
/* Description        : This function updates the Multicast forwarding       */
/*                      for the multicast fwd table received from SNOOP      */
/*                      protocol                                             */
/*                      in active node.                                      */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Addr             */
/*                      FwdPortList  - Forward Port List                     */
/*                      V1FwdPortList  - V1 Forward Port List                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopSyncIpFwdTable (UINT4 u4Instance, tSnoopTag VlanId,
                     tIPvXAddr SnoopGrpIpAddr,
                     tIPvXAddr SnoopSrcIpAddr,
                     tSnoopPortBmp FwdPortList, UINT4 u4SnoopHwId)
{
    UINT1               u1AddressType = 0;

    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying group membership sync up message \r\n");

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    /* Check if group entry is present or not */
    if (SnoopRedSyncFwdListOnNewGrp (u4Instance, VlanId,
                                     SnoopGrpIpAddr, SnoopSrcIpAddr,
                                     FwdPortList, u4SnoopHwId) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Applying Sync up for forwarding portlist for "
                       "new group entry failed\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateMemPortSync                            */
/*                                                                           */
/* Description        : This function updates the Multicast membership       */
/*                      for the multicast group received from SNOOP protocol */
/*                      in active node.                                      */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Addr             */
/*                      FwdPortList  - Forward Port List                     */
/*                      V1FwdPortList  - V1 Forward Port List                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateMemPortSync (UINT4 u4Instance, tSnoopTag VlanId,
                           tIPvXAddr SnoopGrpIpAddr,
                           tSnoopPortBmp FwdPortList,
                           tSnoopPortBmp V1FwdPortList, UINT1 u1MsgType)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT1               u1AddressType = 0;

    UNUSED_PARAM (u1MsgType);
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying group membership sync up message \r\n");

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    /* Check if group entry is present or not */
    if (SnoopGrpGetGroupEntry (u4Instance, VlanId, SnoopGrpIpAddr,
                               &pSnoopGroupEntry) == SNOOP_SUCCESS)
    {
        if (SnoopRedApplyFwdListOnExistingGrp (u4Instance, pSnoopGroupEntry,
                                               FwdPortList, V1FwdPortList)
            != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Applying forwarding portlist for group in "
                           "already existing group entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        if (SnoopRedApplyFwdListOnNewGrp (u4Instance, VlanId,
                                          SnoopGrpIpAddr,
                                          FwdPortList, V1FwdPortList)
            != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Applying forwarding portlist for group in "
                           "new group entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedApplyFwdListOnExistingGrp                    */
/*                                                                           */
/* Description        : This function applies the forwarding port list sync  */
/*                      on existing group entry in standby node.             */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      pSnoopGroupEntry  - Group Entry                      */
/*                      FwdPortList  - Forward Port List                     */
/*                      V1FwdPortList  - V1 Forward Port List                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedApplyFwdListOnExistingGrp (UINT4 u4Instance,
                                   tSnoopGroupEntry * pSnoopGroupEntry,
                                   tSnoopPortBmp FwdPortList,
                                   tSnoopPortBmp V1FwdPortList)
{
    UINT1              *pAddFwdList = NULL;
    UINT1              *pDelFwdList = NULL;
    UINT1              *pAddV1FwdList = NULL;
    UINT1              *pDelV1FwdList = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tSnoopPktInfo       SnoopPktInfo;
    UINT4               u4Index = 0;
    UINT1               u1PortFound = SNOOP_FALSE;
    BOOL1               bAdded = OSIX_FALSE;
    BOOL1               bDeleted = OSIX_FALSE;
    BOOL1               bV1Added = OSIX_FALSE;
    BOOL1               bV1Deleted = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    pAddFwdList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pAddFwdList == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pAddFwdList\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pAddFwdList, 0, sizeof (tSnoopPortBmp));

    pDelFwdList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDelFwdList == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pDelFwdList\r\n");
        UtilPlstReleaseLocalPortList (pAddFwdList);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pDelFwdList, 0, sizeof (tSnoopPortBmp));

    pAddV1FwdList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pAddV1FwdList == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pAddV1FwdList\r\n");
        UtilPlstReleaseLocalPortList (pAddFwdList);
        UtilPlstReleaseLocalPortList (pDelFwdList);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pAddV1FwdList, 0, sizeof (tSnoopPortBmp));

    pDelV1FwdList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDelV1FwdList == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Error in allocating memory for pDelV1FwdList\r\n");
        UtilPlstReleaseLocalPortList (pAddFwdList);
        UtilPlstReleaseLocalPortList (pDelFwdList);
        UtilPlstReleaseLocalPortList (pAddV1FwdList);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pDelV1FwdList, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));

    /* Extract the newly added & deleted ports in the fwding
     * port bitmap*/
    for (u4Index = 0; u4Index < SNOOP_PORT_LIST_SIZE; u4Index++)
    {
        pAddFwdList[u4Index]
            = (UINT1) (~(pSnoopGroupEntry->PortBitmap[u4Index])
                       & (FwdPortList[u4Index]));

        pDelFwdList[u4Index]
            = (UINT1) (~(FwdPortList[u4Index])
                       & (pSnoopGroupEntry->PortBitmap[u4Index]));

        pAddV1FwdList[u4Index]
            = (UINT1) (~(pSnoopGroupEntry->V1PortBitmap[u4Index])
                       & (V1FwdPortList[u4Index]));

        pDelV1FwdList[u4Index]
            = (UINT1) (~(V1FwdPortList[u4Index])
                       & (pSnoopGroupEntry->V1PortBitmap[u4Index]));
    }

    for (u4Index = 1; u4Index <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Index++)
    {
        SNOOP_IS_PORT_PRESENT (u4Index, pAddFwdList, bAdded);
        SNOOP_IS_PORT_PRESENT (u4Index, pDelFwdList, bDeleted);
        SNOOP_IS_PORT_PRESENT (u4Index, pAddV1FwdList, bV1Added);
        SNOOP_IS_PORT_PRESENT (u4Index, pDelV1FwdList, bV1Deleted);

        if (bAdded == OSIX_TRUE)
        {
            SnoopPktInfo.u4InPort = u4Index;
            SnoopPktInfo.u1PktType = SNOOP_IGMP_V2REPORT;
            SNOOP_MEM_CPY (&SnoopPktInfo.GroupAddr,
                           &pSnoopGroupEntry->GroupIpAddr, sizeof (tIPvXAddr));

            /* Port Entry not present so create a new port record */
            if (SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                pSnoopGroupEntry,
                                                &SnoopPktInfo, NULL,
                                                SNOOP_FALSE) != SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Addition of port entry to Group failed\r\n");
                UtilPlstReleaseLocalPortList (pAddFwdList);
                UtilPlstReleaseLocalPortList (pDelFwdList);
                UtilPlstReleaseLocalPortList (pAddV1FwdList);
                UtilPlstReleaseLocalPortList (pDelV1FwdList);
                return SNOOP_FAILURE;
            }

            /* Update the forwarding Table with the new port entry */
            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           u4Index, SNOOP_ADD_PORT, 0)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Unable to add the port in mcast MAC "
                               "forwarding entry\r\n");
                UtilPlstReleaseLocalPortList (pAddFwdList);
                UtilPlstReleaseLocalPortList (pDelFwdList);
                UtilPlstReleaseLocalPortList (pAddV1FwdList);
                UtilPlstReleaseLocalPortList (pDelV1FwdList);
                return SNOOP_FAILURE;
            }
        }

        if (bDeleted == OSIX_TRUE)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                            pSnoopASMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopASMPortEntry->u4Port == u4Index)
                {
                    u1PortFound = SNOOP_TRUE;
                    break;
                }
            }

            if (u1PortFound == SNOOP_TRUE)
            {
                /* Update the Group entry V2 port bitmap */
                SNOOP_DEL_FROM_PORT_LIST (u4Index,
                                          pSnoopGroupEntry->ASMPortBitmap);

                /*Release the port node from the V1V2PortList */
                SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                                  pSnoopASMPortEntry);

                /* Delete all the hosts attached to the port */
                if ((pSnoopASMPortEntry != NULL) &&
                    (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0))
                {
                    while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                            SNOOP_SLL_FIRST (&pSnoopASMPortEntry->
                                             HostList)) != NULL)
                    {
                        pRBElem = (tRBElem *) pSnoopASMHostEntry;
                        /* Remove the host from the Host Information RBTree */
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      HostEntry, pRBElem);
                        SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                          pSnoopASMHostEntry);

                        SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                          pSnoopASMHostEntry);
                    }
                }

                if (pSnoopASMPortEntry != NULL)
                {
                    SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);
                    pSnoopASMPortEntry = NULL;
                }
            }

            /* Update the Group entry port bitmap */
            SNOOP_DEL_FROM_PORT_LIST (u4Index, pSnoopGroupEntry->PortBitmap);

            /* Remove the port from forwarding Table */
            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           u4Index, SNOOP_DEL_PORT, 0)
                != SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Unable to add the port in mcast MAC "
                               "forwarding entry\r\n");
                UtilPlstReleaseLocalPortList (pAddFwdList);
                UtilPlstReleaseLocalPortList (pDelFwdList);
                UtilPlstReleaseLocalPortList (pAddV1FwdList);
                UtilPlstReleaseLocalPortList (pDelV1FwdList);
                return SNOOP_FAILURE;
            }
        }

        if ((bV1Added == OSIX_TRUE) || (bV1Deleted == OSIX_TRUE))
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                            pSnoopASMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopASMPortEntry->u4Port == u4Index)
                {
                    if (bV1Added == OSIX_TRUE)
                    {
                        pSnoopASMPortEntry->u1V1HostPresent = SNOOP_TRUE;
                        SNOOP_ADD_TO_PORT_LIST (u4Index,
                                                pSnoopGroupEntry->V1PortBitmap);
                    }
                    else
                    {
                        pSnoopASMPortEntry->u1V1HostPresent = SNOOP_FALSE;
                        SNOOP_DEL_FROM_PORT_LIST (u4Index,
                                                  pSnoopGroupEntry->
                                                  V1PortBitmap);
                    }

                    break;
                }
            }
        }
    }
    UtilPlstReleaseLocalPortList (pAddFwdList);
    UtilPlstReleaseLocalPortList (pDelFwdList);
    UtilPlstReleaseLocalPortList (pAddV1FwdList);
    UtilPlstReleaseLocalPortList (pDelV1FwdList);

    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                       SNOOP_PORT_LIST_SIZE) == 0)
    {
        /* Delete the group node and free memblock */
        if (SnoopGrpDeleteGroupEntry (u4Instance,
                                      pSnoopGroupEntry->VlanId,
                                      pSnoopGroupEntry->GroupIpAddr)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Deletion of Group record failed\r\n");

            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedSyncFwdListOnNewGrp                          */
/*                                                                           */
/* Description        : This function applies the forwarding port list sync  */
/*                      on existing group entry in standby node.             */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Addr             */
/*                      FwdPortList  - Forward Port List                     */
/*                                                                           */
/* Output(s)          : Group Entry                                          */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedSyncFwdListOnNewGrp (UINT4 u4Instance, tSnoopTag VlanId,
                             tIPvXAddr SnoopGrpIpAddr,
                             tIPvXAddr SnoopSrcIpAddr,
                             tSnoopPortBmp FwdPortList, UINT4 u4SnoopHwId)
{
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBlem = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopRedMcastCacheNode SnoopRedCacheNode;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    UINT4               u4Status = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4SrcAddr = 0;
    UINT1               u1EntryFound = SNOOP_FALSE;

    SNOOP_MEM_SET (&SnoopRedCacheNode, 0, sizeof (tSnoopRedMcastCacheNode));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));
    IpGrpFwdEntry.u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SnoopSrcIpAddr, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                    (UINT1 *) &SnoopGrpIpAddr);

    pRBlem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->
                        IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry);
    if (pRBlem == NULL)
    {
        u1EntryFound = SNOOP_FALSE;
    }
    else
    {
        pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBlem;
        u1EntryFound = SNOOP_TRUE;
    }

    u4GrpAddr = SNOOP_PTR_FETCH_4 (SnoopGrpIpAddr.au1Addr);
    u4SrcAddr = SNOOP_PTR_FETCH_4 (SnoopSrcIpAddr.au1Addr);

    SnoopRedCacheNode.u4ContextId = u4Instance;
    SnoopRedCacheNode.VlanId = SNOOP_OUTER_VLAN (VlanId);
    SnoopRedCacheNode.u4GrpAddr = u4GrpAddr;
    SnoopRedCacheNode.u4SrcAddr = u4SrcAddr;

    SNOOP_MEM_CPY (&(SnoopRedCacheNode.PortList), FwdPortList,
                   SNOOP_PORT_LIST_SIZE);

    SNOOP_MEM_CPY (&(SnoopRedCacheNode.UnTagPortList), FwdPortList,
                   SNOOP_PORT_LIST_SIZE);

    SnoopRedCacheNode.i1RedSyncState = SNOOP_RED_SYNC_SW_UPDATED;

    if (u1EntryFound == SNOOP_TRUE)
    {
        if (SNOOP_MEM_CMP (FwdPortList, gNullPortBitMap, SNOOP_PORT_LIST_SIZE)
            == 0)
        {
            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                          (tRBElem *) pSnoopIpGrpFwdEntry);
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if (pSnoopInstInfo != NULL)
            {
                pSnoopInfo = &(pSnoopInstInfo->SnoopInfo[SNOOP_ADDR_TYPE_IPV4]);
                if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
                {
                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                }
            }
            SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
        }
        else if (SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                FwdPortList, SNOOP_PORT_LIST_SIZE) != 0)
        {
            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);

            SNOOP_UPDATE_PORT_LIST (FwdPortList,
                                    pSnoopIpGrpFwdEntry->PortBitmap);
        }
        if (SNOOP_RED_BULK_SYNC_INPROGRESS == SNOOP_FALSE)
        {
            SnoopRedProcessMcastSyncInfo (&SnoopRedCacheNode);
        }
        return SNOOP_SUCCESS;
    }

    if (SNOOP_MEM_CMP (FwdPortList, gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        if (SNOOP_RED_BULK_SYNC_INPROGRESS == SNOOP_FALSE)
        {
            SnoopRedProcessMcastSyncInfo (&SnoopRedCacheNode);
        }
        return SNOOP_SUCCESS;
    }

    /* IP forward entry not found, so create a new one */
    if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry)
        == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Memory allocation for IP forward entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateIpFwdTable\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    /* Update the fields of the IP entry */
    pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_SUCCESS;
    pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;
    SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId, VlanId, sizeof (tSnoopTag));

    pSnoopIpGrpFwdEntry->u1AddressType = SNOOP_ADDR_TYPE_IPV4;
    pSnoopIpGrpFwdEntry->u1EntryTypeFlag = SNOOP_GRP_DYNAMIC;
    pSnoopIpGrpFwdEntry->u4SnoopHwId = u4SnoopHwId;
    SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr), &SnoopSrcIpAddr,
                   sizeof (tIPvXAddr));

    SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr), &SnoopGrpIpAddr,
                   sizeof (tIPvXAddr));

    SNOOP_UPDATE_PORT_LIST (FwdPortList, pSnoopIpGrpFwdEntry->PortBitmap);

    u4Status =
        RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                   (tRBElem *) pSnoopIpGrpFwdEntry);

    if (u4Status == RB_FAILURE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "RBTree Addition Failed for IP forward Entry \r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateIpFwdTable\r\n");
        SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
        return SNOOP_FAILURE;
    }

    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
    if (pSnoopInstInfo != NULL)
    {
        pSnoopInfo =
            &(pSnoopInstInfo->SnoopInfo[IpGrpFwdEntry.u1AddressType - 1]);
        if (pSnoopInfo != NULL)
        {
            pSnoopInfo->u4FwdGroupsCnt += 1;
        }
    }

    if (SNOOP_RED_BULK_SYNC_INPROGRESS == SNOOP_FALSE)
    {
        SnoopRedProcessMcastSyncInfo (&SnoopRedCacheNode);
    }
    UNUSED_PARAM (u4SrcAddr);
    UNUSED_PARAM (u4GrpAddr);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedApplyFwdListOnNewGrp                         */
/*                                                                           */
/* Description        : This function applies the forwarding port list sync  */
/*                      on existing group entry in standby node.             */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      SnoopGrpIpAddr - Multicast Group IP Addr             */
/*                      FwdPortList  - Forward Port List                     */
/*                      V1FwdPortList  - V1 Forward Port List                */
/*                                                                           */
/* Output(s)          : Group Entry                                          */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedApplyFwdListOnNewGrp (UINT4 u4Instance, tSnoopTag VlanId,
                              tIPvXAddr SnoopGrpIpAddr,
                              tSnoopPortBmp FwdPortList,
                              tSnoopPortBmp V1FwdPortList)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopPktInfo       SnoopPktInfo;
    UINT4               u4Index = 0;
    BOOL1               bGrpCreated = OSIX_FALSE;
    BOOL1               bAddFwdList = OSIX_FALSE;
    BOOL1               bAddV1FwdList = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               SnoopGrpIpAddr.u1Afi,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                      SnoopGrpIpAddr.u1Afi,
                                      &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                "Unable to create VLAN entry for "
                                "VLAN id %d \n", SNOOP_OUTER_VLAN (VlanId));
            return SNOOP_FAILURE;
        }
    }

    for (u4Index = 1; u4Index <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Index++)
    {
        SNOOP_IS_PORT_PRESENT (u4Index, FwdPortList, bAddFwdList);
        SNOOP_IS_PORT_PRESENT (u4Index, V1FwdPortList, bAddV1FwdList);

        SnoopPktInfo.u4InPort = u4Index;
        SnoopPktInfo.u1PktType = SNOOP_IGMP_V2REPORT;
        SNOOP_MEM_CPY (&SnoopPktInfo.GroupAddr, &SnoopGrpIpAddr,
                       sizeof (tIPvXAddr));

        if ((bAddFwdList == OSIX_TRUE) && (bGrpCreated == OSIX_FALSE))
        {
            if (SnoopGrpCreateGroupEntry (u4Instance, VlanId,
                                          SnoopGrpIpAddr, &SnoopPktInfo,
                                          NULL, 0, 0, &pSnoopGroupEntry)
                != SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "New Group entry creation failed\r\n");
                return SNOOP_FAILURE;
            }

            /* Update the forwarding Table with the newly created
             * group entry */
            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           u4Index, SNOOP_CREATE_FWD_ENTRY,
                                           0) != SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                               "Unable to create multicast MAC "
                               "forwarding entry\r\n");
                return SNOOP_FAILURE;
            }

            bGrpCreated = OSIX_TRUE;
            /* Add the router port bitmap if present to the
             * newly created multicast forwarding entry */
            if (SNOOP_MEM_CMP (pSnoopVlanEntry->RtrPortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
            {
                if (SnoopFwdUpdateFwdBitmap (u4Instance,
                                             pSnoopVlanEntry->RtrPortBitmap,
                                             pSnoopVlanEntry,
                                             pSnoopGroupEntry->GroupIpAddr,
                                             SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                        "Unable to add the vlan %d router ports"
                                        " in forwarding port bitmap \n",
                                        SNOOP_OUTER_VLAN (VlanId));
                    return SNOOP_FAILURE;
                }
            }
        }
        else if ((bAddFwdList == OSIX_TRUE))
        {
            if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                                       SnoopGrpIpAddr,
                                       &pSnoopGroupEntry) != SNOOP_SUCCESS)
            {
                /* Port Entry not present so create a new port record */
                if (SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                    pSnoopGroupEntry,
                                                    &SnoopPktInfo, NULL,
                                                    SNOOP_FALSE) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_RED_TRC,
                               "Addition of port entry to Group failed\r\n");
                    return SNOOP_FAILURE;
                }

                /* Update the forwarding Table with the new port entry */
                if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                               u4Index, SNOOP_ADD_PORT, 0)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_RED_TRC,
                               "Unable to add the port in mcast MAC "
                               "forwarding entry\r\n");
                    return SNOOP_FAILURE;
                }
            }
        }

        if (bAddV1FwdList == OSIX_TRUE && pSnoopGroupEntry != NULL)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                            pSnoopASMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopASMPortEntry->u4Port == u4Index)
                {
                    pSnoopASMPortEntry->u1V1HostPresent = SNOOP_TRUE;
                    SNOOP_ADD_TO_PORT_LIST (u4Index,
                                            pSnoopGroupEntry->V1PortBitmap);
                    break;
                }
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateQuerierSync                            */
/*                                                                           */
/* Description        : This function stores changed querier information for */
/*                      the VLAN, present in the sync message.               */
/*                                                                           */
/* Input(s)           : u4Instance - Instance ID                             */
/*                      Vlan Id    - Vlan Identifier                         */
/*                      u1Querier  - Querier Configuration                   */
/*                      u1MsgType - specifies the Msg recvd from IGS/MLDS    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateQuerierSync (UINT4 u4Instance, tSnoopTag VlanId,
                           UINT1 u1Querier, UINT1 u1MsgType,
                           UINT4 u4QuerierAddress)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1AddressType = 0;

    UNUSED_PARAM (u1MsgType);
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
               "Applying oper Querier change sync up message \r\n");

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                      u1AddressType, &pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                                "Unable to create VLAN "
                                "entry for VLAN id %d\n",
                                SNOOP_OUTER_VLAN (VlanId));
            return SNOOP_FAILURE;
        }
    }

    pSnoopVlanEntry->u1Querier = u1Querier;
    pSnoopVlanEntry->u4ElectedQuerier = u4QuerierAddress;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateMcastFwdModeSync                       */
/*                                                                           */
/* Description        : This function stores synced Mcast forward mode for   */
/*                      particular SNOOP Protocol.                           */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance ID                         */
/*                      u1McastFwdMode - Multicast Fowarding Mode            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedUpdateMcastFwdModeSync (UINT4 u4Instance, UINT1 u1McastFwdMode)
{
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (u1McastFwdMode);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedHandleDelVlanSync                            */
/*                                                                           */
/* Description        : This function handles the sync data for the          */
/*                      particular vlan.                                     */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance ID                         */
/*                      Vlan Id        - Vlan Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedHandleDelVlanSync (UINT4 u4Instance, tSnoopTag VlanId, UINT1 u1MsgType)
{
    tIPvXAddr           GrpAddr;
    UINT1               u1AddressType = 0;

    UNUSED_PARAM (u1MsgType);
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    /* Delete all the Group related information */
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SnoopGrpDelGroupOnOuterVlan (u4Instance, VlanId);

    /* Delete all the MCAST forwarding table inforamtion */
    SnoopFwdDeleteVlanMcastFwdEntries (u4Instance, VlanId, 0);

    /* Delete all the VLAN related inforamtion */
#ifdef IGS_WANTED
    if (SnoopVlanDeleteVlanEntry (u4Instance, VlanId,
                                  SNOOP_ADDR_TYPE_IPV4) != SNOOP_SUCCESS)
    {

        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                            SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_VLAN,
                            SNOOP_VLAN_NAME,
                            "Deleting Vlan Entry for IGS %d" " failed \r\n",
                            SNOOP_OUTER_VLAN (VlanId));
        return SNOOP_FAILURE;
    }
#endif

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedHandleAddVlanSync                            */
/*                                                                           */
/* Description        : This function handles the sync data for the          */
/*                      particular vlan.                                     */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance ID                         */
/*                      Vlan Id        - Vlan Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopRedHandleAddVlanSync (UINT4 u4Instance, tSnoopTag VlanId)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1AddressType = 0;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Snooping module disabled.Discarding the sync "
                   "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               u1AddressType,
                               &pSnoopVlanEntry) == SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                   "Vlan Entry already exists\r\n");
        return SNOOP_SUCCESS;
    }

#ifdef IGS_WANTED
    if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                  u1AddressType,
                                  &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                        SNOOP_VLAN_TRC, "Unable to create VLAN entry for "
                        " VLAN id %d\n", SNOOP_OUTER_VLAN (VlanId));
        return SNOOP_FAILURE;
    }
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedAudit                                        */
/*                                                                           */
/* Description        : This function initiates the audit of multicast       */
/*                      forwarding table present in the software with the    */
/*                      multicast table present in the hardware, based on    */
/*                      Snoop forwarding mode.                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedAudit (VOID)
{
    /* Start Audit for both MAC & IP based snooping. 
     * For each on the virtual context, find the mode and do Audit.
     * This is done inside these API's*/

    /*Snoop Audit for MAC Mode */
    SnoopVlanRedStartMcastAudit ();

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : SnoopRedMcastAuditSyncMsg                        */
/*                                                                          */
/*    Description        : This function is invoked by VLAN thread while    */
/*                         performing audit of MAC Multicast table.         */
/*                         It is invoked whenever inconsistencies found     */
/*                         between hardware and software MAC Multicast      */
/*                         Entry.  The type inconsistency is passed as      */
/*                         an argument to the function which determines the */
/*                         action to be taken on SNOOP Software.            */
/*                                                                          */
/*    Input(s)           : u4Instance   - Virtual ContextId                 */
/*                         u1Type       - Type of inconsistency occured     */
/*                                        during Mac Multicast Audit .      */
/*                         VlanId       - Vlan Id.                          */
/*                         MacGroupAddr - MAC Mcast Group                   */
/*                         AuditPortBmp - Audited Port List                 */
/*                                                                          */
/*    Output(s)          : None                                             */
/*                                                                          */
/*    Returns            : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/****************************************************************************/
INT4
SnoopRedMcastAuditSyncMsg (UINT4 u4Instance, UINT1 u1Type, tVlanId VlanId,
                           tMacAddr MacGroupAddr, tPortList AuditPortBmp)
{
    tSnoopMsgNode      *pNode = NULL;
    UINT1              *pNullPortBitMap = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    pNullPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pNullPortBitMap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pNullPortBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while Audit Sync Msg\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pNullPortBitMap, 0, sizeof (tSnoopPortBmp));

    switch (u1Type)
    {
        case SNOOP_RED_MCAST_ENTRY_ONLY_IN_HW:
        case SNOOP_RED_MCAST_PORTS_ONLY_IN_HW:
        case SNOOP_RED_MCAST_PORTS_ONLY_IN_SW:
            if (SNOOP_MEM_CMP (AuditPortBmp, pNullPortBitMap,
                               SNOOP_PORT_LIST_SIZE) == 0)
            {
                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                return SNOOP_SUCCESS;
            }
            break;

        case SNOOP_RED_MCAST_ENTRY_ONLY_IN_SW:
            break;

        default:
            break;
    }
    UtilPlstReleaseLocalPortList (pNullPortBitMap);

    if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Memory allocation failed for queue message\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while Audit Sync Msg\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

    pNode->u2MsgType = SNOOP_RED_AUDIT_SYNC_MSG;

    pNode->u4Instance = u4Instance;
    pNode->uInMsg.AuditSyncMsg.u4Events = u1Type;
    SNOOP_OUTER_VLAN (pNode->uInMsg.AuditSyncMsg.VlanId) = VlanId;
    SNOOP_MEM_CPY (pNode->uInMsg.AuditSyncMsg.McastAddr,
                   MacGroupAddr, SNOOP_MAC_ADDR_LEN);
    SNOOP_MEM_CPY (pNode->uInMsg.AuditSyncMsg.AuditPortBmp,
                   AuditPortBmp, SNOOP_PORT_LIST_SIZE);

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                             SNOOP_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
    }
    else
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_RED_TRC,
                   "Posting message to SNOOP message queue failed\r\n");

        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                           "Queue message release failure \n");
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name      : SnoopRedHandleMcastAuditSyncMsg                     */
/*                                                                          */
/* Description        : This is function handles the received Mcast Audit   */
/*                      events.                                             */
/*                                                                          */
/* Input(s)           : pMsgNode - McastAuditSyncMsg Node                   */
/*                                                                          */
/* Output(s)          : None.                                               */
/*                                                                          */
/* Returns            : None.                                               */
/****************************************************************************/
INT4
SnoopRedHandleMcastAuditSyncMsg (tSnoopMsgNode * pMsgNode)
{
#ifdef IGS_WANTED
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
#endif
    UINT1              *pFwdPortBmp = NULL;
    UINT1              *pNullPortBitMap = NULL;
    UINT1              *pAuditPortBmp = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPktInfo       SnoopPktInfo;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tRBElem            *pRBElem = NULL;
    tIPvXAddr           IpGroupAddr;
    tSnoopTag           VlanId;
    tMacAddr            MacGroupAddr;
    UINT4               u4IpGroupAddr = 0;
    UINT4               u4Port = 0;
    UINT4               u4Status = 0;
    UINT4               u4EventType = 0;
    UINT4               u4VlanCnt = 0;
    UINT2               u2Index = 0;
    UINT4               u4Instance = 0;
    BOOL1               bResult = OSIX_FALSE;
    tSnoopMacGrpFwdEntry MacGrpFwdEntry;

    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));
    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&IpGroupAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    u4EventType = pMsgNode->uInMsg.AuditSyncMsg.u4Events;
    SNOOP_MEM_CPY (VlanId, pMsgNode->uInMsg.AuditSyncMsg.VlanId,
                   sizeof (tSnoopTag));
    u4Instance = pMsgNode->u4Instance;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_MEM_CPY (MacGroupAddr,
                   pMsgNode->uInMsg.AuditSyncMsg.McastAddr, SNOOP_MAC_ADDR_LEN);
    pAuditPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pAuditPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pAuditPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while handling the received Mcast Audit\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pAuditPortBmp, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_CPY (pAuditPortBmp,
                   pMsgNode->uInMsg.AuditSyncMsg.AuditPortBmp,
                   SNOOP_PORT_LIST_SIZE);

    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while handling the received Mcast Audit for Snoop PVLAN LIST \r\n");
        UtilPlstReleaseLocalPortList (pAuditPortBmp);
        return SNOOP_FAILURE;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* scanning is done for the associated vlans in the PVLAN domain and
     * also for the incoming vlan identifier */
    for (u4VlanCnt = SNOOP_ZERO;
         u4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans; u4VlanCnt++)
    {
        if (u4VlanCnt != SNOOP_ZERO)
        {
            SNOOP_OUTER_VLAN (VlanId) =
                L2PvlanMappingInfo.pMappedVlans[u4VlanCnt - 1];
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
        }

        switch (u4EventType)
        {
            case SNOOP_RED_MCAST_PORTS_ONLY_IN_HW:
                /* For the case the port present in the forwarding list of the
                 * multicast entry in the hardware but not present in the 
                 * software,  the port should be included in the multicast 
                 * group information available in the software.
                 */

            case SNOOP_RED_MCAST_ENTRY_ONLY_IN_HW:
                /* Here the case where the entry is present in the hardware 
                 * but not in the software, the entry should be created in 
                 * the control section and a general query should be send on 
                 * non-routers present in the forwarding list.
                 */

                /* For the MAC address present in the hardware, form a 
                 * valid multicast IP address.
                 * 
                 * Example  
                 * 
                 *    For Mac Address       01-00-5E-01-02-03  
                 *    Multicast IP Address  224.1.2.3
                 */

                SNOOP_RED_IP_FROM_MAC (u4IpGroupAddr, MacGroupAddr);

                IpGroupAddr.u1Afi = SNOOP_ADDR_TYPE_IPV4;
                IpGroupAddr.u1AddrLen = IPVX_MAX_INET_ADDR_LEN;
                SNOOP_MEM_CPY (IpGroupAddr.au1Addr, &u4IpGroupAddr,
                               SNOOP_IP_ADDR_SIZE);

                /* Check whether IGMP snooping is disabled for this Vlan */
                if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                           SNOOP_ADDR_TYPE_IPV4,
                                           &pSnoopVlanEntry) == SNOOP_SUCCESS)
                {
                    if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        UtilPlstReleaseLocalPortList (pAuditPortBmp);
                        return SNOOP_SUCCESS;
                    }
                }
                else
                {
                    /* VLAN entry not found, so create a new VLAN entry */
                    if (SnoopVlanCreateVlanEntry (u4Instance, VlanId,
                                                  SNOOP_ADDR_TYPE_IPV4,
                                                  &pSnoopVlanEntry) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);

                        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                            SNOOP_CONTROL_PATH_TRC |
                                            SNOOP_DBG_RED |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_RED_NAME,
                                            "Unable to create VLAN entry "
                                            "for VLAN id %d\n",
                                            SNOOP_OUTER_VLAN (VlanId));
                        UtilPlstReleaseLocalPortList (pAuditPortBmp);
                        return SNOOP_FAILURE;
                    }
                }
                pSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pSnoopRtrPortBmp == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_RED_NAME,
                                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "while handling the received Mcast Audit for pSnoopRtrPortBmp\r\n");
                    UtilPlstReleaseLocalPortList (pAuditPortBmp);
                    return SNOOP_FAILURE;
                }
                MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);

                pNullPortBitMap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pNullPortBitMap == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_RED_NAME,
                                   "Error in allocating memory for pNullPortBitMap\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "while handling the received Mcast Audit for pNullPortBitMap\r\n");
                    UtilPlstReleaseLocalPortList (pAuditPortBmp);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pNullPortBitMap, 0, sizeof (tSnoopPortBmp));
                pFwdPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pFwdPortBmp == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_RED_NAME,
                                   "Error in allocating memory for pFwdPortBmp\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "while handling the received Mcast Audit for pFwdPortBmp\r\n");
                    UtilPlstReleaseLocalPortList (pNullPortBitMap);
                    UtilPlstReleaseLocalPortList (pAuditPortBmp);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pFwdPortBmp, 0, sizeof (tSnoopPortBmp));

                if (SNOOP_MEM_CMP (pSnoopRtrPortBmp,
                                   pNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
                {

                    for (u2Index = 0; u2Index < SNOOP_PORT_LIST_SIZE; u2Index++)
                    {
                        /* Extract the Membership port (other than router ports) 
                         * by performing Xor operation on AuditBitmap port list 
                         * and consolidate router port list (static and dynamic).
                         */
                        pFwdPortBmp[u2Index] = pAuditPortBmp[u2Index] ^
                            pSnoopRtrPortBmp[u2Index];
                    }
                }
                else
                {
                    SNOOP_MEM_CPY (pFwdPortBmp, pAuditPortBmp,
                                   SNOOP_PORT_LIST_SIZE);
                }

                for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE;
                     u4Port++)
                {
                    SNOOP_IS_PORT_PRESENT (u4Port, pFwdPortBmp, bResult);

                    if (bResult == OSIX_TRUE)
                    {
                        SnoopPktInfo.u4InPort = u4Port;
                        SnoopPktInfo.u1PktType = SNOOP_IGMP_V2REPORT;

                        /*Check if group entry is present or not */
                        if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                                                   IpGroupAddr,
                                                   &pSnoopGroupEntry)
                            != SNOOP_SUCCESS)
                        {
                            if (SnoopGrpCreateGroupEntry (u4Instance, VlanId,
                                                          IpGroupAddr,
                                                          &SnoopPktInfo,
                                                          NULL, 0, 0,
                                                          &pSnoopGroupEntry)
                                != SNOOP_SUCCESS)
                            {
                                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                     L2PvlanMappingInfo.
                                                                     pMappedVlans);

                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_RED |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_RED_NAME,
                                               "SNOOP Red New Group entry creation "
                                               "failed\r\n");
                                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                                UtilPlstReleaseLocalPortList (pAuditPortBmp);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                        }
                        else
                        {
                            /* Add the Port Entry to Group Entry */
                            if ((SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                                 pSnoopGroupEntry,
                                                                 &SnoopPktInfo,
                                                                 NULL,
                                                                 SNOOP_FALSE))
                                != SNOOP_SUCCESS)
                            {
                                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                                UtilPlstReleaseLocalPortList (pAuditPortBmp);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                        }

                        if (SnoopFwdGetMacForwardingEntry (u4Instance,
                                                           VlanId, MacGroupAddr,
                                                           SNOOP_ADDR_TYPE_IPV4,
                                                           &pSnoopMacGrpFwdEntry)
                            != SNOOP_FAILURE)
                        {
                            SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                    pSnoopMacGrpFwdEntry->
                                                    PortBitmap);
                        }
                        else
                        {
                            /* MAC forward entry not found, so create a new one */
                            if (SNOOP_VLAN_MAC_FWD_ALLOC_MEMBLK (u4Instance,
                                                                 pSnoopMacGrpFwdEntry)
                                == NULL)
                            {
                                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                     L2PvlanMappingInfo.
                                                                     pMappedVlans);
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_OS_RES_DBG,
                                           "Memory allocation for MAC forward "
                                           "entry \r\n");
                                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                                       SNOOP_DBG_FLAG,
                                                       SNOOP_DBG_RESRC,
                                                       SNOOP_OS_RES_DBG,
                                                       SnoopSysErrString
                                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                                       "for MAC forward \r\n");
                                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                                UtilPlstReleaseLocalPortList (pAuditPortBmp);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }

                            SNOOP_MEM_SET (pSnoopMacGrpFwdEntry, 0,
                                           sizeof (tSnoopMacGrpFwdEntry));

                            /* Update the fields of the MAC entry */
                            SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->VlanId,
                                           pSnoopGroupEntry->VlanId,
                                           sizeof (tSnoopTag));
                            SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->MacGroupAddr,
                                           MacGroupAddr, SNOOP_MAC_ADDR_LEN);

                            SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->
                                                    PortBitmap,
                                                    pSnoopMacGrpFwdEntry->
                                                    PortBitmap);

                            if (u4VlanCnt == SNOOP_ZERO)
                            {
                                SNOOP_SLL_INIT (&pSnoopMacGrpFwdEntry->
                                                GroupEntryList);

                                /* Assign a back pointer to the Group entry node */
                                pSnoopGroupEntry->pMacGrpFwdEntry =
                                    pSnoopMacGrpFwdEntry;

                                /* Add the IP group entry node to the 
                                 * MAC Group IP list */
                                SNOOP_SLL_ADD (&
                                               (pSnoopMacGrpFwdEntry->
                                                GroupEntryList),
                                               &pSnoopGroupEntry->pNextGrpNode);
                            }

                            u4Status =
                                RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->
                                           MacMcastFwdEntry,
                                           (tRBElem *) pSnoopMacGrpFwdEntry);

                            if (u4Status == RB_FAILURE)
                            {
                                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                     L2PvlanMappingInfo.
                                                                     pMappedVlans);
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                           "RBTree Addition Failed \
                                     for MAC forward Entry \r\n");
                                SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                                pSnoopMacGrpFwdEntry);
                                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                                UtilPlstReleaseLocalPortList (pAuditPortBmp);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                            if ((pSnoopInstInfo != NULL)
                                && (pSnoopMacGrpFwdEntry != NULL))
                            {
                                pSnoopInfo =
                                    &(pSnoopInstInfo->
                                      SnoopInfo[pSnoopMacGrpFwdEntry->
                                                u1AddressType - 1]);
                                if (pSnoopInfo != NULL)
                                {
                                    pSnoopInfo->u4FwdGroupsCnt += 1;

                                }
                            }

                        }
                        /* Update VLAN multicast egress ports */
                        if (SnoopMiVlanUpdateDynamicMcastInfo
                            (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                             pSnoopMacGrpFwdEntry->VlanId,
                             u4Port, VLAN_ADD) != VLAN_SUCCESS)
                        {
                            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                 L2PvlanMappingInfo.
                                                                 pMappedVlans);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Updation to VLAN module for port deletion"
                                       " for MAC mcast entry failed\r\n");

                            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                          pSnoopMacGrpFwdEntry,
                                                          SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);

                            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                            if ((pSnoopInstInfo != NULL)
                                && (pSnoopMacGrpFwdEntry != NULL))
                            {
                                pSnoopInfo =
                                    &(pSnoopInstInfo->
                                      SnoopInfo[pSnoopMacGrpFwdEntry->
                                                u1AddressType - 1]);
                                if ((pSnoopInfo != NULL)
                                    && (pSnoopInfo->u4FwdGroupsCnt != 0))
                                {
                                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                                }
                            }
                            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                          MacMcastFwdEntry,
                                          (tRBElem *) pSnoopMacGrpFwdEntry);

                            SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                            pSnoopMacGrpFwdEntry);
                            UtilPlstReleaseLocalPortList (pFwdPortBmp);
                            UtilPlstReleaseLocalPortList (pNullPortBitMap);
                            UtilPlstReleaseLocalPortList (pAuditPortBmp);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            return SNOOP_FAILURE;
                        }

                    }

                }

                if (pSnoopGroupEntry == NULL)
                {
                    UtilPlstReleaseLocalPortList (pFwdPortBmp);
                    UtilPlstReleaseLocalPortList (pNullPortBitMap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    break;
                }

                if (SNOOP_MEM_CMP (pSnoopRtrPortBmp,
                                   pNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
                {
                    if (SnoopFwdUpdateFwdBitmap (u4Instance,
                                                 pSnoopRtrPortBmp,
                                                 pSnoopVlanEntry,
                                                 pSnoopGroupEntry->GroupIpAddr,
                                                 SNOOP_ADD_PORT) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                            SNOOP_DBG_RED |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_RED_NAME,
                                            "Unable to add the vlan %d router ports"
                                            " in forwarding port bitmap \n",
                                            VlanId);

                    }

                }
#ifdef IGS_WANTED
                if (IgsEncodeQuery (u4Instance, 0, SNOOP_IGMP_QUERY,
                                    pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                    u1ConfigOperVer, &pOutBuf,
                                    pSnoopVlanEntry) != SNOOP_SUCCESS)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                                   SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_RED_NAME,
                                   "Unable to form General Query \n");
                    UtilPlstReleaseLocalPortList (pFwdPortBmp);
                    UtilPlstReleaseLocalPortList (pNullPortBitMap);
                    UtilPlstReleaseLocalPortList (pAuditPortBmp);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }

                SnoopVlanForwardQuery (u4Instance, VlanId, SNOOP_ADDR_TYPE_IPV4,
                                       SNOOP_INVALID_PORT,
                                       VLAN_FORWARD_SPECIFIC,
                                       pOutBuf, pFwdPortBmp, SNOOP_QUERY_SENT);
#endif
                UtilPlstReleaseLocalPortList (pFwdPortBmp);
                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                break;

            case SNOOP_RED_MCAST_ENTRY_ONLY_IN_SW:

                /* For the entries present in the software but in the hardware,
                 * remove the entry present in the software. */
                SNOOP_MEM_CPY (MacGrpFwdEntry.VlanId, VlanId,
                               sizeof (tSnoopTag));
                MacGrpFwdEntry.u1AddressType = SNOOP_ADDR_TYPE_IPV4;
                SNOOP_MEM_CPY (&MacGrpFwdEntry.MacGroupAddr, MacGroupAddr,
                               SNOOP_MAC_ADDR_LEN);

                pRBElem = RBTreeGet
                    (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                     (tRBElem *) & MacGrpFwdEntry);

                if (pRBElem != NULL)
                {
                    pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

                    /* Delete the list of pointers to the group entry */
                    while ((pSnoopGroupEntry = (tSnoopGroupEntry *)
                            SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->
                                             GroupEntryList)) != NULL)
                    {

                        /* Send SyncUp Message to Standby Node */
                        MEMSET (pSnoopGroupEntry->PortBitmap, 0,
                                sizeof (tSnoopPortBmp));

                        SnoopRedActiveSendGrpInfoSync (u4Instance,
                                                       pSnoopGroupEntry);

                        if ((SnoopGrpDeleteGroupEntry (u4Instance,
                                                       pSnoopGroupEntry->VlanId,
                                                       pSnoopGroupEntry->
                                                       GroupIpAddr)) !=
                            SNOOP_SUCCESS)
                        {
                            continue;
                        }
                    }

                    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                  pSnoopMacGrpFwdEntry,
                                                  SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if ((pSnoopInstInfo != NULL)
                        && (pSnoopMacGrpFwdEntry != NULL))
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->
                              SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType -
                                        1]);
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }

                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  MacMcastFwdEntry,
                                  (tRBElem *) pSnoopMacGrpFwdEntry);
                    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                    pSnoopMacGrpFwdEntry);
                }

                break;

            case SNOOP_RED_MCAST_PORTS_ONLY_IN_SW:

                /* For the port present in the forwarding list of the multicast
                 * entry in the software but not present in the hardware, the
                 * port should be removed from the forwarding list in the 
                 * software.
                 */
                for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE;
                     u4Port++)
                {
                    SNOOP_IS_PORT_PRESENT (u4Port, pAuditPortBmp, bResult);
                    if (bResult == OSIX_TRUE)
                    {

                        SnoopRedGrpDeletePortEntry (u4Instance, VlanId,
                                                    u4Port, MacGroupAddr);

                        if (SnoopFwdGetMacForwardingEntry (u4Instance,
                                                           VlanId,
                                                           MacGroupAddr,
                                                           SNOOP_ADDR_TYPE_IPV4,
                                                           &pSnoopMacGrpFwdEntry)
                            != SNOOP_FAILURE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                                      pSnoopMacGrpFwdEntry->
                                                      PortBitmap);
                        }

                        /* If the port bitmap becomes NULL delete the 
                         * forward entry */
                        if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->PortBitmap,
                                           gNullPortBitMap,
                                           SNOOP_PORT_LIST_SIZE) == 0)
                        {
                            /* Delete the list of pointers to the group entry */
                            while ((pTempSnoopGroupEntry = (tSnoopGroupEntry *)
                                    SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->
                                                     GroupEntryList)) != NULL)
                            {
                                pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                                SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                                  GroupEntryList,
                                                  pTempSnoopGroupEntry);
                            }

                            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                          pSnoopMacGrpFwdEntry,
                                                          SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);

                            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                            if (pSnoopInstInfo != NULL)
                            {
                                pSnoopInfo =
                                    &(pSnoopInstInfo->
                                      SnoopInfo[pSnoopMacGrpFwdEntry->
                                                u1AddressType - 1]);
                                if ((pSnoopInfo != NULL)
                                    && (pSnoopInfo->u4FwdGroupsCnt != 0))
                                {
                                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                                }
                            }

                            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                          MacMcastFwdEntry,
                                          (tRBElem *) pSnoopMacGrpFwdEntry);

                            SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                            pSnoopMacGrpFwdEntry);
                            break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    UtilPlstReleaseLocalPortList (pAuditPortBmp);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedGrpDeletePortEntry                           */
/*                                                                           */
/* Description        : This function deletes port entry corresponding to    */
/*                      all the group entries in the VLAN                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - Vlan Id                                 */
/*                      u4Port     - Port number                             */
/*                      McastAddr  - Mac Address                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopRedGrpDeletePortEntry (UINT4 u4Instance, tSnoopTag VlanId,
                            UINT4 u4Port, tMacAddr McastAddr)
{
    UINT1              *pNullPortBitMap = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tMacAddr            MacGroupAddr;
    tMacAddr            NullMacAddr;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1PortFound = SNOOP_FALSE;

    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);
    pNullPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pNullPortBitMap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pNullPortBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while deleting port entry corresponding to group entry for pNullPortBitMap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pNullPortBitMap, 0, sizeof (tSnoopPortBmp));

    while (pRBElem != NULL)
    {
        pRBElemNext = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                     GroupEntry, pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        /* If the multicast mac address is not NULL then delete the port entry 
         * for VlanId and Multicast Group address */
        if (SNOOP_MEM_CMP (McastAddr, NullMacAddr, sizeof (tMacAddr)) != 0)
        {
            SNOOP_MEM_SET (MacGroupAddr, 0, sizeof (tMacAddr));

            SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                     (pSnoopGroupEntry->GroupIpAddr.au1Addr),
                                     MacGroupAddr);

            if ((SNOOP_MEM_CMP (pSnoopGroupEntry->VlanId, VlanId,
                                sizeof (tSnoopTag)) != 0) &&
                (SNOOP_MEM_CMP (McastAddr, MacGroupAddr, sizeof (tMacAddr))
                 != 0))
            {
                pRBElem = pRBElemNext;
                continue;
            }
        }
        else if (SNOOP_MEM_CMP (pSnoopGroupEntry->VlanId, VlanId,
                                sizeof (tSnoopTag)) != 0)
        {
            pRBElem = pRBElemNext;
            continue;
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap, bResult);

        if (bResult == OSIX_FALSE)
        {
            pRBElem = pRBElemNext;
            continue;
        }

        /* The flag u1PortFound is not reset at each iteration of the loop */
        u1PortFound = SNOOP_FALSE;

        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortEntry, tSnoopPortEntry *)
        {
            if (pSnoopASMPortEntry->u4Port == u4Port)
            {
                u1PortFound = SNOOP_TRUE;
                break;
            }
        }

        if (u1PortFound == SNOOP_FALSE)
        {
            pRBElem = pRBElemNext;
            continue;
        }

        /* Stop the PortPurge/Leave timer if it is running */
        if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
        }

        if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
        }
        /* Delete all the hosts attached to the port */
        if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
        {
            while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                    SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList)) != NULL)
            {
                pRBElem = (tRBElem *) pSnoopASMHostEntry;
                /* Remove the host from the Host Information RBTree */
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                              pRBElem);
                SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                  pSnoopASMHostEntry);

                SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                  pSnoopASMHostEntry);
            }
        }

        /*Release the port node from the ASMPortList */
        SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList, pSnoopASMPortEntry);
        SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);

        /* Update the Group entry port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

        /* Update the group entry in ASM port bit map */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->ASMPortBitmap);

        SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);

        if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, pNullPortBitMap,
                           SNOOP_PORT_LIST_SIZE) != 0)
        {
            /* Continue with the next group record */
            pRBElem = pRBElemNext;
            continue;
        }
        else
        {
            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr)
                != SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                               SNOOP_DBG_ALL_FAILURE,
                               SNOOP_RED_NAME,
                               "Deletion of Group record failed\r\n");
                UtilPlstReleaseLocalPortList (pNullPortBitMap);
                return SNOOP_FAILURE;
            }
        }
        pRBElem = pRBElemNext;
    }

    UtilPlstReleaseLocalPortList (pNullPortBitMap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedHandleBulkUpdEvent                           */
/*                                                                           */
/* Description        : It Handles the bulk update event. This event is used */
/*                      to start the next sub bulk update. So                */
/*                      SnoopRedActiveProcessBulkReq is triggered.           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopRedHandleBulkUpdEvent (VOID)
{
    if ((SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE))
    {
        SnoopRedActiveProcessBulkReq (NULL);
    }
    else
    {
        SNOOP_RED_BULK_REQ_RECD () = SNOOP_TRUE;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopRedSyncBulkUpdNextEntry                         */
/*                                                                           */
/* Description        : This function is used to update the BulkUpdNextEntry */
/*                      when the entry is removed.                           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopRedSyncBulkUpdNextEntry (UINT4 u4Instance, tRBElem * pSnoopElem,
                              tSnoopBulkUpdSyncTypes SnoopBulkUpdSyncType)
{
    tSnoopVlanEntry    *pBulkUpdNextVlanEntry = NULL;
    tSnoopMacGrpFwdEntry *pBulkUpdNextMacFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pBulkUpdNextIpFwdEntry = NULL;
    tSnoopVlanEntry    *pVlanEntry = NULL;
    tSnoopMacGrpFwdEntry *pMacFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pIpFwdEntry = NULL;
    tSnoopRtrPortEntry *pBulkUpdNextRtrPort = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tIPvXAddr           SrcAddr;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));

    switch (SnoopBulkUpdSyncType)
    {
        case SNOOP_RED_BULK_UPD_VLAN_REMOVE:

            pBulkUpdNextVlanEntry =
                (tSnoopVlanEntry *) SNOOP_RED_BULK_UPD_VLAN_ENTRY ();

            pVlanEntry = (tSnoopVlanEntry *) pSnoopElem;

            if (pBulkUpdNextVlanEntry == NULL || pVlanEntry == NULL)
            {
                return;
            }

            if ((SNOOP_MEM_CMP (pBulkUpdNextVlanEntry->VlanId,
                                pVlanEntry->VlanId,
                                sizeof (tSnoopTag)) == 0) &&
                (pBulkUpdNextVlanEntry->u1AddressType ==
                 pVlanEntry->u1AddressType))
            {
                SNOOP_RED_BULK_UPD_VLAN_ENTRY () =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                                   pSnoopElem, NULL);
            }

            break;

        case SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE:

            pBulkUpdNextMacFwdEntry = (tSnoopMacGrpFwdEntry *)
                SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY ();

            pMacFwdEntry = (tSnoopMacGrpFwdEntry *) pSnoopElem;

            if (pBulkUpdNextMacFwdEntry == NULL || pMacFwdEntry == NULL)
            {
                return;
            }

            if ((SNOOP_MEM_CMP (pBulkUpdNextMacFwdEntry->VlanId,
                                pMacFwdEntry->VlanId,
                                sizeof (tSnoopTag)) == 0) &&
                (pBulkUpdNextMacFwdEntry->u1AddressType ==
                 pMacFwdEntry->u1AddressType) &&
                (SNOOP_MEM_CMP (pBulkUpdNextMacFwdEntry->MacGroupAddr,
                                pMacFwdEntry->MacGroupAddr, SNOOP_MAC_ADDR_LEN)
                 == 0))
            {
                SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   MacMcastFwdEntry, pSnoopElem, NULL);
            }

            break;

        case SNOOP_RED_BULK_UPD_IP_FWD_REMOVE:

            pBulkUpdNextIpFwdEntry = (tSnoopIpGrpFwdEntry *)
                SNOOP_RED_BULK_UPD_IP_FWD_ENTRY ();

            pIpFwdEntry = (tSnoopIpGrpFwdEntry *) pSnoopElem;

            if (pBulkUpdNextIpFwdEntry == NULL || pIpFwdEntry == NULL)
            {
                return;
            }

            if ((SNOOP_MEM_CMP (pBulkUpdNextIpFwdEntry->VlanId,
                                pIpFwdEntry->VlanId,
                                sizeof (tSnoopTag)) == 0) &&
                (SNOOP_MEM_CMP ((UINT1 *) &(pBulkUpdNextIpFwdEntry->SrcIpAddr),
                                (UINT1 *) &SrcAddr,
                                sizeof (tIPvXAddr)) == 0)
                &&
                (SNOOP_MEM_CMP
                 ((UINT1 *) &(pBulkUpdNextIpFwdEntry->GrpIpAddr),
                  (UINT1 *) &(pIpFwdEntry->GrpIpAddr),
                  sizeof (tIPvXAddr)) == 0))

            {
                SNOOP_RED_BULK_UPD_IP_FWD_ENTRY () =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   IpMcastFwdEntry, pSnoopElem, NULL);
            }

            break;

        case SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE:
            pBulkUpdNextRtrPort = (tSnoopRtrPortEntry *)
                SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY ();
            pSnoopRtrPortEntry = (tSnoopRtrPortEntry *) pSnoopElem;

            if (pBulkUpdNextRtrPort == NULL || pSnoopRtrPortEntry == NULL)
            {
                return;
            }

            if (pBulkUpdNextRtrPort->u4PhyPortIndex ==
                pSnoopRtrPortEntry->u4PhyPortIndex)
            {
                if (!SNOOP_MEM_CMP (&pBulkUpdNextRtrPort->pVlanEntry->VlanId,
                                    &pSnoopRtrPortEntry->pVlanEntry->VlanId,
                                    sizeof (tSnoopTag)) &&
                    (pBulkUpdNextRtrPort->pVlanEntry->u1AddressType ==
                     pSnoopRtrPortEntry->pVlanEntry->u1AddressType))
                {
                    SNOOP_RED_BULK_UPD_RTR_PORT_ENTRY () =
                        RBTreeGetNext (gRtrPortTblRBRoot,
                                       pSnoopRtrPortEntry, NULL);
                }
            }
            break;
        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopRedSendBulkUpdTailMsg                           */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Node is not active.Bulk update tail msg not sent\r\n");
        return SNOOP_SUCCESS;
    }

    /* Allocate memory for the bulk request message */
    u2DataLen = SNOOP_RED_BULK_UPD_TAIL_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_RED_NAME,
                       "Buffer creation failure for Snoop Bulk update tail "
                       "Message fails\r\n");
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        return (SNOOP_FAILURE);
    }

    /* Form a bulk update tail message. 

     *        <----------1 Byte----------->
     ************************************* 
     *        *                           *
     * RM Hdr * IGS_RED_BULK_UPD_TAIL_MSG * 
     *        *                           *  
     *************************************

     * The RM Hdr shall be included by RM.
     */

    /* Fill the type of Event as IGS_RED_BULK_UPD_TAIL_MSG */
    u1MsgType = IGS_RED_BULK_UPD_TAIL_MSG;
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Call the API provided by RM to send the data to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        == RM_FAILURE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_RED_NAME,
                       "Enqueuing Snoop Disable Sync fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        SnoopRmHandleProtocolEvent (&ProtoEvt);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopRestartModule                                   */
/*                                                                           */
/* Description        : This function will restart Snooping module           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRestartModule (VOID)
{
    UINT4               u4Instance;

    SNOOP_LOCK ();

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        /* This is done for trace messages */
        gu4SnoopTrcInstId = u4Instance;
        gu4SnoopDbgInstId = u4Instance;

        /*[17-09-2007 Check Added to avoid segmentation fault] */
        if (SNOOP_INSTANCE_INFO (u4Instance) != NULL)
        {
            /* Check whether snooping enabled in the context */
            if (SNOOP_SNOOPING_STATUS (u4Instance, (SNOOP_ADDR_TYPE_IPV4 - 1))
                != SNOOP_ENABLE)
            {
                continue;
            }
        }
        else
        {
            continue;
        }
        SnoopModuleShutdownInstance (u4Instance);

        /* Node will move to IDLE state in this function */
        if (SnoopModuleInitInstance (u4Instance) != SNOOP_SUCCESS)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                           SNOOP_DBG_ALL_FAILURE,
                           SNOOP_RED_NAME,
                           "SNOOPING start failed during restart for applying "
                           "configuration database\n");
            SNOOP_UNLOCK ();
            return SNOOP_FAILURE;
        }
    }

    SNOOP_UNLOCK ();

    MEMSET (&gSnoopRedGlobalInfo, 0, sizeof (tSnoopRedGlobalInfo));

    SNOOP_NODE_STATUS () = SNOOP_IDLE_NODE;

    return SNOOP_SUCCESS;
}

/*****************************************************************************
 * Function Name      : SnoopRedSwitchOverQueryTmrExpiry                       
 *                                                                      
 * Description        : This function handles the expiry of query timer
 *                      (started for query response interval). On expiry it
 *                      does the following -
 *                      i.   Resets flag SNOOP_RED_GENERAL_QUERY_INPROGRESS
 *                      ii.  Initiate hardware audit
 *                      iii. Sends out consolidated over all router ports per
 *                           S-VLAN.
 *                      
 * Input(s)           : None
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedSwitchOverQueryTmrExpiry (VOID)
{
    UINT4               u4Instance = 0;
    UINT1               u1AddressType = 0;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tRBElem            *pRBElemGroupEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortBmp       RtrBitMap;
    tSnoopPortBmp       PortBitmap;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    INT4                i4RetVal = SNOOP_SUCCESS;

    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (&RtrBitMap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&PortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_RED_GENERAL_QUERY_INPROGRESS = SNOOP_FALSE;
    SNOOP_RED_SWITCHOVER_TMR_NODE ().u1TimerType = SNOOP_INVALID_TIMER_TYPE;

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_STARTED)
        {
            for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
                 u1AddressType++)
            {
                if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddressType - 1))
                    != SNOOP_ENABLE)
                {
                    /* For the current context snooping module is disabled.
                     * So no need to send the Consolidated report to the 
                     * upstream ports */
                    continue;
                }

                if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                                          (u1AddressType - 1))
                    == SNOOP_FALSE)
                {
                    continue;
                }

                /* Compose and send consolidated report over router ports */
                SnoopRedSendConsolidatedReport (u4Instance, u1AddressType);
            }
        }
    }
    /* During switch over from standby to active, forwarding table has to be
     * updated for the below scenarios:.
     * The forwarding table would have been in sync when Active node was
     * there, when there is a switch-over based on updated group table,
     * the forwarding table has to be updated.
     * Logic Added:
     * Fetch the entry from forwarding table, using the same index check the
     * entry in the group table -
     * Case 1:If there is no entry in the group table for this index,
     * the entry in fowarding table can be deleted.
     * Case 2:If there is an entry in the group table, compare the port bit map,
     * if there is a change in port bit map, update the forwarding table else
     * leave the entry programmed in the forwarding table as it is */
    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        if ((SNOOP_INSTANCE_INFO (u4Instance) != NULL) &&
            (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry != NULL))
        {
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                IpMcastFwdEntry);

            while (pRBElem != NULL)
            {
                pRBNextElem =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   IpMcastFwdEntry, pRBElem, NULL);
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                SNOOP_MEM_CPY (SnoopGroupEntry.VlanId,
                               pSnoopIpGrpFwdEntry->VlanId, sizeof (tSnoopTag));
                IPVX_ADDR_COPY (&(SnoopGroupEntry.GroupIpAddr),
                                &pSnoopIpGrpFwdEntry->GrpIpAddr);

                pRBElemGroupEntry = RBTreeGet (SNOOP_INSTANCE_INFO
                                               (u4Instance)->GroupEntry,
                                               (tRBElem *) & SnoopGroupEntry);
                pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElemGroupEntry;
                if (pSnoopGroupEntry == NULL)
                {
                    /*There is no entry in the group table for this vlan and
                     * group address.
                     * So entry in the forwarding table can be deleted */
#ifdef NPAPI_WANTED
                    if (pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
                        SNOOP_INVALID_TIMER_TYPE)
                    {
                        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
                    }

                    if (SnoopNpUpdateMcastFwdEntry
                        (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                         pSnoopIpGrpFwdEntry->SrcIpAddr,
                         pSnoopIpGrpFwdEntry->GrpIpAddr,
                         pSnoopIpGrpFwdEntry->PortBitmap,
                         SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_OS_RES_DBG, "Deletion of IP forwarding"
                                   " entry from hardware failed\r\n");
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_FWD_DBG, SNOOP_FWD_DBG,
                                   "Deletion of IP forwarding entry from hardware failed\r\n");
                        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                          SNOOP_DBG_NP, SNOOP_NP_DBG,
                                          SnoopSysErrString
                                          [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                        return;
                    }

#endif
                    SNOOP_MEM_SET (&(SnoopGroupEntry.PortBitmap), 0,
                                   SNOOP_PORT_LIST_SIZE);

                    SnoopActiveRedGrpSync (u4Instance,
                                           pSnoopIpGrpFwdEntry->SrcIpAddr,
                                           &SnoopGroupEntry, gNullPortBitMap);

                    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                    if (RBTreeRemove
                        (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) pSnoopIpGrpFwdEntry) == RB_SUCCESS)
                    {
                        pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                        if (pSnoopInstInfo != NULL)
                        {
                            pSnoopInfo =
                                &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                            if ((pSnoopInfo != NULL)
                                && (pSnoopInfo->u4FwdGroupsCnt != 0))
                            {
                                pSnoopInfo->u4FwdGroupsCnt -= 1;
                            }
                        }
                    }
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);

                }
                else
                {
                    /* Get the router port and Include it in
                     * Forward Entry port bit map */
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (pSnoopIpGrpFwdEntry->
                                                              VlanId),
                                                             pSnoopIpGrpFwdEntry->
                                                             u1AddressType, 0,
                                                             RtrBitMap, NULL);

                    SNOOP_MEM_CPY (&PortBitmap, pSnoopGroupEntry->PortBitmap,
                                   sizeof (tSnoopPortBmp));

                    SNOOP_UPDATE_PORT_LIST (RtrBitMap, PortBitmap);

                    /* Compare the port bit map, if there is change in port bit map
                     * update it else leave the entry as it as */
                    if (SNOOP_MEM_CMP (PortBitmap,
                                       pSnoopIpGrpFwdEntry->PortBitmap,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        i4RetVal =
                            SnoopFwdUpdateIpFwdTable (u4Instance,
                                                      pSnoopGroupEntry,
                                                      pSnoopIpGrpFwdEntry->
                                                      SrcIpAddr, 0, PortBitmap,
                                                      SNOOP_CREATE_FWD_ENTRY);
                        if (i4RetVal == SNOOP_FAILURE)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_FWD_DBG,
                                       SNOOP_FWD_DBG,
                                       "Addition of portList in IP forwarding entry failed\r\n");
                        }
                    }
                }
                pRBElem = pRBNextElem;
            }
        }
    }

    /* Start the hardware audit for IP forwarding mode. */
    SnoopRedStartIPMCAudit ();

    return;
}

/*****************************************************************************
 * Function Name      : SnoopRedStartIPMCAudit                             
 *                                                                           
 * Description        : This function spawns a low priority audit task for 
 *                      IGS, which takes care of doing hardware audit of IPMC 
 *                      table for IP based IGS. IGS module will program 
 *                      the hardware only if PIM, DVMRP and IGMP proxy is 
 *                      disabled. So only if all these three modules are 
 *                      disabled, IPMC auditing needs to be done from IGS.
 *                      This function will spawn the audit task only if all 
 *                      the above three modules are disabled.
 * 
 * Input(s)           : None.                                                
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None                                                 
 *                                                                           
 * Global Variables                                                          
 * Modified           : None                                                 
 *                                                                           
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedStartIPMCAudit (VOID)
{
#ifdef NPAPI_WANTED
    UINT4               i4RetVal;

    if (SNOOP_AUDIT_TASK_ID == 0)
    {
        i4RetVal = OsixTskCrt (SNOOP_AUDIT_TASK_NAME,
                               SNOOP_AUDIT_TASK_PRIORITY,
                               OSIX_DEFAULT_STACK_SIZE,
                               (OsixTskEntry) SnoopRedAuditTaskMain, 0,
                               &SNOOP_AUDIT_TASK_ID);
    }

    UNUSED_PARAM (i4RetVal);
#endif /* NPAPI_WANTED */
    return;
}

/*****************************************************************************
 * Function Name      : SnoopRedAuditTaskMain                             
 *                                                                           
 * Description        : This function start the hardware audit for IP based
 *                      IGS
 *                                                                           
 * Input(s)           : None.                                                
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None                                                 
 *                                                                           
 * Global Variables                                                          
 * Modified           : None                                                 
 *                                                                           
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedAuditTaskMain (VOID)
{
#ifdef NPAPI_WANTED
    UINT4               u4Context;

    gu1SnoopRedAuditInProgress = SNOOP_TRUE;
    for (u4Context = 0; u4Context < SNOOP_MAX_INSTANCES; u4Context++)
    {
#if defined (IGS_WANTED)
        if (SNOOP_SYSTEM_STATUS (u4Context) != SNOOP_STARTED)
        {
            continue;
        }
        if (SnoopIsIgmpSnoopingEnabled (u4Context) != SNOOP_ENABLED)
        {
            continue;
        }
        /* Audit should be done only for Context in which 
         * Snooping is in IP Mode only.*/
        if (SnoopMiGetForwardingType (u4Context) != SNOOP_MCAST_FWD_MODE_IP)
        {
            continue;
        }
        /* Clear all the Multicast forwarding entries form the Hardware */
        SnoopRedSyncPendingMcastEntries (u4Context);
#endif
    }                            /* end for */
    gu1SnoopRedAuditInProgress = SNOOP_FALSE;
#endif
    SNOOP_AUDIT_TASK_ID = 0;
    return;
}

/*****************************************************************************
 * Function Name      : SnoopRedSendGeneralQuery                             
 *                                                                           
 * Description        : This function scan all the snoop enabled outer  
 *                      vlan and send general query over all the member ports.
 *                      It also sets a global flag to indicate the sending of
 *                      general query on switch over. 
 *                                                                           
 * Input(s)           : u4Instance - Context Id
 *                      u1AddressType - Address Type.
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None                                                 
 *                                                                           
 * Global Variables                                                          
 * Modified           : gu1SnoopRedGenQueryInProgress                        
 *                                                                           
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedSendGeneralQuery (UINT4 u4Instance, UINT1 u1AddressType)
{
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopVlanEntry    *pSnoopTmpVlanEntry = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tSnoopMsgNode      *pNode = NULL;
    tSnoopTag           VlanId;
    UINT4               u4Count = 0;
    INT4                i4RetVal = 0;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bDynResult = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Snoop is disabled. \r\n");

        return;
    }

    /* So set the flag that indicates general query is in
     * progress */

    SNOOP_RED_GENERAL_QUERY_INPROGRESS = SNOOP_TRUE;

    if (gu1BatchingInProgress == SNOOP_FALSE)
    {
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);
        if (pRBElem == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "Snoop not enabled on any VLAN \r\n");

            return;
        }

        gu1BatchingInProgress = SNOOP_TRUE;
    }

    else
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Batching in progress \r\n");

        SNOOP_OUTER_VLAN (VlanId) = gu4SnoopVlanId;

        if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_VLAN_TRC, "Unable to get Vlan record "
                       "for the given instance and the VlanId \r\n");
            gu1BatchingInProgress = SNOOP_FALSE;
            return;
        }

        pRBElem = (tRBElem *) pSnoopVlanEntry;
    }

    do
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        if (pSnoopVlanEntry == NULL)
        {
            gu1BatchingInProgress = SNOOP_FALSE;
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "pSnoopVlanEntry NULL \r\n");
            return;
        }
        pRBElem = pRBElemNext;

        if ((pSnoopVlanEntry->u1AddressType != u1AddressType) ||
            (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE))
        {
            continue;
        }

        i4RetVal = SnoopVlanSendGeneralQuery (u4Instance, SNOOP_INVALID_PORT,
                                              pSnoopVlanEntry);
        UNUSED_PARAM (i4RetVal);

        /* If the switch is configured as querier, start the
         * query timer in this VLAN.
         */
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        {
            if ((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier) &&
                (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER))
            {
                if (pSnoopVlanEntry->u1StartUpQCount <
                    pSnoopVlanEntry->u1MaxStartUpQCount)
                {
                    pSnoopVlanEntry->u1StartUpQCount = 0;
                    pSnoopVlanEntry->u1StartUpQCount++;
                    SnoopRedSyncStartUpQryCount (u4Instance, pSnoopVlanEntry);
                }
                /* Transition from Non-Querier to Querier */
                SnoopHandleTransitionToQuerier (u4Instance, pSnoopVlanEntry);
            }
        }
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        {
            if (((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier) ==
                 SNOOP_QUERIER)
                && (pSnoopVlanEntry->u1Querier == SNOOP_NON_QUERIER))
            {
                pSnoopVlanEntry->OtherQPresentTimer.u4Instance = u4Instance;
                pSnoopVlanEntry->OtherQPresentTimer.u1TimerType =
                    SNOOP_OTHER_QUERIER_PRESENT_TIMER;

                /* Start Other Querier present interval timer */
                SnoopTmrStartTimer (&pSnoopVlanEntry->OtherQPresentTimer,
                                    pSnoopVlanEntry->u2OtherQPresentInt);
            }
        }

        /* While transition from standby to active router port purge timers 
         * need to be started for all vlans. As the vlan looping is already 
         * done here so this timer also started here itself to avoid looping
         * multiple times */

        /* Router port purge timer is started on all learnt
         * router ports in this VLAN.
         */
        pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                       SnoopInfo[u1AddressType - 1]);

        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pSnoopRtrPortEntry, tSnoopRtrPortEntry *)
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopVlanEntry->StaticRtrPortBitmap,
                                     pSnoopRtrPortEntry->u4Port,
                                     SNOOP_PORT_LIST_SIZE,
                                     bResult)
                OSIX_BITLIST_IS_BIT_SET (pSnoopVlanEntry->DynRtrPortBitmap,
                                         pSnoopRtrPortEntry->u4Port,
                                         SNOOP_PORT_LIST_SIZE,
                                         bDynResult) if ((bResult == OSIX_TRUE)
                                                         && (bDynResult ==
                                                             OSIX_FALSE))
            {
                continue;
            }
            if ((pSnoopRtrPortEntry->u1OperVer == SNOOP_IGS_IGMP_VERSION1) &&
                (pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType ==
                 SNOOP_INVALID_TIMER_TYPE))
            {
                pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u4Instance = u4Instance;
                pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType =
                    SNOOP_ROUTER_PORT_PURGE_V1_TIMER;
                if (pSnoopInfo != NULL)
                {
                    SnoopTmrStartTimer (&pSnoopRtrPortEntry->
                                        V1RtrPortPurgeTimer,
                                        pSnoopInfo->u4RtrPortPurgeInt);
                }
            }

            if ((pSnoopRtrPortEntry->u1OperVer == SNOOP_IGS_IGMP_VERSION2) &&
                (pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType ==
                 SNOOP_INVALID_TIMER_TYPE))
            {
                pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u4Instance = u4Instance;
                pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType =
                    SNOOP_ROUTER_PORT_PURGE_V2_TIMER;
                if (pSnoopInfo != NULL)
                {
                    SnoopTmrStartTimer (&pSnoopRtrPortEntry->
                                        V2RtrPortPurgeTimer,
                                        pSnoopInfo->u4RtrPortPurgeInt);
                }
            }

            if ((pSnoopRtrPortEntry->u1OperVer == SNOOP_IGS_IGMP_VERSION3) &&
                (pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType ==
                 SNOOP_INVALID_TIMER_TYPE))
            {
                pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u4Instance = u4Instance;
                pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType =
                    SNOOP_ROUTER_PORT_PURGE_V3_TIMER;
                SnoopTmrStartTimer (&pSnoopRtrPortEntry->V3RtrPortPurgeTimer,
                                    pSnoopRtrPortEntry->u4V3RtrPortPurgeInt);
            }
        }

        u4Count++;

        if (u4Count == SNOOP_RED_NO_OF_VLANS_PER_BULK_UPDATE)
        {

            if (pRBElem != NULL)
            {
                pSnoopTmpVlanEntry = (tSnoopVlanEntry *) pRBElem;

                gu4SnoopVlanId = SNOOP_OUTER_VLAN (pSnoopTmpVlanEntry->VlanId);
                gu1BatchingInProgress = SNOOP_TRUE;

                if ((pNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_MEM_NAME,
                                   "Memory allocation failed for queue message\r\n");
                    gu1BatchingInProgress = SNOOP_FALSE;
                    return;
                }

                SNOOP_MEM_SET (pNode, 0, sizeof (tSnoopMsgNode));

                pNode->u2MsgType = SNOOP_IGS_RED_GEN_QUERY;
                pNode->u4Instance = u4Instance;

                if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pNode,
                                         SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
                {
                    if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pNode) != MEM_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                                       SNOOP_MEM_NAME,
                                       "Queue message realease failed port delete\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_PKT_TRC,
                                               SnoopSysErrString
                                               [SYS_LOG_QUEUE_RELEASE_FAIL],
                                               "for queue id %d\r\n",
                                               SNOOP_MSG_QUEUE_ID);
                    }

                    gu1BatchingInProgress = SNOOP_FALSE;
                    return;
                }

                SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT);
                return;
            }

            else
            {
                gu1BatchingInProgress = SNOOP_FALSE;
            }
        }

    }
    while (pRBElem != NULL);

    gu1BatchingInProgress = SNOOP_FALSE;

    return;
}

/*****************************************************************************
 * Function Name      : SnoopRedSendConsolidatedReport
 *                                                                           
 * Description        : This function scans the vlan entries, get the router 
 *                      port list for the vlan and call 
 *                      SnoopProcessGeneralQuerycomposes to send the 
 *                      consolidated report on these router ports. 
 *                                                                           
 * Input(s)           : None.                                                
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None                                                 
 *                                                                           
 * Global Variables                                                          
 * Modified           : None                                                 
 *                                                                           
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedSendConsolidatedReport (UINT4 u4Instance, UINT1 u1AddressType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPktInfo       SnoopPktInfo;

    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));

    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE (u4Instance);

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Snoop is disabled \r\n");

        return;
    }
    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        pRBElem = pRBElemNext;

        if (pSnoopVlanEntry->u1AddressType != u1AddressType)
        {
            continue;
        }

        /* Compose the consolidated report if the router port list is not
         * empty for the vlan.*/
        if (SNOOP_MEM_CMP (pSnoopVlanEntry->RtrPortBitmap,
                           gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
        {
            continue;
        }

        /* Send a Consolidated report to all the router port for the current 
         * vlan. */

        /* As SnoopProcessGeneralQuery() function will take care of sending the
         * consolidated report to the router port after receiving the
         * general query. The same funcion is reused here with 
         * SnoopPktInfo.u4InPort = 0.Which will be considered as a special
         * situation.
         * So inside the function SnoopProcessGeneralQuery() if 
         * pSnoopPktInfo->u4InPort = 0 the consolidated report will be fwd to 
         * all the available router ports for this vlan 
         * (specified by pSnoopVlanEntry->RtrPortBitmap) */
        SnoopRedSendConsolidatedReportOnRtrPorts (u4Instance, pSnoopVlanEntry);
    }
    return;
}

/*****************************************************************************
 * Function Name      : SnoopRedSendConsolidatedReportOnRtrPorts
 *                                                                           
 * Description        : This function scans the group entries, get the router 
 *                      port list for the vlan and send the report on roter
 *                      ports based on router port oper verson. 
 *                                                                           
 * Input(s)           : u4Instance - The instance Id.
 *                      pSnoopVlanEntry - Vlan entry
 *                                                                           
 * Output(s)          : None.                                                
 *                                                                           
 * Global Variables                                                          
 * Referred           : None                                                 
 *                                                                           
 * Global Variables                                                          
 * Modified           : None                                                 
 *                                                                           
 * Return Value(s)    : None
 *****************************************************************************/
VOID
SnoopRedSendConsolidatedReportOnRtrPorts (UINT4 u4Instance,
                                          tSnoopVlanEntry * pSnoopVlanEntry)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pV3ReportBuf = NULL;
    tIPvXAddr           NullSrcIpAddr;
    INT4                i4Result = 0;
    UINT4               u4MaxLength = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    UINT1               u1Forward = SNOOP_FALSE;
    UINT1               u1Version = SNOOP_IGS_IGMP_VERSION3;
    UINT1               u1RecordType = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
    SNOOP_MEM_SET (&NullSrcIpAddr, 0, sizeof (tIPvXAddr));

    /* No Group entry present */
    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        return;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                           pRBElem, NULL);

        pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;
        pRBElem = pRBNextElem;

        if (SNOOP_OUTER_VLAN (pSnoopConsGroupEntry->VlanId) !=
            (SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId)))
        {
            continue;
        }
        if (pSnoopConsGroupEntry->GroupIpAddr.u1Afi !=
            pSnoopVlanEntry->u1AddressType)
        {
            continue;
        }

        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V3RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            /* if the forwarding is done based on MAC then construct the
             * SSM Reports for all the Group records with NULL source list
             * Use the misc preallocated buffer and copy the group
             * information to the buffer when the max length reaches just
             * send the buffer out */

            u1RecordType =
                ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                 SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);

            for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
            {
                if (u1RecordType == SNOOP_IS_INCLUDE)
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pSnoopConsGroupEntry->InclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pSnoopConsGroupEntry->ExclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
            }

#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                IgsUtilFillV3SourceInfo (u4Instance, pSnoopConsGroupEntry,
                                         u1RecordType, u2NoSources,
                                         &u4MaxLength);
                if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                {
                    i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                                     u4MaxLength,
                                                     &pV3ReportBuf);
                }
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                MldsUtilFillV2SourceInfo (u4Instance,
                                          pSnoopConsGroupEntry, u2NoSources,
                                          u1RecordType, &u4MaxLength);
                if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                {
                    i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                                      u4MaxLength,
                                                      &pV3ReportBuf);
                }
            }
#endif
            u2NoSources = 0;
            u2GrpRec++;
            u1Forward = SNOOP_FALSE;

            if (((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4) &&
                 (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                || ((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
            {
                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send SSM report on "
                               "to router ports\r\n");
                    return;
                }

                /* Send the report message to all the router ports with 
                 * oper version v3. */

                SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                                        pSnoopVlanEntry->u1AddressType,
                                        SNOOP_INVALID_PORT,
                                        VLAN_FORWARD_SPECIFIC, pV3ReportBuf,
                                        pSnoopVlanEntry->V3RtrPortBitmap,
                                        SNOOP_SSMREPORT_SENT);

                u2GrpRec = 0;
                u1Forward = SNOOP_TRUE;
                u4MaxLength = 0;
                SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
            }

        }

        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V2RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
            if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer <
                SNOOP_IGS_IGMP_VERSION2)
            {
                u1Version = SNOOP_IGS_IGMP_VERSION1;
            }
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                /* Send the response on a single router port */
                i4Result = IgsEncodeQueryRespMsg (u4Instance,
                                                  pSnoopVlanEntry,
                                                  pSnoopConsGroupEntry,
                                                  u1Version,
                                                  pSnoopVlanEntry->
                                                  V2RtrPortBitmap,
                                                  u1RecordType);
                UNUSED_PARAM (i4Result);
                u1Forward = SNOOP_TRUE;
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                i4Result = MldsEncodeQueryRespMsg (u4Instance,
                                                   pSnoopVlanEntry,
                                                   pSnoopConsGroupEntry,
                                                   SNOOP_MLD_VERSION1,
                                                   pSnoopVlanEntry->
                                                   V2RtrPortBitmap,
                                                   u1RecordType);
                UNUSED_PARAM (i4Result);
                u1Forward = SNOOP_TRUE;
            }
#endif
        }

        if (SNOOP_MEM_CMP (pSnoopVlanEntry->V1RtrPortBitmap, gNullPortBitMap,
                           sizeof (tSnoopPortBmp)) != 0)
        {
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                /* Send the response on a single router port */
                i4Result = IgsEncodeQueryRespMsg (u4Instance,
                                                  pSnoopVlanEntry,
                                                  pSnoopConsGroupEntry,
                                                  SNOOP_IGS_IGMP_VERSION1,
                                                  pSnoopVlanEntry->
                                                  V1RtrPortBitmap,
                                                  u1RecordType);
                UNUSED_PARAM (i4Result);
                u1Forward = SNOOP_TRUE;
            }
#endif

        }
    }

    if (u1Forward == SNOOP_TRUE)
    {
        return;
    }

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                         u4MaxLength, &pV3ReportBuf);
        UNUSED_PARAM (i4Result);
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                          u4MaxLength, &pV3ReportBuf);
        UNUSED_PARAM (i4Result);
    }
#endif
    /* Send the report message to all the router ports with 
     * oper version v3. */

    SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                            pSnoopVlanEntry->u1AddressType,
                            SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pV3ReportBuf,
                            pSnoopVlanEntry->V3RtrPortBitmap,
                            SNOOP_SSMREPORT_SENT);

}

#ifdef CLI_WANTED
/*****************************************************************************/
/* Function Name      : SnoopRedShowDebuggingInfo                            */
/*                                                                           */
/* Description        : This function is used to display the debugging info. */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedShowDebuggingInfo (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedShowForwardEntries                           */
/*                                                                           */
/* Description        : This function is used to display synced up multicast */
/*                      forwarding table information.                        */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handler                              */
/*                      Vlan      - Vlan Identifier                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedShowForwardEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                            tSnoopTag VlanId, UINT1 u1AddressType)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1AddressType);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedShowVlanRouterPorts                          */
/*                                                                           */
/* Description        : This function is used to display synced up Vlan      */
/*                      router ports.                                        */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handler                              */
/*                      Vlan      - Vlan Identifier                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedShowVlanRouterPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                             tSnoopTag VlanId, UINT1 u1AddressType,
                             INT4 i4PrintRtrPortInfo)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1AddressType);
    UNUSED_PARAM (i4PrintRtrPortInfo);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedShowVlanInfo                                 */
/*                                                                           */
/* Description        : This function is used to display synced up Vlan      */
/*                      Snoop information                                    */
/*                                                                           */
/* Input(s)           : CliHandle - Cli Handler                              */
/*                      Vlan      - Vlan Identifier                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS/CLI_FAILURE                              */
/*****************************************************************************/
INT4
SnoopRedShowVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                      tSnoopTag VlanId, UINT1 u1AddressType)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u1AddressType);
    return CLI_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : SnoopRedTrigHigherLayer                              */
/*                                                                           */
/* Description        : This function sends the trigger to next higher layer */
/*                      (LLDP). If no higher layers are defined, then it     */
/*                      sends L2_SYNC_UP_COMPLETED event to RM.              */
/*                                                                           */
/* Input(s)           : u1TrigType - Trigger Type(L2_COMPLETE_SYNC_UP)       */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SnoopRedTrigHigherLayer (UINT1 u1TrigType)
{
#ifdef LLDP_WANTED
    SnoopLldpRedRcvPktFromRm (u1TrigType, NULL, 0);
#else
    UNUSED_PARAM (u1TrigType);
    if (SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE)
    {
        /* Send an event to RM to notify that sync up is completed */
        if (SnoopRmSendEventToRmTask (L2_SYNC_UP_COMPLETED) != RM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_RED_NAME, "SNOOP Node send event "
                           "L2_SYNC_UP_COMPLETED to RM failed\n");
        }
    }
#endif
}

/************************************************************************/
/* Function Name      : SnoopRedHwUpdateMcastSync                       */
/*                                                                      */
/* Description        : This function sends the multicast sync-up       */
/*                      message from  Active node                       */
/*                                                                      */
/* Input(s)           : u4ContextId - Context Identifier.               */
/*                      VLAN Id  - VLAN Identifier.                     */
/*                      u4GrpAddr - Group Address.                      */
/*                      u4SrcAddr - Source Address.                     */
/*                      i1RedSyncState                                  */
/*                                                                      */
/*                        SNOOP_RED_SYNC_HW_NOT_UPDATED     0           */
/*                       SNOOP_RED_SYNC_HW_UPDATED          1           */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Returns         : SNOOP_SUCCESS, if successful                      */
/*                    SNOOP_FAILURE, otherwise                          */
/************************************************************************/

INT4
SnoopRedHwUpdateMcastSync (UINT4 u4Instance, tSnoopVlanId VlanId,
                           UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                           tPortList PortList, tPortList UnTagPortList,
                           UINT1 i1RedSyncState)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2MsgAllocSize = 0;
    UINT4               u4OffSet = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    if (gu1SnoopRedAuditInProgress == SNOOP_TRUE)
    {
        /* Pending entries need not be synced when Audit is in progress */
        return SNOOP_SUCCESS;
    }
    if (SNOOP_NODE_STATUS () == SNOOP_ACTIVE_NODE)
    {
        ProtoEvt.u4AppId = RM_SNOOP_APP_ID;
        ProtoEvt.u4Event = RM_PROTOCOL_SEND_EVENT;
        u2MsgAllocSize = SNOOP_RED_MCAST_CACHE_INFO_SIZE;

        if ((pMsg = RM_ALLOC_TX_BUF (u2MsgAllocSize)) == NULL)
        {
            ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);

            return SNOOP_FAILURE;
        }

        SNOOP_RM_PUT_1_BYTE (pMsg, &u4OffSet, SNOOP_RED_SYNC_MCAST_CACHE_INFO);
        SNOOP_RM_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgAllocSize);
        SNOOP_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4Instance);
        SNOOP_RM_PUT_2_BYTE (pMsg, &u4OffSet, VlanId);
        SNOOP_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4GrpAddr);
        SNOOP_RM_PUT_4_BYTE (pMsg, &u4OffSet, u4SrcAddr);

        SNOOP_RM_PUT_N_BYTE (pMsg, &(PortList),
                             &u4OffSet, sizeof (tLocalPortList));
        SNOOP_RM_PUT_N_BYTE (pMsg, &(UnTagPortList),
                             &u4OffSet, sizeof (tLocalPortList));

        SNOOP_RM_PUT_1_BYTE (pMsg, &u4OffSet, i1RedSyncState);

        /* Call the API provided by RM to send the data to RM */
        if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2MsgAllocSize,
                                       RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
            == RM_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_RED_NAME,
                           "Enqueuing Snoop Disable Sync fails \r\n");
            /* pMsg is freed only in failure case. RM will free the buf
             * in success case. */
            RM_FREE (pMsg);
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            SnoopRmHandleProtocolEvent (&ProtoEvt);
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/************************************************************************/
/* Function Name      : SnoopRedProcessMcastCacheInfo                   */
/*                                                                      */
/* Description        : This function processes the multicast sync-up   */
/*                      received from active node and parses the        */
/*                      message received                                */
/*                                                                      */
/* Input(s)           : tRmMsg - RM message.                            */
/*                      u4ContextId - Context Identifier                */
/*                      *pu4OffSet - u4Offset                           */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Returns         : SNOOP_SUCCESS, if successful                      */
/*                    SNOOP_FAILURE, otherwise                          */
/************************************************************************/

VOID
SnoopRedProcessMcastCacheInfo (tRmMsg * pMsg, UINT4 u4ContextId,
                               UINT4 *pu4OffSet)
{
    tSnoopRedMcastCacheNode SnoopRedCacheNode;

    MEMSET (&SnoopRedCacheNode, 0, sizeof (tSnoopRedMcastCacheNode));

    /* Extract the instance id */
    SNOOP_RM_GET_4_BYTE (pMsg, pu4OffSet, SnoopRedCacheNode.u4ContextId);

    SNOOP_RM_GET_2_BYTE (pMsg, pu4OffSet, SnoopRedCacheNode.VlanId);
    SNOOP_RM_GET_4_BYTE (pMsg, pu4OffSet, SnoopRedCacheNode.u4GrpAddr);
    SNOOP_RM_GET_4_BYTE (pMsg, pu4OffSet, SnoopRedCacheNode.u4SrcAddr);
    SNOOP_RM_GET_N_BYTE (pMsg, &(SnoopRedCacheNode.PortList),
                         pu4OffSet, sizeof (tLocalPortList));
    SNOOP_RM_GET_N_BYTE (pMsg, &(SnoopRedCacheNode.UnTagPortList),
                         pu4OffSet, sizeof (tLocalPortList));
    SNOOP_RM_GET_1_BYTE (pMsg, pu4OffSet, SnoopRedCacheNode.i1RedSyncState);

    SnoopRedCacheNode.u4ContextId = u4ContextId;

    if (SNOOP_RED_BULK_SYNC_INPROGRESS == SNOOP_FALSE)
    {
        if (SNOOP_FAILURE == SnoopRedProcessMcastSyncInfo (&SnoopRedCacheNode))
        {
            return;
        }
    }

    return;

}

/************************************************************************/
/* Function Name      : SnoopRedProcessMcastSyncInfo                    */
/*                                                                      */
/* Description        : This function processes the multicast sync-up   */
/*                      received from active node and does the          */
/*                      necessary processing in standby                 */
/*                                                                      */
/* Input(s)           : pSnoopRedCacheNode - Red Cache node.            */
/*                                                                      */
/*  Output(s)       : None                                              */
/*                                                                      */
/*  Global Variables Referred :None                                     */
/*                                                                      */
/*  Global variables Modified :None                                     */
/*                                                                      */
/*  Returns         : SNOOP_SUCCESS, if successful                      */
/*                    SNOOP_FAILURE, otherwise                          */
/************************************************************************/

INT4
SnoopRedProcessMcastSyncInfo (tSnoopRedMcastCacheNode * pUpdtSnoopRedCacheNode)
{
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode = NULL;

    pSnoopRedCacheNode =
        SnoopRedGetMcastCacheEntry (pUpdtSnoopRedCacheNode->u4ContextId,
                                    pUpdtSnoopRedCacheNode->VlanId,
                                    pUpdtSnoopRedCacheNode->u4GrpAddr,
                                    pUpdtSnoopRedCacheNode->u4SrcAddr);

    if (NULL != pSnoopRedCacheNode)
    {
        if ((pSnoopRedCacheNode->i1RedSyncState == SNOOP_RED_SYNC_HW_UPDATED) &&
            (pUpdtSnoopRedCacheNode->i1RedSyncState ==
             SNOOP_RED_SYNC_SW_UPDATED))
        {
            RBTreeRem (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                       (tRBElem *) pSnoopRedCacheNode);
            SNOOP_RED_GRP_FREE_MEMBLK (pUpdtSnoopRedCacheNode->u4ContextId,
                                       pSnoopRedCacheNode);
        }

        else if ((pSnoopRedCacheNode->i1RedSyncState ==
                  SNOOP_RED_SYNC_SW_UPDATED)
                 && (pUpdtSnoopRedCacheNode->i1RedSyncState ==
                     SNOOP_RED_SYNC_HW_UPDATED))
        {
            RBTreeRem (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                       (tRBElem *) pSnoopRedCacheNode);
            SNOOP_RED_GRP_FREE_MEMBLK (pUpdtSnoopRedCacheNode->u4ContextId,
                                       pSnoopRedCacheNode);
        }

        else
        {
            pSnoopRedCacheNode->i1RedSyncState =
                pUpdtSnoopRedCacheNode->i1RedSyncState;
        }

    }

    else
    {
        SnoopRedAddMcastCacheEntry (pUpdtSnoopRedCacheNode);
    }

    return SNOOP_SUCCESS;

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : SnoopRedGetMcastCacheEntry                              */
/*                                                                            */
/*  Description     : This function will be invoked to fetch an entry from    */
/*                    SnoopRedCacheTable.                                     */
/*                                                                            */
/*  Input(s)        : u4ContextId- Context Identifier.                        */
/*                    VLAN Id    - Vlan Identifier                            */
/*                    u4GrpAddr - Group Identifier                            */
/*                    u4SrcAddr- Source Identifier                            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : SNOOP_SUCCESS, if successful                            */
/*                    SNOOP_FAILURE, otherwise                                */
/******************************************************************************/

tSnoopRedMcastCacheNode *
SnoopRedGetMcastCacheEntry (UINT4 u4ContextId,
                            tVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr)
{
    UNUSED_PARAM (u4ContextId);
    tSnoopRedMcastCacheNode SnoopRedCacheNode;
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode = NULL;

    MEMSET (&SnoopRedCacheNode, 0, sizeof (tSnoopRedMcastCacheNode));

    SnoopRedCacheNode.VlanId = VlanId;
    SnoopRedCacheNode.u4GrpAddr = u4GrpAddr;
    SnoopRedCacheNode.u4SrcAddr = u4SrcAddr;

    pSnoopRedCacheNode = (tSnoopRedMcastCacheNode *)
        RBTreeGet (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                   (tRBElem *) & SnoopRedCacheNode);

    return pSnoopRedCacheNode;
}

/*****************************************************************************/
/* Function Name      : SnoopRedDeInitGlobalInfo                             */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
VOID
SnoopRedDeInitGlobalInfo (VOID)
{
    if (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable != NULL)
    {
        RBTreeDestroy (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                       (tRBKeyFreeFn) SnoopRedMcastCacheFreeNodes, 0);
        gSnoopRedGlobalInfo.SnoopRedMcastCacheTable = NULL;
    }

    return;

}

/*****************************************************************************/
/* Function Name      : SnoopRedInitGlobalInfo                               */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopRedInitGlobalInfo (VOID)
{
    gSnoopRedGlobalInfo.SnoopRedMcastCacheTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopRedMcastCacheNode, RbNode),
                              SnoopUtilRBTreeRedMcastEntryCmp);

    if (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RED | SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                       SNOOP_RED_NAME,
                       "RB Tree creation for Group Entry failed\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : SnoopRedAddMcastCacheEntry                              */
/*                                                                            */
/*  Description     : This function will be invoked to add a RedCacheEntry to */
/*                    SnoopRedCacheTable.                                     */
/*                                                                            */
/*  Input(s)        : pointer to the node of structure tVlanSnoopCacheNode    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : SNOOP_SUCCESS, if successful                            */
/*                    SNOOP_FAILURE, otherwise                                */
/******************************************************************************/
INT4
SnoopRedAddMcastCacheEntry (tSnoopRedMcastCacheNode * pUpdtSnoopRedCacheNode)
{
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode = NULL;

    if (SNOOP_RED_GRP_ALLOC_MEMBLK
        (pUpdtSnoopRedCacheNode->u4ContextId, pSnoopRedCacheNode) != NULL)
    {
        MEMCPY (pSnoopRedCacheNode, pUpdtSnoopRedCacheNode,
                sizeof (tSnoopRedMcastCacheNode));

        if (RB_SUCCESS !=
            RBTreeAdd (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                       (tRBElem *) (pSnoopRedCacheNode)))
        {
            SNOOP_RED_GRP_FREE_MEMBLK (pUpdtSnoopRedCacheNode->u4ContextId,
                                       pSnoopRedCacheNode);
            return SNOOP_FAILURE;
        }

        pSnoopRedCacheNode->u4ContextId = pUpdtSnoopRedCacheNode->u4ContextId;
        pSnoopRedCacheNode->i1RedSyncState =
            pUpdtSnoopRedCacheNode->i1RedSyncState;
    }
    else
    {
        /*Memory allocation failed for Static Multicast Table.
         * Sync-up failure has reached the maximum limit.
         * Allowed further could lead to configuration loss during audit.
         * Hence, Reverting to full audit mode from optimal audit mechanism.
         */
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "Memory allocation for cache entry failed \r\n");
    }
    return SNOOP_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : SnoopRedSyncPendingMcastEntries                         */
/*                                                                            */
/*  Description     : This function synchronizes the unicast table            */
/*                    between the hardware and the software.                  */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
SnoopRedSyncPendingMcastEntries (UINT4 u4Context)
{
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBCacheElem = NULL;
    tRBElem            *pRBCacheNextElem = NULL;
    tSnoopRedMcastCacheNode SnoopRedCacheNode;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tIPvXAddr           SrcAddr;
    tIPvXAddr           GrpAddr;
    tSnoopTag           SnoopTag;

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SnoopRedCacheNode, 0, sizeof (tSnoopRedMcastCacheNode));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    SNOOP_MEM_SET (&SnoopTag, 0, sizeof (tSnoopTag));

    SnoopRedCacheNode.u4ContextId = u4Context;

    pRBCacheElem = RBTreeGetNext (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                                  &SnoopRedCacheNode, NULL);

    while (NULL != pRBCacheElem)
    {
        pSnoopRedCacheNode = (tSnoopRedMcastCacheNode *) pRBCacheElem;

        pRBCacheNextElem =
            RBTreeGetNext (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                           pRBCacheElem, NULL);

        if (pSnoopRedCacheNode->u4ContextId != u4Context)
        {
            break;
        }
        else
        {
            /* if entry present in S/W add it to H/W */
            IPVX_ADDR_INIT_IPV4 (GrpAddr, SNOOP_ADDR_TYPE_IPV4,
                                 (UINT1 *) &pSnoopRedCacheNode->u4GrpAddr);

            IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                                 (UINT1 *) &pSnoopRedCacheNode->u4SrcAddr);

            IpGrpFwdEntry.u1AddressType = SNOOP_ADDR_TYPE_IPV4;

            IpGrpFwdEntry.VlanId[0] = pSnoopRedCacheNode->VlanId;

            SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                           (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

            SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                           (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));

            pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Context)->
                                 IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry);

            if (pRBElem == NULL)
            {
                /* Remove the entry from hardware since the IPMC entry is not available
                 * in software after consolidation post switchover
                 */
#ifdef NPAPI_WANTED
                if (IgsFsMiNpUpdateIpmcFwdEntries
                    (u4Context, pSnoopRedCacheNode->VlanId,
                     pSnoopRedCacheNode->u4GrpAddr,
                     pSnoopRedCacheNode->u4SrcAddr, IpGrpFwdEntry.PortBitmap,
                     pSnoopRedCacheNode->UnTagPortList,
                     SNOOP_HW_DELETE_ENTRY) == FNP_FAILURE)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_RED_DBG,
                                    SNOOP_RED_DBG,
                                    "Failed to update IPMC table entry for VLAN %d \r\n",
                                    pSnoopRedCacheNode->VlanId);

                }

#endif
            }
        }
        RBTreeRem (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                   (tRBElem *) pSnoopRedCacheNode);
        SNOOP_RED_GRP_FREE_MEMBLK (pSnoopRedCacheNode->u4ContextId,
                                   pSnoopRedCacheNode);
        pRBCacheElem = pRBCacheNextElem;
    }                            /* End of while */

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopActiveRedGrpDeleteSync                          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4InstId     - Instance Id                           */
/*                      SnoopGrpIpAddr - Multicast Group IP Address          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopActiveRedGrpDeleteSync (UINT4 u4Instance,
                             tIPvXAddr SrcIpAddr,
                             tSnoopGroupEntry * pSnoopGroupEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2DataLen = 0;
    UINT1               u1MsgType = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */

    if ((RmGetNodeState () != RM_ACTIVE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        return (SNOOP_FAILURE);
    }

    /* Allocate memory for dynamic sync of Multicast forwarding
     * information sync to the standby through RM.
     * This memory will be freed by RM after Txmitting.
     */
    u2DataLen = (UINT2) SNOOP_RED_IP_INFO_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_RED_NAME,
                       "Buffer creation failure for forming dynamic sync of \
                 Multicast Forwarding Info fails\r\n");
        return (SNOOP_FAILURE);
    }

    /* Form a dynamic sync up message for updation of Multicast forwarding table
     * information with the following format :-

     *        <---1 Bytes---><---2---><--1--><-2-><-----20----><--N--><--N-->
     ********************************************************************************
     *        *             *Length  *      *     * Group IP * Source * Fwd  * V1   *
     * RM Hdr * IGS_RED_IP *1+2+4+N * Inst *Vlan  * Addr     * Addr   * Port * Port *
     *        * _INFO_MSG   *Bytes   *  Id  * Id  *          *        * List * List *
     ********************************************************************************

     * The RM Hdr shall be included by RM.
     */

    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_IP_INFO_MSG */
        u1MsgType = IGS_RED_IP_INFO_MSG;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);
    /* Fill the Frwding entry information length */
    u2MsgLen = SNOOP_RED_IP_INFO_MSG_LEN;
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset, u2MsgLen);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4Instance);

    /* Fill the Outer-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId));

    /* Fill the Inner-VLAN Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId));
    /* Fill the Snoop HW Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, 0);

    /* Fill the Multicast Source IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, &(SrcIpAddr), &u4Offset, sizeof (tIPvXAddr));

    /* Fill the Multicast Group IP Address */
    SNOOP_RM_PUT_N_BYTE (pMsg, &(pSnoopGroupEntry->GroupIpAddr),
                         &u4Offset, sizeof (tIPvXAddr));

    /* Fill the updated forwarding port list */
    SNOOP_RM_PUT_N_BYTE (pMsg, gNullPortBitMap, &u4Offset,
                         SNOOP_PORT_LIST_SIZE);

    /* Call the API provided by RM to send the formed sync up
     * message to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        != RM_SUCCESS)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Frwd Info fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopRedUpdateStartUpQryCount                        */
/*                                                                           */
/* Description        : This function stores the start up query count        */
/*                       information present in the sync message received    */
/*                      from SNOOP  protocol in active node.                 */
/*                                                                           */
/* Input(s)           : u4Instance   - Instance Id                           */
/*                      VlanId       - Vlan Identifier                       */
/*                      u1StartUpQryCount - StartUp query count              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopRedUpdateStartUpQryCount (UINT4 u4Instance, tSnoopTag VlanId,
                               UINT1 u1StartUpQryCount)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SNOOP_SNOOPING_STATUS (gu4SnoopTrcInstId, u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Snooping module disabled.Discarding the sync "
                       "up msg\r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                            SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                            "Unable to get the VLAN entry for %d. Discarding the sync "
                            "up msg\r\n", SNOOP_OUTER_VLAN (VlanId));
        return SNOOP_FAILURE;
    }

    pSnoopVlanEntry->u1StartUpQCount = u1StartUpQryCount;
    SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                        "Applying startup query count sync up message for VLAN : %d count : %d\r\n",
                        SNOOP_OUTER_VLAN (VlanId),
                        pSnoopVlanEntry->u1StartUpQCount);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopRedSyncStartUpQryCount                          */
/*                                                                           */
/* Description        : This function sends the trigger to standby node,     */
/*                      whenever the startup query count changes             */
/*                                                                           */
/* Input(s)           : u4InstId - Instance Id                               */
/*                      VlanId   - Vlan Id                                   */
/*                      u1AddressType - specifies SNOOP/MLDS                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopRedSyncStartUpQryCount (UINT4 u4InstId, tSnoopVlanEntry * pSnoopVlanEntry)
{
    tRmMsg             *pMsg = NULL;
    UINT2               u2DataLen = 0;
    UINT4               u4Offset = SNOOP_RM_OFFSET;
    UINT1               u1MsgType = 0;
    UINT1               u1AddressType = pSnoopVlanEntry->u1AddressType;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstId;
    gu4SnoopDbgInstId = u4InstId;

    /* Sync up messages must be sent only by active node.
     * If no standby nodes are present, no need to send sync up messages.
     * Dynamic sync up messages should not be sent even before bulk
     * updates are completed
     */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE) ||
        (SNOOP_IS_STANDBY_UP () == SNOOP_FALSE))
    {
        /* Standby node, hence don't send sync up messages */
        return SNOOP_SUCCESS;
    }

    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_RED_TRC,
                    "Sync the startup query count change to standby node%d \r\n",
                    u4InstId);

    /* Allocate memory for the trigger to be send to standby through RM.
     * This memory will be freed by RM after Txmitting
     */

    u2DataLen = (UINT2) SNOOP_RED_STARTUP_QRY_MSG_LEN;

    if ((pMsg = RM_ALLOC_TX_BUF (u2DataLen)) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Buffer creation failure for Snoop Protocol Disable "
                       "Sync fails\r\n");
        return (SNOOP_FAILURE);
    }
    /* Form a trigger to flush all the previously synced messages.

     *        <--------1 Byte-----------><--1--><---2--->
     *****************************************************************************
     *        *                                *      *         *                *
     * RM Hdr * IGS_RED_STARTUP_QRY_COUNT_SYNC * Inst * Vlan Id   StartupQryCount*
     *        *                                *  Id  *         *                *
     *****************************************************************************

     * The RM Hdr shall be included by RM.
     */
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Fill the type of Event as IGS_RED_DEL_VLAN_SYNC */
        u1MsgType = IGS_RED_STARTUP_QRY_COUNT_SYNC;
    }

    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, u1MsgType);

    /* Fill the Instance Id */
    SNOOP_RM_PUT_4_BYTE (pMsg, &u4Offset, u4InstId);

    /* Fill the Outer-Vlan Identifier */
    SNOOP_RM_PUT_2_BYTE (pMsg, &u4Offset,
                         SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

    /* Fill the Inner-Vlan Identifier */
    SNOOP_RM_PUT_1_BYTE (pMsg, &u4Offset, pSnoopVlanEntry->u1StartUpQCount);

    /* Call the API provided by RM to send the data to RM */
    if (SnoopRmEnqMsgToRmFromAppl (pMsg, u2DataLen,
                                   RM_SNOOP_APP_ID, RM_SNOOP_APP_ID)
        == RM_FAILURE)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_RED |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_RED_NAME,
                       "Enqueuing Snoop Disable Sync fails \r\n");
        /* pMsg is freed only in failure case. RM will free the buf
         * in success case. */
        RM_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SnoopRedMcastCacheFreeNodes                                */
/*                                                                           */
/* Description  : Frees Snooping redundancy RBTree Cache nodes               */
/*                                                                           */
/* Input        : pRBElem- Pointer to the RBTree CAche nodes                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS                                               */
/*****************************************************************************/
PUBLIC INT4
SnoopRedMcastCacheFreeNodes (tRBElem * pRBElem, UINT4 u4Arg)
{
    tSnoopRedMcastCacheNode *pSnoopRedCacheNode =
        (tSnoopRedMcastCacheNode *) pRBElem;

    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        RBTreeRem (gSnoopRedGlobalInfo.SnoopRedMcastCacheTable,
                   (tRBElem *) pSnoopRedCacheNode);
        SNOOP_RED_GRP_FREE_MEMBLK (pSnoopRedCacheNode->u4ContextId,
                                   pSnoopRedCacheNode);

    }
    return OSIX_SUCCESS;
}
