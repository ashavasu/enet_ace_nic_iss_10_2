/* $Id: snpvlan.c,v 1.94 2018/01/22 12:42:51 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpvlan.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : snooping vlan module                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to the VLAN*/
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006            Initial Creation                       */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

extern UINT4        FsSnoopVlanStartupQueryInterval[12];
/*****************************************************************************/
/* Function Name      : SnoopVlanCreateVlanEntry                             */
/*                                                                           */
/* Description        : This routine creates a VLAN entry for the  VLAN ID   */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      VlanId       - VLAN Identifier                       */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : pSnoopVlanEntry - VLAN Entry                         */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopVlanCreateVlanEntry (UINT4 u4Instance,
                          tSnoopTag VlanId,
                          UINT1 u1AddressType,
                          tSnoopVlanEntry ** ppRetSnoopVlanEntry)
{
    tSnoopVlanStatsEntry *pSnoopVlanStatsEntry = NULL;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    UINT4               u4Status = 0;
    UINT1               u1Version = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (SnoopMiIsVlanActive (u4Instance, VlanId) == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "VLAN creation failed - VLAN not active\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_VLAN_ENTRY_ALLOC_MEMBLK (u4Instance, pSnoopVlanEntry) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Snoop VLAN entry failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for pool id %d \r\n",
                               SNOOP_VLAN_ENTRY_POOL_ID (u4Instance));
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    /* Set the default values for the structure members */
    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId) = SNOOP_OUTER_VLAN (VlanId);
    pSnoopVlanEntry->u1AddressType = u1AddressType;

    /* Add Node */
    u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                          (tRBElem *) pSnoopVlanEntry);

    if (u4Status == RB_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RBTree Addition Failed for Snoop VLAN Entry \r\n");
        SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, pSnoopVlanEntry);
        return SNOOP_FAILURE;
    }

    if (pSnoopVlanEntry->pSnoopVlanStatsEntry == NULL)
    {
        /* Allocate memory block for the Stats Entry */
        if (SNOOP_VLAN_STATS_ALLOC_MEMBLK (u4Instance,
                                           pSnoopVlanStatsEntry) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed for Snoop Vlan Stats Entry\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                          (tRBElem *) pSnoopVlanEntry);
            SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, pSnoopVlanEntry);
            return SNOOP_FAILURE;
        }
    }

    SNOOP_MEM_SET (pSnoopVlanStatsEntry, 0, sizeof (tSnoopVlanStatsEntry));

    pSnoopVlanEntry->pSnoopVlanStatsEntry = pSnoopVlanStatsEntry;

    /* Allocate memory block for the Config Entry */
    if (SNOOP_VLAN_CFG_ALLOC_MEMBLK (u4Instance, pSnoopVlanCfgEntry) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation failed for Snoop Vlan"
                       " configuration Entry\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "pool id\r\n",
                               SNOOP_VLAN_CFG_POOL_ID (u4Instance));
        SNOOP_VLAN_STATS_FREE_MEMBLK (u4Instance, pSnoopVlanStatsEntry);
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                      (tRBElem *) pSnoopVlanEntry);
        SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, pSnoopVlanEntry);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopVlanCfgEntry, 0, sizeof (tSnoopVlanCfgEntry));

    pSnoopVlanEntry->pSnoopVlanCfgEntry = pSnoopVlanCfgEntry;

    SNOOP_SLL_INIT (&pSnoopVlanEntry->RtrPortList);
    SNOOP_MEM_SET (pSnoopVlanEntry->RtrPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopVlanEntry->QuerierRtrPortBitmap, 0,
                   SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopVlanEntry->StaticRtrPortBitmap, 0,
                   SNOOP_PORT_LIST_SIZE);

    /* Set default values for the VLAN table */
    pSnoopVlanEntry->u2QueryInterval = SNOOP_DEF_QUERY_INTERVAL;
    pSnoopVlanEntry->u2StartUpQInterval = SNOOP_DEF_STARTUP_QINTERVAL;
    pSnoopVlanEntry->u2OtherQPresentInt = SNOOP_OTHER_QUERIER_PRESENT_INTERVAL;
    pSnoopVlanEntry->u1SnoopStatus = SNOOP_ENABLE;

    pSnoopVlanEntry->u1StartUpQCount = 0;
    pSnoopVlanEntry->u1MaxStartUpQCount = SNOOP_DEF_STARTUP_QRY_COUNT;

    if (SNOOP_PROXY_STATUS (u4Instance, u1AddressType - 1) == SNOOP_ENABLE)
    {
        pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;
    }
    else
    {
        pSnoopVlanEntry->u1Querier = SNOOP_NON_QUERIER;
    }

    /*Set the default value for Config Oper version. */
    u1Version =
        (u1AddressType ==
         SNOOP_ADDR_TYPE_IPV4 ? SNOOP_IGS_IGMP_VERSION3 : SNOOP_MLD_VERSION2);
    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer = u1Version;
    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier = SNOOP_NON_QUERIER;
    pSnoopVlanEntry->pSnoopVlanCfgEntry->u2ConfigQueryInt =
        SNOOP_DEF_QUERY_INTERVAL;
    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigFastLeave = SNOOP_DISABLE;
    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigSnoopStatus = SNOOP_ENABLE;
    pSnoopVlanEntry->u1FastLeave = SNOOP_DISABLE;
    pSnoopVlanEntry->u1RowStatus = SNOOP_ACTIVE;
    pSnoopVlanEntry->u4RobustnessValue = SNOOP_ROBUST_MIN_VALUE;

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        pSnoopVlanEntry->u2MaxRespCode = SNOOP_IGS_DEF_MAX_RESP_CODE;
    }
#endif
#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        pSnoopVlanEntry->u2MaxRespCode = SNOOP_MLDS_DEF_MAX_RESP_CODE;
    }
#endif

    pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                   SnoopInfo[u1AddressType - 1]);
    if (pSnoopInfo != NULL)
    {
        SNOOP_SET_PORT_PURGE_INTERVAL (pSnoopVlanEntry,
                                       pSnoopVlanEntry->u4RobustnessValue,
                                       u1Version);
    }

    *ppRetSnoopVlanEntry = pSnoopVlanEntry;
    if (pSnoopInfo != NULL)
    {
        if (pSnoopInfo->u1SnoopStatus == SNOOP_ENABLE)
        {
            SnoopUtilEnableSnoopInVlan (u4Instance, pSnoopVlanEntry);
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanGetVlanEntry                                */
/*                                                                           */
/* Description        : This routine returns the SNOOP VLAN entry for the    */
/*                      VLAN ID                                              */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                    : VlanId       - VLAN Identifier                       */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : pSnoopVlanEntry - VLAN Entry                         */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopVlanGetVlanEntry (UINT4 u4Instance, tSnoopTag VlanId,
                       UINT1 u1AddressType, tSnoopVlanEntry ** ppSnoopVlanEntry)
{
    tSnoopVlanEntry     SnoopVlanEntry;
    tRBElem            *pRBElem = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&SnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    SNOOP_OUTER_VLAN (SnoopVlanEntry.VlanId) = SNOOP_OUTER_VLAN (VlanId);
    SnoopVlanEntry.u1AddressType = u1AddressType;

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                         (tRBElem *) & SnoopVlanEntry);

    if (pRBElem == NULL)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_DBG,
                            "Vlan entry not found ,in Snoop Vlan Entry table for,"
                            "VLAN %d\r\n", SNOOP_OUTER_VLAN (VlanId));

        return SNOOP_FAILURE;
    }
    else
    {
        *ppSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanDeleteVlanEntry                             */
/*                                                                           */
/* Description        : This function deletes VLAN information related for   */
/*                      this VLAN                                            */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      Vlan Id    - Vlan Identifier                         */
/*                      u1AddressType - Indicates Address family v4/v6       */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SnoopVlanDeleteVlanEntry (UINT4 u4Instance, tSnoopTag VlanId,
                          UINT1 u1AddressType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tMVlanMapping      *pMVlanMapping = NULL;
    tPortList          *pTmpPortList = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               u1AddressType, &pSnoopVlanEntry)
        != SNOOP_SUCCESS)
    {

        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_NAME,
                            "VLAN entry not found ,in Snoop Vlan"
                            "Entry Table, for VLAN %d\r\n",
                            SNOOP_OUTER_VLAN (VlanId));
        return SNOOP_FAILURE;
    }

    /* Delete all the learnt router port list in the VLAN */
    if (SNOOP_SLL_COUNT (&pSnoopVlanEntry->RtrPortList) != 0)
    {
        SnoopVlanDeleteRtrPortList (u4Instance, pSnoopVlanEntry, SNOOP_TRUE);
    }

    SNOOP_MEM_SET (pSnoopVlanEntry->RtrPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopVlanEntry->QuerierRtrPortBitmap, 0,
                   SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopVlanEntry->StaticRtrPortBitmap, 0,
                   SNOOP_PORT_LIST_SIZE);

    /* Stop the query timer if it is running */
    if (pSnoopVlanEntry->QueryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
    }

    /* Stop the Other Querier Present timer if it is running */
    if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
    }

    if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
    {
        SNOOP_VLAN_STATS_FREE_MEMBLK (u4Instance,
                                      pSnoopVlanEntry->pSnoopVlanStatsEntry);
        pSnoopVlanEntry->pSnoopVlanStatsEntry = NULL;
    }
    if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
    {
        pTmpPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pTmpPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Error in allocating memory for pTmpPortList\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));
        SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                       (pSnoopVlanEntry->VlanId),
                                       (tPortList *) (*pTmpPortList));
        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
    }
    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u4ConfigProfileId != 0)
        {
            /* Update the ProfileID -> MVlan mapping SLL */
            SNOOP_SLL_SCAN (&(SNOOP_INSTANCE_INFO (u4Instance)->
                              MVlanMappingList), pMVlanMapping, tMVlanMapping *)
            {
                if (pMVlanMapping->VlanId == SNOOP_OUTER_VLAN (VlanId))
                {
                    if (SnoopTacApiUpdateVlanRefCount
                        (u4Instance, pMVlanMapping->u4ProfileId, u1AddressType,
                         TACM_UNMAP) == SNOOP_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    SNOOP_SLL_DELETE (&(SNOOP_INSTANCE_INFO (u4Instance)->
                                        MVlanMappingList),
                                      &pMVlanMapping->NextMVlanMapEntry);

                    SNOOP_MVLAN_MAPPING_ENTRY_FREE_MEMBLK
                        (u4Instance, pMVlanMapping);

                    break;
                }
            }
        }
        SNOOP_VLAN_CFG_FREE_MEMBLK (u4Instance,
                                    pSnoopVlanEntry->pSnoopVlanCfgEntry);
        pSnoopVlanEntry->pSnoopVlanCfgEntry = NULL;
    }

    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                  pSnoopVlanEntry,
                                  SNOOP_RED_BULK_UPD_VLAN_REMOVE);

    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                  (tRBElem *) pSnoopVlanEntry);

    SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, pSnoopVlanEntry);
    pSnoopVlanEntry = NULL;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanDeleteVlanEntries                           */
/*                                                                           */
/* Description        : This function deletes all the VLAN related           */
/*                      information for a instance                           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopVlanDeleteVlanEntries (UINT4 u4Instance, UINT1 u1AddressType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT1               u1DeleteRtrPort = SNOOP_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE (u4Instance);
    SNOOP_VALIDATE_ADDRESS_TYPE (u1AddressType);

    u4Instance = SNOOP_GET_INSTANCE_ID (u4Instance);
    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
    if (pSnoopInstInfo == NULL)
    {
        return;
    }

    pSnoopInfo = &(pSnoopInstInfo->SnoopInfo[u1AddressType]);
    if (pSnoopInfo == NULL)
    {
        SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_DBG_MGMT, SNOOP_MGMT_DBG,
                        "instance %d for the address type %d is not created\r\n",
                        u4Instance, u1AddressType);
        return;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "No Group entries found for this instance\r\n");
        return;
    }

    /* To clear the static router ports from global rbtree when module 
     * is shut */
    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN_IN_PROGRESS)
    {
        u1DeleteRtrPort = SNOOP_TRUE;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        if (pSnoopVlanEntry->u1AddressType == u1AddressType)
        {
            if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
            {
                /* Need to free the memory allocated for statistics
                 * entry */
                SNOOP_VLAN_STATS_FREE_MEMBLK (u4Instance,
                                              pSnoopVlanEntry->
                                              pSnoopVlanStatsEntry);
                pSnoopVlanEntry->pSnoopVlanStatsEntry = NULL;
            }
            if (pSnoopVlanEntry->QueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
            }
            pSnoopVlanEntry->u1StartUpQCount = 0;
            if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
            }

            /* Delete all the learnt router port list in the VLAN */
            if (SNOOP_SLL_COUNT (&pSnoopVlanEntry->RtrPortList) != 0)
            {
                /* Delete only dynamic router ports */
                SnoopVlanDeleteRtrPortList (u4Instance, pSnoopVlanEntry,
                                            u1DeleteRtrPort);
            }
            /* Reset Router port consolidated bitmap for this VLAN */
            SNOOP_MEM_SET (pSnoopVlanEntry->RtrPortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            /* Reset Igmp Router port bitmap for this VLAN */
            SNOOP_MEM_SET (pSnoopVlanEntry->QuerierRtrPortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);

            /* Check if any static configuration is done for this VLAN
             * If the static configuration is done, then revert the dynamic
             * updated objects based on the static configurations. This will be set
             * to the configured value in SnoopVlanCfgEntry when the snooping is
             * enabled
             */
            pSnoopVlanCfgEntry = pSnoopVlanEntry->pSnoopVlanCfgEntry;

            if (pSnoopVlanCfgEntry != NULL)
            {
                if (SNOOP_SYSTEM_STATUS (u4Instance)
                    == SNOOP_SHUTDOWN_IN_PROGRESS)
                {
                    /* Module shutdown initiated.Need to free the static
                     * configurations done for that vlan.
                     */
                    SNOOP_VLAN_CFG_FREE_MEMBLK (u4Instance,
                                                pSnoopVlanEntry->
                                                pSnoopVlanCfgEntry);
                    pSnoopVlanEntry->pSnoopVlanCfgEntry = NULL;
                }
                else
                {
                    pSnoopVlanEntry->u2QueryInterval = SNOOP_DEF_QUERY_INTERVAL;

                    SNOOP_SET_PORT_PURGE_INTERVAL (pSnoopVlanEntry,
                                                   pSnoopVlanEntry->
                                                   u4RobustnessValue,
                                                   pSnoopVlanEntry->
                                                   u1OperVersion);

                    SNOOP_MEM_SET (pSnoopVlanEntry->StaticRtrPortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                    pSnoopVlanEntry->u1FastLeave = SNOOP_DISABLE;

                    if (SNOOP_PROXY_STATUS (u4Instance, u1AddressType - 1) ==
                        SNOOP_ENABLE)
                    {
                        pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;
                    }
                    else
                    {
                        pSnoopVlanEntry->u1Querier = SNOOP_NON_QUERIER;
                    }

                    /* If the Static Router port is configured dont delete
                     * the entry continue with the next */
                    if (SNOOP_MEM_CMP (pSnoopVlanCfgEntry->CfgStaticRtrPortBmp,
                                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE)
                        != 0)
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }

                    SNOOP_MEM_SET (pSnoopVlanEntry->StaticRtrPortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                    /* If the switch is configured as querier, stop the query
                     * timer dont delete the entry continue with the next */
                    if (pSnoopVlanCfgEntry->u1ConfigQuerier == SNOOP_QUERIER)
                    {
                        pSnoopVlanEntry->u1SnoopStatus = SNOOP_DISABLE;
                        pRBElem = pRBNextElem;
                        continue;
                    }

                    /* If fast leave is enabled or any operating version is 
                     * configured or query interval or Multicast VLAN Profile
                     * is configured or SNOOP status is configured manually 
                     * then do not delete the entry continue with the next */
                    if ((pSnoopVlanCfgEntry->u1ConfigFastLeave) ||
                        (pSnoopVlanCfgEntry->u1ConfigOperVer) ||
                        (pSnoopVlanCfgEntry->u1ConfigSnoopStatus) ||
                        (pSnoopVlanCfgEntry->u4ConfigProfileId) ||
                        (pSnoopVlanCfgEntry->u2ConfigQueryInt))
                    {
                        pSnoopVlanEntry->u1SnoopStatus = SNOOP_DISABLE;
                        pRBElem = pRBNextElem;
                        continue;
                    }
                }
            }

            /* Delete the entry and free the mem block */
            SNOOP_MEM_SET (pSnoopVlanEntry->StaticRtrPortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);

            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                          pSnoopVlanEntry,
                                          SNOOP_RED_BULK_UPD_VLAN_REMOVE);

            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                          (tRBElem *) pSnoopVlanEntry);

            SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, pSnoopVlanEntry);
            pSnoopVlanEntry = NULL;
        }
        pRBElem = pRBNextElem;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanDeleteRtrPortList                           */
/*                                                                           */
/* Description        : This function deletes all the router port entries    */
/*                      associated with the VLAN                             */
/*                                                                           */
/* Input(s)           : pSnoopVlanEntry - Pointer to VLAN entry              */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopVlanDeleteRtrPortList (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry,
                            UINT1 u1DelStaticRtrPort)
{
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tSnoopRtrPortEntry *pTempSnoopRtrPortEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4Port;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DYN_SLL_SCAN (&(pSnoopVlanEntry->RtrPortList),
                        pSnoopRtrPortEntry, pTempSnoopRtrPortEntry,
                        tSnoopRtrPortEntry *)
    {
        /* Stop the V1 Querier Timer, if it is running for the router port */
        if (pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopRtrPortEntry->V1RtrPortPurgeTimer);
        }

        /* Stop the V2 Querier Timer, if it is running for the router port */
        if (pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopRtrPortEntry->V2RtrPortPurgeTimer);
        }

        /* Stop the V3 Querier Timer, if it is running for the router port */
        if (pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopRtrPortEntry->V3RtrPortPurgeTimer);
        }
        u4Port = pSnoopRtrPortEntry->u4Port;
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);

        /* Check if the port is learnt from the general query then delete the
         * from the QuerierRtrPortBitmap of the VLAN entry */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->QuerierRtrPortBitmap);
        }
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V3RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V3RtrPortBitmap);
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V2RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V2RtrPortBitmap);
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V1RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V1RtrPortBitmap);
        }
        /* Check if the port is static router port */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap,
                               bResult);
        if ((bResult == OSIX_TRUE))
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->StaticRtrPortBitmap);

            if (u1DelStaticRtrPort == SNOOP_FALSE)
            {
                /* Don't delete the static rtr port, igmp snooping is disabled.
                 * When snooping is enabled then static configurations needs to be restored\
                 */
                continue;
            }
        }

        SNOOP_SLL_DELETE (&pSnoopVlanEntry->RtrPortList,
                          &pSnoopRtrPortEntry->NextRtrPortEntry);
        SnoopRedSyncBulkUpdNextEntry (u4Instance, pSnoopRtrPortEntry,
                                      SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE);
        /* Remove the router port entry from the global RBTree */
        RBTreeRem (gRtrPortTblRBRoot, pSnoopRtrPortEntry);

        SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4Instance, pSnoopRtrPortEntry);
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUpdateLearntRtrPort                         */
/*                                                                           */
/* Description        : This function updates the dynamically learnt VLAN    */
/*                      router ports in a VLAN and also updates the multicast*/
/*                      forward entries with the learnt router ports for the */
/*                      VLAN                                                 */
/*                                                                           */
/* Input(s)           : u4Instance      - Instance number                    */
/*                      pSnoopVlanEntry - Vlan Entry                         */
/*                      u4Port          - Port number                        */
/*                      u1PktType       - SNOOP_IGMP_QUERY/SNOOP_MLD_QUERY   */
/*                      u1Version       - RtrPort Version                    */
/*                      u4QuerierV3QryInt - Rtrport port purge interval      */
/*                                        retrived from the V3 query message.*/
/*                                        This will be used if rtr port oper */
/*                                        version is IGMPv3/MLDv2            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopVlanUpdateLearntRtrPort (UINT4 u4Instance,
                              tSnoopVlanEntry * pSnoopVlanEntry,
                              UINT4 u4Port, UINT1 u1PktType,
                              UINT1 u1Version, UINT4 u4QuerierV3QryInt)
{
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tMacAddr            NullMacAddr;
    tPortList          *pTmpPortList = NULL;
    tSnoopIfPortList   *pSnpPortList = NULL;
    UINT1              *pBitmapVlan = NULL;
    UINT1               u1IcclInterface = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    UINT4               u4PhyPort = 0;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));
    pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pBitmapVlan == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pBitmapVlan\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_CPY (pBitmapVlan, &pSnoopVlanEntry->RtrPortBitmap,
                   SNOOP_PORT_LIST_SIZE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        UtilPlstReleaseLocalPortList (pBitmapVlan);
        return SNOOP_FAILURE;
    }

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u4Port);
        u1IcclInterface = (UINT1) SnoopIcchIsIcclInterface (u4PhyPort);
    }

    /* Check if the  port is present in the static router port bitmap, 
     * if present dont do anything */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap,
                           bResult);

    if (bResult == OSIX_TRUE)
    {
        /* Query is received over a statically learnt router port.
         * Restart the router port purge timer */
        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4Port)
            {
                break;
            }
        }
        if (pRtrPortNode == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME, "pRtrPortNode is empty\r\n");
            UtilPlstReleaseLocalPortList (pBitmapVlan);
            return SNOOP_FAILURE;
        }
        if ((pRtrPortNode->V1RtrPortPurgeTimer.u1TimerType
             == SNOOP_INVALID_TIMER_TYPE) &&
            (pRtrPortNode->V2RtrPortPurgeTimer.u1TimerType
             == SNOOP_INVALID_TIMER_TYPE) &&
            (pRtrPortNode->V3RtrPortPurgeTimer.u1TimerType
             == SNOOP_INVALID_TIMER_TYPE))
        {
            /* Current operating version is either configured or
             * default operating version of the static router port
             * so over ride the operating version with the dynamically
             * learnt operating version
             */
            /* Update the operating version of the router port as configured version */

            if ((u1PktType == SNOOP_IGMP_QUERY)
                || (u1PktType == SNOOP_MLD_QUERY))
            {
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->QuerierRtrPortBitmap);
            }
            SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
            SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);
            /* Send a Router Port Sync Message to Standby Node. */
            SnoopRedActiveSendRtrPortListSync (u4Instance, pSnoopVlanEntry);
            if (u1IcclInterface == SNOOP_FALSE)
            {
                SnoopUtilUpdateRtrPortOperVer (pRtrPortNode, u1Version);
            }

        }
        else if (pRtrPortNode->u1OperVer > u1Version)
        {
            /* update the router port operating version */
            if (u1IcclInterface == SNOOP_FALSE)
            {
                SnoopUtilUpdateRtrPortOperVer (pRtrPortNode, u1Version);
            }
        }

        /* Restart the router port purge timer */
        SnoopUtilStopRtrPortPurgeTmr (pRtrPortNode, pRtrPortNode->u1OperVer);
        SnoopUtilStartRtrPortPurgeTmr (u4Instance, pRtrPortNode, u1Version,
                                       u4QuerierV3QryInt);
        pRtrPortNode->u1IsDynamic = SNOOP_TRUE;
        UtilPlstReleaseLocalPortList (pBitmapVlan);
        return SNOOP_SUCCESS;
    }

    /* Check if the port is already dynamically learnt as a router port 
     * If so just restart the router port purge timer */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap, bResult);

    if (bResult == OSIX_TRUE)
    {
        if ((u1PktType == SNOOP_IGMP_QUERY) || (u1PktType == SNOOP_MLD_QUERY))
        {
            /* If the port is already present in consolidated bitmap due to
             * the query message added just return */
            SNOOP_IS_PORT_PRESENT (u4Port,
                                   pSnoopVlanEntry->QuerierRtrPortBitmap,
                                   bResult);

            if (bResult == OSIX_FALSE)
            {
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->QuerierRtrPortBitmap);
            }
        }

        /* This router port has already dynamically learnt.
         * Restart the router port purge timer */
        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4Port)
            {
                break;
            }
        }

        if (pRtrPortNode != NULL)
        {
            if (pRtrPortNode->u1OperVer > u1Version)
            {
                /* update the router port operating version */
                SnoopUtilUpdateRtrPortOperVer (pRtrPortNode, u1Version);
            }

            /* Restart the router port purge timer */
            SnoopUtilStopRtrPortPurgeTmr (pRtrPortNode,
                                          pRtrPortNode->u1OperVer);
            SnoopUtilStartRtrPortPurgeTmr (u4Instance, pRtrPortNode, u1Version,
                                           u4QuerierV3QryInt);
        }
    }
    else
    {
        /* This is a new router port. We need to start the router port purge
         * timer and update the bitmap. Also this port needs to be added
         * to all the forwarding entries.
         */

        if (SNOOP_VLAN_RTR_PORT_ALLOC_MEMBLK (u4Instance, pRtrPortNode) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed for Snoop Vlan Rtr Port Entry\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for port %d\r\n", u4Port);
            UtilPlstReleaseLocalPortList (pBitmapVlan);
            return SNOOP_FAILURE;
        }

        /* Downstream port is newly learnt as upstream port. Flush out 
         * all the group information learnt through this port if proxy is 
         * enabled */
        if (SnoopGrpDeletePortEntry (u4Instance, pSnoopVlanEntry, u4Port,
                                     NullMacAddr) == SNOOP_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Group entry flushing failed for new router"
                           "port addition \r\n");
            SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4Instance, pRtrPortNode);
            UtilPlstReleaseLocalPortList (pBitmapVlan);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pRtrPortNode, 0, sizeof (tSnoopRtrPortEntry));

        /* Initialize the Rtr Port */
        SnoopVlanInitializeRtrPort (u4Instance, pRtrPortNode, u4Port,
                                    pSnoopVlanEntry);
        if ((u1PktType == SNOOP_IGMP_QUERY) || (u1PktType == SNOOP_MLD_QUERY))
        {
            SNOOP_ADD_TO_PORT_LIST (u4Port,
                                    pSnoopVlanEntry->QuerierRtrPortBitmap);
        }
        /* Sync the router port list and router port version information
         * to stand by */
        SnoopRedActiveSendRtrPortListSync (u4Instance, pSnoopVlanEntry);
        if (pRtrPortNode->u1OperVer != u1Version)
        {
            /* update the router port operating version */
            SnoopUtilUpdateRtrPortOperVer (pRtrPortNode, u1Version);
        }
        /* Start the router port surge timer */
        SnoopUtilStartRtrPortPurgeTmr (u4Instance, pRtrPortNode, u1Version,
                                       u4QuerierV3QryInt);

        pRtrPortNode->u1IsDynamic = SNOOP_TRUE;

        if (SNOOP_INSTANCE_INFO (u4Instance)->
            SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
            SNOOP_FORWARD_NON_RTR_PORTS)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
                {
                    if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_ENABLE)
                    {
                        pSnpPortList =
                            (tSnoopIfPortList *)
                            FsUtilAllocBitList (sizeof (tSnoopIfPortList));

                        if (pSnpPortList == NULL)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_CONTROL_PATH_TRC |
                                           SNOOP_DBG_ALL_FAILURE,
                                           SNOOP_GRP_NAME,
                                           "Error in allocating memory for pSnpPortList\r\n");
                            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                                   SNOOP_DBG_FLAG,
                                                   SNOOP_DBG_RESRC,
                                                   SNOOP_OS_RES_DBG,
                                                   SnoopSysErrString
                                                   [SYS_LOG_MEM_ALLOC_FAIL],
                                                   "for port %d\r\n", u4Port);
                            UtilPlstReleaseLocalPortList (pBitmapVlan);
                            return SNOOP_FAILURE;
                        }
                        SNOOP_MEM_SET (*pSnpPortList, 0,
                                       sizeof (tSnoopIfPortList));
                        SNOOP_ADD_TO_PORT_LIST (u4Port, pBitmapVlan);
                        if (SnoopGetPhyPortBmp
                            (u4Instance, pBitmapVlan,
                             *pSnpPortList) != SNOOP_SUCCESS)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_DBG_RESRC |
                                           SNOOP_DBG_ALL_FAILURE,
                                           SNOOP_OS_RES_NAME,
                                           "Unable to get the physical port bitmap from the virtual"
                                           " port bitmap\r\n");
                            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                            UtilPlstReleaseLocalPortList (pBitmapVlan);
                            return SNOOP_FAILURE;
                        }
                        SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                       (pSnoopVlanEntry->
                                                        VlanId),
                                                       (tPortList *)
                                                       (*pSnpPortList));
                        FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                    }
                    else
                    {
                        pTmpPortList =
                            (tPortList *)
                            FsUtilAllocBitList (sizeof (tPortList));

                        if (pTmpPortList == NULL)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_CONTROL_PATH_TRC |
                                           SNOOP_DBG_ALL_FAILURE,
                                           SNOOP_GRP_NAME,
                                           "Error in allocating memory for pTmpPortList\r\n");
                            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                                   SNOOP_DBG_FLAG,
                                                   SNOOP_DBG_RESRC,
                                                   SNOOP_OS_RES_DBG,
                                                   SnoopSysErrString
                                                   [SYS_LOG_MEM_ALLOC_FAIL],
                                                   "for port %d\r\n", u4Port);
                            UtilPlstReleaseLocalPortList (pBitmapVlan);
                            return SNOOP_FAILURE;
                        }
                        SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));
                        L2IwfMiGetVlanEgressPorts (u4Instance,
                                                   SNOOP_OUTER_VLAN
                                                   (pSnoopVlanEntry->VlanId),
                                                   *pTmpPortList);
                        SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                       (pSnoopVlanEntry->
                                                        VlanId),
                                                       (tPortList *)
                                                       (*pTmpPortList));
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    }
                }
                /* Forwarding Mode is MAC based so update MAC forwarding table */
                if (SnoopFwdUpdateRtrPortToMacFwdTable
                    (u4Instance, u4Port, pSnoopVlanEntry->VlanId,
                     pSnoopVlanEntry->u1AddressType, NullMacAddr,
                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Addtion of port to MAC forwarding "
                                   "entry failed\r\n");
                    UtilPlstReleaseLocalPortList (pBitmapVlan);
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                /* Forwarding Mode is IP based so update IP forwarding table */
                if (SnoopFwdUpdateRtrPortToIpFwdTable
                    (u4Instance, u4Port, pSnoopVlanEntry->VlanId,
                     pSnoopVlanEntry->u1AddressType,
                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Addtion of port to IP forwarding entry failed\r\n");
                    UtilPlstReleaseLocalPortList (pBitmapVlan);
                    return SNOOP_FAILURE;
                }
            }
        }
    }

    UtilPlstReleaseLocalPortList (pBitmapVlan);
    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopVlanDeleteRtrPortEntry                          */
/*                                                                           */
/* Description        : This function deletes the router port in a VLAN and  */
/*                      also updates the multicast forward entries with the  */
/*                      deletion.                                            */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - Vlan Entry                         */
/*                      u4Port        - Port number                          */
/*                      tMacAddr      - Mcast MAAC addr                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SnoopVlanDeleteRtrPortEntry (UINT4 u4Instance,
                             tSnoopVlanEntry * pSnoopVlanEntry, UINT4 u4Port,
                             tMacAddr McastAddr, UINT1 u1Action)
{
    tSnoopIfPortList   *pSnpPortList = NULL;
    UINT1              *pBitmapVlan = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bIsStaticRtrPort = OSIX_FALSE;
    BOOL1               bPortIsRtrPort = OSIX_FALSE;
    BOOL1               bPortOnlyDynPort = OSIX_FALSE;
    BOOL1               bPortOnlyStaticPort = OSIX_FALSE;

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    /* Check if the  port is a router port */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap,
                           bPortIsRtrPort);

    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap,
                           bIsStaticRtrPort);

    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->DynRtrPortBitmap,
                           bPortOnlyDynPort);

    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        SNOOP_IS_PORT_PRESENT (u4Port,
                               pSnoopVlanEntry->pSnoopVlanCfgEntry->
                               CfgStaticRtrPortBmp, bPortOnlyStaticPort);
    }

    if ((bPortIsRtrPort == OSIX_FALSE) && (bIsStaticRtrPort == OSIX_FALSE))
    {
        /* This port is not a router port. So return. */
        if (SnoopMiIsVlanMemberPort (u4Instance, pSnoopVlanEntry->VlanId,
                                     u4Port) == SNOOP_TRUE)
        {
            return SNOOP_SUCCESS;
        }
    }

    if (SNOOP_SLL_COUNT (&pSnoopVlanEntry->RtrPortList) != 0)
    {
        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pSnoopRtrPortEntry, tSnoopRtrPortEntry *)
        {
            if (pSnoopRtrPortEntry->u4Port == u4Port)
            {
                /* Stop the port purge timer if it is running already. */

                /* Stop the V1 Querier Timer, if it is running for the 
                 * router port */
                if (pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType
                    != SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopRtrPortEntry->
                                       V1RtrPortPurgeTimer);
                }

                /* Stop the V2 Querier Timer, if it is running for the 
                 * router port */
                if (pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType
                    != SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopRtrPortEntry->
                                       V2RtrPortPurgeTimer);
                }
                /* Stop the V3 Querier Timer, if it is running for 
                 * the router port */

                if (pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType
                    != SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopRtrPortEntry->
                                       V3RtrPortPurgeTimer);
                }

                /* If the Port is dynamically learnt then delete the port
                 * If the port is static then delete the port only 
                 * when the action is SNOOP_DEL_PORT
                 */

                /* Check if the  port is present in the static router port bitmap */
                SNOOP_IS_PORT_PRESENT (u4Port,
                                       pSnoopVlanEntry->StaticRtrPortBitmap,
                                       bIsStaticRtrPort);

                if (((bIsStaticRtrPort == OSIX_TRUE)
                     && (u1Action == SNOOP_DEL_PORT))
                    || (bIsStaticRtrPort == OSIX_FALSE))
                {

                    SNOOP_SLL_DELETE (&pSnoopVlanEntry->RtrPortList,
                                      &pSnoopRtrPortEntry->NextRtrPortEntry);

                    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                  pSnoopRtrPortEntry,
                                                  SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE);
                    /* Remove the router port entry from the global RBTree */
                    RBTreeRem (gRtrPortTblRBRoot, pSnoopRtrPortEntry);

                    SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4Instance,
                                                     pSnoopRtrPortEntry);

                    if (bIsStaticRtrPort == OSIX_TRUE)
                    {
                        /* Delete the port from the Static Router Port bitmap */
                        SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                                  pSnoopVlanEntry->
                                                  StaticRtrPortBitmap);
                    }
                }

                if ((bIsStaticRtrPort == OSIX_TRUE)
                    && (u1Action == SNOOP_DISABLE_PORT))
                {
                    pSnoopRtrPortEntry->u1IsDynamic = SNOOP_FALSE;
                    SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                              pSnoopVlanEntry->
                                              DynRtrPortBitmap);
                    /* Send a Router Port Sync Message to Standby Node. */
                    SnoopRedActiveSendRtrPortListSync (u4Instance,
                                                       pSnoopVlanEntry);
                    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
                    {
                        SnoopUtilUpdateRtrPortOperVer (pSnoopRtrPortEntry,
                                                       pSnoopVlanEntry->
                                                       pSnoopVlanCfgEntry->
                                                       u1ConfigOperVer);
                    }
                }
                break;
            }
        }
    }

    /* Delete the port from the consolidated bitmap */
    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);
    if ((pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        && (u1Action == SNOOP_DEL_PORT))
    {
        SNOOP_IS_PORT_PRESENT (u4Port,
                               pSnoopVlanEntry->pSnoopVlanCfgEntry->
                               CfgStaticRtrPortBmp, bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                      CfgStaticRtrPortBmp);
        }
    }
    /* Check if the  port is present in the Querier router port bitmap */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                           bResult);

    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                  pSnoopVlanEntry->QuerierRtrPortBitmap);
    }
    /* Reset the conolidated V3 Rtr port bitmap */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V3RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V3RtrPortBitmap);
    }

    /* Reset the conolidated V2 Rtr port bitmap */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V2RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V2RtrPortBitmap);
    }

    /* Reset the conolidated V1 Rtr port bitmap */
    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V1RtrPortBitmap, bResult);
    if (bResult == OSIX_TRUE)
    {
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V1RtrPortBitmap);
    }

    /* Reset Blocked router port bitmap */
    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        SNOOP_IS_PORT_PRESENT (u4Port,
                               pSnoopVlanEntry->pSnoopVlanCfgEntry->
                               CfgBlockedRtrPortBmp, bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                      CfgBlockedRtrPortBmp);
        }
    }

    /* Send a Router Port Sync Message to Standby Node. */

    if ((bPortOnlyDynPort == OSIX_TRUE) && (bPortOnlyStaticPort == OSIX_FALSE))
    {
        /*Port is present only as dynamic router */
        /*Sync up to be done only for dynamic ports */
        SnoopRedActiveSendRtrPortListSync (u4Instance, pSnoopVlanEntry);
    }

    /* Update the multicast forwarding table by removing the port */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {

        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            pSnpPortList =
                (tSnoopIfPortList *)
                FsUtilAllocBitList (sizeof (tSnoopIfPortList));

            if (pSnpPortList == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Error in allocating memory for pSnpPortList\r\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for port %d\r\n", u4Port);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortList));

            pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pBitmapVlan == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Error in allocating memory for pBitmapVlan\r\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for port %d\r\n", u4Port);
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));
            SNOOP_MEM_CPY (pBitmapVlan, &pSnoopVlanEntry->RtrPortBitmap,
                           SNOOP_PORT_LIST_SIZE);

            if (SnoopGetPhyPortBmp (u4Instance, pBitmapVlan, *pSnpPortList)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Unable to get the physical port bitmap from the virtual"
                               " port bitmap\r\n");
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                return SNOOP_FAILURE;
            }

            SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                           (pSnoopVlanEntry->VlanId),
                                           (tPortList *) (*pSnpPortList));
            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
            UtilPlstReleaseLocalPortList (pBitmapVlan);
        }

        /* Delete the port from MAC based multicast forwarding table */
        if (SnoopFwdUpdateRtrPortToMacFwdTable (u4Instance, u4Port,
                                                pSnoopVlanEntry->VlanId,
                                                pSnoopVlanEntry->u1AddressType,
                                                McastAddr, SNOOP_DEL_PORT)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME, "Deletion of port from MAC"
                           " forwarding entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* Forwarding Mode is IP based so update IP forwarding table */
        if (SnoopFwdUpdateRtrPortToIpFwdTable (u4Instance, u4Port,
                                               pSnoopVlanEntry->VlanId,
                                               pSnoopVlanEntry->u1AddressType,
                                               SNOOP_DEL_PORT) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME, "Deletion of port from IP"
                           " forwarding entry failed\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* Check if there is no router port learnt for this VLAN.
     * If not, then copy the configured Querier and the Version information
     * from the VlanCfgTable to the VLAN table. otherwise do nothing
     */
    if ((SNOOP_MEM_CMP (pSnoopVlanEntry->QuerierRtrPortBitmap, gNullPortBitMap,
                        SNOOP_PORT_LIST_SIZE) != 0))
    {
        return SNOOP_SUCCESS;
    }

    /* 
     * If the switch is configured as a Querier/ Non Querier, 
     * then copy configured Querier information to the Vlan table 
     */
    if (SNOOP_PROXY_STATUS (u4Instance,
                            pSnoopVlanEntry->u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        {
            if ((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier) &&
                (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier !=
                 pSnoopVlanEntry->u1Querier))
            {
                pSnoopVlanEntry->u1Querier =
                    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier;

                if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
                {
                    if (SnoopVlanSendGeneralQuery
                        (u4Instance, SNOOP_INVALID_PORT,
                         pSnoopVlanEntry) != SNOOP_SUCCESS)
                    {
                        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_TMR_TRC,
                                   "Unable to send General Query message on query "
                                   "timer expiry\n");
                        return SNOOP_FAILURE;
                    }
                    pSnoopVlanEntry->u1StartUpQCount++;

                    /* Transition from Non-Querier to Querier */
                    SnoopHandleTransitionToQuerier (u4Instance,
                                                    pSnoopVlanEntry);
                }
                else
                {
                    /* Transition from Querier to Non-Querier */
                    SnoopHandleTransitionToNonQuerier (u4Instance,
                                                       pSnoopVlanEntry);
                }
            }
        }
    }
    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUpdateStats                                 */
/*                                                                           */
/* Description        : This function updates the statistics for the VLAN    */
/*                      data packet                                          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Identifier               */
/*                      pSnoopVlanEntry  - pointer to the VLAN entry         */
/*                      u1StatType       - Type of packet sent/received      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopVlanUpdateStats (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry,
                      UINT1 u1StatType)
{
    tSnoopVlanStatsEntry *pSnoopVlanStatsEntry = NULL;
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (pSnoopVlanEntry->pSnoopVlanStatsEntry == NULL)
    {
        /* Allocate memory block for the Stats Entry */
        if (SNOOP_VLAN_STATS_ALLOC_MEMBLK (u4Instance,
                                           pSnoopVlanStatsEntry) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed for Snoop Vlan Stats Entry\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u1StatType %u\r\n", u1StatType);
            SnoopRedSyncBulkUpdNextEntry (u4Instance, pSnoopVlanEntry,
                                          SNOOP_RED_BULK_UPD_VLAN_REMOVE);

            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                          (tRBElem *) pSnoopVlanEntry);

            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pSnoopVlanStatsEntry, 0, sizeof (tSnoopVlanStatsEntry));
        pSnoopVlanEntry->pSnoopVlanStatsEntry = pSnoopVlanStatsEntry;
    }

    switch (u1StatType)
    {
        case SNOOP_QUERY_SENT:
            SNOOP_GEN_QUERY_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_QUERY_RCVD:
            SNOOP_GEN_QUERY_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_QUERY_SENT:
            SNOOP_GRP_QUERY_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_QUERY_RCVD:
            SNOOP_GRP_QUERY_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_SRC_QUERY_SENT:
            SNOOP_GRP_SRC_QUERY_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_SRC_QUERY_RCVD:
            SNOOP_GRP_SRC_QUERY_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_ASMREPORT_SENT:
            SNOOP_ASMREPORT_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_ASMREPORT_RCVD:
            SNOOP_ASMREPORT_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_SSMREPORT_SENT:
            SNOOP_SSMREPORT_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_SSMREPORT_RCVD:
            SNOOP_SSMREPORT_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_IS_INCLUDE_RCVD:
            SNOOP_IS_INCL_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_IS_EXCLUDE_RCVD:
            SNOOP_IS_EXCL_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_TO_INCLUDE_RCVD:
            SNOOP_TO_INCL_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_TO_EXCLUDE_RCVD:
            SNOOP_TO_EXCL_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_ALLOW_RCVD:
            SNOOP_ALLOW_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_BLOCK_RCVD:
            SNOOP_BLOCK_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_LEAVE_SENT:
            SNOOP_LEAVE_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_LEAVE_RCVD:
            SNOOP_LEAVE_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_PKT_ERROR:
            SNOOP_PKT_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_ACTIVE_JOINS:
            SNOOP_INCR_ACTIVE_JOINS (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_UNSUCCESSFUL_JOINS:
            SNOOP_INCR_UNSUCCESSFUL_JOINS (pSnoopVlanEntry->
                                           pSnoopVlanStatsEntry);
            break;

        case SNOOP_ACTIVE_GRPS:
            SNOOP_ACTIVE_GROUPS (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

/** new **/
        case SNOOP_V1REPORT_RCVD:
            SNOOP_V1REPORT_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V2REPORT_RCVD:
            SNOOP_V2REPORT_RX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V1REPORT_SENT:
            SNOOP_V1REPORT_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V2REPORT_SENT:
            SNOOP_V2REPORT_TX (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V1QUERY_DROPPED:
            SNOOP_V1QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V2QUERY_DROPPED:
            SNOOP_V2QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V3QUERY_DROPPED:
            SNOOP_V3QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V2GRP_QUERY_DROPPED:
            SNOOP_V2GRP_QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V3GRP_QUERY_DROPPED:
            SNOOP_V3GRP_QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_SRC_QUERY_DROPPED:
            SNOOP_GRP_SRC_QUERY_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V1REPORT_DROPPED:
            SNOOP_V1REPORT_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_V2REPORT_DROPPED:
            SNOOP_V2REPORT_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_IS_INCLUDE_DROPPED:
            SNOOP_IS_INCLUDE_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_IS_EXCLUDE_DROPPED:
            SNOOP_IS_EXCLUDE_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_TO_INCLUDE_DROPPED:
            SNOOP_TO_INCLUDE_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_TO_EXCLUDE_DROPPED:
            SNOOP_TO_EXCLUDE_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_ALLOW_DROPPED:
            SNOOP_ALLOW_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_BLOCK_DROPPED:
            SNOOP_BLOCK_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_LEAVE_DROPPED:
            SNOOP_LEAVE_DROPPED (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GEN_QUERY_FWD:
            SNOOP_GEN_QUERY_FWD (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;

        case SNOOP_GRP_QUERY_FWD:
            SNOOP_GRP_QUERY_FWD (pSnoopVlanEntry->pSnoopVlanStatsEntry);
            break;
        /** new **/

        default:
            break;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUpdateVlanTable                             */
/*                                                                           */
/* Description        : This function updates the VLAN table for an instance */
/*                                                                           */
/* Input(s)           : u4Instance  - instance number                        */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopVlanUpdateVlanTable (UINT4 u4Instance, UINT1 u1AddressType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntry = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tSnoopRtrPortEntry *pTempRtrPortNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopVlanStatsEntry *pSnoopVlanStatsEntry;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME, "Snoop Vlan Table not present \n");
        return SNOOP_FAILURE;
    }

    pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                   SnoopInfo[u1AddressType - 1]);

    if (pSnoopInfo == NULL)
    {
        SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_DBG_MGMT, SNOOP_MGMT_DBG,
                        "instance %d for the address type %d is not created\r\n",
                        u4Instance, u1AddressType);
        return SNMP_FAILURE;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        if (pSnoopVlanEntry->u1AddressType == u1AddressType)
        {
            /* Check if any static configuration is done for this VLAN */
            if (pSnoopVlanEntry->pSnoopVlanCfgEntry == NULL)
            {
                return SNOOP_FAILURE;
            }
            pSnoopVlanCfgEntry = pSnoopVlanEntry->pSnoopVlanCfgEntry;

            /* Check if any of the Global or VLAN SNOOP status is disabled
             * then do not copy the configured VLAN information from the 
             * tSnoopVlanCfgEntry table to the VLAN table 
             */
            if ((pSnoopVlanCfgEntry->u1ConfigSnoopStatus == SNOOP_DISABLE) ||
                (SNOOP_SNOOPING_STATUS (u4Instance,
                                        (u1AddressType - 1)) == SNOOP_DISABLE))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            /* Allocate memory block for the Statistics Entry */
            if (SNOOP_VLAN_STATS_ALLOC_MEMBLK (u4Instance,
                                               pSnoopVlanStatsEntry) == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                           "Memory allocation failed for Snoop Vlan Stats Entry\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for u4Instance %d\r\n", u4Instance);
                return SNOOP_FAILURE;
            }

            SNOOP_MEM_SET (pSnoopVlanStatsEntry, 0,
                           sizeof (tSnoopVlanStatsEntry));
            pSnoopVlanEntry->pSnoopVlanStatsEntry = pSnoopVlanStatsEntry;

            if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
            {
                if (pSnoopVlanCfgEntry->u1ConfigSnoopStatus)
                {
                    pSnoopVlanEntry->u1SnoopStatus =
                        pSnoopVlanCfgEntry->u1ConfigSnoopStatus;
                }

                if (pSnoopVlanCfgEntry->u1ConfigFastLeave)
                {
                    pSnoopVlanEntry->u1FastLeave =
                        pSnoopVlanCfgEntry->u1ConfigFastLeave;
                }

                if (pSnoopVlanCfgEntry->u2ConfigQueryInt)
                {
                    pSnoopVlanEntry->u2QueryInterval =
                        pSnoopVlanCfgEntry->u2ConfigQueryInt;

                    SNOOP_SET_PORT_PURGE_INTERVAL (pSnoopVlanEntry,
                                                   pSnoopVlanEntry->
                                                   u4RobustnessValue,
                                                   pSnoopVlanEntry->
                                                   u1OperVersion);

                }
                pSnoopVlanEntry->u2StartUpQInterval =
                    (UINT2) (pSnoopVlanEntry->u2QueryInterval / 4);
                /* Send notification for configuring the StartupQueryInterval */

                pSnoopVlanEntry->u4ProfileId =
                    pSnoopVlanCfgEntry->u4ConfigProfileId;

                /* If the Static Router port is configured add to router port 
                 * bitmap and make the SNOOP querier status as non querier 
                 */
                if (SNOOP_MEM_CMP (pSnoopVlanCfgEntry->CfgStaticRtrPortBmp,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
                {
                    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE;
                         u4Port++)
                    {
                        SNOOP_IS_PORT_PRESENT (u4Port,
                                               pSnoopVlanCfgEntry->
                                               CfgStaticRtrPortBmp, bResult);

                        if (bResult == OSIX_FALSE)
                        {
                            continue;
                        }

                        SnoopGetIfOperStatus (SNOOP_GET_IFINDEX
                                              (u4Instance, u4Port),
                                              &u1OperStatus);
                        if (u1OperStatus == CFA_IF_DOWN)
                        {
                            continue;
                        }

                        if (SnoopMiIsVlanMemberPort
                            (u4Instance, pSnoopVlanEntry->VlanId,
                             SNOOP_GET_IFINDEX (u4Instance,
                                                u4Port)) == SNOOP_FALSE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                                      pSnoopVlanCfgEntry->
                                                      CfgStaticRtrPortBmp);

                            SnoopVlanAddRemRtrPort (u4Instance, u4Port,
                                                    pSnoopVlanEntry,
                                                    SNOOP_DEL_PORT);

                            continue;
                        }

                        SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                pSnoopVlanEntry->
                                                StaticRtrPortBitmap);

                        SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                pSnoopVlanEntry->RtrPortBitmap);

                        /* Update the operating version of the router port */

                        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                                        pRtrPortNode, tSnoopRtrPortEntry *)
                        {
                            if (pRtrPortNode->u4Port == u4Port)
                            {
                                break;
                            }
                        }

                        if (pRtrPortNode != NULL)
                        {
                            /* Update the operating version of the router port 
                             * as default version */
                            pRtrPortNode->u1OperVer =
                                (pSnoopVlanEntry->u1AddressType ==
                                 SNOOP_ADDR_TYPE_IPV4 ? SNOOP_IGS_IGMP_VERSION3
                                 : SNOOP_MLD_VERSION2);

                            SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                    pSnoopVlanEntry->
                                                    V3RtrPortBitmap);

                            if (pRtrPortNode->u1OperVer !=
                                pRtrPortNode->u1ConfigOperVer)
                            {
                                /* Update the operating version of the router 
                                 * port as configured version */
                                SnoopUtilUpdateRtrPortOperVer (pRtrPortNode,
                                                               pRtrPortNode->
                                                               u1ConfigOperVer);
                            }
                        }
                        else
                        {
                            /* Router port entry is not present in router port
                             * list. So add the router port entry */
                            SnoopVlanAddRemRtrPort (u4Instance, u4Port,
                                                    pSnoopVlanEntry,
                                                    SNOOP_ADD_PORT);
                        }

                    }
                }
                SNOOP_DYN_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                                    pRtrPortNode, pTempRtrPortNode,
                                    tSnoopRtrPortEntry *)
                {
                    SNOOP_IS_PORT_PRESENT (pRtrPortNode->u4Port,
                                           pSnoopVlanEntry->StaticRtrPortBitmap,
                                           bResult);
                    if (bResult == OSIX_FALSE)
                    {
                        /* Delete the port entry from SLL */
                        SnoopVlanAddRemRtrPort (u4Instance,
                                                pRtrPortNode->u4Port,
                                                pSnoopVlanEntry,
                                                SNOOP_DEL_PORT);
                    }
                }

                /* Copy the configured querier value for PROXY REPORTING  
                 * mode and transparent snooping mode*/

                if (SNOOP_PROXY_STATUS (u4Instance, u1AddressType - 1)
                    == SNOOP_DISABLE)
                {
                    if ((SNOOP_MEM_CMP (pSnoopVlanEntry->QuerierRtrPortBitmap,
                                        gNullPortBitMap,
                                        SNOOP_PORT_LIST_SIZE) == 0) &&
                        (pSnoopVlanCfgEntry->u1ConfigQuerier) &&
                        (pSnoopVlanCfgEntry->u1ConfigQuerier !=
                         pSnoopVlanEntry->u1Querier))
                    {
                        /* Copy the configured Querier value to the VLAN table 
                         * only when there is no dynamic router port 
                         * present for that VLAN
                         */
                        pSnoopVlanEntry->u1Querier =
                            pSnoopVlanCfgEntry->u1ConfigQuerier;

                        if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
                        {
                            if (SnoopVlanSendGeneralQuery
                                (u4Instance, SNOOP_INVALID_PORT,
                                 pSnoopVlanEntry) != SNOOP_SUCCESS)
                            {
                                SNOOP_TRC (SNOOP_TRC_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_TMR_TRC,
                                           "Unable to send General Query message on query "
                                           "timer expiry\n");
                                return SNOOP_FAILURE;
                            }
#ifdef RM_WANTED
                            if (RmGetNodeState () != RM_STANDBY)
                            {
                                pSnoopVlanEntry->u1StartUpQCount++;
                            }
#endif
                            /* Transition from Non-Querier to Querier */
                            SnoopHandleTransitionToQuerier (u4Instance,
                                                            pSnoopVlanEntry);
                        }
                        else
                        {
                            /* Transition from Querier to Non-Querier */
                            SnoopHandleTransitionToNonQuerier (u4Instance,
                                                               pSnoopVlanEntry);
                        }
                    }
                }
                /* If the switch is configured as querier, make it as a 
                 * querier and send a general query on VLAN member ports 
                 * and start the query timer */
                if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
                {
                    /* Send out general query and start query timer. */
                    SnoopUtilSendGeneralQuery (u4Instance,
                                               pSnoopVlanEntry->u1AddressType,
                                               pSnoopVlanEntry, SNOOP_TRUE);
                }
            }

        }

        pRBElem = pRBNextElem;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUpdateStatusChange                          */
/*                                                                           */
/* Description        : This routine updates the VLAN table when the SNOOP   */
/*                      status changes                                       */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                    : pSnoopVlanEntry - pointer to VLAN entry              */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopVlanUpdateStatusChange (UINT4 u4Instance,
                             tSnoopVlanEntry * pSnoopVlanEntry)
{
    tPortList          *pTmpPortList = NULL;
    UINT1              *pTempPortBitmap = NULL;
    tSnoopVlanStatsEntry *pSnoopVlanStatsEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tIPvXAddr           NullAddr;
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4Port = 0;
    UINT1               au1TempAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1OperStatus = CFA_IF_DOWN;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&au1TempAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&NullAddr, 0, sizeof (tIPvXAddr));

    pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                   SnoopInfo[pSnoopVlanEntry->u1AddressType - 1]);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* When SNOOP is disabled for VLAN */
    if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigSnoopStatus
        == SNOOP_DISABLE)
    {
        /* Stop the Query timer if it is running already */
        if (pSnoopVlanEntry->QueryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopHandleTransitionToNonQuerier (u4Instance, pSnoopVlanEntry);
        }

        /* Stop the Other Querier Present Interval timer if it is running already */
        if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
        }
        pSnoopVlanEntry->u1StartUpQCount = 0;
        /* Delete all router port information learnt for this VLAN */
        SnoopVlanDeleteRtrPortList (u4Instance, pSnoopVlanEntry, SNOOP_TRUE);

        SnoopFwdUpdateFwdBitmap (u4Instance, pSnoopVlanEntry->RtrPortBitmap,
                                 pSnoopVlanEntry, NullAddr, SNOOP_DEL_PORT);

        /* Reset the consolidated bitmap for this VLAN */
        SNOOP_MEM_SET (pSnoopVlanEntry->RtrPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

        /* Reset the IGMP port bitmap for this VLAN */
        SNOOP_MEM_SET (pSnoopVlanEntry->QuerierRtrPortBitmap, 0,
                       SNOOP_PORT_LIST_SIZE);

        /* Delete all statistics information learnt for this VLAN */
        if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
        {
            SNOOP_VLAN_STATS_FREE_MEMBLK (u4Instance,
                                          pSnoopVlanEntry->
                                          pSnoopVlanStatsEntry);

            /* Reset the statistics information */
            pSnoopVlanEntry->pSnoopVlanStatsEntry = NULL;
        }
        pTmpPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (pTmpPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Error in allocating memory for pTmpPortList\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
            {
                L2IwfMiGetVlanEgressPorts (u4Instance,
                                           SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                             VlanId),
                                           *pTmpPortList);
                SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                               (pSnoopVlanEntry->VlanId),
                                               (tPortList *) (*pTmpPortList));
            }
            else
            {
                /* Delete all the MCAST forwarding table information */
                SnoopFwdDeleteVlanMcastFwdEntries (u4Instance,
                                                   pSnoopVlanEntry->VlanId,
                                                   pSnoopVlanEntry->
                                                   u1AddressType);

                /* Delete all Group information for the given VLAN */
                if (SnoopGrpDelGroupOnOuterVlan
                    (u4Instance, pSnoopVlanEntry->VlanId) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                        "Unable to delete the group entry "
                                        "when SNOOP is disable for a VLAN %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    return SNOOP_FAILURE;
                }
            }
        }
        else
        {
            if ((SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_DISABLE)
                && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
            {
                pRBElem =
                    RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                    IpMcastFwdEntry);
                while (pRBElem != NULL)
                {
                    pRBNextElem =
                        RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                       IpMcastFwdEntry, pRBElem, NULL);
                    pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                    if (SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)
                        != SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId))
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }

                    if (pSnoopIpGrpFwdEntry->u1AddressType !=
                        pSnoopVlanEntry->u1AddressType)
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }

                    L2IwfMiGetVlanEgressPorts (u4Instance,
                                               SNOOP_OUTER_VLAN
                                               (pSnoopIpGrpFwdEntry->VlanId),
                                               *pTmpPortList);
                    pTempPortBitmap =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pTempPortBitmap == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                       "Error in allocating memory for pTempPortBitmap\r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_MEM_ALLOC_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        return SNOOP_FAILURE;
                    }
                    MEMSET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));
                    SNOOP_MEM_CPY (pTempPortBitmap, *pTmpPortList,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_UPDATE_PORT_LIST (pTempPortBitmap,
                                            pSnoopIpGrpFwdEntry->PortBitmap);
#ifdef NPAPI_WANTED
                    if (SnoopUtilNpUpdateMcastFwdEntry
                        (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                         pSnoopIpGrpFwdEntry->SrcIpAddr,
                         pSnoopIpGrpFwdEntry->GrpIpAddr,
                         pTempPortBitmap,
                         SNOOP_HW_CREATE_ENTRY) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                       "Addition of port for "
                                       "IP forwarding entry to hardware "
                                       "failed\r\n");
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        return SNOOP_FAILURE;
                    }
#endif
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    pRBElem = pRBNextElem;
                }
            }
            else if ((SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) ==
                      SNOOP_ENABLE)
                     ||
                     ((SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) ==
                       SNOOP_DISABLE)
                      && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) ==
                          SNOOP_DISABLE)))
            {
                SnoopFwdDeleteVlanMcastFwdEntries (u4Instance,
                                                   pSnoopVlanEntry->VlanId,
                                                   pSnoopVlanEntry->
                                                   u1AddressType);

                /* Delete all Group information for the given VLAN */
                if (SnoopGrpDelGroupOnOuterVlan
                    (u4Instance, pSnoopVlanEntry->VlanId) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_NAME,
                                        "Unable to delete the group entry "
                                        "when SNOOP is disable for a VLAN %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    return SNOOP_FAILURE;
                }
            }
        }
        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
    }
    else
    {
        /* If the Static Router port is configured add to router port 
         * bitmap*/
        if (SNOOP_MEM_CMP (pSnoopVlanEntry->StaticRtrPortBitmap,
                           gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
        {
            /* Check for the operating status of router port before updating the router port bitmap. */

            for (u4Port = 1; u4Port <= SNOOP_MAX_PHY_PLUS_LAG_PORTS; u4Port++)
            {
                SNOOP_IS_PORT_PRESENT (u4Port,
                                       pSnoopVlanEntry->StaticRtrPortBitmap,
                                       bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }
                SnoopGetIfOperStatus (SNOOP_GET_IFINDEX (u4Instance, u4Port),
                                      &u1OperStatus);
                if (u1OperStatus == CFA_IF_DOWN)
                {
                    SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                              pSnoopVlanEntry->
                                              StaticRtrPortBitmap);
                    SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                              pSnoopVlanEntry->RtrPortBitmap);
                }
            }

            SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->StaticRtrPortBitmap,
                                    pSnoopVlanEntry->RtrPortBitmap);
        }
        /* Allocate memory block for the Statistics Entry */
        if (SNOOP_VLAN_STATS_ALLOC_MEMBLK (u4Instance,
                                           pSnoopVlanStatsEntry) == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                       "Memory allocation failed for Snoop Vlan Stats Entry\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u4Instance %d\r\n", u4Instance);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pSnoopVlanStatsEntry, 0, sizeof (tSnoopVlanStatsEntry));
        pSnoopVlanEntry->pSnoopVlanStatsEntry = pSnoopVlanStatsEntry;
        if (pSnoopInfo->u1SnoopStatus == SNOOP_ENABLE)
        {
            SnoopUtilEnableSnoopInVlan (u4Instance, pSnoopVlanEntry);
        }
        if ((SNOOP_PROXY_STATUS
             (u4Instance, (pSnoopVlanEntry->u1AddressType) - 1) == SNOOP_ENABLE)
            && (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigSnoopStatus ==
                SNOOP_ENABLE))
        {
            /* If proxy is enabled, check if query timer is already
             * running. If not, send out general query and
             * start query timer. */
            pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;
            SnoopUtilSendGeneralQuery (u4Instance,
                                       pSnoopVlanEntry->u1AddressType,
                                       pSnoopVlanEntry, SNOOP_TRUE);
        }

    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopVlanGetStaticRtrPorts                       */
/*                                                                           */
/*    Description         : This function returns statically configured      */
/*                          port list for given vlan id.                     */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance number                     */
/*                          VlanId  - Vlan Id                                */
/*                          u1AddressType - Indicates Address family v4/v6   */
/*                                                                           */
/*    Output(s)           : pRtrPortList - Statically configured Port list   */
/*                                        in tSNMP_OCTET_STRING_TYPE *       */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/
INT4
SnoopVlanGetStaticRtrPorts (UINT4 u4Instance, tSnoopTag VlanId,
                            UINT1 u1AddressType,
                            tSNMP_OCTET_STRING_TYPE * pRtrPortList)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopVlanEntry     SnoopVlanEntry;
    tRBElem            *pRBTreeElem = NULL;

    SNOOP_MEM_SET (&SnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    if (pRtrPortList == NULL)
    {
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pRtrPortList->pu1_OctetList, 0, SNOOP_PORT_LIST_SIZE);
    pRtrPortList->i4_Length = SNOOP_PORT_LIST_SIZE;

    SNOOP_MEM_CPY (SnoopVlanEntry.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopVlanEntry.u1AddressType = u1AddressType;

    pRBTreeElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                             (tRBElem *) & SnoopVlanEntry);

    if (pRBTreeElem == NULL)
    {
        return SNOOP_FAILURE;
    }

    pSnoopVlanEntry = (tSnoopVlanEntry *) pRBTreeElem;

    SNOOP_MEM_CPY (pRtrPortList->pu1_OctetList,
                   pSnoopVlanEntry->StaticRtrPortBitmap, SNOOP_PORT_LIST_SIZE);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanForwardPacket                               */
/*                                                                           */
/* Description        : This function forwards the packets on to the         */
/*                      member ports of the VLAN                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN Identifier                         */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                      u4Port     - Incoming port number                    */
/*                      u1Forward  - VLAN_FORWARD_SPECIFIC/VLAN_FORWARD_ALL  */
/*                      pBuf       - pointer to the packet buffer            */
/*                      PortBitmap - port bitmap                             */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopVlanForwardPacket (UINT4 u4Instance, tSnoopTag VlanId,
                        UINT1 u1AddressType, UINT4 u4Port,
                        UINT1 u1Forward, tCRU_BUF_CHAIN_HEADER * pBuf,
                        tSnoopPortBmp PortBitmap, UINT4 u4StatType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tMacAddr            DestMacAddr;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = SNOOP_INVALID_PORT;

    SNOOP_MEM_SET (&DestMacAddr, 0, sizeof (tMacAddr));

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

#ifdef L2RED_WANTED

    if ((SNOOP_RED_SWITCHOVER_TMR_NODE ().u1TimerType
         == SNOOP_RED_SWITCHOVER_QUERY_TIMER) &&
        (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
        ((u1AddressType == SNOOP_ADDR_TYPE_IPV4)
         || (u1AddressType == SNOOP_ADDR_TYPE_IPV6))
        && (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance, u1AddressType - 1)
            == SNOOP_TRUE))
    {
        /* Database updation is in progress and transparent snooping is
         * disabled. So  - 
         * 1. Don't send out any query to downstream ports
         * 2. Don't fwd any consolidated report to upstream while learnt a new 
         *    group entry.
         * 3. Don't fwd any consolidated report to upstream while received a
         *    query from upstream router.
         * Consolidated report will be sent once database updation is
         * completed. */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
#endif /* L2RED_WANTED */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Packets should not be transmitted out from standby node */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, DestMacAddr, 0, ETHERNET_ADDR_SIZE);

    if (u4Port != SNOOP_INVALID_PORT)
    {
        /* ASSUMPTION: If u4Port is valid, then the packet is being forwarded
         * by the IGS module. In case of forwarding, VLAN module has to be considered.
         * Hence calling the VLAN API to forward and returning from here.
         *
         * NOTE: Query forwarding will be done through a separate path. So, Enhanced
         * mode operations will not be affected.*/

        u4IfIndex = SNOOP_GET_IFINDEX (u4Instance, u4Port);

    }

    i4RetVal =
        SnoopMiVlanSnoopFwdPacketOnPortList (pBuf, u4Instance, DestMacAddr,
                                             (UINT2) u4IfIndex, VlanId,
                                             OSIX_FALSE, u1Forward, PortBitmap);

    if (i4RetVal == VLAN_FORWARD)
    {
        if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                                   &pSnoopVlanEntry) != SNOOP_FAILURE)
        {
            /* Update the VLAN statistics */
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      (UINT1) u4StatType) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
        }
    }

    return;

}

/*****************************************************************************/
/* Function Name      : SnoopVlanSendGeneralQuery                            */
/*                                                                           */
/* Description        : This function constructs and sends the IGMP general  */
/*                      query on all non router ports                        */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4InstId - Instance number                           */
/*                      u4Port   - Port On which the Toplogy change occured  */
/*                      pSnoopVlanEntry - Pointer to the Vlan Entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopVlanSendGeneralQuery (UINT4 u4Instance, UINT4 u4Port,
                           tSnoopVlanEntry * pSnoopVlanEntry)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pPortBitMap = NULL;
    UINT1              *pRtrBitMap = NULL;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    pRtrBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pRtrBitMap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pRtrBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pRtrBitMap, 0, sizeof (tSnoopPortBmp));
    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                               VlanId),
                                             pSnoopVlanEntry->u1AddressType, 0,
                                             pRtrBitMap, pSnoopVlanEntry);

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        UtilPlstReleaseLocalPortList (pRtrBitMap);
        return SNOOP_FAILURE;
    }

    UNUSED_PARAM (u4Port);

    /*Get the Vlan's Member Portlist and subtract the Vlan's Router portlist
       from member portlist to send the Specific query only on those ports */
    pPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitMap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pPortBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        UtilPlstReleaseLocalPortList (pRtrBitMap);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitMap, 0, sizeof (tSnoopPortBmp));

    if ((SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                         pSnoopVlanEntry->VlanId,
                                         pPortBitMap)) != SNOOP_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pRtrBitMap);
        UtilPlstReleaseLocalPortList (pPortBitMap);
        return SNOOP_FAILURE;
    }
    if (SNOOP_INSTANCE_INFO (u4Instance)->
        SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
        SNOOP_FORWARD_NON_RTR_PORTS)
    {
        SNOOP_XOR_PORT_BMP (pPortBitMap, pRtrBitMap);
    }
    UtilPlstReleaseLocalPortList (pRtrBitMap);
    /* Construct and send a Specific query message on the  VLAN's
     * member ports.*/

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (IgsEncodeQuery (u4Instance, 0, SNOOP_IGMP_QUERY,
                            pSnoopVlanEntry->pSnoopVlanCfgEntry->
                            u1ConfigOperVer, &pBuf,
                            pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPortBitMap);
            return SNOOP_FAILURE;
        }
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (MldsEncodeQuery (u4Instance, 0, SNOOP_MLD_QUERY,
                             SNOOP_MLD_VERSION2, &pBuf,
                             pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPortBitMap);
            return SNOOP_FAILURE;
        }
    }
#endif

    if (pBuf == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortBitMap);
        return SNOOP_FAILURE;
    }

    SnoopVlanForwardQuery (u4Instance, pSnoopVlanEntry->VlanId,
                           pSnoopVlanEntry->u1AddressType,
                           0, VLAN_FORWARD_SPECIFIC,
                           pBuf, pPortBitMap, SNOOP_QUERY_SENT);

    UtilPlstReleaseLocalPortList (pPortBitMap);
    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopVlanForwardQuery                                */
/*                                                                           */
/* Description        : This function forwards the packets on to the         */
/*                      member ports of the VLAN                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN Identifier                         */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - pointer to the packet buffer            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopVlanForwardQuery (UINT4 u4Instance, tSnoopTag VlanId, UINT1 u1AddressType,
                       UINT4 u4Port, UINT1 u1Forward,
                       tCRU_BUF_CHAIN_HEADER * pBuf, tSnoopPortBmp PortBitmap,
                       UINT4 u4StatType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tMacAddr            DestMacAddr;
    INT4                i4RetVal = 0;
    UINT4               u4IfIndex = SNOOP_INVALID_PORT;

    SNOOP_MEM_SET (&DestMacAddr, 0, sizeof (tMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE (u4Instance);

    /* Packets should not be transmitted out from standby node */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, DestMacAddr, 0, ETHERNET_ADDR_SIZE);

    if (u4Port != SNOOP_INVALID_PORT)
    {
        /* ASSUMPTION: If u4Port is valid, then the packet is being forwarded
         * by the IGS module. In case of forwarding, VLAN module has to be considered.
         * Hence calling the VLAN API to forward and returning from here.
         *
         * NOTE: Query forwarding will be done through a separate path. So, Enhanced
         * mode operations will not be affected.*/

        u4IfIndex = SNOOP_GET_IFINDEX (u4Instance, u4Port);
    }

    i4RetVal =
        SnoopMiVlanSnoopFwdPacketOnPortList (pBuf, u4Instance, DestMacAddr,
                                             (UINT2) u4IfIndex, VlanId,
                                             OSIX_TRUE, u1Forward, PortBitmap);
    if (i4RetVal == VLAN_FORWARD)
    {
        if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                                   &pSnoopVlanEntry) != SNOOP_FAILURE)
        {
            /* Update the VLAN statistics */
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      (UINT1) u4StatType) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                    "Unable to update SNOOP "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
        }
    }

    UNUSED_PARAM (u1Forward);

    return;

}

/*****************************************************************************/
/* Function Name      : SnoopVlanInitializeRtrPort                           */
/*                                                                           */
/* Description        : This function initializes the router port node       */
/*                      for the given vlan                                   */
/*                                                                           */
/* Input(s)           : pRtrPortNode  - Pointer to router port node          */
/*                      u4Port     - router  port number                     */
/*                      pSnoopVlanEntry   - pointer to the Vlan Entry        */
/*                      u4Instance        - Instance ID                      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/

VOID
SnoopVlanInitializeRtrPort (UINT4 u4Instance, tSnoopRtrPortEntry * pRtrPortNode,
                            UINT4 u4Port, tSnoopVlanEntry * pSnoopVlanEntry)
{
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    UINT4               u4RetVal = 0;
    UINT1               u1AddressType = pSnoopVlanEntry->u1AddressType;
    BOOL1               bRtrCreate = SNOOP_TRUE;

    pRtrPortNode->pVlanEntry = pSnoopVlanEntry;
    pSnoopRtrPortEntry = (tSnoopRtrPortEntry *) RBTreeGet (gRtrPortTblRBRoot,
                                                           (tRBElem
                                                            *) (pRtrPortNode));
    if (NULL != pSnoopRtrPortEntry)
    {
        SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4Instance, pRtrPortNode);
        pRtrPortNode = pSnoopRtrPortEntry;
        bRtrCreate = SNOOP_FALSE;
    }

    pRtrPortNode->u4V1V2RtrPortPurgeInt =
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo
        [u1AddressType - 1].u4RtrPortPurgeInt;
    pRtrPortNode->u4V3RtrPortPurgeInt = SNOOP_DEF_RTR_PURGE_INTERVAL;
    pRtrPortNode->u1ConfigOperVer = (u1AddressType == SNOOP_ADDR_TYPE_IPV4 ?
                                     SNOOP_IGS_IGMP_VERSION3 :
                                     SNOOP_MLD_VERSION2);
    pRtrPortNode->u1OperVer = (u1AddressType == SNOOP_ADDR_TYPE_IPV4 ?
                               SNOOP_IGS_IGMP_VERSION3 : SNOOP_MLD_VERSION2);
    pRtrPortNode->pVlanEntry = pSnoopVlanEntry;
    /* Instance specific local port number */
    pRtrPortNode->u4Port = u4Port;
    pRtrPortNode->u2InnerVlanId = SNOOP_INVALID_VLAN_ID;
    pRtrPortNode->u1IsDynamic = SNOOP_FALSE;
    /* phyical port index corresponding to the
     * local port number for this instance
     */
    SNOOP_VALIDATE_PORT (u4Port);
    pRtrPortNode->u4PhyPortIndex =
        SNOOP_GET_IFINDEX (u4Instance, (UINT2) u4Port);

    SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
    SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->DynRtrPortBitmap);
    SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->V3RtrPortBitmap);
    /* Add the Router port entry to global Rtr port RBTree */
    if (bRtrCreate == SNOOP_TRUE)
    {
        u4RetVal = RBTreeAdd (gRtrPortTblRBRoot, pRtrPortNode);
        if (u4RetVal == RB_SUCCESS)
        {
            SNOOP_SLL_ADD (&pSnoopVlanEntry->RtrPortList,
                           &pRtrPortNode->NextRtrPortEntry);
        }
        else
        {
            SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_QRY_NAME,
                                "RB Addition for Router port entry failed for port %d VLAN %d\r\n",
                                u4Port,
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanAddRemRtrPort                               */
/*                                                                           */
/* Description        : This function Adds or removes the router port node   */
/*                      for the given vlan                                   */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      u4Port     - router  port number                     */
/*                      pSnoopVlanEntry   - pointer to the Vlan Entry        */
/*                      u1AddRemFlag   -    Flag to identify Addition        */
/*                                          or removal of router port        */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopVlanAddRemRtrPort (UINT4 u4Instance, UINT4 u4Port,
                        tSnoopVlanEntry * pSnoopVlanEntry, UINT1 u1AddRemFlag)
{

    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (u1AddRemFlag == SNOOP_ADD_PORT)
    {

        /* This is a new router port. 
         */
        if (SNOOP_VLAN_RTR_PORT_ALLOC_MEMBLK (u4Instance, pRtrPortNode) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed for Snoop Vlan Rtr Port Entry\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for port %d\r\n", u4Port);
            return SNOOP_FAILURE;
        }

        SNOOP_MEM_SET (pRtrPortNode, 0, sizeof (tSnoopRtrPortEntry));
        SnoopVlanInitializeRtrPort (u4Instance, pRtrPortNode, u4Port,
                                    pSnoopVlanEntry);
    }
    else if (u1AddRemFlag == SNOOP_DEL_PORT)
    {
        /* Delete the Router port Entry 
         * Release the router port node and update the VLAN router port bitmap 
         */
        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4Port)
            {
                break;
            }
        }

        if (pRtrPortNode != NULL)
        {
            /* Stop the V1 Querier Timer, if it is running for the router port */
            if (pRtrPortNode->V1RtrPortPurgeTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pRtrPortNode->V1RtrPortPurgeTimer);
            }

            /* Stop the V2 Querier Timer, if it is running for the router port */
            if (pRtrPortNode->V2RtrPortPurgeTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pRtrPortNode->V2RtrPortPurgeTimer);
            }

            /* Stop the V3 Querier Timer, if it is running for the router port */
            if (pRtrPortNode->V3RtrPortPurgeTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pRtrPortNode->V3RtrPortPurgeTimer);
            }
            SNOOP_SLL_DELETE (&pSnoopVlanEntry->RtrPortList,
                              &pRtrPortNode->NextRtrPortEntry);

            SnoopRedSyncBulkUpdNextEntry (u4Instance, pRtrPortNode,
                                          SNOOP_RED_BULK_UPD_RTR_PORT_REMOVE);
            /* Remove the router port entry from the global RBTree */
            RBTreeRem (gRtrPortTblRBRoot, pRtrPortNode);
            SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (u4InstId, pRtrPortNode);
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap, bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);
        }
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->DynRtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->DynRtrPortBitmap);
        }
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V3RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V3RtrPortBitmap);
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V2RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V2RtrPortBitmap);
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->V1RtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopVlanEntry->V1RtrPortBitmap);
        }
        /* Check if the port is Static Router Port */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->StaticRtrPortBitmap);
        }
        /* Check if the port is learnt from the general query then delete the
         * from the QuerierRtrPortBitmap of the VLAN entry */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->QuerierRtrPortBitmap,
                               bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                      pSnoopVlanEntry->QuerierRtrPortBitmap);
        }
    }

    return SNOOP_SUCCESS;
}
