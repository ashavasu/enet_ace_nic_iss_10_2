/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpmbsm.c,v 1.33 2017/11/28 14:18:43 siva Exp $
 *
 *******************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : snpmbsm.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping  module                               */
/*    MODULE NAME           : Snooping module Card Updation                  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 08 July 2005                                   */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Card Insertion functions for*/
/*                            the Snooping  module.                          */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      :  SnoopMbsmUpdateCardInsertion                        */
/*                                                                           */
/* Description        : This function programs the HW with the Snooping      */
/*                      software configuration.                              */
/*                                                                           */
/*                      This function will be called from the Snooping module*/
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmPortInfo - Structure containing the PortList,  */
/*                                       Starting Index and the port count.  */
/*                       tMbsmSlotInfo - Structure containing the SlotId     */
/*                                       info.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopMbsmUpdateCardInsertion (tMbsmPortInfo * pPortInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tPortList           SnpUntagPortList;
    tIgsIPMCInfoArray   IgsIPMCInfoArray;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopPortBmp       SlotPorts;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntry = NULL;
    tIgsHwRateLmt       IgsHwRateLmt;
    UINT4               u4ContextId = 0;
    UINT4               u4Instance = 0;
    UINT4               u4Index = 0;
    UINT1               u1AddressType = 1;
    UINT1               u1EnhancedStatus = SNOOP_FALSE;
    UINT1               u1SparseStatus = SNOOP_FALSE;
    UINT1               u1Result = SNOOP_FALSE;
    BOOL1               bResult = SNOOP_FALSE;

    SNOOP_MEM_SET (SlotPorts, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&IgsHwRateLmt, 0, sizeof (tIgsHwRateLmt));
    SNOOP_MEM_SET (&SnpUntagPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&IgsIPMCInfoArray, 0, sizeof (tIgsIPMCInfoArray));

    for (u4ContextId = 0; u4ContextId < SNOOP_MAX_INSTANCES; u4ContextId++)
    {
        u4Instance = SNOOP_GET_INSTANCE_ID (u4ContextId);

        if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
        {
            /* Go for next active context */
            continue;
        }

        u1EnhancedStatus = SNOOP_INSTANCE_ENH_MODE (u4Instance);
        u1SparseStatus = SNOOP_SYSTEM_SPARSE_MODE (u4Instance);

        for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO;
             u1AddressType++)
        {
            if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddressType - 1))
                == SNOOP_DISABLE)
            {
                continue;
            }

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {

#ifdef IGS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    {
                        /* Enable MAC based Forwarding at Hardware */
                        if (IgsFsMiIgsMbsmHwEnableIgmpSnooping
                            (u4Instance, pSlotInfo) == FNP_FAILURE)
                        {
                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_DBG_INIT |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_INIT_NAME,
                                                "Unable to enable IGS in Hardware for "
                                                "instance %u\r\n", u4Instance);
                            return MBSM_FAILURE;
                        }

                        /* Program IGS Sparse mode Status at Hardware */
                        if (IgsFsMiIgsMbsmHwSparseMode (u4Instance,
                                                        u1SparseStatus,
                                                        pSlotInfo) ==
                            FNP_FAILURE)
                        {

                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_DBG_INIT |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_INIT_NAME,
                                                "Unable to set IGS sparse mode"
                                                "in Hardware for instance %u\r\n",
                                                u4Instance);
                            return MBSM_FAILURE;
                        }
                    }
                }
#endif
#ifdef MLDS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    {
                        /* Enable MAC based Forwarding at Hardware */
                        if (MldsFsMiMldsMbsmHwEnableMldSnooping
                            (u4Instance, pSlotInfo) == FNP_FAILURE)
                        {

                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_DBG_INIT |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_INIT_NAME,
                                                "Unable to enable MLDS in Hardware for "
                                                "instance %u \r\n", u4Instance);
                            return MBSM_FAILURE;
                        }
                    }
                }
#endif
            }
            else
            {
#ifdef IGS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    {
                        /* Enable IP based Forwarding at Hardware */
                        if (IgsFsMiIgsMbsmHwEnableIpIgmpSnooping
                            (u4Instance, pSlotInfo) == FNP_FAILURE)
                        {
                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_DBG_INIT |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_INIT_NAME,
                                                "Unable to enable IGS in Hardware"
                                                " for instance %u\r\n",
                                                u4Instance);
                            return MBSM_FAILURE;
                        }

                        /* Enable IGS enhanced  mode at Hardware */
                        if (u1EnhancedStatus == SNOOP_ENABLE)
                        {
                            if (IgsFsMiIgsMbsmHwEnhancedMode
                                (u4Instance, u1EnhancedStatus,
                                 pSlotInfo) == FNP_FAILURE)
                            {
                                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                    (u4Instance),
                                                    SNOOP_DBG_INIT |
                                                    SNOOP_DBG_ALL_FAILURE,
                                                    SNOOP_INIT_NAME,
                                                    "Unable to enable IGS Enhanced mode"
                                                    "in Hardware for instance %u\r\n",
                                                    u4Instance);
                                return MBSM_FAILURE;
                            }

                        }
                    }
                    /* Program IGS Sparse mode Status at Hardware */
                    if (IgsFsMiIgsMbsmHwSparseMode (u4Instance,
                                                    u1SparseStatus,
                                                    pSlotInfo) == FNP_FAILURE)
                    {

                        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                            SNOOP_DBG_INIT |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_INIT_NAME,
                                            "Unable to set IGS sparse mode"
                                            "in Hardware for instance %u\r\n",
                                            u4Instance);
                        return MBSM_FAILURE;
                    }

                }
#endif
#ifdef MLDS_WANTED
                if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                    {
                        if (MldsFsMiMldsMbsmHwEnableIpMldSnooping
                            (u4Instance, pSlotInfo) == FNP_FAILURE)
                        {
                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_DBG_INIT |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_INIT_NAME,
                                                "Unable to enable MLDS in Hardware"
                                                "for instance %u \r\n",
                                                u4Instance);
                            return MBSM_FAILURE;
                        }
                    }
                }
#endif
                /* Program all the IP forwarding entries in the hardware */

                pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                          IpMcastFwdEntry);

                while (pRBElem != NULL)
                {
                    pRBNextElem =
                        RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                       IpMcastFwdEntry, pRBElem, NULL);

                    pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                    if (pSnoopIpGrpFwdEntry->GrpIpAddr.u1Afi != u1AddressType)
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }
                    /* MBSM programming is initiated by batching the IPMC entries by the count of 
                     * MAX_SNP_IPMC_INFO.
                     */
                    MEMSET (IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].PortList,
                            0, sizeof (tPortList));
                    MEMSET (IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].
                            UntagPortList, 0, sizeof (tPortList));
                    if (SnoopGetPhyPortBmp
                        (u4Instance, pSnoopIpGrpFwdEntry->PortBitmap,
                         IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].PortList) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_DBG,
                                   "Unable to get the physical port bitmap from the virtual"
                                   " port bitmap\r\n");
                        return SNOOP_FAILURE;
                    }

                    IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].u4Instance =
                        u4Instance;
                    IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].u4GrpAddr =
                        SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->GrpIpAddr.
                                           au1Addr);
                    IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].u4SrcAddr =
                        SNOOP_PTR_FETCH_4 (pSnoopIpGrpFwdEntry->SrcIpAddr.
                                           au1Addr);
                    IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].OuterVlanId =
                        SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId);
                    IgsIPMCInfoArray.aIgsHwIpFwdInfo[u4Index].u4SnoopHwId =
                        pSnoopIpGrpFwdEntry->u4SnoopHwId;

                    u4Index++;
                    if (u4Index == MAX_SNP_IPMC_INFO)
                    {
                        IgsIPMCInfoArray.u4IgsHwIPMCCount = MAX_SNP_IPMC_INFO;
#ifdef NPAPI_WANTED
                        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
                        {
                            if (IgsFsMiIgsMbsmSyncIPMCInfo
                                (&IgsIPMCInfoArray, pSlotInfo) != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                                SNOOP_CONTROL_PATH_TRC,
                                                SNOOP_FWD_TRC,
                                                "Mcast IP forwarding entry failed for instance %u\r\n",
                                                u4Instance);
                            }
                        }
#endif
                        u4Index = 0;
                    }

                    pRBElem = pRBNextElem;
                }
                if (u4Index != 0)
                {
                    IgsIPMCInfoArray.u4IgsHwIPMCCount = u4Index;
#ifdef NPAPI_WANTED
                    if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
                    {
                        if (IgsFsMiIgsMbsmSyncIPMCInfo
                            (&IgsIPMCInfoArray, pSlotInfo) != SNOOP_SUCCESS)
                        {
                            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_CONTROL_PATH_TRC |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_FWD_NAME,
                                                "Mcast IP forwarding entry failed for instance %u\r\n",
                                                u4Instance);
                        }
                    }
#endif
                    u4Index = 0;

                }
            }
        }

        SNOOP_MEM_CPY (SlotPorts, MBSM_PORT_INFO_PORTLIST (pPortInfo),
                       BRG_PHY_PORT_LIST_SIZE);

        /* scan all the PortCfgEntries added for the context */
        if (SNOOP_INSTANCE_INFO (u4Instance)->PortCfgEntry == NULL)
        {
            /* No Port configurations are made in this context */
            continue;
        }
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO
                                  (u4Instance)->PortCfgEntry);
        while (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO
                                         (u4Instance)->PortCfgEntry,
                                         pRBElem, NULL);

            pSnoopPortCfgEntry = (tSnoopPortCfgEntry *) pRBElem;
            pSnoopExtPortCfgEntry = pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
            if ((pSnoopExtPortCfgEntry == NULL) ||
                (pSnoopExtPortCfgEntry->u4SnoopPortRateLimit
                 == SNOOP_PORT_RATE_LMT_DEF))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            /* The port may represent either the physical interface or
             * the SISP logical interface. If it is SISP logical interface,then
             * it should be checked using the corresponding physical ifIndex
             * for its presence in the attached slot.
             * */

            SNOOP_IS_SISP_PORT (pSnoopPortCfgEntry->u4SnoopPortIndex, u1Result);

            if (u1Result == SNOOP_TRUE)
            {
                pSnoopPortCfgEntry->u4SnoopPortIndex =
                    SNOOP_GET_PHY_PORT (pSnoopPortCfgEntry->u4SnoopPortIndex);
            }

            SNOOP_IS_PORT_PRESENT (pSnoopPortCfgEntry->u4SnoopPortIndex,
                                   SlotPorts, bResult);

            if (bResult == OSIX_FALSE)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            IgsHwRateLmt.u4Port = pSnoopPortCfgEntry->u4SnoopPortIndex;
            IgsHwRateLmt.InnerVlanId =
                SNOOP_INNER_VLAN (pSnoopPortCfgEntry->VlanId);
            IgsHwRateLmt.u4Rate = pSnoopExtPortCfgEntry->u4SnoopPortRateLimit;

#ifdef IGS_WANTED
            if (pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype
                == SNOOP_ADDR_TYPE_IPV4)
            {
                if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
                {
                    if (IgsFsMiIgsMbsmHwPortRateLimit
                        (u4Instance, &IgsHwRateLmt, (UINT1) SNOOP_RATELIMIT_ADD,
                         pSlotInfo) == FNP_FAILURE)
                    {
                        SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                            SNOOP_DBG_INIT |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_INIT_DBG,
                                            "Failed to set the rate"
                                            " limit for the port %ld and inner vlan"
                                            " %d\r\n",
                                            pSnoopPortCfgEntry->
                                            u4SnoopPortIndex,
                                            SNOOP_INNER_VLAN
                                            (pSnoopPortCfgEntry->VlanId));
                    }
                }
            }
#endif
            pRBElem = pRBNextElem;
        }

    }                            /* For all contexts */

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :SnoopMbsmProcessUpdateMessage                         */
/*                                                                           */
/* Description        : This function constructs the Snoop Q Mesg and calls  */
/*                      the Snoop MSG event to process this Mesg.            */
/*                                                                           */
/*                      This function will be called from the LA module      */
/*                      when an indication for the Card Insertion is         */
/*                      received from the MBSM.                              */
/*                                                                           */
/* Input(s)           :  tMbsmProtoMsg - Structure containing the SlotInfo,  */
/*                                       PortInfo and the Protocol Id.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
SnoopMbsmProcessUpdateMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Cmd)
{
    tSnoopMsgNode      *pSnoopMsg = NULL;

    if (pProtoMsg == NULL)
        return MBSM_FAILURE;

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_MEM_NAME, "SNOOP Task is not Initialised\n");
        return MBSM_SUCCESS;
    }
    if ((pSnoopMsg = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_MEM_NAME,
                       "Memory allocation failed for MBSM queue message\n");
        return MBSM_FAILURE;
    }

    MEMSET (pSnoopMsg, 0, sizeof (tSnoopMsgNode));

    if (!(pSnoopMsg->uInMsg.MbsmCardUpdate.pMbsmProtoMsg =
          MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
    {
        SNOOP_RELEASE_QMSG_MEM_BLOCK (pSnoopMsg);
        return MBSM_FAILURE;
    }

    pSnoopMsg->u2MsgType = (UINT2) i4Cmd;
    MEMCPY (pSnoopMsg->uInMsg.MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
            sizeof (tMbsmProtoMsg));

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pSnoopMsg,
                             SNOOP_DEF_MSG_LEN) == OSIX_SUCCESS)
    {
        if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT) != OSIX_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME,
                           "Sending SNOOP_MSG_EVENT to for MBSM IGS Task "
                           "FAILED!\n");
        }
        return MBSM_SUCCESS;
    }

    SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_MEM_NAME,
                   "Posting Message to Snoop Queue FAILED!\n");

    MEM_FREE (pSnoopMsg->uInMsg.MbsmCardUpdate.pMbsmProtoMsg);

    SNOOP_RELEASE_QMSG_MEM_BLOCK (pSnoopMsg);
    return MBSM_FAILURE;
}
