/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpmain.c,v 1.100.2.1 2018/03/22 13:07:37 siva Exp $
 *
 * Description: This file contains init, deinit functions for 
 *              snooping(MLD/IGMP) module.
 *****************************************************************************/
/*****************************************************************************/
/*    FILE  NAMEn           : snpmain.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Main Module                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains init, deinit funtions       */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
#ifndef __SNPMAIN_C__
#define __SNPMAIN_C__

#include "snpinc.h"
#include "snpglob.h"
#include "mux.h"

#ifdef SNMP_2_WANTED
#include "fssnpwr.h"
#endif /*SNMP_2_WANTED */
/*****************************************************************************/
/* Function Name      : SnoopMain                                            */
/*                                                                           */
/* Description        : This function is the main entry point function for   */
/*                      the SnpT task                                        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMain (INT1 *pi1Param)
{
    UINT4               u4Events = 0;
    INT4                i4SysLogId = 0;

    UNUSED_PARAM (pi1Param);

    if (SnoopTaskInit () != SNOOP_SUCCESS)
    {
        SnoopTaskDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (SnoopMainStartModule () != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_MEM_NAME, "SNOOP start of resources "
                       "failed\r\n");
        SnoopTaskDeInit ();
        SnoopRedDeRegisterWithRM ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    if (SNOOP_GET_TASK_ID (&SNOOP_TASK_ID) != OSIX_SUCCESS)
    {
        SnoopTaskDeInit ();
        SnoopMainModuleShutdown ();
        SnoopRedDeRegisterWithRM ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register with SysLog Server to Log Error Messages. */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) SNOOP_TASK_NAME,
                          SYSLOG_CRITICAL_LEVEL);

    if (i4SysLogId < 0)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_MEM_NAME, "SNOOP syslog registration "
                       "failed \r\n");
        SNOOP_DELETE_MEM_POOL (SNOOP_GLOBAL_MEMPOOL_ID);
        SNOOP_DELETE_MEM_POOL (SNOOP_QPKT_MEMPOOL_ID);
        SNOOP_DELETE_MEM_POOL (SNOOP_QMSG_MEMPOOL_ID);
        SNOOP_DELETE_QUEUE (SNOOP_MSG_QUEUE_ID);
        SNOOP_DELETE_QUEUE (SNOOP_PKT_QUEUE_ID);
        SNOOP_DELETE_SEM (gSnoopSemId);
        SnoopMemGlobalMemPoolDelete ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }
    gu4IgsSysLogId = i4SysLogId;

    /* Indicate the status of initialization to the main routine */
    lrInitComplete (OSIX_SUCCESS);

#ifdef SNMP_3_WANTED
    RegisterFSSNP ();
#endif /*SNMP_3_WANTED */

    while (1)
    {
        if (SNOOP_RECEIVE_EVENT (SNOOP_TASK_ID, SNOOP_IGS_PKT_ENQ_EVENT |
                                 SNOOP_MLDS_PKT_ENQ_EVENT | SNOOP_MSG_EVENT |
                                 SNOOP_TMR_EXPIRY_EVENT |
                                 SNOOP_RED_GO_ACTIVE_EVENT |
                                 SNOOP_MSR_COMPLETE_EVENT |
                                 SNOOP_RED_BULK_UPD_EVENT,
                                 OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (SNOOP_LOCK () == SNMP_FAILURE)
            {
                continue;
            }

#ifdef IGS_WANTED
            if ((u4Events & SNOOP_IGS_PKT_ENQ_EVENT))
            {
                SnoopMainProcessPktArrivalEvent (SNOOP_ADDR_TYPE_IPV4);
            }
#endif

#ifdef MLDS_WANTED
            if ((u4Events & SNOOP_MLDS_PKT_ENQ_EVENT))
            {
                SnoopMainProcessPktArrivalEvent (SNOOP_ADDR_TYPE_IPV6);
            }
#endif

            if (u4Events & SNOOP_MSG_EVENT)
            {
                SnoopMainProcessQMsgEvent ();
            }

            if (u4Events & SNOOP_TMR_EXPIRY_EVENT)
            {
                SnoopTmrExpiryHandler ();
            }

            if (u4Events & SNOOP_MSR_COMPLETE_EVENT)
            {
                SnoopHandleMsrRestoreEvent ();
            }
#ifdef L2RED_WANTED
            if (u4Events & SNOOP_RED_BULK_UPD_EVENT)
            {
                SnoopRedHandleBulkUpdEvent ();
            }
            if (u4Events & SNOOP_RED_GO_ACTIVE_EVENT)
            {
                SnoopRedMakeNodeActive ();
            }
#endif

            SNOOP_UNLOCK ();
        }
    }
}

/*****************************************************************************/
/* Function Name      : SnoopTaskDeInit                                      */
/*                                                                           */
/* Description        : This function will deinitialise the mempools, Queues */
/*                      semapores created during SNOOP task up.              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopTaskDeInit (VOID)
{
    if (SNOOP_GLOBAL_MEMPOOL_ID != 0)
    {
        SNOOP_DELETE_MEM_POOL (SNOOP_GLOBAL_MEMPOOL_ID);
        SNOOP_GLOBAL_MEMPOOL_ID = 0;
    }
    if (SNOOP_QPKT_MEMPOOL_ID != 0)
    {
        SNOOP_DELETE_MEM_POOL (SNOOP_QPKT_MEMPOOL_ID);
        SNOOP_QPKT_MEMPOOL_ID = 0;
    }
    if (SNOOP_QMSG_MEMPOOL_ID != 0)
    {
        SNOOP_DELETE_MEM_POOL (SNOOP_QMSG_MEMPOOL_ID);
        SNOOP_QMSG_MEMPOOL_ID = 0;
    }
    if (SNOOP_MSG_QUEUE_ID != 0)
    {
        SNOOP_DELETE_QUEUE (SNOOP_MSG_QUEUE_ID);
        SNOOP_MSG_QUEUE_ID = 0;
    }
    if (SNOOP_PKT_QUEUE_ID != 0)
    {
        SNOOP_DELETE_QUEUE (SNOOP_PKT_QUEUE_ID);
        SNOOP_PKT_QUEUE_ID = 0;
    }
    if (gSnoopSemId != 0)
    {
        SNOOP_DELETE_SEM (gSnoopSemId);
        gSnoopSemId = 0;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopTaskInit                                        */
/*                                                                           */
/* Description        : This function will start the Snooping task resources */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopTaskInit (VOID)
{

    /* Create semaphore for mutual exclusion */
    if (SNOOP_CREATE_SEM (SNOOP_MUTUAL_EXCL_SEM_NAME, SNOOP_SEM_COUNT,
                          SNOOP_SEM_FLAGS, &gSnoopSemId) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP mutual exclusion semaphore "
                       "creation failed\r\n");
        return SNOOP_FAILURE;
    }

    /* Message Q for handling events handling */
    if (SNOOP_CREATE_QUEUE (SNOOP_MSG_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                            FsSNPSizingParams[MAX_SNOOP_Q_MSG_SIZING_ID].
                            u4PreAllocatedUnits,
                            &(SNOOP_MSG_QUEUE_ID)) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP message queue creation "
                       "failed\r\n");
        return SNOOP_FAILURE;
    }

    /* Packet Q for handling packets */
    if (SNOOP_CREATE_QUEUE (SNOOP_PKT_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                            FsSNPSizingParams[MAX_SNOOP_QUE_PKT_MSG_SIZING_ID].
                            u4PreAllocatedUnits,
                            &(SNOOP_PKT_QUEUE_ID)) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP packet queue creation "
                       "failed\r\n");
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMainStartModule                                 */
/*                                                                           */
/* Description        : This function will start Snooping module             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopMainStartModule (VOID)
{
    UINT4               u4SnoopFwdMode = 0;

    u4SnoopFwdMode = SnoopGetFwdModeFromNvRam ();
    if (SnoopMainModuleInit ((UINT1) u4SnoopFwdMode) != SNOOP_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP main module initialization "
                       "failed \r\n");
        SnoopMainModuleShutdown ();
        return SNOOP_FAILURE;
    }

    gu1SnoopInit = SNOOP_TRUE;
    /* Register SNOOP Task with Redundancy Manager RM,
     * to receive messages from peer node.
     */
    if (SnoopRedRegisterWithRM () != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "IgsRed: "
                       "Registering with RM Failed\n");
        SnoopMainModuleShutdown ();
        return SNOOP_FAILURE;
    }

    if (SnoopRegisterWithPacketHandler () != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Registering with Packet Handler Failed\r\n");
        SnoopRedDeRegisterWithRM ();
        SnoopMainModuleShutdown ();
        return SNOOP_FAILURE;
    }
    MEMSET (gSnoopLocalPortList, 0, sizeof (tLocalPortList));
    MEMSET (gSnoopIfPortList, 0, sizeof (tSnoopIfPortBmp));

#ifdef ICCH_WANTED
    /* Register vlan with ICCH module */
    if (SnoopIcchRegisterWithICCH () != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_INIT, SNOOP_GBL_NAME,
                       "Registering with ICCH Failed\r\n");
        SnoopRedDeRegisterWithRM ();
        SnoopMainModuleShutdown ();
        return SNOOP_FAILURE;
    }
#endif

#ifdef L2RED_WANTED
    MEMSET (&gSnoopRedGlobalInfo, 0, sizeof (tSnoopRedGlobalInfo));
    SNOOP_NODE_STATUS () = SNOOP_IDLE_NODE;
    SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;
    SNOOP_NUM_STANDBY_NODES () = 0;
    SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () = 0;
    SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
    SNOOP_RED_BULK_UPD_VLAN_ENTRY () = NULL;
    SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = NULL;
    SnoopRedInitGlobalInfo ();
    gu1SnoopLaCardRemovalProcessed = SNOOP_FALSE;
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMainModuleShutdown                              */
/*                                                                           */
/* Description        : This function will shutdown the Snooping module      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopMainModuleShutdown (VOID)
{
    tSnoopQMsg         *pFrameMsg = NULL;
    tSnoopMsgNode      *pMsgNode = NULL;
    UINT4               u4Instance = 0;

#ifdef L2RED_WANTED
    tRmNodeInfo        *pData = NULL;

    SNOOP_NODE_STATUS () = SNOOP_SHUT_START_IN_PROGRESS;
#endif
    gu1SnoopInit = SNOOP_FALSE;
    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        /* This is done for trace messages */
        gu4SnoopTrcInstId = u4Instance;
        gu4SnoopDbgInstId = u4Instance;

        if ((SNOOP_INSTANCE_INFO (u4Instance) == NULL) ||
            (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN))
        {
            continue;
        }
        SnoopModuleShutdownInstance (u4Instance);
        SNOOP_RELEASE_GINSTANCE_MEM_BLOCK (SNOOP_INSTANCE_INFO (u4Instance));
        SNOOP_INSTANCE_INFO (u4Instance) = NULL;
    }
    /* Release source information buffer */
    if (gpSSMSrcInfoBuffer != NULL)
    {
        SNOOP_RELEASE_SRC_MEM_BLOCK (gpSSMSrcInfoBuffer);
        gpSSMSrcInfoBuffer = NULL;
    }
    /* Release SSM report buffer */
    if (gpSSMRepSendBuffer != NULL)
    {
        SNOOP_RELEASE_MISC_MEM_BLOCK (gpSSMRepSendBuffer);
        gpSSMRepSendBuffer = NULL;
    }
    /* Release data information buffer */
    if (gpSnoopDataBuffer != NULL)
    {
        SNOOP_RELEASE_DATA_MEM_BLOCK (gpSnoopDataBuffer);
        gpSnoopDataBuffer = NULL;
    }

    if (gRtrPortTblRBRoot != NULL)
    {
        RBTreeDelete (gRtrPortTblRBRoot);
        gRtrPortTblRBRoot = NULL;
    }
    if (SNOOP_TMR_LIST_ID != 0)
    {
        if (TmrDeleteTimerList (SNOOP_TMR_LIST_ID) != TMR_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Unable to delete the global timer "
                           "list\r\n");
        }
        SNOOP_TMR_LIST_ID = 0;
    }
#ifdef L2RED_WANTED
    SnoopRedDeInitGlobalInfo ();
#endif
    while (SNOOP_RECV_FROM_QUEUE (SNOOP_PKT_QUEUE_ID, (UINT1 *) &pFrameMsg,
                                  SNOOP_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        if (CRU_BUF_Release_MsgBufChain (pFrameMsg->pInQMsg, FALSE)
            == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_INIT_TRC,
                       "CRU buf release failed in incoming packet "
                       "processing\r\n");
        }

        if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pFrameMsg) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Queue message release failed in "
                           "incoming packet processing\r\n");
        }
    }

    while (SNOOP_RECV_FROM_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pMsgNode,
                                  SNOOP_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        switch (pMsgNode->u2MsgType)
        {
            case SNOOP_CREATE_CONTEXT:
            case SNOOP_DELETE_CONTEXT:
            case SNOOP_MAP_PORT:
            case SNOOP_UNMAP_PORT:
                L2MI_SYNC_GIVE_SEM ();
                break;
            case SNOOP_CREATE_PORT_MSG:
                L2_SYNC_GIVE_SEM ();
                break;
#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                MEM_FREE (pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg);
                break;
#endif
#ifdef L2RED_WANTED
            case SNOOP_RM_DATA:
                if ((pMsgNode->uInMsg.RmData.u4Events == RM_STANDBY_UP) ||
                    (pMsgNode->uInMsg.RmData.u4Events == RM_STANDBY_DOWN))
                {
                    pData = (tRmNodeInfo *) pMsgNode->uInMsg.RmData.pRmMsg;
                    SnoopRmReleaseMemoryForMsg ((UINT1 *) pData);
                }
                else if (pMsgNode->uInMsg.RmData.u4Events == RM_MESSAGE)
                {
                    RM_FREE ((tRmMsg *) pMsgNode->uInMsg.RmData.pRmMsg);
                }
                break;
#endif
            default:
                break;
        }
        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pMsgNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Queue message release failed for "
                           "events from lower layer\r\n");
        }
    }
    /* Deletion of memory pool information for the SNOOP module */
    SnpSizingMemDeleteMemPools ();

    /* Delete the Global port config RBTree */
    SnoopMemDeleteGlbRBTreePortConfEntry ();

    /* Unregister with the packet Handler */
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_SNOOP_CONTROL);
#ifdef MLDS_WANTED
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_MLD_QUERY);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_MLD_DONE);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_MLDV1_REPORT);
    CfaMuxUnRegisterProtocolCallBack (PACKET_HDLR_PROTO_MLDV2_REPORT);
#endif

#ifdef L2RED_WANTED
    MEMSET (&gSnoopRedGlobalInfo, 0, sizeof (tSnoopRedGlobalInfo));
    if (SnoopRedDeRegisterWithRM () == SNOOP_FAILURE)
    {
        return SNOOP_FAILURE;
    }
    SNOOP_NODE_STATUS () = SNOOP_IDLE_NODE;
    SNOOP_RED_BULK_REQ_RECD () = SNOOP_FALSE;
    SNOOP_NUM_STANDBY_NODES () = 0;
    SNOOP_RED_BULK_UPD_NEXT_CONTEXTID () = 0;
    SNOOP_RED_BULK_UPD_FLAG = SNOOP_FALSE;
    SNOOP_RED_BULK_UPD_VLAN_ENTRY () = NULL;
    SNOOP_RED_BULK_UPD_MAC_FWD_ENTRY () = NULL;
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMainModuleInit                                  */
/*                                                                           */
/* Description        : This function initialises the snooping module        */
/*                      related data structures                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopMainModuleInit (UINT1 u1McastMode)
{
    UINT4               u4Instance = 0;

    /*  Memory pool creation for SNOOP global structure */
    if (SnpSizingMemCreateMemPools () != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP memory pool creation "
                       "failed\r\n");
        return SNOOP_FAILURE;
    }

    /*Assigning respective mempool Id's */
    SnoopMemAssignMempoolIds (u4Instance);

    /* allocate the memory for Default instance, if not allocated */
    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        if (SNOOP_GINSTANCE_ALLOC_MEMBLK (SNOOP_INSTANCE_INFO (u4Instance))
            == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "SNOOP global instance allocation "
                           "failed\r\n");
            return SNOOP_FAILURE;

        }
    }
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (u4Instance), 0,
                   sizeof (tSnoopGlobalInstInfo));

    SNOOP_SYSTEM_LEAVE_LEVEL (u4Instance) = SNOOP_VLAN_LEAVE_CONFIG;

    /* Global RBTree creation for Port configuration information */
    if (SnoopMemCreateGlbRBTreePortConfEntry () == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME, "Global RBTree creation for "
                       "Port config memory pool creation failed\r\n");
        return SNOOP_FAILURE;

    }

#ifdef L2RED_WANTED
    SNOOP_RED_BULK_SYNC_INPROGRESS = SNOOP_FALSE;
#endif
    SNOOP_INSTANCE_ENH_MODE (u4Instance) = SNOOP_DISABLE;
    SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) = SNOOP_DISABLE;
#ifdef ICCH_WANTED
    SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) = SNOOP_DISABLE;
#endif

    if (SnoopInitInstInfo (u4Instance, u1McastMode) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_NAME, "SNOOP Initialization for "
                            "instance %d failed\r\n", u4Instance);
        return SNOOP_FAILURE;

    }

    SnoopInitInstName (u4Instance);
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* memory allocation for SSM report buffer which is used for contructing 
     * SSM report */
    if (gpSSMRepSendBuffer == NULL)
    {
        if (SNOOP_MISC_ALLOC_MEMBLK (gpSSMRepSendBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed while allocating misc "
                           "buffer failed\r\n");
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_INIT | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_INIT_NAME,
                           "Failure while initializing snooping main module\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "while initializing snooping main module\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* memory allocation for source information which is used for
     * constructing source information in IP based forwarding mode 
     * Since this is a global pointer and is used by all the instances
     * allocation is done even if the forwarding mode in this instance
     * is MAC-based */
    if (gpSSMSrcInfoBuffer == NULL)
    {
        if (SNOOP_SRC_ALLOC_MEMBLK (gpSSMSrcInfoBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "Memory allocation failed "
                           "for misc buffer for sending source "
                           "information\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "misc buffer for sending source\r\n");
            return SNOOP_FAILURE;
        }
    }
    MEMSET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

    /* memory allocation for data buffer which is used for duplicating 
     * data packet */
    if (gpSnoopDataBuffer == NULL)
    {
        if (SNOOP_DATA_ALLOC_MEMBLK (gpSnoopDataBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "Memory allocation failed "
                           "for data buffer for sending data "
                           "information\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* Initialise the per instance port array with zeros */
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (SNOOP_DEFAULT_INSTANCE)->
                   au4SnoopPhyPort, 0, SNOOP_MAX_PORTS_PER_INSTANCE);
    if (TmrCreateTimerList
        ((CONST UINT1 *) SNOOP_TASK_NAME, SNOOP_TMR_EXPIRY_EVENT, NULL,
         &SNOOP_TMR_LIST_ID) != TMR_SUCCESS)

    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP global Timer list creation "
                       "failed\r\n");
        return SNOOP_FAILURE;
    }

    SnoopGetAllPorts ();
    /* Initialize the switch Ip to be used in proxy mode */
    SnoopConfigSwitchIp ();
    /* Create a global RBTree to maintain router port information
     * for management operation 
     */
    if ((gRtrPortTblRBRoot = RBTreeCreateEmbedded (FSAP_OFFSETOF
                                                   (tSnoopRtrPortEntry,
                                                    RbNode),
                                                   SnoopUtilRBTreeRtrPortEntryCmp))
        == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "Failure in creation of SNOOP global"
                       "RBTree for maintaining router ports\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopInitInstInfo                                    */
/*                                                                           */
/* Description        : Initialises the Instance specific Snooping           */
/*                      information                                          */
/*                                                                           */
/* Input(s)           : u4Instance                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_FAILURE on Error else, SNOOP_SUCCESS           */
/*****************************************************************************/
INT4
SnoopInitInstInfo (UINT4 u4Instance, UINT1 u1McastFwdMode)
{

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* Initialises the Instance specific Snooping information */
    SnoopMainSetDefaultInstInfo (u4Instance, u1McastFwdMode);

    /* RBTree Node creation for the Group Entry, MAC forward entrya,
     * IP forward entry and VLAN entry  per instance 
     */

    if (SnoopMemCreateInstanceInfo (u4Instance) != SNOOP_SUCCESS)
    {
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeInitInstInfo                                    */
/*                                                                           */
/* Description        : De-Initialises the Instance specific Snooping        */
/*                      information                                          */
/*                                                                           */
/* Input(s)           : u4Instance                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopDeInitInstInfo (UINT4 u4Instance)
{
    /* Deletion of Hash and RBTree Information for the 
     * VLAN and Group & Forwarding entries.
     */
    SNOOP_VALIDATE_INSTANCE (u4Instance);

    SnoopMemDeleteInstanceInfo (u4Instance);

    /* Reset the system status */
    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_SHUTDOWN;
}

/*****************************************************************************/
/* Function Name      : SnoopMainSetDefaultInstInfo                          */
/*                                                                           */
/* Description        : Initialises the Instance specific Snooping           */
/*                      global information                                   */
/*                                                                           */
/* Input(s)           : u4Instance                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMainSetDefaultInstInfo (UINT4 u4Instance, UINT1 u1McastMode)
{
    UINT1               u1AddrType = 0;

    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_STARTED;

    SNOOP_INSTANCE_INFO (u4Instance)->u1McastFwdMode = u1McastMode;
    for (u1AddrType = 0; u1AddrType < SNOOP_MAX_PROTO; u1AddrType++)
    {
        /* IGS/MLDS Admin Value should be Disabled */
        SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance, u1AddrType) = SNOOP_DISABLE;

        /* IGMP and MLD snooping disabled in startup by default */
        SNOOP_SNOOPING_STATUS (u4Instance, u1AddrType) = SNOOP_DISABLE;
        /* Proxy Disabled in startup by default */
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1ProxyStatus
            = SNOOP_PROXY_REPORTING_MODE;
        /* set default duration for timers */
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].
            u4RtrPortPurgeInt = SNOOP_DEF_RTR_PURGE_INTERVAL;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u4ReportFwdInt
            = SNOOP_DEF_REPORT_FWD_INTERVAL;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u4GrpQueryInt
            = SNOOP_DEF_GRP_QUERY_INTERVAL;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u4TraceOption
            = SNOOP_DEF_TRACE_OPTION;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1RetryCount
            = SNOOP_DEF_RETRY_COUNT;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1ReportFwdAll
            = SNOOP_FORWARD_RTR_PORTS;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1MVlanStatus
            = SNOOP_DISABLE;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1FilterStatus
            = SNOOP_DISABLE;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].
            u1SnoopQueryStatus = SNOOP_DISABLE;
        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u4PortPurgeInt
            =
            (SNOOP_DEF_QUERY_INTERVAL * SNOOP_ROBUST_MIN_VALUE) +
            SNOOP_IGS_MRC_UNIT;

        SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType].u1QueryFwdAll
            = SNOOP_FORWARD_NON_RTR_PORTS;

        SNOOP_SLL_INIT (&(SNOOP_INSTANCE_INFO (u4Instance)->MVlanMappingList));
    }
    SNOOP_SYSTEM_LEAVE_LEVEL (u4Instance) = SNOOP_VLAN_LEAVE_CONFIG;
    SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) = SNOOP_NON_ROUTER_REPORT_CONFIG;
    SNOOP_SYSTEM_SPARSE_MODE (u4Instance) = SNOOP_DISABLE;

    /*Added IgsFsMiIgsHwSparseMode to provide sparse mode support for bcmx */
#ifdef NPAPI_WANTED
#ifdef IGS_WANTED
    if (IgsFsMiIgsHwSparseMode
        (u4Instance, SNOOP_SYSTEM_SPARSE_MODE (u4Instance)) == FNP_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_INIT_DBG, SNOOP_INIT_DBG,
                   "H/W updation for sparse mode is failed \r\n");
    }
#endif
#endif

    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (u4Instance)->au1SnoopTrcMode,
                   0, SNOOP_TRC_NAME_SIZE);

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMainProcessPktArrivalEvent                      */
/*                                                                           */
/* Description        : This function handles the IGMP/MLD packet arrival    */
/*                      event.                                               */
/*                                                                           */
/* Input(s)           : u1AddressType - specifies IGMP/MLD packet            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMainProcessPktArrivalEvent (UINT1 u1AddressType)
{
    tSnoopQMsg         *pFrameMsg = NULL;
    INT4                i4IgsMsgCount = 0;
    INT4                i4MldsMsgCount = 0;

    while (SNOOP_RECV_FROM_QUEUE (SNOOP_PKT_QUEUE_ID, (UINT1 *) &pFrameMsg,
                                  SNOOP_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
#ifdef L2RED_WANTED
        if (SnoopRmGetNodeState () != RM_ACTIVE)
        {
            if (CRU_BUF_Release_MsgBufChain (pFrameMsg->pInQMsg, FALSE)
                == CRU_FAILURE)
            {
                SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC,
                           SNOOP_INIT_TRC, "CRU buffer release failed in "
                           "incoming IGMP packet processing\r\n");
            }
            return;
        }
#endif
        if (pFrameMsg->u1PktType == SNOOP_RTR_ADVT_PKT)
        {
            if (SnoopVlanUpdateTagFrame (pFrameMsg->pInQMsg,
                                         &(pFrameMsg->u2LocalPort),
                                         &(pFrameMsg->VlanInfo),
                                         &(pFrameMsg->u2TagLen)) !=
                SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC,
                           SNOOP_INIT_TRC, "Unable to update VlanTagInfo "
                           "in OSPF/PIM packet processing\r\n");

                if (CRU_BUF_Release_MsgBufChain (pFrameMsg->pInQMsg, FALSE)
                    == CRU_FAILURE)
                {
                    SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC,
                               SNOOP_INIT_TRC, "CRU buffer release failed in "
                               "incoming IGMP packet processing\r\n");
                }

                return;
            }
        }
#ifdef IGS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (IgsDecodeIncomingPacket (pFrameMsg->pInQMsg,
                                         pFrameMsg->u4Instance,
                                         pFrameMsg->u2LocalPort,
                                         pFrameMsg->VlanInfo,
                                         pFrameMsg->u2TagLen) != SNOOP_SUCCESS)
            {
                if (CRU_BUF_Release_MsgBufChain (pFrameMsg->pInQMsg, FALSE)
                    == CRU_FAILURE)
                {
                    SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC,
                               SNOOP_INIT_TRC, "CRU buffer release failed in "
                               "incoming IGMP packet processing\r\n");
                }
            }
            if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pFrameMsg) != MEM_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_MEM_NAME,
                               "Queue message release failed in "
                               "incoming packet processing\r\n");
            }

            i4IgsMsgCount++;
            if (i4IgsMsgCount >= SNOOP_MAX_Q_MSGS)
            {
                if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_IGS_PKT_ENQ_EVENT)
                    != OSIX_SUCCESS)
                {
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "Send IGS PKT ENQ event Failed\r\n");

                }
                else
                {
                    SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                    SNOOP_CONTROL_TRC,
                                    "Relinquishing after Processing %d Messages and "
                                    "Posting Event to IGS Input queue Succeeds\r\n",
                                    i4IgsMsgCount);
                }
                break;
            }
        }
#endif

#ifdef MLDS_WANTED
        if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {

            if (MldsDecodeIncomingPacket (pFrameMsg->pInQMsg,
                                          pFrameMsg->u4Instance,
                                          pFrameMsg->u2LocalPort,
                                          pFrameMsg->VlanInfo,
                                          pFrameMsg->u2TagLen) != SNOOP_SUCCESS)
            {
                if (CRU_BUF_Release_MsgBufChain (pFrameMsg->pInQMsg, FALSE)
                    == CRU_FAILURE)
                {
                    SNOOP_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_BUFFER_TRC,
                               SNOOP_INIT_TRC, "CRU buffer release failed in "
                               "incoming MLD packet processing\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_BUFFER_TRC,
                                           SnoopSysErrString
                                           [SYS_LOG_CRU_BUFF_RELEASE_FAIL],
                                           "incoming MLD packet processing\r\n");
                }
            }
            if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pFrameMsg) != MEM_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_MEM_NAME,
                               "Queue message release failed in "
                               "incoming packet processing\r\n");
            }

            i4MldsMsgCount++;
            if (i4MldsMsgCount >= SNOOP_MAX_Q_MSGS)
            {
                if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MLDS_PKT_ENQ_EVENT)
                    != OSIX_SUCCESS)
                {
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "Send MLDS PKT ENQ Evt Failed\r\n");

                }
                else
                {
                    SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                    SNOOP_CONTROL_TRC,
                                    "Relinquishing after Processing %d Messages and "
                                    "Posting Event to MLDS Input queue Succeeds\r\n",
                                    i4MldsMsgCount);
                }
                break;
            }
        }
#endif
    }
    UNUSED_PARAM (i4MldsMsgCount);
    UNUSED_PARAM (i4IgsMsgCount);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMainProcessQMsgEvent                            */
/*                                                                           */
/* Description        : This function process the Message Q events that are  */
/*                      posted by lower layers                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMainProcessQMsgEvent (VOID)
{
    tSnoopIfPortBmp    *pSnpPortList = NULL;
    UINT1              *pAddPortList = NULL;
    UINT1              *pDelPortList = NULL;
    tSnoopMsgNode      *pMsgNode = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tVlanEvbSbpArray    SbpArray;
    UINT4               u4Instance = 0;
    UINT4               u4VlanIp = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4numOfMsgProcessed = 0;
    INT4                i4BrgPortType = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1Action = 0;
    UINT1               u1LoopIndex = 0;
#ifdef MBSM_WANTED                /* To be updated */
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    INT4                i4ProtoId = 0;
    INT4                i4RetVal = 0;
#endif
    UINT1               u1AddrType = SNOOP_ADDR_TYPE_IPV4;
    UINT2               u2VlanId = 0;
    INT4                i4QMsgCount = 0;
#ifdef ICCH_WANTED
    UINT1               u1McLagInterface = OSIX_FALSE;
    tVlanId             PrevVlanId = 0;
    UINT2               u2VlanCheckId = 0;
#endif

    pAddPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pAddPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pAddPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pAddPortList\r\n");
        return;
    }
    SNOOP_MEM_SET (pAddPortList, 0, sizeof (tSnoopPortBmp));

    pDelPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pDelPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pDelPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pDelPortList\r\n");
        UtilPlstReleaseLocalPortList (pAddPortList);
        return;
    }
    SNOOP_MEM_SET (pDelPortList, 0, sizeof (tSnoopPortBmp));

    while (SNOOP_RECV_FROM_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pMsgNode,
                                  SNOOP_DEF_MSG_LEN, 0) == OSIX_SUCCESS)
    {
        if (pMsgNode == NULL)
        {
            UtilPlstReleaseLocalPortList (pAddPortList);
            UtilPlstReleaseLocalPortList (pDelPortList);
            return;
        }

        switch (pMsgNode->u2MsgType)
        {
            case SNOOP_CREATE_CONTEXT:
                /* create context */
                SnoopHandleCreateContext ((UINT1) (pMsgNode->u4Instance));
                L2MI_SYNC_GIVE_SEM ();
                break;

            case SNOOP_DELETE_CONTEXT:
                /* delete context */
                SnoopHandleDeleteContext ((UINT1) (pMsgNode->u4Instance));
                L2MI_SYNC_GIVE_SEM ();
                break;

            case SNOOP_UPDATE_CONTEXT_NAME:
                /* context name updation */
                SnoopHandleUpdateContextName (pMsgNode->u4Instance);
                break;

            case SNOOP_CREATE_PORT_MSG:
                /* Create port msg is handled only in MI mode. It is not
                 * required in SI mode.
                 * The operation is same as mapping a port to a context 
                 */
                SnoopHandleMapPort ((UINT1) (pMsgNode->u4Instance),
                                    pMsgNode->uInMsg.u4Port,
                                    pMsgNode->u2LocalPort);
                L2_SYNC_GIVE_SEM ();
                break;

            case SNOOP_MAP_PORT:
                /* map port to a context */
                SnoopHandleMapPort ((UINT1) (pMsgNode->u4Instance),
                                    pMsgNode->uInMsg.u4Port,
                                    pMsgNode->u2LocalPort);
                L2MI_SYNC_GIVE_SEM ();
                break;

            case SNOOP_UNMAP_PORT:
                /* unmap port from a context */
                if (SnoopProcessDeletePortEvent (pMsgNode->u4Instance,
                                                 pMsgNode->u2LocalPort,
                                                 SNOOP_DEL_PORT)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC,
                                    "Processing delete port event"
                                    " for port %d failed\r\n",
                                    pMsgNode->uInMsg.u4Port);
                }
                SnoopHandleUnmapPort (pMsgNode->u4Instance,
                                      pMsgNode->uInMsg.u4Port);
                L2MI_SYNC_GIVE_SEM ();
                break;
                /* VLAN create indication received */
            case SNOOP_VLAN_CREATE_EVENT:

                /* Get the VLAN instance info from the incoming packet 
                 * Incoming message contains Instance for which the VLAN
                 * is getting created */
                u4Instance = (UINT1) (pMsgNode->u4Instance);

                /* This is done for trace */
                gu4SnoopTrcInstId = u4Instance;
                gu4SnoopDbgInstId = u4Instance;

                if (SNOOP_MCAST_FWD_MODE (u4Instance) !=
                    SNOOP_MCAST_FWD_MODE_MAC)
                {
                    break;
                }

                if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)

                {
                    pSnpPortList =
                        (tSnoopIfPortBmp *)
                        FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
                    if (pSnpPortList == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_GRP_TRC,
                                   "Error in allocating memory for pSnpPortList\r\n");
                        UtilPlstReleaseLocalPortList (pAddPortList);
                        UtilPlstReleaseLocalPortList (pDelPortList);
                        return;
                    }
                    SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortBmp));
                    SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                   (pMsgNode->uInMsg.VlanId),
                                                   (tPortList
                                                    *) (*pSnpPortList));
                    FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                }
                break;

                /* VLAN delete indication received */
            case SNOOP_VLAN_DELETE_EVENT:

                /* Get the VLAN instance info from the incoming packet 
                 * Incoming message contains Instance for which the VLAN
                 * is getting deleted */
                u4Instance = (UINT1) (pMsgNode->u4Instance);

                /* This is done for trace */
                gu4SnoopTrcInstId = u4Instance;
                gu4SnoopDbgInstId = u4Instance;

                /* Delete all the Group related information */
                SnoopGrpDelGroupOnOuterVlan
                    (u4Instance, pMsgNode->uInMsg.VlanId);

                /* Delete all the MCAST forwarding table information */
                SnoopFwdDeleteVlanMcastFwdEntries (u4Instance,
                                                   pMsgNode->uInMsg.VlanId, 0);

                /* Delete all the VLAN related information */
#ifdef IGS_WANTED

                if (SnoopVlanDeleteVlanEntry (u4Instance,
                                              pMsgNode->uInMsg.VlanId,
                                              SNOOP_ADDR_TYPE_IPV4)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC,
                                    "Deleting Vlan Entry for IGS %d"
                                    " failed \r\n",
                                    SNOOP_OUTER_VLAN (pMsgNode->uInMsg.VlanId));
                }

                SnoopRedActiveSendDelVlanSync (u4Instance,
                                               pMsgNode->uInMsg.VlanId,
                                               SNOOP_ADDR_TYPE_IPV4);
#endif

#ifdef MLDS_WANTED
                if (SnoopVlanDeleteVlanEntry (u4Instance,
                                              pMsgNode->uInMsg.VlanId,
                                              SNOOP_ADDR_TYPE_IPV6)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC,
                                    "Deleting Vlan Entry for MLDS %d"
                                    " failed \r\n",
                                    SNOOP_OUTER_VLAN (pMsgNode->uInMsg.VlanId));
                }
#endif

                break;

                /* Port Down/Delete indication received from other module */
            case SNOOP_DISABLE_PORT_MSG:
            case SNOOP_DELETE_PORT_MSG:

                /* SNOOP will be maintaining the virtual port bitmap 
                 * corresponding to the physical port bitmap obtained from the 
                 * multiple instance manager so get the Virtual port number and 
                 * instance number corresponding to the physical port from the 
                 * multiple instance manager
                 */
                /* This is done for trace */
                gu4SnoopTrcInstId = pMsgNode->u4Instance;
                gu4SnoopDbgInstId = pMsgNode->u4Instance;

                if (pMsgNode->u2MsgType == SNOOP_DELETE_PORT_MSG)
                {
                    u1Action = SNOOP_DEL_PORT;
                }
                else
                {
                    u1Action = SNOOP_DISABLE_PORT;
                }

                SnoopCfaGetInterfaceBrgPortType (pMsgNode->uInMsg.u4Port,
                                                 &i4BrgPortType);

                if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                {
                    MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
                    SnoopVlanApiEvbGetSbpPortsOnUap (pMsgNode->uInMsg.u4Port,
                                                     &SbpArray);
                    while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
                    {
                        if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
                        {
                            /* No more SBPs are present on this interface */
                            break;
                        }
                        if (SnoopVcmGetContextInfoFromIfIndex
                            (SbpArray.au4SbpArray[u1LoopIndex], &u4ContextId,
                             &u2LocalPort) == VCM_SUCCESS)
                        {
                            /* disabling all the SBPs present on the UAP */
                            SnoopProcessDeletePortEvent (u4ContextId,
                                                         u2LocalPort, u1Action);
                        }
                        u1LoopIndex++;
                    }
                }

                if (SnoopProcessDeletePortEvent (pMsgNode->u4Instance,
                                                 (UINT4) pMsgNode->u2LocalPort,
                                                 u1Action) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC,
                                    "Processing delete port event"
                                    " for port %d failed\r\n",
                                    pMsgNode->uInMsg.u4Port);
                }

                /* update global port array and per context array */
                if (u1Action == SNOOP_DEL_PORT)
                {
                    SnoopHandleUnmapPort (pMsgNode->u4Instance,
                                          pMsgNode->uInMsg.u4Port);
                    L2MI_SYNC_GIVE_SEM ();
                }
                break;

                /* Port oper "UP" indication received from other module */

                /* Topology Change Indication Event received from Spanning Tree */

            case SNOOP_TOPO_CHANGE:

                u4Instance = (UINT1) (pMsgNode->u4Instance);

                /* This is done for trace */
                gu4SnoopTrcInstId = u4Instance;
                gu4SnoopDbgInstId = u4Instance;

                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

                for (u1AddrType = 1; u1AddrType <= SNOOP_MAX_PROTO;
                     u1AddrType++)
                {

                    if (pSnoopInstInfo == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG,
                                       SNOOP_DBG_MGMT,
                                       SNOOP_MGMT_NAME,
                                       "Instance not created \r\n");
                        break;
                    }

                    pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                                   SnoopInfo[u1AddrType - 1]);

                    /* Check whether IGS/MLDS is globally enabled or 
                     * disabled */
                    if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddrType - 1))
                        == SNOOP_DISABLE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                        SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_GRP_TRC,
                                        "Processing topology change event"
                                        " for port %d failed\r\n",
                                        pMsgNode->uInMsg.u4Port);

                        continue;
                    }

                    if (pSnoopInfo->u1SnoopQueryStatus == SNOOP_ENABLE)
                    {

                        SnoopProcessTopoChangeEvent (u4Instance,
                                                     pMsgNode->uInMsg.u4Port,
                                                     pMsgNode->uInMsg.
                                                     StpTcInfo.VlanList,
                                                     u1AddrType);
                    }
                }

                break;

            case SNOOP_ENABLE_PORT_MSG:

                /* 
                 * SNOOP will be maintaining the virtual port bitmap 
                 * corresponding to the physical port bitmap obtained from the
                 * multiple instance manager so get the Virtual port number and 
                 * instance number corresponding to the physical port from the 
                 * multiple instance manager
                 */
                /* This is done for trace */

                gu4SnoopTrcInstId = pMsgNode->u4Instance;
                gu4SnoopDbgInstId = pMsgNode->u4Instance;

                if (SnoopProcessEnablePortEvent
                    (pMsgNode->u4Instance,
                     pMsgNode->u2LocalPort) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC, "Processing enable port "
                                    "event for port %d failed\r\n",
                                    pMsgNode->uInMsg.u4Port);
                }

                SnoopCfaGetInterfaceBrgPortType (pMsgNode->uInMsg.u4Port,
                                                 &i4BrgPortType);

                if (i4BrgPortType == CFA_UPLINK_ACCESS_PORT)
                {
                    MEMSET (&SbpArray, 0, sizeof (tVlanEvbSbpArray));
                    SnoopVlanApiEvbGetSbpPortsOnUap (pMsgNode->uInMsg.u4Port,
                                                     &SbpArray);
                    while (u1LoopIndex < VLAN_EVB_MAX_SBP_PER_UAP)
                    {
                        if (SbpArray.au4SbpArray[u1LoopIndex] == 0)
                        {
                            /* No more SBPs are present on this interface */
                            break;
                        }
                        if (SnoopVcmGetContextInfoFromIfIndex
                            (SbpArray.au4SbpArray[u1LoopIndex], &u4ContextId,
                             &u2LocalPort) == VCM_SUCCESS)
                        {
                            /* enabling all the SBPs present on the UAP */
                            SnoopProcessEnablePortEvent (u4ContextId,
                                                         u2LocalPort);
                        }
                        u1LoopIndex++;
                    }
                }

#ifdef ICCH_WANTED
                SnoopLaIsMclagInterface (SNOOP_GET_IFINDEX
                                         (pMsgNode->u4Instance,
                                          pMsgNode->u2LocalPort),
                                         &u1McLagInterface);
                if (u1McLagInterface == OSIX_TRUE)
                {
                    while (L2IwfMiGetNextActiveVlan
                           (pMsgNode->u4Instance, PrevVlanId,
                            &u2VlanCheckId) == L2IWF_SUCCESS)
                    {
                        PrevVlanId = u2VlanCheckId;
                        if (L2IwfMiIsVlanMemberPort
                            (pMsgNode->u4Instance, u2VlanCheckId,
                             SNOOP_GET_IFINDEX (pMsgNode->u4Instance,
                                                pMsgNode->u2LocalPort)) ==
                            OSIX_TRUE)
                        {
                            SnoopIcchSyncMclagStatusChange (u2VlanCheckId);
                        }
                    }
                }
#endif
                break;

            case SNOOP_ADD_PORT_TO_AGG:

                gu4SnoopTrcInstId = pMsgNode->u4Instance;
                gu4SnoopDbgInstId = pMsgNode->u4Instance;

                if (SnoopUtilUpdateAggStatusInHW (pMsgNode->u4Instance,
                                                  pMsgNode->u2AggId,
                                                  pMsgNode->uInMsg.u4Port,
                                                  SNOOP_ADD_LA_PORT)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC, "Processing enable port "
                                    "event for port %d failed\r\n",
                                    pMsgNode->uInMsg.u4Port);
                }
                break;

            case SNOOP_REMOVE_PORT_FROM_AGG:

                gu4SnoopTrcInstId = pMsgNode->u4Instance;
                gu4SnoopDbgInstId = pMsgNode->u4Instance;

                if (SnoopUtilUpdateAggStatusInHW (pMsgNode->u4Instance,
                                                  pMsgNode->u2AggId,
                                                  pMsgNode->uInMsg.u4Port,
                                                  SNOOP_DEL_LA_PORT)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC, "Processing remove port "
                                    "event for port %d failed\r\n",
                                    pMsgNode->uInMsg.u4Port);
                }
                break;

                /* Forbidden port addtion from VLAN */
            case SNOOP_UPDATE_VLAN_PORTS:

                /* SNOOP will be maintaining the virtual port bitmap 
                 * corresponding to the physical port bitmap obtained from the 
                 * multiple instance manager so get the Virtual port number and
                 * instance number corresponding to the physical port from the 
                 * multiple instance manager
                 */
                u4Instance = (UINT1) (pMsgNode->u4Instance);

                /* This is done for trace */
                gu4SnoopTrcInstId = u4Instance;
                gu4SnoopDbgInstId = u4Instance;
                SnoopConvertToLocalPortList (pMsgNode->uInMsg.VlanPortListMac.
                                             AddPortList, pAddPortList);
                SnoopConvertToLocalPortList (pMsgNode->uInMsg.VlanPortListMac.
                                             DelPortList, pDelPortList);

                if (SnoopProcessVlanUpdatePortsEvent
                    (u4Instance, pMsgNode->uInMsg.VlanPortListMac.VlanId,
                     pMsgNode->uInMsg.VlanPortListMac.McastAddr,
                     pAddPortList, pDelPortList) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_GRP_TRC,
                                    "Processing update ports event for Vlan"
                                    "%d failed\r\n",
                                    SNOOP_OUTER_VLAN (pMsgNode->uInMsg.
                                                      VlanPortListMac.VlanId));
                }
                break;

#ifdef MBSM_WANTED                /* To be updated */
            case MBSM_MSG_CARD_INSERT:
                i4ProtoId =
                    pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg->
                    i4ProtoCookie;
                pSlotInfo =
                    &(pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmSlotInfo);
                pPortInfo =
                    &(pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmPortInfo);

                i4RetVal = SnoopMbsmUpdateCardInsertion (pPortInfo, pSlotInfo);

                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = i4RetVal;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg);
                break;

            case MBSM_MSG_CARD_REMOVE:
                i4ProtoId =
                    pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg->
                    i4ProtoCookie;
                pSlotInfo =
                    &(pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg->
                      MbsmSlotInfo);

                MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
                MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
                MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
                MbsmSendAckFromProto (&MbsmProtoAckMsg);

                MEM_FREE (pMsgNode->uInMsg.MbsmCardUpdate.pMbsmProtoMsg);
                break;
#endif

#ifdef L2RED_WANTED
            case SNOOP_RM_DATA:
                SnoopRedHandleRmEvents (pMsgNode);
                break;

            case SNOOP_RED_AUDIT_SYNC_MSG:
                SnoopRedHandleMcastAuditSyncMsg (pMsgNode);
                break;

            case SNOOP_IGS_RED_GEN_QUERY:
                SnoopRedSendGeneralQuery (pMsgNode->u4Instance,
                                          SNOOP_ADDR_TYPE_IPV4);
                break;
#endif
#ifdef ICCH_WANTED
            case SNOOP_ICCH_MSG:
                SnoopIcchProcessUpdateMsg (pMsgNode);
                break;
#endif /* ICCH_WANTED */

#ifdef NPAPI_WANTED
            case SNOOP_NP_CALLBACK:
                IgsProcNPCallback (pMsgNode);
                break;
#endif
            case SNOOP_IPADDR_CHANGE:

                u4Instance = pMsgNode->u4Instance;
                gu4SnoopTrcInstId = u4Instance;
                gu4SnoopDbgInstId = u4Instance;
                u2VlanId = pMsgNode->u2IntfVlan;

                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

                if (SnpPortGetVlanIpInfo (CFA_DEFAULT_ROUTER_VLAN_IFINDEX,
                                          &u4VlanIp) == SNOOP_FAILURE)
                {
                    break;
                }
                /* Assigning the changed ip address in snoop database */
                gu4SwitchIp = u4VlanIp;

                /* If oper status is UP, assign corresponding IP Address */
                u4VlanIp = pMsgNode->u4Addr;

                for (u1AddrType = 1; u1AddrType <= SNOOP_MAX_PROTO;
                     u1AddrType++)
                {
                    if (pSnoopInstInfo == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG,
                                       SNOOP_ALL_FAILURE_TRC,
                                       SNOOP_MGMT_NAME,
                                       "Instance not created \r\n");
                        break;
                    }

                    pSnoopInfo = &(SNOOP_INSTANCE_INFO (u4Instance)->
                                   SnoopInfo[u1AddrType - 1]);
                    /* Check whether IGS/MLDS is globally enabled or
                     * disabled */
                    if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddrType - 1))
                        == SNOOP_DISABLE)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_QRY_TRC,
                                   "Processing IP Address Change event"
                                   " failed\r\n");
                        continue;
                    }

                    SnpProcessIpAddrChangeEvent (u4Instance, u4VlanIp, u2VlanId,
                                                 u1AddrType);
                }
                break;

            default:
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_MEM_NAME, "Invalid event received from "
                               "lower layer\r\n");
                break;
        }
        if (pMsgNode->u2MsgType == SNOOP_RM_DATA)
        {
            i4QMsgCount++;
            if (i4QMsgCount >= SNOOP_MAX_RM_MSGS)
            {
                if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT)
                    != OSIX_SUCCESS)
                {
                    SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                               SNOOP_CONTROL_TRC,
                               "Send MSG queue Evt Failed\r\n");

                }
                else
                {
                    SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                    SNOOP_CONTROL_TRC,
                                    "Relinquishing after Processing %d RM Messages and "
                                    "Posting Event to Message queue Succeeds\r\n",
                                    i4QMsgCount);
                }
                break;
            }
        }

        if (SNOOP_RELEASE_QMSG_MEM_BLOCK (pMsgNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Queue message release failed for "
                           "events from lower layer\r\n");
        }

        u4numOfMsgProcessed++;
        if (u4numOfMsgProcessed > SNOOP_MAX_NUM_OF_MSG_PROC)
        {
            if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT)
                != OSIX_SUCCESS)
            {
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Send IGS PKT ENQ event Failed\r\n");

            }
            else
            {
                SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                SNOOP_CONTROL_TRC,
                                "Relinquishing after Processing "
                                "%d Messages and Posting Event to IGS Input queue Succeeds\r\n",
                                u4numOfMsgProcessed);
            }
            break;
        }
    }

    UtilPlstReleaseLocalPortList (pAddPortList);
    UtilPlstReleaseLocalPortList (pDelPortList);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMainEnableSnooping                              */
/*                                                                           */
/* Description        : This function will be called when manager sets       */
/*                      IGS/MLDS as Enabled.                                 */
/*                                                                           */
/* Input(s)           : u1AddressType - specifies IGS/MLDS                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMainEnableSnooping (UINT4 u4Instance, UINT1 u1AddressType)
{
    /* This is done for trace */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (gNullPortBitMap, 0, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (gNullIfPortBitMap, 0, SNOOP_IF_PORT_LIST_SIZE);

    SNOOP_VALIDATE_ADDRESS_TYPE (u1AddressType);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopMainEnableSnooping\r\n");
    /* Check if Status is already enabled */
    if (SNOOP_SNOOPING_STATUS (u4Instance, u1AddressType - 1) == SNOOP_ENABLE)
    {
        SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_INIT_DBG, SNOOP_INIT_DBG,
                        "Already enabled for this instance %d and the "
                        "address type %d\r\n", u4Instance, u1AddressType);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMainEnableSnooping\r\n");
        return;
    }

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        SNOOP_MEM_SET (gNullSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    }

#ifdef NPAPI_WANTED                /* To be updated */
    /* Initialize Snooping at Hardware */
#ifdef IGS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
        {
            if (SnoopHwEnableIgmpSnooping (u4Instance) == SNOOP_FAILURE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_INIT_DBG, SNOOP_INIT_DBG,
                                "Unable to enable IGMP Snooping in hardware for instance %u\r\n",
                                u4Instance);
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_NP, SNOOP_NP_DBG,
                                  SnoopSysErrString
                                  [SYS_LOG_IGMP_SNOOP_NP_ENABLE_FAIL]);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopMainEnableSnooping\r\n");
                return;
            }
        }
    }
#endif

#ifdef MLDS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
        {
            if (SnoopHwEnableMldSnooping (u4Instance) == SNOOP_FAILURE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_INIT_DBG, SNOOP_INIT_DBG,
                                "Unable to enable MLD Snooping in hardware for instance %u\r\n",
                                u4Instance);
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_NP, SNOOP_NP_DBG,
                                  SnoopSysErrString
                                  [SYS_LOG_MLD_SNOOP_NP_ENABLE_FAIL]);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopMainEnableSnooping\r\n");
                return;
            }
        }
    }
#endif
#endif

    SNOOP_INSTANCE_INFO (u4Instance)->SrcListCleanUpTmr.u1TimerType =
        SNOOP_SRC_LIST_CLEANUP_TIMER;
    if (SnoopTmrStartTimer
        (&SNOOP_INSTANCE_INFO (u4Instance)->SrcListCleanUpTmr,
         SNOOP_SRC_LIST_CLEANUP_INTERVAL) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_ALL_FAILURE_TRC,
                       SNOOP_GBL_NAME,
                       "Failure in starting SourceList cleanup timer \r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMainEnableSnooping\r\n");
        return;
    }

#ifdef L2RED_WANTEd
    SNOOP_RED_BULK_SYNC_INPROGRESS = SNOOP_FALSE;
#endif
    /* Enable IGS/MLDS snooping */
    SNOOP_SNOOPING_STATUS (u4Instance, u1AddressType - 1) = SNOOP_ENABLE;
    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                        SNOOP_DBG_INIT,
                        SNOOP_INIT_NAME,
                        "IGMP Snooping enabled for instance %u\r\n",
                        u4Instance);

    /* Check if any configuration is done for any VLAN in this instance
     * like oper version/ querier/ fast leave/ static router port bitmap
     * If configured take care of those things also 
     */

    if (SnoopVlanUpdateVlanTable (u4Instance, u1AddressType) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                        "Vlan table updation event failed for address type %d"
                        "and instance %u \r\n", u1AddressType, u4Instance);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMainEnableSnooping\r\n");
        return;
    }
    if (SnoopUtilHandleSparseMode (u4Instance,
                                   SNOOP_SYSTEM_SPARSE_MODE (u4Instance)) !=
        SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_ALL_FAILURE_TRC,
                       SNOOP_GBL_NAME, "Unable to change the Sparse Mode\r\n");
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopMainEnableSnooping\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopModuleShutdownInstance                          */
/*                                                                           */
/* Description        : This function starts and enables snooping for a given*/
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopModuleInitInstance (UINT4 u4Instance)
{
    if (SnoopInitInstance (u4Instance, SNOOP_MCAST_FWD_MODE (u4Instance))
        == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "Snoop module init failed\n");
        return SNOOP_FAILURE;
    }

    SnoopMiMapActivePorts (u4Instance);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopModuleShutdownInstance                          */
/*                                                                           */
/* Description        : This function shutdowns the snooping for a given     */
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

VOID
SnoopModuleShutdownInstance (UINT4 u4Instance)
{
    SNOOP_VALIDATE_INSTANCE (u4Instance);

    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_SHUTDOWN_IN_PROGRESS;
#ifdef IGS_WANTED
    /* Make the protocol Admin Value as Disabled */
    SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance,
                                 SNOOP_INFO_IGS_ENTRY) = SNOOP_DISABLE;

    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV4);
#endif

#ifdef MLDS_WANTED
    SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance,
                                 SNOOP_INFO_MLDS_ENTRY) = SNOOP_DISABLE;

    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV6);
#endif
    SnoopDeInitInstance (u4Instance);
    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_SHUTDOWN;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleCreateContext                             */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u4IfIndex - The Physical Port number be unmapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
INT4
SnoopHandleCreateContext (UINT4 u4Instance)
{
    /* For Multi Instance all the memory pools are created,when default 
     * instance is created, so here just allocate instance pointer and 
     * initilise the default values, create RBTrees related to the
     * instance  */

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        return SNOOP_FAILURE;
    }

    if (SNOOP_GINSTANCE_ALLOC_MEMBLK (SNOOP_INSTANCE_INFO (u4Instance)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP global instance allocation "
                       "failed for instance \r\n");
        return SNOOP_FAILURE;

    }
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (u4Instance), 0,
                   sizeof (tSnoopGlobalInstInfo));

    SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) = SNOOP_DISABLE;
    SNOOP_INSTANCE_ENH_MODE (u4Instance) = SNOOP_DISABLE;
    if (SnoopInitInstInfo (u4Instance, SNOOP_MCAST_FWD_MODE_MAC)
        == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_NAME, "SNOOP Initialization for"
                            "instance %d failed\r\n", u4Instance);
        SnoopDeInitInstInfo (u4Instance);
        SNOOP_RELEASE_GINSTANCE_MEM_BLOCK (SNOOP_INSTANCE_INFO (u4Instance));
        SNOOP_INSTANCE_INFO (u4Instance) = NULL;
        return SNOOP_FAILURE;

    }

    /* To fill the context Name */
    SnoopInitInstName (u4Instance);
    /* by default set 0 to per context port array */
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (u4Instance)->au4SnoopPhyPort,
                   0, (SNOOP_MAX_PORTS_PER_INSTANCE * sizeof (UINT4)));

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleDeleteContext                             */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u4IfIndex - The Physical Port number be unmapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
VOID
SnoopHandleDeleteContext (UINT4 u4Instance)
{
    /* if the context is deleted it is as good as the module is shutdown
       so just releasing per context memory is not sufficient, so remove 
       the ha entries release the memory */

    SNOOP_VALIDATE_INSTANCE (u4Instance);

#ifdef IGS_WANTED
    /* Make the protocol Admin Value as Disabled */
    SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance, SNOOP_INFO_IGS_ENTRY)
        = SNOOP_DISABLE;
    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV4);
#endif
#ifdef MLDS_WANTED
    SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance, SNOOP_INFO_MLDS_ENTRY)
        = SNOOP_DISABLE;
    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV6);
#endif
    SnoopDeInitInstance (u4Instance);
    SNOOP_RELEASE_GINSTANCE_MEM_BLOCK (SNOOP_INSTANCE_INFO (u4Instance));
    SNOOP_INSTANCE_INFO (u4Instance) = NULL;
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleUpdateContextName                         */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the alias name modification of a context */
/* Input(s)           : u4Instance - Instance Id.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : L2IWF Module                                         */
/*****************************************************************************/
INT4
SnoopHandleUpdateContextName (UINT4 u4Instance)
{

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    MEMSET (SNOOP_INSTANCE_STR (u4Instance), 0, (SNOOP_INSTANCE_ALIAS_LEN + 1));

    if (SnoopVcmGetAliasName (u4Instance, SNOOP_INSTANCE_STR (u4Instance))
        != VCM_SUCCESS)
    {
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleMapPort                                   */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u4IfIndex - The Physical Port number be unmapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
VOID
SnoopHandleMapPort (UINT4 u4Instance, UINT4 u4IfIndex, UINT2 u2LocalPort)
{
    /* update the global port array for phycal to local port mapping 
       and also the vice versa */
    /* stores local ports info for physical port */

    SNOOP_VALIDATE_INSTANCE (u4Instance);
    SNOOP_VALIDATE_PORT (u2LocalPort);

    SNOOP_GLOBAL_PORT (u4IfIndex) = u2LocalPort;
    SNOOP_GLOBAL_INSTANCE (u4IfIndex) = u4Instance;
    /* stores physical ports info for local port */
    SNOOP_GET_IFINDEX (u4Instance, u2LocalPort) = u4IfIndex;

    SnoopDeriveSispDefaults (u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : SnoopHandleUnmapPort                                 */
/*                                                                           */
/* Description        : This function is called from the L2Iwf Modul         */
/*                      to indicate the unmap of port from a context         */
/* Input(s)           : u4IfIndex - The Physical Port number be unmapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*                                                                           */
/* Called By          : CFA Module                                           */
/*****************************************************************************/
VOID
SnoopHandleUnmapPort (UINT4 u4Instance, UINT4 u4IfIndex)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopPortCfgEntry  SnoopPortCfgEntry;
    tRBElem            *pRBElem = NULL;
    UINT2               u2LocalPort = 0;

    /* reset the global port array for phycal to local port mapping 
       and also the vice versa */
    /* stores local ports info for physical port */
    u2LocalPort = SNOOP_GLOBAL_PORT (u4IfIndex);
    SNOOP_GLOBAL_PORT (u4IfIndex) = 0;
    SNOOP_GLOBAL_INSTANCE (u4IfIndex) = 0;

    SNOOP_VALIDATE_INSTANCE (u4Instance);
    SNOOP_VALIDATE_PORT (u2LocalPort);

    /* stores physical ports info for local port */
    SNOOP_GET_IFINDEX (u4Instance, u2LocalPort) = 0;

    /* Delete the port entry from the instance and global RB Tree */
    MEMSET (&SnoopPortCfgEntry, 0, sizeof (tSnoopPortCfgEntry));
    SnoopPortCfgEntry.u4SnoopPortIndex = u4IfIndex;
    SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) = 0;
    SnoopPortCfgEntry.u1SnoopPortCfgInetAddrtype = 0;

    while ((pRBElem = RBTreeGetNext (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                                     (tRBElem *) & SnoopPortCfgEntry, NULL))
           != NULL)
    {
        pSnoopPortCfgEntry = (tSnoopPortCfgEntry *) pRBElem;

        if (pSnoopPortCfgEntry->u4SnoopPortIndex != u4IfIndex)
        {
            break;
        }

        /* Give an indication to TAC module if profile Id is configured */
        if (pSnoopPortCfgEntry->u4SnoopPortCfgProfileId !=
            SNOOP_PORT_CFG_PROFILE_ID_DEF)
        {
            SnoopTacApiUpdatePortRefCount
                (pSnoopPortCfgEntry->u4SnoopPortCfgProfileId,
                 pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype, TACM_UNMAP);
        }

        if (SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) != NULL)
        {
            SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK (SNOOP_PORT_CFG_EXT_ENTRY
                                                  (pSnoopPortCfgEntry));
            SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) = NULL;
        }

        RBTreeRem (SNOOP_PORT_CFG_GLB_RBTREE, (tRBElem *) pSnoopPortCfgEntry);

        RBTreeRem (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                   (tRBElem *) pSnoopPortCfgEntry);

        SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (pSnoopPortCfgEntry);
        pSnoopPortCfgEntry = NULL;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopInitInstance                                    */
/* Description        : This function Initialises Instance specific          */
/*                      information                                          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopInitInstance (UINT4 u4Instance, UINT1 u1McastMode)
{
    if (SnoopInitInstInfo (u4Instance, u1McastMode) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_NAME, "SNOOP Initialization for "
                            "instance %d failed\r\n", u4Instance);
        SnoopDeInitInstInfo (u4Instance);
        return SNOOP_FAILURE;

    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeInitInstance                                  */
/* Description        : This function De Initialises Instance specific       */
/*                      information                                          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
VOID
SnoopDeInitInstance (UINT4 u4Instance)
{
    UINT4               u4PhyPort = 0;
    UINT2               u2PortCount = 0;
    /* reset/unmap the global port array gau2SnoopLocalPort */
    for (u2PortCount = 1; u2PortCount <= SNOOP_MAX_PORTS_PER_INSTANCE;
         u2PortCount++)
    {
        if (SNOOP_GET_IFINDEX (u4Instance, u2PortCount) != 0)
        {
            u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u2PortCount);
            SNOOP_GLOBAL_PORT (u4PhyPort) = 0;
            /* no need to reset the per instance port array, as instance pointer
               will get released */
        }
    }
    SnoopDeInitInstInfo (u4Instance);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleMcastModeChange                           */
/*                                                                           */
/* Description        : This function handles the Multi cast  forwarding mode*/
/*                      change, this fiunction in turn call disable APIs to  */
/*                      flush entries learnt on other mode.                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      u4McastMode - Multicast Fwd mode                     */
/*                                    MAC based or IP Based                  */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_FAILURE, if changig mode is not success,       */
/*                      SNOOP_SUCCESS                                        */
/*****************************************************************************/
INT4
SnoopHandleMcastModeChange (UINT4 u4Instance, UINT4 u4McastMode)
{
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_SHUTDOWN_IN_PROGRESS;
#ifdef IGS_WANTED
    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV4);
#endif

#ifdef MLDS_WANTED
    SnoopMainDisableSnooping (u4Instance, SNOOP_ADDR_TYPE_IPV6);
#endif
    SnoopDeInitInstInfo (u4Instance);
    /* Release source information buffer */
    if (gpSSMSrcInfoBuffer != NULL)
    {
        SNOOP_RELEASE_SRC_MEM_BLOCK (gpSSMSrcInfoBuffer);
        gpSSMSrcInfoBuffer = NULL;
    }
    /* Release SSM report buffer */
    if (gpSSMRepSendBuffer != NULL)
    {
        SNOOP_RELEASE_MISC_MEM_BLOCK (gpSSMRepSendBuffer);
        gpSSMRepSendBuffer = NULL;
    }
    /* Release data information buffer */
    if (gpSnoopDataBuffer != NULL)
    {
        SNOOP_RELEASE_DATA_MEM_BLOCK (gpSnoopDataBuffer);
        gpSnoopDataBuffer = NULL;
    }

    SNOOP_SYSTEM_STATUS (u4Instance) = SNOOP_SHUTDOWN;
    SnoopNotifyProtocolShutdownStatus ((INT4) u4Instance);

    if (SnoopInitInstance (u4Instance, (UINT1) u4McastMode) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "Snoop module init failed\n");
        return SNOOP_FAILURE;
    }

    /* memory allocation for SSM report buffer which is used for contructing 
     * SSM report */
    if (gpSSMRepSendBuffer == NULL)
    {
        if (SNOOP_MISC_ALLOC_MEMBLK (gpSSMRepSendBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Memory allocation failed while allocating misc "
                           "buffer failed\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                   "while allocating misc\r\n");
            SnoopDeInitInstInfo (u4Instance);
            return SNOOP_FAILURE;
        }
    }

    /* memory allocation for source information which is used for
     * constructing source information in IP based forwarding mode
     * Since this is a global pointer and is used by all the instances
     * allocation is done even if the forwarding mode in this instance
     * is MAC-based */

    if (gpSSMSrcInfoBuffer == NULL)
    {
        if (SNOOP_SRC_ALLOC_MEMBLK (gpSSMSrcInfoBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "Memory allocation failed "
                           "for misc buffer for sending source "
                           "information\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                   "for misc buffer for sending source "
                                   "information\r\n");
            SnoopDeInitInstInfo (u4Instance);
            return SNOOP_FAILURE;
        }
    }

    /* memory allocation for data buffer which is used for duplicating 
     * data packet */
    if (gpSnoopDataBuffer == NULL)
    {
        if (SNOOP_DATA_ALLOC_MEMBLK (gpSnoopDataBuffer) == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_MEM_NAME, "Memory allocation failed "
                           "for data buffer for sending data "
                           "information\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                   "for data buffer for sending data \r\n");
            SnoopDeInitInstInfo (u4Instance);
            return SNOOP_FAILURE;
        }
    }

    /* Disable the enhanced mode if the forwarding mode is MAC */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        SNOOP_INSTANCE_ENH_MODE (u4Instance) = SNOOP_DISABLE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGetAllPorts                                     */
/*                                                                           */
/* Description        : This function scans through the L2IWF port table     */
/*                      and issue create port indications to Snooping module.*/
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGetAllPorts (VOID)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2PrevPort = 0;
    UINT2               u2LocalPort = 0;

    while (SnoopL2IwfGetNextValidPortForContext (SNOOP_DEFAULT_INSTANCE,
                                                 u2PrevPort, &u2LocalPort,
                                                 &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (SnoopL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {
            u2PrevPort = u2LocalPort;
            continue;
        }

        SnoopHandleMapPort (SNOOP_DEFAULT_INSTANCE, u4IfIndex, u2LocalPort);

        u2PrevPort = u2LocalPort;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopRegisterWithPacketHandler                        */
/*                                                                           */
/* Description        : This function registers with the packet handler for  */
/*                      Snooping control packets and multicast data packets. */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopRegisterWithPacketHandler ()
{
    tProtocolInfo       SnoopProtocolInfo;
    static tMacAddr     McastMac = { 0x01, 0x00, 0x5e, 0x00, 0x00, 0x00 };

    /* Register the CallBack Function and Packet Qualifier
     * with packet handler for receiving packets*/
    MEMSET (&SnoopProtocolInfo, 0, sizeof (tProtocolInfo));

    /* The packets must be sent through CFA, so we do not provide a call back
     * function*/
    SnoopProtocolInfo.ProtocolFnPtr = NULL;

    /* Register for Multicast data pcakets */

    MEMCPY (&SnoopProtocolInfo.ProtRegTuple.MacInfo.MacAddr, &McastMac,
            ETHERNET_ADDR_SIZE);
    MEMSET (SnoopProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0,
            ETHERNET_ADDR_SIZE);
    MEMSET (SnoopProtocolInfo.ProtRegTuple.MacInfo.MacMask, 0xFF,
            ETHERNET_ADDR_SIZE - 3);
    SnoopProtocolInfo.i4Command = MUX_PROT_REG_HDLR;
    SnoopProtocolInfo.u1SendToFlag = MUX_REG_SEND_TO_ISS;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_SNOOP_CONTROL, &SnoopProtocolInfo) != CFA_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Packet Registration "
                       "failed \r\n");
        return SNOOP_FAILURE;
    }

#ifdef MLDS_WANTED

    /* We need the ICMP6 packets with the following 
     * types. MUX Reg will copy the ICMPv6 type into the
     * IP Protocol field if the packe is ICMPv6.
     * PACKET_HDLR_PROTO_MLD_QUERY 0x82, 
     * PACKET_HDLR_PROTO_MLD_DONE  0x84,
     * PACKET_HDLR_PROTO_MLDV1_REPORT  0x83,
     * PACKET_HDLR_PROTO_MLDV2_REPORT 0x8f */

    MEMSET (&(SnoopProtocolInfo.ProtRegTuple), 0, sizeof (tProtRegTuple));

    SnoopProtocolInfo.ProtRegTuple.EthInfo.u2Protocol = CFA_ENET_IPV6;
    SnoopProtocolInfo.ProtRegTuple.EthInfo.u2Mask = 0xFFFF;

    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = 0x82;
    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFF;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_MLD_QUERY, &SnoopProtocolInfo) != CFA_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Packet Registration "
                       "failed \r\n");
        return SNOOP_FAILURE;
    }

    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = 0x84;
    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFF;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_MLD_DONE, &SnoopProtocolInfo) != CFA_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Packet Registration "
                       "failed \r\n");
        return SNOOP_FAILURE;
    }

    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = 0x83;
    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFF;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_MLDV1_REPORT, &SnoopProtocolInfo) != CFA_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Packet Registration "
                       "failed \r\n");
        return SNOOP_FAILURE;
    }

    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Protocol = 0x8f;
    SnoopProtocolInfo.ProtRegTuple.IPInfo.u2Mask = 0xFF;

    if (CfaMuxRegisterProtocolCallBack
        (PACKET_HDLR_PROTO_MLDV2_REPORT, &SnoopProtocolInfo) != CFA_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MEM_NAME, "SNOOP Packet Registration "
                       "failed \r\n");
        return SNOOP_FAILURE;
    }
#endif

    return SNOOP_SUCCESS;
}
#endif /*SNPMAIN_C */
