/* $Id: snpproc.c,v 1.155.2.2 2018/04/12 12:53:47 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpproc.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snoop                                          */
/*    MODULE NAME           : FutureSoft snoop Processing                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains processing routines         */
/*                            for snooping module                            */
/*                                        */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopProcessMulticastPkt                             */
/*                                                                           */
/* Description        : This function processes the incoming multicast frame.*/
/*                      This function will be called from IGMP/MLD Decoding  */
/*                      modules                                              */
/*                                                                           */
/* Input(s)           : pBuf     - multicast packet                          */
/*                      u4InstanceId - Instance Identifier                   */
/*                      u4Port     - Incoming port number                    */
/*                      VlanId   - Vlan Id of the packet. Will be zero if    */
/*                                 called from Bridge module                 */
/*                      pSnoopPktInfo - Structure contains IGMP/MLD packet   */
/*                                      information.                         */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessMulticastPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                          UINT4 u4InstanceId, UINT4 u4Port,
                          tSnoopTag VlanId, tSnoopPktInfo * pSnoopPktInfo)
{
    UINT1              *pPortBitmap = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tIPvXAddr           NullIpAddr;
    UINT1               u1PktType = 0;
    UINT1               u1Forward = 0;
    UINT1               u1Version = 0;
    UINT1               u1IsUnReg = SNOOP_TRUE;
    UINT1               u1FwdData = SNOOP_FORWARD;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&NullIpAddr, 0, sizeof (tIPvXAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4InstanceId;
    gu4SnoopDbgInstId = u4InstanceId;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstanceId, SNOOP_FAILURE);

    /* Standby node should not process incoming packets */
    if ((SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SNOOP_SUCCESS;
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "in allocating memory for pPortBitmap\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    if (pSnoopPktInfo->u1PktType == SNOOP_DATA_PKT)
    {
        /* In case software forwarding we should be able to forward mcast 
         * data packets */
        if (SnoopProcessMulticastData (u4InstanceId, VlanId, pSnoopPktInfo,
                                       u4Port, pBuf, &u1FwdData, &u1IsUnReg,
                                       pPortBitmap) == SNOOP_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Multicast Data Pkt Processing failed!\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_SUCCESS;
        }

        if ((u1FwdData == SNOOP_FORWARD) && (u1IsUnReg == SNOOP_TRUE))
        {
            if ((SNOOP_SYSTEM_SPARSE_MODE (u4InstanceId) == SNOOP_ENABLE) &&
                (SNOOP_MCAST_FWD_MODE (u4InstanceId) ==
                 SNOOP_MCAST_FWD_MODE_MAC))
            {
                /* Program the NULL port vector in hardware to drop the
                 * unknown multicast data packet */
                u1FwdData = SNOOP_NO_FORWARD;
                SnoopUtilAddSourceInfo (u4InstanceId, pSnoopPktInfo, VlanId);
            }
        }
#ifndef NPAPI_WANTED

        if (u1FwdData == SNOOP_FORWARD)
        {
            /* If the Data falls under unregistered category 
             * then flood on all ports, else forward on specific ports */
            if (u1IsUnReg == SNOOP_FALSE)
            {
                u1Forward = VLAN_FORWARD_SPECIFIC;
            }
            else
            {
                u1Forward = VLAN_FORWARD_ALL;
            }

            SnoopVlanForwardPacket (u4InstanceId, VlanId,
                                    pSnoopPktInfo->GroupAddr.u1Afi,
                                    u4Port, u1Forward, pBuf, pPortBitmap, 0);
        }
        else
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        }
#else
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "CRU buffer release failed in packet processing\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "in packet processing\r\n");
        }
#endif

        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_SUCCESS;
    }

    /* IGMP/MLD Packet or Control message from router */

    /* Check whether snooping is disabled for this Vlan */

    if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                               pSnoopPktInfo->DestIpAddr.u1Afi,
                               &pSnoopVlanEntry) == SNOOP_SUCCESS)
    {
        if ((pSnoopVlanEntry != NULL) &&
            (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE))
        {
            /* 
             * The control packet will not be used by SNOOP. So this packet 
             * should be forwarded to all the member ports of the VLAN 
             */

            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC,
                            "Snooping Disabled for VLAN id %d. Hence fwd all!"
                            "\n", SNOOP_OUTER_VLAN (VlanId));

            SnoopVlanForwardPacket (u4InstanceId, VlanId,
                                    pSnoopPktInfo->GroupAddr.u1Afi,
                                    u4Port, VLAN_FORWARD_ALL, pBuf,
                                    pPortBitmap, 0);

            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_SUCCESS;
        }
    }
    else
    {
        /* VLAN entry not found, so create a new VLAN entry */
        if (SnoopVlanCreateVlanEntry (u4InstanceId, VlanId,
                                      pSnoopPktInfo->DestIpAddr.u1Afi,
                                      &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC, "Unable to create VLAN entry for "
                            " VLAN id %d\n", SNOOP_OUTER_VLAN (VlanId));
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }

        if (SNOOP_PROXY_STATUS (u4InstanceId, pSnoopPktInfo->DestIpAddr.u1Afi
                                - 1) == SNOOP_ENABLE)
        {
            /* Snooping is enabled and proxy is enabled. Send out general query 
             * over this vlan */

            if (pSnoopVlanEntry != NULL)
            {
                SnoopUtilSendGeneralQuery (u4InstanceId,
                                           pSnoopVlanEntry->u1AddressType,
                                           pSnoopVlanEntry, SNOOP_TRUE);

                SnoopRedActiveSendQuerierSync (u4InstanceId,
                                               pSnoopVlanEntry->VlanId,
                                               SNOOP_QUERIER,
                                               pSnoopVlanEntry->u1AddressType,
                                               pSnoopVlanEntry->
                                               u4ElectedQuerier);
            }
        }
    }

    /* 
     * ROUTER Control pkt 
     * 1. PIM packets  
     * 2. OSPF packets
     * 3. DVMRP packets
     */

    /* Do not process the control packet if the port is a
       blocked router port */
    if (pSnoopVlanEntry == NULL)
    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopVlanEntry->pSnoopVlanCfgEntry->
                           CfgBlockedRtrPortBmp, bResult);
    if (bResult == OSIX_TRUE)
    {
        if (SnoopVlanUpdateStats (u4InstanceId, pSnoopVlanEntry,
                                  SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_PKT_TRC, "Unable to update statistics "
                            "for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_SUCCESS;
    }

    if (pSnoopPktInfo->u1PktType == SNOOP_ROUTER_MSG)
    {
        /* 
         * Learn the router reachable port
         * Then this router control message will be forwarded over all the ports
         * except the port through which it was received, by VLAN 
         */

        /* Default operating version */
        u1Version = SNOOP_IGS_IGMP_VERSION3;
        /* Check if the port is already dynamically learnt as a router port 
         * If so just restart the router port purge timer */
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap, bResult);

        if (bResult == OSIX_TRUE)
        {
            /* This router port has already dynamically learnt.
             * Restart the router port purge timer */
            SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                            pRtrPortNode, tSnoopRtrPortEntry *)
            {
                if (pRtrPortNode->u4Port == u4Port)
                {
                    u1Version = pRtrPortNode->u1ConfigOperVer;
                    break;
                }
            }
        }
        /* Add Router port in the VLAN router port list entry */
        if (SnoopVlanUpdateLearntRtrPort (u4InstanceId, pSnoopVlanEntry,
                                          pSnoopPktInfo->u4InPort, u1PktType,
                                          u1Version,
                                          SNOOP_ZERO) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC,
                            "Router port addtion from router control messages "
                            "on port:%d VID:%d  failed\r\n", u4Port,
                            SNOOP_OUTER_VLAN (VlanId));
        }

        /* Do not foreward the router message to downstream/ upstream 
         * interfaces if PROXY is enabled */

        if (SNOOP_PROXY_STATUS (u4InstanceId, pSnoopVlanEntry->u1AddressType
                                - 1) == SNOOP_ENABLE)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_SUCCESS;
        }
#ifndef NPAPI_WANTED
        u1Forward = VLAN_FORWARD_ALL;
#else
        u1Forward = VLAN_NO_FORWARD;
#endif
        SnoopVlanForwardPacket (u4InstanceId, VlanId,
                                pSnoopPktInfo->GroupAddr.u1Afi,
                                u4Port, u1Forward, pBuf, pPortBitmap, 0);

        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_SUCCESS;
    }
    else
    {
        if (SnoopProcessIgmpOrMldPkt (u4InstanceId, VlanId, pSnoopVlanEntry,
                                      pSnoopPktInfo, pBuf) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_PKT_TRC, "Snoop packet processing failed "
                            "for Pkttype:%d, Port:%d\r\n", u1PktType, u4Port);

            /* 
             * Failure is not returned from here because, even though packet 
             * processing fails at IGMP snooping module, this packet has to be 
             * forwarded to all the member ports of the VLAN
             */

            if (SNOOP_PROXY_STATUS (u4InstanceId, pSnoopVlanEntry->u1AddressType
                                    - 1) == SNOOP_ENABLE)
            {
                /* If proxy is enabled then packets will not be forwarded */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_SUCCESS;
            }

            SnoopVlanForwardPacket (u4InstanceId, VlanId,
                                    pSnoopPktInfo->GroupAddr.u1Afi,
                                    u4Port, VLAN_FORWARD_ALL,
                                    pBuf, pPortBitmap, 0);

            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_SUCCESS;
        }
    }

    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessMulticastData                            */
/*                                                                           */
/* Description        : This function processes the incoming multicast       */
/*                      data packet                                          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId     - Vlan Id                                 */
/*                      pSnoopInfo - Packet Information                      */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - multicast packet                        */
/*                      PortBitmap - This will be filled if forwarding       */
/*                                   entry is available.                     */
/*                                                                           */
/* Output(s)          : pu1FwdData - Flag indicated whether to forward the   */
/*                                   packet or Not.                          */
/*                      pu1IsUnreg - Flag indicated whether a registered     */
/*                                   Forwarding Entry is available or Not.   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessMulticastData (UINT4 u4Instance, tSnoopTag VlanId,
                           tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port,
                           tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1FwdData,
                           UINT1 *pu1IsUnReg, tSnoopPortBmp PortBitmap)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopConsolidatedGroupEntry *pIgsConsGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopMacAddr       GrpMacAddr;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bCreateEntry = OSIX_FALSE;
    UINT4               u4Status = 0;

    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    *pu1FwdData = SNOOP_FORWARD;
    /* Check if the forwarding entries in the hardware table is full */
    if (gu2MaxHwEntryCount ==
        FsSNPSizingParams[MAX_SNOOP_IP_FWD_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits)
    {
        *pu1IsUnReg = SNOOP_TRUE;
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               pSnoopPktInfo->DestIpAddr.u1Afi,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        *pu1IsUnReg = SNOOP_TRUE;
        return SNOOP_SUCCESS;
    }

    if ((SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_ENABLE) &&
        (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
        (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
    {
        if (SnoopGrpGetGroupEntry
            (u4Instance, VlanId, pSnoopPktInfo->DestIpAddr,
             &pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_PKT_TRC, "Dropping the unknown multicast"
                       "packets in sparse mode enabled scenario"
                       "in control plane driven approach\r\n");
            SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, gu4IgsSysLogId,
                          "Dropping the unknown multicast packets in sparse mode"
                          "enabled scenario in control plane driven approach"));
            return SNOOP_FAILURE;

        }
    }

    /*Do not process the multicast packet if the port is a
       blocked router port */
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopVlanEntry->pSnoopVlanCfgEntry->
                           CfgBlockedRtrPortBmp, bResult);
    if (bResult == OSIX_TRUE)
    {
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_PKT_TRC, "Unable to update statistics "
                            "for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        return SNOOP_FAILURE;
    }

    if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
    {
        /* The control packet will not be used by SNOOP. So this packet 
         * should be forwarded to all the member ports of the VLAN 
         */

        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                        SNOOP_VLAN_TRC, "SNOOP Disabled for VLAN id %d. "
                        " Hence fwd all!\n", SNOOP_OUTER_VLAN (VlanId));

        *pu1IsUnReg = SNOOP_TRUE;
        return SNOOP_SUCCESS;
    }

    /* Check if group entry is present or not */
    if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC) &&
        (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
    {
        if (SnoopGrpGetGroupEntry
            (u4Instance, VlanId, pSnoopPktInfo->DestIpAddr,
             &pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            *pu1IsUnReg = SNOOP_TRUE;
            return SNOOP_SUCCESS;
        }
        else
        {
            *pu1IsUnReg = SNOOP_FALSE;
            SNOOP_MEM_CPY (PortBitmap, pSnoopGroupEntry->PortBitmap,
                           SNOOP_PORT_LIST_SIZE);
            return SNOOP_SUCCESS;
        }
    }

    /* Forwarding mode is MAC based so get the mac forwarding entry */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
#ifdef IGS_WANTED

        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
        {
            SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                     (pSnoopPktInfo->DestIpAddr.au1Addr),
                                     GrpMacAddr);
        }

#endif
#ifdef MLDS_WANTED
        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
        {
            SNOOP_GET_MAC_FROM_IPV6 (pSnoopPktInfo->DestIpAddr.au1Addr,
                                     GrpMacAddr);
        }
#endif

        if (SnoopFwdGetMacForwardingEntry (u4Instance, VlanId, GrpMacAddr,
                                           pSnoopPktInfo->DestIpAddr.u1Afi,
                                           &pSnoopMacGrpFwdEntry)
            != SNOOP_SUCCESS)
        {
            *pu1IsUnReg = SNOOP_TRUE;
        }
        else
        {
            SNOOP_MEM_CPY (PortBitmap, pSnoopMacGrpFwdEntry->PortBitmap,
                           SNOOP_PORT_LIST_SIZE);
            *pu1IsUnReg = SNOOP_FALSE;
        }
    }
    else                        /* Forwarding mode is IP so get IP forwarding entry */
    {
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            bCreateEntry = OSIX_TRUE;
        }

#ifdef NPAPI_WANTED
        bCreateEntry = OSIX_TRUE;
#endif
        if (bCreateEntry == OSIX_TRUE)
        {
            /* Check if group entry present */

            if (SnoopGrpGetGroupEntry
                (u4Instance, VlanId, pSnoopPktInfo->DestIpAddr,
                 &pSnoopGroupEntry) != SNOOP_SUCCESS)
            {
                /* Check if consolidated group entry present. Checking in consolidation
                   database is done to check whether group entry present for [SVLAN] 
                   in enhanced mode */
                if (SnoopGrpGetConsGroupEntry (u4Instance, VlanId,
                                               pSnoopPktInfo->DestIpAddr,
                                               &pIgsConsGroupEntry) !=
                    SNOOP_SUCCESS)
                {
                    /* create group entry */
                    if (SNOOP_VLAN_GRP_ALLOC_MEMBLK
                        (u4Instance, pSnoopGroupEntry) == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_DBG,
                                   "Maximum groups exceeded.Memory allocation for group entry failed\r\n");
                        SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, gu4IgsSysLogId,
                                      "Memory allocation for group entry failed"));
                        return SNOOP_FAILURE;
                    }

                    SNOOP_MEM_SET (pSnoopGroupEntry, 0,
                                   sizeof (tSnoopGroupEntry));

                    /* Set default values for the Group table */
                    SNOOP_MEM_CPY (&pSnoopGroupEntry->VlanId, VlanId,
                                   sizeof (tSnoopTag));

                    IPVX_ADDR_COPY (&(pSnoopGroupEntry->GroupIpAddr),
                                    &pSnoopPktInfo->DestIpAddr);

                    pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
                    pSnoopGroupEntry->u1EntryTypeFlag = SNOOP_GRP_DYNAMIC;
                    pSnoopGroupEntry->u1IsUnknownMulticast = SNOOP_TRUE;
                    SNOOP_SLL_INIT (&pSnoopGroupEntry->ASMPortList);
                    SNOOP_DLL_INIT_NODE (&pSnoopGroupEntry->NextGrpNode);

                    /* Add Node */
                    u4Status =
                        RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                   (tRBElem *) pSnoopGroupEntry);

                    if (u4Status == RB_FAILURE)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_DBG,
                                   "RBTree Addition Failed for Group Entry \r\n");
                        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                    pSnoopGroupEntry);
                        return SNOOP_FAILURE;
                    }
                    /* Adding the new group entry to the DLL present in the
                     *      * consolidated Group entry.*/
                    if (SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                                   SNOOP_CREATE_GRP_ENTRY) ==
                        SNOOP_FAILURE)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_DBG,
                                   "Updation of consolidated group entry failed\r\n");
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      GroupEntry, (tRBElem *) pSnoopGroupEntry);
                        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                    pSnoopGroupEntry);
                        return SNOOP_FAILURE;
                    }
                }
            }
        }

        if (SnoopProcessMulticastDataOnIpMode
            (u4Instance, VlanId, pSnoopVlanEntry, pSnoopPktInfo, u4Port,
             pBuf, pu1FwdData, pu1IsUnReg) == SNOOP_FAILURE)
        {
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessIgmpOrMldPkt                             */
/*                                                                           */
/* Description        : This function processes the incoming IGMP/MLD packets*/
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      VlanId        - VLAN Identifier                      */
/*                      pSnoopVlanEntry - pointer to the VLAN entry          */
/*                      pSnoopPktInfo   - pointer to packet information      */
/*                      pBuf          - pointer to the packet buffer         */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessIgmpOrMldPkt (UINT4 u4Instance, tSnoopTag VlanId,
                          tSnoopVlanEntry * pSnoopVlanEntry,
                          tSnoopPktInfo * pSnoopPktInfo,
                          tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pVlanPortBitmap = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pPortBitmap = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tIPvXAddr           GrpAddr;
    tSnoopMacAddr       GrpMacAddr;
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2CVid = 0;
    UINT4               u4PhyPort = 0;
    UINT4               u4SrcAddr = 0;
    UINT1               u1Forward = 0;
    UINT1               u1Version = 0;
    UINT2               u2HdrLen = 0;
    UINT4               u4BufSize = 0;
    UINT2               u2ChkSumResult = 0;
    UINT1               u1TagLen = pSnoopPktInfo->u1TagLen;
#ifdef MLDS_WANTED
    UINT2               u2TmpCheckSum = 0;
    UINT2               u2RecvCheckSum = 0;
#endif
    tSnoopFilter        SnoopFilterPkt;
    UINT1               u1QueryType = 0;
    INT4                i4RetVal = SNOOP_SUCCESS;

    SNOOP_MEM_SET (&SnoopFilterPkt, 0, sizeof (tSnoopFilter));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessIgmpOrMldPkt\r\n");
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);

    /* Get the port entry; If port entry is absent, no port-based
     * action will be taken for the packet.*/
    i4RetVal = SnoopGetPortCfgEntry (u4Instance, u4PhyPort, VlanId,
                                     pSnoopPktInfo->GroupAddr.u1Afi,
                                     &pSnoopPortCfgEntry);
    UNUSED_PARAM (i4RetVal);

    /* Verify the IGMP checksum before processing the packet */

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u4BufSize =
            (UINT2) (pSnoopPktInfo->u2PktLen - pSnoopPktInfo->u1IpHdrLen);
        u2ChkSumResult =
            IgsUtilIgmpCalcCheckSum (pBuf, u4BufSize,
                                     SNOOP_DATA_LINK_HDR_LEN +
                                     (UINT4) u1TagLen +
                                     pSnoopPktInfo->u1IpHdrLen);
    }

    if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                 (pSnoopPktInfo->DestIpAddr.au1Addr),
                                 GrpMacAddr);
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        u4BufSize =
            (UINT4) (pSnoopPktInfo->u2PktLen - SNOOP_MLD_HOP_BY_HOP_HDR_LEN);

        /* The header is not contiguous in the buffer */
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2RecvCheckSum,
                                   (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                    SNOOP_IPV6_HDR_RTR_OPT_LEN + 2),
                                   sizeof (UINT2));

        if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2TmpCheckSum,
                                       (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        SNOOP_IPV6_HDR_RTR_OPT_LEN + 2),
                                       sizeof (UINT2)) == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessIgmpOrMldPkt\r\n");
            return SNOOP_FAILURE;
        }

        u2ChkSumResult = MldsUtilCalculateCheckSum (pBuf, u4BufSize,
                                                    (SNOOP_DATA_LINK_HDR_LEN +
                                                     u1TagLen +
                                                     pSnoopPktInfo->u1IpHdrLen),
                                                    pSnoopPktInfo->SrcIpAddr.
                                                    au1Addr,
                                                    pSnoopPktInfo->DestIpAddr.
                                                    au1Addr);
        if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2RecvCheckSum,
                                       (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        SNOOP_IPV6_HDR_RTR_OPT_LEN + 2),
                                       sizeof (UINT2)) == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessIgmpOrMldPkt\r\n");
            return SNOOP_FAILURE;
        }
        u2ChkSumResult = (UINT2) (u2RecvCheckSum - u2ChkSumResult);
    }

    if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        SNOOP_GET_MAC_FROM_IPV6 (pSnoopPktInfo->DestIpAddr.au1Addr, GrpMacAddr);
    }
#endif

    /* Checking whether wildcard entry is available for the MAC
     * If present do not update the group and forwarding table */
#ifndef IGS_OPTIMIZE
    if (SnoopVlanCheckWildCardEntry (GrpMacAddr) == SNOOP_TRUE)
    {
        return SNOOP_FAILURE;
    }
#endif

#ifdef MLDS_WANTED
    if (u2ChkSumResult != 0)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_PKT_TRC, "packet verification failure due "
                   "to invalid chksum\r\n");

        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_PKT_ERROR) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_PKT_TRC, "Unable to update statistics "
                            "for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        return SNOOP_FAILURE;
    }
#endif
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pPortBitmap\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessIgmpOrMldPkt\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));
    SnoopCfaGetIfInfo (SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort),
                       &CfaIfInfo);

    /* If Inner Vlan Id is not present in the packet, and the packet
       received on CEP , then add the CPVID as the inner vlanid. 
     */

    if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE) &&
        (CfaIfInfo.u1BrgPortType == CFA_CUSTOMER_EDGE_PORT) &&
        (SNOOP_INNER_VLAN (VlanId) == SNOOP_INVALID_VLAN_ID))
    {
        if (SnoopVlanGetCpvidForCep
            (SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort),
             &u2CVid) == SNOOP_SUCCESS)
        {
            SNOOP_INNER_VLAN (VlanId) = u2CVid;
        }
    }

    switch (pSnoopPktInfo->u1PktType)
    {
        case SNOOP_IGMP_V1REPORT:
        case SNOOP_IGMP_V2REPORT:
        case SNOOP_MLD_V1REPORT:

            if ((pSnoopPortCfgEntry != NULL) &&
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                 u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_CHANNELS))
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Packets dropped. Threshold "
                                "mode type is CHANNELS for port %d\r\n",
                                SNOOP_GET_IFINDEX (u4Instance,
                                                   pSnoopPktInfo->u4InPort));
                u1Forward = SNOOP_RELEASE_BUFFER;
                /*Updating the statistics for Unsuccessful join count */
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_UNSUCCESSFUL_JOINS) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                break;
            }

            /* reports will be filtered before processing. if the filter
               function returns DENY, report will not be processed */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &(SnoopFilterPkt.MacAddr),
                                       sizeof (tSnoopMacAddr),
                                       sizeof (tSnoopMacAddr));
            SnoopFilterPkt.u1NotifyType = SNOOP_FILTER_PKT_NOTIFY;

            SNOOP_MEM_CPY (&SnoopFilterPkt.VlanId, VlanId, sizeof (tSnoopTag));

            SNOOP_MEM_CPY (SnoopFilterPkt.SrcIpAddr,
                           pSnoopPktInfo->SrcIpAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (SnoopFilterPkt.GrpAddress,
                           pSnoopPktInfo->GroupAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_INET_NTOHL (SnoopFilterPkt.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopFilterPkt.GrpAddress);

            /* TODO whether to pass physical or local port to this call 
             * */
            SnoopFilterPkt.u4InPort = pSnoopPktInfo->u4InPort;
            SnoopFilterPkt.u1PktType = pSnoopPktInfo->u1PktType;

#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                SnoopFilterPkt.u1Version = ((pSnoopPktInfo->u1PktType ==
                                             SNOOP_IGMP_V1REPORT) ?
                                            SNOOP_IGS_IGMP_VERSION1 :
                                            SNOOP_IGS_IGMP_VERSION2);
            }
#endif

#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                SnoopFilterPkt.u1Version = SNOOP_MLD_VERSION1;
            }
#endif

            if (SnoopUtilFilterGrpsAndNotify (&SnoopFilterPkt) == SNOOP_DENY)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "IGMP V1/V2 Report message filterd out\n");
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_UNSUCCESSFUL_JOINS) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }

            if (SnoopProcessReportMessageHandler (u4Instance, VlanId,
                                                  pSnoopVlanEntry,
                                                  pSnoopPktInfo,
                                                  pSnoopPortCfgEntry,
                                                  &u1Forward,
                                                  pPortBitmap,
                                                  pBuf) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP,
                           SNOOP_PKT_TRC,
                           "ASM Report message processing failed\n");
                /*Updating the statistics for Unsuccessful join count */
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_UNSUCCESSFUL_JOINS) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_ALL_FAILURE_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }

            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_ASMREPORT_RCVD) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_DBG_VLAN | SNOOP_ALL_FAILURE_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update Snoop "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
            if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_V1REPORT_RCVD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_ALL_FAILURE_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "on receiving v1 report for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }
            else if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V2REPORT)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_V2REPORT_RCVD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_ALL_FAILURE_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "on receiving v2 report for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
            }
            break;

        case SNOOP_IGMP_QUERY:
        case SNOOP_MLD_QUERY:

            /* query will be filtered before processing. if the filter
               function returns DENY, query will not be processed */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &(SnoopFilterPkt.MacAddr),
                                       sizeof (tSnoopMacAddr),
                                       sizeof (tSnoopMacAddr));

            SnoopFilterPkt.u1NotifyType = SNOOP_FILTER_PKT_NOTIFY;

            SNOOP_MEM_CPY (&SnoopFilterPkt.VlanId, VlanId, sizeof (tSnoopTag));

            SNOOP_MEM_CPY (SnoopFilterPkt.SrcIpAddr,
                           pSnoopPktInfo->SrcIpAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (SnoopFilterPkt.GrpAddress,
                           pSnoopPktInfo->GroupAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_INET_NTOHL (SnoopFilterPkt.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopFilterPkt.GrpAddress);
            SnoopFilterPkt.u4InPort = pSnoopPktInfo->u4InPort;
#ifdef IGS_WANTED
            SnoopFilterPkt.u1PktType = SNOOP_IGMP_QUERY;
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {

                u2HdrLen =
                    (UINT2) (pSnoopPktInfo->u2PktLen -
                             pSnoopPktInfo->u1IpHdrLen);
                if (u2HdrLen == SNOOP_IGMP_HEADER_LEN)
                {
                    if (pSnoopPktInfo->u2MaxRespCode == 0)
                    {
                        u1Version = SNOOP_IGS_IGMP_VERSION1;
                    }
                    else
                    {
                        u1Version = SNOOP_IGS_IGMP_VERSION2;
                    }
                }
                else
                {
                    u1Version = SNOOP_IGS_IGMP_VERSION3;
                }
            }
#endif

#ifdef MLDS_WANTED
            SnoopFilterPkt.u1PktType = SNOOP_MLD_QUERY;
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                u2HdrLen =
                    (UINT2) (pSnoopPktInfo->u2PktLen -
                             SNOOP_MLD_HOP_BY_HOP_HDR_LEN);
                if (u2HdrLen == SNOOP_MLD_HDR_LEN)
                {
                    u1Version = SNOOP_MLD_VERSION1;
                }
                else
                {
                    u1Version = SNOOP_MLD_VERSION2;
                }
            }

#endif
            SnoopFilterPkt.u1Version = u1Version;

            if (SnoopUtilFilterGrpsAndNotify (&SnoopFilterPkt) == SNOOP_DENY)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_QRY | SNOOP_ALL_FAILURE_TRC, SNOOP_QRY_DBG,
                           "IGMP Query message filterd out\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }

            /* For Proxy, learn the router port only if 
             * 1. The senders IP is less than our switch IP */
            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
                SNOOP_FORWARD_NON_RTR_PORTS)
            {
                if (SNOOP_PROXY_STATUS (u4Instance,
                                        pSnoopVlanEntry->u1AddressType - 1)
                    == SNOOP_ENABLE)
                {
                    MEMCPY (&u4SrcAddr, pSnoopPktInfo->SrcIpAddr.au1Addr,
                            IPVX_IPV4_ADDR_LEN);

                    if (u4SrcAddr > gu4SwitchIp)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        UtilPlstReleaseLocalPortList (pPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                        return SNOOP_SUCCESS;
                    }
                }
            }

            if (SnoopProcessQueryMessageHandler (u4Instance, VlanId,
                                                 pSnoopVlanEntry,
                                                 pSnoopPktInfo, pBuf, u1Version)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_QRY | SNOOP_ALL_FAILURE_TRC, SNOOP_QRY_DBG,
                           "Query message processing failed\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_QRY,
                            SNOOP_QRY_DBG, "Query message processing successful"
                            "on interface %u\n", u4Instance);
            if (pSnoopPktInfo->u1QueryType == SNOOP_GENERAL_QUERY)
            {
                u1QueryType = SNOOP_QUERY_RCVD;
            }
            else if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_QUERY)
            {
                u1QueryType = SNOOP_GRP_QUERY_RCVD;
            }
            else if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_SRC_QUERY)
            {
                u1QueryType = SNOOP_GRP_SRC_QUERY_RCVD;
            }
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      u1QueryType) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_DBG_VLAN | SNOOP_ALL_FAILURE_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update Snoop "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
            break;

        case SNOOP_IGMP_V3REPORT:
        case SNOOP_MLD_V2REPORT:

            if (SnoopProcessSSMReportMessageHandler
                (u4Instance, VlanId, pSnoopVlanEntry, pSnoopPktInfo,
                 pSnoopPortCfgEntry, &u1Forward,
                 pPortBitmap, pBuf) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "SSM Report message processing failed\n");
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_UNSUCCESSFUL_JOINS) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_QRY_TRC,
                            "SSM Report message processing successful for instance %u\n",
                            u4Instance);
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_MGMT_DBG,
                            "statistics updated"
                            "for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_SSMREPORT_RCVD) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update Snoop "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
            break;

        case SNOOP_IGMP_LEAVE:
        case SNOOP_MLD_DONE:

            /* leaves will be filtered before processing. if the filter
               function returns DENY, report will not be processed */
            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &(SnoopFilterPkt.MacAddr),
                                       sizeof (tSnoopMacAddr),
                                       sizeof (tSnoopMacAddr));

            SnoopFilterPkt.u1NotifyType = SNOOP_FILTER_PKT_NOTIFY;

            SNOOP_MEM_CPY (&SnoopFilterPkt.VlanId, VlanId, sizeof (tSnoopTag));

            SNOOP_MEM_CPY (SnoopFilterPkt.SrcIpAddr,
                           pSnoopPktInfo->SrcIpAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (SnoopFilterPkt.GrpAddress,
                           pSnoopPktInfo->GroupAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_INET_NTOHL (SnoopFilterPkt.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopFilterPkt.GrpAddress);
            SnoopFilterPkt.u4InPort = pSnoopPktInfo->u4InPort;
            SnoopFilterPkt.u1PktType = pSnoopPktInfo->u1PktType;

#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                SnoopFilterPkt.u1Version = SNOOP_IGS_IGMP_VERSION2;
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                SnoopFilterPkt.u1Version = SNOOP_MLD_VERSION1;
            }
#endif

            if (SnoopUtilFilterGrpsAndNotify (&SnoopFilterPkt) == SNOOP_DENY)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_GRP | SNOOP_ALL_FAILURE_TRC, SNOOP_PKT_TRC,
                           "Leave message filted out\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }

            if (SnoopProcessLeaveOrDoneMessage (u4Instance, VlanId,
                                                pSnoopVlanEntry,
                                                pSnoopPktInfo,
                                                pSnoopPortCfgEntry,
                                                &u1Forward,
                                                pPortBitmap) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ALL_FAILURE_TRC,
                           SNOOP_PKT_TRC, "Leave message processing failed\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_DBG_ARG2 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_PKT_TRC,
                            "Leave message processing done for instance"
                            "%u for port %d\n", u4Instance,
                            pSnoopPktInfo->u4InPort);

            if (SnoopVlanUpdateStats
                (u4Instance, pSnoopVlanEntry,
                 SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }

            break;

        default:
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Invalid packet type received\r\n");
    }

    /* If sparse mode is enabled, the report/leave forwarding is done based on
     * learnt source port information */
    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessIgmpOrMldPkt\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    if ((pSnoopPktInfo->u1PktType == SNOOP_IGMP_V3REPORT) ||
        (pSnoopPktInfo->u1PktType == SNOOP_MLD_V2REPORT) ||
        (pSnoopPktInfo->u1PktType == SNOOP_IGMP_LEAVE))
    {
        if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (u1Forward != SNOOP_RELEASE_BUFFER))
        {
            IPVX_ADDR_COPY (&GrpAddr, &(pSnoopPktInfo->GroupAddr));

            /* Get the group entry for the given group address */
            if (SnoopGrpGetGroupEntry (u4Instance, pSnoopVlanEntry->VlanId,
                                       GrpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                {
                    u1Forward = SNOOP_RELEASE_BUFFER;
                }
            }
        }
        if (u1Forward != SNOOP_RELEASE_BUFFER)
        {
            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_NONEDGE_PORTS)
            {
                pVlanPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pVlanPortBitmap == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_DBG,
                               "Error in allocating memory for pVlanPortBitmap\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessIgmpOrMldPkt\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pVlanPortBitmap, 0, sizeof (tSnoopPortBmp));
                if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                    pSnoopVlanEntry->VlanId,
                                                    pVlanPortBitmap) ==
                    SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                }
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);

                if (SNOOP_MEM_CMP (pFwdPortBitmap, gNullPortBitMap,
                                   sizeof (tSnoopPortBmp)) == 0)
                {
                    u1Forward = SNOOP_RELEASE_BUFFER;
                }
            }
        }
    }

    if (u1Forward == SNOOP_RELEASE_BUFFER)
    {
        /* Release the incoming packet buffer if u1Forward flag is set 
         * to SNOOP_RELEASE_BUFFER */
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_PKT_TRC,
                       "CRU buffer release failed in packet processing\r\n");
        }
    }
    else if (u1Forward)
    {
        if ((pSnoopPktInfo->u1PktType == SNOOP_IGMP_V3REPORT) ||
            (pSnoopPktInfo->u1PktType == SNOOP_MLD_V2REPORT) ||
            (pSnoopPktInfo->u1PktType == SNOOP_IGMP_LEAVE))
        {
            u1Forward =
                ((SNOOP_INSTANCE_INFO (u4Instance)->
                  SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                  u1ReportFwdAll ==
                  SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                 VLAN_FORWARD_SPECIFIC);

            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_RTR_PORTS)
            {
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_CPY (pFwdPortBitmap, pPortBitmap,
                               sizeof (tSnoopPortBmp));
            }
        }
        else
        {
            SNOOP_MEM_CPY (pFwdPortBitmap, pPortBitmap, sizeof (tSnoopPortBmp));
        }

        /* Forward the reports/ leave on to the constructed port-list */
        SnoopVlanForwardPacket (u4Instance, VlanId,
                                pSnoopVlanEntry->u1AddressType,
                                pSnoopPktInfo->u4InPort, u1Forward, pBuf,
                                pFwdPortBitmap, 0);
    }
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessIgmpOrMldPkt\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessQueryMessageHandler                      */
/*                                                                           */
/* Description        : This function processes the incoming IGMP/MLD query  */
/*                      packets                                              */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopPktInfo   - Packet information                 */
/*                      pBuf          - pointer to the packet Buffer         */
/*                      u1Version - The version of the IGMP/MLD query packet */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessQueryMessageHandler (UINT4 u4Instance, tSnoopTag VlanId,
                                 tSnoopVlanEntry * pSnoopVlanEntry,
                                 tSnoopPktInfo * pSnoopPktInfo,
                                 tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Version)
{
    UINT1              *pEgressPortBitmap = NULL;
    UINT1              *pBlkPortBitmap = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntryNode = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tIPvXAddrSource    *pSourceAddr = NULL;
    tIPvXAddr           NullIpAddr;
    UINT4               u4QuerierV3QryInt = 0;
    UINT4               u4OldVerQryTimeoutInt = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT2               u2SrcCount = 0;
    UINT2               u2Count = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2SrcOffSet = 0;
    UINT2               u2HdrLen = 0;
    UINT2               u2IpHdrLen = 0;
    UINT2               u2Ipv4HdrLen = 0;
    UINT2               u2QQICOffSet = 0;
    UINT2               u2MaxRespTime = 0;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1TagLen = pSnoopPktInfo->u1TagLen;
    UINT4               u4ByteIndex = 0;
    UINT1               u1QQIC = 0;
    UINT1               u1QRV = 0;
    UINT1               u1QuerierStatus = SNOOP_FALSE;
    UINT4               u4StatType = 0;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessQueryMessageHandler\r\n");
    if (SNOOP_IPVX_ADDR_ALLOC_MEMBLK (pSourceAddr) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG, "Memory allocation for IPvX entry \r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for IPvX entry \r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessQueryMessageHandler\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pSourceAddr, 0,
                   sizeof (tIPvXAddrSource) * SNOOP_MAX_MCAST_SRCS);
    /* 
     * If the switch is configured as querier,then we need to make
     * the switch as a non querier as there is another router present 
     * in the VLAN  
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        return SNOOP_FAILURE;
    }

    if (((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV4) &&
        ((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV6))
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Not a Valid Address Type\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_QRY_DBG, SNOOP_QRY_DBG,
                   "Invalid query received\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessQueryMessageHandler\r\n");
        return SNOOP_FAILURE;
    }

#ifdef L2RED_WANTED
    /* Avoid query processing during switch over as group table will be built completely
     * only after switch over query timer expiry*/
    if (SNOOP_RED_GENERAL_QUERY_INPROGRESS == SNOOP_TRUE)
    {
        if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_SRC_QUERY)
        {
            if (SnoopVlanUpdateStats
                (u4Instance, pSnoopVlanEntry,
                 SNOOP_GRP_SRC_QUERY_DROPPED) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update Snoop "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }

        }
        else if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_QUERY)
        {
            if (u1Version == SNOOP_IGS_IGMP_VERSION2)
            {
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_V2GRP_QUERY_DROPPED) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update Snoop "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }
            else
            {
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_V3GRP_QUERY_DROPPED) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update Snoop "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }

        }
        else
        {
            if (u1Version == SNOOP_IGS_IGMP_VERSION1)
            {
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_V1QUERY_DROPPED) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update Snoop "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }
            else if (u1Version == SNOOP_IGS_IGMP_VERSION2)
            {
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_V2QUERY_DROPPED) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update Snoop "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }
            else
            {
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_V3QUERY_DROPPED) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update Snoop "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }

        }
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RED | SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_RED_NAME,
                       "Query message is not processed as switch over is in progress"
                       "\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessQueryMessageHandler\r\n");
        return SNOOP_FAILURE;
    }
#endif

    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&NullIpAddr, 0, sizeof (tIPvXAddr));

    if ((SNOOP_PROXY_STATUS (u4Instance, pSnoopVlanEntry->u1AddressType - 1)
         == SNOOP_DISABLE))
    {
        u4SrcAddr = SNOOP_PTR_FETCH_4 (pSnoopPktInfo->SrcIpAddr.au1Addr);

        /* if the elected querier info is 0.0.0.0 , update from the incoming
         * querier .
         * if the incoming query ip is lesser than the elected querier ip
         * moving the switch to non-querier.*/

        /* if the switch is acting in version v1 mode , it doesnt take part in query
         * election . It aceepts any query as elected querier without any comparison */

        if (((pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL) &&
             (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer ==
              SNOOP_IGS_IGMP_VERSION1)) || (((u4SrcAddr != 0)
                                             && (pSnoopVlanEntry->
                                                 u4ElectedQuerier == 0))
                                            || ((u4SrcAddr != 0)
                                                && (u4SrcAddr <=
                                                    pSnoopVlanEntry->
                                                    u4ElectedQuerier))))
        {
            /*
             * It seems to be from a lower address switch.
             * If this switch is the Querier, let the lower address switch continue querying, 
             * it is time to rest for a while.
             */
            u1QuerierStatus = SNOOP_TRUE;
            if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
            {
                SnoopHandleTransitionToNonQuerier (u4Instance, pSnoopVlanEntry);
            }
            if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
            }

            if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
            {
                if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier
                    == SNOOP_QUERIER)
                {
                    /* If the switch is configured as querier and it receives a 
                     * query packet with lower IP address,other querier present timer
                     * needs to be started */
                    /* Start Other Querier present interval timer */
                    pSnoopVlanEntry->OtherQPresentTimer.u4Instance = u4Instance;
                    pSnoopVlanEntry->OtherQPresentTimer.u1TimerType =
                        SNOOP_OTHER_QUERIER_PRESENT_TIMER;
                    SnoopTmrStartTimer (&pSnoopVlanEntry->OtherQPresentTimer,
                                        pSnoopVlanEntry->u2OtherQPresentInt);

                    /* Update the elected Querier - This switch is not the querier */
                    pSnoopVlanEntry->u4ElectedQuerier = u4SrcAddr;
                    SnoopRedActiveSendQuerierSync (u4Instance,
                                                   pSnoopVlanEntry->VlanId,
                                                   pSnoopVlanEntry->u1Querier,
                                                   pSnoopVlanEntry->
                                                   u1AddressType,
                                                   pSnoopVlanEntry->
                                                   u4ElectedQuerier);

                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_TMR_TRC,
                                    "Other Querier Present timer restarted for %d seconds\r\n",
                                    pSnoopVlanEntry->u2OtherQPresentInt);
                }
            }
        }
        else if ((pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL) &&
                 (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier
                  == SNOOP_NON_QUERIER))
        {
            u1QuerierStatus = SNOOP_TRUE;
        }

    }
    else
    {
        u1QuerierStatus = SNOOP_TRUE;
    }

    if (u1QuerierStatus == SNOOP_TRUE)
    {
        if (((u1Version == SNOOP_IGS_IGMP_VERSION3) &&
             (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)) ||
            ((u1Version == SNOOP_MLD_VERSION2) &&
             (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)))
        {
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {

                u2QQICOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        pSnoopPktInfo->u1IpHdrLen +
                                        SNOOP_MLD_HDR_LEN);

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1QQIC,
                                           (UINT4) u2QQICOffSet +
                                           SNOOP_V3_QQIC_OFFSET,
                                           sizeof (UINT1));

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1QRV,
                                           u2QQICOffSet + SNOOP_V3_QRV_OFFSET,
                                           sizeof (UINT1));

                u2MaxRespTime = (UINT2) (SNOOP_MLDS_GET_TIME_FROM_CODE
                                         (pSnoopPktInfo->u2MaxRespCode));
                u2MaxRespTime = (UINT2) (u2MaxRespTime / SNOOP_MLDS_MRC_UNIT);
                u1QRV = (UINT1) SNOOP_GET_QRV (u1QRV);

            }
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                u2QQICOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        pSnoopPktInfo->u1IpHdrLen +
                                        SNOOP_IGMP_HEADER_LEN);

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1QQIC,
                                           (UINT4) (u2QQICOffSet +
                                                    SNOOP_V3_QQIC_OFFSET),
                                           sizeof (UINT1));

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1QRV,
                                           u2QQICOffSet + SNOOP_V3_QRV_OFFSET,
                                           sizeof (UINT1));
                if (u1Version == SNOOP_IGS_IGMP_VERSION3)
                {
                    u2MaxRespTime = (UINT2) (SNOOP_IGS_GET_TIME_FROM_CODE
                                             (pSnoopPktInfo->u2MaxRespCode));
                }
                else
                {
                    u2MaxRespTime = pSnoopPktInfo->u2MaxRespCode;
                }
                u2MaxRespTime = (UINT2) (u2MaxRespTime / SNOOP_IGS_MRC_UNIT);

                u1QRV = (UINT1) SNOOP_GET_QRV (u1QRV);
            }

            /* get the querier's query interval from Querier's query interval 
             * code (QQIC) */

            u4QuerierV3QryInt = ((u1QQIC == SNOOP_ZERO) ?
                                 SNOOP_DEF_RTR_PURGE_INTERVAL :
                                 (UINT4) (SNOOP_GET_QQI_FROM_QQIC (u1QQIC)));

            /* If Querier's Robustness Variable(QRV) is Zero then
               take the default retry count as QRV */

            if (u1QRV == SNOOP_ZERO)
            {
                u1QRV = SNOOP_DEF_RETRY_COUNT;
            }

            /* Router port purge interval for a V3 upstream querier =
               (Robustness Variable (i.e) retry count * query interval in the 
               last query received) + Query Response Interval */

            u4OldVerQryTimeoutInt =
                ((u4QuerierV3QryInt * u1QRV) + u2MaxRespTime);

        }
#ifdef IGS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            u2HdrLen =
                (UINT2) (pSnoopPktInfo->u2PktLen - pSnoopPktInfo->u1IpHdrLen);
        }
#endif

#ifdef MLDS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            u2HdrLen =
                (UINT2) (pSnoopPktInfo->u2PktLen -
                         SNOOP_MLD_HOP_BY_HOP_HDR_LEN);
        }
#endif

        /* Update the router port to VLAN router port list only if source IP 
           is non-zero. Query with with zero source IP will be send out for
           faster convergence. So the port on which query with zero source ip 
           is received should not be learn as router port. It just needs to be
           replied with consolidated report. */

        if (MEMCMP (pSnoopPktInfo->SrcIpAddr.au1Addr, NullIpAddr.au1Addr,
                    MEM_MAX_BYTES (pSnoopPktInfo->SrcIpAddr.u1AddrLen,
                                   IPVX_MAX_INET_ADDR_LEN)))
        {
            /* Update the router port to the VLAN router port list */
            if (SnoopVlanUpdateLearntRtrPort (u4Instance, pSnoopVlanEntry,
                                              pSnoopPktInfo->u4InPort,
                                              pSnoopPktInfo->u1PktType,
                                              u1Version, u4QuerierV3QryInt)
                != SNOOP_SUCCESS)
            {
                SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE, SNOOP_QRY_TRC,
                           "Router port updation failed\r\n");
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessQueryMessageHandler\r\n");
                return SNOOP_FAILURE;
            }

            /* Update the inner vlan and the Robust router Port purge interval for the router port entry */

            SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                            pRtrPortNode, tSnoopRtrPortEntry *)
            {

                if (pRtrPortNode->u4PhyPortIndex ==
                    SNOOP_GET_IFINDEX (u4Instance,
                                       (UINT2) pSnoopPktInfo->u4InPort))
                {
                    pRtrPortNode->u2InnerVlanId = SNOOP_INNER_VLAN (VlanId);
                    pRtrPortNode->u4V3RtrPortPurgeInt = u4OldVerQryTimeoutInt;
                    break;
                }

            }

            if (((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV4) &&
                ((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV6))
            {
                SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "Not a Valid Address Type\r\n");
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_QRY_DBG, SNOOP_QRY_DBG,
                           "Invalid query received\r\n");
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessQueryMessageHandler\r\n");
                return SNOOP_FAILURE;
            }

        }
    }

    if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_SRC_QUERY)
    {
        /* Extract the source information (source count and source addresses
         * from the buffer */
#ifdef IGS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            u2SrcOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                   pSnoopPktInfo->u1IpHdrLen +
                                   SNOOP_IGMP_HEADER_LEN);

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SrcCount,
                                       (UINT4) (u2SrcOffSet +
                                                SNOOP_SRC_CNT_OFFSET),
                                       SNOOP_SRC_CNT_OFFSET);

            u2IpHdrLen = SNOOP_IP_ADDR_SIZE;
            u2Ipv4HdrLen = SNOOP_IP_ADDR_SIZE;
        }
#endif

#ifdef MLDS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (u2HdrLen == SNOOP_MLD_HDR_LEN)
            {
                u2SrcOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                       pSnoopPktInfo->u1IpHdrLen +
                                       SNOOP_MLD_HDR_LEN);
            }
            else
            {
                u2SrcOffSet = (UINT2) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                       pSnoopPktInfo->u1IpHdrLen +
                                       SNOOP_MLD_HDR_LEN + SNOOP_MLDV2_QRY_HDR);
            }

            u2IpHdrLen = IPVX_IPV6_ADDR_LEN;

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2SrcCount,
                                       (UINT4) (u2SrcOffSet -
                                                SNOOP_SRC_CNT_OFFSET),
                                       SNOOP_SRC_CNT_OFFSET);
        }
#endif

        u2SrcCount = SNOOP_NTOHS (u2SrcCount);
        if (u2SrcCount > SNOOP_MAX_MCAST_SRCS)
        {
            SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_QRY | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_QRY_DBG,
                       "Unable to process query message - Source count exceeded\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessQueryMessageHandler\r\n");
            return SNOOP_FAILURE;
        }

        for (u2Count = 0; u2Count < u2SrcCount; u2Count++)
        {
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) au1SrcAddr,
                                       (UINT4) (u2SrcOffSet + u2Ipv4HdrLen +
                                                u2OffSet), u2IpHdrLen);

            SNOOP_INET_NTOHL (au1SrcAddr);
            IPVX_ADDR_INIT (pSourceAddr[u2Count],
                            pSnoopVlanEntry->u1AddressType,
                            (UINT1 *) &au1SrcAddr);
            u2OffSet = (UINT2) (u2OffSet + u2IpHdrLen);
        }

#ifndef SWITCH_IGMP_QUERY_IN_HW
        if (SNOOP_PROXY_REPORTING_STATUS (u4Instance, pSnoopVlanEntry->
                                          u1AddressType - 1) == SNOOP_ENABLE)
        {
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
                    (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
                    SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN;

                pOutBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

                if (pOutBuf == NULL)
                {
                    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                    SNOOP_DBG (SNOOP_IGS_TRC_FLAG, SNOOP_BUFFER_TRC,
                               SNOOP_OS_RES_DBG,
                               "Buffer allocation failure in forming query messages\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                                           SNOOP_BUFFER_TRC,
                                           SnoopSysErrString
                                           [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                           "in forming query messages\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessQueryMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }

                IgsFormGrpQueryFromGrpSrcQuery (u4Instance, pBuf, pSnoopPktInfo,
                                                pOutBuf);
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                /* Allocate buffer for MAC (14) + IPV6 (48) with router 
                 * alert option + MLD (24) header = 86 bytes. */
                u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
                    (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
                    SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN;

                pOutBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

                if (pOutBuf == NULL)
                {
                    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_BUFFER_TRC,
                               SNOOP_OS_RES_DBG,
                               "Buffer allocation failure while constructing  "
                               "MLD query messages\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                                           SNOOP_BUFFER_TRC,
                                           SnoopSysErrString
                                           [SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                                           "while constructing MLD query messages\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessQueryMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }

                MldsFormGrpQueryFrmGrpSrcQuery (u4Instance, pBuf, pSnoopPktInfo,
                                                pOutBuf);
            }
#endif
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            pBuf = pOutBuf;
        }
#endif
    }

#ifndef SWITCH_IGMP_QUERY_IN_HW

    /* Foreward the query only if Proxy is disabled */
    if (SNOOP_PROXY_STATUS (u4Instance, pSnoopVlanEntry->u1AddressType - 1)
        == SNOOP_DISABLE)
    {
        pEgressPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pEgressPortBitmap == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_DBG,
                       "Error in allocating memory for pEgressPortBitmap\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "in allocating memory for pEgressPortBitmap\r\n");
            SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessQueryMessageHandler\r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pEgressPortBitmap, 0, sizeof (tSnoopPortBmp));

        /* Forward the query message to all the member ports of that VLAN 
         * if the query is not already switched in hardware. */
        if (SnoopMiGetVlanLocalEgressPorts (u4Instance, pSnoopVlanEntry->VlanId,
                                            pEgressPortBitmap) == SNOOP_SUCCESS)
        {

            SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort,
                                      pEgressPortBitmap);

            /* query message should not be forwarded on blocked router-ports */
            pSnoopVlanCfgEntryNode = pSnoopVlanEntry->pSnoopVlanCfgEntry;
            if ((pSnoopVlanCfgEntryNode != NULL)
                &&
                (SNOOP_MEM_CMP
                 (pSnoopVlanCfgEntryNode->CfgBlockedRtrPortBmp, gNullPortBitMap,
                  SNOOP_PORT_LIST_SIZE) != 0))
            {
                pBlkPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pBlkPortBitmap == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_DBG,
                               "Error in allocating memory for pBlkPortBitmap \r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "in allocating memory for pBlkPortBitmap \r\n");
                    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                    UtilPlstReleaseLocalPortList (pEgressPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessQueryMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pBlkPortBitmap, 0, sizeof (tSnoopPortBmp));
                SNOOP_MEM_CPY (pBlkPortBitmap,
                               pSnoopVlanCfgEntryNode->CfgBlockedRtrPortBmp,
                               SNOOP_PORT_LIST_SIZE);
                for (u4ByteIndex = 0; u4ByteIndex < SNOOP_PORT_LIST_SIZE;
                     u4ByteIndex++)
                {
                    pBlkPortBitmap[u4ByteIndex] =
                        (UINT1) ~pBlkPortBitmap[u4ByteIndex];
                    pEgressPortBitmap[u4ByteIndex] &=
                        pBlkPortBitmap[u4ByteIndex];
                }
                UtilPlstReleaseLocalPortList (pBlkPortBitmap);
            }

            if (pSnoopPktInfo->u1QueryType == SNOOP_GENERAL_QUERY)
            {
                u4StatType = SNOOP_GEN_QUERY_FWD;
            }
            else if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_QUERY)
            {
                u4StatType = SNOOP_GRP_QUERY_FWD;
            }
            SnoopVlanForwardQuery (u4Instance, VlanId,
                                   pSnoopVlanEntry->u1AddressType,
                                   pSnoopPktInfo->u4InPort,
                                   VLAN_FORWARD_SPECIFIC,
                                   pBuf, pEgressPortBitmap, u4StatType);
        }
        UtilPlstReleaseLocalPortList (pEgressPortBitmap);
    }
    else
#endif
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == CRU_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_DBG,
                       "CRU buffer release failed in packet processing\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString[SYS_LOG_CRU_BUFF_REL_FAIL],
                                   "in packet processing\r\n");
        }
    }
    /* General query message received */
    if (pSnoopPktInfo->u1QueryType == SNOOP_GENERAL_QUERY)
    {
        if (SnoopProcessGeneralQuery (u4Instance, VlanId, pSnoopVlanEntry,
                                      pSnoopPktInfo, u1Version)
            != SNOOP_SUCCESS)
        {
            SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_QRY, SNOOP_QRY_DBG,
                       "Unable to process general query message\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessQueryMessageHandler\r\n");
            return SNOOP_SUCCESS;
        }
    }

    /* Group specific query message received */
    if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_QUERY)
    {
        if (SnoopProcessGroupQuery (u4Instance, VlanId, pSnoopVlanEntry,
                                    pSnoopPktInfo, u1Version) != SNOOP_SUCCESS)
        {
            SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_QRY, SNOOP_QRY_DBG,
                       "Unable to process group specific query message\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessQueryMessageHandler\r\n");
            return SNOOP_SUCCESS;
        }
    }

    /* Group and source query message received */
    if (pSnoopPktInfo->u1QueryType == SNOOP_GRP_SRC_QUERY)
    {
        if (SnoopProcessGrpAndSrcQry (u4Instance, VlanId, pSnoopVlanEntry,
                                      pSnoopPktInfo, u2SrcCount,
                                      (tIPvXAddr *) pSourceAddr) !=
            SNOOP_SUCCESS)
        {
            SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_QRY, SNOOP_QRY_DBG,
                       "Unable to process group and source specific query "
                       "message\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessQueryMessageHandler\r\n");
            return SNOOP_SUCCESS;
        }
    }

    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);

#ifndef MLDS_WANTED
    UNUSED_PARAM (u2HdrLen);
#endif

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessQueryMessageHandler\r\n");
    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopProcessGeneralQuery                             */
/*                                                                           */
/* Description        : This function processes the incoming general query   */
/*                      packets                                              */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopPktInfo - Snoop packet info                    */
/*                      u1Version     - Received query version for as a      */
/*                                      reply to which report is sent.       */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessGeneralQuery (UINT4 u4Instance, tSnoopTag VlanId,
                          tSnoopVlanEntry * pSnoopVlanEntry,
                          tSnoopPktInfo * pSnoopPktInfo, UINT1 u1Version)
{
    UINT1              *pPortBitmap = NULL;
    UINT1              *pTempPortBitMap = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTmpGroupEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tIPvXAddr           NullSrcIpAddr;
    tIPvXAddr           DestIpAddr;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    INT4                i4Result = 0;
    UINT4               u4MaxLength = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    UINT2               u2ConsGrpCnt = 0;
    UINT2               u2SkippedGrpCnt = 0;
    UINT1               u1Forward = SNOOP_FALSE;
    UINT1               u1RecordType = 0;
    UINT1               u1ConvergenceQuery = SNOOP_FALSE;
    UINT4               u4PhyPort = 0;
    UINT1               u1IcclInterface = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
    SNOOP_MEM_SET (&NullSrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&DestIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessGeneralQuery\r\n");
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        u1IcclInterface = (UINT1) SnoopIcchIsIcclInterface (u4PhyPort);
    }

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_FAILURE;
    }
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                    pSnoopRtrPortEntry, tSnoopRtrPortEntry *)
    {
        if (pSnoopRtrPortEntry->u4Port == pSnoopPktInfo->u4InPort)
        {
            break;
        }
    }

    if (pSnoopRtrPortEntry != NULL)
    {
        u1Version = pSnoopRtrPortEntry->u1OperVer;
    }

    /* No Group entry present */
    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_SUCCESS;
    }

    /* Update the First Query response
     * flag for the existing group records */
    SnoopUtilUpdateFirstQueryRespStatus (u4Instance);

#ifdef ICCH_WANTED
    if (u1IcclInterface == OSIX_TRUE)
    {
        i4Result =
            SnoopSendConsolidatedReportOnIccl (pSnoopPktInfo->u4InPort,
                                               u4Instance, VlanId);
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return i4Result;
    }
#else
    UNUSED_PARAM (u1IcclInterface);
#endif

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry);

    if (pRBElem == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_SUCCESS;
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pPortBitmap while processing general query\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pTempPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitMap == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pTempPortBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pTempPortBitMap while processing general query\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

    while (pRBElem != NULL)
    {
        u2ConsGrpCnt++;
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                           pRBElem, NULL);

        pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;

        if ((SnoopIsVlanPresentInFwdTbl
             (SNOOP_OUTER_VLAN (pSnoopConsGroupEntry->VlanId),
              SNOOP_OUTER_VLAN (VlanId), &L2PvlanMappingInfo) == SNOOP_FALSE))
        {
            pRBElem = pRBNextElem;
            continue;
        }

        if (pSnoopConsGroupEntry->GroupIpAddr.u1Afi !=
            pSnoopVlanEntry->u1AddressType)
        {

            pRBElem = pRBNextElem;
            continue;
        }

        if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE) &&
            (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID))
        {
            IPVX_ADDR_COPY (&DestIpAddr, &(pSnoopConsGroupEntry->GroupIpAddr));

            if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopTmpGroupEntry) != SNOOP_SUCCESS)
            {
                pRBElem = pRBNextElem;
                continue;
            }
        }

        if ((SNOOP_INSTANCE_INFO (u4Instance)->
             SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
             SNOOP_FORWARD_ALL_PORTS) ||
            (SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) == SNOOP_ALL_REPORT_CONFIG))
        {
            /* If the group entry in the consolidation report is learnt via the incoming port
             * alone, then that group is learnt from the same router where the query
             * arrives-in and its not require to share the same group information to that
             * router, hence skip that entry in consolidation report */

            IPVX_ADDR_COPY (&DestIpAddr, &(pSnoopConsGroupEntry->GroupIpAddr));

            if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopTmpGroupEntry) == SNOOP_SUCCESS)
            {
                SNOOP_MEM_CPY (pTempPortBitMap,
                               pSnoopTmpGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort,
                                          pTempPortBitMap);

                if (SNOOP_MEM_CMP (pTempPortBitMap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    u2SkippedGrpCnt++;
                    pRBElem = pRBNextElem;
                    continue;
                }
            }
        }

        if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                                  pSnoopVlanEntry->
                                                  u1AddressType - 1)
            == SNOOP_FALSE)
        {
            /* Stop the report forward timer for the all the groups present 
             * in this consolidated group as we are sending
             * query to the hosts and we would be receiving reply from the
             * hosts if proxy reporting is disabled 
             */
            SNOOP_UTL_DLL_OFFSET_SCAN (&
                                       (pSnoopConsGroupEntry->
                                        GroupEntryList), pSnoopGroupEntry,
                                       pSnoopTempGroupEntry, tSnoopGroupEntry *)
            {
                if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType !=
                    SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
                }
                pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
            }
            u1Forward = SNOOP_TRUE;
        }
        else
        {
            if ((u1Version == SNOOP_IGS_IGMP_VERSION3 &&
                 pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4) ||
                (u1Version == SNOOP_MLD_VERSION2 &&
                 pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6))
            {
                /* if the forwarding is done based on MAC then construct the 
                 * SSM Reports for all the Group records with NULL source list
                 * Use the misc preallocated buffer and copy the group 
                 * information to the buffer when the max length reaches just 
                 * send the buffer out */

                u1RecordType =
                    ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);

                for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                     u2Index++)
                {
                    if (u1RecordType == SNOOP_IS_INCLUDE)
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopConsGroupEntry->InclSrcBmp,
                             u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            u2NoSources++;
                        }
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopConsGroupEntry->ExclSrcBmp,
                             u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            u2NoSources++;
                        }
                    }
                }
                u2GrpRec++;

#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    IgsUtilFillV3SourceInfo (u4Instance,
                                             pSnoopConsGroupEntry,
                                             u1RecordType, u2NoSources,
                                             &u4MaxLength);
                    if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                    {
                        i4Result =
                            IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                                  u4MaxLength, &pOutBuf);
                    }
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    MldsUtilFillV2SourceInfo (u4Instance,
                                              pSnoopConsGroupEntry,
                                              u2NoSources, u1RecordType,
                                              &u4MaxLength);
                    if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                    {
                        i4Result =
                            MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                                   u4MaxLength, &pOutBuf);
                    }
                }
#endif
                u2NoSources = 0;
                u1Forward = SNOOP_FALSE;

                if (((pSnoopVlanEntry->u1AddressType ==
                      SNOOP_ADDR_TYPE_IPV4)
                     && (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                    ||
                    ((pSnoopVlanEntry->u1AddressType ==
                      SNOOP_ADDR_TYPE_IPV6)
                     && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                {
                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_PKT_TRC,
                                   "Unable to generate and send SSM report on "
                                   "to router ports\r\n");
                        UtilPlstReleaseLocalPortList (pPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitMap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessGeneralQuery\r\n");
                        return SNOOP_FAILURE;
                    }

                    /* Send the report message to the port on which the 
                     * general query message received */
                    {
                        SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort,
                                                pPortBitmap);
                    }
                    SnoopVlanForwardPacket (u4Instance,
                                            VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            SNOOP_INVALID_PORT,
                                            VLAN_FORWARD_SPECIFIC, pOutBuf,
                                            pPortBitmap, SNOOP_SSMREPORT_SENT);

                    u2GrpRec = 0;
                    u1Forward = SNOOP_TRUE;
                    u4MaxLength = 0;
                    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                   SNOOP_MISC_MEMBLK_SIZE);
                }
            }
            else
            {
                /* If proxy reporting is enabled construct report based on
                 * version configured for this VLAN for this group an 
                 * send to the port on which the query was received 
                 */

                /* If we receive a query with source IP 0.0.0.0, the query 
                 * needs to be replied with v1/v2 reports based on the 
                 * incoming query version. Query with zero IP will be sent 
                 * for faster convergance */

                if ((pSnoopPktInfo->u4InPort != SNOOP_ZERO) &&
                    (!SNOOP_MEM_CMP (pSnoopPktInfo->SrcIpAddr.au1Addr,
                                     &NullSrcIpAddr.au1Addr,
                                     MEM_MAX_BYTES (pSnoopPktInfo->
                                                    SrcIpAddr.u1AddrLen,
                                                    IPVX_MAX_INET_ADDR_LEN))))
                {
                    /* Encode query response based on the incoming version */

                    SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort,
                                            pPortBitmap);
                    u1ConvergenceQuery = SNOOP_TRUE;
                }
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (u1ConvergenceQuery == SNOOP_TRUE)
                    {
                        /* Send the response on a single router port */
                        i4Result = IgsEncodeQueryRespMsg (u4Instance,
                                                          pSnoopVlanEntry,
                                                          pSnoopConsGroupEntry,
                                                          u1Version,
                                                          pPortBitmap,
                                                          u1RecordType);

                    }
                    else
                    {
                        i4Result =
                            IgsEncodeQueryResponse (u4Instance,
                                                    pSnoopVlanEntry,
                                                    pSnoopConsGroupEntry, 0);
                    }
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    if (u1ConvergenceQuery == SNOOP_TRUE)
                    {
                        i4Result = MldsEncodeQueryRespMsg (u4Instance,
                                                           pSnoopVlanEntry,
                                                           pSnoopConsGroupEntry,
                                                           u1Version,
                                                           pPortBitmap,
                                                           u1RecordType);
                    }
                    else
                    {
                        i4Result =
                            MldsEncodeQueryResponse (u4Instance,
                                                     pSnoopVlanEntry,
                                                     pSnoopConsGroupEntry,
                                                     pSnoopPktInfo->u4InPort);
                    }
                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_ALL_FAILURE,
                               SNOOP_QRY_DBG,
                               "Unable to send response to general query\r\n");
                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    UtilPlstReleaseLocalPortList (pTempPortBitMap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessGeneralQuery\r\n");
                    return SNOOP_FAILURE;
                }
            }
        }
        pRBElem = pRBNextElem;
    }
    UtilPlstReleaseLocalPortList (pTempPortBitMap);

    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    if (((u1Version == SNOOP_IGS_IGMP_VERSION1 ||
          u1Version == SNOOP_IGS_IGMP_VERSION2) &&
         pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4) ||
        (u1Version == SNOOP_MLD_VERSION1 &&
         pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6))

    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_SUCCESS;
    }

    /* If proxy is disabled or if the aggregated V2 reports have been 
     * sent to all groups return  */
    if (u1Forward == SNOOP_TRUE)
    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_SUCCESS;
    }

    /* If the consolidation group count is equal to the skipped group 
     * count, then no need to send consolidation report with zero
     * report messages */
    if (u2ConsGrpCnt == u2SkippedGrpCnt)
    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_SUCCESS;
    }

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                         u4MaxLength, &pOutBuf);
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                          u4MaxLength, &pOutBuf);
    }
#endif

    if (i4Result != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_ALL_FAILURE, SNOOP_PKT_TRC,
                   "Unable to generate and send SSM report on to"
                   " router ports\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGeneralQuery\r\n");
        return SNOOP_FAILURE;
    }

    /* Send the report message to the port on which the general query
     * message received */
    {
        SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort, pPortBitmap);
    }

    SnoopVlanForwardPacket (u4Instance, VlanId,
                            pSnoopVlanEntry->u1AddressType, SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, pPortBitmap,
                            SNOOP_SSMREPORT_SENT);

    UtilPlstReleaseLocalPortList (pPortBitmap);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessGeneralQuery\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessGroupQuery                               */
/*                                                                           */
/* Description        : This function processes the incoming Group Specific  */
/*                      query packets                                        */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopPktInfo   - Packet information                 */
/*                      u1Version - Received query version                   */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessGroupQuery (UINT4 u4Instance, tSnoopTag VlanId,
                        tSnoopVlanEntry * pSnoopVlanEntry,
                        tSnoopPktInfo * pSnoopPktInfo, UINT1 u1Version)
{
    UINT1              *pPortBitmap = NULL;
    UINT1              *pTempPortBitMap = NULL;
    tRBElem            *pRBElem = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopConsolidatedGroupEntry SnoopConsGroupEntry;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTmpGroupEntry = NULL;
    tIPvXAddr           NullSrcIpAddr;
    tIPvXAddr           DestIpAddr;
    INT4                i4Result = 0;
    UINT1               u1RecordType = 0;
    UINT1               u1IcclInterface = OSIX_FALSE;
#ifdef ICCH_WANTED
    UINT4               u4PhyPort = 0;
#endif

    SNOOP_MEM_SET (&NullSrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SnoopConsGroupEntry, 0,
                   sizeof (tSnoopConsolidatedGroupEntry));
    SNOOP_MEM_SET (&DestIpAddr, 0, sizeof (tIPvXAddr));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    SNOOP_OUTER_VLAN (SnoopConsGroupEntry.VlanId) = SNOOP_OUTER_VLAN (VlanId);

    IPVX_ADDR_COPY (&SnoopConsGroupEntry.GroupIpAddr,
                    &pSnoopPktInfo->GroupAddr);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessGroupQuery\r\n");

#ifdef ICCH_WANTED
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        u1IcclInterface = (UINT1) SnoopIcchIsIcclInterface (u4PhyPort);
    }
#endif

    /* No Consolidated Group entry present */
    if (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                         (tRBElem *) & SnoopConsGroupEntry);

    /* Do "return" if there is no consolidated group entry present for the 
     * given group address */
    if (pRBElem == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return SNOOP_SUCCESS;
    }

    pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;

#ifdef ICCH_WANTED
    if (u1IcclInterface == OSIX_TRUE)
    {
        i4Result =
            SnoopSendConsolidatedGroupReportOnIccl (u4Instance, pSnoopVlanEntry,
                                                    pSnoopConsGroupEntry,
                                                    pSnoopPktInfo->u4InPort,
                                                    VlanId);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return i4Result;
    }
#else
    UNUSED_PARAM (u1IcclInterface);
#endif

    if (SNOOP_INSTANCE_INFO (u4Instance)->
        SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
        SNOOP_FORWARD_ALL_PORTS)
    {
        /* If the group entry in the consolidation report is learnt via the incoming port
         * alone, then that group is learnt from the same router where the query
         * arrives-in and its not require to share the same group information to that
         * router, hence skip that entry in consolidation report */
        IPVX_ADDR_COPY (&DestIpAddr, &(pSnoopConsGroupEntry->GroupIpAddr));

        if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                   &pSnoopTmpGroupEntry) == SNOOP_SUCCESS)
        {
            pTempPortBitMap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pTempPortBitMap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_DBG,
                           "Error in allocating memory for pTempPortBitMap\r\n");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "allocating memory for pTempPortBitMap while processing group query\r\n");
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessGroupQuery\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

            SNOOP_MEM_CPY (pTempPortBitMap, pSnoopTmpGroupEntry->PortBitmap,
                           sizeof (tSnoopPortBmp));

            SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort, pTempPortBitMap);

            if (SNOOP_MEM_CMP (pTempPortBitMap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                UtilPlstReleaseLocalPortList (pTempPortBitMap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessGroupQuery\r\n");
                return SNOOP_SUCCESS;
            }
            UtilPlstReleaseLocalPortList (pTempPortBitMap);
        }
    }

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              (pSnoopVlanEntry->u1AddressType -
                                               1)) == SNOOP_FALSE)
    {
        /* Stop the report forward timer for the all the groups present 
         * in this consolidated group as we are sending query to the 
         * hosts and we would be receiving reply from the hosts if 
         * proxy reporting is disabled 
         */
        SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                                   pSnoopGroupEntry, pSnoopTempGroupEntry,
                                   tSnoopGroupEntry *)
        {
            if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
            }
            pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return SNOOP_SUCCESS;
    }

    /* If incoming query is with source IP 0.0.0.0, it needs to be replied 
     * with the incoming query version. Queries with source IP 0.0.0.0 will 
     * be send out for faster convergence purpose. So it needs to be replied
     * with the incoming query's version irrespective of if the received port
     * is a router port or not. 
     */
    if ((!SNOOP_MEM_CMP (pSnoopPktInfo->SrcIpAddr.au1Addr,
                         &NullSrcIpAddr.au1Addr,
                         MEM_MAX_BYTES (pSnoopPktInfo->SrcIpAddr.u1AddrLen,
                                        IPVX_MAX_INET_ADDR_LEN))))
    {
        if (u1Version == SNOOP_IGS_IGMP_VERSION3)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_IS_EXCLUDE;
            }
            else
            {
                u1RecordType =
                    ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);
            }
        }
        pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pPortBitmap == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_DBG,
                       "Error in allocating memory for pPortBitmap\r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "allocating memory for pPortBitmap while processing group query\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessGroupQuery\r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

        SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort, pPortBitmap);
        /* Send the response on the port over which query was received */
#ifdef IGS_WANTED
        if (IgsEncodeQueryRespMsg (u4Instance, pSnoopVlanEntry,
                                   pSnoopConsGroupEntry, u1Version, pPortBitmap,
                                   u1RecordType) != SNOOP_SUCCESS)
        {
            UtilPlstReleaseLocalPortList (pPortBitmap);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessGroupQuery\r\n");
            return SNOOP_FAILURE;
        }
#endif
        UtilPlstReleaseLocalPortList (pPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return SNOOP_SUCCESS;
    }

    /* If proxy / proxy reporting is enabled, construct report based on
     * the router port oper version and send the report over the port on which 
     * the query was received 
     */
#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        i4Result = IgsEncodeQueryResponse (u4Instance, pSnoopVlanEntry,
                                           pSnoopConsGroupEntry,
                                           pSnoopPktInfo->u4InPort);
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result = MldsEncodeQueryResponse (u4Instance, pSnoopVlanEntry,
                                            pSnoopConsGroupEntry,
                                            pSnoopPktInfo->u4InPort);
    }
#endif

    if (i4Result != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_ALL_FAILURE,
                   SNOOP_QRY_DBG, "Unable to send response to group "
                   "query\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessGroupQuery\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessGroupQuery\r\n");
    return (SNOOP_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : SnoopProcessGrpAndSrcQry                             */
/*                                                                           */
/* Description        : This function processes the incoming source and      */
/*                      group specific query message                         */
/*                                                                           */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopVlanEntry - VLAN information                   */
/*                      pSnoopPktInfo   - Packet information                 */
/*                      u2SrcCount    - Number of Sources                    */
/*                      *pSources   - Source list                            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessGrpAndSrcQry (UINT4 u4Instance, tSnoopTag VlanId,
                          tSnoopVlanEntry * pSnoopVlanEntry,
                          tSnoopPktInfo * pSnoopPktInfo, UINT2 u2SrcCount,
                          tIPvXAddr * pSources)
{
    UINT1              *pPortBitmap = NULL;
    tRBElem            *pRBElem = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopConsolidatedGroupEntry SnoopConsGroupEntry;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tIPvXAddrSource    *pSourceAddr = NULL;
    UINT4               u4MaxLength = 0;
    UINT4               u4HdrLength = 0;
    INT4                i4Result = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NumSrcs = 0;
    UINT2               u2Count = 0;
    UINT2               u2SrcIndex = 0;
    UINT2               u2IpAddrLen = 0;
    UINT1               u1Forward = SNOOP_FALSE;
    UINT1               u1RecordType = 0;
    UINT1               u1AuxDataLen = 0;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NullGroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = SNOOP_FALSE;
    BOOL1               bSrcSet = OSIX_FALSE;

    if (SNOOP_IPVX_ADDR_ALLOC_MEMBLK (pSourceAddr) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG, "Memory allocation for IPvX entry \r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while processing group and src queries\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pSourceAddr, 0,
                   sizeof (tIPvXAddrSource) * SNOOP_MAX_MCAST_SRCS);
    SNOOP_MEM_SET (&SnoopConsGroupEntry, 0,
                   sizeof (tSnoopConsolidatedGroupEntry));
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NullGroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_OUTER_VLAN (SnoopConsGroupEntry.VlanId) = SNOOP_OUTER_VLAN (VlanId);
    IPVX_ADDR_COPY (&SnoopConsGroupEntry.GroupIpAddr,
                    &pSnoopPktInfo->GroupAddr);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        return SNOOP_FAILURE;
    }

    if (((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV4) &&
        ((pSnoopVlanEntry->u1AddressType) != SNOOP_ADDR_TYPE_IPV6))
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Not a Valid Address Type\r\n");
        return SNOOP_FAILURE;
    }

    /* No Group entry present */
    if (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry == NULL)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        return SNOOP_SUCCESS;
    }

    /* Get the snoop group entry from the RB Tree */

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                         (tRBElem *) & SnoopConsGroupEntry);

    /* Do "return" if there is no group entry present for the given Group
     * address */
    if (pRBElem == NULL)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        return SNOOP_SUCCESS;
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "in allocating memory for pPortBitmap while processing group and src qry\r\n");
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;
    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              (pSnoopVlanEntry->u1AddressType -
                                               1)) == SNOOP_FALSE)
    {
        /* Stop the report forward timer for the all the groups present 
         * in this consolidated group as we are sending query to the 
         * hosts and we would be receiving reply from the hosts if 
         * proxy reporting is disabled 
         */
        SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                                   pSnoopGroupEntry, pSnoopTempGroupEntry,
                                   tSnoopGroupEntry *)
        {
            if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
            }
            pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
        }
        u1Forward = SNOOP_TRUE;
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_GRP_SRC_QUERY_SENT) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_PKT_TRC,
                            "Unable to update statistics " "for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
    }
    else
    {
        /* If proxy reporting is enabled construct report based on
         * version configured for this VLAN for this group an send to the
         * port on which the query was received 
         */

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                i4Result = IgsEncodeQueryResponse (u4Instance, pSnoopVlanEntry,
                                                   pSnoopConsGroupEntry,
                                                   pSnoopPktInfo->u4InPort);
            }
#endif

#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                i4Result = MldsEncodeQueryResponse (u4Instance, pSnoopVlanEntry,
                                                    pSnoopConsGroupEntry,
                                                    pSnoopPktInfo->u4InPort);
            }
#endif

            if (i4Result != SNOOP_SUCCESS)
            {
                SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_QRY_DBG, "Unable to send response to group "
                           "and source specific query\r\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }
            u1Forward = SNOOP_TRUE;
        }
        else
        {
            u2IpAddrLen = (pSnoopVlanEntry->u1AddressType ==
                           SNOOP_ADDR_TYPE_IPV4) ? SNOOP_IP_ADDR_SIZE :
                IPVX_MAX_INET_ADDR_LEN;

            u2NumSrcs = 0;
            for (u2Count = 0; ((u2NumSrcs < SNOOP_MAX_MCAST_SRCS) &&
                               (u2Count < u2SrcCount)); u2Count++)
            {
                SNOOP_IS_SOURCE_PRESENT (u4Instance, pSources[u2Count],
                                         bResult, u2SrcIndex);

                if (bResult == SNOOP_FALSE)
                {
                    if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                    {
                        continue;
                    }
                }
                else
                {
                    u2SrcIndex++;

                    if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopConsGroupEntry->InclSrcBmp,
                             u2SrcIndex, SNOOP_SRC_LIST_SIZE, bSrcSet);

                        if (bSrcSet == OSIX_FALSE)
                        {
                            continue;
                        }
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopConsGroupEntry->ExclSrcBmp,
                             u2SrcIndex, SNOOP_SRC_LIST_SIZE, bSrcSet);

                        if (bSrcSet == OSIX_TRUE)
                        {
                            continue;
                        }
                    }
                }

                SNOOP_MEM_CPY ((pSourceAddr + u2NumSrcs)->au1Addr,
                               pSources[u2Count].au1Addr, u2IpAddrLen);
                SNOOP_INET_HTONL ((pSourceAddr + u2NumSrcs)->au1Addr);
                u2NumSrcs++;
            }

            if (u2NumSrcs == 0)
            {
                if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                {
                    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    return SNOOP_SUCCESS;
                }
                else
                {
                    u1RecordType = SNOOP_IS_EXCLUDE;
                    u2NumSrcs = u2SrcCount;
                    for (u2Count = 0; u2Count < u2SrcCount; u2Count++)
                    {
                        SNOOP_MEM_CPY ((pSourceAddr + u2Count)->au1Addr,
                                       pSources[u2Count].au1Addr, u2IpAddrLen);
                        SNOOP_INET_HTONL ((pSourceAddr + u2Count)->au1Addr);
                    }
                }
            }
            else
            {
                u1RecordType = SNOOP_IS_INCLUDE;
            }

            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

            u1AuxDataLen = 0;
            u2SrcCount = SNOOP_NTOHS (u2NumSrcs);
            SNOOP_MEM_CPY (au1GroupAddr,
                           pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                           u2IpAddrLen);
            SNOOP_INET_HTONL (au1GroupAddr);
            u1Forward = SNOOP_FALSE;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u1RecordType, SNOOP_OFFSET_ONE);
            u4MaxLength += SNOOP_OFFSET_ONE;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u1AuxDataLen, SNOOP_OFFSET_ONE);
            u4MaxLength += SNOOP_OFFSET_ONE;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           &u2SrcCount, SNOOP_OFFSET_TWO);
            u4MaxLength += SNOOP_OFFSET_TWO;

            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                           au1GroupAddr, u2IpAddrLen);
            u4MaxLength += u2IpAddrLen;

            u4HdrLength = u4MaxLength;

            for (u2Count = 0; ((u2Count < SNOOP_MAX_MCAST_SRCS) &&
                               (u2Count < u2NumSrcs)); u2Count++)
            {
                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               (pSourceAddr + u2Count)->au1Addr, u2IpAddrLen);
                u4MaxLength += u2IpAddrLen;

                u2GrpRec = 1;

                if (((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4) &&
                     (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                    || ((pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                        && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                {
#ifdef IGS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                    {
                        i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                                         u4MaxLength, &pOutBuf);
                    }
#endif

#ifdef MLDS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    {
                        i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                                          u4MaxLength,
                                                          &pOutBuf);
                    }
#endif

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                                   "Unable to generate and send SSM report on "
                                   "to router ports\r\n");
                        UtilPlstReleaseLocalPortList (pPortBitmap);
                        return SNOOP_FAILURE;
                    }

                    /* Send the report message to the port on which the 
                     * general query message received */
                    SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort,
                                            pPortBitmap);

                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            SNOOP_INVALID_PORT,
                                            VLAN_FORWARD_SPECIFIC, pOutBuf,
                                            pPortBitmap, SNOOP_SSMREPORT_SENT);
                    u2GrpRec = 0;
                    u1Forward = SNOOP_TRUE;
                    u4MaxLength = u4HdrLength;
                    SNOOP_MEM_SET ((gpSSMRepSendBuffer + u4HdrLength), 0,
                                   (SNOOP_MISC_MEMBLK_SIZE - u4HdrLength));
                }
            }
        }
    }

    /* If proxy is disabled or if the aggregated V2 reports have been sent to 
     * all groups or if the misc buffer is not allocated just return suceess */
    if (u1Forward == SNOOP_TRUE)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_SUCCESS;
    }

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                         u4MaxLength, &pOutBuf);
    }
#endif

#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                          u4MaxLength, &pOutBuf);
    }
#endif

    if (i4Result != SNOOP_SUCCESS)
    {
        SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Unable to generate and send SSM report on to the"
                   " router ports\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }

    /* Send the report message to the port on which the general query
     * message received */
    SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort, pPortBitmap);

    SnoopVlanForwardPacket (u4Instance, VlanId,
                            pSnoopVlanEntry->u1AddressType,
                            SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf,
                            pPortBitmap, SNOOP_SSMREPORT_SENT);

    SNOOP_IPVX_ADDR_FREE_MEMBLK (pSourceAddr);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessReportMessageHandler                     */
/*                                                                           */
/* Description        : This function processes the incoming IGMP V1/V2      */
/*                      or MLD V1 report message                             */
/*                                                                           */
/* Input(s)           : u4Instance         - Instance number                 */
/*                      pSnoopVlanEntry    - pointer to the VLAN Entry       */
/*                      pSnoopPktInfo      - Packet information              */
/*                      pSnoopPortCfgEntry - pointer to port config          */
/*                                           entry                           */
/*                      PortBitmap         - Port Bitmap                     */
/*                      pBuf               - Buffer Pointer                  */
/*                                                                           */
/* Output(s)          : pu1Forward - VLAN_FORWARD_SPECIFIC/VLAN_FORWARD_ALL  */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessReportMessageHandler (UINT4 u4Instance, tSnoopTag VlanId,
                                  tSnoopVlanEntry * pSnoopVlanEntry,
                                  tSnoopPktInfo * pSnoopPktInfo,
                                  tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                                  UINT1 *pu1Forward, tSnoopPortBmp PortBitmap,
                                  tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pVlanPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pTmpPortBitMap = NULL;
#ifndef IGS_OPTIMIZE
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
#endif
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pV3ReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pV2ReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pV1ReportBuf = NULL;
    tSnoopPortBmp       TempPortBitmap;
    tIPvXAddr           DestIpAddr;
    tIPvXAddr           SrcAddr;
    tSnoopMacGrpFwdEntry MacGrpFwdEntry;
    tRBElem            *pRBElem = NULL;
    tSnoopMacAddr       GrpMacAddr;
    INT4                i4Result = 0;
    UINT1               u1Forward = VLAN_FORWARD_SPECIFIC;
    UINT1               u1PktType = 0;
    BOOL1               bIsFirstAsmReportForOuterVlan = OSIX_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bOperEdge = OSIX_TRUE;
    UINT4               u4ProfileId = 0;
    INT4                i4FilterAction = TACM_PERMIT;
    UINT1               u1Version = 0;
    UINT1               u1V1ReportFwd = SNOOP_FALSE;
    UINT1               u1V2ReportFwd = SNOOP_FALSE;
    UINT1               u1V3ReportFwd = SNOOP_FALSE;
    UINT1               u1OprVersion = 0;
    UINT2               u2NumSrcs = 0;
    UINT2               u2HdrLen = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT4               u4LearntPort = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
    UINT4               u4NonEdgePort = SNOOP_INIT_VAL;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1PortFlag = 0;
    UINT1               u1FwdTableUpdate = SNOOP_TRUE;
    UINT1               u1ReportModify = OSIX_FALSE;
    UINT1               u1CounterUpdate = SNOOP_TRUE;
    UINT1               u1IsSwitchOverInProgress = SNOOP_FALSE;
    UINT1               u1GrpFirstCreated = SNOOP_FALSE;
#ifdef ICCH_WANTED
    UINT4               u4PhyPort = 0;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    tCRU_BUF_CHAIN_HEADER *pV3ReportBufOnIccl = NULL;
    UINT1              *pIcclPortBitMap = NULL;
    UINT4               u4IcchPort = 0;
    UINT4               u4InstanceId = 0;
    INT4                i4IcclResult = SNOOP_FAILURE;
    UINT2               u2LocalPort = 0;
    UINT1               u1FirstNonMcLagInterface = SNOOP_FALSE;
    UINT1               u1MclagIpHostOptimize = SNOOP_FALSE;
#endif

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessReportMessageHandler\r\n");
    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));
    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (TempPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));

    if (pSnoopPktInfo == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG,
                   SNOOP_DBG_GRP | SNOOP_DBG_PKT | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_PKT_TRC,
                   "Report message processing failed since Packet"
                   " information is not present \n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessReportMessageHandler\r\n");
        return SNOOP_FAILURE;
    }

#ifdef ICCH_WANTED
    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopIcchInfo.pSnoopPktInfo = pSnoopPktInfo;
#endif

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u2HdrLen =
            (UINT2) (pSnoopPktInfo->u2PktLen - pSnoopPktInfo->u1IpHdrLen);
        if (u2HdrLen == SNOOP_IGMP_HEADER_LEN)
        {
            if (pSnoopPktInfo->u2MaxRespCode == 0)
            {
                u1OprVersion = SNOOP_IGS_IGMP_VERSION1;
            }
            else
            {
                u1OprVersion = SNOOP_IGS_IGMP_VERSION2;
            }
        }
        else
        {
            u1OprVersion = SNOOP_IGS_IGMP_VERSION3;
        }
    }

#ifdef ICCH_WANTED
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        SnoopLaIsMclagInterface (u4PhyPort, &u1ReportModify);
    }
#endif

    /* If report received on non-mclag interface,
       and enhanced mode is enabled we need to modify
       the report to include inner-vlan also
     */

    if (u1ReportModify == OSIX_FALSE)
    {
        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
        {
            u1ReportModify = OSIX_TRUE;
        }
    }

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessReportMessageHandler\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    /* Do not process the report message if the port is a router port  */
    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN (VlanId),
                                             pSnoopVlanEntry->u1AddressType,
                                             SNOOP_IGS_IGMP_INVALID_VERSION,
                                             pSnoopRtrPortBmp, pSnoopVlanEntry);
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort, pSnoopRtrPortBmp, bResult);

    if (bResult == OSIX_TRUE)
    {
        /* Report processing on all ports is allowed when the report-process
         * config level is set to all ports */

        if (SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) != SNOOP_ALL_REPORT_CONFIG)
        {
            if (SNOOP_PROXY_STATUS (u4Instance,
                                    pSnoopVlanEntry->u1AddressType - 1)
                == SNOOP_ENABLE)
            {
                /* In Proxy mode reports received on router ports
                 * will be dropped, so release the report msg buffer 
                 */
                *pu1Forward = SNOOP_RELEASE_BUFFER;
            }
            else
            {
                *pu1Forward = VLAN_FORWARD_SPECIFIC;
                SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, PortBitmap);
            }

            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_PKT_TRC,
                       "Report message processing failed since incoming port"
                       " is a router port\n");
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_V1REPORT_DROPPED) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
            }
            else if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V2REPORT)
            {
                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          SNOOP_V2REPORT_DROPPED) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessReportMessageHandler\r\n");
            return SNOOP_SUCCESS;
        }
    }

    IPVX_ADDR_COPY (&DestIpAddr, &(pSnoopPktInfo->GroupAddr));

#ifdef PIM_WANTED
    if (SnoopUtilGetPIMInterfaceStatus (VlanId) == SNOOP_SUCCESS)
    {
        if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
            (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
        {
            /* If PIM is already running, in that case if a New Report is received
               Check whether the same (S,G) entry present in PIM.If Present then
               Program the H/W */
            if (SnoopUtilIsSrcAndGrpPresentInPim (u4Instance, VlanId,
                                                  pSnoopPktInfo, SNOOP_ADD_PORT)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Report message programming in H/W is failed,When PIM is Enabled\r\n");
            }
        }
    }
#endif

    if (pSnoopPortCfgEntry != NULL)
    {
        u4ProfileId = pSnoopPortCfgEntry->u4SnoopPortCfgProfileId;
    }

    if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
    {
        u1Version = TACM_IGMP_VERSION1;
    }
    else
    {
        u1Version = TACM_IGMP_VERSION2;
    }

    if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
        (SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo
         [pSnoopVlanEntry->u1AddressType - 1].u1FilterStatus
         == SNOOP_ENABLE) && (u4ProfileId != SNOOP_PORT_CFG_PROFILE_ID_DEF))
    {
        /* For V1/V2 report, number of sources and filter mode is 0 */
        i4FilterAction = SnoopTacApiFilterChannel
            (&(pSnoopPktInfo->GroupAddr), u4ProfileId,
             0, u1Version, TACM_IGMP_REPORT, 0);

        if (i4FilterAction == TACM_DENY)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GRP_TRC, "IGMP v1 or V2 report is filtered\r\n");
            /*  The group address has been filtered. The report
             *  should not be sent upstream.
             *  The report is discarded. Set the pu1Forward flag to
             *  SNOOP_RELEASE_BUFFER so that the packet buffer
             *  will be freed outside this function
             *  If snoop failure is returned, then the report will be
             *  forwarded to all VLAN member ports. Since the report
             *  should not be forwarded upstream, success is returned
             */
            *pu1Forward = SNOOP_RELEASE_BUFFER;

            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessReportMessageHandler\r\n");
            return SNOOP_SUCCESS;
        }
    }

    /*  If i4FilterAction is permit, then process the packet and register
     *  to the database.
     */

    /* Check if group entry is present or not */
    if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                               &pSnoopGroupEntry) == SNOOP_SUCCESS)
    {
        /* ASM report changes the mode to EXCLUDE */
        pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
        pSnoopGroupEntry->u1IsUnknownMulticast = SNOOP_FALSE;

        if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                            gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0))
        {
            if ((pSnoopPortCfgEntry != NULL) &&
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL)
                && (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pSnoopPktInfo->GroupAddr.u1Afi -
                              1].u1FilterStatus != SNOOP_DISABLE))
            {
                /* If either group or channel based limit is set, and if the 
                 * maximum limit has already been reached, return success */
                if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u1SnoopPortMaxLimitType != SNOOP_PORT_LMT_TYPE_NONE)
                {
                    /* Check if the maximum limit has already been reached */
                    if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                        u4SnoopPortMaxLimit <=
                        pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                        u4SnoopPortCfgMemCnt)
                    {
                        *pu1Forward = SNOOP_RELEASE_BUFFER;
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_SUCCESS;
                    }
                }
            }

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                /* MAC Based forwarding mode */
                pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

                if (SnoopGrpUpdatePortToGroupEntry
                    (u4Instance, pSnoopGroupEntry, pSnoopPktInfo,
                     pSnoopPortCfgEntry, SNOOP_FALSE) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_TRC,
                               "Updation of port entry to Group failed\r\n");
                    SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                               SNOOP_DELETE_GRP_ENTRY);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                  (tRBElem *) pSnoopGroupEntry);
                    SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi].
                        u4ActiveGroupsCnt--;
                    SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }
                SnoopUpdateConsGrpOnFilterChange (u4Instance, SNOOP_TO_EXCLUDE,
                                                  pSnoopGroupEntry->
                                                  pConsGroupEntry, SNOOP_FALSE);
            }
            else
            {
                SNOOP_SLL_INIT (&pSnoopGroupEntry->SSMPortList);

                SNOOP_MEM_SET (pSnoopGroupEntry->InclSrcBmp, 0,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_MEM_SET (pSnoopGroupEntry->ExclSrcBmp, 0xff,
                               SNOOP_SRC_LIST_SIZE);

                /* IP Forwarding Mode */
                if ((pSnoopPktInfo->u1PktType == SNOOP_IGMP_V3REPORT &&
                     pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                     SNOOP_ADDR_TYPE_IPV4)
                    || (pSnoopPktInfo->u1PktType == SNOOP_MLD_V2REPORT
                        && pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                        SNOOP_ADDR_TYPE_IPV6))
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

                    if (SnoopGrpHandleSSMReports (u4Instance, 0,
                                                  pSnoopGroupEntry, u2NumSrcs,
                                                  pSnoopPktInfo,
                                                  pSnoopPortCfgEntry) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_GRP_TRC,
                                   "Processing of ASM report messages"
                                   " for group record failed\r\n");
                        SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                                   SNOOP_DELETE_GRP_ENTRY);
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      GroupEntry, (tRBElem *) pSnoopGroupEntry);
                        SNOOP_INSTANCE_INFO (u4Instance)->
                            SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi].
                            u4ActiveGroupsCnt--;

                        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                    pSnoopGroupEntry);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;
                    }
                }
                else
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                    SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, gNullSrcBmp,
                                   SNOOP_SRC_LIST_SIZE);

                    if (SnoopGrpUpdatePortToGroupEntry
                        (u4Instance, pSnoopGroupEntry, pSnoopPktInfo,
                         pSnoopPortCfgEntry, SNOOP_FALSE) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_GRP_TRC,
                                   "Updation of port entry to Group failed\r\n");
                        SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                                   SNOOP_DELETE_GRP_ENTRY);
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      GroupEntry, (tRBElem *) pSnoopGroupEntry);
                        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                    pSnoopGroupEntry);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;
                    }
                    SnoopUpdateConsGrpOnFilterChange
                        (u4Instance, SNOOP_TO_EXCLUDE,
                         pSnoopGroupEntry->pConsGroupEntry, SNOOP_TRUE);
                }
            }

            /*Updating the statistics for active Group count */
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_ACTIVE_GRPS) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_PKT_TRC,
                                "Unable to update statistics "
                                "for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                               pSnoopPktInfo->u4InPort,
                                               SNOOP_CREATE_FWD_ENTRY,
                                               0) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Unable to create multicast MAC "
                               "forwarding entry\r\n");
                    /* Delete all the V1/V2 and V3 port records */
                    SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

                    SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                               SNOOP_DELETE_GRP_ENTRY);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                  (tRBElem *) pSnoopGroupEntry);
                    SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi].
                        u4ActiveGroupsCnt--;
                    SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }
            }

        }

        /* Set the exclude bitmap to NULL */
        SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, gNullSrcBmp,
                       SNOOP_SRC_LIST_SIZE);

        /* Check whether the port is already present in the Group Entry's
         * ASM Port Bitmap */
        SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                               pSnoopGroupEntry->ASMPortBitmap, bResult);
#ifdef ICCH_WANTED
        if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) == SNOOP_DISABLE)
        {
            /* Check whether the host is already registered */
            if (SnoopIcchUtilGetHostEntry (u4Instance, pSnoopGroupEntry,
                                           pSnoopPktInfo) == SNOOP_SUCCESS)
            {
                u1MclagIpHostOptimize = SNOOP_TRUE;
            }
        }
#endif

        if (bResult == OSIX_TRUE)
        {
            /* Port Entry already present so just update the timers */
            if (SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                pSnoopGroupEntry,
                                                pSnoopPktInfo,
                                                pSnoopPortCfgEntry, SNOOP_FALSE)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_TRC,
                           "Updation of port entry to Group failed\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessReportMessageHandler\r\n");
                return SNOOP_FAILURE;
            }

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                if ((u1ReportModify == OSIX_TRUE)
                    && (pSnoopGroupEntry->u1McLagLeaveReceived == SNOOP_TRUE))
                {
                    pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_FALSE;
                    bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
                }
            }
#endif

            /* There can be a possiblity of FDB is empty when dynamically learnt port 
             * is added and deleted from the Mrouter table. So update FDB when receiving a Join message.*/

            if ((SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
                 SNOOP_ALL_REPORT_CONFIG))
            {
                /* Update the forwarding Table with the new port entry based on the 
                 * forwarding mode MAC or IP*/
                if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {
                    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof
                                   (tSnoopMacGrpFwdEntry));
                    SNOOP_MEM_CPY (MacGrpFwdEntry.VlanId,
                                   pSnoopGroupEntry->VlanId,
                                   sizeof (tSnoopTag));

                    SNOOP_MEM_CPY (au1GroupAddr, pSnoopGroupEntry->
                                   GroupIpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);

                    MacGrpFwdEntry.u1AddressType =
                        pSnoopGroupEntry->GroupIpAddr.u1Afi;

                    if (pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                        SNOOP_ADDR_TYPE_IPV4)
                    {
                        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                                 (au1GroupAddr), GrpMacAddr);
                    }
                    SNOOP_MEM_CPY (&MacGrpFwdEntry.MacGroupAddr, GrpMacAddr,
                                   SNOOP_MAC_ADDR_LEN);

                    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO
                                         (u4Instance)->MacMcastFwdEntry,
                                         (tRBElem *) & MacGrpFwdEntry);

                    if (pRBElem == NULL)
                    {
                        if (SnoopFwdUpdateMacFwdTable (u4Instance,
                                                       pSnoopGroupEntry,
                                                       pSnoopPktInfo->u4InPort,
                                                       SNOOP_CREATE_FWD_ENTRY,
                                                       0) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Unable to create mcast MAC"
                                       " forwarding entry\r\n");
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopProcessReportMessageHandler\r\n");
                            return SNOOP_FAILURE;
                        }
                    }
                    SnoopRedActiveSendGrpInfoSync (u4Instance,
                                                   pSnoopGroupEntry);

                    if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                                   pSnoopPktInfo->u4InPort,
                                                   SNOOP_ADD_PORT,
                                                   0) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Unable to add the port in mcast MAC"
                                   " forwarding entry\r\n");
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;
                    }
                }
            }

            if ((SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
                 SNOOP_ALL_REPORT_CONFIG) && (pSnoopGroupEntry->
                                              b1InGroupSourceInfo ==
                                              SNOOP_FALSE)
                && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
            {
                pTmpPortBitMap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pTmpPortBitMap == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_TRC,
                               "Error in allocating memory for pTmpPortBitMap\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }
                MEMSET (pTmpPortBitMap, 0, sizeof (tSnoopPortBmp));

                SNOOP_MEM_CPY (pTmpPortBitMap, pSnoopGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort,
                                          pTmpPortBitMap);
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopGroupEntry->
                                                         GroupIpAddr.u1Afi,
                                                         SNOOP_IGS_IGMP_INVALID_VERSION,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);

                for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE;
                     u2BytIndex++)
                {
                    u1PortFlag = pTmpPortBitMap[u2BytIndex];
                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        bResult = OSIX_FALSE;
                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4LearntPort =
                                (UINT4) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);

                            SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                   pSnoopRtrPortBmp, bResult);

                            /* Ensure the port is valid */
                            if (u4LearntPort <= SNOOP_MAX_PORTS_PER_INSTANCE)
                            {
                                /* stores physical ports info for local port */
                                u4NonEdgePort =
                                    SNOOP_GET_IFINDEX (u4Instance,
                                                       (UINT2) u4LearntPort);
                                L2IwfGetPortOperEdgeStatus (u4NonEdgePort,
                                                            &bOperEdge);
                                if ((bResult == OSIX_TRUE)
                                    || (bOperEdge == OSIX_FALSE))
                                {
                                    u4RtrPortCount++;
                                }
                                else
                                {
                                    u4LrtPortCount++;
                                }
                            }

                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
                UtilPlstReleaseLocalPortList (pTmpPortBitMap);

                if ((u4LrtPortCount == 0) && u4RtrPortCount >= 1)
                {
                    bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
                }
                if (pSnoopGroupEntry->u1FirstQueryRes == SNOOP_TRUE)
                {
                    bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
                    pSnoopGroupEntry->u1FirstQueryRes = SNOOP_FALSE;
                }

                if ((pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                     SNOOP_INVALID_TIMER_TYPE) &&
                    (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_FALSE))
                {
                    bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
                }
            }
        }
        else
        {
            /* Check if this is the first ASM port added to this consolidated 
             * group entry. If yes set the bIsFirstAsmReportForOuterVlan to TRUE, 
             * so that the new registration should be sent to router ports */

#ifdef ICCH_WANTED
            /* check if MCLAG is globally enabled */

            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                SnoopCheckBitMapForFirstNonMcLagInterface (pSnoopGroupEntry->
                                                           ASMPortBitmap,
                                                           &u1FirstNonMcLagInterface);
            }
#endif

            if (pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent
                == SNOOP_FALSE)
            {
                bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
            }

#ifdef ICCH_WANTED
            else if (((u1ReportModify == OSIX_TRUE) ||
                      (u1FirstNonMcLagInterface == SNOOP_TRUE)) &&
                     (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
            {
                bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
            }
#endif
            /* Port Entry not present so create a new port record */
            if (SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                pSnoopGroupEntry,
                                                pSnoopPktInfo,
                                                pSnoopPortCfgEntry, SNOOP_FALSE)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_TRC,
                           "Addition of port entry to Group failed\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessReportMessageHandler\r\n");
                return SNOOP_FAILURE;
            }

            /* Update the forwarding Table with the new port entry based on the 
             * forwarding mode MAC or IP*/

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);

                if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                               pSnoopPktInfo->u4InPort,
                                               SNOOP_ADD_PORT,
                                               0) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Unable to add the port in mcast MAC"
                               " forwarding entry\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }
            }
            if (bIsFirstAsmReportForOuterVlan == OSIX_TRUE)
            {
                SnoopUpdateConsGrpOnFilterChange (u4Instance, SNOOP_TO_EXCLUDE,
                                                  pSnoopGroupEntry->
                                                  pConsGroupEntry, SNOOP_FALSE);
            }
        }

        /* In case this is a new host which is requesting same multicast 
           data, a new report to be sent via ICCL with the source IP as 
           the Host Ip address, to learn the host information on remote 
           node */
#ifdef ICCH_WANTED
        if (u1MclagIpHostOptimize == SNOOP_TRUE)
        {
            bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
        }
#endif

    }
    else                        /* Group Entry not found create a new one */
    {
#ifndef IGS_OPTIMIZE
        if (SnoopGrpGetConsGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopConsGroupEntry) != SNOOP_SUCCESS)
        {
            /* Consolidated group entry not present. So set the filter mode 
             * change to TRUE, so that the new registration should be sent 
             * to router ports.*/
            bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
        }
        else
        {
            /* Check if this is the first ASM port added to this consolidated 
             * group entry. If yes set the filter mode change to TRUE, so that 
             * the registration should be sent to router ports  */
            if (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_FALSE)
            {
                bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
            }

#ifdef ICCH_WANTED
            /* If enhanced mode is enabled, and group entry not present
               a new [SVLAN,CVLAN] entry to be created */

            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
            {
                bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
            }
#endif
        }
#else
        /* Consolidated group entry not present. So set the filter mode
         * change to TRUE, so that the new registration should be sent
         * to router ports.*/
        bIsFirstAsmReportForOuterVlan = OSIX_TRUE;
#endif
        if ((pSnoopPktInfo->GroupAddr.u1Afi != SNOOP_ADDR_TYPE_IPV4) &&
            (pSnoopPktInfo->GroupAddr.u1Afi != SNOOP_ADDR_TYPE_IPV6))
        {
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessReportMessageHandler\r\n");
            return SNOOP_FAILURE;
        }

        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL)
            && (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopPktInfo->GroupAddr.u1Afi - 1].u1FilterStatus !=
                SNOOP_DISABLE))
        {
            /* If either group or channel based limit is set, and if the 
             * maximum limit has already been reached, return success */
            if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                u1SnoopPortMaxLimitType != SNOOP_PORT_LMT_TYPE_NONE)
            {
                /* Check if the maximum limit has already been reached */
                if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u4SnoopPortMaxLimit <=
                    pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u4SnoopPortCfgMemCnt)
                {
                    *pu1Forward = SNOOP_RELEASE_BUFFER;
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_SUCCESS;
                }
            }
        }

        if (SnoopGrpCreateGroupEntry (u4Instance, VlanId,
                                      DestIpAddr, pSnoopPktInfo,
                                      pSnoopPortCfgEntry, 0, 0,
                                      &pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GRP_TRC, "New Group entry creation failed\r\n");
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessReportMessageHandler\r\n");
            return SNOOP_FAILURE;
        }
        u1GrpFirstCreated = SNOOP_TRUE;

        /* Update the forwarding Table with the new created group entry */
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);

            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           pSnoopPktInfo->u4InPort,
                                           SNOOP_CREATE_FWD_ENTRY,
                                           0) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Unable to create multicast MAC "
                           "forwarding entry\r\n");
                /* Delete all the V1/V2 and V3 port records */
                SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

                SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                           SNOOP_DELETE_GRP_ENTRY);
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                              (tRBElem *) pSnoopGroupEntry);
                SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessReportMessageHandler\r\n");
                return SNOOP_FAILURE;
            }

            /* Add the router port bitmap if present to the newly created 
             * multicast forwarding entry */
            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN
                                                     (VlanId),
                                                     pSnoopVlanEntry->
                                                     u1AddressType,
                                                     SNOOP_IGS_IGMP_INVALID_VERSION,
                                                     pSnoopRtrPortBmp,
                                                     pSnoopVlanEntry);
            if (SnoopFwdUpdateFwdBitmap
                (u4Instance, pSnoopRtrPortBmp, pSnoopVlanEntry,
                 pSnoopGroupEntry->GroupIpAddr,
                 SNOOP_ADD_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Unable to add router ports to "
                           "forwarding entry\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessReportMessageHandler\r\n");
                return SNOOP_FAILURE;
            }
        }
        else
        {
            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
            {

                /* if Enhanced mode is enabled, Check whether any forwarding 
                   database is already created in Outer Vlan. 

                   1) If Entry is present, add the member ports of this Group  
                   in existing forwarding database.

                   Note: This is done for following purpose,
                   if Outer Vlan - Inner Vlan Support data trap is not available, 
                   the group entries learnt later after the first forwarding 
                   database will not added in forwarding database.

                   Example:
                   --------    
                   [S,C1,Group,P1] 
                   Data arrival for [S,C1,Group] ----->(IPMC) [S, Source, P1]
                   [S,C2,Group,P2] 
                   Data arrival for [S,C2,Group] -----> No Updation as NP will not
                   trap frames
                   If the target supports, IPMC table with (Outer Vid - Inner Id)
                   the following code, to be removed.
                 */

                SnoopUtilAddPortToVlanIpFwdTable (u4Instance, pSnoopGroupEntry);
            }
        }
    }

    if ((pSnoopVlanEntry->u1AddressType != SNOOP_ADDR_TYPE_IPV4) &&
        (pSnoopVlanEntry->u1AddressType != SNOOP_ADDR_TYPE_IPV6))
    {
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessReportMessageHandler\r\n");
        return SNOOP_FAILURE;
    }

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessReportMessageHandler\r\n");
        return SNOOP_FAILURE;
    }

    pVlanPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pVlanPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pVlanPortBitmap\r\n");
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessReportMessageHandler\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              (pSnoopVlanEntry->u1AddressType -
                                               1)) == SNOOP_FALSE)
    {
        /* If Proxy reporting is disabled and when the switch receives the
         * IGMP v1/v2 report forwarding to router ports is done based on 
         * the report forward timer */

        if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (pSnoopGroupEntry->u1ReportFwdFlag == SNOOP_REPORT_FORWARD))
        {
            /* If sparse mode is enabled, the report forwarding is done based on
             * learnt source port information */

            if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
            {
                pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_NO_FORWARD;
            }
        }

        if (pSnoopGroupEntry->u1ReportFwdFlag == SNOOP_REPORT_FORWARD)
        {
            /* 
             * Report forward timer is not running so forward the reports
             * to the router ports else do nothing 
             */

            u1Forward = ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                          u1ReportFwdAll ==
                          SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                         VLAN_FORWARD_SPECIFIC);

            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_NONEDGE_PORTS)
            {
                if (SnoopMiGetVlanLocalEgressPorts
                    (u4Instance, pSnoopVlanEntry->VlanId,
                     pVlanPortBitmap) == SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) == 0)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Null non-edge ports for this Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                }
            }
            else
            {
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType,
                                                         SNOOP_IGS_IGMP_INVALID_VERSION,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                               sizeof (tSnoopPortBmp));
            }

            SnoopVlanForwardPacket (u4Instance, VlanId,
                                    pSnoopVlanEntry->u1AddressType,
                                    pSnoopPktInfo->u4InPort, u1Forward, pBuf,
                                    pFwdPortBitmap, SNOOP_ASMREPORT_SENT);

            /* Restart the report forward timer */
            pSnoopGroupEntry->ReportFwdTimer.u4Instance = u4Instance;
            pSnoopGroupEntry->ReportFwdTimer.u1TimerType =
                SNOOP_REPORT_FWD_TIMER;

            SnoopTmrStartTimer (&pSnoopGroupEntry->ReportFwdTimer,
                                SNOOP_INSTANCE_INFO (u4Instance)->
                                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                                u4ReportFwdInt);
            pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_NO_FORWARD;
        }
        else
        {
            /* Set the pu1Forward flag to SNOOP_RELEASE_BUFFER so that the 
             * packet buffer will be freed outside this function */
            *pu1Forward = SNOOP_RELEASE_BUFFER;
        }
    }
    else
    {

        /* Proxy/Proxy Reporting is enabled so generate a report
         * based upon the operating Version of the router ports 
         * and send it on to the router ports 
         * Set the pu1Forward flag to SNOOP_RELEASE_BUFFER so that the 
         * incoming packet buffer will be freed outside this function */

        *pu1Forward = SNOOP_RELEASE_BUFFER;

        /* For new entry creation this flag wil be set, and for existing entry
         * any changes will make the filter mode change */
        if (bIsFirstAsmReportForOuterVlan == OSIX_TRUE)
        {
            if ((SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                 u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                u1OprVersion == SNOOP_IGS_IGMP_VERSION3)
            {
                u1V3ReportFwd = SNOOP_TRUE;
            }
            else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                      SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                1].u1ReportFwdAll ==
                      SNOOP_FORWARD_NONEDGE_PORTS)
                     && u1OprVersion == SNOOP_IGS_IGMP_VERSION3)
            {
                /* if report forward is set to non-edge ports alone
                 * then check presence of non-edge ports */
                SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                    pSnoopVlanEntry->VlanId,
                                                    pVlanPortBitmap) ==
                    SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) != 0)
                    {
                        u1V3ReportFwd = SNOOP_TRUE;
                    }
                    if (u1V3ReportFwd == SNOOP_TRUE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Reports are forwarded to non-edge port list for Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                }
            }
            else
            {
                /* if report forward is set to router ports alone
                 * then check presence of router ports */
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType,
                                                         SNOOP_IGS_IGMP_VERSION3,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                if (SNOOP_MEM_CMP
                    (pSnoopRtrPortBmp, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) != 0)
                {
                    u1V3ReportFwd = SNOOP_TRUE;
                }
            }

            if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                (u1V3ReportFwd == SNOOP_TRUE))
            {
                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                {
                    u1V3ReportFwd = SNOOP_FALSE;
                }
            }

            if (u1V3ReportFwd == SNOOP_TRUE)
            {
                /* Send either IGMPV3 or MLDv2 Report based
                 * on the vlan address type */
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    u1PktType = SNOOP_IGMP_V3REPORT;
                    i4Result = IgsEncodeV3Report
                        (u4Instance, pSnoopGroupEntry->pConsGroupEntry,
                         SNOOP_TO_EXCLUDE, &pV3ReportBuf);

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_PKT_TRC,
                                   "Unable to generate and send SSM report on to"
                                   " router ports\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;

                    }

#ifdef ICCH_WANTED
                    /* If MCLAG is enabled forward the report on ICCL interface */

                    if ((u1ReportModify == OSIX_TRUE)
                        && (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
                    {

                        i4IcclResult = IgsEncodeV3ReportOnIccl
                            (u4Instance, pSnoopGroupEntry->pConsGroupEntry,
                             SNOOP_TO_EXCLUDE, &pV3ReportBufOnIccl, u4PhyPort,
                             &SnoopIcchInfo);

                        if (i4IcclResult != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_PKT_TRC,
                                       "Unable to generate and send SSM report on to"
                                       " ICCL router ports\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopProcessReportMessageHandler\r\n");
                            return SNOOP_FAILURE;

                        }
                    }
#endif
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    u1PktType = SNOOP_IGMP_V3REPORT;
                    i4Result = MldsEncodeV2Report
                        (u4Instance, pSnoopGroupEntry->pConsGroupEntry,
                         SNOOP_TO_EXCLUDE, &pV3ReportBuf);

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC,
                                   "Unable to generate and send SSM report on to"
                                   " router ports\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;

                    }

#ifdef ICCH_WANTED
                    /* If MCLAG is enabled form MLD-v2 report  */

                    if ((u1ReportModify == OSIX_TRUE)
                        && (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
                    {
                        i4IcclResult = MldsEncodeV2ReportOnIccl
                            (u4Instance, pSnoopGroupEntry->pConsGroupEntry,
                             SNOOP_TO_EXCLUDE, &pV3ReportBufOnIccl, u4PhyPort);

                        if (i4IcclResult != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_PKT_TRC,
                                       "Unable to generate and send SSM report on to"
                                       " ICCL router ports\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopProcessReportMessageHandler\r\n");
                            return SNOOP_FAILURE;

                        }
                    }
#endif
                }
#endif

                u1Forward = ((SNOOP_INSTANCE_INFO (u4Instance)->
                              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                              u1ReportFwdAll ==
                              SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                             VLAN_FORWARD_SPECIFIC);

                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_RTR_PORTS)
                {
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION3,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }

#ifdef ICCH_WANTED
                /* If MCLAG is enabled forward the report on ICCL interface */

                if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                {
                    SnoopIcchGetIcclIfIndex (&u4IcchPort);

                    if (SnoopVcmGetContextInfoFromIfIndex (u4IcchPort,
                                                           &u4InstanceId,
                                                           &u2LocalPort) ==
                        VCM_FAILURE)
                    {
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl, FALSE);
                        CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessReportMessageHandler\r\n");
                        return SNOOP_FAILURE;
                    }

                    /* If port is present in forward bitmap send the report 
                       on iccl interface and reset the port in forward bitmap */

                    SNOOP_IS_PORT_PRESENT (u2LocalPort, pFwdPortBitmap,
                                           bResult);

                    if ((bResult == OSIX_TRUE) && (u1ReportModify == OSIX_TRUE))
                    {
                        pIcclPortBitMap =
                            UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

                        if (pIcclPortBitMap == NULL)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                       SNOOP_OS_RES_DBG,
                                       "Error in allocating memory for pIcclPortBitMap\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl,
                                                         FALSE);
                            CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopProcessReportMessageHandler\r\n");
                            return SNOOP_FAILURE;

                        }

                        SNOOP_MEM_SET (pIcclPortBitMap, SNOOP_INIT_VAL,
                                       sizeof (tSnoopPortBmp));

                        SNOOP_ADD_TO_PORT_LIST (u2LocalPort, pIcclPortBitMap);

                        SnoopVlanForwardPacket (u4Instance, VlanId,
                                                pSnoopVlanEntry->u1AddressType,
                                                pSnoopPktInfo->u4InPort,
                                                u1Forward, pV3ReportBufOnIccl,
                                                pIcclPortBitMap,
                                                SNOOP_SSMREPORT_SENT);

                        UtilPlstReleaseLocalPortList (pIcclPortBitMap);
                        SNOOP_DEL_FROM_PORT_LIST (u2LocalPort, pFwdPortBitmap);
                    }
                }
#endif
                if (u1PktType == SNOOP_IGMP_V3REPORT)
                {
                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            pSnoopPktInfo->u4InPort, u1Forward,
                                            pV3ReportBuf,
                                            pFwdPortBitmap,
                                            SNOOP_SSMREPORT_SENT);
                }
            }

            if ((SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                 u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                u1OprVersion == SNOOP_IGS_IGMP_VERSION2)
            {
                u1V2ReportFwd = SNOOP_TRUE;
            }
            else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                      SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                1].u1ReportFwdAll ==
                      SNOOP_FORWARD_NONEDGE_PORTS)
                     && u1OprVersion == SNOOP_IGS_IGMP_VERSION2)
            {
                /* if report forward is set to non-edge ports alone
                 * then check presence of non-edge ports */
                SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                if (SnoopMiGetVlanLocalEgressPorts
                    (u4Instance, pSnoopVlanEntry->VlanId,
                     pVlanPortBitmap) == SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) != 0)
                    {
                        u1V2ReportFwd = SNOOP_TRUE;
                    }
                    if (u1V2ReportFwd == SNOOP_TRUE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Reports are forwarded to non-edge port list for Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                }
            }
            else
            {
                /* if report forward is set to router ports alone
                 * then check presence of router ports */
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType,
                                                         SNOOP_IGS_IGMP_VERSION2,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                if (SNOOP_MEM_CMP
                    (pSnoopRtrPortBmp, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) != 0)
                {
                    u1V2ReportFwd = SNOOP_TRUE;
                }
            }

            if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                (u1V2ReportFwd == SNOOP_TRUE))
            {
                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                {
                    u1V2ReportFwd = SNOOP_FALSE;
                }
            }

            if (u1V2ReportFwd == SNOOP_TRUE)
            {
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer <
                        SNOOP_IGS_IGMP_VERSION2)
                    {
                        /*If the switch acting version is v1, v1 reports needs
                           to be sent on V2 routers */
                        u1PktType = SNOOP_IGMP_V1REPORT;
                    }
                    else
                    {
                        u1PktType = SNOOP_IGMP_V2REPORT;
                    }
                    i4Result = IgsEncodeV1V2Report
                        (u4Instance, u1PktType,
                         SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->pConsGroupEntry->
                                            GroupIpAddr.au1Addr),
                         &pV2ReportBuf);

                }
#endif
#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    u1PktType = SNOOP_MLD_V1REPORT;
                    i4Result = MldsEncodeV1Report
                        (u4Instance, u1PktType,
                         pSnoopGroupEntry->pConsGroupEntry->GroupIpAddr,
                         &pV2ReportBuf);
                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_IGS_TRC_FLAG,
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send "
                               "ASM report on to router ports\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
#ifdef ICCH_WANTED
                    CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl, FALSE);
#endif
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }

                u1Forward = ((SNOOP_INSTANCE_INFO (u4Instance)->
                              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                              u1ReportFwdAll ==
                              SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                             VLAN_FORWARD_SPECIFIC);

                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_RTR_PORTS)
                {
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION2,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }

                SnoopVlanForwardPacket (u4Instance, VlanId,
                                        pSnoopVlanEntry->u1AddressType,
                                        pSnoopPktInfo->u4InPort, u1Forward,
                                        pV2ReportBuf,
                                        pFwdPortBitmap, SNOOP_ASMREPORT_SENT);

                /*Reports will not be forwarded to router ports during switchover.Hence do not
                 * increment report transmitted counters when u1IsSwitchOverInProgress flag is set*/
#ifdef L2RED_WANTED
                SNOOP_IS_SWITCHOVER_IN_PROGRESS (u4Instance,
                                                 pSnoopVlanEntry->u1AddressType,
                                                 u1IsSwitchOverInProgress);
#endif
                if ((u1PktType == SNOOP_IGMP_V2REPORT)
                    && (u1IsSwitchOverInProgress == SNOOP_FALSE))
                {
                    u1CounterUpdate = SNOOP_V2REPORT_SENT;
                }
                else if ((u1PktType == SNOOP_IGMP_V1REPORT)
                         && (u1IsSwitchOverInProgress == SNOOP_FALSE))
                {
                    u1CounterUpdate = SNOOP_V1REPORT_SENT;
                }

                if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                          u1CounterUpdate) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_PKT_TRC,
                                    "Unable to update statistics "
                                    "for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }

            }

            if ((SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                 u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                u1OprVersion == SNOOP_IGS_IGMP_VERSION1)
            {
                u1V1ReportFwd = SNOOP_TRUE;
            }
            else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                      SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                1].u1ReportFwdAll ==
                      SNOOP_FORWARD_NONEDGE_PORTS)
                     && u1OprVersion == SNOOP_IGS_IGMP_VERSION1)
            {
                /* if report forward is set to non-edge ports alone
                 * then check presence of non-edge ports */
                SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                if (SnoopMiGetVlanLocalEgressPorts
                    (u4Instance, pSnoopVlanEntry->VlanId,
                     pVlanPortBitmap) == SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) != 0)
                    {
                        u1V1ReportFwd = SNOOP_TRUE;
                    }
                    if (u1V1ReportFwd == SNOOP_TRUE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Reports are forwarded to non-edge port list for Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                }
            }
            else
            {
                /* if report forward is set to router ports alone
                 * then check presence of router ports */
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType,
                                                         SNOOP_IGS_IGMP_VERSION1,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                if (SNOOP_MEM_CMP
                    (pSnoopRtrPortBmp, gNullPortBitMap,
                     sizeof (tSnoopPortBmp)) != 0)
                {
                    u1V1ReportFwd = SNOOP_TRUE;
                }
            }

            if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                (u1V1ReportFwd == SNOOP_TRUE))
            {
                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                {
                    u1V1ReportFwd = SNOOP_FALSE;
                }
            }

            if (u1V1ReportFwd == SNOOP_TRUE)
            {
#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigOperVer <
                        SNOOP_IGS_IGMP_VERSION2)
                    {
                        /*If the switch acting version is v1, v1 reports needs
                           to be sent on V2 routers */
                        u1PktType = SNOOP_IGMP_V1REPORT;
                    }
                    else
                    {
                        u1PktType = SNOOP_IGMP_V2REPORT;
                    }

                    i4Result = IgsEncodeV1V2Report
                        (u4Instance, u1PktType,
                         SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->pConsGroupEntry->
                                            GroupIpAddr.au1Addr),
                         &pV1ReportBuf);

                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_IGS_TRC_FLAG,
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send "
                               "ASM report on to router ports\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessReportMessageHandler\r\n");
                    return SNOOP_FAILURE;
                }

                u1Forward = ((SNOOP_INSTANCE_INFO (u4Instance)->
                              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                              u1ReportFwdAll ==
                              SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                             VLAN_FORWARD_SPECIFIC);

                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_RTR_PORTS)
                {
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION1,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }

                SnoopVlanForwardPacket (u4Instance, VlanId,
                                        pSnoopVlanEntry->u1AddressType,
                                        pSnoopPktInfo->u4InPort, u1Forward,
                                        pV1ReportBuf,
                                        pFwdPortBitmap, SNOOP_ASMREPORT_SENT);

#ifdef L2RED_WANTED
                SNOOP_IS_SWITCHOVER_IN_PROGRESS (u4Instance,
                                                 pSnoopVlanEntry->u1AddressType,
                                                 u1IsSwitchOverInProgress)
#endif
                    /*Reports will not be forwarded to router ports during switchover.Hence do not 
                     * increment report transmitted counters when u1IsSwitchOverInProgress flag is set*/
                    if ((u1PktType == SNOOP_IGMP_V1REPORT)
#ifdef L2RED_WANTED
                        && (u1IsSwitchOverInProgress == SNOOP_FALSE)
#endif
                    )
                {
                    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                              SNOOP_V1REPORT_SENT) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                        SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                        SNOOP_PKT_TRC,
                                        "Unable to update statistics "
                                        "for VLAN ID %d\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }

                }

            }
        }
    }
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    UtilPlstReleaseLocalPortList (pVlanPortBitmap);

    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                              SNOOP_ACTIVE_JOINS) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                        SNOOP_PKT_TRC, "Unable to update statistics "
                        "for VLAN ID %d\r\n",
                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
    }

    /*If Control plane driven approach is enabled , Update the forwarding database */
    if ((SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_ENABLE) &&
        (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP))
    {
        /* IP Based forwarding mode */
        SNOOP_MEM_SET (pSnoopPktInfo->SrcIpAddr.au1Addr, 0,
                       sizeof (IPVX_MAX_INET_ADDR_LEN));

        /* *,G forwarding entry should be updated for below ports.
         * 1. Dynamic V1/V2 report
         * 2. Static ASM ports
         * 3. Static & dynamic mrouters*/

        SNOOP_MEM_SET (TempPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
        SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->ASMPortBitmap,
                                TempPortBitmap);

        SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap, TempPortBitmap);

        if (((u1GrpFirstCreated == SNOOP_FALSE)
#ifdef L2RED_WANTED
             || (SNOOP_TRUE == SNOOP_RED_GENERAL_QUERY_INPROGRESS)
#endif
            )
            &&
            (SnoopFwdGetIpForwardingEntry (u4Instance, pSnoopVlanEntry->VlanId,
                                           pSnoopPktInfo->DestIpAddr,
                                           SrcAddr,
                                           &pSnoopIpGrpFwdEntry) ==
             SNOOP_SUCCESS))
        {

            SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                                   pSnoopIpGrpFwdEntry->PortBitmap, bResult);
            if (bResult == OSIX_TRUE)
            {
                u1FwdTableUpdate = SNOOP_FALSE;
            }
        }

        if (u1FwdTableUpdate == SNOOP_TRUE)
        {

            if (SnoopFwdUpdateIpFwdTable
                (u4Instance, pSnoopGroupEntry,
                 pSnoopPktInfo->SrcIpAddr,
                 0, TempPortBitmap, SNOOP_CREATE_FWD_ENTRY) != SNOOP_SUCCESS)
            {
                SNOOP_TRC (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "Unable to update "
                           "forwarding-database table\r\n");
                if (SnoopGrpDeleteGroupEntry
                    (u4Instance, pSnoopGroupEntry->VlanId,
                     pSnoopGroupEntry->GroupIpAddr) != SNOOP_SUCCESS)
                {
                    SNOOP_TRC (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC, "Unable to Delete "
                               "Group-table table\r\n");
                }

            }
        }
        *pu1Forward = SNOOP_RELEASE_BUFFER;
    }

    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessReportMessageHandler\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessSSMReportMessageHandler                  */
/*                                                                           */
/* Description        : This function processes the incoming IGMPv3/MLDv2    */
/*                      report message                                       */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      pSnoopVlanEntry - pointer to the VLAN Entry          */
/*                      pSnoopPktInfo   - Packet information                 */
/*                      PortBitmap     - Port Bitmap                         */
/*                      pBuf - Bufer Pointer                                 */
/*                                                                           */
/* Output(s)          : pu1Forward - Flag indicating forward specific info   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessSSMReportMessageHandler (UINT4 u4Instance, tSnoopTag VlanId,
                                     tSnoopVlanEntry * pSnoopVlanEntry,
                                     tSnoopPktInfo * pSnoopPktInfo,
                                     tSnoopPortCfgEntry
                                     * pSnoopPortCfgEntry,
                                     UINT1 *pu1Forward,
                                     tSnoopPortBmp PortBitmap,
                                     tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pVlanPortBitmap = NULL;
    UINT1              *pTempPortBitMap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopFilter        SnoopFilterPkt;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pV3ReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pV2ReportBuf = NULL;
    tCRU_BUF_CHAIN_HEADER *pV1ReportBuf = NULL;
    tSnoopSSMReport     SSMReport;
    tSnoopIgmpGroupRec  IgmpGrpRec;
    tSnoopMLDGroupRec   MldGrpRec;
    tSnoopMacGrpFwdEntry MacGrpFwdEntry;
    tIPvXAddr           GrpAddr;
    tRBElem            *pRBElem = NULL;
    tSnoopMacAddr       GrpMacAddr;
#ifdef IGS_WANTED
    UINT4               u4GrpAddr = 0;
    UINT4               u4ProfileId = 0;
    INT4                i4FilterAction = TACM_PERMIT;
#endif
    INT4                i4Result = 0;
    UINT4               u4MaxLength = 0;
    UINT4               u4ActMaxLength = 0;
    UINT4               u4LearntPort = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
    UINT4               u4NonEdgePort = SNOOP_INIT_VAL;
    UINT4               u4RcvPort = 0;
    UINT2               u2NumGrpRec = 0;
    UINT2               u2GrpRecCount = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2NumSrcs = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2IpAddrLen = 0;
    UINT2               u2HdrLen = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1Proxy = SNOOP_FALSE;
    UINT1               u1Snooping = SNOOP_FALSE;
    UINT1               u1PktType = 0;
    UINT1               u1AuxDataLen = 0;
    UINT1               u1RecordType = 0;
    UINT1               u1TagLen = pSnoopPktInfo->u1TagLen;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bOperEdge = OSIX_TRUE;
    UINT1               u1DiscardReport = SNOOP_TRUE;
    UINT1               u1V1ReportFwd = SNOOP_FALSE;
    UINT1               u1V2ReportFwd = SNOOP_FALSE;
    UINT1               u1V3ReportFwd = SNOOP_FALSE;
    UINT1               u1ReportFwd = SNOOP_FALSE;
    UINT1               u1QueryResFwd = SNOOP_FALSE;
    UINT1               u1UpdateMacTable = SNOOP_FALSE;
    UINT1               u1Version = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1Forward = VLAN_FORWARD_SPECIFIC;

#ifdef ICCH_WANTED
    tCRU_BUF_CHAIN_HEADER *pV3ReportBufOnIccl = NULL;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    UINT1              *pIcclPortBitMap = NULL;
    UINT4               u4McLagPort = 0;
    INT4                i4IcclResult = SNOOP_FAILURE;
    UINT4               u4PhyPort = 0;
    UINT4               u4InstanceId = 0;
    UINT4               u4IcchPort = 0;
    UINT2               u2LocalMcLagPort = 0;
    UINT2               u2AuxData = 0;
    UINT2               u2LocalIcchPort = 0;
    UINT1               u1ReportModify = OSIX_FALSE;
    UINT1               u1FirstReportOnMcLag = SNOOP_FALSE;
    UINT1               u1IcclDataPresent = SNOOP_FALSE;
    UINT1               u1AuxDataLenRecv = 0;
    UINT1               u1FirstNonMcLagInterface = SNOOP_FALSE;
    UINT1               u1ReportOnIcclHandled = SNOOP_FALSE;
    UINT1               u1IcclReportVersion = SNOOP_FALSE;
    BOOL1               bIcclInterface = OSIX_FALSE;
#endif
#ifdef MLDS_WANTED
    tIp6Addr            tempAddr;
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);

    SNOOP_MEM_SET (&SnoopFilterPkt, 0, sizeof (tSnoopFilter));
    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));
    SNOOP_MEM_SET (&MldGrpRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (au1GrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));
    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));

#ifdef ICCH_WANTED
    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopIcchInfo.pSnoopPktInfo = pSnoopPktInfo;
#endif

    u4RcvPort = pSnoopPktInfo->u4InPort;
#ifdef ICCH_WANTED
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);

    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        SnoopLaIsMclagInterface (u4PhyPort, &u1ReportModify);
        bIcclInterface = (BOOL1) SnoopIcchIsIcclInterface (u4PhyPort);
    }

    /* If report received on non-mclag interface,
       and enhanced mode is enabled we need to modify
       the report to include inner-vlan also
     */

    if (u1ReportModify == OSIX_FALSE)
    {
        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
        {
            u1ReportModify = OSIX_TRUE;
        }
    }

#endif

    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u2HdrLen =
            (UINT2) (pSnoopPktInfo->u2PktLen - pSnoopPktInfo->u1IpHdrLen);
        if (u2HdrLen == SNOOP_IGMP_HEADER_LEN)
        {
            if (pSnoopPktInfo->u2MaxRespCode == 0)
            {
                u1Version = SNOOP_IGS_IGMP_VERSION1;
            }
            else
            {
                u1Version = SNOOP_IGS_IGMP_VERSION2;
            }
        }
        else
        {
            u1Version = SNOOP_IGS_IGMP_VERSION3;
        }
    }
    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    /* Do not process the report message if the port is a router port */
    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN (VlanId),
                                             pSnoopVlanEntry->u1AddressType, 0,
                                             pSnoopRtrPortBmp, pSnoopVlanEntry);
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort, pSnoopRtrPortBmp, bResult);

#ifdef ICCH_WANTED
    if ((bResult == OSIX_TRUE) && (bIcclInterface == OSIX_FALSE))
#else
    if (bResult == OSIX_TRUE)
#endif
    {
        /* Report processing on all ports is allowed when the report-process
         * config level is set to all ports */

        if (SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) != SNOOP_ALL_REPORT_CONFIG)
        {
            if (SNOOP_PROXY_STATUS (u4Instance,
                                    pSnoopVlanEntry->u1AddressType - 1)
                == SNOOP_ENABLE)
            {
                /* In Proxy mode reports received on router ports
                 * will be dropped, so release the report msg buffer 
                 */
                *pu1Forward = SNOOP_RELEASE_BUFFER;
            }
            else
            {
                *pu1Forward = VLAN_FORWARD_SPECIFIC;
                SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, PortBitmap);
            }

            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Report message processing failed since incoming port"
                       " is a router port\n");
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            return SNOOP_SUCCESS;
        }
    }

    /* Set the variable if both proxy and proxy reporting is disabled. */
    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              pSnoopVlanEntry->u1AddressType -
                                              1) == SNOOP_FALSE)
    {
        u1Snooping = SNOOP_TRUE;
    }

    /* Set the pu1Forward to VLAN_FORWARD_SPECIFIC for snooping mode so that
     * the leave message is forewarded.
     * If in proxy or proxy-reporting mode, set pu1Forward to 
     * SNOOP_RELEASE_BUFFER so that the packet buffer will be freed outside 
     * this function */

    *pu1Forward = (u1Snooping == SNOOP_TRUE) ? VLAN_FORWARD_SPECIFIC :
        SNOOP_RELEASE_BUFFER;

    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &SSMReport,
                               (UINT4) (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        pSnoopPktInfo->u1IpHdrLen),
                               sizeof (tSnoopSSMReport));

    u2NumGrpRec = SNOOP_NTOHS (SSMReport.u2NumGrps);

    u2OffSet = SNOOP_IGMP_SSMGROUP_RECD_SIZE;

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               " for pFwdPortBitmap in SSM Report Message Handler \r\n");
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    pVlanPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pVlanPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pVlanPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               " for pVlanPortBitmap in SSM Report Message Handler \r\n");
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pVlanPortBitmap, 0, sizeof (tSnoopPortBmp));

    for (u2GrpRecCount = 0; u2GrpRecCount < u2NumGrpRec; u2GrpRecCount++)
    {
#ifdef IGS_WANTED
        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {

                /* Consolidated group entry received on ICCL interface with ICCL and MCLAG bit set. 
                   Since as such we can add only one port to the port-list, looping again 
                   for the same group. Hence decrementing counters and moving back the offset */

                if ((bIcclInterface == OSIX_TRUE)
                    && (u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (u1IcclDataPresent != SNOOP_FALSE)
                    && (u1ReportOnIcclHandled == SNOOP_TRUE))
                {
                    u2OffSet = (UINT2) (u2OffSet -
                                        (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                         (u2NoSources * 4)));
                    pSnoopPktInfo->u4InPort = u4RcvPort;
                }

                if ((bIcclInterface == OSIX_TRUE)
                    && (u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (u1ReportOnIcclHandled == SNOOP_FALSE))
                {
                    /* Report received on ICCL interface and auxillary data is not present 
                       move the offset to next group record */

                    u2OffSet = (UINT2) (u2OffSet + SNOOP_AUX_DATA_SIZE);

                    /* Incase of Enhanced mode is enabled, the auxiliary data will
                       be 8 bytes */

                    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
                    {
                        u2OffSet = (UINT2) (u2OffSet + SNOOP_AUX_DATA_SIZE);
                    }

                    pSnoopPktInfo->u4InPort = u4RcvPort;
                }

                u1FirstReportOnMcLag = SNOOP_FALSE;
            }
#endif

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IgmpGrpRec,
                                       (UINT4) (SNOOP_DATA_LINK_HDR_LEN +
                                                u1TagLen +
                                                pSnoopPktInfo->u1IpHdrLen +
                                                u2OffSet),
                                       sizeof (tSnoopIgmpGroupRec));

            u4GrpAddr = SNOOP_NTOHL (IgmpGrpRec.u4GroupAddress);
            u2NoSources = SNOOP_NTOHS (IgmpGrpRec.u2NumSrcs);
            u1RecordType = IgmpGrpRec.u1RecordType;

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                u1AuxDataLenRecv = IgmpGrpRec.u1AuxDataLen;

                if ((u1AuxDataLenRecv == SNOOP_OFFSET_ONE) ||
                    (u1AuxDataLenRecv == SNOOP_OFFSET_TWO))
                {
                    if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                        SNOOP_MCAST_FWD_MODE_IP)
                    {
                        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2AuxData,
                                                   (UINT4)
                                                   (SNOOP_DATA_LINK_HDR_LEN +
                                                    u1TagLen +
                                                    pSnoopPktInfo->u1IpHdrLen +
                                                    u2OffSet +
                                                    (SNOOP_IGMP_SSMGROUP_RECD_SIZE
                                                     +
                                                     (u2NoSources *
                                                      SNOOP_IGMP_SSMSOURCE_RECD_SIZE))),
                                                   SNOOP_OFFSET_TWO);

                        u2AuxData = SNOOP_NTOHS (u2AuxData);

                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *) &u1IcclDataPresent,
                                                   (UINT4)
                                                   (SNOOP_DATA_LINK_HDR_LEN +
                                                    u1TagLen +
                                                    pSnoopPktInfo->u1IpHdrLen +
                                                    u2OffSet +
                                                    (SNOOP_IGMP_SSMGROUP_RECD_SIZE
                                                     +
                                                     (u2NoSources *
                                                      SNOOP_IGMP_SSMSOURCE_RECD_SIZE)
                                                     + SNOOP_OFFSET_TWO)),
                                                   SNOOP_OFFSET_ONE);

                        /* If ICCL report version is V1 or V2 report then
                           the data structures to be updated such that 
                           it received V1 or V2 report.
                         */
                        u1IcclReportVersion = 0;
                        pSnoopPktInfo->u1IcchReportVersion = 0;

                        CRU_BUF_Copy_FromBufChain (pBuf,
                                                   (UINT1 *)
                                                   &u1IcclReportVersion,
                                                   (UINT4)
                                                   (SNOOP_DATA_LINK_HDR_LEN +
                                                    u1TagLen +
                                                    pSnoopPktInfo->u1IpHdrLen +
                                                    u2OffSet +
                                                    (SNOOP_IGMP_SSMGROUP_RECD_SIZE
                                                     +
                                                     (u2NoSources *
                                                      SNOOP_IGMP_SSMSOURCE_RECD_SIZE)
                                                     + SNOOP_OFFSET_TWO +
                                                     SNOOP_OFFSET_ONE)),
                                                   SNOOP_OFFSET_ONE);

                        if (pSnoopPktInfo != NULL)
                        {
                            pSnoopPktInfo->u1IcchReportVersion =
                                u1IcclReportVersion;
                        }

                        /* If enhanced mode is enabled, Get the Inner vlan id for
                           the group */

                        if ((u1AuxDataLenRecv == SNOOP_OFFSET_TWO) &&
                            (SNOOP_INSTANCE_ENH_MODE (u4Instance) ==
                             SNOOP_ENABLE))
                        {

                            CRU_BUF_Copy_FromBufChain (pBuf,
                                                       (UINT1 *)
                                                       &SNOOP_INNER_VLAN
                                                       (VlanId),
                                                       (UINT4)
                                                       (SNOOP_DATA_LINK_HDR_LEN
                                                        + u1TagLen +
                                                        pSnoopPktInfo->
                                                        u1IpHdrLen + u2OffSet +
                                                        (SNOOP_IGMP_SSMGROUP_RECD_SIZE
                                                         +
                                                         (u2NoSources *
                                                          SNOOP_IGMP_SSMSOURCE_RECD_SIZE)
                                                         +
                                                         SNOOP_AUX_DATA_SIZE)),
                                                       SNOOP_OFFSET_TWO);

                            SNOOP_INNER_VLAN (VlanId) =
                                SNOOP_NTOHS (SNOOP_INNER_VLAN (VlanId));
                        }
                    }
                    else
                    {
                        /* Get the auxilary data if MCLAG enabled */
                        u1IcclDataPresent = IgmpGrpRec.u1IcclData;

                        u2AuxData = SNOOP_NTOHS (IgmpGrpRec.u2AuxData);

                    }
                }

                if ((u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (bIcclInterface == OSIX_TRUE))
                {
                    if (u1ReportOnIcclHandled == SNOOP_FALSE)
                    {
                        if (SnoopLaGetAggIndexFromAdminKey
                            (u2AuxData, &u4McLagPort) == LA_TRUE)
                        {
                            if (SnoopVcmGetContextInfoFromIfIndex (u4McLagPort,
                                                                   &u4InstanceId,
                                                                   &u2LocalMcLagPort)
                                == VCM_SUCCESS)
                            {
                                pSnoopPktInfo->u4InPort =
                                    (UINT4) u2LocalMcLagPort;
                            }
                        }
                    }

                    if ((u1IcclDataPresent == SNOOP_TRUE)
                        && (u1ReportOnIcclHandled == SNOOP_FALSE))
                    {
                        u1ReportOnIcclHandled = SNOOP_TRUE;
                        u2GrpRecCount--;
                    }

                    else if ((u1IcclDataPresent == SNOOP_TRUE)
                             && (u1ReportOnIcclHandled == SNOOP_TRUE))
                    {
                        u1ReportOnIcclHandled = SNOOP_FALSE;
                    }
                }
            }
#endif
            bResult = OSIX_FALSE;
            SNOOP_VALIDATE_RECORD_TYPE (u1RecordType, bResult);

            if (bResult == OSIX_TRUE)
            {
                u2OffSet =
                    (UINT2) (u2OffSet +
                             (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                              (u2NoSources * 4)));
                continue;
            }

            /* This is the check to see whether the group address falls under reserved addresses
             * 224.0.0.1 to 224.0.0.255 or not falling under multicast group itself. If condition match
             * then do not process anything, do continue with the next group address */
            if (((u4GrpAddr >= SNOOP_ALL_HOST_IP) &&
                 (u4GrpAddr <= SNOOP_RESVD_MCAST_ADDR_RANGE_MAX))
                ||
                (u4GrpAddr < SNOOP_VALID_MCAST_IP_RANGE_MIN)
                || (u4GrpAddr > SNOOP_VALID_MCAST_IP_RANGE_MAX))
            {
                u2OffSet =
                    (UINT2) (u2OffSet +
                             (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                              (u2NoSources * 4)));
                continue;
            }

            /* SSM reports will be filtered before processing. if the filter
             * function returns DENY, report will not be processed,
             * currently all SSM reports except TO_INCLUDE with NULL source list
             * (none) are treated as ASM joins */

            CRU_BUF_Copy_FromBufChain (pBuf,
                                       (UINT1 *) &(SnoopFilterPkt.MacAddr),
                                       sizeof (tSnoopMacAddr),
                                       sizeof (tSnoopMacAddr));
            SnoopFilterPkt.u1NotifyType = SNOOP_FILTER_PKT_NOTIFY;

            SNOOP_MEM_CPY (&SnoopFilterPkt.VlanId, VlanId, sizeof (tSnoopTag));

            SNOOP_MEM_CPY (SnoopFilterPkt.SrcIpAddr,
                           pSnoopPktInfo->SrcIpAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (SnoopFilterPkt.GrpAddress,
                           &u4GrpAddr, IPVX_MAX_INET_ADDR_LEN);
            SNOOP_INET_NTOHL (SnoopFilterPkt.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopFilterPkt.GrpAddress);

            SnoopFilterPkt.u4InPort = pSnoopPktInfo->u4InPort;

            SnoopFilterPkt.u1Version = SNOOP_IGS_IGMP_VERSION2;
            SnoopFilterPkt.u1PktType = SNOOP_IGMP_V2REPORT;

            /* if the recored type is TO_INCLUDE for zero sources, it means
             * it is a leave for that group */
            if ((u1RecordType == SNOOP_TO_INCLUDE) && (u2NoSources == 0))
            {
                SnoopFilterPkt.u1PktType = SNOOP_IGMP_LEAVE;
            }

            if (SnoopUtilFilterGrpsAndNotify (&SnoopFilterPkt) == SNOOP_DENY)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC, "ASM Report message filtered out\n");
                u2OffSet =
                    (UINT2) (u2OffSet +
                             (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                              (u2NoSources * 4)));
                continue;
            }

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
            {
                SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) gpSSMSrcInfoBuffer,
                                           (UINT4) (SNOOP_DATA_LINK_HDR_LEN +
                                                    u1TagLen +
                                                    pSnoopPktInfo->u1IpHdrLen +
                                                    u2OffSet +
                                                    SNOOP_IGMP_SSMGROUP_RECD_SIZE),
                                           (UINT4) (u2NoSources * 4));
            }

            /* Check if the group entry is present or not */
            IPVX_ADDR_INIT_IPV4 (GrpAddr, SNOOP_ADDR_TYPE_IPV4,
                                 (UINT1 *) &u4GrpAddr);

            if (pSnoopPortCfgEntry != NULL)
            {
                u4ProfileId = pSnoopPortCfgEntry->u4SnoopPortCfgProfileId;
            }

            if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
                && (SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo
                    [pSnoopVlanEntry->u1AddressType - 1].u1FilterStatus
                    == SNOOP_ENABLE) &&
                (u4ProfileId != SNOOP_PORT_CFG_PROFILE_ID_DEF))
            {
                i4FilterAction = SnoopTacApiFilterChannel
                    (&GrpAddr, u4ProfileId,
                     u2NoSources, TACM_IGMP_VERSION3,
                     TACM_IGMP_REPORT, u1RecordType);

                if (i4FilterAction == TACM_DENY)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_GRP_TRC,
                               "IGMP v3 group record is filtered\r\n");
                    /*  Since the complete group record is filtered,
                     *  scan the next group record. In case of snooping mode,
                     *  the report forward time needs to be stopped
                     */
                    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                                              pSnoopVlanEntry->
                                                              u1AddressType -
                                                              1) == SNOOP_FALSE)
                    {
                        /* Proxy / proxy-reporting is disabled, so stop the
                         * report forward timer if it is running */

                        if (SnoopGrpGetGroupEntry (u4Instance, VlanId, GrpAddr,
                                                   &pTempSnoopGroupEntry)
                            == SNOOP_SUCCESS)
                        {
                            if (pTempSnoopGroupEntry->ReportFwdTimer.u1TimerType
                                != SNOOP_INVALID_TIMER_TYPE)
                            {
                                SnoopTmrStopTimer
                                    (&pTempSnoopGroupEntry->ReportFwdTimer);
                                pTempSnoopGroupEntry->u1ReportFwdFlag
                                    = SNOOP_REPORT_FORWARD;
                            }
                        }
                    }

                    u2OffSet =
                        (UINT2) (u2OffSet +
                                 ((pSnoopVlanEntry->u1AddressType ==
                                   SNOOP_ADDR_TYPE_IPV4)
                                  ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                     (u2NoSources *
                                      4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources * 16))));
                    continue;

                }
            }
        }
#endif
#ifdef MLDS_WANTED

        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
        {

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                /* Consolidated group entry received on ICCL interface with ICCL and
                   MCLAG bit set. Since as such we can add only one port to the port-list,
                   looping again for the same group. Hence decrementing counters and
                   moving back the offset */

                if ((bIcclInterface == OSIX_TRUE)
                    && (u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (u1IcclDataPresent != SNOOP_FALSE)
                    && (u1ReportOnIcclHandled == SNOOP_TRUE))
                {
                    u2OffSet -=
                        (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                         (u2NoSources * IPVX_IPV6_ADDR_LEN));
                    pSnoopPktInfo->u4InPort = u4RcvPort;
                }

                if ((bIcclInterface == OSIX_TRUE)
                    && (u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (u1ReportOnIcclHandled == SNOOP_FALSE))
                {
                    /* Report received on ICCL interface and auxillary data is not present
                       move the offset to next group record */

                    u2OffSet += (UINT2) SNOOP_AUX_DATA_SIZE;

                    /* Incase of Enhanced mode is enabled, the auxiliary data will
                       be 8 bytes */

                    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
                    {
                        u2OffSet += (UINT2) SNOOP_AUX_DATA_SIZE;
                    }

                    pSnoopPktInfo->u4InPort = u4RcvPort;
                }

                u1FirstReportOnMcLag = SNOOP_FALSE;
            }
#endif

            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &MldGrpRec,
                                       (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                        pSnoopPktInfo->u1IpHdrLen + u2OffSet),
                                       sizeof (tSnoopMLDGroupRec));

            SNOOP_MEM_CPY (au1GrpAddr, MldGrpRec.u1GrpAddr, IPVX_IPV6_ADDR_LEN);
            SNOOP_INET_NTOHL (au1GrpAddr);
            SNOOP_MEM_CPY (&tempAddr, au1GrpAddr, IPVX_IPV6_ADDR_LEN);
            u2NoSources = SNOOP_NTOHS (MldGrpRec.u2NumSrcs);

            if ((SNOOP_IS_ALL_ROUTERS (tempAddr))
                || (SNOOP_IS_ALL_NODES (tempAddr))
                || (SNOOP_IS_ALL_V2_ROUTERS (tempAddr))
                || SNOOP_IS_RESERVED_MULTI (tempAddr)
                || SNOOP_IS_RESERVED_LINK_SCOPE_MULTI (tempAddr)
                || SNOOP_IS_RESERVED_NODE_SCOPE_MULTI (tempAddr))
            {
                u2OffSet +=
                    (UINT2) (SNOOP_MLD_SSMSOURCE_RECD_SIZE +
                             (u2NoSources * 16));
                continue;
            }

            u1RecordType = MldGrpRec.u1RecordType;

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                /* Get the auxilary data if MCLAG enabled */

                u1AuxDataLenRecv = MldGrpRec.u1AuxDataLen;

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2AuxData,
                                           (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                            pSnoopPktInfo->u1IpHdrLen +
                                            u2OffSet +
                                            (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources *
                                              IPVX_IPV6_ADDR_LEN))),
                                           SNOOP_OFFSET_TWO);

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u1IcclDataPresent,
                                           (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                            pSnoopPktInfo->u1IpHdrLen +
                                            u2OffSet +
                                            (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources *
                                              IPVX_IPV6_ADDR_LEN) +
                                             SNOOP_OFFSET_TWO)),
                                           SNOOP_OFFSET_ONE);

                u2AuxData = SNOOP_NTOHS (u2AuxData);

                if ((u1AuxDataLenRecv != SNOOP_INIT_VAL)
                    && (bIcclInterface == OSIX_TRUE))
                {
                    if (u1ReportOnIcclHandled == SNOOP_FALSE)
                    {
                        if (SnoopLaGetAggIndexFromAdminKey
                            (u2AuxData, &u4McLagPort) == LA_TRUE)
                        {
                            if (SnoopVcmGetContextInfoFromIfIndex (u4McLagPort,
                                                                   &u4InstanceId,
                                                                   &u2LocalMcLagPort)
                                == VCM_SUCCESS)
                            {
                                pSnoopPktInfo->u4InPort =
                                    (UINT4) u2LocalMcLagPort;
                            }
                        }
                    }

                    if ((u1IcclDataPresent == SNOOP_TRUE)
                        && (u1ReportOnIcclHandled == SNOOP_FALSE))
                    {
                        u1ReportOnIcclHandled = SNOOP_TRUE;
                        u2GrpRecCount--;
                    }

                    else if ((u1IcclDataPresent == SNOOP_TRUE)
                             && (u1ReportOnIcclHandled == SNOOP_TRUE))
                    {
                        u1ReportOnIcclHandled = SNOOP_FALSE;
                    }
                }
            }
#endif
            bResult = OSIX_FALSE;
            SNOOP_VALIDATE_RECORD_TYPE (u1RecordType, bResult);

            if (bResult == OSIX_TRUE)
            {
                u2OffSet +=
                    (UINT2) (SNOOP_MLD_SSMSOURCE_RECD_SIZE +
                             (u2NoSources * 16));
                continue;
            }

            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
            {
                SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) gpSSMSrcInfoBuffer,
                                           (SNOOP_DATA_LINK_HDR_LEN + u1TagLen +
                                            pSnoopPktInfo->u1IpHdrLen + u2OffSet
                                            + SNOOP_MLD_SSMGROUP_RECD_SIZE),
                                           u2NoSources * 16);
            }

            IPVX_ADDR_INIT_IPV6 (GrpAddr, SNOOP_ADDR_TYPE_IPV6, au1GrpAddr);
        }
#endif

        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
            (SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[pSnoopPktInfo->
                                                         GroupAddr.u1Afi -
                                                         1].u1FilterStatus !=
             SNOOP_DISABLE))
        {
            /* If the limit type is CHANNEL and the incoming
             * record type is EXCLUDE, dont process the group record */
            if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_CHANNELS)
            {
                if ((u1RecordType == SNOOP_TO_EXCLUDE) ||
                    (u1RecordType == SNOOP_IS_EXCLUDE))
                {
                    u2OffSet =
                        (UINT2) (u2OffSet +
                                 (pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV4)
                                 ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                    (u2NoSources *
                                     4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                            (u2NoSources * 16)));

                    continue;
                }
            }
        }
        /*  The group record is processed during the following cases
         *  1. Filter status is disabled
         *  2. Porfile is not configured in the incoming port
         *  3. The group record has atleast one permitted source.
         *
         *  Set the discard report flag to false to indicate that
         *  atleast one group record in the report is allowed.
         *  The report is forwarded upstream if the flag is set
         *  to false
         */
        u1DiscardReport = SNOOP_FALSE;

        /* Check if the group entry is present or not */
        if (SnoopGrpGetGroupEntry (u4Instance, VlanId, GrpAddr,
                                   &pSnoopGroupEntry) == SNOOP_SUCCESS)
        {
            pSnoopGroupEntry->u1IsUnknownMulticast = SNOOP_FALSE;
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                /* If the group record type is IS_INCL/TO_INCL and no of sources 
                 * is 0 then process like a leave message has come for 
                 * this group */

                if (((u1RecordType == SNOOP_IS_INCLUDE)
                     || (u1RecordType == SNOOP_TO_INCLUDE))
                    && (u2NoSources == 0))
                {
                    IPVX_ADDR_COPY (&(pSnoopPktInfo->GroupAddr),
                                    &(pSnoopGroupEntry->GroupIpAddr));

                    if (SnoopProcessLeaveOrDoneMessage (u4Instance,
                                                        VlanId,
                                                        pSnoopVlanEntry,
                                                        pSnoopPktInfo,
                                                        pSnoopPortCfgEntry,
                                                        &u1Forward,
                                                        PortBitmap)
                        != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC,
                                   "IGMP Leave message processing failed\n");
                    }

                    u2OffSet =
                        (UINT2) (u2OffSet +
                                 (pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV4)
                                 ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                    (u2NoSources *
                                     4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                            (u2NoSources * 16)));

                    if (SnoopClassifyNonePacketType (u4Instance, u1RecordType,
                                                     pSnoopVlanEntry) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Failed to update status \r\n");
                        return SNOOP_FAILURE;
                    }

                    /* Increment the Leave received counter 
                       if INCLUDE_NONE record type is received */

                    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                              SNOOP_LEAVE_RCVD) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Unable to update SNOOP "
                                        "statistics for VLAN ID %d\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }

                    continue;
                }

                if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                                    gNullPortBitMap,
                                    SNOOP_PORT_LIST_SIZE) == 0))
                {
                    if ((pSnoopPortCfgEntry != NULL) &&
                        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
                        (SNOOP_INSTANCE_INFO (u4Instance)->
                         SnoopInfo[pSnoopPktInfo->GroupAddr.u1Afi -
                                   1].u1FilterStatus != SNOOP_DISABLE))
                    {
                        /* If either group or channel based limit is set, and if the 
                         * maximum limit has already been reached, return success */
                        if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                            u1SnoopPortMaxLimitType != SNOOP_PORT_LMT_TYPE_NONE)
                        {
                            /* Check if the maximum limit has already been reached */
                            if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                                u4SnoopPortMaxLimit <= pSnoopPortCfgEntry->
                                pSnoopExtPortCfgEntry->u4SnoopPortCfgMemCnt)
                            {
                                u2OffSet =
                                    (UINT2) (u2OffSet +
                                             ((pSnoopVlanEntry->u1AddressType ==
                                               SNOOP_ADDR_TYPE_IPV4)
                                              ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                                 (u2NoSources *
                                                  4))
                                              : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                                 (u2NoSources * 16))));
                                continue;
                            }
                        }
                    }
                    if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                        SNOOP_MCAST_FWD_MODE_MAC)
                    {
                        /* MAC Based forwarding mode */
                        pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

                        if (SnoopGrpUpdatePortToGroupEntry
                            (u4Instance, pSnoopGroupEntry, pSnoopPktInfo,
                             pSnoopPortCfgEntry, SNOOP_FALSE) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_GRP_TRC,
                                       "Updation of port entry to Group failed\r\n");
                            SnoopConsGroupEntryUpdate (u4Instance,
                                                       pSnoopGroupEntry,
                                                       SNOOP_DELETE_GRP_ENTRY);
                            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                          GroupEntry,
                                          (tRBElem *) pSnoopGroupEntry);
                            SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                        pSnoopGroupEntry);
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            return SNOOP_FAILURE;
                        }
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_GRP_TRC,
                                   "Updation of port entry to Group is done\r\n");
                        SnoopUpdateConsGrpOnFilterChange (u4Instance,
                                                          SNOOP_TO_EXCLUDE,
                                                          pSnoopGroupEntry->
                                                          pConsGroupEntry,
                                                          SNOOP_FALSE);
                    }
                    else
                    {
                        SNOOP_SLL_INIT (&pSnoopGroupEntry->SSMPortList);

                        SNOOP_MEM_SET (pSnoopGroupEntry->InclSrcBmp, 0,
                                       SNOOP_SRC_LIST_SIZE);
                        SNOOP_MEM_SET (pSnoopGroupEntry->ExclSrcBmp, 0xff,
                                       SNOOP_SRC_LIST_SIZE);

                        /* IP Forwarding Mode */
                        if ((pSnoopPktInfo->u1PktType == SNOOP_IGMP_V3REPORT &&
                             pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                             SNOOP_ADDR_TYPE_IPV4)
                            || (pSnoopPktInfo->u1PktType == SNOOP_MLD_V2REPORT
                                && pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                                SNOOP_ADDR_TYPE_IPV6))
                        {
                            if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                                (u1RecordType == SNOOP_IS_INCLUDE))
                            {
                                pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
                            }
                            else
                            {
                                pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                            }

                            if (SnoopGrpHandleSSMReports
                                (u4Instance, u1RecordType, pSnoopGroupEntry,
                                 u2NumSrcs, pSnoopPktInfo,
                                 pSnoopPortCfgEntry) != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_GRP_TRC,
                                           "Processing of ASM report messages"
                                           " for group record failed\r\n");
                                SnoopConsGroupEntryUpdate (u4Instance,
                                                           pSnoopGroupEntry,
                                                           SNOOP_DELETE_GRP_ENTRY);
                                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                              GroupEntry,
                                              (tRBElem *) pSnoopGroupEntry);
                                SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                            pSnoopGroupEntry);
                                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                            SNOOP_DBG (SNOOP_DBG_FLAG,
                                       SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_GRP_TRC,
                                       "Processing of ASM report messages"
                                       " for group record is done\r\n");

                        }
                        else
                        {
                            pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                            SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp,
                                           gNullSrcBmp, SNOOP_SRC_LIST_SIZE);

                            if (SnoopGrpUpdatePortToGroupEntry
                                (u4Instance, pSnoopGroupEntry, pSnoopPktInfo,
                                 pSnoopPortCfgEntry,
                                 SNOOP_FALSE) != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_GRP_TRC,
                                           "Updation of port entry to Group failed\r\n");
                                SnoopConsGroupEntryUpdate (u4Instance,
                                                           pSnoopGroupEntry,
                                                           SNOOP_DELETE_GRP_ENTRY);
                                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                              GroupEntry,
                                              (tRBElem *) pSnoopGroupEntry);
                                SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance,
                                                            pSnoopGroupEntry);
                                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                            SnoopUpdateConsGrpOnFilterChange
                                (u4Instance, SNOOP_TO_EXCLUDE,
                                 pSnoopGroupEntry->pConsGroupEntry, SNOOP_TRUE);
                        }
                    }

                    /*Updating the statistics for active Group count */
                    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                              SNOOP_ACTIVE_GRPS) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_PKT_TRC,
                                        "Unable to update statistics "
                                        "for VLAN ID %d\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                    if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                                   pSnoopPktInfo->u4InPort,
                                                   SNOOP_CREATE_FWD_ENTRY,
                                                   0) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC, "Unable to create multicast "
                                   "MAC forwarding entry\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Multicast "
                               "MAC forwarding entry created\r\n");
                }

                /* Check whether the port is already present in the 
                 * Group Entry's consolidated bitmap */

                SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                                       pSnoopGroupEntry->PortBitmap, bResult);

#ifdef ICCH_WANTED

                /* If MCLAG is enabled, check whether this is the first report
                   learnt on mc-lag or non-mclag. If so send the frame to
                   remote node
                 */

                if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                {
                    SnoopCheckBitMapForFirstNonMcLagInterface
                        (pSnoopGroupEntry->PortBitmap,
                         &u1FirstNonMcLagInterface);

                    if ((bResult == SNOOP_TRUE) && (u1ReportModify == OSIX_TRUE)
                        && (pSnoopGroupEntry->u1McLagLeaveReceived ==
                            SNOOP_TRUE))
                    {
                        pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_FALSE;
                        u1FirstReportOnMcLag = SNOOP_TRUE;
                    }
                }
#endif

                /* Create the port entry if the port is not present else
                 * just update the port purge timers */
                if (SnoopGrpUpdatePortToGroupEntry (u4Instance,
                                                    pSnoopGroupEntry,
                                                    pSnoopPktInfo,
                                                    pSnoopPortCfgEntry,
                                                    SNOOP_FALSE) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_GRP_TRC,
                               "Addition of port entry to Group failed\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC,
                           "Addition of port entry to Group is done\r\n");

                if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
                     SNOOP_ALL_REPORT_CONFIG) &&
                    (SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopVlanEntry->u1AddressType -
                               1].u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS))
                {
                    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof
                                   (tSnoopMacGrpFwdEntry));
                    SNOOP_MEM_CPY (MacGrpFwdEntry.VlanId,
                                   pSnoopGroupEntry->VlanId,
                                   sizeof (tSnoopTag));
                    MacGrpFwdEntry.u1AddressType =
                        pSnoopGroupEntry->GroupIpAddr.u1Afi;

                    if (pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                        SNOOP_ADDR_TYPE_IPV4)
                    {
                        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4 (au1GrpAddr),
                                                 GrpMacAddr);
                    }
                    SNOOP_MEM_CPY (&MacGrpFwdEntry.MacGroupAddr, GrpMacAddr,
                                   SNOOP_MAC_ADDR_LEN);

                    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO
                                         (u4Instance)->MacMcastFwdEntry,
                                         (tRBElem *) & MacGrpFwdEntry);

                    if (pRBElem == NULL)
                    {
                        if (SnoopFwdUpdateMacFwdTable (u4Instance,
                                                       pSnoopGroupEntry,
                                                       pSnoopPktInfo->u4InPort,
                                                       SNOOP_CREATE_FWD_ENTRY,
                                                       0) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Unable to create mcast MAC"
                                       " forwarding entry\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            return SNOOP_FAILURE;
                        }
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Mcast MAC" "forwarding entry created\r\n");

                    }
                    u1UpdateMacTable = SNOOP_TRUE;
                }
                else if (bResult == OSIX_FALSE)
                {
                    u1UpdateMacTable = SNOOP_TRUE;

#ifdef ICCH_WANTED
                    /* If MCLAG is enabled, check whether this is the first report
                       learnt on mc-lag or non-mclag. If so send the frame to 
                       remote node 
                     */
                    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                    {
                        if (((u1ReportModify == OSIX_TRUE)
                             || (u1FirstNonMcLagInterface == SNOOP_TRUE))
                            && (u1IcclDataPresent != SNOOP_TRUE)
                            && (bIcclInterface != OSIX_TRUE))
                        {
                            u1FirstReportOnMcLag = SNOOP_TRUE;
                        }
                    }
#endif
                }

                /* If a new port is added to the existing group entry
                 * add it to the forwarding table */
                if (u1UpdateMacTable == SNOOP_TRUE)
                {
                    SnoopRedActiveSendGrpInfoSync (u4Instance,
                                                   pSnoopGroupEntry);

                    /* Update the forwarding Table with the new port */
                    if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                                   pSnoopPktInfo->u4InPort,
                                                   SNOOP_ADD_PORT,
                                                   0) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Unable to add the port in mcast MAC"
                                   " forwarding entry\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                }
                if (SnoopClassifyPacketType (u4Instance, u1RecordType,
                                             pSnoopGroupEntry, u2NoSources,
                                             pSnoopPktInfo,
                                             pSnoopPortCfgEntry)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Failed to update status \r\n");
                }

            }
            else                /* IP based */
            {

#ifdef ICCH_WANTED
                /* If MCLAG is enabled, check whether this is the first report
                   learnt on mc-lag or non-mclag. If so send the frame to 
                   remote node 
                 */
                if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                {
                    SnoopCheckBitMapForFirstNonMcLagInterface
                        (pSnoopGroupEntry->PortBitmap,
                         &u1FirstNonMcLagInterface);

                    if ((bResult == SNOOP_TRUE) && (u1ReportModify == OSIX_TRUE)
                        && (pSnoopGroupEntry->u1McLagLeaveReceived ==
                            SNOOP_TRUE))
                    {
                        pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_FALSE;
                        u1FirstReportOnMcLag = SNOOP_TRUE;
                    }
                }

                if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) ==
                    SNOOP_DISABLE)
                {
                    /* Check whether the host is already registered */
                    if (SnoopIcchUtilGetHostEntry (u4Instance, pSnoopGroupEntry,
                                                   pSnoopPktInfo) ==
                        SNOOP_SUCCESS)
                    {
                        u1FirstReportOnMcLag = SNOOP_TRUE;
                    }
                }
#endif

                /* Update the group membership data base and forwarding table */
                if (SnoopGrpHandleSSMReports (u4Instance, u1RecordType,
                                              pSnoopGroupEntry, u2NoSources,
                                              pSnoopPktInfo,
                                              pSnoopPortCfgEntry)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_GRP_TRC, "Processing of IGMP v3 messages"
                               " for group record failed\r\n");
                    /* Get the Next Group Record */
                    u2OffSet =
                        (UINT2) (u2OffSet +
                                 ((pSnoopVlanEntry->u1AddressType ==
                                   SNOOP_ADDR_TYPE_IPV4)
                                  ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                     (u2NoSources *
                                      4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources * 16))));
                    continue;
                }

#ifdef ICCH_WANTED

                /* If MCLAG is enabled, check whether this is the first report
                   learnt on mc-lag or non-mclag. If so send the frame to 
                   remote node 
                 */

                if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                {
                    if (((u1ReportModify == OSIX_TRUE)
                         || (u1FirstNonMcLagInterface == SNOOP_TRUE))
                        && (u1IcclDataPresent != SNOOP_TRUE)
                        && (bIcclInterface != OSIX_TRUE))
                    {
                        u1FirstReportOnMcLag = SNOOP_TRUE;
                    }
                }
#endif
            }
            pTempPortBitMap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pTempPortBitMap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG,
                           "Error in allocating memory for pTempPortBitMap\r\n");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for pTempPortBitMap  in SSM Report Message Handler \r\n");
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

            if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                                      pSnoopVlanEntry->
                                                      u1AddressType
                                                      - 1) == SNOOP_FALSE)
            {
                /* Proxy reporting is disabled so forward the report on to 
                 * the router ports and stop the report forward timer if it
                 * is running */

                /* The GroupEntry pointer cannot be used here directly 
                 * since it may have been deleted in case of INCLUDE_NONE.  
                 * So get the group entry again to stop the timer. */
                if (SnoopGrpGetGroupEntry (u4Instance, VlanId, GrpAddr,
                                           &pTempSnoopGroupEntry) ==
                    SNOOP_SUCCESS)
                {
                    if (pTempSnoopGroupEntry->ReportFwdTimer.u1TimerType !=
                        SNOOP_INVALID_TIMER_TYPE)
                    {
                        SnoopTmrStopTimer (&pTempSnoopGroupEntry->
                                           ReportFwdTimer);
                        pTempSnoopGroupEntry->u1ReportFwdFlag =
                            SNOOP_REPORT_FORWARD;
                    }

                    if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
                        && (pSnoopGroupEntry->b1InGroupSourceInfo ==
                            SNOOP_TRUE))
                    {
                        u1DiscardReport = SNOOP_TRUE;
                    }

                    if ((SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
                         SNOOP_ALL_REPORT_CONFIG) && (pSnoopGroupEntry->
                                                      b1InGroupSourceInfo ==
                                                      SNOOP_FALSE)
                        && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) ==
                            SNOOP_ENABLE))
                    {
                        SNOOP_MEM_CPY (pTempPortBitMap,
                                       pSnoopGroupEntry->PortBitmap,
                                       sizeof (tSnoopPortBmp));
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pSnoopVlanEntry->
                                                                 u1AddressType,
                                                                 0,
                                                                 pSnoopRtrPortBmp,
                                                                 pSnoopVlanEntry);
                        SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort,
                                                  pTempPortBitMap);
                        for (u2BytIndex = 0;
                             u2BytIndex < SNOOP_IF_PORT_LIST_SIZE; u2BytIndex++)
                        {
                            if (u2BytIndex >= SNOOP_PORT_LIST_SIZE)
                            {
                                continue;
                            }
                            u1PortFlag = pTempPortBitMap[u2BytIndex];
                            for (u2BitIndex = 0;
                                 ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
                            {
                                bResult = OSIX_FALSE;
                                if ((u1PortFlag & SNOOP_BIT8) != 0)
                                {
                                    u4LearntPort =
                                        (UINT4) ((u2BytIndex *
                                                  SNOOP_PORTS_PER_BYTE) +
                                                 u2BitIndex + 1);

                                    SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                           pSnoopRtrPortBmp,
                                                           bResult);

                                    /* stores physical ports info for local port */
                                    u4NonEdgePort = SNOOP_GET_IFINDEX
                                        (u4Instance, (UINT2) u4LearntPort);
                                    L2IwfGetPortOperEdgeStatus (u4NonEdgePort,
                                                                &bOperEdge);
                                    if ((bResult == OSIX_TRUE)
                                        || (bOperEdge == OSIX_FALSE))
                                    {
                                        u4RtrPortCount++;
                                    }
                                    else
                                    {
                                        u4LrtPortCount++;
                                    }
                                }
                                u1PortFlag = (UINT1) (u1PortFlag << 1);
                            }
                        }

                        if ((u4LrtPortCount > 1) && u4RtrPortCount == 0)
                        {
                            u1DiscardReport = SNOOP_TRUE;
                        }
                        if (pSnoopGroupEntry->u1FirstQueryRes == SNOOP_TRUE)
                        {
                            u1DiscardReport = SNOOP_FALSE;
                            pSnoopGroupEntry->u1FirstQueryRes = SNOOP_FALSE;
                        }

                        if ((pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                             SNOOP_INVALID_TIMER_TYPE) &&
                            (pSnoopGroupEntry->b1InGroupSourceInfo ==
                             SNOOP_FALSE))
                        {
                            u1DiscardReport = SNOOP_FALSE;
                        }
                    }
                }
            }
            else
            {
                if ((SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
                     SNOOP_ALL_REPORT_CONFIG) && (pSnoopGroupEntry->
                                                  b1InGroupSourceInfo ==
                                                  SNOOP_FALSE)
                    && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
                {
                    SNOOP_MEM_CPY (pTempPortBitMap,
                                   pSnoopGroupEntry->PortBitmap,
                                   sizeof (tSnoopPortBmp));

                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopGroupEntry->
                                                             GroupIpAddr.u1Afi,
                                                             0,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);

                    SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort,
                                              pTempPortBitMap);
                    for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE;
                         u2BytIndex++)
                    {
                        if (u2BytIndex >= SNOOP_PORT_LIST_SIZE)
                        {
                            continue;
                        }
                        u1PortFlag = pTempPortBitMap[u2BytIndex];
                        for (u2BitIndex = 0;
                             ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
                        {
                            bResult = OSIX_FALSE;
                            if ((u1PortFlag & SNOOP_BIT8) != 0)
                            {
                                u4LearntPort =
                                    (UINT4) ((u2BytIndex *
                                              SNOOP_PORTS_PER_BYTE) +
                                             u2BitIndex + 1);

                                SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                                       pSnoopRtrPortBmp,
                                                       bResult);

                                /* stores physical ports info for local port */
                                u4NonEdgePort = SNOOP_GET_IFINDEX
                                    (u4Instance, (UINT2) u4LearntPort);
                                L2IwfGetPortOperEdgeStatus (u4NonEdgePort,
                                                            &bOperEdge);
                                if ((bResult == OSIX_TRUE)
                                    || (bOperEdge == OSIX_FALSE))
                                {
                                    u4RtrPortCount++;
                                }
                                else
                                {
                                    u4LrtPortCount++;
                                }
                            }
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                        }
                    }

                    if ((u4LrtPortCount == 0) && u4RtrPortCount >= 1)
                    {
                        u1QueryResFwd = SNOOP_TRUE;
                        gu4SendRecType =
                            (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                            SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE;
                    }
                    if (pSnoopGroupEntry->u1FirstQueryRes == SNOOP_TRUE)
                    {
                        u1QueryResFwd = SNOOP_TRUE;
                        gu4SendRecType =
                            (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                            SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE;
                        pSnoopGroupEntry->u1FirstQueryRes = SNOOP_FALSE;
                    }

                    if ((pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                         SNOOP_INVALID_TIMER_TYPE) &&
                        (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_FALSE))
                    {
                        u1QueryResFwd = SNOOP_TRUE;
                        gu4SendRecType =
                            (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                            SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE;
                    }
                }
                else
                {
                    /* If the forwarding mode MAC based we treat V3 messages as
                     * normal group joins (v2) so reporting will be done for 
                     * first time */

                    /*Check if MCLAG enabled globally */
#ifdef ICCH_WANTED
                    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                    {
                        if (u1FirstReportOnMcLag == SNOOP_TRUE)
                        {
                            u1QueryResFwd = SNOOP_TRUE;
                        }
                    }
                    else
                    {
#endif
                        if (SNOOP_MCAST_FWD_MODE (u4Instance)
                            == SNOOP_MCAST_FWD_MODE_MAC)
                        {
                            /* Get the Next Group Record */
                            u2OffSet =
                                (UINT2) (u2OffSet +
                                         ((pSnoopVlanEntry->u1AddressType ==
                                           SNOOP_ADDR_TYPE_IPV4)
                                          ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                             (u2NoSources *
                                              4))
                                          : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources * 16))));
                            UtilPlstReleaseLocalPortList (pTempPortBitMap);
                            continue;
                        }

                        /* If there is no change in group membership database skip this
                         * group record we dont need to send any report upstream */
                        if (gu4SendRecType == 0)
                        {
                            u2OffSet =
                                (UINT2) (u2OffSet +
                                         ((pSnoopVlanEntry->u1AddressType ==
                                           SNOOP_ADDR_TYPE_IPV4)
                                          ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                             (u2NoSources *
                                              4))
                                          : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                             (u2NoSources * 16))));
                            UtilPlstReleaseLocalPortList (pTempPortBitMap);
                            continue;
                        }
                        u1QueryResFwd = SNOOP_TRUE;
#ifdef ICCH_WANTED
                    }
#endif
                }

                /* Proxy is enabled so generate a report based upon the 
                 * operating Version for the VLAN and send it */
                if (u1QueryResFwd == SNOOP_TRUE)
                {
                    if ((SNOOP_INSTANCE_INFO (u4Instance)->
                         SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                         u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                        u1Version == SNOOP_IGS_IGMP_VERSION3)
                    {
                        u1V3ReportFwd = SNOOP_TRUE;
                    }
                    else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                              SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                        1].u1ReportFwdAll ==
                              SNOOP_FORWARD_NONEDGE_PORTS)
                             && u1Version == SNOOP_IGS_IGMP_VERSION3)
                    {
                        /* if report forward is set to non-edge ports alone
                         * then check presence of non-edge ports */
                        SNOOP_MEM_SET (pVlanPortBitmap, 0,
                                       SNOOP_PORT_LIST_SIZE);
                        SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                        if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                            pSnoopVlanEntry->
                                                            VlanId,
                                                            pVlanPortBitmap) ==
                            SNOOP_SUCCESS)
                        {
                            SnoopUtilGetNonEdgePortList (u4Instance,
                                                         pVlanPortBitmap,
                                                         pFwdPortBitmap);
                            if ((SNOOP_MEM_CMP
                                 (pFwdPortBitmap, gNullPortBitMap,
                                  sizeof (tSnoopPortBmp)) != 0))
                            {
                                u1V3ReportFwd = SNOOP_TRUE;
                            }
                            if (u1V3ReportFwd == SNOOP_TRUE)
                            {
                                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                                SNOOP_CONTROL_PATH_TRC,
                                                SNOOP_VLAN_TRC,
                                                "Reports are forwarded on non-edge port list for Vlan %d\n",
                                                SNOOP_OUTER_VLAN
                                                (pSnoopVlanEntry->VlanId));
                            }
                        }
                    }
                    else
                    {
                        /* if report forward is set to router ports alone
                         * then check presence of router ports */
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pSnoopVlanEntry->
                                                                 u1AddressType,
                                                                 SNOOP_IGS_IGMP_VERSION3,
                                                                 pSnoopRtrPortBmp,
                                                                 pSnoopVlanEntry);
                        if ((SNOOP_MEM_CMP
                             (pSnoopRtrPortBmp, gNullPortBitMap,
                              sizeof (tSnoopPortBmp)) != 0))
                        {
                            u1V3ReportFwd = SNOOP_TRUE;
                        }
                    }

                    if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
                        && (u1V3ReportFwd == SNOOP_TRUE))
                    {
                        /* If sparse mode is enabled, the report forwarding is done based on
                         * learnt source port information */

                        if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                        {
                            u1V3ReportFwd = SNOOP_FALSE;
                        }
                    }

                    if (u1V3ReportFwd == SNOOP_TRUE)
                    {
                        /* Use the misc preallocated buffer and copy the group 
                         * information to the buffer when the max length reaches 
                         * just send the buffer out */

                        if (u4MaxLength == 0)
                        {
                            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                           SNOOP_MISC_MEMBLK_SIZE);
                        }

                        SNOOP_MEM_CPY (au1GrpAddr, GrpAddr.au1Addr,
                                       IPVX_MAX_INET_ADDR_LEN);
                        SNOOP_INET_HTONL (au1GrpAddr);

                        u2IpAddrLen = (pSnoopVlanEntry->u1AddressType ==
                                       SNOOP_ADDR_TYPE_IPV4) ?
                            SNOOP_IP_ADDR_SIZE : IPVX_MAX_INET_ADDR_LEN;

                        u1AuxDataLen = 0;

                        /* Get the record type and number of sources from 
                         * gu4SendRecType which is updated for any new registration 
                         * or source list change for a group record */
                        u1RecordType = (UINT1) (gu4SendRecType & 0x0000ffff);
                        u2NumSrcs = SNOOP_HTONS (gu4SendRecType >> 16);

#ifdef ICCH_WANTED
                        if (u1FirstReportOnMcLag == SNOOP_TRUE)
                        {
                            if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                                SNOOP_MCAST_FWD_MODE_MAC)
                            {
                                u1RecordType = SNOOP_TO_EXCLUDE;
                                u2NumSrcs = SNOOP_INIT_VAL;
                            }
                            else
                            {
                                SNOOP_VALIDATE_RECORD_TYPE (u1RecordType,
                                                            bResult);

                                if (bResult == OSIX_TRUE)
                                {
                                    u1RecordType =
                                        (pSnoopGroupEntry->u1FilterMode ==
                                         SNOOP_INCLUDE) ? SNOOP_TO_INCLUDE :
                                        SNOOP_TO_EXCLUDE;
                                }
                            }
                        }
#endif
                        u2GrpRec++;
                        u1Proxy = SNOOP_FALSE;

                        SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                       &u1RecordType, SNOOP_OFFSET_ONE);
                        u4MaxLength += SNOOP_OFFSET_ONE;

                        SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                       &u1AuxDataLen, SNOOP_OFFSET_ONE);
                        u4MaxLength += SNOOP_OFFSET_ONE;

                        SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                       &u2NumSrcs, SNOOP_SRC_CNT_OFFSET);
#ifdef ICCH_WANTED
                        if (u1FirstReportOnMcLag == SNOOP_TRUE)
                        {
                            u2NumSrcs = SNOOP_HTONS (u2NoSources);
                            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                           &u2NumSrcs, SNOOP_SRC_CNT_OFFSET);
                        }
#endif
                        u4MaxLength += SNOOP_SRC_CNT_OFFSET;

                        SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                       au1GrpAddr, u2IpAddrLen);
                        u4MaxLength += u2IpAddrLen;

                        u2NumSrcs = (UINT2) (gu4SendRecType >> 16);
#ifdef ICCH_WANTED
                        if (u1FirstReportOnMcLag == SNOOP_TRUE)
                        {
                            u2NumSrcs = u2NoSources;
                        }
#endif
                        if (u2NumSrcs)
                        {
                            SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                           gpSSMSrcInfoBuffer,
                                           u2IpAddrLen * u2NumSrcs);
                            u4MaxLength += (UINT4) (u2NumSrcs * u2IpAddrLen);
                        }

                        gu4SendRecType = 0;

#ifdef ICCH_WANTED
                        /* Incase of Sending report on ICCL include auxiliary 
                           data also for total length
                         */
                        if ((u1ReportModify == OSIX_TRUE)
                            && (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
                        {
                            u4ActMaxLength +=
                                (UINT4) (u2GrpRec * SNOOP_AUX_DATA_SIZE);

                            /* Incase of Enhanced mode is enabled, the auxiliary data will
                               be 8 bytes */
                            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) ==
                                SNOOP_ENABLE)
                            {
                                u4ActMaxLength +=
                                    (UINT4) (u2GrpRec *
                                             (2 * SNOOP_AUX_DATA_SIZE));
                            }
                        }
                        else
                        {
                            u4ActMaxLength = u4MaxLength;
                        }
#else
                        u4ActMaxLength = u4MaxLength;
#endif

                        if (((pSnoopVlanEntry->u1AddressType ==
                              SNOOP_ADDR_TYPE_IPV4)
                             && (u4ActMaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                            ||
                            ((pSnoopVlanEntry->u1AddressType ==
                              SNOOP_ADDR_TYPE_IPV6)
                             && (u4ActMaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                        {
                            /* If sparse mode is enabled, the report/leave forwarding is done based on
                             * learnt source port information */

#ifdef IGS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV4)
                            {
                                i4Result = IgsEncodeAggV3Report (u4Instance,
                                                                 u2GrpRec,
                                                                 u4ActMaxLength,
                                                                 &pV3ReportBuf);

                                if (i4Result != SNOOP_SUCCESS)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_PKT_TRC,
                                               "Unable to generate and send V3 report "
                                               "on to router ports\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitMap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;

                                }

#ifdef ICCH_WANTED
                                if ((u1ReportModify == OSIX_TRUE) &&
                                    (SnoopLaGetMCLAGSystemStatus () ==
                                     LA_ENABLED))
                                {
                                    i4IcclResult =
                                        IgsEncodeAggV3ReportOnIccl (u4Instance,
                                                                    u2GrpRec,
                                                                    u4ActMaxLength,
                                                                    &pV3ReportBufOnIccl,
                                                                    u4PhyPort,
                                                                    &SnoopIcchInfo);

                                    if (i4IcclResult != SNOOP_SUCCESS)
                                    {
                                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                                   SNOOP_CONTROL_PATH_TRC,
                                                   SNOOP_PKT_TRC,
                                                   "Unable to generate and send V3 report "
                                                   "on to router ports\r\n");
                                        UtilPlstReleaseLocalPortList
                                            (pFwdPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pVlanPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pTempPortBitMap);
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        CRU_BUF_Release_MsgBufChain
                                            (pV3ReportBuf, FALSE);
                                        return SNOOP_FAILURE;

                                    }
                                }
#endif
                            }
#endif
#ifdef MLDS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV6)
                            {
                                i4Result = MldsEncodeAggV2Report (u4Instance,
                                                                  u2GrpRec,
                                                                  u4ActMaxLength,
                                                                  &pV3ReportBuf);

                                if (i4Result != SNOOP_SUCCESS)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_PKT_TRC,
                                               "Unable to generate and send V3 report "
                                               "on to router ports\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitMap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;

                                }

#ifdef ICCH_WANTED
                                if ((u1ReportModify == OSIX_TRUE) &&
                                    (SnoopLaGetMCLAGSystemStatus () ==
                                     LA_ENABLED))
                                {
                                    i4IcclResult = MldsEncodeAggV2ReportOnIccl
                                        (u4Instance, u2GrpRec,
                                         u4ActMaxLength, &pV3ReportBufOnIccl,
                                         u4PhyPort);

                                    if (i4IcclResult != SNOOP_SUCCESS)
                                    {
                                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                                   SNOOP_CONTROL_PATH_TRC,
                                                   SNOOP_PKT_TRC,
                                                   "Unable to generate and send V3 report "
                                                   "on to router ports\r\n");
                                        UtilPlstReleaseLocalPortList
                                            (pFwdPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pVlanPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pTempPortBitMap);
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        CRU_BUF_Release_MsgBufChain
                                            (pV3ReportBuf, FALSE);
                                        return SNOOP_FAILURE;

                                    }

                                }
#endif
                            }
#endif

                            u1Forward =
                                ((SNOOP_INSTANCE_INFO (u4Instance)->
                                  SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                                  u1ReportFwdAll ==
                                  SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                                 VLAN_FORWARD_SPECIFIC);

                            if (SNOOP_INSTANCE_INFO (u4Instance)->
                                SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                          1].u1ReportFwdAll ==
                                SNOOP_FORWARD_RTR_PORTS)
                            {
                                SNOOP_MEM_SET (pFwdPortBitmap, 0,
                                               SNOOP_PORT_LIST_SIZE);
                                SnoopVlanGetRtrPortFromPVlanMappingInfo
                                    (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                     pSnoopVlanEntry->u1AddressType,
                                     SNOOP_IGS_IGMP_VERSION3, pSnoopRtrPortBmp,
                                     pSnoopVlanEntry);
                                SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                               sizeof (tSnoopPortBmp));
                            }

#ifdef ICCH_WANTED
                            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                            {
                                SnoopIcchGetIcclIfIndex (&u4IcchPort);

                                if (SnoopVcmGetContextInfoFromIfIndex
                                    (u4IcchPort, &u4InstanceId,
                                     &u2LocalIcchPort) != VCM_SUCCESS)
                                {
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitMap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    CRU_BUF_Release_MsgBufChain (pV3ReportBuf,
                                                                 FALSE);
                                    CRU_BUF_Release_MsgBufChain
                                        (pV3ReportBufOnIccl, FALSE);
                                    return SNOOP_FAILURE;
                                }

                                SNOOP_IS_PORT_PRESENT (u2LocalIcchPort,
                                                       pFwdPortBitmap, bResult);

                                if ((bResult == OSIX_TRUE)
                                    && (u1ReportModify == OSIX_TRUE))
                                {
                                    pIcclPortBitMap =
                                        UtilPlstAllocLocalPortList (sizeof
                                                                    (tSnoopPortBmp));

                                    if (pIcclPortBitMap == NULL)
                                    {
                                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                                   SNOOP_CONTROL_PATH_TRC,
                                                   SNOOP_OS_RES_DBG,
                                                   "Error in allocating memory for pIcclPortBitMap\r\n");
                                        UtilPlstReleaseLocalPortList
                                            (pFwdPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pVlanPortBitmap);
                                        UtilPlstReleaseLocalPortList
                                            (pTempPortBitMap);
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        CRU_BUF_Release_MsgBufChain
                                            (pV3ReportBufOnIccl, FALSE);
                                        CRU_BUF_Release_MsgBufChain
                                            (pV3ReportBuf, FALSE);
                                        return SNOOP_FAILURE;

                                    }

                                    SNOOP_MEM_SET (pIcclPortBitMap,
                                                   SNOOP_INIT_VAL,
                                                   sizeof (tSnoopPortBmp));

                                    SNOOP_ADD_TO_PORT_LIST (u2LocalIcchPort,
                                                            pIcclPortBitMap);

                                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                                            pSnoopVlanEntry->
                                                            u1AddressType,
                                                            u4RcvPort,
                                                            u1Forward,
                                                            pV3ReportBufOnIccl,
                                                            pIcclPortBitMap,
                                                            SNOOP_SSMREPORT_SENT);

                                    UtilPlstReleaseLocalPortList
                                        (pIcclPortBitMap);
                                    SNOOP_DEL_FROM_PORT_LIST (u2LocalIcchPort,
                                                              pFwdPortBitmap);
                                }
                            }
#endif

                            SnoopVlanForwardPacket (u4Instance,
                                                    VlanId,
                                                    pSnoopVlanEntry->
                                                    u1AddressType,
                                                    u4RcvPort,
                                                    u1Forward, pV3ReportBuf,
                                                    pFwdPortBitmap,
                                                    SNOOP_SSMREPORT_SENT);

                            u2GrpRec = 0;
                            u1Proxy = SNOOP_TRUE;
                            u4MaxLength = 0;
                        }
                    }
                }
            }
            UtilPlstReleaseLocalPortList (pTempPortBitMap);
        }
        /* Group Entry not found create a new one */
        else
        {
            /* If the group record type is TO_INCL/IS_INCL and number of
             * sources is 0 then process like a leave message has come for 
             * this group, so when group entry does not exist skip this 
             * group record.
             * ALLOW or BLOCK messages should not be processed prior to the 
             * reception of TO_EXCLUDE or TO_INCLUDE (ie if the group record
             * does not exist */

            if (((u1RecordType == SNOOP_TO_INCLUDE ||
                  u1RecordType == SNOOP_IS_INCLUDE) && (u2NoSources == 0)) ||
                (u1RecordType == SNOOP_ALLOW) || (u1RecordType == SNOOP_BLOCK))
            {
                u2OffSet =
                    (UINT2) (u2OffSet +
                             ((pSnoopVlanEntry->u1AddressType ==
                               SNOOP_ADDR_TYPE_IPV4)
                              ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                 (u2NoSources *
                                  4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                         (u2NoSources * 16))));
                if (SnoopClassifyNonePacketType
                    (u4Instance, u1RecordType,
                     pSnoopVlanEntry) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Failed to update status \r\n");
                }
                continue;
            }

            /* SnoopGrpCreateGroupEntry function will take care of adding 
             * the group entry to the consolidated group entry of the  
             * corresponding Outer-VLAN and also the consolidation of 
             * source bitmaps. */
            /* Update the group membership and forwarding database */

            if ((pSnoopPortCfgEntry != NULL) &&
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
                (SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[pSnoopPktInfo->
                                                             GroupAddr.u1Afi -
                                                             1].
                 u1FilterStatus != SNOOP_DISABLE))
            {
                /* If either group or channel based limit is set, and if the 
                 * maximum limit has already been reached, return success */
                if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u1SnoopPortMaxLimitType != SNOOP_PORT_LMT_TYPE_NONE)
                {
                    /* Check if the maximum limit has already been reached */
                    if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                        u4SnoopPortMaxLimit <=
                        pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                        u4SnoopPortCfgMemCnt)
                    {
                        u2OffSet =
                            (UINT2) (u2OffSet +
                                     ((pSnoopVlanEntry->u1AddressType ==
                                       SNOOP_ADDR_TYPE_IPV4)
                                      ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                         (u2NoSources *
                                          4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                                 (u2NoSources * 16))));

                        continue;
                    }
                }
            }
            if (SnoopGrpCreateGroupEntry (u4Instance, VlanId,
                                          GrpAddr, pSnoopPktInfo,
                                          pSnoopPortCfgEntry,
                                          u2NoSources, u1RecordType,
                                          &pSnoopGroupEntry) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_GRP_TRC,
                                "New Grp entry %s :creation failed\r\n",
                                SnoopPrintIPvxAddress (GrpAddr));
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }

#ifdef ICCH_WANTED
            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
            {
                u1ReportModify = OSIX_TRUE;
                u1FirstReportOnMcLag = SNOOP_TRUE;
            }
#endif

            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_GRP_TRC,
                            "New Grp entry %s :creation successful\r\n",
                            SnoopPrintIPvxAddress (GrpAddr));
            /* Update the forwarding Table with the new group entry */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                if (SnoopClassifyPacketType (u4Instance, u1RecordType,
                                             pSnoopGroupEntry, u2NoSources,
                                             pSnoopPktInfo,
                                             pSnoopPortCfgEntry)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Failed to update status \r\n");
                }
                SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);
                if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                               pSnoopPktInfo->u4InPort,
                                               SNOOP_CREATE_FWD_ENTRY,
                                               0) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC, "Unable to create multicast"
                               "MAC forwarding entry\r\n");
                    /* Delete all the V1/V2 and V3 port records */
                    SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

                    SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                               SNOOP_DELETE_GRP_ENTRY);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                  (tRBElem *) pSnoopGroupEntry);
                    SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Multicast"
                           "MAC forwarding entry created\r\n");

                /* Add the router port bitmap if present to the newly created 
                 * multicast forwarding entry */
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                if (SnoopFwdUpdateFwdBitmap
                    (u4Instance, pSnoopRtrPortBmp, pSnoopVlanEntry,
                     pSnoopGroupEntry->GroupIpAddr,
                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Unable to add router ports to "
                               "forwarding entry\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
            }
            if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                                      pSnoopVlanEntry->
                                                      u1AddressType - 1) ==
                SNOOP_TRUE)
            {
                /* If this is not the first group entry for the consolidated 
                 * group entry then we dont need to send any report upstream */
                if (gu4SendRecType == 0)
                {
                    /* If enhanced mode is enabled, and group entry not present
                       a new [SVLAN,CVLAN] entry to be created.
                       Sync the received information with remote node */
#ifdef ICCH_WANTED
                    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) != SNOOP_ENABLE)
                    {
#endif
                        /* Get the Next Group Record */
                        u2OffSet =
                            (UINT2) (u2OffSet +
                                     ((pSnoopGroupEntry->pConsGroupEntry->
                                       GroupIpAddr.u1Afi ==
                                       SNOOP_ADDR_TYPE_IPV4)
                                      ? (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                         (u2NoSources *
                                          4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                                 (u2NoSources * 16))));
#ifdef ICCH_WANTED
                        continue;
                    }
#endif

                    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
                    {
                        SnoopUtilAddPortToVlanIpFwdTable (u4Instance,
                                                          pSnoopGroupEntry);
                    }

                }

                /* Proxy is enabled so generate a report based upon the 
                 * operation Version for the VLAN and send it */

                if ((SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                     u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                    u1Version == SNOOP_IGS_IGMP_VERSION3)
                {
                    u1V3ReportFwd = SNOOP_TRUE;
                }
                else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                    1].u1ReportFwdAll ==
                          SNOOP_FORWARD_NONEDGE_PORTS)
                         && u1Version == SNOOP_IGS_IGMP_VERSION3)
                {
                    /* if report forward is set to non-edge ports alone
                     * then check presence of non-edge ports */
                    SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                    if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                        pSnoopVlanEntry->VlanId,
                                                        pVlanPortBitmap) ==
                        SNOOP_SUCCESS)
                    {
                        SnoopUtilGetNonEdgePortList (u4Instance,
                                                     pVlanPortBitmap,
                                                     pFwdPortBitmap);
                        if ((SNOOP_MEM_CMP
                             (pFwdPortBitmap, gNullPortBitMap,
                              sizeof (tSnoopPortBmp)) != 0))
                        {
                            u1V3ReportFwd = SNOOP_TRUE;
                        }
                        if (u1V3ReportFwd == SNOOP_TRUE)
                        {
                            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                            SNOOP_CONTROL_PATH_TRC,
                                            SNOOP_VLAN_TRC,
                                            "Reports are forwarded on non-edge port list for Vlan %d\n",
                                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                              VlanId));
                        }
                    }
                }
                else
                {
                    /* if report forward is set to router ports alone
                     * then check presence of router ports */
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION3,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    if ((SNOOP_MEM_CMP
                         (pSnoopRtrPortBmp, gNullPortBitMap,
                          sizeof (tSnoopPortBmp)) != 0))
                    {
                        u1V3ReportFwd = SNOOP_TRUE;
                    }
                }

                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (u1V3ReportFwd == SNOOP_TRUE))
                {
                    if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                    {
                        u1V3ReportFwd = SNOOP_FALSE;
                    }
                }

                if (u1V3ReportFwd == SNOOP_TRUE)
                {
                    /* If the forwarding is done based on MAC then construct 
                     * the SSM Reports for all the Group records with NULL 
                     * source list */

                    /* if the forwarding is done based on IP then constrcut the 
                       SSM Reports for all the Group records with list 
                       of sources */
                    if (u4MaxLength == 0)
                    {
                        SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                       SNOOP_MISC_MEMBLK_SIZE);
                    }
                    SNOOP_MEM_CPY (au1GrpAddr, GrpAddr.au1Addr,
                                   IPVX_MAX_INET_ADDR_LEN);
                    SNOOP_INET_HTONL (au1GrpAddr);
                    u2IpAddrLen = (pSnoopVlanEntry->u1AddressType ==
                                   SNOOP_ADDR_TYPE_IPV4) ?
                        SNOOP_IP_ADDR_SIZE : IPVX_MAX_INET_ADDR_LEN;

                    if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                        SNOOP_MCAST_FWD_MODE_MAC)
                    {
                        u1RecordType = SNOOP_TO_EXCLUDE;
                        u2NumSrcs = 0;
                    }
                    else
                    {
                        u1RecordType = (UINT1) (gu4SendRecType & 0x0000ffff);
                        u2NumSrcs = SNOOP_HTONS (gu4SendRecType >> 16);
                    }

                    u1AuxDataLen = 0;
                    u2GrpRec++;
                    u1Proxy = SNOOP_FALSE;

                    SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                   &u1RecordType, SNOOP_OFFSET_ONE);
                    u4MaxLength += SNOOP_OFFSET_ONE;

                    SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                   &u1AuxDataLen, SNOOP_OFFSET_ONE);
                    u4MaxLength += SNOOP_OFFSET_ONE;

                    SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                   &u2NumSrcs, SNOOP_SRC_CNT_OFFSET);
                    u4MaxLength += SNOOP_SRC_CNT_OFFSET;

                    SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                   au1GrpAddr, u2IpAddrLen);
                    u4MaxLength += u2IpAddrLen;

                    if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                        SNOOP_MCAST_FWD_MODE_IP)
                    {
                        u2NumSrcs = (UINT2) (gu4SendRecType >> 16);
                    }

                    if (u2NumSrcs != 0)
                    {
                        SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                       gpSSMSrcInfoBuffer,
                                       u2IpAddrLen * u2NumSrcs);
                        u4MaxLength += (UINT4) (u2NumSrcs * u2IpAddrLen);
                    }

                    gu4SendRecType = 0;

#ifdef ICCH_WANTED
                    /* Incase of Sending report on ICCL include auxiliary
                       data also for total length
                     */
                    if ((u1ReportModify == OSIX_TRUE)
                        && (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
                    {
                        u4ActMaxLength =
                            u4MaxLength +
                            (UINT4) (u2GrpRec * SNOOP_AUX_DATA_SIZE);

                        /* Incase of Enhanced mode is enabled, the auxiliary data will
                           be 8 bytes */
                        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) ==
                            SNOOP_ENABLE)
                        {
                            u4ActMaxLength =
                                u4MaxLength +
                                (UINT4) (u2GrpRec * (2 * SNOOP_AUX_DATA_SIZE));
                        }

                    }
                    else
                    {
                        u4ActMaxLength = u4MaxLength;
                    }
#else
                    u4ActMaxLength = u4MaxLength;
#endif

                    if (((pSnoopVlanEntry->u1AddressType ==
                          SNOOP_ADDR_TYPE_IPV4)
                         && (u4ActMaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                        ||
                        ((pSnoopVlanEntry->u1AddressType ==
                          SNOOP_ADDR_TYPE_IPV6)
                         && (u4ActMaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                    {
#ifdef IGS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV4)
                        {
                            i4Result = IgsEncodeAggV3Report (u4Instance,
                                                             u2GrpRec,
                                                             u4ActMaxLength,
                                                             &pV3ReportBuf);

                            if (i4Result != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_PKT_TRC,
                                           "Unable to generate and send SSM report"
                                           " on to the router ports\r\n");
                                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }

#ifdef ICCH_WANTED
                            if ((u1ReportModify == OSIX_TRUE)
                                && (SnoopLaGetMCLAGSystemStatus () ==
                                    LA_ENABLED))
                            {
                                i4IcclResult =
                                    IgsEncodeAggV3ReportOnIccl (u4Instance,
                                                                u2GrpRec,
                                                                u4ActMaxLength,
                                                                &pV3ReportBufOnIccl,
                                                                u4PhyPort,
                                                                &SnoopIcchInfo);

                                if (i4IcclResult != SNOOP_SUCCESS)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_PKT_TRC,
                                               "Unable to generate and send SSM report"
                                               " on to the router ports\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    CRU_BUF_Release_MsgBufChain (pV3ReportBuf,
                                                                 FALSE);
                                    return SNOOP_FAILURE;
                                }

                            }
#endif
                        }
#endif
#ifdef MLDS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV6)
                        {
                            i4Result = MldsEncodeAggV2Report (u4Instance,
                                                              u2GrpRec,
                                                              u4ActMaxLength,
                                                              &pV3ReportBuf);

                            if (i4Result != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_PKT_TRC,
                                           "Unable to generate and send SSM report"
                                           " on to the router ports\r\n");
                                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }

#ifdef ICCH_WANTED
                            if ((u1ReportModify == OSIX_TRUE)
                                && (SnoopLaGetMCLAGSystemStatus () ==
                                    LA_ENABLED))
                            {
                                i4IcclResult = MldsEncodeAggV2ReportOnIccl
                                    (u4Instance, u2GrpRec,
                                     u4ActMaxLength, &pV3ReportBufOnIccl,
                                     u4PhyPort);

                                if (i4IcclResult != SNOOP_SUCCESS)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_PKT_TRC,
                                               "Unable to generate and send SSM report"
                                               " on to the router ports\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    CRU_BUF_Release_MsgBufChain (pV3ReportBuf,
                                                                 FALSE);
                                    return SNOOP_FAILURE;
                                }
                            }
#endif

                        }
#endif

                        u1Forward =
                            ((SNOOP_INSTANCE_INFO (u4Instance)->
                              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                              u1ReportFwdAll ==
                              SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                             VLAN_FORWARD_SPECIFIC);

                        if (SNOOP_INSTANCE_INFO (u4Instance)->
                            SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                      1].u1ReportFwdAll ==
                            SNOOP_FORWARD_RTR_PORTS)
                        {
                            SNOOP_MEM_SET (pFwdPortBitmap, 0,
                                           SNOOP_PORT_LIST_SIZE);
                            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                     SNOOP_OUTER_VLAN
                                                                     (VlanId),
                                                                     pSnoopVlanEntry->
                                                                     u1AddressType,
                                                                     SNOOP_IGS_IGMP_VERSION3,
                                                                     pSnoopRtrPortBmp,
                                                                     pSnoopVlanEntry);
                            SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                           sizeof (tSnoopPortBmp));
                        }

#ifdef ICCH_WANTED
                        if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                        {
                            SnoopIcchGetIcclIfIndex (&u4IcchPort);

                            if (SnoopVcmGetContextInfoFromIfIndex (u4IcchPort,
                                                                   &u4InstanceId,
                                                                   &u2LocalIcchPort)
                                != VCM_SUCCESS)
                            {
                                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                                UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                CRU_BUF_Release_MsgBufChain (pV3ReportBuf,
                                                             FALSE);
                                CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl,
                                                             FALSE);
                                return SNOOP_FAILURE;
                            }

                            SNOOP_IS_PORT_PRESENT (u2LocalIcchPort,
                                                   pFwdPortBitmap, bResult);

                            if ((bResult == OSIX_TRUE)
                                && (u1ReportModify == OSIX_TRUE))
                            {
                                pIcclPortBitMap =
                                    UtilPlstAllocLocalPortList (sizeof
                                                                (tSnoopPortBmp));

                                if (pIcclPortBitMap == NULL)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_OS_RES_DBG,
                                               "Error in allocating memory for pIcclPortBitMap\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pFwdPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pVlanPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    CRU_BUF_Release_MsgBufChain
                                        (pV3ReportBufOnIccl, FALSE);
                                    CRU_BUF_Release_MsgBufChain (pV3ReportBuf,
                                                                 FALSE);
                                    return SNOOP_FAILURE;
                                }

                                SNOOP_MEM_SET (pIcclPortBitMap, SNOOP_INIT_VAL,
                                               sizeof (tSnoopPortBmp));

                                SNOOP_ADD_TO_PORT_LIST (u2LocalIcchPort,
                                                        pIcclPortBitMap);

                                SnoopVlanForwardPacket (u4Instance, VlanId,
                                                        pSnoopVlanEntry->
                                                        u1AddressType,
                                                        u4RcvPort, u1Forward,
                                                        pV3ReportBufOnIccl,
                                                        pIcclPortBitMap,
                                                        SNOOP_SSMREPORT_SENT);

                                UtilPlstReleaseLocalPortList (pIcclPortBitMap);
                                SNOOP_DEL_FROM_PORT_LIST (u2LocalIcchPort,
                                                          pFwdPortBitmap);
                            }
                        }
#endif

                        SnoopVlanForwardPacket (u4Instance,
                                                VlanId,
                                                pSnoopVlanEntry->
                                                u1AddressType,
                                                u4RcvPort,
                                                u1Forward, pV3ReportBuf,
                                                pFwdPortBitmap,
                                                SNOOP_SSMREPORT_SENT);

                        u2GrpRec = 0;
                        u1Proxy = SNOOP_TRUE;
                        u4MaxLength = 0;
                    }
                }

                if ((SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                     u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                    u1Version == SNOOP_IGS_IGMP_VERSION2)
                {
                    u1V2ReportFwd = SNOOP_TRUE;
                }
                else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                    1].u1ReportFwdAll ==
                          SNOOP_FORWARD_NONEDGE_PORTS)
                         && u1Version == SNOOP_IGS_IGMP_VERSION2)
                {
                    /* if report forward is set to non-edge ports alone
                     * then check presence of non-edge ports */
                    SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                    if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                        pSnoopVlanEntry->VlanId,
                                                        pVlanPortBitmap) ==
                        SNOOP_SUCCESS)
                    {
                        SnoopUtilGetNonEdgePortList (u4Instance,
                                                     pVlanPortBitmap,
                                                     pFwdPortBitmap);
                        if (SNOOP_MEM_CMP
                            (pFwdPortBitmap, gNullPortBitMap,
                             sizeof (tSnoopPortBmp)) != 0)
                        {
                            u1V2ReportFwd = SNOOP_TRUE;
                        }
                        if (u1V2ReportFwd == SNOOP_TRUE)
                        {
                            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                            SNOOP_CONTROL_PATH_TRC,
                                            SNOOP_VLAN_TRC,
                                            "Reports are forwarded to non-edge port list for Vlan %d\n",
                                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                              VlanId));
                        }
                    }
                }
                else
                {
                    /* if report forward is set to router ports alone
                     * then check presence of router ports */
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION2,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    if (SNOOP_MEM_CMP
                        (pSnoopRtrPortBmp, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) != 0)
                    {
                        u1V2ReportFwd = SNOOP_TRUE;
                    }
                }

                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (u1V2ReportFwd == SNOOP_TRUE))
                {
                    if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                    {
                        u1V2ReportFwd = SNOOP_FALSE;
                    }
                }

                if (u1V2ReportFwd == SNOOP_TRUE)
                {
#ifdef IGS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                    {
                        u1PktType = SNOOP_IGMP_V2REPORT;
                        i4Result = IgsEncodeV1V2Report (u4Instance,
                                                        u1PktType,
                                                        SNOOP_PTR_FETCH_4
                                                        (pSnoopGroupEntry->
                                                         pConsGroupEntry->
                                                         GroupIpAddr.au1Addr),
                                                        &pV2ReportBuf);
                    }
#endif
#ifdef MLDS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    {
                        u1PktType = SNOOP_MLD_V1REPORT;
                        i4Result = MldsEncodeV1Report (u4Instance, u1PktType,
                                                       pSnoopGroupEntry->
                                                       pConsGroupEntry->
                                                       GroupIpAddr,
                                                       &pV2ReportBuf);
                    }
#endif

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC, "Unable to generate and send "
                                   "ASM report on to router ports\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }

                    u1Forward =
                        ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                          u1ReportFwdAll ==
                          SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                         VLAN_FORWARD_SPECIFIC);

                    if (SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                  1].u1ReportFwdAll == SNOOP_FORWARD_RTR_PORTS)
                    {
                        SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pSnoopVlanEntry->
                                                                 u1AddressType,
                                                                 SNOOP_IGS_IGMP_VERSION2,
                                                                 pSnoopRtrPortBmp,
                                                                 pSnoopVlanEntry);
                        SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                       sizeof (tSnoopPortBmp));
                    }

                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            pSnoopPktInfo->u4InPort, u1Forward,
                                            pV2ReportBuf,
                                            pFwdPortBitmap,
                                            SNOOP_ASMREPORT_SENT);
                }

                if ((SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                     u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS) &&
                    u1Version == SNOOP_IGS_IGMP_VERSION1)
                {
                    u1V1ReportFwd = SNOOP_TRUE;
                }
                else if ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                    1].u1ReportFwdAll ==
                          SNOOP_FORWARD_NONEDGE_PORTS)
                         && u1Version == SNOOP_IGS_IGMP_VERSION1)
                {
                    /* if report forward is set to non-edge ports alone
                     * then check presence of non-edge ports */
                    SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                    if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                        pSnoopVlanEntry->VlanId,
                                                        pVlanPortBitmap) ==
                        SNOOP_SUCCESS)
                    {
                        SnoopUtilGetNonEdgePortList (u4Instance,
                                                     pVlanPortBitmap,
                                                     pFwdPortBitmap);
                        if (SNOOP_MEM_CMP
                            (pFwdPortBitmap, gNullPortBitMap,
                             sizeof (tSnoopPortBmp)) != 0)
                        {
                            u1V1ReportFwd = SNOOP_TRUE;
                        }
                        if (u1V1ReportFwd == SNOOP_TRUE)
                        {
                            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                            SNOOP_CONTROL_PATH_TRC,
                                            SNOOP_VLAN_TRC,
                                            "Reports are forwarded to non-edge port list for Vlan %d\n",
                                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                              VlanId));
                        }
                    }
                }
                else
                {
                    /* if report forward is set to router ports alone
                     * then check presence of router ports */
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION1,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    if (SNOOP_MEM_CMP
                        (pSnoopRtrPortBmp, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) != 0)
                    {
                        u1V1ReportFwd = SNOOP_TRUE;
                    }
                }

                /* If sparse mode is enabled, the report forwarding is done based on
                 * learnt source port information */

                if ((SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (u1V1ReportFwd == SNOOP_TRUE))
                {
                    if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                    {
                        u1V1ReportFwd = SNOOP_FALSE;
                    }
                }

                if (u1V1ReportFwd == SNOOP_TRUE)
                {
#ifdef IGS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                    {
                        u1PktType = SNOOP_IGMP_V1REPORT;
                        i4Result = IgsEncodeV1V2Report (u4Instance,
                                                        u1PktType,
                                                        SNOOP_PTR_FETCH_4
                                                        (pSnoopGroupEntry->
                                                         pConsGroupEntry->
                                                         GroupIpAddr.au1Addr),
                                                        &pV1ReportBuf);
                    }
#endif
#ifdef MLDS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    {
                        u1PktType = SNOOP_MLD_V1REPORT;
                        i4Result = MldsEncodeV1Report (u4Instance, u1PktType,
                                                       pSnoopGroupEntry->
                                                       pConsGroupEntry->
                                                       GroupIpAddr,
                                                       &pV1ReportBuf);
                    }
#endif

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC, "Unable to generate and send "
                                   "ASM report on to router ports\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }

                    u1Forward =
                        ((SNOOP_INSTANCE_INFO (u4Instance)->
                          SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                          u1ReportFwdAll ==
                          SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                         VLAN_FORWARD_SPECIFIC);

                    if (SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pSnoopVlanEntry->u1AddressType -
                                  1].u1ReportFwdAll == SNOOP_FORWARD_RTR_PORTS)
                    {
                        SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pSnoopVlanEntry->
                                                                 u1AddressType,
                                                                 SNOOP_IGS_IGMP_VERSION1,
                                                                 pSnoopRtrPortBmp,
                                                                 pSnoopVlanEntry);
                        SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                                       sizeof (tSnoopPortBmp));
                    }

                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            pSnoopPktInfo->u4InPort, u1Forward,
                                            pV1ReportBuf,
                                            pFwdPortBitmap,
                                            SNOOP_ASMREPORT_SENT);
                }
            }
        }
        /* Get the Next Group Record */
        u2OffSet = (UINT2) (u2OffSet + ((pSnoopVlanEntry->u1AddressType ==
                                         SNOOP_ADDR_TYPE_IPV4) ?
                                        (SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                         (u2NoSources *
                                          4)) : (SNOOP_MLD_SSMGROUP_RECD_SIZE +
                                                 (u2NoSources * 16))));
    }

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              pSnoopVlanEntry->
                                              u1AddressType - 1) == SNOOP_FALSE)
    {
        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                 SNOOP_OUTER_VLAN (VlanId),
                                                 pSnoopVlanEntry->u1AddressType,
                                                 0, pSnoopRtrPortBmp,
                                                 pSnoopVlanEntry);

        SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, PortBitmap);
    }

    /* If proxy is enabled and max length is not zero */
#ifdef ICCH_WANTED
    if (((u1Proxy == SNOOP_FALSE) && (u4ActMaxLength != 0))
        || (u1FirstReportOnMcLag == SNOOP_TRUE))
#else
    if ((u1Proxy == SNOOP_FALSE) && (u4ActMaxLength != 0))
#endif
    {
        u1ReportFwd = SNOOP_TRUE;

        /* If sparse mode is enabled, the report/leave forwarding is done based on
         * learnt source port information */
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            /* Check if the group entry is present or not */
            if (SnoopGrpGetGroupEntry
                (u4Instance, VlanId, pSnoopPktInfo->GroupAddr,
                 &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
                {
                    u1ReportFwd = SNOOP_FALSE;
                }
            }
        }
        if (u1ReportFwd == SNOOP_TRUE)
        {
            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_NONEDGE_PORTS)
            {
                SNOOP_MEM_SET (pVlanPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

                if (SnoopMiGetVlanLocalEgressPorts (u4Instance,
                                                    pSnoopVlanEntry->VlanId,
                                                    pVlanPortBitmap) ==
                    SNOOP_SUCCESS)
                {
                    SnoopUtilGetNonEdgePortList (u4Instance, pVlanPortBitmap,
                                                 pFwdPortBitmap);
                    if (SNOOP_MEM_CMP
                        (pFwdPortBitmap, gNullPortBitMap,
                         sizeof (tSnoopPortBmp)) == 0)
                    {
                        u1ReportFwd = SNOOP_FALSE;
                    }
                    if (u1ReportFwd == SNOOP_TRUE)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Report forwarded on non-edge port list for Vlan %d\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                }
            }
        }

        if (u1ReportFwd == SNOOP_TRUE)
        {
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                i4Result = IgsEncodeAggV3Report (u4Instance,
                                                 u2GrpRec,
                                                 u4ActMaxLength, &pV3ReportBuf);

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send V3 report on to"
                               " router ports\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;

                }

#ifdef ICCH_WANTED
                if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
                {
                    if (u1ReportModify == OSIX_TRUE)
                    {
                        /* In enhanced mode, the report should be learnt in 
                           both Master and Slave as same. So just copy the 
                           received buffer and modify the report to include
                           port-channel admin key, cvlan.
                         */

                        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) ==
                            SNOOP_ENABLE)
                        {
                            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                           SNOOP_MISC_MEMBLK_SIZE);
                            /* Copy only the information about Group records) */
                            CRU_BUF_Copy_FromBufChain (pBuf, gpSSMRepSendBuffer,
                                                       (UINT4)
                                                       (SNOOP_DATA_LINK_HDR_LEN
                                                        + u1TagLen +
                                                        pSnoopPktInfo->
                                                        u1IpHdrLen +
                                                        SNOOP_IGMP_HEADER_LEN),
                                                       (UINT4) (pSnoopPktInfo->
                                                                u2PktLen -
                                                                pSnoopPktInfo->
                                                                u1IpHdrLen));

                            u4ActMaxLength =
                                (UINT4) ((pSnoopPktInfo->u2PktLen -
                                          pSnoopPktInfo->u1IpHdrLen) +
                                         (u2GrpRec *
                                          (2 * SNOOP_AUX_DATA_SIZE)));
                        }

                        i4IcclResult = IgsEncodeAggV3ReportOnIccl (u4Instance,
                                                                   u2GrpRec,
                                                                   u4ActMaxLength,
                                                                   &pV3ReportBufOnIccl,
                                                                   u4PhyPort,
                                                                   &SnoopIcchInfo);

                        if (i4IcclResult != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_IGS_TRC_FLAG,
                                       SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                                       "Unable to generate and send V3 report on to"
                                       " router ports\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                            return SNOOP_FAILURE;

                        }
                    }
                }
#endif
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                                  u4ActMaxLength,
                                                  &pV3ReportBuf);

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send V3 report on to"
                               " router ports\r\n");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;

                }

#ifdef ICCH_WANTED
                if ((u1ReportModify == OSIX_TRUE)
                    && (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED))
                {
                    i4IcclResult = MldsEncodeAggV2ReportOnIccl
                        (u4Instance, u2GrpRec,
                         u4ActMaxLength, &pV3ReportBufOnIccl, u4PhyPort);

                    if (i4IcclResult != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC,
                                   "Unable to generate and send V3 report on to"
                                   " router ports\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                        return SNOOP_FAILURE;

                    }

                }
#endif
            }
#endif

            u1Forward =
                ((SNOOP_INSTANCE_INFO (u4Instance)->
                  SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                  u1ReportFwdAll ==
                  SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                 VLAN_FORWARD_SPECIFIC);

            if (SNOOP_INSTANCE_INFO (u4Instance)->
                SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                == SNOOP_FORWARD_RTR_PORTS)
            {
                SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (VlanId),
                                                         pSnoopVlanEntry->
                                                         u1AddressType,
                                                         SNOOP_IGS_IGMP_VERSION3,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopRtrPortBmp,
                               sizeof (tSnoopPortBmp));
            }

#ifdef ICCH_WANTED
            if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
            {
                SnoopIcchGetIcclIfIndex (&u4IcchPort);

                if (SnoopVcmGetContextInfoFromIfIndex (u4IcchPort,
                                                       &u4InstanceId,
                                                       &u2LocalIcchPort) !=
                    VCM_SUCCESS)
                {
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                    CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl, FALSE);
                    return SNOOP_FAILURE;
                }

                SNOOP_IS_PORT_PRESENT (u2LocalIcchPort, pFwdPortBitmap,
                                       bResult);

                if ((bResult == OSIX_TRUE) && (u1ReportModify == OSIX_TRUE))
                {
                    pIcclPortBitMap =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

                    if (pIcclPortBitMap == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_OS_RES_DBG,
                                   "Error in allocating memory for pIcclPortBitMap\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        CRU_BUF_Release_MsgBufChain (pV3ReportBufOnIccl, FALSE);
                        CRU_BUF_Release_MsgBufChain (pV3ReportBuf, FALSE);
                        return SNOOP_FAILURE;
                    }

                    SNOOP_MEM_SET (pIcclPortBitMap, SNOOP_INIT_VAL,
                                   sizeof (tSnoopPortBmp));

                    SNOOP_ADD_TO_PORT_LIST (u2LocalIcchPort, pIcclPortBitMap);

                    SnoopVlanForwardPacket (u4Instance, VlanId,
                                            pSnoopVlanEntry->u1AddressType,
                                            u4RcvPort, u1Forward,
                                            pV3ReportBufOnIccl,
                                            pIcclPortBitMap,
                                            SNOOP_SSMREPORT_SENT);

                    UtilPlstReleaseLocalPortList (pIcclPortBitMap);
                    SNOOP_DEL_FROM_PORT_LIST (u2LocalIcchPort, pFwdPortBitmap);
                }
            }
#endif

            SnoopVlanForwardPacket (u4Instance, VlanId,
                                    pSnoopVlanEntry->u1AddressType,
                                    u4RcvPort, u1Forward,
                                    pV3ReportBuf, pFwdPortBitmap,
                                    SNOOP_SSMREPORT_SENT);
        }
    }

    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

    if (u1DiscardReport == SNOOP_TRUE)
    {
        /*  The report is discarded. Set the pu1Forward flag to
         *  SNOOP_RELEASE_BUFFER so that the packet buffer
         *  will be freed outside this function
         */
        *pu1Forward = SNOOP_RELEASE_BUFFER;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessLeaveOrDoneMessage                       */
/*                                                                           */
/* Description        : This function processes the incoming IGMP leave or   */
/*                      MLD done message.                                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                      pSnoopVlanEntry - pointer to VLAN entry              */
/*                      pSnoopPktInfo - pointer to Packet information        */
/*                                                                           */
/* Output(s)          : pu1Forward - VLAN_FORWARD_ALL/                       */
/*                                   VLAN_FORWARD_SPECIFIC/                  */
/*                                   SNOOP_RELEASE_BUFFER                    */
/*                      OutPortBitmap - Port bitmap                          */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessLeaveOrDoneMessage (UINT4 u4Instance, tSnoopTag VlanId,
                                tSnoopVlanEntry * pSnoopVlanEntry,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                                UINT1 *pu1Forward, tSnoopPortBmp OutPortBitmap)
{
    UINT1              *pTempPortBitMap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pPortBitmap = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           tempSrcAddr;
    tCfaIfInfo          CfaIfInfo;
    tSnoopSrcBmp        SnoopExclBmp;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
#ifdef IGS_WANTED
    UINT4               u4GrpAddr = 0;
#endif
    INT4                i4Result = 0;
    UINT4               u4LearntPort = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
#ifdef ICCH_WANTED
    UINT4               u4PhyPort = 0;
    UINT2               u2AdminKey = 0;
    tSnoopIcchFillInfo  SnoopIcchInfo;
#endif
    UINT1               u1IsLastGroupEntry = SNOOP_FALSE;
    UINT1               u1LeaveProcMode = 0;
    UINT1               u1HostFound = SNOOP_FALSE;
    UINT1               u1SSMPortFound = SNOOP_FALSE;
    UINT1               u1Snooping = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bRes = OSIX_FALSE;
    BOOL1               bIcclInterface = OSIX_FALSE;
    UINT1               u1AsmReceiver = SNOOP_FALSE;
    UINT1               u1PortFlag = 0;
    UINT1               u1McLagInterface = OSIX_FALSE;
#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

#ifdef ICCH_WANTED
    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));
#endif

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessLeaveOrDoneMessage\r\n");

#ifdef ICCH_WANTED
    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, pSnoopPktInfo->u4InPort);
    if (SnoopLaGetMCLAGSystemStatus () == LA_ENABLED)
    {
        SnoopLaIsMclagInterface (u4PhyPort, &u1McLagInterface);
        bIcclInterface = SnoopIcchIsIcclInterface (u4PhyPort);
    }
#else
    UNUSED_PARAM (u1McLagInterface);
#endif

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                     SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopPktInfo->GroupAddr.u1Afi,
                                     SNOOP_FAILURE);

    /* Do not process the Leave message if the port (in which the leave 
     * is received) is a router port */

    /* Set the variable if both proxy and proxy reporting is disabled. */
    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              pSnoopVlanEntry->u1AddressType -
                                              1) == SNOOP_FALSE)
    {
        u1Snooping = SNOOP_TRUE;
    }

    /* Set the pu1Forward to VLAN_FORWARD_SPECIFIC for snooping mode so that
     * the leave message is forewarded.
     * If in proxy or proxy-reporting mode, set pu1Forward to 
     * SNOOP_RELEASE_BUFFER so that the packet buffer will be freed outside 
     * this function */

    *pu1Forward = (u1Snooping == SNOOP_TRUE) ? VLAN_FORWARD_SPECIFIC :
        SNOOP_RELEASE_BUFFER;
    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for pSnoopRtrPortBmp while processing leave or done message\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN (VlanId),
                                             pSnoopVlanEntry->u1AddressType, 0,
                                             pSnoopRtrPortBmp, pSnoopVlanEntry);
    SNOOP_MEM_CPY (OutPortBitmap, pSnoopRtrPortBmp, sizeof (tSnoopPortBmp));
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort, pSnoopRtrPortBmp, bResult);

#ifdef ICCH_WANTED
    if ((bResult == OSIX_TRUE) && (bIcclInterface == OSIX_FALSE))
#else
    UNUSED_PARAM (bIcclInterface);
    if (bResult == OSIX_TRUE)
#endif
    {
        if (SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) != SNOOP_ALL_REPORT_CONFIG)
        {
            if (SNOOP_PROXY_STATUS (u4Instance,
                                    pSnoopVlanEntry->u1AddressType - 1)
                == SNOOP_ENABLE)
            {
                /* In Proxy mode reports received on router ports
                 * will be dropped, so release the report msg buffer 
                 */
                *pu1Forward = SNOOP_RELEASE_BUFFER;
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_ALL_FAILURE,
                           SNOOP_PKT_TRC,
                           "Processing of leave message failed - incoming port is a"
                           " router port\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_SUCCESS;
            }
            else
            {
                *pu1Forward = VLAN_FORWARD_SPECIFIC;
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_PKT_TRC,
                           "Forwarding the leave message to other router ports - "
                           "incoming port is a router port\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_SUCCESS;
            }
        }
    }

    IPVX_ADDR_COPY (&GrpAddr, &(pSnoopPktInfo->GroupAddr));

    /* Get the group entry for the given group address */
    if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                               GrpAddr, &pSnoopGroupEntry) != SNOOP_SUCCESS)
    {
        SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, OutPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_PKT_TRC,
                   "Processing of leave message failed -Group entry not "
                   "present\n");
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

        }

        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }

    /* Check If the incoming port is not learnt as a member for the
     * given multicast group */
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopGroupEntry->PortBitmap, bResult);

    if (bResult == OSIX_FALSE)
    {
        if ((SNOOP_SYSTEM_REPORT_LEVEL (u4Instance) ==
             SNOOP_ALL_REPORT_CONFIG) &&
            (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
        {
            *pu1Forward = SNOOP_RELEASE_BUFFER;
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Processing of leave message failed - "
                   "Incoming port is not learnt as a member "
                   "in the given multicast group \r\n");
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                    pSnoopASMPortEntry, tSnoopPortEntry *)
    {
        if (pSnoopASMPortEntry->u4Port == pSnoopPktInfo->u4InPort)
        {
            break;
        }
    }

    if (pSnoopASMPortEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Processing of leave message failed - "
                   "Incoming port is not learnt as a member "
                   "in the given multicast group \r\n");
        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }

#ifdef ICCH_WANTED
    /* If Leave is received on MCLAG interface update the flag 
       and process it */
    if (u1McLagInterface == OSIX_TRUE)
    {
        pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_TRUE;
        SnoopLaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);

        SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, pSnoopGroupEntry->VlanId,
                       sizeof (tSnoopTag));
        SnoopIcchSyncLeaveMessage (u4Instance, GrpAddr, &SnoopIcchInfo,
                                   u2AdminKey);
    }
#endif

    /* Check the leave processing level to be used on this instance
     * and obtain the leave mode configured for the interface. */
    if (SNOOP_SYSTEM_LEAVE_LEVEL (u4Instance) == SNOOP_VLAN_LEAVE_CONFIG)
    {
        if (pSnoopVlanEntry->u1FastLeave == SNOOP_ENABLE)
        {
            u1LeaveProcMode = SNOOP_LEAVE_FAST_LEAVE;
        }
        else
        {
            u1LeaveProcMode = SNOOP_LEAVE_NORMAL_LEAVE;
        }
    }
    else
    {
        /* Obtain the leave mode from the port configuration entry */

        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL))
        {
            u1LeaveProcMode = pSnoopPortCfgEntry->
                pSnoopExtPortCfgEntry->u1SnoopPortLeaveMode;
        }
        else
        {
            u1LeaveProcMode = SNOOP_LEAVE_NORMAL_LEAVE;
        }
    }

    if (u1LeaveProcMode == SNOOP_LEAVE_EXPLICIT_TRACK)
    {
        /* Process the leave message by explicit host tracking 
         * verify if the host has already sent a join report for the 
         * group. If yes remove the host from host list.
         * If the host is the last host present, remove the port entry */

        SNOOP_SLL_SCAN (&pSnoopASMPortEntry->HostList,
                        pSnoopASMHostEntry, tSnoopHostEntry *)
        {
            if (IPVX_ADDR_COMPARE (pSnoopASMHostEntry->HostIpAddr,
                                   pSnoopPktInfo->SrcIpAddr) == 0)
            {
                u1HostFound = SNOOP_TRUE;
                break;
            }
        }
        if (u1HostFound == SNOOP_FALSE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Processing of leave message failed - "
                       "The host has not registered for the given"
                       " multicast group \r\n");
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            }
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
            return SNOOP_SUCCESS;
        }

        if (pSnoopASMHostEntry->SnoopHostType == SNOOP_ASM_V1HOST)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Processing of leave message failed - "
                       "The v1 host has erroneously sent an "
                       "invalid leave message\r\n");
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                      SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

            }
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
            return SNOOP_SUCCESS;

        }

        /* Host had previously sent a join message for the multicast group
         * Remove the host entry from the multicast group */
        SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList, pSnoopASMHostEntry);

        /* Remove the host from the Host Information RBTree */
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                      (tRBElem *) pSnoopASMHostEntry);

        SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopASMHostEntry);

        /* Check is the port has any other interested receivers */

        if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) == 0)
        {
            /* No more hosts are attached to the ASM port
             * stop the ASM port purge timer and
             * call Asm port purge timer expiry */

            if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
            }

            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&(pSnoopASMPortEntry->PurgeOrGrpQueryTimer));
            }

            if (SnoopTmrASMPortPurgeTimerExpiry (u4Instance,
                                                 pSnoopASMPortEntry) !=
                SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "Unable to delete port from group record\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_FAILURE;
            }
        }

        /* We have atleast one receiver present on the port. 
         * Hence set the pu1Forward flag to SNOOP_RELEASE_BUFFER so that the 
         * packet buffer will be freed outside this function and return */

        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        if (SnoopVlanUpdateStats
            (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC, "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }

    /* If fast leave is enabled on this vlan or on the interface
     * the port is immediately removed from the group entry and 
     * from the forwarding entries learnt for the group. All the
     * hosts learnt on the port are also removed. */
#ifdef ICCH_WANTED
    if ((u1LeaveProcMode == SNOOP_LEAVE_FAST_LEAVE)
        && (u1McLagInterface == OSIX_FALSE))
#else
    if (u1LeaveProcMode == SNOOP_LEAVE_FAST_LEAVE)
#endif
    {
        /* Stop the PortPurge/Leave timer if it is running */
        if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
        }

        /* Update the Group entry ASM port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                  pSnoopGroupEntry->ASMPortBitmap);

        /* Update the Group entry port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                  pSnoopGroupEntry->PortBitmap);

        /*Release the port node from the ASMPortList */
        SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList, pSnoopASMPortEntry);
        /* Stop the Asm Host Present Timer and
         * release all the hosts attached to the port */

        if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
        }

        if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
        {
            while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                    SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList)) != NULL)
            {
                SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                  pSnoopASMHostEntry);

                /* Remove the host from the Host Information RBTree */
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                              (tRBElem *) pSnoopASMHostEntry);

                SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                  pSnoopASMHostEntry);
            }
        }

        SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);

        /* Remove the SSM hosts attached on this port */
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopSSMPortEntry->u4Port == pSnoopPktInfo->u4InPort)
                {
                    u1SSMPortFound = SNOOP_TRUE;
                    break;
                }
            }
        }

        if (u1SSMPortFound == SNOOP_TRUE)
        {
            /* As an ASM port entry is present on the same port, 
             * the port purge timer will not be running for the 
             * ssm port entry */

            /*Release the port node from the SSMPortList */
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortEntry);

            /* Stop the Host Present Timer and
             * release all the hosts attached to the port */

            if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
            }

            if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
            {
                while ((pSnoopHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList,
                                      pSnoopHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopHostEntry);

                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopHostEntry);
                }
            }

            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortEntry);
        }

        /* Get the port configuration. */
        /* Get the port entry; If port entry is absent, no port-based action
         * will be taken for the packet.*/
        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
            (SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[pSnoopPktInfo->
                                                         GroupAddr.u1Afi -
                                                         1].u1FilterStatus !=
             SNOOP_DISABLE))
        {
            if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_GROUPS)
            {
                /* If the limit type set is for groups, decrement
                 * the current limit count */
                SNOOP_DECR_MEMBER_CNT (pSnoopPortCfgEntry->
                                       pSnoopExtPortCfgEntry);
            }
        }
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);

            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           pSnoopPktInfo->u4InPort,
                                           SNOOP_DEL_PORT, 0) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC,
                           "Updation of MAC forwarding table on reception "
                           "of leave messaga failed\r\n");
            }
        }
        else
        {
            if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                          tempSrcAddr, pSnoopPktInfo->u4InPort,
                                          gNullPortBitMap,
                                          SNOOP_DEL_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_FWD | SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_TRC,
                           "Updation of IP forwarding table on reception "
                           "of leave message failed\r\n");
            }
        }
        /* If there is atleast one receiver for the group just return 
         * success and set the pu1Forward to SNOOP_RELEASE_BUFFER and so
         * that the packet buffer will be freed */

        if (((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
              u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS)) &&
            ((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
              u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS)))
        {
            pTempPortBitMap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pTempPortBitMap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG,
                           "Error in allocating memory for pTempPortBitMap\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));
            SNOOP_MEM_CPY (pTempPortBitMap, pSnoopGroupEntry->PortBitmap,
                           sizeof (tSnoopPortBmp));
            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pSnoopVlanEntry->
                                                     u1AddressType, 0,
                                                     pSnoopRtrPortBmp,
                                                     pSnoopVlanEntry);

            SNOOP_DEL_FROM_PORT_LIST (pSnoopPktInfo->u4InPort, pTempPortBitMap);

            for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE;
                 u2BytIndex++)
            {
                if (u2BytIndex >= SNOOP_PORT_LIST_SIZE)
                {
                    continue;
                }
                u1PortFlag = pTempPortBitMap[u2BytIndex];
                for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {

                    if ((u1PortFlag & SNOOP_BIT8) != 0)
                    {
                        u4LearntPort =
                            (UINT4) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                        SNOOP_IS_PORT_PRESENT (u4LearntPort, pSnoopRtrPortBmp,
                                               bRes);
                        if (bRes == OSIX_TRUE)
                        {
                            u4RtrPortCount++;
                        }
                        else
                        {
                            u4LrtPortCount++;
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
            UtilPlstReleaseLocalPortList (pTempPortBitMap);

            if ((u4LrtPortCount > 0) || u4RtrPortCount > 1)
            {
                u1AsmReceiver = SNOOP_TRUE;
            }
        }
        else
        {
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                               SNOOP_PORT_LIST_SIZE) != 0)
            {
                u1AsmReceiver = SNOOP_TRUE;
            }
        }

        if (u1AsmReceiver == SNOOP_TRUE)
        {
            /* Check if any ASM receivers are there if present nothing needs 
             * to be done */
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                /* If the ASM port bitmap has become NULL set the exclude source
                 * bitmap for the group to default value and consolidate the 
                 * group source bitmaps */
                SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                               SNOOP_SRC_LIST_SIZE);

                SnoopGrpConsolidateSrcInfo (u4Instance, 0, 0, gNullSrcBmp,
                                            gNullSrcBmp, pSnoopGroupEntry,
                                            SNOOP_TRUE);

                /* After SnoopGrpConsolidateSrcInfo if the u1ASMHostPresent of 
                 * consolidated group is SNOOP_FALSE then send the report to 
                 * the router ports */
                if (pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent
                    != SNOOP_FALSE)
                {
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    if (SnoopVlanUpdateStats
                        (u4Instance, pSnoopVlanEntry,
                         SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                        SNOOP_VLAN_TRC,
                                        "Unable to update SNOOP "
                                        "statistics for VLAN ID %d\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                          VlanId));
                    }
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                    return SNOOP_SUCCESS;
                }

                if (u1Snooping == SNOOP_FALSE)
                {
                    /* Send a filter mode change record */
#ifdef IGS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                    {
                        i4Result = IgsEncodeQueryResponse
                            (u4Instance, pSnoopVlanEntry,
                             pSnoopGroupEntry->pConsGroupEntry, 0);
                    }
#endif

#ifdef MLDS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    {
                        i4Result = MldsEncodeQueryResponse
                            (u4Instance, pSnoopVlanEntry,
                             pSnoopGroupEntry->pConsGroupEntry, 0);
                    }
#endif

                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC,
                                   "Unable to send response to query\r\n");
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        if (SnoopVlanUpdateStats
                            (u4Instance, pSnoopVlanEntry,
                             SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                            SNOOP_DBG_VLAN |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_VLAN_TRC,
                                            "Unable to update SNOOP "
                                            "statistics for VLAN ID %d\r\n",
                                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                              VlanId));
                        }

                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                        return SNOOP_FAILURE;
                    }
                }
            }

            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            if (SnoopVlanUpdateStats
                (u4Instance, pSnoopVlanEntry,
                 SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_VLAN_TRC,
                                "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
            return SNOOP_SUCCESS;
        }

        /* If this is the only group present in the consolidated group 
         * entry then send a Leave message to the router ports */

        pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;

        if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 1)
        {
            u1IsLastGroupEntry = SNOOP_TRUE;
        }

        if (u1IsLastGroupEntry == SNOOP_TRUE && (u1Snooping == SNOOP_FALSE))
        {
            /* Form or forward the Leave message for this group */
#ifdef IGS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                i4Result = IgsEncodeAndSendLeaveMsg
                    (u4Instance, VlanId, pSnoopConsGroupEntry);
            }
#endif
#ifdef MLDS_WANTED
            if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                i4Result = MldsEncodeAndSendDoneMsg
                    (u4Instance, VlanId, pSnoopConsGroupEntry);
            }
#endif

            if (i4Result != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Unable to send Leave message for the vlan "
                                "%d\r\n", SNOOP_OUTER_VLAN (VlanId));
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                    SNOOP_VLAN_TRC, "Unable to update SNOOP "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_FAILURE;
            }
        }

        if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType ==
            SNOOP_INVALID_TIMER_TYPE)
        {
            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_TRC,
                           "Deletion of Group record failed\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_VLAN_TRC,
                                    "Unable to update SNOOP "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_FAILURE;
            }
        }

        if (u1IsLastGroupEntry == SNOOP_FALSE)
        {
            if (SnoopGrpGetConsGroupEntry (u4Instance, VlanId, GrpAddr,
                                           &pSnoopConsGroupEntry)
                == SNOOP_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                if (SnoopVlanUpdateStats
                    (u4Instance, pSnoopVlanEntry,
                     SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                    SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_VLAN_TRC,
                                    "Unable to update SNOOP "
                                    "statistics for VLAN ID %d\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
                return SNOOP_SUCCESS;
            }

            SnoopGrpConsolidateGroupInfo (u4Instance, pSnoopConsGroupEntry);

            if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
                && (u1Snooping == SNOOP_FALSE))
            {
                /* If there is some ASM host present then there is no 
                 * change in the consolidated bitmap. */
                if (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_FALSE)
                {
#ifdef IGS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                    {
                        i4Result = IgsEncodeQueryResponse
                            (u4Instance, pSnoopVlanEntry,
                             pSnoopConsGroupEntry, 0);
                    }
#endif

#ifdef MLDS_WANTED
                    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                    {
                        i4Result = MldsEncodeQueryResponse
                            (u4Instance, pSnoopVlanEntry,
                             pSnoopConsGroupEntry, 0);
                    }
#endif
                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC, "Unable to send the updation"
                                   " of consolidated bitmap \r\n");
                    }
                }
            }
        }

        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        if (SnoopVlanUpdateStats
            (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }
    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

    /* If the group query timer is running on the port on which the leave
     * message is received, do not process the Leave message */
#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        /* Check if the v1 host is present in the port, then
         * do not process the leave message.*/

        if (pSnoopASMPortEntry->u1V1HostPresent == SNOOP_TRUE)
        {
            /* Since a V1 host is present on this port, do not 
             * process the leave.*/
            if (SnoopVlanUpdateStats
                (u4Instance, pSnoopVlanEntry,
                 SNOOP_LEAVE_DROPPED) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_VLAN_TRC,
                                "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
            return SNOOP_SUCCESS;
        }
    }
#endif

    /* If the group query timer is running on the port on which the leave
     * message is received, do not process the Leave message */
    if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType
        == SNOOP_GRP_QUERY_TIMER)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Leave message already received\n");

        /* Set the pu1Forward flag to SNOOP_RELEASE_BUFFER so that the 
         * packet buffer will be freed outside this function */
        if (SnoopVlanUpdateStats
            (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_SUCCESS;
    }

    /* Stop the port purge timer and start the group query timer */
    if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
    }

    pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u4Instance = u4Instance;
    pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType =
        SNOOP_GRP_QUERY_TIMER;

    /* In snooping mode, foreward the leave. */

    /* Send a Group Specific Query message on the port on which
     * the Leave message was received */
#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u4GrpAddr = SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr);
        i4Result = IgsEncodeQuery (u4Instance, u4GrpAddr, SNOOP_GRP_QUERY,
                                   pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                   u1ConfigOperVer, &pOutBuf, pSnoopVlanEntry);
    }
#endif
#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result =
            MldsEncodeQuery (u4Instance, GrpAddr.au1Addr, SNOOP_GRP_QUERY,
                             SNOOP_MLD_VERSION2, &pOutBuf, pSnoopVlanEntry);
    }
#endif
    if (i4Result != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "Group Specific Query construct and send - failed\r\n");
        if (SnoopVlanUpdateStats
            (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC, "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_FAILURE;
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_GRP_TRC,
                   "Error in allocating memory for pPortBitmap\r\n");
        CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
        if (SnoopVlanUpdateStats
            (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_TRC,
                            "Unable to update SNOOP "
                            "statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort, pPortBitmap);

    if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        SnoopVlanTagFrame (pOutBuf, SNOOP_INNER_VLAN (VlanId),
                           SNOOP_VLAN_HIGHEST_PRIORITY);
    }

    if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE))
    {
        SnoopCfaGetIfInfo (pSnoopPktInfo->u4InPort, &CfaIfInfo);

        if (CfaIfInfo.u1BrgPortType == CFA_CUSTOMER_EDGE_PORT)
        {
            /* Outgoing port is CEP. The tag will be added
               in SnoopMiVlanSnoopFwdPacketOnPortList */
            SnoopVlanUntagFrame (pOutBuf);
        }
    }

    SnoopVlanForwardPacket (u4Instance, VlanId,
                            pSnoopVlanEntry->u1AddressType, SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, pPortBitmap,
                            SNOOP_GRP_QUERY_SENT);
    UtilPlstReleaseLocalPortList (pPortBitmap);

    SnoopTmrStartTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer,
                        SNOOP_INSTANCE_INFO (u4Instance)->
                        SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                        u4GrpQueryInt);

    /* Increment the RetryCount */
    pSnoopASMPortEntry->u1RetryCount++;
    if (SnoopVlanUpdateStats
        (u4Instance, pSnoopVlanEntry, SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                        SNOOP_VLAN_TRC, "Unable to update SNOOP "
                        "statistics for VLAN ID %d\r\n",
                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopProcessLeaveOrDoneMessage\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessDeletePortEvent                          */
/*                                                                           */
/* Description        : This function processes the incoming Port            */
/*                      Down/Delete event.                                   */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u4Port      - Port Number                            */
/*                      u1Action    - SNOOP_DEL_PORT/SNOOP_DISABLE_PORT      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessDeletePortEvent (UINT4 u4Instance, UINT4 u4Port, UINT1 u1Action)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tMacAddr            NullMacAddr;
    UINT4               u4PhyPort = 0;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    gu1SnoopPortAction = u1Action;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessDeletePortEvent\r\n");
    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_GRP_TRC,
                   "No Group entries found for this instance\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_VALIDATE_PORT_RET (u4Port, SNOOP_FAILURE);

    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, (UINT2) u4Port);
    /* Delete the port from SNOOP VLAN table */
    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;
        pRBElem = pRBNextElem;

        SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopVlanEntry->u1AddressType,
                                         SNOOP_FAILURE);
        if (SnoopMiIsVlanMemberPort (u4Instance, pSnoopVlanEntry->VlanId,
                                     (UINT2) u4PhyPort) == SNOOP_FALSE)
        {
            /* This port is not a member port of current vlan */
            continue;
        }

        if ((SNOOP_SNOOPING_STATUS (u4Instance,
                                    (pSnoopVlanEntry->u1AddressType - 1))
             == SNOOP_DISABLE) && (u1Action == SNOOP_DISABLE_PORT))
        {
            continue;
        }

        if ((pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE) &&
            (u1Action == SNOOP_DISABLE_PORT))
        {
            continue;
        }

        /* delete the port from the router port bitmap for this VLAN */
        if (SnoopVlanDeleteRtrPortEntry (u4Instance, pSnoopVlanEntry,
                                         u4Port, NullMacAddr,
                                         u1Action) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       " Delete port entry on port delete event failed\n");
        }

        /* Delete the port from the group membership information if present */
        if (SnoopGrpDeletePortEntry (u4Instance, pSnoopVlanEntry, u4Port,
                                     NullMacAddr) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_TRC,
                       " Delete port entry on port delete event failed\n");
            return SNOOP_FAILURE;
        }
    }

    /* update the global port arrays */
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessEnablePortEvent                          */
/*                                                                           */
/* Description        : This function processes the enable port event        */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance Number                        */
/*                      u4Port      - Port Number                            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessEnablePortEvent (UINT4 u4Instance, UINT4 u4Port)
{
    tSnoopIfPortList   *pSnpPortList = NULL;
    UINT1              *pBitmapVlan = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tMacAddr            NullMacAddr;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessEnablePortEvent\r\n");
    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_VLAN_TRC,
                   "No Vlan entries found for this instance\r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);
    pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pBitmapVlan == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pBitmapVlan\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for pBitmapVlan while processes the enable port event\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        if ((pSnoopVlanEntry->u1AddressType != SNOOP_ADDR_TYPE_IPV4) &&
            (pSnoopVlanEntry->u1AddressType != SNOOP_ADDR_TYPE_IPV6))
        {
            UtilPlstReleaseLocalPortList (pBitmapVlan);
            return SNOOP_FAILURE;
        }

        /* Do nothing if Snooping is disabled */
        if (SNOOP_SNOOPING_STATUS (u4Instance,
                                   (pSnoopVlanEntry->u1AddressType - 1))
            == SNOOP_DISABLE)
        {
            pRBElem = pRBNextElem;
            continue;
        }
        if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
        {
            pRBElem = pRBNextElem;
            continue;
        }
        /* Add the port into consolidated router port bitmap if the port was
         * already present in cfg static router port bitmap */
        SNOOP_IS_PORT_PRESENT (u4Port,
                               pSnoopVlanEntry->pSnoopVlanCfgEntry->
                               CfgStaticRtrPortBmp, bResult);
        if (bResult == OSIX_FALSE)
        {
            pRBElem = pRBNextElem;
            continue;
        }

        /* Add the port to consolidated router port bitmap */
        SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->RtrPortBitmap);

        SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopVlanEntry->StaticRtrPortBitmap);

        SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                        pRtrPortNode, tSnoopRtrPortEntry *)
        {
            if (pRtrPortNode->u4Port == u4Port)
            {
                break;
            }
        }

        if (pRtrPortNode != NULL)
        {
            /* Update the operating version of the router port as default version */
            pRtrPortNode->u1OperVer =
                (pSnoopVlanEntry->u1AddressType ==
                 SNOOP_ADDR_TYPE_IPV4 ? pSnoopVlanEntry->pSnoopVlanCfgEntry->
                 u1ConfigOperVer : SNOOP_MLD_VERSION2);

            if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION3)
            {
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->V3RtrPortBitmap);
            }
            else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION2)
            {
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->V2RtrPortBitmap);
            }
            else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION1)
            {
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->V1RtrPortBitmap);
            }

            if (pRtrPortNode->u1OperVer != pRtrPortNode->u1ConfigOperVer)

            {
                /* Update the operating version of the router port as configured version */
                SnoopUtilUpdateRtrPortOperVer (pRtrPortNode,
                                               pRtrPortNode->u1ConfigOperVer);
            }
        }

        /* Update the multicast forwarding table by adding the port */
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
            {
                pSnpPortList =
                    (tSnoopIfPortList *)
                    FsUtilAllocBitList (sizeof (tSnoopIfPortList));
                if (pSnpPortList == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_DBG,
                               "Error in allocating memory for pSnpPortList\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for pSnpPortList while processes the enable port event\r\n");
                    UtilPlstReleaseLocalPortList (pBitmapVlan);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortList));

                SNOOP_MEM_CPY (pBitmapVlan, &pSnoopVlanEntry->RtrPortBitmap,
                               SNOOP_PORT_LIST_SIZE);

                if (SnoopGetPhyPortBmp (u4Instance, pBitmapVlan, *pSnpPortList)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_DBG,
                               "Unable to get the physical port bitmap from the virtual"
                               " port bitmap\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                    UtilPlstReleaseLocalPortList (pBitmapVlan);
                    return SNOOP_FAILURE;
                }
                SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                               (pSnoopVlanEntry->VlanId),
                                               (tPortList *) (*pSnpPortList));
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
            }
            /* Add the port from MAC based multicast forwarding table */
            if (SnoopFwdUpdateRtrPortToMacFwdTable (u4Instance, u4Port,
                                                    pSnoopVlanEntry->VlanId,
                                                    pSnoopVlanEntry->
                                                    u1AddressType, NullMacAddr,
                                                    SNOOP_ADD_PORT) !=
                SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC,
                           "Additon of port to MAC forwarding entry failed\r\n");
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                return SNOOP_FAILURE;
            }
        }
        else
        {
            /* Forwarding Mode is IP based so update IP forwarding table */
            if (SnoopFwdUpdateRtrPortToIpFwdTable (u4Instance, u4Port,
                                                   pSnoopVlanEntry->VlanId,
                                                   pSnoopVlanEntry->
                                                   u1AddressType,
                                                   SNOOP_ADD_PORT) !=
                SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG,
                           SNOOP_DBG_FWD | SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_TRC,
                           "Additon of port to IP forwarding entry failed\r\n");
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                return SNOOP_FAILURE;
            }
        }
        pRBElem = pRBNextElem;
    }
    UtilPlstReleaseLocalPortList (pBitmapVlan);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessVlanUpdatePortsEvent                     */
/*                                                                           */
/* Description        : This function processes the vlan port event          */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance Number                        */
/*                      VlanId      - VLAN identifier                        */
/*                      McastAddr   - Multicast MAC address                  */
/*                      AddPortBitmap - Added port list                      */
/*                      DelPortBitmap - Deleted port list                    */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessVlanUpdatePortsEvent (UINT4 u4Instance, tSnoopTag VlanId,
                                  tMacAddr McastAddr,
                                  tSnoopPortBmp AddPortBitmap,
                                  tSnoopPortBmp DelPortBitmap)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bAddResult = OSIX_FALSE;
    BOOL1               bDelResult = OSIX_FALSE;
    UINT1               u1AddressType = 1;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    BOOL1               b1AddPort = OSIX_FALSE;

#ifdef ICCH_WANTED
    UINT4               u4IcclPort = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2LocalPort = 0;
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessVlanUpdatePortsEvent\r\n");
    /* Check whether snooping is disabled for this Vlan */
    for (u1AddressType = 1; u1AddressType <= SNOOP_MAX_PROTO; u1AddressType++)
    {
        if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                   u1AddressType,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            continue;
        }

        for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Port++)
        {
            SNOOP_IS_PORT_PRESENT (u4Port, AddPortBitmap, bAddResult);

            SNOOP_IS_PORT_PRESENT (u4Port, DelPortBitmap, bDelResult);

            if ((bAddResult == OSIX_FALSE) && (bDelResult == OSIX_FALSE))
            {
                continue;
            }

            /* Check if the port is present in the incoming forbidden ports 
             * AddPortList, then  delete this port from the Snoop VLAN router 
             * portList as well as group table. */
            if (bDelResult == OSIX_TRUE)
            {
                /* Remove the port from the router port information */
                if (SnoopVlanDeleteRtrPortEntry (u4Instance, pSnoopVlanEntry,
                                                 u4Port, McastAddr,
                                                 SNOOP_DEL_PORT)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_TRC,
                               "Delete port entry on forbidden "
                               "port event failed\r\n");
                    return SNOOP_FAILURE;
                }

                /* Update the group membership information for the removal of 
                 * forbidden port */
                if (SnoopGrpDeletePortEntry
                    (u4Instance, pSnoopVlanEntry, u4Port,
                     McastAddr) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG,
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_TRC,
                               "Delete port entry on forbidden "
                               "port event failed\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC, "Delete port entry on forbidden "
                           "port event done\r\n");
            }

            /* Check if the port is present in the incoming forbidden ports
             * DelPortList, then add the port into Snoop VLAN router portList
             * only when the port is configured statically in the SNOOP router 
             * table */

            if (bAddResult == OSIX_TRUE)
            {
                /* 
                 * If that port is present in the static router port bitmap 
                 * update the consolidated router port bitmap and update the 
                 * forwarding table for all the multicast entries in the vlan 
                 */

                SNOOP_IS_PORT_PRESENT (u4Port,
                                       pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                       CfgStaticRtrPortBmp, bResult);

                if (bResult == OSIX_FALSE)
                {
                    continue;
                }
                if (SNOOP_SLL_COUNT (&pSnoopVlanEntry->RtrPortList) != 0)
                {
                    SNOOP_SLL_SCAN (&pSnoopVlanEntry->RtrPortList,
                                    pSnoopRtrPortEntry, tSnoopRtrPortEntry *)
                    {
                        if (pSnoopRtrPortEntry->u4Port == u4Port)
                        {
                            b1AddPort = OSIX_TRUE;
                            break;
                        }
                    }
                }
                if (b1AddPort == OSIX_FALSE)
                {
                    SnoopVlanAddRemRtrPort (u4Instance, u4Port,
                                            pSnoopVlanEntry, SNOOP_ADD_PORT);
                }

                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopVlanEntry->StaticRtrPortBitmap);

                /* Update the multicast forwarding table by removing the port */
                if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {
                    /* Delete the port from MAC based multicast forwarding 
                     * table */
                    if (SnoopFwdUpdateRtrPortToMacFwdTable (u4Instance, u4Port,
                                                            pSnoopVlanEntry->
                                                            VlanId,
                                                            pSnoopVlanEntry->
                                                            u1AddressType,
                                                            McastAddr,
                                                            SNOOP_ADD_PORT) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC, "Addition of port from MAC"
                                   " forwarding entry failed\r\n");
                        return SNOOP_FAILURE;
                    }
                }
                else
                {
                    /* Forwarding Mode is IP based so update IP forwarding 
                     * table */
                    if (SnoopFwdUpdateRtrPortToIpFwdTable (u4Instance, u4Port,
                                                           pSnoopVlanEntry->
                                                           VlanId,
                                                           pSnoopVlanEntry->
                                                           u1AddressType,
                                                           SNOOP_ADD_PORT) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG,
                                   SNOOP_DBG_FWD | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_FWD_TRC,
                                   "Addition of port from IP"
                                   " forwarding entry failed\r\n");
                        return SNOOP_FAILURE;
                    }
                }
            }
        }
    }

#ifdef ICCH_WANTED
    /* This function is called in order to add ICCL interface
     * as router entry in the vlan */
    SnoopIcchGetIcclIfIndex (&u4IcclPort);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4IcclPort, &u4InstanceId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_GBL_NAME, "Instance ID not found\r\n");
        return SNOOP_FAILURE;
    }

    SnoopIcchUpdateRtrPort (u4Instance, VlanId, u2LocalPort);
#endif

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessTopoChangeEvent                          */
/*                                                                           */
/* Description        : This function processes the incoming event occured   */
/*                      due to the topology Change from Spanning tree and    */
/*                      sends general query on all non router ports          */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u4Port      - Port Number                            */
/*                      VlanList    - List of Vlans                          */
/*                      u1AddrType  - Address Type                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopProcessTopoChangeEvent (UINT4 u4Instance, UINT4 u4Port,
                             tSnoopVlanList VlanList, UINT1 u1AddrType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pNullVlanList = NULL;
    tSnoopTag           VlanId;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1VlanBmp = 0;
    INT4                i4RetVal = 0;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopProcessTopoChangeEvent\r\n");
    pNullVlanList = UtilVlanAllocVlanListSize (sizeof (tSnoopVlanList));
    if (pNullVlanList == NULL)
    {
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pNullVlanList, 0, sizeof (tSnoopVlanList));
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        UtilVlanReleaseVlanListSize (pNullVlanList);
        return SNOOP_FAILURE;
    }

    /*For the case of Topology Change occured on a port and VlanId = 0 */

    if (SNOOP_MEM_CMP (VlanList, pNullVlanList, sizeof (tSnoopVlanList)) == 0)
    {
        /*Scan the SnoopVlanEntry for each vlan and call the funcn 
         * to send query*/

        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

        while (pRBElem != NULL)
        {

            pRBNextElem =
                RBTreeGetNext
                (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry, pRBElem, NULL);

            pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

            if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC,
                                "Snooping is disabled"
                                "in the Vlan %d\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));

                pRBElem = pRBNextElem;
                continue;
            }

            i4RetVal =
                SnoopVlanSendGeneralQuery (u4Instance, u4Port, pSnoopVlanEntry);
            UNUSED_PARAM (i4RetVal);

            pRBElem = pRBNextElem;

        }
    }

    else
    {
        /*For the case of Topology Change occured on a port 
         * and  valid Vlan ID*/

        /*Scan for each Byte and for each SET bit indicates 
         * a valid Vlan ID*/

        for (u2BytIndex = 0; u2BytIndex < VLAN_LIST_SIZE; u2BytIndex++)
        {
            if (VlanList[u2BytIndex] != 0)
            {
                u1VlanBmp = VlanList[u2BytIndex];

                for (u2BitIndex = 0; ((u2BitIndex < VLAN_PORTS_PER_BYTE)
                                      && (u1VlanBmp != 0)); u2BitIndex++)
                {
                    if ((u1VlanBmp & VLAN_BIT8) != 0)
                    {
                        SNOOP_OUTER_VLAN (VlanId) = (UINT2)
                            ((u2BytIndex * VLAN_PORTS_PER_BYTE) +
                             u2BitIndex + 1);

                        if (SnoopMiIsVlanMemberPort (u4Instance, VlanId,
                                                     (UINT2) u4Port) !=
                            OSIX_TRUE)
                        {
                            u1VlanBmp = (UINT1) (u1VlanBmp << 1);
                            continue;
                        }

                        if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                                   u1AddrType, &pSnoopVlanEntry)
                            == SNOOP_FAILURE)
                        {
                            u1VlanBmp = (UINT1) (u1VlanBmp << 1);
                            continue;
                        }

                        if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
                        {
                            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG,
                                            SNOOP_CONTROL_PATH_TRC,
                                            SNOOP_VLAN_TRC,
                                            "Snooping is disabled"
                                            "for this Vlan %d\n",
                                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                              VlanId));
                            u1VlanBmp = (UINT1) (u1VlanBmp << 1);
                            continue;
                        }

                        i4RetVal =
                            SnoopVlanSendGeneralQuery (u4Instance, u4Port,
                                                       pSnoopVlanEntry);
                        UNUSED_PARAM (i4RetVal);

                    }
                    u1VlanBmp = (UINT1) (u1VlanBmp << 1);
                }
            }
        }
    }
    UtilVlanReleaseVlanListSize (pNullVlanList);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessMulticastDataOnIpMode                    */
/*                                                                           */
/* Description        : This function processes the incoming multicast       */
/*                      data packet when the multicast mode is IP.           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId     - Vlan Id                                 */
/*                      pSnoopInfo - Packet Information                      */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - multicast packet                        */
/*                                                                           */
/* Output(s)          : pu1FwdData - Flag indicated whether to forward the   */
/*                                   packet or Not.                          */
/*                      pu1IsUnreg - Flag indicated whether a registered     */
/*                                   Forwarding Entry is available or Not.   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessMulticastDataOnIpMode (UINT4 u4Instance, tSnoopTag VlanId,
                                   tSnoopVlanEntry * pSnoopVlanEntry,
                                   tSnoopPktInfo * pSnoopPktInfo,
                                   UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT1 *pu1FwdData, UINT1 *pu1IsUnReg)
{
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        if (SnoopProcessDoubleTaggedData
            (u4Instance, VlanId, pSnoopVlanEntry, pSnoopPktInfo, u4Port, pBuf,
             pu1FwdData, pu1IsUnReg) != SNOOP_SUCCESS)
        {
            return SNOOP_FAILURE;
        }
    }
    else
    {
        if (SnoopProcessSingleTaggedData
            (u4Instance, VlanId, pSnoopVlanEntry, pSnoopPktInfo, u4Port, pBuf,
             pu1FwdData, pu1IsUnReg) != SNOOP_SUCCESS)
        {
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessDoubleTaggedData                         */
/*                                                                           */
/* Description        : This function processes the incoming double tagged   */
/*                      multicast data packet when the multicast mode is IP. */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId     - Vlan Id                                 */
/*                      pSnoopInfo - Packet Information                      */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - multicast packet                        */
/*                                                                           */
/* Output(s)          : pu1FwdData - Flag indicated whether to forward the   */
/*                                   packet or Not.                          */
/*                      pu1IsUnreg - Flag indicated whether a registered     */
/*                                   Forwarding Entry is available or Not.   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessDoubleTaggedData (UINT4 u4Instance, tSnoopTag VlanId,
                              tSnoopVlanEntry * pSnoopVlanEntry,
                              tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port,
                              tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT1 *pu1FwdData, UINT1 *pu1IsUnReg)
{
    UINT1              *pFwdPortBitmap = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopTag           RtrVlanId;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1SGCreate = SNOOP_FALSE;
    UINT1               u1TempIsUnReg = SNOOP_TRUE;
    UINT1               u1DataForwarded = SNOOP_FALSE;
    tSnoopGroupEntry    SnoopGroupEntry;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    /* Forward the data on corresponding Outer, Inner VLAN entry */
    /* Check for (S,G) entry */
    if (SnoopFwdGetIpForwardingEntryForAllSrc (u4Instance, VlanId,
                                               pSnoopPktInfo->DestIpAddr,
                                               pSnoopPktInfo->SrcIpAddr,
                                               &pSnoopIpGrpFwdEntry) ==
        SNOOP_SUCCESS)
    {
        pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

        if (pDupBuf == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                   SnoopSysErrString
                                   [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                   "while processing multicast data packet when the multicast mode is IP\r\n");
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_FAILURE;
        }
        if (SnoopForwardDataonIpFwdEntry (u4Instance, VlanId,
                                          pSnoopIpGrpFwdEntry,
                                          u4Port, pDupBuf) != SNOOP_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_FAILURE;
        }
#ifdef NPAPI_WANTED
        CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
        u1DataForwarded = SNOOP_TRUE;
    }
    else
    {
        SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
        if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                                   pSnoopPktInfo->DestIpAddr,
                                   &pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            /* Group entry is not present for this data so needs to be 
             * forwarded to all the Outer-VLAN member ports. */
            *pu1IsUnReg = SNOOP_TRUE;
            *pu1FwdData = SNOOP_FORWARD;
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_SUCCESS;
        }

        if (SnoopCheckToCreateIpFwdEntry
            (u4Instance, pSnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
             &u1SGCreate, pu1IsUnReg) != SNOOP_SUCCESS)
        {
            if (*pu1IsUnReg == SNOOP_TRUE)
            {
                /* Data for unregistered source needs to be forwarded to 
                 * Outer-VLAN member ports */
                *pu1FwdData = SNOOP_FORWARD;

                /* In Enhanced mode , If double tagging data frame trap is not
                   available, create forwarding entry for all other
                   CVLAN's for this group entry learnt in SVLAN

                   Note: THIS SHOULD BE REMOVED WHEN H/W SUPPORTS
                   DOUBLE TAG IP MULTICAST DATA TRAP
                 */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
                {
                    SnoopUtilProcessDoubleTagDataForOuterVlan (u4Instance,
                                                               pSnoopPktInfo,
                                                               pSnoopGroupEntry);
                }

                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_SUCCESS;
            }
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_FAILURE;
        }

        if (u1SGCreate == SNOOP_TRUE)
        {
            /* Check if any V2 receivers are present */
            if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
            {
                u1ASMHostPresent = SNOOP_TRUE;
            }
            /* Get the list of ports on which receivers are attached for
             * this (S,G) entry */
            SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                      u1ASMHostPresent, pFwdPortBitmap);

            if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId)
                == SNOOP_INVALID_VLAN_ID)
            {
                /* Add the router port bitmap only when Inner VLAN is 0 */
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                        pFwdPortBitmap);
            }

            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

            if (pDupBuf == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                       SnoopSysErrString
                                       [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                       "while processing multicast data packet when the multicast mode is IP\r\n");
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_FAILURE;
            }

            if (SnoopCreateAndFwdDataonIpFwdEntry
                (u4Instance, VlanId, pSnoopPktInfo, pSnoopGroupEntry, u4Port,
                 pDupBuf, pFwdPortBitmap) != SNOOP_SUCCESS)
            {
                *pu1IsUnReg = SNOOP_TRUE;
                *pu1FwdData = SNOOP_FORWARD;
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_SUCCESS;
            }
#ifdef NPAPI_WANTED
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
        }
        else
        {
            *pu1IsUnReg = SNOOP_TRUE;
            *pu1FwdData = SNOOP_FORWARD;
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);

            /* In Enhanced mode , If double tagging data frame trap is not
               available, create forwarding entry for all other
               CVLAN's for this group entry learnt in SVLAN
               Note: THIS SHOULD BE REMOVED WHEN H/W SUPPORTS
               DOUBLE TAG IP MULTICAST DATA TRAP
             */

            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
            {
                SnoopUtilProcessDoubleTagDataForOuterVlan (u4Instance,
                                                           pSnoopPktInfo,
                                                           pSnoopGroupEntry);
            }

            return SNOOP_SUCCESS;
        }
    }

    /* In Enhanced mode the double tagged data packet should get forwarded 
     * to the router ports. Where as in case of basic mode inner vlan is   
     * always SNOOP_INVALID_VLAN_ID, so the router ports will present in the 
     * above entry itself */
    if ((SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID) &&
        (SNOOP_MEM_CMP (pSnoopVlanEntry->RtrPortBitmap, gNullPortBitMap,
                        SNOOP_PORT_LIST_SIZE) != 0))
    {
        SNOOP_OUTER_VLAN (RtrVlanId) = SNOOP_OUTER_VLAN (VlanId);
        SNOOP_INNER_VLAN (RtrVlanId) = SNOOP_INVALID_VLAN_ID;
        if (SnoopFwdGetIpForwardingEntry (u4Instance, RtrVlanId,
                                          pSnoopPktInfo->DestIpAddr,
                                          pSnoopPktInfo->SrcIpAddr,
                                          &pSnoopIpGrpFwdEntry)
            == SNOOP_SUCCESS)
        {
            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

            if (pDupBuf == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                       SnoopSysErrString
                                       [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                       "while processing multicast data packet when the multicast mode is IP\r\n");
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_FAILURE;
            }
            if (SnoopForwardDataonIpFwdEntry (u4Instance, VlanId,
                                              pSnoopIpGrpFwdEntry,
                                              u4Port, pDupBuf) != SNOOP_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_SUCCESS;
            }
#ifdef NPAPI_WANTED
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
        }
        else
        {
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
            if (SnoopGrpGetGroupEntry (u4Instance, RtrVlanId,
                                       pSnoopPktInfo->DestIpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                if (SnoopCheckToCreateIpFwdEntry
                    (u4Instance, pSnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
                     &u1SGCreate, &u1TempIsUnReg) == SNOOP_SUCCESS)
                {
                    if (u1SGCreate == SNOOP_TRUE)
                    {
                        /* Check if any V2 receivers are present */
                        if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) !=
                            0)
                        {
                            u1ASMHostPresent = SNOOP_TRUE;
                        }
                        /* Get the list of ports on which receivers are attached 
                         * for this (S,G) entry */
                        SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                                  u1ASMHostPresent,
                                                  pFwdPortBitmap);
                    }
                }
            }
            SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                    pFwdPortBitmap);

            SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

            SNOOP_OUTER_VLAN (SnoopGroupEntry.VlanId) =
                SNOOP_OUTER_VLAN (VlanId);
            SNOOP_MEM_CPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                           (UINT1 *) &(pSnoopPktInfo->DestIpAddr),
                           sizeof (tIPvXAddr));

            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

            if (pDupBuf == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                       SnoopSysErrString
                                       [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                       "while processing multicast data packet when the multicast mode is IP\r\n");
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_FAILURE;
            }
            if (SnoopCreateAndFwdDataonIpFwdEntry
                (u4Instance, VlanId, pSnoopPktInfo, &SnoopGroupEntry, u4Port,
                 pDupBuf, pFwdPortBitmap) != SNOOP_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_SUCCESS;
            }
#ifdef NPAPI_WANTED
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
            u1DataForwarded = SNOOP_TRUE;
        }
    }

    if (u1DataForwarded == SNOOP_TRUE)
    {
        *pu1FwdData = SNOOP_NO_FORWARD;
    }
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);

    /* In Enhanced mode , If double tagging data frame trap is not
       available, create forwarding entry for all other 
       CVLAN's for this group entry learnt in SVLAN.
       Note: THIS SHOULD BE REMOVED WHEN H/W SUPPORTS
       DOUBLE TAG IP MULTICAST DATA TRAP
     */

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SnoopUtilProcessDoubleTagDataForOuterVlan (u4Instance, pSnoopPktInfo,
                                                   pSnoopGroupEntry);
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopProcessSingleTaggedData                         */
/*                                                                           */
/* Description        : This function processes the incoming single tagged   */
/*                      multicast data packet when the multicast mode is IP. */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId     - Vlan Id                                 */
/*                      pSnoopInfo - Packet Information                      */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - multicast packet                        */
/*                                                                           */
/* Output(s)          : pu1FwdData - Flag indicated whether to forward the   */
/*                                   packet or Not.                          */
/*                      pu1IsUnreg - Flag indicated whether a registered     */
/*                                   Forwarding Entry is available or Not.   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopProcessSingleTaggedData (UINT4 u4Instance, tSnoopTag VlanId,
                              tSnoopVlanEntry * pSnoopVlanEntry,
                              tSnoopPktInfo * pSnoopPktInfo, UINT4 u4Port,
                              tCRU_BUF_CHAIN_HEADER * pBuf,
                              UINT1 *pu1FwdData, UINT1 *pu1IsUnReg)
{
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pSnpGrpConsPortBmp = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopTag           RtrVlanId;
    tSnoopTag           SnpVlanId;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    INT4                i4VlanCnt = 0;
    INT4                i4RetVal = 0;
    UINT4               u4IpFwdNotExistCnt = 0;
    UINT4               u4Count = 0;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1SGCreate = SNOOP_FALSE;
    UINT1               u1TempIsUnReg = SNOOP_TRUE;
    UINT1               u1RtrEntryExist = SNOOP_FALSE;
    UINT1               u1DataForwarded = SNOOP_FALSE;
    UINT1               u1EntryFound = SNOOP_FALSE;
    BOOL1               bResult = SNOOP_FALSE;
    tSnoopGroupEntry    SnoopTmpGroupEntry;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    SNOOP_MEM_SET (SnpVlanId, 0, sizeof (tSnoopTag));

    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               " Snoop PVLAN LIST failed while processing single tagged multicast data packet when the multicast mode is IP\r\n");
        return SNOOP_FAILURE;
    }
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* scanning is done for the associated vlans in the PVLAN domain and
     * also for the incoming vlan identifier */
    for (i4VlanCnt = SNOOP_ZERO;
         i4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans; i4VlanCnt++)
    {
        if (i4VlanCnt == SNOOP_ZERO)
        {
            SNOOP_MEM_CPY (SnpVlanId, VlanId, sizeof (tSnoopTag));
        }
        else
        {
            SNOOP_OUTER_VLAN (SnpVlanId) =
                L2PvlanMappingInfo.pMappedVlans[i4VlanCnt - 1];
            SNOOP_INNER_VLAN (SnpVlanId) = SNOOP_INVALID_VLAN_ID;
        }

        if (SnoopGrpGetConsGroupEntry (u4Instance, SnpVlanId,
                                       pSnoopPktInfo->DestIpAddr,
                                       &pSnoopConsGroupEntry) != SNOOP_SUCCESS)
        {
            if (L2PvlanMappingInfo.u2NumMappedVlans == 0)
            {
                *pu1IsUnReg = SNOOP_TRUE;
                *pu1FwdData = SNOOP_FORWARD;
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                return SNOOP_SUCCESS;
            }
            else
            {
                continue;
            }
        }
        break;
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    if (pSnoopConsGroupEntry == NULL)
    {
        *pu1IsUnReg = SNOOP_TRUE;
        *pu1FwdData = SNOOP_FORWARD;
        return SNOOP_SUCCESS;
    }
    pSnpGrpConsPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnpGrpConsPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pSnpGrpConsPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while processing single tagged multicast data packet\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pSnpGrpConsPortBmp, 0, sizeof (tSnoopPortBmp));

    SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                               pSnoopGroupEntry, pSnoopTempGroupEntry,
                               tSnoopGroupEntry *)
    {
        u1EntryFound = SNOOP_TRUE;
#ifdef PIM_WANTED
        /* Check if PIM is enabled on the interface. If yes, and if the 
         * interface is DR, then PIM takes care of programming the IPMC
         * table. 
         * If the interface is Non - DR, then IGS needs to handle this.
         * IGS will check if the interface is NON-DR, and in such a case,
         * it will check if an entry is present in it's RB Tree.
         * If yes, possibilities are the following -
         * - IGS has s/w entry and PIM has changed from DR to Non DR hence
         * MC data is trapped to CPU, or,
         * - IGS has programmed s/w and h/w, but the h/w entry cranked, 
         * packet comes to CPU.
         * Either case, the entry can be re-programmed in the H/W.
         */
        if (SnoopUtilGetPIMInterfaceStatus (VlanId) != SNOOP_SUCCESS)
        {
            if (SnoopFwdGetIpForwardingEntry
                (u4Instance, pSnoopGroupEntry->VlanId,
                 pSnoopPktInfo->DestIpAddr, pSnoopPktInfo->SrcIpAddr,
                 &pSnoopIpGrpFwdEntry) == SNOOP_SUCCESS)
            {
                /* Delete node in IPMC tree */
                SnoopUtilCleanIpForwardingEntry (u4Instance,
                                                 pSnoopIpGrpFwdEntry);

                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
            }
        }
#endif
        /* Forward the data on corresponding Outer, Inner VLAN entry */
        /* Check for (S,G) entry */
        SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0, sizeof (tSnoopGroupEntry));

        SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                       sizeof (tSnoopGroupEntry));
        SNOOP_MEM_CPY (SnoopTmpGroupEntry.VlanId, pSnoopGroupEntry->VlanId,
                       sizeof (tSnoopTag));

        i4RetVal =
            SnoopFwdGetIpForwardingEntryForAllSrc (u4Instance,
                                                   SnoopTmpGroupEntry.VlanId,
                                                   pSnoopPktInfo->DestIpAddr,
                                                   pSnoopPktInfo->SrcIpAddr,
                                                   &pSnoopIpGrpFwdEntry);

        /* Checking for Static case when the Source Ip is zero */
        if ((i4RetVal != SNOOP_SUCCESS)
            && (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_STATIC))
        {

            SNOOP_MEM_SET (pSnoopPktInfo->SrcIpAddr.au1Addr, 0,
                           sizeof (IPVX_MAX_INET_ADDR_LEN));

            i4RetVal =
                SnoopFwdGetIpForwardingEntry (u4Instance,
                                              SnoopTmpGroupEntry.VlanId,
                                              pSnoopPktInfo->DestIpAddr,
                                              pSnoopPktInfo->SrcIpAddr,
                                              &pSnoopIpGrpFwdEntry);
        }

        if (i4RetVal == SNOOP_SUCCESS)
        {
            /* Check whether for this group entry data is needed
               from same source. If Yes add the port is forwarding 
               database 
             */
            SNOOP_IS_SOURCE_PRESENT (u4Instance, pSnoopPktInfo->SrcIpAddr,
                                     bResult, u2SrcIndex);
            if (bResult == SNOOP_TRUE)
            {
                if (u2SrcIndex != 0xffff)
                {
                    u2SrcIndex++;
                }

                /* Check if any V2 receivers are present */
                if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
                {
                    u1ASMHostPresent = SNOOP_TRUE;
                }

                if (pSnoopIpGrpFwdEntry != NULL)
                {
                    pFwdPortBitmap =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

                    if (pFwdPortBitmap == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_OS_RES_DBG,
                                   "Error in allocating memory for pFwdPortBitmap\r\n");
                        UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                        return SNOOP_FAILURE;
                    }

                    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

                    SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                              u1ASMHostPresent, pFwdPortBitmap);
                }
            }

            if (pSnoopIpGrpFwdEntry != NULL)
            {
                u1RtrEntryExist = SNOOP_TRUE;

                if (bResult == SNOOP_TRUE)
                {
                    SNOOP_UPDATE_PORT_LIST (pSnoopIpGrpFwdEntry->PortBitmap,
                                            pFwdPortBitmap);

                    /* Add the port to (S, G) entry */

                    if (SNOOP_MEM_CMP
                        (pSnoopIpGrpFwdEntry->PortBitmap, pFwdPortBitmap,
                         SNOOP_PORT_LIST_SIZE) != SNOOP_FALSE)
                    {
                        if (SnoopFwdUpdateIpFwdTable
                            (u4Instance, &SnoopTmpGroupEntry,
                             pSnoopIpGrpFwdEntry->SrcIpAddr, 0, pFwdPortBitmap,
                             SNOOP_ADD_PORTLIST) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_VLAN_TRC,
                                       "Unable to Create the IP forwarding entry in "
                                       "hardware\r\n");
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            return SNOOP_FAILURE;

                        }
                    }

                    bResult = SNOOP_FALSE;
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                }

                pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                if (pDupBuf == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_OS_RES_DBG,
                               "CRU Buffer allocation failed");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                                           SNOOP_BUFFER_TRC,
                                           SnoopSysErrString
                                           [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                           "while processing single tagged multicast data packet\r\n");
                    UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                    return SNOOP_FAILURE;
                }

                if (SnoopForwardDataonIpFwdEntry (u4Instance, VlanId,
                                                  pSnoopIpGrpFwdEntry,
                                                  u4Port,
                                                  pDupBuf) != SNOOP_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                    UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                    return SNOOP_FAILURE;
                }
#ifdef NPAPI_WANTED
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
                u1DataForwarded = SNOOP_TRUE;
            }
        }
        else
        {
            u4IpFwdNotExistCnt++;
            pFwdPortBitmap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pFwdPortBitmap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG,
                           "Error in allocating memory for pFwdPortBitmap\r\n");
                UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                return SNOOP_FAILURE;
            }
            MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

            if (SnoopCheckToCreateIpFwdEntry
                (u4Instance, pSnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
                 &u1SGCreate, pu1IsUnReg) != SNOOP_SUCCESS)
            {
                if (*pu1IsUnReg == SNOOP_TRUE)
                {
                    /* Data for unregistered source needs to be forwarded to 
                     * Outer-VLAN member ports */
                    *pu1FwdData = SNOOP_FORWARD;
                    UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    return SNOOP_SUCCESS;
                }
                u4Count++;
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                continue;
            }

            if (u1SGCreate == SNOOP_TRUE)
            {
                /* Check if any V2 receivers are present */
                if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
                {
                    u1ASMHostPresent = SNOOP_TRUE;
                }
                /* Get the list of ports on which receivers are attached for
                 * this (S,G) entry */
                SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                          u1ASMHostPresent, pFwdPortBitmap);

                SnoopUtilGetPortBmpFromPVlanMapInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pSnoopPktInfo->DestIpAddr,
                                                     pSnoopPktInfo->SrcIpAddr,
                                                     pSnpGrpConsPortBmp);

                SNOOP_UPDATE_PORT_LIST (pSnpGrpConsPortBmp, pFwdPortBitmap);

                if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId)
                    == SNOOP_INVALID_VLAN_ID)
                {
                    pSnoopRtrPortBmp =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pSnoopRtrPortBmp == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_OS_RES_DBG,
                                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                        UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        return SNOOP_FAILURE;
                    }
                    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopGroupEntry->
                                                             GroupIpAddr.u1Afi,
                                                             0,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    /* Add the router port bitmap only when Inner VLAN is 0 */
                    SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                }

                pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                if (pDupBuf == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_OS_RES_DBG,
                               "CRU Buffer allocation failed");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                                           SNOOP_BUFFER_TRC,
                                           SnoopSysErrString
                                           [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                           "while processing single tagged multicast data packet\r\n");
                    UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    return SNOOP_FAILURE;
                }

                if (SnoopCreateAndFwdDataonIpFwdEntry
                    (u4Instance, VlanId, pSnoopPktInfo, &SnoopTmpGroupEntry,
                     u4Port, pDupBuf, pFwdPortBitmap) != SNOOP_SUCCESS)
                {
                    CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    continue;
                }
                else
                {
                    if (SNOOP_INNER_VLAN (SnoopTmpGroupEntry.VlanId) ==
                        SNOOP_INVALID_VLAN_ID)
                    {
                        u1RtrEntryExist = SNOOP_TRUE;
                    }
                }
#ifdef NPAPI_WANTED
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
                u1DataForwarded = SNOOP_TRUE;
            }
            else
            {
                u4Count++;
            }
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        }
    }

    /* In Enhanced mode the data needs to be forwarded to router port if the 
     * entry was not created in the above DLL_SCAN. */
    if ((u1RtrEntryExist == SNOOP_FALSE) && (u1DataForwarded == SNOOP_TRUE) &&
        (SNOOP_MEM_CMP (pSnoopVlanEntry->RtrPortBitmap, gNullPortBitMap,
                        SNOOP_PORT_LIST_SIZE) != 0))
    {
        SNOOP_OUTER_VLAN (RtrVlanId) = SNOOP_OUTER_VLAN (VlanId);
        SNOOP_INNER_VLAN (RtrVlanId) = SNOOP_INVALID_VLAN_ID;
        if (SnoopFwdGetIpForwardingEntry (u4Instance, RtrVlanId,
                                          pSnoopPktInfo->DestIpAddr,
                                          pSnoopPktInfo->SrcIpAddr,
                                          &pSnoopIpGrpFwdEntry)
            == SNOOP_SUCCESS)
        {
            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

            if (pDupBuf == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                       SnoopSysErrString
                                       [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                       "while processing single tagged multicast data packet\r\n");
                UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                return SNOOP_FAILURE;
            }
            if (SnoopForwardDataonIpFwdEntry (u4Instance, VlanId,
                                              pSnoopIpGrpFwdEntry,
                                              u4Port, pDupBuf) != SNOOP_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
            }
#ifdef NPAPI_WANTED
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
        }
        else
        {
            pFwdPortBitmap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pFwdPortBitmap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG,
                           "Error in allocating memory for pFwdPortBitmap\r\n");
                UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                return SNOOP_FAILURE;
            }
            MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
            if (SnoopGrpGetGroupEntry (u4Instance, RtrVlanId,
                                       pSnoopPktInfo->DestIpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                if (SnoopCheckToCreateIpFwdEntry
                    (u4Instance, pSnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
                     &u1SGCreate, &u1TempIsUnReg) == SNOOP_SUCCESS)
                {
                    if (u1SGCreate == SNOOP_TRUE)
                    {
                        /* Check if any V2 receivers are present */
                        if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) !=
                            0)
                        {
                            u1ASMHostPresent = SNOOP_TRUE;
                        }
                        /* Get the list of ports on which receivers are attached 
                         * for this (S,G) entry */
                        SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                                  u1ASMHostPresent,
                                                  pFwdPortBitmap);
                    }
                }
            }

            SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                    pFwdPortBitmap);

            SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0, sizeof (tSnoopGroupEntry));

            SNOOP_OUTER_VLAN (SnoopTmpGroupEntry.VlanId) =
                SNOOP_OUTER_VLAN (VlanId);
            SNOOP_MEM_CPY ((UINT1 *) &(SnoopTmpGroupEntry.GroupIpAddr),
                           (UINT1 *) &(pSnoopPktInfo->DestIpAddr),
                           sizeof (tIPvXAddr));

            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

            if (pDupBuf == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG, "CRU Buffer allocation failed");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_BUFF, SNOOP_BUFFER_TRC,
                                       SnoopSysErrString
                                       [SYS_LOG_CRU_BUFF_ALLOC_FAIL],
                                       "while processing single tagged multicast data packet\r\n");
                UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                return SNOOP_FAILURE;
            }

            if (SnoopCreateAndFwdDataonIpFwdEntry
                (u4Instance, VlanId, pSnoopPktInfo, &SnoopTmpGroupEntry, u4Port,
                 pDupBuf, pFwdPortBitmap) != SNOOP_SUCCESS)
            {
                CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
            }
#ifdef NPAPI_WANTED
            CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
#endif
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        }
    }

    UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);
    if (u1DataForwarded == SNOOP_FALSE)
    {
        *pu1IsUnReg = SNOOP_TRUE;
    }

    /* The Source was present in the exclude bitmap of all the group entries,
     * So the packet should get dropped  or the data is already forwarded
     * to some/all of the group entries */

    if ((u1EntryFound == SNOOP_TRUE) && ((u4Count == u4IpFwdNotExistCnt) ||
                                         (u1DataForwarded == SNOOP_TRUE)))
    {
        *pu1FwdData = SNOOP_NO_FORWARD;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopForwardDataonIpFwdEntry                         */
/*                                                                           */
/* Description        : This function forwards the multicast data and restart*/
/*                      the appropriate timer for the given Ip Fwd entry     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      VlanId              - VLAN Identifier                */
/*                      pSnoopIpGrpFwdEntry - Pointer to IP FWD entry        */
/*                      u4Port              - Incoming port number           */
/*                      pBuf                - multicast packet               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopForwardDataonIpFwdEntry (UINT4 u4Instance, tSnoopTag VlanId,
                              tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry,
                              UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf)
{
#ifdef NPAPI_WANTED
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pBuf);
#endif

    /* Restart forward entry timer */
    if (&pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
    }
    pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4Instance;
    pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType = SNOOP_IP_FWD_ENTRY_TIMER;

    /* If entry is present in database, but failed in Hardware 
     * then do not start the timer as Hardware failed
     * case should be handled first.
     */
    if (pSnoopIpGrpFwdEntry->u1AddGroupStatus != HW_ADD_GROUP_FAIL)
    {
        if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                                SNOOP_IP_FWD_ENTRY_TIMER_VAL) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_TMR_DBG,
                       "Unable to start IP forwarding entry timer \r\n");
            return SNOOP_FAILURE;
        }
    }

#ifndef NPAPI_WANTED
    if ((SNOOP_INNER_VLAN (VlanId) == SNOOP_INVALID_VLAN_ID) &&
        (SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId) !=
         SNOOP_INVALID_VLAN_ID))
    {
        SnoopVlanTagFrame (pBuf, SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId),
                           SNOOP_VLAN_HIGHEST_PRIORITY);
    }

    SnoopVlanForwardPacket (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                            pSnoopIpGrpFwdEntry->u1AddressType, u4Port,
                            VLAN_FORWARD_SPECIFIC, pBuf,
                            pSnoopIpGrpFwdEntry->PortBitmap, 0);
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopCreateAndFwdDataonIpFwdEntry                    */
/*                                                                           */
/* Description        : This function creates the Ip FWD entry and forwards  */
/*                      the multicast data.                                  */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Id                       */
/*                      VlanId           - VLAN Identifier                   */
/*                      pSnoopPktInfo    - Pointer to Snoop packet info      */
/*                      pSnoopGroupEntry - Pointer to IP FWD entry           */
/*                      u4Port           - Incoming port number              */
/*                      pBuf             - multicast packet                  */
/*                      FwdPortBitmap    - Ports to forward the data         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopCreateAndFwdDataonIpFwdEntry (UINT4 u4Instance, tSnoopTag VlanId,
                                   tSnoopPktInfo * pSnoopPktInfo,
                                   tSnoopGroupEntry * pSnoopGroupEntry,
                                   UINT4 u4Port, tCRU_BUF_CHAIN_HEADER * pBuf,
                                   tSnoopPortBmp FwdPortBitmap)
{
#ifdef NPAPI_WANTED
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pBuf);
#endif
    /* Create a new (S, G) entry */
    if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                  pSnoopPktInfo->SrcIpAddr,
                                  pSnoopPktInfo->u4InPort,
                                  FwdPortBitmap,
                                  SNOOP_CREATE_FWD_ENTRY) != SNOOP_SUCCESS)
    {
        return SNOOP_FAILURE;
    }

#ifdef L2RED_WANTED
    SnoopActiveRedGrpSync (u4Instance, pSnoopPktInfo->SrcIpAddr,
                           pSnoopGroupEntry, FwdPortBitmap);
#endif

#ifndef NPAPI_WANTED
    if ((SNOOP_INNER_VLAN (VlanId) == SNOOP_INVALID_VLAN_ID) &&
        (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) != SNOOP_INVALID_VLAN_ID))
    {
        SnoopVlanTagFrame (pBuf, SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId),
                           SNOOP_VLAN_HIGHEST_PRIORITY);
    }

    SnoopVlanForwardPacket (u4Instance, pSnoopGroupEntry->VlanId,
                            pSnoopPktInfo->GroupAddr.u1Afi, u4Port,
                            VLAN_FORWARD_SPECIFIC, pBuf, FwdPortBitmap, 0);
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopCheckToCreateIpFwdEntry                         */
/*                                                                           */
/* Description        : This function check whether to add an IP Fwd entry   */
/*                      for the given source address.                        */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Id                       */
/*                      pSnoopPktInfo    - Packet Information                */
/*                      pSnoopGroupEntry - Group entry pointer               */
/*                                                                           */
/* Output(s)          : pu1SGEntry       - Flag which indicate whether to    */
/*                                         create IP FWD entry or not.       */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopCheckToCreateIpFwdEntry (UINT4 u4Instance, tSnoopPktInfo * pSnoopPktInfo,
                              tSnoopGroupEntry * pSnoopGroupEntry,
                              UINT2 *pu2SrcIndex, UINT1 *pu1SGEntry,
                              UINT1 *pu1IsUnReg)
{
    UINT2               u2SrcIndex = 0;
    BOOL1               bResult = OSIX_FALSE;
    INT4                i4RetVal = 0;
    tIPvXAddr           SrcAddr;
    *pu2SrcIndex = 0;
    *pu1SGEntry = SNOOP_FALSE;
    *pu1IsUnReg = SNOOP_TRUE;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    /* In control p[lane driven mode, if the (*,G) entry is already available. 
     * don't create (S,G) entry for this group.
     */
    if (SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_ENABLE)
    {
        i4RetVal =
            SnoopFwdGetIpForwardingEntry (u4Instance,
                                          pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr,
                                          SrcAddr, &pSnoopIpGrpFwdEntry);
        if (SNOOP_SUCCESS == i4RetVal)
        {
            *pu1SGEntry = SNOOP_FALSE;
            return SNOOP_SUCCESS;
        }
    }

    /* Check if source is present */
    SNOOP_IS_SOURCE_PRESENT (u4Instance, pSnoopPktInfo->SrcIpAddr,
                             bResult, u2SrcIndex);

    if (u2SrcIndex != 0xffff)
    {
        u2SrcIndex++;
    }

    /* If the filter mode is include and if the source is present in
     * the include bitmap create (S,G) entry */
    if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
    {
        OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->InclSrcBmp,
                                 u2SrcIndex, SNOOP_SRC_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {
            *pu1SGEntry = SNOOP_TRUE;
        }
    }
    else
    {
        /* If filter mode is exclude and source is not present and there
         * are v1/v2 receivers create an (S, G) entry */
        if (u2SrcIndex == 0xffff)
        {
            *pu1SGEntry = SNOOP_TRUE;
        }
        else
        {
            /* filter mode is exclude  */
            OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->InclSrcBmp, u2SrcIndex,
                                     SNOOP_SRC_LIST_SIZE, bResult);

            /* If the bit is set in include bitmap create an 
             * (S,G) entry */
            if (bResult == OSIX_TRUE)
            {
                *pu1SGEntry = SNOOP_TRUE;
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->ExclSrcBmp,
                                         u2SrcIndex,
                                         SNOOP_SRC_LIST_SIZE, bResult);
                *pu1SGEntry = SNOOP_TRUE;
            }
        }
    }
    *pu2SrcIndex = u2SrcIndex;
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleTransitionToNonQuerier                    */
/*                                                                           */
/* Description        : This function handles transition from querier to     */
/*                      non-querier                                          */
/*                                                                           */
/* Input(s)           :  u4Instance - Instance ID                            */
/*                       pSnoopVlanEntry - VLAN information                  */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  None                                 */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopHandleTransitionToNonQuerier (UINT4 u4Instance,
                                   tSnoopVlanEntry * pSnoopVlanEntry)
{
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleTransitionToNonQuerier\r\n");
    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        if ((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier ==
             SNOOP_QUERIER))
        {
            /* If configured as querier and acting as Non-Querier
               startup count should be reset to max . i.e . Election has taken place and
               Switch is moving to Non-querier */
            pSnoopVlanEntry->u1StartUpQCount =
                pSnoopVlanEntry->u1MaxStartUpQCount;
        }
        else if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier ==
                 SNOOP_NON_QUERIER)
        {
            /* Make startup query count to zero */
            pSnoopVlanEntry->u1StartUpQCount = 0;
            /* We have to stop the Other Querier Present timer. */
            if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "Stopped Other Querier present timer \r\n");
            }

        }
    }

    /* Congigure the Querier status as Non-Querier */
    pSnoopVlanEntry->u1Querier = SNOOP_NON_QUERIER;

    /* We have to stop the periodic Query timer. */
    if (pSnoopVlanEntry->QueryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
                   "Stopped Query Timer \r\n");
    }
#ifdef L2RED_WANTED
    SnoopRedSyncStartUpQryCount (u4Instance, pSnoopVlanEntry);
#endif
    SnoopRedActiveSendQuerierSync (u4Instance, pSnoopVlanEntry->VlanId,
                                   pSnoopVlanEntry->u1Querier,
                                   pSnoopVlanEntry->u1AddressType,
                                   pSnoopVlanEntry->u4ElectedQuerier);
}

/*****************************************************************************/
/* Function Name      : SnoopHandleTransitionToQuerier                     */
/*                                                                           */
/* Description        : This function handles transition from non-querier    */
/*                      to querier                                           */
/*                                                                           */
/* Input(s)           :  u4Instance - Instance ID                            */
/*                       pSnoopVlanEntry - VLAN information                  */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  None                                                  */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopHandleTransitionToQuerier (UINT4 u4Instance,
                                tSnoopVlanEntry * pSnoopVlanEntry)
{
    UINT2               u2QueryInterval = 0;
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleTransitionToQuerier\r\n");
    /* Congigure the Querier status as Querier */
    pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;

    /* We have to stop the Other Querier Present timer. */
    if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
                   "Stopped Other Querier present timer \r\n");
    }

    /* Restart the query interval */
    pSnoopVlanEntry->QueryTimer.u4Instance = u4Instance;
    pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;

    if (pSnoopVlanEntry->u1StartUpQCount >= pSnoopVlanEntry->u1MaxStartUpQCount)
    {
        u2QueryInterval = pSnoopVlanEntry->u2QueryInterval;
    }
    else
    {
        u2QueryInterval = pSnoopVlanEntry->u2StartUpQInterval;
    }

    SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer, u2QueryInterval);
    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
                    "Query timer restarted for %d seconds here\r\n",
                    u2QueryInterval);

    SnoopRedActiveSendQuerierSync (u4Instance, pSnoopVlanEntry->VlanId,
                                   pSnoopVlanEntry->u1Querier,
                                   pSnoopVlanEntry->u1AddressType,
                                   pSnoopVlanEntry->u4ElectedQuerier);
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      :  IgsProcNPCallBack                                   */
/*                                                                           */
/* Description        : This function processes the recieved msg node and    */
/*                      and calls the appropriate failure handlers           */
/*                                                                           */
/* Input(s)           : pMsgNode - the message interface structure           */
/*                                                                           */
/* Output(s)          : none                                                 */
/*                                                                           */
/* Return Value(s)    : none                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
IgsProcNPCallback (tSnoopMsgNode * pMsgNode)
{
    INT4                i4NpRetVal = 0;
    UINT4               u4Instance = 0;
    tPortList           PortList;
    tPortList           UntagPortList;
    UINT1               u1EventType = 0;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
    tSnoopTag           VlanId;

    gu4SnoopTrcInstId = pMsgNode->uInMsg.IpmcEntry.u4Instance;
    gu4SnoopDbgInstId = pMsgNode->uInMsg.IpmcEntry.u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "Inside NP callback function\r\n");

    SNOOP_MEM_SET (&PortList, 0, sizeof (tPortList));
    SNOOP_MEM_SET (&UntagPortList, 0, sizeof (tPortList));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    u4Instance = pMsgNode->uInMsg.IpmcEntry.u4Instance;
    u1EventType = pMsgNode->uInMsg.IpmcEntry.u1EventType;
    SNOOP_OUTER_VLAN (VlanId) = pMsgNode->uInMsg.IpmcEntry.OuterVlanId;
    SNOOP_INNER_VLAN (VlanId) = pMsgNode->uInMsg.IpmcEntry.InnerVlanId;
    i4NpRetVal = pMsgNode->uInMsg.IpmcEntry.rval;

    IPVX_ADDR_INIT_IPV4 (GrpAddr, SNOOP_ADDR_TYPE_IPV4,
                         (UINT1 *) &(pMsgNode->uInMsg.IpmcEntry.u4GrpAddr));
    IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                         (UINT1 *) &(pMsgNode->uInMsg.IpmcEntry.u4SrcAddr));

    MEMCPY (PortList, pMsgNode->uInMsg.IpmcEntry.PortList, sizeof (tPortList));
    MEMCPY (UntagPortList, pMsgNode->uInMsg.IpmcEntry.UntagPortList,
            sizeof (tPortList));

    u1EventType = pMsgNode->uInMsg.IpmcEntry.u1EventType;

    switch (u1EventType)
    {
        case SNOOP_HW_CREATE_ENTRY:
            if (i4NpRetVal == FNP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "NP has returned success so no action is required\r\n");
                return SNOOP_SUCCESS;
            }
            else
            {
                SnoopHandleGroupCreateFailure (VlanId, GrpAddr, SrcAddr,
                                               u4Instance);
            }
            break;

        case SNOOP_HW_DELETE_ENTRY:
            if (i4NpRetVal == FNP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "NP has returned success so no action is required\r\n");
                return SNOOP_SUCCESS;
            }
            else
            {
                SnoopHandleGroupDeleteFailure (VlanId, GrpAddr, SrcAddr,
                                               u4Instance);
            }
            break;

        case SNOOP_HW_ADD_PORT:
            if (i4NpRetVal == FNP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "NP has returned success so no action is required\r\n");
                return SNOOP_SUCCESS;
            }
            else
            {
                SnoopHandleAddPortFailure (VlanId, GrpAddr, SrcAddr,
                                           u4Instance);
            }
            break;

        case SNOOP_HW_DEL_PORT:
            if (i4NpRetVal == FNP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC,
                           "NP has returned success so no action is required\r\n");
                return SNOOP_SUCCESS;
            }
            else
            {
                SnoopHandleDeletePortFailure (VlanId, GrpAddr, SrcAddr,
                                              u4Instance);
            }
            break;
        default:
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "Invalid event received\r\n");

    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleGroupCreateFailure                        */
/*                                                                           */
/* Description        : This function handles the Create Group failure       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                       GrpAddr - The multicast grp address                 */
/*                       SrcAddr - the source address                        */
/*                       u4Instance - the instance id                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  SNOOP_SUCCESS/SNOOP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopHandleGroupCreateFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                               tIPvXAddr SrcAddr, UINT4 u4Instance)
{
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleGroupCreateFailure\r\n");
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "Inside NP callback for handling create failure\r\n");

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "NP has returned failed so failure handling is required\r\n");

    IgsNpFailNotify (SNOOP_HARDWARE_CREATE, VlanId, GrpAddr, SrcAddr,
                     u4Instance);
    IgsNpFailLog (SNOOP_HARDWARE_CREATE, GrpAddr, SrcAddr);
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));

    IpGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                   (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & IpGrpFwdEntry);

    if (pRBElem == NULL)
    {
        return SNOOP_FAILURE;
    }
    else
    {
        pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
    }

    if (&pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
    }
    pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4Instance;
    pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType = SNOOP_IP_FWD_ENTRY_TIMER;

    pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_FAIL;
    /*we are restarting the timer with a lower value in order to trigger
     * the removal of the entry fom the software by calling the
     * IpFwdPurgeTimer handler*/
    if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                            SNOOP_IP_FWD_ENTRY_TIMER_LOW_VAL) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Unable to start IP forwarding entry timer \r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleGroupDeleteFailure                        */
/*                                                                           */
/* Description        : This function handles the Create Group failure       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                       GrpAddr - The multicast grp address                 */
/*                       SrcAddr - the source address                        */
/*                       u4Instance - the instance id                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  SNOOP_SUCCESS/SNOOP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopHandleGroupDeleteFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                               tIPvXAddr SrcAddr, UINT4 u4Instance)
{
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    UINT4               u4Status = 0;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleGroupDeleteFailure\r\n");
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "Inside NP callback for handling delete failure\r\n");

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "NP returned failure so handling is required\r\n");

    IgsNpFailLog (SNOOP_HARDWARE_DELETE, GrpAddr, SrcAddr);
    IgsNpFailNotify (SNOOP_HARDWARE_DELETE, VlanId, GrpAddr, SrcAddr,
                     u4Instance);
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));

    IpGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                   (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & IpGrpFwdEntry);

    if (pRBElem == NULL)
    {
        if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry)
            == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_OS_RES_DBG,
                       "Memory allocation for IP forward entry \r\n");
            SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "while Create Group failure\r\n");
            return SNOOP_FAILURE;
        }

        SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

        /* Update the fields of the IP entry */
        pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_SUCCESS;
        pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;

        SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId, VlanId, sizeof (tSnoopTag));

        pSnoopIpGrpFwdEntry->u1AddressType = SNOOP_ADDR_TYPE_IPV4;
        SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                       (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));
        SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                       (UINT1 *) &(GrpAddr), sizeof (tIPvXAddr));
        SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                       SNOOP_PORT_LIST_SIZE);

        u4Status =
            RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                       (tRBElem *) pSnoopIpGrpFwdEntry);

        if (u4Status == RB_FAILURE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                       "RBTree Addition Failed for IP forward Entry \r\n");

            return SNOOP_FAILURE;
        }
        pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
        if ((pSnoopInstInfo != NULL) && (pSnoopIpGrpFwdEntry != NULL))
        {
            pSnoopInfo =
                &(pSnoopInstInfo->
                  SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
            if (pSnoopInfo != NULL)
            {
                pSnoopInfo->u4FwdGroupsCnt += 1;
            }
        }

        pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4Instance;
        pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType = SNOOP_IP_FWD_ENTRY_TIMER;

        if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                                SNOOP_IP_FWD_ENTRY_TIMER_LOW_VAL)
            != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                       "Unable to start IP forwarding entry timer \r\n");
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                          (tRBElem *) pSnoopIpGrpFwdEntry);
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if (pSnoopInstInfo != NULL)
            {
                pSnoopInfo =
                    &(pSnoopInstInfo->
                      SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
                if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
                {
                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                }
            }

            SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
            return SNOOP_FAILURE;
        }

    }
    else
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
                   "Entry is present so no need to create the entry again\r\n");
        return SNOOP_SUCCESS;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleAddPortFailure                            */
/*                                                                           */
/* Description        : This function handles the Create Group failure       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                       SrcAddr - the source address                        */
/*                       u4Instance - the instance id                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  SNOOP_SUCCESS/SNOOP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopHandleAddPortFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                           tIPvXAddr SrcAddr, UINT4 u4Instance)
{
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleAddPortFailure\r\n");
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "Inside NP callback for handling port add failure\r\n");

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "NP returned failure so we need to handle the failure\r\n");

    IgsNpFailLog (SNOOP_HARDWARE_PORT_ADD, GrpAddr, SrcAddr);
    IgsNpFailNotify (SNOOP_HARDWARE_PORT_ADD, VlanId, GrpAddr, SrcAddr,
                     u4Instance);

    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));

    IpGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                   (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));
    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & IpGrpFwdEntry);

    if (pRBElem == NULL)
    {
        return SNOOP_FAILURE;
    }
    else
    {
        pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
    }

    pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_FAIL;

    if (&pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
    }
    pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4Instance;
    pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType = SNOOP_IP_FWD_ENTRY_TIMER;

    if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                            SNOOP_IP_FWD_ENTRY_TIMER_LOW_VAL) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Unable to start IP forwarding entry timer \r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleAddPortFailure                            */
/*                                                                           */
/* Description        : This function handles the Create Group failure       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                       SrcAddr - the source address                        */
/*                       u4Instance - the instance id                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  SNOOP_SUCCESS/SNOOP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopHandleDeletePortFailure (tSnoopTag VlanId, tIPvXAddr GrpAddr,
                              tIPvXAddr SrcAddr, UINT4 u4Instance)
{
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopHandleDeletePortFailure\r\n");
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "Inside NP callback for handling port delete failure\r\n");

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
               "NP returned failure but we are not handling the failures for port delete\r\n");
    IgsNpFailLog (SNOOP_HARDWARE_PORT_DELETE, GrpAddr, SrcAddr);
    IgsNpFailNotify (SNOOP_HARDWARE_PORT_DELETE, VlanId, GrpAddr, SrcAddr,
                     u4Instance);
    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : IgsNpFailLog                                         */
/*                                                                           */
/* Description        : This function logs the failure to the syslog module  */
/*                                                                           */
/* Input(s)           :  u1EventType - The NP call which has failed          */
/*                       GrpAddr - the multicast group address               */
/*                       SrcAddr - the source address                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  SNOOP_SUCCESS/SNOOP_FAILURE                         */
/*                                                                           */
/*****************************************************************************/

VOID
IgsNpFailLog (UINT1 u1EventType, tIPvXAddr GrpAddr, tIPvXAddr SrcAddr)
{
#ifdef SYSLOG_WANTED
    UINT1               au1IgsNpCall[4][25]
        = { "Hardware Create", "Hardware Delete", "Hardware Port Add",
        "Hardware Port Delete"
    };
    UINT1               au1IgsNpCallSta[20] = "NPAPI failed for";
    UINT1               au1IgsNpSysLogMsg[SNOOP_SYSLOG_MSG_LENGTH];
    CHR1               *pu1GrpAddr = NULL;
    CHR1               *pu1SrcAddr = NULL;
    CHR1                au1GrpAddr[20];

    SNOOP_CONVERT_IPADDR_TO_STR (pu1GrpAddr,
                                 SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
    STRNCPY (au1GrpAddr, pu1GrpAddr, STRLEN (pu1GrpAddr));
    au1GrpAddr[STRLEN (pu1GrpAddr)] = '\0';

    SNOOP_CONVERT_IPADDR_TO_STR (pu1SrcAddr,
                                 SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr));

    SPRINTF ((CHR1 *) au1IgsNpSysLogMsg, "%s %s %s %s %s %s\n",
             au1IgsNpCall[u1EventType], au1IgsNpCallSta, "Group: ", au1GrpAddr,
             "Source: ", pu1SrcAddr);

    /* Send the Log Message to SysLog */
    SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, gu4IgsSysLogId,
                  "[NP-FAULT] %s", (CHR1 *) au1IgsNpSysLogMsg));
    SNOOP_DBG_ARG1 (SNOOP_IGS_DBG_FLAG, SNOOP_NP_DBG, SNOOP_NP_DBG,
                    "[NP-FAULT] %s", (CHR1 *) au1IgsNpSysLogMsg);

#else /* NOT SYSLOG_WANTED */
    UNUSED_PARAM (u1EventType);
    UNUSED_PARAM (GrpAddr);
    UNUSED_PARAM (SrcAddr);
#endif /* SYSLOG_WANTED */

    return;
}

/*****************************************************************************/
/* Function Name      : IgsNpFailNotify                                      */
/*                                                                           */
/* Description        : This function handles the Create Group failure       */
/*                                                                           */
/* Input(s)           :                                                      */
/*                       u1EventType - The NP call which has failed          */
/*                       VlanId -  The structure containing the Vlan ids     */
/*                       GrpAddr - The multicast grp address                 */
/*                       SrcAddr - the source address                        */
/*                       u4Instance - the instance id                        */
/*                                                                           */
/* Output(s)          :  none                                                */
/*                                                                           */
/* Return Value(s)    :  none                                                */
/*                                                                           */
/*****************************************************************************/

VOID
IgsNpFailNotify (UINT1 u1EventType, tSnoopTag VlanId, tIPvXAddr GrpAddr,
                 tIPvXAddr SrcAddr, UINT4 u4Instance)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    tSNMP_OID_TYPE      IgsNpNotifyPrefix;
    tSNMP_OID_TYPE      IgsNpNotifySuffix;
    tSNMP_OID_TYPE      IgsNpNotify;
    tSNMP_OID_TYPE      IgsTrapHwErrTypePrefix;
    tSNMP_OID_TYPE      IgsTrapHwErrTypeSuffix;
    tSNMP_OID_TYPE      IgsTrapHwErrType;
    UINT4               au4IgsNpNotifyPrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4IgsNpNotifySuffix[] = { 105, 7, 0 };
    UINT4               au4IgsNpNotify[SNMP_MAX_OID_LENGTH];
    UINT4               au4IgsTrapHwErrTypePrefix[] = { 1, 3, 6, 1, 4, 1 };
    UINT4               au4IgsTrapHwErrTypeSuffix[] =
        { 105, 6, 5, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    UINT4               au4IgsTrapHwErrType[SNMP_MAX_OID_LENGTH];

    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               u4ArrLen = 0;

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    IgsNpNotifyPrefix.pu4_OidList = au4IgsNpNotifyPrefix;
    IgsNpNotifyPrefix.u4_Length =
        sizeof (au4IgsNpNotifyPrefix) / sizeof (UINT4);

    IgsNpNotifySuffix.pu4_OidList = au4IgsNpNotifySuffix;
    IgsNpNotifySuffix.u4_Length =
        sizeof (au4IgsNpNotifySuffix) / sizeof (UINT4);

    MEMSET (au4IgsNpNotify, 0, sizeof (au4IgsNpNotify));

    IgsNpNotify.pu4_OidList = au4IgsNpNotify;
    IgsNpNotify.u4_Length = 0;

    if (SNMPAddEnterpriseOid (&IgsNpNotifyPrefix, &IgsNpNotifySuffix,
                              &IgsNpNotify) == OSIX_FAILURE)
    {
        return;
    }

    IgsTrapHwErrTypePrefix.pu4_OidList = au4IgsTrapHwErrTypePrefix;
    IgsTrapHwErrTypePrefix.u4_Length =
        sizeof (au4IgsTrapHwErrTypePrefix) / sizeof (UINT4);

    IgsTrapHwErrTypeSuffix.pu4_OidList = au4IgsTrapHwErrTypeSuffix;
    IgsTrapHwErrTypeSuffix.u4_Length =
        sizeof (au4IgsTrapHwErrTypeSuffix) / sizeof (UINT4);

    MEMSET (au4IgsTrapHwErrType, 0, sizeof (au4IgsTrapHwErrType));

    IgsTrapHwErrType.pu4_OidList = au4IgsTrapHwErrType;
    IgsTrapHwErrType.u4_Length = 0;

    if (SNMPAddEnterpriseOid (&IgsTrapHwErrTypePrefix, &IgsTrapHwErrTypeSuffix,
                              &IgsTrapHwErrType) == OSIX_FAILURE)
    {
        return;
    }

    /* Trap OID construction. Telling Manager that you have received
     * trap for IGS NP Fail Notifications */
    pOid = alloc_oid (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return;
    }
    MEMCPY (pOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pOid->u4_Length = sizeof (au4SnmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4IgsNpNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return;
    }
    MEMCPY (pOidValue->pu4_OidList, IgsNpNotify.pu4_OidList,
            IgsNpNotify.u4_Length * sizeof (UINT4));
    pOidValue->u4_Length = IgsNpNotify.u4_Length;

    pVbList = (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID, 0L, 0, NULL,
                              pOidValue, SnmpCnt64Type);
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return;
    }

    pStartVb = pVbList;

    /* Trap Oid Construction for NP Call Id which has failed */
    u4ArrLen = sizeof (au4IgsTrapHwErrType) / sizeof (UINT4);
    pOid = alloc_oid ((INT4) u4ArrLen);
    if (pOid == NULL)
    {
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 1] =
        SNOOP_INNER_VLAN (VlanId);
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 2] = GrpAddr.au1Addr[3];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 3] = GrpAddr.au1Addr[2];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 4] = GrpAddr.au1Addr[1];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 5] = GrpAddr.au1Addr[0];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 6] = SrcAddr.au1Addr[3];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 7] = SrcAddr.au1Addr[2];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 8] = SrcAddr.au1Addr[1];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 9] = SrcAddr.au1Addr[0];
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 10] = SrcAddr.u1Afi;
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 11] =
        SNOOP_OUTER_VLAN (VlanId);
    au4IgsTrapHwErrType[IgsTrapHwErrType.u4_Length - 12] = u4Instance;

    MEMCPY (pOid->pu4_OidList, IgsTrapHwErrType.pu4_OidList,
            IgsTrapHwErrType.u4_Length * sizeof (UINT4));
    pOid->u4_Length = IgsTrapHwErrType.u4_Length;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *)
         SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32,
                               0, (INT4) u1EventType,
                               NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

#else /* NOT SNMP_3_WANTED */
    UNUSED_PARAM (u1EventType);
    UNUSED_PARAM (GrpAddr);
    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
#endif /* SNMP_3_WANTED */

    return;
}
#endif
/*****************************************************************************/
/* Function Name      : SnpProcessIpAddrChangeEvent                          */
/*                                                                           */
/* Description        : This function processes the incoming event occured   */
/*                      due to the IP Addr Change in Default Vlan interface  */
/*                      it starts querier re-election process.               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnpProcessIpAddrChangeEvent (UINT4 u4Instance, UINT4 u4VlanIp, UINT2 u2VlanId,
                             UINT1 u1AddrType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopTag           VlanId;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    /*Scan the SnoopVlanEntry for each vlan and start the querier re-election
     * process */

    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    SNOOP_OUTER_VLAN (VlanId) = (tVlanId) u2VlanId;

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               u1AddrType, &pSnoopVlanEntry) == SNOOP_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_QRY_DBG, "Unable to get the vlan entry \n");
        return SNOOP_FAILURE;
    }

    if (pSnoopVlanEntry->u4InterfaceIp != u4VlanIp)
    {
        /*Update the Interface IP Address */
        pSnoopVlanEntry->u4InterfaceIp = u4VlanIp;

        if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_VLAN_TRC, "Snooping is disabled"
                            "in the Vlan %d\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            return SNOOP_SUCCESS;
        }

        if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
        {
            /* We have to stop the periodic Query timer. */
            if (pSnoopVlanEntry->QueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_TMR_TRC, "Stopped Query Timer \r\n");
            }
            /* Make startup query count to zero */
            pSnoopVlanEntry->u1StartUpQCount = 0;
        }

        if (pSnoopVlanEntry->u1StartUpQCount == 0)
        {
            if (SnoopVlanSendGeneralQuery (u4Instance, SNOOP_INVALID_PORT,
                                           pSnoopVlanEntry) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_QRY_DBG,
                           "Unable to send General Query message on IP "
                           "Addr Change\n");
            }
            pSnoopVlanEntry->u1StartUpQCount++;
        }
        SnoopHandleTransitionToQuerier (u4Instance, pSnoopVlanEntry);
    }

    return SNOOP_SUCCESS;
}
