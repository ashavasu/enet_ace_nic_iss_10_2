/***********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: snpicch.c,v 1.28 2017/11/17 12:09:58 siva Exp $
 *
 * Description     : This file contains SNOOPING ICCH related routines
 *
 ************************************************************************/
/*****************************************************************************/
/*  FILE NAME             : snpicch.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : SNOOP Module                                     */
/*  MODULE NAME           : SNOOP                                            */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE :                                                  */
/*  AUTHOR                : Aricent Inc.                                     */
/*****************************************************************************/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopIcchRegisterWithICCH                            */
/*                                                                           */
/* Description        : Registers Snooping module with ICCH to receive update*/
/*                      messages.                                            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then SNOOP_SUCCESS        */
/*                      Otherwise SNOOP_FAILURE                               */
/*****************************************************************************/
INT4
SnoopIcchRegisterWithICCH (VOID)
{
    tIcchRegParams      IcchRegParams;

    SNOOP_MEM_SET (&IcchRegParams, SNOOP_INIT_VAL, sizeof (tIcchRegParams));

    IcchRegParams.u4EntId = ICCH_SNOOP_APP_ID;
    IcchRegParams.pFnRcvPkt = SnoopIcchHandleUpdateEvents;

    if (SnoopIcchRegisterProtocols (&IcchRegParams) == ICCH_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC,
                       "SnpRegisterWithIcch: Registration with ICCH FAILED\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchHandleUpdateEvents                           */
/*                                                                           */
/* Description        : This function will be invoked by the ICCH module to  */
/*                      pass events  to Snooping module                      */
/*                                                                           */
/* Input(s)           : u1Event - Event given by ICCH module.                */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : ICCH_SUCCESS - If the message is enq'ed successfully */
/*                      ICCH_FAILURE otherwise                               */
/*****************************************************************************/
INT4
SnoopIcchHandleUpdateEvents (UINT1 u1Event, tIcchMsg * pData, UINT2 u2DataLen)
{
    tSnoopMsgNode      *pSnoopMsgNode = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = SNOOP_DEFAULT_INSTANCE;
    gu4SnoopDbgInstId = SNOOP_DEFAULT_INSTANCE;

    if ((u1Event != ICCH_PEER_DOWN) &&
        (u1Event != ICCH_MESSAGE) &&
        (u1Event != ICCH_PEER_UP) &&
        (u1Event != MCLAG_ENABLED) &&
        (u1Event != MCLAG_DISABLED) &&
        (u1Event != GO_MASTER) && (u1Event != GO_SLAVE))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_ICCH_TRC, "ICCH event is invalid \r\n");

        return ICCH_FAILURE;
    }

    if ((pData == NULL) && (u1Event == ICCH_MESSAGE))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to Snooping task. */
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC,
                       "Message from ICCH ignored - Buffer missing !!!\r\n");
        return ICCH_FAILURE;
    }

    if (gu1SnoopInit == SNOOP_FALSE)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC, "Module not initialised\r\n");
        return ICCH_FAILURE;
    }
    if ((pSnoopMsgNode = (tSnoopMsgNode *) SNOOP_QMSG_ALLOC_MEMBLK) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC,
                       "Memory allocation failed for queue message\r\n");
        return ICCH_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopMsgNode, SNOOP_INIT_VAL, sizeof (tSnoopMsgNode));

    pSnoopMsgNode->u2MsgType = SNOOP_ICCH_MSG;

    pSnoopMsgNode->IcchData.u4Events = (UINT4) u1Event;
    pSnoopMsgNode->IcchData.pIcchMsg = pData;
    pSnoopMsgNode->IcchData.u2DataLen = u2DataLen;

    if (SNOOP_SEND_TO_QUEUE (SNOOP_MSG_QUEUE_ID, (UINT1 *) &pSnoopMsgNode,
                             SNOOP_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        if (SNOOP_RELEASE_QPKT_MEM_BLOCK (pSnoopMsgNode) != MEM_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                           SNOOP_MEM_NAME, "Queue message release failed in "
                           "ICCH packet processing\n");

        }
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_RESRC,
                       SNOOP_ICCH_TRC, "ICCH Message enqueue FAILED \n")
            return ICCH_FAILURE;
    }

    if (SNOOP_SEND_EVENT (SNOOP_TASK_ID, SNOOP_MSG_EVENT) != OSIX_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC, "ICCH Event send FAILED \n")
            return ICCH_FAILURE;
    }

    return ICCH_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchProcessUpdateMsg                             */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the ICCH module. This function invokes          */
/*                      appropriate functions to handle the update message.  */
/*                                                                           */
/* Input(s)           : pSnoopQMsg - Pointer to the VLAN Queue message.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SnoopIcchProcessUpdateMsg (tSnoopMsgNode * pSnoopMsgNode)
{
    tIcchProtoAck       ProtoAck;
    UINT4               u4SeqNum = 0;
    INT4                i4VlanId = SNOOP_INIT_VAL;

    MEMSET (&ProtoAck, 0, sizeof (tIcchProtoAck));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = SNOOP_DEFAULT_INSTANCE;
    gu4SnoopDbgInstId = SNOOP_DEFAULT_INSTANCE;

    if (pSnoopMsgNode == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_ICCH_TRC, "Received corrupted Message node = NULL \n");

        return;
    }

    switch (pSnoopMsgNode->IcchData.u4Events)
    {
        case ICCH_PEER_DOWN:
            SnoopIcchProcessPeerDown ();
            IcchReleaseMemoryForMsg ((UINT1 *) pSnoopMsgNode->IcchData.
                                     pIcchMsg);
            break;

        case ICCH_PEER_UP:
            SnoopIcchProcessPeerUp (SNOOP_INIT_VAL);
            IcchReleaseMemoryForMsg ((UINT1 *) pSnoopMsgNode->IcchData.
                                     pIcchMsg);
            break;

        case ICCH_MESSAGE:

            /* Read the sequence number from the ICCH Header */
            ICCH_PKT_GET_SEQNUM (pSnoopMsgNode->IcchData.pIcchMsg, &u4SeqNum);
            /* Remove the ICCH Header */
            ICCH_STRIP_OFF_ICCH_HDR (pSnoopMsgNode->IcchData.pIcchMsg,
                                     pSnoopMsgNode->IcchData.u2DataLen);

            ProtoAck.u4AppId = ICCH_SNOOP_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            SnoopIcchHandleUpdates (pSnoopMsgNode->IcchData.pIcchMsg,
                                    pSnoopMsgNode->IcchData.u2DataLen);

            ICCH_FREE (pSnoopMsgNode->IcchData.pIcchMsg);

            /* Sending ACK to ICCH */
            IcchApiSendProtoAckToICCH (&ProtoAck);
            break;
        case MCLAG_ENABLED:
            SnoopIcchProcessMCLAGEnableStatus (i4VlanId);
            break;
        case MCLAG_DISABLED:
            SnoopIcchProcessMCLAGDisableStatus ();
            break;
        case GO_MASTER:
        case GO_SLAVE:
            break;

        default:
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC, "Received unknown event \n");
            break;
    }                            /* End of switch */
}

/*****************************************************************************/
/* Function Name      : SnoopIcchProcessPeerDown                             */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      ICCH_PEER_DOWN from ICCH module.                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
SnoopIcchProcessPeerDown (VOID)
{
    tSnoopTag           VlanId;
    tVlanId             PrevVlanId = 0;
    tMacAddr            NullMacAddr;
    tIPvXAddr           NullAddr;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT4               u4Port = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2LocalPort = 0;
    UINT1              *pPortBitmap = NULL;
    UINT1               u1AddressType = 0;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    SNOOP_MEM_SET (&NullAddr, SNOOP_INIT_VAL, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (VlanId, SNOOP_INIT_VAL, sizeof (tSnoopTag));

    IcchGetIcclIfIndex (&u4Port);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4Port, &u4InstanceId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "SnoopIcchProcessPeerDown: Instance ID not found\r\n");
        return SNOOP_FAILURE;
    }

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV4 - 1))
        == SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV4;
    }

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV6 - 1)) ==
        SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV6;
    }

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "SnoopIcchProcessPeerDown: Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_ADD_TO_PORT_LIST (u2LocalPort, pPortBitmap);

    while (L2IwfMiGetNextActiveVlan (u4InstanceId, PrevVlanId,
                                     &u2VlanId) == L2IWF_SUCCESS)
    {
        PrevVlanId = u2VlanId;
        SNOOP_OUTER_VLAN (VlanId) = u2VlanId;

        if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                                   u1AddressType,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            continue;
        }

        if (SnoopGrpDeletePortEntry
            (u4InstanceId, pSnoopVlanEntry, (UINT4) u2LocalPort,
             NullMacAddr) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "SnoopIcchProcessPeerDown: Delete port entry on port delete event failed\r\n");
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }

        SnoopFwdUpdateFwdBitmap (u4InstanceId,
                                 pPortBitmap,
                                 pSnoopVlanEntry, NullAddr, SNOOP_DEL_PORT);

    }

    UtilPlstReleaseLocalPortList (pPortBitmap);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_ICCH_TRC,
               "SnoopIcchProcessPeerDown: Event Successfully Handled\r\n");

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchProcessPeerUp                               */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      ICCH_PEER_UP from ICCH module.                       */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE.                         */
/*****************************************************************************/
VOID
SnoopIcchProcessPeerUp (UINT2 u2VlanId)
{

    tSnoopTag           VlanId;
    tVlanId             PrevVlanId = 0;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tMacAddr            NullMacAddr;
    tIPvXAddr           NullAddr;
    UINT4               u4Port = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1AddressType = 0;
    UINT1              *pPortBitmap = NULL;

    IcchGetIcclIfIndex (&u4Port);

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NullAddr, SNOOP_INIT_VAL, sizeof (tIPvXAddr));

    if ((SnoopVcmGetContextInfoFromIfIndex (u4Port, &u4InstanceId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "SnoopIcchProcessPeerUp: Instance ID not found\r\n");
        return;
    }

    SNOOP_MEM_SET (VlanId, SNOOP_INIT_VAL, sizeof (tSnoopTag));

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "SnoopIcchProcessPeerDown: Error in allocating memory for pPortBitmap\r\n");
        return;
    }

    /* If VlanId received in 0, then peer up event is received. So
       consolidated report is triggered for all the vlan's present
       in the system. 
       If VlanId received is a valid vlan, then snooping is enabled
       in peer node on that particular vlan, and consolidated report
       is triggered for that vlan alone.
     */

    if (u2VlanId == 0)
    {
        PrevVlanId = 0;

        while (L2IwfMiGetNextActiveVlan (u4InstanceId, PrevVlanId,
                                         &u2VlanId) == L2IWF_SUCCESS)
        {

            PrevVlanId = u2VlanId;
            SNOOP_OUTER_VLAN (VlanId) = u2VlanId;
            for (u1AddressType = SNOOP_ADDR_TYPE_IPV4;
                 u1AddressType <= SNOOP_ADDR_TYPE_IPV6; u1AddressType++)
            {
                if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                                           u1AddressType,
                                           &pSnoopVlanEntry) != SNOOP_SUCCESS)
                {
                    continue;
                }
                SnoopFwdUpdateFwdBitmap (u4InstanceId,
                                         pPortBitmap,
                                         pSnoopVlanEntry, NullAddr,
                                         SNOOP_ADD_PORT);
                if (SNOOP_MCAST_FWD_MODE (u4InstanceId) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {

                    SnoopFwdUpdateRtrPortToMacFwdTable (u4InstanceId,
                                                        (UINT4) u2LocalPort,
                                                        pSnoopVlanEntry->VlanId,
                                                        pSnoopVlanEntry->
                                                        u1AddressType,
                                                        NullMacAddr,
                                                        SNOOP_ADD_PORT);
                }
                else if (SNOOP_MCAST_FWD_MODE (u4InstanceId) ==
                         SNOOP_MCAST_FWD_MODE_IP)
                {
                    SnoopFwdUpdateRtrPortToIpFwdTable (u4InstanceId,
                                                       (UINT4) u2LocalPort,
                                                       pSnoopVlanEntry->VlanId,
                                                       pSnoopVlanEntry->
                                                       u1AddressType,
                                                       SNOOP_ADD_PORT);
                }
            }
            SnoopSendConsolidatedReportOnIccl (u2LocalPort, u4InstanceId,
                                               VlanId);
        }
    }

    else
    {
        SNOOP_OUTER_VLAN (VlanId) = u2VlanId;
        for (u1AddressType = SNOOP_ADDR_TYPE_IPV4;
             u1AddressType <= SNOOP_ADDR_TYPE_IPV6; u1AddressType++)
        {
            if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                                       u1AddressType,
                                       &pSnoopVlanEntry) == SNOOP_SUCCESS)
            {

                SnoopFwdUpdateFwdBitmap (u4InstanceId,
                                         pPortBitmap,
                                         pSnoopVlanEntry, NullAddr,
                                         SNOOP_ADD_PORT);

                if (SNOOP_MCAST_FWD_MODE (u4InstanceId) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {

                    SnoopFwdUpdateRtrPortToMacFwdTable (u4InstanceId,
                                                        (UINT4) u2LocalPort,
                                                        pSnoopVlanEntry->VlanId,
                                                        pSnoopVlanEntry->
                                                        u1AddressType,
                                                        NullMacAddr,
                                                        SNOOP_ADD_PORT);
                }
                else if (SNOOP_MCAST_FWD_MODE (u4InstanceId) ==
                         SNOOP_MCAST_FWD_MODE_IP)
                {
                    SnoopFwdUpdateRtrPortToIpFwdTable (u4InstanceId,
                                                       (UINT4) u2LocalPort,
                                                       pSnoopVlanEntry->VlanId,
                                                       pSnoopVlanEntry->
                                                       u1AddressType,
                                                       SNOOP_ADD_PORT);
                }
            }
        }

        SnoopSendConsolidatedReportOnIccl (u2LocalPort, u4InstanceId, VlanId);
    }

    UtilPlstReleaseLocalPortList (pPortBitmap);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_ICCH_TRC,
               "SnoopIcchProcessPeerUp: Event Successfully Handled\r\n");

    return;
}

/******************************************************************************/
/*  Function Name   : SnoopIcchSendMsgToIcch                                  */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the ICCH  */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to RM module                 */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : SNOOP_SUCCESS/SNOOP_FAILURE                             */
/******************************************************************************/
INT4
SnoopIcchSendMsgToIcch (tIcchMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = SNOOP_DEFAULT_INSTANCE;
    gu4SnoopDbgInstId = SNOOP_DEFAULT_INSTANCE;

    u4RetVal = SnoopIcchEnqMsgToIcchFromAppl (pMsg, u2Len, ICCH_SNOOP_APP_ID,
                                              ICCH_SNOOP_APP_ID);

    if (u4RetVal != ICCH_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_ICCH_TRC,
                   "Enqueuing of Messages from Appl to Icch failed \n");
        ICCH_FREE (pMsg);
        return SNOOP_FAILURE;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_ICCH_TRC,
               "SnoopIcchSendMsgToIcch: Message Enqueued to ICCH module \n");

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchProcessMCLAGDisableStatus                   */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      MCLAG_DISABLED from ICCH module.                     */
/*                      It flushes down all the ICCL router entries learnt on*/
/*                      switch deletes the corresponding entries             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopIcchProcessMCLAGDisableStatus (VOID)
{
    tSnoopTag           VlanId;
    tVlanId             PrevVlanId = 0;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntry = NULL;
    tIPvXAddr           NullAddr;
    tMacAddr            NullMacAddr;
    UINT4               u4Port = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2LocalPort = 0;
    UINT1               u1AddressType = 0;
    BOOL1               bResult = 0;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    IcchGetIcclIfIndex (&u4Port);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4Port, &u4InstanceId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "SnoopIcchProcessMCLAGDisableStatus: Instance ID not found\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (&NullAddr, SNOOP_INIT_VAL, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (VlanId, SNOOP_INIT_VAL, sizeof (tSnoopTag));

    for (u1AddressType = SNOOP_ADDR_TYPE_IPV4;
         u1AddressType <= SNOOP_ADDR_TYPE_IPV6; u1AddressType++)
    {
        while (L2IwfMiGetNextActiveVlan (u4InstanceId, PrevVlanId,
                                         &u2VlanId) == L2IWF_SUCCESS)
        {
            PrevVlanId = u2VlanId;

            SNOOP_OUTER_VLAN (VlanId) = u2VlanId;

            if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                                       u1AddressType,
                                       &pSnoopVlanEntry) != SNOOP_SUCCESS)
            {
                continue;
            }

            pSnoopVlanCfgEntry = pSnoopVlanEntry->pSnoopVlanCfgEntry;

            if (pSnoopVlanCfgEntry != NULL)
            {
                SNOOP_IS_PORT_PRESENT (u2LocalPort,
                                       pSnoopVlanCfgEntry->CfgStaticRtrPortBmp,
                                       bResult);

                if (bResult == OSIX_TRUE)
                {
                    SNOOP_DEL_FROM_PORT_LIST (u2LocalPort,
                                              pSnoopVlanCfgEntry->
                                              CfgStaticRtrPortBmp);

                    SnoopFwdUpdateFwdBitmap (u4InstanceId,
                                             pSnoopVlanEntry->RtrPortBitmap,
                                             pSnoopVlanEntry,
                                             NullAddr, SNOOP_DEL_PORT);

                    SnoopVlanAddRemRtrPort (u4InstanceId, (UINT4) u2LocalPort,
                                            pSnoopVlanEntry, SNOOP_DEL_PORT);

                    SNOOP_MEM_CPY (pSnoopVlanEntry->StaticRtrPortBitmap,
                                   pSnoopVlanCfgEntry->CfgStaticRtrPortBmp,
                                   SNOOP_PORT_LIST_SIZE);

                    SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->
                                            StaticRtrPortBitmap,
                                            pSnoopVlanEntry->RtrPortBitmap);

                    if (SnoopGrpDeletePortEntry
                        (u4InstanceId, pSnoopVlanEntry, (UINT4) u2LocalPort,
                         NullMacAddr) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_GRP_TRC,
                                   " SnoopIcchProcessMCLAGDisableStatus: Delete port entry on port delete event failed\n");
                        return SNOOP_FAILURE;
                    }

                }
            }
        }
        PrevVlanId = 0;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_ICCH_TRC,
               "SnoopIcchProcessMCLAGDisableStatus: Event Successfully Handled\r\n");

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchProcessMCLAGEnableStatus                    */
/*                                                                           */
/* Description        : This function handles the processing of the event    */
/*                      MCLAG_ENABLED from ICCH module.                      */
/*                      It adds the ICCL interface as a router entry in all  */
/*                      existing VLANs                                       */
/*                                                                           */
/* Input(s)           : i4VlanId - Vlan identifier.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
VOID
SnoopIcchProcessMCLAGEnableStatus (INT4 i4VlanId)
{
    tSnoopTag           VlanId;
    UINT4               u4Port = 0;
    UINT4               u4InstanceId = 0;
    tVlanId             PrevVlanId;
    UINT2               u2VlanId = 0;
    UINT2               u2LocalPort = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = SNOOP_DEFAULT_INSTANCE;
    gu4SnoopDbgInstId = SNOOP_DEFAULT_INSTANCE;

    IcchGetIcclIfIndex (&u4Port);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4Port, &u4InstanceId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_GBL_NAME,
                   "SnoopIcchProcessMCLAGEnableStatus: Instance ID not found\r\n");
        return;
    }

    SNOOP_MEM_SET (VlanId, SNOOP_INIT_VAL, sizeof (tSnoopTag));

    if (((SNOOP_SNOOPING_STATUS (u4InstanceId, SNOOP_ADDR_TYPE_IPV4 - 1)) ==
         SNOOP_DISABLE) &&
        ((SNOOP_SNOOPING_STATUS (u4InstanceId, SNOOP_ADDR_TYPE_IPV6 - 1)) ==
         SNOOP_DISABLE))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_GBL_NAME,
                   "SnoopIcchProcessMCLAGEnableStatus: Snooping is disabled \r\n");
        return;
    }
    /*If vlan id is received as 0, then we loop for all existing
     * VLANs and add the ICCL interface as static mrouter port
     * If a specific vlan id is received, then we add it to the
     * specific VLAN as a static router port */

    if (i4VlanId == 0)
    {
        PrevVlanId = 0;
        SnoopIcchSyncMclagStatusChange (u2VlanId);

        while (L2IwfMiGetNextActiveVlan (u4InstanceId, PrevVlanId,
                                         &u2VlanId) == L2IWF_SUCCESS)
        {

            PrevVlanId = u2VlanId;

            SNOOP_OUTER_VLAN (VlanId) = u2VlanId;

            SnoopIcchUpdateRtrPort (u4InstanceId, VlanId, u2LocalPort);

            SnoopSendConsolidatedReportOnIccl (u2LocalPort, u4InstanceId,
                                               VlanId);
        }
    }

    else
    {
        SNOOP_OUTER_VLAN (VlanId) = (UINT2) i4VlanId;

        SnoopIcchUpdateRtrPort (u4InstanceId, VlanId, u2LocalPort);
        SnoopIcchSyncMclagStatusChange ((UINT2) i4VlanId);

        SnoopSendConsolidatedReportOnIccl (u2LocalPort, u4InstanceId, VlanId);
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_ICCH_TRC,
               "SnoopIcchProcessMCLAGEnableStatus: Event Successfully Handled\r\n");

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchUpdateRtrPort                                       */
/*                                                                           */
/* Description        : This function updates the ICCL link as mrouter port  */
/*                      and update the port list                             */
/*                                                                           */
/* Input(s)           : u4InstanceId - Instance ID                           */
/*                      VlanId - Vlan Identifier                             */
/*                      pSnoopVlanEntry - Vlan Information                   */
/*                      u2LocalPort - Port list                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopIcchUpdateRtrPort (UINT4 u4InstanceId, tSnoopTag VlanId, UINT2 u2LocalPort)
{
    tMacAddr            NullMacAddr;
    tIPvXAddr           NullAddr;
    tSnoopVlanCfgEntry *pSnoopVlanCfgEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1AddressType = 0;
    BOOL1               bIsVlanPresent = OSIX_TRUE;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&NullMacAddr, SNOOP_INIT_VAL, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NullAddr, SNOOP_INIT_VAL, sizeof (tIPvXAddr));

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV4 - 1))
        == SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV4;
    }

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV6 - 1)) ==
        SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV6;
    }

    if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                               u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        bIsVlanPresent = OSIX_FALSE;

    }
    if (bIsVlanPresent == OSIX_FALSE)
    {
        if (SnoopVlanCreateVlanEntry (u4InstanceId, VlanId,
                                      u1AddressType,
                                      &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_ICCH_TRC,
                            "SnoopIcchUpdateRtrPort: Unable to create VLAN entry for "
                            " VLAN id %d\n", VlanId);
            return SNOOP_FAILURE;
        }
    }
    if (pSnoopVlanEntry != NULL)
    {

        pSnoopVlanCfgEntry = pSnoopVlanEntry->pSnoopVlanCfgEntry;

        if ((bIsVlanPresent == OSIX_FALSE) && (pSnoopVlanCfgEntry != NULL))
        {
            SNOOP_IS_PORT_PRESENT (u2LocalPort,
                                   pSnoopVlanCfgEntry->
                                   CfgStaticRtrPortBmp, bResult);
        }
    }

    if ((bResult == OSIX_FALSE) && (pSnoopVlanCfgEntry != NULL))
    {
        SNOOP_ADD_TO_PORT_LIST (u2LocalPort,
                                pSnoopVlanCfgEntry->CfgStaticRtrPortBmp);
        if (SnoopGrpDeletePortEntry (u4InstanceId,
                                     pSnoopVlanEntry,
                                     (UINT4) u2LocalPort,
                                     NullMacAddr) == SNOOP_FAILURE)
        {

            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_ICCH_TRC,
                            "Failed to delete port entry for VLAN %d\r\n",
                            pSnoopVlanEntry->VlanId);
        }

        SnoopVlanAddRemRtrPort (u4InstanceId, (UINT4) u2LocalPort,
                                pSnoopVlanEntry, SNOOP_ADD_PORT);

        /* Update the Router port Bitmap and forwarding bitmap with
         ** the new router port bitmap set */
        SNOOP_MEM_CPY (pSnoopVlanEntry->StaticRtrPortBitmap,
                       pSnoopVlanCfgEntry->CfgStaticRtrPortBmp,
                       SNOOP_PORT_LIST_SIZE);

        SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->StaticRtrPortBitmap,
                                pSnoopVlanEntry->RtrPortBitmap);

        if (SnoopFwdUpdateFwdBitmap (u4InstanceId,
                                     pSnoopVlanEntry->RtrPortBitmap,
                                     pSnoopVlanEntry, NullAddr, SNOOP_ADD_PORT)
            == SNOOP_FAILURE)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_ICCH_TRC,
                            "Failed to update forward entry for VLAN %d\r\n",
                            pSnoopVlanEntry->VlanId);
        }
    }

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopSendConsolidatedReportOnIccl              */
/*                                                                          */
/* Description           :   Sends a consolidated Report on ICCL interface  */
/*                           on receiving a general query in ICCH interface */
/*                                                                          */
/* Input (s)             :   u4IcclPort  - ICCL port                        */
/*                           u4Instance  - Instance id                      */
/*                           VlanId      - Vlan Id                          */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopSendConsolidatedReportOnIccl (UINT4 u4IcclPort, UINT4 u4Instance,
                                   tSnoopTag VlanId)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tIPvXAddr           DestIpAddr;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTmpGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT1              *pPortBitmap = NULL;
    UINT1              *pTempPortBitMap = NULL;
    INT4                i4Result = 0;
    UINT4               u4PhyPort = 0;
    UINT4               u4MaxLength = 0;
    UINT4               u4IcchPort = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2AdminKey = 0;
    UINT2               u2Index = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2LocalIcchPort = 0;
    UINT1               u1McLagInterfacePresent = SNOOP_FALSE;
    UINT1               u1NonMcLagInterfacePresent = SNOOP_FALSE;
    UINT1               u1Version = 0;
    UINT1               u1Forward = SNOOP_FALSE;
    UINT1               u1RecordType = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&DestIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));

    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "SnoopSendConsolidatedReportOnIccl: Group Entry not present\r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry);
    if (pRBElem == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME,
                       "SnoopSendConsolidatedReportOnIccl: Consolidated Group entry not present\r\n");
        return SNOOP_SUCCESS;
    }

    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        return SNOOP_FAILURE;
    }
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pPortBitmap while processing general query\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pTempPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitMap == NULL)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pTempPortBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "allocating memory for pTempPortBitMap while processing general query\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitMap, 0, sizeof (tSnoopPortBmp));

    if (SNOOP_SNOOPING_STATUS (u4Instance, (SNOOP_ADDR_TYPE_IPV4 - 1))
        == SNOOP_ENABLE)
    {
        u1Version = SNOOP_IGS_IGMP_VERSION3;
    }

    if (SNOOP_SNOOPING_STATUS (u4Instance, (SNOOP_ADDR_TYPE_IPV6 - 1))
        == SNOOP_ENABLE)
    {
        u1Version = SNOOP_MLD_VERSION2;
    }

    IcchGetIcclIfIndex (&u4IcchPort);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4IcchPort, &u4InstanceId,
                                            &u2LocalIcchPort)) != VCM_SUCCESS)
    {
        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME, "Instance ID not found\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }

    while (pRBElem != NULL)
    {

        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                           pRBElem, NULL);

        u1McLagInterfacePresent = SNOOP_FALSE;
        u1NonMcLagInterfacePresent = SNOOP_FALSE;
        u4PhyPort = 0;

        pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;

        if ((SnoopIsVlanPresentInFwdTbl
             (SNOOP_OUTER_VLAN (pSnoopConsGroupEntry->VlanId),
              SNOOP_OUTER_VLAN (VlanId), &L2PvlanMappingInfo) == SNOOP_FALSE))
        {
            pRBElem = pRBNextElem;
            continue;
        }

        SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                                   pSnoopGroupEntry, pSnoopTempGroupEntry,
                                   tSnoopGroupEntry *)
        {
            /* Reset the inner vlan id */
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;

            IPVX_ADDR_COPY (&DestIpAddr, &(pSnoopConsGroupEntry->GroupIpAddr));

            if (pSnoopGroupEntry != NULL)
            {
                /* Fill the inner vlan id */
                SNOOP_INNER_VLAN (VlanId) =
                    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId);
                SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId,
                               sizeof (tSnoopTag));
            }

            if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                       &pSnoopTmpGroupEntry) == SNOOP_SUCCESS)
            {
                SNOOP_MEM_CPY (pTempPortBitMap,
                               pSnoopTmpGroupEntry->PortBitmap,
                               sizeof (tSnoopPortBmp));

                SNOOP_DEL_FROM_PORT_LIST (u2LocalIcchPort, pTempPortBitMap);

                if (SNOOP_MEM_CMP (pTempPortBitMap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }

                /* Get the list of MCLAG interfaces available and
                   check if the GroupEntry bitmap has an entry */

                SnoopCheckBitMapForMcLagInterface (pTempPortBitMap,
                                                   &u1McLagInterfacePresent,
                                                   &u1NonMcLagInterfacePresent,
                                                   &u4PhyPort);
            }
            else
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if ((u1Version == SNOOP_IGS_IGMP_VERSION3)
                || (u1Version == SNOOP_MLD_VERSION2))
            {
                /* if the forwarding is done based on MAC then construct the
                 * SSM Reports for all the Group records with NULL source list
                 * Use the misc preallocated buffer and copy the group
                 * information to the buffer when the max length reaches just
                 * send the buffer out */

                if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
                {
                    u1RecordType =
                        ((pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                         SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);

                    for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                         u2Index++)
                    {
                        if (u1RecordType == SNOOP_IS_INCLUDE)
                        {
                            OSIX_BITLIST_IS_BIT_SET
                                (pSnoopConsGroupEntry->InclSrcBmp,
                                 u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                            if (bResult == OSIX_TRUE)
                            {
                                u2NoSources++;
                            }
                        }
                        else
                        {
                            OSIX_BITLIST_IS_BIT_SET
                                (pSnoopConsGroupEntry->ExclSrcBmp,
                                 u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                            if (bResult == OSIX_TRUE)
                            {
                                u2NoSources++;
                            }
                        }
                    }
                    u2GrpRec++;
                }
                else
                {
                    /* If enhanced mode is enabled, sync the group 
                       information instead of Consolidated group 
                       information 
                     */

                    if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) ==
                        SNOOP_DISABLE)
                    {
                        /* If optimization is disabled the reports are sent for every 
                           host entry present in the system */

                        SnoopSendAllHostReportsOnIccl (u4Instance,
                                                       pSnoopGroupEntry);
                        continue;
                    }
                    else
                    {
                        u1RecordType =
                            ((pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                             SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);

                        for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                             u2Index++)
                        {
                            if (u1RecordType == SNOOP_IS_INCLUDE)
                            {
                                OSIX_BITLIST_IS_BIT_SET
                                    (pSnoopGroupEntry->InclSrcBmp,
                                     u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                                if (bResult == OSIX_TRUE)
                                {
                                    u2NoSources++;
                                }
                            }
                            else
                            {
                                OSIX_BITLIST_IS_BIT_SET
                                    (pSnoopGroupEntry->ExclSrcBmp,
                                     u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                                if (bResult == OSIX_TRUE)
                                {
                                    u2NoSources++;
                                }
                            }
                        }
                        u2GrpRec++;
                    }
                }

#ifdef IGS_WANTED
                if (u1Version == SNOOP_IGS_IGMP_VERSION3)
                {
                    if (u1McLagInterfacePresent == SNOOP_TRUE)
                    {
                        LaUpdateActorPortChannelAdminKey (u4PhyPort,
                                                          &u2AdminKey);
                        IgsUtilFillV3SourceInfoForIccl (u4Instance,
                                                        pSnoopGroupEntry,
                                                        u1RecordType,
                                                        u2NoSources,
                                                        &u4MaxLength,
                                                        u2AdminKey,
                                                        u1NonMcLagInterfacePresent,
                                                        &SnoopIcchInfo);
                    }
                    else
                    {
                        IgsUtilFillV3SourceInfo (u4Instance,
                                                 pSnoopConsGroupEntry,
                                                 u1RecordType, u2NoSources,
                                                 &u4MaxLength);
                    }

                    if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                    {
                        i4Result =
                            IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                                  u4MaxLength, &pOutBuf);
                    }
                }
#endif

#ifdef MLDS_WANTED
                if (u1Version == SNOOP_MLD_VERSION_2)
                {
                    if (u1McLagInterfacePresent == SNOOP_TRUE)
                    {
                        LaUpdateActorPortChannelAdminKey (u4PhyPort,
                                                          &u2AdminKey);
                        MldsUtilFillV2SourceInfoOnIccl (u4Instance,
                                                        pSnoopConsGroupEntry,
                                                        u1RecordType,
                                                        u2NoSources,
                                                        &u4MaxLength,
                                                        u2AdminKey,
                                                        u1NonMcLagInterfacePresent);
                    }
                    else
                    {
                        MldsUtilFillV2SourceInfo (u4Instance,
                                                  pSnoopConsGroupEntry,
                                                  u2NoSources, u1RecordType,
                                                  &u4MaxLength);
                    }
                    if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                    {
                        i4Result =
                            MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                                   u4MaxLength, &pOutBuf);
                    }
                }
#endif
                u2NoSources = 0;
                u1Forward = SNOOP_FALSE;

                if (((u1Version == SNOOP_MLD_VERSION_2)
                     && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE))
                    || ((u1Version == SNOOP_IGS_IGMP_VERSION3)
                        && (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)))
                {
                    if (i4Result != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_PKT_TRC,
                                   "Unable to generate and send SSM report on "
                                   "to router ports\r\n");
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);

                        UtilPlstReleaseLocalPortList (pPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitMap);
                        return SNOOP_FAILURE;
                    }

                    SNOOP_ADD_TO_PORT_LIST (u4IcclPort, pPortBitmap);

                    SnoopVlanForwardPacket (u4Instance,
                                            VlanId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            SNOOP_INVALID_PORT,
                                            VLAN_FORWARD_SPECIFIC, pOutBuf,
                                            pPortBitmap, SNOOP_SSMREPORT_SENT);

                    u2GrpRec = 0;
                    u1Forward = SNOOP_TRUE;
                    u4MaxLength = 0;
                    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                   SNOOP_MISC_MEMBLK_SIZE);
                }
            }
        }

        pRBElem = pRBNextElem;
    }

    UtilPlstReleaseLocalPortList (pTempPortBitMap);

    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    if ((u1Forward == SNOOP_TRUE) || (u4MaxLength == 0))
    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_SUCCESS;
    }

#ifdef IGS_WANTED
    if (u1Version == SNOOP_IGS_IGMP_VERSION3)
    {
        i4Result = IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                         u4MaxLength, &pOutBuf);
    }
#endif

#ifdef MLDS_WANTED
    if (u1Version == SNOOP_MLD_VERSION_2)
    {
        i4Result = MldsEncodeAggV2Report (u4Instance, u2GrpRec,
                                          u4MaxLength, &pOutBuf);
    }
#endif

    if (i4Result == SNOOP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }

    SNOOP_ADD_TO_PORT_LIST (u4IcclPort, pPortBitmap);

    SnoopVlanForwardPacket (u4Instance, VlanId,
                            SNOOP_ADDR_TYPE_IPV4, SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, pPortBitmap,
                            SNOOP_SSMREPORT_SENT);

    UtilPlstReleaseLocalPortList (pPortBitmap);

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopCheckBitMapForMcLagInterface              */
/*                                                                          */
/* Description           :   Checks port bit map for MCLAG and non MCLAG    */
/*                           interfaces                                     */
/*                                                                          */
/* Input (s)             :   PortBitMap  - Group ports bitmap               */
/*                                                                          */
/* Output (s)            :   u1McLagInterfacePresent - SUCCESS if bitmap    */
/*                           has a MCLAG interface                          */
/*                           u1NonMcLagInterfacePresent - SUCCESS if bitmap */
/*                           has a Non MCLAG interface                      */
/*                           u4PhyPort - Physical port of MCLAG interface   */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopCheckBitMapForMcLagInterface (tSnoopPortBmp PortBitmap,
                                   UINT1 *pu1McLagInterfacePresent,
                                   UINT1 *pu1NonMcLagInterfacePresent,
                                   UINT4 *pu4PhyPort)
{
    UINT4               u4CheckPort = 0;
    UINT4               u4Instance = 0;
    UINT2               u2BytIndex = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT2               u2Port = 0;
    UINT1               u1IsMcLagEnabled = 0;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* stores physical ports info for local port */
                    u4CheckPort = SNOOP_GET_IFINDEX (u4Instance, u2Port);
                    LaApiIsMclagInterface (u4CheckPort, &u1IsMcLagEnabled);
                    if (u1IsMcLagEnabled == SNOOP_TRUE)
                    {
                        *pu1McLagInterfacePresent = SNOOP_TRUE;
                        SNOOP_DEL_FROM_PORT_LIST (u2Port, PortBitmap);
                        *pu4PhyPort = u4CheckPort;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if (SNOOP_MEM_CMP (PortBitmap, gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        *pu1NonMcLagInterfacePresent = SNOOP_FALSE;
    }
    else
    {
        *pu1NonMcLagInterfacePresent = SNOOP_TRUE;
    }

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopCheckBitMapForFirstNonMcLagInterface      */
/*                                                                          */
/* Description           :   Checks port bit map for first non MCLAG port   */
/*                                                                          */
/* Input (s)             :   PortBitMap  - Group ports bitmap               */
/*                                                                          */
/* Output (s)            :   u1FirstNonMcLagInterfacePresent - SUCCESS if   */
/*                           port is the first NON-MCLAG interface          */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopCheckBitMapForFirstNonMcLagInterface (tSnoopPortBmp AsmPortBitmap,
                                           UINT1 *pu1FirstNonMcLagInterface)
{
    UINT4               u4CheckPort = 0;
    UINT2               u2Port = 0;
    UINT2               u2BytIndex = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT2               u4Instance = 0;
    UINT1               u1IsMcLagEnabled = OSIX_FALSE;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;
    tSnoopPortBmp       PortBitmap;

    SNOOP_MEM_SET (PortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_CPY (PortBitmap, AsmPortBitmap, sizeof (tSnoopPortBmp));

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* stores physical ports info for local port */
                    u4CheckPort = SNOOP_GET_IFINDEX (u4Instance, u2Port);
                    LaApiIsMclagInterface (u4CheckPort, &u1IsMcLagEnabled);
                    if (u1IsMcLagEnabled == OSIX_TRUE)
                    {
                        SNOOP_DEL_FROM_PORT_LIST (u2Port, PortBitmap);
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    if (SNOOP_MEM_CMP (PortBitmap, gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        *pu1FirstNonMcLagInterface = SNOOP_TRUE;
    }
    else
    {
        *pu1FirstNonMcLagInterface = SNOOP_FALSE;
    }

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopProcessLeaveSyncMessage                   */
/*                                                                          */
/* Description           :   This function Processes Sync message received  */
/*                           on ICCL and starts Group Specific query timer  */
/*                           and related frame handling                     */
/*                                                                          */
/* Input (s)             :   GrpAddr - Group Address for which leave has    */
/*                                     been received                        */
/*                           u2AdminKey - Admin Key of port-channel         */
/*                           u2VlanId   - Vlan Id of the group              */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopProcessLeaveSyncMessage (tIPvXAddr GrpAddr, UINT2 u2AdminKey,
                              tSnoopTag VlanId)
{

    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pPortBitmap = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    INT4                i4Result = 0;
    UINT4               u4GrpAddr = 0;
    UINT4               u4InPort = 0;
    UINT4               u4InstanceId = 0;
    UINT2               u2LocalPort = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1SSMReceiver = SNOOP_FALSE;
    UINT1               u1AddressType = 0;

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV4 - 1))
        == SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV4;
    }

    if (SNOOP_SNOOPING_STATUS (u4InstanceId, (SNOOP_ADDR_TYPE_IPV6 - 1)) ==
        SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV6;
    }

    if (LaGetAggIndexFromAdminKey (u2AdminKey, &u4InPort) == LA_TRUE)
    {
        if (SnoopVcmGetContextInfoFromIfIndex (u4InPort,
                                               &u4InstanceId,
                                               &u2LocalPort) == VCM_SUCCESS)
        {
            u4InPort = u2LocalPort;
        }
        else
        {
            /* Local port get failed for the given port */
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "SnoopProcessLeaveSyncMessage: Processing of leave message failed - Port info get failed\n");
            return SNOOP_FAILURE;
        }
    }

    if (SnoopVlanGetVlanEntry (u4InstanceId, VlanId,
                               u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "SnoopProcessLeaveSyncMessage: Processing of leave message failed - VlanEntry Not present\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopGrpGetGroupEntry (u4InstanceId, VlanId,
                               GrpAddr, &pSnoopGroupEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "SnoopProcessLeaveSyncMessage: Processing of leave message failed -Group entry not "
                   "present\n");
        return SNOOP_SUCCESS;
    }

    pSnoopGroupEntry->u1McLagLeaveReceived = SNOOP_TRUE;

    SNOOP_IS_PORT_PRESENT (u4InPort, pSnoopGroupEntry->PortBitmap, bResult);

    if (bResult == OSIX_FALSE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "SnoopProcessLeaveSyncMessage: Processing of leave message failed - "
                   "Incoming port is not learnt as a member "
                   "in the given multicast group \r\n");
        return SNOOP_SUCCESS;
    }

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                    pSnoopASMPortEntry, tSnoopPortEntry *)
    {
        if (pSnoopASMPortEntry->u4Port == u4InPort)
        {
            break;
        }
    }

    if (pSnoopASMPortEntry == NULL)
    {

        /* If a Leave is received and ASM ports not available, then
           the leave is message is sent for the expiry of SSM group
           entry. For this send group specific query alone */

        if (SNOOP_MCAST_FWD_MODE (u4InstanceId) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopSSMPortEntry->u4Port == u4InPort)
                {
                    u1SSMReceiver = SNOOP_TRUE;
                    break;
                }
            }
        }

        if (u1SSMReceiver == SNOOP_FALSE)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                       "SnoopProcessLeaveSyncMessage: Processing of leave message failed - "
                       "Incoming port is not learnt as a member "
                       "in the given multicast group \r\n");
            return SNOOP_SUCCESS;
        }
    }

    if (u1SSMReceiver == SNOOP_FALSE)
    {
#ifdef IGS_WANTED

        if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
        {
            /* Check if the v1 host is present in the port, then
             * do not process the leave message.*/

            if (pSnoopASMPortEntry->u1V1HostPresent == SNOOP_TRUE)
            {
                /* Since a V1 host is present on this port, do not
                 * process the leave.*/
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC,
                           "SnoopProcessLeaveSyncMessage: V1 host present."
                           "Processing of leave message ignored\n");

                return SNOOP_SUCCESS;
            }
        }
#endif

        /* If the group query timer is running on the port on which the leave
         * message is received, do not process the Leave message */
        if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType
            == SNOOP_GRP_QUERY_TIMER)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "SnoopProcessLeaveSyncMessage: Leave message already received\n");

            /* Set the pu1Forward flag to SNOOP_RELEASE_BUFFER so that the
             * packet buffer will be freed outside this function */
            return SNOOP_SUCCESS;
        }

        /* Stop the port purge timer and start the group query timer */
        if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
        }

        pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u4Instance = u4InstanceId;
        pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType =
            SNOOP_GRP_QUERY_TIMER;
    }

#ifdef IGS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        u4GrpAddr = SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr);
        i4Result = IgsEncodeQuery (u4InstanceId, u4GrpAddr, SNOOP_GRP_QUERY,
                                   pSnoopVlanEntry->pSnoopVlanCfgEntry->
                                   u1ConfigOperVer, &pOutBuf, pSnoopVlanEntry);
    }
#endif
#ifdef MLDS_WANTED
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        i4Result =
            MldsEncodeQuery (u4InstanceId, GrpAddr.au1Addr, SNOOP_GRP_QUERY,
                             SNOOP_MLD_VERSION2, &pOutBuf, pSnoopVlanEntry);
    }
#endif

    if (i4Result != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_PKT_TRC,
                   "SnoopProcessLeaveSyncMessage: Group Specific Query construct and send - failed\r\n");
        return SNOOP_FAILURE;
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopProcessLeaveSyncMessage: Error in allocating memory for pPortBitmap\r\n");
        CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_ADD_TO_PORT_LIST (u4InPort, pPortBitmap);

    if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        if (pOutBuf != NULL)
        {
            SnoopVlanTagFrame (pOutBuf, SNOOP_INNER_VLAN (VlanId),
                               SNOOP_VLAN_HIGHEST_PRIORITY);
        }
    }

    SnoopVlanForwardPacket (u4InstanceId, VlanId,
                            pSnoopVlanEntry->u1AddressType, SNOOP_INVALID_PORT,
                            VLAN_FORWARD_SPECIFIC, pOutBuf, pPortBitmap,
                            SNOOP_GRP_QUERY_SENT);

    UtilPlstReleaseLocalPortList (pPortBitmap);

    if (u1SSMReceiver == SNOOP_FALSE)
    {

        SnoopTmrStartTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer,
                            SNOOP_INSTANCE_INFO (u4InstanceId)->
                            SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                            u4GrpQueryInt);

        /* Increment the RetryCount */
        pSnoopASMPortEntry->u1RetryCount++;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
               "SnoopProcessLeaveSyncMessage: Leave Message processed successfully \r\n");

    return SNOOP_SUCCESS;

}

/****************************************************************************/
/* Function Name         :   SnoopSendConsolidatedGroupReportOnIccl         */
/*                                                                          */
/* Description           :   This function Sends a consolidated group Report*/
/*                           for a particular group on ICCL on receiving    */
/*                           a group specific query                         */
/*                                                                          */
/* Input (s)             :   u4Instance  - Instance id                      */
/*                           pIgsVlanEntry - Vlan entry                     */
/*                           pIgsConsGroupEntry - Consolidated group entry  */
/*                           u4InPort    - Incoming Port                    */
/*                           VlanId      - Vlan Id                          */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopSendConsolidatedGroupReportOnIccl (UINT4 u4Instance,
                                        tSnoopVlanEntry * pIgsVlanEntry,
                                        tSnoopConsolidatedGroupEntry *
                                        pIgsConsGroupEntry, UINT4 u4InPort,
                                        tSnoopTag VlanId)
{
    tIPvXAddr           DestIpAddr;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    UINT1              *pPortBitmap = NULL;
    UINT1              *pTempPortBitmap = NULL;
    UINT4               u4PhyPort = 0;
    UINT4               u4MaxLength = 0;
    UINT2               u2AdminKey = 0;
    UINT2               u2Index = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT1               u1Version = SNOOP_IGS_IGMP_VERSION3;
    UINT1               u1RecordType = 0;
    UINT1               u1McLagInterfacePresent = SNOOP_FALSE;
    UINT1               u1NonMcLagInterfacePresent = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&DestIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendConsolidatedGroupReportOnIccl: "
                   "Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    if (pTempPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendConsolidatedGroupReportOnIccl: "
                   "Error in allocating memory for pTempPortBitmap\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_UTL_DLL_OFFSET_SCAN (&(pIgsConsGroupEntry->GroupEntryList),
                               pSnoopGroupEntry, pSnoopTempGroupEntry,
                               tSnoopGroupEntry *)
    {
        /* Reset the parameters for each report */
        SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
        u2NoSources = 0;
        u4MaxLength = 0;

        if (u1Version == SNOOP_IGS_IGMP_VERSION3)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                u1RecordType = SNOOP_IS_EXCLUDE;
            }
            else
            {
                u1RecordType =
                    ((pIgsConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);
            }
        }

        SNOOP_ADD_TO_PORT_LIST (u4InPort, pPortBitmap);

        if (pSnoopGroupEntry != NULL)
        {
            /* Fill the inner vlan id */
            SNOOP_INNER_VLAN (VlanId) =
                SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId);
            SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, VlanId, sizeof (tSnoopTag));
        }

        IPVX_ADDR_COPY (&DestIpAddr, &(pIgsConsGroupEntry->GroupIpAddr));

        if (SnoopGrpGetGroupEntry (u4Instance, VlanId, DestIpAddr,
                                   &pSnoopGroupEntry) == SNOOP_SUCCESS)
        {
            SNOOP_MEM_CPY (pTempPortBitmap,
                           pSnoopGroupEntry->PortBitmap,
                           sizeof (tSnoopPortBmp));

            SnoopCheckBitMapForMcLagInterface (pTempPortBitmap,
                                               &u1McLagInterfacePresent,
                                               &u1NonMcLagInterfacePresent,
                                               &u4PhyPort);
        }

        if (pSnoopGroupEntry == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "SnoopSendConsolidatedGroupReportOnIccl: "
                       "Error in getting Group entry\r\n");
            UtilPlstReleaseLocalPortList (pTempPortBitmap);
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }

        /* If enhanced mode is enabled, fill the source information
           for the [SVLAN,CLVAN] group entry 
         */
        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
        {
            /* Send all the host reports to neighbour node */

            if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) == SNOOP_DISABLE)
            {
                /* If optimization is disabled the reports are sent for every 
                   host entry present in the system */

                SnoopSendAllHostReportsOnIccl (u4Instance, pSnoopGroupEntry);
                continue;
            }
            else
            {
                u1RecordType =
                    ((pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
                     SNOOP_IS_INCLUDE : SNOOP_IS_EXCLUDE);

                for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                     u2Index++)
                {
                    if (u1RecordType == SNOOP_IS_INCLUDE)
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopGroupEntry->InclSrcBmp,
                             u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            u2NoSources++;
                        }
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET
                            (pSnoopGroupEntry->ExclSrcBmp,
                             u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            u2NoSources++;
                        }
                    }
                }
            }

        }
        else
        {
            /* Enhanced mode is disabled. Send consolidated report */

            u2NoSources = 0;
            for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
            {
                if (u1RecordType == SNOOP_IS_INCLUDE)
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pIgsConsGroupEntry->InclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pIgsConsGroupEntry->ExclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
            }
        }

        if (u1McLagInterfacePresent == SNOOP_TRUE)
        {
            LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);
            IgsUtilFillV3SourceInfoForIccl (u4Instance,
                                            pSnoopGroupEntry,
                                            u1RecordType, u2NoSources,
                                            &u4MaxLength, u2AdminKey,
                                            u1NonMcLagInterfacePresent,
                                            &SnoopIcchInfo);
        }
        else
        {
            IgsUtilFillV3SourceInfo (u4Instance,
                                     pIgsConsGroupEntry,
                                     u1RecordType, u2NoSources, &u4MaxLength);
        }

        u2GrpRec = 1;

        /* Encode V3 report and forward to member porton which query received */
        if (IgsEncodeAggV3Report (u4Instance, u2GrpRec, u4MaxLength, &pOutBuf)
            == SNOOP_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pTempPortBitmap);
            UtilPlstReleaseLocalPortList (pPortBitmap);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "SnoopSendConsolidatedGroupReportOnIccl: "
                       "V3 Report Encoding failed \r\n");
            return SNOOP_FAILURE;
        }

        SnoopVlanForwardPacket (u4Instance, pIgsVlanEntry->VlanId,
                                SNOOP_ADDR_TYPE_IPV4, SNOOP_INVALID_PORT,
                                VLAN_FORWARD_SPECIFIC, pOutBuf, pPortBitmap,
                                SNOOP_SSMREPORT_SENT);
    }

    UtilPlstReleaseLocalPortList (pTempPortBitmap);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsUtilFillV3SourceInfoForIccl                       */
/*                                                                           */
/* Description        : This function Fills source information in the global */
/*                      buffer for a given group record                      */
/*                                                                           */
/* Input(s)           : pIgsConsGroupEntry - Pointer to the consolidated     */
/*                                           group entry                     */
/*                      u1RecordType   - Group record type to be sent        */
/*                      u2NumSrcs      - Number of sources                   */
/*                      u2AdminKey     - port-channel admin key              */
/*                      u1NonMcLagInterface - interface is non MCLAG         */
/*                      pSnoopIcchInfo - ICCH parameters information         */
/*                                                                           */
/* Output(s)          : pu4MaxLength - pointer to Filled length              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
IgsUtilFillV3SourceInfoForIccl (UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                UINT1 u1RecordType, UINT2 u2NumSrcs,
                                UINT4 *pu4MaxLength, UINT2 u2AdminKey,
                                UINT1 u1NonMcLagInterface,
                                tSnoopIcchFillInfo * pSnoopIcchInfo)
{
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopIgmpGroupRec  IgmpGrpRec;
    UINT4               u4SrcAddr = 0;
    UINT2               u2Count = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));

    IgmpGrpRec.u1RecordType = u1RecordType;

    IgmpGrpRec.u2NumSrcs = SNOOP_HTONS (u2NumSrcs);

    if ((pSnoopGroupEntry == NULL)
        || (pSnoopGroupEntry->pConsGroupEntry == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsUtilFillV3SourceInfoForIccl:  "
                   "Error in getting Group entry\r\n");
        return;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
    {
        if (pSnoopGroupEntry->pConsGroupEntry->u1FilterMode == SNOOP_EXCLUDE)
        {
            if (SNOOP_MEM_CMP
                (pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp, gNullSrcBmp,
                 SNOOP_SRC_LIST_SIZE) == 0)
            {
                IgmpGrpRec.u2NumSrcs = 0;
            }
        }
    }
    else
    {
        /* Enhanced mode is enabled. Fill the mode based on group entry
           instead of consolidated group entry */

        if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) == SNOOP_DISABLE)
        {
            /* If optimization is disabled the reports are sent for every 
               host entry present in the system */

            if (pSnoopIcchInfo->pSnoopHostEntry == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC,
                           "IgsUtilFillV3SourceInfoForIccl:  "
                           "Error in getting Host entry \r\n");
                return;
            }

            /* if V2 host present, encode V1 in Auxiliary data fields  */
            if (pSnoopIcchInfo->pSnoopHostEntry->SnoopHostType ==
                SNOOP_ASM_V1HOST)
            {
                IgmpGrpRec.u1IcchReportVersion = SNOOP_IGMP_V1REPORT;
                IgmpGrpRec.u2NumSrcs = 0;
            }
            /* if V2 host present, encode V2 in Auxiliary data fields  */
            else if (pSnoopIcchInfo->pSnoopHostEntry->SnoopHostType ==
                     SNOOP_ASM_V2HOST)
            {
                IgmpGrpRec.u1IcchReportVersion = SNOOP_IGMP_V2REPORT;
                IgmpGrpRec.u2NumSrcs = 0;
            }

        }
        else
        {
            /* Optimization enabled */
            if (pSnoopGroupEntry->u1FilterMode == SNOOP_EXCLUDE)
            {
                if (SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp, gNullSrcBmp,
                                   SNOOP_SRC_LIST_SIZE) == 0)
                {
                    IgmpGrpRec.u2NumSrcs = 0;
                }
            }

        }

    }
    /* Fill the ICCL report version based on whether V1,V2 report
       present                             */

    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
    {
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortEntry, tSnoopPortEntry *)
        {
            if ((pSnoopASMPortEntry != NULL) &&
                (pSnoopASMPortEntry->u1V1HostPresent == SNOOP_TRUE))
            {
                /* Since a V1 host is present on this port, fill as V1 report */
                IgmpGrpRec.u1IcchReportVersion = SNOOP_IGMP_V1REPORT;
                break;
            }
        }
        /* If V1 host not present, fill as V2 report */
        if (IgmpGrpRec.u1IcchReportVersion != SNOOP_IGMP_V1REPORT)
        {
            IgmpGrpRec.u1IcchReportVersion = SNOOP_IGMP_V2REPORT;
        }

    }

    /* Frame Format 
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |  Record Type  |  Aux Data Len |     Number of Sources (N)     |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Multicast Address                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Source Address [1]                      |
       +-                            ...                              -+
       |                       Source Address [N]                      |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |  Admin Key of ether channel   |learnt on mclag| Version of    |
       |                               | and non-mclag | report        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |Inner-Vlan Id (if Aux Len is 2)|      Reserved                 |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */

    IgmpGrpRec.u1AuxDataLen = SNOOP_OFFSET_ONE;

    /* If Enhanced mode is enabled, fill the inner vlan id
       as first two bytes, and remaining bytes as 0. So the
       auxiliary data is filled as 2 incase of enhanced mode */

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        IgmpGrpRec.u1AuxDataLen = SNOOP_OFFSET_TWO;
    }

    IgmpGrpRec.u4GroupAddress =
        SNOOP_HTONL (SNOOP_PTR_FETCH_4
                     (pSnoopGroupEntry->pConsGroupEntry->GroupIpAddr.au1Addr));

    IgmpGrpRec.u2AuxData = SNOOP_HTONS (u2AdminKey);

    /* Check whether group entry is learnt on both
       MCLAG and other interfaces. If yes fill Iccl
       data as TRUE so that it will be extracted on
       the other end.
     */

    if (u1NonMcLagInterface == SNOOP_TRUE)
    {
        IgmpGrpRec.u1IcclData = SNOOP_TRUE;
    }

    /* Copy the Group information */
    SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                   &IgmpGrpRec, SNOOP_IGMP_SSMGROUP_RECD_SIZE);

    *pu4MaxLength += SNOOP_IGMP_SSMGROUP_RECD_SIZE;

    if (IgmpGrpRec.u2NumSrcs != 0)
    {
        for (u2Count = 1; u2Count <= (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
        {

            if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
            {
                if (pSnoopGroupEntry->pConsGroupEntry->u1FilterMode ==
                    SNOOP_INCLUDE)
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->pConsGroupEntry->
                                             InclSrcBmp, u2Count,
                                             SNOOP_SRC_LIST_SIZE, bResult);
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->pConsGroupEntry->
                                             ExclSrcBmp, u2Count,
                                             SNOOP_SRC_LIST_SIZE, bResult);
                }
            }
            else
            {
                /* If enhanced mode is enabled, sync the group
                   information instead of Consolidated group
                   information
                 */

                if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) ==
                    SNOOP_DISABLE)
                {
                    /* If optimization is disabled the reports are sent for every 
                       host entry present in the system */

                    if (pSnoopIcchInfo->pSnoopHostEntry->pSourceBmp == NULL)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_GRP_TRC,
                                   "IgsUtilFillV3SourceInfoForIccl:  "
                                   "Error in getting Host entry source bit map\r\n");
                        return;
                    }

                    if (u1RecordType == SNOOP_IS_INCLUDE)
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopIcchInfo->
                                                 pSnoopHostEntry->pSourceBmp->
                                                 InclSrcBmp, u2Count,
                                                 SNOOP_SRC_LIST_SIZE, bResult);
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopIcchInfo->
                                                 pSnoopHostEntry->pSourceBmp->
                                                 ExclSrcBmp, u2Count,
                                                 SNOOP_SRC_LIST_SIZE, bResult);
                    }
                }
                else
                {

                    if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->InclSrcBmp,
                                                 u2Count, SNOOP_SRC_LIST_SIZE,
                                                 bResult);
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->ExclSrcBmp,
                                                 u2Count, SNOOP_SRC_LIST_SIZE,
                                                 bResult);
                    }
                }
            }

            if ((bResult == OSIX_TRUE) && (u2Count <= SNOOP_MAX_MCAST_SRCS))
            {
                u4SrcAddr =
                    SNOOP_HTONL (SNOOP_PTR_FETCH_4
                                 (SNOOP_SRC_INFO (u4Instance,
                                                  (u2Count -
                                                   1)).SrcIpAddr.au1Addr));

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                               &u4SrcAddr, SNOOP_IP_ADDR_SIZE);
                *pu4MaxLength += SNOOP_IP_ADDR_SIZE;
            }
        }
    }

    SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                   &(IgmpGrpRec.u2AuxData), SNOOP_OFFSET_TWO);

    *pu4MaxLength += SNOOP_OFFSET_TWO;
    SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                   &(IgmpGrpRec.u1IcclData), SNOOP_OFFSET_ONE);

    *pu4MaxLength += SNOOP_OFFSET_ONE;
    SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                   &(IgmpGrpRec.u1IcchReportVersion), SNOOP_OFFSET_ONE);

    *pu4MaxLength += SNOOP_OFFSET_ONE;

    /* If Enhanced mode is enabled, fill the inner vlan id
       as first two bytes, and remaining bytes as 0. */

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId) =
            SNOOP_HTONS (SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId));
        SNOOP_MEM_CPY (gpSSMRepSendBuffer + *pu4MaxLength,
                       &(SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId)),
                       SNOOP_OFFSET_TWO);

        /*Restore the inner vlan id */
        SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId) =
            SNOOP_HTONS (SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId));

        *pu4MaxLength += SNOOP_AUX_DATA_SIZE;
    }
}

/*****************************************************************************/
/* Function Name      : IgsEncodeV3ReportOnIccl                              */
/*                                                                           */
/* Description        : This function forms the IGMP V3 report to be         */
/*                      sent on ICCL with auxillary data for the grp entry   */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      pIgsConsGroupEntry - Consolidated grp entry          */
/*                      u1RecordType - Record type                           */
/*                      ppOutBuf    - buffer to be sent                      */
/*                      u4PhyPort - Port on which report received            */
/*                      pSnoopIcchInfo - ICCH parameters information         */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
IgsEncodeV3ReportOnIccl (UINT4 u4Instance,
                         tSnoopConsolidatedGroupEntry * pIgsConsGroupEntry,
                         UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER ** ppOutBuf,
                         UINT4 u4PhyPort, tSnoopIcchFillInfo * pSnoopIcchInfo)
{
    tSnoopSSMReport     IgmpV3Hdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tSnoopIgmpGroupRec  IgmpGrpRec;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IgmpPktSize = 0;
    UINT4               u4MaxLength = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2EthType = 0;
    UINT2               u2Index = 0;
    UINT2               u2AdminKey = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&IgmpV3Hdr, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&IgmpGrpRec, 0, sizeof (tSnoopIgmpGroupRec));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if ((pSnoopIcchInfo == NULL) || (pSnoopIcchInfo->pSnoopPktInfo == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeV3ReportOnIccl: "
                   "Error in getting the ICCH information in forming V3 report message \r\n");
        return SNOOP_FAILURE;
    }
    /*
     * For IGMP V3 packets the size depends upon the number of sources for
     * the group.
     * So allocate buf for MAC (14) header + IP (20 bytes)
     *  + IGMP (8) header + Group Record (8) + (No of srcs * 4).
     */

    for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
    {
        if ((u1RecordType == SNOOP_IS_INCLUDE) ||
            (u1RecordType == SNOOP_TO_INCLUDE))
        {
            OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->InclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pIgsConsGroupEntry->ExclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
    }

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IP_HDR_RTR_OPT_LEN +
        SNOOP_IGMP_HEADER_LEN + SNOOP_IGMP_SSMGROUP_RECD_SIZE +
        (UINT4) (u2NoSources * SNOOP_IGMP_SSMSOURCE_RECD_SIZE) +
        SNOOP_AUX_DATA_SIZE;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        u4PktSize += SNOOP_AUX_DATA_SIZE;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "IgsEncodeV3ReportOnIccl: Buffer allocation failure in forming V3 report message\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpV3Hdr.u1PktType = SNOOP_IGMP_V3REPORT;

    IgmpV3Hdr.u1Resvd = 0;

    IgmpV3Hdr.u2CheckSum = 0;
    IgmpV3Hdr.u2Resvd = 0;
    IgmpV3Hdr.u2NumGrps = SNOOP_HTONS (0x0001);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    IgsUtilFillV3SourceInfo (u4Instance, pIgsConsGroupEntry, u1RecordType,
                             u2NoSources, &u4MaxLength);

    CRU_BUF_Copy_OverBufChain (pBuf, gpSSMRepSendBuffer,
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN), u4MaxLength);

    /* Frame Format
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |  Record Type  |  Aux Data Len |     Number of Sources (N)     |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Multicast Address                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Source Address [1]                      |
       +-                            ...                              -+
       |                       Source Address [N]                      |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |  Admin Key of ether channel   |learnt on mclag| Version of    |
       |                               | and non-mclag | report        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |Inner-Vlan Id (if Aux Len is 2)|      Reserved                 |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     */

    /* Add Auxiliary data length in Igs */
    IgmpGrpRec.u1AuxDataLen = SNOOP_OFFSET_ONE;

    /* If Enhanced mode is enabled, fill the inner vlan id
       as first two bytes, and remaining bytes as 0. So the
       auxiliary data is filled as 2 incase of enhanced mode */

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        IgmpGrpRec.u1AuxDataLen = SNOOP_OFFSET_TWO;
    }

    CRU_BUF_Copy_OverBufChain (pBuf, &(IgmpGrpRec.u1AuxDataLen),
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN + SNOOP_OFFSET_ONE),
                               SNOOP_OFFSET_ONE);

    LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);

    IgmpGrpRec.u2AuxData = SNOOP_HTONS (u2AdminKey);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &(IgmpGrpRec.u2AuxData),
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN +
                                SNOOP_IGMP_SSMGROUP_RECD_SIZE),
                               SNOOP_AUX_DATA_SIZE);

    /* If enhanced mode is enabled, then fill the 4th byte of 
       first auxiliary data with the version of the report 
       received.
     */

    CRU_BUF_Copy_OverBufChain (pBuf,
                               (UINT1 *) &(pSnoopIcchInfo->pSnoopPktInfo->
                                           u1PktType),
                               (SNOOP_IGMP_PACKET_START_OFFSET +
                                SNOOP_IGMP_HEADER_LEN +
                                SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                SNOOP_OFFSET_TWO + SNOOP_OFFSET_ONE),
                               SNOOP_OFFSET_ONE);

    u4IgmpPktSize = SNOOP_IGMP_HEADER_LEN + u4MaxLength + SNOOP_AUX_DATA_SIZE;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {

        IgmpGrpRec.u2AuxData =
            SNOOP_HTONS (SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId));

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &(IgmpGrpRec.u2AuxData),
                                   (SNOOP_IGMP_PACKET_START_OFFSET +
                                    SNOOP_IGMP_HEADER_LEN +
                                    SNOOP_IGMP_SSMGROUP_RECD_SIZE +
                                    SNOOP_AUX_DATA_SIZE), SNOOP_AUX_DATA_SIZE);

        u4IgmpPktSize += SNOOP_AUX_DATA_SIZE;
    }

    /* Calculate IGMP checksum */
    IgmpV3Hdr.u2CheckSum =
        IgsUtilIgmpCalcCheckSum (pBuf, u4IgmpPktSize,
                                 SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpV3Hdr.u2CheckSum = SNOOP_HTONS (IgmpV3Hdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET +
                               SNOOP_OFFSET_TWO, SNOOP_OFFSET_TWO);

    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }

    /* If MCLAG IP based optimization is disabled, the source IP is filled
       as same from which the report message is arrived. 
     */
    if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) == SNOOP_DISABLE)
    {
        u4SrcAddr =
            SNOOP_PTR_FETCH_4 (pSnoopIcchInfo->pSnoopPktInfo->SrcIpAddr.
                               au1Addr);
    }

    /* Form IP Header */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr,
                         SNOOP_PTR_FETCH_4 (pIgsConsGroupEntry->GroupIpAddr.
                                            au1Addr), SNOOP_IGMP_V3REPORT,
                         (UINT2) u4IgmpPktSize);

    /* Fill MAC Header, v3 Reports are sent to 224.0.0.22 (All IGMP v3 routers)
     * fill corresponding Dest MAC address */
    SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_IGMPV3_ROUTERS, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */

#ifndef SRCMAC_CTP__WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : IgsEncodeAggV3ReportOnIccl                           */
/*                                                                           */
/* Description        : This function forms the consolidated IGMP V3 report  */
/*                      to be sent on ICCL with auxillary data               */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u2NumGrpRec - Number of groups                       */
/*                      u4MaxLength - Packet size                            */
/*                      pSnoopIcchInfo - ICCH parameters information         */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
IgsEncodeAggV3ReportOnIccl (UINT4 u4Instance, UINT2 u2NumGrpRec,
                            UINT4 u4MaxLength,
                            tCRU_BUF_CHAIN_HEADER ** ppOutBuf, UINT4 u4PhyPort,
                            tSnoopIcchFillInfo * pSnoopIcchInfo)
{
    tSnoopSSMReport     IgmpV3Hdr;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4IgmpPktSize = 0;
    UINT4               u4SrcAddr = 0;
    UINT4               u2PktSize = 0;
    UINT2               u2EthType = 0;
    UINT2               u2Count = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2LinearBufOffSet = 0;
    UINT2               u2GrpRecrdSize = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2AdminKey = 0;
    UINT1               u1AuxDataLen = 0;

    SNOOP_MEM_SET (&IgmpV3Hdr, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if ((pSnoopIcchInfo == NULL) || (pSnoopIcchInfo->pSnoopPktInfo == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "IgsEncodeAggV3ReportOnIccl: "
                   "Error in getting the ICCH information in forming V3 report message \r\n");
        return SNOOP_FAILURE;
    }
    /* If the mode is MAC 14 (MAC) + 24 (Ip with Router Alert option)
     * + 8 (IGMP) +  group record (s) size  */
    u2PktSize = (SNOOP_DATA_LINK_HDR_LEN +
                 (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
                 SNOOP_IP_HDR_RTR_OPT_LEN + SNOOP_IGMP_HEADER_LEN +
                 u4MaxLength);

    pBuf = CRU_BUF_Allocate_MsgBufChain (u2PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_IGS_DBG_FLAG, SNOOP_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "IgsEncodeAggV3ReportOnIccl: Buffer allocation failure in forming V3 report message\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_BUFF,
                          SNOOP_BUFFER_TRC,
                          SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    IgmpV3Hdr.u1PktType = SNOOP_IGMP_V3REPORT;

    IgmpV3Hdr.u1Resvd = 0;

    IgmpV3Hdr.u2CheckSum = 0;
    IgmpV3Hdr.u2Resvd = 0;
    IgmpV3Hdr.u2NumGrps = SNOOP_HTONS (u2NumGrpRec);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr,
                               SNOOP_IGMP_PACKET_START_OFFSET,
                               SNOOP_IGMP_HEADER_LEN);

    /* Move the offset to start of IGMP field */
    u2OffSet = (SNOOP_IGMP_PACKET_START_OFFSET + SNOOP_IGMP_HEADER_LEN);

    for (u2Count = 0; u2Count < u2NumGrpRec; u2Count++)
    {
        /* Copy each group record and add ICCL data into it */

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_MEM_CPY (&u2NoSources,
                           (gpSSMRepSendBuffer + (u2LinearBufOffSet) +
                            (SNOOP_OFFSET_TWO)), SNOOP_OFFSET_TWO);
            u2NoSources = SNOOP_HTONS (u2NoSources);
            u2GrpRecrdSize = (UINT2) ((SNOOP_IGMP_SSMGROUP_RECD_SIZE) +
                                      (u2NoSources *
                                       SNOOP_IGMP_SSMSOURCE_RECD_SIZE));
        }
        else
        {
            u2GrpRecrdSize = SNOOP_IGMP_SSMGROUP_RECD_SIZE;
        }

        CRU_BUF_Copy_OverBufChain (pBuf,
                                   (gpSSMRepSendBuffer + u2LinearBufOffSet),
                                   u2OffSet, u2GrpRecrdSize);

        /* Frame Format
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           |  Record Type  |  Aux Data Len |     Number of Sources (N)     |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           |                       Multicast Address                       |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           |                       Source Address [1]                      |
           +-                            ...                              -+
           |                       Source Address [N]                      |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           |  Admin Key of ether channel   |learnt on mclag| Version of    |
           |                               | and non-mclag | report        |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
           |Inner-Vlan Id (if Aux Len is 2)|      Reserved                 |
           +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
         */

        /* Fill the auxillary data length as 1 */
        u2OffSet = (UINT2) (u2OffSet + SNOOP_OFFSET_ONE);

        u1AuxDataLen = SNOOP_OFFSET_ONE;

        /* If Enhanced mode is enabled, fill the inner vlan id
           as first two bytes, and remaining bytes as 0. So the
           auxiliary data is filled as 2 incase of enhanced mode */

        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
        {
            u1AuxDataLen = SNOOP_OFFSET_TWO;
        }

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u1AuxDataLen,
                                   u2OffSet, SNOOP_OFFSET_ONE);
        /* Reset the offset to point it to filter mode */
        u2OffSet = (UINT2) (u2OffSet - SNOOP_OFFSET_ONE);

        /* Fill the port-channel key for MCLAG */
        LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);

        u2AdminKey = SNOOP_HTONS (u2AdminKey);
        u2OffSet = (UINT2) (u2OffSet + u2GrpRecrdSize);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                   u2OffSet, SNOOP_OFFSET_TWO);

        u2OffSet = (UINT2) (u2OffSet + SNOOP_OFFSET_TWO);

        /* Fill ICCL information as 0 */
        u2AdminKey = 0;
        u2AdminKey = SNOOP_HTONS (u2AdminKey);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                   u2OffSet, SNOOP_OFFSET_TWO);

        u2OffSet = (UINT2) (u2OffSet + SNOOP_OFFSET_TWO);

        /* If Enhanced mode is enabled, fill the inner vlan id
           as first two bytes, and remaining bytes as 0 */

        if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
        {
            u2AdminKey = 0;
            u2AdminKey =
                SNOOP_HTONS (SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId));

            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                       u2OffSet, SNOOP_OFFSET_TWO);

            u2OffSet = (UINT2) (u2OffSet + SNOOP_OFFSET_TWO);

            /* Fill reserved bytes as 0 */
            u2AdminKey = 0;
            u2AdminKey = SNOOP_HTONS (u2AdminKey);

            CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                       u2OffSet, SNOOP_OFFSET_TWO);

            u2OffSet = (UINT2) (u2OffSet + SNOOP_OFFSET_TWO);
        }

        u2LinearBufOffSet = (UINT2) (u2LinearBufOffSet + u2GrpRecrdSize);

    }

    u4IgmpPktSize = SNOOP_IGMP_HEADER_LEN + u4MaxLength;
    /* Calculate IGMP checksum */
    IgmpV3Hdr.u2CheckSum =
        IgsUtilIgmpCalcCheckSum (pBuf, u4IgmpPktSize,
                                 SNOOP_IGMP_PACKET_START_OFFSET);

    IgmpV3Hdr.u2CheckSum = SNOOP_HTONS (IgmpV3Hdr.u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &IgmpV3Hdr.u2CheckSum,
                               SNOOP_IGMP_PACKET_START_OFFSET + 2, 2);

    /* Assign 0.0.0.0 for Source IP as per standard */
    u4SrcAddr = 0;
    /* Source IP address will be set to Switch IP in Proxy mode */
    if (SNOOP_PROXY_STATUS
        (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1) == SNOOP_ENABLE)
    {
        u4SrcAddr = gu4SwitchIp;
    }

    /* If MCLAG IP based optimization is disabled, the source IP is filled
       as same from which the report message is arrived.
     */
    if (SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE (u4Instance) == SNOOP_DISABLE)
    {
        u4SrcAddr =
            SNOOP_PTR_FETCH_4 (pSnoopIcchInfo->pSnoopPktInfo->SrcIpAddr.
                               au1Addr);
    }

    /* Form IP Header , V3 Report should go to All V3 Routers 224.0.0.22 */
    IgsUtilIpFormPacket (pBuf, u4SrcAddr, SNOOP_ALL_IGMPV3_ROUTERS,
                         SNOOP_IGMP_V3REPORT, (UINT2) u4IgmpPktSize);

    /* Fill MAC Header */
    SNOOP_GET_MAC_FROM_IPV4 (SNOOP_ALL_IGMPV3_ROUTERS, MacGroupAddr);

    SNOOP_MEM_SET (SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

#ifndef SRCMAC_CTP_WANTED
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);
#endif

    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IP_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchSyncLeaveMessage                            */
/*                                                                           */
/* Description        :  This function will send leave sync to peer          */
/*                                                                           */
/* Input(s)           : GrpAddr - Addr of group to which leave has been      */
/*                      received                                             */
/*                      pSnoopIcchInfo - ICCH parameters information         */
/*                      u2AdminKey - AdminKey of port-channel                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopIcchSyncLeaveMessage (UINT4 u4Instance, tIPvXAddr GrpAddr,
                           tSnoopIcchFillInfo * pSnoopIcchInfo,
                           UINT2 u2AdminKey)
{
    tIcchMsg           *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2MsgLen = 0;

    u2MsgLen =
        SNOOP_ICCH_TYPE_FIELD_SIZE + SNOOP_ICCH_LEN_FIELD_SIZE +
        sizeof (tIPvXAddr) + SNOOP_ICCH_SYNC_LEAVE_DATA_SIZE;

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        u2MsgLen = (UINT2) (u2MsgLen + SNOOP_OFFSET_TWO);
    }

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC,
                       "SnoopIcchSyncLeaveMessage: ICCH alloc failed. Leave sync up message"
                       "not sent \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_ICCH_PUT_1_BYTE (pMsg, &u4OffSet, SNOOP_ICCH_SYNC_LEAVE);
    SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    SNOOP_ICCH_PUT_N_BYTE (pMsg, &GrpAddr, &u4OffSet, sizeof (tIPvXAddr));
    SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet, u2AdminKey);
    SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet,
                           SNOOP_OUTER_VLAN (pSnoopIcchInfo->VlanId));

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet,
                               SNOOP_INNER_VLAN (pSnoopIcchInfo->VlanId));
    }

    SnoopIcchSendMsgToIcch (pMsg, (UINT2) u4OffSet);

    SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_ICCH_TRC,
                   "SnoopIcchSyncLeaveMessage: Leave Message enqueued to ICCH module \r\n");

    return SNOOP_SUCCESS;

}

#ifdef MLDS_WANTED
/*****************************************************************************/
/* Function Name      : MldsEncodeV2ReportOnIccl                             */
/*                                                                           */
/* Description        : This function frames the MLD V2 packet for the Group */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                      u1RecordType - MLD V2 packet type                    */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeV2ReportOnIccl (UINT4 u4Instance,
                          tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry,
                          UINT1 u1RecordType, tCRU_BUF_CHAIN_HEADER ** ppOutBuf,
                          UINT4 u4PhyPort)
{
    tSnoopSSMReport     SSMReport;
    tSnoopMLDGroupRec   MldGroupRec;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MaxLength = 0;
    UINT4               u4MldPktSize = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2EthType = 0;
    UINT2               u2Index = 0;
    UINT2               u2AdminKey = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               au1Dst[IPVX_IPV6_ADDR_LEN];

    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MldGroupRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1Dst, 0, IPVX_IPV6_ADDR_LEN);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
    {
        if ((u1RecordType == SNOOP_IS_INCLUDE) ||
            (u1RecordType == SNOOP_TO_INCLUDE))
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->InclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->ExclSrcBmp, u2Index,
                                     SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                u2NoSources++;
            }
        }
    }

    /*
     * For MLDS packets the size depends upon the number of sources for
     * the group.
     * So allocate buf for MAC (14) header + IPV6 with router alert
     * option (48 bytes)
     *  + MLD (24) header + Group Record (20) + (No of srcs * 16).
     */

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLD_HDR_LEN +
        SNOOP_MLD_SSMGROUP_RECD_SIZE +
        (u2NoSources * SNOOP_MLD_SSMSOURCE_RECD_SIZE) + SNOOP_AUX_DATA_SIZE;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, MLDS_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "MldsEncodeV2ReportOnIccl: Buffer allocation failure in MLD V2 Aggregation report "
                   "formation\r\n");
        return SNOOP_FAILURE;
    }

    SSMReport.u1PktType = SNOOP_MLD_V2REPORT;

    SSMReport.u2NumGrps = SNOOP_HTONS (0x0001);

    /* Copy MLD v2 header information */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLD_HDR_LEN);

    /* Fill group and source records information in gpSSMRepSendBuffer */
    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    MldsUtilFillV2SourceInfo (u4Instance, pSnoopConsGroupEntry, u2NoSources,
                              u1RecordType, &u4MaxLength);

    /* Copy group and source records information */
    CRU_BUF_Copy_OverBufChain (pBuf, gpSSMRepSendBuffer,
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLDV2_HDR_LEN), u4MaxLength);

    /* Auxiliary data length is filled interms of 4 byte words.
       The first two fields are filled with port-channel key.
       The third field is set to 1 if the group entry is learnt
       on both mc-lag and non mc-lag interface.
       The fourth field is reserved */

    MldGroupRec.u1AuxDataLen = 1;
    CRU_BUF_Copy_OverBufChain (pBuf, &(MldGroupRec.u1AuxDataLen),
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLDV2_HDR_LEN + SNOOP_OFFSET_ONE),
                               SNOOP_OFFSET_ONE);

    LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);
    MldGroupRec.u2AuxData = SNOOP_HTONS (u2AdminKey);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &(MldGroupRec.u2AuxData),
                               (SNOOP_MLD_PACKET_START_OFFSET +
                                SNOOP_MLDV2_HDR_LEN +
                                SNOOP_MLD_SSMGROUP_RECD_SIZE),
                               SNOOP_AUX_DATA_SIZE);

    u4MldPktSize = SNOOP_MLDV2_HDR_LEN + u4MaxLength + SNOOP_AUX_DATA_SIZE;

    /* Fill MAC Header, v2 Reports are sent to 33:33::16 (All MLD V2 routers)
     * fill corresponding Dest MAC address */
    SNOOP_MEM_CPY (au1Dst, gau1MldsAllMldv2Rtr, IPVX_IPV6_ADDR_LEN);

    SNOOP_INET_NTOHL (au1Dst);

    /* Calculate MLD checksum */
    SSMReport.u2CheckSum = MldsUtilCalculateCheckSum (pBuf, u4MldPktSize,
                                                      SNOOP_MLD_PACKET_START_OFFSET,
                                                      gNullSrcAddr, au1Dst);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPV6 Header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr,
                              pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                              SNOOP_MLD_V2REPORT, (UINT2) u4MldPktSize);

    SNOOP_GET_MAC_FROM_IPV6 (au1Dst, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Form ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsEncodeAggV2ReportOnIccl                          */
/*                                                                           */
/* Description        : This function forms the consolidated MLD V2 report   */
/*                      to be sent on ICCL with auxillary data               */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      u2NumGrpRec - Number of groups                       */
/*                      u4MaxLength - Packet size                            */
/*                                                                           */
/* Output(s)          : ppOutBuf -  pointer to buffer                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
MldsEncodeAggV2ReportOnIccl (UINT4 u4Instance, UINT2 u2NumGrpRec,
                             UINT4 u4MaxLength,
                             tCRU_BUF_CHAIN_HEADER ** ppOutBuf, UINT4 u4PhyPort)
{
    tSnoopSSMReport     SSMReport;
    tSnoopMLDGroupRec   MldGroupRec;
    tSnoopMacAddr       MacGroupAddr;
    tSnoopMacAddr       SrcMacAddr;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4MldPktSize = 0;
    UINT4               u4PktSize = 0;
    UINT2               u2EthType = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2AdminKey = 0;
    UINT2               u2Count = 0;
    UINT1               au1DstIpAddr[IPVX_IPV6_ADDR_LEN];
    UINT1               u1AuxDataLen = 0;

    SNOOP_MEM_SET (&SSMReport, 0, sizeof (tSnoopSSMReport));
    SNOOP_MEM_SET (&MldGroupRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1DstIpAddr, 0, IPVX_IPV6_ADDR_LEN);

    /*
     * For MLDS packets the size depends upon the number of sources for
     * the group.
     * So allocate buf for MAC (14) header + IPV6 with router alert
     * option (48 bytes)
     *  + MLD (24) header + Group Record (20) + (No of srcs * 16).
     */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_CPY (au1DstIpAddr, gau1MldsAllMldv2Rtr, IPVX_IPV6_ADDR_LEN);

    SNOOP_INET_NTOHL (au1DstIpAddr);

    u4PktSize = SNOOP_DATA_LINK_HDR_LEN +
        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) +
        SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_MLDV2_HDR_LEN + u4MaxLength;

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4PktSize, 0);

    if (pBuf == NULL)
    {
        SNOOP_DBG (SNOOP_MLDS_DBG_FLAG, MLDS_BUFFER_TRC, SNOOP_OS_RES_DBG,
                   "MldsEncodeAggV2ReportOnIccl: Buffer allocation failure in MLD V2 Aggregation report "
                   "formation\r\n");
        return SNOOP_FAILURE;
    }

    SSMReport.u1PktType = SNOOP_MLD_V2REPORT;
    SSMReport.u2NumGrps = SNOOP_HTONS (u2NumGrpRec);

    /* Copy MLD v2 header information */
    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport,
                               SNOOP_MLD_PACKET_START_OFFSET,
                               SNOOP_MLDV2_HDR_LEN);

    for (u2Count = 0; u2Count < u2NumGrpRec; u2Count++)
    {
        /* Copy each group record and add ICCL data into it */
        CRU_BUF_Copy_OverBufChain (pBuf,
                                   (gpSSMRepSendBuffer +
                                    (u2Count * SNOOP_MLD_SSMGROUP_RECD_SIZE)),
                                   (SNOOP_MLD_PACKET_START_OFFSET +
                                    SNOOP_MLDV2_HDR_LEN + u2OffSet),
                                   SNOOP_MLD_SSMGROUP_RECD_SIZE);

        /* Fill the auxillary data length as 1 */
        u2OffSet = (u2OffSet + 1);
        u1AuxDataLen = 1;
        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u1AuxDataLen,
                                   (SNOOP_MLD_PACKET_START_OFFSET +
                                    SNOOP_MLDV2_HDR_LEN + u2OffSet),
                                   SNOOP_OFFSET_ONE);
        /* Reset the offset to point it to filter mode */
        u2OffSet = (u2OffSet - 1);

        /* Fill the port-channel key for MCLAG */
        LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);

        u2AdminKey = SNOOP_HTONS (u2AdminKey);
        u2OffSet += SNOOP_MLD_SSMGROUP_RECD_SIZE;

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                   (SNOOP_MLD_PACKET_START_OFFSET +
                                    SNOOP_MLDV2_HDR_LEN + u2OffSet),
                                   SNOOP_OFFSET_TWO);

        u2OffSet += SNOOP_OFFSET_TWO;
        u2AdminKey = 0;
        u2AdminKey = SNOOP_HTONS (u2AdminKey);

        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2AdminKey,
                                   (SNOOP_MLD_PACKET_START_OFFSET +
                                    SNOOP_MLDV2_HDR_LEN + u2OffSet),
                                   SNOOP_OFFSET_TWO);

        u2OffSet += SNOOP_OFFSET_TWO;

    }

    u4MldPktSize = SNOOP_MLDV2_HDR_LEN + u4MaxLength;

    /* Calculate MLD checksum */
    SSMReport.u2CheckSum = MldsUtilCalculateCheckSum (pBuf, u4MldPktSize,
                                                      SNOOP_MLD_PACKET_START_OFFSET,
                                                      gNullSrcAddr,
                                                      au1DstIpAddr);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &SSMReport.u2CheckSum,
                               (SNOOP_MLD_PACKET_START_OFFSET + 2),
                               sizeof (UINT2));

    /* Form IPV6 Header */
    MldsUtilEncodeIpv6Packet (pBuf, gNullSrcAddr, gau1MldsAllMldv2Rtr,
                              SNOOP_MLD_V2REPORT, (UINT2) u4MldPktSize);

    /* Fill MAC Header, v2 Reports are sent to 33:33::16 (All MLD V2 routers)
     * fill corresponding Dest MAC address */
    SNOOP_GET_MAC_FROM_IPV6 (au1DstIpAddr, MacGroupAddr);

    SNOOP_MEM_SET (&SrcMacAddr, 0, SNOOP_ETH_ADDR_SIZE);

    /* Set the Switch base MAC Address as Source MAC address */
    SnoopGetSysMacAddress (u4Instance, SrcMacAddr);

    /* Form ethernet header */
    CRU_BUF_Copy_OverBufChain (pBuf, MacGroupAddr, 0, SNOOP_ETH_ADDR_SIZE);
    CRU_BUF_Copy_OverBufChain (pBuf, SrcMacAddr, SNOOP_ETH_ADDR_SIZE,
                               SNOOP_ETH_ADDR_SIZE);

    u2EthType = SNOOP_HTONS (SNOOP_IPV6_PKT_TYPE);

    CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2EthType,
                               SNOOP_ETH_TYPE_OFFSET, SNOOP_ETH_TYPE_SIZE);
    *ppOutBuf = pBuf;

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MldsUtilFillV3SourceInfoForIccl                      */
/*                                                                           */
/* Description        : This function Fills source information in the global */
/*                      buffer for a given group record                      */
/*                                                                           */
/* Input(s)           : pSnoopConsGroupEntry- Pointer to the consolidated    */
/*                                           group entry                     */
/*                      u1RecordType   - Group record type to be sent        */
/*                      u2NumSrcs      - Number of sources                   */
/*                      u2AdminKey     - port-channel admin key              */
/*                      u1NonMcLagInterface - interface is non MCLAG         */
/*                                                                           */
/* Output(s)          : pu4MaxLength - pointer to Filled length              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
MldsUtilFillV2SourceInfoOnIccl (UINT4 u4Instance,
                                tSnoopConsolidatedGroupEntry *
                                pSnoopConsGroupEntry, UINT1 u1RecordType,
                                UINT2 u2NumSrcs, UINT4 *pu4MaxLength,
                                UINT2 u2AdminKey, UINT1 u1NonMcLagInterface)
{
    tSnoopMLDGroupRec   MldGrpRec;
    UINT2               u2Count = 0;
    UINT1               SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&MldGrpRec, 0, sizeof (tSnoopMLDGroupRec));
    SNOOP_MEM_SET (SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    /* Copy group record information to the global MLD v2 send buffer */
    MldGrpRec.u1RecordType = u1RecordType;
    MldGrpRec.u2NumSrcs = SNOOP_HTONS (u2NumSrcs);
    MldGrpRec.u1AuxDataLen = 1;

    SNOOP_MEM_CPY (&MldGrpRec.u1GrpAddr,
                   pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (MldGrpRec.u1GrpAddr);

    SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength), &MldGrpRec,
                   SNOOP_MLD_SSMGROUP_RECD_SIZE);

    *pu4MaxLength += SNOOP_MLD_SSMGROUP_RECD_SIZE;

    /* for MAC based forwarding source count will be zero */
    if (MldGrpRec.u2NumSrcs != 0)
    {
        for (u2Count = 1; u2Count <= (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
        {
            u1RecordType = pSnoopConsGroupEntry->u1FilterMode;

            /* Sources can be taken based on the Incl/Excl bitmap set
             * for the group. This can be done based on the filter mode
             * set for the group entry  */
            if (u1RecordType == SNOOP_INCLUDE)
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->InclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopConsGroupEntry->ExclSrcBmp,
                                         u2Count, SNOOP_SRC_LIST_SIZE, bResult);
            }
            /* copy the source info to the global MLD v2 send buffer */
            if ((bResult == OSIX_TRUE) && (u2Count <= SNOOP_MAX_MCAST_SRCS))
            {
                SNOOP_MEM_CPY (SrcAddr,
                               SNOOP_SRC_INFO (u4Instance,
                                               (u2Count - 1)).SrcIpAddr.au1Addr,
                               IPVX_IPV6_ADDR_LEN);

                SNOOP_INET_HTONL (SrcAddr);

                SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength),
                               SrcAddr, IPVX_MAX_INET_ADDR_LEN);

                *pu4MaxLength += IPVX_MAX_INET_ADDR_LEN;
            }
        }
    }

    /* Fill admin key and iccl data as auxillary data */
    u2AdminKey = SNOOP_HTONS (u2AdminKey);

    SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength),
                   &u2AdminKey, SNOOP_OFFSET_TWO);

    *pu4MaxLength += SNOOP_OFFSET_TWO;

    /* Check whether group entry is learnt on both
       MCLAG and other interfaces. If yes fill Iccl
       data as TRUE so that it will be extracted on
       the other end.
     */

    if (u1NonMcLagInterface == SNOOP_TRUE)
    {
        MldGrpRec.u1IcclData = SNOOP_TRUE;
    }

    SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength),
                   &(MldGrpRec.u1IcclData), SNOOP_OFFSET_ONE);

    *pu4MaxLength += SNOOP_OFFSET_ONE;
    /*Fill one byte reserved */
    MldGrpRec.u1IcclData = SNOOP_INIT_VAL;

    SNOOP_MEM_CPY ((gpSSMRepSendBuffer + *pu4MaxLength),
                   &(MldGrpRec.u1IcclData), SNOOP_OFFSET_ONE);
    *pu4MaxLength += SNOOP_OFFSET_ONE;
}

#endif
/*****************************************************************************/
/* Function Name      : SnoopIcchHandleUpdates                               */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      ICCH module. This function invokes appropriate fns.  */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : pMsg - Pointer to tIcchMsg.                          */
/*                      u2DataLen - Data length.                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

VOID
SnoopIcchHandleUpdates (tIcchMsg * pMsg, UINT2 u2DataLen)
{
    tIcchProtoEvt       ProtoEvt;
    tIPvXAddr           GrpAddr;
    UINT2               u2MinLen = 0;
    UINT4               u4OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2AdminKey = 0;
    tSnoopTag           VlanId = { 0 };
    UINT1               u1MsgType = 0;

    MEMSET (&ProtoEvt, 0, sizeof (tIcchProtoEvt));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));

    ProtoEvt.u4AppId = ICCH_SNOOP_APP_ID;
    u2MinLen = SNOOP_ICCH_TYPE_FIELD_SIZE + SNOOP_ICCH_LEN_FIELD_SIZE;

    SNOOP_ICCH_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    SNOOP_ICCH_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u2DataLen < u2MinLen)
    {
        /* doesnot have room for type and length field itself.
         *         **/
        return;
    }

    switch (u1MsgType)
    {
        case SNOOP_ICCH_SYNC_LEAVE:

            SNOOP_ICCH_GET_N_BYTE (pMsg, &GrpAddr, &u4OffSet,
                                   sizeof (tIPvXAddr));
            SNOOP_ICCH_GET_2_BYTE (pMsg, &u4OffSet, u2AdminKey);
            SNOOP_ICCH_GET_2_BYTE (pMsg, &u4OffSet, SNOOP_OUTER_VLAN (VlanId));

            if (SNOOP_INSTANCE_ENH_MODE (SNOOP_DEFAULT_INSTANCE) ==
                SNOOP_ENABLE)
            {
                SNOOP_ICCH_GET_2_BYTE (pMsg, &u4OffSet,
                                       SNOOP_INNER_VLAN (VlanId));
            }

            SnoopProcessLeaveSyncMessage (GrpAddr, u2AdminKey, VlanId);
            break;

        case SNOOP_ICCH_SYNC_CONS_REPORT:
            SNOOP_ICCH_GET_2_BYTE (pMsg, &u4OffSet, SNOOP_OUTER_VLAN (VlanId));
            SnoopIcchProcessPeerUp (SNOOP_OUTER_VLAN (VlanId));
            break;

        default:
            break;
    }                            /* End of switch */
    return;
}

/****************************************************************************/
/* Function Name         :   SnoopCheckBitMapForIcclInterface               */
/*                                                                          */
/* Description           :   Checks port bit consists of ICCL link as a     */
/*                           member port                                    */
/*                                                                          */
/* Input (s)             :   PortBitMap  - Group ports bitmap               */
/*                           u4InstanceId - Instance Id                     */
/*                                                                          */
/* Output (s)            :  None                                            */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopCheckBitMapForIcclInterface (tSnoopPortBmp PortBitmap, UINT4 u4InstanceId)
{
    UINT2               u2BytIndex = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;
    UINT2               u2Port = 0;
    UINT4               u4CheckPort = 0;

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* Check whether physical port is ICCL interface */
                    u4CheckPort = SNOOP_GET_IFINDEX (u4InstanceId, u2Port);
                    if (IcchIsIcclInterface (u4CheckPort) == OSIX_TRUE)
                    {
                        return SNOOP_SUCCESS;
                    }
                }
            }

            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    return SNOOP_FAILURE;
}

/****************************************************************************/
/* Function Name         :   SnoopSendGeneralQueryOnIccl                    */
/*                                                                          */
/* Description           :   Sends a consolidated query on ICCL interface   */
/*                                                                          */
/* Input (s)             :   u2IcclPort  - ICCL port                        */
/*                           u4Instance  - Instance id                      */
/*                           VlanId      - Vlan Id                          */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                        */
/****************************************************************************/

INT4
SnoopSendGeneralQueryOnIccl (UINT2 u2LocalIcclPort, UINT4 u4Instance,
                             tSnoopTag VlanId)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pPortBitMap = NULL;
    UINT1               u1AddressType = SNOOP_ADDR_TYPE_IPV4;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_MGMT,
                       SNOOP_MGMT_NAME, "Instance not created \r\n");
        return SNOOP_SUCCESS;
    }

    if (SNOOP_SNOOPING_STATUS (u4Instance, (SNOOP_ADDR_TYPE_IPV4 - 1))
        == SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV4;
    }

    if (SNOOP_SNOOPING_STATUS (u4Instance, (SNOOP_ADDR_TYPE_IPV6 - 1)) ==
        SNOOP_ENABLE)
    {
        u1AddressType = SNOOP_ADDR_TYPE_IPV6;
    }

    if (SNOOP_SNOOPING_STATUS (u4Instance, (u1AddressType - 1)) ==
        SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendGeneralQueryOnIccl: Snoop is disabled. \r\n");
        return SNOOP_SUCCESS;
    }

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendGeneralQueryOnIccl: Vlan Entry not present \r\n");
        return SNOOP_FAILURE;
    }

#ifdef IGS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (IgsEncodeQuery (u4Instance, 0, SNOOP_IGMP_QUERY,
                            pSnoopVlanEntry->pSnoopVlanCfgEntry->
                            u1ConfigOperVer, &pBuf,
                            pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GRP_TRC,
                       "SnoopSendGeneralQueryOnIccl: Query Encoding failed\n");

            return SNOOP_FAILURE;
        }
    }
#endif
#ifdef MLDS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (MldsEncodeQuery (u4Instance, NULL, SNOOP_MLD_QUERY,
                             SNOOP_MLD_VERSION2, &pBuf, pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GRP_TRC,
                       "SnoopSendGeneralQueryOnIccl: Query Encoding failed.\n");
            return SNOOP_FAILURE;
        }
    }
#endif

    pPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitMap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendGeneralQueryOnIccl: Error in allocating memory for pPortBitMap\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitMap, 0, sizeof (tSnoopPortBmp));

    SNOOP_ADD_TO_PORT_LIST (u2LocalIcclPort, pPortBitMap);

    SnoopVlanForwardPacket (u4Instance, VlanId, u1AddressType,
                            0, VLAN_FORWARD_SPECIFIC, pBuf,
                            pPortBitMap, SNOOP_QUERY_SENT);
    UtilPlstReleaseLocalPortList (pPortBitMap);

    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopIcchSyncMclagStatusChange                           */
/*                                                                           */
/* Description        :  This function will send peer-up sync to peer        */
/*                                                                           */
/* Input(s)           : u2VlanId - Vlan ID                                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopIcchSyncMclagStatusChange (UINT2 u2VlanId)
{
    tIcchMsg           *pMsg = NULL;
    UINT4               u4OffSet = 0;
    UINT2               u2MsgLen = 0;

    u2MsgLen = SNOOP_ICCH_TYPE_FIELD_SIZE + SNOOP_ICCH_LEN_FIELD_SIZE + 2;

    if ((pMsg = ICCH_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_ICCH_TRC,
                       "SnoopIcchSyncMclagStatusChange: ICCH alloc failed. MCLAG status sync up message"
                       "not sent \r\n");
        return SNOOP_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (u2MsgLen));
    SNOOP_ICCH_PUT_1_BYTE (pMsg, &u4OffSet, SNOOP_ICCH_SYNC_CONS_REPORT);
    SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet, u2MsgLen);
    SNOOP_ICCH_PUT_2_BYTE (pMsg, &u4OffSet, u2VlanId);

    SnoopIcchSendMsgToIcch (pMsg, (UINT2) u4OffSet);

    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopUpdateGroupEntryInEnhMode                       */
/*                                                                           */
/* Description        : This function will update the data structures based  */
/*                      on the received report version from remote node.     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Group entry                       */
/*                      pSnoopPktInfo - Packet Information                   */
/*                      pSnoopPortCfgEntry - Port entry information          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopUpdateGroupEntryInEnhMode (UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    UINT1               u1PktType = 0;

    if ((pSnoopGroupEntry == NULL) || (pSnoopPktInfo == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "SnoopUpdateGroupEntryInEnhMode: Error in getting the Group entry \r\n");
        return SNOOP_FAILURE;
    }

    /* MAC Based forwarding mode */
    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

    /* Store the Original Packet Type and replace it with the ICCL 
       report received version. Once updation done restore the 
       packet type
     */

    u1PktType = pSnoopPktInfo->u1PktType;

    pSnoopPktInfo->u1PktType = pSnoopPktInfo->u1IcchReportVersion;

    if (SnoopGrpUpdatePortToGroupEntry (u4Instance, pSnoopGroupEntry,
                                        pSnoopPktInfo,
                                        pSnoopPortCfgEntry, SNOOP_FALSE) !=
        SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                        SNOOP_GRP_TRC,
                        "Updation of port entry to Group failed for group %s\r\n",
                        SnoopPrintIPvxAddress (pSnoopGroupEntry->GroupIpAddr));
        SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                   SNOOP_DELETE_GRP_ENTRY);
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                      (tRBElem *) pSnoopGroupEntry);
        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
        /* Restore the packet type */
        pSnoopPktInfo->u1PktType = u1PktType;
        return SNOOP_FAILURE;
    }

    /* Restore the packet type */
    pSnoopPktInfo->u1PktType = u1PktType;

    SnoopUpdateConsGrpOnFilterChange (u4Instance, SNOOP_TO_EXCLUDE,
                                      pSnoopGroupEntry->pConsGroupEntry,
                                      SNOOP_FALSE);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchUtilGetHostEntry                            */
/*                                                                           */
/* Description        : This function returns if the host is present or not  */
/*                      in the global RB Tree                                */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      pSnoopPktInfo   - Pointer to packet information      */
/*                                                                           */
/* Output(s)          : ppRetSnoopHostNode - pointer to the host node        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopIcchUtilGetHostEntry (UINT4 u4Instance,
                           tSnoopGroupEntry * pSnoopGroupEntry,
                           tSnoopPktInfo * pSnoopPktInfo)
{
    tRBElem            *pRBElem = NULL;
    tSnoopPortEntry    *pSnoopPortNode = NULL;
    tSnoopHostEntry     SnoopHostEntry;
    BOOL1               bResult = OSIX_FALSE;

    if ((pSnoopGroupEntry == NULL) || (pSnoopPktInfo == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_OS_RES_DBG,
                   "SnoopIcchUtilGetHostEntry: Error in getting the Group entry \r\n");
        return SNOOP_FAILURE;
    }

    /* Check whether the port is already present in the Group Entry's
     * ASM Port Bitmap */
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopGroupEntry->ASMPortBitmap, bResult);

    if (bResult == OSIX_TRUE)
    {
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopPortNode, tSnoopPortEntry *)
        {
            if (pSnoopPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                break;
            }
        }
    }
    else
    {
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopPortNode, tSnoopPortEntry *)
        {
            if (pSnoopPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                break;
            }
        }
    }

    if (pSnoopPortNode != NULL)
    {
        SNOOP_MEM_SET (&SnoopHostEntry, 0, sizeof (tSnoopHostEntry));
        /* Assign the back pointer to the ASM port entry */
        SnoopHostEntry.pPortEntry = pSnoopPortNode;

        IPVX_ADDR_COPY (&(SnoopHostEntry.HostIpAddr),
                        &pSnoopPktInfo->SrcIpAddr);

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                             (tRBElem *) & SnoopHostEntry);

        /* if Host entry is not present already, we need to sync
           the information to neighbour node */
        if (pRBElem == NULL)
        {
            return SNOOP_SUCCESS;
        }
    }

    /* Host is already present in global RB Tree. No need to sync the 
       information to neighbour node */

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopSendAllHostReportsOnIccl                        */
/*                                                                           */
/* Description        : This function forms report based on every host that  */
/*                      is present for the group and sent to peer node       */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopSendAllHostReportsOnIccl (UINT4 u4Instance,
                               tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopPortEntry    *pSnoopPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    UINT1              *pPortBitmap = NULL;
    tSnoopIcchFillInfo  SnoopIcchInfo;
    tSnoopPktInfo       SnoopPktInfo;
    tSnoopSrcBmp        SnoopExclBmp;
    INT4                i4Result = 0;
    UINT4               u4IcchPort = 0;
    UINT4               u4InstanceId = 0;
    UINT4               u4PhyPort = 0;
    UINT4               u4MaxLength = 0;
    UINT2               u2AdminKey = 0;
    UINT2               u2GrpRec = 0;
    UINT2               u2LocalIcchPort = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    UINT1               u1McLagInterfacePresent = OSIX_FALSE;
    UINT1               u1NonMcLagInterfacePresent = OSIX_FALSE;
    UINT1               u1RecordType = 0;
    BOOL1               bResult = 0;

    /* The host optimization flag is used to sync every host
       information that is learnt on one node to the neighbour
       mc-lag node. This will result in sending report message for
       every host present. */

    /* Scan the ASM ports for the group and send the frame to the 
       neighbour node */

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (pSnoopGroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendAllHostReportsOnIccl: "
                   "Error in getting the group entry \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (&SnoopIcchInfo, 0, sizeof (tSnoopIcchFillInfo));
    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));
    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);

    SNOOP_MEM_CPY (SnoopIcchInfo.VlanId, pSnoopGroupEntry->VlanId,
                   sizeof (tSnoopTag));
    SnoopIcchInfo.pSnoopPktInfo = &SnoopPktInfo;

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    if (pPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopSendAllHostReportsOnIccl: "
                   "Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }

    IcchGetIcclIfIndex (&u4IcchPort);

    if ((SnoopVcmGetContextInfoFromIfIndex (u4IcchPort, &u4InstanceId,
                                            &u2LocalIcchPort)) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_GBL_NAME, "Instance ID not found\r\n");
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_ADD_TO_PORT_LIST (u2LocalIcchPort, pPortBitmap);

    /*
       Group record is fixed as 1 and number of sources is initialized
       to 0 for ASM reports. Exclude none report to be sent on ICCL.
     */

    u2GrpRec = SNOOP_OFFSET_ONE;
    u2NoSources = 0;
    u1RecordType = SNOOP_IS_EXCLUDE;

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                    pSnoopPortNode, tSnoopPortEntry *)
    {
        u1McLagInterfacePresent = SNOOP_FALSE;
        u4PhyPort = 0;
        u4PhyPort =
            SNOOP_GET_IFINDEX (u4Instance, (UINT2) pSnoopPortNode->u4Port);

        /* Scan all the host entries attached, and send the report */

        SNOOP_SLL_SCAN (&pSnoopPortNode->HostList,
                        pSnoopHostEntry, tSnoopHostEntry *)
        {
            SnoopIcchInfo.pSnoopHostEntry = pSnoopHostEntry;

            SnoopLaIsMclagInterface (u4PhyPort, &u1McLagInterfacePresent);

#ifdef IGS_WANTED
            if (u1McLagInterfacePresent == OSIX_TRUE)
            {
                LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);
                IgsUtilFillV3SourceInfoForIccl (u4Instance,
                                                pSnoopGroupEntry,
                                                u1RecordType, u2NoSources,
                                                &u4MaxLength, u2AdminKey,
                                                u1NonMcLagInterfacePresent,
                                                &SnoopIcchInfo);
            }
            else
            {
                IgsUtilFillV3SourceInfo (u4Instance,
                                         pSnoopGroupEntry->pConsGroupEntry,
                                         u1RecordType, u2NoSources,
                                         &u4MaxLength);
            }

            i4Result =
                IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                      u4MaxLength, &pOutBuf);

#endif
            if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
            {

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send SSM report on "
                               "to router ports\r\n");

                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    return SNOOP_FAILURE;
                }
            }

            /* Update source ip with the host ip address */
            SnoopIcchUpdateSrcIpCheckSum (u4Instance, pOutBuf, pSnoopHostEntry);

            SnoopVlanForwardPacket (u4Instance,
                                    pSnoopGroupEntry->VlanId,
                                    SNOOP_ADDR_TYPE_IPV4,
                                    SNOOP_INVALID_PORT,
                                    VLAN_FORWARD_SPECIFIC, pOutBuf,
                                    pPortBitmap, SNOOP_SSMREPORT_SENT);

            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

        }
    }

    /* Send the reports for the hosts attached in SSM Port list */

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopPortNode, tSnoopPortEntry *)
    {
        u1McLagInterfacePresent = OSIX_FALSE;
        u4PhyPort = 0;
        u4MaxLength = 0;
        u4PhyPort =
            SNOOP_GET_IFINDEX (u4Instance, (UINT2) pSnoopPortNode->u4Port);

        /* Scan all the host entries attached, and send the report */

        SNOOP_SLL_SCAN (&pSnoopPortNode->HostList,
                        pSnoopHostEntry, tSnoopHostEntry *)
        {
            SnoopIcchInfo.pSnoopHostEntry = pSnoopHostEntry;

            SnoopLaIsMclagInterface (u4PhyPort, &u1McLagInterfacePresent);

            if (pSnoopHostEntry->pSourceBmp == NULL)
            {
                continue;
            }

            /* If exclude bit map is NULL, the filter mode is exclude,
               otherwise filter mode is include */

            if (SNOOP_MEM_CMP
                (pSnoopHostEntry->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                 SNOOP_SRC_LIST_SIZE) != 0)
            {
                u1RecordType = SNOOP_IS_EXCLUDE;
            }
            else
            {
                u1RecordType = SNOOP_IS_INCLUDE;
            }

            for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
            {
                if (u1RecordType == SNOOP_IS_INCLUDE)
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pSnoopHostEntry->pSourceBmp->InclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET
                        (pSnoopHostEntry->pSourceBmp->ExclSrcBmp,
                         u2Index, SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u2NoSources++;
                    }
                }
            }

#ifdef IGS_WANTED
            if (u1McLagInterfacePresent == OSIX_TRUE)
            {
                LaUpdateActorPortChannelAdminKey (u4PhyPort, &u2AdminKey);
                IgsUtilFillV3SourceInfoForIccl (u4Instance,
                                                pSnoopGroupEntry,
                                                u1RecordType, u2NoSources,
                                                &u4MaxLength, u2AdminKey,
                                                u1NonMcLagInterfacePresent,
                                                &SnoopIcchInfo);
            }
            else
            {
                IgsUtilFillV3SourceInfo (u4Instance,
                                         pSnoopGroupEntry->pConsGroupEntry,
                                         u1RecordType, u2NoSources,
                                         &u4MaxLength);
            }

            i4Result =
                IgsEncodeAggV3Report (u4Instance, u2GrpRec,
                                      u4MaxLength, &pOutBuf);

#endif

            if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
            {

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_PKT_TRC,
                               "Unable to generate and send SSM report on "
                               "to router ports\r\n");

                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    return SNOOP_FAILURE;
                }
            }

            /* Update source ip with the host ip address */
            SnoopIcchUpdateSrcIpCheckSum (u4Instance, pOutBuf, pSnoopHostEntry);

            SnoopVlanForwardPacket (u4Instance,
                                    pSnoopGroupEntry->VlanId,
                                    SNOOP_ADDR_TYPE_IPV4,
                                    SNOOP_INVALID_PORT,
                                    VLAN_FORWARD_SPECIFIC, pOutBuf,
                                    pPortBitmap, SNOOP_SSMREPORT_SENT);

            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

        }
    }

    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopIcchUpdateSrcIpCheckSum                         */
/*                                                                           */
/* Description        : This function modifies the source Ip with the host   */
/*                      ip address and recalculates checksum                 */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pOutBuf - Frame that needs to modified               */
/*                      pSnoopHostEntry- Pointer to the host entry           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/

INT4
SnoopIcchUpdateSrcIpCheckSum (UINT4 u4Instance, tCRU_BUF_CHAIN_HEADER * pOutBuf,
                              tSnoopHostEntry * pSnoopHostEntry)
{
    UINT4               u4SrcIpAddr = 0;
    UINT2               u2CheckSum = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if ((pOutBuf == NULL) || (pSnoopHostEntry == NULL))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopIcchUpdateSrcIpCheckSum:  "
                   "Error in getting Host entry \r\n");
        return SNOOP_FAILURE;
    }

    u4SrcIpAddr = SNOOP_PTR_FETCH_4 (pSnoopHostEntry->HostIpAddr.au1Addr);

    u4SrcIpAddr = SNOOP_HTONL (u4SrcIpAddr);

    /* Fill the Source IP of IP header with the Host IP address */
    CRU_BUF_Copy_OverBufChain (pOutBuf,
                               (UINT1 *) &u4SrcIpAddr,
                               (SNOOP_IGMP_CHKSUM_OFFSET + SNOOP_OFFSET_TWO),
                               IPVX_IPV4_ADDR_LEN);
    u2CheckSum = 0;
    /* Reset the checksum field to 0, and again calculate it */
    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &u2CheckSum,
                               SNOOP_IGMP_CHKSUM_OFFSET, SNOOP_OFFSET_TWO);
    /* Calculate the checksum */
    u2CheckSum =
        IgsUtilCalculateIpCheckSum (pOutBuf, SNOOP_IP_HDR_RTR_OPT_LEN,
                                    SNOOP_DATA_LINK_HDR_LEN);

    u2CheckSum = SNOOP_HTONS (u2CheckSum);

    CRU_BUF_Copy_OverBufChain (pOutBuf, (UINT1 *) &u2CheckSum,
                               SNOOP_IGMP_CHKSUM_OFFSET, SNOOP_OFFSET_TWO);

    return SNOOP_SUCCESS;
}
