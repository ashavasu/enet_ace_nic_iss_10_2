/* $Id: snpgrp.c,v 1.83 2017/12/19 09:59:30 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpgrp.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping Group Module                          */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains processing routines         */
/*                            for snooping group module                      */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopGrpCreateGroupEntry                             */
/*                                                                           */
/* Description        : This function creates a new group entry              */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr  -   Grp IP address                          */
/*                      pSnoopPktInfo - Packet Information                   */
/*                      u2NumSrcs  - Number of sources                       */
/*                      u1RecordType - Record type                           */
/*                                                                           */
/* Output(s)          : ppRetSnoopGroupEntry - pointer to the group entry    */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpCreateGroupEntry (UINT4 u4Instance, tSnoopTag VlanId,
                          tIPvXAddr GrpAddr, tSnoopPktInfo * pSnoopPktInfo,
                          tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                          UINT2 u2NumSrcs, UINT1 u1RecordType,
                          tSnoopGroupEntry ** ppRetSnoopGroupEntry)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT4               u4Status = 0;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, sizeof (tSnoopSrcBmp));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpCreateGroupEntry\r\n");
    if (SNOOP_VLAN_GRP_ALLOC_MEMBLK (u4Instance, pSnoopGroupEntry) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Maximum groups exceeded.Memory allocation for group entry failed\r\n");
        SYS_LOG_MSG ((SYSLOG_NOTICE_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for group entry failed"));
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpCreateGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                        SNOOP_DBG_RESRC,
                        SNOOP_OS_RES_NAME,
                        "Memory allocation done for group entry for group %s \r\n",
                        SnoopPrintIPvxAddress (GrpAddr));

    SNOOP_MEM_SET (pSnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

    /* Set default values for the Group table */
    SNOOP_MEM_CPY (&pSnoopGroupEntry->VlanId, VlanId, sizeof (tSnoopTag));

    IPVX_ADDR_COPY (&(pSnoopGroupEntry->GroupIpAddr), &GrpAddr);

    pSnoopGroupEntry->u1EntryTypeFlag = SNOOP_GRP_DYNAMIC;
    pSnoopGroupEntry->u1IsUnknownMulticast = SNOOP_FALSE;
    pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
    SNOOP_SLL_INIT (&pSnoopGroupEntry->ASMPortList);
    SNOOP_DLL_INIT_NODE (&pSnoopGroupEntry->NextGrpNode);

    /* Add Node */
    u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                          (tRBElem *) pSnoopGroupEntry);

    if (u4Status == RB_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RBTree Addition Failed for Group Entry \r\n");
        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpCreateGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    /* Adding the new group entry to the DLL present in the 
     * consolidated Group entry.*/
    if (SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                   SNOOP_CREATE_GRP_ENTRY) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Updation of consolidated group entry failed\r\n");
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                      (tRBElem *) pSnoopGroupEntry);
        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpCreateGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                        SNOOP_DBG_GRP,
                        SNOOP_GRP_NAME,
                        "New group entry %s created\r\n",
                        SnoopPrintIPvxAddress (pSnoopGroupEntry->GroupIpAddr));

    if (pSnoopPktInfo->u1PktType != SNOOP_DATA_PKT)
    {
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            /* MAC Based forwarding mode */
            pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

            if (SnoopGrpUpdatePortToGroupEntry (u4Instance, pSnoopGroupEntry,
                                                pSnoopPktInfo,
                                                pSnoopPortCfgEntry,
                                                SNOOP_TRUE) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                    "Updation of port entry to Group failed for group %s\r\n",
                                    SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                           GroupIpAddr));
                SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                           SNOOP_DELETE_GRP_ENTRY);
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                              (tRBElem *) pSnoopGroupEntry);
                SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopGrpCreateGroupEntry\r\n");
                return SNOOP_FAILURE;
            }
            SnoopUpdateConsGrpOnFilterChange (u4Instance, SNOOP_TO_EXCLUDE,
                                              pSnoopGroupEntry->pConsGroupEntry,
                                              SNOOP_FALSE);
        }

#ifdef ICCH_WANTED

        /* Incase of ICCH enabled we need to update the data structures based
           on received report information from remote node.
         */
        else if ((pSnoopPktInfo->u1IcchReportVersion == SNOOP_IGMP_V1REPORT) ||
                 (pSnoopPktInfo->u1IcchReportVersion == SNOOP_IGMP_V2REPORT))
        {
            SnoopUpdateGroupEntryInEnhMode (u4Instance, pSnoopGroupEntry,
                                            pSnoopPktInfo, pSnoopPortCfgEntry);
        }
#endif
        else
        {
            SNOOP_SLL_INIT (&pSnoopGroupEntry->SSMPortList);

            SNOOP_MEM_SET (pSnoopGroupEntry->InclSrcBmp, 0,
                           sizeof (tSnoopSrcBmp));
            SNOOP_MEM_SET (pSnoopGroupEntry->ExclSrcBmp, 0xff,
                           sizeof (tSnoopSrcBmp));

            /* IP Forwarding Mode */
            if ((pSnoopPktInfo->u1PktType == SNOOP_IGMP_V3REPORT &&
                 pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4) ||
                (pSnoopPktInfo->u1PktType == SNOOP_MLD_V2REPORT &&
                 pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6))
            {
                if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                    (u1RecordType == SNOOP_IS_INCLUDE))
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
                }
                else
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                }

                if (SnoopGrpHandleSSMReports (u4Instance, u1RecordType,
                                              pSnoopGroupEntry, u2NumSrcs,
                                              pSnoopPktInfo,
                                              pSnoopPortCfgEntry) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Processing of ASM report messages"
                                   " for group record failed\r\n");
                    SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                               SNOOP_DELETE_GRP_ENTRY);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                  (tRBElem *) pSnoopGroupEntry);
                    SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopGrpCreateGroupEntry\r\n");
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, gNullSrcBmp,
                               SNOOP_SRC_LIST_SIZE);

                if (SnoopGrpUpdatePortToGroupEntry
                    (u4Instance, pSnoopGroupEntry, pSnoopPktInfo,
                     pSnoopPortCfgEntry, SNOOP_TRUE) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Updation of port entry to Group failed\r\n");
                    SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                               SNOOP_DELETE_GRP_ENTRY);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                  (tRBElem *) pSnoopGroupEntry);
                    SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopGrpCreateGroupEntry\r\n");
                    return SNOOP_FAILURE;
                }
                SnoopUpdateConsGrpOnFilterChange
                    (u4Instance, SNOOP_TO_EXCLUDE,
                     pSnoopGroupEntry->pConsGroupEntry, SNOOP_TRUE);
            }
        }
    }
    if (SnoopVlanGetVlanEntry (u4Instance, pSnoopGroupEntry->VlanId,
                               (pSnoopGroupEntry->GroupIpAddr.u1Afi),
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME, "Unable to get the vlan entry \n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpCreateGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    *ppRetSnoopGroupEntry = pSnoopGroupEntry;
    if (pSnoopPktInfo->u1PktType != SNOOP_DATA_PKT)
    {
        /*Updating the statistics for active Group count */
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_ACTIVE_GRPS) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_PKT_NAME, "Unable to update statistics "
                                "for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpCreateGroupEntry\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeleteGroupEntry                             */
/*                                                                           */
/* Description        : This function deletes a group entry                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr  -   Grp IP address                          */
/*                                                                           */
/* Output(s)          : pointer to the group entry                           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpDeleteGroupEntry (UINT4 u4Instance, tSnoopTag VlanId, tIPvXAddr GrpAddr)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopMacGrpFwdEntry *pTempSnoopMacGrpFwdEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               tempAddr[IPVX_MAX_INET_ADDR_LEN];
    tSnoopTag           NullVlanId;
    tSnoopTag           VlanID;
    UINT4               u4TempGrpIpAddr = 0;
    CHR1               *pc1String = NULL;
    CHR1                au1Ip6Addr[IPVX_IPV6_ADDR_LEN];
    UINT1               u1AddrType = 0;

    /* This function can be used to delete based on
     * VlanId(Outer, Inner), Group  OR
     * VlanId(Outer, Inner)  alone */

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpDeleteGroupEntry\r\n");
    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (tempAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (NullVlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (VlanID, 0, sizeof (tSnoopTag));

    if ((SNOOP_MEM_CMP (VlanId, NullVlanId, sizeof (tSnoopTag)) != 0) &&
        (SNOOP_MEM_CMP (GrpAddr.au1Addr, tempAddr,
                        IPVX_MAX_INET_ADDR_LEN) != 0))
    {
        SNOOP_MEM_CPY (SnoopGroupEntry.VlanId, VlanId, sizeof (tSnoopTag));

        IPVX_ADDR_COPY (&SnoopGroupEntry.GroupIpAddr, &GrpAddr);

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                             (tRBElem *) & SnoopGroupEntry);

        if (pRBElem == NULL)
        {
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_MEM_CPY (&u4TempGrpIpAddr, GrpAddr.au1Addr,
                               IPVX_IPV4_ADDR_LEN);
                u4TempGrpIpAddr = SNOOP_HTONL (u4TempGrpIpAddr);
                SNOOP_CONVERT_IPADDR_TO_STR (pc1String, u4TempGrpIpAddr);
            }
            else if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
                SNOOP_MEM_CPY (au1Ip6Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
                SNOOP_INET_NTOHL (au1Ip6Addr);
                pc1String = (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                   au1Ip6Addr);
            }
            SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_OS_RES_NAME,
                                "Group Entry not found for OuterVLAN %d for group %s\r\n"
                                "InnerVLAN %d\n",
                                SNOOP_OUTER_VLAN (VlanId), pc1String,
                                SNOOP_INNER_VLAN (VlanId));
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopGrpDeleteGroupEntry\r\n");
            return SNOOP_FAILURE;
        }
        else
        {
            pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

            /* Stop the report forward timer if it is running */
            if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
            }

            /* Stop the group specific source information timer
             * timer if it is running */
            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
            }

            /* Delete all the V1/V2 and V3 port records */
            SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

            /* Delete the group entry pointer from the MAC group entry`s 
             * Group entry list */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                pTempSnoopMacGrpFwdEntry = pSnoopGroupEntry->pMacGrpFwdEntry;

                if (pTempSnoopMacGrpFwdEntry != NULL)
                {
                    SNOOP_SLL_SCAN (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                    pTempSnoopGroupEntry, tSnoopGroupEntry *)
                    {
                        if (IPVX_ADDR_COMPARE
                            (pTempSnoopGroupEntry->GroupIpAddr,
                             pSnoopGroupEntry->GroupIpAddr) == 0)
                        {
                            break;
                        }
                    }

                    SNOOP_SLL_DELETE (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                      pSnoopGroupEntry);
                }

                pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
            }

            SNOOP_MEM_CPY (&VlanID, pSnoopGroupEntry->VlanId,
                           sizeof (tSnoopTag));
            u1AddrType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

            if (SnoopVlanGetVlanEntry (u4Instance, VlanID,
                                       u1AddrType,
                                       &pSnoopVlanEntry) == SNOOP_SUCCESS)
            {

                if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
                {
                    if (pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps !=
                        0)
                    {
                        pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps--;

                    }
                }
            }

            SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                       SNOOP_DELETE_GRP_ENTRY);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                          pRBElem);

            SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
        }
    }
    else if (SNOOP_MEM_CMP (VlanId, NullVlanId, sizeof (tSnoopTag)) != 0)
    {
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

        while (pRBElem != NULL)
        {
            pRBNextElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                               pRBElem, NULL);

            pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

            if (SNOOP_MEM_CMP (pSnoopGroupEntry->VlanId,
                               VlanId, sizeof (tSnoopTag)))
            {
                pRBElem = pRBNextElem;
                continue;
            }
            /* Stop the report forward timer if it is running */
            if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
            }

            /* Stop the group specific source information timer
             * timer if it is running */
            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
            }

            /* Delete all the V1/V2 and V3 port records */
            SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

            /* Delete the group entry pointer from the MAC group entry`s 
             * Group entry list */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                pTempSnoopMacGrpFwdEntry = pSnoopGroupEntry->pMacGrpFwdEntry;

                if (pTempSnoopMacGrpFwdEntry != NULL)
                {
                    SNOOP_SLL_SCAN (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                    pTempSnoopGroupEntry, tSnoopGroupEntry *)
                    {
                        if (IPVX_ADDR_COMPARE
                            (pTempSnoopGroupEntry->GroupIpAddr,
                             pSnoopGroupEntry->GroupIpAddr) == 0)
                        {
                            break;
                        }
                    }

                    SNOOP_SLL_DELETE (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                      pSnoopGroupEntry);
                }

                pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
            }
#ifdef NPAPI_WANTED
            else
            {
                /* If the forrwarding mode is IP indicate MRPs PIM/DVMRP/IGMP
                 * regarding the group deletion event */
                SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                    pSnoopGroupEntry->VlanId);
            }
#endif
            SNOOP_MEM_CPY (&VlanID, pSnoopGroupEntry->VlanId,
                           sizeof (tSnoopTag));
            u1AddrType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

            if (SnoopVlanGetVlanEntry (u4Instance, VlanID,
                                       u1AddrType,
                                       &pSnoopVlanEntry) == SNOOP_SUCCESS)
            {
                if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
                {
                    if (pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps !=
                        0)
                    {
                        pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps--;

                    }
                }
            }
            SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                       SNOOP_DELETE_GRP_ENTRY);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                          (tRBElem *) pSnoopGroupEntry);

            SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);

            pRBElem = pRBNextElem;
        }
    }
    else
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpDeleteGroupEntry\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpDeleteGroupEntry\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDelGroupOnOuterVlan                          */
/*                                                                           */
/* Description        : This function deletes a group entry                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpDelGroupOnOuterVlan (UINT4 u4Instance, tSnoopTag VlanId)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopMacGrpFwdEntry *pTempSnoopMacGrpFwdEntry = NULL;
    UINT1               tempAddr[IPVX_MAX_INET_ADDR_LEN];
    tSnoopTag           NullVlanId;
    UINT1               u1EntryUsed = SNOOP_FALSE;
    UINT2               u2Count = 0;

    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (tempAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (NullVlanId, 0, sizeof (tSnoopTag));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpDelGroupOnOuterVlan\r\n");
    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        if (SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)
            != SNOOP_OUTER_VLAN (VlanId))
        {
            pRBElem = pRBNextElem;
            continue;
        }
        /* Stop the report forward timer if it is running */
        if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType
            != SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
        }

        /* Stop the group specific source information timer
         * timer if it is running */
        if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
        }

        /* Delete all the V1/V2 and V3 port records */
        SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

        /* Delete the group entry pointer from the MAC group entry`s 
         * Group entry list */
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            pTempSnoopMacGrpFwdEntry = pSnoopGroupEntry->pMacGrpFwdEntry;

            if (pTempSnoopMacGrpFwdEntry != NULL)
            {
                SNOOP_SLL_SCAN (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                pTempSnoopGroupEntry, tSnoopGroupEntry *)
                {
                    if (IPVX_ADDR_COMPARE
                        (pTempSnoopGroupEntry->GroupIpAddr,
                         pSnoopGroupEntry->GroupIpAddr) == 0)
                    {
                        break;
                    }
                }

                SNOOP_SLL_DELETE (&pTempSnoopMacGrpFwdEntry->GroupEntryList,
                                  pSnoopGroupEntry);
            }

            pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
        }
#ifdef NPAPI_WANTED
        else
        {
            /* If the forrwarding mode is IP indicate MRPs PIM/DVMRP/IGMP
             * regarding the group deletion event */
            SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                pSnoopGroupEntry->VlanId);
        }
#endif

        SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                   SNOOP_DELETE_GRP_ENTRY);
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                      (tRBElem *) pSnoopGroupEntry);

        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);

        pRBElem = pRBNextElem;
    }

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        /* Before decrementing the group reference count check if this
         * source is not used by any other group */
        for (u2Count = 1; u2Count <= SNOOP_MAX_MCAST_SRCS; u2Count++)
        {
            if ((SNOOP_MEM_CMP (&(SNOOP_SRC_INFO (u4Instance,
                                                  (u2Count -
                                                   1)).SrcIpAddr).au1Addr,
                                &tempAddr, IPVX_MAX_INET_ADDR_LEN) == 0)
                || (SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount ==
                    0))
            {
                continue;
            }

            SnoopUtilCheckIsSourceUsed (u4Instance, u2Count, &u1EntryUsed);
            if (u1EntryUsed == SNOOP_FALSE)
            {
                SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount--;
                if (SNOOP_SRC_INFO (u4Instance,
                                    (u2Count - 1)).u2GrpRefCount == 0)
                {
                    /* Remove the source from the linked list */
                    SNOOP_REMOVE_SOURCE (u4Instance,
                                         SNOOP_SRC_INFO (u4Instance,
                                                         (u2Count - 1)));
                    SNOOP_MEM_SET (&(SNOOP_SRC_INFO (u4Instance,
                                                     (u2Count - 1)).SrcIpAddr),
                                   0, sizeof (tIPvXAddr));
                    gu2SourceCount--;
                }
            }
        }
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpDelGroupOnOuterVlan\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpGetGroupEntry                                */
/*                                                                           */
/* Description        : This function gets the group entry from group        */
/*                      membership data base                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr  - Grp IP address                            */
/*                                                                           */
/* Output(s)          : pointer to the group entry or NULL                   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpGetGroupEntry (UINT4 u4Instance, tSnoopTag VlanId,
                       tIPvXAddr GrpAddr,
                       tSnoopGroupEntry ** ppRetSnoopGroupEntry)
{
    tRBElem            *pRBTreeElem = NULL;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopTag           NullVlanId;
    tSnoopTag           VlanID;
    UINT1               tempAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4TempGrpIpAddr = 0;
    CHR1                au1Ip6Addr[IPVX_IPV6_ADDR_LEN];

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpGetGroupEntry\r\n");
    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

    SNOOP_MEM_SET (tempAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (NullVlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (VlanID, 0, sizeof (tSnoopTag));

    SNOOP_MEM_CPY (SnoopGroupEntry.VlanId, VlanId, sizeof (tSnoopTag));
    IPVX_ADDR_COPY (&(SnoopGroupEntry.GroupIpAddr), &GrpAddr);

    if (SnoopGroupEntry.GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        SNOOP_MEM_CPY (&u4TempGrpIpAddr, GrpAddr.au1Addr, IPVX_IPV4_ADDR_LEN);
        u4TempGrpIpAddr = SNOOP_HTONL (u4TempGrpIpAddr);
    }
    else if (SnoopGroupEntry.GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
        SNOOP_MEM_CPY (au1Ip6Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
        SNOOP_INET_NTOHL (au1Ip6Addr);
    }

    UNUSED_PARAM (au1Ip6Addr);

    if ((SNOOP_MEM_CMP (VlanId, NullVlanId, sizeof (tSnoopTag)) != 0) &&
        (SNOOP_MEM_CMP (GrpAddr.au1Addr, tempAddr,
                        IPVX_MAX_INET_ADDR_LEN) != 0))
    {
        pRBTreeElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                                 (tRBElem *) & SnoopGroupEntry);
    }

    if (pRBTreeElem == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpGetGroupEntry\r\n");
        return SNOOP_FAILURE;
    }
    else
    {
        *ppRetSnoopGroupEntry = (tSnoopGroupEntry *) pRBTreeElem;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpGetGroupEntry\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpUpdatePortToGroupEntry                       */
/*                                                                           */
/* Description        : This function adds a port to the group entry         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      pSnoopPktInfo   - Pointer to packet information      */
/*                                                                           */
/* Output(s)          : pointer to the group entry or NULL                   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpUpdatePortToGroupEntry (UINT4 u4Instance,
                                tSnoopGroupEntry * pSnoopGroupEntry,
                                tSnoopPktInfo * pSnoopPktInfo,
                                tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                                BOOL1 bNewGroup)
{
    tSnoopPortEntry    *pSnoopASMPortNode = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortBmp       TempPortBitmap;
    tIPvXAddr           tempSrcAddr;
    tIPvXAddr           SrcAddr;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tRBElem            *pRBElem = NULL;
    UINT4               u4Status = RB_FAILURE;
    UINT2               u2V1V2HostPresentInt = 0;
    UINT1               u1HostFound = SNOOP_FALSE;
    UINT1               u1EntryFound = SNOOP_FALSE;
    UINT1               u1FwdTableUpdate = SNOOP_TRUE;
    UINT1               u1SSMEntryFound = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;

    UNUSED_PARAM (bNewGroup);
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpUpdatePortToGroupEntry\r\n");
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpUpdatePortToGroupEntry\r\n");
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                     SNOOP_FAILURE);

    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    if (SnoopVlanGetVlanEntry (u4Instance, pSnoopGroupEntry->VlanId,
                               (pSnoopGroupEntry->GroupIpAddr.u1Afi),
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME,
                       "Unable to get the vlan entry to update ASM port entry\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
        return SNOOP_FAILURE;
    }
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        /* Stop the V3 port purge timer running on this port  */
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopSSMPortNode, tSnoopPortEntry *)
        {
            if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                u1SSMEntryFound = SNOOP_TRUE;
                break;
            }
        }
    }
    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopGroupEntry->ASMPortBitmap, bResult);
    u1EntryFound = (bResult == OSIX_TRUE) ? SNOOP_TRUE : SNOOP_FALSE;

    if (u1EntryFound == SNOOP_TRUE)
    {
        /* Get the ASM port entry */
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortNode, tSnoopPortEntry *)
        {
            if (pSnoopASMPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                break;
            }
        }

        SNOOP_CHK_NULL_PTR_RET (pSnoopASMPortNode, SNOOP_FAILURE);

        /* Check the received report message is V1 report then
         * set the u1V1HostPresent as SNOOP_TRUE */
#ifdef IGS_WANTED
        if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
        {
            pSnoopASMPortNode->u1V1HostPresent = SNOOP_TRUE;
#ifdef L2RED_WANTED
            SNOOP_ADD_TO_PORT_LIST (pSnoopASMPortNode->u4Port,
                                    pSnoopGroupEntry->V1PortBitmap);
#endif
        }
#endif

        /* Stop PurgeOrGrpQueryTimer if it is running already */
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                        "Stopping the purge or group timer running on port %u\n",
                        pSnoopASMPortNode->u4Port);
        SnoopTmrStopTimer (&pSnoopASMPortNode->PurgeOrGrpQueryTimer);

        /* Updating the ASM Host information */

        if (SnoopGrpGetHostEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                  pSnoopGroupEntry->GroupIpAddr,
                                  pSnoopPktInfo->u4InPort,
                                  pSnoopPktInfo->SrcIpAddr,
                                  &pSnoopASMHostEntry) == SNOOP_SUCCESS)
        {
            u1HostFound = SNOOP_TRUE;
        }

        if (u1HostFound == SNOOP_FALSE)
        {
            /* Add the host information to the host list */
            if (SNOOP_ASM_HOST_ENTRY_ALLOC_MEMBLK (u4Instance,
                                                   pSnoopASMHostEntry) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory alloc for ASM Host entry failed\n");
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                              "Memory alloc for ASM Host entry failed"));
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pSnoopASMHostEntry, 0, sizeof (tSnoopHostEntry));

            IPVX_ADDR_COPY (&pSnoopASMHostEntry->HostIpAddr,
                            &pSnoopPktInfo->SrcIpAddr);
            pSnoopASMHostEntry->u1HostTimerStatus = SNOOP_FIRST_HALF;

#ifdef IGS_WANTED
            if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
            {
                pSnoopASMHostEntry->SnoopHostType = SNOOP_ASM_V1HOST;
            }
            else
#endif
            {
                pSnoopASMHostEntry->SnoopHostType = SNOOP_ASM_V2HOST;
            }

            /* Assign the back pointer to the ASM port entry */
            pSnoopASMHostEntry->pPortEntry = pSnoopASMPortNode;

            SNOOP_SLL_ADD (&pSnoopASMPortNode->HostList,
                           &pSnoopASMHostEntry->NextHostEntry);

            pRBElem = (tRBElem *) pSnoopASMHostEntry;
            /* Add the host node to the Host Information RBTree */
            u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  pRBElem);

            if (u4Status == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for Host Entry \r\n");
                SNOOP_SLL_DELETE (&pSnoopASMPortNode->HostList,
                                  pSnoopASMHostEntry);
                SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                  pSnoopASMHostEntry);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
                return SNOOP_FAILURE;
            }
        }
        else
        {
            /* Reset the timer flag status */
            pSnoopASMHostEntry->u1HostTimerStatus = SNOOP_FIRST_HALF;
        }
        /* Start the port purge timer for the Port on which v1/v2 report 
         * was received */

        pSnoopASMPortNode->PurgeOrGrpQueryTimer.u4Instance = u4Instance;
        pSnoopASMPortNode->PurgeOrGrpQueryTimer.u1TimerType =
            SNOOP_ASM_PORT_PURGE_TIMER;

        SnoopTmrStartTimer (&pSnoopASMPortNode->PurgeOrGrpQueryTimer,
                            pSnoopVlanEntry->u2PortPurgeInt);
        pSnoopASMPortNode->u1RetryCount = 0;

        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
        return SNOOP_SUCCESS;
    }

    /* Check the threshold, if the port is not in SSM-portlist also. */
    if (u1SSMEntryFound != SNOOP_TRUE)
    {
        /* Get the port entry; If port entry is absent, no port-based action
         * will be taken for the packet.*/
        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL))
        {
            if ((pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                 u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_GROUPS) &&
                (SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                 u1FilterStatus != SNOOP_DISABLE))
            {
                if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u4SnoopPortCfgMemCnt <
                    pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                    u4SnoopPortMaxLimit)
                {
                    SNOOP_INCR_MEMBER_CNT ((pSnoopPortCfgEntry->
                                            pSnoopExtPortCfgEntry));
                }
                else
                {
                    SNOOP_GBL_DBG_ARG1
                        (SNOOP_DBG_FLAG_INST (u4Instance),
                         SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                         SNOOP_VLAN_NAME,
                         "Maximum join Limit has been reached; No more "
                         "entries can be learnt on this port %d\n",
                         pSnoopPktInfo->u4InPort);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
                    return SNOOP_FAILURE;
                }
            }
        }
    }

    if (SNOOP_ASM_PORT_ALLOC_MEMBLK (u4Instance, pSnoopASMPortNode) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for port entry for group failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for port entry for group failed"));
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopASMPortNode, 0, sizeof (tSnoopPortEntry));

    SNOOP_SLL_INIT (&pSnoopASMPortNode->HostList);

    pSnoopASMPortNode->u4Port = pSnoopPktInfo->u4InPort;

    /* Assign back pointer to the group node */
    pSnoopASMPortNode->pGroupEntry = pSnoopGroupEntry;
    pSnoopASMPortNode->u1RetryCount = 0;

    /* Add the host information to the host list */
    if (SNOOP_ASM_HOST_ENTRY_ALLOC_MEMBLK (u4Instance,
                                           pSnoopASMHostEntry) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory alloc for ASM Host entry failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory alloc for ASM Host entry failed"));
        SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortNode);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopASMHostEntry, 0, sizeof (tSnoopHostEntry));

    IPVX_ADDR_COPY (&pSnoopASMHostEntry->HostIpAddr, &pSnoopPktInfo->SrcIpAddr);

    pSnoopASMHostEntry->u1HostTimerStatus = SNOOP_FIRST_HALF;

    /* Check the received report message is V1 report then
     * set the u1V1HostPresent as SNOOP_TRUE */
    if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
    {
        pSnoopASMPortNode->u1V1HostPresent = SNOOP_TRUE;
#ifdef L2RED_WANTED
        SNOOP_ADD_TO_PORT_LIST (pSnoopASMPortNode->u4Port,
                                pSnoopGroupEntry->V1PortBitmap);
#endif
    }
    else
    {
        pSnoopASMPortNode->u1V1HostPresent = SNOOP_FALSE;
    }
#ifdef IGS_WANTED
    if (pSnoopPktInfo->u1PktType == SNOOP_IGMP_V1REPORT)
    {
        pSnoopASMHostEntry->SnoopHostType = SNOOP_ASM_V1HOST;
    }
    else
#endif
    {
        pSnoopASMHostEntry->SnoopHostType = SNOOP_ASM_V2HOST;
    }

    /* Assign the back pointer to the ASM port entry */
    pSnoopASMHostEntry->pPortEntry = pSnoopASMPortNode;

    SNOOP_SLL_ADD (&pSnoopASMPortNode->HostList,
                   &pSnoopASMHostEntry->NextHostEntry);

    /* Add the host node to the Host Information RBTree */
    u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                          (tRBElem *) pSnoopASMHostEntry);

    if (u4Status == RB_FAILURE)
    {
        /* Check already any of the host information present for the same */

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                             (tRBElem *) pSnoopASMHostEntry);
        if (pRBElem != NULL)
        {
            if (u1SSMEntryFound == SNOOP_TRUE)
            {
                SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                                pSnoopSSMPortNode, tSnoopPortEntry *)
                {
                    if ((pSnoopSSMPortNode != NULL) &&
                        (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort))
                    {
                        /* Delete the Host information for V3 port entry */
                        if (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) != 0)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                       SNOOP_MGMT_DBG,
                                       "Deleting all the Host information for V3 port entry");

                            SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList,
                                            pSnoopHostEntry, tSnoopHostEntry *)
                            {
                                /* Delete the matching host entry from SSM list */
                                if (pSnoopHostEntry ==
                                    (tSnoopHostEntry *) pRBElem)
                                {
                                    SNOOP_SLL_DELETE (&pSnoopSSMPortNode->
                                                      HostList,
                                                      pSnoopHostEntry);

                                    /* Remove the host from the Host Information RBTree */
                                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                               SNOOP_MGMT_DBG,
                                               "Removing the host from the Host Information RBTree");
                                    RBTreeRemove (SNOOP_INSTANCE_INFO
                                                  (u4Instance)->HostEntry,
                                                  (tRBElem *) pSnoopHostEntry);

                                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK
                                        (u4Instance, pSnoopHostEntry);
                                    break;
                                }
                            }
                        }

                        /* If all the hosts are deleted, delete the SSM port entry */
                        if (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 0)
                        {
                            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                                              pSnoopSSMPortNode);

                            if (pSnoopSSMPortNode->HostPresentTimer.u1TimerType
                                != SNOOP_INVALID_TIMER_TYPE)
                            {
                                SnoopTmrStopTimer (&pSnoopSSMPortNode->
                                                   HostPresentTimer);
                            }

                            /* Stop the PortPurge/Leave timer if it is running */
                            if (pSnoopASMPortNode->PurgeOrGrpQueryTimer.
                                u1TimerType != SNOOP_INVALID_TIMER_TYPE)
                            {
                                SnoopTmrStopTimer (&pSnoopASMPortNode->
                                                   PurgeOrGrpQueryTimer);
                            }

                            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance,
                                                        pSnoopSSMPortNode);
                        }

                        break;
                    }
                }

                /* Add the host node to the Host Information RBTree */
                u4Status =
                    RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                               (tRBElem *) pSnoopASMHostEntry);
            }
        }

        if (u4Status == RB_FAILURE)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "RBTree Addition Failed for Host Entry \r\n");

            SNOOP_SLL_DELETE (&pSnoopASMPortNode->HostList, pSnoopASMHostEntry);
            SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopASMHostEntry);
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortNode);
            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortNode);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
            return SNOOP_FAILURE;
        }
    }

    /* Start the Asm Host present timer */
    pSnoopASMPortNode->HostPresentTimer.u4Instance = u4Instance;
    pSnoopASMPortNode->HostPresentTimer.u1TimerType = SNOOP_HOST_PRESENT_TIMER;
    u2V1V2HostPresentInt =
        (UINT2) (pSnoopVlanEntry->u2PortPurgeInt / SNOOP_TWO);
    SnoopTmrStartTimer (&pSnoopASMPortNode->HostPresentTimer,
                        u2V1V2HostPresentInt);
    /* Start the port purge timer for the Port on which V1/V2 report 
     * was received */
    pSnoopASMPortNode->PurgeOrGrpQueryTimer.u4Instance = u4Instance;
    pSnoopASMPortNode->PurgeOrGrpQueryTimer.u1TimerType =
        SNOOP_ASM_PORT_PURGE_TIMER;
    SnoopTmrStartTimer (&pSnoopASMPortNode->PurgeOrGrpQueryTimer,
                        pSnoopVlanEntry->u2PortPurgeInt);

    SNOOP_SLL_ADD (&pSnoopGroupEntry->ASMPortList,
                   &pSnoopASMPortNode->NextPortEntry);

    SNOOP_ADD_TO_PORT_LIST (pSnoopASMPortNode->u4Port,
                            pSnoopGroupEntry->ASMPortBitmap);

    SNOOP_ADD_TO_PORT_LIST (pSnoopASMPortNode->u4Port,
                            pSnoopGroupEntry->PortBitmap);

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        /* Update the port to the forwarding table */
        SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

        SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->ASMPortBitmap,
                                TempPortBitmap);

        SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap, TempPortBitmap);
        if (SnoopFwdGetIpForwardingEntry (u4Instance, pSnoopVlanEntry->VlanId,
                                          pSnoopPktInfo->DestIpAddr,
                                          SrcAddr,
                                          &pSnoopIpGrpFwdEntry) ==
            SNOOP_SUCCESS)
        {

            SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                                   pSnoopIpGrpFwdEntry->PortBitmap, bResult);
            if (bResult == OSIX_TRUE)
            {
                u1FwdTableUpdate = SNOOP_FALSE;
            }
        }

        if (u1FwdTableUpdate == SNOOP_TRUE)
        {

            if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                          tempSrcAddr,
                                          pSnoopASMPortNode->u4Port,
                                          pSnoopGroupEntry->PortBitmap,
                                          SNOOP_ADD_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                SNOOP_OS_RES_DBG,
                                "Unable to add the V1/V2 port %d to the forwarding entry\n",
                                pSnoopASMPortNode->u4Port);
                SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                          pSnoopGroupEntry->PortBitmap);

                if (pSnoopASMPortNode->PurgeOrGrpQueryTimer.u1TimerType !=
                    SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopASMPortNode->
                                       PurgeOrGrpQueryTimer);
                }

                /* If threshold was incremented in this function, set a flag.
                 * Decrement it accordingly here If there was no SSM port, and the
                 * limit type is group-imit,decrement the previously incremented
                 * threshold */
                if (u1SSMEntryFound != SNOOP_TRUE)
                {
                    if ((pSnoopPortCfgEntry != NULL) &&
                        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
                        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                         u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_GROUPS)
                        && (SNOOP_INSTANCE_INFO (u4Instance)->
                            SnoopInfo[pSnoopPktInfo->GroupAddr.u1Afi -
                                      1].u1FilterStatus != SNOOP_DISABLE))
                    {
                        SNOOP_DECR_MEMBER_CNT (pSnoopPortCfgEntry->
                                               pSnoopExtPortCfgEntry);
                    }
                }

                /* Delete all the hosts attached to the port */
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                           "Deleting all the hosts attached to the port");
                if (SNOOP_SLL_COUNT (&pSnoopASMPortNode->HostList) != 0)
                {
                    while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                            SNOOP_SLL_FIRST (&pSnoopASMPortNode->HostList)) !=
                           NULL)
                    {
                        SNOOP_SLL_DELETE (&pSnoopASMPortNode->HostList,
                                          pSnoopASMHostEntry);

                        /* Remove the host from the Host Information RBTree */
                        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                        SNOOP_MGMT_DBG,
                                        "Removing the host %s from the Host Information RBTree",
                                        SnoopPrintIPvxAddress
                                        (pSnoopASMHostEntry->HostIpAddr));
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      HostEntry,
                                      (tRBElem *) pSnoopASMHostEntry);

                        SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                          pSnoopASMHostEntry);
                    }
                }

                SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                                  pSnoopASMPortNode);
                SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                          pSnoopGroupEntry->ASMPortBitmap);

                SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                          pSnoopGroupEntry->PortBitmap);

#ifdef L2RED_WANTED
                SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                          pSnoopGroupEntry->V1PortBitmap);
#endif

                SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortNode);

                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
                return SNOOP_FAILURE;
            }
        }
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpUpdatePortToGroupEntry\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeleteGroupEntries                           */
/*                                                                           */
/* Description        : This function deletes all the group entries in the   */
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpDeleteGroupEntries (UINT4 u4Instance, UINT1 u1AddressType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopMacGrpFwdEntry *pTempSnoopMacGrpFwdEntry = NULL;
    UINT1               tempAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1EntryUsed = SNOOP_FALSE;
    UINT2               u2Count = 0;

    SNOOP_MEM_SET (tempAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_VALIDATE_INSTANCE (u4Instance);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpDeleteGroupEntries\r\n");
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_GRP_NAME,
                            "No Group entries found for this instance %u\r\n",
                            u4Instance);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpDeleteGroupEntries\r\n");
        return;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        if (pSnoopGroupEntry->GroupIpAddr.u1Afi == u1AddressType)
        {
            /* Stop the report forward timer if it is running */
            if (pSnoopGroupEntry->ReportFwdTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->ReportFwdTimer);
            }

            /* Stop the group specific source information timer
             * timer if it is running */
            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG, SNOOP_GRP_DBG,
                           "Stop the group specific source information timer\r\n");
            }

            /* Delete all the V1/V2 and V3 port records */
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                       "Deleting all the V1/V2 and V3 port records");
            SnoopGrpDeletePortEntries (u4Instance, pSnoopGroupEntry);

            /* Delete the group entry pointer from the MAC group entry`s 
             * Group entry list */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                if (pSnoopGroupEntry->pMacGrpFwdEntry != NULL)
                {
                    pTempSnoopMacGrpFwdEntry =
                        pSnoopGroupEntry->pMacGrpFwdEntry;

                    if (pTempSnoopMacGrpFwdEntry != NULL)
                    {
                        SNOOP_SLL_SCAN (&pTempSnoopMacGrpFwdEntry->
                                        GroupEntryList, pTempSnoopGroupEntry,
                                        tSnoopGroupEntry *)
                        {
                            if (IPVX_ADDR_COMPARE
                                (pTempSnoopGroupEntry->GroupIpAddr,
                                 pSnoopGroupEntry->GroupIpAddr) == 0)
                            {
                                break;
                            }
                        }

                        SNOOP_SLL_DELETE (&pTempSnoopMacGrpFwdEntry->
                                          GroupEntryList, pSnoopGroupEntry);
                    }
                    pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                }
            }
#ifdef NPAPI_WANTED
            else
            {
                /* If the forrwarding mode is IP indicate MRPs PIM/DVMRP/IGMP
                 * regarding the group deletion event */
                SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                    pSnoopGroupEntry->VlanId);
            }
#endif

            SnoopConsGroupEntryUpdate (u4Instance, pSnoopGroupEntry,
                                       SNOOP_DELETE_GRP_ENTRY);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                          (tRBElem *) pSnoopGroupEntry);

            SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, pSnoopGroupEntry);
            pSnoopGroupEntry = NULL;

        }
        pRBElem = pRBElemNext;
    }

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        /* Before decrementing the group reference count check if this
         * source is not used by any other group */
        for (u2Count = 1; u2Count <= SNOOP_MAX_MCAST_SRCS; u2Count++)
        {
            if ((SNOOP_MEM_CMP (&(SNOOP_SRC_INFO (u4Instance,
                                                  (u2Count -
                                                   1)).SrcIpAddr.au1Addr),
                                &tempAddr, IPVX_MAX_INET_ADDR_LEN) == 0)
                || (SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount ==
                    0))
            {
                continue;
            }

            SnoopUtilCheckIsSourceUsed (u4Instance, u2Count, &u1EntryUsed);
            if (u1EntryUsed == SNOOP_FALSE)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_SRC_DBG, SNOOP_SRC_DBG,
                           "source is not used by any other group\r\n");
                SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount--;
                if (SNOOP_SRC_INFO (u4Instance,
                                    (u2Count - 1)).u2GrpRefCount == 0)
                {
                    /* Remove the source from the linked list */
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_SRC_DBG, SNOOP_SRC_DBG,
                               "Removing the source from the linked list\r\n");
                    SNOOP_REMOVE_SOURCE (u4Instance,
                                         SNOOP_SRC_INFO (u4Instance,
                                                         (u2Count - 1)));
                    SNOOP_MEM_SET (&(SNOOP_SRC_INFO (u4Instance,
                                                     (u2Count - 1)).SrcIpAddr),
                                   0, sizeof (tIPvXAddr));
                    gu2SourceCount--;
                }
            }
        }
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpDeleteGroupEntries\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeletePortEntries                            */
/*                                                                           */
/* Description        : This function deletes all the port records associated*/
/*                      with group entry                                     */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - pointer to the group entry        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpDeletePortEntries (UINT4 u4Instance,
                           tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntry = NULL;
    UINT4               u4PhyPort = 0;
    UINT4               u4PortNumber = 1;
    UINT2               u2ByteInd = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1Value = 0;
    BOOL1               bResult = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_ADDRESS_TYPE (pSnoopGroupEntry->GroupIpAddr.u1Afi);

    /* Loop through the bitmap and decrement the threshold */
    /* 'OR' the Current BitMap and ASM bitmap, just to make sure that the 
     * portBitmap is correct */
    for (u2ByteInd = 0; u2ByteInd < SNOOP_PORT_LIST_SIZE; u2ByteInd++)
    {
        pSnoopGroupEntry->PortBitmap[u2ByteInd] |=
            pSnoopGroupEntry->ASMPortBitmap[u2ByteInd];
    }

    for (u2ByteInd = 0; u2ByteInd < SNOOP_PORT_LIST_SIZE; u2ByteInd++)
    {
        if (pSnoopGroupEntry->PortBitmap[u2ByteInd] == 0)
        {
            u4PortNumber = u4PortNumber + 8;
            continue;
        }

        for (u2BitIndex = 0; u2BitIndex < 8; u2BitIndex++)
        {
            u4PortNumber = u4PortNumber + u2BitIndex;
            OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->PortBitmap, u4PortNumber,
                                     sizeof (tSnoopPortBmp), bResult);

            if (bResult == OSIX_TRUE)
            {
                u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u4PortNumber);

                /* Get the port entry; If port entry is absent, no port-based
                 * action will be taken for the packet.*/
                if (SnoopGetPortCfgEntry (u4Instance, u4PhyPort,
                                          pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                          &pSnoopPortCfgEntry) == SNOOP_SUCCESS)
                {
                    pSnoopExtPortCfgEntry =
                        pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
                    if (pSnoopExtPortCfgEntry != NULL)
                    {
                        if ((pSnoopExtPortCfgEntry->u1SnoopPortMaxLimitType
                             == SNOOP_PORT_LMT_TYPE_GROUPS) &&
                            (SNOOP_INSTANCE_INFO (u4Instance)->
                             SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                             u1FilterStatus != SNOOP_DISABLE))
                        {
                            SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntry);
                        }
                    }
                }
            }
        }
    }

    /* Delete the V2 port records if present */
    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
    {
        while ((pSnoopASMPortEntry = (tSnoopPortEntry *)
                SNOOP_SLL_FIRST (&pSnoopGroupEntry->ASMPortList)) != NULL)
        {
            /* Stop the Port purge or group query timer if it is running */
            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                           "Stopping the Port purge or group query timer \r\n");
                SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Stop the Asm Host present timer if it is running */
            if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                           "Stopping the Asm Host present timer \r\n");
                SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
            }

            /* Delete all the hosts attached to the port */

            if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                           "Deleting all the hosts attached to the port \r\n");
                while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList))
                       != NULL)
                {
                    pRBElem = (tRBElem *) pSnoopASMHostEntry;
                    /* Remove the host from the Host Information RBTree */
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  pRBElem);

                    SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                      pSnoopASMHostEntry);

                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopASMHostEntry);
                }
            }

            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortEntry);

            SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                      pSnoopGroupEntry->ASMPortBitmap);
            SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                      pSnoopGroupEntry->PortBitmap);
#ifdef L2RED_WANTED
            SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                      pSnoopGroupEntry->V1PortBitmap);
#endif
            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);
        }
    }

    /* Check if the forwarding mode is MAC based then do not delete the
     * V3 portlist */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        return;
    }

    /* Delete the V3 port records if present */
    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) != 0)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                   "Deleting the V3 port records");
        while ((pSnoopSSMPortEntry = (tSnoopPortEntry *)
                SNOOP_SLL_FIRST (&pSnoopGroupEntry->SSMPortList)) != NULL)
        {
            /* Stop the Host present timer if it is running */
            if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                           "Stopping the Host present timer");
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
            }

            u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u4PortNumber);
            /* Get the port entry; If port entry is absent, no port-based 
             * action will be taken for the packet.*/
            if (SnoopGetPortCfgEntry (u4Instance, u4PhyPort,
                                      pSnoopGroupEntry->VlanId,
                                      pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                      &pSnoopPortCfgEntry) == SNOOP_SUCCESS)
            {
                pSnoopExtPortCfgEntry =
                    pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
                if (pSnoopExtPortCfgEntry != NULL)
                {
                    if ((pSnoopExtPortCfgEntry->u1SnoopPortMaxLimitType
                         == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
                        (SNOOP_INSTANCE_INFO (u4Instance)->
                         SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                         u1FilterStatus != SNOOP_DISABLE))
                    {
                        for (u2ByteInd = 0; u2ByteInd < SNOOP_SRC_LIST_SIZE;
                             u2ByteInd++)
                        {
                            u1Value =
                                pSnoopSSMPortEntry->pSourceBmp->
                                InclSrcBmp[u2ByteInd];
                            for (u2BitIndex = 0;
                                 ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                  && (u1Value != 0)); u2BitIndex++)
                            {
                                if ((u1Value & SNOOP_BIT8) != 0)
                                {
                                    SNOOP_DECR_MEMBER_CNT
                                        (pSnoopExtPortCfgEntry);
                                }
                                u1Value = (UINT1) (u1Value << 1);
                            }
                        }
                    }
                }
            }

            /* Delete all the Host information for V3 port entry */
            if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                           "Deleting all the Host information for V3 port entry");
                while ((pSnoopHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList,
                                      pSnoopHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                               "Removing the host from the Host Information RBTree");
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopHostEntry);

                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopHostEntry);
                }
            }

            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortEntry);

            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortEntry);
        }
    }

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpClearPortEntry                               */
/*                                                                           */
/* Description        : This function deletes port entry corresponding to    */
/*                      all the group entries in the VLAN                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - Vlan Id                                 */
/*                      u4Port     - Port number                             */
/*                      u1AdressType  - Address Type                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpClearPortEntry (UINT4 u4Instance, tSnoopTag VlanId, UINT4 u4Port,
                        UINT1 u1AddressType)
{
    UINT1              *pPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tMacAddr            MacGroupAddr;
    tMacAddr            NullMacAddr;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           tempSrcAddr;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT4               u4MaxLength = 0;
    INT4                i4Result = 0;
    UINT2               u2NumGrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    UINT1               u1RecordType = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1PortFound = SNOOP_FALSE;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1SSMPortFound = SNOOP_FALSE;
    UINT1               u1IsLastGroupEntry = SNOOP_FALSE;
#ifdef IGS_WANTED
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
#endif

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pPortBitmap);
        return SNOOP_FAILURE;
    }
    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    while (pRBElem != NULL)
    {
        u1PortFound = SNOOP_FALSE;
        u1SSMPortFound = SNOOP_FALSE;

        pRBElemNext = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                     GroupEntry, pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        /* If the multicast mac address is not NULL then delete the port entry
         * for VlanId and Multicast Group address address */

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_FWD_TRC, "support for IP-based only\r\n");
        }
        else if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId)
                 != SNOOP_INNER_VLAN (VlanId))
        {
            pRBElem = pRBElemNext;
            continue;
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap, bResult);

        if (bResult == OSIX_FALSE)
        {
            pRBElem = pRBElemNext;
            continue;
        }

        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortEntry, tSnoopPortEntry *)
        {
            if (pSnoopASMPortEntry->u4Port == u4Port)
            {
                u1PortFound = SNOOP_TRUE;
                break;
            }
        }

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopSSMPortEntry->u4Port == u4Port)
                {
                    u1SSMPortFound = SNOOP_TRUE;
                    break;
                }
            }
        }

        if ((u1PortFound == SNOOP_FALSE) && (u1SSMPortFound == SNOOP_FALSE))
        {
            pRBElem = pRBElemNext;
            continue;
        }
        /* Set the outer vlan id */
        SNOOP_OUTER_VLAN (VlanId) = SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);

        if (SnoopGetPortCfgEntry
            (u4Instance, u4Port, VlanId, u1AddressType,
             &pSnoopPortCfgEntry) != SNOOP_FAILURE)
        {
            pSnoopExtPortCfgEntry = pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
            pSnoopExtPortCfgEntry->u4SnoopPortCfgMemCnt = 0;
        }

        if (u1PortFound == SNOOP_TRUE)
        {
            /* Stop the PortPurge/Leave timer if it is running */
            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the Port Purge/Leave timer on port %u\r",
                                pSnoopASMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Update the Group entry V2 port bitmap */
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->ASMPortBitmap);

#ifdef L2RED_WANTED
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->V1PortBitmap);
#endif
            /* Stop the Host present timer if it is running */
            if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the Host present timer on port %u\r",
                                pSnoopASMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
            }

            /* Delete all the hosts attached to the port */
            if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                                "Deleting all the hosts attached to the port %u\r",
                                pSnoopASMPortEntry->u4Port);
                while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                      pSnoopASMHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                    SNOOP_MGMT_DBG,
                                    "Removing the host %s from the Host Information RBTree",
                                    SnoopPrintIPvxAddress (pSnoopASMHostEntry->
                                                           HostIpAddr));
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopASMHostEntry);

                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopASMHostEntry);
                }
            }

            /*Release the port node from the V1V2PortList */
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG, SNOOP_MGMT_DBG,
                            "Releasing the port node %u from the V1/V2 PortList\r\n",
                            pSnoopASMPortEntry->u4Port);
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortEntry);
            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);
        }

        if (u1SSMPortFound == SNOOP_TRUE)
        {
            /* Stop the Host present timer if it is running */
            if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the Host present timer on port %u\r",
                                pSnoopSSMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
            }

            /* Delete all the Host information for SSM port entry */
            if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
            {
                while ((pSnoopHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList,
                                      pSnoopHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopHostEntry);

                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopHostEntry);
                }
            }

            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortEntry);
            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortEntry);
        }

        /* Update the Group entry port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

        IPVX_ADDR_INIT (GrpAddr, pSnoopGroupEntry->GroupIpAddr.u1Afi,
                        pSnoopGroupEntry->GroupIpAddr.au1Addr);

        /* IP based mode */
        SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
        if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                      tempSrcAddr, u4Port,
                                      gNullPortBitMap, SNOOP_DEL_PORT)
            != SNOOP_SUCCESS)
        {
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                            SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                            "on V1/V2 port %d  purge timer expiry failed\r\n",
                            u4Port);
        }

        if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                           SNOOP_PORT_LIST_SIZE) != 0)
        {

            /* Check if any V2 receivers are there if present nothing needs 
             * to be done, else send filter mode change with existing V3 
             * source information */
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                /* If the ASM port bitmap has become NULL set the exclude 
                 * source bitmap for the group to default value and 
                 * consolidate the group source bitmaps */
                SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                               SNOOP_SRC_LIST_SIZE);

                u1ASMHostPresent =
                    pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent;

                SnoopGrpConsolidateSrcInfo (u4Instance, 0, 0, gNullSrcBmp,
                                            gNullSrcBmp, pSnoopGroupEntry,
                                            SNOOP_TRUE);

                if (SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp,
                                   SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                    u1RecordType = SNOOP_TO_EXCLUDE;
                }
                else
                {
                    pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
                    u1RecordType = SNOOP_TO_INCLUDE;
                }

                /* Check for any filter mode change has occurred so just 
                 * update router with the filter mode change record */
                if (SNOOP_MEM_CMP
                    (pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
                     SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)
                {
                    pSnoopGroupEntry->pConsGroupEntry->u1FilterMode =
                        SNOOP_EXCLUDE;
                    u1RecordType = SNOOP_TO_EXCLUDE;
                }
                else
                {
                    pSnoopGroupEntry->pConsGroupEntry->u1FilterMode =
                        SNOOP_INCLUDE;
                    u1RecordType = SNOOP_TO_INCLUDE;
                }

                /* After SnoopGrpConsolidateSrcInfo if the u1ASMHostPresent
                 * of consolidated group is SNOOP_FALSE then send the 
                 * report to the router ports */
                if ((pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent
                     == u1ASMHostPresent) && (u1ASMHostPresent == SNOOP_FALSE))
                {
                    pRBElem = pRBElemNext;
                    continue;
                }

                if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                           pSnoopGroupEntry->
                                           pConsGroupEntry->GroupIpAddr.
                                           u1Afi,
                                           &pSnoopVlanEntry) == SNOOP_SUCCESS)
                {
                    /* if V3RtrPortBitmap is not null then either IGMPV3 or
                       MLDV2 router ports are present for this Vlan */
                    SnoopVlanGetRtrPortFromPVlanMappingInfo
                        (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                         pSnoopGroupEntry->pConsGroupEntry->
                         GroupIpAddr.u1Afi,
                         SNOOP_IGS_IGMP_VERSION3, pSnoopRtrPortBmp,
                         pSnoopVlanEntry);
                    if ((SNOOP_MEM_CMP
                         (pSnoopRtrPortBmp, gNullPortBitMap,
                          sizeof (tSnoopPortBmp)) != 0))
                    {
                        /* if the forwarding is done based on MAC then 
                         * construct SSM Reports for all the Group records 
                         * with NULL source list */
                        /* Use the misc preallocated buffer and copy the 
                         * group informtaion to the buffer when the max 
                         * length reaches just send the buffer out */

                        for (u2Index = 1;
                             u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++)
                        {
                            if ((u1RecordType == SNOOP_IS_INCLUDE) ||
                                (u1RecordType == SNOOP_TO_INCLUDE))
                            {
                                OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                         pConsGroupEntry->
                                                         InclSrcBmp,
                                                         u2Index,
                                                         SNOOP_SRC_LIST_SIZE,
                                                         bResult);
                                if (bResult == OSIX_TRUE)
                                {
                                    u2NoSources++;
                                }
                            }
                            else
                            {
                                OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                         pConsGroupEntry->
                                                         ExclSrcBmp,
                                                         u2Index,
                                                         SNOOP_SRC_LIST_SIZE,
                                                         bResult);
                                if (bResult == OSIX_TRUE)
                                {
                                    u2NoSources++;
                                }
                            }
                        }

#ifdef IGS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV4)
                        {
                            IgsUtilFillV3SourceInfo (u4Instance,
                                                     pSnoopGroupEntry->
                                                     pConsGroupEntry,
                                                     u1RecordType,
                                                     u2NoSources, &u4MaxLength);
                            u2NoSources = 0;
                            u2NumGrpRec++;
                            if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                            {
                                i4Result = IgsEncodeAggV3Report
                                    (u4Instance, u2NumGrpRec, u4MaxLength,
                                     &pOutBuf);
                            }
                        }
#endif
#ifdef MLDS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV6)
                        {
                            MldsUtilFillV2SourceInfo (u4Instance,
                                                      pSnoopGroupEntry->
                                                      pConsGroupEntry,
                                                      u2NoSources,
                                                      u1RecordType,
                                                      &u4MaxLength);
                            u2NoSources = 0;
                            u2NumGrpRec++;
                            if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                            {
                                i4Result =
                                    MldsEncodeAggV2Report (u4Instance,
                                                           u2NumGrpRec,
                                                           u4MaxLength,
                                                           &pOutBuf);
                            }
                        }
#endif

                        if (((pSnoopVlanEntry->u1AddressType ==
                              SNOOP_ADDR_TYPE_IPV4)
                             && (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                            ||
                            ((pSnoopVlanEntry->u1AddressType ==
                              SNOOP_ADDR_TYPE_IPV6)
                             && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                        {
                            if (i4Result != SNOOP_SUCCESS)
                            {
                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_PKT_NAME,
                                               "Unable to generate and send SSM"
                                               "report on to router ports\r\n");
                                UtilPlstReleaseLocalPortList (pPortBitmap);
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }

                            SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                           SNOOP_MISC_MEMBLK_SIZE);

                            SnoopVlanGetRtrPortFromPVlanMappingInfo
                                (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                 pSnoopVlanEntry->u1AddressType, 0,
                                 pSnoopRtrPortBmp, pSnoopVlanEntry);
                            /* Send the report message to router ports */
                            SNOOP_MEM_CPY (pPortBitmap, pSnoopRtrPortBmp,
                                           sizeof (tSnoopPortBmp));

                            SnoopVlanForwardPacket (u4Instance,
                                                    VlanId,
                                                    pSnoopVlanEntry->
                                                    u1AddressType,
                                                    SNOOP_INVALID_PORT,
                                                    VLAN_FORWARD_SPECIFIC,
                                                    pOutBuf, pPortBitmap,
                                                    SNOOP_SSMREPORT_SENT);

                            u2NumGrpRec = 0;
                        }
                    }
                    else
                    {
#ifdef IGS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV4)
                        {
                            /* if v2 Router port list is not empty then send 
                               a V2 Report */
                            SnoopVlanGetRtrPortFromPVlanMappingInfo
                                (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                 pSnoopVlanEntry->u1AddressType,
                                 SNOOP_IGS_IGMP_VERSION2, pSnoopRtrPortBmp,
                                 pSnoopVlanEntry);
                            if (SNOOP_MEM_CMP
                                (pSnoopRtrPortBmp, gNullPortBitMap,
                                 sizeof (tSnoopPortBmp)) != 0)
                            {
                                if (IgsEncodeQueryRespMsg
                                    (u4Instance, pSnoopVlanEntry,
                                     pSnoopGroupEntry->pConsGroupEntry,
                                     SNOOP_IGS_IGMP_VERSION2,
                                     pSnoopRtrPortBmp, 0) != SNOOP_SUCCESS)
                                {
                                    UtilPlstReleaseLocalPortList (pPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;
                                }

                            }

                            /* if v1 Router port list is not empty then send a V1 Report */
                            SnoopVlanGetRtrPortFromPVlanMappingInfo
                                (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                 pSnoopVlanEntry->u1AddressType,
                                 SNOOP_IGS_IGMP_VERSION1, pSnoopRtrPortBmp,
                                 pSnoopVlanEntry);
                            if (SNOOP_MEM_CMP
                                (pSnoopRtrPortBmp, gNullPortBitMap,
                                 sizeof (tSnoopPortBmp)) != 0)
                            {
                                if (IgsEncodeQueryRespMsg
                                    (u4Instance, pSnoopVlanEntry,
                                     pSnoopGroupEntry->pConsGroupEntry,
                                     SNOOP_IGS_IGMP_VERSION1,
                                     pSnoopRtrPortBmp, 0) != SNOOP_SUCCESS)
                                {
                                    UtilPlstReleaseLocalPortList (pPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;
                                }
                            }
                        }
#endif
#ifdef MLDS_WANTED
                        if (pSnoopVlanEntry->u1AddressType ==
                            SNOOP_ADDR_TYPE_IPV6)
                        {
                            /* if v2 Router port list is not empty then 
                               send a MLDV1 Report */
                            if (SNOOP_MEM_CMP (pSnoopVlanEntry->V2RtrPortBitmap,
                                               gNullPortBitMap,
                                               sizeof (tSnoopPortBmp)) != 0)
                            {
                                if (MldsEncodeQueryRespMsg
                                    (u4Instance, pSnoopVlanEntry,
                                     pSnoopGroupEntry->pConsGroupEntry,
                                     SNOOP_MLD_VERSION1,
                                     pSnoopVlanEntry->V2RtrPortBitmap,
                                     SNOOP_MLD_V1REPORT) != SNOOP_SUCCESS)
                                {
                                    UtilPlstReleaseLocalPortList (pPortBitmap);
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;
                                }

                            }
                        }
#endif

                        if (i4Result != SNOOP_SUCCESS)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_CONTROL_PATH_TRC |
                                           SNOOP_DBG_ALL_FAILURE,
                                           SNOOP_PKT_NAME,
                                           "Unable to send "
                                           "response to query\r\n");
                            UtilPlstReleaseLocalPortList (pPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                            return SNOOP_FAILURE;
                        }
                    }
                }
            }

            pRBElem = pRBElemNext;
            continue;
        }
        else
        {
            pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;

            if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 1)
            {
                u1IsLastGroupEntry = SNOOP_TRUE;
            }

            if (u1IsLastGroupEntry == SNOOP_TRUE)
            {
                /* Form and Send Leave message */
#ifdef IGS_WANTED
                if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
                {
                    i4Result = IgsEncodeAndSendLeaveMsg (u4Instance, VlanId,
                                                         pSnoopConsGroupEntry);
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Result = MldsEncodeAndSendDoneMsg (u4Instance, VlanId,
                                                         pSnoopConsGroupEntry);
                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                        "Unable to send Leave message "
                                        "for the Group %s\r\n",
                                        SnoopPrintIPvxAddress (GrpAddr));
                    UtilPlstReleaseLocalPortList (pPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
            }

            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                          GrpAddr) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Deletion of Group record failed\r\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }

            if (u1IsLastGroupEntry == SNOOP_FALSE)
            {
                SnoopGrpCheckAndSendConsReport (u4Instance,
                                                pSnoopConsGroupEntry);
            }
            u1IsLastGroupEntry = SNOOP_FALSE;
        }
        pRBElem = pRBElemNext;
    }
    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

#ifdef IGS_WANTED
    /* Have to be included for MLDS also */
    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               u1AddressType, &pSnoopVlanEntry)
        == SNOOP_SUCCESS)

    {
        if (IgsEncodeQuery (u4Instance, 0, SNOOP_IGMP_QUERY,
                            pSnoopVlanEntry->pSnoopVlanCfgEntry->
                            u1ConfigOperVer, &pBuf,
                            pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_TMR_NAME,
                           "Unable to send General Query message"
                           "timer expiry\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_QRY_DBG, SNOOP_QRY_DBG,
                       "Unable to send General Query message" "timer expiry\n");
            UtilPlstReleaseLocalPortList (pPortBitmap);
            return SNOOP_FAILURE;
        }
        SNOOP_ADD_TO_PORT_LIST (u4Port, pPortBitmap);
        u4Port = SNOOP_INVALID_PORT;
        SnoopVlanForwardQuery (u4Instance, VlanId, u1AddressType,
                               u4Port, VLAN_FORWARD_SPECIFIC, pBuf,
                               pPortBitmap, SNOOP_QUERY_SENT);
    }
#endif

    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeletePortEntry                              */
/*                                                                           */
/* Description        : This function deletes port entry corresponding to    */
/*                      all the group entries in the VLAN                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnpVlEntry   - Vlan Entry                           */
/*                      u4Port     - Port number                             */
/*                      McastAddr  - Mac Address                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpDeletePortEntry (UINT4 u4Instance, tSnoopVlanEntry * pSnpVlEntry,
                         UINT4 u4Port, tMacAddr McastAddr)
{
    UINT1              *pPortBitmap = NULL;
    UINT1              *pTmpPortBitMap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopExtPortCfgEntry *pSnoopExtPortCfgEntry = NULL;

    tMacAddr            MacGroupAddr;
    tMacAddr            NullMacAddr;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           tempSrcAddr;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopTag           VlanId;
    UINT4               u4TempGrpIpAddr = 0;
    UINT4               u4MaxLength = 0;
    INT4                i4Result = 0;
    UINT2               u2NumGrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;
    UINT1               u1RecordType = 0;
    UINT1               u1PortFound = SNOOP_FALSE;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1SSMPortFound = SNOOP_FALSE;
    UINT1               u1IsLastGroupEntry = SNOOP_FALSE;
    UINT1               u1PhyPortFound = SNOOP_FALSE;
    CHR1               *pc1String = NULL;
    CHR1                au1Ip6Addr[IPVX_IPV6_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bASMPort = OSIX_FALSE;
    BOOL1               bRes = OSIX_FALSE;
    UINT1               u1Receiver = SNOOP_FALSE;
    UINT4               u4LearntPort = 0;
    UINT1               u1PrtFlag = 0;
    UINT4               u4LrtPortCount = 0;
    UINT4               u4RtrPortCount = 0;
#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

    /* This function will be called in the following scenarios:
     *   - Vlan member-port modification 
     *   - Static multicast entry member-port modification 
     *   - Port delete / port disable event
     * So this routine will delete the port in the group entry based on
     * Outer-VLAN alone. Inner-VLAN will not be considered. */
    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (&MacGroupAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&VlanId, 0, sizeof (tSnoopTag));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_CPY (&VlanId, pSnpVlEntry->VlanId, sizeof (tSnoopTag));

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
    pSnoopRtrPortBmp = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pSnoopRtrPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pSnoopRtrPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

    while (pRBElem != NULL)
    {
        u1PortFound = SNOOP_FALSE;
        u1SSMPortFound = SNOOP_FALSE;
        u1Receiver = SNOOP_FALSE;

        pRBElemNext = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                     GroupEntry, pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        /* If the multicast mac address is not NULL then delete the port entry
         * for VlanId and Multicast Group address address */

        if (SNOOP_MEM_CMP (McastAddr, NullMacAddr, sizeof (tMacAddr)) != 0)
        {
            SNOOP_MEM_SET (MacGroupAddr, 0, sizeof (tMacAddr));

#ifdef IGS_WANTED
            if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                         (pSnoopGroupEntry->GroupIpAddr.
                                          au1Addr), MacGroupAddr);
            }

#endif

#ifdef MLDS_WANTED
            if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_GET_MAC_FROM_IPV6 (pSnoopGroupEntry->GroupIpAddr.au1Addr,
                                         MacGroupAddr);
            }
#endif
            if ((SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)
                 != SNOOP_OUTER_VLAN (VlanId)) &&
                (SNOOP_MEM_CMP (McastAddr, MacGroupAddr,
                                sizeof (tMacAddr)) != 0))
            {
                pRBElem = pRBElemNext;
                continue;
            }
        }
        else if (SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)
                 != SNOOP_OUTER_VLAN (VlanId))
        {
            pRBElem = pRBElemNext;
            continue;
        }
        else if (pSnoopGroupEntry->GroupIpAddr.u1Afi !=
                 pSnpVlEntry->u1AddressType)
        {
            pRBElem = pRBElemNext;
            continue;
        }

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap, bResult);

        if (bResult == OSIX_FALSE)
        {
            pRBElem = pRBElemNext;
            continue;
        }
        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->ASMPortBitmap,
                               bASMPort);
        u1PortFound = (bASMPort == OSIX_TRUE) ? SNOOP_TRUE : SNOOP_FALSE;
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopSSMPortEntry->u4Port == u4Port)
                {
                    u1SSMPortFound = SNOOP_TRUE;
                    break;
                }
            }
        }

        if ((u1PortFound == SNOOP_FALSE) && (u1SSMPortFound == SNOOP_FALSE))
        {
            pRBElem = pRBElemNext;
            continue;
        }

        /*  Decrement the threshold couter by 1 */
        /*  Convert to physical port and set a flag */
        if (SnoopGetPortCfgEntry
            (u4Instance, u4Port, pSnoopGroupEntry->VlanId,
             pSnoopGroupEntry->GroupIpAddr.u1Afi,
             &pSnoopPortCfgEntry) != SNOOP_FAILURE)
        {
            pSnoopExtPortCfgEntry = pSnoopPortCfgEntry->pSnoopExtPortCfgEntry;
            if (pSnoopExtPortCfgEntry != NULL)
            {
                if ((pSnoopExtPortCfgEntry->u1SnoopPortMaxLimitType
                     == SNOOP_PORT_LMT_TYPE_GROUPS) &&
                    (SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                     u1FilterStatus != SNOOP_DISABLE))
                {
                    SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntry);
                }
                u1PhyPortFound = SNOOP_TRUE;
            }
        }

        if (u1PortFound == SNOOP_TRUE)
        {
            /* Get the ASM port entry */
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                            pSnoopASMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopASMPortEntry->u4Port == u4Port)
                {
                    break;
                }
            }
            if ((pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_STATIC)
                && (SNOOP_PORT_ACTION == SNOOP_DEL_PORT))
            {
                IPVX_ADDR_INIT (GrpAddr, pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                pSnoopGroupEntry->GroupIpAddr.au1Addr);
                if (SNOOP_MCAST_FWD_MODE (u4Instance) ==
                    SNOOP_MCAST_FWD_MODE_MAC)
                {
                    if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                                   u4Port, SNOOP_DEL_PORT,
                                                   SNOOP_DISABLE_PORT) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                       "Updation of MAC forwarding table"
                                       "failed\r\n");
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                }
                else
                {
                    /* IP based mode */
                    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
                    if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                                  tempSrcAddr, u4Port,
                                                  gNullPortBitMap,
                                                  SNOOP_DEL_PORT) !=
                        SNOOP_SUCCESS)
                    {
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                }

                if (SnoopGrpDeleteGroupEntry
                    (u4Instance, pSnoopGroupEntry->VlanId,
                     GrpAddr) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_DBG_GRP,
                                   SNOOP_GRP_NAME,
                                   "Deletion of Group record failed\r\n");

                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_SUCCESS;
            }

            if (pSnoopASMPortEntry == NULL)
            {
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }

            /* Stop the PortPurge/Leave timer if it is running */
            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_DBG_TMR,
                                    SNOOP_TMR_NAME,
                                    "Stopping the PortPurge/Leave timer on port %d\r\n",
                                    pSnoopASMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Update the Group entry V2 port bitmap */
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG,
                       SNOOP_GRP_DBG,
                       "Updating the Group entry V2 port bitmap\r\n");
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->ASMPortBitmap);

#ifdef L2RED_WANTED
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->V1PortBitmap);
#endif

            /* Stop the Host present timer if it is running */
            if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                                SNOOP_TMR_DBG,
                                "Stopping the Host present timer on port %d\r\n",
                                pSnoopASMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
            }

            /* Delete all the hosts attached to the port */
            if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                SNOOP_MGMT_DBG,
                                "Deleting all the hosts attached to the port %d\r\n",
                                pSnoopASMPortEntry->u4Port);
                while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                      pSnoopASMHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                               SNOOP_MGMT_DBG,
                               "Removing the host from the Host Information RBTree\r\n");
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopASMHostEntry);

                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopASMHostEntry);
                }
            }

            /*Release the port node from the V1V2PortList */
            SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                            SNOOP_MGMT_DBG,
                            "Releasing the port node %d from the V1V2PortList\r\n",
                            pSnoopASMPortEntry->u4Port);
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortEntry);
            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);
        }

        if (u1SSMPortFound == SNOOP_TRUE)
        {
            /* Stop the Host present timer if it is running */
            if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                                SNOOP_TMR_DBG,
                                "Stopping the Host present timer on port %d\r\n",
                                pSnoopSSMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
            }

            if (pSnoopExtPortCfgEntry != NULL)
            {
                if ((u1PhyPortFound == SNOOP_TRUE) &&
                    (pSnoopExtPortCfgEntry->u1SnoopPortMaxLimitType
                     == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
                    (SNOOP_INSTANCE_INFO (u4Instance)->
                     SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                     u1FilterStatus != SNOOP_DISABLE))
                {
                    for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE;
                         u2ByteIndex++)
                    {
                        u1PortFlag =
                            pSnoopSSMPortEntry->pSourceBmp->
                            InclSrcBmp[u2ByteIndex];
                        if (u1PortFlag == 0)
                        {
                            continue;
                        }
                        for (u2BitIndex = 0;
                             ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
                        {
                            if ((u1PortFlag & SNOOP_BIT8) != 0)
                            {
                                SNOOP_DECR_MEMBER_CNT (pSnoopExtPortCfgEntry);
                            }
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                        }
                    }

                }
            }

            /* Delete all the Host information for SSM port entry */
            if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                           SNOOP_MGMT_DBG,
                           "Deleting all the Host information for SSM port entry\r\n");
                while ((pSnoopHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList,
                                      pSnoopHostEntry);
                    /* Remove the host from the Host Information RBTree */
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                               SNOOP_MGMT_DBG,
                               "Removing the host from the Host Information RBTree\r\n");
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopHostEntry);

                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopHostEntry);
                }
            }

            /* Decrement the threshold current Count by the number
             * of bits set in the Include source bitmap */
            /* Release the port node from the V1V2PortList */
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortEntry);
            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortEntry);
        }

        /* Update the Group entry port bitmap */
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

        IPVX_ADDR_INIT (GrpAddr, pSnoopGroupEntry->GroupIpAddr.u1Afi,
                        pSnoopGroupEntry->GroupIpAddr.au1Addr);

        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            /* Send this Group Membership Information sync to the 
             * Standby Node.
             */
            SnoopRedActiveSendGrpInfoSync (u4Instance, pSnoopGroupEntry);

            if (SnoopFwdUpdateMacFwdTable (u4Instance, pSnoopGroupEntry,
                                           u4Port, SNOOP_DEL_PORT,
                                           SNOOP_DISABLE_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                           "on V1/V2 port purge timer expiry failed\r\n");
            }
        }
        else
        {
            /* IP based mode */
            SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
            if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                          tempSrcAddr, u4Port,
                                          gNullPortBitMap, SNOOP_DEL_PORT)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                           "on V1/V2 port purge timer expiry failed\r\n");
            }
        }

        if (((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pSnpVlEntry->u1AddressType - 1].
              u1ReportFwdAll == SNOOP_FORWARD_ALL_PORTS)) &&
            ((SNOOP_INSTANCE_INFO (u4Instance)->
              SnoopInfo[pSnpVlEntry->u1AddressType - 1].
              u1QueryFwdAll == SNOOP_FORWARD_ALL_PORTS)))
        {
            u4LrtPortCount = 0;
            u4RtrPortCount = 0;
            u1Receiver = SNOOP_FALSE;
            pTmpPortBitMap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pTmpPortBitMap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC,
                           "Error in allocating memory for pTmpPortBitMap\r\n");
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pTmpPortBitMap, 0, sizeof (tSnoopPortBmp));

            SNOOP_MEM_CPY (pTmpPortBitMap, pSnoopGroupEntry->PortBitmap,
                           sizeof (tSnoopPortBmp));

            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pSnoopGroupEntry->
                                                     GroupIpAddr.u1Afi, 0,
                                                     pSnoopRtrPortBmp,
                                                     pSnoopVlanEntry);

            SNOOP_DEL_FROM_PORT_LIST (u4Port, pTmpPortBitMap);

            for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PrtFlag = pTmpPortBitMap[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                      && (u1PrtFlag != 0)); u2BitIndex++)
                {

                    if ((u1PrtFlag & SNOOP_BIT8) != 0)
                    {
                        u4LearntPort =
                            (UINT4) ((u2ByteIndex * SNOOP_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                        SNOOP_IS_PORT_PRESENT (u4LearntPort,
                                               pSnoopRtrPortBmp, bRes);
                        if (bRes == OSIX_TRUE)
                        {
                            u4RtrPortCount++;
                        }
                        else
                        {
                            u4LrtPortCount++;
                        }
                    }
                    u1PrtFlag = (UINT1) (u1PrtFlag << 1);
                }
            }
            UtilPlstReleaseLocalPortList (pTmpPortBitMap);

            if ((u4LrtPortCount > 0) || u4RtrPortCount > 1)
            {
                u1Receiver = SNOOP_TRUE;
            }

        }
        else
        {
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                               SNOOP_PORT_LIST_SIZE) != 0)
            {
                u1Receiver = SNOOP_TRUE;
            }
        }

        if (u1Receiver == SNOOP_TRUE)
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
            {
                /* Check if any V2 receivers are there if present nothing needs 
                 * to be done, else send filter mode change with existing V3 
                 * source information */
                if (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* If the ASM port bitmap has become NULL set the exclude 
                     * source bitmap for the group to default value and 
                     * consolidate the group source bitmaps */
                    SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                                   SNOOP_SRC_LIST_SIZE);

                    u1ASMHostPresent =
                        pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent;

                    SnoopGrpConsolidateSrcInfo (u4Instance, 0, 0, gNullSrcBmp,
                                                gNullSrcBmp, pSnoopGroupEntry,
                                                SNOOP_TRUE);

                    /* After SnoopGrpConsolidateSrcInfo if the Consolidated    
                     * entry still has other ASM receivers then there will be 
                     * no change. Hence no need to send any report to the 
                     * router ports. */
                    if ((pSnoopGroupEntry->pConsGroupEntry->u1ASMHostPresent
                         == u1ASMHostPresent) &&
                        (u1ASMHostPresent == SNOOP_FALSE))
                    {
                        pRBElem = pRBElemNext;
                        continue;
                    }

                    /* Check for any filter mode change has occurred so just 
                     * update router with the filter mode change record */
                    if (SNOOP_MEM_CMP
                        (pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
                         SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)
                    {
                        u1RecordType = SNOOP_TO_EXCLUDE;
                    }
                    else
                    {
                        u1RecordType = SNOOP_TO_INCLUDE;
                    }

                    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                                               pSnoopGroupEntry->
                                               pConsGroupEntry->GroupIpAddr.
                                               u1Afi,
                                               &pSnoopVlanEntry)
                        == SNOOP_SUCCESS)
                    {
                        SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                                 SNOOP_OUTER_VLAN
                                                                 (VlanId),
                                                                 pSnoopGroupEntry->
                                                                 pConsGroupEntry->
                                                                 GroupIpAddr.
                                                                 u1Afi,
                                                                 SNOOP_IGS_IGMP_VERSION3,
                                                                 pSnoopRtrPortBmp,
                                                                 pSnoopVlanEntry);
                        /* if V3RtrPortBitmap is not null then either IGMPV3 or
                           MLDV2 router ports are present for this Vlan */
                        if ((SNOOP_MEM_CMP (pSnoopRtrPortBmp, gNullPortBitMap,
                                            sizeof (tSnoopPortBmp)) != 0))
                        {
                            /* if the forwarding is done based on MAC then 
                             * construct SSM Reports for all the Group records 
                             * with NULL source list */
                            /* Use the misc preallocated buffer and copy the 
                             * group informtaion to the buffer when the max 
                             * length reaches just send the buffer out */

                            for (u2Index = 1;
                                 u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                                 u2Index++)
                            {
                                if ((u1RecordType == SNOOP_IS_INCLUDE) ||
                                    (u1RecordType == SNOOP_TO_INCLUDE))
                                {
                                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                             pConsGroupEntry->
                                                             InclSrcBmp,
                                                             u2Index,
                                                             SNOOP_SRC_LIST_SIZE,
                                                             bResult);
                                    if (bResult == OSIX_TRUE)
                                    {
                                        u2NoSources++;
                                    }
                                }
                                else
                                {
                                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                             pConsGroupEntry->
                                                             ExclSrcBmp,
                                                             u2Index,
                                                             SNOOP_SRC_LIST_SIZE,
                                                             bResult);
                                    if (bResult == OSIX_TRUE)
                                    {
                                        u2NoSources++;
                                    }
                                }
                            }

#ifdef IGS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV4)
                            {
                                IgsUtilFillV3SourceInfo (u4Instance,
                                                         pSnoopGroupEntry->
                                                         pConsGroupEntry,
                                                         u1RecordType,
                                                         u2NoSources,
                                                         &u4MaxLength);
                                u2NoSources = 0;
                                u2NumGrpRec++;
                                if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                                {
                                    i4Result = IgsEncodeAggV3Report
                                        (u4Instance, u2NumGrpRec, u4MaxLength,
                                         &pOutBuf);
                                }
                            }
#endif
#ifdef MLDS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV6)
                            {
                                MldsUtilFillV2SourceInfo (u4Instance,
                                                          pSnoopGroupEntry->
                                                          pConsGroupEntry,
                                                          u2NoSources,
                                                          u1RecordType,
                                                          &u4MaxLength);
                                u2NoSources = 0;
                                u2NumGrpRec++;
                                if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                                {
                                    i4Result =
                                        MldsEncodeAggV2Report (u4Instance,
                                                               u2NumGrpRec,
                                                               u4MaxLength,
                                                               &pOutBuf);
                                }
                            }
#endif

                            if (((pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV4)
                                 && (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                                ||
                                ((pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV6)
                                 && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                            {
                                if (i4Result != SNOOP_SUCCESS)
                                {
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE,
                                                   SNOOP_PKT_NAME,
                                                   "Unable to generate and send SSM"
                                                   "report on to router ports\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;
                                }

                                SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                               SNOOP_MISC_MEMBLK_SIZE);
                                pPortBitmap =
                                    UtilPlstAllocLocalPortList (sizeof
                                                                (tSnoopPortBmp));
                                if (pPortBitmap == NULL)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_GRP_TRC,
                                               "Error in allocating memory for pPortBitmap\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pSnoopRtrPortBmp);
                                    return SNOOP_FAILURE;
                                }
                                SNOOP_MEM_SET (pPortBitmap, 0,
                                               sizeof (tSnoopPortBmp));

                                /* Send the report message to IGMPV3 or MLDv2 router ports */
                                SNOOP_MEM_CPY (pPortBitmap, pSnoopRtrPortBmp,
                                               sizeof (tSnoopPortBmp));
                                SNOOP_TRC (SNOOP_TRC_FLAG,
                                           SNOOP_DATA_TRC,
                                           SNOOP_DATA_TRC,
                                           "Sending the report message to IGMPV3 or MLDv2"
                                           "router ports\r\n");

                                SnoopVlanForwardPacket (u4Instance,
                                                        pSnoopVlanEntry->VlanId,
                                                        pSnoopVlanEntry->
                                                        u1AddressType,
                                                        SNOOP_INVALID_PORT,
                                                        VLAN_FORWARD_SPECIFIC,
                                                        pOutBuf, pPortBitmap,
                                                        SNOOP_SSMREPORT_SENT);
                                UtilPlstReleaseLocalPortList (pPortBitmap);
                                u2NumGrpRec = 0;
                            }
                        }
                        else
                        {
#ifdef IGS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV4)
                            {
                                /* if v2 Router port list is not empty then 
                                 * send a V2 Report */
                                SnoopVlanGetRtrPortFromPVlanMappingInfo
                                    (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                     pSnoopVlanEntry->u1AddressType,
                                     SNOOP_IGS_IGMP_VERSION2, pSnoopRtrPortBmp,
                                     pSnoopVlanEntry);
                                if (SNOOP_MEM_CMP
                                    (pSnoopRtrPortBmp, gNullPortBitMap,
                                     sizeof (tSnoopPortBmp)) != 0)
                                {
                                    SNOOP_TRC (SNOOP_TRC_FLAG,
                                               SNOOP_DATA_TRC,
                                               SNOOP_DATA_TRC,
                                               "sending a V2 Report\r\n");
                                    if (IgsEncodeQueryRespMsg
                                        (u4Instance, pSnoopVlanEntry,
                                         pSnoopGroupEntry->pConsGroupEntry,
                                         SNOOP_IGS_IGMP_VERSION2,
                                         pSnoopRtrPortBmp, 0) != SNOOP_SUCCESS)
                                    {
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        return SNOOP_FAILURE;
                                    }

                                }

                                /* if v1 Router port list is not empty then send a 
                                 * V1 Report */
                                SnoopVlanGetRtrPortFromPVlanMappingInfo
                                    (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                     pSnoopVlanEntry->u1AddressType,
                                     SNOOP_IGS_IGMP_VERSION1, pSnoopRtrPortBmp,
                                     pSnoopVlanEntry);
                                if (SNOOP_MEM_CMP
                                    (pSnoopRtrPortBmp, gNullPortBitMap,
                                     sizeof (tSnoopPortBmp)) != 0)
                                {
                                    SNOOP_TRC (SNOOP_TRC_FLAG,
                                               SNOOP_DATA_TRC,
                                               SNOOP_DATA_TRC,
                                               "sending a V1 Report\r\n");
                                    if (IgsEncodeQueryRespMsg
                                        (u4Instance, pSnoopVlanEntry,
                                         pSnoopGroupEntry->pConsGroupEntry,
                                         SNOOP_IGS_IGMP_VERSION1,
                                         pSnoopRtrPortBmp, 0) != SNOOP_SUCCESS)
                                    {
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        return SNOOP_FAILURE;
                                    }

                                }
                            }
#endif
#ifdef MLDS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV6)
                            {
                                /* if v2 Router port list is not empty then 
                                 * send a MLDV1 Report */
                                if (SNOOP_MEM_CMP (pSnoopVlanEntry->
                                                   V2RtrPortBitmap,
                                                   gNullPortBitMap,
                                                   sizeof (tSnoopPortBmp)) != 0)
                                    SNOOP_TRC (SNOOP_TRC_FLAG,
                                               SNOOP_DATA_TRC,
                                               SNOOP_DATA_TRC,
                                               "sending a MLDV1 Report\r\n");
                                {
                                    if (MldsEncodeQueryRespMsg
                                        (u4Instance, pSnoopVlanEntry,
                                         pSnoopGroupEntry->pConsGroupEntry,
                                         SNOOP_MLD_VERSION1,
                                         pSnoopVlanEntry->V2RtrPortBitmap,
                                         SNOOP_MLD_V1REPORT) != SNOOP_SUCCESS)
                                    {
                                        UtilPlstReleaseLocalPortList
                                            (pSnoopRtrPortBmp);
                                        return SNOOP_FAILURE;
                                    }

                                }
                            }
#endif

                            if (i4Result != SNOOP_SUCCESS)
                            {
                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_PKT_NAME,
                                               "Unable to send "
                                               "response to query\r\n");
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                        }
                    }
                }
            }
            pRBElem = pRBElemNext;
            continue;
        }
        else
        {
            pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;

            if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 1)
            {
                u1IsLastGroupEntry = SNOOP_TRUE;
            }

            if (u1IsLastGroupEntry == SNOOP_TRUE)
            {
                SnoopGrpConsolidateSrcInfo (u4Instance, 0, 0, gNullSrcBmp,
                                            gNullSrcBmp, pSnoopGroupEntry,
                                            SNOOP_TRUE);
                /* Form and Send Leave message */
#ifdef IGS_WANTED
                if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
                {
                    SNOOP_TRC (SNOOP_TRC_FLAG,
                               SNOOP_DATA_TRC,
                               SNOOP_DATA_TRC, "sending a Leave message \r\n");
                    i4Result = IgsEncodeAndSendLeaveMsg (u4Instance, VlanId,
                                                         pSnoopConsGroupEntry);
                }
#endif

#ifdef MLDS_WANTED
                if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Result = MldsEncodeAndSendDoneMsg (u4Instance, VlanId,
                                                         pSnoopConsGroupEntry);
                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    if (pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                        SNOOP_ADDR_TYPE_IPV4)
                    {
                        SNOOP_MEM_CPY (&u4TempGrpIpAddr, GrpAddr.au1Addr,
                                       IPVX_IPV4_ADDR_LEN);
                        u4TempGrpIpAddr = SNOOP_HTONL (u4TempGrpIpAddr);
                        SNOOP_CONVERT_IPADDR_TO_STR (pc1String,
                                                     u4TempGrpIpAddr);
                    }
                    else if (pSnoopGroupEntry->GroupIpAddr.u1Afi ==
                             SNOOP_ADDR_TYPE_IPV6)
                    {
                        SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
                        SNOOP_MEM_CPY (au1Ip6Addr, GrpAddr.au1Addr,
                                       IPVX_IPV6_ADDR_LEN);
                        SNOOP_INET_NTOHL (au1Ip6Addr);
                        pc1String =
                            (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                   au1Ip6Addr);
                    }
                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                        "Unable to send Leave message "
                                        "for the Group %s\r\n", pc1String);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }

            }

            if (u1IsLastGroupEntry == SNOOP_TRUE)
            {
                SnoopUtilDeleteIpFwdEntriesForOuterVlan (u4Instance,
                                                         pSnoopGroupEntry);
            }

            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType ==
                SNOOP_INVALID_TIMER_TYPE)
            {
                /* Delete the group node and free memblock */
                if (SnoopGrpDeleteGroupEntry
                    (u4Instance, pSnoopGroupEntry->VlanId,
                     GrpAddr) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Deletion of Group record failed\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return SNOOP_FAILURE;
                }
            }

            if (u1IsLastGroupEntry == SNOOP_FALSE)
            {
                SnoopGrpCheckAndSendConsReport (u4Instance,
                                                pSnoopConsGroupEntry);
            }
            u1IsLastGroupEntry = SNOOP_FALSE;
        }
        pRBElem = pRBElemNext;
    }

    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopClassifyPacketType                              */
/*                                                                           */
/* Description        : This function is used to classify the IGMP v3        */
/*                      /MLD v2 record types and update the statistics       */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      u1RecordType - Record type                           */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - snoop packet information            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopClassifyPacketType (UINT4 u4Instance, UINT1 u1RecordType,
                         tSnoopGroupEntry * pSnoopGroupEntry,
                         UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                         tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    UINT1               u1UpdateStat = 0;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1UpdateMrp = SNOOP_TRUE;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopClassifyPacketType\r\n");
    if (SnoopVlanGetVlanEntry
        (u4Instance, pSnoopGroupEntry->VlanId,
         pSnoopGroupEntry->GroupIpAddr.u1Afi,
         &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_GRP_NAME,
                            "Vlan entry for vlan %d not found\r\n",
                            SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId));
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopClassifyPacketType\r\n");
        return SNOOP_FAILURE;
    }

    if (u2NumSrcs == 0)
    {
        if (SnoopClassifyNonePacketType (u4Instance, u1RecordType,
                                         pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME, "Failed to update status \r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopClassifyPacketType\r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopClassifyPacketType\r\n");
        return SNOOP_SUCCESS;
    }

    /* Depending upon the record type update group entry */
    switch (u1RecordType)
    {
        case SNOOP_TO_INCLUDE:
        case SNOOP_IS_INCLUDE:
            if (SnoopGrpHandleToInclude (u4Instance, pSnoopGroupEntry,
                                         pSnoopVlanEntry, u2NumSrcs,
                                         pSnoopPktInfo,
                                         pSnoopPortCfgEntry,
                                         &u1UpdateMrp) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP,
                           SNOOP_GRP_TRC, "Processing of TO_INCLUDE messages"
                           " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = ((u1RecordType == SNOOP_IS_INCLUDE) ?
                                SNOOP_IS_INCLUDE_RCVD : SNOOP_TO_INCLUDE_RCVD);
            }
            break;

        case SNOOP_TO_EXCLUDE:
        case SNOOP_IS_EXCLUDE:
            if (SnoopGrpHandleToExclude (u4Instance, pSnoopGroupEntry,
                                         u2NumSrcs, pSnoopPktInfo,
                                         pSnoopPortCfgEntry,
                                         &u1UpdateMrp) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_GRP,
                           SNOOP_GRP_TRC, "Processing of TO_EXCLUDE messages"
                           " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = ((u1RecordType == SNOOP_IS_EXCLUDE) ?
                                SNOOP_IS_EXCLUDE_RCVD : SNOOP_TO_EXCLUDE_RCVD);
            }
            break;

        case SNOOP_ALLOW:
            if (SnoopGrpHandleAllow (u4Instance, pSnoopGroupEntry,
                                     u2NumSrcs, pSnoopPktInfo,
                                     pSnoopPortCfgEntry) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC, "Processing of ALLOW messages"
                           " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = SNOOP_ALLOW_RCVD;
            }
            break;

        case SNOOP_BLOCK:
            if (SnoopGrpHandleBlock (u4Instance, pSnoopGroupEntry,
                                     u2NumSrcs, pSnoopPktInfo,
                                     pSnoopPortCfgEntry) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_GRP_TRC, "Processing of BLOCK messages"
                           " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = SNOOP_BLOCK_RCVD;
            }
            break;

        default:
            break;
    }
    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                              u1UpdateStat) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN, SNOOP_PKT_TRC,
                        "Unable to update SNOOP statistics for VLAN ID %d\r\n",
                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
    }

    /* Update Active/Successful joins received count */
    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                              SNOOP_ACTIVE_JOINS) != SNOOP_SUCCESS)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_VLAN, SNOOP_PKT_TRC,
                        "Unable to update SNOOP statistics for VLAN ID %d\r\n",
                        SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopClassifyPacketType\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopClassifyNonePacketType                          */
/*                                                                           */
/* Description        : This function is used to classify the IGMP v3        */
/*                      /MLD v2 INCLUDE NONE /EXCLUDE NONE record types and  */
/*                      update the statistics                                */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     /*                                                                           *//* Input(s)           : u4Instance - Instance number                         */
/*                      u1RecordType - Record type                           */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopClassifyNonePacketType (UINT4 u4Instance, UINT1 u1RecordType,
                             tSnoopVlanEntry * pSnoopVlanEntry)
{
    UINT1               u1UpdateStat = 0;
    /* Depending upon the record type update group entry */
    switch (u1RecordType)
    {
        case SNOOP_TO_INCLUDE:
        case SNOOP_IS_INCLUDE:
            u1UpdateStat = ((u1RecordType == SNOOP_IS_INCLUDE) ?
                            SNOOP_IS_INCLUDE_RCVD : SNOOP_TO_INCLUDE_RCVD);
            break;
        case SNOOP_TO_EXCLUDE:
        case SNOOP_IS_EXCLUDE:
            u1UpdateStat = ((u1RecordType == SNOOP_IS_EXCLUDE) ?
                            SNOOP_IS_EXCLUDE_RCVD : SNOOP_TO_EXCLUDE_RCVD);
            break;
        case SNOOP_ALLOW:
            u1UpdateStat = SNOOP_ALLOW_RCVD;
            break;
        case SNOOP_BLOCK:
            u1UpdateStat = SNOOP_BLOCK_RCVD;
            break;
        default:
            break;
    }
    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry, u1UpdateStat) !=
        SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_VLAN | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_PKT_NAME,
                            "Unable to update SNOOP statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleSSMReports                             */
/*                                                                           */
/* Description        : This function is used to handle various IGMP v3 /    */
/*                      MLD v2 messages                                      */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      u1RecordType - Record type                           */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - snoop packet information            */
/*                                                                           */
/* global variables   : gpSSMRepSendBuffer - used to stores the sources      */
/*                                          received in the report message   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleSSMReports (UINT4 u4Instance, UINT1 u1RecordType,
                          tSnoopGroupEntry * pSnoopGroupEntry,
                          UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                          tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopSrcBmp        OldInclBmp;
    tSnoopSrcBmp        OldExclBmp;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT1               u1CurrFilterMode = 0;
    UINT1               u1UpdateStat = 0;
    UINT1               u1UpdateMrp = SNOOP_FALSE;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    tSnoopSrcBmp        TempInclSrcBmp;
    tSnoopSrcBmp        TempHostInclBmp;
    tSnoopSrcBmp        TempXorInclBmp;
    tSnoopSrcBmp        TempInclBmp;
    tSnoopSrcBmp        TempExclBmp;
    INT4                i4ExtraLimit = 0;
    UINT2               u2Index = 0;
    UINT2               u2Byte = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1Value = 0;
    UINT1               u1PortEntryFound = SNOOP_FALSE;
    UINT1               u1HostDeleted = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;

#ifdef ICCH_WANTED

    /* Incase of ICCH enabled we need to update the data structures based
       on received report information from remote node.
     */

    if ((pSnoopPktInfo->u1IcchReportVersion == SNOOP_IGMP_V1REPORT) ||
        (pSnoopPktInfo->u1IcchReportVersion == SNOOP_IGMP_V2REPORT))
    {
        SnoopUpdateGroupEntryInEnhMode (u4Instance, pSnoopGroupEntry,
                                        pSnoopPktInfo, pSnoopPortCfgEntry);
        return SNOOP_SUCCESS;
    }

#endif

    SNOOP_MEM_SET (TempInclSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempHostInclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempXorInclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempExclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempInclBmp, 0, SNOOP_SRC_LIST_SIZE);

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
        {
            MEMCPY (TempInclBmp, pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                    SNOOP_SRC_LIST_SIZE);
            MEMCPY (TempExclBmp, pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp,
                    SNOOP_SRC_LIST_SIZE);
            break;
        }
    }

    u1CurrFilterMode = pSnoopGroupEntry->pConsGroupEntry->u1FilterMode;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                     SNOOP_FAILURE);

    if (SnoopVlanGetVlanEntry (u4Instance, pSnoopGroupEntry->VlanId,
                               pSnoopGroupEntry->GroupIpAddr.u1Afi,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_GRP_NAME,
                            "Vlan entry for vlan %d not found\r\n",
                            SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId));
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_CPY (OldInclBmp, pSnoopGroupEntry->pConsGroupEntry->InclSrcBmp,
                   SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_CPY (OldExclBmp, pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
                   SNOOP_SRC_LIST_SIZE);

    if ((pSnoopPortCfgEntry != NULL) &&
        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL))
    {
        pSnoopSSMPortNode = NULL;
        u1PortEntryFound = SNOOP_TRUE;
        /* If the Limit type set is CHANNEL, find the SSM port entry */
        if (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->u1SnoopPortMaxLimitType
            == SNOOP_PORT_LMT_TYPE_CHANNELS)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortNode, tSnoopPortEntry *)
            {
                /* if the port entry is found, keep a back-up of the include 
                 * source bitmap */
                if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
                {
                    SNOOP_MEM_CPY (TempInclSrcBmp,
                                   pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                                   SNOOP_SRC_LIST_SIZE);

                    break;
                }
            }
        }
    }

    /* Depending upon the record type update group entry */
    switch (u1RecordType)
    {
        case SNOOP_TO_INCLUDE:
        case SNOOP_IS_INCLUDE:
            if (SnoopGrpHandleToInclude (u4Instance, pSnoopGroupEntry,
                                         pSnoopVlanEntry, u2NumSrcs,
                                         pSnoopPktInfo,
                                         pSnoopPortCfgEntry,
                                         &u1UpdateMrp) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Processing of TO_INCLUDE messages"
                               " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = ((u1RecordType == SNOOP_IS_INCLUDE) ?
                                SNOOP_IS_INCLUDE_RCVD : SNOOP_TO_INCLUDE_RCVD);
            }
            break;

        case SNOOP_TO_EXCLUDE:
        case SNOOP_IS_EXCLUDE:
            if (SnoopGrpHandleToExclude (u4Instance, pSnoopGroupEntry,
                                         u2NumSrcs, pSnoopPktInfo,
                                         pSnoopPortCfgEntry,
                                         &u1UpdateMrp) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Processing of TO_EXCLUDE messages"
                               " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = ((u1RecordType == SNOOP_IS_EXCLUDE) ?
                                SNOOP_IS_EXCLUDE_RCVD : SNOOP_TO_EXCLUDE_RCVD);
            }
            break;

        case SNOOP_ALLOW:
            if (SnoopGrpHandleAllow (u4Instance, pSnoopGroupEntry,
                                     u2NumSrcs, pSnoopPktInfo,
                                     pSnoopPortCfgEntry) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME, "Processing of ALLOW messages"
                               " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = SNOOP_ALLOW_RCVD;
            }
            break;

        case SNOOP_BLOCK:
            if (SnoopGrpHandleBlock (u4Instance, pSnoopGroupEntry,
                                     u2NumSrcs, pSnoopPktInfo,
                                     pSnoopPortCfgEntry) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME, "Processing of BLOCK messages"
                               " for group record failed\r\n");
                u1UpdateStat = SNOOP_PKT_ERROR;
            }
            else
            {
                u1UpdateStat = SNOOP_BLOCK_RCVD;
            }
            break;
        default:
            break;
    }

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                     SNOOP_FAILURE);
    /* If the Limit type set is CHANNEL, find the SSM port entry */
    if ((u1PortEntryFound == SNOOP_TRUE) &&
        (pSnoopPortCfgEntry != NULL) &&
        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
        (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
         u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
        (SNOOP_INSTANCE_INFO (u4Instance)->
         SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
         u1FilterStatus != SNOOP_DISABLE))
    {
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopSSMPortNode, tSnoopPortEntry *)
        {
            if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                /* If port entry is found, consolidate the host bitmaps to
                 * find the new port include bitmap */
                SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList, pSnoopHostNode,
                                tSnoopHostEntry *)
                {
                    /* Consolidate the include source bitmap based on all the
                     * hosts on the port */
                    SNOOP_OR_SRC_BMP (TempHostInclBmp,
                                      pSnoopHostNode->pSourceBmp->InclSrcBmp);
                    if (MEMCMP (pSnoopPktInfo->SrcIpAddr.au1Addr,
                                pSnoopHostNode->HostIpAddr.au1Addr,
                                MEM_MAX_BYTES (pSnoopGroupEntry->GroupIpAddr.
                                               u1AddrLen,
                                               IPVX_MAX_INET_ADDR_LEN)) == 0)
                    {
                        pSnoopHostEntry = pSnoopHostNode;
                    }
                }
                break;
            }
        }

        if (pSnoopSSMPortNode != NULL)
        {
            /* At this point, TempInclSrcBmp will have the old bitmap, before
             * processing the report and TempHostInclBmp will have the new 
             * bitmap, after processing the report. Based on the differences 
             * between the bitmaps, we can know how many channels have been 
             * added/removed */
            MEMCPY (TempXorInclBmp, TempHostInclBmp, SNOOP_SRC_LIST_SIZE);
            SNOOP_XOR_SRC_BMP (TempXorInclBmp, TempInclSrcBmp);

            /* Find (OLD - NEW) to get the number of channels deleted */
            SNOOP_AND_SRC_BMP (TempInclSrcBmp, TempXorInclBmp);
            /* Find (NEW - OLD) to get the number of channels added */
            SNOOP_AND_SRC_BMP (TempHostInclBmp, TempXorInclBmp);
            for (u2Index = 1; u2Index <= SNOOP_MAX_MCAST_SRCS; u2Index++)
            {
                OSIX_BITLIST_IS_BIT_SET (TempInclSrcBmp, u2Index,
                                         SNOOP_SRC_LIST_SIZE, bResult);
                if (bResult == OSIX_TRUE)
                {
                    SNOOP_DECR_MEMBER_CNT (pSnoopPortCfgEntry->
                                           pSnoopExtPortCfgEntry);
                }
                OSIX_BITLIST_IS_BIT_SET (TempHostInclBmp, u2Index,
                                         SNOOP_SRC_LIST_SIZE, bResult);
                if (bResult == OSIX_TRUE)
                {
                    SNOOP_INCR_MEMBER_CNT (pSnoopPortCfgEntry->
                                           pSnoopExtPortCfgEntry);
                }
            }
        }
        /* Now if the number of current joins get greater than the maximum
         * limit allowed for the port, we have to reset that much bits from
         * the host entry */
        if ((pSnoopSSMPortNode != NULL) && (pSnoopHostEntry != NULL))
        {
            i4ExtraLimit =
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                 u4SnoopPortCfgMemCnt) -
                (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->
                 u4SnoopPortMaxLimit);

            if (i4ExtraLimit > 0)
            {
                for (u2Byte = 0; u2Byte < SNOOP_SRC_LIST_SIZE; u2Byte++)
                {
                    u1Value = TempHostInclBmp[u2Byte];
                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE) &&
                                          (u1Value != 0)); u2BitIndex++)
                    {
                        if ((u1Value & SNOOP_BIT8) != 0)
                        {
                            u2Index = (UINT2) ((u2Byte * SNOOP_PORTS_PER_BYTE) +
                                               u2BitIndex + 1);
                            OSIX_BITLIST_RESET_BIT (pSnoopHostEntry->
                                                    pSourceBmp->InclSrcBmp,
                                                    u2Index,
                                                    SNOOP_SRC_LIST_SIZE);
                            /* As of now, the group entry is not updated. So,
                             * if the source is not in the group's include 
                             * bitmap, we can safely decrement the group 
                             * reference count */
                            OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                     InclSrcBmp, u2Index,
                                                     SNOOP_SRC_LIST_SIZE,
                                                     bResult);
                            if (bResult == OSIX_FALSE)
                            {
                                SNOOP_SRC_INFO (u4Instance,
                                                u2Index - 1).u2GrpRefCount--;
                                if (SNOOP_SRC_INFO (u4Instance,
                                                    (u2Index - 1)).u2GrpRefCount
                                    == 0)
                                {
                                    /* Remove the source from the linked list */
                                    SNOOP_REMOVE_SOURCE (u4Instance,
                                                         SNOOP_SRC_INFO
                                                         (u4Instance,
                                                          (u2Index - 1)));
                                    SNOOP_MEM_SET (&
                                                   (SNOOP_SRC_INFO
                                                    (u4Instance,
                                                     (u2Index - 1)).SrcIpAddr),
                                                   0, sizeof (tIPvXAddr));
                                    gu2SourceCount--;
                                }
                            }
                            i4ExtraLimit--;
                            SNOOP_DECR_MEMBER_CNT (pSnoopPortCfgEntry->
                                                   pSnoopExtPortCfgEntry);
                            if (i4ExtraLimit == 0)
                            {
                                /* if the host entry include bitmap becomes null, 
                                 * remove the host entry. If the host entry is the 
                                 * last host entry, remove the port entry. */
                                if (SNOOP_MEM_CMP
                                    (pSnoopHostEntry->pSourceBmp->InclSrcBmp,
                                     gNullSrcBmp, SNOOP_SRC_LIST_SIZE) == 0)
                                {
                                    SNOOP_SLL_DELETE (&pSnoopSSMPortNode->
                                                      HostList,
                                                      pSnoopHostEntry);
                                    RBTreeRemove
                                        (SNOOP_INSTANCE_INFO (u4Instance)->
                                         HostEntry,
                                         (tRBElem *) pSnoopHostEntry);
                                    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK
                                        (u4Instance, pSnoopHostEntry);
                                    pSnoopHostEntry = NULL;

                                    if (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->
                                                         HostList) == 0)
                                    {
                                        if (pSnoopSSMPortNode->
                                            HostPresentTimer.u1TimerType
                                            != SNOOP_INVALID_TIMER_TYPE)
                                        {
                                            SnoopTmrStopTimer
                                                (&pSnoopSSMPortNode->
                                                 HostPresentTimer);
                                        }
                                        /* Update the Group entry port bitmap */
                                        SNOOP_DEL_FROM_PORT_LIST
                                            (pSnoopSSMPortNode->u4Port,
                                             pSnoopGroupEntry->PortBitmap);

                                        SNOOP_SLL_DELETE (&pSnoopGroupEntry->
                                                          SSMPortList,
                                                          pSnoopSSMPortNode);
                                        SNOOP_SSM_PORT_FREE_MEMBLK (u4InstId,
                                                                    pSnoopSSMPortNode);
                                    }
                                    u1HostDeleted = SNOOP_TRUE;

                                }
                                break;
                            }
                        }
                        u1Value = (UINT1) (u1Value << 1);
                    }
                    if (u1HostDeleted == SNOOP_TRUE)
                    {
                        break;
                    }
                }
            }
        }
    }

    /*
     * Update the Port Incl-Bitmap. Check it against the back-up.
     * For the bits that are re-set in the New bitmap, decrement the threshold.
     * For the bits that are newly-set in the New bitmap, increment the threshold.
     * If the current threshold is greater than the MAX, find the host entry and
     * reset bits which are newly set. */

    if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                              u1UpdateStat) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_PKT_NAME,
                            "Unable to update SNOOP statistics for VLAN ID %d\r\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
    }

    if (u1UpdateStat == SNOOP_PKT_ERROR)
    {
        return SNOOP_FAILURE;
    }

    /* The Consolidation of source info was already taken care while handling 
     * INCLUDE_NONE. So no need to do the same for INCLUDE_NONE */
    if (!(((u1RecordType == SNOOP_TO_INCLUDE) ||
           (u1RecordType == SNOOP_IS_INCLUDE)) && (u2NumSrcs == 0)))
    {
        /* Conslodation of include and exclude sources for the group */
        SnoopGrpConsolidateSrcInfo (u4Instance, u1RecordType, u1CurrFilterMode,
                                    OldInclBmp, OldExclBmp, pSnoopGroupEntry,
                                    SNOOP_TRUE);

        /* Summarize the forwading table with latest group and source
         * consolidated info */

        /*If there is any change in the EXCLUDE and INCLUDE port bit map
           MRP should be updated */
        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopSSMPortNode, tSnoopPortEntry *)
        {
            if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
            {
                if ((MEMCMP (TempInclBmp,
                             pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                             SNOOP_SRC_LIST_SIZE) != 0) ||
                    (MEMCMP (TempExclBmp,
                             pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp,
                             SNOOP_SRC_LIST_SIZE) != 0))
                {
                    u1UpdateMrp = SNOOP_TRUE;
                }
                break;
            }
        }

        SnoopFwdSummarizeIpFwdTable (u4Instance, pSnoopGroupEntry, u1UpdateMrp);

        /* Update Active/Successful joins received count */
        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_ACTIVE_JOINS) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_PKT_NAME,
                                "Unable to update SNOOP statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }

    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleToInclude                              */
/*                                                                           */
/* Description        : This function will handle IGMPv3 / MLDv2             */
/*                      TO_INCLUDE message                                   */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - snooped packet information          */
/*                                                                           */
/* global variables   : gu4SendRecType - Send Record Type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleToInclude (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                         tSnoopVlanEntry * pSnoopVlanEntry,
                         UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                         tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                         UINT1 *pu1UpdateMrp)
{
    tIPvXAddr           SrcAddr;
#ifdef IGS_WANTED
    UINT4               u4SrcAddr = 0;
#endif
    UINT2               u2SrcIndex = 0;
    UINT2               u2Count = 0;
    INT1                i1Offset = 0;
    BOOL1               bNewSource = SNOOP_FALSE;
    UINT1               u1ResetHost = SNOOP_FALSE;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1Incr = SNOOP_FALSE;
    UINT1               u1NewGrp = SNOOP_FALSE;
    UINT1               u1NewPort = SNOOP_TRUE;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;

    tSnoopPortBmp       FwdPortBitmap;
    tSnoopPortBmp       RtrBitMap;
    SNOOP_MEM_SET (&FwdPortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&RtrBitMap, 0, sizeof (tSnoopPortBmp));
    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN
                                             (pSnoopGroupEntry->VlanId),
                                             pSnoopVlanEntry->u1AddressType,
                                             0, RtrBitMap, pSnoopVlanEntry);

    SNOOP_MEM_CPY (&FwdPortBitmap, pSnoopGroupEntry->PortBitmap,
                   sizeof (tSnoopPortBmp));
    /* Adding Mrouter port in forward port bit map */
    SNOOP_UPDATE_PORT_LIST (RtrBitMap, FwdPortBitmap);

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (u2NumSrcs == 0)
    {
        if (SnoopGrpHandleToIncludeNone (u4Instance, pSnoopGroupEntry,
                                         pSnoopVlanEntry, pSnoopPktInfo,
                                         pSnoopPortCfgEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Number of sources reached maximum supported\r\n");
            return SNOOP_FAILURE;
        }
#ifdef L2RED_WANTED
        SnoopActiveRedGrpSync (u4Instance,
                               SrcAddr, pSnoopGroupEntry, FwdPortBitmap);
#endif
        /* Increment the Leave received counter
           if INCLUDE_NONE record type is received */

        if (SnoopVlanUpdateStats (u4Instance, pSnoopVlanEntry,
                                  SNOOP_LEAVE_RCVD) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_VLAN_NAME, "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        }

        return SNOOP_SUCCESS;
    }

    for (u2Count = 0; u2Count < u2NumSrcs; u2Count++)
    {
        /* Check if the number of sources reached the maximum supported by
         * the system */
        if (gu2SourceCount == SNOOP_MAX_MCAST_SRCS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "Number of sources reached maximum supported\r\n");
            return SNOOP_SUCCESS;
        }
        else
        {
#ifdef IGS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_MEM_CPY (&u4SrcAddr, gpSSMSrcInfoBuffer + i1Offset,
                               SNOOP_IP_ADDR_SIZE);
                u4SrcAddr = SNOOP_NTOHL (u4SrcAddr);

                /*  If the source address is nullified, then it has
                 *  been filtered
                 */
                if (u4SrcAddr == 0)
                {
                    i1Offset += SNOOP_IP_ADDR_SIZE;
                    continue;
                }

                IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                                     (UINT1 *) &u4SrcAddr);
                SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr, u2SrcIndex,
                                          bNewSource);
                i1Offset += SNOOP_IP_ADDR_SIZE;
            }

#endif
#ifdef MLDS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1SrcAddr, gpSSMSrcInfoBuffer + i1Offset,
                               IPVX_IPV6_ADDR_LEN);
                SNOOP_INET_NTOHL (au1SrcAddr);

                /*  If the source address is nullified, then it has
                 *  been filtered
                 */
                if (SNOOP_MEM_CMP (au1SrcAddr, gNullSrcAddr, IPVX_IPV6_ADDR_LEN)
                    == 0)
                {
                    i1Offset = (INT1) (i1Offset + IPVX_IPV6_ADDR_LEN);
                    continue;
                }

                IPVX_ADDR_INIT_IPV6 (SrcAddr, SNOOP_ADDR_TYPE_IPV6, au1SrcAddr);
                SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr, u2SrcIndex,
                                          bNewSource);
                i1Offset += IPVX_IPV6_ADDR_LEN;
            }
#endif

            /* Check if the SNOOP group entry exist if not present
             * add the group and forwarding table */
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;

                /* Update the include and exclude bitmap for the port and 
                 * host entry present in the group record */
                if (SnoopGrpUpdateSSMPortToGroupEntry
                    (u4Instance, pSnoopGroupEntry, u2SrcIndex, pSnoopPktInfo,
                     pSnoopPortCfgEntry,
                     SNOOP_FALSE, SNOOP_TO_INCLUDE) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Unable to update the port "
                                   "entry for the group\r\n");
                    return SNOOP_FAILURE;
                }

                if (SNOOP_DLL_COUNT (&(pSnoopGroupEntry->
                                       pConsGroupEntry->GroupEntryList)) == 1)
                {
                    /* This is the first group entry for the consolidated 
                     * group entry, so this is directly set to SNOOP_TO_INCLUDE 
                     * which will reduce the operations to be done in 
                     * SnoopUtilUpdateRecordType */
                    gu4SendRecType = SNOOP_TO_INCLUDE;
                }

                /* If the entry is created first then directly increment the 
                 * group reference count */
                SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;

                u1NewGrp = SNOOP_TRUE;
                u1Incr = SNOOP_TRUE;
            }
            else
            {
                /* To check whether we need to overide the previous 
                 * registration send by the host */
                if (u2Count == 0)
                {
                    u1ResetHost = SNOOP_TRUE;
                }

                /* If the source is newly added then directly increment the 
                 * group reference  count  */
                if (bNewSource == SNOOP_TRUE)
                {
                    SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;
                    u1Incr = SNOOP_TRUE;
                }
                else
                {
                    if (u1NewGrp == SNOOP_TRUE)
                    {
                        SNOOP_SRC_INFO (u4Instance,
                                        (u2SrcIndex)).u2GrpRefCount++;
                        u1Incr = SNOOP_TRUE;
                    }
                    else
                    {
                        /* Source is not newly added so check if any registration 
                         * for group source combination has received on any of 
                         * the ports */
                        SnoopUtilUpdateRefCount (u4Instance, u2SrcIndex,
                                                 pSnoopGroupEntry, &u1Incr);
                    }
                }
                SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                                pSnoopSSMPortNode, tSnoopPortEntry *)
                {
                    /* if same (Group,Source) report received from different
                       port MRP should be updated */
                    if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
                    {
                        u1NewPort = SNOOP_FALSE;
                        break;
                    }
                }
                /* If the port record is not present create the port, host
                 * record. 
                 * If the port record is present, if host record is not 
                 * present create the Host record.
                 * Update the include source bitmap of Host with source.
                 */
                if (SnoopGrpUpdateSSMPortToGroupEntry (u4Instance,
                                                       pSnoopGroupEntry,
                                                       u2SrcIndex,
                                                       pSnoopPktInfo,
                                                       pSnoopPortCfgEntry,
                                                       u1ResetHost,
                                                       SNOOP_TO_INCLUDE)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Unable to update the port "
                                   "entry for the group\r\n");
                    if (u1Incr == SNOOP_TRUE)
                    {
                        SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount--;
                    }
                    return SNOOP_FAILURE;
                }
                u1ResetHost = SNOOP_FALSE;
            }

            /* for new source update the global source count */
            if (bNewSource == SNOOP_TRUE)
            {
                gu2SourceCount++;
            }
        }
    }
    if ((u1Incr == SNOOP_TRUE) || (u1NewPort == SNOOP_TRUE))
    {
        *pu1UpdateMrp = SNOOP_TRUE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleToExclude                              */
/*                                                                           */
/* Description        : This function will handle IGMPv3 / MLDv2 TO_EXCLUDE  */
/*                      message                                              */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - IGMP packet information             */
/*                                                                           */
/* global variables   : gu4SendRecType - Send Record Type                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :SNOOP_SUCCESS/ SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopGrpHandleToExclude (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                         UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                         tSnoopPortCfgEntry * pSnoopPortCfgEntry,
                         UINT1 *pu1UpdateMrp)
{
    tIPvXAddr           SrcAddr;
#ifdef IGS_WANTED
    UINT4               u4SrcAddr = 0;
#endif
    UINT2               u2SrcIndex = 0;
    UINT2               u2Count = 0;
    INT1                i1Offset = 0;
    UINT1               u1ResetHost = SNOOP_FALSE;
    UINT1               u1Incr = SNOOP_FALSE;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1NewGrp = SNOOP_FALSE;
    UINT1               u1NewPort = SNOOP_TRUE;
    BOOL1               bNewSource = SNOOP_FALSE;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (u2NumSrcs == 0)
    {
        if (SnoopGrpHandleToExcludeNone (u4Instance, pSnoopGroupEntry,
                                         pSnoopPktInfo,
                                         pSnoopPortCfgEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Number of sources reached maximum supported\r\n");
            return SNOOP_FAILURE;
        }
        return SNOOP_SUCCESS;
    }

    for (u2Count = 0; u2Count < u2NumSrcs; u2Count++)
    {
        /* Check if the number of sources reached the maximum supported by
         * the system */
        if (gu2SourceCount == SNOOP_MAX_MCAST_SRCS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "Number of sources reached maximum supported\r\n");
            return SNOOP_SUCCESS;
        }
        else
        {
#ifdef IGS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_MEM_CPY (&u4SrcAddr, gpSSMSrcInfoBuffer + i1Offset,
                               SNOOP_IP_ADDR_SIZE);
                u4SrcAddr = SNOOP_NTOHL (u4SrcAddr);

                /*  If the source address is nullified, then it has
                 *  been filtered
                 */
                if (u4SrcAddr == 0)
                {
                    i1Offset += SNOOP_IP_ADDR_SIZE;
                    continue;
                }

                IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                                     (UINT1 *) &u4SrcAddr);
                SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr, u2SrcIndex,
                                          bNewSource);
                i1Offset += SNOOP_IP_ADDR_SIZE;
            }
#endif

#ifdef MLDS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1SrcAddr, gpSSMSrcInfoBuffer + i1Offset,
                               IPVX_IPV6_ADDR_LEN);
                SNOOP_INET_NTOHL (au1SrcAddr);
                IPVX_ADDR_INIT_IPV6 (SrcAddr, SNOOP_ADDR_TYPE_IPV6, au1SrcAddr);
                SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr, u2SrcIndex,
                                          bNewSource);
                i1Offset += IPVX_IPV6_ADDR_LEN;
            }
#endif

            /* check if the SNOOP group entry exist if not present
             * add the group and forwarding table */
            if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

                /* Update the include and exclude bitmap for the port and 
                 * host entry present in the group record */
                if (SnoopGrpUpdateSSMPortToGroupEntry (u4Instance,
                                                       pSnoopGroupEntry,
                                                       u2SrcIndex,
                                                       pSnoopPktInfo,
                                                       pSnoopPortCfgEntry,
                                                       SNOOP_FALSE,
                                                       SNOOP_TO_EXCLUDE)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Unable to update the port "
                                   "entry for the group\r\n");
                    return SNOOP_FAILURE;
                }

                if (SNOOP_DLL_COUNT (&(pSnoopGroupEntry->
                                       pConsGroupEntry->GroupEntryList)) == 1)
                {
                    /* This is the first group entry for the consolidated 
                     * group entry, so this is directly set to SNOOP_TO_EXCLUDE 
                     * which will reduce the operations to be done in 
                     * SnoopUtilUpdateRecordType */
                    gu4SendRecType = SNOOP_TO_EXCLUDE;
                }

                /* If the entry is created first then directly increment the 
                 * group reference count */
                SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;

                u1NewGrp = SNOOP_TRUE;
                u1Incr = SNOOP_TRUE;
            }
            else
            {
                /* Change the filter  mode to exclude */
                pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;

                /* If the source is newly added then directly increment the 
                 * group reference count and source count */
                if (bNewSource == SNOOP_TRUE)
                {
                    SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;
                    u1Incr = SNOOP_TRUE;
                }
                else
                {
                    if (u1NewGrp == SNOOP_TRUE)
                    {
                        SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;
                        u1Incr = SNOOP_TRUE;
                    }
                    else
                    {
                        /* Source is not newly added so check if any registration 
                         * for group source combination has received on any of 
                         * the ports */
                        SnoopUtilUpdateRefCount (u4Instance, u2SrcIndex,
                                                 pSnoopGroupEntry, &u1Incr);
                    }
                }
                SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                                pSnoopSSMPortNode, tSnoopPortEntry *)
                {
                    /* if same (Group,Source) report received from different
                       port MRP should be updated */
                    if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
                    {
                        u1NewPort = SNOOP_FALSE;
                        break;
                    }
                }

                /* To check whether we need to overide the previous 
                 * registration send by the host */
                if (u2Count == 0)
                {
                    u1ResetHost = SNOOP_TRUE;
                }

                /* If the port record is not present create the port, host
                 * record. 
                 * If the port record is present, if host record is not 
                 * present create the Host record.
                 * Update the exclude source bitmap of Host with source.
                 */
                if (SnoopGrpUpdateSSMPortToGroupEntry
                    (u4Instance, pSnoopGroupEntry, u2SrcIndex, pSnoopPktInfo,
                     pSnoopPortCfgEntry,
                     u1ResetHost, SNOOP_TO_EXCLUDE) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Unable to update the port "
                                   "entry for the group\r\n");

                    if (u1Incr == SNOOP_TRUE)
                    {
                        SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount--;
                    }
                    return SNOOP_FAILURE;
                }

                u1ResetHost = SNOOP_FALSE;
            }

            if (bNewSource == SNOOP_TRUE)
            {
                gu2SourceCount++;
            }
        }
    }
    if ((u1Incr == SNOOP_TRUE) || (u1NewPort == SNOOP_TRUE))
    {
        *pu1UpdateMrp = SNOOP_TRUE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleAllow                                  */
/*                                                                           */
/* Description        : This function will handle IGMPv3/MLDv2 ALLOW message */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - Pointer to SNOOP packet information */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleAllow (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                     UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                     tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tIPvXAddr           SrcAddr;
    UINT2               u2TempIndex = 0;
    UINT2               u2SrcIndex = 0;
    UINT2               u2Count = 0;
    INT1                i1Offset = 0;
    UINT1               u1Incr = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bNewSource = SNOOP_FALSE;
    BOOL1               bSrcPresent = SNOOP_FALSE;
    UINT1               au1NullIpAddr[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (au1NullIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    for (u2Count = 0; u2Count < u2NumSrcs; u2Count++)
    {
        /* Check if the number of sources reached the maximum supported by
         * the system */
        if (gu2SourceCount == SNOOP_MAX_MCAST_SRCS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "Number of sources reached maximum supported\r\n");
            return SNOOP_SUCCESS;
        }
        else
        {
            /* Update the port and host record only if it has send any previous 
             * registration */
            if (SnoopUtilGetHostEntry (pSnoopGroupEntry, pSnoopPktInfo,
                                       &pSnoopHostEntry) != SNOOP_SUCCESS)
            {
                return SNOOP_FAILURE;
            }

#ifdef IGS_WANTED
            /*  If the source address is nullified, then it has
             *  been filtered
             */
            if (SNOOP_MEM_CMP (gpSSMSrcInfoBuffer + i1Offset,
                               au1NullIpAddr, SNOOP_IP_ADDR_SIZE) == 0)
            {
                i1Offset += SNOOP_IP_ADDR_SIZE;
                continue;
            }

            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                                     (UINT1 *) (gpSSMSrcInfoBuffer + i1Offset));
            }
#endif
#ifdef MLDS_WANTED

            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                IPVX_ADDR_INIT_IPV6 (SrcAddr, SNOOP_ADDR_TYPE_IPV6,
                                     (UINT1 *) (gpSSMSrcInfoBuffer + i1Offset));
            }
#endif
            SNOOP_INET_NTOHL (SrcAddr.au1Addr);

            SNOOP_IS_SOURCE_PRESENT (u4Instance, SrcAddr, bSrcPresent,
                                     u2TempIndex);

            if (bSrcPresent == SNOOP_FALSE)
            {
                if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                {
                    SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr,
                                              u2SrcIndex, bNewSource);
                }
                else
                {
                    if (SNOOP_MEM_CMP
                        (pSnoopHostEntry->pSourceBmp->InclSrcBmp, gNullSrcBmp,
                         SNOOP_SRC_LIST_SIZE) == 0)
                    {
#ifdef IGS_WANTED
                        if (pSnoopPktInfo->DestIpAddr.u1Afi
                            == SNOOP_ADDR_TYPE_IPV4)
                        {
                            i1Offset += SNOOP_IP_ADDR_SIZE;
                        }
#endif

#ifdef MLDS_WANTED
                        if (pSnoopPktInfo->DestIpAddr.u1Afi
                            == SNOOP_ADDR_TYPE_IPV6)
                        {
                            i1Offset += IPVX_IPV6_ADDR_LEN;
                        }
#endif
                        continue;
                    }
                    else
                    {
                        SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr,
                                                  u2SrcIndex, bNewSource);
                    }
                }
            }
            else
            {
                if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                {
                    SNOOP_IS_GRP_SRC_PRESENT (u2TempIndex,
                                              pSnoopHostEntry->pSourceBmp->
                                              InclSrcBmp, bResult);
                    if (bResult == OSIX_TRUE)
                    {
#ifdef IGS_WANTED
                        if (pSnoopPktInfo->DestIpAddr.u1Afi
                            == SNOOP_ADDR_TYPE_IPV4)
                        {
                            i1Offset += SNOOP_IP_ADDR_SIZE;
                        }
#endif
#ifdef MLDS_WANTED
                        if (pSnoopPktInfo->DestIpAddr.u1Afi
                            == SNOOP_ADDR_TYPE_IPV6)
                        {
                            i1Offset += IPVX_IPV6_ADDR_LEN;
                        }
#endif
                        continue;
                    }
                }
            }

#ifdef IGS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                i1Offset += SNOOP_IP_ADDR_SIZE;
            }
#endif

#ifdef MLDS_WANTED
            if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                i1Offset += IPVX_IPV6_ADDR_LEN;
            }
#endif

            /* If the source is newly added then directly increment the 
             * group reference count */
            if (bNewSource == SNOOP_TRUE)
            {
                SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;
                u1Incr = SNOOP_TRUE;
            }
            else
            {
                /* Source is not newly added so check if any registration for
                 * group source combination has received on any of the ports */
                u2SrcIndex = u2TempIndex;
                SnoopUtilUpdateRefCount (u4Instance, u2SrcIndex,
                                         pSnoopGroupEntry, &u1Incr);
            }

            /* Update the host records source bitmaps */
            if (SnoopGrpUpdateSSMPortToGroupEntry (u4Instance,
                                                   pSnoopGroupEntry,
                                                   u2SrcIndex, pSnoopPktInfo,
                                                   pSnoopPortCfgEntry,
                                                   SNOOP_FALSE,
                                                   SNOOP_ALLOW)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Unable to update the port entry for the group\r\n");
                if ((u1Incr == SNOOP_TRUE)
                    && (u2SrcIndex < SNOOP_MAX_MCAST_SRCS))
                {
                    SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount--;
                }
                return SNOOP_FAILURE;
            }

            if (bNewSource == SNOOP_TRUE)
            {
                gu2SourceCount++;
            }
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleBlock                                  */
/*                                                                           */
/* Description        : This function will handle IGMPv3/MLDv2 BLOCK message */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2NumSrcs  - Number of sources                       */
/*                      pSnoopPktInfo  - Pointer to SNOOP packet information */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleBlock (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                     UINT2 u2NumSrcs, tSnoopPktInfo * pSnoopPktInfo,
                     tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tIPvXAddr           SrcAddr;
    tSnoopSrcBmp        SnoopExclBmp;
#ifdef IGS_WANTED
    UINT4               u4SrcAddr = 0;
#endif
    UINT2               u2SrcIndex = 0;
    UINT2               u2TempIndex = 0;
    UINT2               u2Count = 0;
    INT4                i4Offset = 0;
    UINT1               u1Incr = SNOOP_FALSE;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bNewSource = SNOOP_FALSE;
    BOOL1               bSrcPresent = SNOOP_FALSE;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    for (u2Count = 0; u2Count < u2NumSrcs; u2Count++)
    {
        /* Update the port and host record only if it has send any previous 
         * registration */
        if (SnoopUtilGetHostEntry (pSnoopGroupEntry, pSnoopPktInfo,
                                   &pSnoopHostEntry) != SNOOP_SUCCESS)
        {
            return SNOOP_FAILURE;
        }

#ifdef IGS_WANTED
        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
        {
            SNOOP_MEM_CPY (&u4SrcAddr, gpSSMSrcInfoBuffer + i4Offset,
                           SNOOP_IP_ADDR_SIZE);
            u4SrcAddr = SNOOP_NTOHL (u4SrcAddr);

            /*  If the source address is nullified, then it has
             *  been filtered
             */
            if (u4SrcAddr == 0)
            {
                i4Offset += SNOOP_IP_ADDR_SIZE;
                continue;
            }

            IPVX_ADDR_INIT_IPV4 (SrcAddr, SNOOP_ADDR_TYPE_IPV4,
                                 (UINT1 *) &u4SrcAddr);
            SNOOP_IS_SOURCE_PRESENT (u4Instance, SrcAddr, bSrcPresent,
                                     u2TempIndex);
        }

#endif

#ifdef MLDS_WANTED
        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
        {
            SNOOP_MEM_CPY (au1SrcAddr, gpSSMSrcInfoBuffer + i4Offset,
                           IPVX_IPV6_ADDR_LEN);
            SNOOP_INET_NTOHL (au1SrcAddr);
            IPVX_ADDR_INIT_IPV6 (SrcAddr, SNOOP_ADDR_TYPE_IPV6, au1SrcAddr);
            SNOOP_IS_SOURCE_PRESENT (u4Instance, SrcAddr, bSrcPresent,
                                     u2TempIndex);
        }
#endif

        if (bSrcPresent == SNOOP_FALSE)
        {
            if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
#ifdef IGS_WANTED
                if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
                {
                    i4Offset += SNOOP_IP_ADDR_SIZE;
                }
#endif
#ifdef MLDS_WANTED
                if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Offset += IPVX_IPV6_ADDR_LEN;
                }
#endif
                continue;
            }
            else
            {
                /* The host must have sent an exclude before in order to process
                 * the blcok */
                if (SNOOP_MEM_CMP
                    (pSnoopHostEntry->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                     SNOOP_SRC_LIST_SIZE) != 0)
                {
                    SNOOP_ADD_TO_SOURCE_LIST (u4Instance, SrcAddr, u2SrcIndex,
                                              bNewSource);
                }
            }
        }
        else
        {
            /* The group filter mode can be exclude due to the presence
             * of the other hosts sending exclude reports.
             * Check if source is set in the ExclSrcBmp only if the 
             * host node has sent a previous exclude report */
            if (SNOOP_MEM_CMP
                (pSnoopHostEntry->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                 SNOOP_SRC_LIST_SIZE) != 0)
            {
                SNOOP_IS_GRP_SRC_PRESENT (u2TempIndex,
                                          pSnoopHostEntry->pSourceBmp->
                                          ExclSrcBmp, bResult);
                if (bResult == OSIX_TRUE)
                {
#ifdef IGS_WANTED
                    if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
                    {
                        i4Offset += SNOOP_IP_ADDR_SIZE;
                    }
#endif
#ifdef MLDS_WANTED
                    if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
                    {
                        i4Offset += IPVX_IPV6_ADDR_LEN;
                    }
#endif
                    continue;
                }
            }
        }

#ifdef IGS_WANTED
        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
        {
            i4Offset += SNOOP_IP_ADDR_SIZE;
        }
#endif
#ifdef MLDS_WANTED
        if (pSnoopPktInfo->DestIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
        {
            i4Offset += IPVX_IPV6_ADDR_LEN;
        }
#endif
        /* If the source is newly added then directly increment the group 
         * reference count */
        if (bNewSource == SNOOP_TRUE)
        {
            SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount++;
            u1Incr = SNOOP_TRUE;
        }
        else
        {
            /* Source is not newly added so check if any registration for
             * group source combination has received on any of the ports */
            u2SrcIndex = u2TempIndex;
            SnoopUtilUpdateRefCount (u4Instance, u2SrcIndex,
                                     pSnoopGroupEntry, &u1Incr);
        }

        /* Update the host records include bitmap */
        if (SnoopGrpUpdateSSMPortToGroupEntry (u4Instance,
                                               pSnoopGroupEntry,
                                               u2SrcIndex, pSnoopPktInfo,
                                               pSnoopPortCfgEntry,
                                               SNOOP_FALSE,
                                               SNOOP_BLOCK) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Unable to update the port entry for the group\r\n");
            if ((u1Incr == SNOOP_TRUE) && (u2SrcIndex < SNOOP_MAX_MCAST_SRCS))
            {
                SNOOP_SRC_INFO (u4Instance, u2SrcIndex).u2GrpRefCount--;
            }
            return SNOOP_FAILURE;
        }

        if (bNewSource == SNOOP_TRUE)
        {
            gu2SourceCount++;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpUpdateSSMPortToGroupEntry                    */
/*                                                                           */
/* Description        : This function adds the port node to group entry and  */
/*                      also updates the host node, Include and Exclude      */
/*                      bitmaps in both host and port entry                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2SrcIndex  - Source Index                           */
/*                      pSnoopPktInfo   - Pointer to packet information      */
/*                      u1ResetHost -   Indicate whether the incoming packet */
/*                                      from same host or different host.    */
/*                      u1RecordType  - Record type                          */
/*                                                                           */
/* Output(s)          : pointer to the group entry or NULL                   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpUpdateSSMPortToGroupEntry (UINT4 u4Instance,
                                   tSnoopGroupEntry * pSnoopGroupEntry,
                                   UINT2 u2SrcIndex,
                                   tSnoopPktInfo * pSnoopPktInfo,
                                   tSnoopPortCfgEntry *
                                   pSnoopPortCfgEntry, UINT1 u1ResetHost,
                                   UINT1 u1RecordType)
{
    tSnoopPortEntry    *pSnoopASMPortNode = NULL;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    tRBElem            *pRBElem = NULL;
    tSnoopTag           VlanId;
    UINT4               u4Status = RB_FAILURE;
    UINT2               u2V3HostPresentInterval = 0;
    UINT1               u1EntryFound = SNOOP_FALSE;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1HostFound = SNOOP_FALSE;
    UINT1               u1AddressType = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* check if the V2 receivers are present in the port */

    SNOOP_VALIDATE_ADDRESS_TYPE_RET (pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                     SNOOP_FAILURE);

    SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                           pSnoopGroupEntry->ASMPortBitmap, bResult);
    u1ASMHostPresent = (bResult == OSIX_TRUE) ? SNOOP_TRUE : SNOOP_FALSE;

    /* Check if the port is present in the SSM port record */
    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
        {
            u1EntryFound = SNOOP_TRUE;
            break;
        }
    }

    if (u1EntryFound == SNOOP_TRUE)
    {
        /* Update the host information */
        SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList,
                        pSnoopHostNode, tSnoopHostEntry *)
        {
            if (IPVX_ADDR_COMPARE (pSnoopHostNode->HostIpAddr,
                                   pSnoopPktInfo->SrcIpAddr) == 0)
            {
                u1HostFound = SNOOP_TRUE;
                break;
            }
        }

        if (u1HostFound == SNOOP_FALSE)
        {
            /* Add the host information */
            if (SNOOP_SSM_HOST_ENTRY_ALLOC_MEMBLK (u4Instance,
                                                   pSnoopHostNode) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory alloc for Host entry for group failed\n");
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                              "Memory alloc for Host entry for group failed\n"));
                return SNOOP_FAILURE;
            }

            SNOOP_MEM_SET (pSnoopHostNode, 0, sizeof (tSnoopHostEntry));

            if (SNOOP_SRC_BMP_ALLOC_MEMBLK (u4Instance,
                                            pSnoopHostNode->pSourceBmp) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory allocation for SSM host source bitmap failed\n");
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                              "Memory allocation for SSM host source bitmap failed\n"));
                SNOOP_MEM_SET (pSnoopHostNode, 0, sizeof (tSnoopHostEntry));
                MemReleaseMemBlock (SNOOP_HOST_ENTRY_POOL_ID (u4Instance),
                                    (UINT1 *) pSnoopHostNode);
                return SNOOP_FAILURE;
            }

            SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp, 0,
                           sizeof (tSnoopSrcBmpNode));

            /* Reset the Include(0) and exclude(0xff) bitmap for the port
             * entries */

            SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp, 0,
                           SNOOP_SRC_LIST_SIZE);
            SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp, 0xff,
                           SNOOP_SRC_LIST_SIZE);

            /* update Include or Exclude bitmap for the host */
            if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                (u1RecordType == SNOOP_IS_INCLUDE))
            {
                SNOOP_ADD_TO_SRC_BMP_INCLUDE (u2SrcIndex,
                                              pSnoopHostNode->pSourceBmp->
                                              InclSrcBmp);
            }
            else if ((u1RecordType == SNOOP_TO_EXCLUDE) ||
                     (u1RecordType == SNOOP_IS_EXCLUDE))
            {
                SNOOP_ADD_TO_SRC_BMP_EXCLUDE (u2SrcIndex,
                                              pSnoopHostNode->pSourceBmp->
                                              ExclSrcBmp);
            }
            else if (u1RecordType == SNOOP_TO_EXCLUDE_NONE)
            {

                SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp,
                               0, SNOOP_SRC_LIST_SIZE);
            }

            IPVX_ADDR_COPY (&pSnoopHostNode->HostIpAddr,
                            &pSnoopPktInfo->SrcIpAddr);
            pSnoopHostNode->u1HostTimerStatus = SNOOP_FIRST_HALF;
            pSnoopHostNode->SnoopHostType = SNOOP_SSM_HOST;

            /* Assign the back pointer to the SSM port entry */
            pSnoopHostNode->pPortEntry = pSnoopSSMPortNode;

            SNOOP_SLL_ADD (&pSnoopSSMPortNode->HostList,
                           &pSnoopHostNode->NextHostEntry);

            pRBElem = (tRBElem *) pSnoopHostNode;
            /* Add the host entry to Host information RBTree */
            u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  pRBElem);
            if (u4Status == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for SSM Host Entry \r\n");
                SNOOP_SLL_DELETE (&pSnoopSSMPortNode->HostList, pSnoopHostNode);
                SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);
                return SNOOP_FAILURE;
            }
        }
        else
        {
            pSnoopHostNode->u1HostTimerStatus = SNOOP_FIRST_HALF;

            if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                (u1RecordType == SNOOP_IS_INCLUDE))
            {
                /* TO_INCL should be sent to the router when it is new 
                 * registration from a host or registration that overrides 
                 * the prevoius regirstration */
                if ((SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) == 1) &&
                    (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 1))
                {
                    gu1IsSameHost = SNOOP_SAME_HOST;
                }

                if (u1ResetHost == SNOOP_TRUE)
                {

                    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp,
                                   0, SNOOP_SRC_LIST_SIZE);
                    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp,
                                   0xff, SNOOP_SRC_LIST_SIZE);
                }

                SNOOP_ADD_TO_SRC_BMP_INCLUDE (u2SrcIndex,
                                              pSnoopHostNode->pSourceBmp->
                                              InclSrcBmp);
            }
            else if ((u1RecordType == SNOOP_TO_EXCLUDE) ||
                     (u1RecordType == SNOOP_IS_EXCLUDE))
            {
                /* TO_EXCL should be sent to the router when it is new 
                 * registration from a host or registration that overrides 
                 * the prevoius regirstration */
                if ((SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) == 1) &&
                    (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 1))
                {
                    gu1IsSameHost = SNOOP_SAME_HOST;
                }

                if (u1ResetHost == SNOOP_TRUE)
                {

                    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp,
                                   0, SNOOP_SRC_LIST_SIZE);
                    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp,
                                   0xff, SNOOP_SRC_LIST_SIZE);
                }
                SNOOP_ADD_TO_SRC_BMP_EXCLUDE (u2SrcIndex,
                                              pSnoopHostNode->pSourceBmp->
                                              ExclSrcBmp);
            }
            else if (u1RecordType == SNOOP_ALLOW)
            {
                /* The Group filter mode is the consolidated result of 
                 * all the hosts registered for the group. 
                 * To verify the filter mode of the host the ExclSrcBmp
                 * of the host can be used */
                if (SNOOP_MEM_CMP
                    (pSnoopHostNode->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                     SNOOP_SRC_LIST_SIZE) == 0)
                {
                    /* Add the source to the include source bitmap of the 
                     * source */
                    SNOOP_ADD_TO_SRC_BMP_INCLUDE (u2SrcIndex,
                                                  pSnoopHostNode->pSourceBmp->
                                                  InclSrcBmp);
                }
                else
                {
                    /* The host has sent a previous exclude report.
                     * The current allowed source should only be
                     * removed from the ExclSrcBmp and need not be 
                     * added to the InclSrcBmp */

                    SNOOP_DEL_FROM_SRC_BMP_EXCLUDE (u2SrcIndex,
                                                    pSnoopHostNode->pSourceBmp->
                                                    ExclSrcBmp);
                }
            }
            else if (u1RecordType == SNOOP_BLOCK)
            {
                /* The Group filter mode is the consolidated result of 
                 * all the hosts registered for the group. 
                 * To verify the filter mode of the host the ExclSrcBmp
                 * of the host can be used */
                if (SNOOP_MEM_CMP
                    (pSnoopHostNode->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                     SNOOP_SRC_LIST_SIZE) == 0)
                {
                    /* Del the source to the include source bitmap of the 
                     * source */
                    SNOOP_DEL_FROM_SRC_BMP_INCLUDE (u2SrcIndex,
                                                    pSnoopHostNode->pSourceBmp->
                                                    InclSrcBmp);

                    if (SNOOP_MEM_CMP
                        (pSnoopHostNode->pSourceBmp->InclSrcBmp, gNullSrcBmp,
                         SNOOP_SRC_LIST_SIZE) == 0)
                    {
                        /* As the host has moved to Incl None status, 
                         * setting the HostTimerStatus to SNOOP_SECOND_HALF
                         * so that the host node is removed immediately 
                         * on next HostTimer expiry */
                        pSnoopHostNode->u1HostTimerStatus = SNOOP_SECOND_HALF;
                    }
                }
                else
                {
                    SNOOP_DEL_FROM_SRC_BMP_INCLUDE (u2SrcIndex,
                                                    pSnoopHostNode->pSourceBmp->
                                                    InclSrcBmp);
                    SNOOP_ADD_TO_SRC_BMP_EXCLUDE (u2SrcIndex,
                                                  pSnoopHostNode->pSourceBmp->
                                                  ExclSrcBmp);
                }
            }
            else if (u1RecordType == SNOOP_TO_EXCLUDE_NONE)
            {
                /* TO_EXCL None should be sent to the router when it is new 
                 * registration from a host or registration that overrides 
                 * the prevoius regirstration */
                if ((SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) == 1) &&
                    (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 1))
                {
                    gu1IsSameHost = SNOOP_SAME_HOST;
                }

                SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp,
                               0, SNOOP_SRC_LIST_SIZE);
            }
        }
        return SNOOP_SUCCESS;
    }

    /* if u1ASMHostPresent == TRUE continue, else check the threshold limit */
    if (u1ASMHostPresent == SNOOP_FALSE)
    {
        /* Get the port entry; If port entry is absent, no port-based action
         * will be taken for the packet.*/
        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL))
        {
            if (((pSnoopPortCfgEntry->pSnoopExtPortCfgEntry)->
                 u1SnoopPortMaxLimitType == SNOOP_PORT_LMT_TYPE_GROUPS) &&
                (SNOOP_INSTANCE_INFO (u4Instance)->
                 SnoopInfo[pSnoopGroupEntry->GroupIpAddr.u1Afi - 1].
                 u1FilterStatus != SNOOP_DISABLE))
            {
                if ((pSnoopPortCfgEntry->pSnoopExtPortCfgEntry)->
                    u4SnoopPortCfgMemCnt >=
                    (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry)->
                    u4SnoopPortMaxLimit)
                {

                    SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_CONTROL_PATH_TRC |
                                        SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                        "Maximum join Limit has been reached; No "
                                        "more entries can be learnt on this port "
                                        "%d\n", pSnoopPktInfo->u4InPort);
                    return SNOOP_FAILURE;
                }
                else
                {
                    SNOOP_INCR_MEMBER_CNT (pSnoopPortCfgEntry->
                                           pSnoopExtPortCfgEntry);
                }
            }
        }
    }
    if (SNOOP_SSM_PORT_ALLOC_MEMBLK (u4Instance, pSnoopSSMPortNode) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for SSM port entry for group failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for SSM port entry for group failed\n"));
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopSSMPortNode, 0, sizeof (tSnoopPortEntry));
    if (SNOOP_SRC_BMP_ALLOC_MEMBLK (u4Instance,
                                    pSnoopSSMPortNode->pSourceBmp) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for SSM port source bitmap failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for SSM port source bitmap failed\n"));

        SNOOP_MEM_SET (pSnoopSSMPortNode, 0, sizeof (tSnoopPortEntry));
        MemReleaseMemBlock (SNOOP_PORT_ENTRY_POOL_ID (u4InstId),
                            (UINT1 *) pSnoopSSMPortNode);

        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pSnoopSSMPortNode->pSourceBmp, 0, sizeof (tSnoopSrcBmpNode));

    SNOOP_SLL_INIT (&pSnoopSSMPortNode->HostList);
    pSnoopSSMPortNode->u4Port = pSnoopPktInfo->u4InPort;

    /* Assign back pointer to the group node */
    pSnoopSSMPortNode->pGroupEntry = pSnoopGroupEntry;

    /* Reset the Include(0) and exclude(0xff) bitmap for the port entries */

    SNOOP_MEM_SET (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp, 0,
                   SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp, 0xff,
                   SNOOP_SRC_LIST_SIZE);

    /* Add the host information */
    if (SNOOP_SSM_HOST_ENTRY_ALLOC_MEMBLK (u4Instance, pSnoopHostNode) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Host entry for group failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for Host entry for group failed\n"));
        SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortNode);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopHostNode, 0, sizeof (tSnoopHostEntry));

    if (SNOOP_SRC_BMP_ALLOC_MEMBLK (u4Instance,
                                    pSnoopHostNode->pSourceBmp) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for SSM host source bitmap failed\n");
        SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                      "Memory allocation for SSM host source bitmap failed\n"));
        SNOOP_MEM_SET (pSnoopHostNode, 0, sizeof (tSnoopHostEntry));
        MemReleaseMemBlock (SNOOP_HOST_ENTRY_POOL_ID (u4Instance),
                            (UINT1 *) pSnoopHostNode);
        SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortNode);

        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp, 0, sizeof (tSnoopSrcBmpNode));

    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;
    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME, "Failed to get Vlan entry\n");
        SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);
        SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortNode);
        return SNOOP_FAILURE;
    }
    /* Reset the Include(0) and exclude(0xff) bitmap for the port entries */

    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp, 0,
                   SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp, 0xff,
                   SNOOP_SRC_LIST_SIZE);
    IPVX_ADDR_COPY (&pSnoopHostNode->HostIpAddr, &pSnoopPktInfo->SrcIpAddr);

    /* update Include or Exclude bitmap for the host */
    if ((u1RecordType == SNOOP_TO_INCLUDE) ||
        (u1RecordType == SNOOP_IS_INCLUDE))
    {
        SNOOP_ADD_TO_SRC_BMP_INCLUDE (u2SrcIndex,
                                      pSnoopHostNode->pSourceBmp->InclSrcBmp);
    }
    else if ((u1RecordType == SNOOP_TO_EXCLUDE) ||
             (u1RecordType == SNOOP_IS_EXCLUDE))
    {
        SNOOP_ADD_TO_SRC_BMP_EXCLUDE (u2SrcIndex,
                                      pSnoopHostNode->pSourceBmp->ExclSrcBmp);
    }
    else if (u1RecordType == SNOOP_TO_EXCLUDE_NONE)
    {

        SNOOP_MEM_SET (pSnoopHostNode->pSourceBmp->ExclSrcBmp, 0,
                       SNOOP_SRC_LIST_SIZE);
    }
    pSnoopHostNode->u1HostTimerStatus = SNOOP_FIRST_HALF;
    pSnoopHostNode->SnoopHostType = SNOOP_SSM_HOST;

    /* Assign the back pointer to the SSM port entry */
    pSnoopHostNode->pPortEntry = pSnoopSSMPortNode;

    SNOOP_SLL_ADD (&pSnoopSSMPortNode->HostList,
                   &pSnoopHostNode->NextHostEntry);

    /* Add the host entry to Host information RBTree */
    u4Status = RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                          (tRBElem *) pSnoopHostNode);
    if (u4Status == RB_FAILURE)
    {
        /* Check already any of the host information present for the same */

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                             (tRBElem *) pSnoopHostNode);
        if (pRBElem != NULL)
        {
            if (u1ASMHostPresent == SNOOP_TRUE)
            {
                SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                                pSnoopASMPortNode, tSnoopPortEntry *)
                {
                    if ((pSnoopASMPortNode != NULL) &&
                        (pSnoopASMPortNode->u4Port == pSnoopPktInfo->u4InPort))
                    {
                        /* Delete the Host information for V3 port entry */
                        if (SNOOP_SLL_COUNT (&pSnoopASMPortNode->HostList) != 0)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                       SNOOP_MGMT_DBG,
                                       "Deleting all the Host information for V3 port entry");

                            SNOOP_SLL_SCAN (&pSnoopASMPortNode->HostList,
                                            pSnoopASMHostEntry,
                                            tSnoopHostEntry *)
                            {
                                /* Delete the matching host entry from SSM list */
                                if (pSnoopASMHostEntry ==
                                    (tSnoopHostEntry *) pRBElem)
                                {
                                    SNOOP_SLL_DELETE (&pSnoopASMPortNode->
                                                      HostList,
                                                      pSnoopASMHostEntry);

                                    /* Remove the host from the Host Information RBTree */
                                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_MGMT_DBG,
                                               SNOOP_MGMT_DBG,
                                               "Removing the host from the Host Information RBTree");
                                    RBTreeRemove (SNOOP_INSTANCE_INFO
                                                  (u4Instance)->HostEntry,
                                                  (tRBElem *)
                                                  pSnoopASMHostEntry);

                                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK
                                        (u4Instance, pSnoopASMHostEntry);
                                    break;
                                }
                            }
                        }

                        /* If all the hosts are deleted, delete the SSM port entry */
                        if (SNOOP_SLL_COUNT (&pSnoopASMPortNode->HostList) == 0)
                        {
                            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                                              pSnoopASMPortNode);

                            if (pSnoopASMPortNode->HostPresentTimer.u1TimerType
                                != SNOOP_INVALID_TIMER_TYPE)
                            {
                                SnoopTmrStopTimer (&pSnoopASMPortNode->
                                                   HostPresentTimer);
                            }
                            /* Stop the PortPurge/Leave timer if it is running */
                            if (pSnoopASMPortNode->PurgeOrGrpQueryTimer.
                                u1TimerType != SNOOP_INVALID_TIMER_TYPE)
                            {
                                SnoopTmrStopTimer (&pSnoopASMPortNode->
                                                   PurgeOrGrpQueryTimer);
                            }

                            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance,
                                                        pSnoopASMPortNode);
                        }

                        break;
                    }
                }

                /* Add the host node to the Host Information RBTree */
                u4Status =
                    RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                               (tRBElem *) pSnoopHostNode);
            }
        }

        if (u4Status == RB_FAILURE)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "RBTree Addition Failed for SSM Host Entry \r\n");
            SNOOP_SLL_DELETE (&pSnoopSSMPortNode->HostList, pSnoopHostNode);
            SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);
            SNOOP_SLL_DELETE (&pSnoopSSMPortNode->HostList, pSnoopHostNode);
            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortNode);
            return SNOOP_FAILURE;
        }
    }
    pSnoopSSMPortNode->HostPresentTimer.u4Instance = u4Instance;
    pSnoopSSMPortNode->HostPresentTimer.u1TimerType = SNOOP_HOST_PRESENT_TIMER;
    u2V3HostPresentInterval =
        (UINT2) (pSnoopVlanEntry->u2PortPurgeInt / SNOOP_TWO);
    SnoopTmrStartTimer (&pSnoopSSMPortNode->HostPresentTimer,
                        u2V3HostPresentInterval);
    if ((SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) == 0))
    {
        SNOOP_SLL_INIT (&pSnoopGroupEntry->SSMPortList);
        SNOOP_SLL_ADD (&pSnoopGroupEntry->SSMPortList,
                       &pSnoopSSMPortNode->NextPortEntry);
    }
    else
    {
        SNOOP_SLL_ADD (&pSnoopGroupEntry->SSMPortList,
                       &pSnoopSSMPortNode->NextPortEntry);
    }

    SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port,
                            pSnoopGroupEntry->PortBitmap);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpConsolidateSrcInfo                           */
/*                                                                           */
/* Description        : This function will consolidate the source information*/
/*                      for a group record                                   */
/*                                                                           */
/* Input(s)           : pSnoopGroupEntry - pointer to the group entry        */
/*                      u1RecordType  - Record Type                          */
/*                      u1CurrFilterMode  - Filter mode                      */
/*                      OldInclBmp  - Old Include bitmap                     */
/*                      OldExclBmp  - Old Exclude bitmap                     */
/*                                                                           */
/* Output(s)          : pSnoopGroupEntry - pointer to the group entry.       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpConsolidateSrcInfo (UINT4 u4Instance, UINT1 u1RecordType,
                            UINT1 u1CurrFilterMode,
                            tSnoopSrcBmp OldInclBmp, tSnoopSrcBmp OldExclBmp,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            UINT1 u1ConsReqFlag)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    tSnoopSrcBmp        TempHostInclBmp;
    tSnoopSrcBmp        TempHostExclBmp;
    tSnoopSrcBmp        TempPortInclBmp;
    tSnoopSrcBmp        TempPortExclBmp;
    tSnoopSrcBmp        SnoopExclBmp;
    tIPvXAddr           tempSrcAddr;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    SNOOP_MEM_SET (TempHostInclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempHostExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempPortInclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempPortExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);

    /* Consolidate the include and exclude source bitmap for the group record 
     * Perform union of all the include source bitmaps for host and port 
     * record and intersection of all the exclude source bitmaps for host 
     * and port records */

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {

        SNOOP_MEM_SET (TempHostInclBmp, 0, SNOOP_SRC_LIST_SIZE);
        SNOOP_MEM_SET (TempHostExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
        SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList, pSnoopHostNode,
                        tSnoopHostEntry *)
        {
            /* source bitmap updation for all the hosts on the port */
            SNOOP_OR_SRC_BMP (TempHostInclBmp,
                              pSnoopHostNode->pSourceBmp->InclSrcBmp);
            SNOOP_AND_SRC_BMP (TempHostExclBmp,
                               pSnoopHostNode->pSourceBmp->ExclSrcBmp);
        }

        /* Copy to the port bitmap for all the host present */
        SNOOP_MEM_CPY (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                       TempHostInclBmp, SNOOP_SRC_LIST_SIZE);
        SNOOP_MEM_CPY (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp,
                       TempHostExclBmp, SNOOP_SRC_LIST_SIZE);
        SNOOP_UPDATE_SRC_INFO (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                               pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp);

        SNOOP_OR_SRC_BMP (TempPortInclBmp,
                          pSnoopSSMPortNode->pSourceBmp->InclSrcBmp);
        SNOOP_AND_SRC_BMP (TempPortExclBmp,
                           pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp);
    }

    /* source bitmap updation for all ports for the group */
    SNOOP_MEM_CPY (pSnoopGroupEntry->InclSrcBmp, TempPortInclBmp,
                   SNOOP_SRC_LIST_SIZE);

    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
    {

        SNOOP_MEM_SET (pSnoopGroupEntry->ExclSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    }
    else
    {
        SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, TempPortExclBmp,
                       SNOOP_SRC_LIST_SIZE);
    }

    SNOOP_UPDATE_SRC_INFO (pSnoopGroupEntry->InclSrcBmp,
                           pSnoopGroupEntry->ExclSrcBmp);

    /* Update for any filter mode change in the group record */
    if ((SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                        SNOOP_SRC_LIST_SIZE) != 0) ||
        (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0))
    {
        pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
    }
    else
    {
        pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
    }

    if (u1ConsReqFlag == SNOOP_TRUE)
    {
        SnoopGrpConsolidateGroupInfo (u4Instance,
                                      pSnoopGroupEntry->pConsGroupEntry);
    }

    /* Construct the list of sources for a group record that needs to be sent
     * to the router */
    if (u1RecordType != 0)
    {
        SnoopUtilUpdateSendSrcInfo (u4Instance, u1RecordType,
                                    u1CurrFilterMode, OldInclBmp, OldExclBmp,
                                    pSnoopGroupEntry->pConsGroupEntry);
    }

}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleToIncludeNone                          */
/*                                                                           */
/* Description        : This function will handle IGMPv3/MLDv2 TO_INCLUDE    */
/*                      message                                              */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      pSnoopPktInfo  - snoop packet information            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleToIncludeNone (UINT4 u4Instance,
                             tSnoopGroupEntry * pSnoopGroupEntry,
                             tSnoopVlanEntry * pSnoopVlanEntry,
                             tSnoopPktInfo * pSnoopPktInfo,
                             tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    tSnoopPortEntry    *pSnoopASMPortEntry = NULL;
    tSnoopHostEntry    *pSnoopASMHostEntry = NULL;
    tSnoopSrcBmp        OldInclSrcBmp;
    tSnoopSrcBmp        OldExclSrcBmp;
    UINT1               u1EntryFound = SNOOP_FALSE;
    UINT1               u1HostFound = SNOOP_FALSE;
    UINT1               u1FastLeaveStatus = SNOOP_FALSE;
    BOOL1               bResult = SNOOP_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* Check if the port is present in the SSM port record */
    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
        {
            u1EntryFound = SNOOP_TRUE;
            break;
        }
    }

    if (u1EntryFound == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }

    /* Check if the host record is present */
    SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList, pSnoopHostNode,
                    tSnoopHostEntry *)
    {
        if (IPVX_ADDR_COMPARE (pSnoopHostNode->HostIpAddr,
                               pSnoopPktInfo->SrcIpAddr) == 0)
        {
            u1HostFound = SNOOP_TRUE;
            break;
        }
    }
    if (u1HostFound == SNOOP_FALSE)
    {
        return SNOOP_SUCCESS;
    }

    /* Check the leave processing level to be used on this instance
     * and obtain the leave mode configured for the interface. */
    if (SNOOP_SYSTEM_LEAVE_LEVEL (u4Instance) == SNOOP_VLAN_LEAVE_CONFIG)
    {
        if (pSnoopVlanEntry->u1FastLeave == SNOOP_ENABLE)
        {
            u1FastLeaveStatus = SNOOP_TRUE;
        }
    }
    else
    {
        if ((pSnoopPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry != NULL) &&
            (pSnoopPortCfgEntry->pSnoopExtPortCfgEntry->u1SnoopPortLeaveMode ==
             SNOOP_LEAVE_FAST_LEAVE))
        {
            u1FastLeaveStatus = SNOOP_TRUE;
        }
    }

    /* Previously this host has sent some registartion, now we got TO_INCL 
     * (None) from the same host */

    if (u1FastLeaveStatus == SNOOP_FALSE)
    {
        /* Process the Incl None message by removing only the host node */

        /* Delete the host from the port record */
        SNOOP_SLL_DELETE (&pSnoopSSMPortNode->HostList, pSnoopHostNode);

        /* Remove the host from the Host Information RBTree */
        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                      (tRBElem *) pSnoopHostNode);

        SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);

        /* If this is the last host on this port we need to delete the port 
         * record and update the group and forwarding database */
        if (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 0)
        {
            SnoopHandleSSMPortPurge (u4Instance, pSnoopSSMPortNode);
        }
        else
        {
            SNOOP_MEM_CPY (OldInclSrcBmp, pSnoopGroupEntry->pConsGroupEntry->
                           InclSrcBmp, SNOOP_SRC_LIST_SIZE);
            SNOOP_MEM_CPY (OldExclSrcBmp, pSnoopGroupEntry->pConsGroupEntry->
                           ExclSrcBmp, SNOOP_SRC_LIST_SIZE);
            /* Conslodation of include and exclude sources for the group */
            SnoopGrpConsolidateSrcInfo
                (u4Instance, SNOOP_TO_INCLUDE,
                 pSnoopGroupEntry->pConsGroupEntry->u1FilterMode,
                 OldInclSrcBmp, OldExclSrcBmp, pSnoopGroupEntry, SNOOP_TRUE);
            SnoopFwdSummarizeIpFwdTable (u4Instance, pSnoopGroupEntry,
                                         SNOOP_TRUE);
        }

    }
    else
    {
        /* Fast leave is enabled on this interface. Remove all the hosts
         * attached on this port */

        /* Check if a V1/v2 host are attached on this port */
        SNOOP_IS_PORT_PRESENT (pSnoopPktInfo->u4InPort,
                               pSnoopGroupEntry->ASMPortBitmap, bResult);
        if (bResult == SNOOP_TRUE)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                            pSnoopASMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopASMPortEntry->u4Port == pSnoopPktInfo->u4InPort)
                {
                    break;
                }
            }

            SNOOP_CHK_NULL_PTR_RET (pSnoopASMPortEntry, SNOOP_FAILURE);

            /* Stop the PortPurge/Leave timer if it is running */
            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Update the Group entry ASM port bitmap */
            SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortEntry->u4Port,
                                      pSnoopGroupEntry->ASMPortBitmap);

            /*Release the port node from the ASMPortList */
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortEntry);

            /* Stop the Asm Host Present Timer and
             * release all the hosts attached to the port */

            if (pSnoopASMPortEntry->HostPresentTimer.u1TimerType
                != SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopASMPortEntry->HostPresentTimer);
            }
            if (SNOOP_SLL_COUNT (&pSnoopASMPortEntry->HostList) != 0)
            {
                while ((pSnoopASMHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopASMPortEntry->HostList)) !=
                       NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopASMPortEntry->HostList,
                                      pSnoopASMHostEntry);

                    /* Remove the host from the Host Information RBTree */
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                                  (tRBElem *) pSnoopASMHostEntry);

                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance,
                                                      pSnoopASMHostEntry);
                }
            }

            SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortEntry);
        }
        /* Stop the v3 port purge timer if it is running */
        if (pSnoopSSMPortNode->HostPresentTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&(pSnoopSSMPortNode->HostPresentTimer));
        }

        if (SnoopHandleSSMPortPurge (u4Instance, pSnoopSSMPortNode)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_TMR_NAME,
                           "Unable to delete ssmport from group record\r\n");
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpHandleToExcludeNone                          */
/*                                                                           */
/* Description        : This function will handle IGMPv3/MLDv2 TO_EXCLUDE    */
/*                      message                                              */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      pSnoopPktInfo  - Snooped packet information          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopGrpHandleToExcludeNone (UINT4 u4Instance,
                             tSnoopGroupEntry * pSnoopGroupEntry,
                             tSnoopPktInfo * pSnoopPktInfo,
                             tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    /* check if the SNOOP group entry exist if not present
     * add the group and forwarding table */
    if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "SNOOP group entry added to group and forwarding table\r\n");
        pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
        gu4SendRecType = SNOOP_TO_EXCLUDE;
    }
    else
    {
        /* Group entry present, update the entry */
        if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
        {
            /* Change the filter  mode to exclude */
            pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
        }
    }

    /* Update the include and exclude bitmap for the port and 
     * host entry present in the group record */
    if (SnoopGrpUpdateSSMPortToGroupEntry (u4Instance, pSnoopGroupEntry,
                                           0, pSnoopPktInfo,
                                           pSnoopPortCfgEntry,
                                           SNOOP_FALSE,
                                           SNOOP_TO_EXCLUDE_NONE)
        != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Unable to update the port entry for the group\r\n");
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopConsGroupEntryUpdate                            */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      entry for the given group entry                      */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u1Action         - SNOOP_CREATE_GRP_ENTRY /          */
/*                                         SNOOP_DELETE_GRP_ENTRY            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopConsGroupEntryUpdate (UINT4 u4Instance,
                           tSnoopGroupEntry * pSnoopGroupEntry, UINT1 u1Action)
{
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopConsolidatedGroupEntry SnoopConsGroupEntry;
    tRBElem            *pRBElem = NULL;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopConsGroupEntryUpdate\r\n");
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (u1Action == SNOOP_CREATE_GRP_ENTRY)
    {
        SNOOP_MEM_SET (&SnoopConsGroupEntry, 0,
                       sizeof (tSnoopConsolidatedGroupEntry));

        SNOOP_OUTER_VLAN (SnoopConsGroupEntry.VlanId) =
            SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
        IPVX_ADDR_COPY (&SnoopConsGroupEntry.GroupIpAddr,
                        &pSnoopGroupEntry->GroupIpAddr);

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                             (tRBElem *) & SnoopConsGroupEntry);

        pSnoopConsGroupEntry = (tSnoopConsolidatedGroupEntry *) pRBElem;

        if (pSnoopConsGroupEntry == NULL)
        {
            /* Create a new consolidated group entry */
            if (SNOOP_VLAN_CONS_GRP_ALLOC_MEMBLK (pSnoopConsGroupEntry) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory allocation for consolidated group "
                               "entry failed\r\n");
                SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, gu4IgsSysLogId,
                              "Memory allocation for consolidated group entry failed "));
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopConsGroupEntryUpdate\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pSnoopConsGroupEntry, 0,
                           sizeof (tSnoopConsolidatedGroupEntry));

            SNOOP_OUTER_VLAN (pSnoopConsGroupEntry->VlanId) =
                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
            IPVX_ADDR_COPY (&(pSnoopConsGroupEntry->GroupIpAddr),
                            &(pSnoopGroupEntry->GroupIpAddr));
            SNOOP_UTL_DLL_INIT (&(pSnoopConsGroupEntry->GroupEntryList),
                                FSAP_OFFSETOF (tSnoopGroupEntry, NextGrpNode));
            SNOOP_MEM_SET (pSnoopConsGroupEntry->ExclSrcBmp, 0xff,
                           SNOOP_SRC_LIST_SIZE);
            pSnoopConsGroupEntry->u1ASMHostPresent = SNOOP_FALSE;

            if (RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                           (tRBElem *) pSnoopConsGroupEntry) == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for Group Entry \r\n");
                SNOOP_VLAN_CONS_GRP_FREE_MEMBLK (pSnoopConsGroupEntry);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopConsGroupEntryUpdate\r\n");
                return SNOOP_FAILURE;
            }
        }

        SNOOP_DLL_ADD (&(pSnoopConsGroupEntry->GroupEntryList),
                       &(pSnoopGroupEntry->NextGrpNode));
        /* Back pointer to consolidated group entry */
        pSnoopGroupEntry->pConsGroupEntry = pSnoopConsGroupEntry;

    }
    else
    {
        pSnoopConsGroupEntry = pSnoopGroupEntry->pConsGroupEntry;

        if (pSnoopConsGroupEntry == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Consolidated group entry not present. \r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopConsGroupEntryUpdate\r\n");
            return SNOOP_FAILURE;
        }

        SNOOP_DLL_DELETE (&(pSnoopConsGroupEntry->GroupEntryList),
                          &(pSnoopGroupEntry->NextGrpNode));

        if (SNOOP_DLL_COUNT (&pSnoopConsGroupEntry->GroupEntryList) == 0)
        {
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                          (tRBElem *) pSnoopConsGroupEntry);
            SNOOP_VLAN_CONS_GRP_FREE_MEMBLK (pSnoopConsGroupEntry);
        }
        /* Clearing the back pointer of consolidated group entry */
        pSnoopGroupEntry->pConsGroupEntry = NULL;
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopConsGroupEntryUpdate\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpGetConsGroupEntry                            */
/*                                                                           */
/* Description        : This function gets the consolidated group entry from */
/*                      consolidated group membership data base              */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr  - Grp IP address                            */
/*                                                                           */
/* Output(s)          : pointer to the consolidated group entry or NULL      */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpGetConsGroupEntry (UINT4 u4Instance, tSnoopTag VlanId,
                           tIPvXAddr GrpAddr,
                           tSnoopConsolidatedGroupEntry **
                           ppRetSnoopConsGroupEntry)
{
    tRBElem            *pRBTreeElem = NULL;
    tSnoopConsolidatedGroupEntry SnoopConsGroupEntry;
    UINT4               u4TempGrpIpAddr = 0;
    CHR1               *pc1String = NULL;
    CHR1                au1Ip6Addr[IPVX_IPV6_ADDR_LEN];

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&SnoopConsGroupEntry, 0,
                   sizeof (tSnoopConsolidatedGroupEntry));

    SNOOP_OUTER_VLAN (SnoopConsGroupEntry.VlanId) = SNOOP_OUTER_VLAN (VlanId);
    IPVX_ADDR_COPY (&(SnoopConsGroupEntry.GroupIpAddr), &GrpAddr);
    pRBTreeElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                             (tRBElem *) & SnoopConsGroupEntry);

    if (pRBTreeElem == NULL)
    {
        if (SnoopConsGroupEntry.GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
        {
            SNOOP_MEM_CPY (&u4TempGrpIpAddr, GrpAddr.au1Addr,
                           IPVX_IPV4_ADDR_LEN);
            u4TempGrpIpAddr = SNOOP_HTONL (u4TempGrpIpAddr);
            SNOOP_CONVERT_IPADDR_TO_STR (pc1String, u4TempGrpIpAddr);
        }
        else if (SnoopConsGroupEntry.GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
        {
            SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
            SNOOP_MEM_CPY (au1Ip6Addr, GrpAddr.au1Addr, IPVX_IPV6_ADDR_LEN);
            SNOOP_INET_NTOHL (au1Ip6Addr);
            pc1String =
                (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr);
        }
        SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_NAME,
                            "Consolidated Group Entry not found for VLAN %d "
                            "Group %s\r\n", SNOOP_OUTER_VLAN (VlanId),
                            pc1String);
        return SNOOP_FAILURE;
    }
    else
    {
        *ppRetSnoopConsGroupEntry =
            (tSnoopConsolidatedGroupEntry *) pRBTreeElem;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpConsolidateGroupInfo                         */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      entry information by scanning all the group entry    */
/*                      for this Outer VLAN.                                 */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpConsolidateGroupInfo (UINT4 u4Instance,
                              tSnoopConsolidatedGroupEntry *
                              pSnoopConsGroupEntry)
{
    tSnoopGroupEntry   *pSnoopCheckGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopSrcBmp        TempGroupInclBmp;
    tSnoopSrcBmp        TempGroupExclBmp;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;

    UNUSED_PARAM (u4Instance);

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_CPY (TempGroupInclBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_CPY (TempGroupExclBmp, SnoopExclBmp, SNOOP_SRC_LIST_SIZE);
    /* Consolidate the include and exclude source bitmap for the 
     * consolidated group record  */
    SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                               pSnoopCheckGroupEntry, pSnoopTempGroupEntry,
                               VOID *)
    {
        /* source bitmap updation for all the group entry */
        SNOOP_OR_SRC_BMP (TempGroupInclBmp, pSnoopCheckGroupEntry->InclSrcBmp);
        SNOOP_AND_SRC_BMP (TempGroupExclBmp, pSnoopCheckGroupEntry->ExclSrcBmp);

        if (SNOOP_SLL_COUNT (&pSnoopCheckGroupEntry->ASMPortList) != 0)
        {
            u1ASMHostPresent = SNOOP_TRUE;
        }
    }

    pSnoopConsGroupEntry->u1ASMHostPresent = u1ASMHostPresent;
    /* source bitmap updation for all groups for the consolidated group */
    SNOOP_MEM_CPY (pSnoopConsGroupEntry->InclSrcBmp, TempGroupInclBmp,
                   SNOOP_SRC_LIST_SIZE);

    if (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_TRUE)
    {
        SNOOP_MEM_CPY (pSnoopConsGroupEntry->ExclSrcBmp, gNullSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
    }
    else
    {
        SNOOP_MEM_CPY (pSnoopConsGroupEntry->ExclSrcBmp, TempGroupExclBmp,
                       SNOOP_SRC_LIST_SIZE);
    }

    SNOOP_UPDATE_SRC_INFO (pSnoopConsGroupEntry->InclSrcBmp,
                           pSnoopConsGroupEntry->ExclSrcBmp);

    /* Update for any filter mode change in the consolidated group record */
    if ((SNOOP_MEM_CMP (pSnoopConsGroupEntry->ExclSrcBmp, SnoopExclBmp,
                        SNOOP_SRC_LIST_SIZE) != 0) ||
        (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_TRUE))
    {
        pSnoopConsGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
    }
    else
    {
        pSnoopConsGroupEntry->u1FilterMode = SNOOP_INCLUDE;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopUpdateConsGrpOnFilterChange                     */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      entry on filter mode change                          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                      u1UpdateFlag - This flag is used to check whether    */
/*                                     the updation of send sorce info is    */
/*                                     required or not                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopUpdateConsGrpOnFilterChange (UINT4 u4Instance, UINT1 u1RecordType,
                                  tSnoopConsolidatedGroupEntry *
                                  pSnoopConsGroupEntry, UINT1 u1UpdateFlag)
{
    tSnoopSrcBmp        OldInclBmp;
    tSnoopSrcBmp        OldExclBmp;
    UINT1               u1CurrFilterMode = 0;

    SNOOP_MEM_SET (OldInclBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (OldExclBmp, 0, SNOOP_SRC_LIST_SIZE);

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        pSnoopConsGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
        SNOOP_MEM_CPY (pSnoopConsGroupEntry->ExclSrcBmp, gNullSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
        pSnoopConsGroupEntry->u1ASMHostPresent = SNOOP_TRUE;
        gu4SendRecType = SNOOP_TO_EXCLUDE;
    }
    else
    {
        SNOOP_MEM_CPY (OldInclBmp, pSnoopConsGroupEntry->InclSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
        SNOOP_MEM_CPY (OldExclBmp, pSnoopConsGroupEntry->ExclSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
        u1CurrFilterMode = pSnoopConsGroupEntry->u1FilterMode;

        SnoopGrpConsolidateGroupInfo (u4Instance, pSnoopConsGroupEntry);

        if (u1UpdateFlag == SNOOP_TRUE)
        {
            SnoopUtilUpdateSendSrcInfo (u4Instance, u1RecordType,
                                        u1CurrFilterMode, OldInclBmp,
                                        OldExclBmp, pSnoopConsGroupEntry);
        }
    }
}

/*****************************************************************************/
/* Function Name      : SnoopGrpCheckAndSendConsReport                       */
/*                                                                           */
/* Description        : This function will update the consolidated group     */
/*                      and if any change present inform router port         */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      pSnoopConsGroupEntry - Pointer to the consolidated   */
/*                                             group entry                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpCheckAndSendConsReport (UINT4 u4Instance,
                                tSnoopConsolidatedGroupEntry *
                                pSnoopConsGroupEntry)
{
    UINT1              *pPortBitmap = NULL;
    UINT1              *pVlanPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tIPvXAddr           tempSrcAddr;
    tSnoopTag           VlanId;
    UINT4               u4MaxLength = 0;
    INT4                i4Result = 0;
    UINT2               u2IpAddrLen = 0;
    UINT2               u2NumSrcs = 0;
    UINT2               u2Count = 0;
    UINT1               u1EntryUsed = SNOOP_FALSE;
    UINT1               u1RecordType = 0;
    UINT1               u1AuxDataLen = 0;
    UINT1               u1Forward = VLAN_FORWARD_SPECIFIC;
    UINT1               au1GrpAddr[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    SNOOP_VALIDATE_INSTANCE (u4Instance);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_OUTER_VLAN (VlanId) = SNOOP_OUTER_VLAN (pSnoopConsGroupEntry->VlanId);

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               pSnoopConsGroupEntry->GroupIpAddr.u1Afi,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME, "Vlan entry not found\r\n");
        return;
    }

    if ((pSnoopVlanEntry->u1AddressType == 0) ||
        (pSnoopVlanEntry->u1AddressType > SNOOP_MAX_PROTO))
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME, "Invalid Address Type \r\n");
        return;
    }

    if (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance,
                                              (pSnoopVlanEntry->u1AddressType) -
                                              1) == SNOOP_TRUE)
    {
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SnoopUpdateConsGrpOnFilterChange (u4Instance,
                                              SNOOP_HOST_TIMER_EXPIRY,
                                              pSnoopConsGroupEntry, SNOOP_TRUE);

            /* If there is no change in group membership database
             * we dont need to send any report upstream */
            if (gu4SendRecType == 0)
            {
                return;
            }
            /* Either Proxy or proxy reporting is enabled 
             * so generate a report based upon the 
             * operating Version of the router ports for the VLAN and send it */

            /* if v3 Rtrport list is not empty then send IGMPv3 or MLDv2 
             * report */
            pSnoopRtrPortBmp =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pSnoopRtrPortBmp == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                return;
            }
            SNOOP_MEM_SET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN (VlanId),
                                                     pSnoopVlanEntry->
                                                     u1AddressType,
                                                     SNOOP_IGS_IGMP_VERSION3,
                                                     pSnoopRtrPortBmp,
                                                     pSnoopVlanEntry);
            if (SNOOP_MEM_CMP
                (pSnoopRtrPortBmp, gNullPortBitMap,
                 sizeof (tSnoopPortBmp)) != 0)

            {
                /* Use the misc preallocated buffer and copy the group 
                 * information to the buffer */

                SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);
                SNOOP_MEM_CPY (au1GrpAddr,
                               pSnoopConsGroupEntry->GroupIpAddr.au1Addr,
                               IPVX_MAX_INET_ADDR_LEN);
                SNOOP_INET_HTONL (au1GrpAddr);

                u2IpAddrLen = (pSnoopVlanEntry->u1AddressType ==
                               SNOOP_ADDR_TYPE_IPV4) ?
                    SNOOP_IP_ADDR_SIZE : IPVX_MAX_INET_ADDR_LEN;
                u1AuxDataLen = 0;

                /* Get the record type and number of sources from 
                 * gu4SendRecType which is updated for any new registration 
                 * or source list change for a group record */
                u1RecordType = (UINT1) (gu4SendRecType & 0x0000ffff);
                u2NumSrcs = SNOOP_HTONS (gu4SendRecType >> 16);

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               &u1RecordType, SNOOP_OFFSET_ONE);
                u4MaxLength += SNOOP_OFFSET_ONE;

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               &u1AuxDataLen, SNOOP_OFFSET_ONE);
                u4MaxLength += SNOOP_OFFSET_ONE;

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               &u2NumSrcs, SNOOP_SRC_CNT_OFFSET);
                u4MaxLength += SNOOP_SRC_CNT_OFFSET;

                SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                               au1GrpAddr, u2IpAddrLen);
                u4MaxLength += u2IpAddrLen;

                u2NumSrcs = (UINT2) (gu4SendRecType >> 16);
                if (u2NumSrcs)
                {
                    SNOOP_MEM_CPY (gpSSMRepSendBuffer + u4MaxLength,
                                   gpSSMSrcInfoBuffer, u2IpAddrLen * u2NumSrcs);
                    u4MaxLength += (u2NumSrcs * u2IpAddrLen);
                }

                gu4SendRecType = 0;

#ifdef IGS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                {
                    i4Result = IgsEncodeAggV3Report (u4Instance,
                                                     1, u4MaxLength, &pOutBuf);
                }
#endif
#ifdef MLDS_WANTED
                if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                {
                    i4Result = MldsEncodeAggV2Report (u4Instance,
                                                      1, u4MaxLength, &pOutBuf);
                }
#endif

                if (i4Result != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_PKT_NAME,
                                   "Unable to generate and send V3 report "
                                   "on to router ports\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    return;

                }
                pPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pPortBitmap == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                   "Error in allocating memory for pPortBitmap\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
                    return;
                }
                SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

                if (SNOOP_INSTANCE_INFO (u4Instance)->
                    SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1ReportFwdAll
                    == SNOOP_FORWARD_NONEDGE_PORTS)
                {
                    pVlanPortBitmap =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pVlanPortBitmap == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                       "Error in allocating memory for pVlanPortBitmap\r\n");
                        UtilPlstReleaseLocalPortList (pPortBitmap);
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        CRU_BUF_Release_MsgBufChain (pOutBuf, FALSE);
                        return;
                    }
                    SNOOP_MEM_SET (pVlanPortBitmap, 0, sizeof (tSnoopPortBmp));

                    if (SnoopMiGetVlanLocalEgressPorts
                        (u4Instance, pSnoopVlanEntry->VlanId,
                         pVlanPortBitmap) == SNOOP_SUCCESS)
                    {
                        SnoopUtilGetNonEdgePortList (u4Instance,
                                                     pVlanPortBitmap,
                                                     pPortBitmap);
                    }
                    UtilPlstReleaseLocalPortList (pVlanPortBitmap);
                }
                else
                {
                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType,
                                                             SNOOP_IGS_IGMP_VERSION3,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);

                    SNOOP_MEM_CPY (pPortBitmap, pSnoopRtrPortBmp,
                                   sizeof (tSnoopPortBmp));
                }

                u1Forward =
                    ((SNOOP_INSTANCE_INFO (u4Instance)->
                      SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].
                      u1ReportFwdAll ==
                      SNOOP_FORWARD_ALL_PORTS) ? VLAN_FORWARD_ALL :
                     VLAN_FORWARD_SPECIFIC);

                SnoopVlanForwardPacket (u4Instance,
                                        pSnoopVlanEntry->VlanId,
                                        pSnoopVlanEntry->u1AddressType,
                                        0, u1Forward, pOutBuf,
                                        pPortBitmap, SNOOP_SSMREPORT_SENT);
                UtilPlstReleaseLocalPortList (pPortBitmap);
            }
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        }
    }
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
    {
        /* Before decrementing the group reference count check if this
         * source is not used by any other group */
        for (u2Count = 1; u2Count <= SNOOP_MAX_MCAST_SRCS; u2Count++)
        {
            if ((SNOOP_MEM_CMP (&(SNOOP_SRC_INFO (u4Instance,
                                                  (u2Count - 1)).SrcIpAddr),
                                &tempSrcAddr, sizeof (tIPvXAddr)) == 0) ||
                (SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount == 0))
            {
                continue;
            }

            SnoopUtilCheckIsSourceUsed (u4Instance, u2Count, &u1EntryUsed);
            if (u1EntryUsed == SNOOP_FALSE)
            {
                SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount--;
                if (SNOOP_SRC_INFO (u4Instance,
                                    (u2Count - 1)).u2GrpRefCount == 0)
                {
                    /* Remove the source from the linked list */
                    SNOOP_REMOVE_SOURCE (u4Instance,
                                         SNOOP_SRC_INFO (u4Instance,
                                                         (u2Count - 1)));
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_SRC_DBG,
                               SNOOP_SRC_DBG,
                               "Removing the source from the linked list\r\n");

                    SNOOP_MEM_SET
                        (&(SNOOP_SRC_INFO (u4Instance,
                                           (u2Count - 1)).SrcIpAddr),
                         0, sizeof (tIPvXAddr));
                    gu2SourceCount--;
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : SnoopGrpGetHostEntry                                 */
/*                                                                           */
/* Description        : This function gets the host entry from host          */
/*                      information data base                                */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr    - Grp IP address                          */
/*                      u4Port     - Port Index                              */
/*                      HostAddr   - Host IP address                         */
/*                                                                           */
/* Output(s)          : pointer to the host entry or NULL                    */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGrpGetHostEntry (UINT4 u4Instance, tSnoopTag VlanId,
                      tIPvXAddr GrpAddr, UINT4 u4Port,
                      tIPvXAddr HostAddr,
                      tSnoopHostEntry ** ppRetSnoopHostEntry)
{
    tRBElem            *pRBTreeElem = NULL;
    tSnoopHostEntry     SnoopHostEntry;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopPortEntry     SnoopPortEntry;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&SnoopHostEntry, 0, sizeof (tSnoopHostEntry));
    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (&SnoopPortEntry, 0, sizeof (tSnoopPortEntry));

    SNOOP_MEM_CPY (SnoopGroupEntry.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopPortEntry.u4Port = u4Port;

    IPVX_ADDR_COPY (&(SnoopGroupEntry.GroupIpAddr), &GrpAddr);
    IPVX_ADDR_COPY (&(SnoopHostEntry.HostIpAddr), &HostAddr);

    SnoopPortEntry.pGroupEntry = &SnoopGroupEntry;
    SnoopHostEntry.pPortEntry = &SnoopPortEntry;

    pRBTreeElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry,
                             (tRBElem *) & SnoopHostEntry);

    if (pRBTreeElem == NULL)
    {
        SNOOP_GBL_DBG_ARG4 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_OS_RES_NAME,
                            "Host Entry not found for VLAN %d inner vlan %d"
                            " Group %s and Host %s\r\n",
                            SNOOP_OUTER_VLAN (VlanId),
                            SNOOP_INNER_VLAN (VlanId),
                            SnoopPrintIPvxAddress (GrpAddr),
                            SnoopPrintIPvxAddress (HostAddr)) return
            SNOOP_FAILURE;
    }
    else
    {
        *ppRetSnoopHostEntry = (tSnoopHostEntry *) pRBTreeElem;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopNoOfStaticAndDynamicGrpEntry                    */
/*                                                                           */
/* Description        : This function gets the no of static and dynamic      */
/*                      group entries found for the given port               */
/*                                                                           */
/* Input(s)           : u1Port - Port number                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopNoOfStaticAndDynamicGrpEntry (UINT4 u4Port, UINT2 *pu2StaticCnt,
                                   UINT2 *pu2DynamicCnt)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT1               u1Instance = 0;
    BOOL1               bResult = OSIX_FALSE;

    *pu2StaticCnt = 0;
    *pu2DynamicCnt = 0;

    pRBElem = RBTreeGetFirst (gapSnoopGlobalInstInfo[u1Instance]->GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext = RBTreeGetNext (gapSnoopGlobalInstInfo[u1Instance]->
                                     GroupEntry, pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap, bResult);

        if (bResult == OSIX_FALSE)
        {
            pRBElem = pRBElemNext;
            continue;
        }
        else
        {
            if (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_STATIC)
                (*pu2StaticCnt)++;
            else if (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_DYNAMIC)
                (*pu2DynamicCnt)++;
        }
        pRBElem = pRBElemNext;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeleteExceededGrpEntry                          */
/*                                                                           */
/* Description        : This function deletes exceeded group entry           */
/*                      corresponding to a port                              */
/*                                                                           */
/* Input(s)           : u4Port     - Port number                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopDeleteExceededGrpEntry (UINT4 u4Port, UINT2 u2NoOfEntriesToDelete)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopASMPortEntry *pSnoopASMPortEntry = NULL;
    /*tSnoopSSMPortEntry *pSnoopSSMPortEntry = NULL; */
    tSnoopPortEntry    *pSnoopSSMPortEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pPortBitmap = NULL;
    tCRU_BUF_CHAIN_HEADER *pOutBuf = NULL;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           tempSrcAddr;
    tSnoopHostEntry    *pSnoopHostEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT4               u4MaxLength = 0;
    UINT4               u1RecordType = 0;
    INT4                i4Result = 0;
    UINT2               u2NumGrpRec = 0;
    UINT2               u2NoSources = 0;
    UINT2               u2Index = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1PortFound = SNOOP_FALSE;
    UINT1               u1SSMPortFound = SNOOP_FALSE;
    UINT1               u1Instance = 0;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u1Instance;
    gu4SnoopDbgInstId = u1Instance;

    SNOOP_MEM_SET (gpSSMRepSendBuffer, 0, SNOOP_MISC_MEMBLK_SIZE);

    pRBElem = RBTreeGetFirst (gapSnoopGlobalInstInfo[u1Instance]->GroupEntry);

    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pPortBitmap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    while ((pRBElem != NULL) && (u2NoOfEntriesToDelete != 0))
    {
        u1PortFound = SNOOP_FALSE;
        u1SSMPortFound = SNOOP_FALSE;

        pRBElemNext = RBTreeGetNext (gapSnoopGlobalInstInfo[u1Instance]->
                                     GroupEntry, pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap, bResult);

        if ((bResult == OSIX_FALSE) ||
            (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_STATIC))
        {
            pRBElem = pRBElemNext;
            continue;
        }

        SNOOP_SLL_SCAN (&pSnoopGroupEntry->ASMPortList,
                        pSnoopASMPortEntry, tSnoopASMPortEntry *)
        {
            if (pSnoopASMPortEntry->u4Port == u4Port)
            {
                u1PortFound = SNOOP_TRUE;
                break;
            }
        }

        if (SNOOP_MCAST_FWD_MODE (u1Instance) == SNOOP_MCAST_FWD_MODE_IP)
        {
            SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                            pSnoopSSMPortEntry, tSnoopPortEntry *)
            {
                if (pSnoopSSMPortEntry->u4Port == u4Port)
                {
                    u1SSMPortFound = SNOOP_TRUE;
                    break;
                }
            }
        }

        if ((u1PortFound == SNOOP_FALSE) && (u1SSMPortFound == SNOOP_FALSE))
        {
            pRBElem = pRBElemNext;
            continue;
        }

        if ((u1PortFound == SNOOP_TRUE) &&
            (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_DYNAMIC))
        {
            /* Stop the PortPurge/Leave timer if it is running */
            if (pSnoopASMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the PortPurge/Leave timer on port %d\r\n",
                                pSnoopASMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopASMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Update the Group entry V2 port bitmap */
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG, SNOOP_GRP_DBG,
                       "Updating the Group entry V2 port bitmap\r\n");
            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->ASMPortBitmap);

            /*Release the port node from the V1V2PortList */
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList,
                              pSnoopASMPortEntry);
            SNOOP_ASM_PORT_FREE_MEMBLK (u1Instance, pSnoopASMPortEntry);
            pSnoopASMPortEntry = NULL;
        }

        if (u1SSMPortFound == SNOOP_TRUE)
        {
            /* Stop the PortPurge/Leave timer if it is running */
            if (pSnoopSSMPortEntry->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the PortPurge/Leave timer on port %d\r\n",
                                pSnoopSSMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->PurgeOrGrpQueryTimer);
            }

            /* Stop the Host present timer if it is running */
            if (pSnoopSSMPortEntry->HostPresentTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                                "Stopping the Host present timer on port %d\r\n",
                                pSnoopSSMPortEntry->u4Port);
                SnoopTmrStopTimer (&pSnoopSSMPortEntry->HostPresentTimer);
            }

            /* Delete all the Host information for SSM port entry */
            if (SNOOP_SLL_COUNT (&pSnoopSSMPortEntry->HostList) != 0)
            {
                while ((pSnoopHostEntry = (tSnoopHostEntry *)
                        SNOOP_SLL_FIRST (&pSnoopSSMPortEntry->HostList))
                       != NULL)
                {
                    SNOOP_SLL_DELETE (&pSnoopSSMPortEntry->HostList,
                                      pSnoopHostEntry);
                    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u1Instance,
                                                      pSnoopHostEntry);
                    pSnoopHostEntry = NULL;
                }
            }

            /* Release the port node from the V1V2PortList */
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortEntry);
            SNOOP_SSM_PORT_FREE_MEMBLK (u1Instance, pSnoopSSMPortEntry);
        }

        /* Update the Group entry port bitmap */
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG, SNOOP_GRP_DBG,
                   "Update the Group entry V2 port bitmap\r\n");
        SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopGroupEntry->PortBitmap);

        if ((SNOOP_GRP_ENTRY_COUNT (u4Port)) > 0)
        {
            SNOOP_GRP_ENTRY_COUNT (u4Port)--;
        }

        u2NoOfEntriesToDelete--;

        IPVX_ADDR_INIT (GrpAddr, pSnoopGroupEntry->GroupIpAddr.u1Afi,
                        pSnoopGroupEntry->GroupIpAddr.au1Addr);

        if (SNOOP_MCAST_FWD_MODE (u1Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            if (SnoopFwdUpdateMacFwdTable (u1Instance, pSnoopGroupEntry,
                                           u4Port, SNOOP_DEL_PORT,
                                           SNOOP_DISABLE_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                           "on V1/V2 port purge timer expiry failed\r\n");
            }

            /* Send this Group Membership Information sync to the 
             * Standby Node.
             */
            SnoopRedActiveSendGrpInfoSync (u1Instance, pSnoopGroupEntry);
        }
        else
        {
            /* IP based mode */
            if (SnoopFwdUpdateIpFwdTable (u1Instance, pSnoopGroupEntry,
                                          tempSrcAddr, u4Port,
                                          gNullPortBitMap,
                                          SNOOP_DEL_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Updation of MAC forwarding table "
                           "on V1/V2 port purge timer expiry failed\r\n");
            }
        }

        if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap, gNullPortBitMap,
                           SNOOP_PORT_LIST_SIZE) != 0)
        {
            if (SNOOP_MCAST_FWD_MODE (u1Instance) == SNOOP_MCAST_FWD_MODE_IP)
            {
                /* Check if any V2 receivers are there if present nothing needs 
                 * to be done, else send filter mode change with existing V3 
                 * source information */
                if (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* If the ASM port bitmap has become NULL set the exclude 
                     * source bitmap for the group to default value and 
                     * consolidate the group source bitmaps */
                    SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                                   SNOOP_SRC_LIST_SIZE);

                    SnoopGrpConsolidateSrcInfo (u1Instance, 0, 0, gNullSrcBmp,
                                                gNullSrcBmp, pSnoopGroupEntry,
                                                SNOOP_TRUE);

                    /* Check for any filter mode change has occurred so just 
                     * update router with the filter mode change record */
                    if (SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp,
                                       SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)
                    {
                        pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
                        u1RecordType = SNOOP_TO_EXCLUDE;
                    }
                    else
                    {
                        pSnoopGroupEntry->u1FilterMode = SNOOP_INCLUDE;
                        u1RecordType = SNOOP_TO_INCLUDE;
                    }

                    if (SnoopVlanGetVlanEntry (u1Instance,
                                               pSnoopGroupEntry->VlanId,
                                               pSnoopGroupEntry->GroupIpAddr.
                                               u1Afi,
                                               &pSnoopVlanEntry)
                        == SNOOP_SUCCESS)
                    {
                        if ((pSnoopVlanEntry->u1OperVersion ==
                             SNOOP_IGS_IGMP_VERSION3 &&
                             pSnoopVlanEntry->u1AddressType ==
                             SNOOP_ADDR_TYPE_IPV4) ||
                            (pSnoopVlanEntry->u1OperVersion ==
                             SNOOP_MLD_VERSION2 &&
                             pSnoopVlanEntry->u1AddressType ==
                             SNOOP_ADDR_TYPE_IPV6))
                        {
                            /* if the forwarding is done based on MAC then 
                             * construct SSM Reports for all the Group records 
                             * with NULL source list */
                            /* Use the misc preallocated buffer and copy the 
                             * group informtaion to the buffer when the max 
                             * length reaches just send the buffer out */

                            for (u2Index = 1;
                                 u2Index <= (SNOOP_SRC_LIST_SIZE * 8);
                                 u2Index++)
                            {
                                if ((u1RecordType == SNOOP_IS_INCLUDE) ||
                                    (u1RecordType == SNOOP_TO_INCLUDE))
                                {
                                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                             InclSrcBmp,
                                                             u2Index,
                                                             SNOOP_SRC_LIST_SIZE,
                                                             bResult);
                                    if (bResult == OSIX_TRUE)
                                    {
                                        u2NoSources++;
                                    }
                                }
                                else
                                {
                                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->
                                                             ExclSrcBmp,
                                                             u2Index,
                                                             SNOOP_SRC_LIST_SIZE,
                                                             bResult);
                                    if (bResult == OSIX_TRUE)
                                    {
                                        u2NoSources++;
                                    }
                                }
                            }

#ifdef IGS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV4)
                            {
                                IgsUtilFillV3SourceInfo (u1Instance,
                                                         pSnoopGroupEntry->
                                                         pConsGroupEntry,
                                                         u1RecordType,
                                                         u2NoSources,
                                                         &u4MaxLength);
                                u2NoSources = 0;
                                u2NumGrpRec++;
                                if (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE)
                                {
                                    i4Result = IgsEncodeAggV3Report
                                        (u1Instance, u2NumGrpRec, u4MaxLength,
                                         &pOutBuf);
                                }
                            }
#endif
#ifdef MLDS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV6)
                            {
                                MldsUtilFillV2SourceInfo (u1Instance,
                                                          pSnoopGroupEntry->
                                                          pConsGroupEntry,
                                                          u2NoSources,
                                                          u1RecordType,
                                                          &u4MaxLength);
                                u2NoSources = 0;
                                u2NumGrpRec++;
                                if (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)
                                {
                                    i4Result =
                                        MldsEncodeAggV2Report (u1Instance,
                                                               u2NumGrpRec,
                                                               u4MaxLength,
                                                               &pOutBuf);
                                }
                            }
#endif

                            if (((pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV4)
                                 && (u4MaxLength >= SNOOP_IGS_MAX_PKT_SIZE))
                                ||
                                ((pSnoopVlanEntry->u1AddressType ==
                                  SNOOP_ADDR_TYPE_IPV6)
                                 && (u4MaxLength >= SNOOP_MLDS_MAX_PKT_SIZE)))
                            {
                                if (i4Result != SNOOP_SUCCESS)
                                {
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u1Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE,
                                                   SNOOP_PKT_NAME,
                                                   "Unable to generate and send SSM"
                                                   "report on to router ports\r\n");
                                    UtilPlstReleaseLocalPortList (pPortBitmap);
                                    return SNOOP_FAILURE;
                                }

                                SNOOP_MEM_SET (gpSSMRepSendBuffer, 0,
                                               SNOOP_MISC_MEMBLK_SIZE);
                                /* Send the report message to router ports */
                                SNOOP_MEM_CPY (pPortBitmap,
                                               pSnoopVlanEntry->RtrPortBitmap,
                                               sizeof (tSnoopPortBmp));

                                SnoopVlanForwardPacket (u1Instance,
                                                        pSnoopVlanEntry->VlanId,
                                                        pSnoopVlanEntry->
                                                        u1AddressType,
                                                        SNOOP_INVALID_PORT,
                                                        VLAN_FORWARD_SPECIFIC,
                                                        pOutBuf, pPortBitmap,
                                                        SNOOP_SSMREPORT_SENT);

                                u2NumGrpRec = 0;
                            }
                        }
                        else
                        {
#ifdef IGS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV4)
                            {
                                i4Result = IgsEncodeQueryResponse
                                    (u1Instance, pSnoopVlanEntry,
                                     pSnoopGroupEntry->pConsGroupEntry, 0);
                            }
#endif
#ifdef MLDS_WANTED
                            if (pSnoopVlanEntry->u1AddressType ==
                                SNOOP_ADDR_TYPE_IPV6)
                            {
                                i4Result = MldsEncodeQueryResponse
                                    (u1Instance, pSnoopVlanEntry,
                                     pSnoopGroupEntry->pConsGroupEntry, 0);
                            }
#endif

                            if (i4Result != SNOOP_SUCCESS)
                            {
                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_PKT_NAME,
                                               "Unable to send "
                                               "response to query\r\n");
                                UtilPlstReleaseLocalPortList (pPortBitmap);
                                return SNOOP_FAILURE;
                            }
                        }
                    }
                }
            }
            pRBElem = pRBElemNext;
            continue;
        }
        else
        {
            /* Form and Send Leave message */
#ifdef IGS_WANTED
            if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                i4Result = IgsEncodeAndSendLeaveMsg (u1Instance,
                                                     pSnoopGroupEntry->VlanId,
                                                     pSnoopGroupEntry->
                                                     pConsGroupEntry);
            }
#endif

#ifdef MLDS_WANTED
            if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                i4Result = MldsEncodeAndSendDoneMsg (u1Instance,
                                                     pSnoopGroupEntry->VlanId,
                                                     pSnoopGroupEntry->
                                                     pConsGroupEntry);
            }
#endif

            if (i4Result != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u1Instance),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                    "Unable to send Leave message "
                                    "for the Group %s\r\n",
                                    SnoopPrintIPvxAddress (GrpAddr));
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }

            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u1Instance, pSnoopGroupEntry->VlanId,
                                          GrpAddr) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Deletion of Group record failed\r\n");
                UtilPlstReleaseLocalPortList (pPortBitmap);
                return SNOOP_FAILURE;
            }
        }
        pRBElem = pRBElemNext;
    }
    UtilPlstReleaseLocalPortList (pPortBitmap);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeleteAllGroupEntryInfo                         */
/*                                                                           */
/* Description        : This function gets the IP forwarding entry           */
/*                                                                           */
/* Input(s)           : u1Instance - Instance number                         */
/*                      pSnoopGroupEntry - Pointer to snoop group entry      */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopDeleteAllGroupEntryInfo (UINT1 u1Instance,
                              tSnoopGroupEntry * pSnoopGroupEntry)
{
    tIPvXAddr           SrcAddr;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    if (SnoopFwdUpdateFwdEntries (pSnoopGroupEntry) == SNOOP_FAILURE)
    {
        if ((SNOOP_MCAST_FWD_MODE ((UINT4) u1Instance) ==
             SNOOP_MCAST_FWD_MODE_IP)
            &&
            (SnoopFwdGetIpForwardingEntry
             ((UINT4) u1Instance, pSnoopGroupEntry->VlanId,
              pSnoopGroupEntry->GroupIpAddr, SrcAddr,
              &pSnoopIpGrpFwdEntry) != SNOOP_SUCCESS))
        {
            if (SnoopGrpDeleteGroupEntry (u1Instance, pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_GRP_NAME,
                               "Deletion of Group record failed\r\n");
                return SNOOP_FAILURE;
            }
            return SNOOP_SUCCESS;
        }
        return SNOOP_FAILURE;
    }

    if (SnoopGrpDeleteGroupEntry (u1Instance, pSnoopGroupEntry->VlanId,
                                  pSnoopGroupEntry->GroupIpAddr)
        != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME, "Deletion of Group record failed\r\n");
        return SNOOP_FAILURE;
    }

#ifdef IGS_WANTED
    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        if (IgsEncodeAndSendLeaveMsg (u1Instance, pSnoopGroupEntry->VlanId,
                                      (tSnoopConsolidatedGroupEntry *)
                                      pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u1Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_VLAN_NAME,
                                "Unable to send Leave message for the Group "
                                "%s\r\n",
                                SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                       GroupIpAddr));
            return SNOOP_FAILURE;
        }
    }
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpCreateStaticGroupEntry                       */
/*                                                                           */
/* Description        : This function creates a new Static group entry       */
/*                                                                           */
/* Input(s)           : u1Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr  -   Grp IP address                          */
/*                                                                           */
/* Output(s)          : ppRetSnoopGroupEntry - pointer to the static group   */
/*             entry                             */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopGrpCreateStaticGroupEntry (UINT1 u1Instance, tSnoopTag VlanId,
                                tIPvXAddr GrpAddr,
                                tSnoopGroupEntry ** ppRetSnoopGroupEntry)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT4               u4Status = 0;

    if (gapSnoopGlobalInstInfo[u1Instance]->GroupEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "No Group entries found for this instance\r\n");
        return SNOOP_FAILURE;
    }

    if (SnoopGrpGetGroupEntry (u1Instance, VlanId, GrpAddr,
                               &pSnoopGroupEntry) == SNOOP_SUCCESS)
    {
        if (pSnoopGroupEntry->u1EntryTypeFlag == SNOOP_GRP_DYNAMIC)
        {
            SnoopDeleteAllGroupEntryInfo (u1Instance, pSnoopGroupEntry);
        }
    }

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u1Instance;
    gu4SnoopDbgInstId = u1Instance;

    if (SNOOP_VLAN_GRP_ALLOC_MEMBLK (u1Instance, pSnoopGroupEntry) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Maximum groups exceeded. Memory allocation for group entry failed\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_NOTICE_LEVEL, SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                          SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

    /* Set default values for the Group table */
    /* pSnoopGroupEntry->VlanId = VlanId; */
    SNOOP_MEM_CPY (pSnoopGroupEntry->VlanId, VlanId, sizeof (tSnoopTag));
    IPVX_ADDR_COPY (&(pSnoopGroupEntry->GroupIpAddr), &GrpAddr);
    pSnoopGroupEntry->u1EntryTypeFlag = SNOOP_GRP_STATIC;
    pSnoopGroupEntry->u1ReportFwdFlag = SNOOP_REPORT_FORWARD;
    pSnoopGroupEntry->u1FilterMode = SNOOP_EXCLUDE;
    pSnoopGroupEntry->u1IsUnknownMulticast = SNOOP_FALSE;

    SNOOP_SLL_INIT (&pSnoopGroupEntry->ASMPortList);
    SNOOP_SLL_INIT (&pSnoopGroupEntry->SSMPortList);

    SNOOP_MEM_CPY (pSnoopGroupEntry->InclSrcBmp, gNullSrcBmp,
                   SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_CPY (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                   SNOOP_SRC_LIST_SIZE);

    /* Add Node */
    u4Status = RBTreeAdd (gapSnoopGlobalInstInfo[u1Instance]->GroupEntry,
                          (tRBElem *) pSnoopGroupEntry);

    if (u4Status == RB_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RBTree Addition Failed for Group Entry \r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_SNP_RB_TREE_ADD_FAIL]);
        return SNOOP_FAILURE;
    }

    if (SnoopConsGroupEntryUpdate (u1Instance, pSnoopGroupEntry,
                                   SNOOP_CREATE_GRP_ENTRY) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Updation of consolidated group entry failed\r\n");
        RBTreeRemove (SNOOP_INSTANCE_INFO (u1Instance)->GroupEntry,
                      (tRBElem *) pSnoopGroupEntry);
        SNOOP_VLAN_GRP_FREE_MEMBLK (u1Instance, pSnoopGroupEntry);
        return SNOOP_FAILURE;
    }

    *ppRetSnoopGroupEntry = pSnoopGroupEntry;
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeleteSSMHostEntry                           */
/*                                                                           */
/* Description        : This function deletes SSM host entry                 */
/*                      for the port/group/host.                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance                                */
/*                     tSnoopPortEntry - pointer to the port entry           */
/*                      pSnoopGroupEntry - pointer to the group entry        */
/*                     pSnoopHostNode - pointer to the host entry            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpDeleteSSMHostEntry (UINT4 u4Instance,
                            tSnoopPortEntry * pSnoopSSMPortNode,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            tSnoopHostEntry * pSnoopHostNode)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    UINT2               u2HostPurgeInterval = 0;
    UINT1               u1AddressType = 0;
    tSnoopTag           VlanId;
    tIPvXAddr           tempSrcAddr;
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    if (SNOOP_MCAST_FWD_MODE (u4Instance) != SNOOP_MCAST_FWD_MODE_IP)
    {
        /* This routing is written with respect to IP Forwarding Mode */
        return;
    }
    SNOOP_VALIDATE_ADDRESS_TYPE (pSnoopGroupEntry->GroupIpAddr.u1Afi);

    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;
    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME,
                       "Unable to get Vlan record for the given instance "
                       "and the VlanId \r\n");
        return;
    }
    /* Stop the Host present timer if it is running */
    if (pSnoopSSMPortNode->HostPresentTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopSSMPortNode->HostPresentTimer);
    }
    pRBElem = (tRBElem *) pSnoopHostNode;
    /* Remove the host from the Host Information RBTree */
    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry, pRBElem);
    /* Remove the host from the Host Information SLL */
    SNOOP_SLL_DELETE (&pSnoopSSMPortNode->HostList, pSnoopHostNode);

    SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);
    /* It there is no more host connected to the port,
     * remove the port from the list and
     * clear the Forwarding database for the same */
    if (SNOOP_SLL_COUNT (&pSnoopSSMPortNode->HostList) == 0)
    {
        if (pSnoopSSMPortNode->pSourceBmp != NULL)
        {
            /* Stop the port purge interval timer */
            if (pSnoopSSMPortNode->PurgeOrGrpQueryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopSSMPortNode->PurgeOrGrpQueryTimer);
            }

            SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
            if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                          tempSrcAddr,
                                          pSnoopSSMPortNode->u4Port,
                                          gNullPortBitMap,
                                          SNOOP_DEL_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_TRC, "Updation of IP forwarding table "
                               "for handling ASM Report failed\r\n");
            }
            SNOOP_SLL_DELETE (&pSnoopGroupEntry->SSMPortList,
                              pSnoopSSMPortNode);

            SNOOP_SSM_PORT_FREE_MEMBLK (u4Instance, pSnoopSSMPortNode);

        }
        return;
    }
    /* Meaning : Atleast one host is present in this port, so
     * start the host purge interval timer */
    u2HostPurgeInterval = (UINT2) (pSnoopVlanEntry->u2PortPurgeInt /
                                   SNOOP_DEF_RETRY_COUNT);
    pSnoopSSMPortNode->HostPresentTimer.u4Instance = u4Instance;
    pSnoopSSMPortNode->HostPresentTimer.u1TimerType = SNOOP_HOST_PRESENT_TIMER;
    SnoopTmrStartTimer (&pSnoopSSMPortNode->HostPresentTimer,
                        u2HostPurgeInterval);

    /* For the ASM port entry, we have atleast one host present on the port.
     * As, it does not result in any filter mode change, group consolidation
     * is not required */
    if (pSnoopSSMPortNode->pSourceBmp == NULL)
    {
        return;
    }

    SnoopGrpConsolidateSrcInfo
        (u4Instance, SNOOP_HOST_TIMER_EXPIRY,
         pSnoopGroupEntry->pConsGroupEntry->u1FilterMode,
         pSnoopGroupEntry->pConsGroupEntry->InclSrcBmp,
         pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
         pSnoopGroupEntry, SNOOP_TRUE);
    SnoopFwdSummarizeIpFwdTable (u4Instance, pSnoopGroupEntry, SNOOP_TRUE);

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopGrpDeleteASMHostEntry                           */
/*                                                                           */
/* Description        : This function deletes ASM host entry                 */
/*                      for the port/group/source of host ip.                */
/*                                                                           */
/* Input(s)           : u4Instance - Instance                                */
/*                     tSnoopPortEntry - pointer to the port entry           */
/*                      pSnoopGroupEntry - pointer to the group entry        */
/*                     pSnoopHostNode - pointer to host entry                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopGrpDeleteASMHostEntry (UINT4 u4Instance,
                            tSnoopPortEntry * pSnoopASMPortNode,
                            tSnoopGroupEntry * pSnoopGroupEntry,
                            tSnoopHostEntry * pSnoopHostNode)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    UINT2               u2HostPurgeInterval = 0;
    UINT1               u1AddressType = 0;
    tIPvXAddr           tempSrcAddr;
    tSnoopTag           VlanId;

    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopGrpDeleteASMHostEntry\r\n");
    SNOOP_VALIDATE_ADDRESS_TYPE (pSnoopGroupEntry->GroupIpAddr.u1Afi);

    SNOOP_MEM_CPY (VlanId, pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                   "Unable to get Vlan record for the given instance "
                   "and the VlanId \r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpDeleteASMHostEntry\r\n");
        return;
    }
    /* Stop the Asm Host present timer if it is running */
    if (pSnoopASMPortNode->HostPresentTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopASMPortNode->HostPresentTimer);
    }
    pRBElem = (tRBElem *) pSnoopHostNode;
    /* Remove the host from the Host Information RBTree and SLL */
    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry, pRBElem);
    SNOOP_SLL_DELETE (&pSnoopASMPortNode->HostList, pSnoopHostNode);

    SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK (u4Instance, pSnoopHostNode);
    if (SNOOP_SLL_COUNT (&pSnoopASMPortNode->HostList) == 0)
    {
        /* It there is no more host connected to the port,
         * remove the port from the list and
         * clear the Forwarding database for the same */
        /* Stop the Port purge or group query timer if it is running */
        if (pSnoopASMPortNode->PurgeOrGrpQueryTimer.u1TimerType !=
            SNOOP_INVALID_TIMER_TYPE)
        {
            SnoopTmrStopTimer (&pSnoopASMPortNode->PurgeOrGrpQueryTimer);
        }
        SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
        if (SnoopFwdUpdateIpFwdTable (u4Instance, pSnoopGroupEntry,
                                      tempSrcAddr, pSnoopASMPortNode->u4Port,
                                      gNullPortBitMap, SNOOP_DEL_PORT)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_FWD | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_FWD_NAME, "Updation of IP forwarding table "
                           "for handling SSM Report failed\r\n");
        }
        SNOOP_SLL_DELETE (&pSnoopGroupEntry->ASMPortList, pSnoopASMPortNode);

        SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                  pSnoopGroupEntry->ASMPortBitmap);
        SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                  pSnoopGroupEntry->PortBitmap);
#ifdef L2RED_WANTED
        SNOOP_DEL_FROM_PORT_LIST (pSnoopASMPortNode->u4Port,
                                  pSnoopGroupEntry->V1PortBitmap);
#endif
        SNOOP_ASM_PORT_FREE_MEMBLK (u4Instance, pSnoopASMPortNode);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpDeleteASMHostEntry\r\n");
        return;
    }
    /* We have atleast one host present on the port. Let us re-start
     * the host present timer
     */

    u2HostPurgeInterval = (UINT2) (pSnoopVlanEntry->u2PortPurgeInt /
                                   SNOOP_DEF_RETRY_COUNT);
    pSnoopASMPortNode->HostPresentTimer.u4Instance = u4Instance;
    pSnoopASMPortNode->HostPresentTimer.u1TimerType = SNOOP_HOST_PRESENT_TIMER;
    SnoopTmrStartTimer (&pSnoopASMPortNode->HostPresentTimer,
                        u2HostPurgeInterval);

    /* For the ASM port entry, we have atleast one host present on the port.
     * As, it does not result in any filter mode change, group consolidation
     * is not required */
    if (pSnoopASMPortNode->pSourceBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopGrpDeleteASMHostEntry\r\n");
        return;
    }

    SnoopGrpConsolidateSrcInfo
        (u4Instance, SNOOP_HOST_TIMER_EXPIRY,
         pSnoopGroupEntry->pConsGroupEntry->u1FilterMode,
         pSnoopGroupEntry->pConsGroupEntry->InclSrcBmp,
         pSnoopGroupEntry->pConsGroupEntry->ExclSrcBmp,
         pSnoopGroupEntry, SNOOP_TRUE);
    SnoopFwdSummarizeIpFwdTable (u4Instance, pSnoopGroupEntry, SNOOP_TRUE);

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopGrpDeleteASMHostEntry\r\n");
    return;
}
