
/* SOURCE FILE HEADER :
 *
 * $Id: snpcli.c,v 1.162.2.2 2018/05/18 06:58:32 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : snpcli.c                                         |
 * |  SUBSYSTEM NAME        : SNOOP                                            |
 * |  MODULE NAME           : SNOOP configuration                              |
 * |  LANGUAGE              : C                                                |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |  DESCRIPTION           : Action routines for fssnoop.mib set/get objects  | 
 * |                                                                           |
 * |                                                                           |
 *  ---------------------------------------------------------------------------
 */
#ifndef __SNPCLI_C__
#define __SNPCLI_C__

#if defined (IGS_WANTED) || defined (MLDS_WANTED)
#include "snpinc.h"
#endif
#include "fssnpcli.h"

/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_snoop_cmd                              */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the SNOOP module as   */
/*                        defined in snoopcmd.def                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
INT4
cli_process_snoop_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
#ifdef IGS_WANTED
    UINT1              *pau1PortList = NULL;
#endif
    UINT4              *args[SNOOP_CLI_MAX_ARGS];
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4              *pu4Inst = NULL;
    UINT4               u4ContextId = SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4RetStatus = CLI_SUCCESS;
    tVlanId             VlanId = 0;
    tSnoopTag           InnerVlanId;
    INT1                argno = 0;
    tSnoopIfPortBmp    *pRouterPorts = NULL;
    tSnoopIfPortBmp    *pMemberPorts = NULL;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4IpAddr = 0;
    UINT1               u1IpFlag = 0;

    /* If SNOOP is not Started, then all the SNOOP/MLDS commands are considered 
     * as invalid except Start/Enable command. */

    SNOOP_MEM_SET (gNullGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    if (SnoopCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId) == SNOOP_FAILURE)
    {
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */

    pu4Inst = va_arg (ap, UINT4 *);

    if (pu4Inst != NULL)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu4Inst);
    }
    else
    {
        u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == SNOOP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    CliRegisterLock (CliHandle, SnpLock, SnpUnLock);

    SNOOP_LOCK ();

    VlanId = (tVlanId) CLI_GET_VLANID ();

    pRouterPorts =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pRouterPorts == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pRouterPorts, 0, SNOOP_IF_PORT_LIST_SIZE);

    pMemberPorts =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pMemberPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pRouterPorts);
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pMemberPorts, 0, SNOOP_IF_PORT_LIST_SIZE);

    switch (u4Command)
    {
        case SNOOP_SYS_SHUT:
            i4RetStatus = SnoopCliSetSystemControl (CliHandle, u4ContextId,
                                                    SNOOP_SHUTDOWN);
            break;

        case SNOOP_SYS_NO_SHUT:
            i4RetStatus = SnoopCliSetSystemControl (CliHandle, u4ContextId,
                                                    SNOOP_STARTED);
            break;

        case SNOOP_MCAST_FWD_MODE_SET:
            i4RetStatus =
                SnoopCliSetMcastFwdMode (CliHandle, (INT4) u4ContextId,
                                         CLI_PTR_TO_I4 (args[0]));
            break;

        case SNOOP_MCAST_FWD_MODE_CP_MODEL_SET:
            i4RetStatus =
                SnoopCliSetControlPlaneDriven (CliHandle, (INT4) u4ContextId,
                                               CLI_PTR_TO_I4 (args[0]));
            break;

        case SNOOP_MCAST_LEAVE_CONFIG_LEVEL:
            i4RetStatus =
                SnoopCliSetLeaveConfigLevel (CliHandle, (INT4) u4ContextId,
                                             CLI_PTR_TO_I4 (args[0]));
            break;

        case SNOOP_SYS_ENH_MODE_ENABLE:
            i4RetStatus = SnoopCliSetSystemEnhMode (CliHandle, u4ContextId,
                                                    SNOOP_ADDR_TYPE_IPV4,
                                                    SNOOP_ENABLE);
            break;
        case SNOOP_SYS_ENH_MODE_DISABLE:
            i4RetStatus = SnoopCliSetSystemEnhMode (CliHandle, u4ContextId,
                                                    SNOOP_ADDR_TYPE_IPV4,
                                                    SNOOP_DISABLE);
            break;

        case SNOOP_SYS_SPARSE_MODE_ENABLE:
            i4RetStatus = SnoopCliSetSystemSparseMode (CliHandle, u4ContextId,
                                                       SNOOP_ADDR_TYPE_IPV4,
                                                       SNOOP_ENABLE);
            break;
        case SNOOP_SYS_SPARSE_MODE_DISABLE:
            i4RetStatus = SnoopCliSetSystemSparseMode (CliHandle, u4ContextId,
                                                       SNOOP_ADDR_TYPE_IPV4,
                                                       SNOOP_DISABLE);
            break;

        case SNOOP_MCAST_REPORT_CONFIG_LEVEL:
            i4RetStatus = SnoopCliSetReportConfigLevel (CliHandle, u4ContextId,
                                                        CLI_PTR_TO_I4 (args
                                                                       [0]));
            break;
#ifdef IGS_WANTED

        case IGS_SYS_ENABLE:
            i4RetStatus = SnoopCliSetModuleStatus (CliHandle, u4ContextId,
                                                   SNOOP_ADDR_TYPE_IPV4,
                                                   SNOOP_ENABLE);
            break;

        case IGS_SYS_DISABLE:
            i4RetStatus = SnoopCliSetModuleStatus (CliHandle, u4ContextId,
                                                   SNOOP_ADDR_TYPE_IPV4,
                                                   SNOOP_DISABLE);
            break;

        case IGS_SEND_QUERY_ENABLE:
            i4RetStatus = SnoopCliSetSendQuery (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_ENABLE);

            break;
        case IGS_SEND_QUERY_DISABLE:
            i4RetStatus = SnoopCliSetSendQuery (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_DISABLE);

            break;

        case IGS_VLAN_STATS_CLEAR_COUNTERS:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle, "\r%% This CLI command is not"
                           " supported in standby node\r\n");
                break;
            }

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliVlanResetStatistics (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 (UINT2) *args[0]);
            }
            else
            {
                i4RetStatus =
                    SnoopCliVlanResetStatistics (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_INVALID_VLAN_ID);
            }

            break;
        case IGS_PROXY_ENABLE:
            i4RetStatus =
                SnoopCliSetProxyStatus (CliHandle, u4ContextId,
                                        SNOOP_ADDR_TYPE_IPV4, SNOOP_ENABLE);
            break;

        case IGS_PROXY_DISABLE:
            i4RetStatus =
                SnoopCliSetProxyStatus (CliHandle, u4ContextId,
                                        SNOOP_ADDR_TYPE_IPV4, SNOOP_DISABLE);
            break;

        case IGS_PROXY_REPORTING_ENABLE:
            i4RetStatus =
                SnoopCliSetProxyReportingStatus (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_ENABLE);
            break;

        case IGS_PROXY_REPORTING_DISABLE:
            i4RetStatus =
                SnoopCliSetProxyReportingStatus (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_DISABLE);
            break;

        case IGS_ROUTER_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetRtrPortPurgeInterval (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 *args[0]);
            break;

        case IGS_NO_ROUTER_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetRtrPortPurgeInterval (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_DEF_RTR_PURGE_INTERVAL);
            break;

        case IGS_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetPortPurgeInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4, *args[0]);
            break;

        case IGS_NO_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetPortPurgeInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4,
                                              SNOOP_DEF_PORT_PURGE_INTERVAL);
            break;

        case IGS_REPORT_SUPPRESS_INTERVAL:
            i4RetStatus =
                SnoopCliSetReportFwdInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4, *args[0]);
            break;

        case IGS_NO_REPORT_SUPPRESS_INTERVAL:
            i4RetStatus =
                SnoopCliSetReportFwdInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4,
                                              SNOOP_DEF_REPORT_FWD_INTERVAL);
            break;

        case IGS_RETRY_COUNT:
            i4RetStatus =
                SnoopCliSetRetryCount (CliHandle, u4ContextId,
                                       SNOOP_ADDR_TYPE_IPV4, (UINT1) *args[0]);
            break;

        case IGS_NO_RETRY_COUNT:
            i4RetStatus =
                SnoopCliSetRetryCount (CliHandle, u4ContextId,
                                       SNOOP_ADDR_TYPE_IPV4,
                                       SNOOP_DEF_RETRY_COUNT);
            break;

        case IGS_GROUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetGrpQueryInterval (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV4, *args[0]);
            break;

        case IGS_NO_GROUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetGrpQueryInterval (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV4,
                                             SNOOP_DEF_GRP_QUERY_INTERVAL);
            break;

        case IGS_REPORT_FORWADING:
            i4RetStatus =
                SnoopCliSetReportForwardAll (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV4,
                                             CLI_PTR_TO_I4 (args[0]));
            break;

        case IGS_NO_REPORT_FORWADING:
            i4RetStatus =
                SnoopCliSetReportForwardAll (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV4,
                                             SNOOP_FORWARD_RTR_PORTS);
            break;

        case IGS_QUERY_FORWADING:
            i4RetStatus =
                SnoopCliSetQueryForwardAll (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            CLI_PTR_TO_I4 (args[0]));
            break;

        case IGS_VLAN_ENABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopModuleStatus (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                  SNOOP_ENABLE);
            break;

        case IGS_VLAN_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopModuleStatus (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                  SNOOP_DISABLE);
            break;

        case IGS_VLAN_OPER_VERSION:
            i4RetStatus =
                SnoopCliSetVlanSnoopOperVersion (CliHandle, u4ContextId,
                                                 VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;

        case IGS_VLAN_FAST_LEAVE_ENABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopFastLeave (CliHandle, u4ContextId,
                                               VlanId, SNOOP_ADDR_TYPE_IPV4,
                                               SNOOP_ENABLE);
            break;

        case IGS_VLAN_FAST_LEAVE_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopFastLeave (CliHandle, u4ContextId,
                                               VlanId, SNOOP_ADDR_TYPE_IPV4,
                                               SNOOP_DISABLE);
            break;

        case IGS_VLAN_QUERIER_ENABLE:
            if (NULL != args[1])
            {
                u4IpAddr = *(UINT4 *) (args[1]);
            }
            else
            {
                u4IpAddr = 0;
            }
            u1IpFlag = (UINT1) CLI_PTR_TO_I4 (args[0]);

            i4RetStatus =
                SnoopCliSetVlanSnoopQuerier (CliHandle, u4ContextId,
                                             VlanId, SNOOP_ADDR_TYPE_IPV4,
                                             u1IpFlag, u4IpAddr, SNOOP_ENABLE);
            break;

        case IGS_VLAN_QUERIER_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopQuerier (CliHandle, u4ContextId,
                                             VlanId, SNOOP_ADDR_TYPE_IPV4,
                                             u1IpFlag, u4IpAddr, SNOOP_DISABLE);
            break;

        case IGS_VLAN_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanQueryInterval (CliHandle, u4ContextId,
                                              VlanId, SNOOP_ADDR_TYPE_IPV4,
                                              *args[0]);
            break;

        case IGS_NO_VLAN_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanQueryInterval (CliHandle, u4ContextId,
                                              VlanId, SNOOP_ADDR_TYPE_IPV4,
                                              SNOOP_DEF_QUERY_INTERVAL);
            break;

        case IGS_VLAN_STARTUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanStartupQueryInterval (CliHandle, u4ContextId,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     *((UINT4 *) args[0]));
            break;

        case IGS_NO_VLAN_STARTUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanStartupQueryInterval (CliHandle, u4ContextId,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     SNOOP_DEF_STARTUP_QINTERVAL);
            break;

        case IGS_VLAN_STARTUP_QUERY_COUNT:
            i4RetStatus =
                SnoopCliSetVlanStartupQueryCount (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                  *((UINT4 *) args[0]));
            break;

        case IGS_NO_VLAN_STARTUP_QUERY_COUNT:
            i4RetStatus =
                SnoopCliSetVlanStartupQueryCount (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                  SNOOP_DEF_STARTUP_QRY_COUNT);
            break;

        case IGS_VLAN_OTHER_QUERIER_PRESENT_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanOtherQuerierPrsntInterval (CliHandle,
                                                          u4ContextId, VlanId,
                                                          SNOOP_ADDR_TYPE_IPV4,
                                                          *((UINT4 *) args[0]));
            break;

        case IGS_NO_VLAN_OTHER_QUERIER_PRESENT_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanOtherQuerierPrsntInterval (CliHandle,
                                                          u4ContextId, VlanId,
                                                          SNOOP_ADDR_TYPE_IPV4,
                                                          SNOOP_OTHER_QUERIER_PRESENT_INTERVAL);
            break;

        case IGS_VLAN_MAX_RESP_CODE:
            i4RetStatus =
                SnoopCliSetVlanMaxResponseCode (CliHandle, (INT4) u4ContextId,
                                                VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                *((INT4 *) args[0]));
            break;

        case IGS_NO_VLAN_MAX_RESP_CODE:
            i4RetStatus =
                SnoopCliSetVlanMaxResponseCode (CliHandle, (INT4) u4ContextId,
                                                VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_IGS_DEF_MAX_RESP_CODE);
            break;
        case IGS_VLAN_RTR_PORT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRouterPorts (CliHandle, u4ContextId,
                                            VlanId, SNOOP_ADDR_TYPE_IPV4,
                                            (UINT1 *) *pRouterPorts);
            break;

        case IGS_VLAN_NO_RTR_PORT:
            /* Check for the invalid router port list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliDelVlanRouterPorts (CliHandle, u4ContextId,
                                            VlanId, SNOOP_ADDR_TYPE_IPV4,
                                            (UINT1 *) *pRouterPorts);
            break;
        case IGS_RTR_PORT_PURGE_INT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortPurgeInterval (CliHandle,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     (UINT1 *) *pRouterPorts,
                                                     *((INT4 *) args[2]));
            break;

        case IGS_RTR_PORT_NO_PURGE_INT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortPurgeInterval (CliHandle,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     (UINT1 *) *pRouterPorts,
                                                     SNOOP_DEF_RTR_PURGE_INTERVAL);
            break;

        case IGS_RTR_PORT_OPER_VERSION:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortOperVersion (CliHandle,
                                                   VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                   (UINT1 *) *pRouterPorts,
                                                   CLI_PTR_TO_I4 (args[2]));
            break;

        case IGS_RTR_PORT_NO_OPER_VERSION:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortOperVersion (CliHandle,
                                                   VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                   (UINT1 *) *pRouterPorts,
                                                   SNOOP_IGS_IGMP_VERSION3);
            break;

        case IGS_VLAN_BLOCKED_RTR_PORT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanBlockedRtrPorts (CliHandle, u4ContextId,
                                                VlanId, SNOOP_ADDR_TYPE_IPV4,
                                                (UINT1 *) *pRouterPorts);
            break;

        case IGS_VLAN_NO_BLOCKED_RTR_PORT:
            /* Check for the invalid router port list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliDelVlanBlkRtrPorts (CliHandle, u4ContextId,
                                            VlanId, SNOOP_ADDR_TYPE_IPV4,
                                            (UINT1 *) *pRouterPorts);
            break;

        case IGS_VLAN_MVLAN_PROFILE:

            i4RetStatus =
                SnoopCliMapMVlanToProfile (CliHandle, u4ContextId, VlanId,
                                           SNOOP_ADDR_TYPE_IPV4, *args[0]);
            break;
        case IGS_VLAN_ROBUSTNESS_VALUE:
            i4RetStatus = SnoopVlanSetRobustnessValue (CliHandle, u4ContextId,
                                                       VlanId,
                                                       SNOOP_ADDR_TYPE_IPV4,
                                                       *((UINT4 *) args[0]));
            break;

        case IGS_NO_VLAN_ROBUSTNESS_VALUE:
            i4RetStatus = SnoopVlanSetRobustnessValue (CliHandle, u4ContextId,
                                                       VlanId,
                                                       SNOOP_ADDR_TYPE_IPV4,
                                                       SNOOP_ROBUST_MIN_VALUE);
            break;

        case IGS_VLAN_MVLAN_NO_PROFILE:

            i4RetStatus =
                SnoopCliUnMapMVlanFromProfile (CliHandle, u4ContextId, VlanId,
                                               SNOOP_ADDR_TYPE_IPV4);
            break;

        case IGS_TRACE_ENABLE:

            if (args[0] != NULL)
            {
                if (SnoopVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = SNOOP_DEFAULT_INSTANCE;
            }

            i4RetStatus = SnoopCliSetTrace (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            CLI_PTR_TO_I4 (args[1]));

            break;
        case IGS_DEBUG_ENABLE:

            if (args[0] != NULL)
            {
                if (SnoopVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = SNOOP_DEFAULT_INSTANCE;
            }

            i4RetStatus = SnoopCliSetDebug (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            CLI_PTR_TO_I4 (args[1]));

            break;

        case SNOOP_PORT_LEAVE_CONFIGS:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgLeaveMode (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 CLI_PTR_TO_I4 (args[0]),
                                                 InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgLeaveMode (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 CLI_PTR_TO_I4 (args[0]),
                                                 InnerVlanId);
            }
            break;
        case SNOOP_PORT_RATE_LIMIT:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgRateLimit (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 *args[0], InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgRateLimit (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 *args[0], InnerVlanId);
            }
            break;
        case SNOOP_NO_PORT_RATE_LIMIT:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgRateLimit (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_PORT_CFG_RATE_LMT_DEF,
                                                 InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgRateLimit (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_PORT_CFG_RATE_LMT_DEF,
                                                 InnerVlanId);
            }
            break;
        case SNOOP_PORT_MAX_LIMIT:
            if (args[2] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[2]);
                i4RetStatus =
                    SnoopSetCliPortCfgMaxLimit (CliHandle, u4IfIndex,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                (UINT1) CLI_PTR_TO_I4 (args[0]),
                                                *((INT4 *) (args[1])),
                                                InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgMaxLimit (CliHandle, u4IfIndex,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                (UINT1) CLI_PTR_TO_I4 (args[0]),
                                                *((INT4 *) (args[1])),
                                                InnerVlanId);
            }
            break;
        case SNOOP_NO_PORT_MAX_LIMIT:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgMaxLimit (CliHandle, u4IfIndex,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_PORT_LMT_TYPE_NONE,
                                                SNOOP_DEF_MCAST_THRESH_LIMIT,
                                                InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgMaxLimit (CliHandle, u4IfIndex,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                SNOOP_PORT_LMT_TYPE_NONE,
                                                SNOOP_DEF_MCAST_THRESH_LIMIT,
                                                InnerVlanId);
            }
            break;
        case SNOOP_PORT_PROFILE_ID:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgProfileId (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 *(UINT4 *) (args[0]),
                                                 InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgProfileId (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 *(UINT4 *) (args[0]),
                                                 InnerVlanId);
            }
            break;
        case SNOOP_NO_PORT_PROFILE_ID:
            if (args[1] != NULL)
            {
                SNOOP_INNER_VLAN (InnerVlanId) =
                    (tSnoopVlanId) * ((UINT4 *) args[1]);
                i4RetStatus =
                    SnoopSetCliPortCfgProfileId (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_PORT_CFG_PROFILE_ID_DEF,
                                                 InnerVlanId);
            }
            else
            {
                SNOOP_INNER_VLAN (InnerVlanId) = 0;
                i4RetStatus =
                    SnoopSetCliPortCfgProfileId (CliHandle, u4IfIndex,
                                                 SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_PORT_CFG_PROFILE_ID_DEF,
                                                 InnerVlanId);
            }
            break;
        case IGS_MVLAN_ENABLE:

            i4RetStatus = SnoopCliSetMVlanStatus (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV4,
                                                  SNOOP_ENABLE);
            break;

        case IGS_MVLAN_DISABLE:

            i4RetStatus = SnoopCliSetMVlanStatus (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV4,
                                                  SNOOP_DISABLE);
            break;
        case IGS_FILTER_ENABLE:

            i4RetStatus = SnoopCliSetFilterStatus
                (CliHandle, u4ContextId, SNOOP_ADDR_TYPE_IPV4, SNOOP_ENABLE);
            break;

        case IGS_FILTER_DISABLE:

            i4RetStatus = SnoopCliSetFilterStatus
                (CliHandle, u4ContextId, SNOOP_ADDR_TYPE_IPV4, SNOOP_DISABLE);
            break;

        case IGS_VLAN_ADD_STATIC_GRP_ENTRY:

            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the IfXtype       */
            /* args[2] -> contains the PortList      */
            /* args[3] -> contains the srclist       */

            if (args[0] != NULL)
            {
                SNOOP_MEM_CPY (au1GroupAddr, (UINT1 *) args[0],
                               SNOOP_IP_ADDR_SIZE);
            }
            if (args[1] != NULL)
            {

                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[1],
                                                    (INT1 *) args[2],
                                                    (UINT1 *) *pMemberPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);

                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }

            i4RetStatus = SnoopCliVlanSetStaticMcastGrpEntry (CliHandle,
                                                              u4ContextId,
                                                              VlanId,
                                                              SNOOP_ADDR_TYPE_IPV4,
                                                              au1GroupAddr,
                                                              (UINT1 *)
                                                              *pMemberPorts);
            break;

        case IGS_VLAN_REMOVE_STATIC_GRP_ENTRY:

            /* args[0] -> contains the Group Address */
            /* args[1] -> contains the Src Address   */

            /*Taking address in the variables */
            SNOOP_MEM_CPY (au1GroupAddr, (UINT1 *) args[0], SNOOP_IP_ADDR_SIZE);
            pau1PortList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
            if (pau1PortList == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG,
                           SNOOP_GRP_DBG,
                           "cli_process_snoop_cmd: "
                           "Error in allocating memory for pau1PortList\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "cli_process_snoop_cmd: "
                           "Error in allocating memory for pau1PortList\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                break;
            }
            MEMSET (pau1PortList, 0, SNOOP_PORT_LIST_SIZE);

            if (args[1] != NULL)
            {
                i4RetStatus = CfaCliGetIfList ((INT1 *) args[1],
                                               (INT1 *) args[2],
                                               (UINT1 *) pau1PortList,
                                               SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pau1PortList);
                    break;
                }
            }

            i4RetStatus = SnoopCliVlanDelStaticMcastGrpEntry (CliHandle,
                                                              u4ContextId,
                                                              VlanId,
                                                              SNOOP_ADDR_TYPE_IPV4,
                                                              au1GroupAddr,
                                                              pau1PortList);
            FsUtilReleaseBitList ((UINT1 *) pau1PortList);
            break;

#endif

#ifdef MLDS_WANTED

        case MLDS_SYS_ENABLE:
            i4RetStatus = SnoopCliSetModuleStatus (CliHandle, u4ContextId,
                                                   SNOOP_ADDR_TYPE_IPV6,
                                                   SNOOP_ENABLE);
            break;

        case MLDS_SYS_DISABLE:
            i4RetStatus = SnoopCliSetModuleStatus (CliHandle, u4ContextId,
                                                   SNOOP_ADDR_TYPE_IPV6,
                                                   SNOOP_DISABLE);
            break;
        case MLDS_SEND_QUERY_ENABLE:
            i4RetStatus = SnoopCliSetSendQuery (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV6,
                                                SNOOP_ENABLE);

            break;
        case MLDS_SEND_QUERY_DISABLE:
            i4RetStatus = SnoopCliSetSendQuery (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV6,
                                                SNOOP_DISABLE);
            break;

        case MLDS_VLAN_STATS_CLEAR_COUNTERS:

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliVlanResetStatistics (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 (UINT2) *args[0]);
            }
            else
            {
                i4RetStatus =
                    SnoopCliVlanResetStatistics (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 SNOOP_INVALID_VLAN_ID);
            }

            break;
        case MLDS_PROXY_ENABLE:
            i4RetStatus =
                SnoopCliSetProxyStatus (CliHandle, u4ContextId,
                                        SNOOP_ADDR_TYPE_IPV6, SNOOP_ENABLE);
            break;

        case MLDS_PROXY_DISABLE:
            i4RetStatus =
                SnoopCliSetProxyStatus (CliHandle, u4ContextId,
                                        SNOOP_ADDR_TYPE_IPV6, SNOOP_DISABLE);
            break;

        case MLDS_PROXY_REPORTING_ENABLE:
            i4RetStatus =
                SnoopCliSetProxyReportingStatus (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 SNOOP_ENABLE);
            break;

        case MLDS_PROXY_REPORTING_DISABLE:
            i4RetStatus =
                SnoopCliSetProxyReportingStatus (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 SNOOP_DISABLE);
            break;

        case MLDS_ROUTER_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetRtrPortPurgeInterval (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 *args[0]);
            break;

        case MLDS_NO_ROUTER_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetRtrPortPurgeInterval (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 SNOOP_DEF_RTR_PURGE_INTERVAL);
            break;

        case MLDS_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetPortPurgeInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV6, *args[0]);
            break;

        case MLDS_NO_PORT_PURGE_INTERVAL:
            i4RetStatus =
                SnoopCliSetPortPurgeInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV6,
                                              SNOOP_DEF_PORT_PURGE_INTERVAL);
            break;

        case MLDS_REPORT_SUPPRESS_INTERVAL:
            i4RetStatus =
                SnoopCliSetReportFwdInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV6, *args[0]);
            break;

        case MLDS_NO_REPORT_SUPPRESS_INTERVAL:
            i4RetStatus =
                SnoopCliSetReportFwdInterval (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV6,
                                              SNOOP_DEF_REPORT_FWD_INTERVAL);
            break;

        case MLDS_RETRY_COUNT:
            i4RetStatus =
                SnoopCliSetRetryCount (CliHandle, u4ContextId,
                                       SNOOP_ADDR_TYPE_IPV6, (UINT1) *args[0]);
            break;

        case MLDS_NO_RETRY_COUNT:
            i4RetStatus =
                SnoopCliSetRetryCount (CliHandle, u4ContextId,
                                       SNOOP_ADDR_TYPE_IPV6,
                                       SNOOP_DEF_RETRY_COUNT);
            break;

        case MLDS_GROUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetGrpQueryInterval (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV6, *args[0]);
            break;

        case MLDS_NO_GROUP_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetGrpQueryInterval (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV6,
                                             SNOOP_DEF_GRP_QUERY_INTERVAL);
            break;

        case MLDS_REPORT_FORWADING:
            i4RetStatus =
                SnoopCliSetReportForwardAll (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV6,
                                             CLI_PTR_TO_I4 (args[0]));
            break;

        case MLDS_NO_REPORT_FORWADING:
            i4RetStatus =
                SnoopCliSetReportForwardAll (CliHandle, u4ContextId,
                                             SNOOP_ADDR_TYPE_IPV6,
                                             SNOOP_FORWARD_RTR_PORTS);
            break;

        case MLDS_VLAN_ENABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopModuleStatus (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                  SNOOP_ENABLE);
            break;

        case MLDS_VLAN_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopModuleStatus (CliHandle, u4ContextId,
                                                  VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                  SNOOP_DISABLE);
            break;

        case MLDS_VLAN_OPER_VERSION:
            i4RetStatus =
                SnoopCliSetVlanSnoopOperVersion (CliHandle, u4ContextId,
                                                 VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;

        case MLDS_VLAN_FAST_LEAVE_ENABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopFastLeave (CliHandle, u4ContextId,
                                               VlanId, SNOOP_ADDR_TYPE_IPV6,
                                               SNOOP_ENABLE);
            break;

        case MLDS_VLAN_FAST_LEAVE_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopFastLeave (CliHandle, u4ContextId,
                                               VlanId, SNOOP_ADDR_TYPE_IPV6,
                                               SNOOP_DISABLE);
            break;

        case MLDS_VLAN_QUERIER_ENABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopQuerier (CliHandle, u4ContextId,
                                             VlanId, SNOOP_ADDR_TYPE_IPV6,
                                             u1IpFlag, u4IpAddr, SNOOP_ENABLE);
            break;

        case MLDS_VLAN_QUERIER_DISABLE:
            i4RetStatus =
                SnoopCliSetVlanSnoopQuerier (CliHandle, u4ContextId,
                                             VlanId, SNOOP_ADDR_TYPE_IPV6,
                                             u1IpFlag, u4IpAddr, SNOOP_DISABLE);
            break;

        case MLDS_VLAN_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanQueryInterval (CliHandle, u4ContextId,
                                              VlanId, SNOOP_ADDR_TYPE_IPV6,
                                              *args[0]);
            break;

        case MLDS_NO_VLAN_QUERY_INTERVAL:
            i4RetStatus =
                SnoopCliSetVlanQueryInterval (CliHandle, u4ContextId,
                                              VlanId, SNOOP_ADDR_TYPE_IPV6,
                                              SNOOP_DEF_QUERY_INTERVAL);
            break;

        case MLDS_VLAN_MAX_RESP_CODE:
            i4RetStatus =
                SnoopCliSetVlanMaxResponseCode (CliHandle, (INT4) u4ContextId,
                                                VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                *((INT4 *) args[0]));
            break;

        case MLDS_NO_VLAN_MAX_RESP_CODE:
            i4RetStatus =
                SnoopCliSetVlanMaxResponseCode (CliHandle, (INT4) u4ContextId,
                                                VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                SNOOP_MLDS_DEF_MAX_RESP_CODE);
            break;

        case MLDS_VLAN_RTR_PORT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRouterPorts (CliHandle, u4ContextId,
                                            VlanId, SNOOP_ADDR_TYPE_IPV6,
                                            (UINT1 *) *pRouterPorts);
            break;

        case MLDS_VLAN_NO_RTR_PORT:
            /* Check for the invalid router port list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliDelVlanRouterPorts (CliHandle, u4ContextId,
                                            VlanId, SNOOP_ADDR_TYPE_IPV6,
                                            (UINT1 *) *pRouterPorts);
            break;

        case MLDS_RTR_PORT_PURGE_INT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortPurgeInterval (CliHandle,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV6,
                                                     (UINT1 *) *pRouterPorts,
                                                     *((INT4 *) args[2]));
            break;

        case MLDS_RTR_PORT_NO_PURGE_INT:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortPurgeInterval (CliHandle,
                                                     VlanId,
                                                     SNOOP_ADDR_TYPE_IPV6,
                                                     (UINT1 *) *pRouterPorts,
                                                     SNOOP_DEF_RTR_PURGE_INTERVAL);
            break;

        case MLDS_RTR_PORT_OPER_VERSION:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortOperVersion (CliHandle,
                                                   VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                   (UINT1 *) *pRouterPorts,
                                                   CLI_PTR_TO_I4 (args[2]));
            break;

        case MLDS_RTR_PORT_NO_OPER_VERSION:
            /* Check for the invalid router ports list */
            if (args[1] != NULL)
            {
                i4RetStatus = SnoopCfaCliGetIfList ((INT1 *) args[0],
                                                    (INT1 *) args[1],
                                                    (UINT1 *) *pRouterPorts,
                                                    SNOOP_IF_PORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\rInvalid Port List\r\n");
                    break;
                }
            }
            i4RetStatus =
                SnoopCliSetVlanRtrPortOperVersion (CliHandle,
                                                   VlanId, SNOOP_ADDR_TYPE_IPV6,
                                                   (UINT1 *) *pRouterPorts,
                                                   SNOOP_MLD_VERSION2);
            break;
        case MLDS_TRACE_ENABLE:

            if (args[0] != NULL)
            {
                if (SnoopVcmIsSwitchExist ((UINT1 *)
                                           args[0], &u4ContextId) != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = SNOOP_DEFAULT_INSTANCE;
            }

            i4RetStatus = SnoopCliSetTrace (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV6,
                                            CLI_PTR_TO_I4 (args[1]));
            break;

        case MLDS_DEBUG_ENABLE:

            if (args[0] != NULL)
            {
                if (SnoopVcmIsSwitchExist ((UINT1 *)
                                           args[0], &u4ContextId) != VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = SNOOP_DEFAULT_INSTANCE;
            }

            i4RetStatus = SnoopCliSetDebug (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV6,
                                            CLI_PTR_TO_I4 (args[1]));
            break;
#endif
        default:
            CliPrintf (CliHandle, "%%Command Not Supported\r\n");
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SNOOP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", SnoopCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);
    FsUtilReleaseBitList ((UINT1 *) pRouterPorts);
    FsUtilReleaseBitList ((UINT1 *) pMemberPorts);

    SNOOP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : cli_process_snoop_Show_cmd                         */
/*                                                                           */
/*     DESCRIPTION      : This function takes in variable no. of arguments   */
/*                        and process the commands for the SNOOP module as   */
/*                        defined in snoopcmd.def                            */
/*                                                                           */
/*     INPUT            : CliHandle -  CLIHandler                            */
/*                        u4Command -  Command Identifier                    */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
INT4
cli_process_snoop_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[SNOOP_CLI_MAX_ARGS];
    UINT4               u4ErrCode = 0;
    UINT4               u4IfIndex = 0;
    UINT4              *pu4Inst = NULL;
    UINT4               u4ContextId = SNOOP_CLI_INVALID_CONTEXT;
    UINT4               u4TmpContextId = SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4EntryType = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    tSnoopTag           VlanId;
#ifdef IGS_WANTED
    UINT2               u2VlanId = SNOOP_INVALID_VLAN_ID;
#endif
    INT1                argno = 0;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1              *pu1ContextName = NULL;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];

    /* If SNOOP is not Started, then all the SNOOP/MLDS commands are considered 
     * as invalid except Start/Enable command. */

    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (gNullGrpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    va_start (ap, u4Command);

    /* Third arguement is always interface name/index */

    pu4Inst = va_arg (ap, UINT4 *);

    if (pu4Inst != NULL)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu4Inst);
    }
    else
    {
        u4IfIndex = (UINT4) CLI_GET_IFINDEX ();
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     *       After the u4IfIndex. (ie) In all the cli commands we 
     *       are passing IfIndex as the first argument in 
     *       variable argument list. Like that as the second argument
     *       we have to pass context-name, In SI case this will be alwayes NULL, 
     *       so only one context will exists. */
    pu1ContextName = va_arg (ap, UINT1 *);

    if (pu1ContextName != NULL)
    {
        if (SnoopVcmIsSwitchExist (pu1ContextName, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "\r%%Switch %s Does not exist.\r\n",
                       pu1ContextName);
            va_end (ap);
            return SNOOP_FAILURE;
        }
        if (u4ContextId >= SNOOP_MAX_INSTANCES)
        {
            va_end (ap);
            return CLI_FAILURE;
        }
        if (SNOOP_SYSTEM_STATUS (u4ContextId) == SNOOP_SHUTDOWN)
        {
            CliPrintf (CliHandle, "\r%% Snooping is Shutdown\r\n");
            va_end (ap);
            return CLI_FAILURE;
        }
    }

    if (u4ContextId == SNOOP_CLI_INVALID_CONTEXT)
    {
        if (SnoopVcmGetSystemModeExt (SNOOP_PROTOCOL_ID) == VCM_SI_MODE)
        {
            /* If context ID is invalid make it as default context */
            u4ContextId = 0;
            if (SNOOP_SYSTEM_STATUS (u4ContextId) == SNOOP_SHUTDOWN)
            {
                CliPrintf (CliHandle, "\r%% Snooping is Shutdown\r\n");
                va_end (ap);
                return CLI_FAILURE;
            }
        }

        else
        {

            /* if u4ContextId is INVALID, then check for SNOOPING MODULE STATUS for all VALID contexts */
            for (u4TmpContextId = 0;
                 u4TmpContextId <= (SNOOP_MAX_INSTANCES - 1); u4TmpContextId++)
            {
                if (SNOOP_INSTANCE_INFO (u4TmpContextId) == NULL)
                {
                    continue;
                }

                if (SNOOP_SYSTEM_STATUS (u4TmpContextId) == SNOOP_SHUTDOWN)
                {
                    if (SnoopGetSwitchName ((INT4) u4TmpContextId,
                                            (INT4) SNOOP_CLI_INVALID_CONTEXT,
                                            au1TempContextName) ==
                        SNOOP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
                        CliPrintf (CliHandle, "\r%% Snooping is Shutdown\r\n");
                    }
                }
            }
        }
    }
    else
    {
        u4TmpContextId = u4ContextId;

        if ((u4TmpContextId < SNOOP_MAX_INSTANCES)
            && (SNOOP_SYSTEM_STATUS (u4TmpContextId) == SNOOP_SHUTDOWN))
        {
            CliPrintf (CliHandle, "\r%% Snooping is Shutdown\r\n");
            va_end (ap);
            return CLI_FAILURE;
        }
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store SNOOP_CLI_MAX_ARGS arguements at the max. */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == SNOOP_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);

    if (u4Command == IGS_SHOW_FWD_ENTRIES)
    {
        /*To Display the summary of forwarding table */
        if (args[1] != NULL)
        {
            i4RetStatus =
                SnoopCliShowForwardSummary (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4);

            return i4RetStatus;
        }
    }

    if (u4Command == IGS_SHOW_GRP_ENTRIES)
    {
        if ((CLI_PTR_TO_I4 (args[1])) == SNOOP_CLI_SHOW_SUMMARY)
        {
            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle,
                           "\r%% This command can not "
                           "be executed in Standby Card\r\n");
                return CLI_SUCCESS;
            }
            i4RetStatus =
                SnoopCliShowGroupSummary (CliHandle, SNOOP_DEFAULT_INSTANCE,
                                          SNOOP_ADDR_TYPE_IPV4);
            return i4RetStatus;
        }
    }

    CliRegisterLock (CliHandle, SnpLock, SnpUnLock);

    SNOOP_LOCK ();

    switch (u4Command)
    {
#ifdef IGS_WANTED

        case IGS_SHOW_VLAN_RTR_PORTS:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle, "\r%% This CLI command"
                           " is not supported in standby node\r\n");
                break;
            }

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                 (INT4) SNOOP_ADDR_TYPE_IPV4,
                                                 (UINT2) (*args[0]),
                                                 CLI_PTR_TO_I4 (args[1]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                 (INT4) SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_INVALID_VLAN_ID,
                                                 CLI_PTR_TO_I4 (args[1]));
            }
            break;

        case IGS_SHOW_VLAN_BLOCKED_RTR_PORTS:
            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowVlanBlkRtrPorts (CliHandle, u4ContextId,
                                                 (INT4) SNOOP_ADDR_TYPE_IPV4,
                                                 (UINT2) (*args[0]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowVlanBlkRtrPorts (CliHandle, u4ContextId,
                                                 (INT4) SNOOP_ADDR_TYPE_IPV4,
                                                 SNOOP_INVALID_VLAN_ID);
            }
            break;

        case IGS_RED_SHOW_VLAN_RTR_PORTS:

#ifdef L2RED_WANTED
            if (SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE)
            {
                if (args[0] != NULL)
                {
                    i4RetStatus =
                        SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                     (INT4)
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     (UINT2) (*args[0]),
                                                     CLI_PTR_TO_I4 (args[1]));
                }
                else
                {
                    i4RetStatus =
                        SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                     (INT4)
                                                     SNOOP_ADDR_TYPE_IPV4,
                                                     SNOOP_INVALID_VLAN_ID,
                                                     CLI_PTR_TO_I4 (args[1]));
                }
            }
#endif
            break;

        case IGS_SHOW_GLOBALS_INFO:

            i4RetStatus = SnoopCliShowGlobalInfo (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV4);
            break;

        case IGS_SHOW_VLAN_INFO:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle, "\r%% This CLI command is not"
                           " supported in standby node\r\n");
                break;

            }

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                          SNOOP_ADDR_TYPE_IPV4,
                                          (UINT2) (*args[0]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                          SNOOP_ADDR_TYPE_IPV4,
                                          SNOOP_INVALID_VLAN_ID);
            }
            break;

        case IGS_RED_SHOW_VLAN_INFO:

#ifdef L2RED_WANTED
            if (SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE)
            {
                if (args[0] != NULL)
                {
                    i4RetStatus =
                        SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4,
                                              (UINT2) (*args[0]));
                }
                else
                {
                    i4RetStatus =
                        SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                              SNOOP_ADDR_TYPE_IPV4,
                                              SNOOP_INVALID_VLAN_ID);
                }
            }
#endif
            break;

        case IGS_SHOW_GRP_ENTRIES:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle, "\r%% This CLI command is not"
                           " supported in standby node\r\n");
                break;
            }

            if (args[0] != NULL)
            {
                u2VlanId = (UINT2) (*args[0]);
            }
            if (args[2] != NULL)
            {
                i4EntryType = SNOOP_GRP_STATIC;
            }
            else if (args[3] != NULL)
            {
                i4EntryType = SNOOP_GRP_DYNAMIC;
            }

            if (args[1] != NULL)
            {
                SNOOP_MEM_CPY (au1GroupAddr, (UINT1 *) args[1],
                               SNOOP_IP_ADDR_SIZE);

                i4RetStatus =
                    SnoopCliShowGroupInformation (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV4,
                                                  u2VlanId,
                                                  au1GroupAddr, i4EntryType);

            }
            else
            {
                i4RetStatus =
                    SnoopCliShowGroupInformation (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV4,
                                                  u2VlanId,
                                                  (UINT1 *) gNullGrpAddr,
                                                  i4EntryType);
            }
            break;

        case IGS_SHOW_FWD_ENTRIES:

            if (args[0] != NULL)
            {
                u2VlanId = (UINT2) (*args[0]);
            }

            if (args[1] != NULL)
            {
                i4EntryType = SNOOP_GRP_STATIC;
            }
            else if (args[2] != NULL)
            {
                i4EntryType = SNOOP_GRP_DYNAMIC;
            }

            i4RetStatus =
                SnoopCliShowForwardEntries (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            u2VlanId, i4EntryType);
            break;

        case IGS_RED_SHOW_FWD_ENTRIES:

#ifdef L2RED_WANTED
            if (SNOOP_NODE_STATUS () != SNOOP_ACTIVE_NODE)
            {
                if (args[0] != NULL)
                {
                    u2VlanId = (UINT2) (*args[0]);
                }

                if (args[1] != NULL)
                {
                    i4EntryType = SNOOP_GRP_STATIC;
                }
                else if (args[2] != NULL)
                {
                    i4EntryType = SNOOP_GRP_DYNAMIC;
                }

                i4RetStatus =
                    SnoopCliShowForwardEntries (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV4,
                                                u2VlanId, i4EntryType);

            }
#endif
            break;

        case IGS_SHOW_STATS:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle, "\r%% This CLI command is not"
                           " supported in standby node\r\n");
                break;
            }

            if (args[0] != NULL)
            {
                i4RetStatus = SnoopCliShowStatistics (CliHandle, u4ContextId,
                                                      SNOOP_ADDR_TYPE_IPV4,
                                                      (tVlanId) (*args[0]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowStatistics (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV4,
                                            SNOOP_INVALID_VLAN_ID);
            }

            break;

        case IGS_SHOW_HOST_ENTRIES:

            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                CliPrintf (CliHandle,
                           "\r%% This CLI command is not"
                           " supported in standby node\r\n");
                break;
            }

            if (args[0] != NULL)
            {
                if (args[1] != NULL)
                {
                    SNOOP_MEM_CPY (au1GroupAddr, (UINT1 *) args[1],
                                   SNOOP_IP_ADDR_SIZE);

                    i4RetStatus = SnoopCliShowMcastReceiverInfo
                        (CliHandle, u4ContextId, SNOOP_ADDR_TYPE_IPV4,
                         ((UINT2) (*args[0])), au1GroupAddr);
                }
                else
                {
                    i4RetStatus = SnoopCliShowMcastReceiverInfo
                        (CliHandle, u4ContextId, SNOOP_ADDR_TYPE_IPV4,
                         ((UINT2) (*args[0])), (UINT1 *) gNullGrpAddr);
                }
            }
            else
            {
                i4RetStatus = SnoopCliShowMcastReceiverInfo
                    (CliHandle, u4ContextId, SNOOP_ADDR_TYPE_IPV4,
                     SNOOP_INVALID_VLAN_ID, (UINT1 *) gNullGrpAddr);
            }

            break;

        case SNOOP_SHOW_PORT_CFG_ENTRIES:

            if (args[0] != NULL)
            {
                SNOOP_INNER_VLAN (VlanId) = (tSnoopVlanId) * args[0];
            }
            else
            {
                SNOOP_INNER_VLAN (VlanId) = SNOOP_PORT_CFG_INNER_VLAN_ID;
            }

            i4RetStatus =
                SnoopCliShowPortCfgEntries (CliHandle,
                                            u4ContextId,
                                            u4IfIndex,
                                            SNOOP_ADDR_TYPE_IPV4, VlanId);
            break;
        case IGS_SHOW_MVLAN_MAPPING:

            i4RetStatus =
                SnoopCliShowMVlanMapping (CliHandle, u4ContextId,
                                          SNOOP_ADDR_TYPE_IPV4);

            break;

#endif

#ifdef MLDS_WANTED

        case MLDS_SHOW_VLAN_RTR_PORTS:
            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 (UINT2) (*args[0]),
                                                 CLI_PTR_TO_I4 (args[1]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowVlanRouterPorts (CliHandle, u4ContextId,
                                                 SNOOP_ADDR_TYPE_IPV6,
                                                 SNOOP_INVALID_VLAN_ID,
                                                 CLI_PTR_TO_I4 (args[1]));
            }
            break;

        case MLDS_SHOW_GLOBALS_INFO:

            i4RetStatus = SnoopCliShowGlobalInfo (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV6);
            break;

        case MLDS_SHOW_VLAN_INFO:

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                          SNOOP_ADDR_TYPE_IPV6,
                                          (UINT2) (*args[0]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowVlanInfo (CliHandle, u4ContextId,
                                          SNOOP_ADDR_TYPE_IPV6,
                                          SNOOP_INVALID_VLAN_ID);
            }
            break;

        case MLDS_SHOW_GRP_ENTRIES:

            if (args[0] != NULL)
            {
                if (args[1] != NULL)
                {
                    /* get the string in args[1], remove ":" or "." from the
                       addess string and copy it to the variable below */
                    if (INET_ATON6 (args[1], au1GroupAddr) == 0)
                    {
                        CLI_SET_ERR (CLI_SNOOP_INVALID_ADDRESS_ERR);
                        break;
                    }

                    SNOOP_INET_NTOHL (au1GroupAddr);

                    i4RetStatus =
                        SnoopCliShowGroupInformation (CliHandle,
                                                      u4ContextId,
                                                      SNOOP_ADDR_TYPE_IPV6,
                                                      ((UINT2) (*args[0])),
                                                      ((UINT1 *) au1GroupAddr),
                                                      i4EntryType);
                }
                else
                {
                    i4RetStatus =
                        SnoopCliShowGroupInformation (CliHandle,
                                                      u4ContextId,
                                                      SNOOP_ADDR_TYPE_IPV6,
                                                      ((UINT2) (*args[0])),
                                                      (UINT1 *) gNullGrpAddr,
                                                      i4EntryType);
                }
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowGroupInformation (CliHandle, u4ContextId,
                                                  SNOOP_ADDR_TYPE_IPV6,
                                                  SNOOP_INVALID_VLAN_ID,
                                                  (UINT1 *) gNullGrpAddr,
                                                  i4EntryType);
            }
            break;

        case MLDS_SHOW_FWD_ENTRIES:

            if (args[0] != NULL)
            {
                i4RetStatus =
                    SnoopCliShowForwardEntries (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV6,
                                                ((UINT2) (*args[0])),
                                                i4EntryType);
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowForwardEntries (CliHandle, u4ContextId,
                                                SNOOP_ADDR_TYPE_IPV6,
                                                SNOOP_INVALID_VLAN_ID,
                                                i4EntryType);
            }

            break;

        case MLDS_SHOW_STATS:

            if (args[0] != NULL)
            {
                i4RetStatus = SnoopCliShowStatistics (CliHandle, u4ContextId,
                                                      SNOOP_ADDR_TYPE_IPV6,
                                                      (UINT2) (*args[0]));
            }
            else
            {
                i4RetStatus =
                    SnoopCliShowStatistics (CliHandle, u4ContextId,
                                            SNOOP_ADDR_TYPE_IPV6,
                                            SNOOP_INVALID_VLAN_ID);
            }

            break;

#endif
        default:
            CliPrintf (CliHandle, "%%Command Not Supported\r\n");
            break;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_SNOOP_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", SnoopCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    SNOOP_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetSystemControl                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Snoop System Control        */
/*                                                                           */
/*     INPUT            : i4SnoopStatus - SNOOP_START/SNOOP_SHUT             */
/*                        CliHandle  - CLI Handler                           */
/*                        u4ContextId - Instance Id                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopInstanceGlobalSystemControl (&u4ErrCode,
                                                     (INT4) u4ContextId,
                                                     i4SnoopStatus) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopInstanceGlobalSystemControl
        ((INT4) u4ContextId, i4SnoopStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetMcastFwdMode                            */
/*                                                                           */
/*     DESCRIPTION      : This function configures multicast forwarding mode */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4McastFwdMode - SNOOP_MAC_FWD_MODE/               */
/*                                         SNOOP_IP_FWD_MODE                 */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetMcastFwdMode (tCliHandle CliHandle, INT4 i4InstId,
                         INT4 i4McastFwdMode)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopInstanceGlobalMcastFwdMode (&u4ErrCode,
                                                    i4InstId,
                                                    i4McastFwdMode) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopInstanceGlobalMcastFwdMode (i4InstId, i4McastFwdMode)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    /* If multicast mode is IP, by default control plane approach is disabled */
    if (i4McastFwdMode == SNOOP_MCAST_FWD_MODE_IP)
    {
        if (nmhTestv2FsSnoopInstanceControlPlaneDriven (&u4ErrCode,
                                                        i4InstId,
                                                        SNOOP_DISABLE) ==
            SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }

        if (nmhSetFsSnoopInstanceControlPlaneDriven (i4InstId, SNOOP_DISABLE)
            == SNMP_FAILURE)
        {
            return (CLI_FAILURE);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetSystemEnhMode                           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Snoop System Enhanced Mode  */
/*     INPUT            : CliHandle  - CLI Handler                          */
/*                        u4ContextId - Instance Id                          */
/*              u1AddrType -SNOOP_ADDR_TYPE_IPV4 /                 */
/*                                      SNOOP_ADDR_TYPE_IPV6                 */
/*                        i4SnoopSystemEnhMode - SNOOP_ENH_ENABLE /          */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetSystemEnhMode (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT1 u1AddrType, INT4 i4SnoopSystemEnhMode)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (u1AddrType);

    if (nmhTestv2FsSnoopInstanceGlobalEnhancedMode (&u4ErrCode,
                                                    (INT4) u4ContextId,
                                                    i4SnoopSystemEnhMode) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopInstanceGlobalEnhancedMode
        (u4ContextId, i4SnoopSystemEnhMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetSystemSparMode                          */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Snoop System Sparse Mode    */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4ContextId - Instance Id                          */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopSystemSparMode - SNOOP_SPAR_ENABLE /        */
/*                                                SNOOP_SPAR_DISABLE         */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetSystemSparseMode (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT1 u1AddrType, INT4 i4SnoopSystemSparMode)
{
    UINT4               u4ErrCode = 0;

    UNUSED_PARAM (u1AddrType);

    if (nmhTestv2FsSnoopInstanceGlobalSparseMode (&u4ErrCode,
                                                  (INT4) u4ContextId,
                                                  i4SnoopSystemSparMode) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopInstanceGlobalSparseMode
        (u4ContextId, i4SnoopSystemSparMode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetModuleStatus                            */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables IGS/MLDS module     */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4/                 */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetModuleStatus (tCliHandle CliHandle, INT4 i4InstId,
                         INT4 i4AddrType, INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsSnoopStatus (&u4ErrCode, i4InstId, i4AddrType, i4SnoopStatus)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopStatus (i4InstId, i4AddrType, i4SnoopStatus) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetSendQuery                               */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables the  transmission   */
/*                        of IGMP General Queries                            */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4/                 */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopQueryStatus - SNOOP_ENABLE/                 */
/*                                             SNOOP_DISABLE                 */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetSendQuery (tCliHandle CliHandle, INT4 i4InstId,
                      UINT1 u1AddressType, INT4 i4SnoopQueryStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopSendQueryOnTopoChange (&u4ErrCode, i4InstId,
                                               u1AddressType,
                                               i4SnoopQueryStatus) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopSendQueryOnTopoChange (i4InstId, u1AddressType,
                                            i4SnoopQueryStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetProxyStatus                             */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables Proxy for           */
/*                        the snooping module                                */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetProxyStatus (tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType,
                        INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopProxyStatus (&u4ErrCode, i4InstId, i4AddrType,
                                     i4SnoopStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopProxyStatus (i4InstId, i4AddrType, i4SnoopStatus)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetProxyReportingStatus                    */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables Proxy Reporting for */
/*                        the snooping module                                */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetProxyReportingStatus (tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4AddrType, INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopProxyReportingStatus (&u4ErrCode, i4InstId,
                                              i4AddrType, i4SnoopStatus)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopProxyReportingStatus (i4InstId, i4AddrType,
                                           i4SnoopStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetRtrPortPurgeInterval                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Router Port Purge interval  */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4Timeout  - Router Port Time Out Value            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetRtrPortPurgeInterval (tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopRouterPortPurgeInterval (&u4ErrCode, i4InstId,
                                                 i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopRouterPortPurgeInterval (i4InstId, i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetPortPurgeInterval                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Port Purge interval value   */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4Timeout  - Port Purge interval value             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetPortPurgeInterval (tCliHandle CliHandle, INT4 i4InstId,
                              INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopPortPurgeInterval (&u4ErrCode, i4InstId,
                                           i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopPortPurgeInterval (i4InstId, i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetReportFwdInterval                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets Report Forward interval value   */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4Timeout  - Report suppress interval value        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetReportFwdInterval (tCliHandle CliHandle, INT4 i4InstId,
                              INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsSnoopReportForwardInterval (&u4ErrCode, i4InstId,
                                               i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopReportForwardInterval (i4InstId, i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetRetryCount                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the maximum retry count value   */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u1RetryCount - Maximum retry count value           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetRetryCount (tCliHandle CliHandle, INT4 i4InstId,
                       INT4 i4AddrType, UINT1 u1RetryCount)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopRetryCount (&u4ErrCode, i4InstId, i4AddrType,
                                    u1RetryCount) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopRetryCount (i4InstId, i4AddrType, u1RetryCount)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetGrpQueryInterval                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Group specific query        */
/*                        interval value                                     */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4Timeout  - Group specific query interval value   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetGrpQueryInterval (tCliHandle CliHandle, INT4 i4InstId,
                             INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopGrpQueryInterval (&u4ErrCode, i4InstId, i4AddrType,
                                          i4Timeout) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopGrpQueryInterval (i4InstId, i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetReportForwardAll                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the report forward all flag     */
/*                        which forwards reports on all vlan memberports or  */
/*                        only on to the router ports.                       */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4FwdAll   - Forward flag                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetReportForwardAll (tCliHandle CliHandle, INT4 i4InstId,
                             INT4 i4AddrType, INT4 i4FwdAll)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopReportFwdOnAllPorts (&u4ErrCode, i4InstId, i4AddrType,
                                             i4FwdAll) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopReportFwdOnAllPorts (i4InstId, i4AddrType, i4FwdAll)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetQueryForwardAll                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the query forward all flag      */
/*                        which forwards query messages on all vlan member   */
/*                        ports or only on to the non-router ports.          */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4FwdAll   - Forward flag                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetQueryForwardAll (tCliHandle CliHandle, INT4 i4InstId,
                            INT4 i4AddrType, INT4 i4FwdAll)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopQueryFwdOnAllPorts (&u4ErrCode, i4InstId, i4AddrType,
                                            i4FwdAll) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopQueryFwdOnAllPorts (i4InstId, i4AddrType, i4FwdAll)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanSnoopModuleStatus                   */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables IGS/MLDS for a      */
/*                        specific VLAN.                                     */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4VlanId   - Vlan Identifier                       */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanSnoopModuleStatus (tCliHandle CliHandle, INT4 i4InstId,
                                  INT4 i4VlanId, INT4 i4AddrType,
                                  INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanSnoopStatus (&u4ErrCode, i4InstId, i4VlanId,
                                         i4AddrType, i4SnoopStatus)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanSnoopStatus (i4InstId, i4VlanId, i4AddrType,
                                      i4SnoopStatus) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanSnoopOperVersion                    */
/*                                                                           */
/*     DESCRIPTION      : This function sets the operating version for the   */
/*                        IGS/MLDS switch for a specific VLAN.               */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u1OperVersion - IGS_IGMP_VERSION1 /                */
/*                                        IGS_IGMP_VERSION2 /                */
/*                                        IGS_IGMP_VERSION3 /                */
/*                                        MLDS_MLD_VERSION1 /                */
/*                                        MLDS_MLD_VERSION2                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanSnoopOperVersion (tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4VlanId, INT4 i4AddrType,
                                 INT4 i4OperVersion)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanCfgOperVersion (&u4ErrCode, i4InstId, i4VlanId,
                                            i4AddrType, i4OperVersion)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanCfgOperVersion (i4InstId, i4VlanId, i4AddrType,
                                         i4OperVersion) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanSnoopFastLeave                      */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables fast leave          */
/*                        processing for a specific VLAN.                    */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4FastLeave- SNOOP_ENABLE/SNOOP_DISABLE            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanSnoopFastLeave (tCliHandle CliHandle, INT4 i4InstId,
                               INT4 i4VlanId, INT4 i4AddrType, INT4 i4FastLeave)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanFastLeave (&u4ErrCode, i4InstId, i4VlanId,
                                       i4AddrType, i4FastLeave) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanFastLeave (i4InstId, i4VlanId, i4AddrType, i4FastLeave)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanSnoopQuerier                        */
/*                                                                           */
/*     DESCRIPTION      : This function enables/disables querier feature for */
/*                        IGS/MLDS switch for a specific VLAN.               */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4Querier  - SNOOP_QUERIER/SNOOP_NON_QUERIER       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanSnoopQuerier (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                             INT4 i4AddrType, UINT1 u1Flag, UINT4 u4IpAddr,
                             INT4 i4SnoopStatus)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanCfgQuerier (&u4ErrCode, i4InstId, i4VlanId,
                                        i4AddrType,
                                        i4SnoopStatus) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanCfgQuerier (i4InstId, i4VlanId, i4AddrType,
                                     i4SnoopStatus) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*Test and set routine for vlan Querier Ip address configured by
     * the user*/
    if (nmhTestv2FsSnoopVlanQuerierIpAddress (&u4ErrCode, i4InstId, i4VlanId,
                                              i4AddrType,
                                              u4IpAddr) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;

    }
    if (nmhSetFsSnoopVlanQuerierIpAddress (i4InstId, i4VlanId, i4AddrType,
                                           u4IpAddr) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /* Test and set routine for vlan Querier Ip address flag*
     * If it is enabled and user has not configured the querier ip,
     * ip address configured on interface vlan will be
     * used in igmp query message */

    if (i4SnoopStatus == SNOOP_DISABLE)
    {
        u1Flag = (UINT1) SNOOP_QUERIER_IP_FLAG_DISABLE;
    }
    if (nmhTestv2FsSnoopVlanQuerierIpFlag (&u4ErrCode, i4InstId, i4VlanId,
                                           i4AddrType,
                                           (INT4) u1Flag) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    if (nmhSetFsSnoopVlanQuerierIpFlag (i4InstId, i4VlanId, i4AddrType,
                                        (INT4) u1Flag) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanQueryInterval                       */
/*                                                                           */
/*     DESCRIPTION      : This function sets the time period with which the  */
/*                        general queries are sent by the IGS/MLDS           */
/*                        switch when it is configured as querier for a VLAN */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanQueryInterval (tCliHandle CliHandle, INT4 i4InstId,
                              INT4 i4VlanId, INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanQueryInterval (&u4ErrCode, i4InstId, i4VlanId,
                                           i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanQueryInterval (i4InstId, i4VlanId, i4AddrType,
                                        i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanStartupQueryCount                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the time period with which the  */
/*                        general queries are sent by the IGS/MLDS           */
/*                        switch when it is configured as querier for a VLAN */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanStartupQueryCount (tCliHandle CliHandle, INT4 i4InstId,
                                  INT4 i4VlanId, INT4 i4AddrType,
                                  INT4 i4QueryCount)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanStartupQueryCount (&u4ErrCode, i4InstId, i4VlanId,
                                               i4AddrType, i4QueryCount)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanStartupQueryCount (i4InstId, i4VlanId, i4AddrType,
                                            i4QueryCount) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanStartupQueryInterval                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the time period with which the  */
/*                        general queries are sent by the IGS/MLDS           */
/*                        switch when it is configured as querier for a VLAN */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanStartupQueryInterval (tCliHandle CliHandle, INT4 i4InstId,
                                     INT4 i4VlanId, INT4 i4AddrType,
                                     INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanStartupQueryInterval
        (&u4ErrCode, i4InstId, i4VlanId, i4AddrType, i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CliPrintf (CliHandle,
                   "\r%% Invalid value, it should be <= (Query Interval/4) \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanStartupQueryInterval (i4InstId, i4VlanId, i4AddrType,
                                               i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanOtherQuerierPrsntInterval           */
/*                                                                           */
/*     DESCRIPTION      : This function sets the time period with which the  */
/*                        general queries are sent by the IGS/MLDS           */
/*                        switch when it is configured as querier for a VLAN */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanOtherQuerierPrsntInterval (tCliHandle CliHandle, INT4 i4InstId,
                                          INT4 i4VlanId, INT4 i4AddrType,
                                          INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanOtherQuerierPresentInterval
        (&u4ErrCode, i4InstId, i4VlanId, i4AddrType, i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CliPrintf (CliHandle, "\r%% Invalid value, it should be >= "
                   "((Robustness * Query Interval) + ((Max Response Code/10)/2))\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanOtherQuerierPresentInterval
        (i4InstId, i4VlanId, i4AddrType, i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanMaxResponseCode                     */
/*                                                                           */
/*     DESCRIPTION      : This function sets the time period within which    */
/*                        the query reply must be received.                  */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SnoopStatus - SNOOP_ENABLE/SNOOP_DISABLE         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanMaxResponseCode (tCliHandle CliHandle, INT4 i4InstId,
                                INT4 i4VlanId, INT4 i4AddrType, INT4 i4Timeout)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanMaxResponseTime (&u4ErrCode, i4InstId, i4VlanId,
                                             i4AddrType, i4Timeout)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanMaxResponseTime (i4InstId, i4VlanId, i4AddrType,
                                          i4Timeout) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanRouterPorts                         */
/*                                                                           */
/*     DESCRIPTION      : This function adds the router ports statically     */
/*                        for a VLAN.                                        */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        pu1RouterPorts - Contains router ports             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanRouterPorts (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                            INT4 i4AddrType, UINT1 *pu1RouterPorts)
{
    tSnoopIfPortBmp    *pRtrPortList = NULL;
    tSNMP_OCTET_STRING_TYPE RouterPortList;
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (pu1RouterPorts == NULL)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    RouterPortList.pu1_OctetList = pu1RouterPorts;
    RouterPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    /* Validate the configured port list */
    if (nmhTestv2FsSnoopVlanRtrPortList (&u4ErrCode, i4InstId, i4VlanId,
                                         i4AddrType, &RouterPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    pRtrPortList = (tSnoopIfPortBmp *) FsUtilAllocBitList
        (sizeof (tSnoopIfPortBmp));
    if (pRtrPortList == NULL)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET ((*pRtrPortList), 0, SNOOP_IF_PORT_LIST_SIZE);

    MEMCPY ((*pRtrPortList), pu1RouterPorts, SNOOP_IF_PORT_LIST_SIZE);
    nmhGetFsSnoopVlanRtrPortList (i4InstId, i4VlanId, i4AddrType,
                                  &RouterPortList);
    OSIX_ADD_PORT_LIST (RouterPortList.pu1_OctetList, (*pRtrPortList),
                        SNOOP_IF_PORT_LIST_SIZE, SNOOP_IF_PORT_LIST_SIZE);

    if (nmhTestv2FsSnoopVlanRtrPortList (&u4ErrCode, i4InstId, i4VlanId,
                                         i4AddrType, &RouterPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        return CLI_FAILURE;
    }
    if (nmhSetFsSnoopVlanRtrPortList (i4InstId, i4VlanId, i4AddrType,
                                      &RouterPortList) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        return CLI_FAILURE;
    }

    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetVlanBlockedRtrPorts                     */
/*                                                                           */
/*     DESCRIPTION      : This function adds the blocked router ports        */
/*                        statically for a VLAN.                             */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId   - Vlan Identifier                       */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        pu1RouterPorts - Contains blocked router ports     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetVlanBlockedRtrPorts (tCliHandle CliHandle, INT4 i4InstId,
                                INT4 i4VlanId, INT4 i4AddrType,
                                UINT1 *pu1RouterPorts)
{
    UINT1              *pau1LocalPortList = NULL;
    UINT1              *pTmpPortBitmap = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSNMP_OCTET_STRING_TYPE LocalPortList;
    UINT4               u4ErrCode = 0;
    UINT4               u4Instance = SNOOP_INIT_VAL;

    pTmpPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTmpPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pTmpPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESR, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pTmpPortBitmap, 0, sizeof (tSnoopPortBmp));

    pau1LocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1LocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1LocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        UtilPlstReleaseLocalPortList (pTmpPortBitmap);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pau1LocalPortList, SNOOP_INIT_VAL, SNOOP_PORT_LIST_SIZE);

    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;
    LocalPortList.pu1_OctetList = pau1LocalPortList;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        UtilPlstReleaseLocalPortList (pTmpPortBitmap);
        return CLI_FAILURE;
    }

    if (pu1RouterPorts == NULL)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        UtilPlstReleaseLocalPortList (pTmpPortBitmap);
        return CLI_FAILURE;
    }

    SnoopConvertToLocalPortList (pu1RouterPorts, pTmpPortBitmap);

    MEMCPY ((LocalPortList.pu1_OctetList), pTmpPortBitmap,
            SNOOP_PORT_LIST_SIZE);

    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    if (nmhTestv2FsSnoopVlanBlkRtrLocalPortList (&u4ErrCode, i4InstId, i4VlanId,
                                                 i4AddrType, &LocalPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        UtilPlstReleaseLocalPortList (pTmpPortBitmap);
        return CLI_FAILURE;
    }

    /* Add the configured port list with the existed blocked router port list 
       and do the validation */
    SNOOP_MEM_SET (pau1LocalPortList, SNOOP_INIT_VAL, SNOOP_PORT_LIST_SIZE);

    nmhGetFsSnoopVlanBlkRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                          &LocalPortList);

    OSIX_ADD_PORT_LIST (pau1LocalPortList, pTmpPortBitmap,
                        SNOOP_PORT_LIST_SIZE, SNOOP_PORT_LIST_SIZE)
        UtilPlstReleaseLocalPortList (pTmpPortBitmap);
    /* If SISP interfaces are present, then size of port list can extend beyond 
     * size of PortList expected by nmhSetFsSnoopVlanBlkRtrPortList. Hence,
     * invoke the new nmhs introduced
     * */
    u4Instance = (UINT4) SNOOP_GET_INSTANCE_ID (i4InstId);
    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
    if (pSnoopInstInfo == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME, "Instance not created \r\n");
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanBlkRtrLocalPortList (&u4ErrCode, i4InstId, i4VlanId,
                                                 i4AddrType, &LocalPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanBlkRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                              &LocalPortList) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1LocalPortList);
    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliDelVlanRouterPorts                         */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the statically configured    */
/*                        router ports for a VLAN.                           */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        pu1RouterPorts - Contains router ports             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliDelVlanRouterPorts (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                            INT4 i4AddrType, UINT1 *pu1RouterPorts)
{
    UINT1              *pau1TmpPortList = NULL;
    UINT1              *pTempPortBitmap = NULL;
    UINT1              *pPortBitmap = NULL;
    tSNMP_OCTET_STRING_TYPE LocalPortList;
    tSNMP_OCTET_STRING_TYPE LocalRouterPortList;
    tSnoopPortBmp      *pRtrPortList = NULL;
    UINT4               u4ErrCode = 0;
    UINT2               u2Index = 0;
    UINT4               u4Index = 0;

    pau1TmpPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1TmpPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1TmpPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return CLI_FAILURE;
    }
    MEMSET (pau1TmpPortList, 0, SNOOP_PORT_LIST_SIZE);

    SNOOP_MEM_SET (pau1TmpPortList, SNOOP_INIT_VAL, SNOOP_PORT_LIST_SIZE);

    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;
    LocalPortList.pu1_OctetList = pau1TmpPortList;

    pRtrPortList =
        (tSnoopPortBmp *) FsUtilAllocBitList (sizeof (tSnoopPortBmp));

    if (pRtrPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }

    LocalRouterPortList.i4_Length = SNOOP_PORT_LIST_SIZE;
    LocalRouterPortList.pu1_OctetList = *pRtrPortList;

    if (pu1RouterPorts == NULL)
    {
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }
    pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_TRC_CONTROL |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                       "Error in allocating memory for pTempPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));

    SnoopConvertToLocalPortList (pu1RouterPorts, pTempPortBitmap);

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }

    /* Get the existing router port list for a specific Vlan and delete the
     * configured ports from the existing port list, this port list
     * is set as router ports for the vlan */

    nmhGetFsSnoopVlanRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                       &LocalPortList);

    nmhGetFsSnoopVlanRouterLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                          &LocalRouterPortList);

    for (u2Index = 0; u2Index < SNOOP_PORT_LIST_SIZE; u2Index++)
    {
        LocalPortList.pu1_OctetList[u2Index] &=
            LocalRouterPortList.pu1_OctetList[u2Index];
    }
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_CPY (pPortBitmap, LocalPortList.pu1_OctetList,
                   SNOOP_PORT_LIST_SIZE);

    for (u4Index = 0; u4Index < SNOOP_PORT_LIST_SIZE; u4Index++)
    {
        pTempPortBitmap[u4Index] = (UINT1) (~pTempPortBitmap[u4Index]);
        pPortBitmap[u4Index] &= pTempPortBitmap[u4Index];
    }

    UtilPlstReleaseLocalPortList (pTempPortBitmap);
    SNOOP_MEM_CPY (LocalPortList.pu1_OctetList, pPortBitmap,
                   SNOOP_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    if (nmhTestv2FsSnoopVlanRtrLocalPortList (&u4ErrCode, i4InstId, i4VlanId,
                                              i4AddrType, &LocalPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                           &LocalPortList) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1TmpPortList);

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        return CLI_FAILURE;
    }

    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliDelVlanBlkRtrPorts                         */
/*                                                                           */
/*     DESCRIPTION      : This function deletes the statically configured    */
/*                        blocked router ports for a VLAN.                   */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId   - Vlan Identifier                       */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        pu1RouterPorts - Contains blocked router ports     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliDelVlanBlkRtrPorts (tCliHandle CliHandle, INT4 i4InstId,
                            INT4 i4VlanId, INT4 i4AddrType,
                            UINT1 *pu1RouterPorts)
{
    UINT1              *pTempPortBitmap = NULL;
    UINT1              *pPortBitmap = NULL;
    UINT1              *pau1LocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE LocalPortList;
    UINT4               u4ErrCode = 0;
    UINT1               u1Index = 0;

    pau1LocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1LocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_TMR | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_TMR_NAME,
                       "Error in allocating memory for pau1LocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pau1LocalPortList, SNOOP_INIT_VAL, SNOOP_PORT_LIST_SIZE);

    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;
    LocalPortList.pu1_OctetList = pau1LocalPortList;

    if (pu1RouterPorts == NULL)
    {
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }
    pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_TRC_CONTROL |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                       "Error in allocating memory for pTempPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));

    SnoopConvertToLocalPortList (pu1RouterPorts, pTempPortBitmap);

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }

    /* Get the existing blocked router port list for a specific Vlan and
       delete the configured ports from the existing port list, this
       port list is set as blocked router ports for the vlan */

    nmhGetFsSnoopVlanBlkRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                          &LocalPortList);
    pPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitmap == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (i4InstId),
                       SNOOP_DBG_GRP | SNOOP_TRC_CONTROL |
                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                       "Error in allocating memory for pPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_BUFF_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_CPY (pPortBitmap, LocalPortList.pu1_OctetList,
                   SNOOP_PORT_LIST_SIZE);

    for (u1Index = 0; u1Index < (UINT1) SNOOP_PORT_LIST_SIZE; u1Index++)
    {
        pTempPortBitmap[u1Index] = (UINT1) (~pTempPortBitmap[u1Index]);
        pPortBitmap[u1Index] &= pTempPortBitmap[u1Index];
    }

    UtilPlstReleaseLocalPortList (pTempPortBitmap);
    SNOOP_MEM_CPY (LocalPortList.pu1_OctetList, pPortBitmap,
                   SNOOP_PORT_LIST_SIZE);
    UtilPlstReleaseLocalPortList (pPortBitmap);
    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    /* If SISP interfaces are present, then size of port list can extend beyond 
     * size of PortList expected by nmhSetFsSnoopVlanBlkRtrPortList. Hence,
     * invoke the new nmhs introduced
     * */

    if (nmhTestv2FsSnoopVlanBlkRtrLocalPortList (&u4ErrCode, i4InstId, i4VlanId,
                                                 i4AddrType, &LocalPortList)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanBlkRtrLocalPortList (i4InstId, i4VlanId, i4AddrType,
                                              &LocalPortList) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_FAILURE;
    }
    UtilPlstReleaseLocalPortList (pau1LocalPortList);

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliMapMVlanToProfile                          */
/*                                                                           */
/*     DESCRIPTION      : This function maps a Multicast Profile to a VLAN.  */
/*                        By this, corresponding VLAN will act as Multicast  */
/*                        VLAN.                                              */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4ProfileId- Profile to map to the VLAN.           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliMapMVlanToProfile (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                           INT4 i4AddrType, UINT4 u4ProfileId)
{
    UINT4               u4ErrorCode = 0;

    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhTestv2FsSnoopVlanMulticastProfileId
        (&u4ErrorCode, i4InstId, i4VlanId, i4AddrType,
         u4ProfileId) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    if (nmhSetFsSnoopVlanMulticastProfileId (i4InstId, i4VlanId, i4AddrType,
                                             u4ProfileId) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliUnMapMVlanFromProfile                      */
/*                                                                           */
/*     DESCRIPTION      : This function unmap a Multicast Profile to a VLAN. */
/*                        (If any profile is configured to the VLAN.         */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        i4InstId   - Instance Identifier                   */
/*                        i4VlanId     - Vlan Identifier                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliUnMapMVlanFromProfile (tCliHandle CliHandle, INT4 i4InstId,
                               INT4 i4VlanId, INT4 i4AddrType)
{
    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSnoopVlanMulticastProfileId (i4InstId, i4VlanId, i4AddrType,
                                             0) == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    if (SnoopCliUpdateVlanEntry (CliHandle, i4InstId, i4VlanId, i4AddrType,
                                 SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopCliVlanResetStatistics                          */
/*                                                                           */
/* Description        : This function clears  the snooping statistics        */
/*                      maintained for a Vlan                                */
/*                                                                           */
/* Input(s)           : CliHandle       - Handle to the CLI Context         */
/*                      u4InstId         - Instance Identifier               */
/*                      u1AddrType       - SNOOP_ADDR_TYPE_IPV4              */
/*                      VlanInd           - VlanIdentifier                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : CLI_SUCCESS / CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliVlanResetStatistics (tCliHandle CliHandle, UINT4 u4InstId,
                             UINT1 u1AddrType, tVlanId VlanId)
{
    UNUSED_PARAM (CliHandle);

    if (SnoopUtilVlanResetStats (u4InstId, u1AddrType, VlanId) != SNOOP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetTrace                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable trace         */
/*                                                                           */
/*     INPUT            : i4InstId  - Instance Identifier                    */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                      : i4TraceStatus  - Trace Flag                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliSetTrace (tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType,
                  INT4 i4TraceStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetTraceVal = 0;

    if (i4TraceStatus < 0)
    {
        nmhGetFsSnoopTraceOption (i4InstId, i4AddrType, &i4RetTraceVal);
        i4TraceStatus &= SNOOP_MAX_INT4;
        i4RetTraceVal &= (~i4TraceStatus);
        i4TraceStatus = i4RetTraceVal;
    }

    if (nmhTestv2FsSnoopTraceOption (&u4ErrCode, i4InstId, i4AddrType,
                                     i4TraceStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopTraceOption (i4InstId, i4AddrType,
                                  i4TraceStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetDebug                                   */
/*                                                                           */
/*     DESCRIPTION      : This function will enable or disable Debug         */
/*                                                                           */
/*     INPUT            : i4InstId  - Instance Identifier                    */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                      : i4DebugStatus  - Debug Flag                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/

INT4
SnoopCliSetDebug (tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType,
                  INT4 i4DebugStatus)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RetDebugVal = 0;

    if (i4DebugStatus < 0)
    {
        nmhGetFsSnoopDebugOption (i4InstId, i4AddrType, &i4RetDebugVal);
        i4DebugStatus &= SNOOP_MAX_INT4;
        i4RetDebugVal &= (~i4DebugStatus);
        i4DebugStatus = i4RetDebugVal;
    }

    if (nmhTestv2FsSnoopDebugOption (&u4ErrCode, i4InstId, i4AddrType,
                                     i4DebugStatus) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopDebugOption (i4InstId, i4AddrType,
                                  i4DebugStatus) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopSetCliPortCfgLeaveMode                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the fast leave                  */
/*                                  process mode for the given interface.    */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4IfIndex   - Interface Number                     */
/*                        VlanId     - Inner Vlan Identifier                 */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u1LeaveMode - Leave process mode                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopSetCliPortCfgLeaveMode (tCliHandle CliHandle, INT4 u4IfIndex,
                             UINT1 u1AddrType, INT4 i4LeaveMode,
                             tSnoopTag VlanId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4Instance = 0;
    UINT4               u4InnerVlanId = 0;
    INT4                i4CurrLeaveMode = 0;
    INT4                i4Result = 0;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    u4Instance = SNOOP_GLOBAL_INSTANCE (u4IfIndex);
    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);
    u4InnerVlanId = SNOOP_INNER_VLAN (VlanId);

    if (SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_CREATE_AND_GO) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
    {
        i4Result = nmhTestv2FsSnoopPortLeaveMode (&u4ErrCode, u4IfIndex,
                                                  u1AddrType, i4LeaveMode);
    }
    else
    {
        i4Result = nmhTestv2FsSnoopEnhPortLeaveMode (&u4ErrCode, u4IfIndex,
                                                     u4InnerVlanId, u1AddrType,
                                                     i4LeaveMode);
    }

    if (i4Result == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    i4Result =
        nmhGetFsSnoopEnhPortLeaveMode (u4IfIndex, u4InnerVlanId, u1AddrType,
                                       &i4CurrLeaveMode);

    if (nmhSetFsSnoopEnhPortLeaveMode (u4IfIndex, u4InnerVlanId,
                                       u1AddrType, i4LeaveMode) == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (SnoopCliUpdatePortCfgEntry
        (CliHandle, u4IfIndex, InnerVlanId, u1AddrType,
         SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (i4Result);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopSetCliPortCfgRateLimit                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the RateLimit                   */
/*                                  for the given interface.                 */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4IfIndex   - Interface Number                     */
/*                        VlanId     - Inner Vlan Identifier                 */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4RateLimit - Rate limit                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopSetCliPortCfgRateLimit (tCliHandle CliHandle, INT4 u4IfIndex,
                             UINT1 u1AddrType, UINT4 u4RateLimit,
                             tSnoopTag VlanId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4Instance = 0;
    UINT4               u4InnerVlanId = 0;
    UINT4               u4CurrRateLimit = 0;
    INT4                i4Result = SNMP_FAILURE;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    u4Instance = SNOOP_GLOBAL_INSTANCE (u4IfIndex);
    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);
    u4InnerVlanId = SNOOP_INNER_VLAN (VlanId);

    if (SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_CREATE_AND_GO) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
    {
        i4Result = nmhTestv2FsSnoopPortRateLimit (&u4ErrCode, u4IfIndex,
                                                  u1AddrType, u4RateLimit);
    }
    else
    {
        i4Result = nmhTestv2FsSnoopEnhPortRateLimit (&u4ErrCode, u4IfIndex,
                                                     u4InnerVlanId, u1AddrType,
                                                     u4RateLimit);
    }

    if (i4Result == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    i4Result =
        nmhGetFsSnoopEnhPortRateLimit (u4IfIndex, u4InnerVlanId, u1AddrType,
                                       &u4CurrRateLimit);

    if (nmhSetFsSnoopEnhPortRateLimit (u4IfIndex, u4InnerVlanId, u1AddrType,
                                       u4RateLimit) == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, (UINT4) u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);

        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdatePortCfgEntry
        (CliHandle, u4IfIndex, InnerVlanId, u1AddrType,
         SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (i4Result);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopSetCliPortCfgMaxLimit                         */
/*                                                                           */
/*     DESCRIPTION      : This function sets the RateLimit                   */
/*                                  for the given interface.                 */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4IfIndex   - Interface Number                     */
/*                        VlanId     - Inner Vlan Identifier                 */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4MaxLimit - Maximum limit                         */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopSetCliPortCfgMaxLimit (tCliHandle CliHandle, INT4 u4IfIndex,
                            UINT1 u1AddrType, UINT1 u1LimitType,
                            INT4 i4MaxLimit, tSnoopTag VlanId)
{
    tSnoopTag           InnerVlanId;
    UINT4               u4InnerVlanId = 0;
    INT4                i4Result = 0;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);
    u4InnerVlanId = SNOOP_INNER_VLAN (VlanId);

    if (SnoopCliUpdatePortCfgEntry (CliHandle, (UINT4) u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_CREATE_AND_GO) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    i4Result = SnoopTestPortMaxLimit (u4IfIndex, u1LimitType,
                                      (UINT4) i4MaxLimit);

    if (i4Result == SNOOP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopEnhPortMaxLimitType (u4IfIndex, u4InnerVlanId,
                                          u1AddrType, (INT4) u1LimitType) ==
        SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopEnhPortMaxLimit (u4IfIndex, u4InnerVlanId,
                                      u1AddrType, i4MaxLimit) == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    if (SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_SET_ACTIVE) !=
        CLI_SUCCESS)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, u4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopSetCliPortCfgProfileId                        */
/*                                                                           */
/*     DESCRIPTION      : This function sets the ProfileId                   */
/*                                  for the given interface.                 */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4IfIndex   - Interface Number                     */
/*                        VlanId     - Inner Vlan Identifier                 */
/*                        u1AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        u4ProfileId - Profile Id                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopSetCliPortCfgProfileId (tCliHandle CliHandle, INT4 i4IfIndex,
                             UINT1 u1AddrType, UINT4 u4ProfileId,
                             tSnoopTag VlanId)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4Instance = 0;
    UINT4               u4InnerVlanId = 0;
    UINT4               u4CurrProfileId = 0;
    INT4                i4Result = 0;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    u4Instance = SNOOP_GLOBAL_INSTANCE (i4IfIndex);
    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);
    u4InnerVlanId = SNOOP_INNER_VLAN (VlanId);

    if (SnoopCliUpdatePortCfgEntry (CliHandle, i4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_CREATE_AND_GO) !=
        CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
    {
        i4Result = nmhTestv2FsSnoopPortProfileId (&u4ErrCode, i4IfIndex,
                                                  u1AddrType, u4ProfileId);
    }
    else
    {
        i4Result = nmhTestv2FsSnoopEnhPortProfileId (&u4ErrCode, i4IfIndex,
                                                     u4InnerVlanId, u1AddrType,
                                                     u4ProfileId);
    }

    if (i4Result == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, i4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    i4Result =
        nmhGetFsSnoopEnhPortProfileId (i4IfIndex, u4InnerVlanId, u1AddrType,
                                       &u4CurrProfileId);

    if (nmhSetFsSnoopEnhPortProfileId (i4IfIndex, u4InnerVlanId, u1AddrType,
                                       u4ProfileId) == SNMP_FAILURE)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, (UINT4) i4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdatePortCfgEntry (CliHandle, i4IfIndex, InnerVlanId,
                                    u1AddrType,
                                    SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        SnoopCliUpdatePortCfgEntry (CliHandle, i4IfIndex, InnerVlanId,
                                    u1AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (i4Result);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowVlanRouterPorts                        */
/*                                                                           */
/*     DESCRIPTION      : Displays router ports learnt or configured for     */
/*                        specified Vlan                                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4InstId  - Instance Identifier                    */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    -  VLAN Identifier                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliShowVlanRouterPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4AddrType, tVlanId VlanId,
                             INT4 i4PrintRtPortInfo)
{
    UINT1              *pau1StaticRtrLocalPortList = NULL;
    UINT1              *pau1VlanRouterLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE VlanRouterPortList;
    tSNMP_OCTET_STRING_TYPE StaticVlanRtrPortList;
    tSNMP_OCTET_STRING_TYPE StaticRtrLocalPortList;
    tSNMP_OCTET_STRING_TYPE VlanRouterLocalPortList;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT2               u2NewLineFlag = SNOOP_FALSE;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    tSnoopIfPortBmp    *pRtrPortList = NULL;
    tSnoopIfPortBmp    *pStaticRtrPortList = NULL;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    UINT1               u1IsShowAll = SNOOP_TRUE;
    INT1                i1Print = SNOOP_FALSE;

    pRtrPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pRtrPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        return CLI_FAILURE;
    }

    pStaticRtrPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pStaticRtrPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        return CLI_FAILURE;
    }
    pau1StaticRtrLocalPortList =
        UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1StaticRtrLocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1StaticRtrLocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1StaticRtrLocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    pau1VlanRouterLocalPortList =
        UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1VlanRouterLocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1VlanRouterLocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
        UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1VlanRouterLocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    VlanRouterPortList.pu1_OctetList = *pRtrPortList;
    VlanRouterPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;

    VlanRouterLocalPortList.pu1_OctetList = pau1VlanRouterLocalPortList;
    VlanRouterLocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    StaticVlanRtrPortList.pu1_OctetList = *pStaticRtrPortList;
    StaticVlanRtrPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;

    StaticRtrLocalPortList.pu1_OctetList = pau1StaticRtrLocalPortList;
    StaticRtrLocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    if (nmhGetFirstIndexFsSnoopVlanRouterTable (&i4NextInstId, &i4NextVlanId,
                                                &i4NextAddrType)
        == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
        FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
        UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
        UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
        return CLI_SUCCESS;
    }

    do
    {
        SNOOP_MEM_SET (VlanRouterPortList.pu1_OctetList, 0,
                       VlanRouterPortList.i4_Length);
        SNOOP_MEM_SET (VlanRouterLocalPortList.pu1_OctetList, 0,
                       VlanRouterLocalPortList.i4_Length);
        SNOOP_MEM_SET (StaticVlanRtrPortList.pu1_OctetList, 0,
                       StaticVlanRtrPortList.i4_Length);
        SNOOP_MEM_SET (StaticRtrLocalPortList.pu1_OctetList, 0,
                       StaticRtrLocalPortList.i4_Length);

        /* Display the router port entries only for the VLAN and the address
         * type configured */
        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanRouterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
                FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
                UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
                UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
                return CLI_SUCCESS;
            }
        }

        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tVlanId) i4NextVlanId)
            {
                i4CurrInstId = i4NextInstId;
                i4CurrVlanId = i4NextVlanId;
                i4CurrAddrType = i4NextAddrType;

                if (nmhGetNextIndexFsSnoopVlanRouterTable
                    (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                     i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
                    FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
                    UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
                    UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
                    return CLI_SUCCESS;
                }
            }
        }

        /* If the configured address type is not matched then get the next 
         * router port entries entries for the VLAN */

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanRouterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
                FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
                UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
                UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
                return CLI_SUCCESS;
            }
        }

        /* Display VLAN router port entries based on the address type 
         * (IGS/MLDS) configured */

        nmhGetFsSnoopVlanRouterLocalPortList (i4NextInstId, i4NextVlanId,
                                              i4AddrType,
                                              &VlanRouterLocalPortList);

        SnoopGetIfPortBmp ((UINT4) i4NextInstId,
                           VlanRouterLocalPortList.pu1_OctetList,
                           VlanRouterPortList.pu1_OctetList);

        nmhGetFsSnoopVlanRtrLocalPortList (i4NextInstId, i4NextVlanId,
                                           i4AddrType, &StaticRtrLocalPortList);

        SnoopGetIfPortBmp ((UINT4) i4NextInstId,
                           StaticRtrLocalPortList.pu1_OctetList,
                           StaticVlanRtrPortList.pu1_OctetList);

        if ((FsUtilBitListIsAllZeros (VlanRouterPortList.pu1_OctetList,
                                      SNOOP_IF_PORT_LIST_SIZE) == OSIX_FALSE) ||
            (FsUtilBitListIsAllZeros (StaticVlanRtrPortList.pu1_OctetList,
                                      SNOOP_IF_PORT_LIST_SIZE) == OSIX_FALSE))
        {
            if (i4PrintRtPortInfo == SNOOP_CLI_SHOW_ALL_RTR_PORT_INFO)
            {
                if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                        au1TempContextName) == SNOOP_SUCCESS)
                {
                    i1Print = SNOOP_TRUE;
                    CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
                    CliPrintf (CliHandle,
                               "\r\nVlan   Ports                            Version   ");
                    CliPrintf (CliHandle,
                               "Older-QuerierInterval   V3-QuerierInterval\r\n");
                    CliPrintf (CliHandle,
                               "-----  -------------------              -------   ");
                    CliPrintf (CliHandle,
                               "---------------------   -----------------\r\n");
                }
                else if (i1Print == SNOOP_FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r\nVlan   Ports                       Version   ");
                    CliPrintf (CliHandle,
                               "Older-QuerierInterval   V3-QuerierInterval\r\n");
                    CliPrintf (CliHandle,
                               "-----  -------------------         -------   ");
                    CliPrintf (CliHandle,
                               "---------------------   -----------------\r\n");
                    i1Print = SNOOP_TRUE;
                }

                if (SnoopCliPrintPortListInfo (CliHandle, i4NextInstId,
                                               VlanRouterPortList,
                                               StaticVlanRtrPortList,
                                               (UINT4) i4NextVlanId,
                                               i4AddrType) != CLI_SUCCESS)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
                    FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
                    UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
                    UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
                    return CLI_SUCCESS;
                }
            }
            else
            {
                if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                        au1TempContextName) == SNOOP_SUCCESS)
                {
                    i1Print = SNOOP_TRUE;
                    CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
                    CliPrintf (CliHandle, "\r\nVlan   Ports\r\n");
                    CliPrintf (CliHandle, "-----  ----------------\r\n");
                }
                else if (i1Print == SNOOP_FALSE)
                {
                    CliPrintf (CliHandle, "Vlan   Ports\r\n");
                    CliPrintf (CliHandle, "-----  ----------------\r\n");
                    i1Print = SNOOP_TRUE;
                }
                CliPrintf (CliHandle, "%5d", i4NextVlanId);

                if (SnoopCliPrintPortList (CliHandle, i4NextInstId,
                                           VlanRouterPortList,
                                           StaticVlanRtrPortList,
                                           (UINT4) i4NextVlanId,
                                           i4AddrType,
                                           &u2NewLineFlag) != CLI_SUCCESS)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
                    FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
                    UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
                    UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
                    return CLI_SUCCESS;
                }
            }

        }

        MEMSET (StaticVlanRtrPortList.pu1_OctetList, 0,
                SNOOP_IF_PORT_LIST_SIZE);

        MEMSET (VlanRouterPortList.pu1_OctetList, 0, SNOOP_IF_PORT_LIST_SIZE);

        if (u2NewLineFlag != SNOOP_TRUE)
        {
            u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r\n");
        }
        else
        {
            u4PagingStatus = CliPrintf (CliHandle, "\r");
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

        if (nmhGetNextIndexFsSnoopVlanRouterTable (i4CurrInstId, &i4NextInstId,
                                                   i4CurrVlanId, &i4NextVlanId,
                                                   i4CurrAddrType,
                                                   &i4NextAddrType)
            != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pRtrPortList);
    FsUtilReleaseBitList ((UINT1 *) pStaticRtrPortList);
    UtilPlstReleaseLocalPortList (pau1StaticRtrLocalPortList);
    UtilPlstReleaseLocalPortList (pau1VlanRouterLocalPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowVlanBlkRtrPorts                        */
/*                                                                           */
/*     DESCRIPTION      : Displays blocked router ports configured for       */
/*                        specified Vlan                                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        u4InstId  - Instance Identifier                    */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    -  VLAN Identifier                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliShowVlanBlkRtrPorts (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4AddrType, tSnoopVlanId VlanId)
{
    UINT1              *pau1BlkRtrLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE VlanBlkRouterPortList;
    tSNMP_OCTET_STRING_TYPE VlanBlkRouterLocalPortList;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    tSnoopIfPortBmp    *pBlkRtrPortList = NULL;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    UINT1               u1IsShowAll = SNOOP_TRUE;
    INT1                i1Print = SNOOP_FALSE;

    pBlkRtrPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pBlkRtrPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        return CLI_FAILURE;
    }

    VlanBlkRouterPortList.pu1_OctetList = *pBlkRtrPortList;
    VlanBlkRouterPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    pau1BlkRtrLocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1BlkRtrLocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1BlkRtrLocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1BlkRtrLocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    VlanBlkRouterLocalPortList.pu1_OctetList = pau1BlkRtrLocalPortList;
    VlanBlkRouterLocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    if (nmhGetFirstIndexFsSnoopVlanFilterTable (&i4NextInstId,
                                                &i4NextVlanId,
                                                &i4NextAddrType)
        == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
        UtilPlstReleaseLocalPortList (pau1BlkRtrLocalPortList);
        return CLI_SUCCESS;
    }

    do
    {
        SNOOP_MEM_SET (pBlkRtrPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
        SNOOP_MEM_SET (pau1BlkRtrLocalPortList, SNOOP_INIT_VAL,
                       SNOOP_PORT_LIST_SIZE);

        /* Display the router port entries only for the VLAN and the address
         * type configured */
        /* if specific switch name is given , display only the respective
         * switch information */

        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanFilterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
                UtilPlstReleaseLocalPortList (pau1BlkRtrLocalPortList);
                return CLI_SUCCESS;
            }
        }

        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tVlanId) i4NextVlanId)
            {
                i4CurrInstId = i4NextInstId;
                i4CurrVlanId = i4NextVlanId;
                i4CurrAddrType = i4NextAddrType;
                if (nmhGetNextIndexFsSnoopVlanFilterTable
                    (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                     i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
                    UtilPlstReleaseLocalPortList (pau1BlkRtrLocalPortList);
                    return CLI_SUCCESS;
                }
            }
        }

        /* If the configured address type is not matched then get the next 
         * router port entries entries for the VLAN */

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanFilterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
                UtilPlstReleaseLocalPortList (pau1BlkRtrLocalPortList);
                return CLI_SUCCESS;
            }
        }

        /* Display VLAN blocked router port entries based on the address type 
         * (IGS/MLDS) configured */

        nmhGetFsSnoopVlanBlkRtrLocalPortList (i4NextInstId, i4NextVlanId,
                                              i4AddrType,
                                              &VlanBlkRouterLocalPortList);

        SnoopGetIfPortBmp ((UINT4) i4NextInstId,
                           VlanBlkRouterLocalPortList.pu1_OctetList,
                           VlanBlkRouterPortList.pu1_OctetList);

        if (FsUtilBitListIsAllZeros (VlanBlkRouterPortList.pu1_OctetList,
                                     SNOOP_IF_PORT_LIST_SIZE) == OSIX_FALSE)
        {
            if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                    au1TempContextName) == SNOOP_SUCCESS)
            {
                i1Print = SNOOP_TRUE;
                CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
                CliPrintf (CliHandle, "\r\nVlan   Ports\r\n");
                CliPrintf (CliHandle, "-----  ------\r\n");
            }
            else if (i1Print == SNOOP_FALSE)
            {
                CliPrintf (CliHandle, "Vlan   Ports\r\n");
                CliPrintf (CliHandle, "-----  ------\r\n");
                i1Print = SNOOP_TRUE;
            }

            CliPrintf (CliHandle, "%5d", i4NextVlanId);

            SnoopCliPrintBlkPortList (CliHandle, i4NextInstId,
                                      VlanBlkRouterPortList);
            CliPrintf (CliHandle, "\r\n");
        }

        u4PagingStatus = CliPrintf (CliHandle, "\r");

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

        if (nmhGetNextIndexFsSnoopVlanFilterTable
            (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
             i4CurrAddrType, &i4NextAddrType) != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    FsUtilReleaseBitList ((UINT1 *) pBlkRtrPortList);
    UtilPlstReleaseLocalPortList (pau1BlkRtrLocalPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowGlobalInfo                             */
/*                                                                           */
/*     DESCRIPTION      : Displays IGS/MLDS information for                  */
/*                        a specified vlan or for all Vlans.                 */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4AddrType)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4NextInstId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4SnoopStatus = 0;
    INT4                i4SnoopQueryStatus = 0;
    INT4                i4SnoopOperStatus = 0;
    INT4                i4ProxyStatus = 0;
    INT4                i4ProxyReportingStatus = 0;
    INT4                i4FwdMode = 0;
    INT4                i4RtrPurgeInt = 0;
    INT4                i4PortPurgeInt = 0;
    INT4                i4RepFwdInt = 0;
    INT4                i4GrpQryInt = 0;
    INT4                i4RetryCnt = 0;
    INT4                i4ReportFwd = 0;
    INT4                i4QueryFwd = 0;
    INT4                i4LeaveCfgLevel = 0;
    INT4                i4FilterStatus = 0;
    INT4                i4EnhMode = 0;
    INT4                i4SparMode = 0;
    INT4                i4ReportCfgLevel = 0;
    INT4                i4MVlanStatus = 0;
    UINT1               u1IsShowAll = SNOOP_TRUE;
    INT4                i4ControlPlaneDriven = 0;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    INT1                i1RetVal = SNOOP_SUCCESS;

    if (nmhGetFirstIndexFsSnoopInstanceConfigTable (&i4NextInstId,
                                                    &i4NextAddrType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r Snooping module not started \r\n");
        return CLI_SUCCESS;
    }

    do
    {

        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopInstanceConfigTable
                (i4CurrInstId, &i4NextInstId, i4CurrAddrType,
                 &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }
        }
        if (i4AddrType != i4NextAddrType)
        {
            i4CurrAddrType = i4NextAddrType;
            i4CurrInstId = i4NextInstId;

            if (nmhGetNextIndexFsSnoopInstanceConfigTable
                (i4CurrInstId, &i4NextInstId, i4CurrAddrType,
                 &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                return CLI_SUCCESS;
            }

        }
        nmhGetFsSnoopStatus (i4NextInstId, i4NextAddrType, &i4SnoopStatus);
        nmhGetFsSnoopOperStatus (i4NextInstId, i4NextAddrType,
                                 &i4SnoopOperStatus);

        /* Get multicast forwarding mode from the global instance table 
         * based on the instance id */
        nmhGetFsSnoopInstanceGlobalMcastFwdMode (i4NextInstId, &i4FwdMode);

        nmhGetFsSnoopInstanceGlobalEnhancedMode (i4NextInstId, &i4EnhMode);
        nmhGetFsSnoopInstanceGlobalSparseMode (i4NextInstId, &i4SparMode);
        i1RetVal =
            nmhGetFsSnoopProxyStatus (i4NextInstId, i4NextAddrType,
                                      &i4ProxyStatus);
        UNUSED_PARAM (i1RetVal);

        i1RetVal =
            nmhGetFsSnoopProxyReportingStatus (i4NextInstId, i4NextAddrType,
                                               &i4ProxyReportingStatus);
        UNUSED_PARAM (i1RetVal);

        nmhGetFsSnoopRouterPortPurgeInterval (i4NextInstId, i4NextAddrType,
                                              &i4RtrPurgeInt);
        nmhGetFsSnoopPortPurgeInterval (i4NextInstId, i4NextAddrType,
                                        &i4PortPurgeInt);
        nmhGetFsSnoopReportForwardInterval (i4NextInstId, i4NextAddrType,
                                            &i4RepFwdInt);
        nmhGetFsSnoopGrpQueryInterval (i4NextInstId, i4NextAddrType,
                                       &i4GrpQryInt);
        nmhGetFsSnoopReportFwdOnAllPorts (i4NextInstId, i4NextAddrType,
                                          &i4ReportFwd);
        nmhGetFsSnoopRetryCount (i4NextInstId, i4NextAddrType, &i4RetryCnt);

        nmhGetFsSnoopSendQueryOnTopoChange (i4NextInstId, i4NextAddrType,
                                            &i4SnoopQueryStatus);

        nmhGetFsSnoopInstanceGlobalLeaveConfigLevel (i4NextInstId,
                                                     &i4LeaveCfgLevel);

        nmhGetFsSnoopMulticastVlanStatus (i4NextInstId, i4NextAddrType,
                                          &i4MVlanStatus);
        nmhGetFsSnoopFilterStatus (i4NextInstId, i4NextAddrType,
                                   &i4FilterStatus);

        nmhGetFsSnoopInstanceGlobalReportProcessConfigLevel (i4NextInstId,
                                                             &i4ReportCfgLevel);

        nmhGetFsSnoopQueryFwdOnAllPorts (i4NextInstId, i4NextAddrType,
                                         &i4QueryFwd);

        nmhGetFsSnoopInstanceControlPlaneDriven (i4NextInstId,
                                                 &i4ControlPlaneDriven);

        if (SnoopGetSwitchName (i4NextInstId, (INT4) SNOOP_CLI_INVALID_CONTEXT,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
        }
        CliPrintf (CliHandle, "\r\nSnooping Configuration\r\n");
        CliPrintf (CliHandle, "-----------------------------\r\n");

#ifdef IGS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (i4SnoopStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "IGMP Snooping globally enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "IGMP Snooping globally disabled\r\n");
            }

            if (i4SnoopOperStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "IGMP Snooping is operationally "
                           "enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "IGMP Snooping is operationally "
                           "disabled\r\n");
            }
            if (i4EnhMode == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "IGMP Snooping Enhanced mode is "
                           "enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "IGMP Snooping Enhanced mode is "
                           "disabled \r\n");
            }
            if (i4SparMode == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "IGMP Snooping Sparse mode is "
                           "enabled \r\n");
            }
            else
            {
                CliPrintf (CliHandle, "IGMP Snooping Sparse mode is "
                           "disabled \r\n");
            }

        }
#endif
#ifdef MLDS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (i4SnoopStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "MLD Snooping globally enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "MLD Snooping globally disabled\r\n");
            }

            if (i4SnoopOperStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "MLD Snooping is operationally "
                           "enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "MLD Snooping is operationally "
                           "disabled\r\n");
            }
        }
#endif

        if (i4SnoopQueryStatus == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "Transmit Query on Topology Change "
                       "globally enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Transmit Query on Topology Change  "
                       "globally disabled\r\n");
        }

        if (i4FwdMode == SNOOP_MCAST_FWD_MODE_MAC)
        {
            CliPrintf (CliHandle, "Multicast forwarding mode is MAC based\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Multicast forwarding mode is IP based\r\n");
        }

        if (i4ProxyStatus == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "Proxy globally enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Proxy globally disabled\r\n");
        }

        if (i4ProxyReportingStatus == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "Proxy reporting globally enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Proxy reporting globally disabled\r\n");
        }

        if (i4FilterStatus == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "Filter is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Filter is disabled\r\n");
        }

        CliPrintf (CliHandle,
                   "Router port purge interval is %d seconds\r\n",
                   i4RtrPurgeInt);
        CliPrintf (CliHandle, "Port purge interval is %d seconds\r\n",
                   i4PortPurgeInt);
        CliPrintf (CliHandle, "Report forward interval is %d seconds\r\n",
                   i4RepFwdInt);
        CliPrintf (CliHandle,
                   "Group specific query interval is %d seconds\r\n",
                   i4GrpQryInt);

        if (i4ReportFwd == SNOOP_FORWARD_ALL_PORTS)
        {
            CliPrintf (CliHandle, "Reports are forwarded on all ports\r\n");
        }
        else if (i4ReportFwd == SNOOP_FORWARD_RTR_PORTS)
        {
            CliPrintf (CliHandle, "Reports are forwarded on router ports\r\n");
        }
        else if (i4ReportFwd == SNOOP_FORWARD_NONEDGE_PORTS)
        {
            CliPrintf (CliHandle,
                       "Reports are forwarded on non-edge ports\r\n");
        }

        if (i4QueryFwd == SNOOP_FORWARD_ALL_PORTS)
        {
            CliPrintf (CliHandle, "Queries are forwarded on all ports\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "Queries are forwarded on non-router ports\r\n");
        }

        CliPrintf (CliHandle,
                   "Group specific query retry count is %d\r\n", i4RetryCnt);
        if (i4MVlanStatus == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "Multicast VLAN enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Multicast VLAN disabled\r\n");
        }

        if (i4LeaveCfgLevel == SNOOP_VLAN_LEAVE_CONFIG)
        {
            CliPrintf (CliHandle, "Leave config level is Vlan based\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Leave config level is Port based\r\n");
        }

        if (i4ReportCfgLevel == SNOOP_NON_ROUTER_REPORT_CONFIG)
        {
            CliPrintf (CliHandle,
                       "Report processing config level is on non-router ports\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       "Report processing config level is on all Ports\r\n");
        }
        u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");

        if (i4ControlPlaneDriven == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle,
                       "Control Plane Driven for report message is enabled\r\n");
        }
        else if (i4ControlPlaneDriven == SNOOP_DISABLE)
        {
            CliPrintf (CliHandle,
                       "Control Plane Driven for report message is disabled\r\n");
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrAddrType = i4NextAddrType;

        if (nmhGetNextIndexFsSnoopInstanceConfigTable
            (i4CurrInstId, &i4NextInstId, i4CurrAddrType,
             &i4NextAddrType) != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }

    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowVlanInfo                               */
/*                                                                           */
/*     DESCRIPTION      : Displays IGS/MLDS VLAN specific information for    */
/*                        all VLANS                                          */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    - Vlan Identifier                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowVlanInfo (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4AddrType,
                      tVlanId VlanId)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4MulticastProfileId = 0;
    UINT4               u4RobVal = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4NextInstId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4SnoopStatus = 0;
    INT4                i4Querier = 0;
    INT4                i4CfgQuerier = 0;
    INT4                i4StartQCount = 0;
    INT4                i4StartQInterval = 0;
    INT4                i4QryInterval = 0;
    INT4                i4OthrQryPrsntInterval = 0;
    INT4                i4PortPurgeInt = 0;
    INT4                i4MaxRespCode = 0;
    INT4                i4FastLeave = 0;
    INT4                i4CfgVersion = 0;
    FLT4                fMaxRespTime = 0;
    UINT4               u4ElectedQuerier = 0;
    UINT1               u1IsShowAll = SNOOP_TRUE;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    CHR1               *pu1String = NULL;

    if (nmhGetFirstIndexFsSnoopVlanFilterTable (&i4NextInstId, &i4NextVlanId,
                                                &i4NextAddrType)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    do
    {
        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanFilterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                return CLI_SUCCESS;
            }
        }
        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tVlanId) i4NextVlanId)
            {
                i4CurrInstId = i4NextInstId;
                i4CurrVlanId = i4NextVlanId;
                i4CurrAddrType = i4NextAddrType;

                if (nmhGetNextIndexFsSnoopVlanFilterTable
                    (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                     i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                    return CLI_SUCCESS;
                }
            }
        }

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopVlanFilterTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                return CLI_SUCCESS;
            }
        }

        if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
        }

        CliPrintf (CliHandle,
                   "\r\nSnooping VLAN Configuration for the VLAN %d\r\n",
                   i4NextVlanId);

        nmhGetFsSnoopVlanSnoopStatus (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &i4SnoopStatus);

        nmhGetFsSnoopVlanCfgOperVersion (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &i4CfgVersion);

        nmhGetFsSnoopVlanFastLeave (i4NextInstId, i4NextVlanId,
                                    i4NextAddrType, &i4FastLeave);

        nmhGetFsSnoopVlanCfgQuerier (i4NextInstId, i4NextVlanId,
                                     i4NextAddrType, &i4CfgQuerier);

        nmhGetFsSnoopVlanQuerier (i4NextInstId, i4NextVlanId,
                                  i4NextAddrType, &i4Querier);

        nmhGetFsSnoopVlanElectedQuerier (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4ElectedQuerier);

        nmhGetFsSnoopVlanStartupQueryCount (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType, &i4StartQCount);

        nmhGetFsSnoopVlanStartupQueryInterval (i4NextInstId, i4NextVlanId,
                                               i4NextAddrType,
                                               &i4StartQInterval);

        nmhGetFsSnoopVlanQueryInterval (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &i4QryInterval);

        nmhGetFsSnoopVlanOtherQuerierPresentInterval (i4NextInstId,
                                                      i4NextVlanId,
                                                      i4NextAddrType,
                                                      &i4OthrQryPrsntInterval);

        nmhGetFsSnoopVlanPortPurgeInterval (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType, &i4PortPurgeInt);

        nmhGetFsSnoopVlanMaxResponseTime (i4NextInstId, i4NextVlanId,
                                          i4NextAddrType, &i4MaxRespCode);
        nmhGetFsSnoopVlanRobustnessValue (i4NextInstId, i4NextVlanId,
                                          i4NextAddrType, &u4RobVal);

        if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            fMaxRespTime =
                (i4CfgVersion ==
                 SNOOP_IGS_IGMP_VERSION3) ?
                SNOOP_IGS_GET_TIME_FROM_CODE ((UINT1) i4MaxRespCode) :
                i4MaxRespCode;
            fMaxRespTime = fMaxRespTime / SNOOP_IGS_MRC_UNIT;
        }
        if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            fMaxRespTime =
                SNOOP_MLDS_GET_TIME_FROM_CODE ((UINT2) i4MaxRespCode);
            fMaxRespTime = fMaxRespTime / SNOOP_MLDS_MRC_UNIT;
        }

        nmhGetFsSnoopVlanMulticastProfileId (i4NextInstId, i4NextVlanId,
                                             i4NextAddrType,
                                             &u4MulticastProfileId);

#ifdef IGS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (i4SnoopStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "  IGMP Snooping enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "  IGMP Snooping disabled\r\n");
            }
        }
#endif
#ifdef MLDS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (i4SnoopStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "  MLD Snooping enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "  MLD Snooping disabled\r\n");
            }
        }

#endif

#ifdef IGS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            if (i4CfgVersion == SNOOP_IGS_IGMP_VERSION3)
            {
                CliPrintf (CliHandle, "  IGMP configured version is V3\r\n");
            }
            else if (i4CfgVersion == SNOOP_IGS_IGMP_VERSION2)
            {
                CliPrintf (CliHandle, "  IGMP configured version is V2\r\n");
            }
            else if (i4CfgVersion == SNOOP_IGS_IGMP_VERSION1)
            {
                CliPrintf (CliHandle, "  IGMP configured version is V1\r\n");
            }

        }
#endif
#ifdef MLDS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            if (i4CfgVersion == SNOOP_MLD_VERSION1)
            {
                CliPrintf (CliHandle, "  MLD configured version is V1\r\n");
            }
            else if (i4CfgVersion == SNOOP_MLD_VERSION2)
            {
                CliPrintf (CliHandle, "  MLD configured version is V2\r\n");
            }

        }
#endif

        if (i4FastLeave == SNOOP_ENABLE)
        {
            CliPrintf (CliHandle, "  Fast leave is enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "  Fast leave is disabled\r\n");
        }

        if (i4CfgQuerier == SNOOP_QUERIER)
        {
            CliPrintf (CliHandle,
                       "  Snooping switch is configured as Querier\r\n");
        }
        else if (i4CfgQuerier == SNOOP_NON_QUERIER)
        {
            CliPrintf (CliHandle,
                       "  Snooping switch is configured as Non-Querier\r\n");
        }

        if (i4Querier == SNOOP_QUERIER)
        {
            CliPrintf (CliHandle, "  Snooping switch is acting as Querier\r\n");
        }
        else if (i4Querier == SNOOP_NON_QUERIER)
        {
            CliPrintf (CliHandle,
                       "  Snooping switch is acting as Non-Querier\r\n");
        }

        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4ElectedQuerier);
        CliPrintf (CliHandle, "  Elected Querier is %s\r\n", pu1String);

        CliPrintf (CliHandle, "  Startup Query Count is %d\r\n", i4StartQCount);

        CliPrintf (CliHandle,
                   "  Startup Query Interval is %d seconds\r\n",
                   i4StartQInterval);

        CliPrintf (CliHandle,
                   "  Query interval is %d seconds\r\n", i4QryInterval);

        CliPrintf (CliHandle,
                   "  Other Querier Present Interval is %d seconds\r\n",
                   i4OthrQryPrsntInterval);

        CliPrintf (CliHandle,
                   "  Port Purge Interval is %d seconds\r\n", i4PortPurgeInt);

        CliPrintf (CliHandle,
                   "  Max Response Code is %d, Time is %.2f seconds\r\n",
                   i4MaxRespCode, fMaxRespTime);

        if (u4MulticastProfileId != 0)
        {
            CliPrintf (CliHandle, "  Multicast profile %u is configured\r\n",
                       u4MulticastProfileId);
        }

        CliPrintf (CliHandle, "  Robustness variable is %d\r\n", u4RobVal);

        u4PagingStatus = CliPrintf (CliHandle, "\r");

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        if (nmhGetNextIndexFsSnoopVlanFilterTable (i4CurrInstId, &i4NextInstId,
                                                   i4CurrVlanId, &i4NextVlanId,
                                                   i4CurrAddrType,
                                                   &i4NextAddrType) !=
            SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }

    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowGroupInformation                       */
/*                                                                           */
/*     DESCRIPTION      : Displays IGMP/MLD Snooping group information       */
/*                        for specific VLAN or all VLANS                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        VlanId    - Vlan Identifier                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowGroupInformation (tCliHandle CliHandle, UINT4 u4Inst,
                              UINT1 u1AddressType, tVlanId VlanId,
                              UINT1 *pu1GroupIpAddr, INT4 i4EntryType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT4               u4EndInst = 0;
    UINT4               u4Instance = 0;
    UINT2               u2GroupEntryCnt = 0;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];

    if (u4Inst == SNOOP_CLI_INVALID_CONTEXT)
    {
        u4EndInst = (SNOOP_MAX_INSTANCES - 1);
    }
    else
    {
        u4EndInst = u4Inst;
        u4Instance = u4Inst;
    }

    for (; u4Instance <= u4EndInst; u4Instance++)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
        {
            continue;
        }
        if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
        {
            continue;
        }
        if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG, SNOOP_GRP_DBG,
                       "No Group entries found \r\n");
            SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC, SNOOP_CONTROL_TRC,
                       "No Group entries found \r\n");
            continue;
        }

        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

        if (pRBElem == NULL)
        {
            continue;
        }

        if (SnoopGetSwitchName ((INT4) u4Instance,
                                (INT4) SNOOP_CLI_INVALID_CONTEXT,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
        }
        CliPrintf (CliHandle, "\r\nSnooping Group information\r\n");
        CliPrintf (CliHandle, "-----------------------------\r\n");

        while (pRBElem != NULL)
        {
            pRBElemNext =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                               pRBElem, NULL);

            pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

            if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
            {
                if ((SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                                    gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                    && (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE))
                {
                    pRBElem = pRBElemNext;
                    continue;
                }
            }

            if ((VlanId != SNOOP_INVALID_VLAN_ID) &&
                (SNOOP_MEM_CMP (pu1GroupIpAddr, gNullGrpAddr,
                                IPVX_MAX_INET_ADDR_LEN) != 0))
            {
                if ((VlanId == SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)) &&
                    (pSnoopGroupEntry->GroupIpAddr.u1Afi == u1AddressType) &&
                    (SNOOP_MEM_CMP (pSnoopGroupEntry->GroupIpAddr.au1Addr,
                                    pu1GroupIpAddr,
                                    IPVX_MAX_INET_ADDR_LEN) == 0))
                {
                    SnoopCliPrintGroupInformation (CliHandle, u4Instance,
                                                   pSnoopGroupEntry);
                    u2GroupEntryCnt++;
                }
                else
                {
                    pRBElem = pRBElemNext;
                    continue;
                }
            }
            else if ((VlanId != SNOOP_INVALID_VLAN_ID) &&
                     (SNOOP_MEM_CMP
                      (pu1GroupIpAddr,
                       gNullGrpAddr, IPVX_MAX_INET_ADDR_LEN) == 0))
            {
                if ((VlanId == SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)) &&
                    (u1AddressType == pSnoopGroupEntry->GroupIpAddr.u1Afi))
                {
                    SnoopCliPrintGroupInformation (CliHandle, u4Instance,
                                                   pSnoopGroupEntry);
                    u2GroupEntryCnt++;
                }
                else
                {
                    pRBElem = pRBElemNext;
                    continue;
                }
            }
            else
            {
                if (((i4EntryType == pSnoopGroupEntry->u1EntryTypeFlag)
                     || (i4EntryType == SNOOP_ZERO))
                    && (u1AddressType == pSnoopGroupEntry->GroupIpAddr.u1Afi))
                {
                    SnoopCliPrintGroupInformation (CliHandle, u4Instance,
                                                   pSnoopGroupEntry);
                    u2GroupEntryCnt++;
                }
            }
            pRBElem = pRBElemNext;
        }
    }
    if (u2GroupEntryCnt == 0)
    {
        CliPrintf (CliHandle, "\r\n");
    }
    CliPrintf (CliHandle, "Total Num of Group Addresses [%d] \r\n",
               u2GroupEntryCnt);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowForwardEntries                         */
/*                                                                           */
/*     DESCRIPTION      : Displays Snooping forwarding information           */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4InstId  - Instance Identifier                    */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    - Vlan Identifier                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowForwardEntries (tCliHandle CliHandle, INT4 i4InstId,
                            INT4 i4AddrType, tVlanId VlanId, INT4 i4EntryType)
{
    SnoopCliShowMacForwardTable (CliHandle, (UINT4) i4InstId, i4AddrType,
                                 VlanId, i4EntryType);

    SnoopCliShowIpForwardTable (CliHandle, (UINT4) i4InstId, i4AddrType, VlanId,
                                i4EntryType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowMacForwardTable                        */
/*                                                                           */
/*     DESCRIPTION      : Displays MAC forwarding information                */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    - Vlan Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliShowMacForwardTable (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4AddrType, tVlanId VlanId, INT4 i4EntryType)
{
    UINT1              *pau1McastLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE McastFwdPortList;
    tSNMP_OCTET_STRING_TYPE PortList;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    tMacAddr            CurrMacAddr;
    tMacAddr            NextMacAddr;
    tSnoopIfPortBmp    *pMcastPortList = NULL;
    UINT1               au1MacAddr[21];
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    UINT1               u1IsShowAll = SNOOP_TRUE;
    INT1                i1Print = SNOOP_FALSE;
    INT1                i1Flag = SNOOP_FALSE;
    UINT2               u2ByteInd = 0;
    UINT4               u4GroupMacCounter = 0;
    INT4                i4DupInstId = 0;
    INT4                i4DupVlanId = 0;
    INT4                i4DupAddrType = 0;
    INT4                i4McastFwdEntryType = 0;
    tMacAddr            DupMacAddr;

    pMcastPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pMcastPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }
    pau1McastLocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1McastLocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1McastLocalPortList\r\n");
        FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1McastLocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    SNOOP_MEM_SET (pMcastPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (au1MacAddr, 0, 21);
    SNOOP_MEM_SET (&CurrMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&NextMacAddr, 0, sizeof (tMacAddr));

    PortList.pu1_OctetList = *pMcastPortList;
    PortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;

    McastFwdPortList.pu1_OctetList = pau1McastLocalPortList;
    McastFwdPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    if (nmhGetFirstIndexFsSnoopVlanMcastMacFwdTable
        (&i4NextInstId, &i4NextVlanId, &i4NextAddrType,
         &NextMacAddr) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
        UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
        return CLI_SUCCESS;
    }
    if (VlanId != SNOOP_INVALID_VLAN_ID)
    {
        /*Get the required vlan */
        while (VlanId != i4NextVlanId)
        {
            i4DupInstId = i4NextInstId;
            i4DupVlanId = i4NextVlanId;
            i4DupAddrType = i4NextAddrType;
            SNOOP_MEM_CPY (DupMacAddr, NextMacAddr, sizeof (tMacAddr));
            if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
                (i4DupInstId, &i4NextInstId, i4DupVlanId, &i4NextVlanId,
                 i4DupAddrType, &i4NextAddrType, DupMacAddr,
                 &NextMacAddr) == SNMP_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
                UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
                return CLI_SUCCESS;
            }
        }
    }

    do
    {
        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            SNOOP_MEM_CPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

            if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, CurrMacAddr,
                 &NextMacAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
                UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
                return CLI_SUCCESS;
            }
        }
        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tSnoopVlanId) i4NextVlanId)
            {
                while (VlanId != i4NextVlanId)
                {
                    i4DupInstId = i4NextInstId;
                    i4DupVlanId = i4NextVlanId;
                    i4DupAddrType = i4NextAddrType;
                    SNOOP_MEM_CPY (DupMacAddr, NextMacAddr, sizeof (tMacAddr));
                    if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
                        (i4DupInstId, &i4NextInstId, i4DupVlanId, &i4NextVlanId,
                         i4DupAddrType, &i4NextAddrType, DupMacAddr,
                         &NextMacAddr) == SNMP_FAILURE)
                    {
                        FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
                        UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
                        return CLI_SUCCESS;
                    }
                }

            }
        }

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            SNOOP_MEM_CPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

            if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, CurrMacAddr,
                 &NextMacAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }
        }

        nmhGetFsSnoopVlanMcastMacFwdEntryFlag (i4NextInstId, i4NextVlanId,
                                               i4NextAddrType, NextMacAddr,
                                               &i4McastFwdEntryType);
        if ((i4EntryType != i4McastFwdEntryType) && (i4EntryType != SNOOP_ZERO))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            SNOOP_MEM_CPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

            if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, CurrMacAddr,
                 &NextMacAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }

        }
        if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
            i1Print = SNOOP_FALSE;
        }
        if (i1Print == SNOOP_FALSE)
        {
            CliPrintf (CliHandle, "\r\n%5s%13s%15s\r\n", "Vlan", "MAC-Address",
                       "Ports");
            CliPrintf (CliHandle, "%5s%19s%9s\r\n", "----", "-----------------",
                       "-----");
            i1Print = SNOOP_TRUE;
        }

        nmhGetFsSnoopVlanMcastMacFwdLocalPortList (i4NextInstId, i4NextVlanId,
                                                   i4NextAddrType, NextMacAddr,
                                                   &McastFwdPortList);

        SnoopGetIfPortBmp ((UINT4) i4NextInstId,
                           McastFwdPortList.pu1_OctetList,
                           PortList.pu1_OctetList);
        for (u2ByteInd = 0; u2ByteInd <= SNOOP_IF_PORT_LIST_SIZE; u2ByteInd++)
        {
            if (PortList.pu1_OctetList[u2ByteInd])
            {
                i1Flag = SNOOP_TRUE;
                break;
            }
        }
        if (i1Flag == SNOOP_TRUE)
        {

            u4GroupMacCounter++;
            CliPrintf (CliHandle, "%5d", i4NextVlanId);

            PrintMacAddress (NextMacAddr, au1MacAddr);

            CliPrintf (CliHandle, "%22s", au1MacAddr);

            CliOctetToIfName (CliHandle, NULL, &PortList,
                              SNOOP_MAX_PORTS, SNOOP_IF_PORT_LIST_SIZE,
                              27, &u4PagingStatus, 4);
        }
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;
        SNOOP_MEM_CPY (CurrMacAddr, NextMacAddr, sizeof (tMacAddr));

        if (nmhGetNextIndexFsSnoopVlanMcastMacFwdTable
            (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
             i4CurrAddrType, &i4NextAddrType, CurrMacAddr,
             &NextMacAddr) != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }

        SNOOP_MEM_SET (pMcastPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
        SNOOP_MEM_SET (pau1McastLocalPortList, 0, SNOOP_PORT_LIST_SIZE);
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " Total Group Mac entries = %d\r\n",
               u4GroupMacCounter);
    FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
    UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowIpForwardTable                         */
/*                                                                           */
/*     DESCRIPTION      : Displays IP forwarding information                 */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    - Vlan Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliShowIpForwardTable (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4AddrType, tVlanId VlanId, INT4 i4EntryType)
{
    UINT1              *pau1McastLocalPortList = NULL;
    tSNMP_OCTET_STRING_TYPE McastFwdPortList;
    tSNMP_OCTET_STRING_TYPE CurrSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE NextSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE CurrGrpIpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpIpAddr;
    tSNMP_OCTET_STRING_TYPE McastFwdLocalPortList;
    tSnoopIfPortBmp    *pMcastPortList = NULL;
    UINT4               u1IsShowAll = SNOOP_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
#ifdef IGS_WANTED
    UINT4               u4TempSrcIpAddr = 0;
    UINT4               u4TempGrpIpAddr = 0;
#endif
    UINT4               u4CurrVlanId = 0;
    UINT4               u4NextVlanId = 0;
    UINT4               u4CurrInnerVlanId = 0;
    UINT4               u4NextInnerVlanId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    UINT1               au1CurrSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4IpFwdCounter = 0;
#ifdef IGS_WANTED
    CHR1               *pu1String = NULL;
#endif
    CHR1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    INT1                i1Print = SNOOP_FALSE;
    INT4                i4McastFwdEntryType = 0;

    pMcastPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pMcastPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        return CLI_FAILURE;
    }

    SNOOP_MEM_SET (pMcastPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (au1CurrSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1CurrGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);

    McastFwdPortList.pu1_OctetList = *pMcastPortList;
    McastFwdPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    pau1McastLocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1McastLocalPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pau1McastLocalPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for VlanId %d\r\n", VlanId);
        FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1McastLocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    McastFwdLocalPortList.pu1_OctetList = pau1McastLocalPortList;
    McastFwdLocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;
    CurrSrcIpAddr.pu1_OctetList = au1CurrSrcIpAddr;
    CurrSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    CurrGrpIpAddr.pu1_OctetList = au1CurrGrpIpAddr;
    CurrGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextSrcIpAddr.pu1_OctetList = au1NextSrcIpAddr;
    NextSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextGrpIpAddr.pu1_OctetList = au1NextGrpIpAddr;
    NextGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (nmhGetFirstIndexFsSnoopVlanIpFwdTable
        (&i4NextInstId, &u4NextVlanId, &i4NextAddrType, &NextSrcIpAddr,
         &NextGrpIpAddr, &u4NextInnerVlanId) == SNMP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
        UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }
    do
    {
        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            u4CurrVlanId = u4NextVlanId;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddrType = i4NextAddrType;
            SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            if (nmhGetNextIndexFsSnoopVlanIpFwdTable
                (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr,
                 &NextSrcIpAddr, &CurrGrpIpAddr, &NextGrpIpAddr,
                 u4CurrInnerVlanId, &u4NextInnerVlanId) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }
        }
        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tVlanId) u4NextVlanId)
            {
                i4CurrInstId = i4NextInstId;
                u4CurrVlanId = u4NextVlanId;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddrType = i4NextAddrType;

                SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                               NextSrcIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                               NextGrpIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                if (nmhGetNextIndexFsSnoopVlanIpFwdTable
                    (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
                     i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr,
                     &NextSrcIpAddr, &CurrGrpIpAddr, &NextGrpIpAddr,
                     u4CurrInnerVlanId, &u4NextInnerVlanId) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }
        }

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            u4CurrVlanId = u4NextVlanId;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddrType = i4NextAddrType;

            SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            if (nmhGetNextIndexFsSnoopVlanIpFwdTable
                (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr,
                 &NextSrcIpAddr, &CurrGrpIpAddr, &NextGrpIpAddr,
                 u4CurrInnerVlanId, &u4NextInnerVlanId) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }
        }

        nmhGetFsSnoopVlanMcastIpFwdEntryFlag (i4NextInstId, u4NextVlanId,
                                              i4NextAddrType, &NextSrcIpAddr,
                                              &NextGrpIpAddr,
                                              &i4McastFwdEntryType);
        if ((i4EntryType != i4McastFwdEntryType) && (i4EntryType != SNOOP_ZERO))
        {
            i4CurrInstId = i4NextInstId;
            u4CurrVlanId = u4NextVlanId;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddrType = i4NextAddrType;

            SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            if (nmhGetNextIndexFsSnoopVlanIpFwdTable
                (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr,
                 &NextSrcIpAddr, &CurrGrpIpAddr, &NextGrpIpAddr,
                 u4CurrInnerVlanId, &u4NextInnerVlanId) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }

        }

        if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            i1Print = SNOOP_FALSE;
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);

        }
        if (i1Print == SNOOP_FALSE)
        {
            if (SNOOP_INSTANCE_ENH_MODE (i4NextInstId) == SNOOP_DISABLE)
            {
                CliPrintf (CliHandle, "\r\n%5s%15s%14s%7s\r\n", "Vlan",
                           "Source Address", "Group Address", "Ports");
                CliPrintf (CliHandle, "%5s%15s%14s%7s\r\n", "----",
                           "--------------", "-------------", "-----");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n%11s%15s%14s%11s%7s\r\n",
                           "Outer-Vlan", "Source Address", "Group Address",
                           "Inner-Vlan", "Ports");
                CliPrintf (CliHandle, "%11s%15s%14s%11s%7s\r\n",
                           "----------", "--------------", "-------------",
                           "----------", "-----");
            }
            i1Print = SNOOP_TRUE;
        }
        nmhGetFsSnoopVlanIpFwdLocalPortList (i4NextInstId, u4NextVlanId,
                                             i4NextAddrType, &NextSrcIpAddr,
                                             &NextGrpIpAddr, u4NextInnerVlanId,
                                             &McastFwdLocalPortList);

        SnoopGetIfPortBmp ((UINT4) i4NextInstId,
                           McastFwdLocalPortList.pu1_OctetList,
                           McastFwdPortList.pu1_OctetList);

        if (SNOOP_INSTANCE_ENH_MODE (i4NextInstId) == SNOOP_DISABLE)
        {
            CliPrintf (CliHandle, "%5d", u4NextVlanId);
        }
        else
        {
            CliPrintf (CliHandle, "%11d", u4NextVlanId);
        }

#ifdef IGS_WANTED
        if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            u4TempSrcIpAddr =
                SNOOP_HTONL (*(UINT4 *) (VOID *) NextSrcIpAddr.pu1_OctetList);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempSrcIpAddr);
            CliPrintf (CliHandle, "%15s", pu1String);

            u4TempGrpIpAddr =
                SNOOP_HTONL (*(UINT4 *) (VOID *) NextGrpIpAddr.pu1_OctetList);

            CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempGrpIpAddr);
            CliPrintf (CliHandle, "%14s", pu1String);
        }
#endif

#ifdef MLDS_WANTED
        if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            SNOOP_MEM_CPY (au1Ip6Addr, NextSrcIpAddr.pu1_OctetList,
                           IPVX_IPV6_ADDR_LEN);

            CliPrintf (CliHandle, "%15s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));

            SNOOP_MEM_CPY (au1Ip6Addr, NextGrpIpAddr.pu1_OctetList,
                           IPVX_IPV6_ADDR_LEN);

            CliPrintf (CliHandle, "%14s",
                       Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
        }
#endif

        if (SNOOP_INSTANCE_ENH_MODE (i4NextInstId) != SNOOP_DISABLE)
        {
            CliPrintf (CliHandle, "%11d", u4NextInnerVlanId);
        }

        CliPrintf (CliHandle, " ");

        CliOctetToIfName (CliHandle, NULL, &McastFwdPortList,
                          SNOOP_MAX_PORTS, SNOOP_IF_PORT_LIST_SIZE,
                          35, &u4PagingStatus, 4);

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        u4IpFwdCounter++;
        i4CurrInstId = i4NextInstId;
        u4CurrVlanId = u4NextVlanId;
        u4CurrInnerVlanId = u4NextInnerVlanId;
        i4CurrAddrType = i4NextAddrType;

        SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                       NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                       NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        if (nmhGetNextIndexFsSnoopVlanIpFwdTable
            (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
             i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr, &NextSrcIpAddr,
             &CurrGrpIpAddr, &NextGrpIpAddr, u4CurrInnerVlanId,
             &u4NextInnerVlanId) != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }

        SNOOP_MEM_SET (pMcastPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle,
               " Total number of IP based forwarding entries = %d\r\n",
               u4IpFwdCounter);
    FsUtilReleaseBitList ((UINT1 *) pMcastPortList);
    UtilPlstReleaseLocalPortList (pau1McastLocalPortList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowForwardSummary                         */
/*                                                                           */
/*     DESCRIPTION      : Displays forwarding table summaryinformation       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*              i4InstId - InstanceId                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*****************************************************************************/

INT4
SnoopCliShowForwardSummary (tCliHandle CliHandle, INT4 i4InstId,
                            INT4 i4AddrType)
{
    UINT4               u4FwdEntryCount = SNOOP_ZERO;

    UNUSED_PARAM (i4AddrType);

    SNOOP_VALIDATE_INSTANCE_RET ((UINT4) i4InstId, CLI_FAILURE);

    RBTreeCount (SNOOP_INSTANCE_INFO ((UINT4) i4InstId)->IpMcastFwdEntry,
                 &u4FwdEntryCount);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle,
               " Total number of IP based forwarding entries = %d\r\n",
               u4FwdEntryCount);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowGroupSummary                         */
/*                                                                           */
/*     DESCRIPTION      : Displays forwarding table summaryinformation       */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*              i4InstId - InstanceId                     */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS                                        */
/*****************************************************************************/

INT4
SnoopCliShowGroupSummary (tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType)
{
    UINT4               u4GroupEntryCount = SNOOP_ZERO;

    UNUSED_PARAM (i4AddrType);

    SNOOP_VALIDATE_INSTANCE_RET ((UINT4) i4InstId, SNMP_FAILURE);

    RBTreeCount (SNOOP_INSTANCE_INFO ((UINT4) i4InstId)->GroupEntry,
                 &u4GroupEntryCount);

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, " Total number of Group entries = %d\r\n",
               u4GroupEntryCount);
    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowStatistics                             */
/*                                                                           */
/*     DESCRIPTION      : Displays IGS/MLDS statistics information           */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        VlanId    - Vlan Index                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
SnoopCliShowStatistics (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4AddrType, tVlanId VlanId)
{
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4SnoopStatus = 0;
    UINT4               u4RxGenQueries = 0;
    UINT4               u4RxGrpQueries = 0;
    UINT4               u4RxGrpAndSrcQueries = 0;
    UINT4               u4RxASMReports = 0;
    UINT4               u4RxSSMReports = 0;
    UINT4               u4RxIsInMsgs = 0;
    UINT4               u4RxIsExMsgs = 0;
    UINT4               u4RxToInMsgs = 0;
    UINT4               u4RxToExMsgs = 0;
    UINT4               u4RxAllowMsgs = 0;
    UINT4               u4RxBlockMsgs = 0;
    UINT4               u4RxLeaves = 0;
    UINT4               u4TxGenQueries = 0;
    UINT4               u4TxGrpQueries = 0;
    UINT4               u4TxGrpSrcQueries = 0;
    UINT4               u4TxASMReports = 0;
    UINT4               u4TxSSMReports = 0;
    UINT4               u4TxLeaves = 0;
    UINT4               u4PktsDropped = 0;
    UINT4               u4UnsuccessfulJoins = 0;
    UINT4               u4ActiveJoins = 0;
    UINT4               u4ActiveGroups = 0;
    UINT4               u4RxV1Report = 0;
    UINT4               u4RxV2Report = 0;
    UINT4               u4TxV1Report = 0;
    UINT4               u4TxV2Report = 0;
    UINT4               u4DropV1GenQueries = 0;
    UINT4               u4DropV2GenQueries = 0;
    UINT4               u4DropV3GenQueries = 0;
    UINT4               u4DropV2GrpQueries = 0;
    UINT4               u4DropV3GrpQueries = 0;
    UINT4               u4DropGrpAndSrcQueries = 0;
    UINT4               u4DropV1Reports = 0;
    UINT4               u4DropV2Reports = 0;
    UINT4               u4DropSsmIsInMsgs = 0;
    UINT4               u4DropSsmIsExMsgs = 0;
    UINT4               u4DropSsmToInMsgs = 0;
    UINT4               u4DropSsmToExMsgs = 0;
    UINT4               u4DropSsmAllowMsgs = 0;
    UINT4               u4DropSsmBlockMsgs = 0;
    UINT4               u4DropAsmLeaves = 0;
    UINT4               u4FwdGenQueries = 0;
    UINT4               u4FwdGrpQueries = 0;
    UINT1               u1IsShowAll = SNOOP_TRUE;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];

    if (nmhGetFirstIndexFsSnoopStatsTable (&i4NextInstId, &i4NextVlanId,
                                           &i4NextAddrType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    do
    {
        /* if specif switch name is given , display only the respective
         * switch information */
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopStatsTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                 i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                return CLI_SUCCESS;
            }
        }

        if (VlanId != SNOOP_INVALID_VLAN_ID)
        {
            if (VlanId != (tVlanId) i4NextVlanId)
            {
                i4CurrInstId = i4NextInstId;
                i4CurrVlanId = i4NextVlanId;
                i4CurrAddrType = i4NextAddrType;

                if (nmhGetNextIndexFsSnoopStatsTable
                    (i4CurrInstId, &i4NextInstId, i4CurrVlanId, &i4NextVlanId,
                     i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                    return CLI_SUCCESS;
                }
            }
        }

        if (i4AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;

            if (nmhGetNextIndexFsSnoopStatsTable (i4CurrInstId, &i4NextInstId,
                                                  i4CurrVlanId, &i4NextVlanId,
                                                  i4CurrAddrType,
                                                  &i4NextAddrType) ==
                SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
                return CLI_SUCCESS;
            }
        }

        nmhGetFsSnoopVlanSnoopStatus (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &i4SnoopStatus);
        if (i4SnoopStatus != SNOOP_ENABLE)
        {
            return CLI_SUCCESS;
        }

        if (i4CurrAddrType != i4NextAddrType)
        {
            i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
        }
        if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
        }
        CliPrintf (CliHandle,
                   "\r\nSnooping Statistics for VLAN %d\r\n", i4NextVlanId);

        nmhGetFsSnoopStatsRxGenQueries (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4RxGenQueries);
        nmhGetFsSnoopStatsRxGrpQueries (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4RxGrpQueries);
        nmhGetFsSnoopStatsRxGrpAndSrcQueries (i4NextInstId, i4NextVlanId,
                                              i4NextAddrType,
                                              &u4RxGrpAndSrcQueries);
        nmhGetFsSnoopStatsRxAsmReports (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4RxASMReports);
        nmhGetFsSnoopStatsRxSsmReports (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4RxSSMReports);
        nmhGetFsSnoopStatsRxSsmIsInMsgs (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4RxIsInMsgs);
        nmhGetFsSnoopStatsRxSsmIsExMsgs (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4RxIsExMsgs);
        nmhGetFsSnoopStatsRxSsmToInMsgs (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4RxToInMsgs);
        nmhGetFsSnoopStatsRxSsmToExMsgs (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4RxToExMsgs);
        nmhGetFsSnoopStatsRxSsmAllowMsgs (i4NextInstId, i4NextVlanId,
                                          i4NextAddrType, &u4RxAllowMsgs);
        nmhGetFsSnoopStatsRxSsmBlockMsgs (i4NextInstId, i4NextVlanId,
                                          i4NextAddrType, &u4RxBlockMsgs);
        nmhGetFsSnoopStatsRxAsmLeaves (i4NextInstId, i4NextVlanId,
                                       i4NextAddrType, &u4RxLeaves);
        nmhGetFsSnoopStatsTxGenQueries (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4TxGenQueries);
        nmhGetFsSnoopStatsTxGrpQueries (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4TxGrpQueries);
        nmhGetFsSnoopStatsTxGrpAndSrcQueries (i4NextInstId, i4NextVlanId,
                                              i4NextAddrType,
                                              &u4TxGrpSrcQueries);
        nmhGetFsSnoopStatsTxAsmReports (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4TxASMReports);
        nmhGetFsSnoopStatsTxSsmReports (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4TxSSMReports);
        nmhGetFsSnoopStatsTxAsmLeaves (i4NextInstId, i4NextVlanId,
                                       i4NextAddrType, &u4TxLeaves);
        nmhGetFsSnoopStatsDroppedPkts (i4NextInstId, i4NextVlanId,
                                       i4NextAddrType, &u4PktsDropped);
        nmhGetFsSnoopStatsUnsuccessfulJoins (i4NextInstId, i4NextVlanId,
                                             i4NextAddrType,
                                             &u4UnsuccessfulJoins);
        nmhGetFsSnoopStatsActiveJoins (i4NextInstId, i4NextVlanId,
                                       i4NextAddrType, &u4ActiveJoins);
        nmhGetFsSnoopStatsActiveGroups (i4NextInstId, i4NextVlanId,
                                        i4NextAddrType, &u4ActiveGroups);
        nmhGetFsSnoopStatsRxV1Report (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &u4RxV1Report);
        nmhGetFsSnoopStatsRxV2Report (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &u4RxV2Report);
        nmhGetFsSnoopStatsTxV1Report (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &u4TxV1Report);
        nmhGetFsSnoopStatsTxV2Report (i4NextInstId, i4NextVlanId,
                                      i4NextAddrType, &u4TxV2Report);
        nmhGetFsSnoopStatsDropV1GenQueries (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropV1GenQueries);
        nmhGetFsSnoopStatsDropV2GenQueries (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropV2GenQueries);
        nmhGetFsSnoopStatsDropV3GenQueries (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropV3GenQueries);
        nmhGetFsSnoopStatsDropV2GrpQueries (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropV2GrpQueries);
        nmhGetFsSnoopStatsDropV3GrpQueries (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropV3GrpQueries);
        nmhGetFsSnoopStatsDropGrpAndSrcQueries (i4NextInstId, i4NextVlanId,
                                                i4NextAddrType,
                                                &u4DropGrpAndSrcQueries);
        nmhGetFsSnoopStatsDropV1Reports (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4DropV1Reports);
        nmhGetFsSnoopStatsDropV2Reports (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4DropV2Reports);
        nmhGetFsSnoopStatsDropSsmIsInMsgs (i4NextInstId, i4NextVlanId,
                                           i4NextAddrType, &u4DropSsmIsInMsgs);
        nmhGetFsSnoopStatsDropSsmIsExMsgs (i4NextInstId, i4NextVlanId,
                                           i4NextAddrType, &u4DropSsmIsExMsgs);
        nmhGetFsSnoopStatsDropSsmToInMsgs (i4NextInstId, i4NextVlanId,
                                           i4NextAddrType, &u4DropSsmToInMsgs);
        nmhGetFsSnoopStatsDropSsmToExMsgs (i4NextInstId, i4NextVlanId,
                                           i4NextAddrType, &u4DropSsmToExMsgs);
        nmhGetFsSnoopStatsDropSsmAllowMsgs (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropSsmAllowMsgs);
        nmhGetFsSnoopStatsDropSsmBlockMsgs (i4NextInstId, i4NextVlanId,
                                            i4NextAddrType,
                                            &u4DropSsmBlockMsgs);
        nmhGetFsSnoopStatsDropAsmLeaves (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4DropAsmLeaves);
        nmhGetFsSnoopStatsFwdGenQueries (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4FwdGenQueries);
        nmhGetFsSnoopStatsFwdGrpQueries (i4NextInstId, i4NextVlanId,
                                         i4NextAddrType, &u4FwdGrpQueries);

        CliPrintf (CliHandle,
                   "  General queries received : %d\r\n", u4RxGenQueries);

        CliPrintf (CliHandle,
                   "  Group specific queries received : %d\r\n",
                   u4RxGrpQueries);

        CliPrintf (CliHandle,
                   "  Group and source specific queries received : %d\r\n",
                   u4RxGrpAndSrcQueries);

        CliPrintf (CliHandle,
                   "  ASM reports received : %d\r\n", u4RxASMReports);

        CliPrintf (CliHandle,
                   "  SSM reports received : %d\r\n", u4RxSSMReports);

        CliPrintf (CliHandle, "  V1 reports received : %d\r\n", u4RxV1Report);

        CliPrintf (CliHandle, "  V2 reports received : %d\r\n", u4RxV2Report);

        CliPrintf (CliHandle,
                   "  IS_INCLUDE messages received : %d\r\n", u4RxIsInMsgs);

        CliPrintf (CliHandle,
                   "  IS_EXCLUDE messages received : %d\r\n", u4RxIsExMsgs);

        CliPrintf (CliHandle,
                   "  TO_INCLUDE messages received : %d\r\n", u4RxToInMsgs);

        CliPrintf (CliHandle,
                   "  TO_EXCLUDE messages received : %d\r\n", u4RxToExMsgs);

        CliPrintf (CliHandle,
                   "  ALLOW messages received : %d\r\n", u4RxAllowMsgs);

        CliPrintf (CliHandle,
                   "  Block messages received : %d\r\n", u4RxBlockMsgs);

#ifdef IGS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            CliPrintf (CliHandle,
                       "  Leave messages received : %d\r\n", u4RxLeaves);
        }
#endif
#ifdef MLDS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            CliPrintf (CliHandle, "  Done messages received : %d\r\n",
                       u4RxLeaves);
        }
#endif

        CliPrintf (CliHandle,
                   "  General queries transmitted : %d\r\n", u4TxGenQueries);

        CliPrintf (CliHandle, "  Group specific queries transmitted : %d\r\n",
                   u4TxGrpQueries);

        CliPrintf (CliHandle,
                   "  Group and source specific queries transmitted : %d\r\n",
                   u4TxGrpSrcQueries);

        CliPrintf (CliHandle,
                   "  ASM reports transmitted : %d\r\n", u4TxASMReports);

        CliPrintf (CliHandle,
                   "  SSM reports transmitted : %d\r\n", u4TxSSMReports);

        CliPrintf (CliHandle,
                   "  V1 reports transmitted : %d\r\n", u4TxV1Report);

        CliPrintf (CliHandle,
                   "  V2 reports transmitted : %d\r\n", u4TxV2Report);

#ifdef IGS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
        {
            CliPrintf (CliHandle, "  Leaves transmitted : %d\r\n", u4TxLeaves);
        }

#endif
#ifdef MLDS_WANTED
        if (i4AddrType == SNOOP_ADDR_TYPE_IPV6)
        {
            CliPrintf (CliHandle,
                       "  Done messages transmitted : %d\r\n", u4TxLeaves);
        }

#endif

        CliPrintf (CliHandle,
                   "  Unsuccessful joins received count Per Vlan : %d\r\n",
                   u4UnsuccessfulJoins);

        CliPrintf (CliHandle,
                   "  Active/Successful joins received count Per Vlan: %d\r\n",
                   u4ActiveJoins);

        CliPrintf (CliHandle, "  Active Groups count: %d\r\n", u4ActiveGroups);

        CliPrintf (CliHandle, "  V1 Query dropped : %d\r\n",
                   u4DropV1GenQueries);
        CliPrintf (CliHandle, "  V2 Query dropped : %d\r\n",
                   u4DropV2GenQueries);
        CliPrintf (CliHandle, "  V3 Query dropped : %d\r\n",
                   u4DropV3GenQueries);
        CliPrintf (CliHandle, "  V2 Group Query dropped : %d\r\n",
                   u4DropV2GrpQueries);
        CliPrintf (CliHandle, "  V3 Group Query dropped : %d\r\n",
                   u4DropV3GrpQueries);
        CliPrintf (CliHandle, "  Grp and Source Query dropped : %d\r\n",
                   u4DropGrpAndSrcQueries);

        CliPrintf (CliHandle, "  V1 Report dropped : %d\r\n", u4DropV1Reports);
        CliPrintf (CliHandle, "  V2 Report dropped : %d\r\n", u4DropV2Reports);
        CliPrintf (CliHandle, "  IS_INCLUDE messages dropped : %d\r\n",
                   u4DropSsmIsInMsgs);

        CliPrintf (CliHandle, "  IS_EXCLUDE messages dropped : %d\r\n",
                   u4DropSsmIsExMsgs);

        CliPrintf (CliHandle, "  TO_INCLUDE messages dropped : %d\r\n",
                   u4DropSsmToInMsgs);

        CliPrintf (CliHandle, "  TO_EXCLUDE messages dropped : %d\r\n",
                   u4DropSsmToExMsgs);

        CliPrintf (CliHandle, "  ALLOW messages dropped : %d\r\n",
                   u4DropSsmAllowMsgs);

        CliPrintf (CliHandle, "  BLOCK messages dropped : %d\r\n",
                   u4DropSsmBlockMsgs);

        CliPrintf (CliHandle, "  Leave messages dropped : %d\r\n",
                   u4DropAsmLeaves);

        CliPrintf (CliHandle, "  Packets dropped : %d\r\n", u4PktsDropped);

        CliPrintf (CliHandle,
                   "  General queries forwarded : %d\r\n", u4FwdGenQueries);

        CliPrintf (CliHandle, "  Group specific queries forwarded : %d\r\n",
                   u4FwdGrpQueries);

        u4PagingStatus = CliPrintf (CliHandle, "\r");

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

        if (nmhGetNextIndexFsSnoopStatsTable (i4CurrInstId, &i4NextInstId,
                                              i4CurrVlanId, &i4NextVlanId,
                                              i4CurrAddrType, &i4NextAddrType)
            != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }
    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowPortCfgEntries                       */
/*                                                                           */
/*     DESCRIPTION      : Displays IGMP/MLD Snooping group information       */
/*                        for specific VLAN or all VLANS                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        VlanId    - Vlan Identifier                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowPortCfgEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4IfIndex, UINT1 u1AddressType,
                            tSnoopTag VlanId)
{
    tSnoopTag           InnerVlanId;
    UINT4               u4CurrInstId = 0;
    UINT4               u4CurrInnerVlanId = 0;
    UINT4               u4NextInnerVlanId = 0;
    UINT4               u4PrevInst = SNOOP_CLI_INVALID_CONTEXT;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrIfIndex = 0;
    INT4                i4NextIfIndex = 0;
    INT4                i4NextAddressType = 0;
    INT4                i4CurrAddressType = 0;
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    i4CurrAddressType = u1AddressType;

    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);
    u4CurrInnerVlanId = SNOOP_INNER_VLAN (VlanId);

    CliPrintf (CliHandle, "\r\nSnooping Port Configurations\r\n");
    CliPrintf (CliHandle, "----------------------------\r\n");

    /* Displays all the port configurations for the given switch */
    if (u4ContextId != SNOOP_CLI_INVALID_CONTEXT)
    {
        if (SnoopCliGetNextPortTableIndex (u4ContextId, i4CurrIfIndex,
                                           u4CurrInnerVlanId, i4CurrAddressType,
                                           &i4NextIfIndex, &u4NextInnerVlanId,
                                           &i4NextAddressType) == SNOOP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        do
        {
            u4CurrInstId = SNOOP_GLOBAL_INSTANCE (i4NextIfIndex);

            if (u4CurrInstId != u4ContextId)
            {
                i4CurrIfIndex = i4NextIfIndex;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddressType = i4NextAddressType;

                if (SnoopCliGetNextPortTableIndex
                    (u4CurrInstId, i4CurrIfIndex,
                     u4CurrInnerVlanId, i4CurrAddressType,
                     &i4NextIfIndex, &u4NextInnerVlanId,
                     &i4NextAddressType) == SNOOP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            if (u1AddressType != i4NextAddressType)
            {
                i4CurrIfIndex = i4NextIfIndex;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddressType = i4NextAddressType;

                if (SnoopCliGetNextPortTableIndex
                    (u4CurrInstId, i4CurrIfIndex,
                     u4CurrInnerVlanId, i4CurrAddressType,
                     &i4NextIfIndex, &u4NextInnerVlanId,
                     &i4NextAddressType) == SNOOP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            if (SnoopGetSwitchName ((INT4) u4ContextId, (INT4) u4PrevInst,
                                    au1TempContextName) == SNOOP_SUCCESS)
            {
                u4PrevInst = u4ContextId;
                CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
            }

            SnoopCliPrintPortCfgInformation (CliHandle, (UINT4) i4NextIfIndex,
                                             (UINT1) i4NextAddressType,
                                             (tVlanId) u4NextInnerVlanId);

            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }

            i4CurrIfIndex = i4NextIfIndex;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddressType = i4NextAddressType;
        }
        while (SnoopCliGetNextPortTableIndex
               (u4CurrInstId, i4CurrIfIndex, u4CurrInnerVlanId,
                i4CurrAddressType, &i4NextIfIndex, &u4NextInnerVlanId,
                &i4NextAddressType) == SNOOP_SUCCESS);

    }
    else if ((u4IfIndex > 0) && (u4IfIndex <= SNOOP_MAX_PORTS))
    {

        /* Displays all the port configurations for the given port */

        u4ContextId = SNOOP_GLOBAL_INSTANCE (u4IfIndex);

        i4CurrIfIndex = u4IfIndex;
        i4CurrAddressType = 0;

        if (SnoopCliGetNextPortTableIndex (u4ContextId, i4CurrIfIndex,
                                           u4CurrInnerVlanId, i4CurrAddressType,
                                           &i4NextIfIndex, &u4NextInnerVlanId,
                                           &i4NextAddressType) == SNOOP_FAILURE)
        {
            return CLI_SUCCESS;
        }

        do
        {
            if (i4NextIfIndex != (INT4) u4IfIndex)
            {
                return CLI_SUCCESS;
            }
            if ((SNOOP_INNER_VLAN (InnerVlanId) != SNOOP_PORT_CFG_INNER_VLAN_ID)
                && (SNOOP_INNER_VLAN (InnerVlanId) != u4NextInnerVlanId))
            {
                i4CurrIfIndex = i4NextIfIndex;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddressType = i4NextAddressType;

                if (SnoopCliGetNextPortTableIndex
                    (u4ContextId, i4CurrIfIndex,
                     u4CurrInnerVlanId, i4CurrAddressType,
                     &i4NextIfIndex, &u4NextInnerVlanId,
                     &i4NextAddressType) == SNOOP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    return CLI_SUCCESS;
                }
            }
            if (u1AddressType != i4NextAddressType)
            {
                i4CurrIfIndex = i4NextIfIndex;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddressType = i4NextAddressType;

                if (SnoopCliGetNextPortTableIndex
                    (u4ContextId, i4CurrIfIndex,
                     u4CurrInnerVlanId, i4CurrAddressType,
                     &i4NextIfIndex, &u4NextInnerVlanId,
                     &i4NextAddressType) == SNOOP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            if (SnoopGetSwitchName (u4ContextId, u4PrevInst,
                                    au1TempContextName) == SNOOP_SUCCESS)
            {
                u4PrevInst = u4ContextId;
                CliPrintf (CliHandle, "\r\n%s\r\n", au1TempContextName);
            }

            SnoopCliPrintPortCfgInformation (CliHandle, i4NextIfIndex,
                                             (UINT1) i4NextAddressType,
                                             (tVlanId) u4NextInnerVlanId);

            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }

            i4CurrIfIndex = i4NextIfIndex;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddressType = i4NextAddressType;
        }
        while (SnoopCliGetNextPortTableIndex
               (u4ContextId, i4CurrIfIndex, u4CurrInnerVlanId,
                i4CurrAddressType, &i4NextIfIndex, &u4NextInnerVlanId,
                &i4NextAddressType) == SNOOP_SUCCESS);

    }
    else
    {
        /* Displays all the port configurations on all the ports */
        if (SnoopUtilGetFirstPortCfg (&i4NextIfIndex, &u4NextInnerVlanId,
                                      &i4NextAddressType) == SNOOP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        do
        {
            if (i4NextAddressType != u1AddressType)
            {
                i4CurrIfIndex = i4NextIfIndex;
                u4CurrInnerVlanId = u4NextInnerVlanId;
                i4CurrAddressType = i4NextAddressType;

                if (SnoopUtilGetNextPortCfg
                    (i4CurrIfIndex, &i4NextIfIndex,
                     u4CurrInnerVlanId, &u4NextInnerVlanId,
                     i4CurrAddressType, &i4NextAddressType) == SNOOP_SUCCESS)
                {
                    continue;
                }
                else
                {
                    break;
                }
            }

            SnoopCliPrintPortCfgInformation (CliHandle, i4NextIfIndex,
                                             (UINT1) i4NextAddressType,
                                             (tVlanId) u4NextInnerVlanId);

            u4PagingStatus = CliPrintf (CliHandle, "\r\n");

            if (u4PagingStatus == CLI_FAILURE)
            {
                break;
            }

            i4CurrIfIndex = i4NextIfIndex;
            u4CurrInnerVlanId = u4NextInnerVlanId;
            i4CurrAddressType = i4NextAddressType;

        }
        while (SnoopUtilGetNextPortCfg
               (i4CurrIfIndex, &i4NextIfIndex,
                u4CurrInnerVlanId, &u4NextInnerVlanId,
                i4CurrAddressType, &i4NextAddressType) == SNOOP_SUCCESS);
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowMVlanMapping                           */
/*                                                                           */
/*     DESCRIPTION      : Displays IGMP/MLD Snooping group information       */
/*                        for specific VLAN or all VLANS                     */
/*                                                                           */
/*     INPUT            : CliHandle - CLI Handler                            */
/*                        VlanId    - Vlan Identifier                        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowMVlanMapping (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4AddrType)
{
    UINT1               au1ContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    UINT4               u4ProfileId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4RetVal = 0;
    INT4                i4NextInstId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrInstId = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4BannerPrintedInstId =
        (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4MVlanStatus = 0;

    CliPrintf (CliHandle, "\r\nMulticast VLAN Statistics\r\n");
    CliPrintf (CliHandle, "\r=========================\r\n");

    if (nmhGetFirstIndexFsSnoopVlanFilterXTable (&i4NextInstId,
                                                 &i4NextVlanId,
                                                 &i4NextAddrType)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if ((u4ContextId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4ContextId != i4NextInstId))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        if (i4NextAddrType != i4AddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        nmhGetFsSnoopVlanRowStatus (i4NextInstId, i4NextVlanId,
                                    i4NextAddrType, &i4RetVal);
        if (i4RetVal != SNOOP_ACTIVE)    /* Row Status Active */
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        nmhGetFsSnoopVlanMulticastProfileId (i4NextInstId, i4NextVlanId,
                                             i4NextAddrType, &u4ProfileId);
        if (u4ProfileId == 0)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        if (i4BannerPrintedInstId != i4NextInstId)
        {
            CliPrintf (CliHandle, "\r-------------------------------\r\n");
            if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                    au1ContextName) == SNOOP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%s\r\n", au1ContextName);
            }
            nmhGetFsSnoopMulticastVlanStatus (i4NextInstId, i4NextAddrType,
                                              &i4MVlanStatus);

            if (i4MVlanStatus == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "\r\nMulticast VLAN enabled\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r\nMulticast VLAN disabled\r\n");
            }
            CliPrintf (CliHandle, "\r\nProfile ID  -- Multicast VLAN\r\n");
            CliPrintf (CliHandle, "\r----------  -- --------------\r\n");

            i4BannerPrintedInstId = i4NextInstId;
        }

        u4PagingStatus = CliPrintf (CliHandle,
                                    "\r%10u  -- %5lu      \r\n",
                                    u4ProfileId, i4NextVlanId);
        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }
        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;
    }
    while (nmhGetNextIndexFsSnoopVlanFilterXTable (i4CurrInstId,
                                                   &i4NextInstId,
                                                   i4CurrVlanId, &i4NextVlanId,
                                                   i4CurrAddrType,
                                                   &i4NextAddrType) !=
           SNMP_FAILURE);

    CliPrintf (CliHandle, "\r-------------------------------\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssSnoopShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the IGS/MLDS debug level      */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
VOID
IssSnoopShowDebugging (tCliHandle CliHandle, UINT1 u1AddrType)
{
    INT4                i4DbgLevel = 0;
    INT4                i4TrcLevel = 0;
    UINT4               u4Instance = 0;

    u4Instance = SNOOP_GET_INSTANCE_ID (SNOOP_INSTANCE_ID);

    SNOOP_VALIDATE_ADDRESS_TYPE (u1AddrType);

    i4DbgLevel = SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType - 1].
        u4DebugOption;
    i4TrcLevel =
        (INT4) SNOOP_INSTANCE_INFO (u4Instance)->SnoopInfo[u1AddrType -
                                                           1].u4TraceOption;

    if ((i4DbgLevel == 0) && (i4TrcLevel == 0))
    {
        return;
    }

#ifdef IGS_WANTED
    if (u1AddrType == SNOOP_ADDR_TYPE_IPV4)
    {
        CliPrintf (CliHandle, "\rIGS :");
    }
#endif

#ifdef MLDS_WANTED
    if (u1AddrType == SNOOP_ADDR_TYPE_IPV6)
    {
        CliPrintf (CliHandle, "\rMLDS :");
    }
#endif

    if ((i4DbgLevel & SNOOP_DBG_INIT) == SNOOP_DBG_INIT)
    {
        CliPrintf (CliHandle, "\r\n  Init debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_RESRC) == SNOOP_DBG_RESRC)
    {
        CliPrintf (CliHandle, "\r\n  Resource debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_TMR) == SNOOP_DBG_TMR)
    {
        CliPrintf (CliHandle, "\r\n  Timer debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_SRC) == SNOOP_DBG_SRC)
    {
        CliPrintf (CliHandle, "\r\n  Source debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_GRP) == SNOOP_DBG_GRP)
    {
        CliPrintf (CliHandle, "\r\n  Group debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_QRY) == SNOOP_DBG_QRY)
    {
        CliPrintf (CliHandle, "\r\n  Query debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_VLAN) == SNOOP_DBG_VLAN)
    {
        CliPrintf (CliHandle, "\r\n  Vlan debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_PKT) == SNOOP_DBG_PKT)
    {
        CliPrintf (CliHandle, "\r\n  Packet debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_FWD) == SNOOP_DBG_FWD)
    {
        CliPrintf (CliHandle, "\r\n  Forward debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_RED) == SNOOP_DBG_RED)
    {
        CliPrintf (CliHandle, "\r\n  Redundancy debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_MGMT) == SNOOP_DBG_MGMT)
    {
        CliPrintf (CliHandle, "\r\n  Management debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_NP) == SNOOP_DBG_NP)
    {
        CliPrintf (CliHandle, "\r\n  NP debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_ENTRY) == SNOOP_DBG_ENTRY)
    {
        CliPrintf (CliHandle, "\r\n  Entry debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_EXIT) == SNOOP_DBG_EXIT)
    {
        CliPrintf (CliHandle, "\r\n  Exit debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_BUFFER) == SNOOP_DBG_BUFFER)
    {
        CliPrintf (CliHandle, "\r\n  Buffer debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_ICCH) == SNOOP_DBG_ICCH)
    {
        CliPrintf (CliHandle, "\r\n  ICCH debugging is on");
    }
    if ((i4DbgLevel & SNOOP_DBG_ALL_FAILURE) == SNOOP_DBG_ALL_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n  All failure trace is on");
    }
    if ((i4TrcLevel & SNOOP_TRC_DATA) == SNOOP_TRC_DATA)
    {
        CliPrintf (CliHandle, "\r\n  Data  trace is on");
    }
    if ((i4TrcLevel & SNOOP_TRC_CONTROL) == SNOOP_TRC_CONTROL)
    {
        CliPrintf (CliHandle, "\r\n  Control trace is on");
    }
    if ((i4TrcLevel & SNOOP_TRC_Tx) == SNOOP_TRC_Tx)
    {
        CliPrintf (CliHandle, "\r\n  Tx trace is on");
    }
    if ((i4TrcLevel & SNOOP_TRC_Rx) == SNOOP_TRC_Rx)
    {
        CliPrintf (CliHandle, "\r\n  Rx trace is on");
    }

    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliUpdateVlanEntry                            */
/*                                                                           */
/*     DESCRIPTION      : This function creates or updates a VLAN entry      */
/*                                                                           */
/*     INPUT            : u4InstId    - Instance Identifier                  */
/*                        VlanId      - VLAN Identifier                      */
/*                        u1Status    - SNOOP_CREATE/SNOOP_SET_ACTIVE        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliUpdateVlanEntry (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                         INT4 i4AddrType, UINT1 u1Status)
{
    UINT4               u4ErrCode = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Status = 0;

    /* Check if the VlanId already exists in the database. If the Get
     * function returns Success, for CREATE and WAIT then a user already
     * exists. Otherwise we need to create an entry.
     */
    nmhGetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType, &i4RowStatus);

    switch (u1Status)
    {
        case SNOOP_CREATE:

            if (i4RowStatus == SNOOP_INIT_VAL)
            {
                i4Status = SNOOP_CREATE_AND_WAIT;
            }
            else
            {
                i4Status = SNOOP_NOT_IN_SERVICE;
            }
            break;

        case SNOOP_DELETE:

            if (i4RowStatus == SNOOP_NOT_READY)
            {
                /* Newly created Vlan entry, so deleting the entry */
                i4Status = SNOOP_DESTROY;
            }
            else
            {
                /* Vlan entry already exist, so make the entry active 
                 * with the old value.*/
                i4Status = SNOOP_ACTIVE;
            }
            break;

        case SNOOP_SET_ACTIVE:

            i4Status = SNOOP_ACTIVE;
            break;

        default:
            CliPrintf (CliHandle,
                       "\r%% Invalid Rowstatus for the Snoop VLAN table\r\n");
            return CLI_FAILURE;
    }
    if (nmhTestv2FsSnoopVlanRowStatus (&u4ErrCode, i4InstId, i4VlanId,
                                       i4AddrType, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanRowStatus (i4InstId, i4VlanId, i4AddrType,
                                    i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliUpdatePortCfgEntry                         */
/*                                                                           */
/*     DESCRIPTION      : This function creates or updates a Port Cfg entry  */
/*                                                                           */
/*     INPUT            : u4IfIndex         - Interface index                */
/*                        SnoopInnerVlanId  -     Inner vlan identifier        */
/*                        u1AddrType        - SNOOP_ADDR_TYPE_IPV4 /         */
/*                            SNOOP_ADDR_TYPE_IPV6           */
/*                           u1Status    - SNOOP_CREATE/SNOOP_SET_ACTIVE     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliUpdatePortCfgEntry (tCliHandle CliHandle, UINT4 u4IfIndex,
                            tSnoopTag VlanId, UINT1 u1AddrType, UINT1 u1Status)
{
    tSnoopTag           InnerVlanId;
    UINT4               u4ErrCode = 0;
    UINT4               u4InnerVlanId;
    UINT4               u4Instance = 0;
    INT4                i4RowStatus = 0;
    INT4                i4Status = 0;
    INT4                i4Result = 0;
    INT1                i1RetVal = SNOOP_SUCCESS;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    SNOOP_INNER_VLAN (InnerVlanId) = SNOOP_INNER_VLAN (VlanId);

    u4InnerVlanId = SNOOP_INNER_VLAN (VlanId);
    u4Instance = SNOOP_GLOBAL_INSTANCE (u4IfIndex);

    if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE) &&
        (u4InnerVlanId > 0))
    {
        CLI_SET_ERR (CLI_SNOOP_INVALID_MODE_DEF_ERR);
        return CLI_FAILURE;
    }

    /* Check if the port config entry already exists for the given indices in the database. 
     * If the Get function returns Success, for CREATE and WAIT then a user already
     * exists. Otherwise we need to create an entry.
     */
    i1RetVal =
        nmhGetFsSnoopEnhPortRowStatus (u4IfIndex, u4InnerVlanId, u1AddrType,
                                       &i4RowStatus);
    UNUSED_PARAM (i1RetVal);

    switch (u1Status)
    {
        case SNOOP_CREATE_AND_GO:
            if (i4RowStatus == SNOOP_INIT_VAL)
            {
                i4Status = SNOOP_CREATE_AND_GO;
            }
            else
            {
                i4Status = SNOOP_NOT_IN_SERVICE;
            }
            break;

        case SNOOP_DELETE:
            if (i4RowStatus != SNOOP_NOT_IN_SERVICE)
            {
                /* Newly created port cfg entry so delete 
                   it */
                i4Status = SNOOP_DESTROY;
            }
            else
            {
                /* Already port cfg entry exist
                   so make entry active with old value */
                i4Status = SNOOP_ACTIVE;
            }
            break;

        case SNOOP_SET_ACTIVE:
            i4Status = SNOOP_ACTIVE;
            break;

        default:
            CliPrintf (CliHandle,
                       "\r%% Invalid Rowstatus for the Snoop Port"
                       "Config table\r\n");
            return CLI_FAILURE;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE)
    {
        i4Result = nmhTestv2FsSnoopPortRowStatus (&u4ErrCode, u4IfIndex,
                                                  u1AddrType, i4Status);
    }
    else
    {
        i4Result = nmhTestv2FsSnoopEnhPortRowStatus (&u4ErrCode, u4IfIndex,
                                                     u4InnerVlanId, u1AddrType,
                                                     i4Status);
    }
    if (i4Result == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopEnhPortRowStatus (u4IfIndex, u4InnerVlanId, u1AddrType,
                                       i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintBlkPortList                           */
/*                                                                           */
/*     DESCRIPTION      : This function updates the portlist for printing    */
/*                                                                           */
/*     INPUT            : VlanBlkRouterPortList - Vlan Blocked Router port   */
/*                                                                 list      */
/*                                                                           */
/*     OUTPUT           : pu1PortList - Updated port list                    */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliPrintBlkPortList (tCliHandle CliHandle, INT4 i4InstId,
                          tSNMP_OCTET_STRING_TYPE BlockedVlanRtrPortList)
{
    UINT4               u4Port = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Len = 0;
    UINT1               u1FirstPrint = SNOOP_TRUE;
    BOOL1               bResult = OSIX_FALSE;

    UNUSED_PARAM (i4InstId);

    SNOOP_MEM_SET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS; u4Port++)
    {
        OSIX_BITLIST_IS_BIT_SET (BlockedVlanRtrPortList.pu1_OctetList,
                                 u4Port, SNOOP_IF_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {

            if (SnoopCfaCliGetIfName (u4Port, (INT1 *) au1NameStr)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);
                break;
            }
            if (u1FirstPrint == SNOOP_TRUE)
            {
                CliPrintf (CliHandle, "  %s", au1NameStr);
                u1FirstPrint = SNOOP_FALSE;
            }
            else
            {
                CliPrintf (CliHandle, ",  %s", au1NameStr);
            }
            u1Len += STRLEN (au1NameStr) + 10;
        }
        if (u1Len >= 42)
        {
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "%5s", " ");
            u1Len = 0;
            u1FirstPrint = SNOOP_TRUE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintPortList                                */
/*                                                                           */
/*     DESCRIPTION      : This function updates the portlist for printing    */
/*                                                                           */
/*     INPUT            : VlanRouterPortList - Vlan Router port list         */
/*                      : StaticVlanRtrPortList - Vlan static Router port    */
/*                        list                                               */
/*                                                                           */
/*     OUTPUT           : pu1PortList - Updated port list                    */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliPrintPortList (tCliHandle CliHandle, INT4 i4InstId,
                       tSNMP_OCTET_STRING_TYPE VlanRouterPortList,
                       tSNMP_OCTET_STRING_TYPE StaticVlanRtrPortList,
                       UINT4 u4VlanId, INT4 i4AddressType,
                       UINT2 *pu2NewLineFlag)
{
    tSnoopRtrPortEntry  SnoopRtrPortEntry;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tSnoopVlanEntry     SnoopVlanEntry;
    UINT4               u4Port = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Len = 0;
    UINT1               u1FirstPrint = SNOOP_TRUE;
    BOOL1               bResult = OSIX_FALSE;

    UNUSED_PARAM (i4InstId);

    SNOOP_MEM_SET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&SnoopRtrPortEntry, 0, sizeof (tSnoopRtrPortEntry));
    MEMSET (&SnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    SnoopVlanEntry.u1AddressType = (UINT1) i4AddressType;
    SNOOP_OUTER_VLAN (SnoopVlanEntry.VlanId) = (tVlanId) u4VlanId;
    SnoopRtrPortEntry.pVlanEntry = &SnoopVlanEntry;

    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS; u4Port++)
    {
        if ((CliIsMemberPort (VlanRouterPortList.pu1_OctetList,
                              VlanRouterPortList.i4_Length, u4Port)
             == OSIX_SUCCESS) ||
            (CliIsMemberPort (StaticVlanRtrPortList.pu1_OctetList,
                              StaticVlanRtrPortList.i4_Length, u4Port)
             == OSIX_SUCCESS))
        {
            SnoopRtrPortEntry.u4PhyPortIndex = u4Port;
            pSnoopRtrPortEntry =
                (tSnoopRtrPortEntry *) RBTreeGet (gRtrPortTblRBRoot,
                                                  (tRBElem *) &
                                                  SnoopRtrPortEntry);
            if (SnoopCfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);
                *pu2NewLineFlag = SNOOP_TRUE;
                return CLI_FAILURE;
            }

            OSIX_BITLIST_IS_BIT_SET (StaticVlanRtrPortList.pu1_OctetList,
                                     u4Port, SNOOP_IF_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                if (u1FirstPrint == SNOOP_TRUE)
                {
                    CliPrintf (CliHandle, "  %s%s", au1NameStr, "(static)");
                    u1FirstPrint = SNOOP_FALSE;
                    *pu2NewLineFlag = SNOOP_FALSE;
                }
                else
                {
                    CliPrintf (CliHandle, ", %s%s", au1NameStr, "(static)");
                    *pu2NewLineFlag = SNOOP_FALSE;
                }
                u1Len += STRLEN (au1NameStr) + 10;
            }
            if ((pSnoopRtrPortEntry != NULL) &&
                (pSnoopRtrPortEntry->u1IsDynamic == SNOOP_TRUE))
            {
                if (u1FirstPrint == SNOOP_TRUE)
                {
                    CliPrintf (CliHandle, "  %s%s", au1NameStr, "(dynamic)");
                    u1FirstPrint = SNOOP_FALSE;
                    *pu2NewLineFlag = SNOOP_FALSE;
                }
                else
                {
                    if (bResult == OSIX_TRUE)
                    {
                        CliPrintf (CliHandle, ", %s", "(dynamic)");
                        *pu2NewLineFlag = SNOOP_FALSE;
                    }
                    else
                    {
                        CliPrintf (CliHandle, ", %s%s", au1NameStr,
                                   "(dynamic)");
                        *pu2NewLineFlag = SNOOP_FALSE;
                    }
                }
                u1Len += STRLEN (au1NameStr) + 12;
            }
        }
        if (u1Len >= 42)
        {
            CliPrintf (CliHandle, "\r\n");
            CliPrintf (CliHandle, "%5s", " ");
            u1FirstPrint = SNOOP_TRUE;
            *pu2NewLineFlag = SNOOP_TRUE;
            u1Len = 0;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintPortListInfo                          */
/*                                                                           */
/*     DESCRIPTION      : This function prints the router ports, operating   */
/*                        version, older version querier Interval & v3       */
/*                        querier interval associated with each rtr port     */
/*                                                                           */
/*     INPUT            : VlanRouterPortList - Vlan Router port list         */
/*                      : StaticVlanRtrPortList - Vlan static Router port    */
/*                        list                                               */
/*                      : u4VlanId - Vlan ID                                 */
/*                      : i4AddrType - Address Type                          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliPrintPortListInfo (tCliHandle CliHandle, INT4 i4InstId,
                           tSNMP_OCTET_STRING_TYPE VlanRouterPortList,
                           tSNMP_OCTET_STRING_TYPE StaticVlanRtrPortList,
                           UINT4 u4VlanId, INT4 i4AddressType)
{
    tSnoopRtrPortEntry  SnoopRtrPortEntry;
    tSnoopRtrPortEntry *pSnoopRtrPortEntry = NULL;
    tSnoopVlanEntry     SnoopVlanEntry;
    UINT4               u4Port = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    BOOL1               bResult = OSIX_FALSE;
    INT4                i4OperVersion = 0;
    INT4                i4V3QuerierInt = 0;
    INT4                i4OldQuerierInt = 0;
    UINT1               u1DynPresent = SNOOP_FALSE;
    UINT1               u1PrintVersion = SNOOP_FALSE;
    UINT1               u1StaticPresent = SNOOP_FALSE;

    UNUSED_PARAM (i4InstId);

    SNOOP_MEM_SET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    MEMSET (&SnoopRtrPortEntry, 0, sizeof (tSnoopRtrPortEntry));
    MEMSET (&SnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    SnoopVlanEntry.u1AddressType = (UINT1) i4AddressType;
    SNOOP_OUTER_VLAN (SnoopVlanEntry.VlanId) = (tVlanId) u4VlanId;
    SnoopRtrPortEntry.pVlanEntry = &SnoopVlanEntry;

    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS; u4Port++)
    {
        if ((CliIsMemberPort (VlanRouterPortList.pu1_OctetList,
                              VlanRouterPortList.i4_Length, u4Port)
             == OSIX_SUCCESS) ||
            (CliIsMemberPort (StaticVlanRtrPortList.pu1_OctetList,
                              StaticVlanRtrPortList.i4_Length, u4Port)
             == OSIX_SUCCESS))

        {
            SnoopRtrPortEntry.u4PhyPortIndex = u4Port;
            pSnoopRtrPortEntry =
                (tSnoopRtrPortEntry *) RBTreeGet (gRtrPortTblRBRoot,
                                                  (tRBElem *) &
                                                  SnoopRtrPortEntry);
            if (SnoopCfaCliGetIfName (u4Port, (INT1 *) au1NameStr) ==
                CLI_FAILURE)
            {
                CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4Port);
                return CLI_FAILURE;
            }
            if (nmhGetFsSnoopRtrPortOperVersion (u4Port, u4VlanId,
                                                 i4AddressType,
                                                 &i4OperVersion) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsSnoopOlderQuerierInterval (u4Port, u4VlanId,
                                                   i4AddressType,
                                                   &i4OldQuerierInt) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (nmhGetFsSnoopV3QuerierInterval (u4Port, u4VlanId,
                                                i4AddressType,
                                                &i4V3QuerierInt) !=
                SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%5ld", u4VlanId);

            OSIX_BITLIST_IS_BIT_SET (StaticVlanRtrPortList.pu1_OctetList,
                                     u4Port, SNOOP_IF_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
                u1StaticPresent = SNOOP_TRUE;
                CliPrintf (CliHandle, "  %s%s", au1NameStr, "(static) ");
            }

#ifdef RM_WANTED
            /*The below changes are made to check whether the node is in 
             * Standby state . If the router port is both static as well  
             * as dynamic then only static is displayed.
             * Note: Standby Node will not display the dual state
             * of mrouter ports such as static/dynamic. */
            if (SNOOP_NODE_STATUS () == SNOOP_STANDBY_NODE)
            {
                /*Check if mrouter port is not static. 
                 * So the port must be dynamic only*/
                if (pSnoopRtrPortEntry->u1IsDynamic == SNOOP_TRUE)
                {
                    u1DynPresent = SNOOP_TRUE;
                    if (bResult == OSIX_FALSE)
                    {
                        CliPrintf (CliHandle, ", %s%s", au1NameStr,
                                   "(dynamic)");
                    }
                    else
                    {
                        CliPrintf (CliHandle, ",%s", "(dynamic)");
                    }
                }
            }
            else
#endif
            if (pSnoopRtrPortEntry != NULL)
            {
                if ((pSnoopRtrPortEntry->V1RtrPortPurgeTimer.u1TimerType
                     != SNOOP_INVALID_TIMER_TYPE) ||
                    (pSnoopRtrPortEntry->V2RtrPortPurgeTimer.u1TimerType
                     != SNOOP_INVALID_TIMER_TYPE) ||
                    (pSnoopRtrPortEntry->V3RtrPortPurgeTimer.u1TimerType
                     != SNOOP_INVALID_TIMER_TYPE))
                {
                    u1DynPresent = SNOOP_TRUE;
                    if (bResult == OSIX_TRUE)
                    {
                        CliPrintf (CliHandle, ", %s", "(dynamic)");
                        CliPrintf (CliHandle, "          v%d", i4OperVersion);
                        u1PrintVersion = SNOOP_TRUE;
                    }
                    else
                    {
                        CliPrintf (CliHandle, "  %s%s", au1NameStr,
                                   "(dynamic)");
                    }
                }
            }
            if ((bResult != OSIX_TRUE) && (u1DynPresent != SNOOP_TRUE))
            {
                CliPrintf (CliHandle, "               ");
            }

            if (((u1StaticPresent != SNOOP_TRUE)
                 && (u1DynPresent == SNOOP_TRUE))
                || ((u1StaticPresent == SNOOP_TRUE)
                    && (u1DynPresent != SNOOP_TRUE)))
            {
                CliPrintf (CliHandle, "           ");
            }
            if (u1PrintVersion != SNOOP_TRUE)
            {
                CliPrintf (CliHandle, "                     v%d",
                           i4OperVersion);
            }
            CliPrintf (CliHandle, "%15d Seconds", i4OldQuerierInt);
            CliPrintf (CliHandle, "%11d Seconds\r\n", i4V3QuerierInt);

        }
        u1DynPresent = SNOOP_FALSE;
        u1PrintVersion = SNOOP_FALSE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintGroupInformation                      */
/*                                                                           */
/*     DESCRIPTION      : This function prints the group inforamtion         */
/*                                                                           */
/*     INPUT            : u4Instance - Instance Identifier                   */
/*                        pSnoopGroupEntry - pointer to the group entry      */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
SnoopCliPrintGroupInformation (tCliHandle CliHandle, UINT4 u4Instance,
                               tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSNMP_OCTET_STRING_TYPE ASMPhyPortList;
    tSnoopIfPortBmp    *pASMPhyPortList = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
#ifdef IGS_WANTED
    CHR1               *pu1String = NULL;
#endif
    CHR1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    /* Since this macro return ther will be memory leak. so this is validate here */
    SNOOP_VALIDATE_INSTANCE (u4Instance);
    pASMPhyPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pASMPhyPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return;
    }

    SNOOP_MEM_SET (pASMPhyPortList, 0, SNOOP_IF_PORT_LIST_SIZE);

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);

    ASMPhyPortList.pu1_OctetList = *pASMPhyPortList;
    ASMPhyPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;

    SNOOP_VALIDATE_INSTANCE (u4Instance);

#ifdef IGS_WANTED
    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                   SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                      GroupIpAddr.au1Addr));
        CliPrintf (CliHandle, "Outer-VLAN ID:%d  Group Address: %s "
                   "Inner-VLAN ID:%d\r\n",
                   SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId), pu1String,
                   SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId));
    }
#endif
#ifdef MLDS_WANTED
    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        SNOOP_MEM_CPY (au1Ip6Addr, pSnoopGroupEntry->GroupIpAddr.au1Addr,
                       IPVX_IPV6_ADDR_LEN);

        SNOOP_INET_HTONL (au1Ip6Addr);

        CliPrintf (CliHandle, "Outer-VLAN ID:%d  Group Address: %s "
                   "Inner-VLAN ID:%d \r\n",
                   SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr),
                   SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId));
    }
#endif

    if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
    {
        CliPrintf (CliHandle, "Filter Mode: INCLUDE\r\n");
        if (SNOOP_MEM_CMP (pSnoopGroupEntry->InclSrcBmp, gNullSrcBmp,
                           SNOOP_SRC_LIST_SIZE) == 0)
        {
            CliPrintf (CliHandle, "Include sources: None\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Include sources\r\n");
            SnoopCliPrintSources (CliHandle, u4Instance,
                                  pSnoopGroupEntry->InclSrcBmp,
                                  pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                  SNOOP_TRUE);
            CliPrintf (CliHandle, "\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "Filter Mode: EXCLUDE\r\n");
        if ((SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp, gNullSrcBmp,
                            SNOOP_SRC_LIST_SIZE) == 0) ||
            (SNOOP_MEM_CMP (pSnoopGroupEntry->ExclSrcBmp, SnoopExclBmp,
                            SNOOP_SRC_LIST_SIZE) == 0))
        {
            CliPrintf (CliHandle, "Exclude sources: None\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Exclude sources\r\n");
            SnoopCliPrintSources (CliHandle, u4Instance,
                                  pSnoopGroupEntry->ExclSrcBmp,
                                  pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                  SNOOP_TRUE);
            CliPrintf (CliHandle, "\r\n");
        }
    }

    if ((pSnoopGroupEntry->u1FilterMode == SNOOP_EXCLUDE) &&
        (SNOOP_MEM_CMP (pSnoopGroupEntry->ASMPortBitmap, gNullPortBitMap,
                        SNOOP_PORT_LIST_SIZE) != 0))
    {
        if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
        {
            CliPrintf (CliHandle, "Receiver Ports:\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "ASM Receiver Ports:\r\n");
        }

        SNOOP_MEM_SET (pASMPhyPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
        SnoopGetIfPortBmp (u4Instance, pSnoopGroupEntry->ASMPortBitmap,
                           ASMPhyPortList.pu1_OctetList);

        ASMPhyPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
        CliOctetToIfName (CliHandle, " ", &ASMPhyPortList,
                          SNOOP_MAX_PORTS, SNOOP_IF_PORT_LIST_SIZE,
                          2, &u4PagingStatus, 4);
    }

    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->SSMPortList) == 0)
    {
        CliPrintf (CliHandle, "\r\n");
        FsUtilReleaseBitList ((UINT1 *) pASMPhyPortList);
        return;
    }

    CliPrintf (CliHandle, "SSM Receiver Ports:\r\n");

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (SnoopCfaCliGetIfName
            (SNOOP_GET_IFINDEX (u4Instance, pSnoopSSMPortNode->u4Port),
             (INT1 *) au1NameStr) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                       pSnoopSSMPortNode->u4Port);
            FsUtilReleaseBitList ((UINT1 *) pASMPhyPortList);
            return;
        }

        CliPrintf (CliHandle, "  Port Number: %s\r\n", au1NameStr);

        if (SNOOP_MEM_CMP
            (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp, gNullSrcBmp,
             SNOOP_SRC_LIST_SIZE) == 0)
        {
            CliPrintf (CliHandle, "   Include sources: None\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "   Include sources: \r\n");
            SnoopCliPrintSources (CliHandle, u4Instance,
                                  pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                                  pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                  SNOOP_FALSE);
            CliPrintf (CliHandle, "\r\n");
        }

        if (pSnoopSSMPortNode->pSourceBmp != NULL)
        {
            if (SNOOP_MEM_CMP
                (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp, gNullSrcBmp,
                 SNOOP_SRC_LIST_SIZE) == 0)
            {
                CliPrintf (CliHandle, "   Exclude sources: None\r\n");
            }
            else
            {
                if (SNOOP_MEM_CMP
                    (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                     SNOOP_SRC_LIST_SIZE) == 0)
                {
                    CliPrintf (CliHandle, "   Exclude sources: None\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "   Exclude sources:\r\n");
                    SnoopCliPrintSources (CliHandle, u4Instance,
                                          pSnoopSSMPortNode->pSourceBmp->
                                          ExclSrcBmp,
                                          pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                          SNOOP_FALSE);
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }
    }

    CliPrintf (CliHandle, "\r\n");
    u4PagingStatus = (UINT4) CliPrintf (CliHandle, "\r");

    if (u4PagingStatus == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n");
        FsUtilReleaseBitList ((UINT1 *) pASMPhyPortList);
        return;
    }
    FsUtilReleaseBitList ((UINT1 *) pASMPhyPortList);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintPortCfgInformation                    */
/*                                                                           */
/*     DESCRIPTION      : This function prints the Port Confing inforamtion  */
/*                                                                           */
/*     INPUT            : u4IfIndex - Interface Index                       */
/*              u1AddressType - Address Type (IPV4/IPv6)           */
/*              InnerVlanId    -  Inner Vlan Id                 */
/*                        pSnoopPortCfgEntry - pointer to Port config entry  */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
SnoopCliPrintPortCfgInformation (tCliHandle CliHandle, UINT4 u4IfIndex,
                                 UINT1 u1AddressType, tVlanId InnerVlanId)
{
    UINT4               u4Instance = 0;
    UINT4               u4RateLimit = 0;
    UINT4               u4MaxLimit = 0;
    UINT4               u4ProfileId = 0;
    UINT4               u4MemCnt = 0;
    INT4                i4LeaveMode = 0;
    INT4                i4MaxLimitType = 0;

    u4Instance = SNOOP_GLOBAL_INSTANCE (u4IfIndex);

    CliPrintf (CliHandle, "Snooping Port Configuration for Port %d", u4IfIndex);

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, " and Inner Vlan Id %d", InnerVlanId);
    }

    if (nmhGetFsSnoopEnhPortLeaveMode (u4IfIndex, InnerVlanId,
                                       (INT4) u1AddressType, &i4LeaveMode)
        == SNMP_SUCCESS)
    {
        if (i4LeaveMode == IGS_LEAVE_EXPLICIT_TRACK)
        {
            CliPrintf (CliHandle, "\r\n  Leave Process mode"
                       " is Explicit Host Track\r\n");
        }
        else if (i4LeaveMode == IGS_LEAVE_FAST_LEAVE)
        {
            CliPrintf (CliHandle, "\r\n  Leave Process mode"
                       " is Fast Leave \r\n");
        }
        else if (i4LeaveMode == IGS_LEAVE_NORMAL_LEAVE)
        {
            CliPrintf (CliHandle, "\r\n  Leave Process mode"
                       " is Normal Leave \r\n");
        }
    }

    if (nmhGetFsSnoopEnhPortRateLimit (u4IfIndex, InnerVlanId,
                                       (INT4) u1AddressType, &u4RateLimit)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "  Rate limit on the interface is %u\r\n", u4RateLimit);
    }

    if (nmhGetFsSnoopEnhPortMaxLimitType (u4IfIndex, InnerVlanId,
                                          (INT4) u1AddressType, &i4MaxLimitType)
        == SNMP_SUCCESS)
    {
        if (i4MaxLimitType == SNOOP_PORT_LMT_TYPE_NONE)
        {
            CliPrintf (CliHandle, "  Max limit Type " "is None\r\n");
        }
        else if (i4MaxLimitType == SNOOP_PORT_LMT_TYPE_GROUPS)
        {
            CliPrintf (CliHandle, "  Max limit Type " "is Groups \r\n");
        }
        else if (i4MaxLimitType == SNOOP_PORT_LMT_TYPE_CHANNELS)
        {
            CliPrintf (CliHandle, "  Max limit Type " "is Channels \r\n");
        }
    }

    if (nmhGetFsSnoopEnhPortMaxLimit (u4IfIndex, InnerVlanId,
                                      (INT4) u1AddressType, &u4MaxLimit)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "  Max limit is %u\r\n", u4MaxLimit);
    }

    if (nmhGetFsSnoopEnhPortMemberCnt ((INT4) u4IfIndex, (UINT4) InnerVlanId,
                                       (INT4) u1AddressType, &u4MemCnt)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "  Current member count " "is %u\r\n", u4MemCnt);
    }

    if (nmhGetFsSnoopEnhPortProfileId (u4IfIndex, InnerVlanId,
                                       (INT4) u1AddressType, &u4ProfileId)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "  Profile Id is %u\r\n", u4ProfileId);
    }

    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliPrintSources                               */
/*                                                                           */
/*     DESCRIPTION      : This function prints the list of include and       */
/*                        exclude sources                                    */
/*                                                                           */
/*     INPUT            : SrcBitmap - Source Bitmap                          */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
VOID
SnoopCliPrintSources (tCliHandle CliHandle, UINT4 u4Instance,
                      tSnoopSrcBmp SrcBitmap, UINT1 u1AddressType,
                      BOOL1 bGrpPort)
{
    UINT1               u1Len = 0;
    UINT1               u1FirstPrint = SNOOP_TRUE;
    UINT2               u2Count = 0;
    BOOL1               bResult = OSIX_FALSE;
    CHR1               *pu1String = NULL;
    CHR1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);

    for (u2Count = 1; u2Count < (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
    {
        OSIX_BITLIST_IS_BIT_SET (SrcBitmap, u2Count,
                                 SNOOP_SRC_LIST_SIZE, bResult);
        if (bResult == SNOOP_TRUE)
        {
#ifdef IGS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
            {
                if (u2Count <= SNOOP_MAX_MCAST_SRCS)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1String,
                                               SNOOP_PTR_FETCH_4
                                               (SNOOP_SRC_INFO (u4Instance,
                                                                (u2Count -
                                                                 1)).SrcIpAddr.
                                                au1Addr));
                }
            }
#endif

#ifdef MLDS_WANTED
            if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1Ip6Addr,
                               SNOOP_SRC_INFO (u4Instance,
                                               (u2Count - 1)).SrcIpAddr.au1Addr,
                               IPVX_IPV6_ADDR_LEN);
                SNOOP_INET_HTONL (au1Ip6Addr);
            }
#endif

            if (u1FirstPrint == SNOOP_TRUE)
            {
                if (bGrpPort == SNOOP_FALSE)
                {
                    if (pu1String)
                    {
                        CliPrintf (CliHandle, "   %s", pu1String);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "   %s",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6Addr));
                    }
                }
                else
                {
                    if (pu1String)
                    {
                        CliPrintf (CliHandle, " %s", pu1String);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "   %s",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 au1Ip6Addr));
                    }
                }
                u1FirstPrint = SNOOP_FALSE;
            }
            else
            {
                if (pu1String)
                {
                    CliPrintf (CliHandle, ", %s", pu1String);
                }
                else
                {
                    CliPrintf (CliHandle, "   %s",
                               Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
                }
            }
            if (pu1String)
            {
                u1Len += STRLEN (pu1String);
            }
            else
            {
                u1Len += (2 * IPVX_IPV6_ADDR_LEN);
            }
        }

        if (u1Len >= 66)
        {
            CliPrintf (CliHandle, "\r\n");
            u1FirstPrint = SNOOP_TRUE;
            u1Len = 0;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current configuration of  */
/*                        Snoop Module                                       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4ContextId                                        */
/*                        u4Module                                           */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Module)
{
    UINT4               u4SysMode = 0;
    UINT1               u1AddrType = 0;
    INT4                i4RetVal = 0;

    CliRegisterLock (CliHandle, SnpLock, SnpUnLock);
    SNOOP_LOCK ();

    u4SysMode = SnoopVcmGetSystemMode (SNOOP_PROTOCOL_ID);

    if (u4Module != ISS_MLDS_SHOW_RUNNING_CONFIG)
    {
        /* ip igmp snooping enhanced-mode enable is not applicable *
         * for MLDS */
        nmhGetFsSnoopInstanceGlobalEnhancedMode (u4ContextId, &i4RetVal);

        if (i4RetVal == SNOOP_ENABLE)    /* Default is disabled state */
        {
            CliPrintf (CliHandle, "ip igmp snooping enhanced-mode enable\r\n");
        }

        nmhGetFsSnoopInstanceGlobalSparseMode (u4ContextId, &i4RetVal);

        if (i4RetVal == SNOOP_ENABLE)    /* Default is disabled state */
        {
            CliPrintf (CliHandle, "ip igmp snooping sparse-mode enable\r\n");
        }

    }

    for (u1AddrType = 1; u1AddrType <= SNOOP_MAX_PROTO; u1AddrType++)
    {
        if ((u4Module == ISS_CONTEXT_SHOW_RUNNING_CONFIG) ||
            (u4Module == ISS_SHOW_ALL_RUNNING_CONFIG) ||
            ((u4Module == ISS_IGS_SHOW_RUNNING_CONFIG) &&
             (u1AddrType == SNOOP_ADDR_TYPE_IPV4)) ||
            ((u4Module == ISS_MLDS_SHOW_RUNNING_CONFIG) &&
             (u1AddrType == SNOOP_ADDR_TYPE_IPV6)))
        {
            SnoopShowRunningConfigTable (CliHandle, u1AddrType, u4ContextId);
        }
    }
    if (u4SysMode == VCM_MI_MODE)
    {
        CliPrintf (CliHandle, "!\n");
    }
    if (u4Module == ISS_IGS_SHOW_RUNNING_CONFIG)
    {
        SnoopShowRunningConfigInterface (CliHandle, SNOOP_ADDR_TYPE_IPV4,
                                         u4ContextId);
    }
    if (u4Module == ISS_MLDS_SHOW_RUNNING_CONFIG)
    {
        SnoopShowRunningConfigInterface (CliHandle, SNOOP_ADDR_TYPE_IPV6,
                                         u4ContextId);
    }

    SNOOP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfigTable                        */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS          */
/*                          configurations based on the address type         */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType                                         */
/*                        u4ContextId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfigTable (tCliHandle CliHandle, INT4 i4AddrType,
                             UINT4 u4ContextId)
{
    INT4                i4RetVal = 0;
    UINT1               au1String[SNOOP_CSR_STR_LEN];
    INT1                i1RetVal = SNOOP_SUCCESS;

    MEMSET (au1String, 0, sizeof (au1String));
    au1String[STRLEN ("")] = '\0';

    if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
    {
        STRNCPY (au1String, "ip igmp snooping", STRLEN ("ip igmp snooping"));
        au1String[STRLEN ("ip igmp snooping")] = '\0';
    }
    else

    {
        STRNCPY (au1String, "ipv6 mld snooping", STRLEN ("ipv6 mld snooping"));
        au1String[STRLEN ("ipv6 mld snooping")] = '\0';
    }

    nmhGetFsSnoopStatus (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DISABLE)
    {
        CliPrintf (CliHandle, "%s\r\n", au1String);
    }

    i1RetVal =
        nmhGetFsSnoopProxyReportingStatus (u4ContextId, i4AddrType, &i4RetVal);
    UNUSED_PARAM (i1RetVal);

    if (i4RetVal != SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, "no %s proxy-reporting\r\n", au1String);
    }

    nmhGetFsSnoopProxyStatus (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal == SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, "%s proxy\r\n", au1String);
    }

    nmhGetFsSnoopQueryFwdOnAllPorts (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal == SNOOP_FORWARD_ALL_PORTS)
    {
        CliPrintf (CliHandle, "%s query-forward all-ports\r\n", au1String);
    }

    nmhGetFsSnoopFilterStatus (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal == SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, "%s filter\r\n", au1String);
    }

    nmhGetFsSnoopMulticastVlanStatus (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal == SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, "%s multicast-vlan enable\r\n", au1String);
    }

    nmhGetFsSnoopRouterPortPurgeInterval (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DEF_RTR_PURGE_INTERVAL)
    {
        CliPrintf (CliHandle, "%s mrouter-time-out %d\r\n", au1String,
                   i4RetVal);
    }

    nmhGetFsSnoopPortPurgeInterval (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DEF_PORT_PURGE_INTERVAL)
    {
        CliPrintf (CliHandle, "%s port-purge-interval %d\r\n",
                   au1String, i4RetVal);
    }

    nmhGetFsSnoopReportForwardInterval (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DEF_REPORT_FWD_INTERVAL)
    {
        CliPrintf (CliHandle, "%s report-suppression-interval %d\r\n",
                   au1String, i4RetVal);
    }

    nmhGetFsSnoopRetryCount (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DEF_RETRY_COUNT)
    {
        CliPrintf (CliHandle, "%s retry-count %d\r\n", au1String, i4RetVal);
    }

    nmhGetFsSnoopGrpQueryInterval (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_DEF_GRP_QUERY_INTERVAL)
    {
        CliPrintf (CliHandle, "%s group-query-interval %d\r\n", au1String,
                   i4RetVal);
    }

    nmhGetFsSnoopReportFwdOnAllPorts (u4ContextId, i4AddrType, &i4RetVal);
    if (i4RetVal != SNOOP_FORWARD_RTR_PORTS)
    {
        if (i4RetVal == SNOOP_FORWARD_ALL_PORTS)
        {
            CliPrintf (CliHandle, "%s report-forward all-ports\r\n", au1String);
        }
        else if (i4RetVal == SNOOP_FORWARD_NONEDGE_PORTS)
        {
            CliPrintf (CliHandle, "%s report-forward non-edge-ports\r\n",
                       au1String);
        }
    }

    nmhGetFsSnoopSendQueryOnTopoChange (u4ContextId, i4AddrType, &i4RetVal);

    if (i4RetVal == SNOOP_ENABLE)
    {
        CliPrintf (CliHandle, "%s send-query enable\r\n", au1String);
    }

    SnoopShowRunningConfigVlanTable (CliHandle, i4AddrType, u4ContextId, 0);
    SnoopShowRunningConfigVlanStaticMacstTable (CliHandle, (INT4) u4ContextId,
                                                i4AddrType);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfigVlanTable                    */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS          */
/*                          vlan configurations based on the address type    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType                                         */
/*                        u4ContextId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfigVlanTable (tCliHandle CliHandle, INT4 i4AddrType,
                                 UINT4 u4ContextId, UINT2 u2VlanId)
{
    UINT1              *pau1LocalPortList = NULL;
    INT4                i4NextInstanceId = 0, i4NextVlanId = 0;
    INT4                i4NextAddrType = 0, i4CurrAddrType = 0;
    INT4                i4CurrInstanceId = 0, i4CurrVlanId = 0;
    INT4                i4ModePrintFlag = 0;
    INT4                i4RetVal = 0;
    INT4                i4DefaultVersion = 0;
    INT4                i4DefaultRespCode = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT4               u4RetValue = 0;
    UINT4               u4IcclIfIndex = 0;
    UINT4               u4RobVal = 0;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE LocalPortList;
    tSnoopIfPortBmp    *pPortList = NULL;
    UINT1               au1String[SNOOP_CSR_STR_LEN];
    UINT1               au1TempString[SNOOP_CSR_STR_LEN];
    CHR1                ac1DispQuerierAddr[SNOOP_CSR_STR_LEN];
    CHR1               *pc1DispQuerierAddr = NULL;
    BOOL1               b1Print = 0;

    MEMSET (&au1TempString, 0, SNOOP_CSR_STR_LEN);
    MEMSET (&ac1DispQuerierAddr, 0, SNOOP_CSR_STR_LEN);

    pPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pPortList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        return CLI_FAILURE;
    }

    MEMSET (*pPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    PortList.pu1_OctetList = *pPortList;
    PortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    pau1LocalPortList = UtilPlstAllocLocalPortList (SNOOP_PORT_LIST_SIZE);
    if (pau1LocalPortList == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG, SNOOP_GRP_DBG,
                   "Error in allocating memory for pau1LocalPortList\r\n");
        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC, SNOOP_CONTROL_TRC,
                   "Error in allocating memory for pau1LocalPortList\r\n");
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        return CLI_FAILURE;
    }
    MEMSET (pau1LocalPortList, 0, SNOOP_PORT_LIST_SIZE);

    LocalPortList.pu1_OctetList = pau1LocalPortList;
    LocalPortList.i4_Length = SNOOP_PORT_LIST_SIZE;

    MEMSET (au1String, 0, sizeof (au1String));
    au1String[STRLEN ("")] = '\0';

    /* VLAN MODE COMMANDS */

    if (nmhGetFirstIndexFsSnoopVlanFilterTable (&i4NextInstanceId,
                                                &i4NextVlanId, &i4NextAddrType)
        != SNMP_SUCCESS)
    {
        FsUtilReleaseBitList ((UINT1 *) pPortList);
        UtilPlstReleaseLocalPortList (pau1LocalPortList);
        return CLI_SUCCESS;
    }

    SnoopIcchGetIcclIfIndex (&u4IcclIfIndex);

    do
    {
        if ((INT4) u4ContextId != i4NextInstanceId)
        {
            i4CurrInstanceId = i4NextInstanceId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        if (SnoopIcchIsIcclVlan ((UINT2) i4NextVlanId) == OSIX_SUCCESS)
        {
            i4CurrInstanceId = i4NextInstanceId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }
        if (((u2VlanId != 0) && (u2VlanId == i4NextVlanId)) || (u2VlanId == 0))
        {

            if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
            {
                STRNCPY (au1String, "ip igmp snooping",
                         STRLEN ("ip igmp snooping"));
                au1String[STRLEN ("ip igmp snooping")] = '\0';
            }
            else
            {
                STRNCPY (au1String, "ipv6 mld snooping",
                         STRLEN ("ipv6 mld snooping"));
                au1String[STRLEN ("ipv6 mld snooping")] = '\0';
            }

            STRNCPY (au1TempString, au1String, (SNOOP_CSR_STR_LEN - 1));

            if (i4NextAddrType == i4AddrType)
            {
                nmhGetFsSnoopVlanRowStatus (i4NextInstanceId, i4NextVlanId,
                                            i4NextAddrType, &i4RetVal);
                if (i4RetVal == SNOOP_ACTIVE)    /* Row Status Active */
                {
                    nmhGetFsSnoopVlanSnoopStatus (i4NextInstanceId,
                                                  i4NextVlanId,
                                                  i4NextAddrType, &i4RetVal);
                    if (i4RetVal != SNOOP_ENABLE)
                    {
                        CliPrintf (CliHandle, "Vlan %d\r\n", i4NextVlanId);
                        i4ModePrintFlag = 1;
                        CliPrintf (CliHandle, "no %s\r\n", au1String);
                    }

                    nmhGetFsSnoopVlanFastLeave (i4NextInstanceId,
                                                i4NextVlanId,
                                                i4NextAddrType, &i4RetVal);
                    if (i4RetVal == SNOOP_ENABLE)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle, "%s fast-leave\r\n", au1String);
                    }

                    nmhGetFsSnoopVlanCfgOperVersion (i4NextInstanceId,
                                                     i4NextVlanId,
                                                     i4NextAddrType, &i4RetVal);

                    i4DefaultVersion = (i4AddrType == SNOOP_ADDR_TYPE_IPV4) ?
                        SNOOP_IGS_IGMP_VERSION3 : SNOOP_MLD_VERSION_2;

                    if ((i4RetVal != 0) && (i4RetVal != i4DefaultVersion))    /* Default is IGMPv3/MLDv2 */
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle, "%s version v%d\r\n", au1String,
                                   i4RetVal);
                    }

                    nmhGetFsSnoopVlanCfgQuerier (i4NextInstanceId,
                                                 i4NextVlanId,
                                                 i4NextAddrType, &i4RetVal);

                    if (i4RetVal != SNOOP_NON_QUERIER)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        b1Print = 1;
                        STRCAT (au1TempString, " querier ");
                        nmhGetFsSnoopVlanQuerierIpFlag (i4NextInstanceId,
                                                        i4NextVlanId,
                                                        i4NextAddrType,
                                                        &i4RetVal);
                        if (i4RetVal == 1)
                        {
                            b1Print = 1;
                            STRCAT (au1TempString, "address ");

                        }
                        pc1DispQuerierAddr = &ac1DispQuerierAddr[0];
                        nmhGetFsSnoopVlanQuerierIpAddress (i4NextInstanceId,
                                                           i4NextVlanId,
                                                           i4NextAddrType,
                                                           (UINT4 *) &i4RetVal);
                        if (i4RetVal != SNOOP_INIT_VAL)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pc1DispQuerierAddr,
                                                       (UINT4) i4RetVal);
                            if (pc1DispQuerierAddr != NULL)
                            {
                                STRCAT (au1TempString, pc1DispQuerierAddr);
                            }
                            b1Print = 0;
                            CliPrintf (CliHandle, "%s   \r\n", au1TempString);
                        }
                        if (b1Print == 1)
                        {
                            CliPrintf (CliHandle, "%s   \r\n", au1TempString);
                        }
                    }

                    nmhGetFsSnoopVlanQueryInterval (i4NextInstanceId,
                                                    i4NextVlanId,
                                                    i4NextAddrType, &i4RetVal);
                    if (i4RetVal != SNOOP_DEF_QUERY_INTERVAL)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle, "%s query-interval %d\r\n",
                                   au1String, i4RetVal);
                    }

                    nmhGetFsSnoopVlanStartupQueryCount (i4NextInstanceId,
                                                        i4NextVlanId,
                                                        i4NextAddrType,
                                                        &i4RetVal);
                    if (i4RetVal != SNOOP_DEF_STARTUP_QRY_COUNT)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle, "%s startup-query-count %d\r\n",
                                   au1String, i4RetVal);
                    }

                    nmhGetFsSnoopVlanStartupQueryInterval (i4NextInstanceId,
                                                           i4NextVlanId,
                                                           i4NextAddrType,
                                                           &i4RetVal);
                    if (i4RetVal != SNOOP_DEF_STARTUP_QINTERVAL)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle,
                                   "%s startup-query-interval %d\r\n",
                                   au1String, i4RetVal);
                    }

                    nmhGetFsSnoopVlanOtherQuerierPresentInterval
                        (i4NextInstanceId, i4NextVlanId, i4NextAddrType,
                         &i4RetVal);
                    if (i4RetVal != SNOOP_OTHER_QUERIER_PRESENT_INTERVAL)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle,
                                   "%s other-querier-present-interval %d\r\n",
                                   au1String, i4RetVal);
                    }

                    nmhGetFsSnoopVlanRtrLocalPortList (i4NextInstanceId,
                                                       i4NextVlanId,
                                                       i4NextAddrType,
                                                       &LocalPortList);

                    SnoopGetIfPortBmp ((UINT4) i4NextInstanceId,
                                       LocalPortList.pu1_OctetList,
                                       PortList.pu1_OctetList);

                    OSIX_BITLIST_IS_BIT_SET (PortList.pu1_OctetList,
                                             u4IcclIfIndex,
                                             SNOOP_IF_PORT_LIST_SIZE, b1Print);
                    if (b1Print == OSIX_TRUE)
                    {
                        OSIX_BITLIST_RESET_BIT (PortList.pu1_OctetList,
                                                u4IcclIfIndex,
                                                SNOOP_IF_PORT_LIST_SIZE);
                    }
                    if (FsUtilBitListIsAllZeros (PortList.pu1_OctetList,
                                                 SNOOP_IF_PORT_LIST_SIZE)
                        == OSIX_FALSE)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }

                        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
                        {
                            CliConfOctetToIfName (CliHandle,
                                                  "ip igmp snooping mrouter ",
                                                  NULL, &PortList,
                                                  &u4PagingStatus);
                        }
                        else
                        {
                            CliConfOctetToIfName (CliHandle,
                                                  "ipv6 mld  snooping mrouter ",
                                                  NULL, &PortList,
                                                  &u4PagingStatus);
                        }
                        CliPrintf (CliHandle, "\r\n");
                        SnoopShowRunningConfigRouterPortTable (CliHandle,
                                                               i4NextVlanId,
                                                               i4AddrType,
                                                               u4ContextId);

                    }

                    MEMSET (pPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
                    MEMSET (pau1LocalPortList, SNOOP_INIT_VAL,
                            SNOOP_PORT_LIST_SIZE);
                    nmhGetFsSnoopVlanBlkRtrLocalPortList (i4NextInstanceId,
                                                          i4NextVlanId,
                                                          i4NextAddrType,
                                                          &LocalPortList);

                    SnoopGetIfPortBmp ((UINT4) i4NextInstanceId,
                                       LocalPortList.pu1_OctetList,
                                       PortList.pu1_OctetList);

                    if (FsUtilBitListIsAllZeros (PortList.pu1_OctetList,
                                                 SNOOP_IF_PORT_LIST_SIZE)
                        == OSIX_FALSE)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }

                        if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
                        {
                            CliConfOctetToIfName
                                (CliHandle, "ip igmp snooping blocked-router ",
                                 NULL, &PortList, &u4PagingStatus);
                        }
                        else
                        {
                            CliConfOctetToIfName
                                (CliHandle, "ipv6 mld snooping blocked-router ",
                                 NULL, &PortList, &u4PagingStatus);
                        }
                        CliPrintf (CliHandle, "\r\n");
                    }

                    nmhGetFsSnoopVlanMulticastProfileId (i4NextInstanceId,
                                                         i4NextVlanId,
                                                         i4NextAddrType,
                                                         &u4RetValue);
                    if (u4RetValue != SNOOP_DEF_MVLAN_PROFILEID)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf
                            (CliHandle, "%s multicast-vlan profile %u\r\n",
                             au1String, u4RetValue);
                    }
                    nmhGetFsSnoopVlanRobustnessValue (i4NextInstanceId,
                                                      i4NextVlanId,
                                                      i4NextAddrType,
                                                      &u4RobVal);

                    if (u4RobVal != SNOOP_ROBUST_MIN_VALUE)
                    {

                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle,
                                   "%s robustness-variable %d\r\n",
                                   au1String, u4RobVal);
                    }

                    nmhGetFsSnoopVlanMaxResponseTime (i4NextInstanceId,
                                                      i4NextVlanId,
                                                      i4NextAddrType,
                                                      &i4RetVal);

                    i4DefaultRespCode = (i4AddrType == SNOOP_ADDR_TYPE_IPV4) ?
                        SNOOP_IGS_DEF_MAX_RESP_CODE :
                        SNOOP_MLDS_DEF_MAX_RESP_CODE;

                    if (i4RetVal != i4DefaultRespCode)
                    {
                        if (i4ModePrintFlag != 1)
                        {
                            CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                            i4ModePrintFlag = 1;
                        }
                        CliPrintf (CliHandle,
                                   "%s max-response-code %d\r\n",
                                   au1String, i4RetVal);
                    }

                    if (i4ModePrintFlag == 1)
                    {
                        CliPrintf (CliHandle, "\r\n!\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "Vlan %d\r\n", i4NextVlanId);
                        CliPrintf (CliHandle, "%s\r\n", au1String);
                    }
                    MEMSET (au1String, 0, SNOOP_CSR_STR_LEN);
                }
            }
            if (u2VlanId != 0)
            {
                break;
            }
        }
        i4CurrInstanceId = i4NextInstanceId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;
        i4ModePrintFlag = 0;
        MEMSET (PortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
    }
    while (nmhGetNextIndexFsSnoopVlanFilterTable (i4CurrInstanceId,
                                                  &i4NextInstanceId,
                                                  i4CurrVlanId, &i4NextVlanId,
                                                  i4CurrAddrType,
                                                  &i4NextAddrType) !=
           SNMP_FAILURE);

    FsUtilReleaseBitList ((UINT1 *) pPortList);
    UtilPlstReleaseLocalPortList (pau1LocalPortList);
    return CLI_SUCCESS;

}

/********************************************************************************/
/*                                                                              */
/*     FUNCTION NAME     : SnoopShowRunnigConfigValnStaticMcastTable                 */
/*                                                                              */
/*     DESCRIPTION       : This function prints the IGS static multicast table. */
/*                                                                              */
/*     INPUT             : CliHandle                                            */
/*                         i4AddrType                                           */
/*                         i4VlanId                                             */
/*                         i4ContextId                      
 *                                                                              */
/*     OUTPUT            : None                                                 */
/*                                                                              */
/*     RETURN            : CLI_SUCCESS/CLI_FAILURE                              */
/*                                                                              */
/*******************************************************************************/
INT4
SnoopShowRunningConfigVlanStaticMacstTable (tCliHandle CliHandle,
                                            INT4 u4ContextId, INT4 i4AddrType)
{
    tSNMP_OCTET_STRING_TYPE PortList, RetPortList, FwdportList, LocalPortList;
    tSNMP_OCTET_STRING_TYPE *pCurrGrpAddr = NULL, *pNextGrpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrSrcAddr = NULL, *pNextSrcAddr = NULL;
    CHR1               *pu1String = NULL;
    UINT4               u4TempGrpIpAddr = 0;
    INT4                u4CurrInstanceId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrVlanId = 0;
    INT4                i4NextIndex = 0, i4CurrIndex = 0;
    INT4                i4RetVal = 0;
    INT4                i4Index = 0;
    INT4                i4PortFlag = CLI_FALSE;
    INT4                i4PortChannelFlag = CLI_FALSE;
    UINT4               u4PagingStatus = CLI_SUCCESS;

    i4CurrIndex = u4ContextId;

    PortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (PortList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        return CLI_FAILURE;
    }
    PortList.i4_Length = sizeof (tSnoopIfPortBmp);
    MEMSET (PortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    pCurrGrpAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pCurrGrpAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        return CLI_FAILURE;
    }

    pCurrSrcAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pCurrSrcAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        free_octetstring (pCurrGrpAddr);
        return CLI_FAILURE;
    }

    pNextGrpAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pNextGrpAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        return CLI_FAILURE;

    }

    pNextSrcAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pNextSrcAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        free_octetstring (pNextGrpAddr);
        return CLI_FAILURE;

    }

    RetPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (RetPortList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        free_octetstring (pNextGrpAddr);
        free_octetstring (pNextSrcAddr);
        return CLI_FAILURE;
    }
    RetPortList.i4_Length = sizeof (tSnoopIfPortBmp);
    MEMSET (RetPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    FwdportList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (FwdportList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        free_octetstring (pNextGrpAddr);
        free_octetstring (pNextSrcAddr);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        return CLI_FAILURE;
    }
    LocalPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (LocalPortList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for context id %d\r\n", u4ContextId);
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        free_octetstring (pNextGrpAddr);
        free_octetstring (pNextSrcAddr);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        FsUtilReleaseBitList (FwdportList.pu1_OctetList);
        return CLI_FAILURE;
    }

    FwdportList.i4_Length = sizeof (tSnoopIfPortBmp);
    LocalPortList.i4_Length = sizeof (tSnoopIfPortBmp);
    MEMSET (FwdportList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
    MEMSET (LocalPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    if (nmhGetNextIndexFsSnoopVlanStaticMcastGrpTable (i4CurrIndex,
                                                       &i4NextIndex,
                                                       i4CurrVlanId,
                                                       &i4NextVlanId,
                                                       i4AddrType,
                                                       &i4NextAddrType,
                                                       pCurrSrcAddr,
                                                       pNextSrcAddr,
                                                       pCurrGrpAddr,
                                                       pNextGrpAddr) !=
        SNMP_SUCCESS)
    {
        free_octetstring (pCurrGrpAddr);
        free_octetstring (pCurrSrcAddr);
        free_octetstring (pNextGrpAddr);
        free_octetstring (pNextSrcAddr);
        FsUtilReleaseBitList (PortList.pu1_OctetList);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        FsUtilReleaseBitList (FwdportList.pu1_OctetList);
        FsUtilReleaseBitList (LocalPortList.pu1_OctetList);
        return CLI_SUCCESS;
    }

    do
    {
        MEMSET (FwdportList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
        MEMSET (LocalPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
        i4PortFlag = CLI_FALSE;
        i4PortChannelFlag = CLI_FALSE;

        u4CurrInstanceId = SNOOP_GET_INSTANCE_ID (i4NextIndex);
        MEMSET (FwdportList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
        if (u4ContextId != u4CurrInstanceId)
        {
            free_octetstring (pCurrGrpAddr);
            free_octetstring (pCurrSrcAddr);
            free_octetstring (pNextGrpAddr);
            free_octetstring (pNextSrcAddr);
            FsUtilReleaseBitList (PortList.pu1_OctetList);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            FsUtilReleaseBitList (FwdportList.pu1_OctetList);
            FsUtilReleaseBitList (LocalPortList.pu1_OctetList);
            return CLI_SUCCESS;
            /* Free the meory u allocated */ ;
        }

        if (i4NextAddrType == i4AddrType)
        {
            nmhGetFsSnoopVlanStaticMcastGrpRowStatus (i4NextIndex, i4NextVlanId,
                                                      i4NextAddrType,
                                                      pNextSrcAddr,
                                                      pNextGrpAddr, &i4RetVal);
            if (i4RetVal != 0)
            {
                if (i4CurrVlanId != i4NextVlanId)
                {
                    if (i4CurrVlanId != 0)
                    {
                        CliPrintf (CliHandle, "!\r\n");
                    }
                    CliPrintf (CliHandle, "vlan %d\r\n", i4NextVlanId);
                }

                if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
                {
                    u4TempGrpIpAddr = (*(UINT4 *) (VOID *)
                                       pNextGrpAddr->pu1_OctetList);
                    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempGrpIpAddr);

                    nmhGetFsSnoopVlanStaticMcastGrpPortList (i4NextIndex,
                                                             i4NextVlanId,
                                                             i4NextAddrType,
                                                             pNextSrcAddr,
                                                             pNextGrpAddr,
                                                             &RetPortList);
                    SnoopGetIfPortBmp ((UINT4) i4NextIndex,
                                       RetPortList.pu1_OctetList,
                                       FwdportList.pu1_OctetList);

                    for (i4Index = 0; i4Index < SNOOP_IF_PORT_LIST_SIZE;
                         i4Index++)
                    {
                        if (i4Index < (SYS_DEF_MAX_PHYSICAL_INTERFACES / 8))
                        {
                            LocalPortList.pu1_OctetList[i4Index] =
                                FwdportList.pu1_OctetList[i4Index];
                            if ((LocalPortList.pu1_OctetList[i4Index] > 0)
                                && (i4PortFlag != CLI_TRUE))
                            {
                                i4PortFlag = CLI_TRUE;
                            }
                            FwdportList.pu1_OctetList[i4Index] =
                                (LocalPortList.
                                 pu1_OctetList[i4Index]) ^ (FwdportList.
                                                            pu1_OctetList
                                                            [i4Index]);
                        }
                        else
                        {
                            if ((FwdportList.pu1_OctetList[i4Index] > 0)
                                && (i4PortChannelFlag != CLI_TRUE))
                            {
                                i4PortChannelFlag = CLI_TRUE;
                            }
                        }
                    }

                    if (i4PortFlag != CLI_FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "ip igmp snooping static-group %s " "ports",
                                   pu1String);

                        CliOctetPortChannelToIfName (CliHandle, &LocalPortList,
                                                     SNOOP_MAX_PORTS,
                                                     SNOOP_IF_PORT_LIST_SIZE,
                                                     &u4PagingStatus);
                    }
                    if (i4PortChannelFlag != CLI_FALSE)
                    {
                        CliPrintf (CliHandle,
                                   "ip igmp snooping static-group %s " "ports",
                                   pu1String);

                        CliOctetPortChannelToIfName (CliHandle, &FwdportList,
                                                     SNOOP_MAX_PORTS,
                                                     SNOOP_IF_PORT_LIST_SIZE,
                                                     &u4PagingStatus);
                    }
                }
            }
        }

        i4CurrIndex = i4NextIndex;
        i4CurrVlanId = i4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

        MEMCPY (pCurrSrcAddr->pu1_OctetList, pNextSrcAddr->pu1_OctetList,
                pNextSrcAddr->i4_Length);
        pCurrSrcAddr->i4_Length = pNextSrcAddr->i4_Length;

        MEMCPY (pCurrGrpAddr->pu1_OctetList, pNextGrpAddr->pu1_OctetList,
                pNextSrcAddr->i4_Length);
        pCurrGrpAddr->i4_Length = pNextGrpAddr->i4_Length;

    }
    while (nmhGetNextIndexFsSnoopVlanStaticMcastGrpTable (i4CurrIndex,
                                                          &i4NextIndex,
                                                          i4CurrVlanId,
                                                          &i4NextVlanId,
                                                          i4CurrAddrType,
                                                          &i4NextAddrType,
                                                          pCurrSrcAddr,
                                                          pNextSrcAddr,
                                                          pCurrGrpAddr,
                                                          pNextGrpAddr) ==
           SNMP_SUCCESS);

    free_octetstring (pCurrGrpAddr);
    free_octetstring (pCurrSrcAddr);
    free_octetstring (pNextGrpAddr);
    free_octetstring (pNextSrcAddr);
    FsUtilReleaseBitList (PortList.pu1_OctetList);
    FsUtilReleaseBitList (RetPortList.pu1_OctetList);
    FsUtilReleaseBitList (FwdportList.pu1_OctetList);
    FsUtilReleaseBitList (LocalPortList.pu1_OctetList);
    CliPrintf (CliHandle, "!\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfigRouterPortTable              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS router   */
/*                          port configurations based on the address type    */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType                                         */
/*                        i4VlanId                                           */
/*                        i4ContextId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfigRouterPortTable (tCliHandle CliHandle, INT4 i4VlanId,
                                       INT4 i4AddrType, UINT4 u4ContextId)
{
    UINT4               u4NextVlanId = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4NextIndex = 0, i4CurrIndex = 0;
    UINT4               u4CurrVlanId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4RetVal = 0;
    INT4                i4DefaultVersion = 0;
    UINT4               u4CurrInstanceId = 0;
    INT1                ai1PortStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[SNOOP_CSR_STR_LEN];

    MEMSET (ai1PortStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    MEMSET (au1String, 0, sizeof (au1String));
    au1String[STRLEN ("")] = '\0';

    if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
    {
        STRNCPY (au1String, "ip igmp snooping", STRLEN ("ip igmp snooping"));
        au1String[STRLEN ("ip igmp snooping")] = '\0';
    }
    else
    {
        STRNCPY (au1String, "ipv6 mld snooping", STRLEN ("ipv6 mld snooping"));
        au1String[STRLEN ("ipv6 mld snooping")] = '\0';
    }

    if (nmhGetFirstIndexFsSnoopRtrPortTable (&i4NextIndex, &u4NextVlanId,
                                             &i4NextAddrType) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        if (u4NextVlanId > (UINT4) i4VlanId)
        {
            return CLI_SUCCESS;
        }
        else if ((UINT4) i4VlanId != u4NextVlanId)
        {
            i4CurrIndex = i4NextIndex;
            u4CurrVlanId = u4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }

        u4CurrInstanceId = SNOOP_GLOBAL_INSTANCE (i4NextIndex);
        if (u4ContextId != u4CurrInstanceId)
        {
            i4CurrIndex = i4NextIndex;
            u4CurrVlanId = u4NextVlanId;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }

        if (i4NextAddrType == i4AddrType)
        {
            CfaCliConfGetIfName (i4NextIndex, (INT1 *) ai1PortStr);

            nmhGetFsSnoopRtrPortCfgOperVersion (i4NextIndex, u4NextVlanId,
                                                i4NextAddrType, &i4RetVal);

            i4DefaultVersion = (i4AddrType == SNOOP_ADDR_TYPE_IPV4) ?
                SNOOP_IGS_IGMP_VERSION3 : SNOOP_MLD_VERSION_2;

            if ((i4RetVal != i4DefaultVersion) && (i4RetVal != 0))
            {
                CliPrintf (CliHandle, "%s mrouter-port %s version v%d\r\n",
                           au1String, ai1PortStr, i4RetVal);
            }
        }
        i4CurrIndex = i4NextIndex;
        u4CurrVlanId = u4NextVlanId;
        i4CurrAddrType = i4NextAddrType;

    }
    while (nmhGetNextIndexFsSnoopRtrPortTable
           (i4CurrIndex, &i4NextIndex, u4CurrVlanId, &u4NextVlanId,
            i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfigInterface                    */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS          */
/*                        interface configurations based on the address type */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType                                         */
/*                        u4ContextId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfigInterface (tCliHandle CliHandle, INT4 i4AddrType,
                                 UINT4 u4ContextId)
{
    INT4                i4NextIndex = 0, i4NextAddrType = 0;
    INT4                i4CurrIndex = 0, i4CurrAddrType = 0;
    UINT4               u4CurrInnerVlan = 0, u4NextInnerVlan = 0;
    UINT4               u4CurrInstance = 0;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];

    SNOOP_MEM_SET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhGetFirstIndexFsSnoopPortTable (&i4NextIndex, &i4NextAddrType)
        != SNMP_FAILURE)
    {
        do
        {
            u4CurrInstance = SNOOP_GLOBAL_INSTANCE (i4NextIndex);

            if (u4CurrInstance != u4ContextId)
            {
                i4CurrIndex = i4NextIndex;
                i4CurrAddrType = i4NextAddrType;
                continue;
            }

            if (i4AddrType == i4NextAddrType)
            {
                CfaCliConfGetIfName (i4NextIndex, (INT1 *) au1NameStr);
                CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);

                SNOOP_UNLOCK ();
                CliUnRegisterLock (CliHandle);

                SnoopShowRunningConfigInterfaceDetails (CliHandle, i4NextIndex,
                                                        i4NextAddrType);

                CliRegisterLock (CliHandle, SnpLock, SnpUnLock);
                SNOOP_LOCK ();

                CliPrintf (CliHandle, "!\r\n");
            }

            i4CurrIndex = i4NextIndex;
            i4CurrAddrType = i4NextAddrType;

        }
        while (nmhGetNextIndexFsSnoopPortTable
               (i4CurrIndex, &i4NextIndex, i4CurrAddrType,
                &i4NextAddrType) != SNMP_FAILURE);
    }

    if (nmhGetFirstIndexFsSnoopEnhPortTable (&i4NextIndex, &u4NextInnerVlan,
                                             &i4NextAddrType) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    do
    {
        u4CurrInstance = SNOOP_GLOBAL_INSTANCE (i4NextIndex);

        if (u4CurrInstance != u4ContextId)
        {
            i4CurrIndex = i4NextIndex;
            u4CurrInnerVlan = u4NextInnerVlan;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }

        if (i4NextAddrType != i4AddrType)
        {
            i4CurrIndex = i4NextIndex;
            u4CurrInnerVlan = u4NextInnerVlan;
            i4CurrAddrType = i4NextAddrType;
            continue;
        }

        CfaCliConfGetIfName (i4NextIndex, (INT1 *) au1NameStr);
        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);

        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);

        SnoopShowRunningConfigInterfaceDetails (CliHandle, i4NextIndex,
                                                i4NextAddrType);

        CliRegisterLock (CliHandle, SnpLock, SnpUnLock);
        SNOOP_LOCK ();

        CliPrintf (CliHandle, "!\r\n");

        /* The SnoopShowRunningConfigInterfaceDetails displays the 
         * information of all the configurations made for multiple
         * inner vlans for the given interface index. Hence, we need to
         * obtain the configurations made on the next interface index. */

        i4CurrIndex = i4NextIndex + 1;
        u4CurrInnerVlan = SNOOP_PORT_CFG_DEF_MODE_CVLAN;
        i4CurrAddrType = 0;

    }
    while (nmhGetNextIndexFsSnoopEnhPortTable
           (i4CurrIndex, &i4NextIndex, u4CurrInnerVlan, &u4NextInnerVlan,
            i4CurrAddrType, &i4NextAddrType) == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopShowRunningConfigInterfaceDetails             */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS          */
/*                        configurations based on the address type           */
/*                        for the given interface.                           */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4AddrType                                         */
/*                        i4Index                                            */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4Index,
                                        INT4 i4AddrType)
{
    INT4                i4NextIndex = 0, i4NextAddrType = 0;
    INT4                i4CurrIndex = 0, i4CurrAddrType = 0;
    UINT4               u4CurrInnerVlan, u4NextInnerVlan;
    UINT2               u2LocalPort = 0;

    CliRegisterLock (CliHandle, SnpLock, SnpUnLock);
    SNOOP_LOCK ();

    /* The function can be called from SnoopShowRunningConfigInterface
     * and InterfaceShowRunningConfig. Interface index is validated
     * here and returned */
    if ((i4Index <= 0) || (i4Index > SNOOP_MAX_PORTS))
    {
        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    u2LocalPort = SNOOP_GLOBAL_PORT (i4Index);
    if (u2LocalPort == 0)
    {
        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    /* A context can be either in default mode or in enhanced mode.
     * Hence, an index can belong to either the FsSnoopPortTable or
     * FsSnoopEnhPortTable */

    /* FsSnoopPortTable contains a single config entry for a given
     * index and FsSnoopEnhPortTable can contain multiple config 
     * entries across different inner vlans for a given index */

    if (nmhValidateIndexInstanceFsSnoopPortTable (i4Index, i4AddrType)
        == SNMP_SUCCESS)
    {
        SnoopDisplayPortConfigurations (CliHandle, i4Index,
                                        SNOOP_PORT_CFG_DEF_MODE_CVLAN,
                                        i4AddrType);
    }
    else if (nmhGetNextIndexFsSnoopEnhPortTable
             (i4Index, &i4NextIndex, SNOOP_PORT_CFG_DEF_MODE_CVLAN,
              &u4NextInnerVlan, i4CurrAddrType, &i4NextAddrType)
             == SNMP_SUCCESS)
    {
        do
        {
            if (i4Index < i4NextIndex)
            {
                break;
            }
            if ((i4Index == i4NextIndex) && (i4AddrType == i4NextAddrType))
            {
                SnoopDisplayPortConfigurations (CliHandle, i4NextIndex,
                                                u4NextInnerVlan,
                                                i4NextAddrType);
            }

            i4CurrIndex = i4NextIndex;
            u4CurrInnerVlan = u4NextInnerVlan;
            i4CurrAddrType = i4NextAddrType;

        }
        while (nmhGetNextIndexFsSnoopEnhPortTable
               (i4CurrIndex, &i4NextIndex, u4CurrInnerVlan,
                &u4NextInnerVlan, i4CurrAddrType, &i4NextAddrType)
               == SNMP_SUCCESS);
    }

    SNOOP_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopDisplayPortConfigurations                     */
/*                                                                           */
/*     DESCRIPTION      : This function prints the current IGS/MLDS          */
/*                        configurations based on the address type           */
/*                        for the given logical interface.                   */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        i4Index                                            */
/*                        u4InnerVlan                                        */
/*                        i4AddrType                                         */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopDisplayPortConfigurations (tCliHandle CliHandle, INT4 i4Index,
                                UINT4 u4InnerVlan, INT4 i4AddrType)
{
    INT4                i4RetVal = 0;
    UINT4               u4RetValue = 0;
    UINT1               au1String[SNOOP_CSR_STR_LEN];
    INT1                i1RetValue = 0;

    MEMSET (au1String, 0, sizeof (au1String));
    au1String[STRLEN ("")] = '\0';

    if (i4AddrType == SNOOP_ADDR_TYPE_IPV4)
    {
        STRNCPY (au1String, "ip igmp snooping", STRLEN ("ip igmp snooping"));
        au1String[STRLEN ("ip igmp snooping")] = '\0';
    }
    else
    {
        STRNCPY (au1String, "ipv6 mld snooping", STRLEN ("ipv6 mld snooping"));
        au1String[STRLEN ("ipv6 mld snooping")] = '\0';
    }

    /* As the FsSnoopPortTable get routines use the enhanced Port table
     * get routines, the enhanced table get routines are called for 
     * both FsSnoopPortTable and FsSnoopEnhPortTable */

    if (nmhGetFsSnoopEnhPortLeaveMode (i4Index, u4InnerVlan,
                                       i4AddrType, &i4RetVal) == SNMP_SUCCESS)
    {
        if (i4RetVal != SNOOP_LEAVE_NORMAL_LEAVE)
        {
            CliPrintf (CliHandle, "%s leavemode ", au1String);
            if (i4RetVal == IGS_LEAVE_EXPLICIT_TRACK)
            {
                CliPrintf (CliHandle, "exp-hosttrack");
            }
            else
            {
                CliPrintf (CliHandle, "fastLeave");
            }
            if (u4InnerVlan != SNOOP_PORT_CFG_DEF_MODE_CVLAN)
            {
                CliPrintf (CliHandle, " InnerVlanId %d\r\n", u4InnerVlan);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
    i1RetValue =
        nmhGetFsSnoopEnhPortRateLimit (i4Index, u4InnerVlan, i4AddrType,
                                       &u4RetValue);
    UNUSED_PARAM (i1RetValue);

    if (u4RetValue != SNOOP_PORT_CFG_RATE_LMT_DEF)
    {
        CliPrintf (CliHandle, "%s ratelimit %u", au1String, u4RetValue);
        if (u4InnerVlan != SNOOP_PORT_CFG_DEF_MODE_CVLAN)
        {
            CliPrintf (CliHandle, " InnerVlanId %d\r\n", u4InnerVlan);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
    }

    i1RetValue = nmhGetFsSnoopEnhPortMaxLimitType (i4Index, u4InnerVlan,
                                                   i4AddrType, &i4RetVal);
    UNUSED_PARAM (i1RetValue);

    i1RetValue = nmhGetFsSnoopEnhPortMaxLimit (i4Index, u4InnerVlan,
                                               i4AddrType, &u4RetValue);
    UNUSED_PARAM (i1RetValue);

    if (i4RetVal != SNOOP_PORT_LMT_TYPE_NONE)
    {
        CliPrintf (CliHandle, "%s limit ", au1String);
        if (i4RetVal == SNOOP_PORT_LMT_TYPE_GROUPS)
        {
            CliPrintf (CliHandle, "groups %u", u4RetValue);
        }
        else
        {
            CliPrintf (CliHandle, "channels %u", u4RetValue);
        }
        if (u4InnerVlan != SNOOP_PORT_CFG_DEF_MODE_CVLAN)
        {
            CliPrintf (CliHandle, " InnerVlanId %d\r\n", u4InnerVlan);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
    }

    i1RetValue =
        nmhGetFsSnoopEnhPortProfileId (i4Index, u4InnerVlan, i4AddrType,
                                       &u4RetValue);
    if (u4RetValue != SNOOP_PORT_CFG_PROFILE_ID_DEF)
    {
        CliPrintf (CliHandle, "%s filter-profileId %u", au1String, u4RetValue);
        if (u4InnerVlan != SNOOP_PORT_CFG_DEF_MODE_CVLAN)
        {
            CliPrintf (CliHandle, " InnerVlanId %d\r\n", u4InnerVlan);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }
    }
    UNUSED_PARAM (i1RetValue);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCliSelectContextOnMode                      */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command Context.                             */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu2LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                             UINT4 *pu4Context)
{
    UINT4               u4ContextId = 0;
    *pu4Context = SNOOP_DEFAULT_INSTANCE;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    if (u4ContextId != SNOOP_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }

    if (SNOOP_SYSTEM_STATUS ((UINT1) *pu4Context) == SNOOP_SHUTDOWN)
    {
        if ((u4Cmd != IGS_SYS_ENABLE) && (u4Cmd != MLDS_SYS_ENABLE)
            && (u4Cmd != SNOOP_SYS_NO_SHUT))
        {
            if (u4Cmd != SNOOP_SYS_SHUT)
            {
                CliPrintf (CliHandle, "\r%% Snooping module not started \r\n");
            }
            return SNOOP_FAILURE;
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCliSetVlanRtrPortPurgeInterval              */
/*                                                                           */
/*    Description         : This function is used to configure the router    */
/*                          port purge interval (v1/v2) purge interval       */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                         i4VlanId - Vlan ID                                */
/*                         i4AddrType - Address type                         */
/*                         pu1RouterPorts - router ports for which purge Int */
/*                                          will be configured               */
/*                         i4PurgeInterval - router port purge interval      */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopCliSetVlanRtrPortPurgeInterval (tCliHandle CliHandle,
                                     INT4 i4VlanId, INT4 i4AddrType,
                                     UINT1 *pu1RouterPorts,
                                     INT4 i4PurgeInterval)
{
    UINT4               u4ErrCode = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    tSnoopIfPortBmp    *pRtrPortBitmap = NULL;

    if (pu1RouterPorts == NULL)
    {
        return CLI_FAILURE;
    }

    pRtrPortBitmap =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pRtrPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                       SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    MEMCPY (pRtrPortBitmap, pu1RouterPorts, SNOOP_IF_PORT_LIST_SIZE);
    for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = (*pRtrPortBitmap)[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                u2Port = MEM_MAX_BYTES (u2Port, SNOOP_MAX_PORTS);

                if (nmhTestv2FsSnoopOlderQuerierInterval (&u4ErrCode,
                                                          (INT4) u2Port,
                                                          (UINT4) i4VlanId,
                                                          i4AddrType,
                                                          i4PurgeInterval) ==
                    SNMP_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
                    return CLI_FAILURE;
                }

                if (nmhSetFsSnoopOlderQuerierInterval ((INT4) u2Port,
                                                       (UINT4) i4VlanId,
                                                       i4AddrType,
                                                       i4PurgeInterval) ==
                    SNMP_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCliSetVlanRtrPortOperVersion                */
/*                                                                           */
/*    Description         : This function is used to configure the router    */
/*                          port operating version                           */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                         i4VlanId - Vlan ID                                */
/*                         i4AddrType - Address type                         */
/*                         pu1RouterPorts - router ports for which oper ver  */
/*                                          will be configured               */
/*                         i4OperVersion - router port oper version          */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopCliSetVlanRtrPortOperVersion (tCliHandle CliHandle,
                                   INT4 i4VlanId, INT4 i4AddrType,
                                   UINT1 *pu1RouterPorts, INT4 i4OperVersion)
{
    UINT4               u4ErrCode = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2Port = 0;
    UINT1               u1PortFlag = 0;
    tSnoopIfPortBmp    *pRtrPortBitmap = NULL;

    if (pu1RouterPorts == NULL)
    {
        return CLI_FAILURE;
    }

    pRtrPortBitmap =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pRtrPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    MEMCPY (pRtrPortBitmap, pu1RouterPorts, SNOOP_IF_PORT_LIST_SIZE);
    for (u2BytIndex = 0; u2BytIndex < SNOOP_IF_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = (*pRtrPortBitmap)[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                u2Port = MEM_MAX_BYTES (u2Port, SNOOP_MAX_PORTS);
                if (nmhTestv2FsSnoopRtrPortCfgOperVersion (&u4ErrCode,
                                                           (INT4) u2Port,
                                                           (UINT4) i4VlanId,
                                                           i4AddrType,
                                                           i4OperVersion) ==
                    SNMP_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
                    return CLI_FAILURE;
                }

                if (nmhSetFsSnoopRtrPortCfgOperVersion ((INT4) u2Port,
                                                        (UINT4) i4VlanId,
                                                        i4AddrType,
                                                        i4OperVersion) ==
                    SNMP_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    FsUtilReleaseBitList ((UINT1 *) pRtrPortBitmap);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : SnoopBasicShowRunningConfig                        */
/*                                                                           */
/*     DESCRIPTION      : This function prints the basic configuration of    */
/*                        Snoop Module                                       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                        u4ContextId                                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopBasicShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4SysMode = 0;
    INT4                i4RetVal = 0;
    UINT1               au1ContextName[SNOOP_SWITCH_ALIAS_LEN];
    INT4                i4ControlPlaneDriven = SNOOP_DISABLE;

    CliRegisterLock (CliHandle, SnpLock, SnpUnLock);
    SNOOP_LOCK ();

    u4SysMode = SnoopVcmGetSystemMode (SNOOP_PROTOCOL_ID);

    if (u4SysMode == VCM_MI_MODE)
    {
        MEMSET (au1ContextName, 0, SNOOP_SWITCH_ALIAS_LEN);

        SnoopVcmGetAliasName (u4ContextId, au1ContextName);
        if (STRLEN (au1ContextName) != 0)
        {

            CliPrintf (CliHandle, "\rswitch  %s \r\n", au1ContextName);
        }
    }

    nmhGetFsSnoopInstanceGlobalSystemControl (u4ContextId, &i4RetVal);
    if (i4RetVal != 1)            /* Snoop system has been shutdown */
    {
        CliPrintf (CliHandle, "shutdown snooping\n");
        SNOOP_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_SUCCESS;
    }

    nmhGetFsSnoopInstanceGlobalMcastFwdMode (u4ContextId, &i4RetVal);
    nmhGetFsSnoopInstanceControlPlaneDriven (u4ContextId,
                                             &i4ControlPlaneDriven);

    if (i4RetVal == SNOOP_MCAST_FWD_MODE_IP)    /* Default is MAC-based (2) */
    {
        CliPrintf (CliHandle, "snooping multicast-forwarding-mode ip\r\n");
    }

    nmhGetFsSnoopInstanceGlobalLeaveConfigLevel (u4ContextId, &i4RetVal);

    if (i4RetVal == SNOOP_PORT_LEAVE_CONFIG)    /* Default is Vlan-based */
    {
        CliPrintf (CliHandle, "snooping leave-process config-level port\r\n");
    }

    nmhGetFsSnoopInstanceGlobalReportProcessConfigLevel ((INT4) u4ContextId,
                                                         &i4RetVal);
    if (i4RetVal != SNOOP_NON_ROUTER_REPORT_CONFIG)
    {
        CliPrintf (CliHandle,
                   "snooping report-process config-level all-Ports\r\n");
    }

    SNOOP_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCliSetMVlanStatus                           */
/*                                                                           */
/*    Description         : This function enables or disables the Multicas   */
/*                          VLAN feature                                     */
/*                                                                           */
/*    Input(s)            : CliHandle     - Contains error messages.         */
/*                          u4ContextId   - Context Id.                      */
/*                          i4AddressType - Internet address type            */
/*                          i4Status      - enable or disable                */
/*                                                                           */
/*    Output (s)          : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : CLI_SUCCESS / CLI_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliSetMVlanStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4AddressType, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopMulticastVlanStatus (&u4ErrCode, u4ContextId,
                                             i4AddressType, i4Status)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsSnoopMulticastVlanStatus (u4ContextId, i4AddressType,
                                          i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopCliSetFilterStatus                          */
/*                                                                           */
/*    Description         : This function enables or disables filter         */
/*                                                                           */
/*    Input(s)            : CliHandle     - Contains error messages.         */
/*                          u4ContextId   - Context Id.                      */
/*                          i4AddressType - Internet address type            */
/*                          i4Status      - enable or disable                */
/*                                                                           */
/*    Output (s)          : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : CLI_SUCCESS / CLI_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopCliSetFilterStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4AddressType, INT4 i4Status)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopFilterStatus (&u4ErrCode, u4ContextId,
                                      i4AddressType, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopFilterStatus (u4ContextId, i4AddressType,
                                   i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetLeaveConfigLevel                        */
/*                                                                           */
/*     DESCRIPTION      : This function configures leave configuration       */
/*                         level to be used for processing leave messages    */
/*                                                                           */
/*     INPUT            : CliHandle          - CLI Handler                   */
/*                        i4InstId           - Instance Identifier           */
/*                        i4LeaveConfigLevel - SNOOP_VLAN_LEAVE_CONFIG /     */
/*                                              SNOOP_PORT_LEAVE_CONFIG      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetLeaveConfigLevel (tCliHandle CliHandle, INT4 i4InstId,
                             INT4 i4LeaveConfigLevel)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopInstanceGlobalLeaveConfigLevel (&u4ErrCode,
                                                        i4InstId,
                                                        i4LeaveConfigLevel)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopInstanceGlobalLeaveConfigLevel (i4InstId,
                                                     i4LeaveConfigLevel)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetReportConfigLevel                       */
/*                                                                           */
/*     DESCRIPTION      : This function configures report configuration      */
/*                        level to be used for processing report messages    */
/*                                                                           */
/*     INPUT            : CliHandle          - CLI Handler                   */
/*                        i4InstId           - Instance Identifier           */
/*                        i4ReportConfigLevel- SNOOP_NON_ROUTER_REPORT_CONFIG*/
/*                                              SNOOP_ALL_REPORT_CONFIG      */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliSetReportConfigLevel (tCliHandle CliHandle, INT4 i4InstId,
                              INT4 i4ReportConfigLevel)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopInstanceGlobalReportProcessConfigLevel (&u4ErrCode,
                                                                i4InstId,
                                                                i4ReportConfigLevel)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopInstanceGlobalReportProcessConfigLevel (i4InstId,
                                                             i4ReportConfigLevel)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliShowMcastReceiverInfo                      */
/*                                                                           */
/*     DESCRIPTION      : Displays IGMP/MLD Snooping group information       */
/*                        for specific VLAN or all VLANS                     */
/*                                                                           */
/*     INPUT            : CliHandle      - CLI Handler                       */
/*                        u4InstId       - Instance Id                       */
/*                        VlanId         - Vlan Identifier                   */
/*                        pu1GroupIpAddr - Group Address                     */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/ CLI_FAILURE                           */
/*****************************************************************************/
INT4
SnoopCliShowMcastReceiverInfo (tCliHandle CliHandle, UINT4 u4InstId,
                               UINT1 u1AddrType, UINT4 VlanId,
                               UINT1 *pu1GroupIpAddr)
{
    tSNMP_OCTET_STRING_TYPE CurrHostIpAddr;
    tSNMP_OCTET_STRING_TYPE NextHostIpAddr;
    tSNMP_OCTET_STRING_TYPE CurrSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE NextSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE CurrGrpIpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpIpAddr;
    UINT4               u1IsShowAll = SNOOP_TRUE;
    UINT4               u4PagingStatus = CLI_SUCCESS;
#ifdef IGS_WANTED
    UINT4               u4TempSrcIpAddr = 0;
    UINT4               u4TempGrpIpAddr = 0;
    UINT4               u4TempHostIpAddr = 0;
#endif
    INT4                i4CurrVlanId = 0;
    INT4                i4NextVlanId = 0;
    INT4                i4CurrInnerVlanId = 0;
    INT4                i4NextInnerVlanId = 0;
    INT4                i4CurrInstId = 0;
    INT4                i4NextInstId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    INT4                i4CurrPort = 1;
    INT4                i4NextPort = 0;
    INT4                i4HostFilterMode = 0;
    UINT1               au1CurrSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrHostIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextHostIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NullSrcAddr[IPVX_MAX_INET_ADDR_LEN];
#ifdef IGS_WANTED
    CHR1               *pu1String = NULL;
#endif
    CHR1                au1Ip6Addr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1TempContextName[SNOOP_SWITCH_ALIAS_LEN + 7];
    UINT1               ai1PortStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1OperationMode = 0;
    BOOL1               bFirst = SNOOP_TRUE;
    BOOL1               bPrint = SNOOP_FALSE;

    SNOOP_MEM_SET (au1CurrHostIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextHostIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1CurrSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1CurrGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NullSrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (ai1PortStr, 0, CFA_MAX_PORT_NAME_LENGTH);

    CurrHostIpAddr.pu1_OctetList = au1CurrHostIpAddr;
    CurrHostIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    NextHostIpAddr.pu1_OctetList = au1NextHostIpAddr;
    NextHostIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    CurrSrcIpAddr.pu1_OctetList = au1CurrSrcIpAddr;
    CurrSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    CurrGrpIpAddr.pu1_OctetList = au1CurrGrpIpAddr;
    CurrGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextSrcIpAddr.pu1_OctetList = au1NextSrcIpAddr;
    NextSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextGrpIpAddr.pu1_OctetList = au1NextGrpIpAddr;
    NextGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (SNOOP_MEM_CMP (pu1GroupIpAddr, gNullGrpAddr,
                       IPVX_MAX_INET_ADDR_LEN) != 0)
    {
        SNOOP_INET_NTOHL (pu1GroupIpAddr);
    }

    i4CurrAddrType = u1AddrType;

    if (u4InstId != SNOOP_CLI_INVALID_CONTEXT)
    {
        i4CurrInstId = (INT4) u4InstId;
        i4CurrVlanId = VlanId;
        SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList, pu1GroupIpAddr,
                       IPVX_MAX_INET_ADDR_LEN);
    }

    if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
        (i4CurrInstId, &i4NextInstId, i4CurrVlanId, (UINT4 *) &i4NextVlanId,
         i4CurrAddrType, &i4NextAddrType, &CurrGrpIpAddr, &NextGrpIpAddr,
         i4CurrInnerVlanId, (UINT4 *) &i4NextInnerVlanId, i4CurrPort,
         &i4NextPort, &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
         &NextSrcIpAddr) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r\nSnooping Receiver Information");
    CliPrintf (CliHandle, "\r\n-----------------------------");
    do
    {
        /* if specific switch name is given , display only the respective
         * switch information */

        if ((u4InstId != SNOOP_CLI_INVALID_CONTEXT) &&
            ((INT4) u4InstId != i4NextInstId))
        {
            if (i4CurrInstId == i4NextInstId)
            {
                i4CurrInstId = i4NextInstId + 1;
                i4CurrVlanId = 0;
                i4CurrInnerVlanId = 0;
                i4CurrAddrType = 0;
                i4CurrPort = 0;

                SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);
            }
            else
            {
                i4CurrInstId = i4NextInstId;
                i4CurrVlanId = i4NextVlanId;
                i4CurrInnerVlanId = i4NextInnerVlanId;
                i4CurrAddrType = i4NextAddrType;
                i4CurrPort = i4NextPort;

                SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                               NextSrcIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                               NextGrpIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList,
                               NextHostIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);
            }

            if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId,
                 (UINT4 *) &i4NextVlanId, i4CurrAddrType, &i4NextAddrType,
                 &CurrGrpIpAddr, &NextGrpIpAddr, i4CurrInnerVlanId,
                 (UINT4 *) &i4NextInnerVlanId, i4CurrPort, &i4NextPort,
                 &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
                 &NextSrcIpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {
                break;
            }
        }
        if ((VlanId != SNOOP_INVALID_VLAN_ID) &&
            (VlanId != (tSnoopVlanId) i4NextVlanId))
        {
            i4CurrInstId = i4NextInstId;
            if (i4CurrVlanId == i4NextVlanId)
            {
                i4CurrVlanId = i4NextVlanId + 1;
                i4CurrInnerVlanId = 0;
                i4CurrAddrType = 0;
                i4CurrPort = 0;

                SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList, au1NullSrcAddr,
                               IPVX_MAX_INET_ADDR_LEN);
            }
            else
            {
                i4CurrVlanId = i4NextVlanId;
                i4CurrInnerVlanId = i4NextInnerVlanId;
                i4CurrAddrType = i4NextAddrType;
                i4CurrPort = i4NextPort;

                SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                               NextSrcIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                               NextGrpIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList,
                               NextHostIpAddr.pu1_OctetList,
                               IPVX_MAX_INET_ADDR_LEN);
            }
            if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId,
                 (UINT4 *) &i4NextVlanId, i4CurrAddrType, &i4NextAddrType,
                 &CurrGrpIpAddr, &NextGrpIpAddr, i4CurrInnerVlanId,
                 (UINT4 *) &i4NextInnerVlanId, i4CurrPort, &i4NextPort,
                 &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
                 &NextSrcIpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {

                break;
            }
        }

        if (u1AddrType != i4NextAddrType)
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrInnerVlanId = i4NextInnerVlanId;
            i4CurrAddrType = i4NextAddrType;
            i4CurrPort = i4NextPort;

            SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList,
                           NextHostIpAddr.pu1_OctetList,
                           IPVX_MAX_INET_ADDR_LEN);

            if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId,
                 (UINT4 *) &i4NextVlanId, i4CurrAddrType, &i4NextAddrType,
                 &CurrGrpIpAddr, &NextGrpIpAddr, i4CurrInnerVlanId,
                 (UINT4 *) &i4NextInnerVlanId, i4CurrPort, &i4NextPort,
                 &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
                 &NextSrcIpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {

                break;
            }
        }

        if ((SNOOP_MEM_CMP (pu1GroupIpAddr, gNullGrpAddr,
                            IPVX_MAX_INET_ADDR_LEN) != 0) &&
            (SNOOP_MEM_CMP (pu1GroupIpAddr,
                            NextGrpIpAddr.pu1_OctetList,
                            IPVX_MAX_INET_ADDR_LEN) != 0))
        {
            i4CurrInstId = i4NextInstId;
            i4CurrVlanId = i4NextVlanId;
            i4CurrInnerVlanId = i4NextInnerVlanId;
            i4CurrAddrType = i4NextAddrType;
            i4CurrPort = i4NextPort;

            SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList,
                           NextHostIpAddr.pu1_OctetList,
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

            if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
                (i4CurrInstId, &i4NextInstId, i4CurrVlanId,
                 (UINT4 *) &i4NextVlanId, i4CurrAddrType, &i4NextAddrType,
                 &CurrGrpIpAddr, &NextGrpIpAddr, i4CurrInnerVlanId,
                 (UINT4 *) &i4NextInnerVlanId, i4CurrPort, &i4NextPort,
                 &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
                 &NextSrcIpAddr) == SNMP_SUCCESS)
            {
                continue;
            }
            else
            {

                break;
            }
        }

        bPrint = SNOOP_FALSE;

        if (bFirst == SNOOP_TRUE)
        {
            if (SnoopGetSwitchName
                (i4NextInstId, (INT4) SNOOP_CLI_INVALID_CONTEXT,
                 au1TempContextName) == SNOOP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n%s", au1TempContextName);
                bPrint = SNOOP_TRUE;

            }
            bFirst = SNOOP_FALSE;
        }
        if (SnoopGetSwitchName (i4NextInstId, i4CurrInstId,
                                au1TempContextName) == SNOOP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n\r\n%s", au1TempContextName);
            bPrint = SNOOP_TRUE;

        }

        nmhGetFsSnoopVlanMcastReceiverFilterMode
            (i4NextInstId, i4NextVlanId, i4NextAddrType,
             &NextGrpIpAddr, i4NextInnerVlanId, i4NextPort,
             &NextHostIpAddr, &NextSrcIpAddr, &i4HostFilterMode);

        if (i4CurrInstId != i4NextInstId)
        {
            /* Obtain the operation mode to determine the format of display */
            u1OperationMode = SNOOP_INSTANCE_ENH_MODE (i4NextInstId);
        }

        if ((i4CurrVlanId != i4NextVlanId) ||
            (i4CurrInnerVlanId != i4NextInnerVlanId) ||
            (SNOOP_MEM_CMP (CurrGrpIpAddr.pu1_OctetList,
                            NextGrpIpAddr.pu1_OctetList,
                            IPVX_MAX_INET_ADDR_LEN) != 0) ||
            (bPrint == SNOOP_TRUE))
        {
            CliPrintf (CliHandle, "\r\n\r\nVLAN ID: %d", i4NextVlanId);

            if (u1OperationMode == SNOOP_ENABLE)
            {
                CliPrintf (CliHandle, "  Inner Vlan Id: %d", i4NextInnerVlanId);
            }
            CliPrintf (CliHandle, "  Group Address: ");
#ifdef IGS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV4)
            {
                u4TempGrpIpAddr =
                    SNOOP_HTONL (*(UINT4 *) (VOID *)
                                 NextGrpIpAddr.pu1_OctetList);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempGrpIpAddr);
                CliPrintf (CliHandle, "%s", pu1String);
            }
#endif

#ifdef MLDS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1Ip6Addr, NextGrpIpAddr.pu1_OctetList,
                               IPVX_IPV6_ADDR_LEN);

                CliPrintf (CliHandle, "%s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));
            }
#endif
            bPrint = SNOOP_TRUE;
        }

        if ((i4CurrPort != i4NextPort) || (bPrint == SNOOP_TRUE))
        {
            if (SnoopCfaCliGetIfName (i4NextPort, (INT1 *) ai1PortStr)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "%% Invalid Port %d\r\n", i4NextPort);
                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "\r\nReceiver Port: %s", ai1PortStr);
            bPrint = SNOOP_TRUE;
        }
        if ((SNOOP_MEM_CMP (CurrHostIpAddr.pu1_OctetList,
                            NextHostIpAddr.pu1_OctetList,
                            IPVX_MAX_INET_ADDR_LEN) != 0) ||
            (bPrint == SNOOP_TRUE))
        {
            CliPrintf (CliHandle, "\r\n  Attached Hosts: ");
#ifdef IGS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV4)
            {
                u4TempHostIpAddr =
                    SNOOP_HTONL (*(UINT4 *) (VOID *)
                                 NextHostIpAddr.pu1_OctetList);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempHostIpAddr);
                CliPrintf (CliHandle, "%s", pu1String);
            }
#endif

#ifdef MLDS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1Ip6Addr, NextHostIpAddr.pu1_OctetList,
                               IPVX_IPV6_ADDR_LEN);

                CliPrintf (CliHandle, "%s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1Ip6Addr));

            }
#endif
            bPrint = SNOOP_TRUE;
        }
        if (bPrint == SNOOP_TRUE)
        {
            if (i4HostFilterMode != SNOOP_EXCLUDE)
            {
                CliPrintf (CliHandle, "\r\n   Include Sources:");
            }
            else
            {
                CliPrintf (CliHandle, "\r\n   Exclude Sources:");
            }
        }
        if (SNOOP_MEM_CMP (NextSrcIpAddr.pu1_OctetList,
                           au1NullSrcAddr, IPVX_MAX_INET_ADDR_LEN) == 0)
        {
            u4PagingStatus = CliPrintf (CliHandle, " None");
        }
        else
        {
            if (bPrint == SNOOP_FALSE)
            {
                CliPrintf (CliHandle, ",");
            }
#ifdef IGS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV4)
            {
                u4TempSrcIpAddr =
                    SNOOP_HTONL (*(UINT4 *) (VOID *)
                                 NextSrcIpAddr.pu1_OctetList);

                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4TempSrcIpAddr);
                u4PagingStatus = CliPrintf (CliHandle, " %s", pu1String);
            }
#endif

#ifdef MLDS_WANTED
            if (i4NextAddrType == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_MEM_CPY (au1Ip6Addr, NextSrcIpAddr.pu1_OctetList,
                               IPVX_IPV6_ADDR_LEN);

                u4PagingStatus = CliPrintf (CliHandle, " %s",
                                            Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                          au1Ip6Addr));

            }
#endif
        }

        if (u4PagingStatus == CLI_FAILURE)
        {
            break;
        }

        i4CurrInstId = i4NextInstId;
        i4CurrVlanId = i4NextVlanId;
        i4CurrInnerVlanId = i4NextInnerVlanId;
        i4CurrAddrType = i4NextAddrType;
        i4CurrPort = i4NextPort;

        SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                       NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        SNOOP_MEM_CPY (CurrHostIpAddr.pu1_OctetList,
                       NextHostIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                       NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        if (nmhGetNextIndexFsSnoopVlanMcastReceiverTable
            (i4CurrInstId, &i4NextInstId, i4CurrVlanId,
             (UINT4 *) &i4NextVlanId, i4CurrAddrType, &i4NextAddrType,
             &CurrGrpIpAddr, &NextGrpIpAddr, i4CurrInnerVlanId,
             (UINT4 *) &i4NextInnerVlanId, i4CurrPort, &i4NextPort,
             &CurrHostIpAddr, &NextHostIpAddr, &CurrSrcIpAddr,
             &NextSrcIpAddr) != SNMP_SUCCESS)
        {
            u1IsShowAll = SNOOP_FALSE;
        }

    }
    while (u1IsShowAll);

    CliPrintf (CliHandle, "\r\n\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliGetNextPortTableIndex                      */
/*                                                                           */
/*     DESCRIPTION      : Retrives the next available index from the         */
/*                        default or the enhanced port table based on the    */
/*                        operation mode configured for the interface.       */
/*                                                                           */
/*     INPUT            : u4InstId          - Instance Id                    */
/*                        i4CurrIndex       - Current Index                  */
/*                        u4CurrInnerVlanId - Current Inner vlan id          */
/*                        i4CurrAddrType    - Current Address type           */
/*                        pi4NextIndex       - Next Index                    */
/*                        pu4NextInnerVlanId - Next Inner vlan id            */
/*                        pi4NextAddrType    - Next Address type             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : SNOOP_SUCCESS/ SNOOP_FAILURE                       */
/*****************************************************************************/
INT4
SnoopCliGetNextPortTableIndex (UINT4 u4InstId, INT4 i4CurrIndex,
                               UINT4 u4CurrInnerVlanId, INT4 i4CurrAddrType,
                               INT4 *pi4NextIndex, UINT4 *pu4NextInnerVlanId,
                               INT4 *pi4NextAddrType)
{
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    INT4                i4Result = 0;

    SNOOP_VALIDATE_INSTANCE_RET (u4InstId, SNOOP_FAILURE);
    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4InstId);

    if (pSnoopInstInfo == NULL)
    {
        return SNOOP_FAILURE;
    }

    if (SNOOP_INSTANCE_ENH_MODE (u4InstId) == SNOOP_ENABLE)
    {
        i4Result = nmhGetNextIndexFsSnoopEnhPortTable
            (i4CurrIndex, pi4NextIndex, u4CurrInnerVlanId,
             pu4NextInnerVlanId, i4CurrAddrType, pi4NextAddrType);
    }
    else
    {
        *pu4NextInnerVlanId = 0;
        i4Result = nmhGetNextIndexFsSnoopPortTable
            (i4CurrIndex, pi4NextIndex, i4CurrAddrType, pi4NextAddrType);
    }
    return (i4Result == SNMP_SUCCESS ? SNOOP_SUCCESS : SNOOP_FAILURE);
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliVlanSetStaticMcastGrpEntry                 */
/*                                                                           */
/*     DESCRIPTION      : This function adds/delete the static forwording    */
/*                            entry in the igmp snooping switch.                   */
/*                                                                           */
/*     INPUT            : CliHandle         - CLI Handler                         */
/*                            i4InstId        - Instance Id                         */
/*                            tSnoopVlanId    - Vlan Identifier                  */
/*                        i4AddrType        - SNOOP_ADDR_TYPE_IPV4 /            */
/*                                          SNOOP_ADDR_TYPE_IPV6             */
/*                        au1GrpAddr       - Group Address                     */
/*                        au1SrcAddr       - Source Address                    */
/*                        pu1RouterPorts  - Pointer to the port List         */
/*                        i4SnoopStatus   - EventType(ADD/DELETE)            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliVlanSetStaticMcastGrpEntry (tCliHandle CliHandle, INT4 i4InstId,
                                    INT4 i4VlanId, INT4 i4AddrType,
                                    UINT1 *au1GrpAddr, UINT1 *pu1PortList)
{
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE RetPortList;
    tSNMP_OCTET_STRING_TYPE TempPortList;
    tSNMP_OCTET_STRING_TYPE GrpAddr;
    tSNMP_OCTET_STRING_TYPE SrcAddr;
    tSNMP_OCTET_STRING_TYPE *pSrcAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpAddr = NULL;
    UINT1              *pau1TmpPortList = NULL;
    UINT4               u4ErrCode = 0;
    UINT1               au1SrcAddr[IPVX_IPV6_ADDR_LEN];
    INT4                i4RowStatus = 0;
    UINT4               u4GrpAddr = 0;
    tSnoopTag           VlanId;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1               u1GrpCreated = SNOOP_FALSE;
    INT1                i1Ret = 0;

    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&PortList, SNOOP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&RetPortList, SNOOP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&TempPortList, SNOOP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&GrpAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_IPV6_ADDR_LEN);

    SNOOP_OUTER_VLAN (VlanId) = (tVlanId) i4VlanId;

    PortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pu1PortList;
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        GrpAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        GrpAddr.pu1_OctetList = au1GrpAddr;

        SrcAddr.i4_Length = IPVX_IPV4_ADDR_LEN;
        SrcAddr.pu1_OctetList = (UINT1 *) &au1SrcAddr;
    }
    else
    {
        GrpAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        GrpAddr.pu1_OctetList = au1GrpAddr;

        SrcAddr.i4_Length = IPVX_IPV6_ADDR_LEN;
        SrcAddr.pu1_OctetList = (UINT1 *) &au1SrcAddr;
    }

    MEMCPY (&u4GrpAddr, au1GrpAddr, sizeof (SNOOP_IP_ADDR_SIZE));

    /*If PortList is null,then return! */
    if (pu1PortList == NULL)
    {
        return CLI_FAILURE;
    }

    RetPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (RetPortList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    RetPortList.i4_Length = sizeof (tSnoopIfPortBmp);
    MEMSET (RetPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    pSrcAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        return CLI_FAILURE;

    }

    pGrpAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        return CLI_FAILURE;

    }

    /* This is the check to see whether the group address falls under reserved addresses
     * 224.0.0.1 to 224.0.0.255 or not falling under multicast group itself. If condition match
     * then do not process anything, do continue with the next group address */

    if (((u4GrpAddr >= SNOOP_ALL_HOST_IP) &&
         (u4GrpAddr <= SNOOP_RESVD_MCAST_ADDR_RANGE_MAX))
        ||
        (u4GrpAddr < SNOOP_VALID_MCAST_IP_RANGE_MIN)
        || (u4GrpAddr > SNOOP_VALID_MCAST_IP_RANGE_MAX))
    {
        CliPrintf (CliHandle,
                   "\r%% reserved multicast address is not allowed\r\n");
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        return CLI_FAILURE;
    }

    /*Get the Row Status */
    nmhGetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                              i4AddrType, &SrcAddr,
                                              &GrpAddr, &i4RowStatus);

    /*Check: if row status is zero */
    if (i4RowStatus == SNOOP_INIT_VAL)
    {
        /*No Entry exists with this instance, Create One */
        if (nmhTestv2FsSnoopVlanStaticMcastGrpRowStatus (&u4ErrCode, i4InstId,
                                                         i4VlanId, i4AddrType,
                                                         &SrcAddr, &GrpAddr,
                                                         SNOOP_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            return CLI_FAILURE;
        }

        /*Set it as a create-n-wait entry */
        if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                      i4AddrType, &SrcAddr,
                                                      &GrpAddr,
                                                      SNOOP_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            return CLI_FAILURE;
        }

        u1GrpCreated = SNOOP_TRUE;

        if (nmhTestv2FsSnoopVlanStaticMcastGrpPortList (&u4ErrCode, i4InstId,
                                                        i4VlanId, i4AddrType,
                                                        &SrcAddr, &GrpAddr,
                                                        &PortList) ==
            SNMP_FAILURE)
        {
            i1Ret =
                nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                          i4AddrType, &SrcAddr,
                                                          &GrpAddr,
                                                          SNOOP_DESTROY);
            CLI_FATAL_ERROR (CliHandle);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            UNUSED_PARAM (i1Ret);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsSnoopVlanStaticMcastGrpPortList (&u4ErrCode, i4InstId,
                                                        i4VlanId, i4AddrType,
                                                        &SrcAddr, &GrpAddr,
                                                        &PortList)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            return CLI_FAILURE;
        }

        /*Entry present!!! make it to not-in-service */
        if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                      i4AddrType,
                                                      &SrcAddr, &GrpAddr,
                                                      SNOOP_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            return CLI_FAILURE;
        }
    }
    /* To get the port entries for the given group entry */
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pGrpAddr->pu1_OctetList, au1GrpAddr, IPVX_IPV4_ADDR_LEN);
        pGrpAddr->i4_Length = IPVX_IPV4_ADDR_LEN;

        MEMCPY (pSrcAddr->pu1_OctetList, au1SrcAddr, IPVX_IPV4_ADDR_LEN);
        pSrcAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }
    else
    {
        MEMCPY (pGrpAddr->pu1_OctetList, au1GrpAddr, IPVX_IPV6_ADDR_LEN);
        pGrpAddr->i4_Length = IPVX_IPV6_ADDR_LEN;

        MEMCPY (pSrcAddr->pu1_OctetList, au1SrcAddr, IPVX_IPV6_ADDR_LEN);
        pSrcAddr->i4_Length = IPVX_IPV6_ADDR_LEN;

    }

    nmhGetFsSnoopVlanStaticMcastGrpPortList (i4InstId, i4VlanId,
                                             i4AddrType, pSrcAddr, pGrpAddr,
                                             &RetPortList);

    pau1TmpPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pau1TmpPortList == NULL)
    {
        return SNMP_FAILURE;
    }
    SNOOP_MEM_SET (pau1TmpPortList, SNOOP_ZERO, SNOOP_PORT_LIST_SIZE);

    SnoopConvertToLocalPortList (PortList.pu1_OctetList, pau1TmpPortList);

    TempPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (TempPortList.pu1_OctetList == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);

        return CLI_FAILURE;
    }

    TempPortList.i4_Length = sizeof (tSnoopIfPortBmp);

    MEMSET (TempPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    SNOOP_MEM_CPY (TempPortList.pu1_OctetList, pau1TmpPortList,
                   SNOOP_PORT_LIST_SIZE);

    SNOOP_UPDATE_PORT_LIST (TempPortList.pu1_OctetList,
                            RetPortList.pu1_OctetList);

    /* Set the port list */
    if (nmhSetFsSnoopVlanStaticMcastGrpPortList (i4InstId, i4VlanId,
                                                 i4AddrType, &SrcAddr,
                                                 &GrpAddr,
                                                 &RetPortList) == SNMP_FAILURE)
    {
        i1Ret = nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                          i4AddrType, &SrcAddr,
                                                          &GrpAddr,
                                                          SNOOP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        FsUtilReleaseBitList (TempPortList.pu1_OctetList);
        UNUSED_PARAM (i1Ret);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus
        (i4InstId, i4VlanId, i4AddrType, &SrcAddr, &GrpAddr,
         SNOOP_ACTIVE) == SNMP_FAILURE)
    {
        i1Ret = nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                          i4AddrType, &SrcAddr,
                                                          &GrpAddr,
                                                          SNOOP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);
        FsUtilReleaseBitList (TempPortList.pu1_OctetList);
        UNUSED_PARAM (i1Ret);
        return CLI_FAILURE;
    }

    if (u1GrpCreated == SNOOP_TRUE)
    {
        if (SnoopVlanGetVlanEntry ((UINT4) i4InstId,
                                   VlanId,
                                   (UINT1) i4AddrType,
                                   &pSnoopVlanEntry) == SNOOP_SUCCESS)
        {
            if (SnoopVlanUpdateStats ((UINT4) i4InstId, pSnoopVlanEntry,
                                      (UINT1) SNOOP_ACTIVE_GRPS) !=
                SNOOP_SUCCESS)
            {
                SNOOP_TRC_ARG1 (SNOOP_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_VLAN_TRC, "Unable to update SNOOP "
                                "statistics for VLAN ID %d\r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            }

        }
    }

    FsUtilReleaseBitList (RetPortList.pu1_OctetList);
    free_octetstring (pSrcAddr);
    free_octetstring (pGrpAddr);
    UtilPlstReleaseLocalPortList (pau1TmpPortList);
    FsUtilReleaseBitList (TempPortList.pu1_OctetList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliVlanDelStaticMcastGrpEntry                 */
/*                                                                           */
/*     DESCRIPTION      : This function adds/delete the static forwording    */
/*                            entry in the igmp snooping switch.                  */
/*                                                                           */
/*     INPUT            : CliHandle         - CLI Handler                         */
/*                            i4InstId       - Instance Id                          */
/*                            tSnoopVlanId   - Vlan Identifier                   */
/*                        i4AddrType        - SNOOP_ADDR_TYPE_IPV4 /            */
/*                                         SNOOP_ADDR_TYPE_IPV6              */
/*                        au1GrpAddr       - Group Address                     */
/*                        pu1PortList    - Pointer to the port List          */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/
INT4
SnoopCliVlanDelStaticMcastGrpEntry (tCliHandle CliHandle, INT4 i4InstId,
                                    INT4 i4VlanId, INT4 i4AddrType,
                                    UINT1 *au1GrpAddr, UINT1 *pu1PortList)
{
    tSNMP_OCTET_STRING_TYPE GrpAddr, SrcAddr;
    tSNMP_OCTET_STRING_TYPE RetPortList;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE TempPortList;
    tSNMP_OCTET_STRING_TYPE *pSrcAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pGrpAddr = NULL;
    UINT1               au1SrcAddr[IPVX_IPV6_ADDR_LEN];

    UINT1              *pau1TmpPortList = NULL;
    INT4                i4RowStatus = 0;

    if (pu1PortList == NULL)
    {
        return CLI_FAILURE;
    }

    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&PortList, SNOOP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (&TempPortList, SNOOP_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_IPV6_ADDR_LEN);

    PortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;
    PortList.pu1_OctetList = pu1PortList;

    GrpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    GrpAddr.pu1_OctetList = au1GrpAddr;

    SrcAddr.i4_Length = 0;

    RetPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (RetPortList.pu1_OctetList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        return CLI_FAILURE;
    }

    MEMSET (RetPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));
    RetPortList.i4_Length = sizeof (tSnoopIfPortBmp);

    pSrcAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pSrcAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        return CLI_FAILURE;

    }

    pGrpAddr = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pGrpAddr == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% Error in Allocating memory for bitlist \r\n");
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        return CLI_FAILURE;
    }

    if (nmhGetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                  i4AddrType, &SrcAddr,
                                                  &GrpAddr,
                                                  &i4RowStatus) == SNMP_FAILURE)
    {
        /*No entry present with this instance */
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                  i4AddrType, &SrcAddr,
                                                  &GrpAddr,
                                                  SNOOP_NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        return CLI_FAILURE;
    }

    /* To get the port entries for the given group entry */
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (pGrpAddr->pu1_OctetList, au1GrpAddr, IPVX_IPV4_ADDR_LEN);
        pGrpAddr->i4_Length = IPVX_IPV4_ADDR_LEN;

        MEMCPY (pSrcAddr->pu1_OctetList, au1SrcAddr, IPVX_IPV4_ADDR_LEN);
        pSrcAddr->i4_Length = IPVX_IPV4_ADDR_LEN;
    }

    else
    {
        /*Entry Found!!! Delete it */
        MEMCPY (pGrpAddr->pu1_OctetList, au1GrpAddr, IPVX_IPV6_ADDR_LEN);
        pGrpAddr->i4_Length = IPVX_IPV6_ADDR_LEN;

        MEMCPY (pSrcAddr->pu1_OctetList, au1SrcAddr, IPVX_IPV6_ADDR_LEN);
        pSrcAddr->i4_Length = IPVX_IPV6_ADDR_LEN;

    }

    nmhGetFsSnoopVlanStaticMcastGrpPortList (i4InstId, i4VlanId,
                                             i4AddrType, pSrcAddr, pGrpAddr,
                                             &RetPortList);

    pau1TmpPortList = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pau1TmpPortList == NULL)
    {
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        return CLI_FAILURE;
    }
    SNOOP_MEM_SET (pau1TmpPortList, SNOOP_ZERO, SNOOP_PORT_LIST_SIZE);

    SnoopConvertToLocalPortList (PortList.pu1_OctetList, pau1TmpPortList);

    TempPortList.pu1_OctetList = FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (TempPortList.pu1_OctetList == NULL)
    {
        CLI_FATAL_ERROR (CliHandle);
        FsUtilReleaseBitList (RetPortList.pu1_OctetList);
        free_octetstring (pSrcAddr);
        free_octetstring (pGrpAddr);
        UtilPlstReleaseLocalPortList (pau1TmpPortList);

        return CLI_FAILURE;
    }

    MEMSET (TempPortList.pu1_OctetList, 0, sizeof (tSnoopIfPortBmp));

    SNOOP_MEM_CPY (TempPortList.pu1_OctetList, pau1TmpPortList,
                   SNOOP_PORT_LIST_SIZE);

    if (((MEMCMP (RetPortList.pu1_OctetList, TempPortList.pu1_OctetList,
                  sizeof (tSnoopIfPortBmp)) == 0)) ||
        (MEMCMP
         (TempPortList.pu1_OctetList, gNullPortBitMap,
          SNOOP_PORT_LIST_SIZE) == 0))
    {

        if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus (i4InstId, i4VlanId,
                                                      i4AddrType, &SrcAddr,
                                                      &GrpAddr, SNOOP_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            FsUtilReleaseBitList (RetPortList.pu1_OctetList);
            free_octetstring (pSrcAddr);
            free_octetstring (pGrpAddr);
            UtilPlstReleaseLocalPortList (pau1TmpPortList);
            FsUtilReleaseBitList (TempPortList.pu1_OctetList);
            return CLI_FAILURE;
        }

    }

    else
    {

        SNOOP_AND_PORT_BMP (TempPortList.pu1_OctetList,
                            RetPortList.pu1_OctetList);

        SNOOP_XOR_PORT_BMP (RetPortList.pu1_OctetList,
                            TempPortList.pu1_OctetList);

        if (SNOOP_MEM_CMP (TempPortList.pu1_OctetList,
                           gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
        {

            if (nmhSetFsSnoopVlanStaticMcastGrpPortList (i4InstId, i4VlanId,
                                                         i4AddrType, &SrcAddr,
                                                         &GrpAddr,
                                                         &RetPortList) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                FsUtilReleaseBitList (RetPortList.pu1_OctetList);
                free_octetstring (pSrcAddr);
                free_octetstring (pGrpAddr);
                UtilPlstReleaseLocalPortList (pau1TmpPortList);
                FsUtilReleaseBitList (TempPortList.pu1_OctetList);
                return CLI_FAILURE;
            }

            if (nmhSetFsSnoopVlanStaticMcastGrpRowStatus
                (i4InstId, i4VlanId, i4AddrType, &SrcAddr, &GrpAddr,
                 SNOOP_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                FsUtilReleaseBitList (RetPortList.pu1_OctetList);
                free_octetstring (pSrcAddr);
                free_octetstring (pGrpAddr);
                UtilPlstReleaseLocalPortList (pau1TmpPortList);
                FsUtilReleaseBitList (TempPortList.pu1_OctetList);
                return CLI_FAILURE;
            }
        }
    }
    FsUtilReleaseBitList (RetPortList.pu1_OctetList);
    free_octetstring (pSrcAddr);
    free_octetstring (pGrpAddr);
    UtilPlstReleaseLocalPortList (pau1TmpPortList);
    FsUtilReleaseBitList (TempPortList.pu1_OctetList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopVlanSetRobustnessValue                        */
/*                                                                           */
/*     DESCRIPTION      : This function configures the robustness value      */
/*                                                                           */
/*                                                                           */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4ContextId   - Instance Identifier                */
/*                        VlanId     - Vlan Identifier                       */
/*                        i4AddrType - SNOOP_ADDR_TYPE_IPV4 /                */
/*                                     SNOOP_ADDR_TYPE_IPV6                  */
/*                        i4SetRobustnessVal - Robustness value to be        */
/*                          configured                                       */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*****************************************************************************/

INT4
SnoopVlanSetRobustnessValue (tCliHandle CliHandle, UINT4 u4ContextId,
                             tVlanId VlanId, INT4 i4AddrType,
                             UINT4 u4SetRobustnessVal)
{
    UINT4               u4ErrCode = 0;

    if (SnoopCliUpdateVlanEntry
        (CliHandle, (INT4) u4ContextId, VlanId, i4AddrType,
         SNOOP_CREATE) != CLI_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsSnoopVlanRobustnessValue (&u4ErrCode,
                                             (INT4) u4ContextId, VlanId,
                                             i4AddrType, u4SetRobustnessVal)
        == SNMP_FAILURE)
    {
        SnoopCliUpdateVlanEntry (CliHandle, (INT4) u4ContextId, VlanId,
                                 i4AddrType, SNOOP_DELETE);
        return CLI_FAILURE;
    }

    if (nmhSetFsSnoopVlanRobustnessValue ((INT4) u4ContextId,
                                          VlanId, i4AddrType,
                                          u4SetRobustnessVal) == SNMP_FAILURE)

    {
        SnoopCliUpdateVlanEntry (CliHandle, (INT4) u4ContextId, VlanId,
                                 i4AddrType, SNOOP_DELETE);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (SnoopCliUpdateVlanEntry
        (CliHandle, (INT4) u4ContextId, VlanId, i4AddrType,
         SNOOP_SET_ACTIVE) != CLI_SUCCESS)
    {
        nmhSetFsSnoopVlanRowStatus ((INT4) u4ContextId, VlanId, i4AddrType,
                                    SNOOP_DESTROY);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : SnoopCliSetControlPlaneDriven                      */
/*                                                                           */
/*     DESCRIPTION      : This function enables the control plane driven     */
/*                        approach in the system.                            */
/*     INPUT            : CliHandle  - CLI Handler                           */
/*                        u4ContextId - Instance Id                          */
/*                        i4SnoopSystemEnhMode - SNOOP_ENH_ENABLE /          */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success                                            */
/*****************************************************************************/
INT4
SnoopCliSetControlPlaneDriven (tCliHandle CliHandle, INT4 i4InstId,
                               INT4 i4SnoopControlPlaneMode)
{
    UNUSED_PARAM (CliHandle);

    UINT4               u4ErrCode = 0;

    if (nmhTestv2FsSnoopInstanceGlobalMcastFwdMode (&u4ErrCode,
                                                    i4InstId,
                                                    SNOOP_MCAST_FWD_MODE_IP) ==
        SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopInstanceGlobalMcastFwdMode
        (i4InstId, SNOOP_MCAST_FWD_MODE_IP) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    if (nmhTestv2FsSnoopInstanceControlPlaneDriven (&u4ErrCode,
                                                    i4InstId,
                                                    i4SnoopControlPlaneMode)
        == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    if (nmhSetFsSnoopInstanceControlPlaneDriven
        (i4InstId, i4SnoopControlPlaneMode) == SNMP_FAILURE)
    {
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

#endif /* __SNPCLI_C__ */
