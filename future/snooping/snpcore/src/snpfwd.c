/* $Id: snpfwd.c,v 1.73.2.1 2018/04/12 12:53:45 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpfwd.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping Forwarding table                      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains routines related to         */
/*                            updating the forwarding entries                */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 200            Initial Creation                        */
/*---------------------------------------------------------------------------*/

#include "snpinc.h"
#include "snpextn.h"

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateMacFwdTable                            */
/*                                                                           */
/* Description        : This function updates the mac based forwarding table */
/*                      by creating or deleting an forwarding entry          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                      pSnoopGroupEntry   - pointer to group entry          */
/*                      u4Port           - Port number                       */
/*                      u1EventType      - Event CREATE/DELETE entry         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdUpdateMacFwdTable (UINT4 u4Instance,
                           tSnoopGroupEntry * pSnoopGroupEntry, UINT4 u4Port,
                           UINT1 u1EventType, UINT1 u1PortEvent)
{
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pOperUpPortBmp = NULL;
    UINT1              *pTempSnoopRtrPortBmp = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopMacGrpFwdEntry MacGrpFwdEntry;
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tSnoopMacAddr       GrpMacAddr;
    tSnoopFilter        SnoopDelEntry;
    UINT4               u4Status = 0;
    UINT2               u2PortListSize = sizeof (tSnoopPortBmp);
    UINT4               u4VlanCnt = 0;
    UINT4               u4PortNum = 0;
    UINT4               u4GrpNodeCount = 0;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1EntryFound = SNOOP_FALSE;
    UINT1               u1GroupFound = SNOOP_FALSE;
    UINT1               u1PortPresent = SNOOP_FALSE;
    UINT1               au1GroupAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               u1MacGroupFound = SNOOP_FALSE;
    UNUSED_PARAM (u1PortEvent);

    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));
    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (au1GroupAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdUpdateMacFwdTable\r\n");
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
        return SNOOP_FAILURE;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    SNOOP_MEM_CPY (au1GroupAddr, pSnoopGroupEntry->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);
#ifdef IGS_WANTED
    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
    {
        SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4 (au1GroupAddr), GrpMacAddr);
    }

#endif
#ifdef MLDS_WANTED
    if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
    {
        /*    SNOOP_INET_HTONL (au1GroupAddr); */
        SNOOP_GET_MAC_FROM_IPV6 (au1GroupAddr, GrpMacAddr);
    }
#endif

    /* scanning is done for the associated vlans in the PVLAN domain and
     * also for the incoming vlan identifier */
    for (u4VlanCnt = SNOOP_ZERO;
         u4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans; u4VlanCnt++)
    {
        if (u4VlanCnt == SNOOP_ZERO)
        {
            SNOOP_MEM_CPY (MacGrpFwdEntry.VlanId, pSnoopGroupEntry->VlanId,
                           sizeof (tSnoopTag));
        }
        else
        {
            SNOOP_OUTER_VLAN (MacGrpFwdEntry.VlanId) =
                L2PvlanMappingInfo.pMappedVlans[u4VlanCnt - 1];
            SNOOP_INNER_VLAN (MacGrpFwdEntry.VlanId) = SNOOP_INVALID_VLAN_ID;
        }
        MacGrpFwdEntry.u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

        SNOOP_MEM_CPY (&MacGrpFwdEntry.MacGroupAddr, GrpMacAddr,
                       SNOOP_MAC_ADDR_LEN);

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                             (tRBElem *) & MacGrpFwdEntry);

        if (pRBElem == NULL)
        {
            u1EntryFound = SNOOP_FALSE;
        }
        else
        {
            pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;
            u1EntryFound = SNOOP_TRUE;
        }

        switch (u1EventType)
        {
            case SNOOP_CREATE_FWD_ENTRY:

                if (u1EntryFound == SNOOP_TRUE)
                {
                    if (u4VlanCnt == SNOOP_ZERO)
                    {
                        /* Scan if the Group IP entry already exists for the 
                         * MAC entry */
                        SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                        pTempSnoopGroupEntry,
                                        tSnoopGroupEntry *)
                        {
                            u4GrpNodeCount++;
                            if (SNOOP_MEM_CMP ((UINT1 *) pTempSnoopGroupEntry->
                                               GroupIpAddr.au1Addr,
                                               (UINT1 *) pSnoopGroupEntry->
                                               GroupIpAddr.au1Addr,
                                               IPVX_MAX_INET_ADDR_LEN) == 0)
                            {
                                u1GroupFound = SNOOP_TRUE;
                                break;
                            }
                        }

                        if (u1GroupFound == SNOOP_TRUE)
                        {
                            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                 L2PvlanMappingInfo.
                                                                 pMappedVlans);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Group entry for this MAC "
                                       "forwarding entry already present\r\n");
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                            return SNOOP_SUCCESS;
                        }

                        if (u4GrpNodeCount == 0)
                        {
                            SNOOP_SLL_INIT (&pSnoopMacGrpFwdEntry->
                                            GroupEntryList);
                        }
                    }

                    /* Update the port bitmap for the MAC forward entry */
                    SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->PortBitmap,
                                            pSnoopMacGrpFwdEntry->PortBitmap);

                    pTempSnoopGroupEntry = NULL;
                    /* Update VLAN multicast egress ports */
                    if (SnoopMiVlanUpdateDynamicMcastInfo
                        (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                         pSnoopMacGrpFwdEntry->VlanId,
                         u4Port, VLAN_ADD) != SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Updation to VLAN module for port deletion"
                                   " for MAC mcast entry failed\r\n");

                        /* Delete the list of pointers to the group entry */
                        while ((pTempSnoopGroupEntry =
                                (tSnoopGroupEntry *) SNOOP_SLL_FIRST
                                (&pSnoopMacGrpFwdEntry->GroupEntryList)) !=
                               NULL)
                        {
                            pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                            SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                              GroupEntryList,
                                              pTempSnoopGroupEntry);
                        }

                        SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                      pSnoopMacGrpFwdEntry,
                                                      SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);

                        pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                        if (pSnoopInstInfo != NULL)
                        {
                            pSnoopInfo =
                                &(pSnoopInstInfo->
                                  SnoopInfo[(pSnoopMacGrpFwdEntry->
                                             u1AddressType) - 1]);
                            if ((pSnoopInfo != NULL)
                                && (pSnoopInfo->u4FwdGroupsCnt != 0))
                            {
                                pSnoopInfo->u4FwdGroupsCnt -= 1;
                            }
                        }
                        RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                      MacMcastFwdEntry,
                                      (tRBElem *) pSnoopMacGrpFwdEntry);
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);

                        SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                        pSnoopMacGrpFwdEntry);
                        return SNOOP_FAILURE;
                    }

                    if (u4VlanCnt == SNOOP_ZERO)
                    {
                        /* Assign a back pointer to the Group entry node */
                        pSnoopGroupEntry->pMacGrpFwdEntry =
                            pSnoopMacGrpFwdEntry;

                        /* Add the IP group entry node to the MAC Group IP list */
                        SNOOP_SLL_ADD (&(pSnoopMacGrpFwdEntry->GroupEntryList),
                                       &pSnoopGroupEntry->pNextGrpNode);
                    }
                    break;
                }

                /* MAC forward entry not found, so create a new one */
                if (SNOOP_VLAN_MAC_FWD_ALLOC_MEMBLK (u4Instance,
                                                     pSnoopMacGrpFwdEntry) ==
                    NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Memory allocation for MAC forward entry \r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for u4Instance %d\r\n", u4Instance);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_SET (pSnoopMacGrpFwdEntry, 0,
                               sizeof (tSnoopMacGrpFwdEntry));

                pSnoopMacGrpFwdEntry->u1AddressType = pSnoopGroupEntry->
                    GroupIpAddr.u1Afi;
                SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->MacGroupAddr,
                               GrpMacAddr, SNOOP_MAC_ADDR_LEN);
                SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->PortBitmap,
                                        pSnoopMacGrpFwdEntry->PortBitmap);
                pSnoopMacGrpFwdEntry->u1EntryTypeFlag =
                    pSnoopGroupEntry->u1EntryTypeFlag;
                if (u4VlanCnt == SNOOP_ZERO)
                {
                    SNOOP_SLL_INIT (&pSnoopMacGrpFwdEntry->GroupEntryList);

                    /* Update the fields of the MAC entry */
                    SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->VlanId,
                                   pSnoopGroupEntry->VlanId,
                                   sizeof (tSnoopTag));

                    /* Assign a back pointer to the Group entry node */
                    pSnoopGroupEntry->pMacGrpFwdEntry = pSnoopMacGrpFwdEntry;

                    /* Add the IP group entry node to the MAC Group IP list */
                    SNOOP_SLL_ADD (&(pSnoopMacGrpFwdEntry->GroupEntryList),
                                   &pSnoopGroupEntry->pNextGrpNode);
                }
                else
                {
                    /* Update the fields of the MAC entry */
                    SNOOP_OUTER_VLAN (pSnoopMacGrpFwdEntry->VlanId) =
                        L2PvlanMappingInfo.pMappedVlans[u4VlanCnt - 1];
                    SNOOP_INNER_VLAN (pSnoopMacGrpFwdEntry->VlanId) =
                        SNOOP_INVALID_VLAN_ID;
                }

                pTempSnoopGroupEntry = NULL;

                u4Status =
                    RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->
                               MacMcastFwdEntry,
                               (tRBElem *) pSnoopMacGrpFwdEntry);

                if (u4Status == RB_FAILURE)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_DBG,
                               "RBTree Addition Failed for MAC forward Entry \r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_SNP_RB_TREE_ADD_FAIL],
                                           "for u4Instance %d\r\n", u4Instance);

                    /* Delete the list of pointers to the group entry */
                    while ((pTempSnoopGroupEntry =
                            (tSnoopGroupEntry *) SNOOP_SLL_FIRST
                            (&pSnoopMacGrpFwdEntry->GroupEntryList)) != NULL)
                    {
                        pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                        SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                          GroupEntryList, pTempSnoopGroupEntry);
                    }

                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                    pSnoopMacGrpFwdEntry);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_EXIT | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_EXIT_NAME,
                                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if ((pSnoopInstInfo != NULL) && (pSnoopMacGrpFwdEntry != NULL))
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                    if (pSnoopInfo != NULL)
                    {
                        pSnoopInfo->u4FwdGroupsCnt += 1;

                    }
                }

                /* Update VLAN multicast egress ports */
                if (SnoopMiVlanUpdateDynamicMcastInfo
                    (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                     pSnoopMacGrpFwdEntry->VlanId, u4Port,
                     VLAN_ADD) != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Updation to VLAN module for port "
                               "addtion for MAC mcast entry failed\r\n");

                    /* Delete the list of pointers to the group entry */
                    while ((pTempSnoopGroupEntry =
                            (tSnoopGroupEntry *) SNOOP_SLL_FIRST
                            (&pSnoopMacGrpFwdEntry->GroupEntryList)) != NULL)
                    {
                        pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                        SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                          GroupEntryList, pTempSnoopGroupEntry);
                    }

                    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                  pSnoopMacGrpFwdEntry,
                                                  SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if ((pSnoopInstInfo != NULL)
                        && (pSnoopMacGrpFwdEntry != NULL))
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->
                              SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType -
                                        1]);
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  MacMcastFwdEntry,
                                  (tRBElem *) pSnoopMacGrpFwdEntry);

                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                    pSnoopMacGrpFwdEntry);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_EXIT | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_EXIT_NAME,
                                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
                break;

            case SNOOP_DELETE_FWD_ENTRY:

                if (u1EntryFound == SNOOP_FALSE)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_FWD |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "MAC forwarding entry not found\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                /* Delete the list of pointers to the group entry */
                while ((pTempSnoopGroupEntry = (tSnoopGroupEntry *)
                        SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->GroupEntryList))
                       != NULL)
                {
                    pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                    SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                      pTempSnoopGroupEntry);
                }

                SnoopRedSyncBulkUpdNextEntry (u4Instance, pSnoopMacGrpFwdEntry,
                                              SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                /* Indicate the aging out of MAC  forwarding entry */
                SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                               pSnoopMacGrpFwdEntry->VlanId,
                               sizeof (tSnoopTag));

                SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                               pSnoopMacGrpFwdEntry->MacGroupAddr,
                               SNOOP_MAC_ADDR_LEN);
                SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if (pSnoopInstInfo != NULL)
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                              MacMcastFwdEntry,
                              (tRBElem *) pSnoopMacGrpFwdEntry);

                SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                pSnoopMacGrpFwdEntry);
                break;

            case SNOOP_ADD_PORT:

                if (u1EntryFound == SNOOP_FALSE)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "MAC forwarding entry not found\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                /* Update the port bitmap for the MAC forward entry */
                SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->PortBitmap,
                                        pSnoopMacGrpFwdEntry->PortBitmap);

                /* Update VLAN multicast egress ports */
                if (SnoopMiVlanUpdateDynamicMcastInfo
                    (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                     pSnoopMacGrpFwdEntry->VlanId, u4Port,
                     VLAN_ADD) != SNOOP_SUCCESS)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Updation to VLAN module for port "
                                   "addition for MAC mcast entry failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                break;

            case SNOOP_ADD_PORTLIST:

                /* If the entry is found, just add the grp entry pointer to the
                 * list of pointers in the MAC forwarding list */

                if (u1EntryFound == SNOOP_TRUE)
                {
                    /* Scan if the Group IP entry already exists for the 
                     * MAC entry */
                    SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                    pTempSnoopGroupEntry, tSnoopGroupEntry *)
                    {
                        if (SNOOP_MEM_CMP ((UINT1 *) pTempSnoopGroupEntry->
                                           GroupIpAddr.au1Addr,
                                           (UINT1 *) pSnoopGroupEntry->
                                           GroupIpAddr.au1Addr,
                                           IPVX_MAX_INET_ADDR_LEN) == 0)
                        {
                            u1MacGroupFound = SNOOP_TRUE;
                        }
                    }
                    if (u1MacGroupFound == SNOOP_FALSE)
                    {
                        /* Add the IP group entry node to the MAC Group IP list */
                        SNOOP_SLL_ADD (&(pSnoopMacGrpFwdEntry->GroupEntryList),
                                       (tTMO_SLL_NODE *) pSnoopGroupEntry);
                    }
                }
                else
                {
                    /* If the entry not found, create the MAC forward entry and */
                    /* update the group pointers list in the MAC forward table */
                    if (SNOOP_VLAN_MAC_FWD_ALLOC_MEMBLK (u4Instance,
                                                         pSnoopMacGrpFwdEntry)
                        == NULL)
                    {

                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                       "Memory allocation for MAC forward entry \r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_MEM_ALLOC_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }

                    SNOOP_MEM_SET (pSnoopMacGrpFwdEntry, 0,
                                   sizeof (tSnoopMacGrpFwdEntry));
                    SNOOP_SLL_INIT (&pSnoopMacGrpFwdEntry->GroupEntryList);

                    /* Update the fields of the MAC entry */
                    /*pSnoopMacGrpFwdEntry->VlanId = pSnoopGroupEntry->VlanId; */
                    SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->VlanId,
                                   pSnoopGroupEntry->VlanId,
                                   sizeof (tSnoopTag));
                    pSnoopMacGrpFwdEntry->u1AddressType = pSnoopGroupEntry->
                        GroupIpAddr.u1Afi;
                    SNOOP_MEM_CPY (pSnoopMacGrpFwdEntry->MacGroupAddr,
                                   GrpMacAddr, SNOOP_MAC_ADDR_LEN);
                    pOperUpPortBmp =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pOperUpPortBmp == NULL)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_DBG_GRP | SNOOP_TRC_CONTROL |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                                       "Error in allocating memory for pOperUpPortBmp\r\n");
                        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                          SnoopSysErrString
                                          [SYS_LOG_MEM_ALLOC_FAIL]);
                        SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u1Instance,
                                                        pSnoopMacGrpFwdEntry);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }
                    SNOOP_MEM_SET (pOperUpPortBmp, 0, sizeof (tSnoopPortBmp));

                    SNOOP_UPDATE_PORT_LIST (pOperUpPortBmp,
                                            pSnoopMacGrpFwdEntry->PortBitmap);
                    UtilPlstReleaseLocalPortList (pOperUpPortBmp);

                    pSnoopMacGrpFwdEntry->u1EntryTypeFlag =
                        pSnoopGroupEntry->u1EntryTypeFlag;

                    /* Assign a back pointer to the Group entry node */
                    pSnoopGroupEntry->pMacGrpFwdEntry = pSnoopMacGrpFwdEntry;

                    /* Add the IP group entry node to the MAC Group IP list */
                    SNOOP_SLL_ADD (&(pSnoopMacGrpFwdEntry->GroupEntryList),
                                   (tTMO_SLL_NODE *) pSnoopGroupEntry);
                    pTempSnoopGroupEntry = NULL;

                    u4Status =
                        RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->
                                   MacMcastFwdEntry,
                                   (tRBElem *) pSnoopMacGrpFwdEntry);

                    if (u4Status == RB_FAILURE)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_DBG,
                                   "RBTree Addition Failed for MAC forward Entry \r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_SNP_RB_TREE_ADD_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        /* Delete the list of pointers to the group entry */
                        while ((pTempSnoopGroupEntry =
                                (tSnoopGroupEntry *) SNOOP_SLL_FIRST
                                (&pSnoopMacGrpFwdEntry->GroupEntryList)) !=
                               NULL)
                        {
                            pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                            SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                              GroupEntryList,
                                              pTempSnoopGroupEntry);
                        }
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u1Instance,
                                                        pSnoopMacGrpFwdEntry);
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_DBG_EXIT | SNOOP_DBG_ALL_FAILURE,
                                       SNOOP_EXIT_NAME,
                                       "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if ((pSnoopInstInfo != NULL)
                        && (pSnoopMacGrpFwdEntry != NULL))
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->
                              SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType -
                                        1]);
                        if (pSnoopInfo != NULL)
                        {
                            pSnoopInfo->u4FwdGroupsCnt += 1;
                        }
                    }

                    SNOOP_MEM_SET (pSnoopMacGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                }

                SNOOP_IS_PORT_PRESENT (u4Port, pSnoopMacGrpFwdEntry->PortBitmap,
                                       bResult);

                /* Update VLAN only when the port is newly added to the MAC  
                 * forwarding table */
                if (bResult == OSIX_FALSE)
                {
                    SNOOP_ADD_TO_PORT_LIST (u4Port,
                                            pSnoopMacGrpFwdEntry->PortBitmap);

                    /* Update VLAN multicast egress ports */
                    if (SnoopMiVlanUpdateDynamicMcastInfo
                        (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                         pSnoopMacGrpFwdEntry->VlanId, u4Port,
                         VLAN_ADD) != SNOOP_SUCCESS)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_FWD,
                                       SNOOP_FWD_NAME,
                                       "Updation to VLAN module for"
                                       " port addition for MAC mcast entry failed\r\n");
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }
                }
                break;

            case SNOOP_DEL_PORT:

                if (u1EntryFound == SNOOP_FALSE)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_FWD,
                                   SNOOP_FWD_NAME,
                                   "MAC forwarding entry not found\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                /* Check if the port is present as the member of any other Group
                 * which is in the GroupList of the MAC forward table.
                 * If it is present, then do not delete the port in the
                 * MAC forwarding bitmap. otherwise delete the port and update
                 * the same in the VLAN multicast forwarding table */
                if (u4VlanCnt == SNOOP_ZERO)
                {
                    SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                    pTempSnoopGroupEntry, tSnoopGroupEntry *)
                    {
                        if (SNOOP_MEM_CMP
                            (pTempSnoopGroupEntry->GroupIpAddr.au1Addr,
                             pSnoopGroupEntry->GroupIpAddr.au1Addr,
                             IPVX_MAX_INET_ADDR_LEN) == 0)
                        {
                            continue;
                        }

                        SNOOP_IS_PORT_PRESENT (u4Port,
                                               pTempSnoopGroupEntry->PortBitmap,
                                               bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            /* Port is present in a other group entry */
                            u1PortPresent = SNOOP_TRUE;
                            break;
                        }
                    }

                    if (u1PortPresent == SNOOP_TRUE)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        return SNOOP_SUCCESS;
                    }
                }
                if (SnoopVlanGetVlanEntry (u4Instance,
                                           pSnoopMacGrpFwdEntry->VlanId,
                                           pSnoopMacGrpFwdEntry->u1AddressType,
                                           &pSnoopVlanEntry) != SNOOP_SUCCESS)
                {
                    break;
                }

                pSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pSnoopRtrPortBmp == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for u4Instance %d\r\n", u4Instance);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                pTempSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pTempSnoopRtrPortBmp == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_GRP | SNOOP_TRC_CONTROL,
                                   SNOOP_GRP_NAME,
                                   "Error in allocating memory for pTempSnoopRtrPortBmp\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pTempSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (pSnoopMacGrpFwdEntry->
                                                          VlanId),
                                                         pSnoopMacGrpFwdEntry->
                                                         u1AddressType, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                SNOOP_MEM_CPY (pTempSnoopRtrPortBmp, pSnoopRtrPortBmp,
                               SNOOP_PORT_LIST_SIZE);
                SNOOP_IS_PORT_PRESENT (u4Port, pSnoopRtrPortBmp, bResult);
                if (bResult == OSIX_TRUE)
                {
                    OSIX_BITLIST_RESET_BIT (pTempSnoopRtrPortBmp,
                                            (UINT2) u4Port, u2PortListSize);
                }
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

                SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                          pSnoopMacGrpFwdEntry->PortBitmap);

                /* PORT delete indication will be given to VLAN module also. 
                 * It will clear the VLAN group entry. So no need to check this 
                 * VLAN API's return value here. Continue the delete indication 
                 * processing in snooping */
                if (SnoopMiVlanUpdateDynamicMcastInfo (u4Instance,
                                                       pSnoopMacGrpFwdEntry->
                                                       MacGroupAddr,
                                                       pSnoopMacGrpFwdEntry->
                                                       VlanId, u4Port,
                                                       VLAN_DELETE) !=
                    SNOOP_SUCCESS)
                {

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_FWD,
                                   SNOOP_FWD_NAME,
                                   "Updation to VLAN module for"
                                   " port addition for MAC mcast entry failed\r\n");
                }

                /* If the port bitmap becomes NULL delete the forward entry */
                if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->PortBitmap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* Delete the list of pointers to the group entry */
                    while ((pTempSnoopGroupEntry = (tSnoopGroupEntry *)
                            SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->
                                             GroupEntryList)) != NULL)
                    {
                        pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                        SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                          pTempSnoopGroupEntry);
                    }

                    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                  pSnoopMacGrpFwdEntry,
                                                  SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                    SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                    SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                                   pSnoopMacGrpFwdEntry->VlanId,
                                   sizeof (tSnoopTag));
                    SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                                   pSnoopMacGrpFwdEntry->MacGroupAddr,
                                   SNOOP_MAC_ADDR_LEN);

                    SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if (pSnoopInstInfo != NULL)
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->
                              SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType -
                                        1]);
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  MacMcastFwdEntry,
                                  (tRBElem *) pSnoopMacGrpFwdEntry);
                    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                    pSnoopMacGrpFwdEntry);
                    UtilPlstReleaseLocalPortList (pTempSnoopRtrPortBmp);
                    break;
                }
                else
                {
                    /* Check if there is only router ports are present in the
                     * bitmap then delete the group record from the MAC forwarding
                     * database */

                    /* SNOOP does not maintain group information only for the router
                     * ports. so, check if the portList in the group table and the
                     * router portList are same, then delete that entry from the
                     * group forwarding database, and delete the same from the VLAN
                     * forwarding table also only if Sparse mode is disabled*/

                    if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->PortBitmap,
                                       pTempSnoopRtrPortBmp,
                                       SNOOP_PORT_LIST_SIZE) == 0)
                    {
                        for (u4PortNum = 1;
                             u4PortNum <= SNOOP_MAX_PORTS_PER_INSTANCE;
                             u4PortNum++)
                        {
                            SNOOP_IS_PORT_PRESENT (u4PortNum,
                                                   pSnoopGroupEntry->PortBitmap,
                                                   bResult);
                            if (bResult == OSIX_TRUE)
                            {
                                break;

                            }
                        }
                        if (bResult != OSIX_TRUE)
                        {
                            for (u4PortNum = 1;
                                 u4PortNum <= SNOOP_MAX_PORTS_PER_INSTANCE;
                                 u4PortNum++)
                            {
                                SNOOP_IS_PORT_PRESENT (u4PortNum,
                                                       pTempSnoopRtrPortBmp,
                                                       bResult);

                                if (bResult == OSIX_FALSE)
                                {
                                    continue;
                                }

                                if (SnoopMiVlanUpdateDynamicMcastInfo
                                    (u4Instance,
                                     pSnoopMacGrpFwdEntry->MacGroupAddr,
                                     pSnoopMacGrpFwdEntry->VlanId, u4PortNum,
                                     VLAN_DELETE) != SNOOP_SUCCESS)
                                {
                                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK
                                        (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
                                    UtilPlstReleaseLocalPortList
                                        (pTempSnoopRtrPortBmp);
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE,
                                                   SNOOP_FWD_NAME,
                                                   "Updation to VLAN module "
                                                   "for port deletion for MAC mcast entry "
                                                   "failed\r\n");
                                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                               SNOOP_EXIT_DBG,
                                               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
                                    return SNOOP_FAILURE;
                                }
                            }

                            /* Delete the list of pointers to the group entry */
                            while ((pTempSnoopGroupEntry = (tSnoopGroupEntry *)
                                    SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->
                                                     GroupEntryList)) != NULL)
                            {
                                pTempSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                                SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->
                                                  GroupEntryList,
                                                  pTempSnoopGroupEntry);
                            }

                            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                          pSnoopMacGrpFwdEntry,
                                                          SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                            /* Indicate the aging out of MAC  forwarding entry */
                            SnoopDelEntry.u1NotifyType =
                                SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                            SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                                           pSnoopMacGrpFwdEntry->VlanId,
                                           sizeof (tSnoopTag));
                            SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                                           pSnoopMacGrpFwdEntry->MacGroupAddr,
                                           SNOOP_MAC_ADDR_LEN);
                            SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);
                            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                            if (pSnoopInstInfo != NULL)
                            {
                                pSnoopInfo =
                                    &(pSnoopInstInfo->
                                      SnoopInfo[pSnoopMacGrpFwdEntry->
                                                u1AddressType - 1]);
                                if ((pSnoopInfo != NULL)
                                    && (pSnoopInfo->u4FwdGroupsCnt != 0))
                                {
                                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                                }
                            }

                            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                          MacMcastFwdEntry,
                                          (tRBElem *) pSnoopMacGrpFwdEntry);
                            SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                            pSnoopMacGrpFwdEntry);
                        }
                    }
                    UtilPlstReleaseLocalPortList (pTempSnoopRtrPortBmp);
                    break;
                }
            default:
                break;
        }
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdUpdateMacFwdTable\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateIpFwdTable                             */
/*                                                                           */
/* Description        : This function updates the IP  based forwarding table */
/*                      by creating or deleting an forwarding entry          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                      pSnoopGroupEntry - pointer to group entry            */
/*                      SrcAddr          - Source Address                    */
/*                      u4Port           - Port number                       */
/*                      TempPortBitmap   - Port Bitmap                       */
/*                      u1EventType      - Event CREATE/DELETE/ADD PORT/     */
/*                                         DEL PORT                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdUpdateIpFwdTable (UINT4 u4Instance, tSnoopGroupEntry * pSnoopGroupEntry,
                          tIPvXAddr SrcAddr, UINT4 u4Port,
                          tSnoopPortBmp TempPortBitmap, UINT1 u1EventType)
{
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopFilter        SnoopDelEntry;
    tSnoopGroupEntry    SnoopTmpGroupEntry;
    UINT1              *pSnoopModPortBitMap = NULL;
    UINT1              *pSnoopAddedPortBitmap = NULL;;
    UINT1              *pSnoopDeletedPortBitmap = NULL;;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tIPvXAddr           TempSrcAddr;
    UINT4               u4Status = 0;
    UINT1               u1EntryFound = SNOOP_FALSE;
    UINT1               u1AddressType = 0;
    INT4                i4MRPPresent = SNOOP_FALSE;
    INT4                i4VlanCnt = 0;
#ifdef NPAPI_WANTED
#ifdef PIM_WANTED
    BOOL1               bResult = OSIX_FALSE;
#endif
#endif
    UINT1               u1TableDelete = SNOOP_FALSE;

    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    SNOOP_MEM_SET (&TempSrcAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdUpdateIpFwdTable\r\n");
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, pSnoopGroupEntry->VlanId,
                   sizeof (tSnoopTag));
    IpGrpFwdEntry.u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                    (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr));

    switch (u1EventType)
    {
        case SNOOP_CREATE_FWD_ENTRY:

            pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->
                                 IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry);
            if (pRBElem == NULL)
            {
                u1EntryFound = SNOOP_FALSE;
            }
            else
            {
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
                u1EntryFound = SNOOP_TRUE;
            }

            if (u1EntryFound == SNOOP_TRUE)
            {
                pSnoopModPortBitMap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (NULL == pSnoopModPortBitMap)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "Memory allocation failed for bitlist\r\n");
                    return SNOOP_FAILURE;
                }
                pSnoopAddedPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (NULL == pSnoopAddedPortBitmap)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "Memory allocation failed for bitlist\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopModPortBitMap);
                    return SNOOP_FAILURE;
                }
                pSnoopDeletedPortBitmap =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (NULL == pSnoopDeletedPortBitmap)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "Memory allocation failed for bitlist\r\n");
                    UtilPlstReleaseLocalPortList (pSnoopDeletedPortBitmap);
                    UtilPlstReleaseLocalPortList (pSnoopModPortBitMap);
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pSnoopModPortBitMap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pSnoopAddedPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                SNOOP_MEM_SET (pSnoopDeletedPortBitmap, 0,
                               SNOOP_PORT_LIST_SIZE);

                if (SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                   TempPortBitmap, SNOOP_PORT_LIST_SIZE) != 0)
                {
                    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_CPY (pSnoopModPortBitMap,
                                   pSnoopIpGrpFwdEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_XOR_PORT_BMP (pSnoopModPortBitMap, TempPortBitmap);

                    /* XOR of Incoming and existing port bit map will give the modified portlist.
                     * Let XPortList be the XOR output.
                     * AND (IncomingPortList,XPortlist) will give the added list of ports.
                     * AND (Existing,XPortList) will give the list of deleted ports.*/

                    if (SNOOP_MEM_CMP (pSnoopModPortBitMap,
                                       gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        SNOOP_MEM_CPY (pSnoopAddedPortBitmap,
                                       pSnoopModPortBitMap,
                                       SNOOP_PORT_LIST_SIZE);

                        SNOOP_MEM_CPY (pSnoopDeletedPortBitmap,
                                       pSnoopModPortBitMap,
                                       SNOOP_PORT_LIST_SIZE);

                        SNOOP_AND_PORT_BMP (pSnoopAddedPortBitmap,
                                            TempPortBitmap);

                        SNOOP_AND_PORT_BMP (pSnoopDeletedPortBitmap,
                                            pSnoopIpGrpFwdEntry->PortBitmap);
                    }

#ifdef NPAPI_WANTED
                    /* Indicate the hardware with the list of added/deleted ports */

                    if (SNOOP_MEM_CMP (pSnoopAddedPortBitmap,
                                       gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        if (SnoopNpUpdateMcastFwdEntry
                            (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                             pSnoopIpGrpFwdEntry->SrcIpAddr,
                             pSnoopIpGrpFwdEntry->GrpIpAddr,
                             pSnoopAddedPortBitmap,
                             SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_CONTROL_PATH_TRC |
                                           SNOOP_DBG_ALL_FAILURE |
                                           SNOOP_DBG_FWD, SNOOP_OS_RES_NAME,
                                           "Addition of IP forwarding"
                                           " entry in hardware failed\r\n");
                            SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL,
                                              SNOOP_DBG_FLAG, SNOOP_DBG_NP,
                                              SNOOP_NP_DBG,
                                              SnoopSysErrString
                                              [SYS_LOG_ADD_SNOOP_NP_IP_FORWARD_FAIL]);
                            UtilPlstReleaseLocalPortList
                                (pSnoopAddedPortBitmap);
                            UtilPlstReleaseLocalPortList
                                (pSnoopDeletedPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopModPortBitMap);
                            return SNOOP_FAILURE;
                        }

                    }

                    if (SNOOP_MEM_CMP (pSnoopDeletedPortBitmap,
                                       gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        if (SnoopNpUpdateMcastFwdEntry
                            (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                             pSnoopIpGrpFwdEntry->SrcIpAddr,
                             pSnoopIpGrpFwdEntry->GrpIpAddr,
                             pSnoopDeletedPortBitmap,
                             SNOOP_DEL_PORT) != SNOOP_SUCCESS)
                        {
                            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                           SNOOP_CONTROL_PATH_TRC |
                                           SNOOP_DBG_ALL_FAILURE |
                                           SNOOP_DBG_FWD, SNOOP_OS_RES_NAME,
                                           "Deletion of IP forwarding"
                                           " entry from hardware failed\r\n");
                            SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL,
                                              SNOOP_DBG_FLAG, SNOOP_DBG_NP,
                                              SNOOP_NP_DBG,
                                              SnoopSysErrString
                                              [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                            UtilPlstReleaseLocalPortList
                                (pSnoopAddedPortBitmap);
                            UtilPlstReleaseLocalPortList
                                (pSnoopDeletedPortBitmap);
                            UtilPlstReleaseLocalPortList (pSnoopModPortBitMap);
                            return SNOOP_FAILURE;
                        }

                    }
#endif
                    /*Update the software entries */

                    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);

                    SNOOP_UPDATE_PORT_LIST (TempPortBitmap,
                                            pSnoopIpGrpFwdEntry->PortBitmap);
                    SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                        SNOOP_NP_NAME,
                                        "Successfully created IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopGroupEntry->
                                                          VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                           GroupIpAddr.
                                                           au1Addr));

                }
#ifdef L2RED_WANTED
                SnoopActiveRedGrpSync (u4Instance,
                                       pSnoopIpGrpFwdEntry->SrcIpAddr,
                                       pSnoopGroupEntry,
                                       pSnoopIpGrpFwdEntry->PortBitmap);
#endif
                UtilPlstReleaseLocalPortList (pSnoopAddedPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopDeletedPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopModPortBitMap);
                return SNOOP_SUCCESS;
            }

            /* IP forward entry not found, so create a new one */
            if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry)
                == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_NAME,
                               "Memory allocation for IP forward entry failed\r\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for port %d\r\n", u4Port);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }

            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_RESRC,
                                SNOOP_OS_RES_NAME,
                                "Memory allocation done for forwarding entry for group %s \r\n",
                                SnoopPrintIPvxAddress (pSnoopGroupEntry->
                                                       GroupIpAddr));

            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0,
                           sizeof (tSnoopIpGrpFwdEntry));

            /* Update the fields of the IP entry */
            pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_SUCCESS;
            pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;
            SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId,
                           pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
            pSnoopIpGrpFwdEntry->u1AddressType = u1AddressType;
            pSnoopIpGrpFwdEntry->u1EntryTypeFlag =
                pSnoopGroupEntry->u1EntryTypeFlag;
            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                           (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));
            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                           (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr),
                           sizeof (tIPvXAddr));
            SNOOP_UPDATE_PORT_LIST (TempPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);
            u4Status =
                RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           (tRBElem *) pSnoopIpGrpFwdEntry);

            if (u4Status == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for IP forward Entry \r\n");
                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }

            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if (pSnoopInstInfo != NULL)
            {
                pSnoopInfo = &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                if (pSnoopInfo != NULL)
                {
                    pSnoopInfo->u4FwdGroupsCnt += 1;
                }
            }
#ifdef L2RED_WANTED
            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                          pSnoopIpGrpFwdEntry,
                                          SNOOP_RED_BULK_UPD_IP_FWD_REMOVE);
#endif
#ifdef NPAPI_WANTED
            if ((SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0) ||
                ((SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                 gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0) &&
                 (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)))
            {
                if (SnoopNpUpdateMcastFwdEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                     pSnoopIpGrpFwdEntry->PortBitmap,
                     SNOOP_CREATE_FWD_ENTRY) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Addition of IP forwarding entry "
                                   "to hardware failed\r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_NP,
                                           SNOOP_NP_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_ADD_SNOOP_NP_IP_FORWARD_FAIL],
                                           "for group ip %x\r\n",
                                           pSnoopIpGrpFwdEntry->GrpIpAddr);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  IpMcastFwdEntry,
                                  (tRBElem *) pSnoopIpGrpFwdEntry);
                    if (pSnoopInstInfo != NULL)
                    {
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                    return SNOOP_FAILURE;
                }
                SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                    SNOOP_NP_NAME,
                                    "Successfully created IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                    SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                                    SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                    SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                       GroupIpAddr.au1Addr));

            }
#endif
#ifdef L2RED_WANTED
            SnoopActiveRedGrpSync (u4Instance,
                                   pSnoopIpGrpFwdEntry->SrcIpAddr,
                                   pSnoopGroupEntry,
                                   pSnoopIpGrpFwdEntry->PortBitmap);
#endif

#ifdef NPAPI_WANTED
            /* If Group addition failed in Hardware
             * then no need to start the timer here as Hardware Failure case
             * is already handled.
             */
            if (pSnoopIpGrpFwdEntry->u1AddGroupStatus != HW_ADD_GROUP_FAIL)
            {
                /* Start forward entry timer */
                pSnoopIpGrpFwdEntry->EntryTimer.u4Instance = u4Instance;
                pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType =
                    SNOOP_IP_FWD_ENTRY_TIMER;

                if (SnoopTmrStartTimer (&pSnoopIpGrpFwdEntry->EntryTimer,
                                        SNOOP_IP_FWD_ENTRY_TIMER_VAL)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "Unable to start IP forwarding entry timer \r\n");
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  IpMcastFwdEntry,
                                  (tRBElem *) pSnoopIpGrpFwdEntry);
                    if (pSnoopInstInfo != NULL)
                    {
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                    return SNOOP_FAILURE;
                }
            }
#endif
#ifdef L2RED_WANTED
            SnoopActiveRedGrpSync (u4Instance,
                                   pSnoopIpGrpFwdEntry->SrcIpAddr,
                                   pSnoopGroupEntry,
                                   pSnoopIpGrpFwdEntry->PortBitmap);
#endif
            break;

        case SNOOP_DELETE_FWD_ENTRY:

            pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->
                                 IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry);

            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if (pRBElem == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_NAME,
                               "IP forwarding entry not found\r\n");
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }

            if (pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
            }

#ifdef NPAPI_WANTED
#ifdef PIM_WANTED
            SNOOP_IS_PORT_PRESENT (u4Port, TempPortBitmap, bResult);
            if (bResult == OSIX_TRUE)
            {
                if (SnoopNpUpdateMcastFwdEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                     TempPortBitmap, SNOOP_DEL_PORT) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_FWD,
                                   SNOOP_OS_RES_NAME,
                                   "Deletion of IP forwarding"
                                   " entry from hardware failed\r\n");
                    SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                      SNOOP_DBG_NP, SNOOP_NP_DBG,
                                      SnoopSysErrString
                                      [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                }
            }
#endif /* PIM_WANTED */
            if (SnoopNpUpdateMcastFwdEntry
                (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                 pSnoopIpGrpFwdEntry->SrcIpAddr,
                 pSnoopIpGrpFwdEntry->GrpIpAddr,
                 pSnoopIpGrpFwdEntry->PortBitmap,
                 SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_FWD,
                               SNOOP_OS_RES_NAME, "Deletion of IP forwarding"
                               " entry from hardware failed\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_NP, SNOOP_NP_DBG,
                                  SnoopSysErrString
                                  [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }
#endif
            /* Indicate the aging out of IP forwarding entry */
            SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
            SNOOP_MEM_CPY (SnoopDelEntry.VlanId, pSnoopIpGrpFwdEntry->VlanId,
                           sizeof (tSnoopTag));
            SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                           &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                           &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

            /* Indicate the aging out of IP forwarding entry */

            SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

#ifdef L2RED_WANTED
            SnoopActiveRedGrpDeleteSync (u4Instance,
                                         pSnoopIpGrpFwdEntry->SrcIpAddr,
                                         pSnoopGroupEntry);
#endif
            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                          (tRBElem *) pSnoopIpGrpFwdEntry);
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if (pSnoopInstInfo != NULL)
            {
                pSnoopInfo = &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
                {
                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                }
            }
            SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);

            SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                SNOOP_NP_NAME,
                                "Successfully deleted IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                                SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                   GroupIpAddr.au1Addr));

            break;

        case SNOOP_ADD_PORT:

#ifdef NPAPI_WANTED
            i4MRPPresent =
                SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                    pSnoopGroupEntry->VlanId);
#endif
            /* Get the pVlan Mapping information */
            L2PvlanMappingInfo.u4ContextId = u4Instance;
            L2PvlanMappingInfo.InVlanId =
                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
            L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

            if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                                      L2PvlanMappingInfo.
                                                      pMappedVlans) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory allocation for Snoop PVLAN LIST failed\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }
            L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            /* Update the port bitmap for the IP forward entry */
            pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                      IpMcastFwdEntry);
            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                          pSnoopIpGrpFwdEntry,
                                          SNOOP_RED_BULK_UPD_IP_FWD_REMOVE);
            while (pRBElem != NULL)
            {
                pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                             IpMcastFwdEntry, pRBElem, NULL);
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                /* scanning is done for the associated vlans in the PVLAN domain and
                 * also for the incoming vlan identifier */
                for (i4VlanCnt = SNOOP_ZERO;
                     i4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans;
                     i4VlanCnt++)
                {
                    SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0,
                                   sizeof (tSnoopGroupEntry));

                    SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                                   sizeof (tSnoopGroupEntry));
                    if (i4VlanCnt != SNOOP_ZERO)
                    {
                        SNOOP_OUTER_VLAN (SnoopTmpGroupEntry.VlanId)
                            = L2PvlanMappingInfo.pMappedVlans[i4VlanCnt - 1];
                        SNOOP_INNER_VLAN (SnoopTmpGroupEntry.VlanId)
                            = SNOOP_INVALID_VLAN_ID;
                    }

                    /* If the source address is not 0, update only the (S,G) entry
                     * alone */
                    if (SNOOP_MEM_CMP ((UINT1 *) &SrcAddr,
                                       (UINT1 *) &gNullAddr,
                                       sizeof (tIPvXAddr)) != 0)
                    {
                        if ((SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->VlanId,
                                            &SnoopTmpGroupEntry.VlanId,
                                            sizeof (tSnoopTag)) == 0) &&
                            (SNOOP_MEM_CMP ((UINT1 *) &(pSnoopIpGrpFwdEntry->
                                                        SrcIpAddr),
                                            (UINT1 *) &SrcAddr,
                                            sizeof (tIPvXAddr)) == 0)
                            &&
                            (SNOOP_MEM_CMP
                             ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                              (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr),
                              sizeof (tIPvXAddr)) == 0))
                        {
                            SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                    pSnoopIpGrpFwdEntry->
                                                    PortBitmap);
#ifdef NPAPI_WANTED
                            if (i4MRPPresent == SNOOP_FALSE)
                            {

                                if (SnoopNpUpdateMcastFwdEntry
                                    (u4Instance,
                                     pSnoopIpGrpFwdEntry->VlanId,
                                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                                     pSnoopIpGrpFwdEntry->PortBitmap,
                                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                                {
                                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK
                                        (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE |
                                                   SNOOP_DBG_RESRC,
                                                   SNOOP_OS_RES_NAME,
                                                   "Addition of port for "
                                                   "IP forwarding entry to hardware "
                                                   "failed\r\n");
                                    SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL,
                                                      SNOOP_DBG_FLAG,
                                                      SNOOP_DBG_NP,
                                                      SNOOP_NP_DBG,
                                                      SnoopSysErrString
                                                      [SYS_LOG_ADD_SNOOP_NP_IP_FORWARD_FAIL]);
                                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                               SNOOP_EXIT_DBG,
                                               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                                    return SNOOP_FAILURE;
                                }
#ifdef L2RED_WANTED
                                SnoopActiveRedGrpSync (u4Instance,
                                                       pSnoopIpGrpFwdEntry->
                                                       SrcIpAddr,
                                                       pSnoopGroupEntry,
                                                       pSnoopIpGrpFwdEntry->
                                                       PortBitmap);
#endif
                            }
#endif
                            break;
                        }
                    }
                    else
                    {
                        /* If the source address is 0, update only the (S,G) 
                         * and (*,G) entries for the group G */
                        if ((SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->VlanId,
                                            &SnoopTmpGroupEntry.VlanId,
                                            sizeof (tSnoopTag)) == 0) &&
                            (SNOOP_MEM_CMP
                             (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr,
                              pSnoopGroupEntry->GroupIpAddr.au1Addr,
                              IPVX_MAX_INET_ADDR_LEN) == 0))
                        {
                            SNOOP_ADD_TO_PORT_LIST (u4Port,
                                                    pSnoopIpGrpFwdEntry->
                                                    PortBitmap);
#ifdef NPAPI_WANTED
                            if (i4MRPPresent == SNOOP_FALSE)
                            {
                                if (SnoopNpUpdateMcastFwdEntry
                                    (u4Instance,
                                     pSnoopIpGrpFwdEntry->VlanId,
                                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                                     pSnoopIpGrpFwdEntry->PortBitmap,
                                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                                {
                                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK
                                        (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE,
                                                   SNOOP_OS_RES_NAME,
                                                   "Addition of port for IP "
                                                   "forwarding entry to h/w failed\r\n");
                                    SYSLOG_SNOOP_MSG_ARG2
                                        (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                         SNOOP_DBG_NP, SNOOP_NP_DBG,
                                         SnoopSysErrString
                                         [SYS_LOG_ADD_SNOOP_NP_IP_FORWARD_FAIL],
                                         "for u4Instance %d\r\n", u4Instance);
                                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                               SNOOP_EXIT_DBG,
                                               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                                    return SNOOP_FAILURE;
                                }

#if L2RED_WANTED
                                SnoopActiveRedGrpSync (u4Instance,
                                                       pSnoopIpGrpFwdEntry->
                                                       SrcIpAddr,
                                                       pSnoopGroupEntry,
                                                       pSnoopIpGrpFwdEntry->
                                                       PortBitmap);
#endif

                            }
#endif
                            pRBElem = pRBNextElem;
                            continue;
                        }
                    }
                }
                pRBElem = pRBNextElem;
            }

            SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                SNOOP_NP_NAME,
                                "Successfully updated IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                                SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                   GroupIpAddr.au1Addr));

            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                 L2PvlanMappingInfo.
                                                 pMappedVlans);

            break;

        case SNOOP_DEL_PORT:
#ifndef IGS_OPTIMIZE
#ifdef NPAPI_WANTED
            i4MRPPresent =
                SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                    pSnoopGroupEntry->VlanId);
#endif
            /* Get the pVlan Mapping information */
            L2PvlanMappingInfo.u4ContextId = u4Instance;
            L2PvlanMappingInfo.InVlanId =
                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
            L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

            if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                                      L2PvlanMappingInfo.
                                                      pMappedVlans) == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Memory allocation for Snoop PVLAN LIST failed\r\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for u4Instance %d\r\n", u4Instance);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }
            L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

            /* Update the port bitmap for the IP forward entry */
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                IpMcastFwdEntry);
            while (pRBElem != NULL)
            {
                pRBNextElem =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   IpMcastFwdEntry, pRBElem, NULL);
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                /* scanning is done for the associated vlans in the PVLAN domain and
                 * also for the incoming vlan identifier */
                for (i4VlanCnt = SNOOP_ZERO;
                     i4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans;
                     i4VlanCnt++)
                {
                    SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0,
                                   sizeof (tSnoopGroupEntry));

                    SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                                   sizeof (tSnoopGroupEntry));
                    if (i4VlanCnt != SNOOP_ZERO)
                    {
                        SNOOP_OUTER_VLAN (SnoopTmpGroupEntry.VlanId)
                            = L2PvlanMappingInfo.pMappedVlans[i4VlanCnt - 1];
                        SNOOP_INNER_VLAN (SnoopTmpGroupEntry.VlanId)
                            = SNOOP_INVALID_VLAN_ID;
                    }

                    /* If the source address is not 0, update only the (S,G) entry
                     * alone else update (*, G) entries */
                    if ((SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->VlanId,
                                        &SnoopTmpGroupEntry.VlanId,
                                        sizeof (tSnoopTag)) == 0) &&
                        ((SNOOP_MEM_CMP
                          ((UINT1 *) &SrcAddr, (UINT1 *) &gNullAddr,
                           sizeof (tIPvXAddr)) == 0)
                         ||
                         (SNOOP_MEM_CMP
                          ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                           (UINT1 *) &SrcAddr, sizeof (tIPvXAddr)) == 0))
                        &&
                        (SNOOP_MEM_CMP
                         ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                          (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr),
                          sizeof (tIPvXAddr)) == 0))
                    {
                        IPVX_ADDR_COPY ((UINT1 *) &TempSrcAddr,
                                        (UINT1 *) &(pSnoopIpGrpFwdEntry->
                                                    SrcIpAddr));
                        if (SnoopFwdIpFwdEntryDelPort (u4Instance,
                                                       pSnoopIpGrpFwdEntry,
                                                       u4Port, i4MRPPresent,
                                                       &u1TableDelete) !=
                            SNOOP_SUCCESS)
                        {
                            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                 L2PvlanMappingInfo.
                                                                 pMappedVlans);
                            return SNOOP_FAILURE;
                        }

                        if (SnoopFwdUpdateIpFwdRtrEntryOnDelPort
                            (u4Instance, &SnoopTmpGroupEntry,
                             (tSnoopIpGrpFwdEntry *) pRBNextElem, TempSrcAddr)
                            != SNOOP_SUCCESS)
                        {
                            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                 L2PvlanMappingInfo.
                                                                 pMappedVlans);
                            return SNOOP_FAILURE;
                        }

                        if (u1TableDelete == SNOOP_FALSE)
                        {
#ifdef L2RED_WANTED
                            SnoopActiveRedGrpSync (u4Instance,
                                                   pSnoopIpGrpFwdEntry->
                                                   SrcIpAddr, pSnoopGroupEntry,
                                                   pSnoopIpGrpFwdEntry->
                                                   PortBitmap);
#endif
                        }

                        else
                        {
#ifdef L2RED_WANTED
                            SnoopActiveRedGrpDeleteSync (u4Instance,
                                                         TempSrcAddr,
                                                         pSnoopGroupEntry);
#endif
                        }
                    }
                }

                pRBElem = pRBNextElem;
            }
            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                 L2PvlanMappingInfo.
                                                 pMappedVlans);
#else
            pRBElem =
                RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->
                           IpMcastFwdEntry, &IpGrpFwdEntry);
            if (pRBElem != NULL)
            {
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                if (SnoopFwdIpFwdEntryDelPort (u4Instance,
                                               pSnoopIpGrpFwdEntry,
                                               u4Port, i4MRPPresent,
                                               &u1TableDelete) != SNOOP_SUCCESS)
                {
                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                               sizeof (tSnoopGroupEntry));

                if (SnoopFwdUpdateIpFwdRtrEntryOnDelPort
                    (u4Instance, &SnoopTmpGroupEntry,
                     (tSnoopIpGrpFwdEntry *) pRBNextElem, SrcAddr)
                    != SNOOP_SUCCESS)
                {
                    return SNOOP_FAILURE;
                }

                if (u1TableDelete == SNOOP_FALSE)
                {
#ifdef L2RED_WANTED
                    SnoopActiveRedGrpSync (u4Instance,
                                           pSnoopIpGrpFwdEntry->SrcIpAddr,
                                           pSnoopGroupEntry,
                                           pSnoopIpGrpFwdEntry->PortBitmap);
#endif
                }

                else
                {
#ifdef L2RED_WANTED
                    SnoopActiveRedGrpDeleteSync (u4Instance, TempSrcAddr,
                                                 pSnoopGroupEntry);
#endif
                }
            }
#endif
            SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                SNOOP_NP_NAME,
                                "Successfully updated IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                                SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                   GroupIpAddr.au1Addr));

            break;

        case SNOOP_ADD_PORTLIST:

#ifdef NPAPI_WANTED
            i4MRPPresent =
                SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                                    pSnoopGroupEntry->VlanId);
#endif

            pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->
                                 IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry);
            if (pRBElem == NULL)
            {
                u1EntryFound = SNOOP_FALSE;
            }
            else
            {
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
                u1EntryFound = SNOOP_TRUE;
            }

            if (u1EntryFound == SNOOP_TRUE)
            {
#ifdef NPAPI_WANTED
                if (i4MRPPresent == SNOOP_FALSE)
                {
                    if (SnoopNpUpdateMcastFwdEntry
                        (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                         pSnoopIpGrpFwdEntry->SrcIpAddr,
                         pSnoopIpGrpFwdEntry->GrpIpAddr,
                         pSnoopIpGrpFwdEntry->PortBitmap,
                         SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                       "Deletion of IP forwarding"
                                       " entry from hardware failed\r\n");
                        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                          SNOOP_DBG_NP, SNOOP_NP_DBG,
                                          SnoopSysErrString
                                          [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }
                }
#endif
                if (SNOOP_MEM_CMP (TempPortBitmap, gNullPortBitMap,
                                   SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* Indicate the aging out of IP forwarding entry */
                    SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                    SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                                   pSnoopIpGrpFwdEntry->VlanId,
                                   sizeof (tSnoopTag));
                    SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                                   &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                                   IPVX_MAX_INET_ADDR_LEN);

                    SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                                   &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                                   IPVX_MAX_INET_ADDR_LEN);

                    SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
                    SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

                    SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);
                    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  IpMcastFwdEntry,
                                  (tRBElem *) pSnoopIpGrpFwdEntry);
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if (pSnoopInstInfo != NULL)
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                    SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                        SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                        SNOOP_NP_NAME,
                                        "Successfully updated IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                        SNOOP_OUTER_VLAN (pSnoopGroupEntry->
                                                          VlanId),
                                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                        SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                           GroupIpAddr.
                                                           au1Addr));

                    return SNOOP_SUCCESS;
                }
            }
            else
            {
                if (SNOOP_MEM_CMP (TempPortBitmap, gNullPortBitMap,
                                   SNOOP_PORT_LIST_SIZE) == 0)
                {
                    break;
                }

                /* IP forward entry not found, so create a new one */
                if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK
                    (u4Instance, pSnoopIpGrpFwdEntry) == NULL)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Memory allocation for IP forward entry \r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for group %s \r\n",
                                           SnoopDelEntry.GrpAddress);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                    return SNOOP_FAILURE;
                }

                SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0,
                               sizeof (tSnoopIpGrpFwdEntry));

                /* Update the fields of the IP entry */
                SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId,
                               pSnoopGroupEntry->VlanId, sizeof (tSnoopTag));
                pSnoopIpGrpFwdEntry->u1AddressType = u1AddressType;
                SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                               (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));
                SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                               (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr),
                               sizeof (tIPvXAddr));
                pSnoopIpGrpFwdEntry->u1EntryTypeFlag =
                    pSnoopGroupEntry->u1EntryTypeFlag;
                u4Status =
                    RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->
                               IpMcastFwdEntry,
                               (tRBElem *) pSnoopIpGrpFwdEntry);

                if (u4Status == RB_FAILURE)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                   SNOOP_OS_RES_NAME,
                                   "RBTree Addition Failed for IP "
                                   "forward Entry \r\n");
                    SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                      SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                      SnoopSysErrString
                                      [SYS_LOG_SNP_RB_TREE_ADD_FAIL]);
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if (pSnoopInstInfo != NULL)
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                    if (pSnoopInfo != NULL)
                    {
                        pSnoopInfo->u4FwdGroupsCnt += 1;
                    }
                }
            }

            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            SNOOP_UPDATE_PORT_LIST (TempPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);

#ifdef NPAPI_WANTED
            if (i4MRPPresent == SNOOP_FALSE)
            {
                if (SnoopNpUpdateMcastFwdEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                     pSnoopIpGrpFwdEntry->PortBitmap,
                     SNOOP_CREATE_FWD_ENTRY) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Deletion of IP forwarding"
                                   " entry from hardware failed\r\n");
                    SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                      SNOOP_DBG_NP, SNOOP_NP_DBG,
                                      SnoopSysErrString
                                      [SYS_LOG_DEL_SNOOP_NP_IP_FORWARD_FAIL]);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
                    return SNOOP_FAILURE;
                }
            }
#endif
            SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_NP | SNOOP_DBG_FWD,
                                SNOOP_NP_NAME,
                                "Successfully updated IPMC entry for vlan %d Source %x ,Group %x\r\n",
                                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId),
                                SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->
                                                   GroupIpAddr.au1Addr));

            break;

        default:
            break;
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdUpdateIpFwdTable\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdGetMacForwardingEntry                        */
/*                                                                           */
/* Description        : This function gets the MAC forwarding entry          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr    - Grp MAC address                         */
/*                      u1AddressType  -  Indicates IPv4/IPv6                */
/*                                                                           */
/* Output(s)          : pointer to the mac forwarding entry or NULL          */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdGetMacForwardingEntry (UINT4 u4Instance, tSnoopTag VlanId,
                               tSnoopMacAddr GrpMacAddr, UINT1 u1AddressType,
                               tSnoopMacGrpFwdEntry ** ppSnoopMacGrpFwdEntry)
{
    tRBElem            *pRBElem = NULL;
    tSnoopMacGrpFwdEntry SnoopMacGrpFwdEntry;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdGetMacForwardingEntry\r\n");
    SNOOP_MEM_SET (&SnoopMacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));

    SNOOP_MEM_CPY (SnoopMacGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopMacGrpFwdEntry.u1AddressType = u1AddressType;
    SNOOP_MEM_CPY (SnoopMacGrpFwdEntry.MacGroupAddr, GrpMacAddr,
                   SNOOP_MAC_ADDR_LEN);

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                         (tRBElem *) & SnoopMacGrpFwdEntry);

    if (pRBElem == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_FWD_NAME, "MAC forwarding Entry not found\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdGetMacForwardingEntry\r\n");
        return SNOOP_FAILURE;
    }
    else
    {
        *ppSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdGetMacForwardingEntry\r\n");
    return SNOOP_SUCCESS;
}

 /*****************************************************************************/
/* Function Name      : SnoopFwdGetIpForwardingEntryForAllSrc                */
/*                                                                           */
/* Description        : This function gets the IP forwarding entry           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr    - Grp IP address                          */
/*                      SrcAddr    - Src IP address                          */
/*                                                                           */
/* Output(s)          : pointer to the IP forwarding entry or NULL           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdGetIpForwardingEntryForAllSrc (UINT4 u4Instance, tSnoopTag VlanId,
                                       tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                                       tSnoopIpGrpFwdEntry **
                                       ppRetSnoopIpGrpFwdEntry)
{
    tRBElem            *pRBElem = NULL;
    tSnoopIpGrpFwdEntry SnoopIpGrpFwdEntry;
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdGetIpForwardingEntryForAllSrc\r\n");
    SNOOP_MEM_SET (&SnoopIpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_MEM_CPY (SnoopIpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopIpGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;
    SNOOP_MEM_CPY ((UINT1 *) &(SnoopIpGrpFwdEntry.GrpIpAddr),
                   (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));
    SNOOP_MEM_CPY ((UINT1 *) &(SnoopIpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & SnoopIpGrpFwdEntry);

    if (pRBElem == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdGetIpForwardingEntryForAllSrc\r\n");
    }
    else
    {
        *ppRetSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
        return SNOOP_SUCCESS;
    }
    /* Check for (*,G) */

    if (SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_ENABLE)
    {

        MEMSET (&(SnoopIpGrpFwdEntry.SrcIpAddr), 0, sizeof (tIPvXAddr));
        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                             (tRBElem *) & SnoopIpGrpFwdEntry);

        if (pRBElem == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopFwdGetIpForwardingEntryForAllSrc\r\n");
        }
        else
        {
            *ppRetSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
            return SNOOP_SUCCESS;
        }

    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdGetIpForwardingEntryForAllSrc\r\n");

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdGetIpForwardingEntry                         */
/*                                                                           */
/* Description        : This function gets the IP forwarding entry           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                      VlanId     - VLAN identifier                         */
/*                      GrpAddr    - Grp IP address                          */
/*                      SrcAddr    - Src IP address                          */
/*                                                                           */
/* Output(s)          : pointer to the IP forwarding entry or NULL           */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdGetIpForwardingEntry (UINT4 u4Instance, tSnoopTag VlanId,
                              tIPvXAddr GrpAddr, tIPvXAddr SrcAddr,
                              tSnoopIpGrpFwdEntry ** ppRetSnoopIpGrpFwdEntry)
{
    tRBElem            *pRBElem = NULL;
    tSnoopIpGrpFwdEntry SnoopIpGrpFwdEntry;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdGetIpForwardingEntry\r\n");
    SNOOP_MEM_SET (&SnoopIpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_MEM_CPY (SnoopIpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));
    SnoopIpGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;
    SNOOP_MEM_CPY ((UINT1 *) &(SnoopIpGrpFwdEntry.GrpIpAddr),
                   (UINT1 *) &GrpAddr, sizeof (tIPvXAddr));
    SNOOP_MEM_CPY ((UINT1 *) &(SnoopIpGrpFwdEntry.SrcIpAddr),
                   (UINT1 *) &SrcAddr, sizeof (tIPvXAddr));

    pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & SnoopIpGrpFwdEntry);

    if (pRBElem == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdGetIpForwardingEntry\r\n");
        return SNOOP_FAILURE;
    }
    else
    {
        *ppRetSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdGetIpForwardingEntry\r\n");

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdDeleteMcastFwdEntries                        */
/*                                                                           */
/* Description        : This function deletes the multicast forwarding       */
/*                      entries for a instance                               */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                      u1AddressType    - Indicates IPv4/IPv6               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdDeleteMcastFwdEntries (UINT4 u4Instance, UINT1 u1AddressType)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopFilter        SnoopDelEntry;
    tVlanId             VlanId;
    UINT1               u1IsPimEnabled = SNOOP_FALSE;
#ifdef PIM_WANTED
    UINT1               u1EventType = SNOOP_DELETE_FWD_ENTRY;
    BOOL1               bIsSrcGrpFound = OSIX_FALSE;
#elif NPAPI_WANTED
    UINT1               u1EventType = SNOOP_DELETE_FWD_ENTRY;
#endif

#ifdef PIM_WANTED
    if (PimIsPimEnabled () == PIM_SUCCESS)
    {
        u1IsPimEnabled = SNOOP_TRUE;
    }
#endif

    SNOOP_MEM_SET (&VlanId, 0, sizeof (tVlanId));
    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdDeleteMcastFwdEntries\r\n");
    /* If the forwarding mode is MAC based forwarding mode */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry != NULL)
        {
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                MacMcastFwdEntry);

            while (pRBElem != NULL)
            {
                pRBNextElem =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   MacMcastFwdEntry, pRBElem, NULL);

                pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

                if (pSnoopMacGrpFwdEntry->u1AddressType != u1AddressType)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }
                /* Delete the list of pointers to the group entry */
                while ((pSnoopGroupEntry = (tSnoopGroupEntry *)
                        SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->GroupEntryList))
                       != NULL)
                {
                    pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                    SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                      pSnoopGroupEntry);
                }
                SNOOP_MEM_SET (pSnoopMacGrpFwdEntry->PortBitmap, 0,
                               SNOOP_PORT_LIST_SIZE);

                /* Indicate the aging out of MAC  forwarding entry */
                SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                               pSnoopMacGrpFwdEntry->VlanId,
                               sizeof (tSnoopTag));
                SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                               pSnoopMacGrpFwdEntry->MacGroupAddr,
                               SNOOP_MAC_ADDR_LEN);
                SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if ((pSnoopInstInfo != NULL) && (pSnoopMacGrpFwdEntry != NULL))
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }

                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                              MacMcastFwdEntry,
                              (tRBElem *) pSnoopMacGrpFwdEntry);

                SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                pSnoopMacGrpFwdEntry);

                pRBElem = pRBNextElem;
            }

            /* Delete the MAC forward table entries form hardware */
            SnoopMiVlanDeleteAllDynamicMcastInfo (u4Instance,
                                                  SNOOP_INVALID_PORT,
                                                  u1AddressType);
        }
    }
    else                        /* forwarding mode is IP based */
    {
        if (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry != NULL)
        {
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                IpMcastFwdEntry);

            while (pRBElem != NULL)
            {
                pRBNextElem =
                    RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                   IpMcastFwdEntry, pRBElem, NULL);
                pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
                SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
                if (u1IsPimEnabled == SNOOP_TRUE)
                {
#ifdef PIM_WANTED
                    if (SnoopUtilGetPIMInterfaceStatus
                        (pSnoopIpGrpFwdEntry->VlanId) == SNOOP_SUCCESS)
                    {
                        if ((SNOOP_MCAST_FWD_MODE (u4Instance) ==
                             SNOOP_MCAST_FWD_MODE_IP)
                            && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) ==
                                SNOOP_ENABLE))
                        {
                            VlanId =
                                SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId);

                            bIsSrcGrpFound =
                                (BOOL1) SPimPortCheckSGVlanId (VlanId,
                                                               pSnoopIpGrpFwdEntry->
                                                               SrcIpAddr,
                                                               pSnoopIpGrpFwdEntry->
                                                               GrpIpAddr);
                            if (bIsSrcGrpFound == OSIX_TRUE)
                            {
                                u1EventType = SNOOP_DEL_PORT;
                            }
                        }
                        UNUSED_PARAM (u1EventType);
                    }
#endif
#ifdef NPAPI_WANTED
                    if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
                    {
                        if (SnoopNpUpdateMcastFwdEntry
                            (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                             pSnoopIpGrpFwdEntry->SrcIpAddr,
                             pSnoopIpGrpFwdEntry->GrpIpAddr,
                             pSnoopIpGrpFwdEntry->PortBitmap,
                             u1EventType) != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Mcast IP forwarding entry failed\r\n");
                        }
                    }
#endif
                    /* Indicate the aging out of IP forwarding  entry */
                    SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                    SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                                   pSnoopIpGrpFwdEntry->VlanId,
                                   sizeof (tSnoopTag));

                    SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                                   &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                                   IPVX_MAX_INET_ADDR_LEN);

                    SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                                   &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                                   IPVX_MAX_INET_ADDR_LEN);
                    SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
                    SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

                    SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);
                }

                SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                               SNOOP_PORT_LIST_SIZE);
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                              (tRBElem *) pSnoopIpGrpFwdEntry);
                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if (pSnoopInstInfo != NULL)
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->SnoopInfo[u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }

                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);

                pRBElem = pRBNextElem;
            }
        }
    }
    if (u1IsPimEnabled == SNOOP_FALSE)
    {
#ifdef NPAPI_WANTED
        /* When PIM is not enabled, remove all IPMC entries in bulk */
        SnoopNpClearHwIPMcastTable (u4Instance);
#endif
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdDeleteMcastFwdEntries\r\n");

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdDeleteVlanMcastFwdEntries                    */
/*                                                                           */
/* Description        : This function deletes the multicast forwarding       */
/*                      entries for a VLAN                                   */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                      Vlan Id          - VLAN identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdDeleteVlanMcastFwdEntries (UINT4 u4Instance, tSnoopTag VlanId,
                                   UINT1 u1AddressType)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopFilter        SnoopDelEntry;
    tL2PvlanMappingInfo L2PvlanMappingInfo;

    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                       "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for vlanid%d\r\n", SNOOP_OUTER_VLAN (VlanId));
        return SNOOP_FAILURE;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    /* If the forwarding mode is MAC based forwarding mode */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        pRBElem =
            RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry);

        while (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                         MacMcastFwdEntry, pRBElem, NULL);
            pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

            if ((SnoopIsVlanPresentInFwdTbl
                 (SNOOP_OUTER_VLAN (pSnoopMacGrpFwdEntry->VlanId),
                  SNOOP_OUTER_VLAN (VlanId),
                  &L2PvlanMappingInfo) == SNOOP_FALSE))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (u1AddressType)
            {
                if (pSnoopMacGrpFwdEntry->u1AddressType != u1AddressType)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }
            }

            /* Delete the list of pointers to the group entry */
            while ((pSnoopGroupEntry = (tSnoopGroupEntry *)
                    SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->GroupEntryList)) !=
                   NULL)
            {
                pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                  pSnoopGroupEntry);
            }
            /* Indicate the aging out of MAC  forwarding entry */
            SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
            SNOOP_MEM_CPY (&SnoopDelEntry.VlanId,
                           pSnoopMacGrpFwdEntry->VlanId, sizeof (tSnoopTag));
            SNOOP_MEM_CPY (&SnoopDelEntry.MacAddr,
                           pSnoopMacGrpFwdEntry->MacGroupAddr,
                           SNOOP_MAC_ADDR_LEN);
            SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

            SNOOP_MEM_SET (pSnoopMacGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                          pSnoopMacGrpFwdEntry,
                                          SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if ((pSnoopInstInfo != NULL) && (pSnoopMacGrpFwdEntry != NULL))
            {
                pSnoopInfo =
                    &(pSnoopInstInfo->
                      SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
                {
                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                }
            }

            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                          (tRBElem *) pSnoopMacGrpFwdEntry);
            SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance, pSnoopMacGrpFwdEntry);

            /* Delete the MAC forward table entries form hardware */
            SnoopMiVlanDelDynamicMcastInfoForVlan (u4Instance, VlanId);
            pRBElem = pRBNextElem;
        }
    }
    else                        /* forwarding mode is IP based */
    {
        pRBElem =
            RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry);

        while (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                         IpMcastFwdEntry, pRBElem, NULL);
            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if (SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)
                != SNOOP_OUTER_VLAN (VlanId))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (u1AddressType)
            {
                if (pSnoopIpGrpFwdEntry->u1AddressType != u1AddressType)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }
            }

            SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);

#ifdef NPAPI_WANTED
            if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
            {
                if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                                pSnoopIpGrpFwdEntry->VlanId,
                                                pSnoopIpGrpFwdEntry->SrcIpAddr,
                                                pSnoopIpGrpFwdEntry->GrpIpAddr,
                                                pSnoopIpGrpFwdEntry->PortBitmap,
                                                SNOOP_DELETE_FWD_ENTRY)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Mcast IP forwarding entry failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_FWD_DBG, SNOOP_FWD_DBG,
                               "Mcast IP forwarding entry failed\r\n");
                }
            }
#endif
            /* Indicate the aging out of IP forwarding  entry */
            SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
            SNOOP_MEM_CPY (&(SnoopDelEntry.VlanId),
                           pSnoopIpGrpFwdEntry->VlanId, sizeof (tSnoopTag));

            SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                           &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                           IPVX_MAX_INET_ADDR_LEN);

            SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                           &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                           IPVX_MAX_INET_ADDR_LEN);
            SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
            SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

            SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                           SNOOP_PORT_LIST_SIZE);
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if (pSnoopInstInfo != NULL)
            {
                pSnoopInfo =
                    &(pSnoopInstInfo->
                      SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
                if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
                {
                    pSnoopInfo->u4FwdGroupsCnt -= 1;
                }
            }
            RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                          (tRBElem *) pSnoopIpGrpFwdEntry);
            SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);

            pRBElem = pRBNextElem;
        }
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateRtrPortToMacFwdTable                   */
/*                                                                           */
/* Description        : This function updates the outgiong portlist in the   */
/*                      mac based forwarding table (addition/removal         */
/*                      of a router port)                                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                      u4Port     - Port Number                             */
/*                      VlanId     - Vlan Identifier                         */
/*                      u1AddRemove- Event ADD/REMOVE router port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdUpdateRtrPortToMacFwdTable (UINT4 u4Instance, UINT4 u4Port,
                                    tSnoopTag VlanId, UINT1 u1AddressType,
                                    tMacAddr McastAddr, UINT1 u1AddRemove)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tMacAddr            NullMacAddr;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1IsVlanPresent = SNOOP_FALSE;

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for McastAddr %s\r\n", McastAddr);
        return SNOOP_FAILURE;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                              MacMcastFwdEntry);

    while (pRBElem != NULL)
    {
        pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                           pRBElem, NULL);

        u1IsVlanPresent =
            (UINT1) SnoopIsVlanPresentInFwdTbl (SNOOP_OUTER_VLAN
                                                (pSnoopMacGrpFwdEntry->VlanId),
                                                SNOOP_OUTER_VLAN (VlanId),
                                                &L2PvlanMappingInfo);

        /* If the multicast mac address is not NULL then delete the port entry
         * for VlanId and Multicast MAC address */

        if (SNOOP_MEM_CMP (McastAddr, NullMacAddr, sizeof (tMacAddr)) != 0)
        {
            if (!((u1IsVlanPresent) && (SNOOP_MEM_CMP (McastAddr,
                                                       pSnoopMacGrpFwdEntry->
                                                       MacGroupAddr,
                                                       sizeof (tMacAddr)) ==
                                        0)))
            {
                pRBElem = pRBNextElem;
                continue;
            }
        }
        else if (!(u1IsVlanPresent))
        {
            pRBElem = pRBNextElem;
            continue;
        }
        else if (pSnoopMacGrpFwdEntry->u1AddressType != u1AddressType)
        {
            pRBElem = pRBNextElem;
            continue;
        }

        /* Add the router port to the multicast forward entries in the VLAN */
        if (u1AddRemove == SNOOP_ADD_PORT)
        {
            SNOOP_ADD_TO_PORT_LIST (u4Port, pSnoopMacGrpFwdEntry->PortBitmap);

            /* Update VLAN multicast egress ports */
            if (SnoopMiVlanUpdateDynamicMcastInfo
                (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                 pSnoopMacGrpFwdEntry->VlanId,
                 u4Port, VLAN_ADD) != SNOOP_SUCCESS)
            {
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_NAME,
                               "Updation to VLAN for router port "
                               "addition for MAC mcast entry failed\r\n");
                return SNOOP_FAILURE;
            }
        }

        /* Remove the router port from the multicast forward entries 
         * in the VLAN */

        if (u1AddRemove == SNOOP_DEL_PORT)
        {
            /* Check if port already present */
            SNOOP_IS_PORT_PRESENT (u4Port,
                                   pSnoopMacGrpFwdEntry->PortBitmap, bResult);

            if (bResult == OSIX_FALSE)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC, "Router port not present in the "
                           "MAC forwarding entry\r\n");
                pRBElem = pRBNextElem;
                continue;
            }

            SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopMacGrpFwdEntry->PortBitmap);

            /* Update VLAN multicast egress ports */
            if (SnoopMiVlanUpdateDynamicMcastInfo
                (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                 pSnoopMacGrpFwdEntry->VlanId, u4Port,
                 VLAN_DELETE) != SNOOP_SUCCESS)
            {
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_NAME,
                               "Updation to VLAN for router port "
                               "deletion for MAC mcast entry failed\r\n");
                return SNOOP_FAILURE;
            }

            /* If the port bitmap becomes NULL delete the forward entry */
            if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->PortBitmap,
                               gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
            {
                /* Delete the list of pointers to the group entry */
                while ((pSnoopGroupEntry = (tSnoopGroupEntry *)
                        SNOOP_SLL_FIRST (&pSnoopMacGrpFwdEntry->GroupEntryList))
                       != NULL)
                {
                    pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                    SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                      pSnoopGroupEntry);
                }

                SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                              pSnoopMacGrpFwdEntry,
                                              SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                if ((pSnoopInstInfo != NULL) && (pSnoopMacGrpFwdEntry != NULL))
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                              MacMcastFwdEntry,
                              (tRBElem *) pSnoopMacGrpFwdEntry);
                SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                pSnoopMacGrpFwdEntry);
            }
        }
        pRBElem = pRBNextElem;
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateRtrPortToIpFwdTable                    */
/*                                                                           */
/* Description        : This function updates the outgiong portlist in the   */
/*                      IP based forwarding table (addition/removal          */
/*                      of a router port)                                    */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Number                         */
/*                      u4Port     - Port Number                             */
/*                      VlanId     - Vlan Identifier                         */
/*                      u1AddRemove- Event ADD/REMOVE router port            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdUpdateRtrPortToIpFwdTable (UINT4 u4Instance, UINT4 u4Port,
                                   tSnoopTag VlanId,
                                   UINT1 u1AddressType, UINT1 u1AddRemove)
{
    UINT1              *pTempPortBitmap = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    tSnoopIpGrpFwdEntry *pIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tIPvXAddr           SrcIpAddr;
    tIPvXAddr           GrpIpAddr;
    UINT1               u1EntryDeleted = SNOOP_FALSE;
    BOOL1               bResult = OSIX_FALSE;
    tSnoopGroupEntry    SnoopGroupEntry;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
#ifdef NPAPI_WANTED
    UINT4               u4TempSrcIpAddr = 0;
    UINT4               u4TempGrpIpAddr = 0;
    CHR1               *pc1SrcString = NULL;
    CHR1               *pc1GrpString = NULL;
    CHR1                au1Ip6Addr[IPVX_IPV6_ADDR_LEN];
#endif
    UNUSED_PARAM (pSnoopGroupEntry);
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pTempPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for port %d\r\n", u4Port);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));
#ifdef PIM_WANTED
    if (SnoopUtilGetPIMInterfaceStatus (VlanId) == SNOOP_SUCCESS)
    {
        if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
            (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
        {
            /* If PIM is enabled and No Forward Entry is present,
               then Get (S,G) from PIM and Pgm NP */
            SNOOP_ADD_TO_PORT_LIST (u4Port, pTempPortBitmap);

            if (SnoopUtilGetForwardingEntryFromPim (u4Instance,
                                                    VlanId,
                                                    pTempPortBitmap)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_NAME,
                               "Addition of RtrPort to H/W was Failed, When PIM is Enabled\r\n");
                UtilPlstReleaseLocalPortList (pTempPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
                return SNOOP_FAILURE;
            }
        }
    }
#endif
    SNOOP_MEM_SET (&SrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&GrpIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for vlan id %d\r\n", SNOOP_OUTER_VLAN (VlanId));
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
        return SNOOP_FAILURE;
    }
    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    pRBElem =
        RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry);

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pFwdPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    while (pRBElem != NULL)
    {
        pIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           pRBElem, NULL);

        if (SnoopIsVlanPresentInFwdTbl
            (SNOOP_OUTER_VLAN (pIpGrpFwdEntry->VlanId),
             SNOOP_OUTER_VLAN (VlanId), &L2PvlanMappingInfo) == SNOOP_FALSE)
        {
            pRBElem = pRBNextElem;
            continue;
        }
        if (pIpGrpFwdEntry->u1AddressType != u1AddressType)
        {
            pRBElem = pRBNextElem;
            continue;
        }

        /* The below check (GrpIpAddr, SrcIpAddr) is added to get the first
         * IP FWD entry for the Source, Group. If the entry is a router port 
         * entry (Inner vlan is 0) then update the portbmp else create a new 
         * router port entry on u1AddRemove is SNOOP_ADD_PORT */
        if ((SNOOP_MEM_CMP (&GrpIpAddr, &(pIpGrpFwdEntry->GrpIpAddr),
                            sizeof (tIPvXAddr)) == 0) &&
            (SNOOP_MEM_CMP (&SrcIpAddr, &(pIpGrpFwdEntry->SrcIpAddr),
                            sizeof (tIPvXAddr)) == 0))
        {
            pRBElem = pRBNextElem;
            continue;
        }

        if (SNOOP_INNER_VLAN (pIpGrpFwdEntry->VlanId) != SNOOP_INVALID_VLAN_ID)
        {
            if (u1AddRemove != SNOOP_ADD_PORT)
            {
                pRBElem = pRBNextElem;
                continue;
            }
            /* Create a new IP Forward Entry for Inner-Vlan 0 
             * and update with this new router port */
            SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
            SNOOP_OUTER_VLAN (SnoopGroupEntry.VlanId)
                = SNOOP_OUTER_VLAN (pIpGrpFwdEntry->VlanId);
            SNOOP_INNER_VLAN (SnoopGroupEntry.VlanId) = SNOOP_INVALID_VLAN_ID;

            IPVX_ADDR_COPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                            (UINT1 *) &(pIpGrpFwdEntry->GrpIpAddr));

            SNOOP_ADD_TO_PORT_LIST (u4Port, pFwdPortBitmap);

            if (SnoopFwdUpdateIpFwdTable (u4Instance, &SnoopGroupEntry,
                                          pIpGrpFwdEntry->SrcIpAddr, 0,
                                          pFwdPortBitmap,
                                          SNOOP_CREATE_FWD_ENTRY)
                != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_FWD_TRC,
                           "Updation of IP forwarding table on router "
                           "port failed\r\n");
            }
        }
        else
        {
            u1EntryDeleted = SNOOP_FALSE;
            /* Add router port to the multicast forwarding entries 
             * in the VLAN */
            if (u1AddRemove == SNOOP_ADD_PORT)
            {
                /* Check if port already present */
                SNOOP_IS_PORT_PRESENT (u4Port,
                                       pIpGrpFwdEntry->PortBitmap, bResult);

                if (bResult == OSIX_TRUE)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Router Port already present in "
                               "the IP forwarding entry\r\n");
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
                    return SNOOP_SUCCESS;
                }

                SNOOP_ADD_TO_PORT_LIST (u4Port, pIpGrpFwdEntry->PortBitmap);
                SNOOP_ADD_TO_PORT_LIST (u4Port, pTempPortBitmap);
            }
            else
            {
                /* Check if port is present */
                SNOOP_IS_PORT_PRESENT (u4Port,
                                       pIpGrpFwdEntry->PortBitmap, bResult);

                if (bResult == OSIX_FALSE)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Router port not present in the "
                               "IP forwarding entry\r\n");
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
                    return SNOOP_SUCCESS;
                }

                SNOOP_DEL_FROM_PORT_LIST (u4Port, pIpGrpFwdEntry->PortBitmap);
                SNOOP_ADD_TO_PORT_LIST (u4Port, pTempPortBitmap);

                if (SNOOP_MEM_CMP (pIpGrpFwdEntry->PortBitmap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* Delete the IP Forward Entry for Inner-Vlan 0 */
                    SNOOP_MEM_SET (&SnoopGroupEntry, 0,
                                   sizeof (tSnoopGroupEntry));
                    SNOOP_MEM_CPY (SnoopGroupEntry.VlanId,
                                   pIpGrpFwdEntry->VlanId, sizeof (tSnoopTag));
                    IPVX_ADDR_COPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                                    (UINT1 *) &(pIpGrpFwdEntry->GrpIpAddr));

#ifdef PIM_WANTED
                    SNOOP_ADD_TO_PORT_LIST (u4Port, pFwdPortBitmap);
#endif /* PIM_WANTED */

                    if (SnoopFwdUpdateIpFwdTable (u4Instance, &SnoopGroupEntry,
                                                  pIpGrpFwdEntry->SrcIpAddr,
                                                  u4Port, pFwdPortBitmap,
                                                  SNOOP_DELETE_FWD_ENTRY) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Updation of IP forwarding table on router "
                                   "port failed\r\n");
                    }
                    u1EntryDeleted = SNOOP_TRUE;
                }
            }
            if (u1EntryDeleted == SNOOP_FALSE)
            {
                /* Update hardware on addition/removal of port to the 
                 * IP multicast entry */
#ifdef NPAPI_WANTED
                if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
                {
                    if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                                    pIpGrpFwdEntry->VlanId,
                                                    pIpGrpFwdEntry->SrcIpAddr,
                                                    pIpGrpFwdEntry->GrpIpAddr,
                                                    pTempPortBitmap,
                                                    u1AddRemove)
                        != SNOOP_SUCCESS)
                    {
                        if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
                        {
                            SNOOP_MEM_CPY (&u4TempSrcIpAddr,
                                           pIpGrpFwdEntry->SrcIpAddr.au1Addr,
                                           IPVX_IPV4_ADDR_LEN);
                            u4TempSrcIpAddr = SNOOP_HTONL (u4TempSrcIpAddr);
                            SNOOP_CONVERT_IPADDR_TO_STR (pc1SrcString,
                                                         u4TempSrcIpAddr);

                            SNOOP_MEM_CPY (&u4TempGrpIpAddr,
                                           pIpGrpFwdEntry->GrpIpAddr.au1Addr,
                                           IPVX_IPV4_ADDR_LEN);
                            u4TempGrpIpAddr = SNOOP_HTONL (u4TempGrpIpAddr);
                            SNOOP_CONVERT_IPADDR_TO_STR (pc1GrpString,
                                                         u4TempGrpIpAddr);
                        }
                        else if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
                        {
                            SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
                            SNOOP_MEM_CPY (au1Ip6Addr,
                                           pIpGrpFwdEntry->SrcIpAddr.au1Addr,
                                           IPVX_IPV6_ADDR_LEN);
                            SNOOP_INET_NTOHL (au1Ip6Addr);
                            pc1SrcString =
                                (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                       au1Ip6Addr);

                            SNOOP_MEM_SET (au1Ip6Addr, 0, IPVX_IPV6_ADDR_LEN);
                            SNOOP_MEM_CPY (au1Ip6Addr,
                                           pIpGrpFwdEntry->GrpIpAddr.au1Addr,
                                           IPVX_IPV6_ADDR_LEN);
                            SNOOP_INET_NTOHL (au1Ip6Addr);
                            pc1GrpString =
                                (CHR1 *) Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                       au1Ip6Addr);
                        }
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                            SNOOP_CONTROL_PATH_TRC |
                                            SNOOP_DBG_ALL_FAILURE,
                                            SNOOP_FWD_NAME,
                                            "Routerport updation in hardware failed"
                                            " for Source %s,Group %s failed\r\n",
                                            pc1SrcString, pc1GrpString);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
                        return SNOOP_FAILURE;
                    }
                    else
                    {
#ifdef L2RED_WANTED
                        if (SnoopGrpGetGroupEntry
                            (u4Instance, VlanId, pIpGrpFwdEntry->GrpIpAddr,
                             &pSnoopGroupEntry) == SNOOP_SUCCESS)
                        {

                            SnoopActiveRedGrpSync (u4Instance,
                                                   pIpGrpFwdEntry->SrcIpAddr,
                                                   pSnoopGroupEntry,
                                                   pIpGrpFwdEntry->PortBitmap);
                        }
#endif
                    }
                    /* If the deleted port is port channel, in h/w the total
                     * entry could have been flushed because of
                     * non availability of trunk membership informations
                     * in h/w. To avoid this reconfigure the entry
                     * with availabe other ports
                     */
                    if ((u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES) &&
                        (u1AddRemove == SNOOP_DEL_PORT))

                    {
                        if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                                        pIpGrpFwdEntry->VlanId,
                                                        pIpGrpFwdEntry->
                                                        SrcIpAddr,
                                                        pIpGrpFwdEntry->
                                                        GrpIpAddr,
                                                        pIpGrpFwdEntry->
                                                        PortBitmap,
                                                        SNOOP_CREATE_FWD_ENTRY)
                            != SNOOP_SUCCESS)
                        {
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                       SNOOP_FWD_TRC,
                                       "Updation of IP forwarding table on router "
                                       "port failed\r\n");
                        }
                        else
                        {

#ifdef L2RED_WANTED
                            if (SnoopGrpGetGroupEntry
                                (u4Instance, VlanId, pIpGrpFwdEntry->GrpIpAddr,
                                 &pSnoopGroupEntry) == SNOOP_SUCCESS)
                            {

                                SnoopActiveRedGrpSync (u4Instance,
                                                       pIpGrpFwdEntry->
                                                       SrcIpAddr,
                                                       pSnoopGroupEntry,
                                                       pIpGrpFwdEntry->
                                                       PortBitmap);
                            }
#endif
                        }
                    }

                }
#endif
            }
        }
        if (L2PvlanMappingInfo.u2NumMappedVlans == 0)
        {
            IPVX_ADDR_COPY ((UINT1 *) &GrpIpAddr,
                            (UINT1 *) &(pIpGrpFwdEntry->GrpIpAddr));
            IPVX_ADDR_COPY ((UINT1 *) &SrcIpAddr,
                            (UINT1 *) &(pIpGrpFwdEntry->SrcIpAddr));
        }
        pRBElem = pRBNextElem;
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    UtilPlstReleaseLocalPortList (pTempPortBitmap);
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdUpdateRtrPortToIpFwdTable\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateFwdBitmap                              */
/*                                                                           */
/* Description        : This function updates the port bitmap forwarding     */
/*                      entries for a instance and VLAN                      */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                      PortBitmap       - port bitmap to be updated         */
/*                      pSnoopVlanEntry  - vlan entry                        */
/*                      GrpAddr          - group address                     */
/*                      u1AddRemove      - Add or remove                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopFwdUpdateFwdBitmap (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                         tSnoopVlanEntry * pSnoopVlanEntry, tIPvXAddr GrpAddr,
                         UINT1 u1AddRemove)
{
    tSnoopIfPortBmp    *pSnpPortList = NULL;
    UINT1              *pBitmapVlan = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pTempPortBitmap = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pTempIpGrpFwdEntry = NULL;
    tIPvXAddr           GrpIpAddr;
    tIPvXAddr           SrcIpAddr;
    tMacAddr            MacAddr;
    tSnoopTag           VlanId;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT1               u1EntryDeleted = SNOOP_FALSE;
    UINT1               u1DelOuterVlanEntry = SNOOP_FALSE;
    UINT1               u1LoopFlag = SNOOP_FALSE;
    tSnoopGroupEntry    SnoopGroupEntry;
#ifdef L2RED_WANTED
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
#endif
    tSnoopFilter        SnoopDelEntry;
    tSnoopMacGrpFwdEntry MacGrpFwdEntry;

    SNOOP_MEM_SET (&MacAddr, 0, sizeof (tMacAddr));
    SNOOP_MEM_SET (&GrpIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (&VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));

    SNOOP_MEM_CPY (&VlanId, pSnoopVlanEntry->VlanId, sizeof (tSnoopTag));

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdUpdateFwdBitmap\r\n");
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pTempPortBitmap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "SnoopFwdUpdateFwdBitmap: "
                       "Error in allocating memory for pTempPortBitmap\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));

    if ((SnoopMiGetVlanLocalEgressPorts (u4Instance, VlanId, pTempPortBitmap))
        != SNOOP_SUCCESS)
    {
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_AND_PORT_BMP (PortBitmap, pTempPortBitmap);

    if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_DISABLE)
    {
        if (SNOOP_MEM_CMP (PortBitmap, gNullPortBitMap, SNOOP_PORT_LIST_SIZE) ==
            0)
        {
            UtilPlstReleaseLocalPortList (pTempPortBitmap);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopFwdUpdateFwdBitmap\r\n");
            return SNOOP_SUCCESS;
        }
    }

    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for vlan id %d\r\n", SNOOP_OUTER_VLAN (VlanId));
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
        return SNOOP_FAILURE;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {

        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                             L2PvlanMappingInfo.pMappedVlans);
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                       "Error in allocating memory for pFwdPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
        return SNOOP_FAILURE;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    /* If the forwarding mode is MAC based forwarding mode */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pBitmapVlan == NULL)
        {
            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                 L2PvlanMappingInfo.
                                                 pMappedVlans);

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                           SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                           "Error in allocating memory for pBitmapVlan\r\n");
            SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                              SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                              SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            UtilPlstReleaseLocalPortList (pTempPortBitmap);
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopFwdUpdateFwdBitmap\r\n");
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));

        SNOOP_MEM_CPY (pBitmapVlan, PortBitmap, SNOOP_PORT_LIST_SIZE);
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            /*SNOOPING will be maintaining the virtual port bitmap corresponding to 
             * the physical port bitmap obtained from the multiple instance manager
             */
            pSnpPortList =
                (tSnoopIfPortBmp *)
                FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
            if (pSnpPortList == NULL)
            {
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                               "Error in allocating memory for pSnpPortList\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pTempPortBitmap);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortBmp));
            if (SnoopGetPhyPortBmp (u4Instance, pBitmapVlan, *pSnpPortList)
                != SNOOP_SUCCESS)
            {
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "Unable to get the physical port bitmap from the virtual"
                               " port bitmap\r\n");
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pTempPortBitmap);
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                           "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                return SNOOP_FAILURE;
            }

            SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                           (pSnoopVlanEntry->VlanId),
                                           (tPortList *) (*pSnpPortList));
            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
        }
        UtilPlstReleaseLocalPortList (pBitmapVlan);

        SNOOP_MEM_SET (&MacGrpFwdEntry, 0, sizeof (tSnoopMacGrpFwdEntry));
        SNOOP_MEM_CPY (&(MacGrpFwdEntry.VlanId), VlanId, sizeof (tSnoopTag));

        if (((SNOOP_MEM_CMP ((UINT1 *) &GrpAddr, (UINT1 *) &gNullAddr,
                             sizeof (tIPvXAddr))) != 0) &&
            (L2PvlanMappingInfo.u2NumMappedVlans == 0))
        {
#ifdef IGS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
            {
                SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                         (GrpAddr.au1Addr), MacAddr);
            }
#endif
#ifdef MLDS_WANTED
            if (GrpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV6)
            {
                SNOOP_GET_MAC_FROM_IPV6 (GrpAddr.au1Addr, MacAddr);
            }
#endif
            MacGrpFwdEntry.u1AddressType = GrpAddr.u1Afi;

            SNOOP_MEM_CPY (&MacGrpFwdEntry.MacGroupAddr, MacAddr,
                           SNOOP_MAC_ADDR_LEN);
            if ((pRBElem =
                 RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                            (tRBElem *) & MacGrpFwdEntry)) == NULL)
            {
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pTempPortBitmap);
                return SNOOP_FAILURE;
            }
            u1LoopFlag = 1;
        }
        else
        {
            pRBElem =
                RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                MacMcastFwdEntry);
            u1LoopFlag = 0;

        }

        while (pRBElem != NULL)
        {
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
            SNOOP_MEM_CPY (pFwdPortBitmap, PortBitmap, SNOOP_PORT_LIST_SIZE);

            pSnoopMacGrpFwdEntry = (tSnoopMacGrpFwdEntry *) pRBElem;

            if ((SnoopIsVlanPresentInFwdTbl
                 (SNOOP_OUTER_VLAN (pSnoopMacGrpFwdEntry->VlanId),
                  SNOOP_OUTER_VLAN (VlanId), &L2PvlanMappingInfo) == SNOOP_TRUE)
                && (SNOOP_INNER_VLAN (pSnoopMacGrpFwdEntry->VlanId) ==
                    SNOOP_INVALID_VLAN_ID))
            {
                if (pSnoopVlanEntry->u1AddressType ==
                    pSnoopMacGrpFwdEntry->u1AddressType)
                {
                    pSnoopRtrPortBmp =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pSnoopRtrPortBmp == NULL)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                                       SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                                       "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                                   SNOOP_CONTROL_TRC,
                                   "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                          SnoopSysErrString
                                          [SYS_LOG_MEM_ALLOC_FAIL]);
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                        return SNOOP_FAILURE;
                    }
                    MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                             SNOOP_OUTER_VLAN
                                                             (pSnoopMacGrpFwdEntry->
                                                              VlanId),
                                                             pSnoopVlanEntry->
                                                             u1AddressType, 0,
                                                             pSnoopRtrPortBmp,
                                                             pSnoopVlanEntry);
                    /* Check if port is present */
                    SNOOP_AND_PORT_BMP (pFwdPortBitmap, pSnoopRtrPortBmp);

                    if (SNOOP_MEM_CMP (pFwdPortBitmap, gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) != 0)
                    {
                        SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp,
                                                pFwdPortBitmap);
                    }
                    UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);

                    SNOOP_UPDATE_PORT_LIST (PortBitmap, pFwdPortBitmap);

                    if (SnoopFwdAddRemovePorts (u4Instance, pFwdPortBitmap,
                                                pSnoopMacGrpFwdEntry, NULL,
                                                u1AddRemove) != SNOOP_SUCCESS)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                        return SNOOP_FAILURE;
                    }
                }
            }

            if (u1LoopFlag)
                break;

            pRBNextElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               MacMcastFwdEntry, pRBElem, NULL);
            pRBElem = pRBNextElem;
        }

    }
    else                        /* forwarding mode is IP based */
    {
#ifdef PIM_WANTED
        if (SnoopUtilGetPIMInterfaceStatus (VlanId) == SNOOP_SUCCESS)
        {
            if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP)
                && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE))
            {
                pBitmapVlan =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pBitmapVlan == NULL)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                                   SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                                   "Error in allocating memory for pBitmapVlan\r\n");
                    SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for group address %s\r\n",
                                           SnoopPrintIPvxAddress (GrpAddr));
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                    return SNOOP_FAILURE;
                }
                SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));
                /* If PIM is enabled and No Forward Entry is present,
                   then Get (S,G) from PIM and Pgm NP */
                if (SnoopUtilGetForwardingEntryFromPim (u4Instance,
                                                        VlanId,
                                                        pBitmapVlan)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);

                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Addition of RtrPort to H/W was Failed, When PIM is Enabled\r\n");
                    UtilPlstReleaseLocalPortList (pBitmapVlan);
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                    return SNOOP_FAILURE;
                }
                UtilPlstReleaseLocalPortList (pBitmapVlan);
            }
        }
#endif
        pRBElem =
            RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry);

        while (pRBElem != NULL)
        {
            pRBNextElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               IpMcastFwdEntry, pRBElem, NULL);

            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if (SNOOP_MEM_CMP ((UINT1 *) &GrpAddr, (UINT1 *) &gNullAddr,
                               sizeof (tIPvXAddr)) != 0)
            {
                if (!((SnoopIsVlanPresentInFwdTbl (SNOOP_OUTER_VLAN
                                                   (pSnoopIpGrpFwdEntry->
                                                    VlanId),
                                                   SNOOP_OUTER_VLAN (VlanId),
                                                   &L2PvlanMappingInfo) ==
                       SNOOP_TRUE)
                      &&
                      (SNOOP_MEM_CMP
                       (pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr, GrpAddr.au1Addr,
                        IPVX_MAX_INET_ADDR_LEN) == 0)))
                {
                    pRBElem = pRBNextElem;
                    continue;
                }
            }
            else if (SnoopIsVlanPresentInFwdTbl (SNOOP_OUTER_VLAN
                                                 (pSnoopIpGrpFwdEntry->VlanId),
                                                 SNOOP_OUTER_VLAN (VlanId),
                                                 &L2PvlanMappingInfo) ==
                     SNOOP_FALSE)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if ((SNOOP_MEM_CMP (&GrpIpAddr, &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                                sizeof (tIPvXAddr)) == 0) &&
                (SNOOP_MEM_CMP (&SrcIpAddr, &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                                sizeof (tIPvXAddr)) == 0))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (pSnoopVlanEntry->u1AddressType !=
                pSnoopIpGrpFwdEntry->u1AddressType)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (SNOOP_INNER_VLAN (pSnoopIpGrpFwdEntry->VlanId) !=
                SNOOP_INVALID_VLAN_ID)
            {
                if (u1AddRemove != SNOOP_ADD_PORT)
                {
                    pRBElem = pRBNextElem;
                    continue;
                }
                /* Create a new IP Forward Entry for Inner-Vlan 0
                 * and update with this new router port */
                SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
                SNOOP_MEM_CPY (&SnoopGroupEntry.VlanId,
                               VlanId, sizeof (tSnoopTag));

                IPVX_ADDR_COPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                                (UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr));

                if (SnoopFwdUpdateIpFwdTable (u4Instance, &SnoopGroupEntry,
                                              pSnoopIpGrpFwdEntry->SrcIpAddr, 0,
                                              PortBitmap,
                                              SNOOP_CREATE_FWD_ENTRY)
                    != SNOOP_SUCCESS)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_FWD_TRC,
                               "Updation of IP forwarding table on router "
                               "port failed\r\n");
                }
            }
            else
            {
                u1EntryDeleted = SNOOP_FALSE;
                /* Add router port to the multicast forwarding entries 
                 * in the VLAN */
                if (u1AddRemove == SNOOP_ADD_PORT)
                {
                    /* Check if ports already present */
                    SNOOP_MEM_CPY (pTempPortBitmap,
                                   pSnoopIpGrpFwdEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_UPDATE_PORT_LIST (PortBitmap, pTempPortBitmap);

                    if (SNOOP_MEM_CMP (pTempPortBitmap,
                                       pSnoopIpGrpFwdEntry->PortBitmap,
                                       SNOOP_PORT_LIST_SIZE) == 0)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Router Port(s) already "
                                   "present in the IP forwarding entry\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                        return SNOOP_SUCCESS;
                    }

                    SNOOP_MEM_CPY (pFwdPortBitmap,
                                   pSnoopIpGrpFwdEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_UPDATE_PORT_LIST (PortBitmap,
                                            pSnoopIpGrpFwdEntry->PortBitmap);
                    SNOOP_NEG_PORT_BMP (pFwdPortBitmap);
                    SNOOP_AND_PORT_BMP (pFwdPortBitmap, PortBitmap);
                }
                else
                {
                    /* Check if port is present */
                    SNOOP_MEM_CPY (pFwdPortBitmap,
                                   pSnoopIpGrpFwdEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_AND_PORT_BMP (pFwdPortBitmap, PortBitmap);

                    if (SNOOP_MEM_CMP (pFwdPortBitmap, gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) == 0)
                    {
                        SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                             L2PvlanMappingInfo.
                                                             pMappedVlans);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_FWD_TRC,
                                   "Router port(s) not present "
                                   "in the IP forwarding entry\r\n");
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        UtilPlstReleaseLocalPortList (pTempPortBitmap);
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                   SNOOP_EXIT_DBG,
                                   "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                        return SNOOP_SUCCESS;
                    }

                    SNOOP_XOR_PORT_BMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                        pFwdPortBitmap);
                    if (SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                       gNullPortBitMap,
                                       SNOOP_PORT_LIST_SIZE) == 0)
                    {

                        /* Delete the IP Forward Entry for Inner-Vlan 0
                           only if no [S,*] forwarding entries are present */

                        pTempIpGrpFwdEntry =
                            (tSnoopIpGrpFwdEntry *) pRBNextElem;

                        if ((pTempIpGrpFwdEntry != NULL) &&
                            (SNOOP_MEM_CMP ((UINT1 *) &pSnoopIpGrpFwdEntry->
                                            SrcIpAddr,
                                            (UINT1 *) &(pTempIpGrpFwdEntry->
                                                        SrcIpAddr),
                                            sizeof (tIPvXAddr)) == 0) &&
                            (SNOOP_MEM_CMP ((UINT1 *) &pSnoopIpGrpFwdEntry->
                                            GrpIpAddr,
                                            (UINT1 *) &(pTempIpGrpFwdEntry->
                                                        GrpIpAddr),
                                            sizeof (tIPvXAddr)) == 0) &&
                            (SNOOP_OUTER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
                             SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)) &&
                            (SNOOP_INNER_VLAN (pTempIpGrpFwdEntry->VlanId) !=
                             SNOOP_INVALID_VLAN_ID) &&
                            (SNOOP_MEM_CMP (pTempIpGrpFwdEntry->PortBitmap,
                                            gNullPortBitMap,
                                            SNOOP_PORT_LIST_SIZE) != 0))
                        {
                            /* There is some more entry present for this
                             * Outer-VLAN so no need to delete the entry */
                            /* Delete the router ports alone in NP and finally
                               delete the IP forwarding entry */

                            u1DelOuterVlanEntry = SNOOP_TRUE;
                        }
                        else
                        {

                            SNOOP_MEM_SET (&SnoopGroupEntry, 0,
                                           sizeof (tSnoopGroupEntry));
                            SNOOP_MEM_CPY (SnoopGroupEntry.VlanId,
                                           pSnoopIpGrpFwdEntry->VlanId,
                                           sizeof (tSnoopTag));
                            IPVX_ADDR_COPY
                                ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                                 (UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr));

                            if (SnoopFwdUpdateIpFwdTable
                                (u4Instance, &SnoopGroupEntry,
                                 pSnoopIpGrpFwdEntry->SrcIpAddr, 0,
                                 pFwdPortBitmap,
                                 SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                            {
                                SNOOP_DBG (SNOOP_DBG_FLAG,
                                           SNOOP_CONTROL_PATH_TRC,
                                           SNOOP_FWD_TRC,
                                           "Updation of IP forwarding table on router "
                                           "port failed\r\n");
                            }
                            u1EntryDeleted = SNOOP_TRUE;
                        }
                    }
                }
                if (u1EntryDeleted == SNOOP_FALSE)
                {
                    /* Update hardware on addition/removal of port to the 
                     * IP multicast entry */
#ifdef NPAPI_WANTED
                    if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
                    {
                        if (SnoopNpUpdateMcastFwdEntry
                            (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                             pSnoopIpGrpFwdEntry->SrcIpAddr,
                             pSnoopIpGrpFwdEntry->GrpIpAddr,
                             pFwdPortBitmap, u1AddRemove) != SNOOP_SUCCESS)
                        {
                            SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                                 L2PvlanMappingInfo.
                                                                 pMappedVlans);
                            SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST
                                                (u4Instance),
                                                SNOOP_CONTROL_PATH_TRC |
                                                SNOOP_DBG_ALL_FAILURE,
                                                SNOOP_FWD_NAME,
                                                "Routerport updation in hardware failed"
                                                " for Source %s,Group %s failed\r\n",
                                                SnoopPrintIPvxAddress
                                                (pSnoopIpGrpFwdEntry->
                                                 SrcIpAddr),
                                                SnoopPrintIPvxAddress
                                                (pSnoopIpGrpFwdEntry->
                                                 GrpIpAddr));
                            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                            UtilPlstReleaseLocalPortList (pTempPortBitmap);
                            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG,
                                       SNOOP_EXIT_DBG,
                                       "Exit : SnoopFwdUpdateFwdBitmap\r\n");
                            return SNOOP_FAILURE;
                        }
                    }
#endif
#ifdef L2RED_WANTED
                    if (SnoopGrpGetGroupEntry
                        (u4Instance, VlanId, pSnoopIpGrpFwdEntry->GrpIpAddr,
                         &pSnoopGroupEntry) == SNOOP_SUCCESS)
                    {
                        SnoopActiveRedGrpSync (u4Instance,
                                               pSnoopIpGrpFwdEntry->SrcIpAddr,
                                               pSnoopGroupEntry,
                                               pSnoopIpGrpFwdEntry->PortBitmap);
                    }
#endif
                }
            }
            IPVX_ADDR_COPY ((UINT1 *) &GrpIpAddr,
                            (UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr));
            IPVX_ADDR_COPY ((UINT1 *) &SrcIpAddr,
                            (UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr));

            if ((u1DelOuterVlanEntry == SNOOP_TRUE) &&
                (u1EntryDeleted == SNOOP_FALSE))
            {
                if (pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType !=
                    SNOOP_INVALID_TIMER_TYPE)
                {
                    SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
                }

                /* Indicate the aging out of IP forwarding entry */
                SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
                SNOOP_MEM_CPY (SnoopDelEntry.VlanId,
                               pSnoopIpGrpFwdEntry->VlanId, sizeof (tSnoopTag));
                SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                               &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                               &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                               IPVX_MAX_INET_ADDR_LEN);

                SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
                SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

                /* Indicate the aging out of IP forwarding entry */

                SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);
                SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                               SNOOP_PORT_LIST_SIZE);
                RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                              (tRBElem *) pSnoopIpGrpFwdEntry);
                pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

                if (pSnoopInstInfo != NULL)
                {
                    pSnoopInfo =
                        &(pSnoopInstInfo->
                          SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
                    if ((pSnoopInfo != NULL)
                        && (pSnoopInfo->u4FwdGroupsCnt != 0))
                    {
                        pSnoopInfo->u4FwdGroupsCnt -= 1;
                    }
                }

                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);

                pSnoopIpGrpFwdEntry = NULL;
                u1DelOuterVlanEntry = SNOOP_FALSE;
            }

            pRBElem = pRBNextElem;
        }
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    UtilPlstReleaseLocalPortList (pTempPortBitmap);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdUpdateFwdBitmap\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdAddRemovePorts                               */
/*                                                                           */
/* Description        : This function adds or removes the ports to multicast */
/*                      forwarding  entries for a instance and VLAN          */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Number                   */
/*                    : PortBitmap       - Port Bitmap                       */
/*                    : pSnoopMacGrpFwdEntry - pointer to MAC forward entry  */
/*                    : pSnoopIpGrpFwdEntry - pointer to IP forward entry    */
/*                    : u1AddRemove      - Add or remove                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdAddRemovePorts (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                        tSnoopMacGrpFwdEntry * pSnoopMacGrpFwdEntry,
                        tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry,
                        UINT1 u1AddRemove)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    UINT4               u4Port = 0;
    BOOL1               bResult = OSIX_FALSE;
    BOOL1               bResultGrpEntry = OSIX_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopFwdAddRemovePorts\r\n");
    if (SNOOP_MCAST_FWD_MODE (u4Instance) != SNOOP_MCAST_FWD_MODE_MAC)
    {
        SNOOP_CHK_NULL_PTR_RET (pSnoopIpGrpFwdEntry, SNOOP_FAILURE);
    }

    /* If the forwarding mode is MAC based forwarding mode */
    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Port++)
    {
        SNOOP_IS_PORT_PRESENT (u4Port, PortBitmap, bResult);

        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        if (u1AddRemove == SNOOP_ADD_PORT)
        {
            /* Add port to the forwarding table based upon the mode 
             * IP / MAC */
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                if (SnoopMiIsVlanMemberPort
                    (u4Instance, pSnoopMacGrpFwdEntry->VlanId,
                     SNOOP_GET_IFINDEX (u4Instance, u4Port)) == SNOOP_FALSE)
                {
                    continue;
                }

                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopMacGrpFwdEntry->PortBitmap);

                /* Update VLAN multicast egress ports */
                if (SnoopMiVlanUpdateDynamicMcastInfo
                    (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                     pSnoopMacGrpFwdEntry->VlanId, u4Port,
                     VLAN_ADD) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Updation to VLAN for port "
                                   "deletion for MAC mcast entry failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdAddRemovePorts\r\n");
                    return SNOOP_FAILURE;
                }
            }
            else
            {
                if (SnoopMiIsVlanMemberPort
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     SNOOP_GET_IFINDEX (u4Instance, u4Port)) == SNOOP_FALSE)
                {
                    continue;
                }
                SNOOP_ADD_TO_PORT_LIST (u4Port,
                                        pSnoopIpGrpFwdEntry->PortBitmap);

#ifdef NPAPI_WANTED
                if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                                pSnoopIpGrpFwdEntry->VlanId,
                                                pSnoopIpGrpFwdEntry->SrcIpAddr,
                                                pSnoopIpGrpFwdEntry->GrpIpAddr,
                                                pSnoopIpGrpFwdEntry->PortBitmap,
                                                SNOOP_ADD_PORT) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Addition of port for IP "
                                   "forwarding entry to hardware failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdAddRemovePorts\r\n");
                    return SNOOP_FAILURE;
                }
#endif
            }
        }
        else
        {
            if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
            {
                SNOOP_IS_PORT_PRESENT (u4Port, pSnoopMacGrpFwdEntry->PortBitmap,
                                       bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }

                SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                pSnoopGroupEntry, tSnoopGroupEntry *)
                {
                    SNOOP_IS_PORT_PRESENT (u4Port, pSnoopGroupEntry->PortBitmap,
                                           bResultGrpEntry);
                }

                if (bResultGrpEntry == OSIX_TRUE)
                {
                    continue;
                }

                SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                          pSnoopMacGrpFwdEntry->PortBitmap);

                /* Update VLAN multicast egress ports */
                if (SnoopMiVlanUpdateDynamicMcastInfo
                    (u4Instance, pSnoopMacGrpFwdEntry->MacGroupAddr,
                     pSnoopMacGrpFwdEntry->VlanId, u4Port,
                     VLAN_DELETE) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_FWD_NAME,
                                   "Updation to VLAN for port "
                                   "deletion for MAC mcast entry failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdAddRemovePorts\r\n");
                    return SNOOP_FAILURE;
                }
                if (SNOOP_MEM_CMP (pSnoopMacGrpFwdEntry->PortBitmap,
                                   gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
                {
                    /* Delete the list of pointers to the group entry */
                    while ((pSnoopGroupEntry =
                            (tSnoopGroupEntry *) SNOOP_SLL_FIRST
                            (&pSnoopMacGrpFwdEntry->GroupEntryList)) != NULL)
                    {
                        pSnoopGroupEntry->pMacGrpFwdEntry = NULL;
                        SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                                          pSnoopGroupEntry);
                    }

                    SnoopRedSyncBulkUpdNextEntry (u4Instance,
                                                  pSnoopMacGrpFwdEntry,
                                                  SNOOP_RED_BULK_UPD_MAC_FWD_REMOVE);
                    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
                    if (pSnoopInstInfo != NULL)
                    {
                        pSnoopInfo =
                            &(pSnoopInstInfo->
                              SnoopInfo[pSnoopMacGrpFwdEntry->u1AddressType -
                                        1]);
                        if ((pSnoopInfo != NULL)
                            && (pSnoopInfo->u4FwdGroupsCnt != 0))
                        {
                            pSnoopInfo->u4FwdGroupsCnt -= 1;
                        }
                    }
                    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->
                                  MacMcastFwdEntry,
                                  (tRBElem *) pSnoopMacGrpFwdEntry);
                    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance,
                                                    pSnoopMacGrpFwdEntry);
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdAddRemovePorts\r\n");
                    return SNOOP_SUCCESS;
                }
            }
            else
            {
                SNOOP_IS_PORT_PRESENT (u4Port, pSnoopIpGrpFwdEntry->PortBitmap,
                                       bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }

                SNOOP_DEL_FROM_PORT_LIST (u4Port,
                                          pSnoopIpGrpFwdEntry->PortBitmap);

#ifdef NPAPI_WANTED
                if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                                pSnoopIpGrpFwdEntry->VlanId,
                                                pSnoopIpGrpFwdEntry->SrcIpAddr,
                                                pSnoopIpGrpFwdEntry->GrpIpAddr,
                                                pSnoopIpGrpFwdEntry->PortBitmap,
                                                SNOOP_DEL_PORT) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_OS_RES_NAME,
                                   "Deletion of port for IP "
                                   "forwarding entry to hardware failed\r\n");
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                               "Exit : SnoopFwdAddRemovePorts\r\n");
                    return SNOOP_FAILURE;
                }
#endif
            }
        }
    }
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopFwdAddRemovePorts\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdSummarizeIpFwdTable                          */
/*                                                                           */
/* Description        : This function summarizes the IP based forwarding     */
/*                      table by creating/deleting/updating an forwarding    */
/*                      entry                                                */
/*                                                                           */
/* Input(s)           : u4Instance  - Instance number                        */
/*                      pSnoopGroupEntry   - pointer to group entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopFwdSummarizeIpFwdTable (UINT4 u4Instance,
                             tSnoopGroupEntry * pSnoopGroupEntry,
                             UINT1 u1UpdateMrp)
{
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    INT4                i4VlanCnt = 0;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1Delete = SNOOP_FALSE;
    UINT1               u1AddressType = 0;
    BOOL1               bResult = SNOOP_FALSE;
    UINT1               u1EntryMatched = SNOOP_FALSE;
    tSnoopGroupEntry    SnoopTmpGroupEntry;
    UINT1              *pFwdPortBitmap = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1              *pSnpGrpConsPortBmp = NULL;
    tSnoopPortBmp       PortBitmap;

#ifdef NPAPI_WANTED
    UINT1               u1EntryFound = SNOOP_FALSE;
#endif
    SNOOP_VALIDATE_INSTANCE (u4Instance);

    MEMSET (&PortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));

    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList))
    {
        u1ASMHostPresent = SNOOP_TRUE;
    }

    u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

    /* Get the pVlan Mapping information */
    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for Snoop PVLAN LIST failed");
        return;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    pRBElem =
        RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           pRBElem, NULL);

        pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

        if (L2PvlanMappingInfo.u2NumMappedVlans == 0)
        {
            SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0, sizeof (tSnoopGroupEntry));

            SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                           sizeof (tSnoopGroupEntry));
            if ((SNOOP_MEM_CMP (pSnoopGroupEntry->VlanId,
                                pSnoopIpGrpFwdEntry->VlanId,
                                sizeof (tSnoopTag)) == 0) &&
                (pSnoopIpGrpFwdEntry->u1AddressType == u1AddressType) &&
                (SNOOP_MEM_CMP (pSnoopGroupEntry->GroupIpAddr.au1Addr,
                                pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr,
                                IPVX_MAX_INET_ADDR_LEN) == 0))
            {
                u1EntryMatched = SNOOP_TRUE;
            }
        }
        else
        {
            /* scanning is done for the associated vlans in the PVLAN domain and
             * also for the incoming vlan identifier */
            for (i4VlanCnt = SNOOP_ZERO;
                 i4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans; i4VlanCnt++)
            {
                SNOOP_MEM_SET (&SnoopTmpGroupEntry, 0,
                               sizeof (tSnoopGroupEntry));

                SNOOP_MEM_CPY (&SnoopTmpGroupEntry, pSnoopGroupEntry,
                               sizeof (tSnoopGroupEntry));
                if (i4VlanCnt != SNOOP_ZERO)
                {
                    SNOOP_OUTER_VLAN (SnoopTmpGroupEntry.VlanId)
                        = L2PvlanMappingInfo.pMappedVlans[i4VlanCnt - 1];
                    SNOOP_INNER_VLAN (SnoopTmpGroupEntry.VlanId)
                        = SNOOP_INVALID_VLAN_ID;
                }
                if ((SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)) ==
                    (SNOOP_OUTER_VLAN (SnoopTmpGroupEntry.VlanId)) &&
                    (pSnoopIpGrpFwdEntry->u1AddressType == u1AddressType) &&
                    (SNOOP_MEM_CMP (pSnoopGroupEntry->GroupIpAddr.au1Addr,
                                    pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr,
                                    IPVX_MAX_INET_ADDR_LEN) == 0))
                {
                    u1EntryMatched = SNOOP_TRUE;
                    break;
                }
            }
        }

        if (u1EntryMatched == SNOOP_TRUE)
        {
#ifdef NPAPI_WANTED
            u1EntryFound = SNOOP_TRUE;
#endif
            u1EntryMatched = SNOOP_FALSE;

            if (SnoopVlanGetVlanEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                       u1AddressType, &pSnoopVlanEntry)
                != SNOOP_SUCCESS)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            /* If entry is found get the source index for the source 
             * IP address */
            SNOOP_IS_SOURCE_PRESENT (u4Instance, pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     bResult, u2SrcIndex);

            if (u2SrcIndex != 0xffff)
            {
                u2SrcIndex++;
            }

            if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
                /* If the previously present source got reset, 
                 * Delete the (S,G) entry */
                if (u2SrcIndex != 0xffff)
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->InclSrcBmp,
                                             u2SrcIndex, SNOOP_SRC_LIST_SIZE,
                                             bResult);
                    if (bResult == SNOOP_FALSE)
                    {
                        if (SnoopFwdUpdateIpFwdTable
                            (u4Instance, &SnoopTmpGroupEntry,
                             pSnoopIpGrpFwdEntry->SrcIpAddr, 0,
                             pSnoopIpGrpFwdEntry->PortBitmap,
                             SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                        {
                            pRBElem = pRBNextElem;
                            continue;
                        }
                        u1Delete = SNOOP_TRUE;
#ifdef L2RED_WANTED
                        SnoopActiveRedGrpSync (u4Instance,
                                               pSnoopIpGrpFwdEntry->SrcIpAddr,
                                               pSnoopGroupEntry,
                                               gNullPortBitMap);

#endif
                    }
                }
                else
                {
                    /* If the source is not present and filter mode is INCLUDE 
                     * directly delete the (S,G) entry */
                    if (SnoopFwdUpdateIpFwdTable
                        (u4Instance, &SnoopTmpGroupEntry,
                         pSnoopIpGrpFwdEntry->SrcIpAddr,
                         0, pSnoopIpGrpFwdEntry->PortBitmap,
                         SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                    {
                        pRBElem = pRBNextElem;
                        continue;
                    }
#ifdef L2RED_WANTED
                    SnoopActiveRedGrpSync (u4Instance,
                                           pSnoopIpGrpFwdEntry->SrcIpAddr,
                                           pSnoopGroupEntry, gNullPortBitMap);

#endif
                    u1Delete = SNOOP_TRUE;
                }
            }
            else
            {
                /* Filter mode is exclude */
                if (u2SrcIndex != 0xffff)
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->ExclSrcBmp,
                                             u2SrcIndex, SNOOP_SRC_LIST_SIZE,
                                             bResult);
                    /* If the bit is set in exclude bitmap and no ASM receivers 
                     * are present delete the entry */
                    if ((bResult == OSIX_TRUE)
                        && (u1ASMHostPresent == SNOOP_FALSE))
                    {
                        if (SnoopFwdUpdateIpFwdTable
                            (u4Instance, &SnoopTmpGroupEntry,
                             pSnoopIpGrpFwdEntry->SrcIpAddr, 0,
                             pSnoopIpGrpFwdEntry->PortBitmap,
                             SNOOP_DELETE_FWD_ENTRY) != SNOOP_SUCCESS)
                        {
                            pRBElem = pRBNextElem;
                            continue;
                        }
                        u1Delete = SNOOP_TRUE;
#ifdef L2RED_WANTED
                        SnoopActiveRedGrpSync (u4Instance,
                                               pSnoopIpGrpFwdEntry->SrcIpAddr,
                                               pSnoopGroupEntry,
                                               gNullPortBitMap);

#endif
                    }
                }
            }

            if (u1Delete == SNOOP_TRUE)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            /* Update the (S,G) entry */
            /* Get the list of ports on which receivers are attached for this
             * (S,G) entry */
            pFwdPortBitmap =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pFwdPortBitmap == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG,
                           SNOOP_GRP_DBG,
                           "Error in allocating memory for pFwdPortBitmap\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Error in allocating memory for pFwdPortBitmap\r\n");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       " pFwdPortBitmap");
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                return;
            }
            MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

            SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                      u1ASMHostPresent, pFwdPortBitmap);
            pSnpGrpConsPortBmp =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pSnpGrpConsPortBmp == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG,
                           SNOOP_GRP_DBG,
                           "Error in allocating memory for pSnpGrpConsPortBmp\r\n");
                SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_TRC,
                           SNOOP_CONTROL_TRC,
                           "Error in allocating memory for pSnpGrpConsPortBmp\r\n");
                SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for pSnpGrpConsPortBmp");
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                     L2PvlanMappingInfo.
                                                     pMappedVlans);
                return;
            }
            MEMSET (pSnpGrpConsPortBmp, 0, sizeof (tSnoopPortBmp));
            SnoopUtilGetPortBmpFromPVlanMapInfo (u4Instance,
                                                 SNOOP_OUTER_VLAN
                                                 (SnoopTmpGroupEntry.VlanId),
                                                 pSnoopGroupEntry->GroupIpAddr,
                                                 pSnoopIpGrpFwdEntry->SrcIpAddr,
                                                 pSnpGrpConsPortBmp);

            SNOOP_UPDATE_PORT_LIST (pSnpGrpConsPortBmp, pFwdPortBitmap);
            UtilPlstReleaseLocalPortList (pSnpGrpConsPortBmp);

            if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) ==
                SNOOP_INVALID_VLAN_ID)
            {
                ;
                pSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pSnoopRtrPortBmp == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_GRP_DBG,
                               SNOOP_GRP_DBG,
                               "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for pSnoopRtrPortBmp");
                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                                         L2PvlanMappingInfo.
                                                         pMappedVlans);
                    return;
                }
                MEMSET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));

                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (SnoopTmpGroupEntry.
                                                          VlanId),
                                                         pSnoopGroupEntry->
                                                         GroupIpAddr.u1Afi, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                SNOOP_UPDATE_PORT_LIST (pSnoopRtrPortBmp, pFwdPortBitmap);
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            }

            if (SnoopFwdUpdateIpFwdTable (u4Instance, &SnoopTmpGroupEntry,
                                          pSnoopIpGrpFwdEntry->SrcIpAddr,
                                          0, pFwdPortBitmap,
                                          SNOOP_CREATE_FWD_ENTRY)
                != SNOOP_SUCCESS)
            {
                pRBElem = pRBNextElem;
                UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                continue;
            }
            MEMCPY (&PortBitmap, pFwdPortBitmap, sizeof (tSnoopPortBmp));
#ifdef L2RED_WANTED
            SnoopActiveRedGrpSync (u4Instance,
                                   pSnoopIpGrpFwdEntry->SrcIpAddr,
                                   pSnoopGroupEntry, PortBitmap);

#endif
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        }
        pRBElem = pRBNextElem;
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    /* If there is no forwarding entry present in SNOOP module for this group 
     * record indicate MRP (PIM/DVMRP/IGMP Proxy) about the updation for this
     * group entry so that if any forwarding entry is created for this group 
     * record by MRP will get updated for the port bitmap updation*/
#ifdef NPAPI_WANTED
    if ((u1EntryFound == SNOOP_FALSE) && (u1UpdateMrp == SNOOP_TRUE))
    {
        SnoopNpIndicateMRP (pSnoopGroupEntry->GroupIpAddr,
                            pSnoopGroupEntry->VlanId);
    }
#else
    UNUSED_PARAM (u1UpdateMrp);
#endif
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdIpFwdEntryDelPort                            */
/*                                                                           */
/* Description        : This function deletes the specified port in the      */
/*                      IP FWD entry.                                        */
/*                                                                           */
/* Input(s)           : u4Instance         - Instance Identifier             */
/*                      pSnoopIpGrpFwdEntry- pointer to IP FWD entry         */
/*                      u4Port             - Port to be deleted              */
/*                      i4MRPPresent       - Multicat Routing Protocol Flag  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopFwdIpFwdEntryDelPort (UINT4 u4Instance,
                           tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry,
                           UINT4 u4Port, INT4 i4MRPPresent,
                           UINT1 *pu1TableDelete)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    UINT1               u1EntryDeleted = SNOOP_FALSE;
#ifdef NPAPI_WANTED
    UINT1              *pDelPortBitmap = NULL;

#else
    UNUSED_PARAM (i4MRPPresent);
#endif
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    SNOOP_DEL_FROM_PORT_LIST (u4Port, pSnoopIpGrpFwdEntry->PortBitmap);

#ifdef NPAPI_WANTED
    if (i4MRPPresent == SNOOP_FALSE)
    {
        pDelPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pDelPortBitmap == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_GRP | SNOOP_TRC_CONTROL |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_NAME,
                           "Error in allocating memory for pDelPortBitmap\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for port %d\r\n", u4Port);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pDelPortBitmap, 0, sizeof (tSnoopPortBmp));

        SNOOP_ADD_TO_PORT_LIST (u4Port, pDelPortBitmap);

        if (SnoopNpUpdateMcastFwdEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                        pSnoopIpGrpFwdEntry->SrcIpAddr,
                                        pSnoopIpGrpFwdEntry->GrpIpAddr,
                                        pDelPortBitmap, SNOOP_DEL_PORT)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "Deletion of port for IP forwarding "
                           "entry to hardware failed\r\n");
            UtilPlstReleaseLocalPortList (pDelPortBitmap);
            return SNOOP_FAILURE;
        }
        UtilPlstReleaseLocalPortList (pDelPortBitmap);
    }

    /* If the deleted port is port channel, in h/w the total
     * entry could have been flushed because of
     * non availability of trunk membership informations
     * in h/w. To avoid this reconfigure the entry
     * with availabe other ports
     */
    if (u4Port > SYS_DEF_MAX_PHYSICAL_INTERFACES)
    {
        if (SnoopNpUpdateMcastFwdEntry (u4Instance,
                                        pSnoopIpGrpFwdEntry->VlanId,
                                        pSnoopIpGrpFwdEntry->SrcIpAddr,
                                        pSnoopIpGrpFwdEntry->GrpIpAddr,
                                        pSnoopIpGrpFwdEntry->PortBitmap,
                                        SNOOP_CREATE_FWD_ENTRY)
            != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_FWD_TRC,
                       "Updation of IP forwarding table on router "
                       "port failed\r\n");
        }
    }

#endif
    /* If the port bitmap becomes NULL delete the forward entry */
    if (SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                       gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0)
    {
        if (SnoopVlanGetVlanEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                   pSnoopIpGrpFwdEntry->GrpIpAddr.u1Afi,
                                   &pSnoopVlanEntry) == SNOOP_SUCCESS)
        {
#ifdef PIM_WANTED
            if (SnoopUtilGetPIMInterfaceStatus (pSnoopIpGrpFwdEntry->VlanId)
                != SNOOP_SUCCESS)
            {
#endif
                /* Dont delete the Grp Fwd Entry, if static /DYN router port is present */
                if (((pSnoopVlanEntry != NULL) &&
                     SNOOP_MEM_CMP (gNullPortBitMap,
                                    pSnoopVlanEntry->RtrPortBitmap,
                                    SNOOP_PORT_LIST_SIZE) == 0))
                {
                    if (SnoopDeleteIpFwdTable (u4Instance, pSnoopIpGrpFwdEntry)
                        != SNOOP_SUCCESS)
                    {
                        return SNOOP_FAILURE;
                    }
                    *pu1TableDelete = SNOOP_TRUE;
                    u1EntryDeleted = SNOOP_TRUE;
                }

                /* If Enhance mode enabled delete the forwarding database,
                   because Router ports will be in separate forrwarding database.
                 */

                if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE) &&
                    (u1EntryDeleted == SNOOP_FALSE))
                {
                    if (SnoopDeleteIpFwdTable (u4Instance, pSnoopIpGrpFwdEntry)
                        != SNOOP_SUCCESS)
                    {
                        return SNOOP_FAILURE;
                    }
                }

#ifdef PIM_WANTED
            }
#endif
        }
    }
    else
    {
        /* Check if there is only router ports are present in the bitmap 
         * then delete the group record from the IP forwarding database */
        if (SnoopVlanGetVlanEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                   pSnoopIpGrpFwdEntry->GrpIpAddr.u1Afi,
                                   &pSnoopVlanEntry) == SNOOP_SUCCESS)
        {
            pSnoopRtrPortBmp =
                UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pSnoopRtrPortBmp == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_GRP | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_TRC_CONTROL, SNOOP_GRP_NAME,
                               "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));
            SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                     SNOOP_OUTER_VLAN
                                                     (pSnoopIpGrpFwdEntry->
                                                      VlanId),
                                                     pSnoopIpGrpFwdEntry->
                                                     GrpIpAddr.u1Afi, 0,
                                                     pSnoopRtrPortBmp,
                                                     pSnoopVlanEntry);
#ifdef PIM_WANTED
            if (SnoopUtilGetPIMInterfaceStatus (pSnoopIpGrpFwdEntry->VlanId)
                != SNOOP_SUCCESS)
            {
#endif
                /* Dont delete the Grp Fwd Entry, if static /DYN router port is present */
                if ((pSnoopVlanEntry != NULL) &&
                    (SNOOP_MEM_CMP
                     (pSnoopIpGrpFwdEntry->PortBitmap, pSnoopRtrPortBmp,
                      SNOOP_PORT_LIST_SIZE) == 0) &&
                    (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_DISABLE))
                {
                    if (SnoopDeleteIpFwdTable (u4Instance, pSnoopIpGrpFwdEntry)
                        != SNOOP_SUCCESS)
                    {
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                    *pu1TableDelete = SNOOP_TRUE;
                }
                else if ((pSnoopIpGrpFwdEntry->u1AddressType ==
                          IPVX_ADDR_FMLY_IPV6)
                         &&
                         (SNOOP_MEM_CMP
                          (pSnoopIpGrpFwdEntry->PortBitmap, pSnoopRtrPortBmp,
                           SNOOP_PORT_LIST_SIZE) == 0))
                {
                    if (SnoopDeleteIpFwdTable (u4Instance, pSnoopIpGrpFwdEntry)
                        != SNOOP_SUCCESS)
                    {
                        UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                        return SNOOP_FAILURE;
                    }
                    *pu1TableDelete = SNOOP_TRUE;
                }
#ifdef PIM_WANTED
            }
#endif
            UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateIpFwdRtrEntryOnDelPort                 */
/*                                                                           */
/* Description        : This function updates the IP FWD entry created for   */
/*                      Router ports after a port was deleted from the       */
/*                      IP FWD entry.                                        */
/*                      The IP FWD entry created for router ports will get   */
/*                      deleted when there is no more Inner-VLAN entry       */
/*                      present for this Outer-VLAN, SourceIP, Group IP entry*/
/*                      or only the router ports are there in the Inner-VLAN */
/*                      0 entry.                                             */
/*                                                                           */
/* Input(s)           : u4Instance         - Instance Identifier             */
/*                      pSnoopGroupEntry   - pointer to group entry          */
/*                      pIpGrpFwdEntry     - pointer to the next Ip FWD entry*/
/*                      SrcIpAddr          - Source IP address of the        */
/*                                           previously deleted/modified     */
/*                                           IP FWD entry                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopFwdUpdateIpFwdRtrEntryOnDelPort (UINT4 u4Instance,
                                      tSnoopGroupEntry * pSnoopGroupEntry,
                                      tSnoopIpGrpFwdEntry * pIpGrpFwdEntry,
                                      tIPvXAddr SrcIpAddr)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopNextIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT1              *pSnoopRtrPortBmp = NULL;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if ((pIpGrpFwdEntry == NULL) ||
        (SNOOP_OUTER_VLAN (pIpGrpFwdEntry->VlanId) !=
         SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)))
    {
        SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

        SNOOP_OUTER_VLAN (IpGrpFwdEntry.VlanId) =
            SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
        SNOOP_INNER_VLAN (IpGrpFwdEntry.VlanId) = SNOOP_INVALID_VLAN_ID;

        IpGrpFwdEntry.u1AddressType = pSnoopGroupEntry->GroupIpAddr.u1Afi;

        IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                        (UINT1 *) &SrcIpAddr);
        IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                        (UINT1 *) &(pSnoopGroupEntry->GroupIpAddr));

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                             (tRBElem *) & IpGrpFwdEntry);

        if (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                         IpMcastFwdEntry, pRBElem, NULL);

            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
            pSnoopNextIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBNextElem;

            if ((pSnoopNextIpGrpFwdEntry != NULL) &&
                (SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId) ==
                 SNOOP_OUTER_VLAN (pSnoopNextIpGrpFwdEntry->VlanId)))
            {
                return SNOOP_SUCCESS;
            }
            /* Check if there is only router ports are present in the bitmap 
             * then delete the group record from the IP forwarding database */
            if (SnoopVlanGetVlanEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                       pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                       &pSnoopVlanEntry) == SNOOP_SUCCESS)
            {
                pSnoopRtrPortBmp =
                    UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                if (pSnoopRtrPortBmp == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                               SNOOP_GRP_TRC,
                               "Error in allocating memory for pSnoopRtrPortBmp\r\n");
                    SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL,
                                           SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                           SNOOP_OS_RES_DBG,
                                           SnoopSysErrString
                                           [SYS_LOG_MEM_ALLOC_FAIL],
                                           "for pSnoopRtrPortBmp\r\n");
                    return CLI_FAILURE;
                }
                SNOOP_MEM_SET (pSnoopRtrPortBmp, 0, sizeof (tSnoopPortBmp));
                SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                                         SNOOP_OUTER_VLAN
                                                         (pSnoopIpGrpFwdEntry->
                                                          VlanId),
                                                         pSnoopGroupEntry->
                                                         GroupIpAddr.u1Afi, 0,
                                                         pSnoopRtrPortBmp,
                                                         pSnoopVlanEntry);
                if (SNOOP_MEM_CMP
                    (pSnoopIpGrpFwdEntry->PortBitmap, pSnoopRtrPortBmp,
                     SNOOP_PORT_LIST_SIZE) == 0)
                {
#ifdef PIM_WANTED
                    if (SnoopUtilGetPIMInterfaceStatus
                        (pSnoopIpGrpFwdEntry->VlanId) != SNOOP_SUCCESS)
                    {
#endif
                        if ((SNOOP_MEM_CMP (gNullPortBitMap,
                                            pSnoopVlanEntry->RtrPortBitmap,
                                            SNOOP_PORT_LIST_SIZE) == 0))
                        {
                            if (SnoopDeleteIpFwdTable
                                (u4Instance,
                                 pSnoopIpGrpFwdEntry) != SNOOP_SUCCESS)
                            {
                                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
                                return SNOOP_FAILURE;
                            }
                        }
#ifdef PIM_WANTED
                    }
#endif
                }
                UtilPlstReleaseLocalPortList (pSnoopRtrPortBmp);
            }
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeleteIpFwdTable                                */
/*                                                                           */
/* Description        : This function deletes IP based forwarding entry      */
/*                                                                           */
/* Input(s)           : pSnoopGroupEntry   - pointer to group entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopDeleteIpFwdTable (UINT4 u4Instance,
                       tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry)
{
    tSnoopFilter        SnoopDelEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;

    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
    }

#ifdef NPAPI_WANTED
    if ((SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_DISABLE) ||
        (SnoopIsOuterVlanIpFwdEntryExists (u4Instance, pSnoopIpGrpFwdEntry) !=
         SNOOP_SUCCESS))
    {
        if (SnoopNpUpdateMcastFwdEntry (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                        pSnoopIpGrpFwdEntry->SrcIpAddr,
                                        pSnoopIpGrpFwdEntry->GrpIpAddr,
                                        pSnoopIpGrpFwdEntry->PortBitmap,
                                        SNOOP_DELETE_FWD_ENTRY) !=
            SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME, "Deletion of IP forwarding"
                           " entry from hardware failed\r\n");
            return SNOOP_FAILURE;
        }
    }
#endif
    /* Indicate the aging out of IP forwarding entry */
    SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
    SNOOP_MEM_CPY (SnoopDelEntry.VlanId, pSnoopIpGrpFwdEntry->VlanId,
                   sizeof (tSnoopTag));
    SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                   &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                   &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
    SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);
    /* Indicate the aging out of IP forwarding entry */
    SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
    if (pSnoopInstInfo != NULL)
    {
        pSnoopInfo =
            &(pSnoopInstInfo->
              SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
        if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
        {
            pSnoopInfo->u4FwdGroupsCnt -= 1;
        }
    }

    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                  (tRBElem *) pSnoopIpGrpFwdEntry);
    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopFwdUpdateFwdEntries                             */
/*                                                                           */
/* Description        : This function updates the forward table              */
/*                                                                           */
/* Input(s)           : pSnoopGroupEntry   - pointer to group entry          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopFwdUpdateFwdEntries (tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopMacGrpFwdEntry *pSnoopMacGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pTempSnoopGroupEntry = NULL;
    tSnoopMacAddr       GrpMacAddr;
    tIPvXAddr           SrcAddr;
    UINT4               u4Port = 0;
    UINT1               u1Instance = 0;
    BOOL1               bResult = SNOOP_FALSE;

    SNOOP_MEM_SET (&GrpMacAddr, 0, sizeof (tSnoopMacAddr));
    SNOOP_MEM_SET (&SrcAddr, 0, sizeof (tIPvXAddr));
    if (SNOOP_MCAST_FWD_MODE (u1Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
#ifdef IGS_WANTED
        if (pSnoopGroupEntry->GroupIpAddr.u1Afi == SNOOP_ADDR_TYPE_IPV4)
        {
            SNOOP_GET_MAC_FROM_IPV4 (SNOOP_PTR_FETCH_4
                                     (pSnoopGroupEntry->GroupIpAddr.au1Addr),
                                     GrpMacAddr);
        }
#endif
        if (SnoopFwdGetMacForwardingEntry (u1Instance,
                                           pSnoopGroupEntry->VlanId,
                                           GrpMacAddr,
                                           pSnoopGroupEntry->GroupIpAddr.u1Afi,
                                           &pSnoopMacGrpFwdEntry)
            == SNOOP_SUCCESS)
        {
            /* Scan if the Group IP entry already exists for the 
             * MAC entry */
            SNOOP_SLL_SCAN (&pSnoopMacGrpFwdEntry->GroupEntryList,
                            pTempSnoopGroupEntry, tSnoopGroupEntry *)
            {
                if (SNOOP_MEM_CMP ((UINT1 *) pTempSnoopGroupEntry->
                                   GroupIpAddr.au1Addr,
                                   (UINT1 *) pSnoopGroupEntry->GroupIpAddr.
                                   au1Addr, IPVX_MAX_INET_ADDR_LEN) == 0)
                {
                    break;
                }
            }
            SNOOP_SLL_DELETE (&pSnoopMacGrpFwdEntry->GroupEntryList,
                              pTempSnoopGroupEntry);
        }
    }

    for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Port++)
    {
        if ((SNOOP_MCAST_FWD_MODE (u1Instance) == SNOOP_MCAST_FWD_MODE_MAC) &&
            (pSnoopMacGrpFwdEntry != NULL))
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopMacGrpFwdEntry->PortBitmap, u4Port,
                                     SNOOP_PORT_LIST_SIZE, bResult);
        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopGroupEntry->PortBitmap, u4Port,
                                     SNOOP_PORT_LIST_SIZE, bResult);
        }
        if (bResult == OSIX_FALSE)
        {
            continue;
        }

        /* Updations for "On the FLY" chnages */
        /* Decrement the reference count */
        if (SNOOP_GRP_ENTRY_COUNT (u4Port) > 0)
        {
            SNOOP_GRP_ENTRY_COUNT (u4Port)--;
        }

        /* For MAC based forawding table we have to delete the port one by one
         * checking if the port is not present in any other group entry 
         * this logic is done inside SNOOP_DEL_PORT case of 
         * SnoopFwdUpdateMacFwdTable */

        if (pSnoopMacGrpFwdEntry != NULL)
        {
            if (SnoopFwdUpdateMacFwdTable (u1Instance, pSnoopGroupEntry,
                                           u4Port, SNOOP_DEL_PORT,
                                           SNOOP_DEL_PORT) != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u1Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_FWD_NAME, "Error in Deleting Port in Mac "
                               "forwarding table\r\n");
                return SNOOP_FAILURE;
            }
        }
    }
    return SNOOP_SUCCESS;
}
