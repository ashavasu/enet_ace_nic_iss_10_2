/*****************************************************************************/
/*    FILE  NAME            : snpbk.c                                        */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : snooping backward forwarding module            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                   */
/*    DESCRIPTION           : This file contains forwarding routines         */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* $Id: snpbk.c,v 1.21 2017/11/17 12:09:43 siva Exp $*/
/*---------------------------------------------------------------------------*/

#include "snpinc.h"

/*****************************************************************************/
/* Function Name      : SnoopMiVlanSnoopFwdPacketOnPortList                  */
/*                                                                           */
/* Description        : This function calls the VLAN Module to get           */
/*                      forward the packet on a given portlist & given VLAN  */
/*                                                                           */
/* Input(s)           : pBuf - Packet to be forwarded.                       */
/*                     DestMacAddr  - Destination MAC Address                */
/*                      u2Port       - Packet received on this Port Number   */
/*                      VlanId       - VLAN Identifier                       */
/*                      u1Forward       - VLAN_FORWARD_SPECIFIC/             */
/*                                        VLAN_FORWARD_ALL                   */
/*                      PortBitmap   - Port list                             */
/*                                                                           */
/* Output(s)          : TaggedPortBitmap - Tagged port list                  */
/*                      UntaggedPortBitmap - Untagged port list              */
/*                                                                           */
/* Return Value(s)    : VLAN_FORWARD/ VLAN_NO_FORWARD                        */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopMiVlanSnoopFwdPacketOnPortList (tCRU_BUF_CHAIN_DESC * pBuf,
                                     UINT4 u4ContextId, tMacAddr DestMacAddr,
                                     UINT2 u2Port, tSnoopTag VlanId,
                                     UINT1 u1IsQuery, UINT1 u1Forward,
                                     tSnoopPortBmp PortBitmap)
{

    tSnoopIfPortBmp    *pTaggedPortBitmap = NULL;
    tSnoopIfPortBmp    *pUntaggedPortBitmap = NULL;
    UINT1              *pu1Data = NULL;
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT1               au1Buf[VLAN_DOUBLE_TAGGED_CVLAN_SIZE] = { 0 };
    INT4                i4RetVal = 0;
    UINT4               u4PktSize = 0;
    UINT4               u4Index = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT2               u2CVid = 0;
    UINT1               u1PortFlag = 0;
    BOOL1               bTagResult = OSIX_FALSE;
    BOOL1               bUntagResult = OSIX_FALSE;

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG,
               SNOOP_ENTRY_DBG,
               "Entry : SnoopMiVlanSnoopFwdPacketOnPortList\r\n");
    /* SNOOP will be maintaining the virtual port bitmap corresponding to 
     * the physical port bitmap obtained from the multiple instance manager
     */
    if ((SnoopGetPhyPortBmp (u4ContextId, PortBitmap, gSnoopIfPortList)) !=
        SNOOP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return VLAN_NO_FORWARD;
    }
    if (u1Forward == VLAN_FORWARD_ALL)
    {
        i4RetVal =
            SnoopMiGetVlanEgressPorts (u4ContextId, VlanId, gSnoopIfPortList);
        UNUSED_PARAM (i4RetVal);

    }

    /* Check whether IGS and enhanced mode is enabled to take VLAN entry */

    if ((SNOOP_SNOOPING_STATUS (u4ContextId, (SNOOP_ADDR_TYPE_IPV4 - 1)) ==
         SNOOP_ENABLE) &&
        (SNOOP_INSTANCE_ENH_MODE (u4ContextId) == SNOOP_ENABLE))
    {
        if (SnoopVlanGetVlanEntry (u4ContextId, VlanId,
                                   SNOOP_ADDR_TYPE_IPV4,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return VLAN_NO_FORWARD;
        }

        if (pSnoopVlanEntry == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return VLAN_NO_FORWARD;
        }
    }

    pTaggedPortBitmap =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (pTaggedPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopMiVlanSnoopFwdPacketOnPortList: "
                   "Error in allocating memory for pTaggedPortBitmap\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return VLAN_NO_FORWARD;
    }
    SNOOP_MEM_SET (*pTaggedPortBitmap, 0, sizeof (tSnoopIfPortBmp));

    pUntaggedPortBitmap =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
    if (pUntaggedPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "SnoopMiVlanSnoopFwdPacketOnPortList: "
                   "Error in allocating memory for pUntaggedPortBitmap\r\n");
        FsUtilReleaseBitList ((UINT1 *) pTaggedPortBitmap);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return VLAN_NO_FORWARD;
    }
    SNOOP_MEM_SET (*pUntaggedPortBitmap, 0, sizeof (tSnoopIfPortBmp));

    if (u1IsQuery == OSIX_FALSE)
    {
        u1Forward = VLAN_FORWARD_SPECIFIC;

        i4RetVal =
            SnoopMiVlanSnoopGetTxPortList (u4ContextId, DestMacAddr,
                                           u2Port, VlanId,
                                           u1Forward, gSnoopIfPortList,
                                           *pTaggedPortBitmap,
                                           *pUntaggedPortBitmap);

        if (i4RetVal == VLAN_FORWARD)
        {
            if (SNOOP_MEM_CMP (*pUntaggedPortBitmap, gNullIfPortBitMap,
                               SNOOP_IF_PORT_LIST_SIZE) != 0)
            {

                for (u2ByteIndex = 0; u2ByteIndex < SNOOP_IF_PORT_LIST_SIZE;
                     u2ByteIndex++)
                {
                    u1PortFlag = (*pUntaggedPortBitmap)[u2ByteIndex];

                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4Index = (UINT4) ((u2ByteIndex *
                                                SNOOP_PORTS_PER_BYTE) +
                                               u2BitIndex + 1);

                            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                            if (pDupBuf != NULL)
                            {

                                if (SNOOP_INSTANCE_ENH_MODE (u4ContextId) ==
                                    SNOOP_ENABLE)
                                {
                                    SNOOP_MEM_SET (&CfaIfInfo, 0,
                                                   sizeof (tCfaIfInfo));
                                    SnoopCfaGetIfInfo (u4Index, &CfaIfInfo);
                                    if (CfaIfInfo.u1BrgPortType ==
                                        CFA_CUSTOMER_EDGE_PORT)
                                    {
                                        /* Outgoing port is CEP. Modify the Report on CEP
                                           based on CVID registration table.
                                           Send the report to the customer vlan from which
                                           query is received. 
                                         */

                                        SNOOP_SLL_SCAN (&pSnoopVlanEntry->
                                                        RtrPortList,
                                                        pRtrPortNode,
                                                        tSnoopRtrPortEntry *)
                                        {
                                            if (pRtrPortNode->u4PhyPortIndex ==
                                                u4Index)
                                            {
                                                SNOOP_INNER_VLAN (VlanId) =
                                                    pRtrPortNode->u2InnerVlanId;

                                                /* If Inner vlan id is 0, then use default CVLAN id */

                                                if (SNOOP_INNER_VLAN (VlanId) ==
                                                    SNOOP_INVALID_VLAN_ID)
                                                {

                                                    if (SnoopVlanGetCpvidForCep
                                                        (u4Index,
                                                         &u2CVid) ==
                                                        SNOOP_SUCCESS)
                                                    {
                                                        SNOOP_INNER_VLAN
                                                            (VlanId) = u2CVid;
                                                    }
                                                }

                                                break;
                                            }
                                        }

                                        SnoopVlanModReportOnCep (VlanId,
                                                                 u4Index,
                                                                 pDupBuf);

                                        /* Reset the Vlan Identifier for the next router port */
                                        SNOOP_INNER_VLAN (VlanId) =
                                            SNOOP_INVALID_VLAN_ID;
                                    }
                                    else if ((CfaIfInfo.u1BrgPortType ==
                                              CFA_CNP_PORTBASED_PORT)
                                             || (CfaIfInfo.u1BrgPortType ==
                                                 CFA_CNP_STAGGED_PORT))
                                    {
                                        /* Outgoing port is CNP port-based/s-tagged. So modify the report to 
                                           send to the customer vlan in which query is received */
                                        SNOOP_SLL_SCAN (&pSnoopVlanEntry->
                                                        RtrPortList,
                                                        pRtrPortNode,
                                                        tSnoopRtrPortEntry *)
                                        {
                                            if (pRtrPortNode->u4PhyPortIndex ==
                                                u4Index)
                                            {
                                                SNOOP_INNER_VLAN (VlanId) =
                                                    pRtrPortNode->u2InnerVlanId;
                                                break;
                                            }
                                        }
                                        /* If Inner vlan id is 0, then no need to modify the report */
                                        if (SNOOP_INNER_VLAN (VlanId) ==
                                            SNOOP_INVALID_VLAN_ID)
                                        {
                                            u4PktSize =
                                                CRU_BUF_Get_ChainValidByteCount
                                                (pDupBuf);
#ifdef SRCMAC_CTP_WANTED
                                            SnoopHandlePortOutgoingPkt
                                                (u4ContextId, VlanId, pDupBuf,
                                                 u4Index, u4PktSize, 0,
                                                 CFA_ENCAP_NONE);
#else
                                            SnoopHandleOutgoingPktOnPort
                                                (pDupBuf, (UINT2) u4Index,
                                                 u4PktSize, 0, CFA_ENCAP_NONE);
#endif
                                        }
                                        else
                                        {
                                            /* Inner vlan id is present. */
                                            /* For port-based CNP/CNP s-tagged(untagged meber of s-lvan), add cvlan tag and send */
                                            SnoopVlanTagFrame (pDupBuf,
                                                               SNOOP_INNER_VLAN
                                                               (VlanId),
                                                               SNOOP_VLAN_HIGHEST_PRIORITY);
                                            u4PktSize =
                                                CRU_BUF_Get_ChainValidByteCount
                                                (pDupBuf);
                                            SnoopHandleOutgoingPktOnPort
                                                (pDupBuf, (UINT2) u4Index,
                                                 u4PktSize, 0, CFA_ENCAP_NONE);
                                        }
                                    }
                                }
                                else
                                {
                                    u4PktSize =
                                        CRU_BUF_Get_ChainValidByteCount
                                        (pDupBuf);
#ifdef SRCMAC_CTP_WANTED
                                    SnoopHandlePortOutgoingPkt (u4ContextId,
                                                                VlanId, pDupBuf,
                                                                u4Index,
                                                                u4PktSize, 0,
                                                                CFA_ENCAP_NONE);
#else
                                    SnoopHandleOutgoingPktOnPort (pDupBuf,
                                                                  (UINT2)
                                                                  u4Index,
                                                                  u4PktSize, 0,
                                                                  CFA_ENCAP_NONE);
#endif
                                }
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }

            }
            if (SNOOP_MEM_CMP (*pTaggedPortBitmap, gNullIfPortBitMap,
                               SNOOP_IF_PORT_LIST_SIZE) != 0)
            {
                for (u2ByteIndex = 0; u2ByteIndex < SNOOP_IF_PORT_LIST_SIZE;
                     u2ByteIndex++)
                {
                    u1PortFlag = (*pTaggedPortBitmap)[u2ByteIndex];

                    for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                          && (u1PortFlag != 0)); u2BitIndex++)
                    {
                        if ((u1PortFlag & SNOOP_BIT8) != 0)
                        {
                            u4Index = (UINT4) ((u2ByteIndex *
                                                SNOOP_PORTS_PER_BYTE) +
                                               u2BitIndex + 1);

                            pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                            if (pDupBuf != NULL)
                            {
                                SnoopVlanAddOuterTagInFrame
                                    (pDupBuf, VlanId,
                                     SNOOP_VLAN_HIGHEST_PRIORITY, u4Index);

                                if (SNOOP_INSTANCE_ENH_MODE (u4ContextId) ==
                                    SNOOP_ENABLE)
                                {
                                    SNOOP_MEM_SET (&CfaIfInfo, 0,
                                                   sizeof (tCfaIfInfo));
                                    SnoopCfaGetIfInfo (u4Index, &CfaIfInfo);

                                    /* If port is PNP add the Stag based on Svlan translation
                                       table */
                                    if (CfaIfInfo.u1BrgPortType ==
                                        CFA_PROVIDER_NETWORK_PORT)
                                    {
                                        SnoopVlanApplyVidTransInFrame (u4Index,
                                                                       VlanId,
                                                                       pDupBuf);
                                    }
                                    else if (CfaIfInfo.u1BrgPortType ==
                                             CFA_CNP_STAGGED_PORT)
                                    {
                                        /* If port is CNP s-tagged, So modify the report to send to the customer vlan in which query is received */
                                        SNOOP_SLL_SCAN (&pSnoopVlanEntry->
                                                        RtrPortList,
                                                        pRtrPortNode,
                                                        tSnoopRtrPortEntry *)
                                        {
                                            if (pRtrPortNode->u4PhyPortIndex ==
                                                u4Index)
                                            {
                                                SNOOP_INNER_VLAN (VlanId) =
                                                    pRtrPortNode->u2InnerVlanId;
                                                break;
                                            }
                                        }
                                        SnoopVlanTagFrame (pDupBuf,
                                                           SNOOP_INNER_VLAN
                                                           (VlanId),
                                                           SNOOP_VLAN_HIGHEST_PRIORITY);
                                    }
                                }
                                u4PktSize =
                                    CRU_BUF_Get_ChainValidByteCount (pDupBuf);
#ifdef SRCMAC_CTP_WANTED
                                SnoopHandlePortOutgoingPkt (u4ContextId, VlanId,
                                                            pDupBuf, u4Index,
                                                            u4PktSize, 0,
                                                            CFA_ENCAP_NONE);
#else
                                SnoopHandleOutgoingPktOnPort (pDupBuf,
                                                              (UINT2) u4Index,
                                                              u4PktSize, 0,
                                                              CFA_ENCAP_NONE);
#endif
                            }
                        }
                        u1PortFlag = (UINT1) (u1PortFlag << 1);
                    }
                }
            }

        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else
    {
        i4RetVal =
            SnoopMiVlanSnoopGetTxPortList (u4ContextId, DestMacAddr, u2Port,
                                           VlanId, u1Forward,
                                           gSnoopIfPortList, *pTaggedPortBitmap,
                                           *pUntaggedPortBitmap);

        if (i4RetVal == VLAN_FORWARD)
        {
            for (u2ByteIndex = 0; u2ByteIndex < SNOOP_IF_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PortFlag = gSnoopIfPortList[u2ByteIndex];

                for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & SNOOP_BIT8) != 0)
                    {
                        u4Index =
                            (UINT4) ((u2ByteIndex * SNOOP_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                        OSIX_BITLIST_IS_BIT_SET ((*pTaggedPortBitmap), u4Index,
                                                 SNOOP_IF_PORT_LIST_SIZE,
                                                 bTagResult);
                        OSIX_BITLIST_IS_BIT_SET ((*pUntaggedPortBitmap),
                                                 u4Index,
                                                 SNOOP_IF_PORT_LIST_SIZE,
                                                 bUntagResult);

                        if ((bTagResult == OSIX_FALSE) &&
                            (bUntagResult == OSIX_FALSE))
                        {
                            u1PortFlag = (UINT1) (u1PortFlag << 1);
                            continue;
                        }

                        pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                        if (pDupBuf != NULL)
                        {
                            SNOOP_MEM_SET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
                            SnoopCfaGetIfInfo (u4Index, &CfaIfInfo);

                            if (((CfaIfInfo.u1BrgPortType ==
                                  CFA_CUSTOMER_EDGE_PORT) ||
                                 (CfaIfInfo.u1BrgPortType ==
                                  CFA_PROP_CUSTOMER_EDGE_PORT) ||
                                 (CfaIfInfo.u1BrgPortType ==
                                  CFA_PROP_CUSTOMER_NETWORK_PORT)) &&
                                (SNOOP_INSTANCE_ENH_MODE (u4ContextId) ==
                                 SNOOP_ENABLE))
                            {
                                SnoopVlanDupAndFwdQuery (VlanId, u4Index,
                                                         pDupBuf, bTagResult);
                            }

                            else
                            {
                                if (bTagResult == OSIX_TRUE)
                                {
                                    /* If the port is PNP and enhanced mode is enabled
                                       Send only SVLAN tagged query to the provider
                                       network
                                     */
                                    if ((CfaIfInfo.u1BrgPortType ==
                                         CFA_PROVIDER_NETWORK_PORT)
                                        &&
                                        (SNOOP_INSTANCE_ENH_MODE (u4ContextId)
                                         == SNOOP_ENABLE))
                                    {
                                        /* Check whether CVLAN is present or not. */

                                        SNOOP_COPY_FROM_CRU_BUF (pDupBuf,
                                                                 au1Buf, 0,
                                                                 VLAN_DOUBLE_TAGGED_CVLAN_SIZE);
                                        pu1Data = au1Buf;

                                        MEMCPY ((UINT1 *) &u2EtherType,
                                                &pu1Data[VLAN_TAG_OFFSET],
                                                VLAN_TYPE_OR_LEN_SIZE);

                                        u2EtherType = OSIX_NTOHS (u2EtherType);

                                        /* If Ctag present remove the Ctag */

                                        if (u2EtherType == VLAN_PROTOCOL_ID)
                                        {
                                            SnoopVlanUntagFrame (pDupBuf);
                                        }
                                    }

                                    SnoopVlanAddOuterTagInFrame
                                        (pDupBuf, VlanId,
                                         SNOOP_VLAN_HIGHEST_PRIORITY, u4Index);
                                }

                                /* If port is PNP add the Stag based on Svlan translation
                                   table */

                                if ((CfaIfInfo.u1BrgPortType ==
                                     CFA_PROVIDER_NETWORK_PORT)
                                    && (SNOOP_INSTANCE_ENH_MODE (u4ContextId) ==
                                        SNOOP_ENABLE))
                                {
                                    SnoopVlanApplyVidTransInFrame (u4Index,
                                                                   VlanId,
                                                                   pDupBuf);
                                }

                                u4PktSize =
                                    CRU_BUF_Get_ChainValidByteCount (pDupBuf);

                                SnoopHandleOutgoingPktOnPort
                                    (pDupBuf, (UINT2) u4Index,
                                     u4PktSize, 0, CFA_ENCAP_NONE);
                            }
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    SNOOP_MEM_SET (gSnoopIfPortList, 0, sizeof (tSnoopIfPortBmp));

    FsUtilReleaseBitList ((UINT1 *) pTaggedPortBitmap);
    FsUtilReleaseBitList ((UINT1 *) pUntaggedPortBitmap);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopMiVlanSnoopFwdPacketOnPortList\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanDupAndFwdQuery                              */
/*                                                                           */
/* Description        : This function duplicates the query for each C-VLAN.  */
/*                      The C-VLANs will be retrieved from S-VLAN            */
/*                      Classification table whichis mapped to the           */
/*                      corresponding port. If there is no C-VLAN only one   */
/*                      untagged packet will be sent out on the port. This   */
/*                      function is applicable for CEP, PCEP and PCNP ports. */
/*                                                                           */
/* Input(s)           : VlanId     - VLAN Identifier                         */
/*                      u4Port     - Incoming port number                    */
/*                      pBuf       - pointer to the packet buffer            */
/*                      bTag       - Need to add outer-tag or not            */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopVlanDupAndFwdQuery (tSnoopTag VlanId, UINT4 u4Port,
                         tCRU_BUF_CHAIN_HEADER * pBuf, BOOL1 bTag)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf;
    UINT4               u4PktSize = 0;
    tVlanId             CVlanId;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1VlanFlag = 0;
    BOOL1               bResult = OSIX_FALSE;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopVlanDupAndFwdQuery\r\n");
    SNOOP_MEM_SET (&gSnoopCVlanInfo, 0, sizeof (tCVlanInfo));
    SNOOP_MEM_SET (&gSnoopNullVlanList, 0, sizeof (tSnoopVlanList));

    SnoopVlanGetCVlanIdList (u4Port, VlanId, &gSnoopCVlanInfo);

    if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        /* If Inner Vlan is set, already the packet is tagged with CVLAN. So the packet has to be validated
         * only for that C-VLAN ie. we have to send only one copy of the packet for that particular cvlan on
         * the customer port.*/

        OSIX_BITLIST_IS_BIT_SET (gSnoopCVlanInfo.UntaggedCVlanList,
                                 SNOOP_INNER_VLAN (VlanId),
                                 VLAN_DEV_VLAN_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            SnoopVlanUntagFrame (pBuf);

        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (gSnoopCVlanInfo.TaggedCVlanList,
                                     SNOOP_INNER_VLAN (VlanId),
                                     VLAN_DEV_VLAN_LIST_SIZE, bResult);

            if (bResult != OSIX_TRUE)
            {
                /* The Inner Vlan is not part of both tagged c-vlan list and untagged c-vlan list.
                   So no need to send  the packet, just return */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
        }

        if (bTag == OSIX_TRUE)
        {
            /* In case of PCNP, this variable can be set */
            SnoopVlanAddOuterTagInFrame (pBuf, VlanId,
                                         SNOOP_VLAN_HIGHEST_PRIORITY, u4Port);
        }

        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        SnoopHandleOutgoingPktOnPort (pBuf, (UINT2) u4Port,
                                      u4PktSize, 0, CFA_ENCAP_NONE);

        return;
    }

    /* If UntagCEP is true, then we have to send untagged packets for that CVLAN. But it is enough to
     * send only one untagged packet for all the untagged CVLANs */

    if (SNOOP_MEM_CMP
        (gSnoopCVlanInfo.UntaggedCVlanList, gSnoopNullVlanList,
         sizeof (tSnoopVlanList)) != 0)
    {
        pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

        if (pDupBuf == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return;
        }

        if (bTag == OSIX_TRUE)
        {
            SnoopVlanAddOuterTagInFrame (pDupBuf, VlanId,
                                         SNOOP_VLAN_HIGHEST_PRIORITY, u4Port);
        }

        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pDupBuf);

        SnoopHandleOutgoingPktOnPort (pDupBuf, (UINT2) u4Port,
                                      u4PktSize, 0, CFA_ENCAP_NONE);
    }

    /* If UntagCEP is false, then we have to send tagged packets for all those CVLAN. */

    if (SNOOP_MEM_CMP
        (gSnoopCVlanInfo.TaggedCVlanList, gSnoopNullVlanList,
         sizeof (tSnoopVlanList)) != 0)
    {
        for (u2ByteIndex = 0; u2ByteIndex < VLAN_DEV_VLAN_LIST_SIZE;
             u2ByteIndex++)
        {
            u1VlanFlag = gSnoopCVlanInfo.TaggedCVlanList[u2ByteIndex];

            for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                  && (u1VlanFlag != 0)); u2BitIndex++)
            {
                if ((u1VlanFlag & SNOOP_BIT8) != 0)
                {
                    CVlanId = (tVlanId) ((u2ByteIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);
                    pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                    if (pDupBuf != NULL)
                    {
                        /* Adding C-Tag to the packet */
                        SnoopVlanTagFrame (pDupBuf, CVlanId,
                                           SNOOP_VLAN_HIGHEST_PRIORITY);

                        if (bTag == OSIX_TRUE)
                        {
                            SnoopVlanAddOuterTagInFrame
                                (pDupBuf, VlanId,
                                 SNOOP_VLAN_HIGHEST_PRIORITY, u4Port);
                        }
                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pDupBuf);

                        SnoopHandleOutgoingPktOnPort (pDupBuf, (UINT2) u4Port,
                                                      u4PktSize, 0,
                                                      CFA_ENCAP_NONE);
                    }
                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }

        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    else if (gSnoopCVlanInfo.u1IsCEPPort == OSIX_FALSE)
    {
        /* u1IsCEPPort indicates that this port does not uses CVID registration table and uses other table.
         * So, it is mandatory atleast to send a untagged packet for those ports when the tagged cvlan list
         * is null.*/

        if (bTag == OSIX_TRUE)
        {
            SnoopVlanAddOuterTagInFrame (pBuf, VlanId,
                                         SNOOP_VLAN_HIGHEST_PRIORITY, u4Port);
        }
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        SnoopHandleOutgoingPktOnPort (pBuf, (UINT2) u4Port,
                                      u4PktSize, 0, CFA_ENCAP_NONE);
    }
    else
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopVlanDupAndFwdQuery\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanGetCVlanIdList                              */
/*                                                                           */
/* Description        : This function calls the VLAN Module to retrieve      */
/*                      the Customer VLAN list for the given Port, service   */
/*                      VLAN in the CVID Registration table.                 */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                    : VlanId  - Vlan Identifier                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVlanGetCVlanIdList (UINT4 u4Instance, tSnoopTag VlanId,
                         tCVlanInfo * pCVlanInfo)
{
    return (VlanGetCVlanInfoOnCustomerPort (u4Instance,
                                            SNOOP_OUTER_VLAN (VlanId),
                                            pCVlanInfo));
}

/*****************************************************************************/
/* Function Name      : SnoopVlanAddOuterTagInFrame                          */
/*                                                                           */
/* Description        : This function calls the VLAN Module to add the       */
/*                      Outer tag for the given frame                        */
/*                                                                           */
/* Input(s)           : pDupBuf - Buffer to be tagged                        */
/*                      VlanId  - Vlan Identifier                            */
/*                      u1Priority - SNOOP_VLAN_HIGHEST_PRIORITY             */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopVlanAddOuterTagInFrame (tCRU_BUF_CHAIN_HEADER * pDupBuf, tSnoopTag VlanId,
                             UINT1 u1Priority, UINT4 u4IfIndex)
{
    VlanAddOuterTagInFrame (pDupBuf, SNOOP_OUTER_VLAN (VlanId),
                            u1Priority, u4IfIndex);
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUntagFrame                                  */
/*                                                                           */
/* Description        : This function calls the VLAN Module to strip the     */
/*                      tagged frame.                                        */
/*                                                                           */
/* Input(s)           : pDupBuf - Buffer to be untagged                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopVlanUntagFrame (tCRU_BUF_CHAIN_HEADER * pDupBuf)
{
    VlanUnTagFrame (pDupBuf);

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanGetCpvidForCep                              */
/*                                                                           */
/* Description        : This function calls the VLAN Module to get the CPVID */
/*                      of the customer edge port                            */
/*                                                                           */
/* Input(s)           : u4Port - Port index                                  */
/*                                                                           */
/* Output(s)          : pu2CVid - Default customer vlan id of the port       */
/*                                                                           */
/* Return Value(s)    : SNOOP_FAILURE / SNOOP_SUCCESS                        */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVlanGetCpvidForCep (UINT4 u4Port, UINT2 *pu2CVid)
{
#ifdef EVB_WANTED
    if (VlanGetCpvidForCep (u4Port, pu2CVid) == VLAN_FAILURE)
    {
        return SNOOP_FAILURE;
    }
#else
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (pu2CVid);
#endif

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopHandleOutgoingPktOnPort                         */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      snooping packet.                                     */
/*                                                                           */
/* Input(s)           : pDupBuf - pointer to the outgoing packet             */
/*                      u4IfIndex        - Interface IfIndex                 */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopHandleOutgoingPktOnPort (tCRU_BUF_CHAIN_HEADER * pDupBuf, UINT2 u2IfIndex,
                              UINT4 u4PktSize, UINT2 u2Protocol, UINT1 u1Encap)
{
    L2IwfHandleOutgoingPktOnPort (pDupBuf, u2IfIndex,
                                  u4PktSize, u2Protocol, u1Encap);
}

/*****************************************************************************/
/* Function Name      : SnoopHandleOutgoingPktOnPortList                     */
/*                                                                           */
/* Description        : This function calls the L2Iwf module to send out the */
/*                      snooping packet.                                     */
/*                                                                           */
/* Input(s)           : pDupBuf - pointer to the outgoing packet             */
/*                      TaggedPortBitmap - Port list                         */
/*                      u4PktSize        - Packet size                       */
/*                      u2Protocol       - Protocol type                     */
/*                      u1Encap          - Encapsulation Type                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopHandleOutgoingPktOnPortList (tCRU_BUF_CHAIN_HEADER * pDupBuf,
                                  tSnoopIfPortBmp PortBitmap,
                                  UINT4 u4PktSize, UINT2 u2Protocol,
                                  UINT1 u1Encap)
{
    tPortList          *pPortList = NULL;

    pPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pPortList == NULL)
    {
        return;
    }

    SNOOP_MEM_SET (*pPortList, 0, sizeof (tPortList));
    SNOOP_MEM_CPY (*pPortList, PortBitmap, sizeof (tSnoopIfPortList));

    L2IwfHandleOutgoingPktOnPortList (pDupBuf, *pPortList,
                                      u4PktSize, u2Protocol, u1Encap);
    FsUtilReleaseBitList ((UINT1 *) pPortList);
}

/*****************************************************************************/
/* Function Name      : SnoopVlanModReportOnCep                              */
/*                                                                           */
/* Description        : This function adds the inner tag packet for the      */
/*                      report to be sent on CEP based on untagged CEP       */
/*                      configurations (True/False)                          */
/*                                                                           */
/* Input(s)           : VlanId - VLAN Identifier                             */
/*                      u4IfIndex  - Outgoing port number                    */
/*                      pBuf - pointer to the outgoing packet                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopVlanModReportOnCep (tSnoopTag VlanId, UINT4 u4IfIndex,
                         tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4PktSize = 0;
    tVlanId             CVlanId = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT2               u2EtherType = 0;
    UINT1               u1VlanFlag = 0;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (&gSnoopCVlanInfo, 0, sizeof (tCVlanInfo));
    SnoopVlanGetCVlanIdList (u4IfIndex, VlanId, &gSnoopCVlanInfo);

    /* If report transmitted on CEP, we check the list of CVLANs for which
     * UnTagged CEP is set to TRUE, send it as UNTAGGED packet */

    if (SNOOP_INNER_VLAN (VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        /* If Inner Vlan is set, the report packet has been received with an Inner CVLAN. 
         * So the packet has to be validated only for that C-VLAN ie. we have to send only 
         * one copy of the packet for that particular cvlan on the customer edge port. Since 
         * report construction takes place internally, we need to check if tag needs to be
         * added based on untagged CEP (true/false) configurations  */

        OSIX_BITLIST_IS_BIT_SET (gSnoopCVlanInfo.TaggedCVlanList,
                                 SNOOP_INNER_VLAN (VlanId),
                                 VLAN_DEV_VLAN_LIST_SIZE, bResult);

        /* untagged CEP False */
        if (bResult == OSIX_TRUE)
        {
            SnoopVlanTagFrame (pBuf, SNOOP_INNER_VLAN (VlanId),
                               SNOOP_VLAN_HIGHEST_PRIORITY);

        }
        else
        {
            OSIX_BITLIST_IS_BIT_SET (gSnoopCVlanInfo.UntaggedCVlanList,
                                     SNOOP_INNER_VLAN (VlanId),
                                     VLAN_DEV_VLAN_LIST_SIZE, bResult);

            if (bResult != OSIX_TRUE)
            {
                /* The Inner Vlan is not part of both tagged c-vlan list and untagged c-vlan list.
                   So no need to send the packet, just return */

                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return;
            }
            else
            {
                /*This check is added to apply UNTAG CEP configs for group specific queries being sent out on
                   CEP in response to leave messages       */

                CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2EtherType,
                                           VLAN_TAG_OFFSET,
                                           VLAN_TYPE_OR_LEN_SIZE);

                if (OSIX_NTOHS (u2EtherType) == VLAN_PROTOCOL_ID)
                {
                    SnoopVlanUntagFrame (pBuf);
                }
            }

        }

        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        SnoopHandleOutgoingPktOnPort (pBuf, (UINT2) u4IfIndex,
                                      u4PktSize, 0, CFA_ENCAP_NONE);

        return;
    }

    if (SNOOP_MEM_CMP
        (gSnoopCVlanInfo.UntaggedCVlanList, gSnoopNullVlanList,
         sizeof (tSnoopVlanList)) != 0)
    {
        pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

        if (pDupBuf != NULL)
        {
            u4PktSize = CRU_BUF_Get_ChainValidByteCount (pDupBuf);

            /* For UNTAGGED CEP = TRUE, one untagged packet can be sent for all the entries */

            SnoopHandleOutgoingPktOnPort (pDupBuf, (UINT2) u4IfIndex,
                                          u4PktSize, 0, CFA_ENCAP_NONE);
        }
    }

    /* If UntagCEP is false, then we have to send tagged packets for all those CVLAN. */

    if (SNOOP_MEM_CMP
        (gSnoopCVlanInfo.TaggedCVlanList, gSnoopNullVlanList,
         sizeof (tSnoopVlanList)) != 0)
    {
        for (u2ByteIndex = 0; u2ByteIndex < VLAN_DEV_VLAN_LIST_SIZE;
             u2ByteIndex++)
        {
            u1VlanFlag = gSnoopCVlanInfo.TaggedCVlanList[u2ByteIndex];

            for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                                  && (u1VlanFlag != 0)); u2BitIndex++)
            {
                if ((u1VlanFlag & SNOOP_BIT8) != 0)
                {
                    CVlanId = (tVlanId) ((u2ByteIndex * SNOOP_PORTS_PER_BYTE) +
                                         u2BitIndex + 1);
                    pDupBuf = SnoopUtilDuplicateBuffer (pBuf);

                    if (pDupBuf != NULL)
                    {
                        /* Adding C-Tag to the packet */
                        SnoopVlanTagFrame (pDupBuf, CVlanId,
                                           SNOOP_VLAN_HIGHEST_PRIORITY);

                        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pDupBuf);

                        SnoopHandleOutgoingPktOnPort (pDupBuf,
                                                      (UINT2) u4IfIndex,
                                                      u4PktSize, 0,
                                                      CFA_ENCAP_NONE);
                    }
                }
                u1VlanFlag = (UINT1) (u1VlanFlag << 1);
            }
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return;

}

/*****************************************************************************/
/* Function Name      : SnoopVlanApplyVidTransInFrame                        */
/*                                                                           */
/* Description        : This function adds the relay/local vlan id for a     */
/*                      svlan based on translation table                     */
/*                                                                           */
/* Input(s)           : u4Port   - Outgoing port                             */
/*                      VlanId      - Vlan Id                                     */
/*                      pBuf - pointer to the outgoing packet                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopVlanApplyVidTransInFrame (UINT4 u4Port, tSnoopTag VlanId,
                               tCRU_BUF_CHAIN_HEADER * pDupBuf)
{
    INT4                i4RetVal = L2IWF_SUCCESS;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    tSnoopTag           LocalVlanId = { 0 };

    if ((SnoopVcmGetContextInfoFromIfIndex (u4Port, &u4ContextId,
                                            &u2LocalPort)) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4ContextId),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME, "Instance ID not found\r\n");
        return SNOOP_FAILURE;
    }

    /* Search the Local VLAN - Relay VLAN mapping by searching in Relay VLAN
       table. Because the Relay VLAN  is the actual Local VLAN in Provider Network */

    i4RetVal = L2IwfPbGetLocalVidFromRelayVid (u4ContextId, u2LocalPort,
                                               SNOOP_OUTER_VLAN (VlanId),
                                               &LocalVlanId[0]);

    if (i4RetVal == L2IWF_FAILURE)
    {
        return SNOOP_FAILURE;
    }

    /* Untag the frame and add the Local Svlan in the Tag */
    SnoopVlanUntagFrame (pDupBuf);

    SnoopVlanAddOuterTagInFrame
        (pDupBuf, LocalVlanId, SNOOP_VLAN_HIGHEST_PRIORITY, u4Port);

    return SNOOP_SUCCESS;
}
