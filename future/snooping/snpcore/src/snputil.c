/* $Id: snputil.c,v 1.115 2017/12/19 09:59:30 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snputil.c                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : snooping utilities                             */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains utilities                   */
/*                            for snooping module                            */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    10 January 2005            Initial Creation                    */
/*---------------------------------------------------------------------------*/
#include "snpinc.h"
#include "snpextn.h"
/*****************************************************************************/
/* Function Name      : SnoopUtilGetInstIdFromPort                           */
/*                                                                           */
/* Description        : This function returns the instance number associated */
/*                      with the Vlan                                        */
/*                                                                           */
/* Input(s)           : VlanId - Vlan Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : *pu4Instance                                         */
/*****************************************************************************/
VOID
SnoopUtilGetInstIdFromVlan (tSnoopTag VlanId, UINT4 *pu4Instance)
{
    /* External API will be provided when MI manager is implemented */
    UNUSED_PARAM (VlanId);
    *pu4Instance = 1;
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeConsGroupEntryCmp                     */
/*                                                                           */
/* Description        : This routine is used in Consolidated Group Table for */
/*                      comparing two keys used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : ConsGroupEntryNode - Key 1                           */
/*                      ConsGroupEntryIn   - Key 2                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeConsGroupEntryCmp (tRBElem * ConsGroupEntryNode,
                                  tRBElem * ConsGroupEntryIn)
{
    tSnoopConsolidatedGroupEntry *pConsGroupEntryNode = NULL;
    tSnoopConsolidatedGroupEntry *pConsGroupEntryIn = NULL;
    UINT1               au1ConsGroupIpEntryNode[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1ConsGroupIpEntryIn[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (au1ConsGroupIpEntryNode, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1ConsGroupIpEntryIn, 0, IPVX_MAX_INET_ADDR_LEN);

    pConsGroupEntryNode = (tSnoopConsolidatedGroupEntry *) ConsGroupEntryNode;
    pConsGroupEntryIn = (tSnoopConsolidatedGroupEntry *) ConsGroupEntryIn;
    /* Compare the Outer-VLAN ID index */
    if (SNOOP_OUTER_VLAN (pConsGroupEntryNode->VlanId) <
        SNOOP_OUTER_VLAN (pConsGroupEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pConsGroupEntryNode->VlanId) >
             SNOOP_OUTER_VLAN (pConsGroupEntryIn->VlanId))
    {
        return 1;
    }

    /* Compare the address type index */
    if (pConsGroupEntryNode->GroupIpAddr.u1Afi <
        pConsGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return -1;
    }
    else if (pConsGroupEntryNode->GroupIpAddr.u1Afi >
             pConsGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return 1;
    }

    SNOOP_MEM_CPY (au1ConsGroupIpEntryNode,
                   pConsGroupEntryNode->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1ConsGroupIpEntryNode);

    SNOOP_MEM_CPY (au1ConsGroupIpEntryIn,
                   pConsGroupEntryIn->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1ConsGroupIpEntryIn);

    /* Compare the ip address index */
    if (SNOOP_MEM_CMP (au1ConsGroupIpEntryNode, au1ConsGroupIpEntryIn,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1ConsGroupIpEntryNode, au1ConsGroupIpEntryIn,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeRtrPortEntryCmp                       */
/*                                                                           */
/* Description        : This routine is used for comparing two keys used in  */
/*                      router port RB Tree.                                 */
/*                                                                           */
/* Input(s)           : tonsGroupEntryNode - Key 1                           */
/*                      ConsGroupEntryIn   - Key 2                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeRtrPortEntryCmp (tRBElem * rbElem1, tRBElem * rbElem2)
{
    tSnoopRtrPortEntry *pRtrPortNode1 = rbElem1;
    tSnoopRtrPortEntry *pRtrPortNode2 = rbElem2;
    tSnoopVlanEntry    *pSnoopVlanEntry1 = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry2 = NULL;

    pSnoopVlanEntry1 = pRtrPortNode1->pVlanEntry;
    pSnoopVlanEntry2 = pRtrPortNode2->pVlanEntry;

    if (pRtrPortNode1->u4PhyPortIndex < pRtrPortNode2->u4PhyPortIndex)
    {
        return -1;
    }
    else if (pRtrPortNode1->u4PhyPortIndex > pRtrPortNode2->u4PhyPortIndex)
    {
        return 1;
    }

    /* Compare the Outer-VLAN ID index */
    if (SNOOP_OUTER_VLAN (pSnoopVlanEntry1->VlanId) <
        SNOOP_OUTER_VLAN (pSnoopVlanEntry2->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pSnoopVlanEntry1->VlanId) >
             SNOOP_OUTER_VLAN (pSnoopVlanEntry2->VlanId))
    {
        return 1;
    }

    /* Compare the address type index */
    if (pSnoopVlanEntry1->u1AddressType < pSnoopVlanEntry2->u1AddressType)
    {
        return -1;
    }
    else if (pSnoopVlanEntry1->u1AddressType > pSnoopVlanEntry2->u1AddressType)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeGroupEntryCmp                         */
/*                                                                           */
/* Description        : This routine is used in Group Table for comparing    */
/*                      two keys used in RBTree functionality.               */
/*                                                                           */
/* Input(s)           : GroupEntryNode - Key 1                               */
/*                      GroupEntryIn   - Key 2                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeGroupEntryCmp (tRBElem * GroupEntryNode, tRBElem * GroupEntryIn)
{
    tSnoopGroupEntry   *pGroupEntryNode = NULL;
    tSnoopGroupEntry   *pGroupEntryIn = NULL;
    UINT1               au1GroupIpEntryNode[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GroupIpEntryIn[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (au1GroupIpEntryNode, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1GroupIpEntryIn, 0, IPVX_MAX_INET_ADDR_LEN);

    pGroupEntryNode = (tSnoopGroupEntry *) GroupEntryNode;
    pGroupEntryIn = (tSnoopGroupEntry *) GroupEntryIn;
    /* Compare the Outer-VLAN ID index */
    if (SNOOP_OUTER_VLAN (pGroupEntryNode->VlanId) <
        SNOOP_OUTER_VLAN (pGroupEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pGroupEntryNode->VlanId) >
             SNOOP_OUTER_VLAN (pGroupEntryIn->VlanId))
    {
        return 1;
    }

    /* Compare the address type index */
    if (pGroupEntryNode->GroupIpAddr.u1Afi < pGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return -1;
    }
    else if (pGroupEntryNode->GroupIpAddr.u1Afi >
             pGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return 1;
    }

    SNOOP_MEM_CPY (au1GroupIpEntryNode, pGroupEntryNode->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1GroupIpEntryNode);

    SNOOP_MEM_CPY (au1GroupIpEntryIn,
                   pGroupEntryIn->GroupIpAddr.au1Addr, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1GroupIpEntryIn);

    /* Compare the ip address index */
    if (SNOOP_MEM_CMP (au1GroupIpEntryNode, au1GroupIpEntryIn,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1GroupIpEntryNode, au1GroupIpEntryIn,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    /* Compare the Inner-VLAN ID index */
    if (SNOOP_INNER_VLAN (pGroupEntryNode->VlanId) <
        SNOOP_INNER_VLAN (pGroupEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_INNER_VLAN (pGroupEntryNode->VlanId) >
             SNOOP_INNER_VLAN (pGroupEntryIn->VlanId))
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeMacFwdEntryCmp                        */
/*                                                                           */
/* Description        : This routine is used in MAC Fowarding Table for      */
/*                      comparing two keys,used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : MacFwdEntryNode - Key 1                              */
/*                      MacFwdEntryIn   - Key 2                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeMacFwdEntryCmp (tRBElem * MacFwdEntryNode,
                               tRBElem * MacFwdEntryIn)
{
    tSnoopMacGrpFwdEntry *pMacFwdEntryNode = NULL;
    tSnoopMacGrpFwdEntry *pMacFwdEntryIn = NULL;

    pMacFwdEntryNode = (tSnoopMacGrpFwdEntry *) MacFwdEntryNode;
    pMacFwdEntryIn = (tSnoopMacGrpFwdEntry *) MacFwdEntryIn;

    /* Outer-VLAN ID comparison */
    if (SNOOP_OUTER_VLAN (pMacFwdEntryNode->VlanId)
        < SNOOP_OUTER_VLAN (pMacFwdEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pMacFwdEntryNode->VlanId)
             > SNOOP_OUTER_VLAN (pMacFwdEntryIn->VlanId))
    {
        return 1;
    }

    /* Address type comparison */
    if (pMacFwdEntryNode->u1AddressType < pMacFwdEntryIn->u1AddressType)
    {
        return -1;
    }
    else if (pMacFwdEntryNode->u1AddressType > pMacFwdEntryIn->u1AddressType)
    {
        return 1;
    }

    /* MAC address comparison */
    if (SNOOP_MEM_CMP (pMacFwdEntryNode->MacGroupAddr,
                       pMacFwdEntryIn->MacGroupAddr, sizeof (tMacAddr)) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP
             (pMacFwdEntryNode->MacGroupAddr,
              pMacFwdEntryIn->MacGroupAddr, sizeof (tMacAddr)) > 0)
    {
        return 1;
    }

    /* Inner-VLAN ID comparison */
    if (SNOOP_INNER_VLAN (pMacFwdEntryNode->VlanId)
        < SNOOP_INNER_VLAN (pMacFwdEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_INNER_VLAN (pMacFwdEntryNode->VlanId)
             > SNOOP_INNER_VLAN (pMacFwdEntryIn->VlanId))
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeIpFwdEntryCmp                         */
/*                                                                           */
/* Description        : This routine is used in IP Mcast Forwarding Table    */
/*                      for comparing two keys used in RBTree functionality. */
/*                                                                           */
/* Input(s)           : IpFwdEntryNode - Key 1                               */
/*                      IpFwdEntryIn   - Key 2                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeIpFwdEntryCmp (tRBElem * IpFwdEntryNode, tRBElem * IpFwdEntryIn)
{
    tSnoopIpGrpFwdEntry *pIpFwdEntryNode = NULL;
    tSnoopIpGrpFwdEntry *pIpFwdEntryIn = NULL;
    UINT1               au1GroupIpEntryNode[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1GroupIpEntryIn[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcIpEntryNode[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1SrcIpEntryIn[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (au1GroupIpEntryNode, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1GroupIpEntryIn, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1SrcIpEntryNode, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1SrcIpEntryIn, 0, IPVX_MAX_INET_ADDR_LEN);

    pIpFwdEntryNode = (tSnoopIpGrpFwdEntry *) IpFwdEntryNode;
    pIpFwdEntryIn = (tSnoopIpGrpFwdEntry *) IpFwdEntryIn;

    if (SNOOP_OUTER_VLAN (pIpFwdEntryNode->VlanId) <
        SNOOP_OUTER_VLAN (pIpFwdEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pIpFwdEntryNode->VlanId) >
             SNOOP_OUTER_VLAN (pIpFwdEntryIn->VlanId))
    {
        return 1;
    }

    if (pIpFwdEntryNode->u1AddressType < pIpFwdEntryIn->u1AddressType)
    {
        return -1;
    }
    else if (pIpFwdEntryNode->u1AddressType > pIpFwdEntryIn->u1AddressType)
    {
        return 1;
    }

    SNOOP_MEM_CPY (au1SrcIpEntryNode, pIpFwdEntryNode->SrcIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1SrcIpEntryNode);

    SNOOP_MEM_CPY (au1SrcIpEntryIn, pIpFwdEntryIn->SrcIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1SrcIpEntryIn);

    if (SNOOP_MEM_CMP (au1SrcIpEntryNode, au1SrcIpEntryIn,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1SrcIpEntryNode, au1SrcIpEntryIn,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    SNOOP_MEM_CPY (au1GroupIpEntryNode, pIpFwdEntryNode->GrpIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1GroupIpEntryNode);

    SNOOP_MEM_CPY (au1GroupIpEntryIn, pIpFwdEntryIn->GrpIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1GroupIpEntryIn);

    if (SNOOP_MEM_CMP (au1GroupIpEntryNode, au1GroupIpEntryIn,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1GroupIpEntryNode, au1GroupIpEntryIn,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    if (SNOOP_INNER_VLAN (pIpFwdEntryNode->VlanId) <
        SNOOP_INNER_VLAN (pIpFwdEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_INNER_VLAN (pIpFwdEntryNode->VlanId) >
             SNOOP_INNER_VLAN (pIpFwdEntryIn->VlanId))
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeVlanEntryCmp                          */
/*                                                                           */
/* Description        : This routine is used in VLAN Table for comparing     */
/*                      two keys used in RBTree functionality.               */
/*                                                                           */
/* Input(s)           : VlanEntryNode - Key 1                                */
/*                      VlanEntryIn   - Key 2                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeVlanEntryCmp (tRBElem * VlanEntryNode, tRBElem * VlanEntryIn)
{
    /* compare the vlan index */
    if (SNOOP_OUTER_VLAN (((tSnoopVlanEntry *) VlanEntryNode)->VlanId)
        < SNOOP_OUTER_VLAN (((tSnoopVlanEntry *) VlanEntryIn)->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (((tSnoopVlanEntry *) VlanEntryNode)->VlanId)
             > SNOOP_OUTER_VLAN (((tSnoopVlanEntry *) VlanEntryIn)->VlanId))
    {
        return 1;
    }

    /* Compare the vlan table address type index */
    if (((tSnoopVlanEntry *) VlanEntryNode)->u1AddressType
        < ((tSnoopVlanEntry *) VlanEntryIn)->u1AddressType)
    {
        return -1;
    }
    else if (((tSnoopVlanEntry *) VlanEntryNode)->u1AddressType
             > ((tSnoopVlanEntry *) VlanEntryIn)->u1AddressType)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreePortCfgEntryCmp                       */
/*                                                                           */
/* Description        : This routine is used in Port Conf Table for comparing*/
/*                      two keys used in RBTree functionality.               */
/*                                                                           */
/* Input(s)           : PortCfgEntryNode - Key 1                             */
/*                          PortCfgEntryIn   - Key 2                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreePortCfgEntryCmp (tRBElem * PortCfgEntryNode,
                                tRBElem * PortCfgEntryIn)
{
    tSnoopPortCfgEntry *pPortCfgEntryNode = NULL;
    tSnoopPortCfgEntry *pPortCfgEntryIn = NULL;

    pPortCfgEntryNode = (tSnoopPortCfgEntry *) PortCfgEntryNode;
    pPortCfgEntryIn = (tSnoopPortCfgEntry *) PortCfgEntryIn;

    /* Compare the Port Number index */
    if (pPortCfgEntryNode->u4SnoopPortIndex < pPortCfgEntryIn->u4SnoopPortIndex)
    {
        return -1;
    }
    if (pPortCfgEntryNode->u4SnoopPortIndex > pPortCfgEntryIn->u4SnoopPortIndex)
    {
        return 1;
    }

    /* Compare the Inner vlan Id */
    if (SNOOP_INNER_VLAN (pPortCfgEntryNode->VlanId) <
        SNOOP_INNER_VLAN (pPortCfgEntryIn->VlanId))
    {
        return -1;
    }
    if (SNOOP_INNER_VLAN (pPortCfgEntryNode->VlanId) >
        SNOOP_INNER_VLAN (pPortCfgEntryIn->VlanId))
    {
        return 1;
    }

    /* Compare the address type index */
    if (pPortCfgEntryNode->u1SnoopPortCfgInetAddrtype <
        pPortCfgEntryIn->u1SnoopPortCfgInetAddrtype)
    {
        return -1;
    }
    else if (pPortCfgEntryNode->u1SnoopPortCfgInetAddrtype >
             pPortCfgEntryIn->u1SnoopPortCfgInetAddrtype)
    {
        return 1;
    }

    return 0;

}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeRedMcastEntryCmp                      */
/*                                                                           */
/* Description        : This routine is used in IP Mcast Forwarding Table    */
/*                      for comparing two keys used in RBTree functionality. */
/*                                                                           */
/* Input(s)           : IpFwdEntryNode - Key 1                               */
/*                      IpFwdEntryIn   - Key 2                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeRedMcastEntryCmp (tRBElem * SnoopRedCacheEntryNode,
                                 tRBElem * SnoopRedCacheEntryIn)
{
    tSnoopRedMcastCacheNode *pSnoopRedCacheEntryNode = NULL;
    tSnoopRedMcastCacheNode *pSnoopRedCacheEntryIn = NULL;

    pSnoopRedCacheEntryNode =
        (tSnoopRedMcastCacheNode *) SnoopRedCacheEntryNode;
    pSnoopRedCacheEntryIn = (tSnoopRedMcastCacheNode *) SnoopRedCacheEntryIn;

    if ((pSnoopRedCacheEntryNode->VlanId) < (pSnoopRedCacheEntryIn->VlanId))
    {
        return -1;
    }
    else if ((pSnoopRedCacheEntryNode->VlanId) >
             (pSnoopRedCacheEntryIn->VlanId))
    {
        return 1;
    }

    if (pSnoopRedCacheEntryNode->u4SrcAddr < pSnoopRedCacheEntryIn->u4SrcAddr)
    {
        return -1;
    }

    else if (pSnoopRedCacheEntryNode->u4SrcAddr >
             pSnoopRedCacheEntryIn->u4SrcAddr)
    {
        return 1;
    }

    if (pSnoopRedCacheEntryNode->u4GrpAddr < pSnoopRedCacheEntryIn->u4GrpAddr)
    {
        return -1;
    }

    else if (pSnoopRedCacheEntryNode->u4GrpAddr >
             pSnoopRedCacheEntryIn->u4GrpAddr)
    {
        return 1;
    }

    return 0;
}
#endif

/*****************************************************************************/
/* Function Name      : SnoopPortCfgAddEntry                                 */
/*                                                                           */
/* Description        : This routine add a Port config entry in Instance and */
/*                      Global RBTree                                        */
/* Input(s)           : u4Instance    - Instance number                      */
/*                      pSnoopPortCfgEntry - Port configuration entry        */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopPortCfgAddEntry (UINT4 u4Instance, tSnoopPortCfgEntry * pSnoopPortCfgEntry)
{

    /*Add Instance RBTree for Port configuration  Entries */
    if (RBTreeAdd (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                   pSnoopPortCfgEntry) == RB_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Instance RBTree Addition Failed for Snoop Port Config "
                   "Entry \r\n");
        return RB_FAILURE;
    }

    /*Add Global RBTree for Port configuration Entries  */
    if (RBTreeAdd (SNOOP_PORT_CFG_GLB_RBTREE, pSnoopPortCfgEntry) == RB_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Global RBTree Addition Failed for Snoop Port Config "
                   "Entry \r\n");
        RBTreeRem (SNOOP_PORT_CFG_INST_RBTREE (u4Instance), pSnoopPortCfgEntry);
        return RB_FAILURE;
    }
    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopPortCfgCreateEntry                              */
/*                                                                           */
/* Description        : This routine creates a Port confiiguration entry     */
/*                                                                           */
/* Input(s)           : u4PortIndex    - Port number                         */
/*                      VlanId       - Inner VLAN Identifier            */
/*                      u1AddressType - Indicates Address family v4/v6       */
/*                                                                           */
/* Output(s)          : ppRetSnoopPortCfgEntry - Port configuratno entry     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopPortCfgCreateEntry (UINT4 u4Instance, UINT4 u4PortIndex,
                         tSnoopTag VlanId, UINT1 u1AddressType,
                         tSnoopPortCfgEntry ** ppRetSnoopPortCfgEntry)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (SNOOP_PORT_CFG_ENTRY_ALLOC_MEMBLK (pSnoopPortCfgEntry) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop Port Config entry failed\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "Pool id %d\r\n", SNOOP_PORT_CFG_ENTRY_POOL_ID);
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pSnoopPortCfgEntry, 0, sizeof (tSnoopPortCfgEntry));

    /* Set the default values for the structure members */
    pSnoopPortCfgEntry->u4SnoopPortIndex = u4PortIndex;
    SNOOP_INNER_VLAN (pSnoopPortCfgEntry->VlanId) = SNOOP_INNER_VLAN (VlanId);
    pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype = u1AddressType;

    /* Add Global and Instance RBTree Node for Port configuration */

    if (SnoopPortCfgAddEntry (u4Instance, pSnoopPortCfgEntry) == RB_FAILURE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "RBTree Addition Failed for Snoop Port Config Entry \r\n");
        SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (pSnoopPortCfgEntry);
        return SNOOP_FAILURE;
    }

    pSnoopPortCfgEntry->u4SnoopPortCfgProfileId = SNOOP_PORT_CFG_PROFILE_ID_DEF;
    *ppRetSnoopPortCfgEntry = pSnoopPortCfgEntry;

    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopGetPortCfgEntry                                 */
/*                                                                           */
/* Description        : This routine gets a Port config entry for the        */
/*                      given indices                                        */
/*                                                                           */
/* Input(s)           : u4PortIndex    - Port number                         */
/*                      VlanId         - Inner VLAN Identifier               */
/*                      u1AddressType  - Indicates Address family v4/v6      */
/*                      u4Instance     - Insantce number                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopGetPortCfgEntry (UINT4 u4Instance, UINT4 u4PortIndex, tSnoopTag VlanId,
                      UINT1 u1AddressType, tSnoopPortCfgEntry
                      ** ppSnoopPortCfgEntry)
{
    tSnoopPortCfgEntry  SnoopPortCfgEntry;
    tRBElem            *pRBElem = NULL;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (&SnoopPortCfgEntry, 0, sizeof (tSnoopPortCfgEntry));
    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) = SNOOP_INNER_VLAN (VlanId);
    }
    else
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) =
            SNOOP_INNER_VLAN (InnerVlanId);
    }
    SnoopPortCfgEntry.u4SnoopPortIndex = u4PortIndex;
    SnoopPortCfgEntry.u1SnoopPortCfgInetAddrtype = u1AddressType;

    pRBElem = RBTreeGet (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                         (tRBElem *) & SnoopPortCfgEntry);
    if (pRBElem == NULL)
    {
        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                        "Port config entry not found ,in Snoop Port Config"
                        "Entry table, for Port  %d\r\n", u4PortIndex);
        *ppSnoopPortCfgEntry = NULL;
        return SNOOP_FAILURE;
    }
    else
    {
        *ppSnoopPortCfgEntry = (tSnoopPortCfgEntry *) pRBElem;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopPortCfgDeleteEntry                              */
/* Description        : This routine deletes a Port config entry for the     */
/*                        the given indices                               */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance number                     */
/*                      u4PortIndex    - Port number                         */
/*                      VlanId         - Inner VLAN Identifier               */
/*                      u1AddressType  - Indicates Address family v4/v6      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/****************************************************************************/

VOID
SnoopPortCfgDeleteEntry (UINT4 u4Instance, UINT4 u4PortIndex,
                         tSnoopTag VlanId, UINT1 u1AddressType)
{
    SNOOP_VALIDATE_INSTANCE (u4Instance);
    /* Delete port configuration entry from instance based RBTree  */
    SnoopPortCfgInsDeleteEntry (u4Instance, u4PortIndex, VlanId, u1AddressType);
    /* Delete port configuration entry from Global RBTree  */
    SnoopPortCfgGlbDeleteEntry (u4Instance, u4PortIndex, VlanId, u1AddressType);
    return;

}

/*****************************************************************************/
/* Function Name      : SnoopPortCfgInsDeleteEntry                           */
/* Description        : This routine deletes a Port config entry in Instance */
/*                      RBTree for the given indices                         */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance number                     */
/*                        u4PortIndex    - Port number                       */
/*                        VlanId    - Inner VLAN Identifier                  */
/*                      u1AddressType  - Indicates Address family v4/v6      */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/****************************************************************************/
VOID
SnoopPortCfgInsDeleteEntry (UINT4 u4Instance, UINT4 u4PortIndex,
                            tSnoopTag VlanId, UINT1 u1AddressType)
{
    tSnoopPortCfgEntry  SnoopPortCfgEntry;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) = SNOOP_INNER_VLAN (VlanId);
    }
    else
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) =
            SNOOP_INNER_VLAN (InnerVlanId);
    }

    SnoopPortCfgEntry.u4SnoopPortIndex = u4PortIndex;
    SnoopPortCfgEntry.u1SnoopPortCfgInetAddrtype = u1AddressType;

    pSnoopPortCfgEntry = (tSnoopPortCfgEntry *)
        RBTreeGet (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                   (tRBElem *) & SnoopPortCfgEntry);

    SNOOP_CHK_NULL_PTR (pSnoopPortCfgEntry);

    if (SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) != NULL)
    {
        SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK (SNOOP_PORT_CFG_EXT_ENTRY
                                              (pSnoopPortCfgEntry));
        SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) = NULL;
    }

    RBTreeRem (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
               (tRBElem *) pSnoopPortCfgEntry);
    SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (pSnoopPortCfgEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopPortCfgGlbDeleteEntry                           */
/* Description        : This routine deletes Port config entry in the Global */
/*                        RBTree for the given indices                       */
/*                                                                           */
/* Input(s)           : u4Instance     - Instance number                     */
/*                      u4PortIndex    - Port number                         */
/*                      VlanId         - Inner VLAN Identifier               */
/*                      u1AddressType  - Indicates Address family v4/v6      */
/*                                                                           */
/* Return Value(s)    : NOne                                                 */
/****************************************************************************/
VOID
SnoopPortCfgGlbDeleteEntry (UINT4 u4Instance, UINT4 u4PortIndex,
                            tSnoopTag VlanId, UINT1 u1AddressType)
{
    tSnoopPortCfgEntry  SnoopPortCfgEntry;
    tSnoopPortCfgEntry *pSnoopPortCfgEntry;
    tSnoopTag           InnerVlanId;

    SNOOP_MEM_SET (InnerVlanId, 0, sizeof (tSnoopTag));

    if (SNOOP_INSTANCE_ENH_MODE (u4Instance) == SNOOP_ENABLE)
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) = SNOOP_INNER_VLAN (VlanId);
    }
    else
    {
        SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) =
            SNOOP_INNER_VLAN (InnerVlanId);
    }

    SnoopPortCfgEntry.u4SnoopPortIndex = u4PortIndex;
    SnoopPortCfgEntry.u1SnoopPortCfgInetAddrtype = u1AddressType;

    pSnoopPortCfgEntry = (tSnoopPortCfgEntry *)
        RBTreeGet (SNOOP_PORT_CFG_GLB_RBTREE, (tRBElem *) & SnoopPortCfgEntry);

    SNOOP_CHK_NULL_PTR (pSnoopPortCfgEntry);

    if (SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) != NULL)
    {
        SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK (SNOOP_PORT_CFG_EXT_ENTRY
                                              (pSnoopPortCfgEntry));
        SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) = NULL;
    }

    RBTreeRem (SNOOP_PORT_CFG_GLB_RBTREE, (tRBElem *) pSnoopPortCfgEntry);
    SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (pSnoopPortCfgEntry);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopPortCfgDeleteEntries                            */
/*                                                                           */
/* Description        : This function deletes all the Port config related    */
/*                      information when the module is disable               */
/*                                                                           */
/* Input(s)           : u4Instance    -  Instance number                     */
/*                      u1AddressType -  Address Type (IGMP/MLD)             */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopPortCfgDeleteEntries (UINT4 u4Instance, UINT1 u1AddressType)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;

    if (SNOOP_PORT_CFG_INST_RBTREE (u4Instance) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "No Port config entries found for this instance\r\n");
        return;
    }
    pRBElem = RBTreeGetFirst (SNOOP_PORT_CFG_INST_RBTREE (u4Instance));

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                           pRBElem, NULL);

        pSnoopPortCfgEntry = (tSnoopPortCfgEntry *) pRBElem;

        /*  Delete the RB Node from the instance and global RB Tree
         *  for the corresponding address type
         */
        if ((u1AddressType != SNOOP_ADDR_TYPE_IPV4) &&
            (u1AddressType != pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype))
        {
            continue;
        }

        /* Give an indication to TAC module if profile Id is configured */
        if (pSnoopPortCfgEntry->u4SnoopPortCfgProfileId !=
            SNOOP_PORT_CFG_PROFILE_ID_DEF)
        {
            SnoopTacApiUpdatePortRefCount
                (pSnoopPortCfgEntry->u4SnoopPortCfgProfileId,
                 pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype, TACM_UNMAP);
        }

        if (SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) != NULL)
        {
            SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK (SNOOP_PORT_CFG_EXT_ENTRY
                                                  (pSnoopPortCfgEntry));
            SNOOP_PORT_CFG_EXT_ENTRY (pSnoopPortCfgEntry) = NULL;
        }

        RBTreeRem (SNOOP_PORT_CFG_GLB_RBTREE, (tRBElem *) pSnoopPortCfgEntry);

        RBTreeRem (SNOOP_PORT_CFG_INST_RBTREE (u4Instance),
                   (tRBElem *) pSnoopPortCfgEntry);

        SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (pSnoopPortCfgEntry);
        pRBElem = pRBNextElem;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopSystemModeChange                                */
/*                                                                       */
/* Description        : This routine is used delete port config, group and   */
/*                      IP fwd entries                                       */
/*                                                                           */
/* Input(s)           : i4Instance   - Instance number                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopSystemModeChange (UINT4 u4Instance)
{
    SNOOP_VALIDATE_INSTANCE (u4Instance);
    SnoopPortCfgDeleteEntries (u4Instance, SNOOP_ADDR_TYPE_IPV4);
    SnoopFwdDeleteMcastFwdEntries (u4Instance, SNOOP_ADDR_TYPE_IPV4);
    SnoopGrpDeleteGroupEntries (u4Instance, SNOOP_ADDR_TYPE_IPV4);
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreePortCfgEntryFree                      */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                       allocated for Port Conf table.                      */
/*                                                                           */
/* Input(s)           : PortCfgEntryNode - Pointer to Port config table.     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreePortCfgEntryFree (tRBElem * PortCfgEntryNode, UINT4 u4Arg)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;

    UNUSED_PARAM (u4Arg);
    pSnoopPortCfgEntry = (tSnoopPortCfgEntry *) PortCfgEntryNode;

    if (PortCfgEntryNode != NULL)
    {
        RBTreeRemove (SNOOP_PORT_CFG_GLB_RBTREE, PortCfgEntryNode);
        SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK (SNOOP_PORT_CFG_EXT_ENTRY
                                              (pSnoopPortCfgEntry));
        SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK (PortCfgEntryNode);
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeConsGroupEntryFree                    */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                       allocated for consolidated Group table.             */
/*                                                                           */
/* Input(s)           : ConsGroupEntryNode - Pointer to Consolidated Group   */
/*                                           table.                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeConsGroupEntryFree (tRBElem * ConsGroupEntryNode, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);

    if (ConsGroupEntryNode != NULL)
    {
        SNOOP_VLAN_CONS_GRP_FREE_MEMBLK (ConsGroupEntryNode);
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeGroupEntryFree                        */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                       allocated for Group table.                          */
/*                                                                           */
/* Input(s)           : GroupEntryNode - Pointer to Group table.             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeGroupEntryFree (tRBElem * GroupEntryNode, UINT4 u4Arg)
{
    UINT4               u4Instance = 0;

    UNUSED_PARAM (u4Arg);

    u4Instance = gu1SnoopInstanceId;
    if (GroupEntryNode != NULL)
    {
        SNOOP_VLAN_GRP_FREE_MEMBLK (u4Instance, GroupEntryNode);
    }
    UNUSED_PARAM (u4Instance);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeVlanEntryFree                         */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for VLAN table.                            */
/*                                                                           */
/* Input(s)           : VlanEntryNode - Pointer to VLAN table.               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeVlanEntryFree (tRBElem * VlanEntryNode, UINT4 u4Arg)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = (tSnoopVlanEntry *) (VOID *)
        VlanEntryNode;
    UINT4               u4Instance = 0;

    UNUSED_PARAM (u4Arg);

    u4Instance = gu1SnoopInstanceId;

    if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
    {
        SNOOP_VLAN_CFG_FREE_MEMBLK (u4Instance,
                                    pSnoopVlanEntry->pSnoopVlanCfgEntry);
        pSnoopVlanEntry->pSnoopVlanCfgEntry = NULL;
    }

    SNOOP_VLAN_ENTRY_FREE_MEMBLK (u4Instance, VlanEntryNode);

    UNUSED_PARAM (u4Instance);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeMacFwdEntryFree                       */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for MAC forward table.                     */
/*                                                                           */
/* Input(s)           : MacFwdEntryNode - Pointer to MAC forward table.      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeMacFwdEntryFree (tRBElem * MacFwdEntryNode, UINT4 u4Arg)
{
    UINT4               u4Instance = 0;

    UNUSED_PARAM (u4Arg);

    u4Instance = gu1SnoopInstanceId;
    SNOOP_VLAN_MAC_FWD_FREE_MEMBLK (u4Instance, MacFwdEntryNode);

    UNUSED_PARAM (u4Instance);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeIpFwdEntryFree                        */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for IP forward table.                      */
/*                                                                           */
/* Input(s)           : IpFwdEntryNode - Pointer to IP forward table.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeIpFwdEntryFree (tRBElem * IpFwdEntryNode, UINT4 u4Arg)
{
    UINT4               u4Instance = 0;

    UNUSED_PARAM (u4Arg);

    u4Instance = gu1SnoopInstanceId;
    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, IpFwdEntryNode);

    UNUSED_PARAM (u4Instance);
    return SNOOP_SUCCESS;
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeRedFwdEntryFree                       */
/*                                                                           */
/* Description        : This routine is used for releasing the Memory        */
/*                      allocated for RED IP forward table.                  */
/*                                                                           */
/* Input(s)           : IpFwdEntryNode - Pointer to IP forward table.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT4
SnoopUtilRBTreeRedFwdEntryFree (tRBElem * IpRedEntryNode, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    SNOOP_RED_GRP_FREE_MEMBLK (gu1SnoopInstanceId, IpRedEntryNode);

    return SNOOP_SUCCESS;
}
#endif
/*****************************************************************************/
/* Function Name      : SnoopUtilGetModuleeNameTrace                         */
/*                                                                           */
/* Description        : This function returns the module name corresponding  */
/*                      to the trace flag                                    */
/*                                                                           */
/* Input(s)           : u2TrcModulee - Trace Module                          */
/*                                                                           */
/* Output(s)          : PortBitmap - Updated port bitmap                     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
CONST CHR1         *
SnoopUtilGetModuleNameTrace (UINT4 u4TraceModule)
{
    if (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId) == NULL)
    {
        return NULL;
    }
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->au1SnoopTrcMode, 0,
                   SNOOP_TRC_NAME_SIZE);

    switch (u4TraceModule)
    {
        case SNOOP_TRC_DATA:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-DATA", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_TRC_DATA;
            break;
        case SNOOP_TRC_CONTROL:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-CTRL", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_TRC_CONTROL;
            break;
        case SNOOP_TRC_Tx:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-Tx", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_TRC_Tx;
            break;
        case SNOOP_TRC_Rx:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-Rx", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_TRC_Rx;
            break;
        default:
            SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                           au1SnoopTrcMode, 0, SNOOP_TRC_NAME_SIZE);
            break;
    }

    return ((const char *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
            au1SnoopTrcMode);
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetModuleeName                              */
/*                                                                           */
/* Description        : This function returns the module name corresponding  */
/*                      to the trace flag                                    */
/*                                                                           */
/* Input(s)           : u2TrcModulee - Trace Module                           */
/*                                                                           */
/* Output(s)          : PortBitmap - Updated port bitmap                     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
CONST CHR1         *
SnoopUtilGetModuleName (UINT4 u4TraceModule)
{
    if (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId) == NULL)
    {
        return NULL;
    }
    SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->au1SnoopTrcMode, 0,
                   SNOOP_TRC_NAME_SIZE);

    switch (u4TraceModule)
    {
        case SNOOP_DBG_INIT:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-INIT", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_INIT;
            break;

        case SNOOP_DBG_RESRC:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-RESRC", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_RESRC;
            break;

        case SNOOP_DBG_TMR:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-TMR", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_TMR;
            break;

        case SNOOP_DBG_SRC:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-SRC", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_SRC;
            break;

        case SNOOP_DBG_GRP:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-GRP", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_GRP;
            break;

        case SNOOP_DBG_QRY:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-QRY", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_QRY;
            break;

        case SNOOP_DBG_VLAN:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-VLAN", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_VLAN;
            break;

        case SNOOP_DBG_PKT:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-PKT", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_PKT;
            break;

        case SNOOP_DBG_FWD:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-FWD", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_FWD;
            break;

        case SNOOP_DBG_MGMT:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-MGMT", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_MGMT;
            break;

        case SNOOP_DBG_RED:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-RED", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_RED;
            break;

        case SNOOP_DBG_ENTRY:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-ENTRY", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_ENTRY;
            break;
        case SNOOP_DBG_EXIT:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-EXIT", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_EXIT;
            break;

        case SNOOP_DBG_NP:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-NP", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_NP;
            break;

        case SNOOP_DBG_BUFFER:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-BUFFER",
                      SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_BUFFER;
            break;
        case SNOOP_DBG_ICCH:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-ICCH", SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_ICCH;
            break;
        case SNOOP_DBG_ALL_FAILURE:
            SNPRINTF ((CHR1 *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                      au1SnoopTrcMode, SNOOP_TRC_NAME_SIZE,
                      "SNOOP:%s-FAILURE",
                      SNOOP_INSTANCE_STR (gu4SnoopTrcInstId));
            gu4SnoopTrcModule = SNOOP_DBG_ALL_FAILURE;
            break;

        default:
            SNOOP_MEM_SET (SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
                           au1SnoopTrcMode, 0, SNOOP_TRC_NAME_SIZE);
            break;
    }

    return ((const char *) SNOOP_INSTANCE_INFO (gu4SnoopTrcInstId)->
            au1SnoopTrcMode);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilDuplicateBuffer                         */
/*                                                                           */
/*    Description         : This function takes care of duplicating the      */
/*                          specified chain buffer. This function allocates  */
/*                          a new chain buffer of the size equal to the size */
/*                          of the given buffer and copies the data to the   */
/*                          newly allocated buffer.                          */
/*                                                                           */
/*    Input(s)            : pSrcBuf - Pointer to the source buffer.          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to the duplicate buffer OR NULL          */
/*                                                                           */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
SnoopUtilDuplicateBuffer (tCRU_BUF_CHAIN_HEADER * pSrcBuf)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4PktSize = 0;
    UINT1               u1Releaseflag = 0;

    u4PktSize = CRU_BUF_Get_ChainValidByteCount (pSrcBuf);

    pDupBuf = CRU_BUF_Allocate_MsgBufChain
        (u4PktSize + (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN), 0);

    if (pDupBuf == NULL)
    {
        return NULL;
    }

    SNOOP_MEM_SET (gpSnoopDataBuffer, 0, SNOOP_DATA_MEMBLK_SIZE);

    if (CRU_BUF_Copy_FromBufChain (pSrcBuf, (UINT1 *) gpSnoopDataBuffer, 0,
                                   u4PktSize) == CRU_FAILURE)
    {
        u1Releaseflag = 1;
    }

    if (CRU_BUF_Copy_OverBufChain (pDupBuf, (UINT1 *) gpSnoopDataBuffer,
                                   SNOOP_DATA_LINK_HDR_LEN, u4PktSize)
        == CRU_FAILURE)
    {
        u1Releaseflag = 1;
    }

    CRU_BUF_Move_ValidOffset (pDupBuf, SNOOP_DATA_LINK_HDR_LEN);

    if (u1Releaseflag == 1)
    {
        CRU_BUF_Release_MsgBufChain (pDupBuf, FALSE);
        return NULL;
    }

    return pDupBuf;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateSendSrcInfo                           */
/*                                                                           */
/* Description        : This function will updates the list of sources       */
/*                      which needs to be send to the router                 */
/*                                                                           */
/* Input(s)           : u1RecordType  - Record type of the message received  */
/*                      u1OldFilterMode - Old Filter mode                    */
/*                      OldInclSrcBmp - Old Include source bitmap            */
/*                      OldExclSrcBmp - Old Exclude source bitmap            */
/*                      pSnoopGroupEntry - pointer to the group entry        */
/*                                                                           */
/* global variables   : gpSSMSrcInfoBuffer - used to stores the sources      */
/*                                          received in the report message   */
/*                      gu4SendRecType    - Send record type                 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
VOID
SnoopUtilUpdateSendSrcInfo (UINT4 u4Instance, UINT1 u1RecordType,
                            UINT1 u1OldFilterMode,
                            tSnoopSrcBmp OldInclSrcBmp,
                            tSnoopSrcBmp OldExclSrcBmp,
                            tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry)
{
    tSnoopSrcBmp        TempSrcBmp;
    tSnoopSrcBmp        TempExclSrcBmp;
    tSnoopSrcBmp        TempInclSrcBmp;
#ifdef IGS_WANTED
    UINT4               u4SrcAddr = 0;
#endif
    UINT2               u2SendRecType = 0;
    UINT2               u2Count = 0;
    UINT2               u2SrcCount = 0;
    UINT1               u1Offset = 0;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    SNOOP_MEM_SET (TempSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempExclSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (TempInclSrcBmp, 0, SNOOP_SRC_LIST_SIZE);
    /* Get the record type for the group record that needs to be sent */
    SnoopUtilUpdateRecordType (u1RecordType, u1OldFilterMode, OldInclSrcBmp,
                               OldExclSrcBmp, pSnoopConsGroupEntry);

    SNOOP_VALIDATE_INSTANCE (u4Instance);

    if (gu4SendRecType == 0)
    {
        return;
    }

    /* Get the type of record for the group that needs to be sent */
    /* The record type is obtained from the lower 2 bytes of gu4SendRecType */
    u2SendRecType = (UINT2) (gu4SendRecType & 0xffff);

    switch (u2SendRecType)
    {
        case SNOOP_TO_INCLUDE:
            /* If the record type is TO_INCL just copy the sources
             * from the Include source bitmap of the group entry */
            SNOOP_MEM_CPY (TempSrcBmp, pSnoopConsGroupEntry->InclSrcBmp,
                           SNOOP_SRC_LIST_SIZE);
            break;

        case SNOOP_TO_EXCLUDE:
            /* If the record type is TO_EXCL just copy the sources
             * from the Exclude source bitmap of the group entry */
            SNOOP_MEM_CPY (TempSrcBmp, pSnoopConsGroupEntry->ExclSrcBmp,
                           SNOOP_SRC_LIST_SIZE);
            break;

        case SNOOP_ALLOW:
            /* If the record type is ALLOW just copy the changed sources
             * (newly added) in the include source bitmap of the group entry */
            if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
                SNOOP_MEM_CPY (TempInclSrcBmp, OldInclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_NEG_SRC_BMP (TempInclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, pSnoopConsGroupEntry->InclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp, TempInclSrcBmp);
            }
            else
            {
                /* If the record type is ALLOW  and filter mode is EXCLUDE 
                 * just copy the sources which have been deleted from the 
                 * exclude source bitmap for the group */
                SNOOP_MEM_CPY (TempExclSrcBmp, pSnoopConsGroupEntry->ExclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_NEG_SRC_BMP (TempExclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, OldExclSrcBmp, SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp, TempExclSrcBmp);
            }
            break;

        case SNOOP_BLOCK:
            /* If the record type is BLOCK  and filter mode is INCLUDE 
             * just copy the sources which have been removed from the include
             * source bitmap for the group */
            if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
                SNOOP_MEM_CPY (TempExclSrcBmp, pSnoopConsGroupEntry->InclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_NEG_SRC_BMP (TempExclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, TempExclSrcBmp, SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp, OldInclSrcBmp);
            }
            else
            {
                /* If the record type is BLOCK  and filter mode is EXCLUDE 
                 * just copy the sources which have been added to the exclude
                 * source bitmap for the group */
                SNOOP_MEM_CPY (TempExclSrcBmp, OldExclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_NEG_SRC_BMP (TempExclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, pSnoopConsGroupEntry->ExclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp, TempExclSrcBmp);
            }
            break;

        default:
            break;
    }

    /* TempSrcBmp contains the list of sources that needs to be sent 
     * to the router , so fill the Buffer with the source information */
    SNOOP_MEM_SET (gpSSMSrcInfoBuffer, 0, SNOOP_SRC_MEMBLK_SIZE);

    /* If the final source bitmap is NULL no need to fill anything */
    if (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE) != 0)
    {
        for (u2Count = 1; u2Count < (SNOOP_SRC_LIST_SIZE * 8); u2Count++)
        {
            OSIX_BITLIST_IS_BIT_SET (TempSrcBmp, u2Count,
                                     SNOOP_SRC_LIST_SIZE, bResult);

            if (bResult == OSIX_TRUE)
            {
#ifdef IGS_WANTED
                if (pSnoopConsGroupEntry->GroupIpAddr.u1Afi ==
                    SNOOP_ADDR_TYPE_IPV4)
                {
                    if (u2Count <= SNOOP_MAX_MCAST_SRCS)
                    {
                        u4SrcAddr =
                            SNOOP_HTONL (SNOOP_PTR_FETCH_4
                                         (SNOOP_SRC_INFO (u4Instance,
                                                          (u2Count -
                                                           1)).SrcIpAddr.
                                          au1Addr));

                        if (pSnoopConsGroupEntry->u1ASMHostPresent ==
                            SNOOP_FALSE)
                        {
                            SNOOP_MEM_CPY (gpSSMSrcInfoBuffer + u1Offset,
                                           &u4SrcAddr, SNOOP_IP_ADDR_SIZE);
                            u1Offset += SNOOP_IP_ADDR_SIZE;
                            u2SrcCount++;
                        }
                    }
                }
#endif
#ifdef MLDS_WANTED
                if (pSnoopConsGroupEntry->GroupIpAddr.u1Afi ==
                    SNOOP_ADDR_TYPE_IPV6)
                {
                    if (pSnoopConsGroupEntry->u1ASMHostPresent == SNOOP_FALSE)
                    {
                        if (u2Count <= SNOOP_MAX_MCAST_SRCS)
                        {
                            SNOOP_MEM_CPY (au1SrcAddr,
                                           SNOOP_SRC_INFO (u4Instance,
                                                           (u2Count -
                                                            1)).SrcIpAddr.
                                           au1Addr, IPVX_IPV6_ADDR_LEN);

                            SNOOP_INET_HTONL (au1SrcAddr);

                            SNOOP_MEM_CPY (gpSSMSrcInfoBuffer + u1Offset,
                                           au1SrcAddr, IPVX_IPV6_ADDR_LEN);
                            u1Offset += IPVX_IPV6_ADDR_LEN;
                            u2SrcCount++;
                        }
                    }
                }
#endif
            }
        }
    }

    gu4SendRecType |= (u2SrcCount << 16);
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateRecordType                            */
/*                                                                           */
/* Description        : This function will updates the record type           */
/*                      which needs to be send to the router                 */
/*                                                                           */
/* Input(s)           : u1RecordType  - Record type of the message received  */
/*                      u1OldFilterMode - Old Filter mode                    */
/*                      OldInclSrcBmp - Old Include source bitmap            */
/*                      OldExclSrcBmp - Old Exclude source bitmap            */
/*                      pSnoopGroupEntry - pointer to the group entry        */
/*                                                                           */
/* global variables   : gu4SendRecType -  Send record type                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    :SNOOP_SUCCESS/ SNOOP_FAILURE                          */
/*****************************************************************************/
VOID
SnoopUtilUpdateRecordType (UINT1 u1RecordType, UINT1 u1OldFilterMode,
                           tSnoopSrcBmp OldInclSrcBmp,
                           tSnoopSrcBmp OldExclSrcBmp,
                           tSnoopConsolidatedGroupEntry * pSnoopConsGroupEntry)
{
    tSnoopSrcBmp        TempInclSrcBmp;
    tSnoopSrcBmp        TempExclSrcBmp;
    tSnoopSrcBmp        TempSrcBmp;
    tSnoopSrcBmp        SnoopExclBmp;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);

    gu4SendRecType = 0;

    /* Check if the filter mode for the group record 
     * has changed or not */
    if (u1OldFilterMode != pSnoopConsGroupEntry->u1FilterMode)
    {
        gu4SendRecType =
            (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE) ?
            SNOOP_TO_INCLUDE : SNOOP_TO_EXCLUDE;
    }
    else
    {
        if (pSnoopConsGroupEntry->u1FilterMode == SNOOP_INCLUDE)
        {
            /* Check if any new sources have been appended with the 
             * existing include source bitmap then the send record 
             * type should be ALLOW and if any old sources have been 
             * deleted from the existing source list, send record 
             * type should be BLOCK else we need to send TO_INCL */

            if (u1RecordType == SNOOP_HOST_TIMER_EXPIRY)
            {
                /* Get the difference between the old and new 
                 * source bitmap for the group entry */
                SNOOP_MEM_CPY (TempInclSrcBmp, OldInclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_XOR_SRC_BMP (TempInclSrcBmp,
                                   pSnoopConsGroupEntry->InclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, TempInclSrcBmp, SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp,
                                   pSnoopConsGroupEntry->InclSrcBmp);
                SNOOP_AND_SRC_BMP (TempInclSrcBmp, OldInclSrcBmp);

                /* If the (result & old == 0) && (result & new != 0) 
                 * new source has been added so send an ALLOW for new 
                 * sources */
                if ((SNOOP_MEM_CMP (TempInclSrcBmp, gNullSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) == 0) &&
                    (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_ALLOW;
                }
                /* If the (result & old != 0) && (result & new == 0) 
                 * old  source has been deletd so send an BLOCK for 
                 * old sources */
                else if ((SNOOP_MEM_CMP (TempInclSrcBmp, gNullSrcBmp,
                                         SNOOP_SRC_LIST_SIZE) != 0) &&
                         (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0))
                {
                    gu4SendRecType = SNOOP_BLOCK;
                }
                /* If the (result & old == 0) && (result & new == 0) 
                 * then there is no change in the source list */
                else if ((SNOOP_MEM_CMP (TempInclSrcBmp, gNullSrcBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0) &&
                         (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0))
                {
                    gu4SendRecType = 0;
                }
                /* If the (result & old != 0) && (result & new != 0)   
                 * both deletion and addition of sources has been made 
                 * so send a TO_INCL for the new list of sources */
                else
                {
                    gu4SendRecType = SNOOP_TO_INCLUDE;
                }
            }

            else if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                     (u1RecordType == SNOOP_IS_INCLUDE))
            {
                if ((SNOOP_MEM_CMP (OldInclSrcBmp,
                                    pSnoopConsGroupEntry->InclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) == 0) &&
                    (SNOOP_MEM_CMP (OldInclSrcBmp, gNullSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = 0;
                }
                else
                {
                    /* when there is only one host exists on a port, and the 
                     * same host has sent TO_INCLUDE, so it should be 
                     * considerd as fresh registration */
                    if (gu1IsSameHost == SNOOP_SAME_HOST)
                    {
                        gu4SendRecType = SNOOP_TO_INCLUDE;
                    }
                    else
                    {
                        /* Get the difference between the old and new 
                         * source bitmap for the group entry */
                        SNOOP_MEM_CPY (TempInclSrcBmp, OldInclSrcBmp,
                                       SNOOP_SRC_LIST_SIZE);
                        SNOOP_XOR_SRC_BMP (TempInclSrcBmp,
                                           pSnoopConsGroupEntry->InclSrcBmp);
                        SNOOP_MEM_CPY (TempSrcBmp, TempInclSrcBmp,
                                       SNOOP_SRC_LIST_SIZE);
                        SNOOP_AND_SRC_BMP (TempSrcBmp,
                                           pSnoopConsGroupEntry->InclSrcBmp);
                        SNOOP_AND_SRC_BMP (TempInclSrcBmp, OldInclSrcBmp);

                        /* If the (result & old == 0) && (result & new != 0) 
                         * new source has been added so send an ALLOW for new 
                         * sources */
                        if ((SNOOP_MEM_CMP (TempInclSrcBmp, gNullSrcBmp,
                                            SNOOP_SRC_LIST_SIZE) == 0) &&
                            (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                            SNOOP_SRC_LIST_SIZE) != 0))
                        {
                            gu4SendRecType = SNOOP_ALLOW;
                        }
                        /* If the (result & old != 0) && (result & new == 0) 
                         * old  source has been deletd so send an BLOCK for 
                         * old sources */
                        else if ((SNOOP_MEM_CMP (TempInclSrcBmp, gNullSrcBmp,
                                                 SNOOP_SRC_LIST_SIZE) != 0) &&
                                 (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                                 SNOOP_SRC_LIST_SIZE) == 0))
                        {
                            gu4SendRecType = SNOOP_BLOCK;
                        }
                        /* If the (result & old != 0) && (result & new != 0)   
                         * both deletion and addition of sources has been made 
                         * so send a TO_INCL for the new list of sources */
                        else
                        {
                            gu4SendRecType = SNOOP_TO_INCLUDE;
                        }
                    }
                }
            }
            else if (u1RecordType == SNOOP_ALLOW)
            {
                /* Since the old include source bitmap is not equal to the
                 * new include source bitmap send an ALLOW for the 
                 * new sources */
                if ((SNOOP_MEM_CMP (OldInclSrcBmp,
                                    pSnoopConsGroupEntry->InclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_ALLOW;
                }
            }
            else if (u1RecordType == SNOOP_BLOCK)
            {
                /* Since the old include source bitmap is not equal to the
                 * new include source bitmap send an BLOCK for the 
                 * old sources */
                if ((SNOOP_MEM_CMP (OldInclSrcBmp,
                                    pSnoopConsGroupEntry->InclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_BLOCK;
                }
            }
        }
        else
        {
            if (u1RecordType == SNOOP_HOST_TIMER_EXPIRY)
            {
                /* Get the difference between the old and new 
                 * source bitmap for the group entry */
                SNOOP_MEM_CPY (TempExclSrcBmp, OldExclSrcBmp,
                               SNOOP_SRC_LIST_SIZE);
                SNOOP_XOR_SRC_BMP (TempExclSrcBmp,
                                   pSnoopConsGroupEntry->ExclSrcBmp);
                SNOOP_MEM_CPY (TempSrcBmp, TempExclSrcBmp, SNOOP_SRC_LIST_SIZE);
                SNOOP_AND_SRC_BMP (TempSrcBmp,
                                   pSnoopConsGroupEntry->ExclSrcBmp);
                if (SNOOP_MEM_CMP
                    (TempSrcBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE) == 0)
                {
                    SNOOP_MEM_CPY (TempSrcBmp, SnoopExclBmp,
                                   SNOOP_SRC_LIST_SIZE);
                }

                SNOOP_AND_SRC_BMP (TempExclSrcBmp, OldExclSrcBmp);
                if (SNOOP_MEM_CMP (TempExclSrcBmp, gNullSrcBmp,
                                   SNOOP_SRC_LIST_SIZE) == 0)
                {
                    SNOOP_MEM_CPY (TempExclSrcBmp, SnoopExclBmp,
                                   SNOOP_SRC_LIST_SIZE);
                }

                /* If the (result & old == 0) && (result & new != 0) 
                 * new source has been added so send an BLOCK for 
                 * new sources */
                if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                    SNOOP_SRC_LIST_SIZE) == 0) &&
                    (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_BLOCK;
                }
                /* If the (result & old != 0) && (result & new == 0) 
                 * old source has been deletd so send an ALLOW for 
                 * old sources */
                else if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                         SNOOP_SRC_LIST_SIZE) != 0) &&
                         (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0))
                {
                    gu4SendRecType = SNOOP_ALLOW;
                }
                /* If the (result & old == 0) && (result & new == 0) 
                 * then there is no change in the source list */
                else if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0) &&
                         (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                         SNOOP_SRC_LIST_SIZE) == 0))
                {
                    gu4SendRecType = 0;
                }
                /* If the (result & old != 0) && (result & new != 0)   
                 * both deletion and addition of sources has been made 
                 * so send a TO_EXCL for the new list of sources */
                else
                {
                    gu4SendRecType = SNOOP_TO_EXCLUDE;
                }

            }
            else if ((u1RecordType == SNOOP_TO_EXCLUDE) ||
                     (u1RecordType == SNOOP_IS_EXCLUDE))
            {
                /* No change in bitmap, so no need to send any 
                 * thing to router */
                if ((SNOOP_MEM_CMP (OldExclSrcBmp,
                                    pSnoopConsGroupEntry->ExclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) == 0) &&
                    (SNOOP_MEM_CMP (OldExclSrcBmp, SnoopExclBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = 0;
                }
                else
                {
                    /* when there is only one host exists on a port, and the 
                     * same host has sent TO_EXCLUDE, so it should be 
                     * considerd as fresh registration */
                    if (gu1IsSameHost == SNOOP_SAME_HOST)
                    {
                        gu4SendRecType = SNOOP_TO_EXCLUDE;
                    }
                    else
                    {
                        /* Get the difference between the old and new 
                         * source bitmap for the group entry */
                        SNOOP_MEM_CPY (TempExclSrcBmp, OldExclSrcBmp,
                                       SNOOP_SRC_LIST_SIZE);
                        SNOOP_XOR_SRC_BMP (TempExclSrcBmp,
                                           pSnoopConsGroupEntry->ExclSrcBmp);
                        SNOOP_MEM_CPY (TempSrcBmp, TempExclSrcBmp,
                                       SNOOP_SRC_LIST_SIZE);
                        SNOOP_AND_SRC_BMP (TempSrcBmp,
                                           pSnoopConsGroupEntry->ExclSrcBmp);
                        if (SNOOP_MEM_CMP
                            (TempSrcBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE) == 0)
                        {
                            SNOOP_MEM_CPY (TempSrcBmp, SnoopExclBmp,
                                           SNOOP_SRC_LIST_SIZE);
                        }

                        SNOOP_AND_SRC_BMP (TempExclSrcBmp, OldExclSrcBmp);
                        if (SNOOP_MEM_CMP (TempExclSrcBmp, gNullSrcBmp,
                                           SNOOP_SRC_LIST_SIZE) == 0)
                        {
                            SNOOP_MEM_CPY (TempExclSrcBmp, SnoopExclBmp,
                                           SNOOP_SRC_LIST_SIZE);
                        }

                        /* If the (result & old == 0) && (result & new != 0) 
                         * new source has been added so send an BLOCK for 
                         * new sources */
                        if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                            SNOOP_SRC_LIST_SIZE) == 0) &&
                            (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                            SNOOP_SRC_LIST_SIZE) != 0))
                        {
                            gu4SendRecType = SNOOP_BLOCK;
                        }
                        /* If the (result & old != 0) && (result & new == 0) 
                         * old source has been deletd so send an ALLOW for 
                         * old sources */
                        else if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                                 SNOOP_SRC_LIST_SIZE) != 0) &&
                                 (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                                 SNOOP_SRC_LIST_SIZE) == 0))
                        {
                            gu4SendRecType = SNOOP_ALLOW;
                        }
                        /* If the (result & old != 0) && (result & new != 0)   
                         * both deletion and addition of sources has been made 
                         * so send a TO_EXCL for the new list of sources */
                        else
                        {
                            gu4SendRecType = SNOOP_TO_EXCLUDE;
                        }
                    }
                }
            }
            else if (u1RecordType == SNOOP_ALLOW)
            {
                /* Since the old include source bitmap is not equal to the
                 * new include source bitmap send an ALLOW for 
                 * the new sources */
                if ((SNOOP_MEM_CMP (OldExclSrcBmp,
                                    pSnoopConsGroupEntry->ExclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_ALLOW;
                }
            }
            else if (u1RecordType == SNOOP_BLOCK)
            {
                /* Since the old include source bitmap is not equal to the
                 * new include source bitmap send an BLOCK 
                 * for the old sources */
                if ((SNOOP_MEM_CMP (OldExclSrcBmp,
                                    pSnoopConsGroupEntry->ExclSrcBmp,
                                    SNOOP_SRC_LIST_SIZE) != 0))
                {
                    gu4SendRecType = SNOOP_BLOCK;
                }
            }
            else if ((u1RecordType == SNOOP_TO_INCLUDE) ||
                     (u1RecordType == SNOOP_IS_INCLUDE))
            {
                /*if old exclude source bitmap is not changed so no need to 
                 * send any thing to router */
                if (SNOOP_MEM_CMP (OldExclSrcBmp,
                                   pSnoopConsGroupEntry->ExclSrcBmp,
                                   SNOOP_SRC_LIST_SIZE) == 0)
                {
                    gu4SendRecType = 0;
                }
                else
                {
                    /* Get the difference between the old and 
                     * new source bitmap for  the group entry */
                    SNOOP_MEM_CPY (TempExclSrcBmp, OldExclSrcBmp,
                                   SNOOP_SRC_LIST_SIZE);
                    SNOOP_XOR_SRC_BMP (TempExclSrcBmp,
                                       pSnoopConsGroupEntry->ExclSrcBmp);
                    SNOOP_MEM_CPY (TempSrcBmp, TempExclSrcBmp,
                                   SNOOP_SRC_LIST_SIZE);
                    SNOOP_AND_SRC_BMP (TempSrcBmp,
                                       pSnoopConsGroupEntry->ExclSrcBmp);
                    if (SNOOP_MEM_CMP (TempSrcBmp, gNullSrcBmp,
                                       SNOOP_SRC_LIST_SIZE) == 0)
                    {
                        SNOOP_MEM_CPY (TempSrcBmp, SnoopExclBmp,
                                       SNOOP_SRC_LIST_SIZE);
                    }

                    SNOOP_AND_SRC_BMP (TempExclSrcBmp, OldExclSrcBmp);
                    if (SNOOP_MEM_CMP (TempExclSrcBmp, gNullSrcBmp,
                                       SNOOP_SRC_LIST_SIZE) == 0)
                    {
                        SNOOP_MEM_CPY (TempExclSrcBmp, SnoopExclBmp,
                                       SNOOP_SRC_LIST_SIZE);
                    }

                    /* If the (result & old == 0) && (result & new != 0) new 
                     * source has been added so send an BLOCK for new sources */
                    if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                        SNOOP_SRC_LIST_SIZE) == 0) &&
                        (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                        SNOOP_SRC_LIST_SIZE) != 0))
                    {
                        gu4SendRecType = SNOOP_BLOCK;
                    }
                    /* If the (result & old != 0) && (result & new == 0) old  
                     * source has been deletd so send an ALLOW for 
                     * old sources */
                    else if ((SNOOP_MEM_CMP (TempExclSrcBmp, SnoopExclBmp,
                                             SNOOP_SRC_LIST_SIZE) != 0) &&
                             (SNOOP_MEM_CMP (TempSrcBmp, SnoopExclBmp,
                                             SNOOP_SRC_LIST_SIZE) == 0))
                    {
                        gu4SendRecType = SNOOP_ALLOW;
                    }
                }
            }
        }
    }
    /* Reset the is same host flag */
    gu1IsSameHost = 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetHostEntry                                */
/*                                                                           */
/* Description        : This function returns if the host is present or not  */
/*                      for a group record                                   */
/*                                                                           */
/* Input(s)           : pSnoopGroupEntry - Pointer to the group entry        */
/*                      pSnoopPktInfo   - Pointer to packet information      */
/*                                                                           */
/* Output(s)          : ppRetSnoopHostNode - pointer to the host node        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/ SNOOP_FAILURE                         */
/*****************************************************************************/
INT4
SnoopUtilGetHostEntry (tSnoopGroupEntry * pSnoopGroupEntry,
                       tSnoopPktInfo * pSnoopPktInfo,
                       tSnoopHostEntry ** ppRetSnoopHostNode)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    UINT1               u1EntryFound = SNOOP_FALSE;

    /* Check if the port is present in the group record */
    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (pSnoopSSMPortNode->u4Port == pSnoopPktInfo->u4InPort)
        {
            u1EntryFound = SNOOP_TRUE;
            break;
        }
    }

    if (u1EntryFound == SNOOP_TRUE)
    {
        /* Update the host information */
        SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList, pSnoopHostNode,
                        tSnoopHostEntry *)
        {
            if (IPVX_ADDR_COMPARE (pSnoopHostNode->HostIpAddr,
                                   pSnoopPktInfo->SrcIpAddr) == 0)
            {
                *ppRetSnoopHostNode = pSnoopHostNode;
                return SNOOP_SUCCESS;
            }
        }
    }

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetForwardPorts                             */
/*                                                                           */
/* Description        : This function will return the port bitmap for        */
/*                      creating a forwarding entry                          */
/*                                                                           */
/* Input(s)           : pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2SrcIndex     - Source Index                        */
/*                      u1ASMHostPresent - v1/ v2 host present flag          */
/*                                                                           */
/* Output(s)          : PortBitmap - Forward Port Bitmap                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopUtilGetForwardPorts (tSnoopGroupEntry * pSnoopGroupEntry, UINT2 u2SrcIndex,
                          UINT1 u1ASMHostPresent, tSnoopPortBmp PortBitmap)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopSrcBmp        SnoopExclBmp;
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);

    SNOOP_MEM_SET (PortBitmap, 0, SNOOP_PORT_LIST_SIZE);

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
        {
            OSIX_BITLIST_IS_BIT_SET (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp,
                                     u2SrcIndex, SNOOP_SRC_LIST_SIZE, bResult);
            if (bResult == OSIX_TRUE)
            {
                SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port, PortBitmap);
            }
        }
        else
        {
            if (u2SrcIndex == 0xffff)
            {
                if (SNOOP_MEM_CMP
                    (pSnoopSSMPortNode->pSourceBmp->InclSrcBmp, gNullSrcBmp,
                     SNOOP_SRC_LIST_SIZE) == 0)
                {
                    SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port,
                                            PortBitmap);
                }
                else
                {
                    if ((SNOOP_MEM_CMP
                         (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp,
                          SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0))
                    {
                        SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port,
                                                PortBitmap);
                    }
                }
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopSSMPortNode->pSourceBmp->
                                         InclSrcBmp, u2SrcIndex,
                                         SNOOP_SRC_LIST_SIZE, bResult);
                if (bResult == OSIX_TRUE)
                {
                    SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port,
                                            PortBitmap);
                }
                else
                {
                    if (SNOOP_MEM_CMP
                        (pSnoopSSMPortNode->pSourceBmp->ExclSrcBmp,
                         SnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopSSMPortNode->pSourceBmp->
                                                 ExclSrcBmp, u2SrcIndex,
                                                 SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_FALSE)
                        {
                            SNOOP_ADD_TO_PORT_LIST (pSnoopSSMPortNode->u4Port,
                                                    PortBitmap);
                        }
                    }
                }
            }
        }
    }

    if (u1ASMHostPresent == SNOOP_TRUE)
    {
        SNOOP_UPDATE_PORT_LIST (pSnoopGroupEntry->ASMPortBitmap, PortBitmap);
    }
    if ((pSnoopGroupEntry->u1IsUnknownMulticast == SNOOP_TRUE) &&
        (SNOOP_SYSTEM_SPARSE_MODE (gu1SnoopInstanceId) == SNOOP_DISABLE))
    {
        /*Unknown multicast should be flooded to all ports when sparse mode is disabled in the system.
         * So setting the PortBitmap to FF.*/
        SNOOP_MEM_SET (PortBitmap, 0xff, SNOOP_PORT_LIST_SIZE);
    }
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateRefCount                              */
/*                                                                           */
/* Description        : This function will increment the group reference     */
/*                      count for source and source count for group          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Id                             */
/*                      pSnoopGroupEntry - Pointer to the group entry        */
/*                      u2SrcIndex     - Source Index                        */
/*                                                                           */
/* Output(s)          : pu1Incr  - SNOOP_TRUE/ SNOOP_FALSE                   */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

VOID
SnoopUtilUpdateRefCount (UINT4 u4Instance, UINT2 u2SrcIndex,
                         tSnoopGroupEntry * pSnoopGroupEntry, UINT1 *pu1Incr)
{
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    BOOL1               bResult = OSIX_FALSE;
    UINT1               u1SourceFound = SNOOP_FALSE;

    *pu1Incr = SNOOP_FALSE;

    u2SrcIndex++;

    SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                    pSnoopSSMPortNode, tSnoopPortEntry *)
    {
        SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList,
                        pSnoopHostNode, tSnoopHostEntry *)
        {
            if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp,
                                         u2SrcIndex, SNOOP_SRC_LIST_SIZE,
                                         bResult);
                if (bResult == OSIX_TRUE)
                {
                    u1SourceFound = SNOOP_TRUE;
                    break;
                }
            }
            else
            {
                OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->InclSrcBmp,
                                         u2SrcIndex, SNOOP_SRC_LIST_SIZE,
                                         bResult);
                if (bResult == OSIX_TRUE)
                {
                    u1SourceFound = SNOOP_TRUE;
                    break;
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->
                                             ExclSrcBmp, u2SrcIndex,
                                             SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        u1SourceFound = SNOOP_TRUE;
                        break;
                    }
                }
            }
        }
    }

    if ((u1SourceFound == SNOOP_FALSE) && (u2SrcIndex <= SNOOP_MAX_MCAST_SRCS))
    {
        SNOOP_SRC_INFO (u4Instance, (u2SrcIndex - 1)).u2GrpRefCount++;
        *pu1Incr = SNOOP_TRUE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilCheckIsSourceUsed                           */
/*                                                                           */
/* Description        : This function will check whether the source          */
/*                      information is used across groups                    */
/*                                                                           */
/* Input(s)           : u2SrcIndex     - Source Index                        */
/*                                                                           */
/* Output(s)          : pu1EntryUsed  - SNOOP_TRUE/ SNOOP_FALSE              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopUtilCheckIsSourceUsed (UINT4 u4Instance, UINT2 u2SrcIndex,
                            UINT1 *pu1SrcUsed)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    tSnoopPortEntry    *pSnoopSSMPortNode = NULL;
    tSnoopHostEntry    *pSnoopHostNode = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    BOOL1               bResult = OSIX_FALSE;

    *pu1SrcUsed = SNOOP_FALSE;

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        SNOOP_SLL_SCAN (&pSnoopGroupEntry->SSMPortList,
                        pSnoopSSMPortNode, tSnoopPortEntry *)
        {
            SNOOP_SLL_SCAN (&pSnoopSSMPortNode->HostList,
                            pSnoopHostNode, tSnoopHostEntry *)
            {
                if (pSnoopGroupEntry->u1FilterMode == SNOOP_INCLUDE)
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->
                                             InclSrcBmp, u2SrcIndex,
                                             SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        *pu1SrcUsed = SNOOP_TRUE;
                        return;
                    }
                }
                else
                {
                    OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->
                                             InclSrcBmp, u2SrcIndex,
                                             SNOOP_SRC_LIST_SIZE, bResult);
                    if (bResult == OSIX_TRUE)
                    {
                        *pu1SrcUsed = SNOOP_TRUE;
                        return;
                    }
                    else
                    {
                        OSIX_BITLIST_IS_BIT_SET (pSnoopHostNode->pSourceBmp->
                                                 ExclSrcBmp, u2SrcIndex,
                                                 SNOOP_SRC_LIST_SIZE, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            *pu1SrcUsed = SNOOP_TRUE;
                            return;
                        }
                    }
                }
            }
        }
        pRBElem = pRBElemNext;
    }
}

/*****************************************************************************/
/* Function Name      : SnoopUtilIsPortListValid                             */
/*                                                                           */
/* Description        : This function validates the given port list          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      pu1Ports   - PortList to be validated                */
/*                      i4Length - Length of the list                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE /SNOOP_FALSE                              */
/*****************************************************************************/
UINT1
SnoopUtilIsPortListValid (UINT4 u4Instance, UINT1 *pu1Ports, INT4 i4Length)
{
    UINT4               u4Port = 0;
    UINT4               u4PortInstance = 0;
    UINT4               u4LocalPort = 0;
    UINT2               u2Index = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    for (u2Index = 0; u2Index < i4Length; u2Index++)
    {
        if (pu1Ports[u2Index] != 0)
        {
            u1PortFlag = pu1Ports[u2Index];

            for (u2BitIndex = 0;
                 ((u2BitIndex < SNOOP_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & SNOOP_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2Index * SNOOP_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);

                    if (SnoopL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
                    {
                        return SNOOP_FALSE;
                    }

                    if (SnoopGetInstInfoFromPort (u4Port, &u4PortInstance,
                                                  &u4LocalPort) !=
                        SNOOP_SUCCESS)
                    {
                        return SNOOP_FALSE;
                    }

                    if (u4Instance != u4PortInstance)
                    {
                        return SNOOP_FALSE;
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
    }

    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilVlanResetRtrPorts                           */
/*                                                                           */
/* Description        : This function deletes the port from router port list */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      VlanId     - Vlan Id                                 */
/*                      pu1Ports   - PortList to be reset                    */
/*                      i4Length   - Length of the list                      */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS /SNOOP_FAILURE                         */
/*****************************************************************************/

INT4
SnoopUtilVlanResetRtrPorts (INT4 i4Instance, tSnoopTag VlanId,
                            UINT1 u1AddrType, tSnoopPortBmp PortBitmap)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortNode = NULL;
    tSnoopRtrPortEntry *pTempSnoopRtrPortEntry = NULL;
    tMacAddr            NullMacAddr;
    BOOL1               bResult = OSIX_FALSE;
    UINT2               u2Port = 0;
    UINT2               u2BytIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT4               u4Port = 0;
    UINT1               u1PortFlag = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = (UINT4) i4Instance;
    gu4SnoopDbgInstId = (UINT4) i4Instance;

    if (SnoopVlanGetVlanEntry ((UINT4) i4Instance, VlanId,
                               u1AddrType, &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG_ARG3 (SNOOP_DBG_FLAG_INST ((UINT4) i4Instance),
                            SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_MGMT_NAME, "Snoop VLAN table entry for "
                            "instance %d vlan %d and the address type "
                            "%d not found\r\n", i4Instance,
                            SNOOP_OUTER_VLAN (VlanId), u1AddrType);
        return SNMP_FAILURE;
    }

    SNOOP_MEM_SET (&NullMacAddr, 0, sizeof (tMacAddr));

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                SNOOP_DYN_SLL_SCAN (&(pSnoopVlanEntry->RtrPortList),
                                    pRtrPortNode, pTempSnoopRtrPortEntry,
                                    tSnoopRtrPortEntry *)
                {
                    if (pRtrPortNode->u4Port == u2Port)
                    {
                        /*Release the router port node and update the
                         * VLAN router port bitmap */
                        SNOOP_SLL_DELETE (&pSnoopVlanEntry->RtrPortList,
                                          pRtrPortNode);
                        /*SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (i4Instance,
                           pRtrPortNode); */

                        SNOOP_DEL_FROM_PORT_LIST (u2Port,
                                                  pSnoopVlanEntry->
                                                  RtrPortBitmap);

                        /* Check if the port is learnt from the general
                         * query then delete the  port from the
                         * QuerierRtrPortBitmap of the VLAN entry */
                        SNOOP_IS_PORT_PRESENT (u2Port,
                                               pSnoopVlanEntry->
                                               QuerierRtrPortBitmap, bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u2Port,
                                                      pSnoopVlanEntry->
                                                      QuerierRtrPortBitmap);
                        }
                        /* Reset the V3 router port bit map of the Vlan */
                        SNOOP_IS_PORT_PRESENT (u2Port,
                                               pSnoopVlanEntry->V3RtrPortBitmap,
                                               bResult);

                        if (bResult == OSIX_TRUE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u2Port,
                                                      pSnoopVlanEntry->
                                                      V3RtrPortBitmap);
                        }

                        /* Reset the V2 router port bit map of the Vlan */
                        SNOOP_IS_PORT_PRESENT (u2Port,
                                               pSnoopVlanEntry->V2RtrPortBitmap,
                                               bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u2Port,
                                                      pSnoopVlanEntry->
                                                      V2RtrPortBitmap);
                        }

                        /* Reset the V1 router port bit map of the Vlan */
                        SNOOP_IS_PORT_PRESENT (u2Port,
                                               pSnoopVlanEntry->V1RtrPortBitmap,
                                               bResult);
                        if (bResult == OSIX_TRUE)
                        {
                            SNOOP_DEL_FROM_PORT_LIST (u2Port,
                                                      pSnoopVlanEntry->
                                                      V1RtrPortBitmap);
                        }

                        /* Update the multicast forwarding table by
                         * removing the port */
                        if (SNOOP_MCAST_FWD_MODE (i4Instance) ==
                            SNOOP_MCAST_FWD_MODE_MAC)
                        {
                            /*Delete the port from MAC based multicast
                             * forwarding table*/
                            if (SnoopFwdUpdateRtrPortToMacFwdTable
                                (i4Instance, u2Port,
                                 VlanId,
                                 pSnoopVlanEntry->u1AddressType,
                                 NullMacAddr, SNOOP_DEL_PORT) != SNOOP_SUCCESS)
                            {
                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                               ((UINT4) i4Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_FWD_NAME,
                                               "Deletion of port from MAC"
                                               "forwarding entry failed\r\n");
                                return SNOOP_FAILURE;
                            }
                        }
                        else
                        {
                            /* Forwarding Mode is IP based so update the IP
                               forwarding table */
                            if (SnoopFwdUpdateRtrPortToIpFwdTable
                                (i4Instance, u2Port,
                                 VlanId,
                                 pSnoopVlanEntry->u1AddressType,
                                 SNOOP_DEL_PORT) != SNOOP_SUCCESS)
                            {
                                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                               ((UINT4) i4Instance),
                                               SNOOP_CONTROL_PATH_TRC |
                                               SNOOP_DBG_ALL_FAILURE,
                                               SNOOP_FWD_NAME,
                                               "Deletion of port from IP"
                                               "forwarding entry failed\r\n");
                                return SNOOP_FAILURE;
                            }
                        }

                        /* Stop the v1 port purge timer if it is running */
                        if (pRtrPortNode->V1RtrPortPurgeTimer.u1TimerType !=
                            SNOOP_INVALID_TIMER_TYPE)
                        {
                            SnoopTmrStopTimer (&(pRtrPortNode->
                                                 V1RtrPortPurgeTimer));
                        }
                        /* Stop the v2 port purge timer if it is running */
                        if (pRtrPortNode->V2RtrPortPurgeTimer.u1TimerType !=
                            SNOOP_INVALID_TIMER_TYPE)
                        {
                            SnoopTmrStopTimer (&(pRtrPortNode->
                                                 V2RtrPortPurgeTimer));
                        }
                        /* Stop the v3 port purge timer if it is running */
                        if (pRtrPortNode->V3RtrPortPurgeTimer.u1TimerType !=
                            SNOOP_INVALID_TIMER_TYPE)
                        {
                            SnoopTmrStopTimer (&(pRtrPortNode->
                                                 V3RtrPortPurgeTimer));
                        }

                        /* Remove the router port entry from the global RBTree */
                        u4Port = pRtrPortNode->u4Port;
                        RBTreeRem (gRtrPortTblRBRoot, pRtrPortNode);

                        SNOOP_VLAN_RTR_PORT_FREE_MEMBLK (i4Instance,
                                                         pRtrPortNode);
                        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (i4Instance),
                                            SNOOP_DBG_RESRC,
                                            SNOOP_OS_RES_NAME,
                                            "Memory released - mrouter entry for port %d \r\n",
                                            u4Port);

                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }

    /* Send a Sync Message containing Updated Router
     * Port list for the VLAN */
    SnoopRedActiveSendRtrPortListSync ((UINT4) i4Instance, pSnoopVlanEntry);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilFilterGrpsAndNotify                         */
/*                                                                           */
/* Description        : This function will check whether the report needs    */
/*                      to be processed                                      */
/*                                                                           */
/* Input(s)           : pSnoopFilterInfo - Snoop filter information          */
/*                                                                           */
/* Return Value(s)    : SNOOP_PERMIT/ SNOOP_DENY                             */
/*****************************************************************************/

BOOL1
SnoopUtilFilterGrpsAndNotify (tSnoopFilter * pSnoopFilterInfo)
{
    /* currently this function is stub, always returns PERMIT */
    UNUSED_PARAM (pSnoopFilterInfo);
    return SNOOP_PERMIT;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilMiGetMcIgsFwdPorts                      */
/*                                                                           */
/*    Description         : This function returns Portlist, UntagPortlist    */
/*                          for a given Multicast group entry.               */
/*                          The port list will be retrieved from             */
/*                          1. IGS  - If Igs forwarding type is IPMC         */
/*                          1. VLAN - If Igs forwarding type is L2MC         */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance ID                         */
/*                          VlanId  - Vlan Id                                */
/*                          u4GrpAddr - Group address                        */
/*                          u4SrcAddr - Group Source address                 */
/*                                                                           */
/*    Output(s)           : PortList - Port list for this MC group to return */
/*                          UntagPortList - Untag port list                  */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/

INT4
SnoopUtilMiGetMcIgsFwdPorts (UINT4 u4Instance, tSnoopTag VlanId,
                             UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                             tPortList PortList, tPortList UntagPortList)
{
    UINT1               u1FwdType = 0;
    tMacAddr            au1MacAddr;
    tSnoopIfPortBmp    *pNullPortList = NULL;

    /* This function is currently called from L3 modules only, as L3 modules are
       not instantiated , all the oprations are done for Default Insatnce */
    /* Call IGS module to determine the Forwarding type */
    u1FwdType = SnoopMiGetForwardingType (u4Instance);

    if (u1FwdType == SNOOP_MCAST_FWD_MODE_MAC)
    {
        UtilConvMcastIP2Mac (u4GrpAddr, au1MacAddr);
        pNullPortList =
            (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

        if (pNullPortList == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "Error in allocating memory for pNullPortList\r\n");
            SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                   SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                   SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                                   "for u4GrpAddr %d\r\n", u4GrpAddr);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pNullPortList, 0, sizeof (tSnoopIfPortBmp));
        /* MAC based forwarding - 
         * Get PortList from VLAN itself, as VLAN will be updated by IGS
         */
        if (SnoopMiVlanSnoopGetTxPortList
            (u4Instance, au1MacAddr, 0, VlanId, VLAN_FORWARD_ALL,
             *pNullPortList, PortList, UntagPortList) != VLAN_FORWARD)

        {
            FsUtilReleaseBitList ((UINT1 *) pNullPortList);
            return SNOOP_FAILURE;
        }
        FsUtilReleaseBitList ((UINT1 *) pNullPortList);

        /* Untag ports will not be member of Tag portlist, 
         * hence set the un-tag ports in Portlist */
        OSIX_ADD_PORT_LIST (PortList, UntagPortList,
                            sizeof (tPortList), sizeof (tPortList));

    }
    else if (u1FwdType == SNOOP_MCAST_FWD_MODE_IP)
    {
        if (SnoopUtilGetMcastIgsIpFwdPorts (u4Instance, VlanId, u4GrpAddr,
                                            u4SrcAddr, PortList,
                                            UntagPortList) == SNOOP_FAILURE)
        {
            return SNOOP_FAILURE;
        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetMcastIgsIpFwdPorts                   */
/*                                                                           */
/*    Description         : This function returns Portlist, UntagPortlist    */
/*                          for a given IP Multicast group entry from IGS.   */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance Id                         */
/*                          VlanId  - Vlan Id                                */
/*                          GrpAddr - Group address                          */
/*                          SrcAddr - Group Source address                   */
/*                                                                           */
/*    Output(s)           : PortList - Port list for this MC group to return */
/*                          UntagPortList - Untag port list                  */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/

INT4
SnoopUtilGetMcastIgsIpFwdPorts (UINT4 u4Instance, tSnoopTag VlanId,
                                UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                                tPortList PortList, tPortList UntagPortList)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = SNOOP_FAILURE;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
#endif
    tSnoopIfPortBmp    *pTempPortBmp = NULL;
    tSnoopIfPortBmp    *pTagPortBmp = NULL;
    tSnoopIfPortBmp    *pUntagPortBmp = NULL;
    tMacAddr            au1MacAddr;

    UtilConvMcastIP2Mac (u4GrpAddr, au1MacAddr);

    pTempPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pTempPortBmp == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pTempPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4GrpAddr %d\r\n", u4GrpAddr);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTempPortBmp, 0, sizeof (tSnoopIfPortBmp));

    pTagPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pTagPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pTagPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4GrpAddr %d\r\n", u4GrpAddr);
        FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTagPortBmp, 0, sizeof (tSnoopIfPortBmp));

    pUntagPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pUntagPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pUntagPortBmp\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4GrpAddr %d\r\n", u4GrpAddr);
        FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
        FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pUntagPortBmp, 0, sizeof (tSnoopIfPortBmp));
#ifdef NPAPI_WANTED
    IPVX_ADDR_INIT (GrpAddr, SNOOP_ADDR_TYPE_IPV4, (UINT1 *) &u4GrpAddr);

    IPVX_ADDR_INIT (SrcAddr, SNOOP_ADDR_TYPE_IPV4, (UINT1 *) &u4SrcAddr);

    i4RetVal = SnoopNpGetMulticastEgressPorts (u4Instance, VlanId, GrpAddr,
                                               SrcAddr, *pTempPortBmp);
    if (i4RetVal == SNOOP_FAILURE)
#else
    UNUSED_PARAM (u4SrcAddr);
#endif
    {
        /*
         * This Sparse mode check is done here to ensure that
         * 1: When Sparse mode is enabled, and IGS has interested receivers in its forwarding port-list,
         *    the data can be sent on the forwarding port-list
         * 2: When Sparse mode is enabled, and IGS does not have interested receivers
         *    in its forwarding port-list (NULL port-list), data should not be sent to any ports
         * 3: When Sparse mode is disabled, and IGS has interested receivers in its forwarding port-list,
         *    the data can be sent on the forwarding port-list
         * 4: When Sparse mode is disabled, and IGS does not have interested receivers
         *    in its forwarding port-list (NULL port-list),
         *    then query VLAN for member ports (Vlan flooding should happen)
         */
#ifdef NPAPI_WANTED
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            SNOOP_MEM_CPY (PortList, *pTagPortBmp, sizeof (tSnoopIfPortBmp));
            SNOOP_MEM_CPY (UntagPortList, *pUntagPortBmp,
                           sizeof (tSnoopIfPortBmp));

            FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
            return SNOOP_FAILURE;
        }
        else
#endif
        {
            if (SnoopMiGetVlanEgressPorts (u4Instance, VlanId, *pTempPortBmp) ==
                SNOOP_FAILURE)
            {
                FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
                FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
                FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
                return SNOOP_FAILURE;
            }
        }
    }

    SNOOP_MEM_CPY (*pTagPortBmp, PortList, sizeof (tSnoopIfPortBmp));
    SNOOP_MEM_CPY (*pUntagPortBmp, UntagPortList, sizeof (tSnoopIfPortBmp));

    if (SnoopMiVlanSnoopGetTxPortList
        (u4Instance, au1MacAddr, 0, VlanId, VLAN_FORWARD_SPECIFIC,
         *pTempPortBmp, *pTagPortBmp, *pUntagPortBmp) != VLAN_FORWARD)
    {
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) != SNOOP_ENABLE)
        {
            FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);

            /* If the port list is empty, a NULL forwarding
               entry is created in control plane if sparse mode
               is disabled */

            if ((SNOOP_MEM_CMP (PortList,
                                gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0) &&
                (SNOOP_MEM_CMP
                 (UntagPortList, gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0))
            {
                return SNOOP_SUCCESS;
            }
            return SNOOP_FAILURE;
        }
    }

    /* Untag ports will not be member of Tag portlist, 
     * hence set the un-tag ports in Portlist */

    OSIX_ADD_PORT_LIST ((*pTagPortBmp), (*pUntagPortBmp),
                        sizeof (tSnoopIfPortBmp), sizeof (tSnoopIfPortBmp));

    OSIX_ADD_PORT_LIST (PortList, (*pTagPortBmp),
                        sizeof (tPortList), sizeof (tSnoopIfPortBmp));

    OSIX_ADD_PORT_LIST (UntagPortList, (*pUntagPortBmp),
                        sizeof (tPortList), sizeof (tSnoopIfPortBmp));

    FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
    FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
    FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetMcastMldsIpFwdPorts                  */
/*                                                                           */
/*    Description         : This function returns Portlist, UntagPortlist    */
/*                          for a given IP Multicast group entry from IGS.   */
/*                                                                           */
/*    Input(s)            : VlanId  - Vlan Id                                */
/*                          GrpAddr - Group address                          */
/*                          SrcAddr - Group Source address                   */
/*                                                                           */
/*    Output(s)           : PortList - Port list for this MC group to return */
/*                          UntagPortList - Untag port list                  */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/

INT4
SnoopUtilGetMcastMldsIpFwdPorts (UINT4 u4Instance, tSnoopTag VlanId,
                                 UINT1 *pu1GrpAddr, UINT1 *pu1SrcAddr,
                                 tPortList PortList, tPortList UntagPortList)
{
#ifdef NPAPI_WANTED
    INT4                i4RetVal = SNOOP_FAILURE;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;
#endif
    tSnoopIfPortBmp    *pTempPortBmp = NULL;
    tSnoopIfPortBmp    *pTagPortBmp = NULL;
    tSnoopIfPortBmp    *pUntagPortBmp = NULL;
    tMacAddr            au1MacAddr;

    SNOOP_GET_MAC_FROM_IPV6 (pu1GrpAddr, au1MacAddr);
    pTempPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pTempPortBmp == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pTempPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTempPortBmp, 0, sizeof (tSnoopIfPortBmp));

    pTagPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pTagPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pTagPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTagPortBmp, 0, sizeof (tSnoopIfPortBmp));

    pUntagPortBmp =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pUntagPortBmp == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pUntagPortBmp\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
        FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pUntagPortBmp, 0, sizeof (tSnoopIfPortBmp));
#ifdef NPAPI_WANTED
    IPVX_ADDR_INIT (GrpAddr, SNOOP_ADDR_TYPE_IPV6, pu1GrpAddr);

    IPVX_ADDR_INIT (SrcAddr, SNOOP_ADDR_TYPE_IPV6, pu1SrcAddr);

    i4RetVal = SnoopNpGetMulticastEgressPorts (u4Instance, VlanId, GrpAddr,
                                               SrcAddr, *pTempPortBmp);

    if (i4RetVal == SNOOP_FAILURE)
#else
    UNUSED_PARAM (pu1SrcAddr);
#endif

    {
        if (SnoopMiGetVlanEgressPorts (u4Instance, VlanId, *pTempPortBmp) ==
            L2IWF_FAILURE)
        {
            FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
            return SNOOP_FAILURE;
        }
    }

    SNOOP_MEM_CPY (*pTagPortBmp, PortList, sizeof (tSnoopIfPortBmp));
    SNOOP_MEM_CPY (*pUntagPortBmp, UntagPortList, sizeof (tSnoopIfPortBmp));

    if (SnoopMiVlanSnoopGetTxPortList
        (u4Instance, au1MacAddr, 0, VlanId, VLAN_FORWARD_SPECIFIC,
         *pTempPortBmp, *pTagPortBmp, *pUntagPortBmp) != VLAN_FORWARD)
    {
        FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
        FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
        FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
        return SNOOP_FAILURE;
    }

    /* Untag ports will not be member of Tag portlist, 
     * hence set the un-tag ports in Portlist */
    OSIX_ADD_PORT_LIST ((*pTagPortBmp), (*pUntagPortBmp),
                        sizeof (tSnoopIfPortBmp), sizeof (tSnoopIfPortBmp));

    SNOOP_MEM_CPY (PortList, *pTagPortBmp, sizeof (tSnoopIfPortBmp));
    SNOOP_MEM_CPY (UntagPortList, *pUntagPortBmp, sizeof (tSnoopIfPortBmp));

    FsUtilReleaseBitList ((UINT1 *) pTempPortBmp);
    FsUtilReleaseBitList ((UINT1 *) pTagPortBmp);
    FsUtilReleaseBitList ((UINT1 *) pUntagPortBmp);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilValidateInstanceId                      */
/*                                                                           */
/*    Description         : This function validates the given instance id    */
/*                                                                           */
/*    Input(s)            : InstanceId  - Instance Identifier                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/
INT4
SnoopUtilValidateInstanceId (UINT4 u4InstanceId)
{
    if (u4InstanceId >= SNOOP_MAX_INSTANCES)
    {
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilValidateVlanId                          */
/*                                                                           */
/*    Description         : This function validates the given VLAN ID        */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/
INT4
SnoopUtilValidateVlanId (tSnoopTag VlanId)
{
    if ((SNOOP_OUTER_VLAN (VlanId) <= 0) ||
        (SNOOP_OUTER_VLAN (VlanId) > SNOOP_MAX_VLAN_ID))
    {
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilValidateAddressType                     */
/*                                                                           */
/*    Description         : This function validates the given VLAN ID        */
/*                                                                           */
/*    Input(s)            : u1AddressType - Indicates IGS / MLDS             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/
INT4
SnoopUtilValidateAddressType (UINT1 u1AddressType)
{
    if ((u1AddressType == SNOOP_ADDR_TYPE_IPV4) ||
        (u1AddressType == SNOOP_ADDR_TYPE_IPV6))
    {
        return SNOOP_SUCCESS;
    }

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanResetStats                                  */
/*                                                                           */
/* Description        : This function clears the IGS Statistics per/all Vlans */
/*                                                                           */
/* Input(s)           : u4InstId - Instance Identifier                       */
/*                      u1AddrType - Indicates the IGS/MLDS                  */
/*                      VlanId - Vlan Identifier                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/

INT4
SnoopUtilVlanResetStats (UINT4 u4InstId, UINT1 u1AddrType, tVlanId VlanId)
{

    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopTag           SnoopVlanId;
    UINT4               u4ActiveGrps = 0;

    SNOOP_MEM_SET (SnoopVlanId, 0, sizeof (tSnoopTag));
    if (VlanId != 0)
    {
        SNOOP_OUTER_VLAN (SnoopVlanId) = VlanId;
        /*Get the Snoop Vlan Entry for the specific Vlan */

        if (SnoopVlanGetVlanEntry (u4InstId, SnoopVlanId, u1AddrType,
                                   &pSnoopVlanEntry) == SNOOP_FAILURE)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4InstId),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_VLAN_NAME,
                                "VLAN entry not found ,in Snoop Vlan"
                                "Entry Table, for VLAN %d\r\n",
                                SNOOP_OUTER_VLAN (SnoopVlanId));

            return CLI_FAILURE;
        }

        /*Reset the statistics for that particular vlan entry */

        if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
        {
            /* Active group count should not be reset */
            u4ActiveGrps = pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps;

            SNOOP_MEM_SET (pSnoopVlanEntry->pSnoopVlanStatsEntry, 0,
                           sizeof (tSnoopVlanStatsEntry));

            pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps = u4ActiveGrps;
        }

    }
    else
    {
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4InstId)->VlanEntry);

        while (pRBElem != NULL)
        {

            pRBNextElem =
                RBTreeGetNext
                (SNOOP_INSTANCE_INFO (u4InstId)->VlanEntry, pRBElem, NULL);

            pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

            pRBElem = pRBNextElem;

            if (pSnoopVlanEntry->u1AddressType == u1AddrType)
            {
                if (pSnoopVlanEntry->pSnoopVlanStatsEntry != NULL)
                {

                    /* Active group count should not be reset */
                    u4ActiveGrps =
                        pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps;

                    SNOOP_MEM_SET
                        (pSnoopVlanEntry->pSnoopVlanStatsEntry, 0,
                         sizeof (tSnoopVlanStatsEntry));
                    pSnoopVlanEntry->pSnoopVlanStatsEntry->u4ActiveGrps =
                        u4ActiveGrps;

                }
            }

        }
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopGetInstInfoFromPort                             */
/*                                                                           */
/* Description        : This function returns the instance number associated */
/*                      with the port                                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Number                              */
/*                                                                           */
/* Output(s)          : u4Instance - Instance Id                             */
/*                    : u4InstPort - Instance Port Number                    */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SnoopGetInstInfoFromPort (UINT4 u4IfIndex, UINT4 *pu4Instance,
                          UINT4 *pu4InstPort)
{
    /* get the local port and instance ID based on the Physical port stored in
       the global array, if the local port is zero it means the physical port 
       is not mapped to ant instance, if the port is mapped to some instance ,
       it will have value whichis not equal to zero  */
    if (SNOOP_GLOBAL_PORT (u4IfIndex) != 0)
    {
        /*global port array has valid mapped port */
        *pu4Instance = (UINT1) SNOOP_GLOBAL_INSTANCE (u4IfIndex);
        *pu4InstPort = SNOOP_GLOBAL_PORT (u4IfIndex);
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopGetIfPortBmp                                   */
/*                                                                           */
/* Description        : This function returns the physical port bitmap       */
/*                      associated with the virtual port bitmap for a        */
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                    : PortBitmap - Vitual port bitmap                      */
/*                                                                           */
/* Output(s)          : PhyPortBitmap - Physical port bitmap                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SnoopGetIfPortBmp (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                   tSnoopIfPortBmp PhyPortBitmap)
{
    UINT4               u4PhyPort;
    UINT2               u2Port;
    UINT2               u2BytIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* stores physical ports info for local port */
                    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u2Port);
                    if (u4PhyPort != 0)
                    {
                    OSIX_BITLIST_SET_BIT (PhyPortBitmap, (UINT2) u4PhyPort,
                                              SNOOP_IF_PORT_LIST_SIZE)}
                }

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopConvertToLocalPortList                          */
/*                                                                           */
/* Description        : This function returns the local port bitmap          */
/*                      associated with the physical port bitmap            */
/*                                                                           */
/* Input(s)           : au1PhyPortBitmap -  port bitmap                      */
/*                                                                           */
/* Output(s)          : au1LocalPortBitmap - local port bitmap               */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopConvertToLocalPortList (tSnoopIfPortBmp PhyPortBmp,
                             tSnoopPortBmp LocalPortBmp)
{
    UINT4               u4Port;
    UINT2               u2ByteIndex;
    UINT2               u2BitIndex;
    UINT1               u1PortFlag;

    for (u2ByteIndex = 0; u2ByteIndex < SNOOP_IF_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = PhyPortBmp[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                /* Modified for Attachment Circuit interface */
                if ((u4Port != 0) && (u4Port <= SNOOP_MAX_PORTS) &&
                    (SNOOP_IFINDEX_IS_VALID_IFACE (u4Port) == TRUE))
                {
                    SNOOP_ADD_TO_PORT_LIST (SNOOP_GLOBAL_PORT (u4Port),
                                            LocalPortBmp);
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopMiMapActivePorts                            */
/*                                                                           */
/*    Description         : This function scans through the L2IWF port table */
/*                          and issues create port indications to SNOOP      */
/*                          module.                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopMiMapActivePorts (UINT4 u4Instance)
{
    UINT4               u4IfIndex = 0;
    UINT2               u2LocalPort = 0;
    UINT2               u2PrevPort = 0;

    /* Scan through all the MAP ports for this context and update the
     * SNOOP Mapping table */

    while (SnoopL2IwfGetNextValidPortForContext (u4Instance,
                                                 u2PrevPort, &u2LocalPort,
                                                 &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (SnoopL2IwfIsPortInPortChannel (u4IfIndex) == L2IWF_SUCCESS)
        {
            u2PrevPort = u2LocalPort;
            continue;
        }

        /* map port to a context */
        SnoopHandleMapPort (u4Instance, u4IfIndex, u2LocalPort);
        u2PrevPort = u2LocalPort;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopInitInstName                                */
/*                                                                           */
/*    Description         : This function updates the Name of the context.   */
/*                                                                           */
/*    Input(s)            : u4Instance                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopInitInstName (UINT4 u4Instance)
{
    SNOOP_MEM_SET (SNOOP_INSTANCE_STR (u4Instance), 0,
                   (SNOOP_INSTANCE_ALIAS_LEN + 1));
    if (SnoopVcmGetSystemMode (SNOOP_PROTOCOL_ID) == VCM_MI_MODE)
    {
        SnoopVcmGetAliasName (u4Instance, SNOOP_INSTANCE_STR (u4Instance));
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilUpdateRtrPortOperVer                    */
/*                                                                           */
/*    Description         : This function is used to update the operating    */
/*                          version of the router port and to update the     */
/*                          corresponding router port bit map in the vlan    */
/*                          for this router port.                            */
/*                                                                           */
/*    Input(s)            : pRtrPortNode - pointer to router port node       */
/*                          u1Version -  new operating version               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopUtilUpdateRtrPortOperVer (tSnoopRtrPortEntry * pRtrPortNode,
                               UINT1 u1Version)
{
    UINT4               u4Instance = 0;

    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {

        if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION1)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V1RtrPortBitmap);

        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION2)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V2RtrPortBitmap);
        }
        else
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V3RtrPortBitmap);
        }

        /* update the current operating version 
         * of the router port with the new version */
        pRtrPortNode->u1OperVer = u1Version;

        if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION1)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V1RtrPortBitmap);

        }
        else if (pRtrPortNode->u1OperVer == SNOOP_IGS_IGMP_VERSION2)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V2RtrPortBitmap);
        }
        else
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V3RtrPortBitmap);
        }
    }
    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {

        if (pRtrPortNode->u1OperVer == SNOOP_MLD_VERSION1)
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V2RtrPortBitmap);
        }
        else
        {
            SNOOP_DEL_FROM_PORT_LIST (pRtrPortNode->u4Port,
                                      pRtrPortNode->pVlanEntry->
                                      V3RtrPortBitmap);
        }

        /* update the current operating version 
         * of the router port with the new version */
        pRtrPortNode->u1OperVer = u1Version;

        if (pRtrPortNode->u1OperVer == SNOOP_MLD_VERSION1)
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V2RtrPortBitmap);

        }
        else
        {
            SNOOP_ADD_TO_PORT_LIST (pRtrPortNode->u4Port,
                                    pRtrPortNode->pVlanEntry->V3RtrPortBitmap);
        }
    }

    u4Instance = SNOOP_GLOBAL_INSTANCE (pRtrPortNode->u4PhyPortIndex);
    SnoopRedActiveSendRtrPortVerSync (u4Instance, pRtrPortNode->u4Port,
                                      pRtrPortNode->u1OperVer,
                                      pRtrPortNode->pVlanEntry);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilStopRtrPortPurgeTmr                     */
/*                                                                           */
/*    Description         : This function is used to stop the                */
/*                          router port purge timer                          */
/*                                                                           */
/*    Input(s)            : pRtrPortNode - pointer to router port node       */
/*                          u1TmrVer  -  specified the version of purge      */
/*                                       timer to be stopped                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopUtilStopRtrPortPurgeTmr (tSnoopRtrPortEntry * pRtrPortNode, UINT1 u1TmrVer)
{

    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {

        if (u1TmrVer == SNOOP_IGS_IGMP_VERSION1)
        {
            SnoopTmrStopTimer (&pRtrPortNode->V1RtrPortPurgeTimer);
        }
        else if (u1TmrVer == SNOOP_IGS_IGMP_VERSION2)
        {
            SnoopTmrStopTimer (&pRtrPortNode->V2RtrPortPurgeTimer);
        }
        else
        {
            SnoopTmrStopTimer (&pRtrPortNode->V3RtrPortPurgeTimer);
        }

    }
    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {

        if (u1TmrVer == SNOOP_MLD_VERSION1)
        {
            SnoopTmrStopTimer (&pRtrPortNode->V2RtrPortPurgeTimer);
        }
        else
        {
            SnoopTmrStopTimer (&pRtrPortNode->V3RtrPortPurgeTimer);
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilStartRtrPortPurgeTmr                    */
/*                                                                           */
/*    Description         : This function is used to start the               */
/*                          router port purge timer                          */
/*                                                                           */
/*    Input(s)            : pRtrPortNode - pointer to router port node       */
/*                          u1TmrVer  -  specified the version of purge      */
/*                                       timer to be stopped                 */
/*                          u4QuerierV3QryInt- V3 router port purge interval */
/*                          u4Instance - Instance Id of the router port      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
SnoopUtilStartRtrPortPurgeTmr (UINT4 u4Instance,
                               tSnoopRtrPortEntry * pRtrPortNode,
                               UINT1 u1TmrVer, UINT4 u4QuerierV3QryInt)
{
    UINT4               u4RtrPortPurgeInt = 0;

    SNOOP_VALIDATE_ADDRESS_TYPE (pRtrPortNode->pVlanEntry->u1AddressType);

    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if ((u1TmrVer == SNOOP_IGS_IGMP_VERSION1) ||
            (u1TmrVer == SNOOP_IGS_IGMP_VERSION2))
        {
            u4RtrPortPurgeInt = pRtrPortNode->u4V1V2RtrPortPurgeInt;
        }
        else
        {
            if (u4QuerierV3QryInt == SNOOP_ZERO)
            {
                u4RtrPortPurgeInt = SNOOP_DEF_RTR_PURGE_INTERVAL;
            }
            else
            {
                u4RtrPortPurgeInt = u4QuerierV3QryInt;
            }

        }
    }
    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (u1TmrVer == SNOOP_MLD_VERSION1)
        {
            u4RtrPortPurgeInt = pRtrPortNode->u4V1V2RtrPortPurgeInt;
        }
        else
        {
            if (u4QuerierV3QryInt == SNOOP_ZERO)
            {
                u4RtrPortPurgeInt = SNOOP_DEF_RTR_PURGE_INTERVAL;
            }
            else
            {
                u4RtrPortPurgeInt = u4QuerierV3QryInt;
            }
        }
    }
    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (u1TmrVer == SNOOP_IGS_IGMP_VERSION1)
        {
            pRtrPortNode->V1RtrPortPurgeTimer.u4Instance = u4Instance;
            pRtrPortNode->V1RtrPortPurgeTimer.u1TimerType =
                SNOOP_ROUTER_PORT_PURGE_V1_TIMER;
            SnoopTmrStartTimer (&pRtrPortNode->V1RtrPortPurgeTimer,
                                u4RtrPortPurgeInt);
        }
        else if (u1TmrVer == SNOOP_IGS_IGMP_VERSION2)
        {
            pRtrPortNode->V2RtrPortPurgeTimer.u4Instance = u4Instance;
            pRtrPortNode->V2RtrPortPurgeTimer.u1TimerType =
                SNOOP_ROUTER_PORT_PURGE_V2_TIMER;
            SnoopTmrStartTimer (&pRtrPortNode->V2RtrPortPurgeTimer,
                                u4RtrPortPurgeInt);
        }
        else
        {
            pRtrPortNode->V3RtrPortPurgeTimer.u4Instance = u4Instance;
            pRtrPortNode->V3RtrPortPurgeTimer.u1TimerType =
                SNOOP_ROUTER_PORT_PURGE_V3_TIMER;
            pRtrPortNode->u4V3RtrPortPurgeInt = u4RtrPortPurgeInt;
            SnoopTmrStartTimer (&pRtrPortNode->V3RtrPortPurgeTimer,
                                u4RtrPortPurgeInt);
            SnoopRedActiveSendV3RtrPortIntrSync (u4Instance,
                                                 pRtrPortNode->u4Port,
                                                 u4RtrPortPurgeInt,
                                                 pRtrPortNode->pVlanEntry);
        }

    }
    if (pRtrPortNode->pVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {

        if (u1TmrVer == SNOOP_MLD_VERSION1)
        {
            pRtrPortNode->V2RtrPortPurgeTimer.u4Instance = u4Instance;
            pRtrPortNode->V2RtrPortPurgeTimer.u1TimerType =
                SNOOP_ROUTER_PORT_PURGE_V2_TIMER;
            SnoopTmrStartTimer (&pRtrPortNode->V2RtrPortPurgeTimer,
                                u4RtrPortPurgeInt);
        }
        else
        {

            pRtrPortNode->V3RtrPortPurgeTimer.u4Instance = u4Instance;
            pRtrPortNode->u4V3RtrPortPurgeInt = u4RtrPortPurgeInt;
            pRtrPortNode->V3RtrPortPurgeTimer.u1TimerType =
                SNOOP_ROUTER_PORT_PURGE_V3_TIMER;
            SnoopTmrStartTimer (&pRtrPortNode->V3RtrPortPurgeTimer,
                                u4RtrPortPurgeInt);
        }
    }
    return;
}

#ifdef CLI_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopGetSwitchName                               */
/*                                                                           */
/*    Description         : This function is used to get the switch name,    */
/*                          this is called from CLI to print the Switch name */
/*                          this function will return the name if the passed */
/*                          instance ID's are different, this is just to take*/
/*                          care switch name is not printed more than once   */
/*                          for each instance                                */
/*                                                                           */
/*    Input(s)            : i4NextInstId - New Insatnce Id.                  */
/*                          i4CurrInstId - Current Insatnce Id               */
/*                                                                           */
/*    Output(s)           : pu1Name - Context Name                           */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopGetSwitchName (INT4 i4NextInstId, INT4 i4CurrInstId, UINT1 *pu1Name)
{
    if (SnoopVcmGetSystemModeExt (SNOOP_PROTOCOL_ID) == VCM_SI_MODE)
    {
        /* To avoid "Switch <switch-name>" print in SI */
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pu1Name, 0, (SNOOP_SWITCH_ALIAS_LEN + 7));
    if (i4NextInstId != i4CurrInstId)
    {
        SNOOP_MEM_CPY (pu1Name, "Switch ", 7);
        SnoopVcmGetAliasName ((UINT4) i4NextInstId, (pu1Name + 7));
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}
#endif /* CLI_WANTED */

/*****************************************************************************/
/* Function Name      : SnoopSendGeneralQueries                              */
/*                                                                           */
/* Description        : This function scan all the snoop enabled outer vlan  */
/*                      and send general query over all the member ports .   */
/*                                                                           */
/* Input(s)           : u4Instance - Context ID.                             */
/*                      u1AddressType - Address type                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT1
SnoopUtilSendGeneralQueries (UINT4 u4Instance, UINT1 u1AddressType)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_MGMT,
                       SNOOP_MGMT_NAME, "Instance not created \r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;
        pRBElem = pRBElemNext;

        if (pSnoopVlanEntry->u1AddressType != u1AddressType)
        {
            continue;
        }

        pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;
        if (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigSnoopStatus
            == SNOOP_DISABLE)
        {
            continue;
        }
        SnoopUtilSendGeneralQuery (u4Instance, u1AddressType, pSnoopVlanEntry,
                                   SNOOP_TRUE);
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopSendGeneralQuery                                */
/*                                                                           */
/* Description        : This function forms and send out general query       */
/*                      over the specified vlan                              */
/*                                                                           */
/* Input(s)           : u4Instance - Context ID.                             */
/*                      u1AddressType - Address type                         */
/*                      pSnoopVlanEntry  - Pointer to Vlan entry             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
INT1
SnoopUtilSendGeneralQuery (UINT4 u4Instance, UINT1 u1AddressType,
                           tSnoopVlanEntry * pSnoopVlanEntry,
                           UINT1 u1StartUpQueryFlag)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT2               u2QueryInterval = 0;
    UINT1              *pPortBitMap = NULL;
    UINT1              *pRtrBitMap = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);
    SNOOP_VALIDATE_ADDRESS_TYPE_RET (u1AddressType, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_GBL_DBG_FLAG, SNOOP_DBG_MGMT,
                       SNOOP_MGMT_NAME, "Instance not created \r\n");
        return SNOOP_SUCCESS;
    }
    if (SNOOP_SNOOPING_STATUS (u4Instance, u1AddressType - 1) == SNOOP_DISABLE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Snoop is disabled. \r\n");
        return SNOOP_SUCCESS;
    }

    if (pSnoopVlanEntry == NULL)
    {
        return SNOOP_FAILURE;
    }

    /* Send general query on the specific vlan */
#ifdef IGS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV4)
    {
        if (IgsEncodeQuery (u4Instance, 0, SNOOP_IGMP_QUERY,
                            pSnoopVlanEntry->pSnoopVlanCfgEntry->
                            u1ConfigOperVer, &pBuf,
                            pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME, "Query Encoding failed\n");

            return SNOOP_FAILURE;
        }
    }
#endif
#ifdef MLDS_WANTED
    if (u1AddressType == SNOOP_ADDR_TYPE_IPV6)
    {
        if (MldsEncodeQuery (u4Instance, NULL, SNOOP_MLD_QUERY,
                             SNOOP_MLD_VERSION2, &pBuf, pSnoopVlanEntry)
            != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME, "Query Encoding failed.\n");
            return SNOOP_FAILURE;
        }
    }
#endif

    /* General query needs to be sent only on non-router ports. So
     * get the Vlan's Member Portlist and subtract the Vlan's
     * Router portlist from member portlist and then send general 
     * query on those ports */
    pRtrBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pRtrBitMap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pRtrBitMap\r\n");
        SYSLOG_SNOOP_MSG_ARG1 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "while sending general query\r\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pRtrBitMap, 0, sizeof (tSnoopPortBmp));

    pPortBitMap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pPortBitMap == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GRP_NAME,
                       "Error in allocating memory for pPortBitMap\r\n");
        UtilPlstReleaseLocalPortList (pRtrBitMap);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (pPortBitMap, 0, sizeof (tSnoopPortBmp));

    SnoopVlanGetRtrPortFromPVlanMappingInfo (u4Instance,
                                             SNOOP_OUTER_VLAN
                                             (pSnoopVlanEntry->VlanId),
                                             u1AddressType, 0, pRtrBitMap,
                                             pSnoopVlanEntry);

    if (SnoopMiGetVlanLocalEgressPorts (u4Instance, pSnoopVlanEntry->VlanId,
                                        pPortBitMap) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                            SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                            SNOOP_VLAN_NAME,
                            "Failed to get the port list for Vlan %d\n",
                            SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        UtilPlstReleaseLocalPortList (pRtrBitMap);
        UtilPlstReleaseLocalPortList (pPortBitMap);
        return SNOOP_FAILURE;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->
        SnoopInfo[pSnoopVlanEntry->u1AddressType - 1].u1QueryFwdAll ==
        SNOOP_FORWARD_NON_RTR_PORTS)
    {
        SNOOP_XOR_PORT_BMP (pPortBitMap, pRtrBitMap);
    }

    SnoopVlanForwardPacket (u4Instance, pSnoopVlanEntry->VlanId,
                            pSnoopVlanEntry->u1AddressType,
                            0, VLAN_FORWARD_SPECIFIC, pBuf,
                            pPortBitMap, SNOOP_QUERY_SENT);
    UtilPlstReleaseLocalPortList (pRtrBitMap);
    UtilPlstReleaseLocalPortList (pPortBitMap);
    SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
    /* Start the Query Timer for Query Interval */
    pSnoopVlanEntry->QueryTimer.u4Instance = u4Instance;
    pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;
    u2QueryInterval = (u1StartUpQueryFlag == SNOOP_TRUE) ?
        SNOOP_STARTUP_QRY_INTERVAL : pSnoopVlanEntry->u2QueryInterval;

    SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer, u2QueryInterval);

    SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                    "Query timer restarted for %d seconds\r\n",
                    pSnoopVlanEntry->u2QueryInterval);
    return SNOOP_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name :     SnoopUtilIsStaticEntry                          */
/*                                                                         */
/*     Description   :     This function get the router port based on the  */
/*                         the physical port index and checks if it is     */
/*                         a statically configured entry or not.           */
/*                                                                         */
/*     Input(s)      :     u4PhyPortIndex - Physical port index            */
/*                         u4VlanId - Vlan Id                              */
/*                         u1AddrType - Address Type                       */
/*                                                                         */
/*     Output(s)     :     None                                            */
/*                                                                         */
/*     Returns       :     TRUE/FALSE                                      */
/*                                                                         */
/***************************************************************************/
INT4
SnoopUtilIsStaticEntry (UINT4 u4PhyPortIndex, UINT4 u4VlanId, UINT1 u1AddrType)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopRtrPortEntry *pRtrPortEntry = NULL;
    tSnoopRtrPortEntry  RtrPortEntry;
    tSnoopVlanEntry     SnoopVlanEntry;
    tSnoopTag           VlanId;
    UINT4               u4LocalPortNum = 0;
    BOOL1               bPortPresent = SNOOP_FALSE;

    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&RtrPortEntry, 0, sizeof (tSnoopRtrPortEntry));
    MEMSET (&SnoopVlanEntry, 0, sizeof (tSnoopVlanEntry));

    RtrPortEntry.u4PhyPortIndex = u4PhyPortIndex;
    SnoopVlanEntry.u1AddressType = u1AddrType;

    SNOOP_OUTER_VLAN (SnoopVlanEntry.VlanId) = (tVlanId) u4VlanId;
    RtrPortEntry.pVlanEntry = &SnoopVlanEntry;

    pRtrPortEntry = (tSnoopRtrPortEntry *) RBTreeGet (gRtrPortTblRBRoot,
                                                      (tRBElem *) &
                                                      RtrPortEntry);

    while (pRtrPortEntry == NULL)
    {
        return (FALSE);
    }

    pSnoopVlanEntry = pRtrPortEntry->pVlanEntry;
    u4LocalPortNum = pRtrPortEntry->u4Port;

    SNOOP_IS_PORT_PRESENT (u4LocalPortNum, pSnoopVlanEntry->StaticRtrPortBitmap,
                           bPortPresent);
    if (bPortPresent == SNOOP_FALSE)
    {
        return (FALSE);
    }

    return (TRUE);
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetNextRegisteredSource                     */
/*                                                                           */
/* Description        : This function returns the next registered source     */
/*                      address registered by the given host.                */
/*                                                                           */
/* Input(s)           : u4Instance      - Instance Id                        */
/*                      pSnoopHostEntry - Pointer to the host entry          */
/*                      pu1SrcAddr      - Source Address Ip                  */
/*                      u1Status        - SNOOP_TRUE / SNOOP_FALSE           */
/*                                                                           */
/* Output(s)          : pu1NextSrcAddr  - Next registered source address     */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilGetNextRegisteredSource (UINT4 u4Instance,
                                  tSnoopHostEntry * pSnoopHostEntry,
                                  tIPvXAddr SrcAddr, UINT1 u1Status,
                                  UINT1 *pu1NextSrcAddr)
{
    tSnoopSrcBmp        SnoopExclBmp;
    tSnoopSrcBmp        HostSrcBmp;
    tSnoopSourceInfo   *pSnoopSrcInfo = NULL;
    tSnoopDllNode      *pSnoopSrcNode = NULL;
    UINT1               au1SrcAddr[IPVX_MAX_INET_ADDR_LEN];
    BOOL1               bResult = OSIX_FALSE;

    SNOOP_MEM_SET (SnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_CPY (HostSrcBmp, gNullSrcBmp, SNOOP_SRC_LIST_SIZE);
    SNOOP_MEM_SET (au1SrcAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (SNOOP_MEM_CMP (pSnoopHostEntry->pSourceBmp->ExclSrcBmp, gNullSrcBmp,
                       SNOOP_SRC_LIST_SIZE) == 0)
    {
        if (u1Status == SNOOP_TRUE)
        {
            /* Mode is Excl None. Hence return Null Src Addr */
            SNOOP_MEM_CPY (pu1NextSrcAddr, au1SrcAddr, IPVX_MAX_INET_ADDR_LEN);
            return SNOOP_SUCCESS;
        }
        else
        {
            /* The Host has sent a Excl NOne and the Source (0.0.0.0)
             * has been already displayed */
            return SNOOP_FAILURE;
        }
    }
    if (SNOOP_MEM_CMP (pSnoopHostEntry->pSourceBmp->ExclSrcBmp, SnoopExclBmp,
                       SNOOP_SRC_LIST_SIZE) == 0)
    {
        SNOOP_MEM_CPY (HostSrcBmp, pSnoopHostEntry->pSourceBmp->InclSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
    }
    else
    {
        SNOOP_MEM_CPY (HostSrcBmp, pSnoopHostEntry->pSourceBmp->ExclSrcBmp,
                       SNOOP_SRC_LIST_SIZE);
    }

    SNOOP_DLL_SCAN (&(SNOOP_SOURCE_LIST (u4Instance)),
                    pSnoopSrcNode, tSnoopDllNode *)
    {
        pSnoopSrcInfo = (tSnoopSourceInfo *) pSnoopSrcNode;

        if (IPVX_ADDR_COMPARE (SrcAddr, pSnoopSrcInfo->SrcIpAddr) >= 0)
        {
            continue;
        }
        OSIX_BITLIST_IS_BIT_SET (HostSrcBmp, (pSnoopSrcInfo->u2NodeIndex + 1),
                                 SNOOP_SRC_LIST_SIZE, bResult);
        if (bResult == OSIX_TRUE)
        {
            SNOOP_MEM_CPY (pu1NextSrcAddr, pSnoopSrcInfo->SrcIpAddr.au1Addr,
                           IPVX_MAX_INET_ADDR_LEN);
            return SNOOP_SUCCESS;
        }
    }

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilRBTreeHostEntryCmp                          */
/*                                                                           */
/* Description        : This routine is used in Host Table for comparing     */
/*                      two keys used in RBTree functionality.               */
/*                                                                           */
/* Input(s)           : HostEntryNode - Key 1                                */
/*                      HostEntryIn   - Key 2                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
SnoopUtilRBTreeHostEntryCmp (tRBElem * HostEntryNode, tRBElem * HostEntryIn)
{
    tSnoopGroupEntry   *pGroupEntryNode = NULL;
    tSnoopGroupEntry   *pGroupEntryIn = NULL;
    UINT4               u4PortNode, u4PortIn;
    tSnoopHostEntry    *pHostEntryNode = NULL;
    tSnoopHostEntry    *pHostEntryIn = NULL;
    UINT1               au1EntryNodeIp[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1EntryInIp[IPVX_MAX_INET_ADDR_LEN];

    SNOOP_MEM_SET (au1EntryNodeIp, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1EntryInIp, 0, IPVX_MAX_INET_ADDR_LEN);

    pHostEntryNode = (tSnoopHostEntry *) HostEntryNode;
    pHostEntryIn = (tSnoopHostEntry *) HostEntryIn;

    pGroupEntryNode = pHostEntryNode->pPortEntry->pGroupEntry;
    pGroupEntryIn = pHostEntryIn->pPortEntry->pGroupEntry;

    /* The u4Port in the SSMPortEntry and ASMPortEntry should be 
     * in the same position as the both the ASM hosts and SSM hosts
     * are added to the same RBTree and the Port Index, an index
     * to the RBTree is accessed via only SSMPortEntry for accessing
     * the host node. */

    u4PortNode = pHostEntryNode->pPortEntry->u4Port;
    u4PortIn = pHostEntryIn->pPortEntry->u4Port;

    /* Compare the Outer VLAN ID */
    if (SNOOP_OUTER_VLAN (pGroupEntryNode->VlanId) <
        SNOOP_OUTER_VLAN (pGroupEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_OUTER_VLAN (pGroupEntryNode->VlanId) >
             SNOOP_OUTER_VLAN (pGroupEntryIn->VlanId))
    {
        return 1;
    }

    /* Compare the address type index */
    if (pGroupEntryNode->GroupIpAddr.u1Afi < pGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return -1;
    }
    else if (pGroupEntryNode->GroupIpAddr.u1Afi >
             pGroupEntryIn->GroupIpAddr.u1Afi)
    {
        return 1;
    }

    /* Compare the group ip address */
    SNOOP_MEM_CPY (au1EntryNodeIp, pGroupEntryNode->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1EntryNodeIp);

    SNOOP_MEM_CPY (au1EntryInIp, pGroupEntryIn->GroupIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1EntryInIp);

    if (SNOOP_MEM_CMP (au1EntryNodeIp, au1EntryInIp,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1EntryNodeIp, au1EntryInIp,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    /* Compare the Inner VLAN ID */
    if (SNOOP_INNER_VLAN (pGroupEntryNode->VlanId) <
        SNOOP_INNER_VLAN (pGroupEntryIn->VlanId))
    {
        return -1;
    }
    else if (SNOOP_INNER_VLAN (pGroupEntryNode->VlanId) >
             SNOOP_INNER_VLAN (pGroupEntryIn->VlanId))
    {
        return 1;
    }

    /* Compare the port index */
    if (u4PortNode < u4PortIn)
    {
        return -1;
    }
    else if (u4PortNode > u4PortIn)
    {
        return 1;
    }

    /* Compare the host ip address */
    SNOOP_MEM_CPY (au1EntryNodeIp, pHostEntryNode->HostIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1EntryNodeIp);

    SNOOP_MEM_CPY (au1EntryInIp, pHostEntryIn->HostIpAddr.au1Addr,
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_HTONL (au1EntryInIp);

    if (SNOOP_MEM_CMP (au1EntryNodeIp, au1EntryInIp,
                       IPVX_MAX_INET_ADDR_LEN) < 0)
    {
        return -1;
    }
    else if (SNOOP_MEM_CMP (au1EntryNodeIp, au1EntryInIp,
                            IPVX_MAX_INET_ADDR_LEN) > 0)
    {
        return 1;
    }

    return 0;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilAddSource                                   */
/*                                                                           */
/* Description        : This function adds the given registered source       */
/*                      to the doubly linked list in sorted order.           */
/*                                                                           */
/* Input(s)           : u4Instance        - Instance Id                      */
/*                      SnoopSourceInfoIn - Source Ip Address to be added    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilAddSource (UINT4 u4Instance, tSnoopSourceInfo * pSnoopSourceInfoIn)
{
    tSnoopSourceInfo   *pSnoopSourceInfo = NULL;
    tSnoopDllNode      *pSnoopSrcNode = NULL;
    tSnoopSourceInfo   *pSnoopSrcPrevNode = NULL;
    UINT1               u1NextSrcFound = SNOOP_FALSE;

    SNOOP_DLL_SCAN (&(SNOOP_SOURCE_LIST (u4Instance)),
                    pSnoopSrcNode, tSnoopDllNode *)
    {
        pSnoopSourceInfo = (tSnoopSourceInfo *) pSnoopSrcNode;

        if (IPVX_ADDR_COMPARE (pSnoopSourceInfoIn->SrcIpAddr,
                               pSnoopSourceInfo->SrcIpAddr) < 0)
        {
            u1NextSrcFound = SNOOP_TRUE;
            break;
        }

    }

    if (u1NextSrcFound == SNOOP_FALSE)
    {
        SNOOP_DLL_ADD (&(SNOOP_SOURCE_LIST (u4Instance)),
                       &(pSnoopSourceInfoIn->SourceNode));

        return SNOOP_SUCCESS;
    }

    pSnoopSrcPrevNode =
        (tSnoopSourceInfo *) SNOOP_DLL_PREV (&(SNOOP_SOURCE_LIST (u4Instance)),
                                             &(pSnoopSourceInfo->SourceNode));

    if (pSnoopSrcPrevNode == NULL)
    {
        SNOOP_DLL_INSERT_IN_MIDDLE (&(SNOOP_SOURCE_LIST (u4Instance)),
                                    &(SNOOP_SOURCE_LIST (u4Instance).Head),
                                    &(pSnoopSourceInfoIn->SourceNode),
                                    &(pSnoopSourceInfo->SourceNode));
    }
    else
    {
        SNOOP_DLL_INSERT_IN_MIDDLE (&(SNOOP_SOURCE_LIST (u4Instance)),
                                    &(pSnoopSrcPrevNode->SourceNode),
                                    &(pSnoopSourceInfoIn->SourceNode),
                                    &(pSnoopSourceInfo->SourceNode));
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilAddSourceInfo                               */
/*                                                                           */
/* Description        : This function adds the given source port information */
/*                      for the group in its corresponding group entry.      */
/*                                                                           */
/* Input(s)           : u4Instance        - Instance Id                      */
/*                      pSnoopPktInfo     - Packet Information               */
/*                      VlanId            - Vlan Identifier                  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilAddSourceInfo (UINT4 u4Instance, tSnoopPktInfo * pSnoopPktInfo,
                        tSnoopTag VlanId)
{
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Port = 0;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* Check if group entry is present or not */
    if (SnoopGrpGetGroupEntry (u4Instance, VlanId, pSnoopPktInfo->DestIpAddr,
                               &pSnoopGroupEntry) != SNOOP_SUCCESS)
    {
        /* Create Group entry */
        if (SnoopGrpCreateGroupEntry (u4Instance, VlanId,
                                      pSnoopPktInfo->DestIpAddr, pSnoopPktInfo,
                                      NULL, 0, 0,
                                      &pSnoopGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_GRP_NAME,
                           "New Group entry creation failed\r\n");
            return SNOOP_FAILURE;
        }
#ifdef NPAPI_WANTED
        u4GrpAddr = SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->GroupIpAddr.au1Addr);
        SNOOP_GET_MAC_FROM_IPV4 (u4GrpAddr, GroupMacAddr);
        if (VlanMiUpdateDynamicMcastInfo (u4Instance, GroupMacAddr,
                                          SNOOP_OUTER_VLAN (VlanId), u4Port,
                                          VLAN_DROP_UNKNOWN_MCAST) !=
            VLAN_SUCCESS)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DATA_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_PKT_NAME,
                           "Failed to add Group entry in hardware\n");
            return SNOOP_FAILURE;
        }
#endif

        pSnoopGroupEntry->b1InGroupSourceInfo = SNOOP_TRUE;

        /* Start the group specific source information timer */
        pSnoopGroupEntry->InGrpSrcTimer.u4Instance = u4Instance;
        pSnoopGroupEntry->InGrpSrcTimer.u1TimerType =
            SNOOP_IN_GROUP_SOURCE_TIMER;

        SnoopTmrStartTimer (&pSnoopGroupEntry->InGrpSrcTimer,
                            SNOOP_DEF_IN_GRP_SRC_INTERVAL);

        SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_TMR_DBG, SNOOP_TMR_DBG,
                        "Group specific source information  timer started for %d seconds\r\n",
                        SNOOP_DEF_IN_GRP_SRC_INTERVAL);
    }
    else
    {
        pSnoopGroupEntry->b1InGroupSourceInfo = SNOOP_TRUE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilCleanUpSrcList                              */
/*                                                                           */
/* Description        : This function will clears the Unused Source List     */
/*             address from the source List array. This function    */
/*             will be called by the timerNode                      */
/*                                                                           */
/* Input(s)           : pExpiredTmr       - Reference for the Expired Timer  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopUtilCleanUpSrcList (tSnoopTmrNode * pExpiredTmr)
{
    UINT2               u2Count;
    UINT4               u4Instance;
    tIPvXAddr           tempSrcAddr;
    UINT1               u1EntryUsed;

    SNOOP_MEM_SET (&tempSrcAddr, 0, sizeof (tIPvXAddr));
    u4Instance = pExpiredTmr->u4Instance;

    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    /* Before decrementing the group reference count check if this
     * source is not used by any other group */
    for (u2Count = 1; u2Count <= SNOOP_MAX_MCAST_SRCS; u2Count++)
    {
        if ((SNOOP_MEM_CMP (&(SNOOP_SRC_INFO (u4Instance,
                                              (u2Count - 1)).SrcIpAddr),
                            &tempSrcAddr, sizeof (tIPvXAddr)) == 0) ||
            (SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount == 0))
        {
            continue;
        }

        SnoopUtilCheckIsSourceUsed (u4Instance, u2Count, &u1EntryUsed);
        if (u1EntryUsed == SNOOP_FALSE)
        {
            SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount--;
            if (SNOOP_SRC_INFO (u4Instance, (u2Count - 1)).u2GrpRefCount == 0)
            {
                /* Remove the source from the linked list */
                SNOOP_REMOVE_SOURCE (u4Instance,
                                     SNOOP_SRC_INFO (u4Instance,
                                                     (u2Count - 1)));

                SNOOP_MEM_SET (&(SNOOP_SRC_INFO (u4Instance,
                                                 (u2Count - 1)).SrcIpAddr),
                               0, sizeof (tIPvXAddr));
                gu2SourceCount--;
            }
        }
    }

    SNOOP_INSTANCE_INFO (u4Instance)->SrcListCleanUpTmr.u1TimerType =
        SNOOP_SRC_LIST_CLEANUP_TIMER;
    if (SnoopTmrStartTimer
        (&SNOOP_INSTANCE_INFO (u4Instance)->SrcListCleanUpTmr,
         (SNOOP_DEF_PORT_PURGE_INTERVAL / 10)) == SNOOP_FAILURE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME, "Failure in starting SourceList cleanup"
                       "timer \r\n");
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetFirstPortCfg                             */
/*                                                                           */
/* Description        : This function retrives the first indices of the      */
/*                      global port configuration tree.                      */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : pi4IfIndex - Interface Index                         */
/*                      pu4InnerVlanId - Inner Vlan Id                       */
/*                      pi4AddressType - Address Type                        */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilGetFirstPortCfg (INT4 *pi4IfIndex, UINT4 *pu4InnerVlanId,
                          INT4 *pi4AddressType)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;

    pSnoopPortCfgEntry = (tSnoopPortCfgEntry *)
        RBTreeGetFirst (SNOOP_PORT_CFG_GLB_RBTREE);

    if (pSnoopPortCfgEntry != NULL)
    {
        *pi4IfIndex = pSnoopPortCfgEntry->u4SnoopPortIndex;
        *pu4InnerVlanId = SNOOP_INNER_VLAN (pSnoopPortCfgEntry->VlanId);
        *pi4AddressType = (INT4) pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype;

        return SNOOP_SUCCESS;
    }

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetNextPortCfg                              */
/*                                                                           */
/* Description        : This function retrives the next available indices    */
/*                       of the global port configuration tree.              */
/*                                                                           */
/* Input(s)           : i4CurrIfIndex - Interface Index                      */
/*                      u4CurrInnerVlanId - Inner Vlan Id                    */
/*                      i4CurrAddressType - Address Type                     */
/*                                                                           */
/* Output(s)          : pi4NextIfIndex                                       */
/*                      pu4NextInnerVlanId                                   */
/*                      pi4NextAddressType                                   */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilGetNextPortCfg (INT4 i4CurrIfIndex, INT4 *pi4NextIfIndex,
                         UINT4 u4CurrInnerVlanId, UINT4 *pu4NextInnerVlanId,
                         INT4 i4CurrAddressType, INT4 *pi4NextAddressType)
{
    tSnoopPortCfgEntry *pSnoopPortCfgEntry = NULL;
    tSnoopPortCfgEntry  SnoopPortCfgEntry;

    SNOOP_MEM_SET (&SnoopPortCfgEntry, 0, sizeof (tSnoopPortCfgEntry));

    SnoopPortCfgEntry.u4SnoopPortIndex = (UINT4) i4CurrIfIndex;
    SNOOP_INNER_VLAN (SnoopPortCfgEntry.VlanId) = (UINT2) u4CurrInnerVlanId;
    SnoopPortCfgEntry.u1SnoopPortCfgInetAddrtype = (UINT1) i4CurrAddressType;

    pSnoopPortCfgEntry = RBTreeGetNext (SNOOP_PORT_CFG_GLB_RBTREE,
                                        (tRBElem *) & SnoopPortCfgEntry, NULL);

    if (pSnoopPortCfgEntry != NULL)
    {
        *pi4NextIfIndex = pSnoopPortCfgEntry->u4SnoopPortIndex;
        *pu4NextInnerVlanId = SNOOP_INNER_VLAN (pSnoopPortCfgEntry->VlanId);
        *pi4NextAddressType =
            (INT4) pSnoopPortCfgEntry->u1SnoopPortCfgInetAddrtype;

        return SNOOP_SUCCESS;
    }

    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopTestPortMaxLimit                                */
/*                                                                           */
/* Description        : This function does the validation for port limit     */
/*                      type and threshold limit. This is to be called       */
/*                      from CLI and WEB as limit type and limit will be     */
/*                      configured at the same time.                         */
/*                      Changes done here should ber reflected in limit      */
/*                      type and limit test routines                         */
/*                                                                           */
/* Input(s)           : u4Index       - Port index                           */
/*                      u1LimitType   - Limit type                           */
/*                      u4Limit       - Limit                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/

INT4
SnoopTestPortMaxLimit (UINT4 u4Index, UINT1 u1LimitType, UINT4 u4Limit)
{
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    UINT4               u4Instance = 0;
    UINT2               u2LocalPort = 0;

    if ((u4Index <= 0) || (u4Index > SNOOP_MAX_PORTS))
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME, "Invalid Port Number \r\n");
        return SNOOP_FAILURE;
    }

    u4Instance = SNOOP_GLOBAL_INSTANCE (u4Index);
    u2LocalPort = SNOOP_GLOBAL_PORT (u4Index);

    if (u4Instance >= SNOOP_MAX_INSTANCES)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME, "Invalid instance ID \r\n");
        CLI_SET_ERR (CLI_SNOOP_INVALID_INSTANCE_ERR);
        return SNOOP_FAILURE;
    }

    if (u2LocalPort == 0)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME,
                       "Port is not mapped to any instance \r\n");
        CLI_SET_ERR (CLI_SNOOP_INVALID_INSTANCE_ERR);
        return SNOOP_FAILURE;
    }

    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

    if (pSnoopInstInfo == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME, "Instance not created \r\n");
        CLI_SET_ERR (CLI_SNOOP_INVALID_INSTANCE_ERR);
        return SNOOP_FAILURE;
    }

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_MGMT_NAME, "Snooping module not started \r\n");
        CLI_SET_ERR (CLI_SNOOP_MODULE_SHUT_DOWN_ERR);
        return SNOOP_FAILURE;
    }

    /* If the limit type is groups, then limit should be less than maximum
     * groups supported by the system. If the limit type is channels, then
     * limit should be les than the maximum channels supported by the system.
     * If the limit type is none, then the limit should be less than the
     * maximum of maximum groups and maximum channels supported by the
     * system
     */
    if ((u1LimitType == SNOOP_PORT_LMT_TYPE_GROUPS) &&
        (u4Limit > SNOOP_MAX_MCAST_GROUPS))
    {
        CLI_SET_ERR (CLI_SNOOP_PORT_GRP_MAX_LMT_TYPE_ERR);
        return SNOOP_FAILURE;
    }
    else if ((u1LimitType == SNOOP_PORT_LMT_TYPE_CHANNELS) &&
             (u4Limit > SNOOP_MAX_MCAST_CHANNELS))
    {
        CLI_SET_ERR (CLI_SNOOP_PORT_CHN_MAX_LMT_TYPE_ERR);
        return SNOOP_FAILURE;
    }
    else if ((u1LimitType == SNOOP_PORT_LMT_TYPE_NONE) &&
             (u4Limit > SNOOP_MAX_MCAST_CHANNELS))
    {
        CLI_SET_ERR (CLI_SNOOP_PORT_MAX_LMT_ERR);
        return SNOOP_FAILURE;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilIsLocalPortListValid                        */
/*                                                                           */
/* Description        : This function validates the given local port list.   */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      pu1Ports   - PortList to be validated                */
/*                      i4Length - Length of the list                        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE /SNOOP_FALSE                              */
/*****************************************************************************/
UINT1
SnoopUtilIsLocalPortListValid (UINT4 u4Instance, UINT1 *pu1Ports, INT4 i4Length)
{
    UINT4               u4Port = SNOOP_INIT_VAL;
    UINT4               u2LocalPort = SNOOP_INIT_VAL;
    UINT2               u2Index = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;

    if (pu1Ports == NULL)
    {
        return SNOOP_TRUE;
    }

    for (u2Index = 0; u2Index < i4Length; u2Index++)
    {
        if (pu1Ports[u2Index] != 0)
        {
            u1PortFlag = pu1Ports[u2Index];

            for (u2BitIndex = 0;
                 ((u2BitIndex < SNOOP_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & SNOOP_BIT8) != 0)
                {
                    u2LocalPort = (UINT4) ((u2Index * SNOOP_PORTS_PER_BYTE) +
                                           u2BitIndex + 1);

                    u4Port = SNOOP_GET_IFINDEX (u4Instance, u2LocalPort);

                    if (u4Port == SNOOP_INIT_VAL)
                    {
                        return SNOOP_FALSE;
                    }

                    if (SnoopL2IwfIsPortInPortChannel (u4Port) == L2IWF_SUCCESS)
                    {
                        return SNOOP_FALSE;
                    }
                }

                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /* for each bit in the given byte */
        }
    }

    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : SnoopGetPhyPortBmp                                   */
/*                                                                           */
/* Description        : This function returns the physical port bitmap       */
/*                      associated with the virtual port bitmap for a        */
/*                      instance                                             */
/*                                                                           */
/* Input(s)           : u4Instance - Instance number                         */
/*                    : PortBitmap - Vitual port bitmap                      */
/*                                                                           */
/* Output(s)          : PhyPortBitmap - Physical port bitmap                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
INT4
SnoopGetPhyPortBmp (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                    tSnoopIfPortBmp PhyPortBitmap)
{
    UINT4               u4PhyPort = SNOOP_INIT_VAL;
    UINT2               u2Port = SNOOP_INIT_VAL;
    UINT2               u2BytIndex = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* stores physical ports info for local port */
                    u4PhyPort = SNOOP_GET_IFINDEX (u4Instance, u2Port);

                    u4PhyPort = SNOOP_GET_PHY_PORT (u4PhyPort);

                    if (u4PhyPort != 0)
                    {
                        OSIX_BITLIST_SET_BIT (PhyPortBitmap, (UINT2) u4PhyPort,
                                              SNOOP_IF_PORT_LIST_SIZE);
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopDeriveSispDefaults                              */
/*                                                                           */
/* Description        : This function is used to derive the defaults for SISP*/
/*                      logical interfaces.                                  */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopDeriveSispDefaults (UINT4 u4IfIndex)
{
    BOOL1               bResult = SNOOP_FALSE;
    UINT4               u4PhyIndex = SNOOP_INIT_VAL;

    SNOOP_IS_SISP_PORT (u4IfIndex, bResult);

    /*If this port is SISP logical interface, store the corrosponding
       physical interface to which it is mapped */

    if (bResult == SNOOP_TRUE)
    {
        if (SnoopVcmSispGetPhysicalPortOfSispPort (u4IfIndex, &u4PhyIndex)
            != VCM_SUCCESS)
        {
            return SNOOP_FAILURE;
        }

        SNOOP_GET_PHY_PORT (u4IfIndex) = u4PhyIndex;
    }
    else
    {
        if (u4IfIndex >= SNOOP_MAX_PORTS)
        {
            return SNOOP_FAILURE;
        }
        SNOOP_GET_PHY_PORT (u4IfIndex) = u4IfIndex;
    }

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilIsLocalPortListVlanMember                   */
/*                                                                           */
/* Description        : This function validates the given local port list    */
/*                      and checks whether the given ports in the list       */
/*                      are member ports of given vlan                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      i4VlanId   - Vlan Id                                 */
/*                      pu1Ports   - Local PortList to be validated          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE /SNOOP_FALSE                              */
/*****************************************************************************/
UINT1
SnoopUtilIsLocalPortListVlanMember (UINT4 u4Instance, INT4 i4VlanId,
                                    UINT1 *pu1Ports)
{
    tSnoopTag           VlanId;
    UINT1              *pEgressPortBitmap = NULL;
    UINT1              *pVlanPortBitmap = NULL;

    pEgressPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pEgressPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pEgressPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return SNOOP_FALSE;
    }
    SNOOP_MEM_SET (pEgressPortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    /* Get the member ports of the VLAN */
    SNOOP_OUTER_VLAN (VlanId) = (UINT2) i4VlanId;

    if (SnoopMiGetVlanLocalEgressPorts (u4Instance, VlanId, pEgressPortBitmap)
        == SNOOP_FAILURE)
    {
        UtilPlstReleaseLocalPortList (pEgressPortBitmap);
        return SNOOP_FALSE;
    }

    /* Do AND operation between the given port list (pu1Ports) and obtained
     * VLAN member ports (EgressPortBitmap) and store in VlanPortBitmap.
     * If the given port list are member ports of vlan, then VlanPortBitmap and
     * pu1Ports should be same
     */

    pVlanPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pVlanPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pVlanPortBitmap\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        UtilPlstReleaseLocalPortList (pEgressPortBitmap);
        return SNOOP_FALSE;
    }
    SNOOP_MEM_SET (pVlanPortBitmap, 0, sizeof (tSnoopPortBmp));
    MEMCPY (pVlanPortBitmap, pu1Ports, sizeof (tSnoopPortBmp));

    SNOOP_AND_LOCAL_PORT_BMP (pVlanPortBitmap, pEgressPortBitmap);

    UtilPlstReleaseLocalPortList (pEgressPortBitmap);
    if (MEMCMP (pu1Ports, pVlanPortBitmap, SNOOP_PORT_LIST_SIZE) != 0)
    {
        UtilPlstReleaseLocalPortList (pVlanPortBitmap);
        return SNOOP_FALSE;
    }
    UtilPlstReleaseLocalPortList (pVlanPortBitmap);

    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilIsPortListVlanMember                        */
/*                                                                           */
/* Description        : This function validates the given port list          */
/*                      and checks whether the given ports in the list       */
/*                      are member ports of given vlan                       */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      i4VlanId   - Vlan Id                                 */
/*                      pu1Ports   - PortList to be validated                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE /SNOOP_FALSE                              */
/*****************************************************************************/
UINT1
SnoopUtilIsPortListVlanMember (UINT4 u4Instance, INT4 i4VlanId, UINT1 *pu1Ports)
{
    tSnoopTag           VlanId;
    tSnoopIfPortBmp    *pPhyPortList = NULL;
    tSnoopIfPortBmp    *pVlanPhyPortList = NULL;

    pPhyPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pPhyPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                       SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        return SNOOP_FALSE;
    }

    pVlanPhyPortList =
        (tSnoopIfPortBmp *) FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));

    if (pVlanPhyPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                       SNOOP_OS_RES_NAME,
                       "Error in Allocating memory for bitlist \r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for i4VlanId %d\r\n", i4VlanId);
        FsUtilReleaseBitList ((UINT1 *) pPhyPortList);
        return SNOOP_FALSE;
    }

    SNOOP_MEM_SET (*pPhyPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (*pVlanPhyPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    /* Get the member ports of the VLAN */
    SNOOP_OUTER_VLAN (VlanId) = (UINT2) i4VlanId;
    if (SnoopMiGetVlanEgressPorts (u4Instance, VlanId, *pVlanPhyPortList)
        == SNOOP_FAILURE)
    {
        FsUtilReleaseBitList ((UINT1 *) pPhyPortList);
        FsUtilReleaseBitList ((UINT1 *) pVlanPhyPortList);
        return SNOOP_FALSE;
    }

    /* Do AND operation between the given port list (pu1Ports) and obtained
     * VLAN member ports (pVlanPhyPortList) and store in au1PhyPortList.
     * If the given port list are member ports of vlan, then pPhyPortList and
     * pu1Ports should be same
     */
    MEMCPY (pPhyPortList, pu1Ports, SNOOP_IF_PORT_LIST_SIZE);

    SNOOP_AND_PHY_PORT_BMP ((*pPhyPortList), (*pVlanPhyPortList));

    if (MEMCMP (pu1Ports, pPhyPortList, SNOOP_IF_PORT_LIST_SIZE) != 0)
    {
        FsUtilReleaseBitList ((UINT1 *) pPhyPortList);
        FsUtilReleaseBitList ((UINT1 *) pVlanPhyPortList);
        return SNOOP_FALSE;
    }

    FsUtilReleaseBitList ((UINT1 *) pPhyPortList);
    FsUtilReleaseBitList ((UINT1 *) pVlanPhyPortList);
    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilHandleSparseModeChange                      */
/*                                                                           */
/* Description        : This function clean-up the group specific source     */
/*             information learnt during sparse mode                */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopUtilHandleSparseModeChange (UINT4 u4Instance)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
#ifdef NPAPI_WANTED
    tMacAddr            GroupMacAddr;
    UINT4               u4GrpAddr = 0;
    UINT4               u4Port = 0;

    SNOOP_MEM_SET (&GroupMacAddr, 0, sizeof (tMacAddr));
#endif

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        return SNOOP_SUCCESS;
    }

    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        return SNOOP_SUCCESS;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "No Group entries found \r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    if (pRBElem == NULL)
    {
        return SNOOP_SUCCESS;
    }

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        if ((SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                            gNullPortBitMap, SNOOP_PORT_LIST_SIZE) == 0) &&
            (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE))
        {
            /* Delete the group node and free memblock */
            if (SnoopGrpDeleteGroupEntry (u4Instance, pSnoopGroupEntry->VlanId,
                                          pSnoopGroupEntry->GroupIpAddr) !=
                SNOOP_SUCCESS)
            {

                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_GRP,
                               SNOOP_GRP_NAME,
                               "Deletion of Group record failed\r\n");
                return SNOOP_FAILURE;
            }

#ifdef NPAPI_WANTED
            u4GrpAddr =
                SNOOP_PTR_FETCH_4 (pSnoopGroupEntry->GroupIpAddr.au1Addr);
            SNOOP_GET_MAC_FROM_IPV4 (u4GrpAddr, GroupMacAddr);
            if (VlanMiUpdateDynamicMcastInfo (u4Instance, GroupMacAddr,
                                              SNOOP_OUTER_VLAN
                                              (pSnoopGroupEntry->VlanId),
                                              u4Port,
                                              VLAN_CLEAR_UNKNOWN_MCAST) !=
                VLAN_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DATA_PATH_TRC | SNOOP_DBG_GRP |
                           SNOOP_DBG_ALL_FAILURE, SNOOP_GRP_DBG,
                           "Failed to remove Group entry in hardware\n");
                return SNOOP_FAILURE;
            }
#endif

            /* Reset the Group source information bit */
            pSnoopGroupEntry->b1InGroupSourceInfo = SNOOP_FALSE;

            /* Stop the group specific source information timer
             * timer if it is running */
            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "Stopped Source specific information Timer \r\n");
            }
        }
        else if (pSnoopGroupEntry->b1InGroupSourceInfo == SNOOP_TRUE)
        {
            /* Reset the Group source information bit */
            pSnoopGroupEntry->b1InGroupSourceInfo = SNOOP_FALSE;

            /* Stop the group specific source information timer
             * timer if it is running */
            if (pSnoopGroupEntry->InGrpSrcTimer.u1TimerType !=
                SNOOP_INVALID_TIMER_TYPE)
            {
                SnoopTmrStopTimer (&pSnoopGroupEntry->InGrpSrcTimer);
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_TMR_DBG,
                           SNOOP_TMR_DBG,
                           "Stopped Source specific information Timer \r\n");
            }
        }

        pRBElem = pRBElemNext;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetNonEdgePortList                          */
/*                                                                           */
/* Description        : This function validates the given local port list    */
/*                      and checks whether the given ports in the list       */
/*                      are non edge ports.                                  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                      PortBitmap - Snoop Port bitmap                       */
/*                      NonEdgePortBitmap - Non edge PortBit map             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE                                           */
/*****************************************************************************/
UINT1
SnoopUtilGetNonEdgePortList (UINT4 u4Instance, tSnoopPortBmp PortBitmap,
                             tSnoopPortBmp NonEdgePortBitmap)
{
    UINT4               u4NonEdgePort = SNOOP_INIT_VAL;
    UINT2               u2Port = SNOOP_INIT_VAL;
    UINT2               u2BytIndex = SNOOP_INIT_VAL;
    UINT2               u2BitIndex = SNOOP_INIT_VAL;
    UINT1               u1PortFlag = SNOOP_INIT_VAL;
    BOOL1               bOperEdge = OSIX_FALSE;

    SNOOP_MEM_SET (NonEdgePortBitmap, 0, SNOOP_PORT_LIST_SIZE);

    for (u2BytIndex = 0; u2BytIndex < SNOOP_PORT_LIST_SIZE; u2BytIndex++)
    {
        u1PortFlag = PortBitmap[u2BytIndex];

        for (u2BitIndex = 0; ((u2BitIndex < SNOOP_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & SNOOP_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2BytIndex * SNOOP_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (u2Port <= SNOOP_MAX_PORTS_PER_INSTANCE)
                {
                    /* stores physical ports info for local port */
                    u4NonEdgePort = SNOOP_GET_IFINDEX (u4Instance, u2Port);

                    if (u4NonEdgePort != 0)
                    {
                        L2IwfGetPortOperEdgeStatus (u4NonEdgePort, &bOperEdge);
                        if (bOperEdge == OSIX_FALSE)
                        {
                            OSIX_BITLIST_SET_BIT (NonEdgePortBitmap, u2Port,
                                                  SNOOP_PORT_LIST_SIZE);
                        }
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return SNOOP_TRUE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateFirstQueryRespStatus                  */
/*                                                                           */
/* Description        : This function updates the First query response flag  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS / SNOOP_FAILURE                        */
/*****************************************************************************/
INT4
SnoopUtilUpdateFirstQueryRespStatus (UINT4 u4Instance)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBElemNext = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_VALIDATE_INSTANCE_RET (u4Instance, SNOOP_FAILURE);

    if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)
    {
        return SNOOP_SUCCESS;
    }

    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        return SNOOP_SUCCESS;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "No Group entries found \r\n");
        return SNOOP_SUCCESS;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry);

    if (pRBElem == NULL)
    {
        return SNOOP_SUCCESS;
    }

    while (pRBElem != NULL)
    {
        pRBElemNext =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                           pRBElem, NULL);

        pSnoopGroupEntry = (tSnoopGroupEntry *) pRBElem;

        if (SNOOP_MEM_CMP (pSnoopGroupEntry->PortBitmap,
                           gNullPortBitMap, SNOOP_PORT_LIST_SIZE) != 0)
        {
            pSnoopGroupEntry->u1FirstQueryRes = SNOOP_TRUE;
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                       "Updated the First Query Response Flag \r\n");
        }

        pRBElem = pRBElemNext;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateAggStatusInHW                         */
/*                                                                           */
/* Description        : This function updates Aggregation status in HW.      */
/*                                                                           */
/* Input(s)           : u4Instance        - Instance Id                      */
/*                      u2AggId           - Agg Id                           */
/*                      u4Port            - Aggregated Port Id               */
/*                      u1Status          - ADD/DEL port                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT1
SnoopUtilUpdateAggStatusInHW (UINT4 u4Instance,
                              UINT2 u2AggId, UINT4 u4Port, UINT1 u1Status)
{
#ifdef NPAPI_WANTED

    tSNMP_OCTET_STRING_TYPE McastFwdPortList;
    tSNMP_OCTET_STRING_TYPE CurrSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE NextSrcIpAddr;
    tSNMP_OCTET_STRING_TYPE CurrGrpIpAddr;
    tSNMP_OCTET_STRING_TYPE NextGrpIpAddr;
    tSnoopPortBmp       FwdPortBitmap;

    UINT4               u4CurrVlanId = 0;
    UINT4               u4NextVlanId = 0;
    UINT4               u4CurrInnerVlanId = 0;
    UINT4               u4NextInnerVlanId = 0;
    INT4                i4CurrInstId = (INT4) SNOOP_CLI_INVALID_CONTEXT;
    INT4                i4NextInstId = 0;
    INT4                i4CurrAddrType = 0;
    INT4                i4NextAddrType = 0;
    UINT1               au1CurrSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1CurrGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextSrcIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1NextGrpIpAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1McastPortList[SNOOP_IF_PORT_LIST_SIZE];
    BOOL1               bResult = OSIX_FALSE;
    tIPvXAddr           SrcIpAddr;
    tIPvXAddr           GroupIpAddr;
    tSnoopTag           VlanId;

    SNOOP_MEM_SET (&GroupIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&SrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&FwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (au1McastPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (au1CurrSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1CurrGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
    SNOOP_MEM_SET (au1NextGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    McastFwdPortList.pu1_OctetList = au1McastPortList;
    McastFwdPortList.i4_Length = SNOOP_IF_PORT_LIST_SIZE;

    CurrSrcIpAddr.pu1_OctetList = au1CurrSrcIpAddr;
    CurrSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    CurrGrpIpAddr.pu1_OctetList = au1CurrGrpIpAddr;
    CurrGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextSrcIpAddr.pu1_OctetList = au1NextSrcIpAddr;
    NextSrcIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    NextGrpIpAddr.pu1_OctetList = au1NextGrpIpAddr;
    NextGrpIpAddr.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (nmhGetFirstIndexFsSnoopVlanIpFwdTable
        (&i4NextInstId, &u4NextVlanId, &i4NextAddrType, &NextSrcIpAddr,
         &NextGrpIpAddr, &u4NextInnerVlanId) == SNMP_FAILURE)
    {
        /* No entries found */
        return SNOOP_SUCCESS;
    }

    do
    {

        nmhGetFsSnoopVlanIpFwdPortList (i4NextInstId, u4NextVlanId,
                                        i4NextAddrType, &NextSrcIpAddr,
                                        &NextGrpIpAddr, u4NextInnerVlanId,
                                        &McastFwdPortList);

        OSIX_BITLIST_IS_BIT_SET (McastFwdPortList.pu1_OctetList,
                                 u2AggId, SNOOP_IF_PORT_LIST_SIZE, bResult);

        if (bResult == OSIX_TRUE)
        {
            SrcIpAddr.u1Afi = (UINT1) i4NextAddrType;
            SrcIpAddr.u1AddrLen = (UINT1) NextSrcIpAddr.i4_Length;
            SNOOP_MEM_CPY (SrcIpAddr.au1Addr,
                           NextSrcIpAddr.pu1_OctetList,
                           NextSrcIpAddr.i4_Length);

            GroupIpAddr.u1Afi = (UINT1) i4NextAddrType;
            GroupIpAddr.u1AddrLen = (UINT1) NextGrpIpAddr.i4_Length;
            SNOOP_MEM_CPY (GroupIpAddr.au1Addr,
                           NextGrpIpAddr.pu1_OctetList,
                           NextGrpIpAddr.i4_Length);

            SNOOP_INET_NTOHL (GroupIpAddr.au1Addr);
            SNOOP_INET_NTOHL (SrcIpAddr.au1Addr);

            SNOOP_ADD_TO_PORT_LIST (u4Port, FwdPortBitmap);

            SNOOP_OUTER_VLAN (VlanId) = (tVlanId) u4NextVlanId;
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;

            /* Add/Delete the incoming member port of portchannel in h/w */
            if (SnoopNpUpdateMcastFwdEntry (i4NextInstId,
                                            VlanId,
                                            SrcIpAddr,
                                            GroupIpAddr,
                                            FwdPortBitmap,
                                            u1Status) == SNOOP_FAILURE)
            {
                SNOOP_DBG_ARG1 (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                SNOOP_FWD_TRC,
                                "Updation of IP forwarding table port %d failed\r\n",
                                u4Port);
            }
        }

        i4CurrInstId = i4NextInstId;
        u4CurrVlanId = u4NextVlanId;
        u4CurrInnerVlanId = u4NextInnerVlanId;
        i4CurrAddrType = i4NextAddrType;

        SNOOP_MEM_CPY (CurrSrcIpAddr.pu1_OctetList,
                       NextSrcIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        SNOOP_MEM_CPY (CurrGrpIpAddr.pu1_OctetList,
                       NextGrpIpAddr.pu1_OctetList, IPVX_MAX_INET_ADDR_LEN);

        SNOOP_MEM_SET (au1NextSrcIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);
        SNOOP_MEM_SET (au1NextGrpIpAddr, 0, IPVX_MAX_INET_ADDR_LEN);

    }
    while (nmhGetNextIndexFsSnoopVlanIpFwdTable
           (i4CurrInstId, &i4NextInstId, u4CurrVlanId, &u4NextVlanId,
            i4CurrAddrType, &i4NextAddrType, &CurrSrcIpAddr, &NextSrcIpAddr,
            &CurrGrpIpAddr, &NextGrpIpAddr, u4CurrInnerVlanId,
            &u4NextInnerVlanId) == SNMP_SUCCESS);
#else
    UNUSED_PARAM (u2AggId);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u1Status);
    UNUSED_PARAM (u4Instance);
#endif
    return SNOOP_SUCCESS;

}

/*****************************************************************************/
/* Function Name      : SnoopVlanGetRtrPortFromPVlanMappingInfo              */
/*                                                                           */
/* Description        : This function gets the mapping between primary and   */
/*                      secondary vlan or vice versa, it will construct the  */
/*                     consolidation router port bitmap from the mapped vlans*/
/*                                                                           */
/* Input(s)           : u4Instance        - Instance Id                      */
/*                      InVlanId          - Vlan Identifier                  */
/*                      u1AddressType     - Address Type(IPv4/IPv6)          */
/*                      u1Version         - Vlan Version                     */
/*                                                                           */
/* Output(s)          : SnoopRtrPortBmp   - Router port bitmap               */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopVlanGetRtrPortFromPVlanMappingInfo (UINT4 u4Instance, tVlanId InVlanId,
                                         UINT1 u1AddressType, UINT1 u1Version,
                                         tSnoopPortBmp SnoopRtrPortBmp,
                                         tSnoopVlanEntry * pSnoopVlanEntry)
{
    tSnoopTag           VlanId;
#ifndef IGS_OPTIMIZE
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    UINT4               u4VlanCnt = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    SNOOP_MEM_SET (SnoopRtrPortBmp, 0, sizeof (tSnoopPortList));
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = InVlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* scanning is done for getting the Router portBmp for the associated vlans
     * in the PVLAN domain and also for the incoming vlan identifier */
    for (u4VlanCnt = 0; u4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans;
         u4VlanCnt++)
    {
        if (u4VlanCnt == SNOOP_ZERO)
        {
            /* For first Iteration, get the vlanEntry for the incoming vlan
             * identifier */
            SNOOP_OUTER_VLAN (VlanId) = InVlanId;
        }
        else
        {
            /* For rest of the Iteration, get the vlanEntry for the vlans associated
             * in the PVLAN domain */
            SNOOP_OUTER_VLAN (VlanId) =
                L2PvlanMappingInfo.pMappedVlans[u4VlanCnt - 1];
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
        }

        if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                                   &pSnoopVlanEntry) == SNOOP_SUCCESS)
        {
            if (u1Version == SNOOP_IGS_IGMP_VERSION1)
            {
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->V1RtrPortBitmap,
                                        SnoopRtrPortBmp);
            }
            else if (u1Version == SNOOP_IGS_IGMP_VERSION2)
            {
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->V2RtrPortBitmap,
                                        SnoopRtrPortBmp);
            }
            else if (u1Version == SNOOP_IGS_IGMP_VERSION3)
            {
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->V3RtrPortBitmap,
                                        SnoopRtrPortBmp);
            }
            else
            {
                SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                        SnoopRtrPortBmp);
            }
        }
    }
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);
#else
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_OUTER_VLAN (VlanId) = (tVlanId) InVlanId;
    SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;

    if (pSnoopVlanEntry == NULL)
    {
        if (SnoopVlanGetVlanEntry (u4Instance, VlanId, u1AddressType,
                                   &pSnoopVlanEntry) == SNOOP_FAILURE)
        {
            return;
        }
    }
    if (u1Version == SNOOP_IGS_IGMP_VERSION1)
    {
        MEMCPY (SnoopRtrPortBmp, pSnoopVlanEntry->V1RtrPortBitmap,
                sizeof (tSnoopPortBmp));
    }
    else if (u1Version == SNOOP_IGS_IGMP_VERSION2)
    {
        MEMCPY (SnoopRtrPortBmp, pSnoopVlanEntry->V2RtrPortBitmap,
                sizeof (tSnoopPortBmp));
    }
    else if (u1Version == SNOOP_IGS_IGMP_VERSION3)
    {
        MEMCPY (SnoopRtrPortBmp, pSnoopVlanEntry->V3RtrPortBitmap,
                sizeof (tSnoopPortBmp));
    }
    else
    {
        MEMCPY (SnoopRtrPortBmp, pSnoopVlanEntry->RtrPortBitmap,
                sizeof (tSnoopPortBmp));
    }
    UNUSED_PARAM (InVlanId);
#endif
    return;

}

/*****************************************************************************/
/* Function Name      : SnoopIsVlanPresentInFwdTbl                           */
/*                                                                           */
/* Description        : This function gets the mapping between primary and   */
/*                      secondary vlan or vice versa in case if vlan is      */
/*                      mapped to PVLAN domain and check the presence of     */
/*                      these mapeed vlans in forwarding table, otherwise    */
/*                      this function will check whether incoming vlan alone */
/*                      is present in the forwarding table or not.           */
/*                                                                           */
/* Input(s)           : u4Instance       - Instance Identifier               */
/*                      VlanId           - Vlan identifier in Fwd table      */
/*                      InVlanId         - Incoming Vlan Identifier          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_TRUE/SNOOP_FALSE                               */
/*****************************************************************************/
INT1
SnoopIsVlanPresentInFwdTbl (tVlanId VlanId, tVlanId InVlanId,
                            tL2PvlanMappingInfo * pL2PvlanMappingInfo)
{
    UINT4               u4VlanCnt = 0;

    /* Check the presence of the incoming vlan identifier */
    if (VlanId == InVlanId)
    {
        return SNOOP_TRUE;
    }

    /* scanning is done to check whether any of the associated vlans
     * in the PVLAN domain is matched with the forwarding table vlan Id */
    for (u4VlanCnt = 0; u4VlanCnt < pL2PvlanMappingInfo->u2NumMappedVlans;
         u4VlanCnt++)
    {
        if (VlanId == pL2PvlanMappingInfo->pMappedVlans[u4VlanCnt])
        {
            return SNOOP_TRUE;
        }
    }
    return SNOOP_FALSE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetPortBmpFromPVlanMapInfo                  */
/*                                                                           */
/* Description        : This function gets the mapping between primary and   */
/*                      secondary vlan or vice versa, it will construct the  */
/*                      consolidation port bitmap from the mapped vlans,     */
/*                      This will be called in case of IP based snooping.    */
/*                                                                           */
/* Input(s)           : u4Instance        - Instance Id                      */
/*                      InVlanId          - Incoming Vlan Identifier         */
/*                      GrpAddr           - Incoming Group Address           */
/*                      SrcIpAddr         - Incoming SourceIP Address        */
/*                                                                           */
/* Output(s)          : SnoopPortBmp      - Listner port bitmap              */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopUtilGetPortBmpFromPVlanMapInfo (UINT4 u4Instance, tVlanId InVlanId,
                                     tIPvXAddr GrpAddr, tIPvXAddr SrcIpAddr,
                                     tSnoopPortBmp SnoopPortBmp)
{
    tL2PvlanMappingInfo L2PvlanMappingInfo;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    tSnoopTag           VlanId;
    UINT4               u4VlanCnt = 0;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    BOOL1               bResult = SNOOP_FALSE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;

    SNOOP_MEM_SET (&L2PvlanMappingInfo, 0, sizeof (tL2PvlanMappingInfo));
    SNOOP_MEM_SET (SnoopPortBmp, 0, sizeof (tSnoopPortList));
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));

    L2PvlanMappingInfo.u4ContextId = u4Instance;
    L2PvlanMappingInfo.InVlanId = InVlanId;
    L2PvlanMappingInfo.u1RequestType = L2IWF_MAPPED_VLANS;

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_GRP_TRC,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return;
    }
    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));
    if (SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK (u4Instance,
                                              L2PvlanMappingInfo.
                                              pMappedVlans) == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "Memory allocation for Snoop PVLAN LIST failed\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        return;
    }

    L2IwfGetPVlanMappingInfo (&L2PvlanMappingInfo);

    /* scanning is done for getting the Router portBmp for the associated vlans
     * in the PVLAN domain and also for the incoming vlan identifier */
    for (u4VlanCnt = 0; u4VlanCnt <= L2PvlanMappingInfo.u2NumMappedVlans;
         u4VlanCnt++)
    {
        SNOOP_MEM_SET (pFwdPortBitmap, 0, sizeof (tSnoopPortList));
        if (u4VlanCnt == SNOOP_ZERO)
        {
            /* For first Iteration, get the vlanEntry for the incoming vlan
             * identifier */
            SNOOP_OUTER_VLAN (VlanId) = InVlanId;
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
        }
        else
        {
            /* For rest of the Iteration, get the vlanEntry for the vlans associated
             * in the PVLAN domain */
            SNOOP_OUTER_VLAN (VlanId) =
                L2PvlanMappingInfo.pMappedVlans[u4VlanCnt - 1];
            SNOOP_INNER_VLAN (VlanId) = SNOOP_INVALID_VLAN_ID;
        }

        if (SnoopGrpGetGroupEntry (u4Instance, VlanId,
                                   GrpAddr, &pSnoopGroupEntry) == SNOOP_SUCCESS)
        {
            /* Check if source is present */
            SNOOP_IS_SOURCE_PRESENT (u4Instance, SrcIpAddr, bResult,
                                     u2SrcIndex);
            if (u2SrcIndex != 0xffff)
            {
                u2SrcIndex++;
            }

            /* Check if any V2 receivers are present */
            if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
            {
                u1ASMHostPresent = SNOOP_TRUE;
            }

            /* Get the list of ports on which receivers are attached 
             * for this (S,G) entry */
            SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                      u1ASMHostPresent, pFwdPortBitmap);
            SNOOP_UPDATE_PORT_LIST (pFwdPortBitmap, SnoopPortBmp);
        }
    }
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK (u4Instance,
                                         L2PvlanMappingInfo.pMappedVlans);

    UNUSED_PARAM (bResult);
}

/*****************************************************************************/
/* Function Name      : SnoopUtilHandleSparseMode                            */
/*                                                                           */
/* Description        : This function will set the sparse mode based upon    */
/*                      whether snooping is enabled or disabled inside VLAN  */
/*                                                                           */
/* Input(s)           : u4Instance - Instance ID                             */
/*                      i4SparseMode - Sparse mode                           */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS                                        */
/*****************************************************************************/
INT4
SnoopUtilHandleSparseMode (UINT4 u4Instance, INT4 i4SparseMode)
{
    BOOL1               bSnoopDisableVlan = OSIX_TRUE;
    tVlanId             PrevVlanId;
    tVlanId             VlanId;
    tSnoopTag           SnoopVlanId;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBTreeElem = NULL;
    tRBElem            *pRBTreeNextElem = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT1              *pBitmapVlan = NULL;
    UINT1              *pTempPortBitmap = NULL;
    tSnoopIfPortBmp    *pSnpPortList = NULL;
    tPortList          *pTmpPortList = NULL;

    pTmpPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTmpPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_DBG_RESRC,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pTmpPortList\r\n");
        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                               SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                               SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL],
                               "for u4Instance %d\r\n", u4Instance);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        PrevVlanId = 0;

        while (L2IwfMiGetNextActiveVlan (u4Instance, PrevVlanId,
                                         &VlanId) == L2IWF_SUCCESS)
        {
            PrevVlanId = VlanId;

            SNOOP_MEM_SET (SnoopVlanId, 0, sizeof (tSnoopTag));

            SNOOP_OUTER_VLAN (SnoopVlanId) = (tVlanId) PrevVlanId;

            SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));

            if (i4SparseMode == SNOOP_ENABLE)
            {
                if (SnoopVlanGetVlanEntry
                    (u4Instance, SnoopVlanId, SNOOP_ADDR_TYPE_IPV4,
                     &pSnoopVlanEntry) == SNOOP_SUCCESS)
                {
                    if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_ENABLE)
                    {
                        bSnoopDisableVlan = OSIX_FALSE;
                    }
                }

                if (bSnoopDisableVlan == OSIX_FALSE)
                {
                    pBitmapVlan =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pBitmapVlan == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                       SNOOP_OS_RES_NAME,
                                       "Error in allocating memory for pBitmapVlan\r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_MEM_ALLOC_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        return SNOOP_FAILURE;
                    }
                    SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));
                    SNOOP_MEM_CPY (pBitmapVlan,
                                   pSnoopVlanEntry->RtrPortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    pSnpPortList =
                        (tSnoopIfPortBmp *)
                        FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
                    if (pSnpPortList == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                       SNOOP_OS_RES_NAME,
                                       "Error in allocating memory for pSnpPortList\r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_MEM_ALLOC_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        UtilPlstReleaseLocalPortList (pBitmapVlan);
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        return SNOOP_FAILURE;
                    }
                    SNOOP_MEM_SET (*pSnpPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
                    if (SnoopGetPhyPortBmp
                        (u4Instance, pBitmapVlan,
                         *pSnpPortList) != SNOOP_SUCCESS)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                       SNOOP_OS_RES_NAME,
                                       "Unable to get the physical port bitmap from the virtual"
                                       " port bitmap\r\n");
                        UtilPlstReleaseLocalPortList (pBitmapVlan);
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                        return SNOOP_FAILURE;
                    }
                    UtilPlstReleaseLocalPortList (pBitmapVlan);

                    SNOOP_MEM_CPY (*pTmpPortList, *pSnpPortList,
                                   SNOOP_IF_PORT_LIST_SIZE);

                    FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                    SnoopVlanSetForwardUnregPorts (PrevVlanId,
                                                   (tPortList *)
                                                   (*pTmpPortList));
                }
                else if (bSnoopDisableVlan == OSIX_TRUE)
                {
                    L2IwfMiGetVlanEgressPorts (u4Instance, PrevVlanId,
                                               *pTmpPortList);
                    SnoopVlanSetForwardUnregPorts (PrevVlanId,
                                                   (tPortList *)
                                                   (*pTmpPortList));
                }
            }
            else if (i4SparseMode == SNOOP_DISABLE)
            {
                if (L2IwfMiGetVlanEgressPorts (u4Instance, PrevVlanId,
                                               *pTmpPortList) != L2IWF_FAILURE)
                {
                    SnoopVlanSetForwardUnregPorts (PrevVlanId,
                                                   (tPortList
                                                    *) (*pTmpPortList));
                }
            }
        }
    }
    else
    {
        if (SNOOP_MCAST_CONTROL_PLANE_DRIVEN (u4Instance) == SNOOP_DISABLE)
        {
            PrevVlanId = 0;
            while (L2IwfMiGetNextActiveVlan (u4Instance, PrevVlanId,
                                             &VlanId) == L2IWF_SUCCESS)
            {
                PrevVlanId = VlanId;
                SNOOP_MEM_SET (SnoopVlanId, 0, sizeof (tSnoopTag));
                SNOOP_OUTER_VLAN (SnoopVlanId) = (tVlanId) PrevVlanId;

                pRBTreeElem =
                    RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                    GroupEntry);
                if (pRBTreeElem == NULL)
                {
                    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_DBG, "Group Entry Not Exist\r\n");
                    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    return SNOOP_SUCCESS;
                }
                while (pRBTreeElem != NULL)
                {
                    pRBTreeNextElem =
                        RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                       GroupEntry, pRBTreeElem, NULL);

                    pSnoopGroupEntry = (tSnoopGroupEntry *) pRBTreeElem;

                    if (SnoopVlanGetVlanEntry
                        (u4Instance, SnoopVlanId, SNOOP_ADDR_TYPE_IPV4,
                         &pSnoopVlanEntry) != SNOOP_SUCCESS)
                    {
                        break;
                    }

                    if (SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)
                        != SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId))
                    {
                        pRBTreeElem = pRBTreeNextElem;
                        continue;
                    }
                    pRBElem =
                        RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->
                                        IpMcastFwdEntry);

                    pTempPortBitmap =
                        UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
                    if (pTempPortBitmap == NULL)
                    {
                        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                       SNOOP_CONTROL_PATH_TRC |
                                       SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                       SNOOP_OS_RES_NAME,
                                       "Error in allocating memory for pTempPortBitmap\r\n");
                        SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL,
                                               SNOOP_DBG_FLAG, SNOOP_DBG_RESRC,
                                               SNOOP_OS_RES_DBG,
                                               SnoopSysErrString
                                               [SYS_LOG_MEM_ALLOC_FAIL],
                                               "for u4Instance %d\r\n",
                                               u4Instance);
                        FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                        return SNOOP_FAILURE;
                    }
                    SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));
                    while (pRBElem != NULL)
                    {
                        pRBNextElem =
                            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                                           IpMcastFwdEntry, pRBElem, NULL);

                        pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

                        if (SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)
                            != SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId))
                        {
                            pRBElem = pRBNextElem;
                            continue;
                        }

                        if (pSnoopIpGrpFwdEntry->u1AddressType !=
                            pSnoopVlanEntry->u1AddressType)
                        {
                            pRBElem = pRBNextElem;
                            continue;
                        }

                        if (i4SparseMode == SNOOP_DISABLE)
                        {
                            if (SNOOP_MEM_CMP (pSnoopIpGrpFwdEntry->PortBitmap,
                                               gNullPortBitMap,
                                               SNOOP_PORT_LIST_SIZE) == 0)
                            {
                                L2IwfMiGetVlanEgressPorts (u4Instance,
                                                           SNOOP_OUTER_VLAN
                                                           (pSnoopIpGrpFwdEntry->
                                                            VlanId),
                                                           *pTmpPortList);
                                SNOOP_MEM_CPY (pTempPortBitmap, *pTmpPortList,
                                               SNOOP_PORT_LIST_SIZE);
                                SNOOP_UPDATE_PORT_LIST (pTempPortBitmap,
                                                        pSnoopIpGrpFwdEntry->
                                                        PortBitmap);
#ifdef NPAPI_WANTED
                                if (SnoopUtilNpUpdateMcastFwdEntry
                                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                                     pTempPortBitmap,
                                     SNOOP_HW_CREATE_ENTRY) != SNOOP_SUCCESS)
                                {
                                    SNOOP_DBG (SNOOP_DBG_FLAG,
                                               SNOOP_CONTROL_PATH_TRC,
                                               SNOOP_OS_RES_DBG,
                                               "Addition of port for "
                                               "IP forwarding entry to hardware "
                                               "failed\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitmap);
                                    FsUtilReleaseBitList ((UINT1 *)
                                                          pTmpPortList);
                                    return SNOOP_FAILURE;
                                }
#endif
                            }
                        }
                        else
                        {
                            if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_ENABLE)
                            {
                                if (SNOOP_MEM_CMP
                                    (pSnoopVlanEntry->RtrPortBitmap,
                                     gNullPortBitMap,
                                     SNOOP_PORT_LIST_SIZE) != 0)
                                {
                                    SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->
                                                            StaticRtrPortBitmap,
                                                            pSnoopVlanEntry->
                                                            RtrPortBitmap);

                                    if (SNOOP_INNER_VLAN
                                        (pSnoopIpGrpFwdEntry->VlanId) ==
                                        SNOOP_INVALID_VLAN_ID)
                                    {
                                        SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->
                                                       PortBitmap,
                                                       pSnoopVlanEntry->
                                                       RtrPortBitmap,
                                                       SNOOP_PORT_LIST_SIZE);
                                    }

                                    SNOOP_MEM_CPY (pTempPortBitmap,
                                                   pSnoopVlanEntry->
                                                   RtrPortBitmap,
                                                   SNOOP_PORT_LIST_SIZE);
                                }
                                else
                                {
                                    if (SnoopGrpGetGroupEntry
                                        (u4Instance,
                                         pSnoopIpGrpFwdEntry->VlanId,
                                         pSnoopIpGrpFwdEntry->GrpIpAddr,
                                         &pSnoopGroupEntry) != SNOOP_SUCCESS)
                                    {
                                        SNOOP_MEM_SET (pTempPortBitmap, 0,
                                                       SNOOP_PORT_LIST_SIZE);
                                        SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->
                                                       PortBitmap, 0,
                                                       SNOOP_PORT_LIST_SIZE);
                                    }
                                    else
                                    {
                                        SNOOP_MEM_CPY (pTempPortBitmap,
                                                       pSnoopGroupEntry->
                                                       PortBitmap,
                                                       SNOOP_PORT_LIST_SIZE);
                                        SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->
                                                       PortBitmap,
                                                       pSnoopGroupEntry->
                                                       PortBitmap,
                                                       SNOOP_PORT_LIST_SIZE);
                                    }
                                }
#ifdef NPAPI_WANTED
                                if (SnoopNpUpdateMcastFwdEntry
                                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                                     pTempPortBitmap,
                                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                                {
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE |
                                                   SNOOP_DBG_RESRC,
                                                   SNOOP_OS_RES_NAME,
                                                   "Addition of IP forwarding entry "
                                                   "to hardware failed\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitmap);
                                    FsUtilReleaseBitList ((UINT1 *)
                                                          pTmpPortList);
                                    return SNOOP_FAILURE;
                                }
#endif
                            }
                            else
                            {
                                L2IwfMiGetVlanEgressPorts (u4Instance,
                                                           SNOOP_OUTER_VLAN
                                                           (pSnoopIpGrpFwdEntry->
                                                            VlanId),
                                                           *pTmpPortList);
                                SNOOP_MEM_CPY (pTempPortBitmap, *pTmpPortList,
                                               SNOOP_PORT_LIST_SIZE);
                                SNOOP_UPDATE_PORT_LIST (pTempPortBitmap,
                                                        pSnoopIpGrpFwdEntry->
                                                        PortBitmap);
#ifdef NPAPI_WANTED
                                if (SnoopUtilNpUpdateMcastFwdEntry
                                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                                     pTempPortBitmap,
                                     SNOOP_HW_CREATE_ENTRY) != SNOOP_SUCCESS)
                                {
                                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST
                                                   (u4Instance),
                                                   SNOOP_CONTROL_PATH_TRC |
                                                   SNOOP_DBG_ALL_FAILURE |
                                                   SNOOP_DBG_RESRC,
                                                   SNOOP_OS_RES_NAME,
                                                   "Addition of port for "
                                                   "IP forwarding entry to hardware "
                                                   "failed\r\n");
                                    UtilPlstReleaseLocalPortList
                                        (pTempPortBitmap);
                                    FsUtilReleaseBitList ((UINT1 *)
                                                          pTmpPortList);
                                    return SNOOP_FAILURE;
                                }
#endif
                            }
                        }
                        pRBElem = pRBNextElem;
                    }
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    pRBTreeElem = pRBTreeNextElem;
                }
            }
        }
    }
    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilEnableSnoopInVlan                           */
/*                                                                           */
/* Description        : This function will update the forwarding database    */
/*                      based upon whether sparse mode is enabled or disabled*/
/*                                                                           */
/* Input(s)           : u4Instance - Instance.                               */
/*                      pSnoopVlanEntry  - Pointer to the Vlan entry         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS                                        */
/*****************************************************************************/
INT4
SnoopUtilEnableSnoopInVlan (UINT4 u4Instance, tSnoopVlanEntry * pSnoopVlanEntry)
{
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopGroupEntry   *pSnoopGroupEntry = NULL;
    UINT1              *pTempPortBitmap = NULL;
    UINT1              *pBitmapVlan = NULL;
    tSnoopIfPortBmp    *pSnpPortList = NULL;
    tPortList          *pTmpPortList = NULL;

    pTmpPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pTmpPortList == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                       SNOOP_DBG_RESRC,
                       SNOOP_OS_RES_NAME,
                       "Error in allocating memory for pTmpPortList\r\n");
        SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                          SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                          SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
        return SNOOP_FAILURE;
    }
    SNOOP_MEM_SET (*pTmpPortList, 0, sizeof (tPortList));
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
        {
            pBitmapVlan = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
            if (pBitmapVlan == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_NAME,
                               "Error in allocating memory for pBitmapVlan\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (pBitmapVlan, 0, sizeof (tSnoopPortBmp));

            pSnpPortList =
                (tSnoopIfPortBmp *)
                FsUtilAllocBitList (sizeof (tSnoopIfPortBmp));
            if (pSnpPortList == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_NAME,
                               "Error in allocating memory for pSnpPortList\r\n");
                SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                  SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                  SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                return SNOOP_FAILURE;
            }
            SNOOP_MEM_SET (*pSnpPortList, 0, sizeof (tSnoopIfPortBmp));

            SNOOP_MEM_CPY (pBitmapVlan, pSnoopVlanEntry->RtrPortBitmap,
                           SNOOP_PORT_LIST_SIZE);
            if (SnoopGetPhyPortBmp (u4Instance, pBitmapVlan, *pSnpPortList) !=
                SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_NAME,
                               "Unable to get the physical port bitmap from the virtual"
                               " port bitmap\r\n");
                FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
                UtilPlstReleaseLocalPortList (pBitmapVlan);
                FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                return SNOOP_FAILURE;
            }
            UtilPlstReleaseLocalPortList (pBitmapVlan);

            SNOOP_MEM_CPY (*pTmpPortList, *pSnpPortList,
                           SNOOP_IF_PORT_LIST_SIZE);
            SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                           (pSnoopVlanEntry->VlanId),
                                           (tPortList *) (*pTmpPortList));
            FsUtilReleaseBitList ((UINT1 *) pSnpPortList);
        }
        else if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_DISABLE)
        {
            L2IwfMiGetVlanEgressPorts (u4Instance,
                                       SNOOP_OUTER_VLAN (pSnoopVlanEntry->
                                                         VlanId),
                                       *pTmpPortList);
            SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                           (pSnoopVlanEntry->VlanId),
                                           (tPortList *) (*pTmpPortList));
        }
    }
    else
    {
        pRBElem =
            RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry);
        pTempPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));
        if (pTempPortBitmap == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                           SNOOP_DBG_RESRC,
                           SNOOP_OS_RES_NAME,
                           "Error in allocating memory for pTempPortBitmap\r\n");
            SYSLOG_SNOOP_MSG (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                              SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                              SnoopSysErrString[SYS_LOG_MEM_ALLOC_FAIL]);
            FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
            return SNOOP_FAILURE;
        }
        SNOOP_MEM_SET (pTempPortBitmap, 0, sizeof (tSnoopPortBmp));
        while (pRBElem != NULL)
        {
            pRBNextElem =
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               IpMcastFwdEntry, pRBElem, NULL);
            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if (SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)
                != SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId))
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (pSnoopIpGrpFwdEntry->u1AddressType !=
                pSnoopVlanEntry->u1AddressType)
            {
                pRBElem = pRBNextElem;
                continue;
            }

            if (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE)
            {
                if (SnoopGrpGetGroupEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                     &pSnoopGroupEntry) != SNOOP_SUCCESS)
                {
                    SNOOP_MEM_SET (pTempPortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0,
                                   SNOOP_PORT_LIST_SIZE);
                }
                else
                {
                    SNOOP_MEM_CPY (pTempPortBitmap,
                                   pSnoopGroupEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->PortBitmap,
                                   pSnoopGroupEntry->PortBitmap,
                                   SNOOP_PORT_LIST_SIZE);
                }
#ifdef NPAPI_WANTED
                if (SnoopNpUpdateMcastFwdEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                     pSnoopIpGrpFwdEntry->GrpIpAddr, pTempPortBitmap,
                     SNOOP_ADD_PORT) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_NAME,
                                   "Addition of IP forwarding entry "
                                   "to hardware failed\r\n");
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    return SNOOP_FAILURE;
                }
#endif
            }
            else
            {
                if (pSnoopIpGrpFwdEntry->u1EntryTypeFlag == SNOOP_GRP_STATIC)
                {
                    if (SnoopGrpGetGroupEntry (u4Instance,
                                               pSnoopIpGrpFwdEntry->
                                               VlanId,
                                               pSnoopIpGrpFwdEntry->
                                               GrpIpAddr,
                                               &pSnoopGroupEntry) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_MEM_SET (pTempPortBitmap, 0,
                                       SNOOP_PORT_LIST_SIZE);
                        SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->
                                       PortBitmap, 0, SNOOP_PORT_LIST_SIZE);
                    }
                    else
                    {
                        SNOOP_MEM_CPY (pTempPortBitmap,
                                       pSnoopGroupEntry->PortBitmap,
                                       SNOOP_PORT_LIST_SIZE);
                        SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->
                                       PortBitmap,
                                       pSnoopGroupEntry->PortBitmap,
                                       SNOOP_PORT_LIST_SIZE);
                    }
                }
                else
                {
                    L2IwfMiGetVlanEgressPorts (u4Instance,
                                               SNOOP_OUTER_VLAN
                                               (pSnoopIpGrpFwdEntry->
                                                VlanId), *pTmpPortList);
                    SNOOP_MEM_CPY (pTempPortBitmap, *pTmpPortList,
                                   SNOOP_PORT_LIST_SIZE);
                    SNOOP_UPDATE_PORT_LIST (pTempPortBitmap,
                                            pSnoopIpGrpFwdEntry->PortBitmap);
                }
#ifdef NPAPI_WANTED
                if (SnoopUtilNpUpdateMcastFwdEntry
                    (u4Instance, pSnoopIpGrpFwdEntry->VlanId,
                     pSnoopIpGrpFwdEntry->SrcIpAddr,
                     pSnoopIpGrpFwdEntry->GrpIpAddr,
                     pTempPortBitmap, SNOOP_HW_CREATE_ENTRY) != SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE | SNOOP_DBG_RESRC,
                                   SNOOP_OS_RES_NAME,
                                   "Addition of port for "
                                   "IP forwarding entry to hardware "
                                   "failed\r\n");
                    UtilPlstReleaseLocalPortList (pTempPortBitmap);
                    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
                    return SNOOP_FAILURE;
                }
#endif
            }
            pRBElem = pRBNextElem;
        }
        UtilPlstReleaseLocalPortList (pTempPortBitmap);
    }
    FsUtilReleaseBitList ((UINT1 *) pTmpPortList);
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilNpUpdateMcastFwdEntry                       */
/*                                                                           */
/* Description        : This function wiill set the H/W Mcast Entry          */
/*                                                                           */
/* Input(s)           : u4Instance - Instance.                               */
/*                      pSnoopVlanEntry  - Pointer to the Vlan entry         */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS                                        */
/*****************************************************************************/
INT4
SnoopUtilNpUpdateMcastFwdEntry (UINT4 u4Instance, tSnoopTag VlanId,
                                tIPvXAddr SrcAddr, tIPvXAddr GrpAddr,
                                tSnoopPortBmp PortBitmap, UINT1 u1EventType)
{
#ifdef NPAPI_WANTED
    tIgsHwIpFwdInfo     IgsHwIpFwdInfo;
    tPortList           PortList;
    tPortList           UntagPortList;
    tPortList           TempPortList;
    tPortList           TempUntagPortList;
    tSnoopIfPortList    SnpPortList;
    tSnoopIfPortList    SnpUntagPortList;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_MEM_SET (&IgsHwIpFwdInfo, 0, sizeof (tIgsHwIpFwdInfo));
    SNOOP_MEM_SET (&PortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&UntagPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&TempPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&TempUntagPortList, 0, BRG_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&SnpPortList, 0, SNOOP_IF_PORT_LIST_SIZE);
    SNOOP_MEM_SET (&SnpUntagPortList, 0, SNOOP_IF_PORT_LIST_SIZE);

    if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_FALSE)
    {
        return (SNOOP_SUCCESS);
    }
    if (SnoopGetPhyPortBmp (u4Instance, PortBitmap, SnpPortList)
        != SNOOP_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Unable to get the physical port bitmap from the virtual"
                       " port bitmap\r\n");
        return SNOOP_FAILURE;
    }

    IgsHwIpFwdInfo.OuterVlanId = SNOOP_OUTER_VLAN (VlanId);
    IgsHwIpFwdInfo.InnerVlanId = SNOOP_INNER_VLAN (VlanId);
    IgsHwIpFwdInfo.u4GrpAddr = SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr);
    IgsHwIpFwdInfo.u4SrcAddr = SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr);

    SNOOP_MEM_CPY (PortList, SnpPortList, sizeof (tSnoopIfPortList));
    SNOOP_MEM_CPY (UntagPortList, SnpPortList, sizeof (tSnoopIfPortList));
#ifdef IGS_WANTED
    SNOOP_MEM_CPY (TempPortList, PortList, sizeof (tPortList));
    SNOOP_MEM_CPY (TempUntagPortList, UntagPortList, sizeof (tPortList));
    if (IgsFsMiNpUpdateIpmcFwdEntries (u4Instance, SNOOP_OUTER_VLAN (VlanId),
                                       SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                                       SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                                       PortList,
                                       UntagPortList,
                                       u1EventType) != FNP_SUCCESS)
    {
        SNOOP_DBG_ARG3 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                        SNOOP_FWD_TRC,
                        "V4 Multicast forward entry creation in hardware for"
                        " Vlan %d Source %x, Group %x failed\r\n",
                        SNOOP_OUTER_VLAN (VlanId),
                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr));
        SnoopHandleGroupCreateFailure (VlanId, GrpAddr, SrcAddr, u4Instance);
        return SNOOP_SUCCESS;
    }
    SNOOP_MEM_CPY (PortList, TempPortList, sizeof (tPortList));
    SNOOP_MEM_CPY (UntagPortList, TempUntagPortList, sizeof (tPortList));
    SNOOP_MEM_CPY (IgsHwIpFwdInfo.PortList, PortList, sizeof (tPortList));
    SNOOP_MEM_CPY (IgsHwIpFwdInfo.UntagPortList, UntagPortList,
                   sizeof (tPortList));

    if (IgsFsMiIgsHwUpdateIpmcEntry (u4Instance, &IgsHwIpFwdInfo,
                                     SNOOP_HW_CREATE_ENTRY) != FNP_SUCCESS)
    {
        SNOOP_DBG_ARG4 (SNOOP_IGS_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                        SNOOP_FWD_TRC,
                        "Multicast forward entry creation in hardware for"
                        " OuterVlan %d Source %x, Group %x InnerVlan %d "
                        "failed\r\n", SNOOP_OUTER_VLAN (VlanId),
                        SNOOP_PTR_FETCH_4 (SrcAddr.au1Addr),
                        SNOOP_PTR_FETCH_4 (GrpAddr.au1Addr),
                        SNOOP_INNER_VLAN (VlanId));

        SnoopHandleGroupCreateFailure (VlanId, GrpAddr, SrcAddr, u4Instance);

        return SNOOP_SUCCESS;

    }
#endif
    UNUSED_PARAM (u1EventType);
#else
    UNUSED_PARAM (u4Instance);
    UNUSED_PARAM (VlanId);
    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (GrpAddr);
    UNUSED_PARAM (PortBitmap);
    UNUSED_PARAM (u1EventType);
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilGetForwardingDataBase                       */
/*                                                                           */
/* Description        : This function will return (S,G) Entry and PortList   */
/*                      for the incoming VLAN ID                             */
/*                      Note. Source IP and Group IP should be filled        */
/*                      with current value to get the next valid value.      */
/*                      if given with 0's then first S,G will be returned    */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      pSrcAddr - Source Address (initial value)            */
/*                      pGrpAddr - Group Address  (initial value)            */
/*                      PortBitmap - Portlist to be updated.                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : pSrcAddr - Source Address (next value)               */
/*                      pGrpAddr - Group Address  (next value)               */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCCESS/OSIX_FAILURE                           */
/******************************************************************************/
INT4
SnoopUtilGetForwardingDataBase (tVlanId VlanId, tIPvXAddr * pSrcAddr,
                                tIPvXAddr * pGrpAddr)
{

    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpNextFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopTag           SnoopVlanId;
    tIPvXAddr           NullAddr;
    UINT4               u4Instance = 0;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (&NullAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SNOOP_OUTER_VLAN (SnoopVlanId) = (tVlanId) VlanId;

    for (u4Instance = 0; u4Instance < SNOOP_MAX_INSTANCES; u4Instance++)
    {
        pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

        if (pSnoopInstInfo != NULL)
        {
            if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
            {
                return OSIX_FAILURE;
            }
            if (SnoopVlanGetVlanEntry (u4Instance, SnoopVlanId,
                                       SNOOP_ADDR_TYPE_IPV4,
                                       &pSnoopVlanEntry) != SNOOP_SUCCESS)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            continue;
        }
        /* rajesh - tested: Check the Sparse mode IGS dependcy is required
           here or not */

        if (SnoopVlanGetVlanEntry (u4Instance, SnoopVlanId,
                                   SNOOP_ADDR_TYPE_IPV4,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            if ((SNOOP_MCAST_FWD_MODE (u4Instance) != SNOOP_MCAST_FWD_MODE_IP)
                && (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) != SNOOP_ENABLE)
                && (pSnoopVlanEntry->u1SnoopStatus != SNOOP_ENABLE))
            {
                return OSIX_FAILURE;
            }
        }
        if (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry == NULL)
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_MGMT | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_MGMT_NAME,
                                "No multicast IP forwarding entry present for "
                                "the instance %d\r\n", (u4Instance));
            return OSIX_FAILURE;

        }
        if ((SNOOP_MEM_CMP (pSrcAddr->au1Addr, NullAddr.au1Addr,
                            IPVX_MAX_INET_ADDR_LEN) == 0) &&
            (SNOOP_MEM_CMP (pGrpAddr->au1Addr, NullAddr.au1Addr,
                            IPVX_MAX_INET_ADDR_LEN) == 0))
        {
            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *)
                RBTreeGetFirst (SNOOP_INSTANCE_INFO
                                (u4Instance)->IpMcastFwdEntry);
        }
        else
        {
            SNOOP_OUTER_VLAN (IpGrpFwdEntry.VlanId) = (tVlanId) VlanId;
            IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr), pSrcAddr);
            IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr), pGrpAddr);

            IpGrpFwdEntry.u1AddressType = pSrcAddr->u1Afi;

            pSnoopIpGrpNextFwdEntry = (tSnoopIpGrpFwdEntry *)
                RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->
                               IpMcastFwdEntry, (tRBElem *) & IpGrpFwdEntry,
                               NULL);

            pSnoopIpGrpFwdEntry = pSnoopIpGrpNextFwdEntry;
        }

        if (pSnoopIpGrpFwdEntry != NULL)
        {
            SNOOP_MEM_CPY (pSrcAddr, &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                           sizeof (tIPvXAddr));

            SNOOP_MEM_CPY (pGrpAddr, &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                           sizeof (tIPvXAddr));

            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanGetAllRtrPorts                              */
/*                                                                           */
/* Description        : This function will return Mrouter PortList           */
/*                      for the incoming VLAN ID                             */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      u4Instance - Context ID                              */
/*                                                                           */
/* Output(s)          : pSnoopRtrPortList - Mrouter PortList                 */
/*                      for the Incoming Vlan ID                             */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCCESS/OSIX_FAILURE                           */
/******************************************************************************/
INT4
SnoopVlanGetAllRtrPorts (UINT4 u4Instance, tVlanId VlanId,
                         tSnoopPortList * pSnoopRtrPortList)
{
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tSnoopTag           SnoopVlanId;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tSnoopTag));
    SNOOP_OUTER_VLAN (SnoopVlanId) = (tVlanId) VlanId;

    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);

    if (pSnoopInstInfo != NULL)
    {
        if ((SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
            || (SNOOP_MCAST_FWD_MODE (u4Instance) != SNOOP_MCAST_FWD_MODE_IP)
            || (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) != SNOOP_ENABLE))
        {
            return SNOOP_FAILURE;
        }
        if (SnoopVlanGetVlanEntry (u4Instance, SnoopVlanId,
                                   SNOOP_ADDR_TYPE_IPV4,
                                   &pSnoopVlanEntry) != SNOOP_SUCCESS)
        {
            return SNOOP_FAILURE;
        }
        if (pSnoopVlanEntry->u1SnoopStatus == SNOOP_ENABLE)
        {
            SNOOP_MEM_CPY (pSnoopRtrPortList,
                           pSnoopVlanEntry->RtrPortBitmap,
                           SNOOP_PORT_LIST_SIZE);
            return SNOOP_SUCCESS;
        }
    }
    return SNOOP_FAILURE;
}

#ifdef PIM_WANTED

/*****************************************************************************/
/* Function Name      : SnoopUtilGetForwardingEntryFromPim                   */
/*                                                                           */
/* Description        : This function will query PIM for (S,G) for the given */
/*                      VLAN ID                                              */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      PortBitmap - Portlist to be updated.                 */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopUtilGetForwardingEntryFromPim (UINT4 u4Instance, tSnoopTag VlanId,
                                    tSnoopPortBmp RtrPortBitMap)
{
    tSnoopPortBmp       FwdPortBitmap;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tVlanId             SnoopVlanId;
    tIPvXAddr           SrcIpAddr;
    tIPvXAddr           GrpIpAddr;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    UINT4               u4Status = 0;
#ifdef NPAPI_WANTED
    BOOL1               b1NewEntry = OSIX_FALSE;
#endif

    SNOOP_MEM_SET (&FwdPortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));
    SNOOP_MEM_SET (&SrcIpAddr, 0, sizeof (tIPvXAddr));
    SNOOP_MEM_SET (&GrpIpAddr, 0, sizeof (tIPvXAddr));

    SNOOP_MEM_CPY (FwdPortBitmap, RtrPortBitMap, SNOOP_PORT_LIST_SIZE);
    SNOOP_MEM_CPY (IpGrpFwdEntry.VlanId, VlanId, sizeof (tSnoopTag));

    SnoopVlanId = SNOOP_OUTER_VLAN (VlanId);

    /* The following API will get (S,G) Entry from PIM,
       for the particular VLAN ID, if available */
    while (SPimPortGetSGFromVlanId (SnoopVlanId, &SrcIpAddr, &GrpIpAddr)
           != OSIX_FAILURE)
    {
        SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                       (UINT1 *) &SrcIpAddr, sizeof (tIPvXAddr));

        IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                        (UINT1 *) &GrpIpAddr);

        IpGrpFwdEntry.u1AddressType = IpGrpFwdEntry.GrpIpAddr.u1Afi;

        pRBElem = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                             (tRBElem *) & IpGrpFwdEntry);
        if (pRBElem == NULL)
        {
            if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry)
                == NULL)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE |
                               SNOOP_DBG_RESRC,
                               SNOOP_OS_RES_NAME,
                               "Memory allocation for IP forward entry \r\n");
                return SNOOP_FAILURE;
            }

#ifdef NPAPI_WANTED
            b1NewEntry = OSIX_TRUE;
#endif
            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0,
                           sizeof (tSnoopIpGrpFwdEntry));

            /* Update the fields of the IP entry */
            pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_SUCCESS;
            pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;
            SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId, VlanId,
                           sizeof (tSnoopTag));
            pSnoopIpGrpFwdEntry->u1AddressType = SNOOP_ADDR_TYPE_IPV4;

            SNOOP_UPDATE_PORT_LIST (FwdPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);

            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                           (UINT1 *) &SrcIpAddr, sizeof (tIPvXAddr));
            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                           (UINT1 *) &GrpIpAddr, sizeof (tIPvXAddr));
            u4Status =
                RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           (tRBElem *) pSnoopIpGrpFwdEntry);

            if (u4Status == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for IP forward Entry \r\n");
                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
                return SNOOP_FAILURE;
            }
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if ((pSnoopInstInfo != NULL) && (pSnoopIpGrpFwdEntry != NULL))
            {
                pSnoopInfo =
                    &(pSnoopInstInfo->
                      SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
                if (pSnoopInfo != NULL)
                {
                    pSnoopInfo->u4FwdGroupsCnt += 1;

                }
            }
        }
        else
        {
            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
            SNOOP_UPDATE_PORT_LIST (FwdPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);
        }
#ifdef NPAPI_WANTED
        if (SNOOP_IS_NP_PROGRAMMING_ALLOWED () == SNOOP_TRUE)
        {
            if (SnoopUtilNpUpdateMcastFwdEntry (u4Instance, VlanId,
                                                SrcIpAddr, GrpIpAddr,
                                                FwdPortBitmap, SNOOP_ADD_PORT)
                != SNOOP_SUCCESS)
            {
                SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_FWD_NAME,
                                    "RouterPort updation in hardware failed"
                                    " for Source %s,Group %s failed\r\n",
                                    SnoopPrintIPvxAddress (SrcIpAddr),
                                    SnoopPrintIPvxAddress (GrpIpAddr));
                if (b1NewEntry == OSIX_TRUE)
                {
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                }
                return SNOOP_FAILURE;
            }
            if (HW_ADD_GROUP_FAIL == pSnoopIpGrpFwdEntry->u1AddGroupStatus)
            {
                /* If the updation failed, and if PIM has added this
                 *node in the RB Tree, remove all associations.
                 * Probably not the cleanest way, needs to be re-visited. */
                if (b1NewEntry == OSIX_TRUE)
                {
                    SnoopUtilCleanIpForwardingEntry (u4Instance,
                                                     pSnoopIpGrpFwdEntry);
                    SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance,
                                                   pSnoopIpGrpFwdEntry);
                }
                SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                    SNOOP_CONTROL_PATH_TRC |
                                    SNOOP_DBG_ALL_FAILURE,
                                    SNOOP_FWD_NAME,
                                    "RouterPort updation in hardware failed"
                                    " for Source %s,Group %s failed\r\n",
                                    SnoopPrintIPvxAddress (SrcIpAddr),
                                    SnoopPrintIPvxAddress (GrpIpAddr))
                    return SNOOP_FAILURE;
            }
        }
#endif
    }
    return SNOOP_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name      : SnoopUtilIsSrcAndGrpPresentInPim                     */
/*                                                                           */
/* Description        : This function will query PIM whether the given (S,G) */
/*                      is present in the PIM DB,if present then program the */
/*                      H/W else return                                      */
/*                                                                           */
/* Input(s)           : VlanId - Vlan ID                                     */
/*                      pSnoopPktInfo - Pointer to Data Structure which will */
/*                      contain packet related information.                  */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/OSIX_FALSE                                 */
/*****************************************************************************/
INT4
SnoopUtilIsSrcAndGrpPresentInPim (UINT4 u4Instance, tSnoopTag VlanId,
                                  tSnoopPktInfo * pSnoopPktInfo,
                                  UINT1 u1EventType)
{
    tVlanId             SnoopVlanId;
    tSnoopPortBmp       FwdPortBitmap;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopIpGrpFwdEntry IpGrpFwdEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;
    tRBElem            *pRBElem = NULL;
    UINT4               u4Status = 0;
    BOOL1               bIsSrcGrpFound = OSIX_FALSE;

    SNOOP_MEM_SET (&SnoopVlanId, 0, sizeof (tVlanId));
    SNOOP_MEM_SET (&FwdPortBitmap, 0, sizeof (tSnoopPortBmp));
    SNOOP_MEM_SET (&IpGrpFwdEntry, 0, sizeof (tSnoopIpGrpFwdEntry));

    SnoopVlanId = SNOOP_OUTER_VLAN (VlanId);
#ifdef PIM_WANTED
    bIsSrcGrpFound =
        SPimPortCheckSGVlanId (SnoopVlanId, pSnoopPktInfo->SrcIpAddr,
                               pSnoopPktInfo->GroupAddr);
#endif
    if (OSIX_TRUE == bIsSrcGrpFound)
    {
        SNOOP_ADD_TO_PORT_LIST (pSnoopPktInfo->u4InPort, FwdPortBitmap);

        pRBElem
            = RBTreeGet (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                         (tRBElem *) & IpGrpFwdEntry);
        if (pRBElem == NULL)
        {
            if (SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK
                (u4Instance, pSnoopIpGrpFwdEntry) == NULL)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_OS_RES_DBG,
                           "Memory allocation for IP forward entry \r\n");
                SYSLOG_SNOOP_MSG_ARG2 (SYSLOG_CRITICAL_LEVEL, SNOOP_DBG_FLAG,
                                       SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                                       SnoopSysErrString
                                       [SYS_LOG_MEM_ALLOC_FAIL],
                                       "for u4Instance %d\r\n", u4Instance);
                return SNOOP_FAILURE;
            }

            SNOOP_MEM_SET (pSnoopIpGrpFwdEntry, 0,
                           sizeof (tSnoopIpGrpFwdEntry));
            SNOOP_MEM_CPY ((UINT1 *) &(IpGrpFwdEntry.SrcIpAddr),
                           (UINT1 *) &(pSnoopPktInfo->SrcIpAddr),
                           sizeof (tIPvXAddr));
            IPVX_ADDR_COPY ((UINT1 *) &(IpGrpFwdEntry.GrpIpAddr),
                            (UINT1 *) &(pSnoopPktInfo->GroupAddr));
            /* Update the fields of the IP entry */
            pSnoopIpGrpFwdEntry->u1AddGroupStatus = HW_ADD_GROUP_SUCCESS;
            pSnoopIpGrpFwdEntry->u1AddPortStatus = HW_ADD_PORT_SUCCESS;
            SNOOP_MEM_CPY (pSnoopIpGrpFwdEntry->VlanId,
                           VlanId, sizeof (tSnoopTag));
            pSnoopIpGrpFwdEntry->u1AddressType = SNOOP_ADDR_TYPE_IPV4;

            SNOOP_UPDATE_PORT_LIST (FwdPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);
            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                           &(pSnoopPktInfo->SrcIpAddr), sizeof (tIPvXAddr));
            SNOOP_MEM_CPY ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                           &(pSnoopPktInfo->GroupAddr), sizeof (tIPvXAddr));
            u4Status =
                RBTreeAdd (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           (tRBElem *) pSnoopIpGrpFwdEntry);

            if (u4Status == RB_FAILURE)
            {
                SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                               SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                               SNOOP_OS_RES_NAME,
                               "RBTree Addition Failed for IP forward Entry \r\n");
                SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
                return SNOOP_FAILURE;
            }
            pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
            if ((pSnoopInstInfo != NULL) && (pSnoopIpGrpFwdEntry != NULL))
            {
                pSnoopInfo =
                    &(pSnoopInstInfo->
                      SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
                if (pSnoopInfo != NULL)
                {
                    pSnoopInfo->u4FwdGroupsCnt += 1;
                }
            }
        }
        else
        {
            pSnoopIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;
            SNOOP_UPDATE_PORT_LIST (FwdPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);
        }
#ifdef NPAPI_WANTED
        if (SnoopUtilNpUpdateMcastFwdEntry (u4Instance, VlanId,
                                            pSnoopPktInfo->SrcIpAddr,
                                            pSnoopPktInfo->GroupAddr,
                                            FwdPortBitmap,
                                            u1EventType) != SNOOP_SUCCESS)
        {
            SNOOP_GBL_DBG_ARG2 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_FWD_NAME,
                                " IP Forwarding DataBase Updation in hardware failed"
                                " for Source %s,Group %s \r\n",
                                SnoopPrintIPvxAddress (pSnoopPktInfo->
                                                       SrcIpAddr),
                                SnoopPrintIPvxAddress (pSnoopPktInfo->
                                                       GroupAddr));
            SNOOP_VLAN_IP_FWD_FREE_MEMBLK (u4Instance, pSnoopIpGrpFwdEntry);
            return SNOOP_FAILURE;
        }
#else
        UNUSED_PARAM (u1EventType);
#endif
        return SNOOP_SUCCESS;
    }
    else
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_DBG_RESRC, SNOOP_OS_RES_DBG,
                   "This (S,G) Entry was not found in PIM DB\r\n");
        return OSIX_FALSE;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetIgsStatus                            */
/*                                                                           */
/*    Description         : This function validates the given VLAN ID        */
/*                          and check IP based IGS and Sparse Mode is        */
/*                          enabled in a vlan                                */
/*                                                                           */
/*    Input(s)            : VlanId - VLAN Identifier                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS/OSIX_FAILURE                        */
/*****************************************************************************/
INT4
SnoopUtilGetIgsStatus (tVlanId VlanId)
{
    UINT4               u4Instance = 0;
    tSnoopTag           SnoopVlanId;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;

    MEMSET (&SnoopVlanId, 0, sizeof (tSnoopTag));

    if (SNOOP_SYSTEM_STATUS (u4Instance) == SNOOP_SHUTDOWN)
    {
        return OSIX_FAILURE;
    }

    SNOOP_OUTER_VLAN (SnoopVlanId) = (tVlanId) VlanId;

    if (SnoopVlanGetVlanEntry (u4Instance, SnoopVlanId,
                               SNOOP_ADDR_TYPE_IPV4,
                               &pSnoopVlanEntry) == SNOOP_SUCCESS)
    {
        if ((SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&
            (SNOOP_SYSTEM_SPARSE_MODE (u4Instance) == SNOOP_ENABLE) &&
            (pSnoopVlanEntry->u1SnoopStatus == SNOOP_ENABLE))
        {
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilGetPIMInterfaceStatus                   */
/*                                                                           */
/*    Description         : This Function will check whether PIM is enabled  */
/*                          on the VLAN interface, if yes then PIM util will */
/*                          return OSIX_SUCCESS, else it will return         */
/*                          OSIX_FAILURE                                     */
/*                                                                           */
/*    Input(s)            : SnoopVlanId - VLAN Identifier                    */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : SNOOP_SUCCESS/SNOOP_FAILURE                      */
/*****************************************************************************/
INT4
SnoopUtilGetPIMInterfaceStatus (tSnoopTag SnoopVlanId)
{
    tVlanId             VlanId;
    UINT4               u4VlanIfIndex = 0;

    MEMSET (&VlanId, 0, sizeof (tVlanId));
    VlanId = SNOOP_OUTER_VLAN (SnoopVlanId);

#ifdef PIM_WANTED
    if ((PimIsPimEnabled () == PIMSM_FAILURE))
    {
        return SNOOP_FAILURE;
    }
#endif
    u4VlanIfIndex = CfaGetVlanInterfaceIndex (VlanId);

    if (CFA_INVALID_INDEX == u4VlanIfIndex)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME, "Invalid VLAN Index \r\n");
        return SNOOP_FAILURE;
    }
#ifdef PIM_WANTED
    /* The below function will return the status of the PIM module */
    if (SPimPortCheckDROnInterface (u4VlanIfIndex) == OSIX_SUCCESS)
    {
        return SNOOP_SUCCESS;
    }
    else
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "PIM is Either Not Globally Enabled or "
                       "PIM is not enabled in the VLAN Interface\r\n");
        return SNOOP_FAILURE;
    }
#endif
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  SnoopUtilFetchFourBytesFromPointer              */
/*                                                                           */
/*    Description         :  This function copies four bytes of info from the*/
/*                          input parameter. And returns the value of copied */
/*                           variable to calling function.                   */
/*    Input(s)            : pu1Ptr                                           */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : u4Value                                          */
/*****************************************************************************/

UINT4
SnoopUtilFetchFourBytesFromPointer (UINT1 *pu1Ptr)
{
    UINT4               u4Value = 0;
    MEMCPY (&u4Value, pu1Ptr, sizeof (UINT4));
    return u4Value;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SnoopUtilCleanIpForwardingEntry                  */
/*                                                                           */
/*    Description         : This function will remove the IPMC entry from    */
/*                          the tree and undo other related actions.         */
/*                                                                           */
/*    Input(s)            : u4Instance - Instance ID                         */
/*                          pSnoopIpGrpFwdEntry = IPMC Entry                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*****************************************************************************/
VOID
SnoopUtilCleanIpForwardingEntry (UINT4 u4Instance,
                                 tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry)
{
    tSnoopFilter        SnoopDelEntry;
    tSnoopGlobalInstInfo *pSnoopInstInfo = NULL;
    tSnoopInfo         *pSnoopInfo = NULL;

    if (pSnoopIpGrpFwdEntry->EntryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopIpGrpFwdEntry->EntryTimer);
    }

    /* The IPMC node is removed here, if indications need to be given
     * to others using it, the below call will perform the same,
     * currently un-implemented, nevertheless called.
     */
    SNOOP_MEM_SET (&SnoopDelEntry, 0, sizeof (tSnoopFilter));

    SnoopDelEntry.u1NotifyType = SNOOP_FWD_ENTRY_AGEOUT_NOTIFY;
    SNOOP_MEM_CPY (SnoopDelEntry.VlanId, pSnoopIpGrpFwdEntry->VlanId,
                   sizeof (tSnoopTag));
    SNOOP_MEM_CPY (&(SnoopDelEntry.SrcIpAddr),
                   &(pSnoopIpGrpFwdEntry->SrcIpAddr.au1Addr),
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_MEM_CPY (&(SnoopDelEntry.GrpAddress),
                   &(pSnoopIpGrpFwdEntry->GrpIpAddr.au1Addr),
                   IPVX_MAX_INET_ADDR_LEN);

    SNOOP_INET_NTOHL (SnoopDelEntry.SrcIpAddr);
    SNOOP_INET_NTOHL (SnoopDelEntry.GrpAddress);

    SnoopUtilFilterGrpsAndNotify (&SnoopDelEntry);

    SNOOP_MEM_SET (pSnoopIpGrpFwdEntry->PortBitmap, 0, SNOOP_PORT_LIST_SIZE);

    RBTreeRemove (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                  (tRBElem *) pSnoopIpGrpFwdEntry);
    pSnoopInstInfo = SNOOP_INSTANCE_INFO (u4Instance);
    if (pSnoopInstInfo != NULL)
    {
        pSnoopInfo =
            &(pSnoopInstInfo->
              SnoopInfo[pSnoopIpGrpFwdEntry->u1AddressType - 1]);
        if ((pSnoopInfo != NULL) && (pSnoopInfo->u4FwdGroupsCnt != 0))
        {
            pSnoopInfo->u4FwdGroupsCnt -= 1;
        }
    }
    return;
}

/****************************************************************************/
/* Function Name         :   SnoopPrintIPvxAddress                            */
/*                                                                          */
/* Description           :   Converts the Input IpAddress into String and   */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   Addr: Ip Address                               */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/
UINT1              *
SnoopPrintIPvxAddress (tIPvXAddr Addr)
{
    return (SnoopPrintAddress (Addr.au1Addr, Addr.u1Afi));
}

/****************************************************************************/
/* Function Name         :   SnoopPrintAddress                                */
/*                                                                          */
/* Description           :   Converts the Input IpAddress into String and   */
/*                           Prints the IPAddress in the standard format    */
/*                                                                          */
/* Input (s)             :   pu1Addr: Ip Address                            */
/*                           u1Afi  : Ip Address Family                     */
/* Output (s)            :   None                                            */
/*                                                                          */
/* Global Variables Referred : None                                         */
/*                                                                          */
/* Global Variables Modified : None                                         */
/*                                                                          */
/* Returns             : string containing the IpAddress in printable format*/
/****************************************************************************/
UINT1              *
SnoopPrintAddress (UINT1 *pu1Addr, UINT1 u1Afi)
{
    tIp6Addr            Ip6Addr;
    if (pu1Addr == NULL)
    {
        return NULL;
    }
    MEMSET (&Ip6Addr, SNOOP_ZERO, sizeof (tIp6Addr));
    if (u1Afi == IPVX_ADDR_FMLY_IPV4)
    {
        MEMCPY (&Ip6Addr.u1_addr[SNOOP_IPV6_THIRD_WORD_OFFSET], pu1Addr,
                IPVX_IPV4_ADDR_LEN);
    }
    else if (u1Afi == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (&Ip6Addr.u1_addr, pu1Addr, IPVX_IPV6_ADDR_LEN);
    }
    else
    {
        return NULL;
    }
    return (Ip6PrintAddr (&Ip6Addr));
}

/****************************************************************************/
/* Function Name         :   SnoopUtilDeleteIpFwdEntriesForOuterVlan        */
/*                                                                          */
/* Description           :   This function deletes the forwarding database  */
/*                           present for [S,0] entries                      */
/*                                                                          */
/* Input (s)             :   u4InstId - Instance Id                         */
/*                           pSnoopGroupEntry  -   Group entry              */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables                                                         */
/* Referred              :   None                                           */
/*                                                                          */
/* Global Variables      :   None                                           */
/* Modified                                                                 */
/* Returns               :   SNOOP_SUCCESS/SNOOP_FAILURE                    */
/*                                                                          */
/****************************************************************************/

INT4
SnoopUtilDeleteIpFwdEntriesForOuterVlan (UINT4 u4InstId,
                                         tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopIpGrpFwdEntry *pTempIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT2               u2InnerVlanId = 0;

    if (pSnoopGroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC, "Group entry not present \r\n");
        return SNOOP_FAILURE;
    }

    u2InnerVlanId = SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId);

    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = SNOOP_INVALID_VLAN_ID;

    if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_IP)
    {
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4InstId)->
                                  IpMcastFwdEntry);
        while (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4InstId)->
                                         IpMcastFwdEntry, pRBElem, NULL);

            pTempIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if ((pTempIpGrpFwdEntry != NULL) &&
                (SNOOP_MEM_CMP ((UINT1 *) &pSnoopGroupEntry->GroupIpAddr,
                                (UINT1 *) &(pTempIpGrpFwdEntry->
                                            GrpIpAddr),
                                sizeof (tIPvXAddr)) == 0) &&
                (SNOOP_OUTER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
                 SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)) &&
                (SNOOP_INNER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
                 SNOOP_INVALID_VLAN_ID))
            {

                if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry,
                                              pTempIpGrpFwdEntry->SrcIpAddr,
                                              0,
                                              pTempIpGrpFwdEntry->PortBitmap,
                                              SNOOP_DELETE_FWD_ENTRY) !=
                    SNOOP_SUCCESS)
                {
                    SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4InstId),
                                   SNOOP_CONTROL_PATH_TRC |
                                   SNOOP_DBG_ALL_FAILURE, SNOOP_VLAN_NAME,
                                   "Unable to delete the IP forwarding entry in "
                                   "hardware\r\n");
                    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = u2InnerVlanId;
                    return SNOOP_FAILURE;
                }

            }

            pRBElem = pRBNextElem;
        }
    }

    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = u2InnerVlanId;

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopUtilAddPortToVlanIpFwdTable               */
/*                                                                          */
/* Description           :   This function processes [S,0] data and creates */
/*                           forwarding database for all the customer vlans */
/*                           learnt on the same vlan, same group.           */
/*                                                                          */
/*                                                                          */
/* Input (s)             :   u4InstId - Instance Id                         */
/*                           pSnoopGroupEntry  -   Group entry              */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables                                                         */
/* Referred              :   None                                           */
/*                                                                          */
/* Global Variables      :   None                                           */
/* Modified                                                                 */
/* Returns               :   SNOOP_SUCCESS/SNOOP_FAILURE                    */
/*                                                                          */
/****************************************************************************/

INT4
SnoopUtilAddPortToVlanIpFwdTable (UINT4 u4InstId,
                                  tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopIpGrpFwdEntry *pTempIpGrpFwdEntry = NULL;
    tSnoopPktInfo       SnoopPktInfo;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    UINT2               u2InnerVlanId = 0;
    UINT2               u2SrcIndex = 0;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;
    UINT1               u1SGCreate = SNOOP_FALSE;
    UINT1               u1IsUnReg = SNOOP_FALSE;

    if (pSnoopGroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC, "Group entry not present \r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (&SnoopPktInfo, 0, sizeof (tSnoopPktInfo));

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        return SNOOP_FAILURE;
    }

    u2InnerVlanId = SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId);

    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = SNOOP_INVALID_VLAN_ID;

    if (SNOOP_MCAST_FWD_MODE (u4InstId) == SNOOP_MCAST_FWD_MODE_IP)
    {
        pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4InstId)->
                                  IpMcastFwdEntry);
        while (pRBElem != NULL)
        {
            pRBNextElem = RBTreeGetNext (SNOOP_INSTANCE_INFO (u4InstId)->
                                         IpMcastFwdEntry, pRBElem, NULL);

            pTempIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

            if ((pTempIpGrpFwdEntry != NULL) &&
                (SNOOP_MEM_CMP ((UINT1 *) &pSnoopGroupEntry->GroupIpAddr,
                                (UINT1 *) &(pTempIpGrpFwdEntry->
                                            GrpIpAddr),
                                sizeof (tIPvXAddr)) == 0) &&
                (SNOOP_OUTER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
                 SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId)))
            {

                /* Check whether for this group entry data is needed
                   from same source. If Yes add the port in forwarding
                   database
                 */

                SNOOP_MEM_CPY (SnoopPktInfo.SrcIpAddr.au1Addr,
                               pTempIpGrpFwdEntry->SrcIpAddr.au1Addr,
                               SNOOP_MAC_ADDR_LEN);

                if (SnoopCheckToCreateIpFwdEntry
                    (u4InstId, &SnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
                     &u1SGCreate, &u1IsUnReg) != SNOOP_SUCCESS)
                {
                    if (u1IsUnReg == SNOOP_TRUE)
                    {
                        SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) =
                            u2InnerVlanId;
                    }

                    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                    return SNOOP_FAILURE;
                }

                if (u1SGCreate == SNOOP_TRUE)
                {
                    /* Check if any V2 receivers are present */
                    if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) != 0)
                    {
                        u1ASMHostPresent = SNOOP_TRUE;
                    }

                    MEMSET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));
                    SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                              u1ASMHostPresent, pFwdPortBitmap);

                    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = u2InnerVlanId;

                    /* Create a new (S, G) entry */
                    if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopGroupEntry,
                                                  pTempIpGrpFwdEntry->SrcIpAddr,
                                                  0, pFwdPortBitmap,
                                                  SNOOP_CREATE_FWD_ENTRY) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_VLAN_TRC,
                                   "Unable to Create the IP forwarding entry in "
                                   "hardware\r\n");
                        SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) =
                            u2InnerVlanId;
                        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
                        return SNOOP_FAILURE;

                    }

                    /* Created a new multicast forwarding entry */
                    break;
                }
            }

            pRBElem = pRBNextElem;
        }
    }

    SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) = u2InnerVlanId;
    UtilPlstReleaseLocalPortList (pFwdPortBitmap);

    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopUtilProcessDoubleTagDataForOuterVlan      */
/*                                                                          */
/* Description           :   This function processes [S,C] data and creates */
/*                           forwarding database for all the customer vlans */
/*                           learnt on the same vlan, same group.           */
/*                                                                          */
/* Input (s)             :   u4InstId - Instance Id                         */
/*                           pSnoopPktInfo  -   Packet information          */
/*                           pSnoopGroupEntry  -   Group entry              */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables                                                         */
/* Referred              :   None                                           */
/*                                                                          */
/* Global Variables      :   None                                           */
/* Modified                                                                 */
/* Returns               :   SNOOP_SUCCESS/SNOOP_FAILURE                    */
/*                                                                          */
/****************************************************************************/

INT4
SnoopUtilProcessDoubleTagDataForOuterVlan (UINT4 u4InstId,
                                           tSnoopPktInfo * pSnoopPktInfo,
                                           tSnoopGroupEntry * pSnoopGroupEntry)
{
    tSnoopConsolidatedGroupEntry *pSnoopConsGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopRefGroupEntry = NULL;
    tSnoopGroupEntry   *pSnoopTempGroupEntry = NULL;
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    UINT1              *pFwdPortBitmap = NULL;
    tSnoopTag           RtrVlanId;
    tSnoopGroupEntry    SnoopGroupEntry;
    UINT2               u2SrcIndex = 0;
    UINT1               u1IsUnReg = SNOOP_FALSE;
    UINT1               u1SGCreate = SNOOP_FALSE;
    UINT1               u1ASMHostPresent = SNOOP_FALSE;

    if (pSnoopGroupEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC, "Group entry not present \r\n");
        return SNOOP_FAILURE;
    }

    if (pSnoopPktInfo == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC, "Packet info not present \r\n");
        return SNOOP_FAILURE;
    }

    pFwdPortBitmap = UtilPlstAllocLocalPortList (sizeof (tSnoopPortBmp));

    SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));
    SNOOP_MEM_SET (RtrVlanId, 0, sizeof (tSnoopTag));

    if (pFwdPortBitmap == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_OS_RES_DBG,
                   "Error in allocating memory for pFwdPortBitmap\r\n");
        return SNOOP_FAILURE;
    }

    SNOOP_MEM_SET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));

    if (SNOOP_INNER_VLAN (pSnoopGroupEntry->VlanId) != SNOOP_INVALID_VLAN_ID)
    {
        if (SnoopGrpGetConsGroupEntry (u4InstId, pSnoopGroupEntry->VlanId,
                                       pSnoopGroupEntry->GroupIpAddr,
                                       &pSnoopConsGroupEntry) != SNOOP_SUCCESS)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_VLAN_TRC,
                       "Consolidated Group entry not present \r\n");
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_FAILURE;
        }

        if (pSnoopConsGroupEntry == NULL)
        {
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                       SNOOP_VLAN_TRC,
                       "Consolidated Group entry not present \r\n");
            UtilPlstReleaseLocalPortList (pFwdPortBitmap);
            return SNOOP_FAILURE;
        }

        SNOOP_UTL_DLL_OFFSET_SCAN (&(pSnoopConsGroupEntry->GroupEntryList),
                                   pSnoopRefGroupEntry, pSnoopTempGroupEntry,
                                   tSnoopGroupEntry *)
        {
            /* Except this group, check to create a forwarding database */

            if ((pSnoopRefGroupEntry != NULL) &&
                (pSnoopRefGroupEntry != pSnoopGroupEntry))
            {
                u1IsUnReg = SNOOP_FALSE;
                u1SGCreate = SNOOP_FALSE;
                u1ASMHostPresent = SNOOP_FALSE;

                if (SnoopCheckToCreateIpFwdEntry
                    (u4InstId, pSnoopPktInfo, pSnoopRefGroupEntry, &u2SrcIndex,
                     &u1SGCreate, &u1IsUnReg) != SNOOP_SUCCESS)
                {
                    if (u1IsUnReg == SNOOP_TRUE)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_VLAN_TRC,
                                   "Unrecognized data frame \r\n");
                    }
                }

                if (u1SGCreate == SNOOP_TRUE)
                {
                    /* Check if any V2 receivers are present */
                    if (SNOOP_SLL_COUNT (&pSnoopRefGroupEntry->ASMPortList) !=
                        0)
                    {
                        u1ASMHostPresent = SNOOP_TRUE;
                    }

                    SNOOP_MEM_SET (pFwdPortBitmap, 0, sizeof (tSnoopPortBmp));
                    SnoopUtilGetForwardPorts (pSnoopRefGroupEntry, u2SrcIndex,
                                              u1ASMHostPresent, pFwdPortBitmap);

                    /* Create a new (S, G) entry */
                    if (SnoopFwdUpdateIpFwdTable (u4InstId, pSnoopRefGroupEntry,
                                                  pSnoopPktInfo->SrcIpAddr,
                                                  0, pFwdPortBitmap,
                                                  SNOOP_CREATE_FWD_ENTRY) !=
                        SNOOP_SUCCESS)
                    {
                        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_VLAN_TRC,
                                   "Unable to Create the IP forwarding entry in "
                                   "hardware\r\n");

                    }

                    /* Created a new multicast forwarding entry */

                }                /* SG Create End */

            }                    /* Reference entry */

        }                        /* For all the vlan's */

    }                            /* Inner vlan id present */

    /* In Enhanced mode the data needs to be forwarded to router port if the
     * entry was not created in the above DLL_SCAN. */

    /* Check whether VLAN entry is present */

    if (SnoopVlanGetVlanEntry (u4InstId, pSnoopGroupEntry->VlanId,
                               pSnoopPktInfo->DestIpAddr.u1Afi,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC,
                   "Unable to Create the IP forwarding entry in "
                   "hardware\r\n");
    }

    if (pSnoopVlanEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_VLAN_TRC, "Vlan entry is not present \r\n");
        UtilPlstReleaseLocalPortList (pFwdPortBitmap);
        return SNOOP_FAILURE;
    }

    /* If router port entries available, create [S,0] forwarding entry */

    if ((SNOOP_MEM_CMP (pSnoopVlanEntry->RtrPortBitmap, gNullPortBitMap,
                        SNOOP_PORT_LIST_SIZE) != 0))
    {
        SNOOP_OUTER_VLAN (RtrVlanId) =
            SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
        SNOOP_INNER_VLAN (RtrVlanId) = SNOOP_INVALID_VLAN_ID;

        if (SnoopFwdGetIpForwardingEntry (u4InstId, RtrVlanId,
                                          pSnoopPktInfo->DestIpAddr,
                                          pSnoopPktInfo->SrcIpAddr,
                                          &pSnoopIpGrpFwdEntry)
            != SNOOP_SUCCESS)
        {
            SNOOP_MEM_SET (pFwdPortBitmap, 0, SNOOP_PORT_LIST_SIZE);

            if (SnoopGrpGetGroupEntry (u4InstId, RtrVlanId,
                                       pSnoopPktInfo->DestIpAddr,
                                       &pSnoopGroupEntry) == SNOOP_SUCCESS)
            {
                if (SnoopCheckToCreateIpFwdEntry
                    (u4InstId, pSnoopPktInfo, pSnoopGroupEntry, &u2SrcIndex,
                     &u1SGCreate, &u1IsUnReg) == SNOOP_SUCCESS)
                {
                    if (u1SGCreate == SNOOP_TRUE)
                    {
                        /* Check if any V2 receivers are present */
                        if (SNOOP_SLL_COUNT (&pSnoopGroupEntry->ASMPortList) !=
                            0)
                        {
                            u1ASMHostPresent = SNOOP_TRUE;
                        }
                        /* Get the list of ports on which receivers are attached
                         * for this (S,G) entry */
                        SnoopUtilGetForwardPorts (pSnoopGroupEntry, u2SrcIndex,
                                                  u1ASMHostPresent,
                                                  pFwdPortBitmap);
                    }
                }
            }

            SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                    pFwdPortBitmap);

            SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

            SNOOP_OUTER_VLAN (SnoopGroupEntry.VlanId) =
                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);
            SNOOP_MEM_CPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                           (UINT1 *) &(pSnoopPktInfo->DestIpAddr),
                           sizeof (tIPvXAddr));

            /* Create a new (S, G) entry */
            if (SnoopFwdUpdateIpFwdTable (u4InstId, &SnoopGroupEntry,
                                          pSnoopPktInfo->SrcIpAddr,
                                          0, pFwdPortBitmap,
                                          SNOOP_CREATE_FWD_ENTRY) !=
                SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_VLAN_TRC,
                           "Unable to Create the IP forwarding entry in "
                           "hardware\r\n");

            }
        }
        else
        {
            /* Forwarding entry already present */

            SNOOP_UPDATE_PORT_LIST (pSnoopVlanEntry->RtrPortBitmap,
                                    pSnoopIpGrpFwdEntry->PortBitmap);

            SNOOP_MEM_SET (&SnoopGroupEntry, 0, sizeof (tSnoopGroupEntry));

            SNOOP_OUTER_VLAN (SnoopGroupEntry.VlanId) =
                SNOOP_OUTER_VLAN (pSnoopGroupEntry->VlanId);

            SNOOP_MEM_CPY ((UINT1 *) &(SnoopGroupEntry.GroupIpAddr),
                           (UINT1 *) &(pSnoopPktInfo->DestIpAddr),
                           sizeof (tIPvXAddr));

            SNOOP_MEM_CPY (pFwdPortBitmap, pSnoopIpGrpFwdEntry->PortBitmap,
                           SNOOP_PORT_LIST_SIZE);

            if (SnoopFwdUpdateIpFwdTable (u4InstId, &SnoopGroupEntry,
                                          pSnoopPktInfo->SrcIpAddr,
                                          0, pFwdPortBitmap,
                                          SNOOP_ADD_PORTLIST) != SNOOP_SUCCESS)
            {
                SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                           SNOOP_VLAN_TRC,
                           "Unable to Create the IP forwarding entry in "
                           "hardware\r\n");

            }
        }
    }

    UtilPlstReleaseLocalPortList (pFwdPortBitmap);
    return SNOOP_SUCCESS;
}

/****************************************************************************/
/* Function Name         :   SnoopIsOuterVlanIpFwdEntryExists               */
/*                                                                          */
/* Description           :   This function checks whether any forwarding    */
/*                           entry exists for outer-vlan and returns the    */
/*                           status                                         */
/*                                                                          */
/* Input (s)             :   u4InstId - Instance Id                         */
/*                           pSnoopIpGrpFwdEntry -  Forwarding entry        */
/*                                                                          */
/* Output (s)            :   None                                           */
/*                                                                          */
/* Global Variables                                                         */
/* Referred              :   None                                           */
/*                                                                          */
/* Global Variables      :   None                                           */
/* Modified                                                                 */
/* Returns               :   SNOOP_SUCCESS/SNOOP_FAILURE                    */
/*                                                                          */
/****************************************************************************/
INT4
SnoopIsOuterVlanIpFwdEntryExists (UINT4 u4InstId,
                                  tSnoopIpGrpFwdEntry * pSnoopIpGrpFwdEntry)
{
    tSnoopIpGrpFwdEntry *pTempIpGrpFwdEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;

    if (pSnoopIpGrpFwdEntry == NULL)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_VLAN_TRC,
                   "Input validation failed at SnoopIsOuterVlanIpFwdEntryExists \r\n");
        return SNOOP_FAILURE;
    }

    /* If enhanced mode is disabled, no need to check for
       forwarding entry with inner-vlan 
     */

    if (SNOOP_INSTANCE_ENH_MODE (u4InstId) == SNOOP_DISABLE)
    {
        return SNOOP_SUCCESS;
    }

    /* Get the first entry */

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4InstId)->IpMcastFwdEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4InstId)->IpMcastFwdEntry,
                           pRBElem, NULL);

        pTempIpGrpFwdEntry = (tSnoopIpGrpFwdEntry *) pRBElem;

        if ((pTempIpGrpFwdEntry != NULL) &&
            (pTempIpGrpFwdEntry != pSnoopIpGrpFwdEntry) &&
            (SNOOP_MEM_CMP ((UINT1 *) &(pSnoopIpGrpFwdEntry->SrcIpAddr),
                            (UINT1 *) &(pTempIpGrpFwdEntry->
                                        SrcIpAddr),
                            sizeof (tIPvXAddr)) == 0) &&
            (SNOOP_MEM_CMP ((UINT1 *) &(pSnoopIpGrpFwdEntry->GrpIpAddr),
                            (UINT1 *) &(pTempIpGrpFwdEntry->
                                        GrpIpAddr),
                            sizeof (tIPvXAddr)) == 0) &&
            (SNOOP_OUTER_VLAN (pTempIpGrpFwdEntry->VlanId) ==
             SNOOP_OUTER_VLAN (pSnoopIpGrpFwdEntry->VlanId)))
        {
            /* There is some more entry present for this
             * Outer-VLAN so no need to delete the entry */
            return SNOOP_SUCCESS;
        }

        pRBElem = pRBNextElem;
    }

    /* No forwarding entry present for outer-vlan.
       This is the last entry. 
     */

    return SNOOP_FAILURE;
}

VOID
SnoopHandleMsrRestoreEvent (VOID)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    tRBElem            *pRBElem = NULL;
    tRBElem            *pRBNextElem = NULL;
    UINT4               u4Instance = SNOOP_DEFAULT_INSTANCE;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
#ifdef L2RED_WANTED
    if (RmGetNodeState () == RM_STANDBY)
    {
        return;
    }
#endif

    if (SNOOP_SNOOPING_STATUS (u4Instance,
                               (SNOOP_ADDR_TYPE_IPV4 - 1)) == SNOOP_DISABLE)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME, "Snooping is disabled globally \n");
        return;
    }

    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_VLAN_NAME, "Snoop Vlan Table not present \n");
        return;
    }

    pRBElem = RBTreeGetFirst (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry);

    while (pRBElem != NULL)
    {
        pRBNextElem =
            RBTreeGetNext (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                           pRBElem, NULL);

        pSnoopVlanEntry = (tSnoopVlanEntry *) pRBElem;

        if ((pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL) &&
            (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigSnoopStatus ==
             SNOOP_DISABLE))
        {
            SNOOP_GBL_DBG_ARG1 (SNOOP_DBG_FLAG_INST (u4Instance),
                                SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                                SNOOP_OS_RES_NAME,
                                "Snooping is disabled on vlan %d \r\n",
                                SNOOP_OUTER_VLAN (pSnoopVlanEntry->VlanId));
            pRBElem = pRBNextElem;
            continue;
        }

        if (pSnoopVlanEntry->pSnoopVlanCfgEntry != NULL)
        {
            if ((pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier ==
                 SNOOP_QUERIER)
                && (pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier !=
                    pSnoopVlanEntry->u1Querier))
            {
                /* Copy the configured Querier value to the VLAN table 
                 * only when there is no dynamic router port 
                 * present for that VLAN
                 */
                pSnoopVlanEntry->u1Querier =
                    pSnoopVlanEntry->pSnoopVlanCfgEntry->u1ConfigQuerier;

                if (pSnoopVlanEntry->u1Querier == SNOOP_QUERIER)
                {
                    if (SnoopVlanSendGeneralQuery
                        (u4Instance, SNOOP_INVALID_PORT,
                         pSnoopVlanEntry) != SNOOP_SUCCESS)
                    {
                        SNOOP_TRC (SNOOP_TRC_FLAG, SNOOP_CONTROL_PATH_TRC,
                                   SNOOP_TMR_TRC,
                                   "Unable to send General Query message on query "
                                   "timer expiry\n");
                        return;
                    }
                    pSnoopVlanEntry->u1StartUpQCount++;
                    /* Transition from Non-Querier to Querier */
                    SnoopHandleTransitionToQuerier (u4Instance,
                                                    pSnoopVlanEntry);
                }
            }
        }

        pRBElem = pRBNextElem;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilUpdateHwIdForIPMC                           */
/*                                                                           */
/* Description        : This function updates the HwId for IPMC entry        */
/*                                                                           */
/* Input(s)           : pIgsHwIpFwdInfo - Pointer to tIgsHwIpFwdInfo         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopUtilUpdateHwIdForIPMC (tIgsHwIpFwdInfo * pIgsHwIpFwdInfo)
{
    tSnoopIpGrpFwdEntry *pSnoopIpGrpFwdEntry = NULL;
    tSnoopTag           SnoopTag;
    tIPvXAddr           GrpAddr;
    tIPvXAddr           SrcAddr;

    MEMSET (&SnoopTag, 0, sizeof (tSnoopTag));
    MEMSET (&GrpAddr, 0, sizeof (tIPvXAddr));
    MEMSET (&SrcAddr, 0, sizeof (tIPvXAddr));

    SnoopTag[0] = pIgsHwIpFwdInfo->OuterVlanId;
    SnoopTag[1] = pIgsHwIpFwdInfo->InnerVlanId;

    GrpAddr.u1Afi = SNOOP_ADDR_TYPE_IPV4;
    GrpAddr.u1AddrLen = IPVX_MAX_INET_ADDR_LEN;
    SNOOP_MEM_CPY (GrpAddr.au1Addr, &pIgsHwIpFwdInfo->u4GrpAddr,
                   SNOOP_IP_ADDR_SIZE);

    SrcAddr.u1Afi = SNOOP_ADDR_TYPE_IPV4;
    SrcAddr.u1AddrLen = IPVX_MAX_INET_ADDR_LEN;
    SNOOP_MEM_CPY (SrcAddr.au1Addr, &pIgsHwIpFwdInfo->u4SrcAddr,
                   SNOOP_IP_ADDR_SIZE);

    if ((SnoopFwdGetIpForwardingEntry (pIgsHwIpFwdInfo->u4Instance, SnoopTag,
                                       GrpAddr,
                                       SrcAddr,
                                       &pSnoopIpGrpFwdEntry) == SNOOP_SUCCESS))
    {
        if (pIgsHwIpFwdInfo->u4SnoopHwId != SNOOP_ZERO)
        {
            pSnoopIpGrpFwdEntry->u4SnoopHwId = pIgsHwIpFwdInfo->u4SnoopHwId;
        }
        return SNOOP_SUCCESS;
    }
    return SNOOP_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SnoopUtilHandleQuerierOnChange                    */
/*                                                                           */
/* Description        : This function does the following in a querier switch */
/*                      (i)  Stop the query timer which is running already.  */
/*                      (ii) Send a General query on all the available       */
/*                           member ports of VLAN.                           */
/*                      (iii) Restart the query timer                        */
/*                                                                           */
/* Input(s)           :  u4Instance - Instance ID                            */
/*                       pSnoopVlanEntry - VLAN information                  */
/*                                                                           */
/* Output(s)          :  None                                                */
/*                                                                           */
/* Return Value(s)    :  None                                                  */
/*                                                                           */
/*****************************************************************************/
VOID
SnoopUtilHandleQuerierOnChange (tSnoopVlanEntry * pSnoopVlanEntry,
                                UINT4 u4Instance)
{
    UINT2               u2QueryInterval = 0;
    /* Stop the periodic Query timer. */
    if (pSnoopVlanEntry->QueryTimer.u1TimerType != SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->QueryTimer);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_TMR_TRC, "Stopped Query Timer \r\n");
    }
    /* We have to stop the Other Querier Present timer. */
    if (pSnoopVlanEntry->OtherQPresentTimer.u1TimerType !=
        SNOOP_INVALID_TIMER_TYPE)
    {
        SnoopTmrStopTimer (&pSnoopVlanEntry->OtherQPresentTimer);
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC, SNOOP_TMR_TRC,
                   "Stopped Other Querier present timer \r\n");
    }

    if ((pSnoopVlanEntry->u1SnoopStatus == SNOOP_DISABLE) ||
        (SNOOP_SNOOPING_STATUS (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1)
         == SNOOP_DISABLE))
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_QRY_DBG, "Snoop is disabled. \r\n");
        return;
    }

    if (MsrIsMibRestoreInProgress () == MSR_TRUE)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_TMR_TRC, "MSR is in progress \r\n");
        return;
    }
    pSnoopVlanEntry->u1Querier = SNOOP_QUERIER;

    /* Send VLAN General query */
    if (SnoopVlanSendGeneralQuery (u4Instance, SNOOP_INVALID_PORT,
                                   pSnoopVlanEntry) != SNOOP_SUCCESS)
    {
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_CONTROL_PATH_TRC,
                   SNOOP_QRY_DBG, "Unable to send General Query message \n");
    }

    if (pSnoopVlanEntry->u1StartUpQCount < pSnoopVlanEntry->u1MaxStartUpQCount)
    {
        pSnoopVlanEntry->u1StartUpQCount++;
    }

    if (pSnoopVlanEntry->u1StartUpQCount >= pSnoopVlanEntry->u1MaxStartUpQCount)
    {
        u2QueryInterval = pSnoopVlanEntry->u2QueryInterval;
    }
    else
    {
        u2QueryInterval = pSnoopVlanEntry->u2StartUpQInterval;
    }

    /* Restart the query timer */
    pSnoopVlanEntry->QueryTimer.u4Instance = u4Instance;
    pSnoopVlanEntry->QueryTimer.u1TimerType = SNOOP_QUERY_TIMER;

    SnoopTmrStartTimer (&pSnoopVlanEntry->QueryTimer, u2QueryInterval);
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopVlanUpdateTagFrame                                    */
/*                                                                           */
/* Description        : This function calls the VLAN Module to send the      */
/*                      tag frame.                                           */
/*                                                                           */
/* Input(s)           : pDupBuf - Buffer to be tagged                        */
/*                      VlanId  - Vlan Identifier                            */
/*                      u1Priority - SNOOP_VLAN_HIGHEST_PRIORITY             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
SnoopVlanUpdateTagFrame (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 *pu4IfIndex,
                         tVlanTag * pVlanTag, UINT2 *pu2TagLen)
{
#ifdef IGS_OPTIMIZE
    UINT1              *pu1Data = NULL;
    UINT1               au1Buf[SNOOP_TAGGED_PROTOCOL_HEADER_OFFSET];
    UINT2               u2EtherType = 0;
    UINT1               u1FrameType = 0;
#endif
#ifndef IGS_OPTIMIZE
    UINT4               u4InstanceId = 0;
    UINT2               u2InstPort = 0;
    if (SnoopVcmGetContextInfoFromIfIndex ((UINT4) *pu4IfIndex,
                                           &u4InstanceId,
                                           &u2InstPort) != VCM_SUCCESS)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME,
                       "Instance ID not found, packet dropped\r\n");
        return SNOOP_FAILURE;
    }

    *pu4IfIndex = (UINT4) u2InstPort;

    if (VlanGetVlanInfoAndUntagFrame (pBuf, *pu4IfIndex,
                                      pVlanTag) == VLAN_NO_FORWARD)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_CONTROL_PATH_TRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_GBL_NAME, "Unable to update Vlan tag\r\n");
        return SNOOP_FAILURE;
    }

    L2IwfUpdateIgmpTagInfo (pBuf, CFA_CUSTOMER_BRIDGE_PORT, pVlanTag,
                            pu2TagLen);
#else

    VLAN_COPY_FROM_BUF (pBuf, au1Buf, 0, SNOOP_TAGGED_PROTOCOL_HEADER_OFFSET);

    pu1Data = au1Buf;

    MEMCPY ((UINT1 *) &u2EtherType, &pu1Data[VLAN_TAG_OFFSET],
            VLAN_TYPE_OR_LEN_SIZE);

    u2EtherType = OSIX_NTOHS (u2EtherType);
    u1FrameType =
        (UINT1) VlanClassifyFrame (pBuf, u2EtherType, (UINT4) *pu4IfIndex);

    if (pVlanTag->OuterVlanTag.u1TagType != VLAN_UNTAGGED)
    {
        VlanUnTagFrame (pBuf);
    }
    L2IwfUpdateIgmpTagInfo (pBuf, CFA_CUSTOMER_BRIDGE_PORT, pVlanTag,
                            pu2TagLen);
#endif

    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopUpdateRtrPortToVlanFwdUnRegPorts                */
/*                                                                           */
/* Description        : This function is called to update the unregports     */
/*                      for all VLANs based on the spard mode to prevent     */
/*                      unknownmulticast pkts entering to snoooping module   */
/* Input(s)           : pSnoopGroupEntry                                     */
/* Output(s)          : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT4
SnoopUpdateRtrPortToVlanFwdUnRegPorts (UINT4 u4Instance, INT4 i4VlanId,
                                       UINT4 u4InPort,
                                       tSnoopPortBmp * pRouterPortBmp,
                                       INT4 i4AddOrDel)
{
    tSnoopVlanEntry    *pSnoopVlanEntry = NULL;
    INT4                i4RetVal = 0;
    INT4                i4CurSpaMode = SNOOP_DISABLE;
    INT4                i4SnoopAdminStatus = SNOOP_DISABLE;
    tPortList           RouterPortBitmap;
    tPortList           NewRouterPortBitmap;
    tSnoopTag           VlanId;
    BOOL1               bResult = OSIX_TRUE;
    BOOL1               bResult1 = OSIX_TRUE;
    UINT4               u4Port = 0;
    tSnoopPortBmp       PortBitmap;

    SNOOP_MEM_SET (&RouterPortBitmap, 0, sizeof (tPortList));
    SNOOP_MEM_SET (&NewRouterPortBitmap, 0, sizeof (tPortList));
    SNOOP_MEM_SET (VlanId, 0, sizeof (tSnoopTag));
    SNOOP_MEM_SET (PortBitmap, 0, sizeof (tSnoopPortBmp));

    SNOOP_MEM_CPY (PortBitmap, pRouterPortBmp, SNOOP_PORT_LIST_SIZE);

    SNOOP_OUTER_VLAN (VlanId) = (tVlanId) i4VlanId;

    i4SnoopAdminStatus =
        SNOOP_PROTOCOL_ADMIN_STATUS (u4Instance, SNOOP_ADDR_TYPE_IPV4 - 1);

    i4CurSpaMode = SNOOP_SYSTEM_SPARSE_MODE (u4Instance);

    if (SnoopVlanGetVlanEntry (u4Instance, VlanId,
                               SNOOP_ADDR_TYPE_IPV4,
                               &pSnoopVlanEntry) != SNOOP_SUCCESS)

    {
        return SNMP_FAILURE;
    }

    if ((i4SnoopAdminStatus == SNOOP_ENABLE) && (i4CurSpaMode == SNOOP_ENABLE))
    {
        SNOOP_MEM_CPY (RouterPortBitmap, pSnoopVlanEntry->RtrPortBitmap,
                       sizeof (tSnoopPortBmp));

        if (i4AddOrDel == SNOOP_ADD_PORT)
        {
            SNOOP_MEM_CPY (NewRouterPortBitmap, pRouterPortBmp,
                           sizeof (tSnoopPortBmp));

            SNOOP_UPDATE_PORT_LIST (NewRouterPortBitmap, RouterPortBitmap);

            i4RetVal = SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                      (pSnoopVlanEntry->VlanId),
                                                      &RouterPortBitmap);
        }
        else if (u4InPort != 0)
        {
            SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap,
                                   bResult);
            if (bResult == OSIX_TRUE)
            {
                SNOOP_DEL_FROM_PORT_LIST (u4InPort, RouterPortBitmap);
            }

            i4RetVal = SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                      (pSnoopVlanEntry->VlanId),
                                                      &RouterPortBitmap);

        }
        else
        {
            for (u4Port = 1; u4Port <= SNOOP_MAX_PORTS_PER_INSTANCE; u4Port++)
            {
                SNOOP_IS_PORT_PRESENT (u4Port, PortBitmap, bResult);
                SNOOP_IS_PORT_PRESENT (u4Port, pSnoopVlanEntry->RtrPortBitmap,
                                       bResult1);

                if ((bResult == OSIX_FALSE) && (bResult1 == OSIX_TRUE))
                {
                    SNOOP_DEL_FROM_PORT_LIST (u4Port, RouterPortBitmap);
                }
                else
                {
                    continue;
                }
            }

            i4RetVal = SnoopVlanSetForwardUnregPorts (SNOOP_OUTER_VLAN
                                                      (pSnoopVlanEntry->VlanId),
                                                      &RouterPortBitmap);

        }
    }

    UNUSED_PARAM (i4RetVal);
    return SNOOP_SUCCESS;
}
