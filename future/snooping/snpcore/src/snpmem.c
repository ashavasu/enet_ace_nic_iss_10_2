/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: snpmem.c,v 1.32 2017/11/17 12:09:07 siva Exp $
 *
 * Description: This file contains creation and deletion functions for 
 *              snooping(MLD/IGMP) module.
 *****************************************************************************/
/*****************************************************************************/
/*    FILE  NAME            : snpmem.c                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping module mempool creation/deletion      */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains creation and deletion       */
/*                            functions for snooping module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    25 Nov 2005           Initial Creation                        */
/*---------------------------------------------------------------------------*/
#ifndef _SNOOPMEM_C
#define _SNOOPMEM_C

#include "snpinc.h"

#ifdef SNMP_2_WANTED
#include "fssnpwr.h"
#endif

/*****************************************************************************/
/* Function Name      : SnoopMemGlobalMemPoolDelete                          */
/*                                                                           */
/* Description        : Deletes SNOOP Global Structure Memory Pool           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMemGlobalMemPoolDelete (VOID)
{
    if (SNOOP_MISC_MEMPOOL_ID != 0)
    {
        SNOOP_DELETE_MEM_POOL (SNOOP_MISC_MEMPOOL_ID);
        SNOOP_MISC_MEMPOOL_ID = 0;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMemCreateInstanceInfo                           */
/*                                                                           */
/* Description        : This routine creates RBTree table and allocates      */
/*                      buffer for Group and VLAN information.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopMemCreateInstanceInfo (UINT4 u4Instance)
{
    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopMemCreateInstanceInfo\r\n");
    /* Local RBTree creation for Port Configuration  */
    SNOOP_PORT_CFG_INST_RBTREE (u4Instance) =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopPortCfgEntry,
                                             InstanceRbNode),
                              SnoopUtilRBTreePortCfgEntryCmp);
    if (SNOOP_PORT_CFG_INST_RBTREE (u4Instance) == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "Instance RB Tree creation for PortConfig Entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMemCreateInstanceInfo\r\n");
        return SNOOP_FAILURE;
    }

    /* RBTree creation for Consolidated Group Entries */
    SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopConsolidatedGroupEntry,
                                             RbNode),
                              SnoopUtilRBTreeConsGroupEntryCmp);
    if (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RB Tree creation for Consolidated Group Entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMemCreateInstanceInfo\r\n");
        return SNOOP_FAILURE;
    }

    /* RBTree creation for Group Entries */
    SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopGroupEntry, RbNode),
                              SnoopUtilRBTreeGroupEntryCmp);
    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RB Tree creation for Group Entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMemCreateInstanceInfo\r\n");
        return SNOOP_FAILURE;
    }

    /* RBTree creation for VLAN Entries */
    SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopVlanEntry, RbNode),
                              SnoopUtilRBTreeVlanEntryCmp);
    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RB Tree creation for VLAN Entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMemCreateInstanceInfo\r\n");
        return SNOOP_FAILURE;
    }

    /* RBTree creation for Host Entries */
    SNOOP_INSTANCE_INFO (u4Instance)->HostEntry =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopHostEntry, RbNode),
                              SnoopUtilRBTreeHostEntryCmp);

    if (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry == NULL)
    {
        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RB Tree creation for Host Entry failed\r\n");
        SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                   "Exit : SnoopMemCreateInstanceInfo\r\n");
        return SNOOP_FAILURE;
    }
    /* RBTree creation for MAC multicast forward entries */
    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry =
            RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopMacGrpFwdEntry, RbNode),
                                  SnoopUtilRBTreeMacFwdEntryCmp);

        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry == NULL)
        {

            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "RB Tree creation for MAC Mcast Forward Entry "
                           "failed\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopMemCreateInstanceInfo\r\n");
            return SNOOP_FAILURE;
        }
    }
    else
    {
        /* RBTree creation for IP multicast forward entries */
        SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry =
            RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopIpGrpFwdEntry, RbNode),
                                  SnoopUtilRBTreeIpFwdEntryCmp);

        if (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry == NULL)
        {
            SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (u4Instance),
                           SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                           SNOOP_OS_RES_NAME,
                           "RB Tree creation for IP Mcast Forward Entry "
                           "failed\r\n");
            SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
                       "Exit : SnoopMemCreateInstanceInfo\r\n");
            return SNOOP_FAILURE;
        }
    }

    SNOOP_DLL_INIT (&(SNOOP_SOURCE_LIST (u4Instance)));

    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopMemCreateInstanceInfo\r\n");
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMemDeleteInstanceInfo                           */
/*                                                                           */
/* Description        : This routine deletes the RBTree table and the buffer */
/*                      info that are created for Group and VLAN Entries.    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMemDeleteInstanceInfo (UINT4 u4Instance)
{
    tMVlanMapping      *pMVlanMapping = NULL;
    tMVlanMapping      *pTempMVlanMapping = NULL;
    UINT2               u2Index = 0;
    INT4                i4RetVal = 0;

    /* This is done for trace messages */
    gu4SnoopTrcInstId = u4Instance;
    gu4SnoopDbgInstId = u4Instance;
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_ENTRY_DBG, SNOOP_ENTRY_DBG,
               "Entry : SnoopMemDeleteInstanceInfo\r\n");
    /* Local RBTree creation for Port Configuration  */
    /* Delete the port config entries from this instance and from the
     * global RB Tree */
    SnoopPortCfgDeleteEntries (u4Instance, SNOOP_ADDR_TYPE_IPV4);

    /* RBTree deletion for Port Configuration Entries */
    if (SNOOP_PORT_CFG_INST_RBTREE (u4Instance) != NULL)
    {
        RBTreeDestroy (SNOOP_PORT_CFG_INST_RBTREE (u4Instance), NULL, 0);
        SNOOP_PORT_CFG_INST_RBTREE (u4Instance) = NULL;
    }

    SNOOP_INSTANCE_INFO (u4Instance)->PortCfgEntry = NULL;

    if (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_MAC)
    {
        /* RBTree deletion for MAC multicast forward entries */
        if (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry != NULL)
        {
            RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry,
                           SnoopUtilRBTreeMacFwdEntryFree, 0);
            SNOOP_INSTANCE_INFO (u4Instance)->MacMcastFwdEntry = NULL;
        }
    }
    else
    {

        /* Reset the global list of sources */
        for (u2Index = 0; u2Index < SNOOP_MAX_MCAST_SRCS; u2Index++)
        {
            SNOOP_MEM_SET (&(SNOOP_SRC_INFO (u4Instance, u2Index).SrcIpAddr), 0,
                           sizeof (tIPvXAddr));
            SNOOP_SRC_INFO (u4Instance, u2Index).u2GrpRefCount = 0;
            SNOOP_SRC_INFO (u4Instance, u2Index).u2NodeIndex = 0;
            SNOOP_DLL_INIT_NODE
                (&(SNOOP_SRC_INFO (u4Instance, u2Index).SourceNode));
        }
        gu2SourceCount = 0;
        SNOOP_DLL_INIT (&(SNOOP_SOURCE_LIST (u4Instance)));

        /* RBTree deletion for IP multicast forward entries */
        if (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry != NULL)
        {
            RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry,
                           SnoopUtilRBTreeIpFwdEntryFree, 0);
            SNOOP_INSTANCE_INFO (u4Instance)->IpMcastFwdEntry = NULL;
        }
    }

    /* RBTree deletion for Host Entries */
    if (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry != NULL)
    {
        RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->HostEntry, NULL, 0);
        SNOOP_INSTANCE_INFO (u4Instance)->HostEntry = NULL;
    }

    /* RBTree deletion for Group Entries */
    if (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry != NULL)
    {
        RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry,
                       SnoopUtilRBTreeGroupEntryFree, 0);
        SNOOP_INSTANCE_INFO (u4Instance)->GroupEntry = NULL;
    }

    /* RBTree deletion for Group Entries */
    if (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry != NULL)
    {
        RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry,
                       SnoopUtilRBTreeConsGroupEntryFree, 0);
        SNOOP_INSTANCE_INFO (u4Instance)->ConsGroupEntry = NULL;
    }

    /* Deleting of Multicast VLAN to Profile mapping */
    SNOOP_DYN_SLL_SCAN (&(SNOOP_INSTANCE_INFO (u4Instance)->MVlanMappingList),
                        pMVlanMapping, pTempMVlanMapping, tMVlanMapping *)
    {
        i4RetVal =
            SnoopTacApiUpdateVlanRefCount (u4Instance,
                                           pMVlanMapping->u4ProfileId,
                                           SNOOP_ADDR_TYPE_IPV4, TACM_UNMAP);
        UNUSED_PARAM (i4RetVal);

        SNOOP_SLL_DELETE (&(SNOOP_INSTANCE_INFO (u4Instance)->MVlanMappingList),
                          pMVlanMapping);
        SNOOP_MVLAN_MAPPING_ENTRY_FREE_MEMBLK (u4Instance, pMVlanMapping);
    }

    /* RBTree deletion for VLAN Entries */
    if (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry != NULL)
    {
        RBTreeDestroy (SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry,
                       SnoopUtilRBTreeVlanEntryFree, 0);
        SNOOP_INSTANCE_INFO (u4Instance)->VlanEntry = NULL;
    }

    SnoopMainSetDefaultInstInfo (u4Instance, SNOOP_MCAST_FWD_MODE_MAC);
    SNOOP_DBG (SNOOP_DBG_FLAG, SNOOP_EXIT_DBG, SNOOP_EXIT_DBG,
               "Exit : SnoopMemDeleteInstanceInfo\r\n");
    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMemCreateGlbRBTreePortConfEntry                 */
/*                                                                           */
/* Description        : This routine creates global RBTree table and         */
/*                      allocates buffer for Port configuration information. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : SNOOP_SUCCESS/SNOOP_FAILURE                          */
/*****************************************************************************/
INT4
SnoopMemCreateGlbRBTreePortConfEntry ()
{

    /* RBTree creation for Port Conf  Entries */
    SNOOP_PORT_CFG_GLB_RBTREE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tSnoopPortCfgEntry, GlobalRbNode),
                              SnoopUtilRBTreePortCfgEntryCmp);
    if (SNOOP_PORT_CFG_GLB_RBTREE == NULL)
    {

        SNOOP_GBL_DBG (SNOOP_DBG_FLAG_INST (SNOOP_DEFAULT_INSTANCE),
                       SNOOP_DBG_RESRC | SNOOP_DBG_ALL_FAILURE,
                       SNOOP_OS_RES_NAME,
                       "RB Tree creation for Port Conf Entry is failed\r\n");
        return SNOOP_FAILURE;
    }
    return SNOOP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SnoopMemDeleteGlbRBTreePortConfEntry                 */
/*                                                                           */
/* Description        : This routine deletes global RBTree for Port          */
/*                      configuration information.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
SnoopMemDeleteGlbRBTreePortConfEntry ()
{

    /*  Global RBTree deletion of port cofiguration entries */
    if (SNOOP_PORT_CFG_GLB_RBTREE != NULL)
    {
        RBTreeDestroy (SNOOP_PORT_CFG_GLB_RBTREE, NULL, 0);
        SNOOP_PORT_CFG_GLB_RBTREE = NULL;
    }

    return;
}

/*****************************************************************************/
/* Function Name      : SnoopMemAssignMempoolIds                             */
/*                                                                           */
/* Description        : This function is to assign all mempool Ids in        */
/*                      snooping module.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
SnoopMemAssignMempoolIds (UINT4 u4Instance)
{
    UNUSED_PARAM (u4Instance);

    /*tSnoopMsgNode */
    SNOOP_QMSG_MEMPOOL_ID = SNPMemPoolIds[MAX_SNOOP_Q_MSG_SIZING_ID];

    /*tSnoopQMsg */
    SNOOP_QPKT_MEMPOOL_ID = SNPMemPoolIds[MAX_SNOOP_QUE_PKT_MSG_SIZING_ID];

    /*tSnoopGlobalInstInfo */
    SNOOP_GLOBAL_MEMPOOL_ID =
        SNPMemPoolIds[MAX_SNOOP_GLOBAL_INSTANCES_SIZING_ID];

    SNOOP_MISC_MEMPOOL_ID = SNPMemPoolIds[MAX_SNOOP_MISC_BLK_COUNT_SIZING_ID];

    SNOOP_DATA_MEMPOOL_ID = SNPMemPoolIds[MAX_SNOOP_DATA_BLOCK_COUNT_SIZING_ID];

    /*tSnoopPortCfgEntry */
    SNOOP_PORT_CFG_ENTRY_POOL_ID =
        SNPMemPoolIds[MAX_SNOOP_PORT_CFG_ENTRIES_SIZING_ID];

    /*tSnoopExtPortCfgEntry */
    SNOOP_PORT_EXT_CFG_ENTRY_POOL_ID =
        SNPMemPoolIds[MAX_SNOOP_EXT_PORT_CFG_ENTRIES_SIZING_ID];

    /*tSnoopConsolidatedGroupEntry */
    SNOOP_VLAN_CONS_GRP_ENTRY_POOL_ID =
        SNPMemPoolIds[MAX_SNOOP_CONSO_GROUP_ENTRIES_SIZING_ID];

    /*tSnoopGroupEntry */
    SNOOP_VLAN_GRP_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_GROUP_ENTRIES_SIZING_ID];

    /*tSnoopPortEntry */
    SNOOP_PORT_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_PORT_ENTRIES_SIZING_ID];

    /*tSnoopVlanEntry */
    SNOOP_VLAN_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_VLAN_ENTRIES_SIZING_ID];

    /*tSnoopVlanStatsEntry */
    SNOOP_VLAN_STATS_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_VLAN_STATS_ENTRIES_SIZING_ID];

    /*tSnoopVlanCfgEntry */
    SNOOP_VLAN_CFG_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_VLAN_CONFIG_ENTRIES_SIZING_ID];

    /*SnoopRtrPortEntry */
    SNOOP_RTR_PORT_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_ROUTER_PORT_ENTRIES_SIZING_ID];

    /*tSnoopMacGrpFwdEntry */
    SNOOP_MAC_FWD_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_MAC_FWD_ENTRIES_SIZING_ID];

    /*tSnoopSrcBmpNode */
    SNOOP_SRC_BMP_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_SRC_BMP_NODES_SIZING_ID];

    /*tSnoopHostEntry */
    SNOOP_HOST_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_HOST_ENTRIES_SIZING_ID];

    /*tSnoopIpGrpFwdEntry */
    SNOOP_IP_FWD_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_IP_FWD_ENTRIES_SIZING_ID];

    /*tMVlanMapping */
    SNOOP_MVLAN_MAPPING_POOLID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_MCAST_VLAN_MAPPING_SIZING_ID];

    /*tGrpSrcStatusEntry */
    SNOOP_GRPSRC_RECORDS_POOLID =
        SNPMemPoolIds[MAX_SNOOP_MVLAN_GRP_SRC_STATUS_ENTRIES_SIZING_ID];

    SNOOP_SRC_MEMPOOL_ID = SNPMemPoolIds[MAX_SNOOP_SRC_BLOCK_COUNT_SIZING_ID];

    /*tSnoopPVlanListSize */
    SNOOP_PVLAN_LIST_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_PVLAN_LIST_COUNT_SIZING_ID];

    /*tIPvXAddrSource */
    SNOOP_IPVX_ADDR_POOL_ID =
        SNPMemPoolIds[MAX_SNOOP_MCAST_SRCS_ENTRIES_SIZING_ID];

    /*tIgsHwIpFwdInfo */
    SNOOP_HW_IP_FW_INFO_POOL_ID =
        SNPMemPoolIds[MAX_SNOOP_HW_IP_FW_INFO_COUNT_SIZING_ID];
#ifdef L2RED_WANTED
    SNOOP_RED_HW_ENTRY_POOL_ID (u4Instance) =
        SNPMemPoolIds[MAX_SNOOP_RED_HW_ENTRY_SIZING_ID];
#endif
}
#endif /* _SNOOPMEM_C */
