/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: snptdfs.h,v 1.53 2017/09/12 13:27:28 siva Exp $
 *
 * Description: This file contains Data Structures for 
 *              snooping(MLD/IGMP) module.
 *****************************************************************************/
#ifndef _SNPTDFS_H
#define _SNPTDFS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : snptdfs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Data Structures            */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Data Structures             */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
/* Snoop source list bitmap */
typedef UINT1 tSnoopSrcBmp[SNOOP_SRC_LIST_SIZE + 1];

typedef tIPvXAddr tIPvXAddrSource;

typedef struct SnoopPortInstanceMap {
    UINT4   u4Instance;  /* instance for whcih this local port mapped to*/
    UINT4   u4PhyPort;   /* If this port is SISP enabled, this will have 
                            the corresponding physical port's index*/
    UINT2   u2LocalPort; /* local port number corresponding to physical port
                            for a given instance */
    UINT2   u2Reserved;  /* reseved for packing */
}tSnoopPortInstanceMap;

/* Timer Node */
typedef struct SnoopTmrNode {
   tTmrAppTimer  AppTmr;
   UINT4         u4Instance;       /* Indicates the instance number    */ 
   tSnoopTag     VlanId;           /* VLAN Identifier             */
   UINT1         u1TimerType;      /* Indicates the type of the timer 
                                      that is started                  */
   UINT1         au1Reserved[3];   /* for packing   */ 
} tSnoopTmrNode;

typedef struct SnoopSrcBmpNode {
    tSnoopSrcBmp         InclSrcBmp;         /* Indicates the set of sources
                                                for which include registrations
                                                have been reported */
    tSnoopSrcBmp         ExclSrcBmp;         /* Indicates the set of sources
                                                for which exclude registrations
                                                have been reported */
    UINT1         au1Reserved[2];   /* for packing   */ 
} tSnoopSrcBmpNode;


/******************************************************************************/
/*              MAC and IP based forwarding Information                       */
/******************************************************************************/
enum {
    HW_ADD_GROUP_SUCCESS = 0,
    HW_ADD_GROUP_FAIL
};

enum {
    HW_ADD_PORT_SUCCESS = 0,
    HW_ADD_PORT_FAIL
};

/* MAC based forwarding Information  */
typedef struct MacGrpFwdEntry {
   tRBNodeEmbd            RbNode;
   tSnoopSll              GroupEntryList;      /* List if pointers to the 
                                                  Group Entry node            */
   tSnoopPortBmp          PortBitmap;          /* Bitmap of ports that are
                                                  members of this Mcast MAC
                                                  group                       */
   tSnoopTag              VlanId;              /* VLAN Identifier             */
   tSnoopMacAddr          MacGroupAddr;        /* Multicast MAC Group address */
   UINT1                  u1AddressType;       /* Indicates IPv4/IPV6         */
   UINT1                  u1EntryTypeFlag;     /* Indicates EntryType         */
} tSnoopMacGrpFwdEntry;

/* IP based forwarding Information  */
typedef struct IpGrpFwdEntry {
   tRBNodeEmbd          RbNode;
   tSnoopPortBmp        PortBitmap;          /* Bitmap of ports that are
                                                members of this Mcast MAC
                                                group                    */
   tSnoopTmrNode        EntryTimer;          /* Entry timer */ 
   tIPvXAddr            SrcIpAddr;           /* Source IPv4/IPv6 address */ 
   tIPvXAddr            GrpIpAddr;           /* Group IPv4/IPv6 address  */
   tSnoopTag            VlanId;              /* VLAN Identifier          */
   UINT4                u4SnoopHwId;
   UINT1                u1AddGroupStatus;
   /* This flag indicates the HW programming status while adding ports to a group.
    * It can either be HW_ADD_PORT_SUCCESS of HW_ADD_PORT_FAIL 
    */
   UINT1                u1AddPortStatus;
   UINT1                u1AddressType;       /* Indicates IPv4/IPV6      */
   UINT1                u1EntryTypeFlag;     /* Indicates EntryType         */
} tSnoopIpGrpFwdEntry;



/******************************************************************************/
/*       MAC and IP based Group Membership Information                        */
/******************************************************************************/
typedef struct SourceInfo {
   tSnoopDllNode  SourceNode;   /* Pointer to the next source node */
   tIPvXAddr SrcIpAddr;     /* Source Ipv4/IPv6 adddress          */
   UINT2 u2GrpRefCount;     /* number of groups using this source */
   UINT2          u2NodeIndex;  /* The index of the source in global array */
   UINT1          u1EntryTypeFlag;     /* Indicates EntryType         */
   UINT1          au1Reserved[3];   /* for packing   */
}tSnoopSourceInfo;

/* In Proxy-reporting, the consolidation should happen across Inner-VLANs and 
 * with in Outer-VLAN. For consolidation using tSnoopGroupEntry will lead to  
 * consolidation of across Inner-VLANs and Outer-VLANs. So the below 
 * new structure will be used for consolidation. */
typedef struct ConsolidatedGroupEntry{
    tRBNodeEmbd             RbNode;
    tSnoopDll               GroupEntryList;   /* List of group records    
                                                 for the Consolidated entry's 
                                                 Outer-VLAN                  */
    tSnoopSrcBmp            InclSrcBmp;       /* Indicates the set of sources
                                                 for which include registrations
                                                 have been reported for this
                                                 group. This will be used in
                                                 IP based forwarding        */
    tSnoopSrcBmp            ExclSrcBmp;       /* Indicates the set of sources
                                                 for which exclude registrations
                                                 have been reported for this
                                                 group. This will be used in
                                                 IP based forwarding        */
    tIPvXAddr               GroupIpAddr;      /* Group IP Address            */
    tSnoopTag               VlanId;           /* Outer-VLAN alone will be 
                                                 filled                      */
    UINT1                   u1FilterMode;     /* Indicates the Filter mode
                                                 (Include / Exclude). This
                                                 will be used in IP based
                                                 forwarding                 */
    UINT1                   u1ASMHostPresent; /* Indicates whether any ASM 
                                                 host is present in any of 
                                                 the group entry in this 
                                                 consolidated group entry   */
}tSnoopConsolidatedGroupEntry;

typedef struct GroupEntry{
   tSnoopSllNode           *pNextGrpNode;   
   tSnoopDllNode           NextGrpNode;        /* For Consolidated group entry*/
   tRBNodeEmbd             RbNode;
   tSnoopSll               SSMPortList;        /* List of Port records which are
                                                  learned from specific source
                                                  multicast reports.
                                                  This is used in IP based 
                                                  forwarding mode            */ 
   tSnoopSll               ASMPortList;        /* List of Port records which 
                                                  are learned from Any source 
                                                  multicast reports          */
   tSnoopTmrNode           ReportFwdTimer;     /* Indicates the timer used for 
                                                  suppressing the any source
                                                  multicast reports for 
                                                  the same group from getting 
                                                  forwarded on to the 
                                                  router ports               */
   tSnoopTmrNode           InGrpSrcTimer;      /* Indicates the timer used for 
                                                  clearing-off the source
                                                  information learnt for unknown 
                                                  multicast packet(to drop the data)*/
   tSnoopMacGrpFwdEntry    *pMacGrpFwdEntry;   /* Pointer to the MAC based 
                                                  forwarding entry           */
   tSnoopConsolidatedGroupEntry *pConsGroupEntry; /* Pointer to the 
                                                    consolidated group entry */
   tSnoopPortBmp           PortBitmap;         /* Indicates the consolidated 
                                                  port bitmap for the Group IP 
                                                  record.                    */ 
   tSnoopPortBmp           ASMPortBitmap;       /* Indicates the IGMPv2/MLDv1 
         receivers port bitmap for 
         the Group IP record.      */ 
#ifdef L2RED_WANTED
   tSnoopPortBmp           V1PortBitmap;       /* Indicates the list of ports
                                                  on which v1 report is
                                                  received.                 */
#endif
   tSnoopSrcBmp            InclSrcBmp;        /* Indicates the set of sources 
                                                 for which include registrations
                                                 have been reported for this 
                                                 group. This will be used in 
                                                 IP based forwarding        */
   tSnoopSrcBmp            ExclSrcBmp;       /* Indicates the set of sources 
                                                 for which exclude registrations
                                                 have been reported for this 
                                                 group. This will be used in 
                                                 IP based forwarding        */
   tIPvXAddr               GroupIpAddr;         /* Group IP Address         */ 
   tSnoopTag               VlanId;              /* VLAN Identifier          */
   UINT1                   u1FilterMode;        /* Indicates the Filter mode
                                                   (Include / Exclude). This 
                                                   will be used in IP based 
                                                   forwarding               */
   UINT1                   u1ReportFwdFlag;     /* Indicates whether the report 
                                                   forward timer is running or 
                                                   not                      */
   BOOL1                   b1InGroupSourceInfo;  /* Indicates the source information
                                                   for the corresponding group*/
   UINT1                   u1FirstQueryRes;       /* First Query Response */
   UINT1                   u1EntryTypeFlag;        /* Indicates EntryType         */
   UINT1                   u1RowStatus;
/* Used to classify unknown multicast traffic. This is used for applying sparse mode functionality*/
   UINT1                   u1IsUnknownMulticast;
#ifdef ICCH_WANTED
   UINT1                   u1McLagLeaveReceived;
   UINT1                   au1Reserved[2];
#else
   UINT1                   au1Reserved[3];
#endif
}tSnoopGroupEntry;


/* Port Record Structure for each Group IP entry. This structure is used
 * to contain both the information of both the ASM ports and SSM ports */

typedef struct SnoopPortEntry {
   tSnoopSllNode        NextPortEntry;
   tSnoopSll            HostList;            /* List of Host records      */ 
   tSnoopGroupEntry    *pGroupEntry;         /* Back pointer to the group 
                                                entry                     */
   tSnoopTmrNode        HostPresentTimer;    /* Timer for aging out hosts */
   tSnoopTmrNode        PurgeOrGrpQueryTimer; /* Port Purge or Group query
                                                 Timer for the ASM Ports */
   tSnoopSrcBmpNode    *pSourceBmp;          /* SnoopSrcBmpNode to contain the
                                                set of Include and Exclude 
                                                reported on this port */
   tSnoopSrcBmpNode    *pStaticSourceBmp;    /* SnoopSrcBmpNode to contain
         static include sources */    
   UINT4                u4Port;              /* Port Number.            */ 
   UINT1                u1RetryCount;        /* Retry Count for sending Grp 
                                                Specific queries for ASM ports */
   UINT1                u1V1HostPresent;     /* Flag to indicate the presence of 
                                                v1 host in the ASM Port  */ 
   UINT2                u2Reserved;
} tSnoopPortEntry;

 /* Host Record structure for each ASM/SSM port record for a Group. */

typedef struct HostEntry {
   tSnoopSllNode        NextHostEntry;
   tRBNodeEmbd          RbNode;              /* RBTree node for the host entry */
   tSnoopPortEntry     *pPortEntry;          /* Back pointer to the Ssm port
                                                entry */
   tIPvXAddr            HostIpAddr;          /* Indicates the host IP address */
   tSnoopHostType       SnoopHostType;       /* Indicate the version of the
                                                host */
   tSnoopSrcBmpNode    *pSourceBmp;          /* SnoopSrcBmpNode to contain the
                                                set of Include and Exclude
                                                registrations that have been
                                                reported by this host */
   /* The HOST PRESENT TIMER is split and run as two halves.
    * This variable indicates if it runs in the first half or in the second half.
    * The supported values are SNOOP_FIRST_HALF and SNOOP_SECOND_HALF
    */
   UINT1                u1HostTimerStatus;   /* Timer Status */ 
   UINT1  u1HostFilterMode;
   UINT1                au1Reserved[2];
} tSnoopHostEntry;

typedef struct SnoopVlanCfgEntry {
   tSnoopPortBmp        CfgStaticRtrPortBmp; /* Statically configured router 
                                                port bitmap for this VLAN */
   tSnoopPortBmp        CfgBlockedRtrPortBmp;/* Statically configured blocked 
                                                router port bitmap for this    
                                                VLAN                      */
   UINT4                u4ConfigProfileId;    /* Multicast Profile-Id, for
                                                 M-VLAN associations */
   UINT2                u2ConfigStartUpInterval; /*Configured startup query interval*/
   UINT2                u2ConfiguredOtherQPreInterval; /*Configured other query present interval*/
   UINT2                u2ConfigQueryInt;    /* Configured interval when the 
                                                row is not active         */  
   UINT1                u1ConfigOperVer;     /* Configured Operating 
                                                Version is (V1/V2/V3) for IGS
                                                and V1/V2 for MLDS        */
   UINT1                u1ConfigQuerier;     /* Configured Querier flag   */ 
   UINT1                u1ConfigSnoopStatus; /* Configured IGS/MLDS Status*/ 
   UINT1                u1ConfigFastLeave;   /* Cofigured Fast leave      */ 
   UINT2                u2Reserved;          /* Reserved                  */
} tSnoopVlanCfgEntry;
        
typedef struct SnoopVlanStatsEntry {
   UINT4                u4GenQueryRx;        /* Recvd General queries      */
   UINT4                u4GrpQueryRx;        /* Recvd Grp specific queries */
   UINT4                u4GrpAndSrcQueryRx;  /* Recvd Grp and Src specific 
                                                queries                    */
   UINT4                u4ASMReportRx;          /* Recvd ASM reports          */ 
   UINT4                u4SSMReportRx;       /* Recvd SSM reports          */   
   UINT4                u4v1ReportRx;        /* Recvd v1 report */          
   UINT4                u4v2ReportRx;        /* Recvd v2 report */
   UINT4                u4SSMIsInMsgRx;      /* Recvd IS_IN messages       */
   UINT4                u4SSMIsExMsgRx;      /* Recvd IS_EX messages       */
   UINT4                u4SSMToInMsgRx;      /* Recvd TO_EX messages       */
   UINT4                u4SSMToExMsgRx;      /* Recvd TO_IN messages       */
   UINT4                u4SSMAllowMsgRx;     /* Recvd ALLOW messages       */
   UINT4                u4SSMBlockMsgRx;     /* Recvd BLOCK messages       */
   UINT4                u4LeaveRx;           /* Recvd leaves               */
   UINT4                u4GenQueryTx;        /* Tx General queries         */
   UINT4                u4GrpQueryTx;        /* Tx Grp Specific queries    */
   UINT4                u4GrpAndSrcQueryTx;  /* Tx Grp and Src specific 
                                                queries                    */
   UINT4                u4ASMReportsTx;         /* Tx ASM reports             */ 
   UINT4                u4SSMReportsTx;      /* Tx SSM reports             */
   UINT4                u4v1ReportsTx;       /* Tx v1 reports              */
   UINT4                u4v2ReportsTx;       /* Tx v2 reports              */
   UINT4                u4LeaveTx;           /* Tx Leaves                  */


   UINT4                u4UnSuccessfulJoins; /* Unsuccessful joins         */
   UINT4                u4ActiveJoins;       /* Active joins               */
   UINT4                u4ActiveGrps;        /* Active Groups              */
   UINT4                u4PktsDropped;       /* Pkts dropped               */


   UINT4                u4v1GenQueryDrop;        /* Dropped General queries         */
   UINT4                u4v2GenQueryDrop;        /* Dropped General queries         */
   UINT4                u4v3GenQueryDrop;        /* Dropped General queries         */
   UINT4                u4v2GrpQueryDrop;        /* Dropped Grp Specific queries    */
   UINT4                u4v3GrpQueryDrop;        /* Dropped Grp Specific queries    */
   UINT4                u4GrpAndSrcQueryDrop;  /* Dropped Grp and Src specific */
                           
   UINT4                u4v1ReportsDrop;       /* Dropped v1 reports           */
   UINT4                u4v2ReportsDrop;       /* Dropped v2 reports           */
   UINT4                u4SSMIsInMsgDrop;      /* Dropped IS_INCLUDE messages       */
   UINT4                u4SSMIsExMsgDrop;      /* Dropped IS_EXCLUDE messages       */
   UINT4                u4SSMToInMsgDrop;      /* Dropped TO_EXCLUDE messages       */
   UINT4                u4SSMToExMsgDrop;      /* Dropped TO_INCLUDE messages       */
   UINT4                u4SSMAllowMsgDrop;     /* Dropped ALLOW messages       */
   UINT4                u4SSMBlockMsgDrop;     /* Dropped BLOCK messages       */
   UINT4                u4LeaveDrop;           /* Dropped Leaves               */
   UINT4                u4FwdGenQueries;        /* Fwd General Specific queries */
   UINT4                u4FwdGSQs;              /* Fwd Grp Specific queries    */  

}tSnoopVlanStatsEntry;

/* SNOOP Vlan Data Structure */

typedef struct SnoopVlanEntry {
   tRBNodeEmbd            RbNode;
   tSnoopVlanStatsEntry   *pSnoopVlanStatsEntry; /* Pointer to VLAN Stats 
                                                    Entry                  */ 
   tSnoopVlanCfgEntry     *pSnoopVlanCfgEntry;   /* Pointer to VLAN Config 
                                                    Entry                  */
   tSnoopSll              RtrPortList;        /* List of Dynamically learnt & 
                                                 Static router port records */   
   tSnoopTmrNode          QueryTimer;         /* Timer Node for sending general 
                                                 queries when the switch is 
                                                 configured as a querier for 
                                                 this VLAN                 */
   tSnoopTmrNode          OtherQPresentTimer; /* Timer Node for resuming 
                                                 the role of querier */
   tSnoopPortBmp          RtrPortBitmap;      /* Consolidated Router port 
                                                 bitmap for this VLAN      */
   tSnoopPortBmp          V1RtrPortBitmap;    /* Consolidated Router ports
                                                 bitmap with operating version as V1 
                                                 for this VLAN      */
   tSnoopPortBmp          V2RtrPortBitmap;    /* Consolidated Router port 
                                                 bitmap with operating version as V2 
                                                 for this VLAN      */
   tSnoopPortBmp          V3RtrPortBitmap;      /* Consolidated Router port 
                                                 bitmap with operating version as V3 
                                                 for this VLAN      */
   /* The multicast router port bitlist learnt from general query received.
    * This bit specific to the port will be cleared after router port purge interval, 
    * if queries are not received on the port till the timer expiry.
    */
    
   tSnoopPortBmp          QuerierRtrPortBitmap;  /* Querier port bitmap for 
                                                 this VLAN which is learnt 
                                                 by MLD/IGMP query messages*/
   tSnoopPortBmp          StaticRtrPortBitmap;/* Statically configured router 
                                                 port bitmap for this VLAN */
   tSnoopPortBmp          DynRtrPortBitmap;   /* Dynamically learned router
                                                 port bitmap for this VLAN */
   UINT4                  u4ProfileId;        /* Multicast Profile-Id, for
                                                 M-VLAN associations */
   UINT4                 u4QuerierIp;       /*Ip Address configured by the user*/
   UINT4                 u4InterfaceIp;     /*Interface vlan IP. This will be updated
                                              when snooping module receives ip change
                                              notification*/
   UINT4                 u4ElectedQuerier;    /*IP Address of elected querier*/
   UINT4                  u4RobustnessValue;
   tSnoopTag              VlanId;             /* VLAN Identifier           */
   UINT2                  u2QueryInterval;    /* The interval over which the 
                                                 switch sends general queries
                                                 when it is configured as a 
                                                 querier for this VLAN     */
   UINT2                  u2OtherQPresentInt;    /* The interval over which the 
                                                 switch sends general queries
                                                 when it is configured as a 
                                                 querier for this VLAN     */
   UINT2                  u2MaxRespCode;      /* The max time within which hosts
                                                 hosts are expected to reply
                                                 for genereal queries.This holds 
                                                 the MRC value */
   UINT2                  u2PortPurgeInt;     /* Port Purge Interval       */
   UINT2                  u2StartUpQInterval; /* The interval over which the 
                                                 switch sends the startup general 
                                                 queries for this VLAN*/ 
   UINT1                  u1AddressType;      /* Indicates IPv4/IPv6       */
   UINT1                  u1SnoopStatus;      /* Enable or disable IGS/MLDS 
                                                 in this VLAN              */ 
   UINT1                  u1Querier;          /* Indicates whether the 
                                                 snooping switch is 
                                                 configured as a querier   */ 
   UINT1                  u1StartUpQCount;    /* Indicates the startup query 
                                                 count */
   UINT1                  u1MaxStartUpQCount; /* Indicates max startup query 
                                                 count */
   UINT1                  u1FastLeave;        /* Enable or Disable Fast leave 
                                                 in this VLAN              */
   UINT1                  u1RowStatus;        /* Row Status                */
   UINT1                  u1OperVersion;      /* Operationg Version (V1/V2/V3)*/ 
   UINT1                 u1IpFlag;          /*If u1IpFlag is set to 1 and querier ip address
                                             *is not configured, interface ip will be used
                                             *if it is configured or switch ip will be used*/
   UINT1                  au1Reserved[1];      /* Reserved*/
} tSnoopVlanEntry;

/* Router Port Record Structure */
typedef struct SnoopRtrPortEntry {
   tSnoopSllNode           NextRtrPortEntry;
   tRBNodeEmbd             RbNode;
   tSnoopTmrNode           V1RtrPortPurgeTimer;       /* V1 Rtr port purgetimer       */
   tSnoopTmrNode           V2RtrPortPurgeTimer;       /* V2 Rtr port purgetimer       */
   tSnoopTmrNode           V3RtrPortPurgeTimer;       /* V3 Rtr port purgetimer       */
   tSnoopVlanEntry        *pVlanEntry;       /* back pointer to the VLAN entry*/
   UINT4                   u4PhyPortIndex;                 /* Physical port number
                                                       * for the router port
                                                       */
   UINT4                   u4V1V2RtrPortPurgeInt;     /* V1V2 Rtr port purge 
                                                         interval */
   UINT4                   u4V3RtrPortPurgeInt;       /* V1 Rtr port purge 
                                                         interval */
   UINT4                   u4Port;           /* Port number through which a
                                                router cntrl msg was received */
   UINT2                   u2InnerVlanId;         /* This is used to learn the Customer
                                                   Vlan in enhanced mode.  */
   UINT1                   u1ConfigOperVer;     /* Configured Operating Version */ 
   UINT1                   u1OperVer;           /* Current Operating Version */
   UINT1                   u1IsDynamic;           /* to determine the port is dynamically 
                                                     learnt router port */
   UINT1                   au1Pad[3];
} tSnoopRtrPortEntry;

/*********For Static*************/
/* ASM Port record structure for each Group IP entry.*/ 

typedef struct ASMPortEntry {
   tSnoopSllNode          NextPortEntry;
   tSnoopGroupEntry       *pGroupEntry;         /* Back pointer to the group 
                                                   entry             */
   tSnoopTmrNode          PurgeOrGrpQueryTimer; /* Port Purge or 
                                                   Group query Timer */
   UINT4                  u4Port;               /* Port Number       */  
   UINT1                  u1RetryCount;         /* Retry Count for sending Grp 
                                                   Specific queries  */
   UINT1                  u1V1HostPresent;      /* Check the presence of 
                                                   version v1 host   */ 
   UINT2                  u2Reserved;
} tSnoopASMPortEntry;



/* Snooping Memory Pool Structure per instance */
typedef struct SnoopInstMemPoolId{
   tSnoopMemPoolId        GrpEntryPoolId;        /* Group membership 
                                                    information              */
   tSnoopMemPoolId        ConsGrpEntryPoolId;    /* Group consolidated  
                                                    membership information   */
   tSnoopMemPoolId        PortEntryPoolId;       /* Port entry for the ASM & SSM
                                                    report messages          */
   tSnoopMemPoolId        HostEntryPoolId;       /* Host record entry for
                                                    a Group IP address       */
   tSnoopMemPoolId        SourceBmpPoolId;       /* Include & Exclude Bitmaps 
                                                    for the SSM port and host 
                                                    entries */
   tSnoopMemPoolId        VlanEntryPoolId;       /* VLAN record entries      */
   tSnoopMemPoolId        VlanStatsPoolId;       /* SNOOP Statistics entry for 
          a VLAN                   */
   tSnoopMemPoolId        VlanCfgPoolId;         /* Configured VLAN entries  */
   tSnoopMemPoolId        RtrPortEntryPoolId;    /* Router Port Entry for a 
                                                    Vlan record              */ 
   tSnoopMemPoolId        MacFwdEntryPoolId;     /* MAC based forwarding 
                                                    entry                    */ 
   tSnoopMemPoolId        IpFwdEntryPoolId;      /* IP based forwarding entry */
 
   tSnoopMemPoolId        MVlanMappingPoolId;   /* M-VLAN and Profile mapping
                                                   mempool for M-VLAN 
                                                   Classification*/
   tSnoopMemPoolId        MappedPvlanListPoolId; /* Mapped Pvlan List    */ 
   tSnoopMemPoolId        HwIpFwPoolId;          /* Hardware IP-forward information */
#ifdef L2RED_WANTED
   tSnoopMemPoolId        RedMCastMemPoolId;
#endif
}tSnoopInstMemPoolId;

/* Snooping Global Memory Pool Structure */
typedef struct SnoopGlobalMemPoolId{
   tSnoopMemPoolId        SnoopGblInstPoolId;   /* Mem Pool Id for Global 
                                                   instance  node          */   
   tSnoopMemPoolId        QMsgMemPoolId;        /* Mem pool id for queue 
                                                   messages                */
   tSnoopMemPoolId        QPktMemPoolId;        /* Mem pool id for Frames
                                                   in Queue to be process by
                                                   SNOOP                   */
   tSnoopMemPoolId        MiscMemPoolId;        /* Misc mem pool for forming 
                                                   and sending V3 report   */
   tSnoopMemPoolId        SrcMemPoolId;         /* Src mem pool for forming 
                                                   and sending SSM report  */
   tSnoopMemPoolId        GrpSrcRecordsPoolId;   /* Grp Src Records mem pool 
                                                    for M-VLAN Classification*/ 
  
   tSnoopInstMemPoolId    SnoopInstMemPool;     /* SNOOP Memory pool struture 
                                                   for an instance */
   tSnoopMemPoolId   PortCfgEntryPoolId;   /* Port Configuration entries*/
   tSnoopMemPoolId   ExtPortCfgEntryPoolId;/* Extended Port configuration
                                                   Entries(leavemode,
                                                   RateLmt..etc)             */
   tSnoopMemPoolId        DataMemPoolId;        /* Data mem pool for forming 
                                                   and sending Data packet  */

   tSnoopMemPoolId        IpvxAddrSrcPoolId;     /*IPVx Source Addr Pool*/

} tSnoopGlobalMemPoolId;

typedef struct SnoopExtPortCfgEntry{
   UINT4                  u4SnoopPortRateLimit; /* To configure the rate limit 
                                                   for a down stream interface*/
   UINT4    u4SnoopPortCfgMemCnt; /* Total number of groups or 
                                                   channels active on the 
                                                   interface at the given time*/
   UINT4                  u4SnoopPortMaxLimit;  /* Max limit of channels or 
                                                   goups for a down stream 
                                                   interface                  */
   UINT1                  u1SnoopPortLeaveMode; /* Leave processing mode      */
   UINT1                  u1SnoopPortMaxLimitType;/*Type of max limit for 
                                                    a down stream interface.  */
   UINT1                  au1Reserved[2];        /* Reserved                   */
} tSnoopExtPortCfgEntry;

typedef struct SnoopPortCfgEntry{
   tRBNodeEmbd            GlobalRbNode;         /* RB Node for the global 
                                                   RBTree                     */
   tRBNodeEmbd            InstanceRbNode;       /* RB Node for RB Tree within
                                                   an instance                */
   UINT4                  u4SnoopPortCfgProfileId;/* Multicast profile index 
                                                   for a down stream interface*/
   UINT4                  u4SnoopPortIndex;       /* Port Number              */
   tSnoopExtPortCfgEntry  *pSnoopExtPortCfgEntry;/* Pointer to a structure of
                                                    port parameters           */
   tSnoopTag              VlanId;               /* Inner Vlan Id or C-VLAN Id */
   UINT1    u1SnoopPortCfgInetAddrtype;/* Address type IGMP or 
                                                        MLD snooping          */
   UINT1                  u1SnoopPortRowStatus; /* Row status of an entry in 
                                                   the Port Config table      */
   UINT1                  au1Reserved[2];        /*  Reserved                  */
} tSnoopPortCfgEntry;


/* IGMP Snooping Module's Global Instance Data structure */

typedef struct SnoopInfo{
   UINT4                u4RtrPortPurgeInt;    /* Router port purge interval */
   UINT4                u4PortPurgeInt;       /* Port purge interval        */
   UINT4                u4ReportFwdInt;       /* Report forward interval    */
   UINT4                u4GrpQueryInt;        /* Group Query interval       */
   UINT4                u4TraceOption;        /* Indicates snooping trace 
                                                 options                    */
   UINT4                u4DebugOption;        /*Indicates snooping trace
        Options      */
   UINT4                u4FwdGroupsCnt;        /*Ip Forwarding DataBase Group Count */
   UINT4                u4ActiveGroupsCnt;    /* Active Groups Count in the system */
   UINT1                u1SnoopStatus;        /* Indicates snooping Admin status
                                                 in the system 
                                                 (Enable / Disable)         */
   /* The Snooping Switch can be configured to send IGMP/MLD General
    * queries on all non router ports when spanning tree topology
    * change occurs in a network based on the status of this variable.
    */
   UINT1                u1SnoopQueryStatus;   /*Indicates the snooping Query
                                                Transmission status 
                                                (Enable/Disable)*/
   UINT1                u1ProxyStatus;        /* Indicates the Proxy / proxy 
                                                 reporting status in the system 
                                                 (Enable / Disable)         */
   UINT1                u1RetryCount;         /* Retry Count for sending Grp 
                                                 Specific queries           */
   UINT1                u1ReportFwdAll;       /* Report forward all flag to
                                                 provide administrative 
                                                 control to forward the reports
                                                 on all ports               */
   UINT1                u1SnoopOperStatus;    /* Indicates the snooping
                                                 Operational status in the
                                                 system (Enable/Disable)    */
   UINT1                u1MVlanStatus;       /* Indicates the Multicast VLAN
                                                translation  status
                                                 (Enable/Disable)           */
   UINT1                u1FilterStatus;       /* Indicates the filter status
                                                 (Enable/Disable)           */

   UINT1                u1QueryFwdAll;        /* Query forward all flag to
                                                 provide administrative 
                                                 control to forward the queries
                                                 on all ports               */
   UINT1                au1Reserved[3];        /*  Reserved                  */
} tSnoopInfo;

typedef struct SnoopGlobalInstInfo{
   tRBTree              GroupEntry;           /* Pointer to group membership 
                                                 table                      */
   tRBTree              ConsGroupEntry;       /* Pointer to consolidated 
                                                 group membershiptable      */
   tRBTree              MacMcastFwdEntry;     /* Pointer to MAC based
                                                 mcast forwarding table     */
   tRBTree              IpMcastFwdEntry;      /* IP based mcast forwarding
                                                 table                      */
   tRBTree              VlanEntry;            /* Pointer to SNOOP VLAN 
                                                 information                */
   tRBTree              HostEntry;            /* Pointer to the attached 
                                                 host information           */ 
   tRBTree              PortCfgEntry;         /* Pointer to Port config Entry*/
   tSnoopInfo           SnoopInfo[SNOOP_MAX_PROTO]; /* It points to the 
                                                          Snooping Information.
                                                          i.e., IGS and MLDS 
                                                          information currently
                                                                            */
   tSnoopSll            MVlanMappingList; /* List of M-Vlan to Profile ID
                                             mapping */

   tSnoopSourceInfo     SnoopSrcInfo[SNOOP_MAX_MCAST_SRCS]; /* Array of source 
                                                         addresses for which 
                                                         registrations 
                                                    (Include /Exclude) have 
                                                    been for reported for this 
                                                    group. This will be used in
                                                    IP based forwarding */
   tSnoopDll            SnoopSrcList;        /* List of sorted registered 
                                                 source addresses.
                                                 This will be used for
                                                 lexicographical ordering
                                                 of the sources */ 
   tSnoopTmrNode       SrcListCleanUpTmr;   /* Timer Node for
            source list garbage collection
         */
   UINT4    au4SnoopPhyPort [SNOOP_MAX_PORTS_PER_INSTANCE]; /*array indexed with 
                                                         local port,In case of
                                                         MI for quick reference 
                                                         from local port to 
                                                         physical port this
                                                         array is used */
   UINT1                au1SnoopTrcMode[SNOOP_TRC_NAME_SIZE]; 
   
   UINT1                au1ContextStr [SNOOP_INSTANCE_ALIAS_LEN + 1]; 
                                                                   /* Alias name
                                                                     of this 
                                                                     Instance */
   UINT1                u1McastFwdMode;       /* Specifies the multicast 
                                                 forwading mode (IP based or 
                                                 MAC based)                 */
   UINT1  u1SystemEnhMode;  /* Indicates whether the system mode  is
             default or enhanced mode */
   UINT1                u1SystemSparseMode;   /* Specifies sparse mode setting for
                                                 the system */
   UINT1                u1LeaveConfLevel;     /* Specifies the leave config
                                                 level (vlan based or port 
                                                 based) */
   UINT1                u1ReportConfLevel;     /* Specifies the report config
                                                 level (on non-router ports or 
                                                 all ports) */
   UINT1               u1ControlPlaneDriven;
#ifdef ICCH_WANTED
   UINT1                u1MclagIpHostOptimize;  /* Incase of MCLAG IP based 
                                                   Enhanced mode, the group 
                                                   information from multiple
                                                   hosts to be synced to neighbour
                                                   node as same as received from
                                                   original IP address of the host.
                                                   This will result in multiple 
                                                   messages between MCLAG nodes. 
                                                   If enabled, this will sync
                                                   all the host information to
                                                   neighbour nodes. */
#else
   UINT1                au1Reserved[1];        /*  Reserved                  */
#endif
} tSnoopGlobalInstInfo;


/* Constant declaration which defines the status of the node */

typedef UINT4 tSnoopNodeState;
#define SNOOP_IDLE_NODE                       RM_INIT
#define SNOOP_ACTIVE_NODE                     RM_ACTIVE
#define SNOOP_STANDBY_NODE                    RM_STANDBY
#define SNOOP_ACTIVE_TO_STANDBY_IN_PROGRESS   4
#define SNOOP_SHUT_START_IN_PROGRESS          5


/******************************************************************************/
/*                           Message Queue Structure                          */
/******************************************************************************/

typedef struct SnoopPortListVlanMac {
   tSnoopTag       VlanId;
   tSnoopIfPortBmp AddPortList;
   tSnoopIfPortBmp DelPortList;
   tMacAddr        McastAddr;
   UINT1           au1Reserved[2];
}tSnoopPortListVlanMac;

typedef struct SnoopStpTcInfo {
    UINT4 u4Port;
    tSnoopVlanList VlanList;
}tSnoopStpTcInfo;

#ifdef NPAPI_WANTED
typedef struct {
 UINT4              u4npCallId;
 UINT4              u4Instance;
 UINT4              u4GrpAddr;
 UINT4              u4SrcAddr;
 INT4               rval;
 tPortList          PortList;
 tPortList          UntagPortList;
 tSnoopVlanId       VlanId;
 UINT1              u1EventType;
 UINT1              au1Reserved[1];
} tNpUpdateIpmcFwdEntries;

typedef struct {
 UINT4              u4npCallId;
 UINT4              u4Instance;
 tSnoopVlanId       OuterVlanId;
 tSnoopVlanId       InnerVlanId;
 UINT4              u4GrpAddr;
 UINT4              u4SrcAddr;
 INT4               rval;
 tPortList          PortList;
 tPortList        UntagPortList;
 UINT1            u1EventType;
 UINT1              au1Reserved[3];
} tIgsUpdateIpmcEntry;

#endif


#ifdef L2RED_WANTED

/* Typedef to represent the peer Id */
typedef VOID *  tSnoopRedPeerId;

/* Structure to enqueue RM data to the Snoop module */
typedef struct _tSnoopRmData 
{
   UINT4           u4Events;
   tSnoopRedPeerId PeerId;
   tRmMsg          *pRmMsg;
   UINT2           u2DataLen;
   UINT1           au1Reserved[2];
} tSnoopRmData;

/* Structure to post Vlan Multicast table Audit sync msg
 * to the Snoop Module */
typedef struct SnoopRedAuditSyncMsg {
   UINT4           u4Events;
   tSnoopTag       VlanId;
   tSnoopPortBmp   AuditPortBmp;
   tMacAddr        McastAddr;
   UINT1           au1Reserved[2];
} tSnoopRedAuditSyncMsg;

#endif

typedef struct SnoopMsgNode {
   union {
   UINT4                  u4Port;
   tSnoopTag              VlanId;
   tSnoopPortListVlanMac  VlanPortListMac;
   tSnoopStpTcInfo        StpTcInfo;

#ifdef NPAPI_WANTED
   /*tNpUpdateIpmcFwdEntries IpmcFwdEntries;*/
   tIgsUpdateIpmcEntry     IpmcEntry;
#endif   

#ifdef L2RED_WANTED
   tSnoopRmData       RmData;
   tSnoopRedAuditSyncMsg  AuditSyncMsg;
#endif
#ifdef MBSM_WANTED
   struct MbsmIndication {
       tMbsmProtoMsg *pMbsmProtoMsg;
   }MbsmCardUpdate;
#endif
   } uInMsg;
#ifdef ICCH_WANTED
   tSnoopIcchData IcchData; /* Icch Data posted by ICCH module */
#endif
   UINT4 u4Instance; /* This is used in Multi Instnace case, indicates which 
                        instnce this message belongs to */
   UINT4 u4Addr;
   UINT4 u4OperStatus;
   UINT2 u2IntfVlan;
   UINT2 u2MsgType;
   UINT2 u2LocalPort; /* virtual port number corresponding to physical port 
                         generated by VCM */
   UINT2 u2AggId;
   UINT1 u1Type;
   UINT1 au1Reserved[3]; /* reserved for packing */
}tSnoopMsgNode;

/* Structure to hold the msgs posted to snooping module */
typedef struct SnoopQMsg {
      tCRU_BUF_CHAIN_HEADER  *pInQMsg;
      UINT4                   u4Instance;
      tSnoopVlanInfo          VlanInfo;
      UINT2                   u2LocalPort;
      UINT2                   u2TagLen;
      UINT1                   u1PktType;
      UINT1                   u1Reserved[3];
} tSnoopQMsg;

/******************************************************************************/
/* Reference data structure to hold Information extracted from IP header */
/******************************************************************************/

typedef struct  SnoopPktInfo {
    tIPvXAddr       SrcIpAddr;
    tIPvXAddr       DestIpAddr;
    tIPvXAddr       GroupAddr;
    UINT4           u4InPort;
    UINT2           u2PktLen;
    UINT2           u2MaxRespCode;
    UINT1           u1IpHdrLen;
    UINT1           u1PktType;
    UINT1           u1Protocol;
    UINT1           u1IsQueryMsg;
    UINT1           u1QueryType;
    UINT1           u1TagLen;
#ifdef ICCH_WANTED
    UINT1           u1IcchReportVersion;
    UINT1           au1Reserved[1];
#else
    UINT1           au1Reserved[2];
#endif
} tSnoopPktInfo; 



#ifdef ICCH_WANTED
typedef struct SnoopIcchFillInfo
{
    tSnoopPktInfo *pSnoopPktInfo;
    tSnoopHostEntry *pSnoopHostEntry;
    tSnoopTag   VlanId;
} tSnoopIcchFillInfo;
#endif

/* Used for constructing IGMP packet */
typedef struct SnoopIgmpHdr {
    UINT1  u1PktType;
    UINT1  u1MaxRespTime;
    UINT2  u2CheckSum;
    UINT4  u4GrpAddr;      
} tSnoopIgmpHdr;


/* Data structures to extract IGMP v3 report messages */
typedef struct SnoopIgmpGroupRec {
    UINT1             u1RecordType;
    UINT1             u1AuxDataLen;
    UINT2             u2NumSrcs;
    UINT4             u4GroupAddress;
#ifdef ICCH_WANTED
    UINT2             u2AuxData;
    UINT1             u1IcclData;
    UINT1             u1IcchReportVersion;
#endif
} tSnoopIgmpGroupRec;

/* Used for constructing MLD packet */
typedef struct SnoopMLDHdr {
    UINT1        u1PktType;
    UINT1        u1Code;
    UINT2        u2CheckSum;
    UINT2        u2MaxRespTime;
    UINT2        u2Reserved;
    tIp6Addr     GrpAddr;      
} tSnoopMLDHdr;

/* Data structures to extract MLD v2 report messages */
typedef struct SnoopMLDGroupRec {
    UINT1             u1RecordType;
    UINT1             u1AuxDataLen;
    UINT2             u2NumSrcs;
    UINT1             u1GrpAddr[IPVX_MAX_INET_ADDR_LEN];
#ifdef ICCH_WANTED
    UINT2             u2AuxData;
    UINT1             u1IcclData;
    UINT1             au1Reserved[1];
#endif
} tSnoopMLDGroupRec;


/* Used in validation of IPV6 jumbogram headers in MLDS processing */
typedef struct SnoopJmbHdr{
    UINT1  u1OptType;  /* Option type, for Jumbogram - 0xc2 */

    UINT1  u1OptLen;
    UINT2  u2Reserved;
    UINT4  u4JmbLen;   /* Jumbo Payload length */
}
tSnoopJmbHdr;

/* Data Structure to hold the router alert header information */
typedef struct SnoopRtAlertHdr {
    UINT1 u1OptType;     /* Option type - for router alert- 5*/
    UINT1 u1OptLen;      /* Option length */
    UINT2 u2OptValue;    /* Option value */
}tSnoopRtAlertHdr;

typedef struct ExtHdr
{
    UINT1  u1NextHdr;  /* Type of Header immediately following the
                        *  Dest Options header
                        */
    UINT1  u1HdrLen;   /* Length of the Destination option header
                        * in 8-octet units, not including the first
                        * 8-octets
                        */
    UINT2 u2Reserved;
}
tSnoopExtHdr;

/* Data structure used to extract and construct IGMPv3/MLDv2 report 
 * messages */
typedef struct SnoopSSMReport {
    UINT1             u1PktType;
    UINT1             u1Resvd;
    UINT2             u2CheckSum;
    UINT2             u2Resvd;
    UINT2             u2NumGrps;
} tSnoopSSMReport;

typedef struct SnoopFilterPkt {
        tSnoopTag     VlanId;        /* Vlan Identifier */
        UINT1         SrcIpAddr[IPVX_MAX_INET_ADDR_LEN];     
        UINT1         GrpAddress[IPVX_MAX_INET_ADDR_LEN];
        UINT4         u4InPort;      /* The port on which the packet was 
                                      received */
    tSnoopMacAddr MacAddr;    /* Source MAC address in received packet or
                                 Group MAC address of the MAC group
                                 forwarding entry*/
        UINT1         u1PktType;     /* Report/Leave */
        UINT1         u1Version;     /* version v1/v2/v3 */
        UINT1         u1NotifyType;
    UINT1         au1Reserved[3];     /* For padding */
}tSnoopFilter;

typedef struct
{
    tSnoopSllNode NextMVlanMapEntry;
    UINT4         u4ProfileId;
    tVlanId       VlanId;
    UINT1         au1Reserved[2];     /* For padding */
}tMVlanMapping;

/* This structure tracks the maximum number of groups created on the fly */
/******For static***********/
typedef struct GroupEntryStatusNode
{
    UINT2 u2MaxGrpEntry;
    UINT2 u2GrpEntryCount;
}tGroupEntryStatusNode;



/* BELOW STRUCTURE IS USED FOR MEMPOOL CREATION ONLY*/
/* START */
typedef struct SnoopPVlanListSize{
    tVlanId  VlanIdList[SNOOP_MAX_COMMUNITY_VLANS + 1];
}tSnoopPVlanListSize;
/*END */
#endif /* _SNPTDFS_H */
