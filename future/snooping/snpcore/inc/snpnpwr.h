#ifndef _SNPNP_WR_H_
#define _SNPNP_WR_H_

/* This file contains prototypes for NPAPI's */
#ifdef IGS_WANTED
INT4 FsMiIgsHwEnableIgmpSnooping PROTO ((UINT4));
INT4 FsMiIgsHwDisableIgmpSnooping PROTO ((UINT4));

INT4
FsMiNpUpdateMcFwdEntries PROTO ((UINT4 u4Instance, 
                                 tSnoopVlanId VlanId, UINT4 u4GrpAddr, 
                        UINT4 u4SrcAddr, UINT1 *pu1PortList, 
                        UINT1 u1EventType));

INT4 FsMiNpGetFwdEntryHitBitStatus PROTO ((UINT4 u4Instance, UINT2, UINT4,
                                         UINT4));

INT4 FsMiIgsHwAddRtrPort PROTO ((UINT4, tSnoopVlanId, UINT2));
INT4 FsMiIgsHwDelRtrPort PROTO ((UINT4, tSnoopVlanId, UINT2));

INT4 FsMiIgsHwEnableIpIgmpSnooping PROTO ((UINT4));
INT4 FsMiIgsHwDisableIpIgmpSnooping PROTO ((UINT4));

INT4 FsMiNpUpdateIpmcFwdEntries (UINT4, tSnoopVlanId, UINT4, UINT4, tPortList, tPortList, UINT1);

INT4 FsMiIgsHwUpdateIpmcEntry PROTO ((UINT4, tIgsHwIpFwdInfo *, UINT1));
INT4 FsMiIgsHwGetIpFwdEntryHitBitStatus PROTO ((UINT4, tIgsHwIpFwdInfo *));
VOID FsMiIgsHwClearIpmcFwdEntries PROTO ((UINT4));
VOID FsNpClearIpmcFwdEntries PROTO ((VOID));
INT4 FsMiIgsHwSparseMode PROTO ((UINT4, UINT1));
INT4 FsMiIgsHwEnhancedMode PROTO ((UINT4, UINT1));
INT4 FsMiIgsHwPortRateLimit PROTO ((UINT4, tIgsHwRateLmt *, UINT1));
INT4 FsIgsHwEnhancedMode PROTO ((UINT1));
INT4 FsIgsHwSparseMode PROTO ((UINT1));
INT4 FsIgsHwPortRateLimit PROTO ((tIgsHwRateLmt *, UINT1));
#ifdef L2RED_WANTED
INT4
FsNpGetIpMcastFwdEntry (tSnoopVlanId VlanId, UINT4 u4GrpAddr, UINT4 u4SrcAddr,
                        UINT1 *pu1PortList);
INT4
FsNpGetFirstIpMcastFwdEntry (INT4 *pi4VlanId, UINT4 *pu4McastGrpIpAddr,
                             UINT4 *pu4McastSrcIpAddr);

INT4
FsNpGetNextIpMcastFwdEntry (INT4 i4VlanId, INT4 *pi4VlanId,
                            UINT4 u4McastGrpIpAddr, UINT4 *pu4McastGrpIpAddr,
                            UINT4 u4McastSrcIpAddr, UINT4 *pu4McastSrcIpAddr);
#endif

#ifdef MBSM_WANTED
INT4 FsMiIgsMbsmHwEnableIgmpSnooping PROTO ((UINT4 u4ContextId, 
                                             tMbsmSlotInfo * pSlotInfo));
INT4 FsMiIgsMbsmHwEnableIpIgmpSnooping PROTO ((UINT4 u4ContextId, 
                                               tMbsmSlotInfo * pSlotInfo));
#endif

#endif /* IGS_WANTED */

#ifdef MLDS_WANTED
INT4 FsMiMldsHwEnableMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDisableMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDestroyMLDFP PROTO ((UINT4));
INT4 FsMiMldsHwDestroyMLDFilter PROTO ((UINT4));

INT4 FsMiNpGetIp6FwdEntryHitBitStatus PROTO ((UINT4, UINT2, UINT1 *, UINT1 *));

INT4 FsMiMldsHwEnableIpMldSnooping PROTO ((UINT4));
INT4 FsMiMldsHwDisableIpMldSnooping PROTO ((UINT4));

INT4 FsMiNpUpdateIp6mcFwdEntries PROTO ((UINT4, tSnoopVlanId, UINT1 *, UINT1 *,
                                         tPortList, tPortList, UINT1));

#ifdef MBSM_WANTED
INT4 FsMiMldsMbsmHwEnableMldSnooping PROTO ((UINT4 u4ContextId, 
                                             tMbsmSlotInfo * pSlotInfo));
INT4 FsMiMldsMbsmHwEnableIpMldSnooping PROTO ((UINT4 u4ContextId, 
                                               tMbsmSlotInfo * pSlotInfo));
#endif

#endif /* MLDS_WANTED */

#endif /* _SNPNP_WR_H_ */
