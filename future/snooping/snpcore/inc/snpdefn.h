#ifndef _SNPDEFN_H
#define _SNPDEFN_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: snpdefn.h,v 1.62 2017/08/01 13:50:24 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpdefn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Definitions                */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains Definitions                 */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0     January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
/* Snooping Module related directives */
#define SNOOP_TASK_NAME            "SnpT"
#ifdef L2RED_WANTED
#define SNOOP_AUDIT_TASK_NAME      ((UINT1 *) "SnAu")
#define SNOOP_AUDIT_TASK_PRIORITY  250
#endif /* L2RED_WANTED */
#define SNOOP_MUTUAL_EXCL_SEM_NAME (const UINT1 *) "SNPM" 
#define SNOOP_MSG_QUEUE_NAME       (UINT1 *) "SNMQ"
#define SNOOP_PKT_QUEUE_NAME       (UINT1 *) "SNPQ"

#define SNOOP_IGS_PKT_ENQ_EVENT       0x01
#define SNOOP_MLDS_PKT_ENQ_EVENT      0x02
#define SNOOP_MSG_EVENT               0x04
#define SNOOP_TMR_EXPIRY_EVENT        0x08
#define SNOOP_RED_BULK_UPD_EVENT      0x10 
#define SNOOP_RED_GO_ACTIVE_EVENT     0x20 
#define SNOOP_MSR_COMPLETE_EVENT      0x40 

#define SNOOP_QUEUE_MODE          0
#define SNOOP_PKT_QUEUE_DEPTH         SNOOP_MAX_IGMP_PKTS_PROCESSED_IN_A_SEC
#define SNOOP_MSG_QUEUE_DEPTH         ((4*SNOOP_MAX_PORTS)>SNOOP_STP_TOPO_CHG_MSG) \
                                        ?4*SNOOP_MAX_PORTS:SNOOP_STP_TOPO_CHG_MSG

#define SNOOP_MAX_IP_BUF          82 /* Ethernet header + vlanTag Length + IP header Length */
                                           
#define SNOOP_RTR_ADVT_PKT        2

/* 
 * SNOOP_STP_TOPO_CHG_MSG : Spanning-tree topology change indication is given 
 * to snooping module. Number of messages posted by STP module depends on 
 * number of instances supported by STP module, this adds up to number of
 * messages to be handled by snooping module */

#define   SNOOP_BASIC_MODE                  1
#define   SNOOP_ENHANCED_MODE               2

#define   SNOOP_MAX_VLANIDS           VLAN_DEV_MAX_VLAN_ID
#define   SNOOP_MAX_VLAN_ID           VLAN_MAX_VLAN_ID
#define   SNOOP_VLAN_TAG_PID_LEN      VLAN_TAG_PID_LEN
#define   SNOOP_MAX_PROTO             2 /* IGS and MLDS currently */

#define   SNOOP_INIT_VAL          0 

 /* MEM Pool Block Count */
/* 
 * The below constants had to be ported according to the requirement,
 * this constants will be later handled using system sizing 
 * implementation.   
 */ 

#define   SNOOP_VLAN_TABLE_SIZE          SNOOP_MAX_VLANIDS 
#define   SNOOP_VLAN_STATS_SIZE          SNOOP_MAX_VLANIDS 
#define   SNOOP_VLAN_CFG_SIZE            SNOOP_MAX_VLANIDS 
       
#define   SNOOP_SRC_LIST_SIZE           ((SNOOP_MAX_MCAST_SRCS + 31)/32 * 4)


/* Default vlaues for Port Config Entry parameters */

#define SNOOP_DEF_MCAST_THRESH_LIMIT     0
#define SNOOP_PORT_RATE_LMT_DEF          0xFFFFFFFF
#define SNOOP_PORT_CFG_LMT_DEF          0xFFFFFFFF 
#define SNOOP_PORT_CFG_LMT_TYPE_DEF      0
#define SNOOP_PORT_CFG_MAX_PROFILE_ID    0xFFFFFFFF 
#define SNOOP_PORT_CFG_DEF_MODE_CVLAN    0
#define SNOOP_PORT_CFG_INNER_VLAN_ID     0 

#define SNOOP_INVALID_IFINDEX            0xffffffff
    
/*  The below constants are Range of Inner Vlan Ids */
#define SNOOP_MIN_INNER_VLANIDS          0
#define SNOOP_MAX_INNER_VLANIDS          4094

/* Max number of chnnels that system support in the port config entries */
#define SNOOP_MAX_MCAST_CHANNELS         SNOOP_MAX_MCAST_GROUPS * SNOOP_MAX_MCAST_SRCS

/* Default multicast vlan profie */    
#define SNOOP_DEF_MVLAN_PROFILEID        0

/* Number of MAC Forwarding table entries */
#define   SNOOP_MAX_MAC_FWD_ENTRIES       VLAN_DEV_MAX_MCAST_TABLE_SIZE  

/* Number of IP Forwarding table entries */
#define   SNOOP_MAX_IP_FWD_ENTRIES   SNOOP_MAX_HW_ENTRIES

/* FSAP and OSIX related defentions */
          
#define   SNOOP_MEM_TYPE                   MEM_DEFAULT_MEMORY_TYPE

/* PVLANs Macros */
#define  SNOOP_MAX_COMMUNITY_VLANS         256
/* Maximum possible number of MappedVlan List is number of community vlans plus
 * one isolated vlan */
#define  SNOOP_MAX_MAPPED_PVLANS           (sizeof(tVlanId) *(SNOOP_MAX_COMMUNITY_VLANS + 1))
/* There are possibilites for invoking the MappedVlan List multiple times
 * in a single thread, hence 5 blocks has been allocated */
#define  SNOOP_MAX_MAPPED_PVLANS_BLOCKS     5

#define     SNOOP_MAX_IPVX_ADDR_BLOCKS         2

#define   SNOOP_INST_MEM_TYPE  \
          ((VcmGetSystemMode (SNOOP_PROTOCOL_ID) == VCM_SI_MODE) \
           ? SNOOP_MEM_TYPE: MEM_HEAP_MEMORY_TYPE)
                                         
#define   SNOOP_MEM_SUCCESS                MEM_SUCCESS
#define   SNOOP_MEM_FAILURE                MEM_FAILURE
#define   SNOOP_MEM_CPY                    MEMCPY
#define   SNOOP_MEM_CMP                    MEMCMP
#define   SNOOP_MEM_SET                    MEMSET
#define   tSnoopSll                        tTMO_SLL
#define   tSnoopSllNode                    tTMO_SLL_NODE
#define   tSnoopDll                        tTMO_DLL
#define   tSnoopDllNode                    tTMO_DLL_NODE
#define   tSnoopHashTable                  tTMO_HASH_TABLE
#define   tSnoopHashNode                   tTMO_HASH_NODE
#define   tSnoopAppTimer                   tTmrAppTimer
#define   tSnoopMemPoolId                  tMemPoolId
#define   tSnoopSemId                      tOsixSemId
#define   tSnoopTaskId                     tOsixTaskId
#define   tSnoopQId                        tOsixQId
#define   tSnoopTmrListId                  tTimerListId
                                         
#define   SNOOP_NTOHS                      (UINT2 )OSIX_NTOHS
#define   SNOOP_NTOHL                      (UINT4 )OSIX_NTOHL
#define   SNOOP_HTONS                      (UINT2 )OSIX_HTONS
#define   SNOOP_HTONL                      (UINT4 )OSIX_HTONL
                                         
/* Definitions from other modules */     
#define   tSnoopMacAddr                    tMacAddr

/* Snooping definitions */          
#define SNOOP_MAX_INSTANCES                L2IWF_MAX_CONTEXTS 
#define SNOOP_INSTANCE_ALIAS_LEN           L2IWF_CONTEXT_ALIAS_LEN
#define SNOOP_IGS_IGMP_VERSION1                1
#define SNOOP_IGS_IGMP_VERSION2                2
#define SNOOP_IGS_IGMP_VERSION3                3
#define SNOOP_IGS_IGMP_INVALID_VERSION         0
#define SNOOP_IGS INVALID_GRPADDR              0

#define SNOOP_PROXY_REPORTING_MODE        3
#define SNOOP_PROXY_MODE                  4
          
#define SNOOP_MLD_VERSION1                1
#define SNOOP_MLD_VERSION2                2

#define SNOOP_INVALID_VLAN_ID              0
/* Events from other modules */
        
/* These constants are used in switch statements where MBSM events
   are also used. So keep the values for these constants lesser than 
   MBSM events (less than 100). */ 
#define SNOOP_ENABLE_PORT_MSG              1
#define SNOOP_DELETE_PORT_MSG              2
#define SNOOP_DISABLE_PORT_MSG             3
#define SNOOP_VLAN_DELETE_EVENT            4
#define SNOOP_UPDATE_VLAN_PORTS            5
#define SNOOP_RM_DATA                      6
#define SNOOP_RED_AUDIT_SYNC_MSG           7
#define SNOOP_CREATE_CONTEXT               8
#define SNOOP_DELETE_CONTEXT               9
#define SNOOP_MAP_PORT                    10 
#define SNOOP_UNMAP_PORT                  11 
#define SNOOP_CREATE_PORT_MSG             12
#define SNOOP_TOPO_CHANGE                 13
#define SNOOP_UPDATE_CONTEXT_NAME         14
#define SNOOP_NP_CALLBACK                 15
#define SNOOP_IPADDR_CHANGE               18
#define SNOOP_VLAN_CREATE_EVENT           19
#define SNOOP_ICCH_MSG                    20
#define SNOOP_IGS_RED_GEN_QUERY           21


#define SNOOP_ADD_FORBIDDEN_PORT           1
#define SNOOP_DEL_FORBIDDEN_PORT           2

/* SEM releated definitions */
        
#define SNOOP_SEM_NAME                     SNOOP_MUTUAL_EXCL_SEM_NAME
#define SNOOP_SEM_COUNT                    1
#define SNOOP_SEM_FLAGS                    OSIX_DEFAULT_SEM_MODE
#define SNOOP_SEM_NODE_ID                  SELF
          
/* Snooping trace option default defintion */
          
#define SNOOP_DEF_TRACE_OPTION             0
          
/* Snooping Timer related definitions */
          
#define   SNOOP_REPORT_FWD_TIMER           1
#define   SNOOP_ASM_PORT_PURGE_TIMER       2        
#define   SNOOP_ROUTER_PORT_PURGE_TIMER    3
#define   SNOOP_QUERY_TIMER                4
#define   SNOOP_GRP_QUERY_TIMER            5
#define   SNOOP_HOST_PRESENT_TIMER         6
#define   SNOOP_IP_FWD_ENTRY_TIMER         7
#define   SNOOP_ROUTER_PORT_PURGE_V1_TIMER 8
#define   SNOOP_ROUTER_PORT_PURGE_V2_TIMER 9
#define   SNOOP_ROUTER_PORT_PURGE_V3_TIMER 10
#ifdef L2RED_WANTED
#define   SNOOP_RED_SWITCHOVER_QUERY_TIMER 11
#endif
#define   SNOOP_INVALID_TIMER_TYPE         0        

#define   SNOOP_SRC_LIST_CLEANUP_TIMER     12
#define   SNOOP_OTHER_QUERIER_PRESENT_TIMER 13
#define   SNOOP_IN_GROUP_SOURCE_TIMER       14
#define   SNOOP_MAX_TIMER_COUNTS           20

/* Values for the different timer intervals */
#define   SNOOP_SRC_LIST_CLEANUP_INTERVAL  (SNOOP_DEF_PORT_PURGE_INTERVAL/10)        
        
#define   SNOOP_DEF_IN_GRP_SRC_INTERVAL    300
        
#define   SNOOP_MIN_QUERY_INTERVAL         60
#define   SNOOP_MAX_QUERY_INTERVAL         600
#define   SNOOP_MIN_STARTUP_QRY_INTERVAL   15
#define   SNOOP_MAX_STARTUP_QRY_INTERVAL   150
#define   SNOOP_IGS_MIN_OTHER_QPRESENT_INTERVAL 120
#define   SNOOP_IGS_MAX_OTHER_QPRESENT_INTERVAL 1215
#define   SNOOP_MLDS_MIN_OTHER_QPRESENT_INTERVAL 120
#define   SNOOP_MLDS_MAX_OTHER_QPRESENT_INTERVAL 1235
#define   SNOOP_DEF_QUERY_INTERVAL         125
#define   SNOOP_STARTUP_QRY_INTERVAL       60
#define   SNOOP_DEF_STARTUP_QINTERVAL      (SNOOP_DEF_QUERY_INTERVAL/4)
#define   SNOOP_ROB_VARIABLE               2
#define   SNOOP_MIN_STARTUP_QRY_COUNT      SNOOP_ROB_VARIABLE
#define   SNOOP_MAX_STARTUP_QRY_COUNT      5       
#define   SNOOP_DEF_STARTUP_QRY_COUNT    SNOOP_ROB_VARIABLE
#define   SNOOP_OTHER_QUERIER_PRESENT_INTERVAL ((SNOOP_ROB_VARIABLE) * \
                                                (SNOOP_DEF_QUERY_INTERVAL) + \
                                         ((SNOOP_MAX_RESP_TIME)/2))

#define   SNOOP_QUERIER_RTR_PORT           1
#define   SNOOP_NON_QUERIER_RTR_PORT       2
#define   SNOOP_QUERY_INTERVAL_CODE        128

#define   SNOOP_QUERIER_IP_FLAG_ENABLE    1
#define   SNOOP_QUERIER_IP_FLAG_DISABLE    2

#define   SNOOP_IGS_MIN_MAX_RESP_CODE      1 
#define   SNOOP_IGS_MAX_MAX_RESP_CODE      255
#define   SNOOP_IGS_DEF_MAX_RESP_CODE      100
#define   SNOOP_IGS_MRC_UNIT               10
         
#define   SNOOP_MLDS_MIN_MAX_RESP_CODE     0 
#define   SNOOP_MLDS_MAX_MAX_RESP_CODE     65025
#define   SNOOP_MLDS_DEF_MAX_RESP_CODE     10000
 
#define   SNOOP_MLDS_MAX_RESP_CODE         32768
#define   SNOOP_MLDS_MRC_UNIT              1000
          
#define   SNOOP_MIN_RTR_PURGE_INTERVAL     60 
#define   SNOOP_MAX_RTR_PURGE_INTERVAL     600
         
#define   SNOOP_MIN_PORT_PURGE_INTERVAL    130 
#define   SNOOP_MAX_PORT_PURGE_INTERVAL    1225
#define   SNOOP_DEF_PORT_PURGE_INTERVAL    260 /* 260 */

#define   SNOOP_MIN_REPORT_FWD_INTERVAL    1 
#define   SNOOP_MAX_REPORT_FWD_INTERVAL    25 
#define   SNOOP_DEF_REPORT_FWD_INTERVAL    5

#define   SNOOP_MIN_GRP_QUERY_INTERVAL     2 
#define   SNOOP_MAX_GRP_QUERY_INTERVAL     5 
#define   SNOOP_DEF_GRP_QUERY_INTERVAL     2
#define   SNOOP_IGS_V1_HOST_INTERVAL       10

#define   SNOOP_MIN_RETRY_COUNT            1 
#define   SNOOP_MAX_RETRY_COUNT            5 
#define   SNOOP_DEF_RETRY_COUNT            2
       
#define   SNOOP_FIRST_HALF                 1 
#define   SNOOP_SECOND_HALF                2 
#define   SNOOP_HOST_TIMER_EXPIRY          8 
                                   
#define   SNOOP_IP_FWD_ENTRY_TIMER_VAL     100
#define   SNOOP_IP_FWD_ENTRY_TIMER_LOW_VAL 5          

#define   SNOOP_FORWARD_ALL_PORTS          1 
#define   SNOOP_FORWARD_NON_RTR_PORTS      2 

#define   SNOOP_FORWARD_RTR_PORTS          2 
#define   SNOOP_FORWARD_NONEDGE_PORTS      3

#define   SNOOP_FORWARD_NON_RTR_PORTS      2

#define   SNOOP_MAX_RESP_TIME              10
#define   SNOOP_IGS_MAX_PKT_SIZE           1500 - \
          (SNOOP_IP_HDR_RTR_OPT_LEN+ SNOOP_DATA_LINK_HDR_LEN + \
                          (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) + \
                          SNOOP_IGMP_HEADER_LEN + 4)  /* 24 + 14 + 8 + 8 + 4 */         
#define   SNOOP_MLDS_MAX_PKT_SIZE          1500 - \
          (SNOOP_IPV6_HDR_RTR_OPT_LEN + SNOOP_DATA_LINK_HDR_LEN + \
                        (SNOOP_MAX_VLAN_TAGS * SNOOP_VLAN_TAG_PID_LEN) + \
                        SNOOP_MLDV2_HDR_LEN + 4 + 16) /* 48 + 14 + 8 + 8 + 4 + 16 */
#define   SNOOP_MAX_Q_MSGS                 40
#define   SNOOP_MAX_RM_MSGS                40

#define   SNOOP_GRP_STATIC            1
#define   SNOOP_GRP_DYNAMIC            2

/* Utility Defintions */
#define   SNOOP_CREATE_FWD_ENTRY           1
#define   SNOOP_DELETE_FWD_ENTRY           2
#define   SNOOP_ADD_PORT                   3
#define   SNOOP_DEL_PORT                   4
#define   SNOOP_DISABLE_PORT               5

#define   SNOOP_ADD_LA_PORT                6
#define   SNOOP_DEL_LA_PORT                7

#define   SNOOP_ADD_PORTLIST               8

#define   SNOOP_CREATE_GRP_ENTRY           1
#define   SNOOP_DELETE_GRP_ENTRY           2

#define   SNOOP_TMP_ARRAY_SIZE             32
#define   SNOOP_INVALID_PORT               0

#define   SNOOP_REPORT_FORWARD             1
#define   SNOOP_REPORT_NO_FORWARD          2

#define   SNOOP_QUERIER                    1
#define   SNOOP_NON_QUERIER                2

#define   SNOOP_INCLUDE               1
#define   SNOOP_EXCLUDE               2

#define   SNOOP_VLAN_HIGHEST_PRIORITY      7

#define   SNOOP_TRC_NAME_SIZE              44 /* SNOOP_INSTANCE_ALIAS_LEN + 
                                               *  12 characters for type 
                                               *  of message */
#define   SNOOP_MAX_UINT4_VALUE            0xffffffff

#define SNOOP_ACTIVE                      ACTIVE
#define SNOOP_NOT_IN_SERVICE              NOT_IN_SERVICE 
#define SNOOP_NOT_READY                   NOT_READY
#define SNOOP_CREATE_AND_GO               CREATE_AND_GO
#define SNOOP_CREATE_AND_WAIT             CREATE_AND_WAIT
#define SNOOP_DESTROY                     DESTROY


#define   SNOOP_AND_PORT_BITMAP            1
#define   SNOOP_CREATE                     1
#define   SNOOP_SET_ACTIVE                 2
#define   SNOOP_DELETE                     3
#define   SNOOP_RELEASE_BUFFER             1

#define   SNOOP_SUMMARIZE_FWD_TABLE        1
#define   SNOOP_SAME_HOST                  1


/* Packet Type Definitions */          
#define   SNOOP_ROUTER_MSG                 3
#define   SNOOP_DATA_PKT                   4
#define   SNOOP_IGMP_V1REPORT              0x12
#define   SNOOP_IGMP_V2REPORT              0x16
#define   SNOOP_IGMP_V3REPORT              0x22
#define   SNOOP_IGMP_LEAVE                 0x17
#define   SNOOP_IGMP_QUERY                 0x11
#define   SNOOP_IGMP_PROXY_QUERY           0
#define   SNOOP_MLD_QUERY                  0x82      
#define   SNOOP_MLD_V1REPORT               0x83
#define   SNOOP_MLD_DONE                   0x84
#define   SNOOP_MLD_V2REPORT               0x8f

/* MLDS_CHANGES */
#define   SNOOP_IP6_HDR_LEN                 sizeof(tIp6Hdr)
#define   SNOOP_MLD_PROTOCOL               0x81 /* non standard number */
#define   SNOOP_IPV6_PKT_TYPE                0x86DD 
#define   SNOOP_IP6_TYPE_LEN_BYTES                 2
#define   SNOOP_MLDS_PAD1_OPTION               0
#define   SNOOP_MLDS_PADN_OPTION               1
#define   SNOOP_MLDS_ROUTE_ALERT_OPT           5
#define   SNOOP_MLDS_JMB_OPT_TYPE              0xc2  
#define   SNOOP_MLDS_JMB_MIN_PAYLOAD_LEN       65536 
#define   SNOOP_MLDS_JMB_OPT_TYPE_OFFSET       43         
#define   SNOOP_MLDS_JMB_OPT_LEN_OFFSET        44    
#define   SNOOP_BYTES_IN_OCTECT                  8 
#define   SNOOP_MLDS_DEST_HDR_OPTION_00               0x3f
#define   SNOOP_MLDS_DEST_HDR_OPTION_01               0x7f
#define   SNOOP_MLDS_DEST_HDR_OPTION_10               0xbf
#define   SNOOP_MLDS_DEST_HDR_OPTION_11               0xff
#define  SNOOP_MLDS_OFFSET_FOR_PAYLOAD_LEN        4                     
#define  SNOOP_MLDS_ICMP6_PKT_PARAM_PROBLEM      4   
#define  SNOOP_MLDS_ICMP6_HDR_PROB               0 
#define  SNOOP_MLDS_JMB_HDR_LEN          8     
#define  SNOOP_MLDS_ICMP6_MLD_PKT       0
#define   SNOOP_INVALID_MLD_PKT           0 
#define   SNOOP_MLD_HDR_LEN               24
#define   SNOOP_MLDV2_HDR_LEN            8 
#define   SNOOP_MLDV2_QRY_HDR            4

#define  SNOOP_IPV6_AUTH_HDR          51
#define  SNOOP_AH_FIXED_PART_LEN      8 

#define   SNOOP_IS_INCLUDE            1
#define   SNOOP_IS_EXCLUDE            2
#define   SNOOP_TO_INCLUDE            3
#define   SNOOP_TO_EXCLUDE            4
#define   SNOOP_ALLOW                 5
#define   SNOOP_BLOCK                 6
#define   SNOOP_TO_EXCLUDE_NONE       7

#define   SNOOP_INVALID_PKT_TYPE           0xff

#define   SNOOP_GENERAL_QUERY         11
#define   SNOOP_GRP_QUERY             12
#define   SNOOP_GRP_SRC_QUERY         13

#define SNOOP_DBG_TIME_LEN      30         
/* Router Protocol Definitions */        
#define   SNOOP_PIM_CTRL_MSG               103
#define   SNOOP_OSPF_CTRL_MSG              89
#define   SNOOP_OSPF_VERSION_3             3

#define   SNOOP_PIM_VERSION2               0x20
#define   SNOOP_PIMV2_HELLO                0x0

#define   SNOOP_OSPF_HELLO                 0x0001
#define   SNOOP_DVMRP_CTRL_MSG             0x13
                                   
/* packet Header Definitions */          
#define   SNOOP_DATA_LINK_HDR_LEN          14 
#define   SNOOP_IP_HEADER_LEN              20 
#define   SNOOP_IP_HDR_RTR_OPT_LEN         24 
#define   SNOOP_IGMP_PACKET_START_OFFSET   \
                           (SNOOP_DATA_LINK_HDR_LEN + SNOOP_IP_HDR_RTR_OPT_LEN)
#define   SNOOP_IGMP_HEADER_LEN            8 

#define   SNOOP_NON_IP_PKT                 0 
#define   SNOOP_IGMP_SSMGROUP_RECD_SIZE     8 
#define   SNOOP_IGMP_SSMSOURCE_RECD_SIZE    4 
#define   SNOOP_MLD_SSMGROUP_RECD_SIZE     20 
#define   SNOOP_MLD_SSMSOURCE_RECD_SIZE    16 
#define   SNOOP_ETH_ADDR_SIZE              6 
#define   SNOOP_IP_VERS_AND_HLEN           0x45        
#define   SNOOP_IP_RTR_OPT_VERS_AND_HLEN   0x46        
#define   SNOOP_IP_DF_BIT                  0x0001 /* Set DF bit */
#define   SNOOP_IP_DF                      0x4000
#define   SNOOP_IGMP_PACKET_TTL            1
#define   SNOOP_ETH_TYPE_OFFSET            12
#define   SNOOP_ETH_TYPE_SIZE              2
#define   SNOOP_ICMP_TYPE_OFFSET           54  /* to denote the ICMP pkt type offset location in the buffer */
#define   SNOOP_INVALID_IGMP_PKT           0 
#define   SNOOP_MAC_ADDR_LEN               6

#define   SNOOP_IGMP_TOS                   0xc0
#define   SNOOP_IGMP_CHKSUM_OFFSET         SNOOP_DATA_LINK_HDR_LEN + 10
#define   SNOOP_IGMPV3_QRY_HDR             4
        

/* Address Range definitons */                                   
#define   SNOOP_ALL_ROUTER_IP              0xE0000002
#define   SNOOP_ALL_HOST_IP                0xE0000001
#define   SNOOP_ALL_PIM_ROUTERS            0xE000000D
#define   SNOOP_ALL_OSPF_ROUTERS           0xE0000005
#define   SNOOP_ALL_DVMRP_ROUTERS          0xE0000004
#define   SNOOP_ALL_IGMPV3_ROUTERS         0xE0000016
#define   SNOOP_RESVD_MCAST_ADDR_RANGE_MIN 0xE0000003
#define   SNOOP_RESVD_MCAST_ADDR_RANGE_MAX 0xE00000FF
#define   SNOOP_VALID_MCAST_IP_RANGE_MIN   0xE0000100
#define   SNOOP_VALID_MCAST_IP_RANGE_MAX   0xEFFFFFFF

#define   SNOOP_CLI_INVALID_CONTEXT   0xFFFFFFFF
#define SNOOP_SWITCH_ALIAS_LEN      L2IWF_CONTEXT_ALIAS_LEN

/* VLAN Statistics definitions */          
enum {
SNOOP_ASMREPORT_SENT = 1,
SNOOP_SSMREPORT_SENT,
SNOOP_SSMREPORT_RCVD,
SNOOP_QUERY_SENT, 
SNOOP_GRP_QUERY_SENT,
SNOOP_GRP_QUERY_RCVD,
SNOOP_IS_INCLUDE_RCVD,
SNOOP_IS_EXCLUDE_RCVD,
SNOOP_TO_INCLUDE_RCVD,
SNOOP_TO_EXCLUDE_RCVD,
SNOOP_ALLOW_RCVD,    
SNOOP_BLOCK_RCVD,   
SNOOP_GRP_SRC_QUERY_SENT,
SNOOP_GRP_SRC_QUERY_RCVD,
SNOOP_ASMREPORT_RCVD, 
SNOOP_QUERY_RCVD,
SNOOP_LEAVE_SENT,
SNOOP_LEAVE_RCVD,
SNOOP_UNSUCCESSFUL_JOINS,
SNOOP_ACTIVE_JOINS,
SNOOP_ACTIVE_GRPS,
SNOOP_PKT_ERROR,
SNOOP_V1REPORT_RCVD,                                                                                                      
SNOOP_V2REPORT_RCVD,                                                                                                      SNOOP_V1REPORT_SENT,                                                                                                      SNOOP_V2REPORT_SENT,                                                                                                      SNOOP_V1QUERY_DROPPED,
SNOOP_V2QUERY_DROPPED,                                                                                                    
SNOOP_V3QUERY_DROPPED,
SNOOP_V2GRP_QUERY_DROPPED,                                                                                                
SNOOP_V3GRP_QUERY_DROPPED,
SNOOP_GRP_SRC_QUERY_DROPPED,                                                                                              
SNOOP_V1REPORT_DROPPED,
SNOOP_V2REPORT_DROPPED,
SNOOP_IS_INCLUDE_DROPPED,
SNOOP_IS_EXCLUDE_DROPPED,
SNOOP_TO_INCLUDE_DROPPED,
SNOOP_TO_EXCLUDE_DROPPED,
SNOOP_ALLOW_DROPPED,
SNOOP_BLOCK_DROPPED,
SNOOP_LEAVE_DROPPED,
SNOOP_GEN_QUERY_FWD, 
SNOOP_GRP_QUERY_FWD,
SNOOP_MAX_STATS_VLAN_INFO
};

#define  SNOOP_IP_ADDR_SIZE   4
#define  SNOOP_V3_QRV_OFFSET  0
#define  SNOOP_V3_QQIC_OFFSET 1
#define  SNOOP_SRC_CNT_OFFSET 2
#define  SNOOP_OFFSET_TWO     2
#define  SNOOP_OFFSET_ONE     1

#define SNOOP_DENY            0
#define SNOOP_PERMIT          1

#define SNOOP_INFO_IGS_ENTRY 0
#define SNOOP_INFO_MLDS_ENTRY 1


#define SNOOP_IPV6_HDR_RTR_OPT_LEN       48
#define SNOOP_MLD_PACKET_START_OFFSET  (SNOOP_DATA_LINK_HDR_LEN + SNOOP_IPV6_HDR_RTR_OPT_LEN)
#define SNOOP_MLD_NH_ICMPV6 58
#define SNOOP_MLD_NH_HOP_BY_HOP 0
#define SNOOP_MLD_HOP_BY_HOP_HDR_LEN 8
#define SNOOP_MLD_HOP_BY_HOP_OPT_VAL 5
#define SNOOP_MLD_HOP_BY_HOP_OPT_LEN 2

#define MLD_MAX_RESP_TIME      1000
#define SNOOP_IPV6_HDR_VERSION  CRU_HTONL(0x60000000)

#define  SNOOP_FILTER_PKT_NOTIFY 1
#define  SNOOP_FWD_ENTRY_AGEOUT_NOTIFY    2

#define SNOOP_HARDWARE_CREATE 0
#define SNOOP_HARDWARE_DELETE 1
#define SNOOP_HARDWARE_PORT_ADD 2
#define SNOOP_HARDWARE_PORT_DELETE 3

/* Host type definitions */
typedef enum {
    SNOOP_ASM_V1HOST = 1,
    SNOOP_ASM_V2HOST,
    SNOOP_SSM_HOST
} tSnoopHostType;

/* Leave configuration level */
#define SNOOP_VLAN_LEAVE_CONFIG      1
#define SNOOP_PORT_LEAVE_CONFIG      2

#define SNOOP_NON_ROUTER_REPORT_CONFIG      1
#define SNOOP_ALL_REPORT_CONFIG             2

/* Leave configuration mode */
#define SNOOP_LEAVE_EXPLICIT_TRACK   1
#define SNOOP_LEAVE_FAST_LEAVE       2
#define SNOOP_LEAVE_NORMAL_LEAVE     3

#define  SNOOP_CONVERT_IPADDR_TO_STR(pString, u4Value)\
{\
    tUtlInAddr          IpAddr;\
\
         IpAddr.u4Addr = u4Value;\
\
         pString = (CHR1 *)UtlInetNtoa (IpAddr);\
\
}

/* Macro added to check for the physical or link aggreagation or 
 * pseudo wire interface. */
#define SNOOP_IFINDEX_IS_VALID_IFACE(u4Port) \
        CFA_IFINDEX_IS_VALID_IFACE(u4Port)

/* Added for Attachment Circuit inteface */
#define SNOOP_IFINDEX_IS_VALID_IFACE(u4Port) CFA_IFINDEX_IS_VALID_IFACE(u4Port)

/* End:  IPvX original macros */

#define  SNOOP_IPV6_THIRD_WORD_OFFSET   12

#define   SNOOP_NON_SSM_RANGE                     2
#define   SNOOP_SSM_RANGE                         1
#define   SNOOP_START_OF_SSM_ONLY_GRP_ADDR        0xe8000000
#define   SNOOP_END_OF_SSM_ONLY_GRP_ADDR          0xe8ffffff


/*Macro to check whether the group lies in SSM range*/
#define SNOOP_CHK_IF_SSM_RANGE(u4GrpAddr, retVal)\
{ \
   retVal = SNOOP_NON_SSM_RANGE;\
   if((u4GrpAddr > SNOOP_START_OF_SSM_ONLY_GRP_ADDR)\
        && (u4GrpAddr <= SNOOP_END_OF_SSM_ONLY_GRP_ADDR))\
    {\
        retVal = SNOOP_SSM_RANGE;\
     }\
}


#endif /* _SNPDEFN_H */
