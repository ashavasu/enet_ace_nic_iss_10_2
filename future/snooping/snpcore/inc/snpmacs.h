#ifndef _SNPMACS_H
#define _SNPMACS_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: snpmacs.h,v 1.51 2017/11/24 12:41:03 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpmacs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Macros                     */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains macro definitions           */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
#define   SNOOP_DEFAULT_INSTANCE          L2IWF_DEFAULT_CONTEXT
#define   SNOOP_SYSTEM_STATUS(u4Instance) gu1SnoopSystemStatus [u4Instance]
#define   SNOOP_GET_INSTANCE_ID(u4InstId) u4InstId
#define   SNOOP_INSTANCE_INFO(u4Instance)  gapSnoopGlobalInstInfo[u4Instance]

#define   SNOOP_INVALID_MAXGRP_ENTRY (SNOOP_MAX_MCAST_GROUPS+1)
#define  SNOOP_GLOBAL_PORT(u4PhyPort) gaSnoopPortInstanceMap[u4PhyPort -1].u2LocalPort
#define  SNOOP_GLOBAL_INSTANCE(u4PhyPort) gaSnoopPortInstanceMap[u4PhyPort -1].u4Instance

#define   SNOOP_GRP_ENTRY_COUNT(u4Port) gaGroupEntryStatus[u4Port].u2GrpEntryCount
#define   SNOOP_MAX_GRP_ENTRY(u4Port) gaGroupEntryStatus[u4Port].u2MaxGrpEntry
#define SNOOP_GET_PHY_PORT(u4SispIndex)  \
    gaSnoopPortInstanceMap[u4SispIndex].u4PhyPort

#define  SNOOP_GET_IFINDEX(u4Instance, u2LocalPort)\
 SNOOP_INSTANCE_INFO (u4Instance)->au4SnoopPhyPort[u2LocalPort - 1]

#define SNOOP_SRC_INFO(u4Inst, SrcIndex)\
        SNOOP_INSTANCE_INFO (u4Inst)->SnoopSrcInfo[SrcIndex]

#define SNOOP_PROTOCOL_ADMIN_STATUS(u4InstId, u1AddrIndex) \
        SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1SnoopStatus

#define SNOOP_OUTER_VLAN(VlanId) VlanId[0]
#define SNOOP_INNER_VLAN(VlanId) VlanId[1]

#define SNOOP_ROBUST_MIN_VALUE 2
#define SNOOP_ROBUST_MAX_VALUE 7

#define SNOOP_VLAN_TAGGED       VLAN_TAGGED
#define SNOOP_VLAN_UNTAGGED     VLAN_UNTAGGED
#define SNOOP_PRIORITY_UNTAGGED VLAN_PRIORITY_TAGGED

/* Macros needed for Snooping redundancy module. */
#ifdef L2RED_WANTED

#define SNOOP_RED_GENERAL_QUERY_INPROGRESS    gu1SnoopRedGenQueryInProgress
#define SNOOP_RED_BULK_SYNC_INPROGRESS        gu1SnoopRedBulkSyncInProgress
#define SNOOP_RED_AUDIT_INPROGRESS        gu1SnoopRedAuditInProgress
#define SNOOP_NODE_STATUS()   gSnoopNodeStatus
#define SNOOP_IS_NP_PROGRAMMING_ALLOWED() \
        ((L2RED_IS_NP_PROGRAMMING_ALLOWED () == OSIX_TRUE) ? SNOOP_TRUE: SNOOP_FALSE)

#else
        
   #define SNOOP_NODE_STATUS()   gSnoopNodeStatus
   #define SNOOP_IS_NP_PROGRAMMING_ALLOWED()   (SNOOP_TRUE)
        
#endif

#define   SNOOP_SNOOPING_STATUS(u4InstId, u1AddrIndex) \
          SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1SnoopOperStatus

#define   SNOOP_SNOOPING_STATUS_IN_CONTEXT(u4InstId, u1AddrIndex) \
         (SNOOP_INSTANCE_INFO (u4InstId) == NULL)?SNOOP_DISABLE:SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1SnoopOperStatus

#define   SNOOP_PROXY_STATUS(u4InstId, u1AddrIndex) \
         ((SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1ProxyStatus\
          == SNOOP_PROXY_MODE)?SNOOP_ENABLE:SNOOP_DISABLE)
   
#define   SNOOP_GET_QQI_FROM_QQIC(u1QQIC)\
         ((u1QQIC < SNOOP_QUERY_INTERVAL_CODE)?u1QQIC:( ((u1QQIC & 0x0F) | 0x10) << (((u1QQIC & 0x70) >> 4) + 3)))

#define   SNOOP_IGS_GET_TIME_FROM_CODE(Code) \
    ((Code < SNOOP_QUERY_INTERVAL_CODE)?(Code):(((Code & 0x0F) | 0x10) << (((Code & 0x70) >> 4)+ 3)))

#define   SNOOP_MLDS_GET_TIME_FROM_CODE(Code) \
    ((Code < SNOOP_MLDS_MAX_RESP_CODE)?(Code):(((Code & 0x0FFF) | 0x1000) << (((Code & 0x7000) >> 12)+ 3)))


#define  SNOOP_GET_QRV(u1QRV) (u1QRV & 0x7)
#define SNOOP_GET_CODE_FROM_TIME(Code, Time) \
{                                                               \
   UINT1   u1Code = SNOOP_ZERO;                                  \
   UINT2   u2CodeVal = SNOOP_ZERO;                               \
   for(u1Code = 0x80; u1Code < 0xff; u1Code++)                  \
   {                                                            \
        u2CodeVal = (UINT2) SNOOP_IGS_GET_TIME_FROM_CODE(u1Code);      \
        if (u2CodeVal >= Time)                                  \
        {                                                       \
           Code = u1Code;                                       \
           break;                                               \
        }                                                       \
   }                                                            \
   if (u2CodeVal < Time)                                        \
   {                                                            \
        Code = u1Code;                                          \
   }                                                            \
}

#define SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED(u4InstId, u1AddrIndex) \
      (((SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1ProxyStatus) \
            == SNOOP_DISABLE) ? SNOOP_FALSE : SNOOP_TRUE)


#define   SNOOP_PROXY_REPORTING_STATUS(u4InstId, u1AddrIndex) \
   ((SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1ProxyStatus \
    == SNOOP_PROXY_REPORTING_MODE)?SNOOP_ENABLE:SNOOP_DISABLE)
   
#define   SNOOP_SET_PROXY_REPORTING_STATUS(u4InstId, u1AddrIndex) \
   SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1ProxyStatus

#define   SNOOP_SET_PROXY_STATUS(u4InstId, u1AddrIndex) \
   SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1ProxyStatus 


#define   SNOOP_INSTANCE_STR(u4InstId) \
          SNOOP_INSTANCE_INFO (u4InstId)->au1ContextStr

#define   SNOOP_TMR_LIST_ID               gSnoopTimerListId 

/* Task, Queue and Event related macros, OSIX related macros */
#define   SNOOP_TASK_ID                  gSnoopTaskId
#ifdef L2RED_WANTED
#define   SNOOP_AUDIT_TASK_ID            gSnoopAuditTaskId
#endif
#define   SNOOP_MSG_QUEUE_ID             gSnoopMsqQId
#define   SNOOP_PKT_QUEUE_ID             gSnoopPktQId
#define   SNOOP_GET_TASK_ID              OsixTskIdSelf
#define   SNOOP_SEND_EVENT               OsixEvtSend
#define   SNOOP_RECEIVE_EVENT            OsixEvtRecv
#define   SNOOP_CREATE_QUEUE             OsixQueCrt
#define   SNOOP_DELETE_QUEUE             OsixQueDel
#define   SNOOP_SEND_TO_QUEUE            OsixQueSend
#define   SNOOP_RECV_FROM_QUEUE          OsixQueRecv
#define   SNOOP_CREATE_SEM               OsixCreateSem
#define   SNOOP_DELETE_SEM               OsixSemDel
#define   SNOOP_TAKE_SEM                 OsixSemTake
#define   SNOOP_GIVE_SEM                 OsixSemGive
#define   SNOOP_DEF_MSG_LEN              OSIX_DEF_MSG_LEN
#define   SNOOP_MAX_NUM_OF_MSG_PROC  10
/*Snoop port action indication*/
#define   SNOOP_PORT_ACTION             gu1SnoopPortAction
   
/* FSAP SRM - Memory related definitions */
#define   SNOOP_CREATE_MEM_POOL          MemCreateMemPool
#define   SNOOP_DELETE_MEM_POOL(PoolId)  \
          if (PoolId != 0) (VOID) MemDeleteMemPool(PoolId)

       
/* Memory Pool Information for Queue Messages */        
#define  SNOOP_Q_MSG_MEMBLK_SIZE    sizeof(tSnoopMsgNode)
#define  SNOOP_Q_MSG_MEMBLK_COUNT   SNOOP_MSG_QUEUE_DEPTH 
#define  SNOOP_QMSG_MEMPOOL_ID      gSnoopGlobalPoolId.QMsgMemPoolId
#define  SNOOP_GINSTANCE_MEMPOOL_ID      gSnoopGlobalPoolId.SnoopGblInstPoolId

/* Memory Pool Information for Packets in Queue to be 
 * processed by snooping */       
#define  SNOOP_Q_PKT_MEMBLK_SIZE    sizeof(tSnoopQMsg)
#define  SNOOP_Q_PKT_MEMBLK_COUNT   SNOOP_PKT_QUEUE_DEPTH  
#define  SNOOP_QPKT_MEMPOOL_ID      gSnoopGlobalPoolId.QPktMemPoolId

    
/* Misc mem block for forming and sending SSM report */
#define  SNOOP_MISC_MEMBLK_SIZE     1500
#define  SNOOP_MISC_MEMBLK_COUNT    1  
#define  SNOOP_MISC_MEMPOOL_ID      gSnoopGlobalPoolId.MiscMemPoolId
        
#define  SNOOP_SRC_MEMBLK_SIZE     1500
#define  SNOOP_SRC_MEMBLK_COUNT    1  
#define  SNOOP_SRC_MEMPOOL_ID      gSnoopGlobalPoolId.SrcMemPoolId

/* Data mem block for forming and sending Data packet */
#define  SNOOP_DATA_MEMBLK_SIZE     1500
#define  SNOOP_DATA_MEMBLK_COUNT    1  
#define  SNOOP_DATA_MEMPOOL_ID      gSnoopGlobalPoolId.DataMemPoolId
        
/* Snooping Memory Pool related macros for per instance */
      
/* Snooping Global memory pool information */
#define  SNOOP_GLOBAL_ENTRY_MEMBLK_SIZE    sizeof(tSnoopGlobalInstInfo)
#define  SNOOP_GLOBAL_ENTRY_MEMBLK_COUNT   SNOOP_MAX_INSTANCES
#define  SNOOP_GLOBAL_MEMPOOL_ID     \
        gSnoopGlobalPoolId.SnoopGblInstPoolId

/* Consolidated Group Membership memory pool information */
#define  SNOOP_VLAN_CONS_GRP_ENTRY_MEMBLK_SIZE \
         sizeof(tSnoopConsolidatedGroupEntry)
#define  SNOOP_VLAN_CONS_GRP_ENTRY_MEMBLK_COUNT  SNOOP_MAX_MCAST_GROUPS
#define  SNOOP_VLAN_CONS_GRP_ENTRY_POOL_ID \
         gSnoopGlobalPoolId.SnoopInstMemPool.ConsGrpEntryPoolId

/* Group Membership memory pool information */
#define  SNOOP_VLAN_GRP_ENTRY_MEMBLK_SIZE   sizeof(tSnoopGroupEntry)
#define  SNOOP_VLAN_GRP_ENTRY_MEMBLK_COUNT  SNOOP_MAX_MCAST_GROUPS
#define  SNOOP_VLAN_GRP_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.GrpEntryPoolId

/* PortRecord memory pool information */
#define  SNOOP_PORT_ENTRY_MEMBLK_SIZE   sizeof(tSnoopPortEntry)
#define  SNOOP_PORT_ENTRY_MEMBLK_COUNT \
         (SNOOP_MAX_PORTS_PER_INSTANCE * (SNOOP_MAX_MCAST_GROUPS / 2))
#define  SNOOP_PORT_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.PortEntryPoolId

/* Source Bitmap Node memory pool information */
#define  SNOOP_SRC_BMP_ENTRY_MEMBLK_SIZE   sizeof(tSnoopSrcBmpNode)
#define  SNOOP_SRC_BMP_ENTRY_MEMBLK_COUNT \
         (SNOOP_MAX_PORTS + SNOOP_MAX_HOSTS)
#define  SNOOP_SRC_BMP_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.SourceBmpPoolId

/* Host record memory pool information */
#define  SNOOP_HOST_ENTRY_MEMBLK_SIZE   sizeof(tSnoopHostEntry)
#define  SNOOP_HOST_ENTRY_MEMBLK_COUNT  SNOOP_MAX_HOSTS  
#define  SNOOP_HOST_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.HostEntryPoolId
         
/* VLAN record memory pool information */
#define  SNOOP_VLAN_ENTRY_MEMBLK_SIZE   sizeof(tSnoopVlanEntry)
#define  SNOOP_VLAN_ENTRY_MEMBLK_COUNT  SNOOP_VLAN_TABLE_SIZE 
#define  SNOOP_VLAN_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.VlanEntryPoolId

/* Snooping VLAN Statistics memory pool information */
#define  SNOOP_VLAN_STATS_MEMBLK_SIZE   sizeof(tSnoopVlanStatsEntry)
#define  SNOOP_VLAN_STATS_MEMBLK_COUNT  SNOOP_VLAN_STATS_SIZE  
#define  SNOOP_VLAN_STATS_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.VlanStatsPoolId
         
/* Configured VLAN memory pool information */
#define  SNOOP_VLAN_CFG_MEMBLK_SIZE   sizeof(tSnoopVlanCfgEntry)
#define  SNOOP_VLAN_CFG_MEMBLK_COUNT  SNOOP_VLAN_CFG_SIZE  
#define  SNOOP_VLAN_CFG_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.VlanCfgPoolId
         
/* Router port record memory pool information */
#define  SNOOP_RTR_PORT_ENTRY_MEMBLK_SIZE   sizeof(tSnoopRtrPortEntry)
#define  SNOOP_RTR_PORT_ENTRY_MEMBLK_COUNT  \
         (SNOOP_MAX_PORTS_PER_INSTANCE * (SNOOP_MAX_VLANIDS / 2))
#define  SNOOP_RTR_PORT_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.RtrPortEntryPoolId

/* MAC based forwarding memory pool information */
#define  SNOOP_MAC_FWD_ENTRY_MEMBLK_SIZE   sizeof(tSnoopMacGrpFwdEntry)
#define  SNOOP_MAC_FWD_ENTRY_MEMBLK_COUNT  SNOOP_MAX_MAC_FWD_ENTRIES
#define  SNOOP_MAC_FWD_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.MacFwdEntryPoolId

/* IP based forwarding memory pool information */
#define  SNOOP_IP_FWD_ENTRY_MEMBLK_SIZE   sizeof(tSnoopIpGrpFwdEntry)
#define  SNOOP_IP_FWD_ENTRY_MEMBLK_COUNT  SNOOP_MAX_IP_FWD_ENTRIES
#define  SNOOP_IP_FWD_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.IpFwdEntryPoolId

/* Mapped PVLAN List memory pool information */
#define  SNOOP_MAPPED_PVLAN_LIST_MEMBLK_SIZE  SNOOP_MAX_MAPPED_PVLANS
#define  SNOOP_MAPPED_PVLAN_LIST_MEMBLK_COUNT  SNOOP_MAX_MAPPED_PVLANS_BLOCKS
#define  SNOOP_PVLAN_LIST_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.MappedPvlanListPoolId

#ifdef L2RED_WANTED
#define  SNOOP_RED_HW_ENTRY_POOL_ID(u4InstId) \
         gSnoopGlobalPoolId.SnoopInstMemPool.RedMCastMemPoolId
#endif
/*  IPVx ADDR  memory pool information */
#define  SNOOP_IPVX_ADDR_LIST_MEMBLK_SIZE  sizeof(tIPvXAddrSource)
#define  SNOOP_IPVX_ADDR_LIST_MEMBLK_COUNT  SNOOP_MAX_IPVX_ADDR_BLOCKS
#define  SNOOP_IPVX_ADDR_POOL_ID \
         gSnoopGlobalPoolId.IpvxAddrSrcPoolId

#define SNOOP_PORT_CFG_GLB_RBTREE  gPortCfgTblEntry
#define SNOOP_PORT_CFG_INST_RBTREE(u4InstId) \
         SNOOP_INSTANCE_INFO (u4InstId)->PortCfgEntry

#define SNOOP_INSTANCE_ENH_MODE(u4InstId) \
           SNOOP_INSTANCE_INFO(u4InstId)->u1SystemEnhMode

#ifdef ICCH_WANTED
#define SNOOP_MCLAG_IP_HOST_OPTIMIZE_MODE(u4InstId) \
           SNOOP_INSTANCE_INFO(u4InstId)->u1MclagIpHostOptimize
#endif

#define SNOOP_PORT_CFG_EXT_ENTRY(pSnoopPortCfgEntry) \
           pSnoopPortCfgEntry->pSnoopExtPortCfgEntry
           

/* Port Configuration  memory pool information */
#define  SNOOP_PORT_CFG_ENTRY_MEMBLK_SIZE   sizeof(tSnoopPortCfgEntry)
#define  SNOOP_PORT_CFG_ENTRY_MEMBLK_COUNT  SNOOP_MAX_PORT_CFG_ENTRIES
#define  SNOOP_PORT_CFG_ENTRY_POOL_ID \
         gSnoopGlobalPoolId.PortCfgEntryPoolId


#define  SNOOP_PORT_CFG_ENTRY_ALLOC_MEMBLK(pu1PrtCfg) \
         (pu1PrtCfg = (tSnoopPortCfgEntry *)MemAllocMemBlk\
          (SNOOP_PORT_CFG_ENTRY_POOL_ID))

#define SNOOP_GET_FREE_UNITS_IN_POOL(PoolId) (MemGetFreeUnits (PoolId))

#define  SNOOP_PORT_CFG_ENTRY_FREE_MEMBLK(pu1PrtCfg)\
{\
         MemReleaseMemBlock(SNOOP_PORT_CFG_ENTRY_POOL_ID,(UINT1 *)pu1PrtCfg);\
         pu1PrtCfg = NULL;\
}

#define  SNOOP_PORT_EXT_CFG_ENTRY_MEMBLK_SIZE   sizeof(tSnoopExtPortCfgEntry)
#define  SNOOP_PORT_EXT_CFG_ENTRY_MEMBLK_COUNT  SNOOP_MAX_PORT_CFG_ENTRIES
#define  SNOOP_PORT_EXT_CFG_ENTRY_POOL_ID \
         gSnoopGlobalPoolId.ExtPortCfgEntryPoolId

#define  SNOOP_PORT_CFG_EXT_ENTRY_ALLOC_MEMBLK(pu1Prt) \
         (pu1Prt = (tSnoopExtPortCfgEntry*)MemAllocMemBlk\
          (SNOOP_PORT_EXT_CFG_ENTRY_POOL_ID))

#define  SNOOP_PORT_CFG_EXT_ENTRY_FREE_MEMBLK(pu1Prt)\
{\
         MemReleaseMemBlock(SNOOP_PORT_EXT_CFG_ENTRY_POOL_ID,(UINT1 *)pu1Prt);\
         pu1Prt= NULL;\
}

#define  SNOOP_MVLAN_MAPPING_POOLID(u4InstId)\
         gSnoopGlobalPoolId.SnoopInstMemPool.MVlanMappingPoolId

#define SNOOP_MVLAN_MAPPING_ENTRY_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tMVlanMapping *)MemAllocMemBlk\
          (SNOOP_MVLAN_MAPPING_POOLID(u4InstId)))

#define  SNOOP_MVLAN_MAPPING_ENTRY_FREE_MEMBLK(u4InstId, pu1Msg)\
{\
         MemReleaseMemBlock (SNOOP_MVLAN_MAPPING_POOLID(u4InstId), \
                              (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define SNOOP_MULTICAST_VLAN_STATUS(u4InstId, u1AddrIndex) \
        SNOOP_INSTANCE_INFO (u4InstId)->SnoopInfo[u1AddrIndex].u1MVlanStatus

#define  SNOOP_GRPSRC_RECORDS_POOLID \
         gSnoopGlobalPoolId.GrpSrcRecordsPoolId

#define SNOOP_GRPSRC_STATUS_ENTRY_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tGrpSrcStatusEntry *)MemAllocMemBlk\
          (SNOOP_GRPSRC_RECORDS_POOLID))

#define  SNOOP_GRPSRC_STATUS_ENTRY_FREE_MEMBLK(pu1Msg)\
{\
         MemReleaseMemBlock(SNOOP_GRPSRC_RECORDS_POOLID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define SNOOP_MVLAN_GRPSRC_REC_SIZE  sizeof(tGrpSrcStatusEntry)

/* Max pkt Size - 1522, Headers - 50 Bytes(14(Ethernet)+20(IP)
  +8(IGMP+8(IGMP Group Record Header Len), Src IP Size - 4
* SNOOP_MVLAN_GRPSRC_REC_COUNT  ~= (1522-50)/4*/
#define SNOOP_MVLAN_GRPSRC_REC_COUNT 370 /* Maximum number of Group & Source
                                            records, possible.*/

/* MEM Pool Block Allocation Macros */
#define  SNOOP_QMSG_ALLOC_MEMBLK MemAllocMemBlk(SNOOP_QMSG_MEMPOOL_ID)

#define  SNOOP_RELEASE_QMSG_MEM_BLOCK(pMsg) \
         MemReleaseMemBlock(SNOOP_QMSG_MEMPOOL_ID, (UINT1 *) pMsg)

#define  SNOOP_GINSTANCE_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tSnoopGlobalInstInfo *)\
          MemAllocMemBlk(SNOOP_GINSTANCE_MEMPOOL_ID))

#define  SNOOP_RELEASE_GINSTANCE_MEM_BLOCK(pMsg) \
         MemReleaseMemBlock(SNOOP_GINSTANCE_MEMPOOL_ID, (UINT1 *) pMsg)

#define  SNOOP_QPKT_ALLOC_MEMBLK MemAllocMemBlk(SNOOP_QPKT_MEMPOOL_ID)

#define  SNOOP_RELEASE_QPKT_MEM_BLOCK(pPkt) \
         MemReleaseMemBlock(SNOOP_QPKT_MEMPOOL_ID, (UINT1 *) pPkt)

#define  SNOOP_MISC_ALLOC_MEMBLK(pu1Misc) \
         (pu1Misc = MemAllocMemBlk(SNOOP_MISC_MEMPOOL_ID))

#define  SNOOP_RELEASE_MISC_MEM_BLOCK(pMisc) \
{\
         MemReleaseMemBlock(SNOOP_MISC_MEMPOOL_ID, (UINT1 *) pMisc);\
         pMisc = NULL;\
}

#define  SNOOP_SRC_ALLOC_MEMBLK(pu1Src) \
         (pu1Src = MemAllocMemBlk(SNOOP_SRC_MEMPOOL_ID))

#define  SNOOP_RELEASE_SRC_MEM_BLOCK(pSrc) \
{\
         MemReleaseMemBlock(SNOOP_SRC_MEMPOOL_ID, (UINT1 *) pSrc);\
         pSrc = NULL;\
}

#define  SNOOP_DATA_ALLOC_MEMBLK(pu1Data) \
         (pu1Data = MemAllocMemBlk(SNOOP_DATA_MEMPOOL_ID))

#define  SNOOP_RELEASE_DATA_MEM_BLOCK(pData) \
{\
         MemReleaseMemBlock(SNOOP_DATA_MEMPOOL_ID, (UINT1 *) pData);\
         pData = NULL;\
}

#define  SNOOP_VLAN_ENTRY_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopVlanEntry *)MemAllocMemBlk\
          (SNOOP_VLAN_ENTRY_POOL_ID(u4InstId)))
        
#define  SNOOP_VLAN_ENTRY_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_VLAN_ENTRY_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_RTR_PORT_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopRtrPortEntry *)\
          MemAllocMemBlk(SNOOP_RTR_PORT_POOL_ID(u4InstId)))
        
#define  SNOOP_VLAN_RTR_PORT_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_RTR_PORT_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_STATS_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopVlanStatsEntry *)\
          MemAllocMemBlk(SNOOP_VLAN_STATS_POOL_ID(u4InstId)))

#define  SNOOP_VLAN_STATS_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
       MemReleaseMemBlock(SNOOP_VLAN_STATS_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
       pu1Msg = NULL;\
}

#define  SNOOP_VLAN_CFG_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopVlanCfgEntry *)MemAllocMemBlk\
          (SNOOP_VLAN_CFG_POOL_ID(u4InstId)))

#define  SNOOP_VLAN_CFG_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_VLAN_CFG_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_MAC_FWD_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopMacGrpFwdEntry *)\
          MemAllocMemBlk(SNOOP_MAC_FWD_POOL_ID(u4InstId)))
        
#define  SNOOP_VLAN_MAC_FWD_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_MAC_FWD_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_IP_FWD_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopIpGrpFwdEntry *)\
          MemAllocMemBlk(SNOOP_IP_FWD_POOL_ID(u4InstId)))
        
#define  SNOOP_VLAN_IP_FWD_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_IP_FWD_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_IPVX_ADDR_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tIPvXAddrSource *)\
          MemAllocMemBlk(SNOOP_IPVX_ADDR_POOL_ID))
        
#define  SNOOP_IPVX_ADDR_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_IPVX_ADDR_POOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_CONS_GRP_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tSnoopConsolidatedGroupEntry *)\
          MemAllocMemBlk(SNOOP_VLAN_CONS_GRP_ENTRY_POOL_ID))
                                                             
#define  SNOOP_VLAN_CONS_GRP_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_VLAN_CONS_GRP_ENTRY_POOL_ID, \
                                                   (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_VLAN_GRP_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopGroupEntry *)\
          MemAllocMemBlk(SNOOP_VLAN_GRP_ENTRY_POOL_ID(u4InstId)))
                                                             
#define  SNOOP_VLAN_GRP_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         MemReleaseMemBlock(SNOOP_VLAN_GRP_ENTRY_POOL_ID(u4InstId), \
                                                   (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_ASM_PORT_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopPortEntry *)MemAllocMemBlk\
          (SNOOP_PORT_ENTRY_POOL_ID(u4InstId)))

#define  SNOOP_ASM_PORT_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         SNOOP_MEM_SET(pu1Msg, 0, sizeof(tSnoopPortEntry)); \
         MemReleaseMemBlock(SNOOP_PORT_ENTRY_POOL_ID(u4InstId), \
                                                    (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_SSM_PORT_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopPortEntry *)MemAllocMemBlk\
             (SNOOP_PORT_ENTRY_POOL_ID(u4InstId)))

#define  SNOOP_SSM_PORT_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         if (pu1Msg->pSourceBmp != NULL) \
         {\
             SNOOP_MEM_SET(pu1Msg->pSourceBmp, 0, sizeof(tSnoopSrcBmpNode)); \
             MemReleaseMemBlock(SNOOP_SRC_BMP_ENTRY_POOL_ID(u4InstId), \
                                (UINT1 *) pu1Msg->pSourceBmp); \
         }\
         SNOOP_MEM_SET(pu1Msg, 0, sizeof(tSnoopPortEntry)); \
         MemReleaseMemBlock(SNOOP_PORT_ENTRY_POOL_ID(u4InstId), \
                                                    (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_SSM_HOST_ENTRY_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tSnoopHostEntry *)\
             MemAllocMemBlk(SNOOP_HOST_ENTRY_POOL_ID(u4InstId)))

#define  SNOOP_SSM_HOST_ENTRY_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         if (pu1Msg->pSourceBmp != NULL) \
         {\
             SNOOP_MEM_SET(pu1Msg->pSourceBmp, 0, sizeof(tSnoopSrcBmpNode)); \
             MemReleaseMemBlock(SNOOP_SRC_BMP_ENTRY_POOL_ID(u4InstId), \
                                (UINT1 *) pu1Msg->pSourceBmp); \
         }\
         SNOOP_MEM_SET(pu1Msg, 0, sizeof(tSnoopHostEntry)); \
         MemReleaseMemBlock(SNOOP_HOST_ENTRY_POOL_ID(u4InstId),\
                                                  (UINT1 *) pu1Msg);\
         pu1Msg = NULL;\
}

#define  SNOOP_ASM_HOST_ENTRY_ALLOC_MEMBLK(u4InstId, pu1Msg) \
    (pu1Msg = (tSnoopHostEntry *)\
        MemAllocMemBlk(SNOOP_HOST_ENTRY_POOL_ID(u4InstId)))


#define  SNOOP_ASM_HOST_ENTRY_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
    SNOOP_MEM_SET(pu1Msg, 0, sizeof(tSnoopHostEntry)); \
    MemReleaseMemBlock(SNOOP_HOST_ENTRY_POOL_ID(u4InstId),\
                       (UINT1 *) pu1Msg);\
    pu1Msg = NULL;\
}

#define  SNOOP_SRC_BMP_ALLOC_MEMBLK(u4Instd, pu1Msg) \
      (pu1Msg = (tSnoopSrcBmpNode *)\
          MemAllocMemBlk(SNOOP_SRC_BMP_ENTRY_POOL_ID (u4Instance)))

#define  SNOOP_MAPPED_PVLAN_LIST_ALLOC_MEMBLK(u4InstId, pu1Msg) \
         (pu1Msg = (tVlanId *)\
          MemAllocMemBlk(SNOOP_PVLAN_LIST_POOL_ID(u4InstId)))
        
#define  SNOOP_MAPPED_PVLAN_LIST_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         if(pu1Msg != NULL)\
         {\
             SNOOP_MEM_SET(pu1Msg, 0, SNOOP_MAPPED_PVLAN_LIST_MEMBLK_SIZE); \
             MemReleaseMemBlock(SNOOP_PVLAN_LIST_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
             pu1Msg = NULL;\
         }\
}

#define SNOOP_RED_GRP_ALLOC_MEMBLK(u4InstId, pu1Msg) \
        (pu1Msg = (tSnoopRedMcastCacheNode *)\
         MemAllocMemBlk(SNOOP_RED_HW_ENTRY_POOL_ID(u4InstId)))

#define SNOOP_RED_GRP_FREE_MEMBLK(u4InstId, pu1Msg) \
{\
         if(pu1Msg != NULL)\
         {\
             SNOOP_MEM_SET(pu1Msg, 0, sizeof (tSnoopRedMcastCacheNode)); \
             MemReleaseMemBlock(SNOOP_RED_HW_ENTRY_POOL_ID(u4InstId), (UINT1 *)pu1Msg);\
             pu1Msg = NULL;\
         }\
}

#define SNOOP_HW_IP_FW_INFO_POOL_ID \
        gSnoopGlobalPoolId.SnoopInstMemPool.HwIpFwPoolId

#define SNOOP_HW_IP_ALLOC_MEMBLK(pu1Msg) \
      (pu1Msg = (tIgsHwIpFwdInfo*)\
        MemAllocMemBlk(SNOOP_HW_IP_FW_INFO_POOL_ID))

#define SNOOP_HW_IP_FREE_MEMBLK(pu1Msg) \
{ \
    if(pu1Msg != NULL)\
    {\
         SNOOP_MEM_SET(pu1Msg, 0, sizeof (tIgsHwIpFwdInfo)); \
         MemReleaseMemBlock(SNOOP_HW_IP_FW_INFO_POOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
    }\
}

/* UTL SLL related macros */
#define  SNOOP_SLL_ADD(pList, pNode) \
         TMO_SLL_Add((tSnoopSll *)pList, (tSnoopSllNode  *)pNode)\
         
#define  SNOOP_SLL_SCAN(pList, pNode, Typecast) \
         TMO_SLL_Scan((tSnoopSll *)pList, pNode, Typecast)
         
#define  SNOOP_DYN_SLL_SCAN(pList, pNode, pTempNode, Typecast) \
         TMO_DYN_SLL_Scan((tSnoopSll *)pList, pNode, pTempNode, Typecast)
        
#define  SNOOP_SLL_INIT(pList) \
         TMO_SLL_Init((tSnoopSll *)pList)
         
#define  SNOOP_SLL_FIRST(pList)\
         TMO_SLL_First((tSnoopSll *)pList)

#define  SNOOP_SLL_PREVIOUS(pList, pNode)\
         TMO_SLL_Previous((tSnoopSll *)pList, (tSnoopSllNode *) pNode)
        
#define  SNOOP_SLL_INSERT(pList, pPrev, pNode)\
         TMO_SLL_Insert((tSnoopSll *)pList, (tSnoopSllNode *) pPrev, \
                         (tSnoopSllNode *) pNode)
        
#define  SNOOP_SLL_NEXT(pList,pNode)  TMO_SLL_Next((tSnoopSll *)pList, pNode)

#define  SNOOP_SLL_DELETE(pList,pNode) \
         TMO_SLL_Delete((tSnoopSll *)pList, (tSnoopSllNode  *) (VOID *) pNode)

#define  SNOOP_SLL_COUNT(pList) \
         TMO_SLL_Count((tSnoopSll *)pList)

/* Threshold related Macros */
#define SNOOP_INCR_MEMBER_CNT(pEntry) \
        pEntry->u4SnoopPortCfgMemCnt++; 


#define SNOOP_DECR_MEMBER_CNT(pEntry) \
        pEntry->u4SnoopPortCfgMemCnt--; 
      
/* UTL DLL related macros */
#define  SNOOP_DLL_ADD(pList, pNode) \
         TMO_DLL_Add((tSnoopDll *)pList, (tSnoopDllNode  *)pNode)\
         
#define  SNOOP_DLL_DELETE(pList,pNode) \
         TMO_DLL_Delete((tSnoopDll *)pList, (tSnoopDllNode  *)(VOID *)pNode)

#define  SNOOP_UTL_DLL_INIT(pList, u4OffSet) \
         UTL_DLL_INIT((tSnoopDll *)pList, u4OffSet)
         
#define  SNOOP_DLL_INIT_NODE(pNode) \
         TMO_DLL_Init_Node((tSnoopDllNode *) (VOID *)pNode)
         
#define  SNOOP_DLL_COUNT(pList) \
         TMO_DLL_Count((tSnoopDll *)(VOID *)pList)

#define  SNOOP_UTL_DLL_OFFSET_SCAN(pList, pType, pTemp, TypeCast) \
         UTL_DLL_OFFSET_SCAN((tSnoopDll *)(VOID *)pList, pType, pTemp,TypeCast)

#define SNOOP_DLL_INIT(pList) TMO_DLL_Init((tSnoopDll *) pList)
         
#define  SNOOP_DLL_INSERT_IN_MIDDLE(pList, pPrev, pNode, pNext)\
         TMO_DLL_Insert_In_Middle((tSnoopDll *)pList, (tSnoopDllNode *) pPrev, \
                         (tSnoopDllNode *) pNode, (tSnoopDllNode *) pNext)
        
#define SNOOP_DLL_SCAN(pList, pNode, Typecast) \
         TMO_DLL_Scan((tSnoopDll *)(VOID *) pList, pNode, Typecast)

#define SNOOP_DLL_PREV(pList, pNode) \
         TMO_DLL_Previous((tSnoopDll *) pList, (tSnoopDllNode *) pNode)


#define  SNOOP_MCAST_FWD_MODE(u4InstId) \
           SNOOP_INSTANCE_INFO (u4InstId)->u1McastFwdMode
          
#define  SNOOP_MCAST_CONTROL_PLANE_DRIVEN(u4InstId) \
           SNOOP_INSTANCE_INFO (u4InstId)->u1ControlPlaneDriven

/* Statistics updation macros */
          
#define SNOOP_GEN_QUERY_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GenQueryRx++; \
        if (pSnoopStatsEntry->u4GenQueryRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GenQueryRx = 0; \
        } \
}

#define SNOOP_GEN_QUERY_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GenQueryTx++; \
        if (pSnoopStatsEntry->u4GenQueryTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GenQueryTx = 0; \
        } \
}

#define SNOOP_GRP_QUERY_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GrpQueryRx++; \
        if (pSnoopStatsEntry->u4GrpQueryRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GrpQueryRx = 0; \
        } \
}

#define SNOOP_GRP_QUERY_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GrpQueryTx++; \
        if (pSnoopStatsEntry->u4GrpQueryTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GrpQueryTx = 0; \
        } \
}

#define SNOOP_GRP_SRC_QUERY_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GrpAndSrcQueryRx++; \
        if (pSnoopStatsEntry->u4GrpAndSrcQueryRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GrpAndSrcQueryRx = 0; \
        } \
}

#define SNOOP_GRP_SRC_QUERY_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4GrpAndSrcQueryTx++; \
        if (pSnoopStatsEntry->u4GrpAndSrcQueryTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4GrpAndSrcQueryTx = 0; \
        } \
}

#define SNOOP_ASMREPORT_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4ASMReportRx++; \
        if (pSnoopStatsEntry->u4ASMReportRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4ASMReportRx = 0; \
        } \
}

#define SNOOP_ASMREPORT_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4ASMReportsTx++; \
        if (pSnoopStatsEntry->u4ASMReportsTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4ASMReportsTx = 0; \
        } \
}

#define SNOOP_SSMREPORT_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMReportRx++; \
        if (pSnoopStatsEntry->u4SSMReportRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMReportRx = 0; \
        } \
}

#define SNOOP_SSMREPORT_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMReportsTx++; \
        if (pSnoopStatsEntry->u4SSMReportsTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMReportsTx = 0; \
        } \
}

#define SNOOP_IS_INCL_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMIsInMsgRx++; \
        if (pSnoopStatsEntry->u4SSMIsInMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMIsInMsgRx = 0; \
        } \
}

#define SNOOP_IS_EXCL_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMIsExMsgRx++; \
        if (pSnoopStatsEntry->u4SSMIsExMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMIsExMsgRx = 0; \
        } \
}

#define SNOOP_TO_INCL_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMToInMsgRx++; \
        if (pSnoopStatsEntry->u4SSMToInMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMToInMsgRx = 0; \
        } \
}

#define SNOOP_TO_EXCL_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMToExMsgRx++; \
        if (pSnoopStatsEntry->u4SSMToExMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMToExMsgRx = 0; \
        } \
}

#define SNOOP_ALLOW_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMAllowMsgRx++; \
        if (pSnoopStatsEntry->u4SSMAllowMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMAllowMsgRx = 0; \
        } \
}

#define SNOOP_BLOCK_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4SSMBlockMsgRx++; \
        if (pSnoopStatsEntry->u4SSMBlockMsgRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4SSMBlockMsgRx = 0; \
        } \
}

#define SNOOP_LEAVE_RX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4LeaveRx++; \
        if (pSnoopStatsEntry->u4LeaveRx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4LeaveRx = 0; \
        } \
}

#define SNOOP_LEAVE_TX(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4LeaveTx++; \
        if (pSnoopStatsEntry->u4LeaveTx == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4LeaveTx = 0; \
        } \
}

#define SNOOP_PKT_DROPPED(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4PktsDropped++; \
        if (pSnoopStatsEntry->u4PktsDropped == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4PktsDropped = 0; \
        } \
}
#define SNOOP_INCR_ACTIVE_JOINS(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4ActiveJoins++; \
    if (pSnoopStatsEntry->u4ActiveJoins == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4ActiveJoins = 0; \
    }\
}
          
#define SNOOP_INCR_UNSUCCESSFUL_JOINS(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4UnSuccessfulJoins++; \
    if (pSnoopStatsEntry->u4UnSuccessfulJoins == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4UnSuccessfulJoins = 0; \
    }\
}
          
#define SNOOP_ACTIVE_GROUPS(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4ActiveGrps++; \
    if (pSnoopStatsEntry->u4ActiveGrps == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4ActiveGrps = 0; \
    }\
}

#define SNOOP_V1REPORT_RX(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v1ReportRx++; \
    if (pSnoopStatsEntry->u4v1ReportRx == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v1ReportRx = 0; \
    }\
}

#define SNOOP_V2REPORT_RX(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v2ReportRx++; \
    if (pSnoopStatsEntry->u4v2ReportRx == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v2ReportRx = 0; \
    }\
}

#define SNOOP_V1REPORT_TX(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v1ReportsTx++; \
    if (pSnoopStatsEntry->u4v1ReportsTx == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v1ReportsTx = 0; \
    }\
}

#define SNOOP_V2REPORT_TX(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v2ReportsTx++; \
    if (pSnoopStatsEntry->u4v2ReportsTx == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v2ReportsTx = 0; \
    }\
}


#define SNOOP_V1QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v1GenQueryDrop++; \
    if (pSnoopStatsEntry->u4v1GenQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v1GenQueryDrop = 0; \
    }\
}

#define SNOOP_V2QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v2GenQueryDrop++; \
    if (pSnoopStatsEntry->u4v2GenQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v2GenQueryDrop = 0; \
    }\
}

#define SNOOP_V3QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v3GenQueryDrop++; \
    if (pSnoopStatsEntry->u4v3GenQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v3GenQueryDrop = 0; \
    }\
}


#define SNOOP_V2GRP_QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v2GrpQueryDrop++; \
    if (pSnoopStatsEntry->u4v2GrpQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v2GrpQueryDrop = 0; \
    }\
}

#define SNOOP_V3GRP_QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v3GrpQueryDrop++; \
    if (pSnoopStatsEntry->u4v3GrpQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v3GrpQueryDrop = 0; \
    }\
}

#define SNOOP_GRP_SRC_QUERY_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4GrpAndSrcQueryDrop++; \
    if (pSnoopStatsEntry->u4GrpAndSrcQueryDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4GrpAndSrcQueryDrop = 0; \
    }\
}

#define SNOOP_V1REPORT_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v1ReportsDrop++; \
    if (pSnoopStatsEntry->u4v1ReportsDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v1ReportsDrop = 0; \
    }\
}

#define SNOOP_V2REPORT_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4v2ReportsDrop++; \
    if (pSnoopStatsEntry->u4v2ReportsDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4v2ReportsDrop = 0; \
    }\
}

#define SNOOP_IS_INCLUDE_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMIsInMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMIsInMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMIsInMsgDrop = 0; \
    }\
}

#define SNOOP_IS_EXCLUDE_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMIsExMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMIsExMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMIsExMsgDrop = 0; \
    }\
}

#define SNOOP_TO_INCLUDE_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMToInMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMToInMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMToInMsgDrop = 0; \
    }\
}

#define SNOOP_TO_EXCLUDE_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMToExMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMToExMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMToExMsgDrop = 0; \
    }\
}

#define SNOOP_ALLOW_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMAllowMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMAllowMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMAllowMsgDrop = 0; \
    }\
}

#define SNOOP_BLOCK_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4SSMBlockMsgDrop++; \
    if (pSnoopStatsEntry->u4SSMBlockMsgDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4SSMBlockMsgDrop = 0; \
    }\
}

#define SNOOP_LEAVE_DROPPED(pSnoopStatsEntry) \
{\
    pSnoopStatsEntry->u4LeaveDrop++; \
    if (pSnoopStatsEntry->u4LeaveDrop == 0xFFFFFFFF) \
    {\
        pSnoopStatsEntry->u4LeaveDrop = 0; \
    }\
}


#define SNOOP_IS_SWITCHOVER_IN_PROGRESS(u4Instance,u1AddressType,u1IsSwitchOverInProgress) \
{\
     if ((SNOOP_RED_SWITCHOVER_TMR_NODE ().u1TimerType\
          == SNOOP_RED_SWITCHOVER_QUERY_TIMER) &&\
         (SNOOP_MCAST_FWD_MODE (u4Instance) == SNOOP_MCAST_FWD_MODE_IP) &&\
         ((u1AddressType == SNOOP_ADDR_TYPE_IPV4) || \
          (u1AddressType == SNOOP_ADDR_TYPE_IPV6))&& \
          (SNOOP_IS_PROXY_FUNCTIONALITY_ENABLED (u4Instance, u1AddressType - 1)\
             == SNOOP_TRUE))\
     {\
         u1IsSwitchOverInProgress = SNOOP_TRUE;\
     }\
     else\
     {\
         u1IsSwitchOverInProgress =  SNOOP_FALSE;\
     }\
}

#define SNOOP_GRP_QUERY_FWD(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4FwdGSQs++; \
        if (pSnoopStatsEntry->u4FwdGSQs == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4FwdGSQs = 0; \
        } \
}

#define SNOOP_GEN_QUERY_FWD(pSnoopStatsEntry) \
{ \
        pSnoopStatsEntry->u4FwdGenQueries++; \
        if (pSnoopStatsEntry->u4FwdGenQueries == 0xFFFFFFFF) \
        { \
            pSnoopStatsEntry->u4FwdGenQueries = 0; \
        } \
}


#define SNOOP_SET_PORT_PURGE_INTERVAL(pSnoopVlanEntry, u4Robustness, version) \
{\
    UINT4    u4MaxRespTime = 0;\
    UINT2    u2QueryTime = 0;\
    UINT2    u2PortPurge = 0;\
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV4)\
    {\
        if (version == SNOOP_IGMP_VERSION_3) \
       {\
           u4MaxRespTime = (UINT4)(SNOOP_IGS_GET_TIME_FROM_CODE (pSnoopVlanEntry->u2MaxRespCode));\
       }\
       else \
       {\
           u4MaxRespTime = pSnoopVlanEntry->u2MaxRespCode; \
       }\
    u4MaxRespTime = (UINT4)(u4MaxRespTime / SNOOP_IGS_MRC_UNIT);\
    }\
    if (pSnoopVlanEntry->u1AddressType == SNOOP_ADDR_TYPE_IPV6)\
    {\
    u4MaxRespTime = \
    (UINT4)(SNOOP_MLDS_GET_TIME_FROM_CODE (pSnoopVlanEntry->u2MaxRespCode));\
    u4MaxRespTime = (UINT4)(u4MaxRespTime / SNOOP_MLDS_MRC_UNIT);\
    }\
    u2QueryTime = (UINT2) (pSnoopVlanEntry->u2QueryInterval * u4Robustness);\
    u2PortPurge = (UINT2) (u2QueryTime + u4MaxRespTime);\
    pSnoopVlanEntry->u2PortPurgeInt = u2PortPurge;\
}
/*IP based utility macros */

#define SNOOP_IS_SOURCE_PRESENT(u4Inst, SrcAddr, bResult, u2SrcIndex) \
{ \
    UINT2   u2SourceCount = 0; \
    bResult = SNOOP_FALSE;\
    u2SrcIndex = 0xffff; \
    for (u2SourceCount = 0;u2SourceCount < SNOOP_MAX_MCAST_SRCS;u2SourceCount++)\
    {\
       if (IPVX_ADDR_COMPARE(SrcAddr,\
              SNOOP_SRC_INFO (u4Inst,u2SourceCount).SrcIpAddr) == 0)\
       {\
          u2SrcIndex = u2SourceCount;\
          bResult = SNOOP_TRUE;\
   break;\
       }\
    }\
}

#define SNOOP_IS_GRP_SRC_PRESENT(u2SourceIndex, SrcBitmap, bResult) \
{ \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
    UINT2      u2Index = (UINT2)(u2SourceIndex + 1); \
    bResult = OSIX_FALSE;\
    OSIX_BITLIST_IS_BIT_SET(SrcBitmap, u2Index, SNOOP_SRC_LIST_SIZE, bResult);\
}

#define SNOOP_ADD_TO_SRC_BMP(SrcIndex, SrcBitmap, u1BmpType)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   if (u1BmpType == SNOOP_INCLUDE)\
   {\
        OSIX_BITLIST_SET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);\
   }\
   else\
   {\
        if (SNOOP_MEM_CMP (SrcBitmap, TempSnoopExclBmp, SNOOP_SRC_LIST_SIZE) == 0)\
        {\
           SNOOP_MEM_SET (SrcBitmap, 0, SNOOP_SRC_LIST_SIZE); \
        }\
        OSIX_BITLIST_SET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);\
   }\
}

#define SNOOP_DEL_FROM_SRC_BMP(SrcIndex, SrcBitmap, u1BmpType)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   if (u1BmpType == SNOOP_EXCLUDE)\
   {\
      if (SNOOP_MEM_CMP (SrcBitmap, TempSnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)\
      {\
         OSIX_BITLIST_RESET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);  \
         /* when the ExclSrcBmp has none of the sources \
          * set, then the filter mode of the host should only be Excl None. */ \
      }\
   }\
   else\
   {\
      OSIX_BITLIST_RESET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);  \
   }\
}

/* Macros specific to INCLUDE and EXCLUDE */
#define SNOOP_ADD_TO_SRC_BMP_INCLUDE(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   {\
        OSIX_BITLIST_SET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);\
   }\
}

#define SNOOP_DEL_FROM_SRC_BMP_INCLUDE(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   {\
      OSIX_BITLIST_RESET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);  \
   }\
}

#define SNOOP_ADD_TO_SRC_BMP_EXCLUDE(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   {\
        if (SNOOP_MEM_CMP (SrcBitmap, TempSnoopExclBmp, SNOOP_SRC_LIST_SIZE) == 0)\
        {\
           SNOOP_MEM_SET (SrcBitmap, 0, SNOOP_SRC_LIST_SIZE); \
        }\
        OSIX_BITLIST_SET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);\
   }\
}

#define SNOOP_DEL_FROM_SRC_BMP_EXCLUDE(SrcIndex, SrcBitmap)\
{\
   UINT2       u2TempSrcIndex = 0;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
    /* The SrcIndex always have to be less than SNOOP_SRC_LIST_SIZE that \
     * in the range of UINT2 */ \
   u2TempSrcIndex = (UINT2)(SrcIndex + 1);\
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
    if (SNOOP_MEM_CMP (SrcBitmap, TempSnoopExclBmp, SNOOP_SRC_LIST_SIZE) != 0)\
    {\
        OSIX_BITLIST_RESET_BIT(SrcBitmap, u2TempSrcIndex, SNOOP_SRC_LIST_SIZE);  \
        /* when the ExclSrcBmp has none of the sources \
         * set, then the filter mode of the host should only be Excl None. */ \
    }\
}

#define SNOOP_OR_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] |= SrcBmp[u2ByteIndex];\
   }\
}

#define SNOOP_AND_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] &= SrcBmp[u2ByteIndex];\
   }\
}

#define SNOOP_NEG_SRC_BMP(SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      SrcBmp[u2ByteIndex] = (UINT1) (~(SrcBmp[u2ByteIndex]));\
   }\
}

#define SNOOP_XOR_SRC_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_SRC_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] ^= SrcBmp[u2ByteIndex];\
   }\
}


/* This macro will return the first emply slot in the source list array 
 * and add the source address to the array or the slot in which the source 
 * address is present 
 * Also, the source address is added in the sorted order in the linked list*/
#define SNOOP_ADD_TO_SOURCE_LIST(u4Inst, SrcAddr, u2SrcIndex, bNewSource)\
{\
   UINT2 u2Index; \
   UINT1 au1NullAddr[16]; \
   bNewSource = SNOOP_FALSE;\
   MEMSET (au1NullAddr, 0, 16);\
   for (u2Index = 0; u2Index < SNOOP_MAX_MCAST_SRCS; u2Index++) \
   {\
       if ((MEMCMP(SNOOP_SRC_INFO (u4Inst, u2Index).SrcIpAddr.au1Addr, \
                                   au1NullAddr ,16) == 0)\
                                  && (bNewSource == SNOOP_FALSE))\
       {\
          u2SrcIndex = u2Index;\
          bNewSource = SNOOP_TRUE;\
       }\
       if (IPVX_ADDR_COMPARE(SrcAddr, &(SNOOP_SRC_INFO (u4Inst,\
                            u2Index).SrcIpAddr)) == 0) \
       {\
          u2SrcIndex = u2Index;\
          bNewSource = SNOOP_FALSE;\
          break;\
       }\
   }\
   if (u2SrcIndex < SNOOP_MAX_MCAST_SRCS) \
   {\
       IPVX_ADDR_COPY(&SNOOP_SRC_INFO (u4Inst, u2SrcIndex).SrcIpAddr, &SrcAddr);\
       /* Adding the index to the source info structure*/\
       SNOOP_SRC_INFO (u4Inst, u2SrcIndex).u2NodeIndex = u2SrcIndex;\
       if (bNewSource == SNOOP_TRUE) \
       {\
           SnoopUtilAddSource (u4Inst, &(SNOOP_SRC_INFO (u4Inst, u2SrcIndex))); \
       }\
    }\
}

/* This macro will make the Include and Exclude bitmap mutually exclusive */
#define SNOOP_UPDATE_SRC_INFO(InclSrcBmp, ExclSrcBmp)\
{\
   UINT2 u2Index = 0; \
   BOOL1 bResult = OSIX_FALSE;\
   tSnoopSrcBmp    TempSnoopExclBmp; \
   SNOOP_MEM_SET(TempSnoopExclBmp, 0xff, SNOOP_SRC_LIST_SIZE);\
   if (SNOOP_MEM_CMP (ExclSrcBmp, TempSnoopExclBmp, \
                      SNOOP_SRC_LIST_SIZE) != 0) \
   {\
   for (u2Index = 1; u2Index <= (SNOOP_SRC_LIST_SIZE * 8); u2Index++) \
   {\
      OSIX_BITLIST_IS_BIT_SET (InclSrcBmp, u2Index,\
                                 SNOOP_SRC_LIST_SIZE, bResult);\
      if (bResult == OSIX_TRUE)\
       {\
             OSIX_BITLIST_RESET_BIT (ExclSrcBmp, u2Index, SNOOP_SRC_LIST_SIZE);\
          }\
       }\
   }\
}



/* Port list updation macros */
#define SNOOP_IS_PORT_PRESENT_IN_IF_PORT_LIST(Port, PortBitmap, bResult)\
        OSIX_BITLIST_IS_BIT_SET(PortBitmap, (UINT2) Port, \
                              SNOOP_IF_PORT_LIST_SIZE, bResult)
        
#define SNOOP_IS_PORT_PRESENT(Port, PortBitmap, bResult)\
        OSIX_BITLIST_IS_BIT_SET(PortBitmap, (UINT2) Port, \
                              SNOOP_PORT_LIST_SIZE, bResult)
        
#define SNOOP_ADD_TO_PORT_LIST(Port, PortBitmap)\
        OSIX_BITLIST_SET_BIT(PortBitmap, (UINT2) Port, SNOOP_PORT_LIST_SIZE)  
        
#define SNOOP_DEL_FROM_PORT_LIST(Port, PortBitmap)\
        OSIX_BITLIST_RESET_BIT(PortBitmap, (UINT2) Port, SNOOP_PORT_LIST_SIZE)  
        
#define SNOOP_COPY_PORT_LIST(SrcPortBmp, DstPortBmp)\
        SNOOP_MEM_CPY(DstPortBmp, SrcPortBmp, SNOOP_PORT_LIST_SIZE)  

#define SNOOP_UPDATE_PORT_LIST(PortBmp1, PortBmp2)\
        OSIX_ADD_PORT_LIST(PortBmp2, PortBmp1, \
                            SNOOP_PORT_LIST_SIZE, SNOOP_PORT_LIST_SIZE)
        
#define SNOOP_XOR_PORT_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] ^= SrcBmp[u2ByteIndex];\
   }\
}

#define SNOOP_AND_PORT_BMP(DestBmp, SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] &= SrcBmp[u2ByteIndex];\
   }\
}

#define SNOOP_NEG_PORT_BMP(PortBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE; u2ByteIndex++) \
   {\
      PortBmp[u2ByteIndex] = (UINT1) (~(PortBmp[u2ByteIndex]));\
   }\
}


#define SNOOP_AND_PHY_PORT_BMP(DestBmp,SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_IF_PORT_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] &= SrcBmp[u2ByteIndex];\
   }\
}

#define SNOOP_AND_LOCAL_PORT_BMP(DestBmp,SrcBmp)\
{\
   UINT2 u2ByteIndex; \
   \
   for (u2ByteIndex = 0; u2ByteIndex < SNOOP_PORT_LIST_SIZE; u2ByteIndex++) \
   {\
      DestBmp[u2ByteIndex] &= SrcBmp[u2ByteIndex];\
   }\
}

/* Utillity macros */
#define SNOOP_GET_MAC_FROM_IPV4(IpAddr, MacAddr) \
{\
    UINT4     u4Dword; \
    UINT2     u2Word; \
    u4Dword = 0x01005E00 | ((IpAddr & 0x007f0000)>>16); \
    u2Word = (UINT2) (IpAddr & 0x0000ffff); \
    u4Dword = SNOOP_NTOHL(u4Dword); \
    u2Word = SNOOP_NTOHS(u2Word); \
    SNOOP_MEM_CPY (MacAddr, (UINT1 *) &u4Dword, 4); \
    SNOOP_MEM_CPY (&MacAddr[4], (UINT1 *) &u2Word, 2); \
}

#define  SNOOP_OFFSET(x,y)   FSAP_OFFSETOF(x,y)

#define  SNOOP_VALIDATE_RECORD_TYPE(u1RecordType, bResult) \
{\
    switch (u1RecordType)\
    {\
       case SNOOP_IS_INCLUDE:   \
       case SNOOP_TO_INCLUDE:   \
       case SNOOP_IS_EXCLUDE:   \
       case SNOOP_TO_EXCLUDE:   \
       case SNOOP_ALLOW:   \
       case SNOOP_BLOCK:   \
            bResult = OSIX_FALSE;\
            break;\
       default:\
            bResult = OSIX_TRUE;\
    }\
}

/* MLD module related macros */
/* MLDS_CHANGES */
#define SNOOP_IS_MLD_PACKET(a) ( a->u1PktType == 130 ||\
                           a->u1PktType == 131 ||\
                           a->u1PktType == 132 ||\
                           a->u1PktType == 143 )

#define SNOOP_IS_ND6_PACKET(a) (a == 133 ||\
                           a == 134 ||\
                           a == 135 ||\
                           a == 136 ||\
                           a == 137 )

#define SNOOP_IS_ALL_NODES(a) (((a).u4_addr[0] == 0xff020000) && \
                          ((a).u4_addr[1] == 0)          && \
                          ((a).u4_addr[2] == 0)          && \
                          ((a).u4_addr[3] == 1))

#define SNOOP_IS_ALL_ROUTERS(a) (((a).u4_addr[0] == 0xff020000) && \
                          ((a).u4_addr[1] == 0)          && \
                          ((a).u4_addr[2] == 0)          && \
                          ((a).u4_addr[3] == 2))

#define SNOOP_IS_ALL_V2_ROUTERS(a) (((a).u4_addr[0] == 0xff020000) && \
                              ((a).u4_addr[1] == 0)          && \
                              ((a).u4_addr[2] == 0)          && \
                              ((a).u4_addr[3] == 0x16))

#define SNOOP_IS_ALL_PIM6_ROUTERS(a) (((a).u4_addr[0] == 0xff020000) && \
                              ((a).u4_addr[1] == 0)          && \
                              ((a).u4_addr[2] == 0)          && \
                              ((a).u4_addr[3] == 0xd))

#define SNOOP_IS_ALL_OSPFV3_ROUTERS(a) (((a).u4_addr[0] == 0xff020000) && \
                              ((a).u4_addr[1] == 0)          && \
                              ((a).u4_addr[2] == 0)          && \
                              ((a).u4_addr[3] == 5))

#define SNOOP_IS_RESERVED_IPV6_MULTI_ADDR(a, bResult) \
{ \
   tIPvXAddr addrToCmp; \
   UINT1 u1Count; \
   bResult = SNOOP_FALSE; \
   addrToCmp.u1Afi = SNOOP_ADDR_TYPE_IPV6; \
   addrToCmp.u1AddrLen = IPVX_IPV6_ADDR_LEN; \
   MEMSET (&(addrToCmp.au1Addr), 0, IPVX_IPV6_ADDR_LEN);\
   addrToCmp.au1Addr[3] = 0xff;\
   if ((a.au1Addr[2] != 0) && (a.au1Addr[2] != 1)) \
   {\
      for (u1Count = 2; u1Count < IPVX_IPV6_ADDR_LEN; u1Count++) \
      { \
         addrToCmp.au1Addr[2] = u1Count; \
         if (MEMCMP (&(a.au1Addr), &(addrToCmp.au1Addr), IPVX_IPV6_ADDR_LEN) == 0) \
         { \
            break; \
         } \
      } \
      if (u1Count != IPVX_IPV6_ADDR_LEN)\
         bResult = SNOOP_TRUE;\
   }\
}

#define SNOOP_IS_RESERVED_LINK_SCOPE_MULTI(a)  (((a).u4_addr[0] == 0xff020000) && \
                                  ((a).u4_addr[1] == 0 )         && \
                                  ((a).u4_addr[2] == 0 )         && \
                                  ((((a).u4_addr[3] >= 1) && \
                                  ((a).u4_addr[3] <= 0x0000000E)) || \
                                  ((a).u4_addr[3] == 0x00010001) ||\
                                  ((a).u4_addr[3] == 0x00010002)))

#define SNOOP_IS_RESERVED_NODE_SCOPE_MULTI(a)  ((((a).u4_addr[0] == 0xff010000) || (a.u4_addr[0] == 0xff000000)) && \
                                  ((a).u4_addr[1] == 0 )         && \
                                  ((a).u4_addr[2] == 0 )         && \
                                  (((a).u4_addr[3] == 1) || \
                                  ((a).u4_addr[3] == 2)))

#define SNOOP_IS_RESERVED_MULTI(a)  \
   ((a).u4_addr[1] == 0 && (a).u4_addr[2] == 0 && (a).u4_addr[3] == 0 \
   && (((a).u4_addr[0]) >= 0xff000000) \
   && (((a).u4_addr[0]) <= 0xff0f0000))


#define SNOOP_PTR_FETCH_4(ptr) \
              SnoopUtilFetchFourBytesFromPointer(ptr)

/* MLDS_CHANGES */
#define SNOOP_INET_HTONL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
     \
    SNOOP_MEM_SET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
        u1Index++, u4Count = u4Count + 4)\
    {\
 SNOOP_MEM_CPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
        u4TmpGrpAddr = SNOOP_HTONL(u4TmpGrpAddr);\
 SNOOP_MEM_CPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
    SNOOP_MEM_CPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define SNOOP_INET_NTOHL(GrpIp1)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
    UINT1   au1TmpGrpIp[IPVX_MAX_INET_ADDR_LEN];\
     \
    SNOOP_MEM_SET (au1TmpGrpIp, 0, IPVX_MAX_INET_ADDR_LEN);\
    \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
        u1Index++, u4Count = u4Count + 4)\
    {\
 SNOOP_MEM_CPY (&u4TmpGrpAddr, (GrpIp1 + u4Count), sizeof(UINT4));\
        u4TmpGrpAddr = SNOOP_NTOHL(u4TmpGrpAddr);\
 SNOOP_MEM_CPY (&(au1TmpGrpIp[u4Count]), &u4TmpGrpAddr, sizeof(UINT4));\
    }\
    SNOOP_MEM_CPY (GrpIp1, au1TmpGrpIp, IPVX_MAX_INET_ADDR_LEN);\
}

#define  SNOOP_BUF_READ_OFFSET(buf)   CB_READ_OFFSET(buf)                               
#define SNOOP_MLDS_GRP_ADDR_COPY(GrpIp1, GrpIp2)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT4   u4Count = 0;\
    UINT1   u1Index = 0;\
     \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
        u1Index++, u4Count = u4Count + 4)\
    {\
 if (GrpIp2 == NULL)\
  break;\
 SNOOP_MEM_CPY (&u4TmpGrpAddr, (GrpIp2 + u4Count), sizeof(UINT4));\
 SNOOP_MEM_CPY (&(GrpIp1.u4_addr[u1Index]), &u4TmpGrpAddr,\
   sizeof(UINT4));\
    }\
}

#define SNOOP_MLDS_GRP_ADDR_COPY1(GrpIp1, GrpIp2)\
{\
    UINT4   u4TmpGrpAddr = 0;\
    UINT1   u4Count = 0;\
    UINT1   u1Index = 0;\
     \
    for (u4Count = 0, u1Index = 0; u4Count < IPVX_MAX_INET_ADDR_LEN; \
        u1Index++, u4Count = u4Count + 4)\
    {\
 SNOOP_MEM_CPY (&u4TmpGrpAddr, &(GrpIp2[u4Count]), sizeof(UINT4));\
   SNOOP_NTOHL(u4TmpGrpAddr);\
 SNOOP_MEM_CPY (&(GrpIp1.u4_addr[u1Index]), &u4TmpGrpAddr,\
   sizeof(UINT4));\
    }\
}

#define SNOOP_GET_MAC_FROM_IPV6(pMldGroupIp, MldMac)\
{\
   UINT4 u4TmpWord1 = 0;\
   UINT4 u4TmpWord2 = 0;\
   UINT2 u2TmpWord = 0;\
   \
   memcpy ((UINT1 *) &u4TmpWord2,  &(pMldGroupIp[12]), sizeof(UINT4));\
   u4TmpWord1 = 0x33330000 | ((u4TmpWord2 & 0xffff0000) >> 16);\
   u4TmpWord1 = SNOOP_NTOHL(u4TmpWord1);\
   u2TmpWord = (UINT2) (u4TmpWord2 & 0x0000ffff); \
   u2TmpWord = SNOOP_NTOHS(u2TmpWord); \
   SNOOP_MEM_CPY (MldMac, (UINT1 *) &u4TmpWord1, 4); \
   SNOOP_MEM_CPY (&MldMac[4], (UINT1 *) &u2TmpWord, 2); \
}

#define SNOOP_SOURCE_LIST(u4Inst)      SNOOP_INSTANCE_INFO(u4Inst)->SnoopSrcList

#define SNOOP_SYSTEM_LEAVE_LEVEL(u4InstId) \
           SNOOP_INSTANCE_INFO(u4InstId)->u1LeaveConfLevel
           
#define SNOOP_SYSTEM_REPORT_LEVEL(u4InstId) \
           SNOOP_INSTANCE_INFO(u4InstId)->u1ReportConfLevel
           
#define SNOOP_SYSTEM_SPARSE_MODE(u4InstId) \
           SNOOP_INSTANCE_INFO(u4InstId)->u1SystemSparseMode
           
#define SNOOP_REMOVE_SOURCE(u4Inst, SourceInfo) \
{\
    SourceInfo.u2NodeIndex = 0; \
    SNOOP_DLL_DELETE (&(SNOOP_SOURCE_LIST (u4Inst)), &(SourceInfo)); \
    SNOOP_DLL_INIT_NODE (&(SourceInfo)); \
}


#define SNOOP_SYSLOG_MSG_LENGTH           200

#define SNOOP_FREE_GRP_SRC_RECORDLIST()\
{\
    tGrpSrcStatusEntry *pGrpSrcStatNode = NULL;\
    tGrpSrcStatusEntry *pTempGrpSrcStatusNode = NULL;\
    if (gGrpRecList.u4_Count != 0)\
    {\
        SNOOP_DYN_SLL_SCAN(&gGrpRecList,pGrpSrcStatNode,pTempGrpSrcStatusNode,\
                           tGrpSrcStatusEntry *)\
        {\
            SNOOP_SLL_DELETE (&gGrpRecList, pGrpSrcStatNode);\
                SNOOP_GRPSRC_STATUS_ENTRY_FREE_MEMBLK(pGrpSrcStatNode);\
        }\
    }\
}
/* CRU Buffer related definitions */
                                                                                                               
#define   SNOOP_ALLOC_CRU_BUF                    CRU_BUF_Allocate_MsgBufChain
#define   SNOOP_RELEASE_CRU_BUF                  CRU_BUF_Release_MsgBufChain
#define   SNOOP_COPY_OVER_CRU_BUF                CRU_BUF_Copy_OverBufChain
#define   SNOOP_COPY_FROM_CRU_BUF                CRU_BUF_Copy_FromBufChain
#define   SNOOP_CRU_BUF_GET_BYTECOUNT            CRU_BUF_Get_ChainValidByteCount
#define   SNOOP_CRU_BUF_CONCAT_BUFCHAINS         CRU_BUF_Concat_MsgBufChains
#define   SNOOP_IS_CRU_BUF_LINEAR                CRU_BUF_Get_DataPtr_IfLinear
#define   SNOOP_DUPLICATE_CRU_BUF                CRU_BUF_Duplicate_BufChain
#define   SNOOP_CRU_SUCCESS                      CRU_SUCCESS
#define   SNOOP_CRU_FAILURE                      CRU_FAILURE


#define   SNOOP_VALIDATE_INSTANCE(u4Instance) \
{\
    if (u4Instance >= SNOOP_MAX_INSTANCES)\
    {\
        return;\
    }\
}

#define   SNOOP_VALIDATE_INSTANCE_RET(u4Instance,u4Ret) \
{\
    if (u4Instance >= SNOOP_MAX_INSTANCES)\
    {\
        return u4Ret;\
    }\
 if (SNOOP_INSTANCE_INFO (u4Instance) == NULL)\
 {\
        return u4Ret;\
    }\
}

#define   SNOOP_VALIDATE_ADDRESS_TYPE(u1Type)\
{\
    if ((u1Type != SNOOP_ADDR_TYPE_IPV4) && \
        (u1Type != SNOOP_ADDR_TYPE_IPV6))\
    {\
        return;\
    }\
}

#define   SNOOP_VALIDATE_ADDRESS_TYPE_RET(u1Type,u4Ret)\
{\
    if ((u1Type != SNOOP_ADDR_TYPE_IPV4) && \
        (u1Type != SNOOP_ADDR_TYPE_IPV6))\
    {\
        return u4Ret;\
    }\
}

#define   SNOOP_VALIDATE_PORT(u2Port) \
{\
    if ((u2Port == 0) ||  (u2Port > SNOOP_MAX_PORTS_PER_INSTANCE))\
    {\
        return;\
    }\
}

#define   SNOOP_VALIDATE_PORT_RET(u2Port,u4Ret) \
{\
    if ((u2Port == 0) ||  (u2Port > SNOOP_MAX_PORTS_PER_INSTANCE))\
    {\
        return u4Ret;\
    }\
}

#define   SNOOP_CHK_NULL_PTR_RET(ptr,RetVal)                  \
          if(ptr == NULL)                                     \
          {                                                   \
              return RetVal;                                  \
          }

#define   SNOOP_CHK_NULL_PTR(ptr)                             \
          if(ptr == NULL)                                     \
          {                                                   \
              return;                                         \
          }
#endif /* _SNPMACS_H */
