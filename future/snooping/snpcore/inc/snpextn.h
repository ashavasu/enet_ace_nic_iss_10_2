#ifndef _SNPEXTN_H
#define _SNPEXTN_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/* $Id: snpextn.h,v 1.25 2017/12/04 13:45:46 siva Exp $*/
/*****************************************************************************/
/*    FILE  NAME            : snpextn.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) external Global variables  */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains extern global variables     */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/
extern tSnoopPortInstanceMap gaSnoopPortInstanceMap[SNOOP_MAX_PORTS];
extern tSnoopGlobalInstInfo     *gapSnoopGlobalInstInfo [SNOOP_MAX_INSTANCES];
extern tSnoopGlobalMemPoolId    gSnoopGlobalPoolId;
extern tTimerListId           gSnoopTimerListId;          /* Timer List Id */
extern tSnoopSemId              gSnoopSemId;
extern const CHR1 *gau1TmrType[SNOOP_MAX_TIMER_COUNTS];
extern tGroupEntryStatusNode    gaGroupEntryStatus[SNOOP_MAX_PORTS_PER_INSTANCE + 1];
extern tSnoopTaskId             gSnoopTaskId; /* Snoop Task Id */
extern tSnoopQId                gSnoopMsqQId;
extern tIPvXAddr gNullAddr;
extern tSnoopQId                gSnoopPktQId;
extern UINT1         gu1SnoopSystemStatus[];  /* Indicates whether 
                                                   * resources have 
                                                   * been freed */
extern UINT1         *gpSSMRepSendBuffer;  
extern UINT1         *gpSSMSrcInfoBuffer; 
extern UINT1         *gpSnoopDataBuffer; 
extern UINT1         gu1SnoopInstanceId;  

extern tSnoopSourceInfo gaSrcInfo [SNOOP_MAX_MCAST_SRCS]; 
extern UINT2         gu2SourceCount;
extern UINT4         gu4SendRecType;  
extern tSnoopPortBmp   gNullPortBitMap;
extern tSnoopIfPortBmp gNullIfPortBitMap;
extern tSnoopSrcBmp    gNullSrcBmp;
extern tLocalPortList  gSnoopLocalPortList;
extern tSnoopIfPortBmp gSnoopIfPortList;
extern UINT1         gu1IsSameHost; 
extern UINT1         gu1IsResetHost;
extern UINT1        gu1ConsModified;
extern UINT2         gu2MaxHwEntryCount;
extern UINT1         gNullGrpAddr[IPVX_MAX_INET_ADDR_LEN];
extern UINT1         gNullSrcAddr[IPVX_MAX_INET_ADDR_LEN];

extern tRBTree gPortCfgTblEntry;       /* Indicates global RBTree 
              for Port config table */

/* For Trace messages */
extern UINT4         gu4SnoopTrcInstId;  
extern UINT4         gu4SnoopTrcModule;  

/* For Debug messages */
extern UINT4         gu4SnoopDbgInstId;
extern UINT4         gu4SnoopDbgModule;

extern tSnoopNodeState     gSnoopNodeStatus;
extern UINT1             gu1SnoopInit;
extern UINT1             gu1SnoopLaCardRemovalProcessed;
extern UINT1             gu1BatchingInProgress;
extern UINT4                    gu4SwitchIp;        
extern UINT4                    gu4SnoopVlanId;
extern tRBTree                  gRtrPortTblRBRoot; 

extern tSnoopSll               gGrpRecList;
extern tCVlanInfo              gSnoopCVlanInfo;
extern tSnoopVlanList          gSnoopNullVlanList;

#ifdef L2RED_WANTED
extern UINT1                  gu1NumPeerPresent;
extern tSnoopRedGlobalInfo  gSnoopRedGlobalInfo;
extern UINT1          gu1SnoopRedGenQueryInProgress;
extern UINT1          gu1SnoopRedAuditInProgress;
extern UINT1          gu1SnoopRedBulkSyncInProgress;
extern tSnoopTaskId   gSnoopAuditTaskId; /* Snoop Audit Task Id */
#endif

#ifdef MLDS_WANTED
extern UINT1  gau1MldsAllHostIp[IPVX_MAX_INET_ADDR_LEN];
extern UINT1  gau1MldsAllMldv2Rtr[IPVX_MAX_INET_ADDR_LEN];
extern UINT1  gau1MldsAllRtrIp[IPVX_MAX_INET_ADDR_LEN];
#endif

extern UINT4 gu4IgsSysLogId;
extern UINT1 gu1SnoopPortAction;

/* Sizing Params Structure */
extern tFsModSizingParams gFsIgsSizingParams [];
extern tFsModSizingInfo gFsIgsSizingInfo;
#endif /* _SNPEXTN_H */
