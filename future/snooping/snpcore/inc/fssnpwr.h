/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fssnpwr.h,v 1.19 2017/08/25 13:43:52 siva Exp $
 * *
 * * Description: Proto types for Wrapper Level  Routines
 * *********************************************************************/
#ifndef _FSSNPWR_H
#define _FSSNPWR_H
INT4 GetNextIndexFsSnoopInstanceGlobalTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSSNP(VOID);

VOID UnRegisterFSSNP(VOID);
INT4 FsSnoopInstanceGlobalMcastFwdModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalLeaveConfigLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalEnhancedModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSparseModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalReportProcessConfigLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceControlPlaneDrivenGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalMcastFwdModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalLeaveConfigLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalEnhancedModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSparseModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalReportProcessConfigLevelSet(tSnmpIndex *, tRetVal *);

INT4 FsSnoopInstanceControlPlaneDrivenSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalMcastFwdModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalLeaveConfigLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalEnhancedModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalSparseModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalReportProcessConfigLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceControlPlaneDrivenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceGlobalTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsSnoopInstanceConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyReportingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRouterPortPurgeIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortPurgeIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportForwardIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRetryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopGrpQueryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportFwdOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendQueryOnTopoChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendLeaveOnTopoChangeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopFilterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopMulticastVlanStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopQueryFwdOnAllPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopFwdGroupsCntGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyReportingStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRouterPortPurgeIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortPurgeIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportForwardIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRetryCountSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopGrpQueryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportFwdOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendQueryOnTopoChangeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendLeaveOnTopoChangeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopFilterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopMulticastVlanStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopQueryFwdOnAllPortsSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyReportingStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopRouterPortPurgeIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortPurgeIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportForwardIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopRetryCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopGrpQueryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopReportFwdOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendQueryOnTopoChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopSendLeaveOnTopoChangeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopFilterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopMulticastVlanStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopProxyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopQueryFwdOnAllPortsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopInstanceConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);














INT4 GetNextIndexFsSnoopVlanMcastMacFwdTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanMcastMacFwdPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMcastMacFwdLocalPortListGet(tSnmpIndex *, tRetVal *);
/*MAC FLAG*/
INT4 FsSnoopVlanMcastMacFwdEntryFlagGet(tSnmpIndex *, tRetVal *);

INT4 GetNextIndexFsSnoopVlanMcastIpFwdTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanMcastIpFwdPortListGet(tSnmpIndex *, tRetVal *);
/*IP FLAG*/
INT4 FsSnoopVlanMcastIpFwdEntryFlagGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopVlanRouterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanRouterPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRouterLocalPortListGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopVlanFilterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanSnoopStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanOperatingVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFastLeaveGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgQuerierGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryCountGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQueryIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanOtherQuerierPresentIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanElectedQuerierGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRobustnessValueGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanSnoopStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgOperVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFastLeaveSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgQuerierSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryCountSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQueryIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanOtherQuerierPresentIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRobustnessValueSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanSnoopStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgOperVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFastLeaveTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanCfgQuerierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStartupQueryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQueryIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanOtherQuerierPresentIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanQuerierIpFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRobustnessValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsSnoopVlanMcastGroupTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanMcastGroupPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMcastGroupLocalPortListGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopVlanMcastReceiverTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanMcastReceiverFilterModeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopVlanIpFwdTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanIpFwdPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanIpFwdLocalPortListGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopVlanFilterXTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanBlkRtrPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilter8021pPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterDropReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMulticastProfileIdGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanPortPurgeIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMaxResponseTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrLocalPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanBlkRtrLocalPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanBlkRtrPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilter8021pPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterDropReportsSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMulticastProfileIdSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMaxResponseTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrLocalPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanBlkRtrLocalPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanBlkRtrPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterMaxLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilter8021pPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterDropReportsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMulticastProfileIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanMaxResponseTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanRtrLocalPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanBlkRtrLocalPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanFilterXTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsSnoopVlanStaticMcastGrpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopVlanStaticMcastGrpPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopVlanStaticMcastGrpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsSnoopStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopStatsRxGenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxGrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxGrpAndSrcQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxAsmReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmIsInMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmIsExMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmToInMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmToExMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmAllowMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxSsmBlockMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxAsmLeavesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxGenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxGrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxGrpAndSrcQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxAsmReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxSsmReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxAsmLeavesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDroppedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsUnsuccessfulJoinsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsActiveJoinsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsActiveGroupsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsRxV1ReportGet(tSnmpIndex *, tRetVal *);                                                                      INT4 FsSnoopStatsRxV2ReportGet(tSnmpIndex *, tRetVal *);                                                                      INT4 FsSnoopStatsTxV1ReportGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsTxV2ReportGet(tSnmpIndex *, tRetVal *);                                                                      INT4 FsSnoopStatsDropV1GenQueriesGet(tSnmpIndex *, tRetVal *);                                                                INT4 FsSnoopStatsDropV2GenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropV3GenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropV2GrpQueriesGet(tSnmpIndex *, tRetVal *);                                                                INT4 FsSnoopStatsDropV3GrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropGrpAndSrcQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropV1ReportsGet(tSnmpIndex *, tRetVal *);                                                                   INT4 FsSnoopStatsDropV2ReportsGet(tSnmpIndex *, tRetVal *);                                                                   INT4 FsSnoopStatsDropSsmIsInMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropSsmIsExMsgsGet(tSnmpIndex *, tRetVal *);                                                                 INT4 FsSnoopStatsDropSsmToInMsgsGet(tSnmpIndex *, tRetVal *);                                                                 INT4 FsSnoopStatsDropSsmToExMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropSsmAllowMsgsGet(tSnmpIndex *, tRetVal *);                                                                INT4 FsSnoopStatsDropSsmBlockMsgsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsDropAsmLeavesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsFwdGenQueriesGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopStatsFwdGrpQueriesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsSnoopPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopPortLeaveModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRateLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortProfileIdGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMemberCntGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxBandwidthLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortDropReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortLeaveModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRateLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortProfileIdSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxBandwidthLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortDropReportsSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortLeaveModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRateLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortProfileIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortMaxBandwidthLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortDropReportsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








INT4 GetNextIndexFsSnoopEnhPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopEnhPortLeaveModeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRateLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortProfileIdGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMemberCntGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxBandwidthLimitGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortDropReportsGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortLeaveModeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRateLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortProfileIdSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxBandwidthLimitSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortDropReportsSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortLeaveModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRateLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortProfileIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortMaxBandwidthLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortDropReportsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopEnhPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








INT4 GetNextIndexFsSnoopRtrPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopRtrPortOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRtrPortCfgOperVersionGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopOlderQuerierIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopV3QuerierIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRtrPortCfgOperVersionSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopOlderQuerierIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsSnoopRtrPortCfgOperVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopOlderQuerierIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSnoopRtrPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsSnoopTrapObjectsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsSnoopTrapHwErrTypeGet(tSnmpIndex *, tRetVal *);
#endif /* _FSSNPWR_H */
