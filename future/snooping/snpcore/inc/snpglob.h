/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: snpglob.h,v 1.29 2017/12/04 13:45:46 siva Exp $
 *
 * Description: This file contains global variables for
 *              snooping(MLD/IGMP) module.
 *****************************************************************************/
#ifndef _SNPGLOB_H
#define _SNPGLOB_H
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Licensee Aricent Inc., 2006                              */
/*****************************************************************************/
/*    FILE  NAME            : snpglob.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                */
/*    SUBSYSTEM NAME        : Snooping                                       */
/*    MODULE NAME           : Snooping (MLD/IGMP) Globals                    */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : Aricent Inc.                                */
/*    DESCRIPTION           : This file contains global variables            */
/*                            for snooping(MLD/IGMP) module                  */
/*---------------------------------------------------------------------------*/
/* CHANGE HISTORY :                                                          */
/*---------------------------------------------------------------------------*/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */
/*---------------------------------------------------------------------------*/
/* 1.0.0.0    January 2006           Initial Creation                       */ 
/*---------------------------------------------------------------------------*/

tSnoopPortInstanceMap   gaSnoopPortInstanceMap[SNOOP_MAX_PORTS]; /* This array 
                                             is used to hold the
                                             local port number corresponding
                                             to physical port, indexed with 
                                             physical port, in MI case this
                                             array is used for quick reference
                                             of local port from physical port */
tSnoopGlobalInstInfo     *gapSnoopGlobalInstInfo [SNOOP_MAX_INSTANCES];
tSnoopGlobalMemPoolId    gSnoopGlobalPoolId;
tTimerListId             gSnoopTimerListId;          /* Timer List Id */
tSnoopSemId              gSnoopSemId;

tGroupEntryStatusNode    gaGroupEntryStatus[SNOOP_MAX_PORTS_PER_INSTANCE + 1];

tSnoopTaskId             gSnoopTaskId; /* Snoop Task Id */
tIPvXAddr gNullAddr;
#ifdef L2RED_WANTED
tSnoopTaskId             gSnoopAuditTaskId; /* Snoop Audit Task Id */
#endif
tSnoopQId                gSnoopMsqQId;
tSnoopQId                gSnoopPktQId;

UINT1         gu1SnoopSystemStatus [SNOOP_MAX_INSTANCES];  /* Indicates whether 
                                                  * snooping Syetem control is
                                                  * SHUTDOWN or not for a given
                                                  * Instance, In SI case
                                                  * the resources will freed,
                                                  * when this status is
                                                  * SHUTDOWN.
                                                  * In MI case the Mem polls 
                                                  * are global with heap type,
                                                  * so resources above the MAX
                                                  * values will be allocated
                                                  * from system, and freed to
                                                  * system after the usage,
                                                  * created Mempolls will not be 
                                                  * deleted, even when the
                                                  * status is SHUTDOWN in all
                                                  * the instances */

UINT1         *gpSSMRepSendBuffer = NULL;        /* Misc buffer for contructing 
                                                    and sending SSM reports */  
UINT1         *gpSSMSrcInfoBuffer = NULL;         /* Src buffer for having 
                                                    source information */  
UINT1         *gpSnoopDataBuffer = NULL;          /* Data buffer for duplicating 
                                                    data information */  
UINT1         gu1SnoopInstanceId = 0;  
tSnoopSourceInfo  gaSrcInfo[SNOOP_MAX_MCAST_SRCS]; /* Array of source addresses 
                                                    for which registrations 
                                                    (Include /Exclude) have 
                                                    been for reported for this 
                                                    group. This will be used in
                                                    IP based forwarding */
UINT2         gu2SourceCount = 0;                /* Number of sources for which
                                                    (Include /Exclude) have 
                                                    been reported */
UINT4         gu4SendRecType = 0;                /* First two bytes reprsents 
                                                    Send record type and the 
                                                    next two bytes (MSB) 
                                                    represents the number of 
                                                    sources needs to be
                                                    reported */
                                                    
UINT1         gu1IsSameHost = 0;                 /* Used to indicate whether 
                                                    a fresh registration has 
                                                    come from the host which has
                                                    already registered */

 UINT1         gu1IsResetHost = 0;              /* Used to indicate whether report 
         is received from already existing 
         host */

UINT2         gu2MaxHwEntryCount = 0;             /* Indicates the number of 
                                                     hardware forwarding entries
                                                     programmed */ 

UINT1         gu1SnoopPortAction = 0;             /* Indicates the port action 
                                                     events*/
UINT1        gu1ConsModified = 0;
#ifdef L2RED_WANTED
tSnoopNodeState          gSnoopNodeStatus = SNOOP_IDLE_NODE;
#else
tSnoopNodeState          gSnoopNodeStatus = SNOOP_ACTIVE_NODE;
#endif
tSnoopPortBmp            gNullPortBitMap;
tSnoopIfPortBmp          gNullIfPortBitMap;
tSnoopSrcBmp             gNullSrcBmp;
tCVlanInfo               gSnoopCVlanInfo;
tSnoopVlanList           gSnoopNullVlanList;
tLocalPortList           gSnoopLocalPortList;
tSnoopIfPortBmp          gSnoopIfPortList;
UINT1                    gNullGrpAddr[IPVX_MAX_INET_ADDR_LEN];
UINT1                    gNullSrcAddr[IPVX_MAX_INET_ADDR_LEN];

UINT1                    gu1SnoopInit= SNOOP_FALSE;
UINT1                    gu1BatchingInProgress = SNOOP_FALSE;
UINT4                    gu4SnoopVlanId = 0;
UINT4                    gu4SwitchIp;        /* IP Address to be used as
                                                source IP in Proxy mode */ 
tRBTree                  gRtrPortTblRBRoot;   /* Implementation -
                                            * router port entries for all the instances are
                                            * stored globally in a RBTRee
                                            * The key for RBtree comparison function is
                                            * physical port number, vlan ID, address type.
                                            */

/* Temporary SLL Buffer to store G,S information received in reports */
tSnoopSll               gGrpRecList;

#ifdef L2RED_WANTED
UINT1                    gu1NumPeerPresent = 0;
tSnoopRedGlobalInfo      gSnoopRedGlobalInfo;
UINT1                    gu1SnoopRedGenQueryInProgress = SNOOP_FALSE;
UINT1                    gu1SnoopRedAuditInProgress = SNOOP_FALSE;
UINT1                    gu1SnoopRedBulkSyncInProgress = SNOOP_FALSE;
UINT1                    gu1SnoopLaCardRemovalProcessed = SNOOP_FALSE;
#endif

UINT4 gu4IgsSysLogId = 0;
tRBTree     gPortCfgTblEntry;  /* RBTree for Port configuration table with
                                  port number,inner vlanID and
                                  Addresstype as indices   */


/* For Trace messages */
UINT4         gu4SnoopTrcInstId = 0;  
UINT4         gu4SnoopTrcModule = 0;  

UINT4         gu4SnoopDbgInstId = 0;  
UINT4         gu4SnoopDbgModule = 0;  
CHR1          gacSnpSyslogmsg[MAX_LOG_STR_LEN];

#ifdef MLDS_WANTED
UINT1             gau1MldsAllHostIp[IPVX_MAX_INET_ADDR_LEN] =
           {0xff, 0x02, 0, 0 ,0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 1};    

UINT1             gau1MldsAllMldv2Rtr[IPVX_MAX_INET_ADDR_LEN] =
           {0xff, 0x02, 0, 0 ,0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0x16};    

UINT1             gau1MldsAllRtrIp[IPVX_MAX_INET_ADDR_LEN] =
           {0xff, 0x02, 0, 0 ,0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 2};    
#endif
const CHR1 *gau1TmrType[SNOOP_MAX_TIMER_COUNTS]= {
"NULL",
"Report Forward Timer",
"ASM Port Purge Timer",
"Router Port Purge Timer",
"Query Timer",
"Group Query Timer",
"Host Present Timer",
"IP Forward Timer",
"V1 Router Port Timer",
"V2 Router Port Timer",
"V3 Router Port Timer",
"Switchover Query Timer",
"Source List Cleanup Timer",
"Other Querier Present Interval",
"In Group Source Timer"
};


#endif /* _SNPGLOB_H */
