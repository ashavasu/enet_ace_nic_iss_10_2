/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                       */
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                              */
/* $Id: snpclipt.h,v 1.28 2017/10/06 13:26:21 siva Exp $*/
/*****************************************************************************/
/* |  FILE NAME             : snpclipt.h                                     */ 
/* |  PRINCIPAL AUTHOR      :                                                */ 
/* |  SUBSYSTEM NAME        : CLI                                            */ 
/* |  MODULE NAME           : Snooping configuration                         */ 
/* |  LANGUAGE              : C                                              */ 
/* |  TARGET ENVIRONMENT    :                                                */ 
/* |  DATE OF FIRST RELEASE :                                                */ 
/* |  DESCRIPTION           : This file includes all the Snooping CLI related*/ 
/* |                          prototype declarations.                        */ 
/* |                                                                         */ 
/*****************************************************************************/
/* CHANGE HISTORY :                                                          */ 
/*****************************************************************************/
/* VERSION    DATE/                   DESCRIPTION OF CHANGE/                 */
/*            MODIFIED BY             FAULT REPORT NO                        */ 
/*****************************************************************************/
/* 1.0.0.0    January 2005           Initial Creation                       */ 
/*****************************************************************************/

#ifndef SNOOPCLI_H
#define SNOOPCLI_H

INT4
SnoopCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, 
                          INT4 i4SnoopStatus);

INT4
SnoopCliSetMcastFwdMode PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                         INT4 i4McastFwdMode));

INT4
SnoopCliSetControlPlaneDriven (tCliHandle CliHandle, INT4 i4InstId, INT4 i4SnoopControlPlaneMode);

INT4
SnoopCliSetModuleStatus PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                         INT4 i4AddrType, INT4 i4SnoopStatus));
INT4

SnoopCliSetProxyStatus PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                               INT4 i4AddrType, INT4 i4SnoopStatus));
INT4
SnoopCliSetProxyReportingStatus PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4AddrType, INT4 i4SnoopStatus));
INT4
SnoopCliSetRtrPortPurgeInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetPortPurgeInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetReportFwdInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetRetryCount PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                       INT4 i4AddrType, UINT1 u1RetryCount));
INT4
SnoopCliSetGrpQueryInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                             INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetReportForwardAll PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                            INT4 i4AddrType, INT4 i4FwdAll));
INT4
SnoopCliSetQueryForwardAll PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                            INT4 i4AddrType, INT4 i4FwdAll));
INT4
SnoopCliSetVlanSnoopModuleStatus PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                                  INT4 i4VlanId, INT4 i4AddrType, 
                                  INT4 i4SnoopStatus));
INT4
SnoopCliSetVlanSnoopOperVersion PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                 INT4 i4VlanId, INT4 i4AddrType, 
                                 INT4 i4OperVersion));
INT4
SnoopCliSetVlanSnoopFastLeave PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                               INT4 i4VlanId, INT4 i4AddrType, 
                               INT4 i4FastLeave));

INT4
SnoopCliSetVlanSnoopQuerier PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
   INT4 i4VlanId, INT4 i4AddrType, UINT1 u1IpFlag, UINT4 u4IpAddr,
        INT4 i4SnoopStatus));

INT4
SnoopCliSetVlanQueryInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4VlanId, INT4 i4AddrType, INT4 i4Timeout));

INT4
SnoopCliSetVlanStartupQueryCount PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4VlanId, INT4 i4AddrType, INT4 i4QueryCount));
INT4
SnoopCliSetVlanStartupQueryInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4VlanId, INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetVlanOtherQuerierPrsntInterval PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                              INT4 i4VlanId, INT4 i4AddrType, INT4 i4Timeout));
INT4
SnoopCliSetVlanMaxResponseCode PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                                       INT4 i4VlanId, INT4 i4AddrType, 
                                       INT4 i4Timeout));

INT4
SnoopCliSetVlanRouterPorts PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
              INT4 i4VlanId, INT4 i4AddrType, 
       UINT1 *pu1RouterPorts));

INT4
SnoopCliSetVlanBlockedRtrPorts PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                       INT4 i4VlanId, INT4 i4AddrType,
                                       UINT1 *pu1RouterPorts));


INT4
SnoopCliDelVlanRouterPorts PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
              INT4 i4VlanId, INT4 i4AddrType, 
       UINT1 *pu1RouterPorts));

INT4
SnoopCliDelVlanBlkRtrPorts PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                               INT4 i4VlanId, INT4 i4AddrType,
                               UINT1 *pu1RouterPorts));

INT4
SnoopCliVlanResetStatistics (tCliHandle CliHandle, UINT4 u4InstId,
                              UINT1 u1AddrType, tVlanId VlanId);

 

INT4
SnoopCliSetTrace PROTO ((tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType, 
                         INT4 i4TraceStatus));
INT4
SnoopCliSetDebug PROTO ((tCliHandle CliHandle, INT4 i4InstId, INT4 i4AddrType,
                         INT4 i4DebugStatus));

INT4
SnoopCliShowVlanRouterPorts PROTO ((tCliHandle CliHandle, UINT4 u4Context, 
                                    INT4 i4AddrType, tVlanId VlanId,
                                    INT4 i4PrintRtPortInfo));

INT4
SnoopCliShowVlanBlkRtrPorts PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                                    INT4 i4AddrType, tSnoopVlanId VlanId));

INT4
SnoopCliShowGlobalInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
          INT4 i4AddrType));

INT4
SnoopCliShowVlanInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                             INT4 i4AddrType, tVlanId VlanId));
INT4
SnoopCliShowGroupInformation PROTO ((tCliHandle CliHandle, 
                                     UINT4 u4Instance, UINT1 u1AddressType, 
                                     tVlanId VlanId, UINT1 *pu1GroupIpAddr,
                                     INT4 i4EntryType));
INT4
SnoopCliShowForwardEntries PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
                                   INT4 i4AddrType, tVlanId VlanId,
                                   INT4 i4EntryType));
INT4
SnoopCliShowMacForwardTable PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                                    INT4 i4AddrType, tVlanId VlanId,
                                     INT4 i4EntryType));
INT4
SnoopCliShowIpForwardTable PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                                   INT4 i4AddrType, tVlanId VlanId,
                                   INT4 i4EntryType));

INT4
SnoopCliShowForwardSummary (tCliHandle CliHandle, INT4 i4InstId,
        INT4 i4AddrType);

INT4
SnoopCliShowGroupSummary (tCliHandle CliHandle, INT4 i4InstId,INT4 i4AddrType);

INT4
SnoopCliShowStatistics PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, 
                               INT4 i4AddrType, tVlanId VlanId));
INT4
SnoopCliShowMVlanMapping PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                                 INT4 i4AddrType));


INT4 
SnoopCliSetSystemEnhMode PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT1 u1AddrType, INT4 i4SnoopSystemEnhMode));

INT4 
SnoopCliSetSystemSparseMode PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT1 u1AddrType, INT4 i4SnoopSystemSparseMode));

INT4
SnoopSetCliPortCfgProfileId PROTO ((tCliHandle CliHandle, INT4 u4IfIndex,
                                    UINT1  u1AddrType, UINT4 u4ProfileId, 
                                    tSnoopTag VlanId));

INT4
SnoopCliShowPortCfgEntries PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                                   UINT4 u4IfIndex, UINT1 u1AddressType, 
                                   tSnoopTag VlanId));
INT4 SnoopVlanSetRobustnessValue(tCliHandle CliHandle,UINT4 u4ContextId,
                                         tVlanId VlanId,INT4 i4AddrType,
                                         UINT4 i4SetRobustnessVal); 

INT4
SnoopSetCliPortCfgLeaveMode PROTO ((tCliHandle CliHandle, INT4 u4IfIndex,
                                    UINT1 u1AddrType, INT4 i4LeaveMode, 
                                    tSnoopTag VlanId));
INT4
SnoopCliUpdatePortCfgEntry PROTO ((tCliHandle CliHandle, UINT4 u4IfIndex, 
       tSnoopTag VlanId, UINT1 u1AddrType, 
       UINT1 u1Status));

INT4
SnoopSetCliPortCfgRateLimit PROTO ((tCliHandle CliHandle, INT4 u4IfIndex,
                                    UINT1 u1AddrType, UINT4 u4RateLimit, 
                                    tSnoopTag VlanId));

INT4
SnoopSetCliPortCfgMaxLimit PROTO ((tCliHandle CliHandle, INT4 u4IfIndex,
                                   UINT1 u1AddrType,UINT1 u1LimitType, 
                                   INT4 i4MaxLimit, tSnoopTag VlanId));

VOID
SnoopCliPrintPortCfgInformation (tCliHandle CliHandle, UINT4 u4IfIndex,
                                 UINT1 u1AddressType, tVlanId InnerVlanId);
VOID
IssSnoopShowDebugging PROTO ((tCliHandle CliHandle, UINT1 u1AddrType));

INT4
SnoopCliUpdateVlanEntry PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
           INT4 i4VlanId, INT4 i4AddrType, 
    UINT1 u1Status));
INT4
SnoopCliPrintPortList PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                              tSNMP_OCTET_STRING_TYPE VlanRouterPortList,
                              tSNMP_OCTET_STRING_TYPE StaticVlanRtrPortList,
                              UINT4 u4VlanId, INT4 i4AddrType, UINT2 *pu2NewLineFlag));
INT4
SnoopCliPrintPortListInfo PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                       tSNMP_OCTET_STRING_TYPE VlanRouterPortList,
                       tSNMP_OCTET_STRING_TYPE StaticVlanRtrPortList, 
                       UINT4 u4VlanId, INT4 i4AddrType));

INT4
SnoopCliPrintBlkPortList PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                 tSNMP_OCTET_STRING_TYPE 
                                 pBlockedVlanRtrPortList));

VOID
SnoopCliPrintGroupInformation PROTO ((tCliHandle CliHandle, UINT4 u4Instance,
                                      tSnoopGroupEntry * pSnoopGroupEntry));
VOID
SnoopCliPrintSources PROTO ((tCliHandle CliHandle,UINT4 u4Instance, 
                             tSnoopSrcBmp SrcBitmap, 
                             UINT1 u1AddressType, BOOL1 bGrpPort));

INT4
SnoopShowRunningConfig PROTO ((tCliHandle, UINT4, UINT4));

INT4
SnoopShowRunningConfigTable PROTO ((tCliHandle CliHandle, INT4 i4AddressType, 
                                    UINT4 u4ContextId));

INT4
SnoopShowRunningConfigVlanTable PROTO ((tCliHandle, INT4, UINT4, UINT2));

INT4
SnoopShowRunningConfigRouterPortTable PROTO ((tCliHandle, INT4, INT4, UINT4));

INT4
SnoopShowRunningConfigInterface PROTO ((tCliHandle, INT4, UINT4));

INT4
SnoopShowRunningConfigInterfaceDetails PROTO ((tCliHandle, INT4, INT4));

INT4
SnoopShowRunningConfigVlanStaticMacstTable PROTO ((tCliHandle, INT4, INT4));

INT4
SnoopDisplayPortConfigurations PROTO ((tCliHandle, INT4, UINT4, INT4));

INT4
SnoopCliSetSendQuery PROTO ((tCliHandle CliHandle, INT4 i4InstId,
                                     UINT1 u1AddressType, INT4 i4SnoopQueryStatus));



INT4
SnoopCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                            UINT4 *pu4Context);

INT4
SnoopCliSetVlanRtrPortOperVersion PROTO ((tCliHandle CliHandle,
                                INT4 i4VlanId, INT4 i4AddrType,
                                UINT1 *pu1RouterPorts, INT4 i4OperVersion));

INT4
SnoopCliSetVlanRtrPortPurgeInterval PROTO ((tCliHandle CliHandle,
                                INT4 i4VlanId, INT4 i4AddrType,
                                UINT1 *pu1RouterPorts, INT4 i4PurgeInterval));

INT4 SnoopCliSetMVlanStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4AddressType, INT4 i4Status);
INT4
SnoopCliMapMVlanToProfile (tCliHandle CliHandle, INT4 i4InstId, INT4 i4VlanId,
                           INT4 i4AddrType, UINT4 u4ProfileId);

INT4
SnoopCliUnMapMVlanFromProfile (tCliHandle CliHandle, INT4 i4InstId, 
                               INT4 i4VlanId, INT4 i4AddrType);

INT4 SnoopCliSetFilterStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                              INT4 i4AddressType, INT4 i4Status);

INT4
SnoopCliShowMcastReceiverInfo PROTO ((tCliHandle, UINT4, UINT1,
                                      UINT4, UINT1 *));

INT4
SnoopCliSetLeaveConfigLevel PROTO ((tCliHandle, INT4, INT4));

INT4
SnoopCliSetReportConfigLevel PROTO ((tCliHandle, INT4, INT4));

INT4
SnoopBasicShowRunningConfig PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));

INT4
SnoopCliGetNextPortTableIndex PROTO ((UINT4, INT4, UINT4, INT4,
                               INT4 *, UINT4 *, INT4 *));


UINT1 * SnoopPrintAddress(UINT1 *pu1Addr, UINT1 u1Afi);

UINT1 * SnoopPrintIPvxAddress(tIPvXAddr Addr);

/***********For Static Entry************************************/

INT4
SnoopCliVlanSetStaticMcastGrpEntry PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
    INT4 VlanId, INT4 i4AddrType, 
    UINT1 *au1GrpAddr, UINT1 *pu1RouterPorts));

INT4 
SnoopCliVlanDelStaticMcastGrpEntry PROTO ((tCliHandle CliHandle, INT4 i4InstId, 
    INT4 VlanId, INT4 i4AddrType, 
    UINT1 *au1GrpAddr, UINT1 *pu1RouterPorts));


#endif
