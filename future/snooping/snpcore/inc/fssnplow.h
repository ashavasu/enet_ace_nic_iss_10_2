/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssnplow.h,v 1.20 2017/08/01 13:50:24 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsSnoopInstanceGlobalTable. */
INT1
nmhValidateIndexInstanceFsSnoopInstanceGlobalTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopInstanceGlobalTable  */

INT1
nmhGetFirstIndexFsSnoopInstanceGlobalTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopInstanceGlobalTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopInstanceGlobalMcastFwdMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceGlobalSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceGlobalLeaveConfigLevel ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceGlobalEnhancedMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceGlobalSparseMode ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceControlPlaneDriven ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsSnoopInstanceGlobalReportProcessConfigLevel ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopInstanceGlobalMcastFwdMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceGlobalSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceGlobalLeaveConfigLevel ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceGlobalEnhancedMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceGlobalSparseMode ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceControlPlaneDriven ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsSnoopInstanceGlobalReportProcessConfigLevel ARG_LIST((INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopInstanceGlobalMcastFwdMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceGlobalSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceGlobalLeaveConfigLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceGlobalEnhancedMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceGlobalSparseMode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceControlPlaneDriven ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopInstanceGlobalReportProcessConfigLevel ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopInstanceGlobalTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopInstanceConfigTable. */
INT1
nmhValidateIndexInstanceFsSnoopInstanceConfigTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopInstanceConfigTable  */

INT1
nmhGetFirstIndexFsSnoopInstanceConfigTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopInstanceConfigTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopProxyReportingStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopRouterPortPurgeInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopPortPurgeInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopReportForwardInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopRetryCount ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopGrpQueryInterval ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopReportFwdOnAllPorts ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopTraceOption ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopDebugOption ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopOperStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopSendQueryOnTopoChange ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopSendLeaveOnTopoChange ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopFilterStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopMulticastVlanStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopProxyStatus ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopQueryFwdOnAllPorts ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopFwdGroupsCnt ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopProxyReportingStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopRouterPortPurgeInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopPortPurgeInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopReportForwardInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopRetryCount ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopGrpQueryInterval ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopReportFwdOnAllPorts ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopTraceOption ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopDebugOption ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopSendQueryOnTopoChange ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopSendLeaveOnTopoChange ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopFilterStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopMulticastVlanStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopProxyStatus ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopQueryFwdOnAllPorts ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopProxyReportingStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopRouterPortPurgeInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopPortPurgeInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopReportForwardInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopRetryCount ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopGrpQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopReportFwdOnAllPorts ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopTraceOption ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));


INT1
nmhTestv2FsSnoopDebugOption ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopSendQueryOnTopoChange ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopSendLeaveOnTopoChange ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopFilterStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopMulticastVlanStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopProxyStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopQueryFwdOnAllPorts ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopInstanceConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopVlanMcastMacFwdTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanMcastMacFwdTable ARG_LIST((INT4  , INT4  , INT4  , tMacAddr ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanMcastMacFwdTable  */

INT1
nmhGetFirstIndexFsSnoopVlanMcastMacFwdTable ARG_LIST((INT4 * , INT4 * , INT4 * , tMacAddr * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanMcastMacFwdTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tMacAddr , tMacAddr * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanMcastMacFwdPortList ARG_LIST((INT4  , INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanMcastMacFwdLocalPortList ARG_LIST((INT4  , INT4  , INT4  , tMacAddr ,tSNMP_OCTET_STRING_TYPE * ));
/* MAC FLAG*/
INT1
nmhGetFsSnoopVlanMcastMacFwdEntryFlag ARG_LIST((INT4  , INT4  , INT4  , tMacAddr  , INT4 *));

/* Proto Validate Index Instance for FsSnoopVlanMcastIpFwdTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanMcastIpFwdTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanMcastIpFwdTable  */

INT1
nmhGetFirstIndexFsSnoopVlanMcastIpFwdTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanMcastIpFwdTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanMcastIpFwdPortList ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/*IP FLAG*/
INT1
nmhGetFsSnoopVlanMcastIpFwdEntryFlag ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsSnoopVlanRouterTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanRouterTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanRouterTable  */

INT1
nmhGetFirstIndexFsSnoopVlanRouterTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanRouterTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanRouterPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanRouterLocalPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsSnoopVlanFilterTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanFilterTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanFilterTable  */

INT1
nmhGetFirstIndexFsSnoopVlanFilterTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanFilterTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanSnoopStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanOperatingVersion ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanCfgOperVersion ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanFastLeave ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanQuerier ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanCfgQuerier ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanStartupQueryCount ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanStartupQueryInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanQueryInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanOtherQuerierPresentInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanQuerierIpAddress ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopVlanQuerierIpFlag ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanElectedQuerier ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopVlanRobustnessValue ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopVlanRtrPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanRowStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopVlanSnoopStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanCfgOperVersion ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanFastLeave ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanCfgQuerier ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanStartupQueryCount ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanStartupQueryInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanQueryInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanOtherQuerierPresentInterval ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanQuerierIpAddress ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopVlanQuerierIpFlag ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));


INT1
nmhSetFsSnoopVlanRobustnessValue ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopVlanRtrPortList ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnoopVlanRowStatus ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopVlanSnoopStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanCfgOperVersion ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanFastLeave ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanCfgQuerier ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanStartupQueryCount ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanStartupQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanQueryInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanOtherQuerierPresentInterval ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanQuerierIpAddress ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopVlanQuerierIpFlag ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanRobustnessValue ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopVlanRtrPortList ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnoopVlanRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopVlanFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopVlanMcastGroupTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanMcastGroupTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanMcastGroupTable  */

INT1
nmhGetFirstIndexFsSnoopVlanMcastGroupTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanMcastGroupTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanMcastGroupPortList ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanMcastGroupLocalPortList ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsSnoopVlanMcastReceiverTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanMcastReceiverTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanMcastReceiverTable  */

INT1
nmhGetFirstIndexFsSnoopVlanMcastReceiverTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanMcastReceiverTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanMcastReceiverFilterMode ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for FsSnoopVlanIpFwdTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanIpFwdTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanIpFwdTable  */

INT1
nmhGetFirstIndexFsSnoopVlanIpFwdTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanIpFwdTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanIpFwdPortList ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanIpFwdLocalPortList ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsSnoopVlanFilterXTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanFilterXTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanFilterXTable  */

INT1
nmhGetFirstIndexFsSnoopVlanFilterXTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanFilterXTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanBlkRtrPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanFilterMaxLimitType ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanFilterMaxLimit ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopVlanFilter8021pPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanFilterDropReports ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanMulticastProfileId ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopVlanPortPurgeInterval ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanMaxResponseTime ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopVlanRtrLocalPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanBlkRtrLocalPortList ARG_LIST((INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopVlanBlkRtrPortList ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnoopVlanFilterMaxLimitType ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanFilterMaxLimit ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopVlanFilter8021pPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanFilterDropReports ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanMulticastProfileId ARG_LIST((INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopVlanMaxResponseTime ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopVlanRtrLocalPortList ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnoopVlanBlkRtrLocalPortList ARG_LIST((INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopVlanBlkRtrPortList ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnoopVlanFilterMaxLimitType ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanFilterMaxLimit ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopVlanFilter8021pPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanFilterDropReports ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanMulticastProfileId ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopVlanMaxResponseTime ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopVlanRtrLocalPortList ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnoopVlanBlkRtrLocalPortList ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopVlanFilterXTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopVlanStaticMcastGrpTable. */
INT1
nmhValidateIndexInstanceFsSnoopVlanStaticMcastGrpTable ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsSnoopVlanStaticMcastGrpTable  */

INT1
nmhGetFirstIndexFsSnoopVlanStaticMcastGrpTable ARG_LIST((INT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopVlanStaticMcastGrpTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopVlanStaticMcastGrpPortList ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSnoopVlanStaticMcastGrpRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopVlanStaticMcastGrpPortList ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsSnoopVlanStaticMcastGrpRowStatus ARG_LIST((INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopVlanStaticMcastGrpPortList ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsSnoopVlanStaticMcastGrpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopVlanStaticMcastGrpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopStatsTable. */
INT1
nmhValidateIndexInstanceFsSnoopStatsTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopStatsTable  */

INT1
nmhGetFirstIndexFsSnoopStatsTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopStatsRxGenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxGrpQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxGrpAndSrcQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxAsmReports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmReports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmIsInMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmIsExMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmToInMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmToExMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmAllowMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxSsmBlockMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxAsmLeaves ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxGenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxGrpQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxGrpAndSrcQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsFwdGenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsFwdGrpQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxAsmReports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxSsmReports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsTxAsmLeaves ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDroppedPkts ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsUnsuccessfulJoins ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsActiveJoins ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsActiveGroups ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsRxV1Report ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
                                                                                                                              INT1
nmhGetFsSnoopStatsRxV2Report ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
                                                                                                                              INT1                                                                                                                          nmhGetFsSnoopStatsTxV1Report ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
                                                                                                                              INT1                                                                                                                          nmhGetFsSnoopStatsTxV2Report ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
                                                                                                                              INT1
nmhGetFsSnoopStatsDropV1GenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));
                                                                                                                              INT1
nmhGetFsSnoopStatsDropV2GenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropV3GenQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropV2GrpQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropV3GrpQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropGrpAndSrcQueries ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropV1Reports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropV2Reports ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmIsInMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmIsExMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmToInMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmToExMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmAllowMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropSsmBlockMsgs ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopStatsDropAsmLeaves ARG_LIST((INT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsSnoopPortTable. */
INT1
nmhValidateIndexInstanceFsSnoopPortTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopPortTable  */

INT1
nmhGetFirstIndexFsSnoopPortTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopPortLeaveMode ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopPortRateLimit ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopPortMaxLimitType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopPortMaxLimit ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopPortProfileId ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopPortMemberCnt ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopPortMaxBandwidthLimit ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopPortDropReports ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopPortRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopPortLeaveMode ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopPortRateLimit ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopPortMaxLimitType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopPortMaxLimit ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopPortProfileId ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopPortMaxBandwidthLimit ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopPortDropReports ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopPortRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopPortLeaveMode ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopPortRateLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopPortMaxLimitType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopPortMaxLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopPortProfileId ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopPortMaxBandwidthLimit ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopPortDropReports ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopPortRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopEnhPortTable. */
INT1
nmhValidateIndexInstanceFsSnoopEnhPortTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopEnhPortTable  */

INT1
nmhGetFirstIndexFsSnoopEnhPortTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopEnhPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopEnhPortLeaveMode ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopEnhPortRateLimit ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopEnhPortMaxLimitType ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopEnhPortMaxLimit ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopEnhPortProfileId ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopEnhPortMemberCnt ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopEnhPortMaxBandwidthLimit ARG_LIST((INT4  , UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsSnoopEnhPortDropReports ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopEnhPortRowStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopEnhPortLeaveMode ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopEnhPortRateLimit ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopEnhPortMaxLimitType ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopEnhPortMaxLimit ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopEnhPortProfileId ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopEnhPortMaxBandwidthLimit ARG_LIST((INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsSnoopEnhPortDropReports ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopEnhPortRowStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopEnhPortLeaveMode ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopEnhPortRateLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopEnhPortMaxLimitType ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopEnhPortMaxLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopEnhPortProfileId ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopEnhPortMaxBandwidthLimit ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsSnoopEnhPortDropReports ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopEnhPortRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopEnhPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopRtrPortTable. */
INT1
nmhValidateIndexInstanceFsSnoopRtrPortTable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopRtrPortTable  */

INT1
nmhGetFirstIndexFsSnoopRtrPortTable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopRtrPortTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopRtrPortOperVersion ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopRtrPortCfgOperVersion ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopOlderQuerierInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsSnoopV3QuerierInterval ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSnoopRtrPortCfgOperVersion ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsSnoopOlderQuerierInterval ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSnoopRtrPortCfgOperVersion ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsSnoopOlderQuerierInterval ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSnoopRtrPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsSnoopTrapObjectsTable. */
INT1
nmhValidateIndexInstanceFsSnoopTrapObjectsTable ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsSnoopTrapObjectsTable  */

INT1
nmhGetFirstIndexFsSnoopTrapObjectsTable ARG_LIST((INT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsSnoopTrapObjectsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSnoopTrapHwErrType ARG_LIST((INT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));
